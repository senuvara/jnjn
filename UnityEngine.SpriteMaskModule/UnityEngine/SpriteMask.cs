using System.Runtime.CompilerServices;
using UnityEngine.Scripting;

namespace UnityEngine
{
	/// <summary>
	///   <para>A component for masking Sprites and Particles.</para>
	/// </summary>
	[RejectDragAndDropMaterial]
	public sealed class SpriteMask : Renderer
	{
		/// <summary>
		///   <para>The Sprite used to define the mask.</para>
		/// </summary>
		public Sprite sprite
		{
			[MethodImpl(MethodImplOptions.InternalCall)]
			[GeneratedByOldBindingsGenerator]
			get;
			[MethodImpl(MethodImplOptions.InternalCall)]
			[GeneratedByOldBindingsGenerator]
			set;
		}

		/// <summary>
		///   <para>The minimum alpha value used by the mask to select the area of influence defined over the mask's sprite.</para>
		/// </summary>
		public float alphaCutoff
		{
			[MethodImpl(MethodImplOptions.InternalCall)]
			[GeneratedByOldBindingsGenerator]
			get;
			[MethodImpl(MethodImplOptions.InternalCall)]
			[GeneratedByOldBindingsGenerator]
			set;
		}

		/// <summary>
		///   <para>Mask sprites from front to back sorting values only.</para>
		/// </summary>
		public bool isCustomRangeActive
		{
			[MethodImpl(MethodImplOptions.InternalCall)]
			[GeneratedByOldBindingsGenerator]
			get;
			[MethodImpl(MethodImplOptions.InternalCall)]
			[GeneratedByOldBindingsGenerator]
			set;
		}

		/// <summary>
		///   <para>Unique ID of the sorting layer defining the start of the custom range.</para>
		/// </summary>
		public int frontSortingLayerID
		{
			[MethodImpl(MethodImplOptions.InternalCall)]
			[GeneratedByOldBindingsGenerator]
			get;
			[MethodImpl(MethodImplOptions.InternalCall)]
			[GeneratedByOldBindingsGenerator]
			set;
		}

		/// <summary>
		///   <para>Order within the front sorting layer defining the start of the custom range.</para>
		/// </summary>
		public int frontSortingOrder
		{
			[MethodImpl(MethodImplOptions.InternalCall)]
			[GeneratedByOldBindingsGenerator]
			get;
			[MethodImpl(MethodImplOptions.InternalCall)]
			[GeneratedByOldBindingsGenerator]
			set;
		}

		/// <summary>
		///   <para>Unique ID of the sorting layer defining the end of the custom range.</para>
		/// </summary>
		public int backSortingLayerID
		{
			[MethodImpl(MethodImplOptions.InternalCall)]
			[GeneratedByOldBindingsGenerator]
			get;
			[MethodImpl(MethodImplOptions.InternalCall)]
			[GeneratedByOldBindingsGenerator]
			set;
		}

		/// <summary>
		///   <para>Order within the back sorting layer defining the end of the custom range.</para>
		/// </summary>
		public int backSortingOrder
		{
			[MethodImpl(MethodImplOptions.InternalCall)]
			[GeneratedByOldBindingsGenerator]
			get;
			[MethodImpl(MethodImplOptions.InternalCall)]
			[GeneratedByOldBindingsGenerator]
			set;
		}

		internal Bounds GetSpriteBounds()
		{
			INTERNAL_CALL_GetSpriteBounds(this, out Bounds value);
			return value;
		}

		[MethodImpl(MethodImplOptions.InternalCall)]
		[GeneratedByOldBindingsGenerator]
		private static extern void INTERNAL_CALL_GetSpriteBounds(SpriteMask self, out Bounds value);
	}
}
