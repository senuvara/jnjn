using Newtonsoft.Json.Shims;

namespace System.ComponentModel
{
	[Preserve]
	public enum NotifyCollectionChangedAction
	{
		Add,
		Remove,
		Replace,
		Move,
		Reset
	}
}
