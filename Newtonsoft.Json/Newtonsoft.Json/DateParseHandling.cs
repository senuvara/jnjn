using Newtonsoft.Json.Shims;

namespace Newtonsoft.Json
{
	[Preserve]
	public enum DateParseHandling
	{
		None,
		DateTime,
		DateTimeOffset
	}
}
