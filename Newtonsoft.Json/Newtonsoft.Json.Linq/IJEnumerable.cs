using Newtonsoft.Json.Shims;
using System.Collections;
using System.Collections.Generic;

namespace Newtonsoft.Json.Linq
{
	[Preserve]
	public interface IJEnumerable<T> : IEnumerable<T>, IEnumerable where T : JToken
	{
		IJEnumerable<JToken> this[object key]
		{
			get;
		}
	}
}
