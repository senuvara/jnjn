using Newtonsoft.Json.Shims;

namespace Newtonsoft.Json.Linq
{
	[Preserve]
	public enum MergeArrayHandling
	{
		Concat,
		Union,
		Replace,
		Merge
	}
}
