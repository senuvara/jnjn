using System;

namespace Sirenix.Utilities
{
	[Flags]
	public enum AssemblyTypeFlags
	{
		None = 0x0,
		UserTypes = 0x1,
		PluginTypes = 0x2,
		UnityTypes = 0x4,
		UserEditorTypes = 0x8,
		PluginEditorTypes = 0x10,
		UnityEditorTypes = 0x20,
		OtherTypes = 0x40,
		CustomTypes = 0x1B,
		GameTypes = 0x47,
		EditorTypes = 0x38,
		All = 0x7F
	}
}
