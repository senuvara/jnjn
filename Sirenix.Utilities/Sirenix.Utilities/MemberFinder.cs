using System;
using System.Collections.Generic;
using System.Globalization;
using System.Linq;
using System.Reflection;

namespace Sirenix.Utilities
{
	public class MemberFinder
	{
		[Flags]
		private enum ConditionFlags
		{
			None = 0x0,
			IsStatic = 0x2,
			IsProperty = 0x4,
			IsInstance = 0x8,
			IsDeclaredOnly = 0x10,
			HasNoParamaters = 0x20,
			IsMethod = 0x40,
			IsField = 0x80,
			IsPublic = 0x100,
			IsNonPublic = 0x200
		}

		private Type type;

		private Cache<MemberFinder> cache;

		private ConditionFlags conditionFlags;

		private string name;

		private Type returnType;

		private List<Type> paramTypes = new List<Type>();

		private bool returnTypeCanInherit;

		public MemberFinder()
		{
		}

		public MemberFinder(Type type)
		{
			Start(type, null);
		}

		public static MemberFinder Start<T>()
		{
			Cache<MemberFinder> cache = Cache<MemberFinder>.Claim();
			return cache.Value.Start(typeof(T), cache);
		}

		public static MemberFinder Start(Type type)
		{
			Cache<MemberFinder> cache = Cache<MemberFinder>.Claim();
			return cache.Value.Start(type, cache);
		}

		public MemberFinder HasNoParameters()
		{
			conditionFlags |= ConditionFlags.HasNoParamaters;
			return this;
		}

		public MemberFinder IsDeclaredOnly()
		{
			conditionFlags |= ConditionFlags.IsDeclaredOnly;
			return this;
		}

		public MemberFinder HasParameters(Type param1)
		{
			conditionFlags |= ConditionFlags.IsMethod;
			paramTypes.Add(param1);
			return this;
		}

		public MemberFinder HasParameters(Type param1, Type param2)
		{
			conditionFlags |= ConditionFlags.IsMethod;
			paramTypes.Add(param1);
			paramTypes.Add(param2);
			return this;
		}

		public MemberFinder HasParameters(Type param1, Type param2, Type param3)
		{
			conditionFlags |= ConditionFlags.IsMethod;
			paramTypes.Add(param1);
			paramTypes.Add(param2);
			paramTypes.Add(param3);
			return this;
		}

		public MemberFinder HasParameters(Type param1, Type param2, Type param3, Type param4)
		{
			conditionFlags |= ConditionFlags.IsMethod;
			paramTypes.Add(param1);
			paramTypes.Add(param2);
			paramTypes.Add(param3);
			paramTypes.Add(param4);
			return this;
		}

		public MemberFinder HasParameters<T>()
		{
			return HasParameters(typeof(T));
		}

		public MemberFinder HasParameters<T1, T2>()
		{
			return HasParameters(typeof(T1), typeof(T2));
		}

		public MemberFinder HasParameters<T1, T2, T3>()
		{
			return HasParameters(typeof(T1), typeof(T2), typeof(T3));
		}

		public MemberFinder HasParameters<T1, T2, T3, T4>()
		{
			return HasParameters(typeof(T1), typeof(T2), typeof(T3), typeof(T4));
		}

		public MemberFinder HasReturnType(Type returnType, bool inherit = false)
		{
			returnTypeCanInherit = inherit;
			this.returnType = returnType;
			return this;
		}

		public MemberFinder HasReturnType<T>(bool inherit = false)
		{
			return HasReturnType(typeof(T), inherit);
		}

		public MemberFinder IsFieldOrProperty()
		{
			IsField();
			IsProperty();
			return this;
		}

		public MemberFinder IsStatic()
		{
			conditionFlags |= ConditionFlags.IsStatic;
			return this;
		}

		public MemberFinder IsInstance()
		{
			conditionFlags |= ConditionFlags.IsInstance;
			return this;
		}

		public MemberFinder IsNamed(string name)
		{
			this.name = name;
			return this;
		}

		public MemberFinder IsProperty()
		{
			conditionFlags |= ConditionFlags.IsProperty;
			return this;
		}

		public MemberFinder IsMethod()
		{
			conditionFlags |= ConditionFlags.IsMethod;
			return this;
		}

		public MemberFinder IsField()
		{
			conditionFlags |= ConditionFlags.IsField;
			return this;
		}

		public MemberFinder IsPublic()
		{
			conditionFlags |= ConditionFlags.IsPublic;
			return this;
		}

		public MemberFinder IsNonPublic()
		{
			conditionFlags |= ConditionFlags.IsNonPublic;
			return this;
		}

		public bool IsNamed(object customDeleteFunction)
		{
			throw new NotImplementedException();
		}

		public MemberFinder ReturnsVoid()
		{
			conditionFlags |= ConditionFlags.IsMethod;
			return HasReturnType(typeof(void));
		}

		public T GetMember<T>() where T : MemberInfo
		{
			string errorMessage = null;
			return GetMember<T>(out errorMessage);
		}

		public T GetMember<T>(out string errorMessage) where T : MemberInfo
		{
			TryGetMember(out T memberInfo, out errorMessage);
			return memberInfo;
		}

		public MemberInfo GetMember(out string errorMessage)
		{
			TryGetMember(out MemberInfo memberInfo, out errorMessage);
			return memberInfo;
		}

		public bool TryGetMember<T>(out T memberInfo, out string errorMessage) where T : MemberInfo
		{
			MemberInfo memberInfo2;
			bool result = TryGetMember(out memberInfo2, out errorMessage);
			memberInfo = (memberInfo2 as T);
			return result;
		}

		public bool TryGetMember(out MemberInfo memberInfo, out string errorMessage)
		{
			if (TryGetMembers(out MemberInfo[] memberInfos, out errorMessage))
			{
				memberInfo = memberInfos[0];
				return true;
			}
			memberInfo = null;
			return false;
		}

		public bool TryGetMembers(out MemberInfo[] memberInfos, out string errorMessage)
		{
			try
			{
				IEnumerable<MemberInfo> source = Enumerable.Empty<MemberInfo>();
				BindingFlags bindingFlags = HasCondition(ConditionFlags.IsDeclaredOnly) ? BindingFlags.DeclaredOnly : BindingFlags.FlattenHierarchy;
				bool flag = HasCondition(ConditionFlags.HasNoParamaters);
				bool flag2 = HasCondition(ConditionFlags.IsInstance);
				bool flag3 = HasCondition(ConditionFlags.IsStatic);
				bool flag4 = HasCondition(ConditionFlags.IsPublic);
				bool flag5 = HasCondition(ConditionFlags.IsNonPublic);
				bool isMethod = HasCondition(ConditionFlags.IsMethod);
				bool isField = HasCondition(ConditionFlags.IsField);
				bool isProperty = HasCondition(ConditionFlags.IsProperty);
				if (!flag4 && !flag5)
				{
					flag4 = true;
					flag5 = true;
				}
				if (!flag3 && !flag2)
				{
					flag3 = true;
					flag2 = true;
				}
				if (!(isField || isProperty || isMethod))
				{
					isMethod = true;
					isField = true;
					isProperty = true;
				}
				if (flag2)
				{
					bindingFlags |= BindingFlags.Instance;
				}
				if (flag3)
				{
					bindingFlags |= BindingFlags.Static;
				}
				if (flag4)
				{
					bindingFlags |= BindingFlags.Public;
				}
				if (flag5)
				{
					bindingFlags |= BindingFlags.NonPublic;
				}
				if (isMethod && isField && isProperty)
				{
					source = ((name != null) ? (from n in type.GetAllMembers(bindingFlags)
						where n.Name == name
						select n) : type.GetAllMembers(bindingFlags));
					if (flag)
					{
						source = source.Where((MemberInfo x) => !(x is MethodInfo) || (x as MethodInfo).GetParameters().Length == 0);
					}
				}
				else
				{
					if (isMethod)
					{
						IEnumerable<MethodInfo> source2 = (name == null) ? type.GetAllMembers<MethodInfo>(bindingFlags) : (from x in type.GetAllMembers<MethodInfo>(bindingFlags)
							where x.Name == name
							select x);
						if (flag)
						{
							source2 = source2.Where((MethodInfo x) => x.GetParameters().Length == 0);
						}
						else if (paramTypes.Count > 0)
						{
							source2 = source2.Where((MethodInfo x) => x.HasParamaters(paramTypes));
						}
						source = source2.OfType<MemberInfo>();
					}
					if (isField)
					{
						source = ((name != null) ? source.AppendWith((from n in type.GetAllMembers<FieldInfo>(bindingFlags)
							where n.Name == name
							select n).Cast<MemberInfo>()) : source.AppendWith(type.GetAllMembers<FieldInfo>(bindingFlags).Cast<MemberInfo>()));
					}
					if (isProperty)
					{
						source = ((name != null) ? source.AppendWith((from n in type.GetAllMembers<PropertyInfo>(bindingFlags)
							where n.Name == name
							select n).Cast<MemberInfo>()) : source.AppendWith(type.GetAllMembers<PropertyInfo>(bindingFlags).Cast<MemberInfo>()));
					}
				}
				if (returnType != null)
				{
					source = ((!returnTypeCanInherit) ? source.Where((MemberInfo x) => x.GetReturnType() == returnType) : source.Where((MemberInfo x) => x.GetReturnType().InheritsFrom(returnType)));
				}
				memberInfos = source.ToArray();
				if (memberInfos != null && memberInfos.Length != 0)
				{
					errorMessage = null;
					return true;
				}
				MemberInfo memberInfo = (name == null) ? null : type.GetMember(name, BindingFlags.Instance | BindingFlags.Static | BindingFlags.Public | BindingFlags.NonPublic | BindingFlags.FlattenHierarchy).FirstOrDefault((MemberInfo t) => (t is MethodInfo && isMethod) || (t is FieldInfo && isField) || (t is PropertyInfo && isProperty));
				if (memberInfo != null)
				{
					string text = memberInfo.IsStatic() ? "Static " : "Non-static ";
					if (flag && memberInfo is MethodInfo && (memberInfo as MethodInfo).GetParameters().Length != 0)
					{
						errorMessage = text + "method " + name + " can not take parameters.";
						return false;
					}
					if (isMethod && paramTypes.Count > 0 && memberInfo is MethodInfo && !(memberInfo as MethodInfo).HasParamaters(paramTypes))
					{
						errorMessage = text + "method " + name + " must have the following parameters: " + string.Join(", ", paramTypes.Select((Type x) => x.GetNiceName()).ToArray()) + ".";
						return false;
					}
					if (returnType != null && returnType != memberInfo.GetReturnType())
					{
						if (returnTypeCanInherit)
						{
							errorMessage = text + memberInfo.MemberType.ToString().ToLower(CultureInfo.InvariantCulture) + " " + name + " must have a return type that is assignable from " + returnType.GetNiceName() + ".";
						}
						else
						{
							errorMessage = text + memberInfo.MemberType.ToString().ToLower(CultureInfo.InvariantCulture) + " " + name + " must have a return type of " + returnType.GetNiceName() + ".";
						}
						return false;
					}
				}
				int num = (isField ? 1 : 0) + (isProperty ? 1 : 0) + (isMethod ? 1 : 0);
				string text2 = (isField ? ("fields" + ((num-- <= 1) ? " " : ((num == 1) ? " or " : ", "))) : string.Empty) + (isProperty ? ("properties" + ((num-- <= 1) ? " " : ((num == 1) ? " or " : ", "))) : string.Empty) + (isMethod ? ("methods" + ((num-- <= 1) ? " " : ((num == 1) ? " or " : ", "))) : string.Empty);
				string text3 = ((flag4 == flag5) ? string.Empty : (flag4 ? "public " : "non-public ")) + ((flag3 == flag2) ? string.Empty : (flag3 ? "static " : "non-static "));
				string text4 = (returnType == null) ? " " : ("with a return type of " + returnType.GetNiceName() + " ");
				string text5 = (paramTypes.Count == 0) ? " " : (((text4 == " ") ? "" : "and ") + "with the parameter signature (" + string.Join(", ", paramTypes.Select((Type n) => n.GetNiceName()).ToArray()) + ") ");
				if (name == null)
				{
					errorMessage = "No " + text3 + text2 + text4 + text5 + "was found in " + type.GetNiceName() + ".";
					return false;
				}
				errorMessage = "No " + text3 + text2 + "named " + name + " " + text4 + text5 + "was found in " + type.GetNiceName() + ".";
				return false;
			}
			finally
			{
				Cache<MemberFinder>.Release(cache);
			}
		}

		private MemberFinder Start(Type type, Cache<MemberFinder> cache)
		{
			this.type = type;
			this.cache = cache;
			Reset();
			return this;
		}

		private void Reset()
		{
			returnType = null;
			name = null;
			conditionFlags = ConditionFlags.None;
			paramTypes.Clear();
		}

		private bool HasCondition(ConditionFlags flag)
		{
			return (conditionFlags & flag) == flag;
		}
	}
}
