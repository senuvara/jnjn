using System;
using System.Collections.Generic;

namespace UnityEngine.Timeline
{
	internal static class TimelineCreateUtilities
	{
		public static string GenerateUniqueActorName(List<ScriptableObject> tracks, string prefix)
		{
			if (!tracks.Exists((ScriptableObject x) => (object)x != null && x.name == prefix))
			{
				return prefix;
			}
			int num = 1;
			string newName = prefix + num;
			while (tracks.Exists((ScriptableObject x) => (object)x != null && x.name == newName))
			{
				num++;
				newName = prefix + num;
			}
			return newName;
		}

		public static void SaveAssetIntoObject(Object childAsset, Object masterAsset)
		{
			if ((masterAsset.hideFlags & HideFlags.DontSave) != 0)
			{
				childAsset.hideFlags |= HideFlags.DontSave;
			}
			else
			{
				childAsset.hideFlags |= HideFlags.HideInHierarchy;
			}
		}

		internal static bool ValidateParentTrack(TrackAsset parent, Type childType)
		{
			if (childType == null || !typeof(TrackAsset).IsAssignableFrom(childType))
			{
				return false;
			}
			if (parent == null)
			{
				return true;
			}
			SupportsChildTracksAttribute supportsChildTracksAttribute = Attribute.GetCustomAttribute(parent.GetType(), typeof(SupportsChildTracksAttribute)) as SupportsChildTracksAttribute;
			if (supportsChildTracksAttribute == null)
			{
				return false;
			}
			if (supportsChildTracksAttribute.childType == null)
			{
				return true;
			}
			if (childType == supportsChildTracksAttribute.childType)
			{
				int num = 0;
				TrackAsset trackAsset = parent;
				while (trackAsset != null && trackAsset.isSubTrack)
				{
					num++;
					trackAsset = (trackAsset.parent as TrackAsset);
				}
				return num < supportsChildTracksAttribute.levels;
			}
			return false;
		}
	}
}
