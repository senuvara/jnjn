using System;

namespace UnityEngine.Timeline
{
	[Flags]
	public enum ClipCaps
	{
		None = 0x0,
		Looping = 0x1,
		Extrapolation = 0x2,
		ClipIn = 0x4,
		SpeedMultiplier = 0x8,
		Blending = 0x10,
		All = -1
	}
}
