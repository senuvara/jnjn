using System;

namespace UnityEngine.Timeline
{
	[Flags]
	public enum MatchTargetFields
	{
		PositionX = 0x1,
		PositionY = 0x2,
		PositionZ = 0x4,
		RotationX = 0x8,
		RotationY = 0x10,
		RotationZ = 0x20
	}
}
