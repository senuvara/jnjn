using System;
using System.Runtime.CompilerServices;
using UnityEngine.Bindings;
using UnityEngine.Scripting;

namespace UnityEngine.Playables
{
	/// <summary>
	///   <para>Instantiates a PlayableAsset and controls playback of Playable objects.</para>
	/// </summary>
	[NativeHeader("Runtime/Director/Module/PlayableDirector.h")]
	[RequiredByNativeCode]
	public class PlayableDirector : Behaviour, IExposedPropertyTable
	{
		/// <summary>
		///   <para>The current playing state of the component. (Read Only)</para>
		/// </summary>
		public PlayState state => GetPlayState();

		/// <summary>
		///   <para>Controls how the time is incremented when it goes beyond the duration of the playable.</para>
		/// </summary>
		public DirectorWrapMode extrapolationMode
		{
			get
			{
				return GetWrapMode();
			}
			set
			{
				SetWrapMode(value);
			}
		}

		/// <summary>
		///   <para>The PlayableAsset that is used to instantiate a playable for playback.</para>
		/// </summary>
		public PlayableAsset playableAsset
		{
			get
			{
				return GetPlayableAssetInternal() as PlayableAsset;
			}
			set
			{
				SetPlayableAssetInternal(value);
			}
		}

		/// <summary>
		///   <para>The PlayableGraph created by the PlayableDirector.</para>
		/// </summary>
		public PlayableGraph playableGraph => GetGraphHandle();

		/// <summary>
		///   <para>Whether the playable asset will start playing back as soon as the component awakes.</para>
		/// </summary>
		public bool playOnAwake
		{
			get
			{
				return GetPlayOnAwake();
			}
			set
			{
				SetPlayOnAwake(value);
			}
		}

		/// <summary>
		///   <para>Controls how time is incremented when playing back.</para>
		/// </summary>
		public DirectorUpdateMode timeUpdateMode
		{
			[MethodImpl(MethodImplOptions.InternalCall)]
			get;
			[MethodImpl(MethodImplOptions.InternalCall)]
			set;
		}

		/// <summary>
		///   <para>The component's current time. This value is incremented according to the PlayableDirector.timeUpdateMode when it is playing. You can also change this value manually.</para>
		/// </summary>
		public double time
		{
			[MethodImpl(MethodImplOptions.InternalCall)]
			get;
			[MethodImpl(MethodImplOptions.InternalCall)]
			set;
		}

		/// <summary>
		///   <para>The time at which the Playable should start when first played.</para>
		/// </summary>
		public double initialTime
		{
			[MethodImpl(MethodImplOptions.InternalCall)]
			get;
			[MethodImpl(MethodImplOptions.InternalCall)]
			set;
		}

		/// <summary>
		///   <para>The duration of the Playable in seconds.</para>
		/// </summary>
		public double duration
		{
			[MethodImpl(MethodImplOptions.InternalCall)]
			get;
		}

		[MethodImpl(MethodImplOptions.InternalCall)]
		[GeneratedByOldBindingsGenerator]
		private extern void SetPlayableAssetInternal(ScriptableObject asset);

		[MethodImpl(MethodImplOptions.InternalCall)]
		[GeneratedByOldBindingsGenerator]
		private extern ScriptableObject GetPlayableAssetInternal();

		/// <summary>
		///   <para>Sets an ExposedReference value.</para>
		/// </summary>
		/// <param name="id">Identifier of the ExposedReference.</param>
		/// <param name="value">The object to bind to set the reference value to.</param>
		public void SetReferenceValue(PropertyName id, Object value)
		{
			INTERNAL_CALL_SetReferenceValue(this, ref id, value);
		}

		[MethodImpl(MethodImplOptions.InternalCall)]
		[GeneratedByOldBindingsGenerator]
		private static extern void INTERNAL_CALL_SetReferenceValue(PlayableDirector self, ref PropertyName id, Object value);

		public Object GetReferenceValue(PropertyName id, out bool idValid)
		{
			return INTERNAL_CALL_GetReferenceValue(this, ref id, out idValid);
		}

		[MethodImpl(MethodImplOptions.InternalCall)]
		[GeneratedByOldBindingsGenerator]
		private static extern Object INTERNAL_CALL_GetReferenceValue(PlayableDirector self, ref PropertyName id, out bool idValid);

		/// <summary>
		///   <para>Sets the binding of a reference object from a PlayableBinding.</para>
		/// </summary>
		/// <param name="key">The source object in the PlayableBinding.</param>
		/// <param name="value">The object to bind to the key.</param>
		[MethodImpl(MethodImplOptions.InternalCall)]
		[GeneratedByOldBindingsGenerator]
		public extern void SetGenericBinding(Object key, Object value);

		/// <summary>
		///   <para>Returns a binding to a reference object.</para>
		/// </summary>
		/// <param name="key">The object that acts as a key.</param>
		[MethodImpl(MethodImplOptions.InternalCall)]
		[GeneratedByOldBindingsGenerator]
		public extern Object GetGenericBinding(Object key);

		[MethodImpl(MethodImplOptions.InternalCall)]
		[GeneratedByOldBindingsGenerator]
		internal extern bool HasGenericBinding(Object key);

		/// <summary>
		///   <para>Tells the PlayableDirector to evaluate it's PlayableGraph on the next update.</para>
		/// </summary>
		public void DeferredEvaluate()
		{
			EvaluateNextFrame();
		}

		/// <summary>
		///   <para>Instatiates a Playable using the provided PlayableAsset and starts playback.</para>
		/// </summary>
		/// <param name="asset">An asset to instantiate a playable from.</param>
		/// <param name="mode">What to do when the time passes the duration of the playable.</param>
		public void Play(PlayableAsset asset)
		{
			if (asset == null)
			{
				throw new ArgumentNullException("asset");
			}
			Play(asset, extrapolationMode);
		}

		/// <summary>
		///   <para>Instatiates a Playable using the provided PlayableAsset and starts playback.</para>
		/// </summary>
		/// <param name="asset">An asset to instantiate a playable from.</param>
		/// <param name="mode">What to do when the time passes the duration of the playable.</param>
		public void Play(PlayableAsset asset, DirectorWrapMode mode)
		{
			if (asset == null)
			{
				throw new ArgumentNullException("asset");
			}
			playableAsset = asset;
			extrapolationMode = mode;
			Play();
		}

		/// <summary>
		///   <para>Evaluates the currently playing Playable at  the current time.</para>
		/// </summary>
		[MethodImpl(MethodImplOptions.InternalCall)]
		public extern void Evaluate();

		/// <summary>
		///   <para>Instatiates a Playable using the provided PlayableAsset and starts playback.</para>
		/// </summary>
		/// <param name="asset">An asset to instantiate a playable from.</param>
		/// <param name="mode">What to do when the time passes the duration of the playable.</param>
		[MethodImpl(MethodImplOptions.InternalCall)]
		public extern void Play();

		/// <summary>
		///   <para>Stops playback of the current Playable and destroys the corresponding graph.</para>
		/// </summary>
		[MethodImpl(MethodImplOptions.InternalCall)]
		public extern void Stop();

		/// <summary>
		///   <para>Pauses playback of the currently running playable.</para>
		/// </summary>
		[MethodImpl(MethodImplOptions.InternalCall)]
		public extern void Pause();

		/// <summary>
		///   <para>Resume playing a paused playable.</para>
		/// </summary>
		[MethodImpl(MethodImplOptions.InternalCall)]
		public extern void Resume();

		/// <summary>
		///   <para>Discards the existing PlayableGraph and creates a new instance.</para>
		/// </summary>
		[MethodImpl(MethodImplOptions.InternalCall)]
		public extern void RebuildGraph();

		/// <summary>
		///   <para>Clears an exposed reference value.</para>
		/// </summary>
		/// <param name="id">Identifier of the ExposedReference.</param>
		public void ClearReferenceValue(PropertyName id)
		{
			ClearReferenceValue_Injected(ref id);
		}

		[MethodImpl(MethodImplOptions.InternalCall)]
		internal extern void ProcessPendingGraphChanges();

		[MethodImpl(MethodImplOptions.InternalCall)]
		private extern PlayState GetPlayState();

		[MethodImpl(MethodImplOptions.InternalCall)]
		private extern void SetWrapMode(DirectorWrapMode mode);

		[MethodImpl(MethodImplOptions.InternalCall)]
		private extern DirectorWrapMode GetWrapMode();

		[MethodImpl(MethodImplOptions.InternalCall)]
		private extern void EvaluateNextFrame();

		private PlayableGraph GetGraphHandle()
		{
			GetGraphHandle_Injected(out PlayableGraph ret);
			return ret;
		}

		[MethodImpl(MethodImplOptions.InternalCall)]
		private extern void SetPlayOnAwake(bool on);

		[MethodImpl(MethodImplOptions.InternalCall)]
		private extern bool GetPlayOnAwake();

		[MethodImpl(MethodImplOptions.InternalCall)]
		private extern void ClearReferenceValue_Injected(ref PropertyName id);

		[MethodImpl(MethodImplOptions.InternalCall)]
		private extern void GetGraphHandle_Injected(out PlayableGraph ret);
	}
}
