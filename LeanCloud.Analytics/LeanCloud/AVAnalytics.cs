using LeanCloud.Analytics.Internal;
using LeanCloud.Core.Internal;
using LeanCloud.Storage.Internal;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading;
using System.Threading.Tasks;

namespace LeanCloud
{
	public class AVAnalytics
	{
		private static AVAnalytics _current;

		internal readonly object mutex = new object();

		internal IAVAnalyticsDevice deviceHook;

		private IList<AVAnalyticsEvent> @event;

		private AVAnalyticsLaunch launch;

		private AVAnalyticsTerminate terminate;

		internal static IAVAnalyticsController AnalyticsController => AVAnalyticsPlugins.Instance.AnalyticsController;

		internal static IAVCurrentUserController CurrentUserController => AVAnalyticsPlugins.Instance.CorePlugins.CurrentUserController;

		public static AVAnalytics Current
		{
			get
			{
				return _current;
			}
			internal set
			{
				_current = value;
			}
		}

		public string SessionId
		{
			get;
			internal set;
		}

		public bool Enable
		{
			get;
			internal set;
		}

		public int Policy
		{
			get;
			internal set;
		}

		public IDictionary<string, object> CloudParameters
		{
			get;
			internal set;
		}

		public static Task<bool> InitAsync(IAVAnalyticsDevice device)
		{
			if (device == null)
			{
				throw new NotImplementedException("InitAsync need an IAVAnalyticsDevice implementment");
			}
			return CurrentUserController.GetCurrentSessionTokenAsync(CancellationToken.None).OnSuccess((Task<string> t) => AnalyticsController.GetPolicyAsync(t.Result, CancellationToken.None)).Unwrap()
				.OnSuccess(delegate(Task<IDictionary<string, object>> s)
				{
					IDictionary<string, object> result = s.Result;
					AVAnalytics aVAnalytics = new AVAnalytics();
					aVAnalytics.CloudParameters = (result["parameters"] as IDictionary<string, object>);
					aVAnalytics.Enable = bool.Parse(result["enable"].ToString());
					aVAnalytics.Policy = int.Parse(result["policy"].ToString());
					aVAnalytics.deviceHook = device;
					aVAnalytics.Reset();
					Current = aVAnalytics;
					return aVAnalytics.Enable;
				});
		}

		public void TrackAppOpened()
		{
			TrackEvent("!AV!AppOpen", "!AV!AppOpen");
		}

		public void TrackAppOpenedWithPush(IDictionary<string, object> pushHash = null)
		{
			TrackEvent("!AV!PushOpen", "!AV!PushOpen", pushHash);
		}

		public string TrackEvent(string name)
		{
			return TrackEvent(name, null);
		}

		public string TrackEvent(string name, string tag = "", IDictionary<string, object> attributes = null)
		{
			string text = $"event_{Guid.NewGuid().ToString()}";
			AVAnalyticsEvent item = new AVAnalyticsEvent(SessionId)
			{
				eventId = text,
				attributes = attributes,
				name = name,
				tag = tag,
				ts = UnixTimestampFromDateTime(DateTime.Now),
				du = 0L
			};
			@event.Add(item);
			return text;
		}

		public string BeginEvent(string name, string tag, IDictionary<string, object> attributes = null)
		{
			return TrackEvent(name, tag, attributes);
		}

		public void EndEvent(string eventId, IDictionary<string, object> attributes = null)
		{
			AVAnalyticsEvent aVAnalyticsEvent = @event.First((AVAnalyticsEvent e) => e.eventId == eventId);
			if (aVAnalyticsEvent == null)
			{
				throw new ArgumentOutOfRangeException("can not find event whick id is " + eventId);
			}
			aVAnalyticsEvent.du = UnixTimestampFromDateTime(DateTime.Now) - aVAnalyticsEvent.ts;
			if (attributes != null)
			{
				if (aVAnalyticsEvent.attributes == null)
				{
					aVAnalyticsEvent.attributes = new Dictionary<string, object>();
				}
				foreach (KeyValuePair<string, object> attribute in attributes)
				{
					aVAnalyticsEvent.attributes[attribute.Key] = attribute.Value;
				}
			}
		}

		public string TrackPage(string name, long duration)
		{
			string text = $"activity_{Guid.NewGuid().ToString()}";
			AVAnalyticsActivity item = new AVAnalyticsActivity
			{
				activityId = text,
				du = duration,
				name = name,
				ts = UnixTimestampFromDateTime(DateTime.Now)
			};
			terminate.activities.Add(item);
			return text;
		}

		public string BeginPage(string name)
		{
			return TrackPage(name, 0L);
		}

		public void EndPage(string pageId)
		{
			AVAnalyticsActivity aVAnalyticsActivity = terminate.activities.First((AVAnalyticsActivity a) => a.activityId == pageId);
			if (aVAnalyticsActivity == null)
			{
				throw new ArgumentOutOfRangeException("can not find page with id is " + pageId);
			}
			aVAnalyticsActivity.du = UnixTimestampFromDateTime(DateTime.Now) - aVAnalyticsActivity.ts;
		}

		internal Task SendAsync()
		{
			if (!Enable)
			{
				return Task.FromResult(result: false);
			}
			IDictionary<string, object> analyticsData = ToJson();
			return CurrentUserController.GetCurrentSessionTokenAsync(CancellationToken.None).OnSuccess((Task<string> t) => AnalyticsController.SendAsync(analyticsData, t.Result, CancellationToken.None)).Unwrap();
		}

		internal void StartSession()
		{
		}

		public void CloseSession()
		{
			SendAsync();
		}

		internal void Reset()
		{
			SessionId = Guid.NewGuid().ToString();
			@event = new List<AVAnalyticsEvent>();
			launch = new AVAnalyticsLaunch(SessionId);
			terminate = new AVAnalyticsTerminate(SessionId);
		}

		public IDictionary<string, object> ToJson()
		{
			Dictionary<string, object> dictionary = new Dictionary<string, object>();
			lock (mutex)
			{
				dictionary.Add("device", makeDeviceJson());
				dictionary.Add("events", makeEventsJson());
				return dictionary;
			}
		}

		internal IDictionary<string, object> makeDeviceJson()
		{
			return new Dictionary<string, object>
			{
				{
					"access",
					deviceHook.network_access
				},
				{
					"app_version",
					deviceHook.app_version
				},
				{
					"carrier",
					deviceHook.network_carrier
				},
				{
					"channel",
					deviceHook.channel
				},
				{
					"device_id",
					deviceHook.device_id
				},
				{
					"device_model",
					deviceHook.device_model
				},
				{
					"device_brand",
					deviceHook.device_brand
				},
				{
					"language",
					deviceHook.language
				},
				{
					"mc",
					deviceHook.mc
				},
				{
					"os",
					deviceHook.platform
				},
				{
					"os_version",
					deviceHook.os_version
				},
				{
					"resolution",
					deviceHook.device_resolution
				},
				{
					"timezone",
					deviceHook.timezone
				}
			};
		}

		internal IList<IDictionary<string, object>> makeEventJson()
		{
			List<IDictionary<string, object>> list = new List<IDictionary<string, object>>();
			foreach (AVAnalyticsEvent item in @event)
			{
				list.Add(item.ToJson());
			}
			return list;
		}

		internal IDictionary<string, object> makeEventsJson()
		{
			return new Dictionary<string, object>
			{
				{
					"event",
					makeEventJson()
				},
				{
					"launch",
					launch.ToJson()
				},
				{
					"terminate",
					terminate.ToJson()
				}
			};
		}

		internal static long UnixTimestampFromDateTime(DateTime date)
		{
			return (date.Ticks - new DateTime(1970, 1, 1).Ticks) / 10000;
		}
	}
}
