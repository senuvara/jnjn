using System.Collections.Generic;
using System.Linq;

namespace LeanCloud
{
	internal class AVAnalyticsTerminate
	{
		public long duration;

		public IList<AVAnalyticsActivity> activities;

		public string sessionId;

		public AVAnalyticsTerminate(string _sessionId)
		{
			sessionId = _sessionId;
			activities = new List<AVAnalyticsActivity>();
		}

		public IDictionary<string, object> ToJson()
		{
			IEnumerable<IDictionary<string, object>> source = activities.Select((AVAnalyticsActivity activity) => activity.ToJson());
			return new Dictionary<string, object>
			{
				{
					"duration",
					duration
				},
				{
					"activities",
					source.ToList()
				},
				{
					"SessionId",
					sessionId
				}
			};
		}
	}
}
