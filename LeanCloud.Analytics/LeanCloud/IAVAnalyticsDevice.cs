namespace LeanCloud
{
	public interface IAVAnalyticsDevice
	{
		string network_access
		{
			get;
		}

		string app_version
		{
			get;
		}

		string network_carrier
		{
			get;
		}

		string channel
		{
			get;
		}

		string device_id
		{
			get;
		}

		string device_model
		{
			get;
		}

		string device_brand
		{
			get;
		}

		string language
		{
			get;
		}

		string mc
		{
			get;
		}

		string platform
		{
			get;
		}

		string os_version
		{
			get;
		}

		string device_resolution
		{
			get;
		}

		string timezone
		{
			get;
		}
	}
}
