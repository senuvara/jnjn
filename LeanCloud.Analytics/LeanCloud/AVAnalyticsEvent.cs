using System.Collections.Generic;

namespace LeanCloud
{
	internal class AVAnalyticsEvent
	{
		public string eventId;

		public long du;

		public string name;

		public string tag;

		public long ts;

		public IDictionary<string, object> attributes;

		public string sessionId;

		public AVAnalyticsEvent(string _sessionId)
		{
			sessionId = _sessionId;
		}

		public IDictionary<string, object> ToJson()
		{
			return new Dictionary<string, object>
			{
				{
					"eventId",
					eventId
				},
				{
					"du",
					du
				},
				{
					"name",
					name
				},
				{
					"tag",
					tag
				},
				{
					"ts",
					ts
				},
				{
					"attributes",
					attributes
				},
				{
					"SessionId",
					sessionId
				}
			};
		}
	}
}
