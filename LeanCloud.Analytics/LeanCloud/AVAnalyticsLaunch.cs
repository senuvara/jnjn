using System;
using System.Collections.Generic;

namespace LeanCloud
{
	internal class AVAnalyticsLaunch
	{
		public long date;

		public string sessionId;

		public AVAnalyticsLaunch(string _sessionId)
		{
			sessionId = _sessionId;
			date = AVAnalytics.UnixTimestampFromDateTime(DateTime.Now);
		}

		public IDictionary<string, object> ToJson()
		{
			return new Dictionary<string, object>
			{
				{
					"date",
					date
				},
				{
					"SessionId",
					sessionId
				}
			};
		}
	}
}
