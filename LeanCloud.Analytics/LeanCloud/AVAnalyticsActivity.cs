using System.Collections.Generic;

namespace LeanCloud
{
	internal class AVAnalyticsActivity
	{
		public string activityId;

		public long du;

		public string name;

		public long ts;

		public IDictionary<string, object> ToJson()
		{
			return new Dictionary<string, object>
			{
				{
					"activityId",
					activityId
				},
				{
					"du",
					du
				},
				{
					"name",
					name
				},
				{
					"ts",
					ts
				}
			};
		}
	}
}
