using LeanCloud.Core.Internal;
using LeanCloud.Storage.Internal;
using System;
using System.Collections.Generic;
using System.Net;
using System.Threading;
using System.Threading.Tasks;

namespace LeanCloud.Analytics.Internal
{
	public class AVAnalyticsController : IAVAnalyticsController
	{
		private readonly IAVCommandRunner commandRunner;

		public AVAnalyticsController(IAVCommandRunner commandRunner)
		{
			this.commandRunner = commandRunner;
		}

		public Task TrackEventAsync(string name, IDictionary<string, string> dimensions, string sessionToken, CancellationToken cancellationToken)
		{
			IDictionary<string, object> dictionary = new Dictionary<string, object>
			{
				{
					"at",
					DateTime.Now
				},
				{
					"name",
					name
				}
			};
			if (dimensions != null)
			{
				dictionary["dimensions"] = dimensions;
			}
			AVCommand command = new AVCommand("events/" + name, "POST", sessionToken, null, PointerOrLocalIdEncoder.Instance.Encode(dictionary) as IDictionary<string, object>);
			return commandRunner.RunCommandAsync(command, null, null, cancellationToken);
		}

		public Task TrackAppOpenedAsync(string pushHash, string sessionToken, CancellationToken cancellationToken)
		{
			IDictionary<string, object> dictionary = new Dictionary<string, object>
			{
				{
					"at",
					DateTime.Now
				}
			};
			if (pushHash != null)
			{
				dictionary["push_hash"] = pushHash;
			}
			AVCommand command = new AVCommand("events/AppOpened", "POST", sessionToken, null, PointerOrLocalIdEncoder.Instance.Encode(dictionary) as IDictionary<string, object>);
			return commandRunner.RunCommandAsync(command, null, null, cancellationToken);
		}

		public Task<IDictionary<string, object>> GetPolicyAsync(string sessionToken, CancellationToken cancellationToken)
		{
			AVCommand command = new AVCommand($"statistics/apps/{AVClient.CurrentConfiguration.ApplicationId}/sendPolicy", "GET", sessionToken, (IList<KeyValuePair<string, string>>)null, (IDictionary<string, object>)null);
			return commandRunner.RunCommandAsync(command, null, null, cancellationToken).OnSuccess((Task<Tuple<HttpStatusCode, IDictionary<string, object>>> t) => t.Result.Item2);
		}

		public Task<bool> SendAsync(IDictionary<string, object> analyticsData, string sessionToken, CancellationToken cancellationToken)
		{
			AVCommand command = new AVCommand("stats/collect", "POST", sessionToken, null, analyticsData);
			return commandRunner.RunCommandAsync(command, null, null, cancellationToken).OnSuccess((Task<Tuple<HttpStatusCode, IDictionary<string, object>>> t) => t.Result.Item1 == HttpStatusCode.OK);
		}
	}
}
