using LeanCloud.Core.Internal;

namespace LeanCloud.Analytics.Internal
{
	public class AVAnalyticsPlugins : IAVAnalyticsPlugins
	{
		private static readonly object instanceMutex = new object();

		private static IAVAnalyticsPlugins instance;

		private readonly object mutex = new object();

		private IAVCorePlugins corePlugins;

		private IAVAnalyticsController analyticsController;

		public static IAVAnalyticsPlugins Instance
		{
			get
			{
				lock (instanceMutex)
				{
					instance = (instance ?? new AVAnalyticsPlugins());
					return instance;
				}
			}
			set
			{
				lock (instanceMutex)
				{
					instance = value;
				}
			}
		}

		public IAVCorePlugins CorePlugins
		{
			get
			{
				lock (mutex)
				{
					corePlugins = (corePlugins ?? AVPlugins.Instance);
					return corePlugins;
				}
			}
			set
			{
				lock (mutex)
				{
					corePlugins = value;
				}
			}
		}

		public IAVAnalyticsController AnalyticsController
		{
			get
			{
				lock (mutex)
				{
					analyticsController = (analyticsController ?? new AVAnalyticsController(CorePlugins.CommandRunner));
					return analyticsController;
				}
			}
			set
			{
				lock (mutex)
				{
					analyticsController = value;
				}
			}
		}

		public void Reset()
		{
			lock (mutex)
			{
				CorePlugins = null;
				AnalyticsController = null;
			}
		}
	}
}
