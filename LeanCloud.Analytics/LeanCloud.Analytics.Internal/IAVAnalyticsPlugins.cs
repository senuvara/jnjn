using LeanCloud.Core.Internal;

namespace LeanCloud.Analytics.Internal
{
	public interface IAVAnalyticsPlugins
	{
		IAVCorePlugins CorePlugins
		{
			get;
		}

		IAVAnalyticsController AnalyticsController
		{
			get;
		}

		void Reset();
	}
}
