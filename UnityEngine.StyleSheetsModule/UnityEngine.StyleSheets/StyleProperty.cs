using System;

namespace UnityEngine.StyleSheets
{
	[Serializable]
	internal class StyleProperty
	{
		[SerializeField]
		private string m_Name;

		[SerializeField]
		private StyleValueHandle[] m_Values;

		public string name
		{
			get
			{
				return m_Name;
			}
			internal set
			{
				m_Name = value;
			}
		}

		public StyleValueHandle[] values
		{
			get
			{
				return m_Values;
			}
			internal set
			{
				m_Values = value;
			}
		}
	}
}
