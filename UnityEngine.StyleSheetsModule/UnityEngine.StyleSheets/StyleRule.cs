using System;

namespace UnityEngine.StyleSheets
{
	[Serializable]
	internal class StyleRule
	{
		[SerializeField]
		private StyleProperty[] m_Properties;

		[SerializeField]
		internal int line;

		public StyleProperty[] properties
		{
			get
			{
				return m_Properties;
			}
			internal set
			{
				m_Properties = value;
			}
		}
	}
}
