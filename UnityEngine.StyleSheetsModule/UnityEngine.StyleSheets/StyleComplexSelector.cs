using System;

namespace UnityEngine.StyleSheets
{
	[Serializable]
	internal class StyleComplexSelector
	{
		[SerializeField]
		private int m_Specificity;

		[SerializeField]
		private StyleSelector[] m_Selectors;

		[SerializeField]
		internal int ruleIndex;

		public int specificity
		{
			get
			{
				return m_Specificity;
			}
			internal set
			{
				m_Specificity = value;
			}
		}

		public StyleRule rule
		{
			get;
			internal set;
		}

		public bool isSimple => selectors.Length == 1;

		public StyleSelector[] selectors
		{
			get
			{
				return m_Selectors;
			}
			internal set
			{
				m_Selectors = value;
			}
		}
	}
}
