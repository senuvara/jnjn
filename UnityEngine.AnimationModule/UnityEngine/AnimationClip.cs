using System;
using System.Runtime.CompilerServices;
using UnityEngine.Bindings;
using UnityEngine.Scripting;

namespace UnityEngine
{
	/// <summary>
	///   <para>Stores keyframe based animations.</para>
	/// </summary>
	[NativeType("Runtime/Animation/AnimationClip.h")]
	public sealed class AnimationClip : Motion
	{
		/// <summary>
		///   <para>Animation length in seconds. (Read Only)</para>
		/// </summary>
		public float length
		{
			[MethodImpl(MethodImplOptions.InternalCall)]
			[GeneratedByOldBindingsGenerator]
			get;
		}

		internal float startTime
		{
			[MethodImpl(MethodImplOptions.InternalCall)]
			[GeneratedByOldBindingsGenerator]
			get;
		}

		internal float stopTime
		{
			[MethodImpl(MethodImplOptions.InternalCall)]
			[GeneratedByOldBindingsGenerator]
			get;
		}

		/// <summary>
		///   <para>Frame rate at which keyframes are sampled. (Read Only)</para>
		/// </summary>
		public float frameRate
		{
			[MethodImpl(MethodImplOptions.InternalCall)]
			[GeneratedByOldBindingsGenerator]
			get;
			[MethodImpl(MethodImplOptions.InternalCall)]
			[GeneratedByOldBindingsGenerator]
			set;
		}

		/// <summary>
		///   <para>Sets the default wrap mode used in the animation state.</para>
		/// </summary>
		public WrapMode wrapMode
		{
			[MethodImpl(MethodImplOptions.InternalCall)]
			[GeneratedByOldBindingsGenerator]
			get;
			[MethodImpl(MethodImplOptions.InternalCall)]
			[GeneratedByOldBindingsGenerator]
			set;
		}

		/// <summary>
		///   <para>AABB of this Animation Clip in local space of Animation component that it is attached too.</para>
		/// </summary>
		public Bounds localBounds
		{
			get
			{
				INTERNAL_get_localBounds(out Bounds value);
				return value;
			}
			set
			{
				INTERNAL_set_localBounds(ref value);
			}
		}

		/// <summary>
		///   <para>Set to true if the AnimationClip will be used with the Legacy Animation component ( instead of the Animator ).</para>
		/// </summary>
		public new bool legacy
		{
			[MethodImpl(MethodImplOptions.InternalCall)]
			[GeneratedByOldBindingsGenerator]
			get;
			[MethodImpl(MethodImplOptions.InternalCall)]
			[GeneratedByOldBindingsGenerator]
			set;
		}

		/// <summary>
		///   <para>Returns true if the animation contains curve that drives a humanoid rig.</para>
		/// </summary>
		public bool humanMotion
		{
			[MethodImpl(MethodImplOptions.InternalCall)]
			[GeneratedByOldBindingsGenerator]
			get;
		}

		/// <summary>
		///   <para>Returns true if the animation clip has no curves and no events.</para>
		/// </summary>
		public bool empty
		{
			[MethodImpl(MethodImplOptions.InternalCall)]
			[GeneratedByOldBindingsGenerator]
			get;
		}

		/// <summary>
		///   <para>Animation Events for this animation clip.</para>
		/// </summary>
		public AnimationEvent[] events
		{
			get
			{
				return (AnimationEvent[])GetEventsInternal();
			}
			set
			{
				SetEventsInternal(value);
			}
		}

		internal bool hasRootMotion
		{
			[MethodImpl(MethodImplOptions.InternalCall)]
			[GeneratedByOldBindingsGenerator]
			get;
		}

		/// <summary>
		///   <para>Creates a new animation clip.</para>
		/// </summary>
		public AnimationClip()
		{
			Internal_CreateAnimationClip(this);
		}

		/// <summary>
		///   <para>Samples an animation at a given time for any animated properties.</para>
		/// </summary>
		/// <param name="go">The animated game object.</param>
		/// <param name="time">The time to sample an animation.</param>
		[MethodImpl(MethodImplOptions.InternalCall)]
		[GeneratedByOldBindingsGenerator]
		public extern void SampleAnimation(GameObject go, float time);

		[MethodImpl(MethodImplOptions.InternalCall)]
		[GeneratedByOldBindingsGenerator]
		private static extern void Internal_CreateAnimationClip([Writable] AnimationClip self);

		/// <summary>
		///   <para>Assigns the curve to animate a specific property.</para>
		/// </summary>
		/// <param name="relativePath">Path to the game object this curve applies to. The relativePath
		///   is formatted similar to a pathname, e.g. "rootspineleftArm".  If relativePath
		///   is empty it refers to the game object the animation clip is attached to.</param>
		/// <param name="type">The class type of the component that is animated.</param>
		/// <param name="propertyName">The name or path to the property being animated.</param>
		/// <param name="curve">The animation curve.</param>
		[MethodImpl(MethodImplOptions.InternalCall)]
		[GeneratedByOldBindingsGenerator]
		public extern void SetCurve(string relativePath, Type type, string propertyName, AnimationCurve curve);

		/// <summary>
		///   <para>Realigns quaternion keys to ensure shortest interpolation paths.</para>
		/// </summary>
		public void EnsureQuaternionContinuity()
		{
			INTERNAL_CALL_EnsureQuaternionContinuity(this);
		}

		[MethodImpl(MethodImplOptions.InternalCall)]
		[GeneratedByOldBindingsGenerator]
		private static extern void INTERNAL_CALL_EnsureQuaternionContinuity(AnimationClip self);

		/// <summary>
		///   <para>Clears all curves from the clip.</para>
		/// </summary>
		public void ClearCurves()
		{
			INTERNAL_CALL_ClearCurves(this);
		}

		[MethodImpl(MethodImplOptions.InternalCall)]
		[GeneratedByOldBindingsGenerator]
		private static extern void INTERNAL_CALL_ClearCurves(AnimationClip self);

		[MethodImpl(MethodImplOptions.InternalCall)]
		[GeneratedByOldBindingsGenerator]
		private extern void INTERNAL_get_localBounds(out Bounds value);

		[MethodImpl(MethodImplOptions.InternalCall)]
		[GeneratedByOldBindingsGenerator]
		private extern void INTERNAL_set_localBounds(ref Bounds value);

		/// <summary>
		///   <para>Adds an animation event to the clip.</para>
		/// </summary>
		/// <param name="evt">AnimationEvent to add.</param>
		public void AddEvent(AnimationEvent evt)
		{
			if (evt == null)
			{
				throw new ArgumentNullException("evt");
			}
			AddEventInternal(evt);
		}

		[MethodImpl(MethodImplOptions.InternalCall)]
		[GeneratedByOldBindingsGenerator]
		internal extern void AddEventInternal(object evt);

		[MethodImpl(MethodImplOptions.InternalCall)]
		[GeneratedByOldBindingsGenerator]
		internal extern void SetEventsInternal(Array value);

		[MethodImpl(MethodImplOptions.InternalCall)]
		[GeneratedByOldBindingsGenerator]
		internal extern Array GetEventsInternal();
	}
}
