using UnityEngine;
using UnityEngine.UI;

public class Loading : MonoBehaviour
{
	private RectTransform rectComponent;

	private Image imageComp;

	private bool up;

	public float rotateSpeed = 200f;

	public float openSpeed = 0.005f;

	public float closeSpeed = 0.01f;

	private void Start()
	{
		rectComponent = GetComponent<RectTransform>();
		imageComp = rectComponent.GetComponent<Image>();
		up = true;
	}

	private void Update()
	{
		rectComponent.Rotate(0f, 0f, rotateSpeed * Time.deltaTime);
		changeSize();
	}

	private void changeSize()
	{
		float fillAmount = imageComp.fillAmount;
		if (fillAmount < 0.3f && up)
		{
			imageComp.fillAmount += openSpeed;
		}
		else if (fillAmount >= 0.3f && up)
		{
			up = false;
		}
		else if (fillAmount >= 0.02f && !up)
		{
			imageComp.fillAmount -= closeSpeed;
		}
		else if (fillAmount < 0.02f && !up)
		{
			up = true;
		}
	}
}
