using System;
using System.Collections.Generic;
using System.Text;
using UnityEngine;
using xdsdk.Unity.MiniJSON;

namespace xdsdk.Unity.Service
{
	public class Api
	{
		private static Api instance;

		private static GameObject netObject;

		private static readonly string BASE_URL = "https://api.xd.com/v1";

		private static readonly string BASE_URL_V2 = "https://api.xd.com/v2";

		private static readonly string INIT_SDK = BASE_URL_V2 + "/sdk/appid_info";

		private static readonly string XD_AUTH = BASE_URL + "/authorizations";

		private static readonly string QQ_AUTH = BASE_URL_V2 + "/authorizations/qq";

		private static readonly string WX_AUTH = BASE_URL_V2 + "/authorizations/weixin";

		private static readonly string TAP_AUTH = BASE_URL_V2 + "/authorizations/taptap";

		private static readonly string VERIFY_CODE = BASE_URL + "/authorizations/get_mobile_verify_code";

		private static readonly string WX_CHECK = BASE_URL_V2 + "/authorizations/refresh_wx_token";

		private static readonly string QQ_CHECK = BASE_URL_V2 + "/authorizations/check_qq_token";

		private static readonly string TAPTAP_CHECK = BASE_URL_V2 + "/authorizations/check_taptap_token";

		private static readonly string USER = BASE_URL + "/user";

		private static readonly string REALNAME = BASE_URL + "/user/user_fcm_set";

		private static readonly string LOGOUT = BASE_URL + "/users/get_auth_url";

		public static Api Instance
		{
			get
			{
				if (instance == null)
				{
					instance = new Api();
				}
				if (netObject == null)
				{
					instance.Init();
				}
				return instance;
			}
		}

		private void Init()
		{
			GameObject gameObject = new GameObject();
			gameObject.name = "XDSDKNet";
			netObject = gameObject;
			netObject.AddComponent<Net>();
			UnityEngine.Object.DontDestroyOnLoad(netObject);
		}

		public void InitSDK(string appid, Action<AppInfo> methodForResult, Action<string> methodForError)
		{
			Net net = netObject.GetComponent<Net>();
			if (net == null)
			{
				net = netObject.AddComponent<Net>();
			}
			Dictionary<string, string> dictionary = new Dictionary<string, string>();
			dictionary.Add("client_id", appid);
			Dictionary<string, string> parameters = dictionary;
			net.GetOnServer(INIT_SDK, parameters, delegate(string data)
			{
				try
				{
					Debug.Log("Init Success : " + data);
					Dictionary<string, object> appinfoDict = Json.Deserialize(data) as Dictionary<string, object>;
					AppInfo obj = AppInfo.InitWithDict(appinfoDict);
					methodForResult(obj);
				}
				catch (Exception ex)
				{
					Debug.LogError("Init Failed : " + ex.Message);
					methodForError("Init Failed : " + ex.Message);
				}
			}, delegate(int code, string error)
			{
				Debug.LogError("Init Failed : " + error);
				methodForError("Init Failed : " + error);
			});
		}

		public void LoginQQ(string appid, string openID, string accessToken, Action<string> methodForResult, Action<string> methodForError)
		{
			Net net = netObject.GetComponent<Net>();
			if (net == null)
			{
				net = netObject.AddComponent<Net>();
			}
			List<string> list = new List<string>();
			list.Add("user");
			list.Add("sdk");
			List<string> value = list;
			Dictionary<string, object> dictionary = new Dictionary<string, object>();
			dictionary.Add("client_id", appid);
			dictionary.Add("qq_access_token", accessToken);
			dictionary.Add("qq_openid", openID);
			dictionary.Add("scopes", value);
			dictionary.Add("auth_type", "web");
			Dictionary<string, object> parameters = dictionary;
			net.PostOnServer(QQ_AUTH, parameters, delegate(string data)
			{
				try
				{
					Debug.Log("QQ Login Success : " + data);
					Dictionary<string, object> dictionary2 = Json.Deserialize(data) as Dictionary<string, object>;
					if (dictionary2.ContainsKey("access_token"))
					{
						methodForResult(dictionary2["access_token"] as string);
					}
					else
					{
						methodForError("Could not get QQ access token.");
					}
				}
				catch (Exception ex)
				{
					if (methodForError != null)
					{
						methodForError(ex.Message);
					}
				}
			}, delegate(int code, string error)
			{
				if (methodForError != null)
				{
					methodForError(error);
				}
			});
		}

		public void LoginWechat(string appid, string wechatCode, Action<string, string> methodForResult, Action<string> methodForError)
		{
			Net net = netObject.GetComponent<Net>();
			if (net == null)
			{
				net = netObject.AddComponent<Net>();
			}
			List<string> list = new List<string>();
			list.Add("user");
			list.Add("sdk");
			List<string> value = list;
			Dictionary<string, object> dictionary = new Dictionary<string, object>();
			dictionary.Add("client_id", appid);
			dictionary.Add("weixin_code", wechatCode);
			dictionary.Add("scopes", value);
			dictionary.Add("auth_type", "web");
			Dictionary<string, object> parameters = dictionary;
			net.PostOnServer(WX_AUTH, parameters, delegate(string data)
			{
				try
				{
					Debug.Log("Wechat Login Success : " + data);
					Dictionary<string, object> dictionary2 = Json.Deserialize(data) as Dictionary<string, object>;
					if (dictionary2.ContainsKey("access_token") && dictionary2.ContainsKey("refresh_token"))
					{
						methodForResult(dictionary2["access_token"] as string, dictionary2["refresh_token"] as string);
					}
					else
					{
						methodForError("Could not get Wechat access token.");
					}
				}
				catch (Exception ex)
				{
					if (methodForError != null)
					{
						methodForError(ex.Message);
					}
				}
			}, delegate(int code, string error)
			{
				if (methodForError != null)
				{
					methodForError(error);
				}
			});
		}

		public void LoginTapTap(string appid, string kid, string accessToken, string tokenType, string macKey, string macAlgorithm, Action<string> methodForResult, Action<string> methodForError)
		{
			Net net = netObject.GetComponent<Net>();
			if (net == null)
			{
				net = netObject.AddComponent<Net>();
			}
			List<string> list = new List<string>();
			list.Add("user");
			list.Add("sdk");
			List<string> value = list;
			Dictionary<string, object> dictionary = new Dictionary<string, object>();
			dictionary.Add("client_id", appid);
			dictionary.Add("kid", kid);
			dictionary.Add("access_token", accessToken);
			dictionary.Add("token_type", tokenType);
			dictionary.Add("mac_key", macKey);
			dictionary.Add("mac_algorithm", macAlgorithm);
			dictionary.Add("scopes", value);
			dictionary.Add("auth_type", "app");
			Dictionary<string, object> parameters = dictionary;
			net.PostOnServer(TAP_AUTH, parameters, delegate(string data)
			{
				try
				{
					Debug.Log("TapTap Login Success : " + data);
					Dictionary<string, object> dictionary2 = Json.Deserialize(data) as Dictionary<string, object>;
					if (dictionary2.ContainsKey("access_token"))
					{
						methodForResult(dictionary2["access_token"] as string);
					}
					else
					{
						methodForError("Could not get TapTap access token.");
					}
				}
				catch (Exception ex)
				{
					if (methodForError != null)
					{
						methodForError(ex.Message);
					}
				}
			}, delegate(int code, string error)
			{
				if (methodForError != null)
				{
					methodForError(error);
				}
			});
		}

		public void LoginXD(string appid, string username, string password, string captcha, string code, Action<string> methodForResult, Action<string, object> methodForError)
		{
			byte[] bytes = Encoding.UTF8.GetBytes(username + ":" + password);
			string value = "Basic " + Convert.ToBase64String(bytes);
			Dictionary<string, string> dictionary = new Dictionary<string, string>();
			dictionary.Add("Authorization", value);
			Dictionary<string, string> headers = dictionary;
			List<string> list = new List<string>();
			list.Add("user");
			list.Add("sdk");
			List<string> value2 = list;
			Dictionary<string, object> dictionary2 = new Dictionary<string, object>();
			dictionary2.Add("client_id", appid);
			dictionary2.Add("open_twoauth", true);
			dictionary2.Add("scopes", value2);
			Dictionary<string, object> dictionary3 = dictionary2;
			if (!string.IsNullOrEmpty(code))
			{
				dictionary3.Add("code", code);
			}
			Net net = netObject.GetComponent<Net>();
			if (net == null)
			{
				net = netObject.AddComponent<Net>();
			}
			net.PostOnServer(XD_AUTH, headers, dictionary3, delegate(string data)
			{
				try
				{
					Dictionary<string, object> dictionary4 = Json.Deserialize(data) as Dictionary<string, object>;
					if (dictionary4.ContainsKey("error") && dictionary4.ContainsKey("needTwoauth") && (bool)dictionary4["needTwoauth"] && dictionary4.ContainsKey("user_id"))
					{
						string text = dictionary4["error"] as string;
						if (!text.Equals("短信验证码错误"))
						{
							methodForError("need_verification_code", dictionary4);
						}
						else
						{
							methodForError("error", dictionary4);
						}
					}
					else if (dictionary4.ContainsKey("error"))
					{
						methodForError("error", dictionary4["error"]);
					}
					else if (dictionary4.ContainsKey("access_token"))
					{
						methodForResult(dictionary4["access_token"] as string);
					}
					else
					{
						methodForError("error", data);
					}
				}
				catch (Exception ex)
				{
					if (methodForError != null)
					{
						methodForError("error", ex.Message + ":" + data);
					}
				}
			}, delegate(int errorCode, string error)
			{
				if (methodForError != null)
				{
					methodForError("error", error);
				}
			});
		}

		public void FetchVerificationCode(string appid, string userID, Action<string> methodForResult, Action<string> methodForError)
		{
			Dictionary<string, object> dictionary = new Dictionary<string, object>();
			dictionary.Add("client_id", appid);
			dictionary.Add("user_id", userID);
			dictionary.Add("type", "login");
			Dictionary<string, object> parameters = dictionary;
			Net net = netObject.GetComponent<Net>();
			if (net == null)
			{
				net = netObject.AddComponent<Net>();
			}
			net.PostOnServer(VERIFY_CODE, parameters, delegate(string result)
			{
				if (methodForResult != null)
				{
					methodForResult(result);
				}
			}, delegate(int code, string error)
			{
				if (methodForError != null)
				{
					methodForError(error);
				}
			});
		}

		public void GetUser(string token, Action<User> methodForResult, Action<string> methodForError)
		{
			Dictionary<string, string> dictionary = new Dictionary<string, string>();
			dictionary.Add("access_token", token);
			Dictionary<string, string> parameters = dictionary;
			Net net = netObject.GetComponent<Net>();
			if (net == null)
			{
				net = netObject.AddComponent<Net>();
			}
			net.GetOnServer(USER, parameters, delegate(string result)
			{
				try
				{
					Dictionary<string, object> dict = Json.Deserialize(result) as Dictionary<string, object>;
					User obj = User.InitWithDict(dict);
					if (methodForResult != null)
					{
						methodForResult(obj);
					}
				}
				catch (Exception ex)
				{
					if (methodForError != null)
					{
						methodForError(ex.Message + " : " + result);
					}
				}
			}, delegate(int code, string error)
			{
				if (methodForError != null)
				{
					methodForError(error);
				}
			});
		}

		public void VerifyRealName(string token, string name, string id, Action<string> methodForResult, Action<string> methodForError)
		{
			Dictionary<string, object> dictionary = new Dictionary<string, object>();
			dictionary.Add("access_token", token);
			dictionary.Add("is_tmp", "0");
			dictionary.Add("realname", name);
			dictionary.Add("realid", id);
			Dictionary<string, object> parameters = dictionary;
			Net net = netObject.GetComponent<Net>();
			if (net == null)
			{
				net = netObject.AddComponent<Net>();
			}
			net.PostOnServer(REALNAME, parameters, delegate(string result)
			{
				if (methodForResult != null)
				{
					methodForResult(result);
				}
			}, delegate(int code, string error)
			{
				if (methodForError != null)
				{
					methodForError(error);
				}
			});
		}

		public void Logout(Action method)
		{
			Net net = netObject.GetComponent<Net>();
			if (net == null)
			{
				net = netObject.AddComponent<Net>();
			}
			net.GetOnServer(LOGOUT, delegate
			{
				if (method != null)
				{
					method();
				}
			}, delegate
			{
				if (method != null)
				{
					method();
				}
			});
		}

		public void CheckWeChatToken(string appid, string refreshToken, string authType, Action success, Action<int> failed)
		{
			Net net = netObject.GetComponent<Net>();
			if (net == null)
			{
				net = netObject.AddComponent<Net>();
			}
			Dictionary<string, object> dictionary = new Dictionary<string, object>();
			dictionary.Add("client_id", appid);
			dictionary.Add("refresh_token", refreshToken);
			dictionary.Add("auth_type", authType);
			Dictionary<string, object> parameters = dictionary;
			net.PostOnServer(WX_CHECK, parameters, delegate
			{
				if (success != null)
				{
					success();
				}
			}, delegate(int code, string error)
			{
				if (failed != null)
				{
					failed(code);
				}
			});
		}

		public void CheckQQToken(string appid, string openid, string token, string authType, Action success, Action<int> failed)
		{
			Net net = netObject.GetComponent<Net>();
			if (net == null)
			{
				net = netObject.AddComponent<Net>();
			}
			Dictionary<string, object> dictionary = new Dictionary<string, object>();
			dictionary.Add("client_id", appid);
			dictionary.Add("qq_openid", openid);
			dictionary.Add("qq_access_token", token);
			dictionary.Add("auth_type", authType);
			Dictionary<string, object> parameters = dictionary;
			net.PostOnServer(QQ_CHECK, parameters, delegate
			{
				if (success != null)
				{
					success();
				}
			}, delegate(int code, string error)
			{
				if (failed != null)
				{
					failed(code);
				}
			});
		}

		public void CheckTapTapToken(string appid, string macKey, string kid, string tokenType, string accessToken, string macAlgorithm, string authType, Action success, Action<int> failed)
		{
			Net net = netObject.GetComponent<Net>();
			if (net == null)
			{
				net = netObject.AddComponent<Net>();
			}
			Dictionary<string, object> dictionary = new Dictionary<string, object>();
			dictionary.Add("client_id", appid);
			dictionary.Add("mac_key", macKey);
			dictionary.Add("kid", kid);
			dictionary.Add("token_type", authType);
			dictionary.Add("access_token", accessToken);
			dictionary.Add("mac_algorithm", macAlgorithm);
			Dictionary<string, object> parameters = dictionary;
			net.PostOnServer(TAPTAP_CHECK, parameters, delegate
			{
				if (success != null)
				{
					success();
				}
			}, delegate(int code, string error)
			{
				if (failed != null)
				{
					failed(code);
				}
			});
		}
	}
}
