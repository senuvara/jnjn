using System;
using System.Collections.Generic;
using System.Collections.Specialized;
using UnityEngine;

namespace xdsdk.Unity.Service
{
	public static class Token
	{
		public enum ThirdPartyTokenType
		{
			Wechat,
			QQ,
			TapTap
		}

		private static readonly string TOKEN_KEY = "com.xindong.xdsdk.unity.token";

		private static readonly string THIRD_PARTY_TOKEN_KEY = "com.xindong.xdsdk.unity.3rd.token";

		private static readonly string THIRD_PARTY_TOKEN_CHECK_FAILED_TIMESTAMP = "com.xindong.xdsdk.unity.3rd.token.failed.timestamp";

		private static readonly double THIRD_PARTY_TOKEN_VERIFY_TIME = 1800000.0;

		public static string GetToken(string appid)
		{
			return DataStorage.LoadString(TOKEN_KEY + "." + appid);
		}

		public static void SetToken(string appid, string token)
		{
			DataStorage.SaveString(TOKEN_KEY + "." + appid, token);
		}

		public static void ClearToken(string appid)
		{
			ClearThirdPartyToken(appid);
			DataStorage.SaveString(TOKEN_KEY + "." + appid, string.Empty);
		}

		public static void SetThirdPartyToken(string appid, ThirdPartyTokenType type, Dictionary<string, string> tokenDict)
		{
			try
			{
				ClearThirdPartyToken(appid);
				switch (type)
				{
				case ThirdPartyTokenType.Wechat:
					tokenDict.Add("type", "wechat");
					break;
				case ThirdPartyTokenType.QQ:
					tokenDict.Add("type", "qq");
					break;
				case ThirdPartyTokenType.TapTap:
					tokenDict.Add("type", "taptap");
					break;
				}
				DataStorage.SaveString(THIRD_PARTY_TOKEN_CHECK_FAILED_TIMESTAMP + "." + appid, "0");
				string value = appid + "?" + Net.DictToQueryString(tokenDict);
				DataStorage.SaveString(THIRD_PARTY_TOKEN_KEY + "." + appid, value);
			}
			catch (Exception ex)
			{
				Debug.LogError(ex.Message);
			}
		}

		public static bool HasThirdPartyToken(string appid)
		{
			return !string.IsNullOrEmpty(DataStorage.LoadString(THIRD_PARTY_TOKEN_KEY + "." + appid));
		}

		private static void ClearThirdPartyToken(string appid)
		{
			DataStorage.SaveString(THIRD_PARTY_TOKEN_CHECK_FAILED_TIMESTAMP, "0");
			DataStorage.SaveString(THIRD_PARTY_TOKEN_KEY + "." + appid, string.Empty);
		}

		public static void CheckThirdPartyToken(string appid, Action callback)
		{
			try
			{
				if (HasThirdPartyToken(appid))
				{
					string url = DataStorage.LoadString(THIRD_PARTY_TOKEN_KEY + "." + appid);
					NameValueCollection nvc = new NameValueCollection();
					string baseUrl = string.Empty;
					Net.ParseUrl(url, out baseUrl, out nvc);
					string text = nvc["type"];
					if (!string.IsNullOrEmpty(text) && text.Equals("wechat"))
					{
						Api.Instance.CheckWeChatToken(appid, nvc["refresh_token"], "web", delegate
						{
							if (callback != null)
							{
								callback();
							}
						}, delegate(int code)
						{
							switch (code)
							{
							case 403:
							case 500:
							{
								double num3 = double.Parse(DataStorage.LoadString(THIRD_PARTY_TOKEN_CHECK_FAILED_TIMESTAMP + "." + appid));
								double totalMilliseconds3 = (DateTime.UtcNow - new DateTime(1970, 1, 1)).TotalMilliseconds;
								if (Math.Abs(num3) < 1.0)
								{
									DataStorage.SaveString(THIRD_PARTY_TOKEN_CHECK_FAILED_TIMESTAMP + "." + appid, totalMilliseconds3.ToString());
									if (callback != null)
									{
										callback();
									}
								}
								else if (Math.Abs(num3 - totalMilliseconds3) < THIRD_PARTY_TOKEN_VERIFY_TIME)
								{
									if (callback != null)
									{
										callback();
									}
								}
								else
								{
									ClearToken(appid);
									ClearThirdPartyToken(appid);
									if (callback != null)
									{
										callback();
									}
								}
								break;
							}
							case 400:
								ClearToken(appid);
								ClearThirdPartyToken(appid);
								if (callback != null)
								{
									callback();
								}
								break;
							default:
								if (callback != null)
								{
									callback();
								}
								break;
							}
						});
					}
					else if (!string.IsNullOrEmpty(text) && text.Equals("qq"))
					{
						Api.Instance.CheckQQToken(appid, nvc["open_id"], nvc["token"], "web", delegate
						{
							if (callback != null)
							{
								callback();
							}
						}, delegate(int code)
						{
							switch (code)
							{
							case 403:
							case 500:
							{
								double num2 = double.Parse(DataStorage.LoadString(THIRD_PARTY_TOKEN_CHECK_FAILED_TIMESTAMP + "." + appid));
								double totalMilliseconds2 = (DateTime.UtcNow - new DateTime(1970, 1, 1)).TotalMilliseconds;
								if (Math.Abs(num2) < 1.0)
								{
									DataStorage.SaveString(THIRD_PARTY_TOKEN_CHECK_FAILED_TIMESTAMP + "." + appid, totalMilliseconds2.ToString());
									if (callback != null)
									{
										callback();
									}
								}
								else if (Math.Abs(num2 - totalMilliseconds2) < THIRD_PARTY_TOKEN_VERIFY_TIME)
								{
									if (callback != null)
									{
										callback();
									}
								}
								else
								{
									ClearToken(appid);
									ClearThirdPartyToken(appid);
									if (callback != null)
									{
										callback();
									}
								}
								break;
							}
							case 400:
								ClearToken(appid);
								ClearThirdPartyToken(appid);
								if (callback != null)
								{
									callback();
								}
								break;
							default:
								if (callback != null)
								{
									callback();
								}
								break;
							}
						});
					}
					else if (!string.IsNullOrEmpty(text) && text.Equals("taptap"))
					{
						Api.Instance.CheckTapTapToken(appid, nvc["mac_key"], nvc["kid"], nvc["token_type"], nvc["access_token"], nvc["mac_algorithm"], "web", delegate
						{
							if (callback != null)
							{
								callback();
							}
						}, delegate(int code)
						{
							switch (code)
							{
							case 403:
							case 500:
							{
								double num = double.Parse(DataStorage.LoadString(THIRD_PARTY_TOKEN_CHECK_FAILED_TIMESTAMP + "." + appid));
								double totalMilliseconds = (DateTime.UtcNow - new DateTime(1970, 1, 1)).TotalMilliseconds;
								if (Math.Abs(num) < 1.0)
								{
									DataStorage.SaveString(THIRD_PARTY_TOKEN_CHECK_FAILED_TIMESTAMP + "." + appid, totalMilliseconds.ToString());
									if (callback != null)
									{
										callback();
									}
								}
								else if (Math.Abs(num - totalMilliseconds) < THIRD_PARTY_TOKEN_VERIFY_TIME)
								{
									if (callback != null)
									{
										callback();
									}
								}
								else
								{
									ClearToken(appid);
									ClearThirdPartyToken(appid);
									if (callback != null)
									{
										callback();
									}
								}
								break;
							}
							case 400:
								ClearToken(appid);
								ClearThirdPartyToken(appid);
								if (callback != null)
								{
									callback();
								}
								break;
							default:
								if (callback != null)
								{
									callback();
								}
								break;
							}
						});
					}
					else
					{
						ClearThirdPartyToken(appid);
						if (callback != null)
						{
							callback();
						}
					}
				}
				else if (callback != null)
				{
					callback();
				}
			}
			catch (Exception)
			{
				ClearThirdPartyToken(appid);
				ClearToken(appid);
				if (callback != null)
				{
					callback();
				}
			}
		}
	}
}
