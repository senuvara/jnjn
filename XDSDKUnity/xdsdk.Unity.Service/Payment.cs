using System;
using System.Collections.Generic;
using UnityEngine;

namespace xdsdk.Unity.Service
{
	public class Payment
	{
		public static void URL(string appid, string token, Dictionary<string, string> info, Action<string> methodForResult, Action<string> methodForError)
		{
			if (info != null && info.ContainsKey("Product_Name") && info.ContainsKey("Product_Id") && info.ContainsKey("Product_Price") && info.ContainsKey("Sid") && info.ContainsKey("Role_Id") && info.ContainsKey("OrderId") && info.ContainsKey("EXT"))
			{
				try
				{
					string value = WWW.EscapeURL(info["Product_Name"]);
					string value2 = info["Product_Id"];
					string value3 = info["Role_Id"];
					string value4 = info["Sid"];
					string value5 = info["OrderId"];
					string value6 = Convert.ToString((float)int.Parse(info["Product_Price"]) / 100f);
					Dictionary<string, string> dictionary = new Dictionary<string, string>();
					dictionary.Add("client_id", appid);
					dictionary.Add("access_token", token);
					dictionary.Add("product_name", value);
					dictionary.Add("product_id", value2);
					dictionary.Add("role_id", value3);
					dictionary.Add("sid", value4);
					dictionary.Add("order_id", value5);
					dictionary.Add("amount", value6);
					Dictionary<string, string> dictionary2 = dictionary;
					if (info.ContainsKey("EXT"))
					{
						dictionary2.Add("ext", WWW.EscapeURL(info["EXT"]));
					}
					methodForResult?.Invoke("http://www.xd.com/orders/pcsdk_create_order?" + Net.DictToQueryString(dictionary2));
				}
				catch (Exception ex)
				{
					methodForError?.Invoke(ex.Message);
				}
			}
			else
			{
				methodForError?.Invoke("Wrong paramaters.");
			}
		}
	}
}
