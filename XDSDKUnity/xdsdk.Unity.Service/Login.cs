using System;
using System.Collections.Generic;
using System.Security.Cryptography;
using System.Text;

namespace xdsdk.Unity.Service
{
	public static class Login
	{
		public static void QQ(string appid, string openID, string accessToken, Action<Dictionary<string, object>> methodForResult, Action<string> methodForError)
		{
			Api.Instance.LoginQQ(appid, openID, accessToken, delegate(string token)
			{
				User(token, delegate(User user)
				{
					Dictionary<string, object> obj = new Dictionary<string, object>
					{
						{
							"user",
							user
						},
						{
							"token",
							token
						}
					};
					Dictionary<string, string> tokenDict = new Dictionary<string, string>
					{
						{
							"open_id",
							openID
						},
						{
							"token",
							accessToken
						}
					};
					Token.SetThirdPartyToken(appid, Token.ThirdPartyTokenType.QQ, tokenDict);
					if (methodForResult != null)
					{
						methodForResult(obj);
					}
				}, delegate(string error)
				{
					if (methodForError != null)
					{
						methodForError(error);
					}
				});
			}, delegate(string error)
			{
				if (methodForError != null)
				{
					methodForError(error);
				}
			});
		}

		public static void Wechat(string appid, string code, Action<Dictionary<string, object>> methodForResult, Action<string> methodForError)
		{
			Api.Instance.LoginWechat(appid, code, delegate(string token, string refreshToken)
			{
				User(token, delegate(User user)
				{
					Dictionary<string, object> obj = new Dictionary<string, object>
					{
						{
							"user",
							user
						},
						{
							"token",
							token
						}
					};
					Dictionary<string, string> tokenDict = new Dictionary<string, string>
					{
						{
							"refresh_token",
							refreshToken
						}
					};
					Token.SetThirdPartyToken(appid, Token.ThirdPartyTokenType.Wechat, tokenDict);
					if (methodForResult != null)
					{
						methodForResult(obj);
					}
				}, delegate(string error)
				{
					if (methodForError != null)
					{
						methodForError(error);
					}
				});
			}, delegate(string error)
			{
				if (methodForError != null)
				{
					methodForError(error);
				}
			});
		}

		public static void TapTap(string appid, string kid, string accessToken, string tokenType, string macKey, string macAlgorithm, Action<Dictionary<string, object>> methodForResult, Action<string> methodForError)
		{
			Api.Instance.LoginTapTap(appid, kid, accessToken, tokenType, macKey, macAlgorithm, delegate(string token)
			{
				User(token, delegate(User user)
				{
					Dictionary<string, object> obj = new Dictionary<string, object>
					{
						{
							"user",
							user
						},
						{
							"token",
							token
						}
					};
					Dictionary<string, string> tokenDict = new Dictionary<string, string>
					{
						{
							"mac_key",
							macKey
						},
						{
							"kid",
							kid
						},
						{
							"token_type",
							tokenType
						},
						{
							"access_token",
							accessToken
						},
						{
							"mac_algorithm",
							macAlgorithm
						}
					};
					Token.SetThirdPartyToken(appid, Token.ThirdPartyTokenType.TapTap, tokenDict);
					if (methodForResult != null)
					{
						methodForResult(obj);
					}
				}, delegate(string error)
				{
					if (methodForError != null)
					{
						methodForError(error);
					}
				});
			}, delegate(string error)
			{
				if (methodForError != null)
				{
					methodForError(error);
				}
			});
		}

		public static void XD(string appid, string username, string password, string code, Action<Dictionary<string, object>> methodForResult, Action<string, object> methodForError)
		{
			Api.Instance.LoginXD(appid, username, password, null, code, delegate(string token)
			{
				User(token, delegate(User user)
				{
					Dictionary<string, object> obj = new Dictionary<string, object>
					{
						{
							"user",
							user
						},
						{
							"token",
							token
						}
					};
					if (methodForResult != null)
					{
						methodForResult(obj);
					}
				}, delegate(string error)
				{
					if (methodForError != null)
					{
						methodForError("error", error);
					}
				});
			}, delegate(string errorType, object errorContent)
			{
				if (methodForError != null)
				{
					methodForError(errorType, errorContent);
				}
			});
		}

		public static void XD(string appid, string username, string password, Action<Dictionary<string, object>> methodForResult, Action<string, object> methodForError)
		{
			Api.Instance.LoginXD(appid, username, password, null, null, delegate(string token)
			{
				User(token, delegate(User user)
				{
					Dictionary<string, object> obj = new Dictionary<string, object>
					{
						{
							"user",
							user
						},
						{
							"token",
							token
						}
					};
					if (methodForResult != null)
					{
						methodForResult(obj);
					}
				}, delegate(string error)
				{
					if (methodForError != null)
					{
						methodForError("error", error);
					}
				});
			}, delegate(string errorType, object errorContent)
			{
				if (methodForError != null)
				{
					methodForError(errorType, errorContent);
				}
			});
		}

		public static void VerificationCode(string appid, string userID, Action<string> methodForResult, Action<string> methodForError)
		{
			Api.Instance.FetchVerificationCode(appid, userID, delegate(string result)
			{
				if (methodForResult != null)
				{
					methodForResult(result);
				}
			}, delegate(string errorType)
			{
				if (methodForError != null)
				{
					methodForError(errorType);
				}
			});
		}

		public static void User(string accessToken, Action<User> methodForResult, Action<string> methodForError)
		{
			Api.Instance.GetUser(accessToken, delegate(User result)
			{
				if (methodForResult != null)
				{
					methodForResult(result);
				}
			}, delegate(string error)
			{
				if (methodForError != null)
				{
					methodForError(error);
				}
			});
		}

		public static void RealName(string accessToken, string name, string id, Action<string> methodForResult, Action<string> methodForError)
		{
			Api.Instance.VerifyRealName(accessToken, name, id, delegate(string result)
			{
				if (methodForResult != null)
				{
					methodForResult(result);
				}
			}, delegate(string error)
			{
				if (methodForError != null)
				{
					methodForError(error);
				}
			});
		}

		public static void Logout(Action method)
		{
			Api.Instance.Logout(method);
		}

		private static string Md5Sum(string strToEncrypt)
		{
			UTF8Encoding uTF8Encoding = new UTF8Encoding();
			byte[] bytes = uTF8Encoding.GetBytes(strToEncrypt);
			MD5CryptoServiceProvider mD5CryptoServiceProvider = new MD5CryptoServiceProvider();
			byte[] array = mD5CryptoServiceProvider.ComputeHash(bytes);
			string text = string.Empty;
			for (int i = 0; i < array.Length; i++)
			{
				text += Convert.ToString(array[i], 16).PadLeft(2, '0');
			}
			return text.PadLeft(32, '0');
		}
	}
}
