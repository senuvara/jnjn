using System.Collections.Generic;

namespace xdsdk.Unity
{
	public class LoginEntry
	{
		public enum Type
		{
			WX,
			Tap,
			QQ,
			Guest,
			XD,
			None
		}

		private Type first;

		private Type second = Type.QQ;

		private Type third = Type.Tap;

		private Type fourth = Type.None;

		public Type First => first;

		public Type Second => second;

		public Type Third => third;

		public Type Fourth => fourth;

		public LoginEntry()
		{
		}

		public LoginEntry(string[] entries)
			: this()
		{
			if (entries != null)
			{
				SetStringArrayToCurrentEntries(entries);
			}
			else
			{
				SetDefaultEntries();
			}
		}

		public void HideGuest()
		{
			SetStringArrayToCurrentEntries(RemoveStringFromArray("GUEST_LOGIN", CurrentEntriesToStringArray()));
		}

		public void HideWX()
		{
			SetStringArrayToCurrentEntries(RemoveStringFromArray("WX_LOGIN", CurrentEntriesToStringArray()));
			third = Type.XD;
		}

		public void HideQQ()
		{
			SetStringArrayToCurrentEntries(RemoveStringFromArray("QQ_LOGIN", CurrentEntriesToStringArray()));
		}

		public void HideTapTap()
		{
			SetStringArrayToCurrentEntries(RemoveStringFromArray("TAPTAP_LOGIN", CurrentEntriesToStringArray()));
		}

		public void SetEntries(string[] entries)
		{
			SetStringArrayToCurrentEntries(entries);
		}

		private void SetDefaultEntries()
		{
			first = Type.WX;
			second = Type.Tap;
			third = Type.QQ;
			fourth = Type.None;
		}

		private void SetStringArrayToCurrentEntries(string[] entries)
		{
			first = ((entries.Length <= 0) ? Type.None : ParseEntryToType(entries[0]));
			second = ((entries.Length <= 1) ? Type.None : ParseEntryToType(entries[1]));
			third = ((entries.Length <= 2) ? Type.None : ParseEntryToType(entries[2]));
			fourth = ((entries.Length <= 3) ? Type.None : ParseEntryToType(entries[3]));
		}

		private string[] CurrentEntriesToStringArray()
		{
			return new string[4]
			{
				EntryToString(first),
				EntryToString(second),
				EntryToString(third),
				EntryToString(fourth)
			};
		}

		private static string[] RemoveStringFromArray(string s, string[] array)
		{
			for (int i = 0; i < array.Length; i++)
			{
				if (!string.IsNullOrEmpty(array[i]) && array[i].Equals(s))
				{
					array[i] = null;
				}
			}
			return TrimStringArray(array);
		}

		private static string EntryToString(Type entry)
		{
			switch (entry)
			{
			case Type.WX:
				return "WX_LOGIN";
			case Type.Tap:
				return "TAPTAP_LOGIN";
			case Type.QQ:
				return "QQ_LOGIN";
			case Type.Guest:
				return "GUEST_LOGIN";
			case Type.XD:
				return "XD_LOGIN";
			default:
				return null;
			}
		}

		private static Type ParseEntryToType(string entry)
		{
			return string.IsNullOrEmpty(entry) ? Type.None : ((!entry.Equals("WX_LOGIN")) ? (entry.Equals("TAPTAP_LOGIN") ? Type.Tap : (entry.Equals("QQ_LOGIN") ? Type.QQ : (entry.Equals("GUEST_LOGIN") ? Type.Guest : ((!entry.Equals("XD_LOGIN")) ? Type.None : Type.XD)))) : Type.WX);
		}

		private static string[] TrimStringArray(string[] array)
		{
			List<string> list = new List<string>();
			string[] array2 = array;
			foreach (string text in array2)
			{
				if (!string.IsNullOrEmpty(text))
				{
					list.Add(text);
				}
			}
			array = list.ToArray();
			return array;
		}
	}
}
