using UnityEngine;

namespace xdsdk.Unity
{
	public class UIBase : MonoBehaviour
	{
		public virtual void OnEnter()
		{
			Debug.Log(base.name + " : OnEnter");
		}

		public virtual void OnPause()
		{
			Debug.Log(base.name + " : OnPause");
			base.gameObject.SetActive(value: false);
		}

		public virtual void OnResume()
		{
			Debug.Log(base.name + " : OnResume");
			base.gameObject.SetActive(value: true);
		}

		public virtual void OnExit()
		{
			Debug.Log(base.name + " : OnExit");
			Object.Destroy(base.gameObject);
		}
	}
}
