using System;
using System.Collections;
using System.Collections.Generic;
using System.Collections.Specialized;
using System.Text;
using System.Text.RegularExpressions;
using UnityEngine;
using xdsdk.Unity.MiniJSON;

namespace xdsdk.Unity
{
	[DisallowMultipleComponent]
	public class Net : MonoBehaviour
	{
		public void GetOnServer(string url, Dictionary<string, string> parameters, Action<string> methodForResult, Action<int, string> methodForError)
		{
			if (string.IsNullOrEmpty(url))
			{
				methodForError(-1, "empty url");
			}
			else
			{
				StartCoroutine(Get(url, parameters, methodForResult, methodForError));
			}
		}

		public void GetOnServer(string url, Action<string> methodForResult, Action<int, string> methodForError)
		{
			if (string.IsNullOrEmpty(url))
			{
				methodForError(-1, "empty url");
			}
			else
			{
				StartCoroutine(Get(url, null, methodForResult, methodForError));
			}
		}

		public void PostOnServer(string url, Dictionary<string, object> parameters, Action<string> methodForResult, Action<int, string> methodForError)
		{
			if (string.IsNullOrEmpty(url))
			{
				methodForError(-1, "empty url");
			}
			else
			{
				StartCoroutine(Post(url, null, parameters, methodForResult, methodForError));
			}
		}

		public void PostOnServer(string url, Dictionary<string, string> headers, Dictionary<string, object> parameters, Action<string> methodForResult, Action<int, string> methodForError)
		{
			if (string.IsNullOrEmpty(url))
			{
				methodForError(-1, "empty url");
			}
			else
			{
				StartCoroutine(Post(url, headers, parameters, methodForResult, methodForError));
			}
		}

		private IEnumerator Post(string url, Dictionary<string, string> headers, Dictionary<string, object> parameters, Action<string> methodForResult, Action<int, string> methodForError)
		{
			Dictionary<string, object> finalParameter = parameters ?? new Dictionary<string, object>();
			finalParameter.Add("did", DataStorage.GetUniqueID());
			finalParameter.Add("system", "Windows");
			string jsonString = Json.Serialize(finalParameter);
			Dictionary<string, string> postHeader = new Dictionary<string, string>
			{
				{
					"Content-Type",
					"application/json"
				}
			};
			if (headers != null)
			{
				Dictionary<string, string>.KeyCollection keys = headers.Keys;
				foreach (string item in keys)
				{
					postHeader.Add(item, headers[item]);
				}
			}
			byte[] formData = Encoding.UTF8.GetBytes(jsonString);
			WWW w = new WWW(url, formData, postHeader);
			yield return w;
			if (!string.IsNullOrEmpty(w.error))
			{
				Debug.LogError(w.error);
				methodForError(GetResponseCode(w), w.error);
				w.Dispose();
				yield break;
			}
			string text = w.text;
			if (text != null)
			{
				methodForResult(w.text);
			}
			else
			{
				methodForError(GetResponseCode(w), "Empyt response from server : " + url);
			}
		}

		private IEnumerator Get(string url, Dictionary<string, string> parameters, Action<string> methodForResult, Action<int, string> methodForError)
		{
			string finalUrl = string.Empty;
			string system2 = string.Empty;
			Dictionary<string, string> finalParameter = parameters ?? new Dictionary<string, string>();
			system2 = "Windows";
			finalParameter.Add("system", system2);
			finalParameter.Add("did", DataStorage.GetUniqueID());
			NameValueCollection collection = new NameValueCollection();
			ParseUrl(url, out string baseUrl, out collection);
			string[] allKeys = collection.AllKeys;
			foreach (string text in allKeys)
			{
				string[] values = collection.GetValues(text);
				foreach (string value in values)
				{
					finalParameter.Add(text, value);
				}
			}
			finalUrl = baseUrl + "?" + DictToQueryString(finalParameter);
			WWW w = new WWW(finalUrl);
			yield return w;
			if (!string.IsNullOrEmpty(w.error))
			{
				Debug.LogError(w.error);
				methodForError(GetResponseCode(w), w.error);
				w.Dispose();
				yield break;
			}
			string text2 = w.text;
			if (text2 != null)
			{
				methodForResult(w.text);
			}
			else
			{
				methodForError(GetResponseCode(w), "Empyt response from server : " + url);
			}
		}

		public static int GetResponseCode(WWW request)
		{
			int result = 0;
			if (request.responseHeaders == null)
			{
				Debug.LogError("no response headers.");
			}
			else if (!request.responseHeaders.ContainsKey("STATUS"))
			{
				Debug.LogError("response headers has no STATUS.");
			}
			else
			{
				result = ParseResponseCode(request.responseHeaders["STATUS"]);
			}
			return result;
		}

		public static int ParseResponseCode(string statusLine)
		{
			int result = 0;
			string[] array = statusLine.Split(' ');
			if (array.Length < 3)
			{
				Debug.LogError("invalid response status: " + statusLine);
			}
			else if (!int.TryParse(array[1], out result))
			{
				Debug.LogError("invalid response code: " + array[1]);
			}
			return result;
		}

		public static string DictToQueryString(IDictionary<string, string> dict)
		{
			List<string> list = new List<string>();
			foreach (KeyValuePair<string, string> item in dict)
			{
				list.Add(item.Key + "=" + item.Value);
			}
			return string.Join("&", list.ToArray());
		}

		public static void ParseUrl(string url, out string baseUrl, out NameValueCollection nvc)
		{
			if (url == null)
			{
				throw new ArgumentNullException("url");
			}
			nvc = new NameValueCollection();
			baseUrl = string.Empty;
			if (url == string.Empty)
			{
				return;
			}
			int num = url.IndexOf('?');
			if (num == -1)
			{
				baseUrl = url;
				return;
			}
			baseUrl = url.Substring(0, num);
			if (num != url.Length - 1)
			{
				string input = url.Substring(num + 1);
				Regex regex = new Regex("(^|&)?(\\w+)=([^&]+)(&|$)?", RegexOptions.None);
				MatchCollection matchCollection = regex.Matches(input);
				IEnumerator enumerator = matchCollection.GetEnumerator();
				try
				{
					while (enumerator.MoveNext())
					{
						Match match = (Match)enumerator.Current;
						nvc.Add(match.Result("$2").ToLower(), match.Result("$3"));
					}
				}
				finally
				{
					IDisposable disposable;
					if ((disposable = (enumerator as IDisposable)) != null)
					{
						disposable.Dispose();
					}
				}
			}
		}
	}
}
