using System;
using System.Collections.Generic;
using UnityEngine;
using ZenFulcrum.EmbeddedBrowser;

namespace xdsdk.Unity
{
	[DisallowMultipleComponent]
	public class SDKManager : MonoBehaviour
	{
		public static readonly int RESULT_FAILED = -1;

		public static readonly int RESULT_SUCCESS;

		public static readonly int RESULT_BACK = 1;

		public static readonly int RESULT_CLOSE = 2;

		private GameObject containerObj;

		private GameObject dialogObj;

		private GameObject dialogContentObj;

		private GameObject loadingObj;

		private GameObject loadingContentObj;

		private readonly List<UIElement> uiElements = new List<UIElement>();

		private void Awake()
		{
			Browser obj = base.gameObject.AddComponent<Browser>();
			UnityEngine.Object.Destroy(obj);
		}

		public void Pop()
		{
			PopUIElement(null);
		}

		public void Pop(string targetName)
		{
			PopUIElement(targetName);
		}

		public void PopAll()
		{
			if (containerObj != null)
			{
				UIElement exit = null;
				if (uiElements.Count > 0)
				{
					exit = uiElements[uiElements.Count - 1];
				}
				UIAnimator component = UI.GetComponent<UIAnimator>(containerObj);
				component.DoExitAnimation(exit, null, delegate
				{
					for (int num = uiElements.Count - 1; num >= 0; num--)
					{
						uiElements[num].OnExit();
					}
					uiElements.Clear();
					DestoryContainer();
				});
			}
		}

		public void ShowLogin<T>(Dictionary<string, object> configs, Action<int, object> callback) where T : UIElement
		{
			PushUIElement<T>("XDMainLoginWindow", configs, callback);
		}

		public void ShowPlatformLogin<T>(Dictionary<string, object> configs, Action<int, object> callback) where T : UIElement
		{
			PushUIElement<T>("XDPlatformLoginWindow", configs, callback);
		}

		public void ShowWebView<T>(Dictionary<string, object> configs, Action<int, object> callback) where T : UIElement
		{
			PushUIElement<T>("XDWebWindow", configs, callback);
		}

		public void ShowTwoFactorAuth<T>(Dictionary<string, object> configs, Action<int, object> callback) where T : UIElement
		{
			PushUIElement<T>("XDTwoFactorAuthWindow", configs, callback);
		}

		public void ShowRealName<T>(Dictionary<string, object> configs, Action<int, object> callback) where T : UIElement
		{
			PushUIElement<T>("XDRealNameWindow", configs, callback);
		}

		public void ShowDialog(Dictionary<string, object> extra, Action<int, object> callback)
		{
			if (dialogObj == null)
			{
				dialogObj = (UnityEngine.Object.Instantiate(Resources.Load("Prefabs/XDSDK")) as GameObject);
				dialogObj.name = "XDSDKDialog";
				UnityEngine.Object.DontDestroyOnLoad(dialogObj);
				Canvas component = dialogObj.GetComponent<Canvas>();
				component.sortingOrder = 9999;
				UIElement component2 = UI.GetComponent<ContainerWindow>(dialogObj);
				UIAnimator component3 = UI.GetComponent<UIAnimator>(dialogObj);
				component2.OnEnter();
				component3.DoEnterAnimation(null, component2, delegate
				{
				});
			}
			if (dialogContentObj == null)
			{
				dialogContentObj = (UnityEngine.Object.Instantiate(Resources.Load("Prefabs/XDDialog")) as GameObject);
				dialogContentObj.name = "DialogContent";
				UnityEngine.Object.DontDestroyOnLoad(dialogContentObj);
				dialogContentObj.transform.SetParent(dialogObj.transform, worldPositionStays: false);
			}
			UIElement component4 = UI.GetComponent<Dialog>(dialogContentObj);
			UIAnimator component5 = UI.GetComponent<UIAnimator>(dialogContentObj);
			component4.ClearCallback();
			component4.Callback += callback;
			component4.Extra = extra;
			component4.OnEnter();
			component5.DoEnterAnimation(null, component4, delegate
			{
			});
		}

		public void DismissDialog()
		{
			if (dialogContentObj != null)
			{
				UIElement element = UI.GetComponent<Dialog>(dialogContentObj);
				UIAnimator component = UI.GetComponent<UIAnimator>(dialogContentObj);
				component.DoExitAnimation(element, null, delegate
				{
					element.OnExit();
					dialogContentObj = null;
					UIElement component4 = UI.GetComponent<ContainerWindow>(dialogObj);
					UIAnimator component5 = UI.GetComponent<UIAnimator>(dialogObj);
					component4.OnEnter();
					component5.DoExitAnimation(component4, null, delegate
					{
						UnityEngine.Object.Destroy(dialogObj);
						dialogObj = null;
					});
				});
			}
			else if (dialogObj != null)
			{
				UIElement component2 = UI.GetComponent<ContainerWindow>(dialogObj);
				UIAnimator component3 = UI.GetComponent<UIAnimator>(dialogObj);
				component2.OnEnter();
				component3.DoExitAnimation(component2, null, delegate
				{
					UnityEngine.Object.Destroy(dialogObj);
					dialogObj = null;
				});
			}
		}

		public void ShowLoading()
		{
			if (loadingObj == null)
			{
				loadingObj = (UnityEngine.Object.Instantiate(Resources.Load("Prefabs/XDSDK")) as GameObject);
				loadingObj.name = "XDSDKLoading";
				UnityEngine.Object.DontDestroyOnLoad(loadingObj);
				Canvas component = loadingObj.GetComponent<Canvas>();
				component.sortingOrder = 10000;
				UIElement component2 = UI.GetComponent<ContainerWindow>(loadingObj);
				UIAnimator component3 = UI.GetComponent<UIAnimator>(loadingObj);
				component2.OnEnter();
				component3.DoEnterAnimation(null, component2, delegate
				{
				});
			}
			if (loadingContentObj == null)
			{
				loadingContentObj = (UnityEngine.Object.Instantiate(Resources.Load("Prefabs/XDLoading")) as GameObject);
				loadingContentObj.name = "LoadingContent";
				UnityEngine.Object.DontDestroyOnLoad(loadingContentObj);
				loadingContentObj.transform.SetParent(loadingObj.transform, worldPositionStays: false);
			}
			UIElement component4 = UI.GetComponent<Loading>(loadingContentObj);
			UIAnimator component5 = UI.GetComponent<UIAnimator>(loadingContentObj);
			component4.OnEnter();
			component5.DoEnterAnimation(null, component4, delegate
			{
			});
		}

		public void DismissLoading()
		{
			if (loadingContentObj != null)
			{
				UIElement element = UI.GetComponent<Loading>(loadingContentObj);
				UIAnimator component = UI.GetComponent<UIAnimator>(loadingContentObj);
				component.DoExitAnimation(element, null, delegate
				{
					element.OnExit();
					loadingContentObj = null;
					UIElement component4 = UI.GetComponent<ContainerWindow>(loadingObj);
					UIAnimator component5 = UI.GetComponent<UIAnimator>(loadingObj);
					component4.OnEnter();
					component5.DoExitAnimation(component4, null, delegate
					{
						UnityEngine.Object.Destroy(loadingObj);
						loadingObj = null;
					});
				});
			}
			else if (loadingObj != null)
			{
				UIElement component2 = UI.GetComponent<ContainerWindow>(loadingObj);
				UIAnimator component3 = UI.GetComponent<UIAnimator>(loadingObj);
				component2.OnEnter();
				component3.DoExitAnimation(component2, null, delegate
				{
					UnityEngine.Object.Destroy(loadingObj);
					loadingObj = null;
				});
			}
		}

		private void CreateContainer()
		{
			containerObj = (UnityEngine.Object.Instantiate(Resources.Load("Prefabs/XDSDK")) as GameObject);
			containerObj.name = "XDSDKContainer";
			UnityEngine.Object.DontDestroyOnLoad(containerObj);
			UIElement component = UI.GetComponent<ContainerWindow>(containerObj);
			UIAnimator component2 = UI.GetComponent<UIAnimator>(containerObj);
			component.OnEnter();
			component2.DoEnterAnimation(null, component, delegate
			{
			});
		}

		private void DestoryContainer()
		{
			if (containerObj != null)
			{
				UIElement component = UI.GetComponent<ContainerWindow>(containerObj);
				UIAnimator component2 = UI.GetComponent<UIAnimator>(containerObj);
				component.OnEnter();
				component2.DoExitAnimation(component, null, delegate
				{
					UnityEngine.Object.Destroy(containerObj);
					containerObj = null;
				});
			}
		}

		private void PushUIElement<T>(string prefabName, Dictionary<string, object> extra, Action<int, object> callback) where T : UIElement
		{
			GameObject gameObject = UnityEngine.Object.Instantiate(Resources.Load("Prefabs/" + prefabName)) as GameObject;
			if (gameObject == null)
			{
				Debug.LogError("Could not find prefab named \"" + prefabName + "\"");
				return;
			}
			if (uiElements.Count == 0 && containerObj == null)
			{
				CreateContainer();
			}
			gameObject.name = prefabName;
			UnityEngine.Object.DontDestroyOnLoad(gameObject);
			UIElement component = UI.GetComponent<T>(gameObject);
			component.Extra = extra;
			component.Callback += callback;
			component.transform.SetParent(containerObj.transform, worldPositionStays: false);
			UIElement lastElement = null;
			if (uiElements.Count > 0)
			{
				lastElement = uiElements[uiElements.Count - 1];
			}
			uiElements.Add(component);
			UIAnimator component2 = UI.GetComponent<UIAnimator>(containerObj);
			component.OnEnter();
			component2.DoEnterAnimation(lastElement, component, delegate
			{
				if (lastElement != null)
				{
					lastElement.OnPause();
				}
			});
		}

		private void PopUIElement(string targetName)
		{
			if (containerObj == null || uiElements.Count == 0)
			{
				Debug.LogError("No UIElement can be popped.");
				return;
			}
			UIElement element = uiElements[uiElements.Count - 1];
			if (targetName != null && !targetName.Equals(element.name))
			{
				Debug.LogError("Could not find specify UIElement : " + targetName);
				return;
			}
			uiElements.RemoveAt(uiElements.Count - 1);
			UIElement lastElement = null;
			if (uiElements.Count > 0)
			{
				lastElement = uiElements[uiElements.Count - 1];
			}
			UIAnimator component = UI.GetComponent<UIAnimator>(containerObj);
			if (lastElement != null)
			{
				lastElement.OnResume();
			}
			component.DoExitAnimation(element, lastElement, delegate
			{
				element.OnExit();
				if (lastElement != null)
				{
				}
				if (uiElements.Count == 0)
				{
					DestoryContainer();
				}
			});
		}
	}
}
