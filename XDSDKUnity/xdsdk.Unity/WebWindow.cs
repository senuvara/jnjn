using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;
using xdsdk.Unity.MiniJSON;
using ZenFulcrum.EmbeddedBrowser;

namespace xdsdk.Unity
{
	public class WebWindow : UIElement
	{
		public Button close;

		public Button back;

		public Browser browser;

		private string url = string.Empty;

		private string lastUrl = string.Empty;

		private string containsUrl = string.Empty;

		public override Dictionary<string, object> Extra
		{
			get
			{
				return extra;
			}
			set
			{
				extra = value;
				if (extra != null)
				{
					if (extra.ContainsKey("url"))
					{
						url = (extra["url"] as string);
						browser.Url = url;
					}
					if (extra.ContainsKey("contains_url"))
					{
						containsUrl = (extra["contains_url"] as string);
					}
				}
			}
		}

		private void Awake()
		{
			browser.onNavStateChange += delegate
			{
				Debug.Log("onNavStateChange : " + browser.Url);
				HandleUrl(browser.Url);
			};
			browser.onFetchError += delegate(JSONNode error)
			{
				string asJSON = error.AsJSON;
				Dictionary<string, object> dictionary = Json.Deserialize(asJSON) as Dictionary<string, object>;
				if (dictionary.ContainsKey("url"))
				{
					Debug.Log("onFetchError : " + dictionary["url"]);
					HandleUrl(dictionary["url"] as string);
				}
			};
			close.onClick.AddListener(OnCloseClicked);
			back.onClick.AddListener(delegate
			{
				string text = browser.Url;
				if (browser.CanGoBack)
				{
					browser.GoBack();
				}
				else
				{
					OnCloseClicked();
				}
			});
			transitionDurationTime = 0.2f;
			defaultSelected = close;
			selectableItems = new List<Selectable>();
			selectableItems.Add(close);
			foreach (Selectable selectableItem in selectableItems)
			{
				RegisterEventTriggerListener(selectableItem.gameObject);
			}
		}

		protected override void FocusTo(GameObject go)
		{
			RectTransform component = go.GetComponent<RectTransform>();
			float width = component.rect.width;
			float height = component.rect.height;
			if (go == close.gameObject)
			{
				focus.transform.position = go.transform.position;
				focus.rectTransform.sizeDelta = new Vector2(width + 10f, height + 10f);
				ChangeImageSprite(focus, "xdsdk_unity_button_selected_close");
			}
			focus.gameObject.SetActive(value: true);
		}

		private void HandleUrl(string urlStr)
		{
			if (containsUrl != null && urlStr != null && (lastUrl == null || !lastUrl.Equals(urlStr)) && urlStr.Contains(containsUrl))
			{
				OnCallback(SDKManager.RESULT_SUCCESS, urlStr);
				GetSDKManager().Pop(base.name);
			}
			lastUrl = browser.Url;
		}

		private void OnCloseClicked()
		{
			OnCallback(SDKManager.RESULT_BACK, "Close button clicked");
			GetSDKManager().Pop(base.name);
		}

		public override IEnumerator PlayExit()
		{
			if (!animationLaunched)
			{
				animationLaunched = true;
				float startTime = Time.time;
				float endTime = startTime + transitionDurationTime;
				CanvasGroup canvasGroup = UI.GetComponent<CanvasGroup>(base.gameObject);
				canvasGroup.alpha = 1f;
				while (Time.time < endTime)
				{
					yield return new WaitForEndOfFrame();
					float delta = (Time.time - startTime) / transitionDurationTime;
					canvasGroup.alpha = 1f - delta;
				}
			}
			animationLaunched = false;
			yield return null;
		}

		public override IEnumerator PlayEnter()
		{
			if (!animationLaunched)
			{
				animationLaunched = true;
				float startTime = Time.time;
				float endTime = startTime + transitionDurationTime;
				CanvasGroup canvasGroup = UI.GetComponent<CanvasGroup>(base.gameObject);
				canvasGroup.alpha = 0f;
				while (Time.time < endTime)
				{
					yield return new WaitForEndOfFrame();
					float delta = canvasGroup.alpha = (Time.time - startTime) / transitionDurationTime;
				}
			}
			animationLaunched = false;
			yield return null;
		}
	}
}
