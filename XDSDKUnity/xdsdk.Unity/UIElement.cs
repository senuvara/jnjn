using Rewired;
using System;
using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.EventSystems;
using UnityEngine.UI;

namespace xdsdk.Unity
{
	[DisallowMultipleComponent]
	public class UIElement : UIBase
	{
		protected Dictionary<string, object> extra;

		protected bool animationLaunched;

		protected float transitionDurationTime = 0.2f;

		private static readonly int animType;

		protected Selectable defaultSelected;

		protected List<Selectable> selectableItems;

		protected Action onEscPress;

		private GameObject m_CurrentSelected;

		public Image focus;

		public virtual Dictionary<string, object> Extra
		{
			get
			{
				return extra;
			}
			set
			{
				extra = value;
			}
		}

		public event Action<int, object> Callback;

		public void ClearCallback()
		{
			this.Callback = null;
		}

		protected virtual void OnCallback(int code, object data)
		{
			if (this.Callback != null)
			{
				this.Callback(code, data);
			}
		}

		protected internal virtual float DoPauseAnimation()
		{
			Debug.Log(base.name + " : DoPauseAnimation");
			return 0f;
		}

		protected internal virtual float DoResumeAnimation()
		{
			Debug.Log(base.name + " : DoResumeAnimation");
			return 0f;
		}

		protected internal virtual float DoExitAnimation()
		{
			Debug.Log(base.name + " : DoExitAnimation");
			StartCoroutine(PlayExit());
			return transitionDurationTime;
		}

		protected internal virtual float DoEnterAnimation()
		{
			Debug.Log(base.name + " : DoEnterAnimation");
			StartCoroutine(PlayEnter());
			return transitionDurationTime;
		}

		protected SDKManager GetSDKManager()
		{
			GameObject gameObject = GameObject.Find("XDUIManager");
			return (!(gameObject == null)) ? UI.GetComponent<SDKManager>(gameObject) : null;
		}

		public virtual IEnumerator PlayExit()
		{
			if (animType == 0)
			{
				if (!animationLaunched)
				{
					animationLaunched = true;
					float startTime2 = Time.time;
					float endTime2 = startTime2 + transitionDurationTime;
					CanvasGroup canvasGroup = UI.GetComponent<CanvasGroup>(base.gameObject);
					canvasGroup.alpha = 1f;
					while (Time.time < endTime2)
					{
						yield return new WaitForEndOfFrame();
						float delta2 = (Time.time - startTime2) / transitionDurationTime;
						canvasGroup.alpha = 1f - delta2;
					}
				}
				animationLaunched = false;
				yield return null;
				yield break;
			}
			if (!animationLaunched)
			{
				animationLaunched = true;
				float startTime = Time.time;
				float endTime = startTime + transitionDurationTime;
				RectTransform rectTransform = base.transform.GetComponent<RectTransform>();
				Vector3 position = rectTransform.position;
				float targetX = position.x + (float)(Screen.width / 2) + rectTransform.rect.width / 2f + 200f;
				Vector3 position2 = rectTransform.position;
				float x = position2.x;
				Vector3 position3 = rectTransform.position;
				float y = position3.y;
				Vector3 position4 = rectTransform.position;
				Vector3 startV3 = rectTransform.position = new Vector3(x, y, position4.z);
				while (Time.time < endTime)
				{
					yield return new WaitForEndOfFrame();
					float delta = (Time.time - startTime) / transitionDurationTime;
					Vector3 position5 = rectTransform.position;
					float x2 = position5.x;
					Vector3 position6 = rectTransform.position;
					float x3 = x2 + (targetX - position6.x) * delta;
					Vector3 position7 = rectTransform.position;
					float y2 = position7.y;
					Vector3 position8 = rectTransform.position;
					Vector3 currentV3 = rectTransform.position = new Vector3(x3, y2, position8.z);
				}
			}
			animationLaunched = false;
			yield return null;
		}

		public virtual IEnumerator PlayEnter()
		{
			if (animType == 0)
			{
				if (!animationLaunched)
				{
					animationLaunched = true;
					float startTime2 = Time.time;
					float endTime2 = startTime2 + transitionDurationTime;
					CanvasGroup canvasGroup = UI.GetComponent<CanvasGroup>(base.gameObject);
					canvasGroup.alpha = 0f;
					while (Time.time < endTime2)
					{
						yield return new WaitForEndOfFrame();
						float delta2 = canvasGroup.alpha = (Time.time - startTime2) / transitionDurationTime;
					}
				}
				animationLaunched = false;
				yield return null;
				yield break;
			}
			if (!animationLaunched)
			{
				animationLaunched = true;
				float startTime = Time.time;
				float endTime = startTime + transitionDurationTime;
				RectTransform rectTransform = base.transform.GetComponent<RectTransform>();
				Vector3 position = rectTransform.position;
				float targetX = position.x;
				Vector3 position2 = rectTransform.position;
				float x = position2.x + (float)(Screen.width / 2) + rectTransform.rect.width / 2f + 200f;
				Vector3 position3 = rectTransform.position;
				float y = position3.y;
				Vector3 position4 = rectTransform.position;
				Vector3 startV3 = rectTransform.position = new Vector3(x, y, position4.z);
				while (Time.time < endTime)
				{
					yield return new WaitForEndOfFrame();
					float delta = (Time.time - startTime) / transitionDurationTime;
					Vector3 position5 = rectTransform.position;
					float x2 = position5.x;
					Vector3 position6 = rectTransform.position;
					float x3 = x2 + (targetX - position6.x) * delta;
					Vector3 position7 = rectTransform.position;
					float y2 = position7.y;
					Vector3 position8 = rectTransform.position;
					Vector3 currentV3 = rectTransform.position = new Vector3(x3, y2, position8.z);
				}
			}
			animationLaunched = false;
			yield return null;
		}

		private void OnEnable()
		{
			if (focus != null)
			{
				focus.gameObject.SetActive(value: false);
			}
			EventSystem.current.SetSelectedGameObject(null);
		}

		private void Update()
		{
			if (defaultSelected != null && selectableItems != null)
			{
				FocusOnDefaultOnDemand();
			}
			if (ReInput.isReady && ReInput.players.GetPlayer(0).GetButtonDown("Submit") && (bool)m_CurrentSelected && (bool)m_CurrentSelected.GetComponent<Button>())
			{
				m_CurrentSelected.GetComponent<Button>().onClick.Invoke();
			}
		}

		protected void RegisterEventTriggerListener(GameObject go)
		{
			EventTriggerListener.Get(go).onSelect = OnSelected;
			EventTriggerListener.Get(go).onDeselect = OnDeselected;
			EventTriggerListener.Get(go).onDown = OnDown;
		}

		protected virtual void OnSelected(GameObject go)
		{
			m_CurrentSelected = go;
			FocusTo(go);
		}

		protected virtual void OnDeselected(GameObject go)
		{
			ClearFocus(go);
		}

		protected virtual void OnDown(GameObject go)
		{
			ClearFocus(go);
		}

		protected virtual void OnUp(GameObject go)
		{
			ClearFocus(go);
			EventSystem.current.SetSelectedGameObject(null);
		}

		protected virtual void FocusTo(GameObject go)
		{
		}

		protected virtual void ClearFocus(GameObject go)
		{
			focus.gameObject.SetActive(value: false);
		}

		protected void ChangeImageSprite(Image image, string spriteName)
		{
			image.sprite = Resources.Load<Sprite>("Sprites/" + spriteName);
		}

		private void FocusOnDefaultOnDemand()
		{
			if ((Input.GetKeyDown(KeyCode.UpArrow) || Input.GetKeyDown(KeyCode.DownArrow) || Input.GetKeyDown(KeyCode.LeftArrow) || Input.GetKeyDown(KeyCode.RightArrow)) && CurrentSelectedIndex() == -1)
			{
				defaultSelected.Select();
			}
			if (base.name != "XDWebWindow" && (Input.GetKeyDown(KeyCode.W) || Input.GetKeyDown(KeyCode.S) || Input.GetKeyDown(KeyCode.A) || Input.GetKeyDown(KeyCode.D)) && CurrentSelectedIndex() == -1)
			{
				defaultSelected.Select();
			}
			if (Input.GetKeyDown(KeyCode.Tab))
			{
				if (CurrentSelectedIndex() == -1)
				{
					defaultSelected.Select();
				}
				else
				{
					selectableItems[(CurrentSelectedIndex() + 1) % selectableItems.Count].Select();
				}
			}
			if (!ReInput.isReady)
			{
				return;
			}
			if (ReInput.players.GetPlayer(0).GetButtonDown("Vertical"))
			{
				if (CurrentSelectedIndex() <= 0)
				{
					selectableItems[4 % selectableItems.Count].Select();
				}
				else
				{
					selectableItems[(CurrentSelectedIndex() - 1) % selectableItems.Count].Select();
				}
			}
			else if (ReInput.players.GetPlayer(0).GetNegativeButtonDown("Vertical"))
			{
				selectableItems[(CurrentSelectedIndex() + 1) % selectableItems.Count].Select();
			}
		}

		private int CurrentSelectedIndex()
		{
			if (EventSystem.current.currentSelectedGameObject == null)
			{
				return -1;
			}
			Selectable component = EventSystem.current.currentSelectedGameObject.GetComponent<Selectable>();
			return selectableItems.IndexOf(component);
		}
	}
}
