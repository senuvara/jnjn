using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;
using xdsdk.Unity.Service;

namespace xdsdk.Unity
{
	public class RealNameWindow : UIElement
	{
		public Button close;

		public InputField realname;

		public InputField identifyNumber;

		public Button confirm;

		public Text error1;

		public Image error1Icon;

		public Text error2;

		public Image error2Icon;

		private string token;

		private string type;

		public override Dictionary<string, object> Extra
		{
			get
			{
				return extra;
			}
			set
			{
				extra = value;
				if (extra != null)
				{
					if (extra.ContainsKey("token"))
					{
						token = (extra["token"] as string);
					}
					if (extra.ContainsKey("type"))
					{
						type = (extra["type"] as string);
					}
				}
			}
		}

		private void Awake()
		{
			close.onClick.AddListener(delegate
			{
				if (type.Equals("optional"))
				{
					OnCallback(SDKManager.RESULT_SUCCESS, "Close button clicked");
				}
				else
				{
					OnCallback(SDKManager.RESULT_CLOSE, "Close button clicked");
				}
				GetSDKManager().PopAll();
			});
			realname.onValueChanged.AddListener(delegate
			{
				HideError1();
			});
			identifyNumber.onValueChanged.AddListener(delegate
			{
				HideError2();
			});
			confirm.onClick.AddListener(delegate
			{
				HideError1();
				HideError2();
				if (!string.IsNullOrEmpty(realname.text) && !string.IsNullOrEmpty(identifyNumber.text))
				{
					GetSDKManager().ShowLoading();
					Login.RealName(token, realname.text, identifyNumber.text, delegate(string result)
					{
						GetSDKManager().DismissLoading();
						OnCallback(SDKManager.RESULT_SUCCESS, result);
						GetSDKManager().PopAll();
					}, delegate(string error)
					{
						GetSDKManager().DismissLoading();
						HideError1();
						ShowError2(error);
						Debug.LogError(error);
					});
				}
				else if (string.IsNullOrEmpty(realname.text))
				{
					ShowError1("请输入真实姓名");
				}
				else if (string.IsNullOrEmpty(identifyNumber.text))
				{
					ShowError2("请输入身份证号");
				}
			});
			HideError1();
			HideError2();
			defaultSelected = realname;
			selectableItems = new List<Selectable>();
			selectableItems.Add(realname);
			selectableItems.Add(identifyNumber);
			selectableItems.Add(confirm);
			selectableItems.Add(close);
			foreach (Selectable selectableItem in selectableItems)
			{
				RegisterEventTriggerListener(selectableItem.gameObject);
			}
		}

		private void ShowError1(string error)
		{
			error1.text = error;
			error1.gameObject.SetActive(value: true);
			error1Icon.gameObject.SetActive(value: true);
		}

		private void HideError1()
		{
			error1.gameObject.SetActive(value: false);
			error1Icon.gameObject.SetActive(value: false);
		}

		private void ShowError2(string error)
		{
			error2.text = error;
			error2.gameObject.SetActive(value: true);
			error2Icon.gameObject.SetActive(value: true);
		}

		private void HideError2()
		{
			error2.gameObject.SetActive(value: false);
			error2Icon.gameObject.SetActive(value: false);
		}

		protected override void FocusTo(GameObject go)
		{
			RectTransform component = go.GetComponent<RectTransform>();
			float width = component.rect.width;
			float height = component.rect.height;
			if (go == realname.gameObject || go == identifyNumber.gameObject)
			{
				focus.gameObject.SetActive(value: false);
			}
			else if (go == confirm.gameObject)
			{
				focus.transform.position = go.transform.position;
				focus.rectTransform.sizeDelta = new Vector2(width + 10f, height + 10f);
				ChangeImageSprite(focus, "xdsdk_unity_button_selected_center");
				focus.gameObject.SetActive(value: true);
			}
			else if (go == close.gameObject)
			{
				focus.transform.position = go.transform.position;
				focus.rectTransform.sizeDelta = new Vector2(width + 10f, height + 10f);
				ChangeImageSprite(focus, "xdsdk_unity_button_selected_close");
				focus.gameObject.SetActive(value: true);
			}
		}
	}
}
