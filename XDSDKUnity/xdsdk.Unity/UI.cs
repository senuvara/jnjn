using UnityEngine;

namespace xdsdk.Unity
{
	public static class UI
	{
		public static T GetComponent<T>(GameObject obj) where T : Component
		{
			T val = obj.GetComponent<T>();
			if ((Object)val == (Object)null)
			{
				val = obj.AddComponent<T>();
			}
			return val;
		}
	}
}
