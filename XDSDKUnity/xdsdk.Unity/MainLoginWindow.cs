using System;
using System.Collections.Generic;
using System.Collections.Specialized;
using UnityEngine;
using UnityEngine.UI;
using xdsdk.Unity.Service;

namespace xdsdk.Unity
{
	public class MainLoginWindow : UIElement
	{
		private AppInfo appInfo;

		private LoginEntry loginEntry;

		public Button loginButton1st;

		public Button loginButton2nd;

		public Button loginButton3rd;

		public Button loginButton4th;

		public Button close;

		public override Dictionary<string, object> Extra
		{
			get
			{
				return extra;
			}
			set
			{
				extra = value;
				if (extra != null)
				{
					if (extra.ContainsKey("app_info"))
					{
						appInfo = (extra["app_info"] as AppInfo);
					}
					if (extra.ContainsKey("login_entry"))
					{
						loginEntry = (extra["login_entry"] as LoginEntry);
						ChangeLoginEntries();
					}
				}
			}
		}

		private void Awake()
		{
			loginButton1st.onClick.AddListener(OnFirstEntryClicked);
			loginButton2nd.onClick.AddListener(OnSecondEntryClicked);
			loginButton3rd.onClick.AddListener(OnThirdEntryClicked);
			loginButton4th.onClick.AddListener(OnFourthEntryClicked);
			close.onClick.AddListener(OnCloseClicked);
			defaultSelected = loginButton1st;
			selectableItems = new List<Selectable>();
			selectableItems.Add(loginButton1st);
			selectableItems.Add(loginButton2nd);
			selectableItems.Add(loginButton3rd);
			selectableItems.Add(loginButton4th);
			selectableItems.Add(close);
			foreach (Selectable selectableItem in selectableItems)
			{
				RegisterEventTriggerListener(selectableItem.gameObject);
			}
		}

		private void ChangeLoginEntries()
		{
			if (loginEntry != null)
			{
				ChangeLoginEntry(loginButton1st, loginEntry.First, "center");
				ChangeLoginEntry(loginButton2nd, loginEntry.Second, "center");
				ChangeLoginEntry(loginButton3rd, loginEntry.Third, "bottom");
				ChangeLoginEntry(loginButton4th, loginEntry.Fourth, "bottom");
			}
		}

		private void ChangeLoginEntry(Button button, LoginEntry.Type type, string position)
		{
			if (type != LoginEntry.Type.None)
			{
				button.gameObject.SetActive(value: true);
				switch (type)
				{
				case LoginEntry.Type.QQ:
					ChangeButtonSprite(button, "xdsdk_unity_login_" + position + "_qq");
					break;
				case LoginEntry.Type.Tap:
					ChangeButtonSprite(button, "xdsdk_unity_login_" + position + "_taptap");
					break;
				case LoginEntry.Type.WX:
					ChangeButtonSprite(button, "xdsdk_unity_login_" + position + "_wechat");
					break;
				case LoginEntry.Type.Guest:
					ChangeButtonSprite(button, "xdsdk_unity_login_" + position + "_guest");
					break;
				case LoginEntry.Type.XD:
					ChangeButtonSprite(button, "xdsdk_unity_login_" + position + "_xindong");
					break;
				default:
					button.gameObject.SetActive(value: false);
					break;
				}
			}
			else
			{
				button.gameObject.SetActive(value: false);
			}
		}

		protected override void FocusTo(GameObject go)
		{
			RectTransform component = go.GetComponent<RectTransform>();
			float width = component.rect.width;
			float height = component.rect.height;
			if (go == loginButton1st.gameObject || go == loginButton2nd.gameObject)
			{
				focus.transform.position = go.transform.position;
				focus.rectTransform.sizeDelta = new Vector2(width + 10f, height + 10f);
				ChangeImageSprite(focus, "xdsdk_unity_button_selected_center");
			}
			else if (go == loginButton3rd.gameObject || go == loginButton4th.gameObject)
			{
				focus.transform.position = go.transform.position;
				focus.rectTransform.sizeDelta = new Vector2(width + 15f, height + 15f);
				ChangeImageSprite(focus, "xdsdk_unity_button_selected_bottom");
			}
			else if (go == close.gameObject)
			{
				focus.transform.position = go.transform.position;
				focus.rectTransform.sizeDelta = new Vector2(width + 10f, height + 10f);
				ChangeImageSprite(focus, "xdsdk_unity_button_selected_close");
			}
			focus.gameObject.SetActive(value: true);
		}

		private void ChangeButtonSprite(Button button, string spriteName)
		{
			button.gameObject.GetComponent<Image>().sprite = Resources.Load<Sprite>("Sprites/" + spriteName);
			SpriteState spriteState = button.spriteState;
			spriteState.pressedSprite = Resources.Load<Sprite>("Sprites/" + spriteName + "_pressed");
			button.spriteState = spriteState;
		}

		private void OnFirstEntryClicked()
		{
			LoginByType(loginEntry.First);
			Debug.Log(base.name + " OnFirstEntryClicked");
		}

		private void OnSecondEntryClicked()
		{
			LoginByType(loginEntry.Second);
			Debug.Log(base.name + " OnSecondEntryClicked");
		}

		private void OnThirdEntryClicked()
		{
			LoginByType(loginEntry.Third);
			Debug.Log(base.name + " OnThirdEntryClicked");
		}

		private void OnFourthEntryClicked()
		{
			LoginByType(loginEntry.Fourth);
			Debug.Log(base.name + " OnFourthEntryClicked");
		}

		private void LoginByType(LoginEntry.Type type)
		{
			Dictionary<string, object> dictionary = null;
			switch (type)
			{
			case LoginEntry.Type.Guest:
				break;
			case LoginEntry.Type.WX:
			{
				Dictionary<string, object> dictionary2 = new Dictionary<string, object>();
				dictionary2.Add("url", "https://www.xd.com/oauth/sdk_weixin_login?client_id=" + appInfo.Id);
				dictionary2.Add("contains_url", "oauth/sdk_weixin_callback");
				dictionary = dictionary2;
				GetSDKManager().ShowWebView<WebWindow>(dictionary, delegate(int code, object data)
				{
					if (code == SDKManager.RESULT_SUCCESS)
					{
						HandleWXLogin((string)data);
					}
					else if (code == SDKManager.RESULT_BACK)
					{
						Debug.Log(data);
					}
					else
					{
						Debug.LogError(data);
					}
				});
				break;
			}
			case LoginEntry.Type.QQ:
			{
				Dictionary<string, object> dictionary2 = new Dictionary<string, object>();
				dictionary2.Add("url", "https://www.xd.com/oauth/sdk_qq_login");
				dictionary2.Add("contains_url", "oauth/sdk_qq_token");
				dictionary = dictionary2;
				GetSDKManager().ShowWebView<WebWindow>(dictionary, delegate(int code, object data)
				{
					if (code == SDKManager.RESULT_SUCCESS)
					{
						HandleQQLogin((string)data);
					}
					else if (code == SDKManager.RESULT_BACK)
					{
						Debug.Log(data);
					}
					else
					{
						Debug.LogError(data);
					}
				});
				break;
			}
			case LoginEntry.Type.Tap:
			{
				if (string.IsNullOrEmpty(appInfo.TaptapClientID))
				{
					Debug.LogError("TapTap client id has not been set.");
					break;
				}
				string str = "https://www.taptap.com/oauth2/v1/authorize";
				Dictionary<string, string> dictionary3 = new Dictionary<string, string>();
				dictionary3.Add("client_id", appInfo.TaptapClientID);
				dictionary3.Add("response_type", "token");
				dictionary3.Add("version", XDSDK.VERSION);
				dictionary3.Add("platform", "Windows");
				dictionary3.Add("scope", "public_profile");
				dictionary3.Add("redirect_uri", "tapoauth://authorize");
				dictionary3.Add("state", DataStorage.GetUniqueID());
				dictionary3.Add("secret_type", "hmac-sha-1");
				dictionary3.Add("info", "{}");
				dictionary3.Add("region", "cn");
				Dictionary<string, string> dict = dictionary3;
				Dictionary<string, object> dictionary2 = new Dictionary<string, object>();
				dictionary2.Add("url", str + "?" + Net.DictToQueryString(dict));
				dictionary2.Add("contains_url", "tapoauth://authorize#");
				dictionary = dictionary2;
				GetSDKManager().ShowWebView<WebWindow>(dictionary, delegate(int code, object data)
				{
					if (code == SDKManager.RESULT_SUCCESS)
					{
						HandleTapLogin((string)data);
					}
					else if (code == SDKManager.RESULT_BACK)
					{
						Debug.Log(data);
					}
					else
					{
						Debug.LogError(data);
					}
				});
				break;
			}
			case LoginEntry.Type.XD:
			{
				Dictionary<string, object> dictionary2 = new Dictionary<string, object>();
				dictionary2.Add("app_info", appInfo);
				dictionary = dictionary2;
				GetSDKManager().ShowPlatformLogin<PlatformLoginWindow>(dictionary, delegate(int code, object data)
				{
					if (code == SDKManager.RESULT_SUCCESS)
					{
						OnCallback(SDKManager.RESULT_SUCCESS, data);
					}
					else if (code == SDKManager.RESULT_BACK)
					{
						Debug.Log(data);
					}
					else if (code == SDKManager.RESULT_CLOSE)
					{
						Debug.Log(data);
						OnCallback(SDKManager.RESULT_CLOSE, data);
					}
					else
					{
						Debug.LogError(data);
					}
				});
				break;
			}
			}
		}

		private void HandleQQLogin(string url)
		{
			try
			{
				NameValueCollection nvc = new NameValueCollection();
				Net.ParseUrl(url, out string _, out nvc);
				if (nvc.Get("openid") != null && nvc.Get("access_token") != null)
				{
					GetSDKManager().ShowLoading();
					Login.QQ(appInfo.Id, nvc.Get("openid"), nvc.Get("access_token"), delegate(Dictionary<string, object> resultDict)
					{
						GetSDKManager().DismissLoading();
						OnCallback(SDKManager.RESULT_SUCCESS, resultDict);
						GetSDKManager().PopAll();
					}, delegate(string error)
					{
						GetSDKManager().DismissLoading();
						Debug.LogError("QQ login failed." + error);
					});
				}
			}
			catch (Exception ex)
			{
				Debug.LogError("QQ login failed." + ex.Message);
			}
		}

		private void HandleWXLogin(string url)
		{
			try
			{
				NameValueCollection nvc = new NameValueCollection();
				Net.ParseUrl(url, out string _, out nvc);
				if (nvc.Get("code") != null)
				{
					GetSDKManager().ShowLoading();
					Login.Wechat(appInfo.Id, nvc.Get("code"), delegate(Dictionary<string, object> resultDict)
					{
						GetSDKManager().DismissLoading();
						OnCallback(SDKManager.RESULT_SUCCESS, resultDict);
						GetSDKManager().PopAll();
					}, delegate(string error)
					{
						GetSDKManager().DismissLoading();
						Debug.LogError("Wechat login failed." + error);
					});
				}
			}
			catch (Exception ex)
			{
				Debug.LogError("Wechat login failed." + ex.Message);
			}
		}

		private void HandleTapLogin(string url)
		{
			try
			{
				NameValueCollection nvc = new NameValueCollection();
				Net.ParseUrl(url.Replace("#", "?"), out string _, out nvc);
				if (nvc.Get("kid") != null && nvc.Get("access_token") != null && nvc.Get("token_type") != null && nvc.Get("mac_key") != null && nvc.Get("mac_algorithm") != null)
				{
					GetSDKManager().ShowLoading();
					Login.TapTap(appInfo.Id, nvc.Get("kid"), nvc.Get("access_token"), nvc.Get("token_type"), nvc.Get("mac_key"), nvc.Get("mac_algorithm"), delegate(Dictionary<string, object> resultDict)
					{
						GetSDKManager().DismissLoading();
						OnCallback(SDKManager.RESULT_SUCCESS, resultDict);
						GetSDKManager().PopAll();
					}, delegate(string error)
					{
						GetSDKManager().DismissLoading();
						Debug.LogError("TapTap login failed." + error);
					});
				}
			}
			catch (Exception ex)
			{
				Debug.LogError("TapTap login failed." + ex.Message);
			}
		}

		private void OnCloseClicked()
		{
			OnCallback(SDKManager.RESULT_CLOSE, "Close button clicked");
			GetSDKManager().PopAll();
		}
	}
}
