using System;
using System.Collections.Generic;
using UnityEngine;
using xdsdk.Unity.Service;
using ZenFulcrum.EmbeddedBrowser;

namespace xdsdk.Unity
{
	public class XDCore
	{
		private enum SDK_State
		{
			Initialize,
			Initializing,
			Initialized,
			LoggingIn,
			LoggedIn,
			Paying,
			RealName
		}

		private volatile SDK_State state;

		private AppInfo appInfo;

		private User user;

		private GameObject managerObject;

		private readonly LoginEntry loginEntry = new LoginEntry();

		private static XDCore instance;

		public static XDCore Instance
		{
			get
			{
				if (instance == null)
				{
					instance = new XDCore();
				}
				return instance;
			}
		}

		public event Action<ResultCode, string> Callback;

		public void ClearCallback()
		{
			this.Callback = null;
		}

		public void SetLoginEntries(string[] entries)
		{
			loginEntry.SetEntries(entries);
		}

		public void HideGuest()
		{
			loginEntry.HideGuest();
		}

		public void HideWX()
		{
			loginEntry.HideWX();
		}

		public void HideQQ()
		{
			loginEntry.HideQQ();
		}

		public void HideTapTap()
		{
			loginEntry.HideTapTap();
		}

		public void Init(string appid)
		{
			if (this.Callback == null)
			{
				Debug.LogError("Please set callback first.");
			}
			else if (state != 0)
			{
				if (state == SDK_State.Initialized)
				{
					this.Callback(ResultCode.InitSucceed, null);
				}
				else
				{
					this.Callback(ResultCode.InitFailed, "Do not call init while SDK was at state:" + state);
				}
			}
			else if (!string.IsNullOrEmpty(appid))
			{
				state = SDK_State.Initializing;
				UserAgent.SetUserAgent("(Macintosh; Intel Mac OS X 10_9_5) Chrome/45.0.2454.93 XDCustomUA/1 XDUnitySDK " + XDSDK.VERSION);
				Api.Instance.InitSDK(appid, delegate(AppInfo info)
				{
					appInfo = info;
					managerObject = new GameObject
					{
						name = "XDUIManager"
					};
					managerObject.AddComponent<SDKManager>();
					managerObject.AddComponent<Browser>();
					UnityEngine.Object.DontDestroyOnLoad(managerObject);
					state = SDK_State.Initialized;
					this.Callback(ResultCode.InitSucceed, null);
				}, delegate(string error)
				{
					state = SDK_State.Initialize;
					this.Callback(ResultCode.InitFailed, error);
				});
			}
			else
			{
				this.Callback(ResultCode.InitFailed, "a re???");
			}
		}

		public void Login()
		{
			if (state == SDK_State.Initialized || state == SDK_State.LoggedIn)
			{
				state = SDK_State.LoggingIn;
				Token.CheckThirdPartyToken(appInfo.Id, delegate
				{
					if (!string.IsNullOrEmpty(Token.GetToken(appInfo.Id)))
					{
						XDCore xDCore = this;
						string token = Token.GetToken(appInfo.Id);
						xdsdk.Unity.Service.Login.User(Token.GetToken(appInfo.Id), delegate(User user)
						{
							if (user.AuthorizationState == 0 && xDCore.appInfo.NeedLoginRealName != 0)
							{
								Dictionary<string, object> dictionary2 = new Dictionary<string, object>
								{
									{
										"token",
										token
									}
								};
								if (xDCore.appInfo.NeedLoginRealName == 1)
								{
									dictionary2.Add("type", "required");
								}
								else
								{
									dictionary2.Add("type", "optional");
								}
								SDKManager component2 = xDCore.managerObject.GetComponent<SDKManager>();
								component2.ShowRealName<RealNameWindow>(dictionary2, delegate(int code, object result)
								{
									if (code == SDKManager.RESULT_SUCCESS)
									{
										xdsdk.Unity.Service.Login.User(token, delegate(User userAfterRealName)
										{
											xDCore.user = userAfterRealName;
											xDCore.state = SDK_State.LoggedIn;
											xDCore.Callback(ResultCode.LoginSucceed, token);
										}, delegate
										{
											xDCore.user = user;
											xDCore.state = SDK_State.LoggedIn;
											xDCore.Callback(ResultCode.LoginSucceed, token);
										});
									}
									else if (code == SDKManager.RESULT_CLOSE)
									{
										xDCore.state = SDK_State.Initialized;
										xDCore.Callback(ResultCode.LoginCanceled, result.ToString());
									}
									else
									{
										xDCore.state = SDK_State.Initialized;
										xDCore.Callback(ResultCode.LoginFailed, result.ToString());
										Debug.LogError(result.ToString());
									}
								});
							}
							else
							{
								xDCore.user = user;
								xDCore.state = SDK_State.LoggedIn;
								xDCore.Callback(ResultCode.LoginSucceed, token);
							}
						}, delegate(string error)
						{
							Token.ClearToken(xDCore.appInfo.Id);
							xDCore.state = SDK_State.Initialized;
							Debug.LogError(error);
							xDCore.Login();
						});
					}
					else
					{
						SDKManager component = managerObject.GetComponent<SDKManager>();
						if (component != null)
						{
							Dictionary<string, object> configs = new Dictionary<string, object>
							{
								{
									"app_info",
									appInfo
								},
								{
									"login_entry",
									loginEntry
								}
							};
							component.ShowLogin<MainLoginWindow>(configs, delegate(int code, object data)
							{
								if (code == SDKManager.RESULT_SUCCESS)
								{
									if (data.GetType() == typeof(Dictionary<string, object>))
									{
										Dictionary<string, object> dictionary = data as Dictionary<string, object>;
										Token.SetToken(appInfo.Id, dictionary["token"] as string);
										user = (dictionary["user"] as User);
										state = SDK_State.LoggedIn;
										this.Callback(ResultCode.LoginSucceed, dictionary["token"] as string);
									}
									else
									{
										state = SDK_State.Initialized;
										this.Callback(ResultCode.LoginFailed, data.ToString());
									}
								}
								else if (code == SDKManager.RESULT_BACK || code == SDKManager.RESULT_CLOSE)
								{
									state = SDK_State.Initialized;
									this.Callback(ResultCode.LoginCanceled, data.ToString());
								}
								else
								{
									state = SDK_State.Initialized;
									this.Callback(ResultCode.LoginFailed, data.ToString());
								}
							});
						}
						else
						{
							state = SDK_State.Initialized;
							this.Callback(ResultCode.LoginFailed, "Could not open login window.");
						}
					}
				});
			}
			else
			{
				this.Callback(ResultCode.LoginFailed, string.Concat("Pease do not call login in incorrect status (", state, ")."));
			}
		}

		public string GetAccessToken()
		{
			if (state == SDK_State.LoggedIn)
			{
				return Token.GetToken(appInfo.Id);
			}
			return null;
		}

		public void Logout()
		{
			Token.ClearToken(appInfo.Id);
			state = SDK_State.Initialized;
			this.Callback(ResultCode.LogoutSucceed, string.Empty);
		}

		public void Pay(Dictionary<string, string> info)
		{
			if (state != SDK_State.LoggedIn)
			{
				return;
			}
			if (user.AuthorizationState == 0 && appInfo.NeedChargeRealName != 0)
			{
				SDKManager component = managerObject.GetComponent<SDKManager>();
				Dictionary<string, object> dictionary = new Dictionary<string, object>();
				dictionary.Add("token", Token.GetToken(appInfo.Id));
				Dictionary<string, object> dictionary2 = dictionary;
				if (appInfo.NeedChargeRealName == 1)
				{
					dictionary2.Add("type", "required");
				}
				else
				{
					dictionary2.Add("type", "optional");
				}
				state = SDK_State.RealName;
				component.ShowRealName<RealNameWindow>(dictionary2, delegate(int code, object result)
				{
					state = SDK_State.LoggedIn;
					if (code == SDKManager.RESULT_SUCCESS)
					{
						xdsdk.Unity.Service.Login.User(Token.GetToken(appInfo.Id), delegate(User userAfterRealName)
						{
							user = userAfterRealName;
							if (string.IsNullOrEmpty((string)result) || !result.ToString().Equals("Close button clicked"))
							{
								this.Callback(ResultCode.RealNameSucceed, string.Empty);
							}
							PayWithoutRealNameCheck(info);
						}, delegate
						{
							if (string.IsNullOrEmpty((string)result) || !result.ToString().Equals("Close button clicked"))
							{
								this.Callback(ResultCode.RealNameSucceed, string.Empty);
							}
							PayWithoutRealNameCheck(info);
						});
					}
					else
					{
						this.Callback(ResultCode.PayCanceled, "RealName canceled.");
					}
				});
			}
			else
			{
				PayWithoutRealNameCheck(info);
			}
		}

		public void PayWithoutRealNameCheck(Dictionary<string, string> info)
		{
			if (state == SDK_State.LoggedIn)
			{
				state = SDK_State.Paying;
				SDKManager manager = managerObject.GetComponent<SDKManager>();
				Dictionary<string, object> dictionary = new Dictionary<string, object>();
				dictionary.Add("title", "温馨提示");
				dictionary.Add("content", "即将跳转至外部浏览器进行支付，支付完成后请回到游戏进行下一步操作。");
				dictionary.Add("positive", "去支付");
				dictionary.Add("negative", "取消");
				Dictionary<string, object> extra = dictionary;
				manager.ShowDialog(extra, delegate(int code, object data)
				{
					if (code == SDKManager.RESULT_SUCCESS)
					{
						Payment.URL(appInfo.Id, Token.GetToken(appInfo.Id), info, delegate(string result)
						{
							Debug.Log(result);
							Application.OpenURL(result);
						}, delegate
						{
						});
						Dictionary<string, object> extra2 = new Dictionary<string, object>
						{
							{
								"title",
								"温馨提示"
							},
							{
								"content",
								"请在新开页面中完成支付后提交。"
							},
							{
								"positive",
								"已完成"
							},
							{
								"negative",
								"返回修改"
							}
						};
						manager.ShowDialog(extra2, delegate(int resultCode, object resultData)
						{
							if (resultCode == SDKManager.RESULT_SUCCESS)
							{
								state = SDK_State.LoggedIn;
								this.Callback(ResultCode.PayCompleted, string.Empty);
								manager.DismissDialog();
							}
							else
							{
								state = SDK_State.LoggedIn;
								this.Callback(ResultCode.PayCanceled, "User canceled.");
								manager.DismissDialog();
							}
						});
					}
					else
					{
						state = SDK_State.LoggedIn;
						this.Callback(ResultCode.PayCanceled, "User canceled.");
						manager.DismissDialog();
					}
				});
			}
		}

		public void OpenRealName()
		{
			if (state == SDK_State.LoggedIn)
			{
				state = SDK_State.RealName;
				SDKManager component = managerObject.GetComponent<SDKManager>();
				Dictionary<string, object> dictionary = new Dictionary<string, object>();
				dictionary.Add("token", Token.GetToken(appInfo.Id));
				dictionary.Add("type", "required");
				Dictionary<string, object> configs = dictionary;
				component.ShowRealName<RealNameWindow>(configs, delegate(int code, object result)
				{
					state = SDK_State.LoggedIn;
					if (code == SDKManager.RESULT_SUCCESS)
					{
						this.Callback(ResultCode.RealNameSucceed, string.Empty);
					}
					else
					{
						this.Callback(ResultCode.RealNameFailed, result.ToString());
					}
				});
			}
			else
			{
				this.Callback(ResultCode.RealNameFailed, string.Concat("Pease do not call realname in incorrect status (", state, ")."));
			}
		}
	}
}
