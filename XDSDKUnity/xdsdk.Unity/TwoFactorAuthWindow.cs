using System;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;
using xdsdk.Unity.Service;

namespace xdsdk.Unity
{
	public class TwoFactorAuthWindow : UIElement
	{
		private AppInfo appInfo;

		private long userID;

		private string username;

		private string password;

		public Button close;

		public Button back;

		public Text mobile;

		public InputField code;

		public Button send;

		public Button confirm;

		public Text errorText;

		public Image errorIcon;

		private static double lastFetchTime = -60000.0;

		private static DateTime dateTime = new DateTime(1970, 1, 1);

		public override Dictionary<string, object> Extra
		{
			get
			{
				return extra;
			}
			set
			{
				extra = value;
				if (extra != null)
				{
					if (extra.ContainsKey("app_info"))
					{
						appInfo = (extra["app_info"] as AppInfo);
					}
					if (extra.ContainsKey("user_id"))
					{
						userID = (long)extra["user_id"];
					}
					if (extra.ContainsKey("username"))
					{
						username = (extra["username"] as string);
					}
					if (extra.ContainsKey("password"))
					{
						password = (extra["password"] as string);
					}
					if (extra.ContainsKey("mobile"))
					{
						mobile.text = (extra["mobile"] as string);
					}
				}
			}
		}

		private void Awake()
		{
			back.onClick.AddListener(delegate
			{
				OnCallback(SDKManager.RESULT_BACK, "Back button clicked.");
				GetSDKManager().Pop();
			});
			close.onClick.AddListener(delegate
			{
				OnCallback(SDKManager.RESULT_CLOSE, "Close button clicked.");
				GetSDKManager().PopAll();
			});
			send.onClick.AddListener(delegate
			{
				GetSDKManager().ShowLoading();
				Login.VerificationCode(appInfo.Id, userID.ToString(), delegate
				{
					GetSDKManager().DismissLoading();
					lastFetchTime = (DateTime.UtcNow - dateTime).TotalMilliseconds;
				}, delegate(string error)
				{
					GetSDKManager().DismissLoading();
					Debug.Log(error);
				});
			});
			confirm.onClick.AddListener(delegate
			{
				if (!string.IsNullOrEmpty(code.text))
				{
					HideError();
					GetSDKManager().ShowLoading();
					Login.XD(appInfo.Id, username, password, code.text, delegate(Dictionary<string, object> dict)
					{
						TwoFactorAuthWindow twoFactorAuthWindow = this;
						GetSDKManager().DismissLoading();
						User user = dict["user"] as User;
						string token = dict["token"] as string;
						if (user.AuthorizationState == 0 && appInfo.NeedLoginRealName != 0)
						{
							Dictionary<string, object> dictionary = new Dictionary<string, object>
							{
								{
									"token",
									token
								}
							};
							if (appInfo.NeedLoginRealName == 1)
							{
								dictionary.Add("type", "required");
							}
							else
							{
								dictionary.Add("type", "optional");
							}
							GetSDKManager().ShowRealName<RealNameWindow>(dictionary, delegate(int code, object result)
							{
								if (code == SDKManager.RESULT_SUCCESS)
								{
									Dictionary<string, object> data = new Dictionary<string, object>
									{
										{
											"user",
											user
										},
										{
											"token",
											token
										}
									};
									twoFactorAuthWindow.OnCallback(SDKManager.RESULT_SUCCESS, data);
								}
								else if (code == SDKManager.RESULT_CLOSE)
								{
									twoFactorAuthWindow.OnCallback(SDKManager.RESULT_CLOSE, result);
								}
								else
								{
									Debug.LogError(result.ToString());
								}
							});
						}
						else
						{
							OnCallback(SDKManager.RESULT_SUCCESS, dict);
							GetSDKManager().PopAll();
						}
					}, delegate(string errorType, object errorContent)
					{
						GetSDKManager().DismissLoading();
						if (errorContent != null)
						{
							if (errorContent.GetType() == typeof(Dictionary<string, object>) && (errorContent as Dictionary<string, object>).ContainsKey("error"))
							{
								ShowError((errorContent as Dictionary<string, object>)["error"].ToString());
							}
							else
							{
								ShowError(errorContent.ToString());
							}
						}
						Debug.LogError(errorType + ":" + errorContent);
					});
				}
				else
				{
					ShowError("请填写验证码");
				}
			});
			code.onValueChanged.AddListener(delegate
			{
				HideError();
			});
			HideError();
		}

		private void Update()
		{
			double totalMilliseconds = (DateTime.UtcNow - dateTime).TotalMilliseconds;
			if (totalMilliseconds - lastFetchTime < 60000.0)
			{
				send.interactable = false;
				Text componentInChildren = send.GetComponentInChildren<Text>();
				componentInChildren.text = (int)((60000.0 + lastFetchTime - totalMilliseconds) / 1000.0) + "s";
			}
			else if (!send.interactable)
			{
				send.interactable = true;
				Text componentInChildren2 = send.GetComponentInChildren<Text>();
				componentInChildren2.text = "发送验证码";
			}
		}

		private void ShowError(string error)
		{
			errorText.text = error;
			errorText.gameObject.SetActive(value: true);
			errorIcon.gameObject.SetActive(value: true);
		}

		private void HideError()
		{
			errorText.gameObject.SetActive(value: false);
			errorIcon.gameObject.SetActive(value: false);
		}
	}
}
