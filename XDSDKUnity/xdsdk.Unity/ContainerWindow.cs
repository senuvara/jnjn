using System.Collections;
using UnityEngine;
using UnityEngine.UI;

namespace xdsdk.Unity
{
	public class ContainerWindow : UIElement
	{
		public Image backgroung;

		private void Awake()
		{
			transitionDurationTime = 0.1f;
			CanvasScaler component = GetComponent<CanvasScaler>();
			if (Screen.height > 1080)
			{
				component.uiScaleMode = CanvasScaler.ScaleMode.ScaleWithScreenSize;
			}
			else
			{
				component.uiScaleMode = CanvasScaler.ScaleMode.ConstantPixelSize;
			}
		}

		public override IEnumerator PlayExit()
		{
			if (!animationLaunched)
			{
				animationLaunched = true;
				float startTime = Time.time;
				float endTime = startTime + transitionDurationTime;
				CanvasGroup canvasGroup = UI.GetComponent<CanvasGroup>(base.gameObject);
				canvasGroup.alpha = 1f;
				while (Time.time < endTime)
				{
					yield return new WaitForEndOfFrame();
					float delta = (Time.time - startTime) / transitionDurationTime;
					canvasGroup.alpha = 1f - delta;
				}
			}
			animationLaunched = false;
			yield return null;
		}

		public override IEnumerator PlayEnter()
		{
			if (!animationLaunched)
			{
				animationLaunched = true;
				float startTime = Time.time;
				float endTime = startTime + transitionDurationTime;
				CanvasGroup canvasGroup = UI.GetComponent<CanvasGroup>(base.gameObject);
				canvasGroup.alpha = 0f;
				while (Time.time < endTime)
				{
					yield return new WaitForEndOfFrame();
					float delta = canvasGroup.alpha = (Time.time - startTime) / transitionDurationTime;
				}
			}
			animationLaunched = false;
			yield return null;
		}
	}
}
