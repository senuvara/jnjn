using System;
using System.Collections;
using UnityEngine;

namespace xdsdk.Unity
{
	[DisallowMultipleComponent]
	public class UIAnimator : MonoBehaviour
	{
		public void DoExitAnimation(UIElement exit, UIElement resume, Action onAnimationEnd = null)
		{
			float val = 0f;
			float val2 = 0f;
			if (exit != null)
			{
				val = exit.DoExitAnimation();
			}
			if (resume != null)
			{
				val2 = resume.DoResumeAnimation();
			}
			BlockForSecond(Math.Max(val, val2) + 0.1f, onAnimationEnd);
		}

		public void DoEnterAnimation(UIElement pause, UIElement enter, Action onAnimationEnd = null)
		{
			float val = 0f;
			float val2 = 0f;
			if (pause != null)
			{
				val = pause.DoPauseAnimation();
			}
			if (enter != null)
			{
				val2 = enter.DoEnterAnimation();
			}
			BlockForSecond(Math.Max(val, val2) + 0.1f, onAnimationEnd);
		}

		private void BlockForSecond(float duration, Action callback = null)
		{
			if (duration > 0f)
			{
				StartCoroutine(BlockCoroutine(duration, delegate
				{
					if (callback != null)
					{
						callback();
					}
				}));
			}
			else if (callback != null)
			{
				callback();
			}
		}

		private IEnumerator BlockCoroutine(float blockDuration, Action onBlockEnd = null)
		{
			yield return new WaitForSeconds(blockDuration);
			onBlockEnd?.Invoke();
		}
	}
}
