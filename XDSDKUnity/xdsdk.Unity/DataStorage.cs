using System;
using System.Collections.Generic;
using System.IO;
using System.Net.NetworkInformation;
using System.Security.Cryptography;
using System.Text;
using UnityEngine;

namespace xdsdk.Unity
{
	public static class DataStorage
	{
		private static Dictionary<string, string> dataCache;

		private static byte[] Keys = new byte[8]
		{
			18,
			52,
			86,
			120,
			144,
			171,
			205,
			239
		};

		public static void SaveString(string key, string value)
		{
			SaveStringToCache(key, value);
			PlayerPrefs.SetString(key, EncodeString(value));
		}

		public static string LoadString(string key)
		{
			string text = LoadStringFromCache(key);
			if (!string.IsNullOrEmpty(text))
			{
				return text;
			}
			text = ((!PlayerPrefs.HasKey(key)) ? null : DecodeString(PlayerPrefs.GetString(key)));
			if (text != null)
			{
				SaveStringToCache(key, text);
			}
			return text;
		}

		private static void SaveStringToCache(string key, string value)
		{
			if (dataCache == null)
			{
				dataCache = new Dictionary<string, string>();
			}
			if (dataCache.ContainsKey(key))
			{
				dataCache[key] = value;
			}
			else
			{
				dataCache.Add(key, value);
			}
		}

		private static string LoadStringFromCache(string key)
		{
			if (dataCache == null)
			{
				dataCache = new Dictionary<string, string>();
			}
			return (!dataCache.ContainsKey(key)) ? null : dataCache[key];
		}

		private static string EncodeString(string encryptString)
		{
			try
			{
				byte[] bytes = Encoding.UTF8.GetBytes(GetMacAddress().Substring(0, 8));
				byte[] keys = Keys;
				byte[] bytes2 = Encoding.UTF8.GetBytes(encryptString);
				DESCryptoServiceProvider dESCryptoServiceProvider = new DESCryptoServiceProvider();
				MemoryStream memoryStream = new MemoryStream();
				CryptoStream cryptoStream = new CryptoStream(memoryStream, dESCryptoServiceProvider.CreateEncryptor(bytes, keys), CryptoStreamMode.Write);
				cryptoStream.Write(bytes2, 0, bytes2.Length);
				cryptoStream.FlushFinalBlock();
				cryptoStream.Close();
				return Convert.ToBase64String(memoryStream.ToArray());
			}
			catch
			{
				return encryptString;
			}
		}

		private static string DecodeString(string decryptString)
		{
			try
			{
				byte[] bytes = Encoding.UTF8.GetBytes(GetMacAddress().Substring(0, 8));
				byte[] keys = Keys;
				byte[] array = Convert.FromBase64String(decryptString);
				DESCryptoServiceProvider dESCryptoServiceProvider = new DESCryptoServiceProvider();
				MemoryStream memoryStream = new MemoryStream();
				CryptoStream cryptoStream = new CryptoStream(memoryStream, dESCryptoServiceProvider.CreateDecryptor(bytes, keys), CryptoStreamMode.Write);
				cryptoStream.Write(array, 0, array.Length);
				cryptoStream.FlushFinalBlock();
				cryptoStream.Close();
				return Encoding.UTF8.GetString(memoryStream.ToArray());
			}
			catch (Exception)
			{
				return decryptString;
			}
		}

		private static string GetMacAddress()
		{
			string text = "FFFFFFFFFFFF";
			NetworkInterface[] allNetworkInterfaces = NetworkInterface.GetAllNetworkInterfaces();
			NetworkInterface[] array = allNetworkInterfaces;
			foreach (NetworkInterface networkInterface in array)
			{
				if (networkInterface.Description == "en0")
				{
					text = networkInterface.GetPhysicalAddress().ToString();
					break;
				}
				text = networkInterface.GetPhysicalAddress().ToString();
				if (text != string.Empty)
				{
					break;
				}
			}
			return text;
		}

		public static string GetUniqueID()
		{
			string text = LoadString("xdsdk_unique_id");
			if (!string.IsNullOrEmpty(text))
			{
				return text;
			}
			text = Guid.NewGuid().ToString();
			SaveString("xdsdk_unique_id", text);
			return text;
		}
	}
}
