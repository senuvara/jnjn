namespace xdsdk.Unity
{
	public enum ResultCode
	{
		InitSucceed,
		InitFailed,
		LoginSucceed,
		LoginFailed,
		LoginCanceled,
		LogoutSucceed,
		PayCompleted,
		PayFailed,
		PayCanceled,
		RealNameSucceed,
		RealNameFailed
	}
}
