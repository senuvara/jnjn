using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;
using xdsdk.Unity.Service;

namespace xdsdk.Unity
{
	public class PlatformLoginWindow : UIElement
	{
		private AppInfo appInfo;

		public Button close;

		public Button back;

		public InputField username;

		public InputField password;

		public Button login;

		public Button forgot;

		public Button register;

		public Text error1;

		public Image error1Icon;

		public Text error2;

		public Image error2Icon;

		public override Dictionary<string, object> Extra
		{
			get
			{
				return extra;
			}
			set
			{
				extra = value;
				if (extra != null && extra.ContainsKey("app_info"))
				{
					appInfo = (extra["app_info"] as AppInfo);
				}
			}
		}

		private void Awake()
		{
			back.onClick.AddListener(delegate
			{
				OnCallback(SDKManager.RESULT_BACK, "Back button clicked.");
				GetSDKManager().Pop();
			});
			close.onClick.AddListener(delegate
			{
				OnCallback(SDKManager.RESULT_CLOSE, "Close button clicked.");
				GetSDKManager().PopAll();
			});
			forgot.onClick.AddListener(delegate
			{
				Application.OpenURL("https://www.xd.com/security/forget_pass");
			});
			register.onClick.AddListener(delegate
			{
				Application.OpenURL("https://www.xd.com/users/register/");
			});
			login.onClick.AddListener(LoginXD);
			username.onValueChanged.AddListener(delegate
			{
				HideError1();
			});
			password.onValueChanged.AddListener(delegate
			{
				HideError2();
			});
			InputNavigator component = password.gameObject.GetComponent<InputNavigator>();
			component.OnEnter += LoginXD;
			HideError1();
			HideError2();
		}

		private void LoginXD()
		{
			if (!string.IsNullOrEmpty(username.text) && !string.IsNullOrEmpty(password.text))
			{
				HideError1();
				HideError2();
				GetSDKManager().ShowLoading();
				Login.XD(appInfo.Id, username.text, password.text, delegate(Dictionary<string, object> dict)
				{
					PlatformLoginWindow platformLoginWindow = this;
					GetSDKManager().DismissLoading();
					User user = dict["user"] as User;
					string token = dict["token"] as string;
					if (user.AuthorizationState == 0 && appInfo.NeedLoginRealName != 0)
					{
						Dictionary<string, object> dictionary2 = new Dictionary<string, object>
						{
							{
								"token",
								token
							}
						};
						if (appInfo.NeedLoginRealName == 1)
						{
							dictionary2.Add("type", "required");
						}
						else
						{
							dictionary2.Add("type", "optional");
						}
						GetSDKManager().ShowRealName<RealNameWindow>(dictionary2, delegate(int code, object result)
						{
							if (code == SDKManager.RESULT_SUCCESS)
							{
								Login.User(token, delegate(User userAfterRealName)
								{
									Dictionary<string, object> data3 = new Dictionary<string, object>
									{
										{
											"user",
											userAfterRealName
										},
										{
											"token",
											token
										}
									};
									platformLoginWindow.OnCallback(SDKManager.RESULT_SUCCESS, data3);
								}, delegate
								{
									Dictionary<string, object> data2 = new Dictionary<string, object>
									{
										{
											"user",
											user
										},
										{
											"token",
											token
										}
									};
									platformLoginWindow.OnCallback(SDKManager.RESULT_SUCCESS, data2);
								});
							}
							else if (code == SDKManager.RESULT_CLOSE)
							{
								platformLoginWindow.OnCallback(SDKManager.RESULT_CLOSE, result);
							}
							else
							{
								Debug.LogError(result.ToString());
							}
						});
					}
					else
					{
						OnCallback(SDKManager.RESULT_SUCCESS, dict);
						GetSDKManager().PopAll();
					}
				}, delegate(string errorType, object errorContent)
				{
					GetSDKManager().DismissLoading();
					if (errorType != null && errorType.Equals("need_verification_code") && errorContent.GetType() == typeof(Dictionary<string, object>))
					{
						Dictionary<string, object> dictionary = errorContent as Dictionary<string, object>;
						Dictionary<string, object> configs = new Dictionary<string, object>
						{
							{
								"app_info",
								appInfo
							},
							{
								"user_id",
								(long)dictionary["user_id"]
							},
							{
								"mobile",
								dictionary["mobile"] as string
							},
							{
								"username",
								username.text
							},
							{
								"password",
								password.text
							}
						};
						GetSDKManager().ShowTwoFactorAuth<TwoFactorAuthWindow>(configs, delegate(int code, object data)
						{
							if (code == SDKManager.RESULT_SUCCESS)
							{
								OnCallback(SDKManager.RESULT_SUCCESS, data);
							}
							else if (code == SDKManager.RESULT_BACK)
							{
								Debug.Log(data);
							}
							else if (code == SDKManager.RESULT_CLOSE)
							{
								Debug.Log(data);
								OnCallback(SDKManager.RESULT_CLOSE, data);
							}
							else
							{
								Debug.LogError(data);
							}
						});
					}
					else if (errorContent != null)
					{
						ShowError2(errorContent.ToString());
					}
				});
			}
			else if (string.IsNullOrEmpty(username.text))
			{
				ShowError1("请输入用户名");
			}
			else if (string.IsNullOrEmpty(password.text))
			{
				ShowError2("请输入密码");
			}
		}

		private void ShowError1(string error)
		{
			error1.text = error;
			error1.gameObject.SetActive(value: true);
			error1Icon.gameObject.SetActive(value: true);
		}

		private void HideError1()
		{
			error1.gameObject.SetActive(value: false);
			error1Icon.gameObject.SetActive(value: false);
		}

		private void ShowError2(string error)
		{
			error2.text = error;
			error2.gameObject.SetActive(value: true);
			error2Icon.gameObject.SetActive(value: true);
		}

		private void HideError2()
		{
			error2.gameObject.SetActive(value: false);
			error2Icon.gameObject.SetActive(value: false);
		}
	}
}
