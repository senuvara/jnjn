using System;
using UnityEngine;
using UnityEngine.EventSystems;
using UnityEngine.UI;

namespace xdsdk.Unity
{
	[DisallowMultipleComponent]
	public class InputNavigator : MonoBehaviour, ISelectHandler, IDeselectHandler, IEventSystemHandler
	{
		private EventSystem system;

		private bool _isSelect;

		public event Action OnEnter;

		private void Start()
		{
			system = EventSystem.current;
		}

		private void Update()
		{
			if (Input.GetKeyDown(KeyCode.Tab) && _isSelect)
			{
				Selectable selectable = null;
				if (Input.GetKey(KeyCode.LeftShift) || Input.GetKey(KeyCode.RightShift))
				{
					selectable = system.currentSelectedGameObject.GetComponent<Selectable>().FindSelectableOnUp();
					if (selectable == null)
					{
						selectable = system.lastSelectedGameObject.GetComponent<Selectable>();
					}
				}
				else
				{
					selectable = system.currentSelectedGameObject.GetComponent<Selectable>().FindSelectableOnDown();
					try
					{
						if (selectable == null)
						{
							selectable = system.firstSelectedGameObject.GetComponent<Selectable>();
						}
					}
					catch
					{
					}
				}
				if (selectable != null)
				{
					system.SetSelectedGameObject(selectable.gameObject, new BaseEventData(system));
				}
				else
				{
					Debug.Log("Could not find next widget.");
				}
			}
			else if ((Input.GetKeyDown(KeyCode.Return) || Input.GetKeyDown(KeyCode.KeypadEnter)) && _isSelect && this.OnEnter != null)
			{
				this.OnEnter();
			}
		}

		public void OnSelect(BaseEventData eventData)
		{
			_isSelect = true;
		}

		public void OnDeselect(BaseEventData eventData)
		{
			_isSelect = false;
		}
	}
}
