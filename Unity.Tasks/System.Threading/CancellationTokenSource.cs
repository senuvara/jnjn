using System.Collections.Generic;

namespace System.Threading
{
	public sealed class CancellationTokenSource
	{
		private object mutex = new object();

		private Action actions;

		private bool isCancellationRequested;

		internal bool IsCancellationRequested
		{
			get
			{
				lock (mutex)
				{
					return isCancellationRequested;
				}
			}
		}

		public CancellationToken Token => new CancellationToken(this);

		internal CancellationTokenRegistration Register(Action action)
		{
			lock (mutex)
			{
				actions = (Action)Delegate.Combine(actions, action);
				return new CancellationTokenRegistration(this, action);
			}
		}

		internal void Unregister(Action action)
		{
			lock (mutex)
			{
				actions = (Action)Delegate.Remove(actions, action);
			}
		}

		public void Cancel()
		{
			Cancel(throwOnFirstException: false);
		}

		public void Cancel(bool throwOnFirstException)
		{
			lock (mutex)
			{
				isCancellationRequested = true;
				if (actions != null)
				{
					try
					{
						if (!throwOnFirstException)
						{
							Delegate[] invocationList = actions.GetInvocationList();
							int num = 0;
							List<Exception> list;
							while (true)
							{
								if (num >= invocationList.Length)
								{
									return;
								}
								Delegate @delegate = invocationList[num];
								list = new List<Exception>();
								try
								{
									((Action)@delegate)();
								}
								catch (Exception item)
								{
									list.Add(item);
								}
								if (list.Count > 0)
								{
									break;
								}
								num++;
							}
							throw new AggregateException(list);
						}
						actions();
					}
					finally
					{
						actions = null;
					}
				}
			}
		}
	}
}
