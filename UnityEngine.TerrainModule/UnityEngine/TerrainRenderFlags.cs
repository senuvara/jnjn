using System;

namespace UnityEngine
{
	/// <summary>
	///   <para>Enum provding terrain rendering options.</para>
	/// </summary>
	[Flags]
	public enum TerrainRenderFlags
	{
		[Obsolete("TerrainRenderFlags.heightmap is obsolete, use TerrainRenderFlags.Heightmap instead. (UnityUpgradable) -> Heightmap")]
		heightmap = 0x1,
		[Obsolete("TerrainRenderFlags.trees is obsolete, use TerrainRenderFlags.Trees instead. (UnityUpgradable) -> Trees")]
		trees = 0x2,
		[Obsolete("TerrainRenderFlags.details is obsolete, use TerrainRenderFlags.Details instead. (UnityUpgradable) -> Details")]
		details = 0x4,
		[Obsolete("TerrainRenderFlags.all is obsolete, use TerrainRenderFlags.All instead. (UnityUpgradable) -> All")]
		all = 0x7,
		/// <summary>
		///   <para>Render heightmap.</para>
		/// </summary>
		Heightmap = 0x1,
		/// <summary>
		///   <para>Render trees.</para>
		/// </summary>
		Trees = 0x2,
		/// <summary>
		///   <para>Render terrain details.</para>
		/// </summary>
		Details = 0x4,
		/// <summary>
		///   <para>Render all options.</para>
		/// </summary>
		All = 0x7
	}
}
