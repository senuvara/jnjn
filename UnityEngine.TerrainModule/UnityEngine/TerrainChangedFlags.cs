using System;

namespace UnityEngine
{
	/// <summary>
	///   <para>Indicate the types of changes to the terrain in OnTerrainChanged callback.</para>
	/// </summary>
	[Flags]
	public enum TerrainChangedFlags
	{
		/// <summary>
		///   <para>Indicates a change to the heightmap data.</para>
		/// </summary>
		Heightmap = 0x1,
		/// <summary>
		///   <para>Indicates a change to the tree data.</para>
		/// </summary>
		TreeInstances = 0x2,
		/// <summary>
		///   <para>Indicates a change to the heightmap data without computing LOD.</para>
		/// </summary>
		DelayedHeightmapUpdate = 0x4,
		/// <summary>
		///   <para>Indicates that a change was made to the terrain that was so significant that the internal rendering data need to be flushed and recreated.</para>
		/// </summary>
		FlushEverythingImmediately = 0x8,
		/// <summary>
		///   <para>Indicates a change to the detail data.</para>
		/// </summary>
		RemoveDirtyDetailsImmediately = 0x10,
		/// <summary>
		///   <para>Indicates that the TerrainData object is about to be destroyed.</para>
		/// </summary>
		WillBeDestroyed = 0x100
	}
}
