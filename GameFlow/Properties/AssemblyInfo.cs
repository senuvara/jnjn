using System.Reflection;
using System.Runtime.CompilerServices;
using UnityEngine;

[assembly: RuntimeCompatibility(WrapNonExceptionThrows = true)]
[assembly: UnityAPICompatibilityVersion("2017.2.1p3")]
[assembly: AssemblyVersion("0.0.0.0")]
