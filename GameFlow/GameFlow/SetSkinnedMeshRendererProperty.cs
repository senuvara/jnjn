using UnityEngine;
using UnityEngine.Rendering;

namespace GameFlow
{
	[Help("es", "Modifica una propiedad del componente @UnityManual{SkinnedMeshRenderer} especificado.", null)]
	[AddComponentMenu("")]
	[Help("en", "Sets a property of the specified @UnityManual{SkinnedMeshRenderer}.", null)]
	public class SetSkinnedMeshRendererProperty : Action, IExecutableInEditor
	{
		public enum Property
		{
			Bones,
			Enabled,
			LightmapIndex,
			LightmapScaleOffset,
			LocalBounds,
			Material,
			Materials,
			ProbeAnchor,
			Quality,
			RealtimeLightmapScaleOffset,
			ReceiveShadows,
			ReflectionProbeUsage,
			RootBone,
			ShadowCastingMode,
			SharedMaterial,
			SharedMaterials,
			SharedMesh,
			SortingLayerID,
			SortingLayerName,
			SortingOrder,
			UpdateWhenOffscreen,
			LightProbeUsage
		}

		[SerializeField]
		private SkinnedMeshRenderer _target;

		[SerializeField]
		private Variable _targetVar;

		[SerializeField]
		private Property _property = Property.Enabled;

		[SerializeField]
		private bool _enabledValue;

		[SerializeField]
		private int _lightmapIndexValue;

		[SerializeField]
		private Vector4 _lightmapScaleOffsetValue;

		[SerializeField]
		private Bounds _localBoundsValue;

		[SerializeField]
		private Material _materialValue;

		[SerializeField]
		private Transform _probeAnchorValue;

		[SerializeField]
		private SkinQuality _qualityValue;

		[SerializeField]
		private Vector4 _realtimeLightmapScaleOffsetValue;

		[SerializeField]
		private bool _receiveShadowsValue;

		[SerializeField]
		private ReflectionProbeUsage _reflectionProbeUsageValue;

		[SerializeField]
		private Transform _rootBoneValue;

		[SerializeField]
		private ShadowCastingMode _shadowCastingModeValue;

		[SerializeField]
		private Material _sharedMaterialValue;

		[SerializeField]
		private Mesh _sharedMeshValue;

		[SerializeField]
		private int _sortingLayerIDValue;

		[SerializeField]
		private string _sortingLayerNameValue;

		[SerializeField]
		private int _sortingOrderValue;

		[SerializeField]
		private bool _updateWhenOffscreenValue;

		[SerializeField]
		private LightProbeUsage _lightProbeUsageValue;

		[SerializeField]
		private Variable _valueVar;

		protected override void OnReset()
		{
			_target = base.gameObject.GetComponent<SkinnedMeshRenderer>();
		}

		public override void Setup()
		{
		}

		public override void Execute()
		{
			SkinnedMeshRenderer skinnedMeshRenderer = _targetVar.GetValueAsComponent(_target, typeof(SkinnedMeshRenderer)) as SkinnedMeshRenderer;
			if (!(skinnedMeshRenderer == null))
			{
				switch (_property)
				{
				case Property.Materials:
				case Property.SharedMaterials:
					break;
				case Property.Enabled:
					skinnedMeshRenderer.enabled = _valueVar.GetValue(_enabledValue);
					break;
				case Property.LightmapIndex:
					skinnedMeshRenderer.lightmapIndex = _valueVar.GetValue(_lightmapIndexValue);
					break;
				case Property.LightmapScaleOffset:
					skinnedMeshRenderer.lightmapScaleOffset = _valueVar.GetValue(_lightmapScaleOffsetValue);
					break;
				case Property.LocalBounds:
					skinnedMeshRenderer.localBounds = _valueVar.GetValue(_localBoundsValue);
					break;
				case Property.Material:
					skinnedMeshRenderer.material = (_valueVar.GetValue(_materialValue) as Material);
					break;
				case Property.ProbeAnchor:
					skinnedMeshRenderer.probeAnchor = (_valueVar.GetValueAsComponent(_probeAnchorValue, typeof(Transform)) as Transform);
					break;
				case Property.Quality:
					skinnedMeshRenderer.quality = (SkinQuality)(object)_valueVar.GetValue(_qualityValue);
					break;
				case Property.RealtimeLightmapScaleOffset:
					skinnedMeshRenderer.realtimeLightmapScaleOffset = _valueVar.GetValue(_realtimeLightmapScaleOffsetValue);
					break;
				case Property.ReceiveShadows:
					skinnedMeshRenderer.receiveShadows = _valueVar.GetValue(_receiveShadowsValue);
					break;
				case Property.ReflectionProbeUsage:
					skinnedMeshRenderer.reflectionProbeUsage = (ReflectionProbeUsage)(object)_valueVar.GetValue(_reflectionProbeUsageValue);
					break;
				case Property.RootBone:
					skinnedMeshRenderer.rootBone = (_valueVar.GetValueAsComponent(_rootBoneValue, typeof(Transform)) as Transform);
					break;
				case Property.ShadowCastingMode:
					skinnedMeshRenderer.shadowCastingMode = (ShadowCastingMode)(object)_valueVar.GetValue(_shadowCastingModeValue);
					break;
				case Property.SharedMaterial:
					skinnedMeshRenderer.sharedMaterial = (_valueVar.GetValue(_sharedMaterialValue) as Material);
					break;
				case Property.SharedMesh:
					skinnedMeshRenderer.sharedMesh = (_valueVar.GetValue(_sharedMeshValue) as Mesh);
					break;
				case Property.SortingLayerID:
					skinnedMeshRenderer.sortingLayerID = _valueVar.GetValue(_sortingLayerIDValue);
					break;
				case Property.SortingLayerName:
					skinnedMeshRenderer.sortingLayerName = _valueVar.GetValue(_sortingLayerNameValue);
					break;
				case Property.SortingOrder:
					skinnedMeshRenderer.sortingOrder = _valueVar.GetValue(_sortingOrderValue);
					break;
				case Property.UpdateWhenOffscreen:
					skinnedMeshRenderer.updateWhenOffscreen = _valueVar.GetValue(_updateWhenOffscreenValue);
					break;
				case Property.LightProbeUsage:
					skinnedMeshRenderer.lightProbeUsage = (LightProbeUsage)(object)_valueVar.GetValue(_lightProbeUsageValue);
					break;
				}
			}
		}
	}
}
