using UnityEngine;

namespace GameFlow
{
	[Help("en", "Program that will be executed when the value of the specified @GameFlow{Variable} changes.", null)]
	[Help("es", "Programa que se ejecutará cuando el valor de la (@GameFlow{Variable}) especificada sea modificado.", null)]
	[AddComponentMenu("GameFlow/Programs/On Variable Change")]
	public class OnVariableChange : EventProgram
	{
		[SerializeField]
		private Variable _variable;

		public Variable variable => _variable;

		protected override void RegisterAsListener()
		{
			if (_variable != null)
			{
				_variable.AddListener(this);
			}
		}

		public override void EventReceived(EventObject e)
		{
			if (AcceptEvent(e))
			{
				SetParameters(e);
				Restart();
			}
		}
	}
}
