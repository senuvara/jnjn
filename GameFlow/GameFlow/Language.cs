using System.Collections.Generic;
using UnityEngine;

namespace GameFlow
{
	public class Language
	{
		private static Dictionary<string, Language> languages = new Dictionary<string, Language>
		{
			{
				"en",
				new Language("en")
			},
			{
				"es",
				new Language("es")
			}
		};

		public string code
		{
			get;
			private set;
		}

		public SystemLanguage systemLanguage
		{
			get;
			private set;
		}

		private Language(string code)
		{
			systemLanguage = LanguageUtils.ToSystemLanguage(code);
			this.code = code;
		}

		private Language(SystemLanguage systemLanguage, string code)
		{
			this.systemLanguage = systemLanguage;
			this.code = code;
		}

		public static Language GetInstance(string code)
		{
			if (!languages.ContainsKey(code))
			{
				languages.Add(code, new Language(code));
			}
			return languages[code];
		}

		public static Language GetInstance(SystemLanguage systemLanguage)
		{
			string text = LanguageUtils.ToCode(systemLanguage);
			if (!languages.ContainsKey(text))
			{
				languages.Add(text, new Language(systemLanguage, text));
			}
			return languages[text];
		}

		public override string ToString()
		{
			return systemLanguage.ToString();
		}
	}
}
