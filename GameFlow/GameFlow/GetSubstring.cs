using UnityEngine;

namespace GameFlow
{
	[Help("en", "Gets a part of the string stored in the specified @GameFlow{Variable}.", null)]
	[Help("es", "Obtiene una parte de la cadena almacenada en la @GameFlow{Variable} especificada.", null)]
	[AddComponentMenu("")]
	public class GetSubstring : Action, IExecutableInEditor
	{
		public enum Mode
		{
			Length,
			ToEnd
		}

		[SerializeField]
		private Variable _variable;

		[SerializeField]
		private int _start;

		[SerializeField]
		private Variable _startVar;

		[SerializeField]
		private int _length;

		[SerializeField]
		private Variable _lengthVar;

		[SerializeField]
		private Variable _output;

		[SerializeField]
		private Mode _mode;

		public override void Execute()
		{
			if (_variable == null || _output == null)
			{
				return;
			}
			int num = _startVar.GetValue(_start);
			if (num < 0)
			{
				num = 0;
			}
			switch (_mode)
			{
			case Mode.Length:
			{
				int value = _lengthVar.GetValue(_length);
				_output.SetValue(_variable.GetValue(string.Empty).Substring(num, value));
				if (value < 0)
				{
					value = 0;
				}
				break;
			}
			case Mode.ToEnd:
				_output.SetValue(_variable.GetValue(string.Empty).Substring(num));
				break;
			}
		}
	}
}
