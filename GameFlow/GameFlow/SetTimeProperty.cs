using UnityEngine;

namespace GameFlow
{
	[Help("en", "Sets a general time-related property.", null)]
	[AddComponentMenu("")]
	[Help("es", "Modifica una propiedad relativa al tiempo.", null)]
	public class SetTimeProperty : Action, IExecutableInEditor
	{
		public enum Property
		{
			CaptureFramerate,
			FixedDeltaTime,
			MaximumDeltaTime,
			TimeScale
		}

		[SerializeField]
		private Property _property;

		[SerializeField]
		private int _intValue;

		[SerializeField]
		private float _floatValue;

		[SerializeField]
		private Variable _valueVar;

		public override void Execute()
		{
			switch (_property)
			{
			case Property.CaptureFramerate:
				Time.captureFramerate = _valueVar.GetValue(_intValue);
				break;
			case Property.FixedDeltaTime:
				Time.fixedDeltaTime = _valueVar.GetValue(_floatValue);
				break;
			case Property.MaximumDeltaTime:
				Time.maximumDeltaTime = _valueVar.GetValue(_floatValue);
				break;
			case Property.TimeScale:
				Time.timeScale = _valueVar.GetValue(_floatValue);
				break;
			}
		}
	}
}
