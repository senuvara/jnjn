using UnityEngine;

namespace GameFlow
{
	[Help("en", "Sets the rotation of the specified object so it looks at the given target.", null)]
	[AddComponentMenu("")]
	[Help("es", "Ajusta la rotacion del objeto especificado para que mire hacia un objetivo dado.", null)]
	public class LookAt : TimeAction, IExecutableInEditor
	{
		public enum TargetType
		{
			Vector3,
			Transform
		}

		public enum Mode
		{
			Instant,
			Duration,
			Velocity
		}

		[SerializeField]
		private Transform _target;

		[SerializeField]
		private Variable _targetVar;

		[SerializeField]
		private Transform _lookTarget;

		[SerializeField]
		private Vector3 _lookTargetPosition;

		[SerializeField]
		private Variable _lookTargetVar;

		[SerializeField]
		private TargetType _lookTargetType = TargetType.Transform;

		[SerializeField]
		private Mode _mode = Mode.Duration;

		[SerializeField]
		private EasingType _easingType;

		[SerializeField]
		private float _speed = 90f;

		[SerializeField]
		private Variable _speedVar;

		private Transform _transform;

		private Quaternion _startRotation;

		private Transform _lookTransform;

		private Vector3 _lookPosition;

		private bool _finished;

		protected EasingType easingType
		{
			get
			{
				return _easingType;
			}
			set
			{
				_easingType = value;
			}
		}

		public float velocity => _speedVar.GetValue(_speed);

		protected override void OnReset()
		{
			base.OnReset();
			_target = base.gameObject.GetComponent<Transform>();
		}

		public override void FirstStep()
		{
			_transform = (_targetVar.GetValueAsComponent(_target, typeof(Transform)) as Transform);
			if (_transform == null)
			{
				_finished = true;
				return;
			}
			switch (_lookTargetType)
			{
			case TargetType.Vector3:
				_lookPosition = _lookTargetVar.GetValue(_lookTargetPosition);
				break;
			case TargetType.Transform:
				_lookTransform = (_lookTargetVar.GetValueAsComponent(_lookTarget, typeof(Transform)) as Transform);
				if (_lookTransform == null)
				{
					_finished = true;
					return;
				}
				_lookPosition = _lookTransform.position;
				break;
			}
			if (!Application.isPlaying)
			{
				Execute(_transform, _lookPosition);
				_finished = true;
				return;
			}
			switch (_mode)
			{
			case Mode.Instant:
			case Mode.Duration:
				if (base.duration == 0f)
				{
					Execute(_transform, _lookPosition);
					_finished = true;
				}
				else
				{
					_startRotation = _transform.rotation;
					StartTimer();
					Step();
				}
				break;
			case Mode.Velocity:
			{
				_startRotation = _transform.rotation;
				Quaternion b = Quaternion.LookRotation(_lookPosition - base.transform.position);
				float num = Quaternion.Angle(_startRotation, b);
				StartTimer();
				base.timer.duration = num / velocity;
				Step();
				break;
			}
			}
		}

		public override void Step()
		{
			if (_transform == null)
			{
				_finished = true;
				return;
			}
			switch (_lookTargetType)
			{
			case TargetType.Vector3:
				_lookPosition = _lookTargetVar.GetValue(_lookTargetPosition);
				break;
			case TargetType.Transform:
				_lookTransform = (_lookTargetVar.GetValueAsComponent(_lookTarget, typeof(Transform)) as Transform);
				if (_lookTransform == null)
				{
					_finished = true;
					return;
				}
				_lookPosition = _lookTransform.position;
				break;
			}
			float dt = MathUtils.EaseDelta(base.timer.elapsedTimeDelta, easingType);
			Execute(_transform, _lookPosition, dt);
		}

		public override bool Finished()
		{
			return _finished || base.Finished();
		}

		private void Execute(Transform transform, Vector3 position, float dt)
		{
			if (transform != null && transform.position != position)
			{
				Quaternion b = Quaternion.LookRotation(position - transform.position);
				transform.rotation = Quaternion.Slerp(_startRotation, b, dt);
			}
		}

		public static void Execute(Transform transform, Vector3 position)
		{
			if (transform != null && transform.position != position)
			{
				transform.LookAt(position);
			}
		}

		public static void Execute(Transform transform, Transform lookTarget)
		{
			if (!(lookTarget == null))
			{
				Execute(transform, lookTarget.position);
			}
		}
	}
}
