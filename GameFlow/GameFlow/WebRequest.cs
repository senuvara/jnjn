using System.Collections.Generic;
using UnityEngine;

namespace GameFlow
{
	public class WebRequest : ScriptableObject, IVariableFriendly
	{
		public enum Method
		{
			GET,
			POST
		}

		private string _url;

		private Method _method;

		private string _contentType = string.Empty;

		private bool _cancelled;

		private byte[] _data;

		private WWW _www;

		public string url => _url;

		public Method method => _method;

		public string contentType => (_method != Method.POST) ? string.Empty : _contentType;

		public bool isDone => _www != null && _www.isDone;

		public bool isInProgress => _www != null && !_www.isDone;

		public bool isCancelled => _cancelled;

		public AudioClip responseAsAudio => (_www == null || !_www.isDone) ? null : WWWAudioExtensions.GetAudioClip(_www);

		public string responseAsText => (_www == null || !_www.isDone) ? string.Empty : _www.text;

		public Texture2D responseAsImage => (_www == null || !_www.isDone) ? null : _www.texture;

		public float downloadProgress => (_www == null) ? 0f : _www.progress;

		public float uploadProgress => (_www == null) ? 0f : _www.uploadProgress;

		public string error => (_www == null) ? null : _www.error;

		public bool successful => _www != null && _www.isDone && string.IsNullOrEmpty(_www.error);

		public int responseLength => (_www != null && _www.isDone) ? _www.bytesDownloaded : 0;

		public static WebRequest CreateGetRequest(string url)
		{
			WebRequest webRequest = ScriptableObject.CreateInstance<WebRequest>();
			webRequest._url = url;
			webRequest._method = Method.GET;
			return webRequest;
		}

		public static WebRequest CreatePostRequest(string url, byte[] data, string contentType)
		{
			WebRequest webRequest = ScriptableObject.CreateInstance<WebRequest>();
			webRequest._url = url;
			webRequest._method = Method.POST;
			webRequest._contentType = contentType;
			webRequest._data = data;
			return webRequest;
		}

		public void Send()
		{
			switch (_method)
			{
			case Method.GET:
				_www = new WWW(_url);
				break;
			case Method.POST:
				if (_data != null && _data.Length > 0)
				{
					Dictionary<string, string> dictionary = new Dictionary<string, string>();
					dictionary["Content-Type"] = _contentType;
					_www = new WWW(_url, _data, dictionary);
				}
				break;
			}
		}

		public void Cancel()
		{
			_cancelled = true;
			_www.Dispose();
		}

		private void OnDestroy()
		{
			if (_www != null)
			{
				_www.Dispose();
			}
		}
	}
}
