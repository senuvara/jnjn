using UnityEngine;

namespace GameFlow
{
	public class Platform
	{
		public static RuntimePlatform current = Application.platform;

		public static PlatformType type = Application.isMobilePlatform ? PlatformType.Mobile : (Application.isConsolePlatform ? PlatformType.Console : (Application.isWebPlayer ? PlatformType.Web : PlatformType.Desktop));

		public static bool isWindows => Application.platform == RuntimePlatform.WindowsPlayer || Application.platform == RuntimePlatform.WindowsEditor || Application.platform == RuntimePlatform.MetroPlayerX86 || Application.platform == RuntimePlatform.MetroPlayerX64 || Application.platform == RuntimePlatform.MetroPlayerARM;

		public static bool isMac => Application.platform == RuntimePlatform.OSXPlayer || Application.platform == RuntimePlatform.OSXEditor;
	}
}
