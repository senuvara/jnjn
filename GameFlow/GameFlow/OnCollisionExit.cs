using UnityEngine;

namespace GameFlow
{
	[DisallowMultipleComponent]
	[AddComponentMenu("GameFlow/Programs/On Collision Exit")]
	[Help("es", "Programa que se ejecutará una vez tan pronto como un Collider en el rango de escucha deje de colisionar con otro componente.", null)]
	[Help("en", "Program that will be executed one time as soon as a Collider component in the listening range is no longer colliding with another component.", null)]
	public class OnCollisionExit : OnCollisionProgram
	{
		private EventController[] _controllers;

		protected override void RegisterAsListener()
		{
			_controllers = RegisterAsListener(typeof(CollisionExitEvent), typeof(CollisionController), CollisionController.AddController);
			if (base.listeningRange == EventListeningRange.SceneHierarchy)
			{
				CollisionExitEvent.AddListener(this);
			}
		}

		protected override void UnRegisterAsListener()
		{
			CollisionExitEvent.RemoveListener(this);
			UnRegisterAsListener(typeof(CollisionExitEvent), typeof(CollisionController));
		}

		private void OnTransformChildrenChanged()
		{
			if (base.listeningRange == EventListeningRange.GameObjectHierarchy)
			{
				UnsubscribeFrom(_controllers, typeof(CollisionExitEvent));
				_controllers = EventController.GetControllersInHierarchyOf(typeof(CollisionController), base.listeningTarget, CollisionController.AddController);
				SubscribeTo(_controllers, typeof(CollisionExitEvent));
			}
		}
	}
}
