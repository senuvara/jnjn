using UnityEngine;

namespace GameFlow
{
	[DisallowMultipleComponent]
	[AddComponentMenu("GameFlow/Programs/On Trigger Stay")]
	[Help("en", "Program that will be executed every frame while a Collider component configured as trigger in the listening range is being invaded by another component.", null)]
	[Help("es", "Programa que se ejecutará cada fotograma que un componente Collider configurado como disparador (trigger) en el rango de escucha esté siendo invadido por otro componente.", null)]
	public class OnTriggerStay : OnTriggerProgram
	{
		private EventController[] _controllers;

		protected override void RegisterAsListener()
		{
			_controllers = RegisterAsListener(typeof(TriggerStayEvent), typeof(TriggerController), TriggerController.AddController);
			if (base.listeningRange == EventListeningRange.SceneHierarchy)
			{
				TriggerStayEvent.AddListener(this);
			}
		}

		protected override void UnRegisterAsListener()
		{
			TriggerStayEvent.RemoveListener(this);
			UnRegisterAsListener(typeof(TriggerStayEvent), typeof(TriggerController));
		}

		private void OnTransformChildrenChanged()
		{
			if (base.listeningRange == EventListeningRange.GameObjectHierarchy)
			{
				UnsubscribeFrom(_controllers, typeof(TriggerStayEvent));
				_controllers = EventController.GetControllersInHierarchyOf(typeof(TriggerController), base.listeningTarget, TriggerController.AddController);
				SubscribeTo(_controllers, typeof(TriggerStayEvent));
			}
		}
	}
}
