using UnityEngine;

namespace GameFlow
{
	public static class VectorExt
	{
		public static Vector2 Round(this Vector2 v2)
		{
			return new Vector2(Mathf.Round(v2.x), Mathf.Round(v2.y));
		}

		public static Vector3 Round(this Vector3 v3)
		{
			return new Vector3(Mathf.Round(v3.x), Mathf.Round(v3.y), Mathf.Round(v3.z));
		}

		public static Vector2 Multiply(this Vector2 v2, Vector2 multiplier)
		{
			return new Vector2(v2.x * multiplier.x, v2.y * multiplier.y);
		}

		public static Vector2 Divide(this Vector2 v2, Vector2 multiplier)
		{
			return new Vector2(v2.x / multiplier.x, v2.y / multiplier.y);
		}

		public static Vector3 Multiply(this Vector3 v3, Vector3 multiplier)
		{
			return new Vector3(v3.x * multiplier.x, v3.y * multiplier.y, v3.z * multiplier.z);
		}

		public static Vector3 Divide(this Vector3 v3, Vector4 multiplier)
		{
			return new Vector3(v3.x / multiplier.x, v3.y / multiplier.y, v3.z / multiplier.z);
		}

		public static Vector4 Multiply(this Vector4 v4, Vector4 multiplier)
		{
			return new Vector4(v4.x * multiplier.x, v4.y * multiplier.y, v4.z * multiplier.z, v4.w * multiplier.w);
		}

		public static Vector4 Divide(this Vector4 v4, Vector4 multiplier)
		{
			return new Vector4(v4.x / multiplier.x, v4.y / multiplier.y, v4.z / multiplier.z, v4.w / multiplier.w);
		}

		public static Vector3 Clamped(this Vector3 v3, Vector3 min, Vector3 max, bool angles = false)
		{
			v3.x = MathUtils.Clamp(v3.x, min.x, max.x, angles);
			v3.y = MathUtils.Clamp(v3.y, min.y, max.y, angles);
			v3.z = MathUtils.Clamp(v3.z, min.z, max.z, angles);
			return v3;
		}

		public static Vector3 ToMask(this Vector3 v3)
		{
			v3.x = ((v3.x == 0f) ? 0f : 1f);
			v3.y = ((v3.y == 0f) ? 0f : 1f);
			v3.z = ((v3.z == 0f) ? 0f : 1f);
			return v3;
		}

		public static Vector3 ToInverseMask(this Vector3 v3)
		{
			v3.x = ((v3.x == 0f) ? 1 : 0);
			v3.y = ((v3.y == 0f) ? 1 : 0);
			v3.z = ((v3.z == 0f) ? 1 : 0);
			return v3;
		}

		public static Vector3 Masked(this Vector3 v3, Vector3 value, Vector3 mask)
		{
			v3.x = ((mask.x == 0f) ? v3.x : value.x);
			v3.y = ((mask.y == 0f) ? v3.y : value.y);
			v3.z = ((mask.z == 0f) ? v3.z : value.z);
			return v3;
		}
	}
}
