using System;
using UnityEngine;

namespace GameFlow
{
	public static class DataTypeUtils
	{
		public static DataType ToDataType(this Type type)
		{
			if (type == null)
			{
				return DataType.String;
			}
			if (type == typeof(string))
			{
				return DataType.String;
			}
			if (type == typeof(int))
			{
				return DataType.Integer;
			}
			if (type == typeof(float))
			{
				return DataType.Float;
			}
			if (type == typeof(bool))
			{
				return DataType.Boolean;
			}
			if (type == typeof(Vector2))
			{
				return DataType.Vector2;
			}
			if (type == typeof(Vector3))
			{
				return DataType.Vector3;
			}
			if (type == typeof(Rect))
			{
				return DataType.Rect;
			}
			if (type == typeof(Color))
			{
				return DataType.Color;
			}
			if (type == typeof(Quaternion))
			{
				return DataType.Quaternion;
			}
			if (type == typeof(Bounds))
			{
				return DataType.Bounds;
			}
			if (type == typeof(Vector4))
			{
				return DataType.Vector4;
			}
			if (type == typeof(AnimationCurve))
			{
				return DataType.AnimationCurve;
			}
			if (type.IsEnum)
			{
				return DataType.Enum;
			}
			return DataType.Object;
		}

		public static Type ToType(this DataType dataType, Type type = null)
		{
			type = null;
			switch (dataType)
			{
			case DataType.String:
			case DataType.Tag:
				type = typeof(string);
				break;
			case DataType.Integer:
			case DataType.Layer:
				type = typeof(int);
				break;
			case DataType.Float:
				type = typeof(float);
				break;
			case DataType.Boolean:
			case DataType.Toggle:
				type = typeof(bool);
				break;
			case DataType.Vector2:
				type = typeof(Vector2);
				break;
			case DataType.Vector3:
				type = typeof(Vector3);
				break;
			case DataType.Rect:
				type = typeof(Rect);
				break;
			case DataType.Color:
				type = typeof(Color);
				break;
			case DataType.Bounds:
				type = typeof(Bounds);
				break;
			case DataType.Quaternion:
				type = typeof(Quaternion);
				break;
			case DataType.Vector4:
				type = typeof(Vector4);
				break;
			case DataType.AnimationCurve:
				type = typeof(AnimationCurve);
				break;
			}
			return type;
		}

		public static bool IsNumeric(this DataType dataType)
		{
			switch (dataType)
			{
			case DataType.Integer:
			case DataType.Float:
			case DataType.Vector2:
			case DataType.Vector3:
			case DataType.Rect:
			case DataType.Bounds:
			case DataType.Quaternion:
			case DataType.Vector4:
				return true;
			default:
				return false;
			}
		}

		public static bool IsBoolean(this DataType dataType)
		{
			if (dataType == DataType.Boolean || dataType == DataType.Toggle)
			{
				return true;
			}
			return false;
		}
	}
}
