using UnityEngine;

namespace GameFlow
{
	[Help("es", "Devuelve el número de impactos tras lanzar el rayo (@GameFlow{Ray}) especificado.", null)]
	[AddComponentMenu("")]
	[Help("en", "Get the number of impacts after casting the specified @GameFlow{Ray}.", null)]
	public class GetRaycastHitsCount : ListAction
	{
		[SerializeField]
		private Ray _ray;

		[SerializeField]
		private Variable _rayVar;

		[SerializeField]
		private LayerMask _layers;

		[SerializeField]
		private Variable _output;

		protected override void OnReset()
		{
			_layers = 65531;
		}

		public override void Execute()
		{
			if (!(_output == null))
			{
				_output.SetValue(Execute(_rayVar.GetValue(_ray) as Ray, _layers));
			}
		}

		public static int Execute(Ray ray, LayerMask layers)
		{
			if (ray == null)
			{
				return 0;
			}
			UnityEngine.Ray ray2 = ray.ToUnityRay();
			UnityEngine.RaycastHit[] array = Physics.RaycastAll(ray2, ray.length, layers);
			return array.Length;
		}
	}
}
