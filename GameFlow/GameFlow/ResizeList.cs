using UnityEngine;

namespace GameFlow
{
	[AddComponentMenu("")]
	[Help("en", "Sets the size (number of items) in the specified @GameFlow{List}.", null)]
	[Help("es", "Establece el tamaño (número de elementos) de la lista (@GameFlow{List}) especificada.", null)]
	public class ResizeList : ListAction
	{
		[SerializeField]
		private int _size;

		[SerializeField]
		private Variable _sizeVar;

		protected int size => _sizeVar.GetValue(_size);

		public override void Execute()
		{
			Execute(GetList(), size);
		}

		public static void Execute(List list, int size)
		{
			if (list == null)
			{
				return;
			}
			int num = size - list.Count;
			if (num > 0)
			{
				for (int i = 0; i < num; i++)
				{
					list.AddItem();
				}
			}
			else
			{
				if (num >= 0)
				{
					return;
				}
				num = -num;
				for (int j = 0; j < num; j++)
				{
					list.RemoveValueAt(list.Count - 1);
					if (list.Count == 0)
					{
						break;
					}
				}
			}
		}
	}
}
