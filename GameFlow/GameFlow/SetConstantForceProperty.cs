using UnityEngine;

namespace GameFlow
{
	[AddComponentMenu("")]
	[Help("en", "Sets a property of the specified @UnityManual{ConstantForce}.", null)]
	[Help("es", "Modifica una propiedad del componente @UnityManual{ConstantForce2D} especificado.", null)]
	public class SetConstantForceProperty : Action, IExecutableInEditor
	{
		public enum Property
		{
			Force,
			RelativeForce,
			RelativeTorque,
			Torque
		}

		[SerializeField]
		private ConstantForce _target;

		[SerializeField]
		private Variable _targetVar;

		[SerializeField]
		private Property _property;

		[SerializeField]
		private Vector3 _forceValue;

		[SerializeField]
		private Vector3 _relativeForceValue;

		[SerializeField]
		private Vector3 _relativeTorqueValue;

		[SerializeField]
		private Vector3 _torqueValue;

		[SerializeField]
		private Variable _valueVar;

		protected override void OnReset()
		{
			_target = base.gameObject.GetComponent<ConstantForce>();
		}

		public override void Setup()
		{
		}

		public override void Execute()
		{
			ConstantForce constantForce = _targetVar.GetValueAsComponent(_target, typeof(ConstantForce)) as ConstantForce;
			if (!(constantForce == null))
			{
				switch (_property)
				{
				case Property.Force:
					constantForce.force = _valueVar.GetValue(_forceValue);
					break;
				case Property.RelativeForce:
					constantForce.relativeForce = _valueVar.GetValue(_relativeForceValue);
					break;
				case Property.RelativeTorque:
					constantForce.relativeTorque = _valueVar.GetValue(_relativeTorqueValue);
					break;
				case Property.Torque:
					constantForce.torque = _valueVar.GetValue(_torqueValue);
					break;
				}
			}
		}
	}
}
