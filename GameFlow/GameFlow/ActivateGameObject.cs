using UnityEngine;

namespace GameFlow
{
	[Help("en", "Activates the specified @UnityManual{GameObject}.", null)]
	[AddComponentMenu("")]
	[Help("es", "Activa el @UnityManual{GameObject} especificado.", null)]
	public class ActivateGameObject : Action, IExecutableInEditor
	{
		[SerializeField]
		private GameObject _target;

		[SerializeField]
		private Variable _targetVar;

		public override void Setup()
		{
			OnActivate.RegisterAllInactiveAsListeners();
		}

		public override void Execute()
		{
			Execute(_targetVar.GetValue(_target) as GameObject);
		}

		public static void Execute(GameObject go)
		{
			if (!(go == null))
			{
				go.SetActive(value: true);
			}
		}
	}
}
