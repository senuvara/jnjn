using UnityEngine;

namespace GameFlow
{
	[Help("en", "Defines a command.", null)]
	[AddComponentMenu("GameFlow/Tools/Command")]
	[Help("es", "Define un comando.", null)]
	public class Command : Block, IIdentifiable, IVariableFriendly
	{
		[SerializeField]
		private string _id = string.Empty;

		[SerializeField]
		private string _description = "Command description.";

		[SerializeField]
		private Variable _descriptionVar;

		[SerializeField]
		private Program _program;

		[SerializeField]
		private bool _descriptionFoldout;

		public string id => _id;

		public string description => _descriptionVar.GetValue(_description);

		public Program program => _program;

		private void Awake()
		{
		}

		private void Start()
		{
		}

		protected override void OnReset()
		{
			SetContainer();
			if (_id == string.Empty)
			{
				_id = "Command" + Random.Range(1000, 9999);
			}
		}

		private void SetContainer()
		{
			if (container != null || !BlockInsertion.enabled || BlockInsertion.clipboard)
			{
				return;
			}
			ICommandContainer commandContainer = BlockInsertion.target as ICommandContainer;
			if (commandContainer == null)
			{
				commandContainer = base.gameObject.GetComponent<Commands>();
				if (commandContainer == null)
				{
					commandContainer = base.gameObject.AddComponent<Commands>();
				}
				BlockInsertion.index = -1;
			}
			if (BlockInsertion.index < 0 || BlockInsertion.index >= commandContainer.GetCommands().Count)
			{
				if (!commandContainer.GetCommands().Contains(this))
				{
					commandContainer.GetCommands().Add(this);
					commandContainer.CommandAdded(this);
					commandContainer.SetDirty(dirty: true);
				}
			}
			else
			{
				commandContainer.GetCommands().Insert(BlockInsertion.index, this);
				commandContainer.CommandAdded(this);
				commandContainer.SetDirty(dirty: true);
			}
			container = (commandContainer as Object);
			BlockInsertion.Reset();
		}

		public string GetIdForControls()
		{
			return _id;
		}

		public string GetIdForSelectors()
		{
			return _id;
		}

		public virtual string GetTypeForSelectors()
		{
			return "Command";
		}

		private void OnValidate()
		{
			HideInInspector();
		}
	}
}
