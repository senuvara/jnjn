using UnityEngine;

namespace GameFlow
{
	[Help("es", "Obtiene la longitud en caracteres del texto almacenado en la @GameFlow{Variable} especificada.", null)]
	[Help("en", "Gets the length in characters of the string stored in the specified @GameFlow{Variable}.", null)]
	[AddComponentMenu("")]
	public class GetStringLength : Action, IExecutableInEditor
	{
		[SerializeField]
		private Variable _variable;

		[SerializeField]
		private Variable _output;

		public override void Execute()
		{
			if (!(_variable == null) && !(_output == null))
			{
				_output.SetValue(_variable.GetValue(string.Empty).Length);
			}
		}
	}
}
