using UnityEngine;

namespace GameFlow
{
	[Help("en", "Makes a specified object follow the specified @GameFlow{Path}.", null)]
	[Help("es", "Hace que un objeto recorra una trayectoria (@GameFlow{Path}) especificada.", null)]
	[AddComponentMenu("")]
	public class FollowPath : TimeAction
	{
		public enum Mode
		{
			Duration,
			Velocity
		}

		[SerializeField]
		private Path _path;

		[SerializeField]
		private Variable _pathVar;

		[SerializeField]
		private Transform _follower;

		[SerializeField]
		private Variable _followerVar;

		[SerializeField]
		private Direction _direction;

		[SerializeField]
		private Variable _directionVar;

		[SerializeField]
		private int _interpolation = 7;

		[SerializeField]
		private Variable _interpolationVar;

		[SerializeField]
		private Mode _mode;

		[SerializeField]
		private Variable _modeVar;

		[SerializeField]
		private float _velocity = 1f;

		[SerializeField]
		private Variable _velocityVar;

		protected Path path => _pathVar.GetValueAsComponent(_path, typeof(Path)) as Path;

		public Interpolation interpolation => (Interpolation)_interpolationVar.GetValue(_interpolation);

		public override float GetDefaultDuration()
		{
			return 10f;
		}

		protected override void OnReset()
		{
			base.OnReset();
			_follower = base.gameObject.transform;
		}

		public override void Execute()
		{
			Path path = this.path;
			if (path == null)
			{
				return;
			}
			Transform transform = _followerVar.GetValueAsComponent(_follower, typeof(Transform)) as Transform;
			if (transform == null)
			{
				return;
			}
			float t = 0f;
			float length = path.length;
			Segment segment = null;
			Direction direction = (Direction)(object)_directionVar.GetValue(_direction);
			switch ((Mode)(object)_modeVar.GetValue(_mode))
			{
			case Mode.Duration:
				t = base.timer.elapsedTimeDelta;
				t = ((direction != 0) ? (1f - t) : t);
				segment = path.GetSegment(t);
				t = (t * length - segment.offset) / segment.length;
				break;
			case Mode.Velocity:
			{
				float value = _velocityVar.GetValue(_velocity);
				base.timer.duration = length / value;
				t = base.timer.elapsedTime * value / length;
				t = ((direction != 0) ? (1f - t) : t);
				segment = path.GetSegment(t);
				t = (t * length - segment.offset) / segment.length;
				break;
			}
			}
			if (segment != null)
			{
				if ((interpolation & Interpolation.Position) != 0)
				{
					transform.position = segment.GetPosition(t);
				}
				if ((interpolation & Interpolation.Rotation) != 0)
				{
					transform.rotation = segment.GetRotation(t);
				}
				if ((interpolation & Interpolation.Scale) != 0)
				{
					transform.localScale = segment.GetScale(t);
				}
			}
		}
	}
}
