using System;

namespace GameFlow
{
	[AttributeUsage(AttributeTargets.Field)]
	public class LeftToggle : Attribute
	{
	}
}
