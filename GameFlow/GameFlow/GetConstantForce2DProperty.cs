using UnityEngine;

namespace GameFlow
{
	[Help("en", "Gets a property of the specified @UnityManual{ConstantForce2D} component.", null)]
	[Help("es", "Lee una propiedad del componente @UnityManual{ConstantForce2D} especificado.", null)]
	[AddComponentMenu("")]
	public class GetConstantForce2DProperty : Action, IExecutableInEditor
	{
		public enum Property
		{
			Force,
			RelativeForce,
			Torque
		}

		[SerializeField]
		private ConstantForce2D _source;

		[SerializeField]
		private Variable _sourceVar;

		[SerializeField]
		private Property _property;

		[SerializeField]
		private Variable _output;

		protected override void OnReset()
		{
			_source = base.gameObject.GetComponent<ConstantForce2D>();
		}

		public override void Setup()
		{
		}

		public override void Execute()
		{
			ConstantForce2D constantForce2D = _sourceVar.GetValueAsComponent(_source, typeof(ConstantForce2D)) as ConstantForce2D;
			if (!(constantForce2D == null))
			{
				switch (_property)
				{
				case Property.Force:
					_output.SetValue(constantForce2D.force);
					break;
				case Property.RelativeForce:
					_output.SetValue(constantForce2D.relativeForce);
					break;
				case Property.Torque:
					_output.SetValue(constantForce2D.torque);
					break;
				}
			}
		}
	}
}
