using UnityEngine;

namespace GameFlow
{
	[Help("es", "Programa que se ejecutará cuando el puntero del ratón se arrastre (es decir, se mueva mientras se mantiene alguno de sus botones pulsado) sobre un componente Collider o GUIElement activado en el rango de escucha.", null)]
	[DisallowMultipleComponent]
	[AddComponentMenu("GameFlow/Programs/On Mouse Drag")]
	[Help("en", "Program that will be executed when the mouse is dragged (that is, moved with any of the buttons pressed) over an enabled Collider or GUIElement component in the listening range.", null)]
	public class OnMouseDrag : OnMouseProgram
	{
		private Parameter _deltaParam;

		private EventController[] _controllers;

		public Parameter deltaParam
		{
			get
			{
				if (_deltaParam == null)
				{
					_deltaParam = GetParameter("Delta");
				}
				return _deltaParam;
			}
		}

		protected override void RegisterAsListener()
		{
			_controllers = RegisterAsListener(typeof(MouseDragEvent), typeof(MouseController), MouseController.AddController);
			if (base.listeningRange == EventListeningRange.SceneHierarchy)
			{
				MouseDragEvent.AddListener(this);
			}
		}

		protected override void CacheParameters()
		{
			base.CacheParameters();
			CacheParameter(deltaParam);
		}

		protected override void SetParameters(EventObject e)
		{
			base.SetParameters(e);
			MouseDragEvent mouseDragEvent = e as MouseDragEvent;
			deltaParam.SetValue(mouseDragEvent.delta);
		}

		protected override void UnRegisterAsListener()
		{
			MouseDragEvent.RemoveListener(this);
			UnRegisterAsListener(typeof(MouseDragEvent), typeof(MouseController));
		}

		private void OnTransformChildrenChanged()
		{
			if (base.listeningRange == EventListeningRange.GameObjectHierarchy)
			{
				UnsubscribeFrom(_controllers, typeof(MouseDragEvent));
				_controllers = EventController.GetControllersInHierarchyOf(typeof(MouseController), base.listeningTarget, MouseController.AddController);
				SubscribeTo(_controllers, typeof(MouseDragEvent));
			}
		}
	}
}
