using System.Collections.Generic;
using UnityEngine;

namespace GameFlow
{
	public abstract class Conditional : Action, IActionContainer, IBlockContainer, IConditionContainer, IExecutableInEditor
	{
		[SerializeField]
		private List<Condition> _conditions = new List<Condition>();

		[SerializeField]
		private List<Action> _actions = new List<Action>();

		protected ActionSequence sequence;

		private bool _dirty;

		public virtual List<Action> GetActions(int sectionIndex = 0)
		{
			return _actions;
		}

		public int GetActionCount(int sectionIndex = 0)
		{
			return ActionUtils.GetActionCount(this);
		}

		public virtual int GetActionSectionsCount()
		{
			return 1;
		}

		public virtual void ActionAdded(Action action, int sectionIndex = 0)
		{
		}

		public List<Condition> GetConditions()
		{
			return _conditions;
		}

		public virtual void ConditionAdded(Condition condition)
		{
		}

		public override void Setup()
		{
			sequence = new ActionSequence(_actions);
		}

		public virtual bool Evaluate(bool defaultResult)
		{
			List<Condition> conditions = GetConditions();
			if (conditions == null)
			{
				Log.Warning("List of conditions should not be null.", base.gameObject);
				return defaultResult;
			}
			if (conditions.Count == 0)
			{
				return defaultResult;
			}
			int num = 0;
			bool flag = conditions[num].Evaluate();
			Connective connective = conditions[num].connective;
			for (num++; num < conditions.Count; num++)
			{
				switch (connective)
				{
				case Connective.And:
					flag = (flag && conditions[num].Evaluate());
					break;
				case Connective.Or:
					flag = (flag || conditions[num].Evaluate());
					break;
				}
				connective = conditions[num].connective;
			}
			return flag;
		}

		public virtual void Break()
		{
		}

		public void SetDirty(bool dirty)
		{
			_dirty = dirty;
		}

		public bool IsDirty()
		{
			return _dirty;
		}

		public virtual List<Block> GetBlocks()
		{
			return null;
		}

		public virtual bool Contains(Block block)
		{
			if (block != null)
			{
				if (block as Condition != null)
				{
					return _conditions.Contains(block as Condition);
				}
				if (block as Action != null)
				{
					return _actions.Contains(block as Action);
				}
			}
			return false;
		}
	}
}
