namespace GameFlow
{
	public abstract class DataItem : Block, IData, IVariableFriendly
	{
		private void Reset()
		{
			DoNotDestroyOrphan = true;
			OnReset();
		}
	}
}
