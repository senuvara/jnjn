using UnityEngine;

namespace GameFlow
{
	[Help("es", "Programa que se ejecutará cuando la edición de un componente UI en el rango de escucha concluya.", null)]
	[Help("en", "Program that will be executed when the editing of a UI component in the listening range ends.", null)]
	[DisallowMultipleComponent]
	[AddComponentMenu("GameFlow/Programs/On End Edit")]
	public class OnEndEdit : EventProgram
	{
		private Parameter _sourceParam;

		private Parameter _valueParam;

		private EventController[] _controllers;

		public Parameter sourceParam
		{
			get
			{
				if (_sourceParam == null)
				{
					_sourceParam = GetParameter("Source");
				}
				return _sourceParam;
			}
		}

		public Parameter valueParam
		{
			get
			{
				if (_valueParam == null)
				{
					_valueParam = GetParameter("Value");
				}
				return _valueParam;
			}
		}

		protected override void CacheParameters()
		{
			CacheParameter(sourceParam);
			CacheParameter(valueParam);
		}

		protected override void RegisterAsListener()
		{
			_controllers = RegisterAsListener(typeof(EndEditEvent), typeof(UIEventController), UIEventController.AddController);
			if (base.listeningRange == EventListeningRange.SceneHierarchy)
			{
				EndEditEvent.AddListener(this);
			}
		}

		protected override void UnRegisterAsListener()
		{
			EndEditEvent.RemoveListener(this);
			UnRegisterAsListener(typeof(EndEditEvent), typeof(UIEventController));
		}

		protected override void SetParameters(EventObject e)
		{
			EndEditEvent endEditEvent = e as EndEditEvent;
			sourceParam.SetValue(endEditEvent.source);
			valueParam.SetValue(endEditEvent.value);
		}

		private void OnTransformChildrenChanged()
		{
			if (base.listeningRange == EventListeningRange.GameObjectHierarchy)
			{
				UnsubscribeFrom(_controllers, typeof(EndEditEvent));
				_controllers = EventController.GetControllersInHierarchyOf(typeof(UIEventController), base.listeningTarget, UIEventController.AddController);
				SubscribeTo(_controllers, typeof(EndEditEvent));
			}
		}
	}
}
