using UnityEngine;

namespace GameFlow
{
	[AddComponentMenu("")]
	[Help("es", "Lee una propiedad del componente @UnityManual{AudioHighPassFilter} especificado.", null)]
	[Help("en", "Gets a property of the specified @UnityManual{AudioHighPassFilter} component.", null)]
	public class GetAudioHighPassFilterProperty : Action, IExecutableInEditor
	{
		public enum Property
		{
			CutoffFrequency,
			HighpassResonanceQ
		}

		[SerializeField]
		private AudioHighPassFilter _source;

		[SerializeField]
		private Variable _sourceVar;

		[SerializeField]
		private Property _property;

		[SerializeField]
		private Variable _output;

		protected override void OnReset()
		{
			_source = base.gameObject.GetComponent<AudioHighPassFilter>();
		}

		public override void Setup()
		{
		}

		public override void Execute()
		{
			AudioHighPassFilter audioHighPassFilter = _sourceVar.GetValueAsComponent(_source, typeof(AudioHighPassFilter)) as AudioHighPassFilter;
			if (!(audioHighPassFilter == null))
			{
				switch (_property)
				{
				case Property.CutoffFrequency:
					_output.SetValue(audioHighPassFilter.cutoffFrequency);
					break;
				case Property.HighpassResonanceQ:
					_output.SetValue(audioHighPassFilter.highpassResonanceQ);
					break;
				}
			}
		}
	}
}
