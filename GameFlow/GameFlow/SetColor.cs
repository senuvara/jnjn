using UnityEngine;
using UnityEngine.UI;

namespace GameFlow
{
	[Help("en", "Sets the color of the specified object.", null)]
	[AddComponentMenu("")]
	[Help("es", "Cambia el color del objeto especificado.", null)]
	public class SetColor : Action
	{
		[SerializeField]
		private GameObject _target;

		[SerializeField]
		private Variable _targetVar;

		[SerializeField]
		private Color _color = Color.white;

		[SerializeField]
		private Variable _colorVar;

		[SerializeField]
		private int _r = 255;

		[SerializeField]
		private Variable _rVar;

		[SerializeField]
		private int _g = 255;

		[SerializeField]
		private Variable _gVar;

		[SerializeField]
		private int _b = 255;

		[SerializeField]
		private Variable _bVar;

		[SerializeField]
		private int _a = 255;

		[SerializeField]
		private Variable _aVar;

		public GameObject target => _targetVar.GetValue(_target) as GameObject;

		public Color color => _colorVar.GetValue(_color);

		protected int r => _rVar.GetValue(_r);

		protected int g => _gVar.GetValue(_g);

		protected int b => _bVar.GetValue(_b);

		protected int a => _aVar.GetValue(_a);

		protected override void OnReset()
		{
			_target = base.gameObject;
		}

		public override void Execute()
		{
			Execute(target, color);
		}

		public static void Execute(GameObject target, Color color)
		{
			if (!(target == null))
			{
				Renderer component = target.GetComponent<Renderer>();
				if (component != null)
				{
					component.material.color = color;
				}
				Text component2 = target.GetComponent<Text>();
				if (component2 != null)
				{
					component2.color = color;
				}
				Image component3 = target.GetComponent<Image>();
				if (component3 != null)
				{
					component3.color = color;
				}
				GUIText component4 = target.GetComponent<GUIText>();
				if (component4 != null)
				{
					component4.color = color;
				}
				GUITexture component5 = target.GetComponent<GUITexture>();
				if (component5 != null)
				{
					component5.color = color;
				}
			}
		}
	}
}
