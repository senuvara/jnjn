using UnityEngine;

namespace GameFlow
{
	[DisallowMultipleComponent]
	[Help("es", "Programa que se ejecutará solo una vez cuando la aplicación se inicie.", null)]
	[Help("en", "Program that will be executed only once, when the app is initiated.", null)]
	[AddComponentMenu("GameFlow/Programs/On Application Init")]
	public class OnApplicationInit : EventProgram
	{
		protected override void OnAwake()
		{
			base.OnAwake();
			ApplicationEventController.Init();
		}

		protected override void RegisterAsListener()
		{
			ApplicationInitEvent.AddListener(this);
		}

		protected override void UnRegisterAsListener()
		{
			ApplicationInitEvent.RemoveListener(this);
		}
	}
}
