using UnityEngine;

namespace GameFlow
{
	[Help("en", "Prints the 'Hello, World!' message in the console.", null)]
	[AddComponentMenu("")]
	[Help("es", "Imprime el mensaje 'Hello, World!' en la consola.", null)]
	public class HelloWorld : Action, IExecutableInEditor
	{
		public override void Execute()
		{
			Debug.Log("Hello, World!", base.gameObject);
		}
	}
}
