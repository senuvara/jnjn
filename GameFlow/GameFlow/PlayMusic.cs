using UnityEngine;

namespace GameFlow
{
	[Help("es", "Reproduce la música especificada con fading de volumen opcional.", null)]
	[AddComponentMenu("")]
	[Help("en", "Plays the specified music with optional volumen fading.", null)]
	public class PlayMusic : Action
	{
		[SerializeField]
		private AudioClip _audioClip;

		[SerializeField]
		private Variable _audioClipVar;

		[SerializeField]
		private float _volume = 1f;

		[SerializeField]
		private Variable _volumeVar;

		[SerializeField]
		private bool _loop = true;

		[SerializeField]
		private float _fading;

		[SerializeField]
		private Variable _fadingVar;

		public override void Execute()
		{
			AudioController.instance.PlayMusic(_audioClipVar.GetValue(_audioClip) as AudioClip, _volumeVar.GetValue(_volume), _loop, _fadingVar.GetValue(_fading));
		}
	}
}
