using UnityEngine;

namespace GameFlow
{
	[Help("en", "Rotates the specified object to match the rotation of another object.", null)]
	[Help("es", "Rota el objeto especificado para que encaje con la rotación de otro objeto.", null)]
	[AddComponentMenu("")]
	public class RotateTo : Rotate
	{
	}
}
