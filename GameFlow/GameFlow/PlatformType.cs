namespace GameFlow
{
	public enum PlatformType
	{
		Desktop,
		Mobile,
		Console,
		Web
	}
}
