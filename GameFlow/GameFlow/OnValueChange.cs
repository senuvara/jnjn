using UnityEngine;

namespace GameFlow
{
	[AddComponentMenu("GameFlow/Programs/On Value Change")]
	[DisallowMultipleComponent]
	[Help("es", "Programa que se ejecutará cuando el valor de un componente UI en el rango de escucha sea modificado.", null)]
	[Help("en", "Program that will be executed when the value of a UI component in the listening range is modified.", null)]
	public class OnValueChange : EventProgram
	{
		private Parameter _sourceParam;

		private Parameter _valueParam;

		private EventController[] _controllers;

		public Parameter sourceParam
		{
			get
			{
				if (_sourceParam == null)
				{
					_sourceParam = GetParameter("Source");
				}
				return _sourceParam;
			}
		}

		public Parameter valueParam
		{
			get
			{
				if (_valueParam == null)
				{
					_valueParam = GetParameter("Value");
				}
				return _valueParam;
			}
		}

		protected override void CacheParameters()
		{
			CacheParameter(sourceParam);
			CacheParameter(valueParam);
		}

		protected override void RegisterAsListener()
		{
			_controllers = RegisterAsListener(typeof(ValueChangeEvent), typeof(UIEventController), UIEventController.AddController);
			if (base.listeningRange == EventListeningRange.SceneHierarchy)
			{
				ValueChangeEvent.AddListener(this);
			}
		}

		protected override void UnRegisterAsListener()
		{
			ValueChangeEvent.RemoveListener(this);
			UnRegisterAsListener(typeof(ValueChangeEvent), typeof(UIEventController));
		}

		protected override void SetParameters(EventObject e)
		{
			ValueChangeEvent valueChangeEvent = e as ValueChangeEvent;
			sourceParam.SetValue(valueChangeEvent.source);
			switch (valueChangeEvent.valueDataType)
			{
			case DataType.String:
				valueParam.SetValue(valueChangeEvent.stringValue);
				break;
			case DataType.Toggle:
				valueParam.SetToggleValue(valueChangeEvent.toggleValue);
				break;
			case DataType.Float:
				valueParam.SetValue(valueChangeEvent.floatValue);
				break;
			}
		}

		private void OnTransformChildrenChanged()
		{
			if (base.listeningRange == EventListeningRange.GameObjectHierarchy)
			{
				UnsubscribeFrom(_controllers, typeof(ValueChangeEvent));
				_controllers = EventController.GetControllersInHierarchyOf(typeof(UIEventController), base.listeningTarget, UIEventController.AddController);
				SubscribeTo(_controllers, typeof(ValueChangeEvent));
			}
		}
	}
}
