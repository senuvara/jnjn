using System.Collections.Generic;
using UnityEngine;

namespace GameFlow
{
	[AddComponentMenu("GameFlow/Tools/Pool")]
	[Help("en", "Defines a pool of objects.", null)]
	[Help("es", "Define un pool de objetos.", null)]
	[DisallowMultipleComponent]
	public class Pool : BaseBehaviour, IIdentifiable, ITool, IVariableFriendly
	{
		[SerializeField]
		private GameObject _model;

		[SerializeField]
		private Variable _modelVar;

		[SerializeField]
		private int _capacity;

		private List<GameObject> _objects;

		private Dictionary<GameObject, bool> _usage = new Dictionary<GameObject, bool>();

		private Transform _initParent;

		private Vector3 _initPosition;

		private Vector3 _initRotation;

		private Vector3 _initScale;

		private int _cursor;

		public GameObject model => _modelVar.GetValue(_model) as GameObject;

		public int capacity => _capacity;

		protected override void OnReset()
		{
		}

		protected override void OnAwake()
		{
			Init();
		}

		private void Init()
		{
			if (_objects != null)
			{
				return;
			}
			GameObject model = this.model;
			if (model == null)
			{
				_capacity = 0;
				return;
			}
			_objects = new List<GameObject>();
			for (int i = 0; i < _capacity; i++)
			{
				GameObject gameObject = Object.Instantiate(model);
				if (gameObject == null)
				{
					_capacity--;
					continue;
				}
				if (i == 0)
				{
					_initParent = base.transform;
					_initPosition = gameObject.transform.localPosition;
					_initRotation = gameObject.transform.localEulerAngles;
					_initScale = gameObject.transform.localScale;
				}
				_objects.Add(gameObject);
				gameObject.name = model.name + i;
				gameObject.transform.parent = base.transform;
				gameObject.SetActive(value: false);
				OnActivate.RegisterInactiveAsListener(gameObject);
				_usage.Add(gameObject, value: false);
				PoolStamp component = gameObject.GetComponent<PoolStamp>();
				component = ((!(component == null)) ? component : gameObject.AddComponent<PoolStamp>());
				component.pool = this;
				gameObject.HideComponents();
			}
		}

		public GameObject GetObject()
		{
			int cursor = _cursor;
			while (_cursor < _capacity)
			{
				GameObject objectAtIndexIfNotInUse = GetObjectAtIndexIfNotInUse(_cursor++);
				if (objectAtIndexIfNotInUse != null)
				{
					return objectAtIndexIfNotInUse;
				}
			}
			_cursor = 0;
			while (_cursor < cursor)
			{
				GameObject objectAtIndexIfNotInUse2 = GetObjectAtIndexIfNotInUse(_cursor++);
				if (objectAtIndexIfNotInUse2 != null)
				{
					return objectAtIndexIfNotInUse2;
				}
			}
			return null;
		}

		private GameObject GetObjectAtIndexIfNotInUse(int index)
		{
			GameObject gameObject = _objects[index];
			if (!_usage[gameObject])
			{
				_usage[gameObject] = true;
				gameObject.SetActive(value: true);
				return gameObject;
			}
			return null;
		}

		public void ReleaseObject(GameObject go)
		{
			if (_usage.ContainsKey(go) && _usage[go])
			{
				_usage[go] = false;
				go.SetActive(value: false);
				go.transform.parent = _initParent;
				go.transform.localPosition = _initPosition;
				go.transform.localEulerAngles = _initRotation;
				go.transform.localScale = _initScale;
			}
		}

		public void ReleaseAll()
		{
			for (int i = 0; i < _capacity; i++)
			{
				ReleaseObject(_objects[i]);
			}
			_cursor = 0;
		}

		public string GetIdForControls()
		{
			return "Pool";
		}

		public string GetIdForSelectors()
		{
			return "Pool";
		}

		public virtual string GetTypeForSelectors()
		{
			return "Pool";
		}
	}
}
