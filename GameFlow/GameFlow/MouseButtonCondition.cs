using UnityEngine;

namespace GameFlow
{
	[AddComponentMenu("")]
	[Help("es", "Evalúa si el boton de ratón especificado es o acaba de ser pulsado.", null)]
	[Help("en", "Evaluates whether a specific mouse button is or has just been pressed.", null)]
	public class MouseButtonCondition : Condition
	{
		public enum Comparison
		{
			IsDown,
			IsJustDown,
			IsUp,
			IsJustUp
		}

		[SerializeField]
		private MouseButton _button;

		[SerializeField]
		private Variable _buttonVar;

		[SerializeField]
		private Comparison _comparison = Comparison.IsJustDown;

		public MouseButton button => (MouseButton)(object)_buttonVar.GetValue(_button);

		public override bool Evaluate()
		{
			switch (_comparison)
			{
			case Comparison.IsDown:
				return MouseButtonController.IsButtonDown(button);
			case Comparison.IsJustDown:
				return MouseButtonController.IsButtonJustDown(button);
			case Comparison.IsUp:
				return MouseButtonController.IsButtonUp(button);
			case Comparison.IsJustUp:
				return MouseButtonController.IsButtonJustUp(button);
			default:
				return false;
			}
		}
	}
}
