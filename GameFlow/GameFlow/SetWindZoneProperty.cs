using UnityEngine;

namespace GameFlow
{
	[AddComponentMenu("")]
	[Help("en", "Sets a property of the specified @UnityManual{WindZone}.", null)]
	[Help("es", "Modifica una propiedad del componente @UnityManual{WindZone} especificado.", null)]
	public class SetWindZoneProperty : Action, IExecutableInEditor
	{
		public enum Property
		{
			Mode,
			Radius,
			WindMain,
			WindPulseFrequency,
			WindPulseMagnitude,
			WindTurbulence
		}

		[SerializeField]
		private WindZone _target;

		[SerializeField]
		private Variable _targetVar;

		[SerializeField]
		private Property _property;

		[SerializeField]
		private WindZoneMode _modeValue;

		[SerializeField]
		private float _radiusValue;

		[SerializeField]
		private float _windMainValue;

		[SerializeField]
		private float _windPulseFrequencyValue;

		[SerializeField]
		private float _windPulseMagnitudeValue;

		[SerializeField]
		private float _windTurbulenceValue;

		[SerializeField]
		private Variable _valueVar;

		protected override void OnReset()
		{
			_target = base.gameObject.GetComponent<WindZone>();
		}

		public override void Setup()
		{
		}

		public override void Execute()
		{
			WindZone windZone = _targetVar.GetValueAsComponent(_target, typeof(WindZone)) as WindZone;
			if (!(windZone == null))
			{
				switch (_property)
				{
				case Property.Mode:
					windZone.mode = (WindZoneMode)(object)_valueVar.GetValue(_modeValue);
					break;
				case Property.Radius:
					windZone.radius = _valueVar.GetValue(_radiusValue);
					break;
				case Property.WindMain:
					windZone.windMain = _valueVar.GetValue(_windMainValue);
					break;
				case Property.WindPulseFrequency:
					windZone.windPulseFrequency = _valueVar.GetValue(_windPulseFrequencyValue);
					break;
				case Property.WindPulseMagnitude:
					windZone.windPulseMagnitude = _valueVar.GetValue(_windPulseMagnitudeValue);
					break;
				case Property.WindTurbulence:
					windZone.windTurbulence = _valueVar.GetValue(_windTurbulenceValue);
					break;
				}
			}
		}
	}
}
