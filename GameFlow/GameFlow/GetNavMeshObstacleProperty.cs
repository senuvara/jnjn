using UnityEngine;
using UnityEngine.AI;

namespace GameFlow
{
	[Help("es", "Lee una propiedad del componente @UnityManual{NavMeshObstacle} especificado.", null)]
	[Help("en", "Gets a property of the specified @UnityManual{NavMeshObstacle} component.", null)]
	[AddComponentMenu("")]
	public class GetNavMeshObstacleProperty : Action, IExecutableInEditor
	{
		public enum Property
		{
			CarveOnlyStationary,
			Carving,
			CarvingMoveThreshold,
			CarvingTimeToStationary,
			Center,
			Height,
			Radius,
			Shape,
			Size,
			Velocity
		}

		[SerializeField]
		private NavMeshObstacle _source;

		[SerializeField]
		private Variable _sourceVar;

		[SerializeField]
		private Property _property;

		[SerializeField]
		private Variable _output;

		protected override void OnReset()
		{
			_source = base.gameObject.GetComponent<NavMeshObstacle>();
		}

		public override void Setup()
		{
		}

		public override void Execute()
		{
			NavMeshObstacle navMeshObstacle = _sourceVar.GetValueAsComponent(_source, typeof(NavMeshObstacle)) as NavMeshObstacle;
			if (!(navMeshObstacle == null))
			{
				switch (_property)
				{
				case Property.CarveOnlyStationary:
					_output.SetValue(navMeshObstacle.carveOnlyStationary);
					break;
				case Property.Carving:
					_output.SetValue(navMeshObstacle.carving);
					break;
				case Property.CarvingMoveThreshold:
					_output.SetValue(navMeshObstacle.carvingMoveThreshold);
					break;
				case Property.CarvingTimeToStationary:
					_output.SetValue(navMeshObstacle.carvingTimeToStationary);
					break;
				case Property.Center:
					_output.SetValue(navMeshObstacle.center);
					break;
				case Property.Height:
					_output.SetValue(navMeshObstacle.height);
					break;
				case Property.Radius:
					_output.SetValue(navMeshObstacle.radius);
					break;
				case Property.Shape:
					_output.SetValue(navMeshObstacle.shape);
					break;
				case Property.Size:
					_output.SetValue(navMeshObstacle.size);
					break;
				case Property.Velocity:
					_output.SetValue(navMeshObstacle.velocity);
					break;
				}
			}
		}
	}
}
