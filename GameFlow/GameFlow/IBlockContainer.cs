using System.Collections.Generic;

namespace GameFlow
{
	public interface IBlockContainer
	{
		List<Block> GetBlocks();

		void SetDirty(bool dirty);

		bool IsDirty();

		bool Contains(Block block);
	}
}
