using UnityEngine;

namespace GameFlow
{
	[Help("en", "Executes the contained actions repeately until a Break action.", null)]
	[Help("es", "Ejecuta las acciones contenidas repetidamente hasta una accion Break.", null)]
	[AddComponentMenu("")]
	public class Loop : While
	{
		public override bool Evaluate(bool defaultResult)
		{
			return true;
		}
	}
}
