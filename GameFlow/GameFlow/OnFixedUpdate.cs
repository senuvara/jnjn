using UnityEngine;

namespace GameFlow
{
	[Help("en", "Program that will be executed, if enabled, every fixed framerate frame.", null)]
	[AddComponentMenu("GameFlow/Programs/On Fixed Update")]
	[Help("es", "Programa que se ejecutará, si está activo, en cada fotograma del intervalo de refresco fijado.", null)]
	[DisallowMultipleComponent]
	public class OnFixedUpdate : Program
	{
		private void Update()
		{
		}

		private void FixedUpdate()
		{
			Restart();
			Step();
		}

		protected override bool WillFireEventOnProgramFinished()
		{
			return false;
		}
	}
}
