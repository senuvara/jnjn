using UnityEngine;

namespace GameFlow
{
	[Help("es", "Recupera el elemento en el índice dado de la @GameFlow{List} especificada.", null)]
	[AddComponentMenu("")]
	[Help("en", "Gets the item at the given index in the specified @GameFlow{List}.", null)]
	public class GetItemFromList : ListAction
	{
		[SerializeField]
		private Variable _output;

		public override void Execute()
		{
			ExecuteWith(GetList(), GetIndex(), _output);
		}

		public static void ExecuteWith(List list, int index, Variable output)
		{
			if (!(list == null) && !(output == null) && list.IsIndexValid(index))
			{
				switch (list.dataType)
				{
				case DataType.String:
					output.SetValue(list.GetStringAt(index));
					break;
				case DataType.Tag:
					output.SetTagValue(list.GetStringAt(index));
					break;
				case DataType.Integer:
					output.SetValue(list.GetIntAt(index));
					break;
				case DataType.Layer:
					output.SetLayerValue(list.GetIntAt(index));
					break;
				case DataType.Float:
					output.SetValue(list.GetFloatAt(index));
					break;
				case DataType.Boolean:
					output.SetValue(list.GetBoolAt(index));
					break;
				case DataType.Toggle:
					output.SetToggleValue(list.GetBoolAt(index));
					break;
				case DataType.Vector2:
					output.SetValue(list.GetVector2At(index));
					break;
				case DataType.Vector3:
					output.SetValue(list.GetVector3At(index));
					break;
				case DataType.Rect:
					output.SetValue(list.GetRectAt(index));
					break;
				case DataType.Color:
					output.SetValue(list.GetColorAt(index));
					break;
				case DataType.Object:
					output.SetValue(list.GetObjectAt(index));
					break;
				case DataType.Enum:
					output.SetValue(list.GetEnumAt(index));
					break;
				}
			}
		}
	}
}
