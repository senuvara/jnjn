using UnityEngine;

namespace GameFlow
{
	[Help("en", "Enables the specified Collider component.", null)]
	[AddComponentMenu("")]
	[Help("es", "Habilita el componente Collider especificado.", null)]
	public class EnableCollider : Action, IExecutableInEditor
	{
		[SerializeField]
		private Collider _target;

		[SerializeField]
		private Variable _targetVar;

		public override void Execute()
		{
			Execute(_targetVar.GetValueAsComponent(_target, typeof(Collider)) as Collider);
		}

		public static void Execute(Collider collider)
		{
			if (collider != null)
			{
				collider.enabled = true;
			}
		}
	}
}
