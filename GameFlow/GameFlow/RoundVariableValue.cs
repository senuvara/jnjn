using System;
using UnityEngine;

namespace GameFlow
{
	[Help("en", "Rounds the numeric value of the specified @GameFlow{Variable}.", null)]
	[AddComponentMenu("")]
	[Help("es", "Redondea el valor numérico de la @GameFlow{Variable} especificada.", null)]
	public class RoundVariableValue : VariableAction
	{
		[SerializeField]
		private int _decimals = 2;

		public override void Execute()
		{
			Execute(base.variable, _decimals);
		}

		public static void Execute(Variable variable, int decimals)
		{
			if (variable == null)
			{
				return;
			}
			Variable variable2 = (!variable.isIndirect) ? variable : variable.indirection;
			if (!(variable2 == null) && !variable2.isReadOnly)
			{
				switch (variable2.dataType)
				{
				case DataType.Integer:
					break;
				case DataType.Boolean:
					break;
				case DataType.Float:
					variable2.floatValue = (float)Math.Round(variable2.floatValue, decimals);
					break;
				case DataType.Vector2:
				{
					Vector2 vector2Value = variable2.vector2Value;
					float x = (float)Math.Round(vector2Value.x, decimals);
					Vector2 vector2Value2 = variable2.vector2Value;
					float y = (float)Math.Round(vector2Value2.y, decimals);
					variable2.vector2Value = new Vector2(x, y);
					break;
				}
				case DataType.Vector3:
				{
					Vector3 vector3Value = variable2.vector3Value;
					float x = (float)Math.Round(vector3Value.x, decimals);
					Vector3 vector3Value2 = variable2.vector3Value;
					float y = (float)Math.Round(vector3Value2.y, decimals);
					Vector3 vector3Value3 = variable2.vector3Value;
					float z = (float)Math.Round(vector3Value3.z, decimals);
					variable2.vector3Value = new Vector3(x, y, z);
					break;
				}
				case DataType.Rect:
				{
					float x = (float)Math.Round(variable2.rectValue.x, decimals);
					float y = (float)Math.Round(variable2.rectValue.y, decimals);
					float width = (float)Math.Round(variable2.rectValue.width, decimals);
					float height = (float)Math.Round(variable2.rectValue.height, decimals);
					variable2.rectValue = new Rect(x, y, width, height);
					break;
				}
				}
			}
		}
	}
}
