using UnityEngine;

namespace GameFlow
{
	[Help("es", "Espera hasta que el programa (@GameFlow{Program}) especificado termine.", null)]
	[AddComponentMenu("")]
	[Help("en", "Waits for the specified @GameFlow{Program} to finish.", null)]
	public class WaitForProgram : TimeAction
	{
		[SerializeField]
		private Program _program;

		[SerializeField]
		private Variable _programVar;

		private Program _runningProgram;

		public Program program => _programVar.GetValue(_program) as Program;

		public override void FirstStep()
		{
			_runningProgram = program;
			if (_runningProgram != null && (!_runningProgram.running || _runningProgram == this.GetProgram()))
			{
				_runningProgram = null;
			}
		}

		public override bool Finished()
		{
			if (_runningProgram == null)
			{
				return true;
			}
			if (_runningProgram.finished)
			{
				_runningProgram = null;
				return true;
			}
			return false;
		}
	}
}
