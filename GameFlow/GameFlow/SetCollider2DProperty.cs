using UnityEngine;

namespace GameFlow
{
	[Help("en", "Sets a property of the specified @UnityManual{Collider2D}.", null)]
	[AddComponentMenu("")]
	[Help("es", "Modifica una propiedad del componente @UnityManual{Collider2D} especificado.", null)]
	public class SetCollider2DProperty : Action, IExecutableInEditor
	{
		public enum BaseProperty
		{
			Enabled,
			IsTrigger,
			Offset,
			SharedMaterial,
			UsedByEffector
		}

		public enum BoxProperty
		{
			Enabled,
			IsTrigger,
			Offset,
			SharedMaterial,
			UsedByEffector,
			Size
		}

		public enum CircleProperty
		{
			Enabled,
			IsTrigger,
			Offset,
			SharedMaterial,
			UsedByEffector,
			Radius
		}

		public enum EdgeProperty
		{
			Enabled,
			IsTrigger,
			Offset,
			SharedMaterial,
			UsedByEffector,
			EdgeCount,
			PointCount
		}

		public enum PolygonProperty
		{
			Enabled,
			IsTrigger,
			Offset,
			SharedMaterial,
			UsedByEffector,
			PathCount
		}

		[SerializeField]
		private Collider2D _target;

		[SerializeField]
		private Variable _targetVar;

		[SerializeField]
		private BaseProperty _baseProperty;

		[SerializeField]
		private BoxProperty _boxProperty;

		[SerializeField]
		private CircleProperty _circleProperty;

		[SerializeField]
		private EdgeProperty _edgeProperty;

		[SerializeField]
		private PolygonProperty _polygonProperty;

		[SerializeField]
		private Vector2 _vector2Value;

		[SerializeField]
		private float _floatValue;

		[SerializeField]
		private bool _boolValue;

		[SerializeField]
		private PhysicsMaterial2D _materialValue;

		[SerializeField]
		private Variable _valueVar;

		protected override void OnReset()
		{
			_target = base.gameObject.GetComponent<Collider2D>();
		}

		public override void Setup()
		{
		}

		public override void Execute()
		{
			Collider2D collider2D = _target;
			if (collider2D == null)
			{
				collider2D = (_targetVar.GetValueAsComponent(_target, typeof(Collider2D)) as Collider2D);
			}
			if (collider2D == null)
			{
				return;
			}
			BoxCollider2D boxCollider2D = collider2D as BoxCollider2D;
			if (boxCollider2D != null)
			{
				switch (_boxProperty)
				{
				case BoxProperty.Enabled:
					collider2D.enabled = _valueVar.GetValue(_boolValue);
					break;
				case BoxProperty.IsTrigger:
					collider2D.isTrigger = _valueVar.GetValue(_boolValue);
					break;
				case BoxProperty.Offset:
					collider2D.offset = _valueVar.GetValue(_vector2Value);
					break;
				case BoxProperty.SharedMaterial:
					collider2D.sharedMaterial = (_valueVar.GetValue(_materialValue) as PhysicsMaterial2D);
					break;
				case BoxProperty.UsedByEffector:
					collider2D.usedByEffector = _valueVar.GetValue(_boolValue);
					break;
				case BoxProperty.Size:
					boxCollider2D.size = _valueVar.GetValue(_vector2Value);
					break;
				}
				return;
			}
			CircleCollider2D circleCollider2D = collider2D as CircleCollider2D;
			if (circleCollider2D != null)
			{
				switch (_circleProperty)
				{
				case CircleProperty.Enabled:
					collider2D.enabled = _valueVar.GetValue(_boolValue);
					break;
				case CircleProperty.IsTrigger:
					collider2D.isTrigger = _valueVar.GetValue(_boolValue);
					break;
				case CircleProperty.Offset:
					collider2D.offset = _valueVar.GetValue(_vector2Value);
					break;
				case CircleProperty.SharedMaterial:
					collider2D.sharedMaterial = (_valueVar.GetValue(_materialValue) as PhysicsMaterial2D);
					break;
				case CircleProperty.UsedByEffector:
					collider2D.usedByEffector = _valueVar.GetValue(_boolValue);
					break;
				case CircleProperty.Radius:
					circleCollider2D.radius = _valueVar.GetValue(_floatValue);
					break;
				}
				return;
			}
			EdgeCollider2D x = collider2D as EdgeCollider2D;
			if (x != null)
			{
				switch (_edgeProperty)
				{
				case EdgeProperty.Enabled:
					collider2D.enabled = _valueVar.GetValue(_boolValue);
					break;
				case EdgeProperty.IsTrigger:
					collider2D.isTrigger = _valueVar.GetValue(_boolValue);
					break;
				case EdgeProperty.Offset:
					collider2D.offset = _valueVar.GetValue(_vector2Value);
					break;
				case EdgeProperty.SharedMaterial:
					collider2D.sharedMaterial = (_valueVar.GetValue(_materialValue) as PhysicsMaterial2D);
					break;
				case EdgeProperty.UsedByEffector:
					collider2D.usedByEffector = _valueVar.GetValue(_boolValue);
					break;
				}
				return;
			}
			PolygonCollider2D x2 = collider2D as PolygonCollider2D;
			if (x2 != null)
			{
				switch (_polygonProperty)
				{
				case PolygonProperty.Enabled:
					collider2D.enabled = _valueVar.GetValue(_boolValue);
					break;
				case PolygonProperty.IsTrigger:
					collider2D.isTrigger = _valueVar.GetValue(_boolValue);
					break;
				case PolygonProperty.Offset:
					collider2D.offset = _valueVar.GetValue(_vector2Value);
					break;
				case PolygonProperty.SharedMaterial:
					collider2D.sharedMaterial = (_valueVar.GetValue(_materialValue) as PhysicsMaterial2D);
					break;
				case PolygonProperty.UsedByEffector:
					collider2D.usedByEffector = _valueVar.GetValue(_boolValue);
					break;
				}
			}
		}
	}
}
