using UnityEngine;

namespace GameFlow
{
	[Help("en", "Copies the values of the specified Transform.", null)]
	[AddComponentMenu("")]
	[Help("es", "Copia los valores del Transform especificado.", null)]
	public class CopyTransform : TransformAction
	{
		[SerializeField]
		private Transform _source;

		[SerializeField]
		private Variable _sourceVar;

		[SerializeField]
		private bool _copyPosition = true;

		[SerializeField]
		private bool _copyRotation = true;

		[SerializeField]
		private bool _copyScale = true;

		public Transform source => _sourceVar.GetValueAsComponent(_source, typeof(Transform)) as Transform;

		public override void Execute()
		{
			Execute(source, base.target, _copyPosition, _copyRotation, _copyScale);
		}

		public static void Execute(Transform source, Transform target, bool copyPosition, bool copyRotation, bool copyScale)
		{
			if (!(source == null) && !(target == null) && !(source == target))
			{
				if (copyPosition)
				{
					target.position = source.position;
				}
				if (copyRotation)
				{
					target.rotation = source.rotation;
				}
				if (copyScale)
				{
					target.localScale = source.localScale;
				}
			}
		}
	}
}
