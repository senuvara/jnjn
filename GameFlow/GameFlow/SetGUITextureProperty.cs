using UnityEngine;

namespace GameFlow
{
	[Help("es", "Modifica el valor de una propiedad del component GUITexture especificado.", null)]
	[Help("en", "Modifies the value of a property of the specified GUITexture component.", null)]
	[AddComponentMenu("")]
	public class SetGUITextureProperty : Action, IExecutableInEditor
	{
		public enum Property
		{
			Border,
			Color,
			Enabled,
			PixelInset,
			ScreenRect,
			Texture
		}

		[SerializeField]
		private GUITexture _target;

		[SerializeField]
		private Variable _targetVar;

		[SerializeField]
		private Property _property = Property.Texture;

		[SerializeField]
		private RectOffset _borderValue;

		[SerializeField]
		private Rect _screenRectValue;

		[SerializeField]
		private Color _colorValue = Color.white;

		[SerializeField]
		private bool _enabledValue = true;

		[SerializeField]
		private Rect _pixelInsetValue;

		[SerializeField]
		private Texture _textureValue;

		[SerializeField]
		private Variable _valueVar;

		public GUITexture target => _targetVar.GetValueAsComponent(_target, typeof(GUITexture)) as GUITexture;

		protected override void OnReset()
		{
			GUITexture component = base.gameObject.GetComponent<GUITexture>();
			if (component != null)
			{
				_target = component;
				_borderValue = component.border;
				_colorValue = component.color;
				_enabledValue = component.enabled;
				_pixelInsetValue = component.pixelInset;
				_screenRectValue = component.GetScreenRect();
				_textureValue = component.texture;
			}
		}

		public override void Execute()
		{
			GUITexture target = this.target;
			if (!(target == null))
			{
				switch (_property)
				{
				case Property.Border:
					break;
				case Property.ScreenRect:
					break;
				case Property.Color:
					target.color = _valueVar.GetValue(_colorValue);
					break;
				case Property.Enabled:
					target.enabled = _valueVar.GetValue(_enabledValue);
					break;
				case Property.PixelInset:
					target.pixelInset = _valueVar.GetValue(_pixelInsetValue);
					break;
				case Property.Texture:
					target.texture = (_valueVar.GetValue(_textureValue) as Texture);
					break;
				}
			}
		}
	}
}
