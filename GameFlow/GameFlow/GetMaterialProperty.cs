using UnityEngine;

namespace GameFlow
{
	[Help("es", "Lee una propiedad del Material especificado.", null)]
	[Help("en", "Gets a property of the specified Material.", null)]
	[AddComponentMenu("")]
	public class GetMaterialProperty : Action, IExecutableInEditor
	{
		public enum Property
		{
			Color,
			GlobalIlluminationFlags,
			MainTexture,
			MainTextureOffset,
			MainTextureScale,
			NamedColor,
			NamedFloat,
			NamedInteger,
			NamedTag,
			NamedTexture,
			NamedTextureOffset,
			NamedTextureScale,
			NamedVector,
			PassCount,
			RenderQueue,
			Shader
		}

		[SerializeField]
		private Material _source;

		[SerializeField]
		private Variable _sourceVar;

		[SerializeField]
		private Property _property;

		[SerializeField]
		private string _textureName;

		[SerializeField]
		private Variable _textureNameVar;

		[SerializeField]
		private string _propertyName;

		[SerializeField]
		private Variable _propertyNameVar;

		[SerializeField]
		private Variable _output;

		public override void Execute()
		{
			Material material = _source;
			if (material == null)
			{
				material = (_sourceVar.GetValue(_source) as Material);
			}
			if (material == null)
			{
				return;
			}
			switch (_property)
			{
			case Property.Color:
				_output.SetValue(material.color);
				break;
			case Property.GlobalIlluminationFlags:
				_output.SetValue(material.globalIlluminationFlags);
				break;
			case Property.MainTexture:
				_output.SetValue(material.mainTexture);
				break;
			case Property.MainTextureOffset:
				_output.SetValue(material.mainTextureOffset);
				break;
			case Property.MainTextureScale:
				_output.SetValue(material.mainTextureScale);
				break;
			case Property.NamedTexture:
			case Property.NamedTextureOffset:
			case Property.NamedTextureScale:
			{
				string value2 = _textureNameVar.GetValue(_textureName);
				if (material.HasProperty(value2))
				{
					switch (_property)
					{
					case Property.NamedTexture:
						_output.SetValue(material.GetTexture(value2));
						break;
					case Property.NamedTextureOffset:
						_output.SetValue(material.GetTextureOffset(value2));
						break;
					case Property.NamedTextureScale:
						_output.SetValue(material.GetTextureScale(value2));
						break;
					}
				}
				break;
			}
			case Property.PassCount:
				_output.SetValue(material.passCount);
				break;
			case Property.RenderQueue:
				_output.SetValue(material.renderQueue);
				break;
			case Property.Shader:
				_output.SetValue(material.shader);
				break;
			case Property.NamedColor:
			case Property.NamedFloat:
			case Property.NamedInteger:
			case Property.NamedTag:
			case Property.NamedVector:
			{
				string value = _propertyNameVar.GetValue(_propertyName);
				if (value != null && value.Length != 0 && material.HasProperty(value))
				{
					switch (_property)
					{
					case Property.NamedTexture:
					case Property.NamedTextureOffset:
					case Property.NamedTextureScale:
						break;
					case Property.NamedColor:
						_output.SetValue(material.GetColor(value));
						break;
					case Property.NamedFloat:
						_output.SetValue(material.GetFloat(value));
						break;
					case Property.NamedInteger:
						_output.SetValue(material.GetInt(value));
						break;
					case Property.NamedTag:
						_output.SetValue(material.GetTag(value, searchFallbacks: true, string.Empty));
						break;
					case Property.NamedVector:
						_output.SetValue(material.GetVector(value));
						break;
					}
				}
				break;
			}
			}
		}
	}
}
