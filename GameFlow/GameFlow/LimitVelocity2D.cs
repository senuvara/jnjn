using UnityEngine;

namespace GameFlow
{
	[AddComponentMenu("")]
	[Help("en", "Limits the velocity of the specified 2D object.", null)]
	[Help("es", "Limita la velocidad del objeto 2D especificado.", null)]
	public class LimitVelocity2D : Action
	{
		[SerializeField]
		private Rigidbody2D _target;

		[SerializeField]
		private Variable _targetVar;

		[SerializeField]
		private float _min;

		[SerializeField]
		private Variable _minVar;

		[SerializeField]
		private float _max = 1f;

		[SerializeField]
		private Variable _maxVar;

		protected override void OnReset()
		{
			_target = GetComponent<Rigidbody2D>();
		}

		public override void Execute()
		{
			Rigidbody2D rb = _targetVar.GetValueAsComponent(_target, typeof(Rigidbody2D)) as Rigidbody2D;
			Execute(rb, _minVar.GetValue(_min), _maxVar.GetValue(_max));
		}

		public static void Execute(Rigidbody2D rb, float min, float max)
		{
			if (!(rb == null) && !Mathf.Approximately(rb.velocity.magnitude, 0f))
			{
				float d = Mathf.Clamp(rb.velocity.magnitude, min, max);
				rb.velocity = rb.velocity.normalized * d;
			}
		}
	}
}
