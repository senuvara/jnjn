using System;

namespace GameFlow
{
	public interface IEventDispatcher
	{
		void AddListener(IEventListener listener, Type eventType);

		void RemoveListener(IEventListener listener, Type eventType);
	}
}
