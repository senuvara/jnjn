namespace GameFlow
{
	public abstract class OnCollisionProgram2D : EventProgram
	{
		private Parameter _sourceParam;

		private Parameter _otherParam;

		private Parameter _pointParam;

		private Parameter _velocityParam;

		public Parameter sourceParam
		{
			get
			{
				if (_sourceParam == null)
				{
					_sourceParam = GetParameter("Source");
				}
				return _sourceParam;
			}
		}

		public Parameter otherParam
		{
			get
			{
				if (_otherParam == null)
				{
					_otherParam = GetParameter("Other");
				}
				return _otherParam;
			}
		}

		public Parameter pointParam
		{
			get
			{
				if (_pointParam == null)
				{
					_pointParam = GetParameter("Contact Point");
				}
				return _pointParam;
			}
		}

		public Parameter velocityParam
		{
			get
			{
				if (_velocityParam == null)
				{
					_velocityParam = GetParameter("Relative Velocity");
				}
				return _velocityParam;
			}
		}

		protected override void CacheParameters()
		{
			CacheParameter(sourceParam);
			CacheParameter(otherParam);
			CacheParameter(pointParam);
			CacheParameter(velocityParam);
		}

		protected override void SetParameters(EventObject e)
		{
			CollisionEvent collisionEvent = e as CollisionEvent;
			sourceParam.SetValue(collisionEvent.source);
			otherParam.SetValue(collisionEvent.other);
			pointParam.SetValue(collisionEvent.contactPoint);
			velocityParam.SetValue(collisionEvent.relativeVelocity);
		}
	}
}
