using System;
using UnityEngine;

namespace GameFlow
{
	public static class EnumUtils
	{
		private static Type[] _bitmaskTypes = new Type[2]
		{
			typeof(RigidbodyConstraints),
			typeof(RigidbodyConstraints2D)
		};

		private static string[] _RigidbodyConstraints = new string[7]
		{
			string.Empty,
			"Freeze Position X",
			"Freeze Position Y",
			"Freeze Position Z",
			"Freeze Rotation X",
			"Freeze Rotation Y",
			"Freeze Rotation Z"
		};

		private static string[] _RigidbodyConstraints2D = new string[4]
		{
			string.Empty,
			"Freeze Position X",
			"Freeze Position Y",
			"Freeze Rotation"
		};

		private static object[] _specialTypeNames = new object[2]
		{
			_RigidbodyConstraints,
			_RigidbodyConstraints2D
		};

		public static string[] EnumToStringArray(Type enumType, bool replaceUnderscore = false)
		{
			if (!enumType.IsEnum)
			{
				return null;
			}
			int num = ArrayUtils.IndexOf(_bitmaskTypes, enumType);
			if (num >= 0)
			{
				return (string[])_specialTypeNames[num];
			}
			string[] names = Enum.GetNames(enumType);
			for (int i = 0; i < names.Length; i++)
			{
				names[i] = names[i].SplitCamelCase();
				if (replaceUnderscore)
				{
					names[i] = names[i].Replace("_", " /");
				}
			}
			return names;
		}

		public static int[] GetEnumIntValues(Type enumType)
		{
			Array values = Enum.GetValues(enumType);
			int[] array = new int[values.Length];
			int num = 0;
			foreach (object item in values)
			{
				array[num++] = Convert.ToInt32(item);
			}
			return array;
		}

		public static bool IsMaskEnum(Type enumType)
		{
			if (enumType == null)
			{
				return false;
			}
			bool flag = enumType.GetCustomAttributes(typeof(FlagsAttribute), inherit: false).Length > 0;
			if (!flag)
			{
				flag = (ArrayUtils.IndexOf(_bitmaskTypes, enumType) >= 0);
			}
			return flag;
		}

		public static int IndexOfValue(Type enumType, Enum value)
		{
			int[] enumIntValues = GetEnumIntValues(enumType);
			return Array.IndexOf(enumIntValues, Convert.ToInt32(value));
		}
	}
}
