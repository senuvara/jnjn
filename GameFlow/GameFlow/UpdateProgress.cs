using UnityEngine;

namespace GameFlow
{
	[AddComponentMenu("")]
	[Help("es", "Actualiza el indicador de progreso de un @GameFlow{EditorProgram}.", null)]
	[Help("en", "Updates the current progress of a @GameFlow{EditorProgram}.", null)]
	public class UpdateProgress : Action, IExecutableInEditor
	{
		[SerializeField]
		private int _increment = 1;

		[SerializeField]
		private Variable _incrementVar;

		public override void Execute()
		{
			Progress.currentValue += _incrementVar.GetValue(_increment);
		}
	}
}
