using UnityEngine;

namespace GameFlow
{
	[Help("en", "Gets a position in the specified @GameFlow{Path} component.", null)]
	[Help("es", "Obtiene una posición del componente @GameFlow{Path} especificado.", null)]
	[AddComponentMenu("")]
	public class GetPositionInPath : Action, IExecutableInEditor
	{
		public enum Reference
		{
			Point,
			Distance,
			Time
		}

		[SerializeField]
		private Path _path;

		[SerializeField]
		private Variable _pathVar;

		[SerializeField]
		private Reference _reference;

		[SerializeField]
		private int _point;

		[SerializeField]
		private Variable _pointVar;

		[SerializeField]
		private float _distance;

		[SerializeField]
		private Variable _distanceVar;

		[SerializeField]
		private float _time;

		[SerializeField]
		private Variable _timeVar;

		[SerializeField]
		private float _duration = 1f;

		[SerializeField]
		private Variable _durationVar;

		[SerializeField]
		private Direction _direction;

		[SerializeField]
		private Variable _directionVar;

		[SerializeField]
		private Variable _output;

		public Path path => _pathVar.GetValueAsComponent(_path, typeof(Path)) as Path;

		protected Reference reference => _reference;

		protected int point => _pointVar.GetValue(_point);

		protected float distance => _distanceVar.GetValue(_distance);

		protected float time => _timeVar.GetValue(_time);

		protected float duration => _durationVar.GetValue(_duration);

		protected Direction direction => (Direction)(object)_directionVar.GetValue(_direction);

		protected override void OnReset()
		{
			_path = base.gameObject.GetComponent<Path>();
		}

		public override void Execute()
		{
			if (_output == null)
			{
				return;
			}
			Path path = this.path;
			if (path == null)
			{
				return;
			}
			switch (_reference)
			{
			case Reference.Point:
			{
				Transform transformAt = path.GetTransformAt(point);
				if (transformAt != null)
				{
					_output.SetValue(transformAt.position);
				}
				break;
			}
			case Reference.Distance:
				_output.SetValue(PathUtils.GetPositionAtDistance(path, distance, direction));
				break;
			case Reference.Time:
				_output.SetValue(PathUtils.GetPositionAtTime(path, time, duration, direction));
				break;
			}
		}
	}
}
