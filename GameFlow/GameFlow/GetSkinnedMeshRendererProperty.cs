using UnityEngine;

namespace GameFlow
{
	[Help("es", "Lee una propiedad del componente @UnityManual{SkinnedMeshRenderer} especificado.", null)]
	[Help("en", "Gets a property of the specified @UnityManual{SkinnedMeshRenderer} component.", null)]
	[AddComponentMenu("")]
	public class GetSkinnedMeshRendererProperty : Action, IExecutableInEditor
	{
		public enum Property
		{
			Bones,
			Bounds,
			Enabled,
			IsPartOfStaticBatch,
			IsVisible,
			LightmapIndex,
			LightmapScaleOffset,
			LocalBounds,
			LocalToWorldMatrix,
			Material,
			Materials,
			ProbeAnchor,
			Quality,
			RealtimeLightmapIndex,
			RealtimeLightmapScaleOffset,
			ReceiveShadows,
			ReflectionProbeUsage,
			RootBone,
			ShadowCastingMode,
			SharedMaterial,
			SharedMaterials,
			SharedMesh,
			SortingLayerID,
			SortingLayerName,
			SortingOrder,
			UpdateWhenOffscreen,
			LightProbeUsage,
			WorldToLocalMatrix
		}

		[SerializeField]
		private SkinnedMeshRenderer _source;

		[SerializeField]
		private Variable _sourceVar;

		[SerializeField]
		private Property _property;

		[SerializeField]
		private Variable _output;

		protected override void OnReset()
		{
			_source = base.gameObject.GetComponent<SkinnedMeshRenderer>();
		}

		public override void Setup()
		{
		}

		public override void Execute()
		{
			SkinnedMeshRenderer skinnedMeshRenderer = _sourceVar.GetValueAsComponent(_source, typeof(SkinnedMeshRenderer)) as SkinnedMeshRenderer;
			if (!(skinnedMeshRenderer == null))
			{
				switch (_property)
				{
				case Property.LocalToWorldMatrix:
				case Property.Materials:
				case Property.SharedMaterials:
					break;
				case Property.Bounds:
					_output.SetValue(skinnedMeshRenderer.bounds);
					break;
				case Property.Enabled:
					_output.SetValue(skinnedMeshRenderer.enabled);
					break;
				case Property.IsPartOfStaticBatch:
					_output.SetValue(skinnedMeshRenderer.isPartOfStaticBatch);
					break;
				case Property.IsVisible:
					_output.SetValue(skinnedMeshRenderer.isVisible);
					break;
				case Property.LightmapIndex:
					_output.SetValue(skinnedMeshRenderer.lightmapIndex);
					break;
				case Property.LightmapScaleOffset:
					_output.SetValue(skinnedMeshRenderer.lightmapScaleOffset);
					break;
				case Property.LocalBounds:
					_output.SetValue(skinnedMeshRenderer.localBounds);
					break;
				case Property.Material:
					_output.SetValue(skinnedMeshRenderer.material);
					break;
				case Property.ProbeAnchor:
					_output.SetValue(skinnedMeshRenderer.probeAnchor);
					break;
				case Property.Quality:
					_output.SetValue(skinnedMeshRenderer.quality);
					break;
				case Property.RealtimeLightmapIndex:
					_output.SetValue(skinnedMeshRenderer.realtimeLightmapIndex);
					break;
				case Property.RealtimeLightmapScaleOffset:
					_output.SetValue(skinnedMeshRenderer.realtimeLightmapScaleOffset);
					break;
				case Property.ReceiveShadows:
					_output.SetValue(skinnedMeshRenderer.receiveShadows);
					break;
				case Property.ReflectionProbeUsage:
					_output.SetValue(skinnedMeshRenderer.reflectionProbeUsage);
					break;
				case Property.RootBone:
					_output.SetValue(skinnedMeshRenderer.rootBone);
					break;
				case Property.ShadowCastingMode:
					_output.SetValue(skinnedMeshRenderer.shadowCastingMode);
					break;
				case Property.SharedMaterial:
					_output.SetValue(skinnedMeshRenderer.sharedMaterial);
					break;
				case Property.SharedMesh:
					_output.SetValue(skinnedMeshRenderer.sharedMesh);
					break;
				case Property.SortingLayerID:
					_output.SetValue(skinnedMeshRenderer.sortingLayerID);
					break;
				case Property.SortingLayerName:
					_output.SetValue(skinnedMeshRenderer.sortingLayerName);
					break;
				case Property.SortingOrder:
					_output.SetValue(skinnedMeshRenderer.sortingOrder);
					break;
				case Property.UpdateWhenOffscreen:
					_output.SetValue(skinnedMeshRenderer.updateWhenOffscreen);
					break;
				}
			}
		}
	}
}
