using UnityEngine;

namespace GameFlow
{
	public abstract class Condition : Block
	{
		[SerializeField]
		private Connective _connective;

		public Connective connective => _connective;

		private void Reset()
		{
			HideInInspector();
			Disable();
			SetContainer();
			SetResetFlag();
			OnReset();
		}

		private void SetContainer()
		{
			if (container != null || !BlockInsertion.enabled)
			{
				return;
			}
			IConditionContainer conditionContainer = BlockInsertion.target as IConditionContainer;
			if (conditionContainer != null)
			{
				if (BlockInsertion.index < 0 || BlockInsertion.index >= conditionContainer.GetConditions().Count)
				{
					conditionContainer.GetConditions().Add(this);
				}
				else
				{
					conditionContainer.GetConditions().Insert(BlockInsertion.index, this);
				}
				conditionContainer.ConditionAdded(this);
				conditionContainer.SetDirty(dirty: true);
				container = (conditionContainer as Object);
				BlockInsertion.Reset();
			}
		}

		public abstract bool Evaluate();

		public Program GetProgram()
		{
			Object container = base.container;
			while (container != null && container as Action != null)
			{
				container = (container as Action).container;
			}
			return container as Program;
		}
	}
}
