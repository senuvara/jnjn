using UnityEngine;

namespace GameFlow
{
	[Help("en", "Adds a force to the specified target @UnityManual{Rigidbody} component.", null)]
	[AddComponentMenu("")]
	[Help("es", "Añade una fuerza al componente @UnityManual{Rigidbody} especificado como objetivo.", null)]
	public class AddForce : Action
	{
		public enum ForceType
		{
			Force,
			Direction
		}

		[SerializeField]
		private Rigidbody _target;

		[SerializeField]
		private Variable _targetVar;

		[SerializeField]
		private ForceMode _mode;

		[SerializeField]
		private Variable _modeVar;

		[SerializeField]
		private ForceType _type;

		[SerializeField]
		private Vector3 _directionVector3;

		[SerializeField]
		private Force _directionForce;

		[SerializeField]
		private Variable _directionVar;

		[SerializeField]
		private bool _relative;

		[SerializeField]
		private Variable _relativeVar;

		[SerializeField]
		private float _multiplier = 1f;

		[SerializeField]
		private Variable _multiplierVar;

		public override void Execute()
		{
			Rigidbody rigidbody = _targetVar.GetValueAsComponent(_target, typeof(Rigidbody)) as Rigidbody;
			if (rigidbody == null)
			{
				return;
			}
			ForceMode mode = (ForceMode)(object)_modeVar.GetValue(_mode);
			bool value = _relativeVar.GetValue(_relative);
			float value2 = _multiplierVar.GetValue(_multiplier);
			switch (_type)
			{
			case ForceType.Direction:
				Execute(rigidbody, _directionVar.GetValue(_directionVector3), value2, mode, value);
				break;
			case ForceType.Force:
			{
				Force force = _directionVar.GetValue(_directionForce) as Force;
				if (!(force == null))
				{
					Execute(rigidbody, force.direction, force.magnitude * value2, mode, value);
				}
				break;
			}
			}
		}

		public static void Execute(Rigidbody rb, Vector3 direction, float magnitude, ForceMode mode, bool relative)
		{
			if (relative)
			{
				rb.AddRelativeForce(direction * magnitude, mode);
			}
			else
			{
				rb.AddForce(direction * magnitude, mode);
			}
		}
	}
}
