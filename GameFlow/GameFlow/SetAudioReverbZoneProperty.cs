using UnityEngine;

namespace GameFlow
{
	[Help("en", "Sets a property of the specified @UnityManual{AudioReverbZone} component.", null)]
	[AddComponentMenu("")]
	[Help("es", "Modifica una propiedad del componente @UnityManual{AudioReverbZone} especificado.", null)]
	public class SetAudioReverbZoneProperty : Action, IExecutableInEditor
	{
		public enum Property
		{
			DecayHFRatio,
			DecayTime,
			Density,
			Diffusion,
			HFReference,
			LFReference,
			MaxDistance,
			MinDistance,
			Reflections,
			ReflectionsDelay,
			Reverb,
			ReverbDelay,
			ReverbPreset,
			Room,
			RoomHF,
			RoomLF,
			RoomRolloffFactor
		}

		[SerializeField]
		private AudioReverbZone _target;

		[SerializeField]
		private Variable _targetVar;

		[SerializeField]
		private Property _property;

		[SerializeField]
		private float _decayHFRatioValue;

		[SerializeField]
		private float _decayTimeValue;

		[SerializeField]
		private float _densityValue;

		[SerializeField]
		private float _diffusionValue;

		[SerializeField]
		private float _HFReferenceValue;

		[SerializeField]
		private float _LFReferenceValue;

		[SerializeField]
		private float _maxDistanceValue;

		[SerializeField]
		private float _minDistanceValue;

		[SerializeField]
		private int _reflectionsValue;

		[SerializeField]
		private float _reflectionsDelayValue;

		[SerializeField]
		private int _reverbValue;

		[SerializeField]
		private float _reverbDelayValue;

		[SerializeField]
		private AudioReverbPreset _reverbPresetValue;

		[SerializeField]
		private int _roomValue;

		[SerializeField]
		private int _roomHFValue;

		[SerializeField]
		private int _roomLFValue;

		[SerializeField]
		private float _roomRolloffFactorValue;

		[SerializeField]
		private Variable _valueVar;

		protected override void OnReset()
		{
			_target = base.gameObject.GetComponent<AudioReverbZone>();
		}

		public override void Setup()
		{
		}

		public override void Execute()
		{
			AudioReverbZone audioReverbZone = _targetVar.GetValueAsComponent(_target, typeof(AudioReverbZone)) as AudioReverbZone;
			if (!(audioReverbZone == null))
			{
				switch (_property)
				{
				case Property.DecayHFRatio:
					audioReverbZone.decayHFRatio = _valueVar.GetValue(_decayHFRatioValue);
					break;
				case Property.DecayTime:
					audioReverbZone.decayTime = _valueVar.GetValue(_decayTimeValue);
					break;
				case Property.Density:
					audioReverbZone.density = _valueVar.GetValue(_densityValue);
					break;
				case Property.Diffusion:
					audioReverbZone.diffusion = _valueVar.GetValue(_diffusionValue);
					break;
				case Property.HFReference:
					audioReverbZone.HFReference = _valueVar.GetValue(_HFReferenceValue);
					break;
				case Property.LFReference:
					audioReverbZone.LFReference = _valueVar.GetValue(_LFReferenceValue);
					break;
				case Property.MaxDistance:
					audioReverbZone.maxDistance = _valueVar.GetValue(_maxDistanceValue);
					break;
				case Property.MinDistance:
					audioReverbZone.minDistance = _valueVar.GetValue(_minDistanceValue);
					break;
				case Property.Reflections:
					audioReverbZone.reflections = _valueVar.GetValue(_reflectionsValue);
					break;
				case Property.ReflectionsDelay:
					audioReverbZone.reflectionsDelay = _valueVar.GetValue(_reflectionsDelayValue);
					break;
				case Property.Reverb:
					audioReverbZone.reverb = _valueVar.GetValue(_reverbValue);
					break;
				case Property.ReverbDelay:
					audioReverbZone.reverbDelay = _valueVar.GetValue(_reverbDelayValue);
					break;
				case Property.ReverbPreset:
					audioReverbZone.reverbPreset = (AudioReverbPreset)(object)_valueVar.GetValue(_reverbPresetValue);
					break;
				case Property.Room:
					audioReverbZone.room = _valueVar.GetValue(_roomValue);
					break;
				case Property.RoomHF:
					audioReverbZone.roomHF = _valueVar.GetValue(_roomHFValue);
					break;
				case Property.RoomLF:
					audioReverbZone.roomLF = _valueVar.GetValue(_roomLFValue);
					break;
				case Property.RoomRolloffFactor:
					audioReverbZone.roomRolloffFactor = _valueVar.GetValue(_roomRolloffFactorValue);
					break;
				}
			}
		}
	}
}
