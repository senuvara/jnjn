using UnityEngine;

namespace GameFlow
{
	[Help("en", "Activates the objects contained in the specified @GameFlow{List}.", null)]
	[AddComponentMenu("")]
	[Help("es", "Activa los objetos contenidos en la @GameFlow{List} especificada.", null)]
	public class ActivateGameObjectsInList : ListAction, IExecutableInEditor
	{
		public override void Setup()
		{
			OnActivate.RegisterAllInactiveAsListeners();
		}

		public override void Execute()
		{
			if (!(base.list != null) || base.list.dataType != DataType.Object)
			{
				return;
			}
			for (int i = 0; i < base.list.Count; i++)
			{
				GameObject gameObject = base.list.GetObjectAt(i) as GameObject;
				if (gameObject != null)
				{
					gameObject.SetActive(value: true);
				}
			}
		}
	}
}
