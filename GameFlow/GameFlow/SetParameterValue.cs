using UnityEngine;

namespace GameFlow
{
	[Help("es", "Modifica el valor del @GameFlow{Parameter} especificado.", null)]
	[AddComponentMenu("")]
	[Help("en", "Sets the value of the specified @GameFlow{Parameter}.", null)]
	public class SetParameterValue : ParameterAction
	{
		[SerializeField]
		private string _format;

		[SerializeField]
		private Variable _formatVar;

		public string format => _formatVar.GetValue(_format);

		public override void Execute()
		{
			Execute(base.parameter, this, base.gameObject, format);
		}

		public static void Execute(Parameter parameter, ValueAction action, GameObject context = null, string format = "")
		{
			SetVariableValue.Execute(parameter, action, context, format);
		}
	}
}
