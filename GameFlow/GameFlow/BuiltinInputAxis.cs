using System;
using UnityEngine;

namespace GameFlow
{
	[Help("es", "Una @GameFlow{Variable} integrada que devuelve el valor de los ejes virtuales de entrada, tanto horizontal como vertical.", null)]
	[Help("en", "A built-in @GameFlow{Variable} that returns the current value of the virtual input axis, both horizontal and vertical.", null)]
	[AddComponentMenu("")]
	public class BuiltinInputAxis : BuiltinVariable
	{
		private static Vector2 axis = Vector2.zero;

		public override Vector2 vector2Value
		{
			get
			{
				if (Application.isPlaying)
				{
					axis.x = Input.GetAxis("Horizontal");
					axis.y = Input.GetAxis("Vertical");
				}
				return axis;
			}
		}

		public override Type GetVariableType()
		{
			return typeof(Vector2);
		}

		protected override string GetVariableId()
		{
			return "Input Axis";
		}
	}
}
