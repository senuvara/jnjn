using System;
using UnityEngine;

namespace GameFlow
{
	[AddComponentMenu("")]
	[Help("en", "A built-in @GameFlow{Variable} that returns the position of the screen center in Screen coords.", null)]
	[Help("es", "Una @GameFlow{Variable} integrada que devuelve la posición del centro de la pantalla en coordenadas de pantalla.", null)]
	public class BuiltinScreenCenter : BuiltinVariable
	{
		public override Vector2 vector2Value => new Vector2((float)Screen.width / 2f, (float)Screen.height / 2f);

		public override Type GetVariableType()
		{
			return typeof(Vector2);
		}

		protected override string GetVariableId()
		{
			return "Screen Center";
		}
	}
}
