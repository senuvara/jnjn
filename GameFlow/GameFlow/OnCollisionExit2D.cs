using UnityEngine;

namespace GameFlow
{
	[Help("es", "Programa que se ejecutará una vez tan pronto como un Collider 2D en el rango de escucha deje de colisionar con otro componente.", null)]
	[DisallowMultipleComponent]
	[AddComponentMenu("GameFlow/Programs/On Collision Exit 2D")]
	[Help("en", "Program that will be executed one time as soon as a Collider 2D component in the listening range is no longer colliding with another component.", null)]
	public class OnCollisionExit2D : OnCollisionProgram2D
	{
		private EventController[] _controllers;

		protected override void RegisterAsListener()
		{
			_controllers = RegisterAsListener(typeof(CollisionExitEvent), typeof(CollisionController2D), CollisionController2D.AddController);
			if (base.listeningRange == EventListeningRange.SceneHierarchy)
			{
				CollisionExitEvent.AddListener(this);
			}
		}

		protected override void UnRegisterAsListener()
		{
			CollisionExitEvent.RemoveListener(this);
			UnRegisterAsListener(typeof(CollisionExitEvent), typeof(CollisionController2D));
		}

		private void OnTransformChildrenChanged()
		{
			if (base.listeningRange == EventListeningRange.GameObjectHierarchy)
			{
				UnsubscribeFrom(_controllers, typeof(CollisionExitEvent));
				_controllers = EventController.GetControllersInHierarchyOf(typeof(CollisionController2D), base.listeningTarget, CollisionController2D.AddController);
				SubscribeTo(_controllers, typeof(CollisionExitEvent));
			}
		}
	}
}
