using UnityEngine;

namespace GameFlow
{
	[Help("en", "Gets the tag of the specified @UnityManual{GameObject}.", null)]
	[AddComponentMenu("")]
	[Help("es", "Devuelve el tag del @UnityManual{GameObject} especificado.", null)]
	public class GetTag : Action, IExecutableInEditor
	{
		[SerializeField]
		private GameObject _source;

		[SerializeField]
		private Variable _sourceVar;

		[SerializeField]
		private Variable _output;

		public GameObject source => _sourceVar.GetValue(_source) as GameObject;

		public Variable output => _output;

		protected override void OnReset()
		{
			_source = base.gameObject;
		}

		public override void Execute()
		{
			if (!(_output == null))
			{
				GameObject source = this.source;
				if (!(source == null))
				{
					_output.SetTagValue(source.tag);
				}
			}
		}
	}
}
