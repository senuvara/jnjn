using UnityEngine;

namespace GameFlow
{
	[AddComponentMenu("")]
	[Help("en", "Scales the specified object to match the given target scale.", null)]
	[Help("es", "Escala el objeto especificado hasta que alcance la escala indicada.", null)]
	public class Scale : Interpolate
	{
		protected override void OnReset()
		{
			base.OnReset();
			base.interpolation = Interpolation.Scale;
			base.scaleTargetType = TargetType.Transform;
		}

		public override void Step()
		{
			float elapsedTimeDelta = base.timer.elapsedTimeDelta;
			if (_t1 == null)
			{
				_finished = true;
				return;
			}
			switch (base.scaleTargetType)
			{
			case TargetType.Vector3:
				_t1.localScale = MathUtils.Ease(startScale, toScale, elapsedTimeDelta, base.easingType);
				break;
			case TargetType.Transform:
				if (_t2 == null)
				{
					_finished = true;
				}
				else
				{
					_t1.localScale = MathUtils.Ease(startScale, _t2.localScale, elapsedTimeDelta, base.easingType);
				}
				break;
			}
		}
	}
}
