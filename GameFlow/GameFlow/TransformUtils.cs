using UnityEngine;

namespace GameFlow
{
	public static class TransformUtils
	{
		public enum Direction
		{
			Forward,
			Backward,
			Up,
			Down,
			Left,
			Right
		}

		public static string[] directions = new string[6]
		{
			"Forward",
			"Backward",
			"Up",
			"Down",
			"Left",
			"Right"
		};

		public static Bounds CalculateBounds(this Transform transform)
		{
			Bounds bounds = new Bounds(Vector3.zero, transform.lossyScale);
			Vector3[] array = new Vector3[8]
			{
				transform.TransformDirection(bounds.min),
				default(Vector3),
				default(Vector3),
				default(Vector3),
				default(Vector3),
				default(Vector3),
				default(Vector3),
				default(Vector3)
			};
			ref Vector3 reference = ref array[1];
			Vector3 min = bounds.min;
			float x = min.x;
			Vector3 min2 = bounds.min;
			float y = min2.y;
			Vector3 max = bounds.max;
			reference = transform.TransformDirection(new Vector3(x, y, max.z));
			ref Vector3 reference2 = ref array[2];
			Vector3 min3 = bounds.min;
			float x2 = min3.x;
			Vector3 max2 = bounds.max;
			float y2 = max2.y;
			Vector3 min4 = bounds.min;
			reference2 = transform.TransformDirection(new Vector3(x2, y2, min4.z));
			ref Vector3 reference3 = ref array[3];
			Vector3 max3 = bounds.max;
			float x3 = max3.x;
			Vector3 min5 = bounds.min;
			float y3 = min5.y;
			Vector3 min6 = bounds.min;
			reference3 = transform.TransformDirection(new Vector3(x3, y3, min6.z));
			ref Vector3 reference4 = ref array[4];
			Vector3 max4 = bounds.max;
			float x4 = max4.x;
			Vector3 max5 = bounds.max;
			float y4 = max5.y;
			Vector3 min7 = bounds.min;
			reference4 = transform.TransformDirection(new Vector3(x4, y4, min7.z));
			ref Vector3 reference5 = ref array[5];
			Vector3 max6 = bounds.max;
			float x5 = max6.x;
			Vector3 min8 = bounds.min;
			float y5 = min8.y;
			Vector3 max7 = bounds.max;
			reference5 = transform.TransformDirection(new Vector3(x5, y5, max7.z));
			ref Vector3 reference6 = ref array[6];
			Vector3 min9 = bounds.min;
			float x6 = min9.x;
			Vector3 max8 = bounds.max;
			float y6 = max8.y;
			Vector3 max9 = bounds.max;
			reference6 = transform.TransformDirection(new Vector3(x6, y6, max9.z));
			array[7] = transform.TransformDirection(bounds.max);
			Vector3 b = new Vector3(array[0].x, array[0].y, array[0].z);
			Vector3 a = new Vector3(array[7].x, array[7].y, array[7].z);
			for (int i = 0; i < 8; i++)
			{
				b.x = Mathf.Min(b.x, array[i].x);
				b.y = Mathf.Min(b.y, array[i].y);
				b.z = Mathf.Min(b.z, array[i].z);
				a.x = Mathf.Max(a.x, array[i].x);
				a.y = Mathf.Max(a.y, array[i].y);
				a.z = Mathf.Max(a.z, array[i].z);
			}
			return new Bounds(transform.position, a - b);
		}

		public static Vector3 GetDirection(this Transform transform, Direction direction)
		{
			switch (direction)
			{
			case Direction.Forward:
				return transform.forward;
			case Direction.Backward:
				return transform.forward.Multiply(new Vector3(1f, 1f, -1f));
			case Direction.Up:
				return transform.up;
			case Direction.Down:
				return transform.up.Multiply(new Vector3(1f, -1f, 1f));
			case Direction.Left:
				return transform.right.Multiply(new Vector3(-1f, 1f, 1f));
			case Direction.Right:
				return transform.right;
			default:
				return Vector3.zero;
			}
		}

		public static Vector3 GetAxisDirection(Axis axis, Transform transform = null)
		{
			Vector3 vector = Vector3.zero;
			switch (axis)
			{
			case Axis.X:
				vector = Vector3.right;
				break;
			case Axis.Y:
				vector = Vector3.up;
				break;
			case Axis.Z:
				vector = Vector3.forward;
				break;
			}
			if (transform != null)
			{
				vector = transform.TransformDirection(vector);
			}
			return vector;
		}
	}
}
