using UnityEngine;

namespace GameFlow
{
	[AddComponentMenu("")]
	[Help("en", "Gets a property of the specified @UnityManual{Sprite}.", null)]
	[Help("es", "Lee una propiedad del @UnityManual{Sprite} especificado.", null)]
	public class GetSpriteProperty : Action, IExecutableInEditor
	{
		public enum Property
		{
			Border,
			Bounds,
			Packed,
			PackingMode,
			PackingRotation,
			Pivot,
			PixelsPerUnit,
			Rect,
			Texture,
			TextureRect,
			TextureRectOffset,
			Triangles,
			Uv,
			Vertices
		}

		[SerializeField]
		private Sprite _source;

		[SerializeField]
		private Variable _sourceVar;

		[SerializeField]
		private Property _property;

		[SerializeField]
		private Variable _output;

		protected override void OnReset()
		{
		}

		public override void Setup()
		{
		}

		public override void Execute()
		{
			Sprite sprite = _sourceVar.GetValue(_source) as Sprite;
			if (!(sprite == null))
			{
				switch (_property)
				{
				case Property.Border:
					_output.SetValue(sprite.border);
					break;
				case Property.Bounds:
					_output.SetValue(sprite.bounds);
					break;
				case Property.Packed:
					_output.SetValue(sprite.packed);
					break;
				case Property.PackingMode:
					_output.SetValue(sprite.packingMode);
					break;
				case Property.PackingRotation:
					_output.SetValue(sprite.packingRotation);
					break;
				case Property.Pivot:
					_output.SetValue(sprite.pivot);
					break;
				case Property.PixelsPerUnit:
					_output.SetValue(sprite.pixelsPerUnit);
					break;
				case Property.Rect:
					_output.SetValue(sprite.rect);
					break;
				case Property.Texture:
					_output.SetValue(sprite.texture);
					break;
				case Property.TextureRect:
					_output.SetValue(sprite.textureRect);
					break;
				case Property.TextureRectOffset:
					_output.SetValue(sprite.textureRectOffset);
					break;
				}
			}
		}
	}
}
