using UnityEngine;
using UnityEngine.UI;

namespace GameFlow
{
	[Help("en", "Gets a property of the specified Text component.", null)]
	[AddComponentMenu("")]
	[Help("es", "Lee una propiedad del componente Text especificado.", null)]
	public class GetTextProperty : Action, IExecutableInEditor
	{
		public enum Property
		{
			Alignment,
			Canvas,
			CanvasRenderer,
			Color,
			DefaultMaterial,
			Depth,
			FlexibleHeight,
			FlexibleWidth,
			Font,
			FontSize,
			FontStyle,
			HorizontalOverflow,
			LayoutPriority,
			LineSpacing,
			MainTexture,
			Maskable,
			Material,
			MaterialForRendering,
			MinHeight,
			MinWidth,
			PixelsPerUnit,
			PreferredHeight,
			PreferredWidth,
			RectTransform,
			ResizeTextForBestFit,
			ResizeTextMaxSize,
			ResizeTextMinSize,
			SupportRichText,
			Text,
			VerticalOverflow
		}

		[SerializeField]
		private Text _source;

		[SerializeField]
		private Variable _sourceVar;

		[SerializeField]
		private Property _property = Property.Text;

		[SerializeField]
		private Variable _output;

		protected override void OnReset()
		{
			_source = base.gameObject.GetComponent<Text>();
		}

		public override void Setup()
		{
		}

		public override void Execute()
		{
			Text text = _sourceVar.GetValueAsComponent(_source, typeof(Text)) as Text;
			if (!(text == null))
			{
				switch (_property)
				{
				case Property.Alignment:
					_output.SetValue(text.alignment);
					break;
				case Property.Canvas:
					_output.SetValue(text.canvas);
					break;
				case Property.CanvasRenderer:
					_output.SetValue(text.canvasRenderer);
					break;
				case Property.Color:
					_output.SetValue(text.color);
					break;
				case Property.DefaultMaterial:
					_output.SetValue(text.defaultMaterial);
					break;
				case Property.Depth:
					_output.SetValue(text.depth);
					break;
				case Property.FlexibleHeight:
					_output.SetValue(text.flexibleHeight);
					break;
				case Property.FlexibleWidth:
					_output.SetValue(text.flexibleWidth);
					break;
				case Property.Font:
					_output.SetValue(text.font);
					break;
				case Property.FontSize:
					_output.SetValue(text.fontSize);
					break;
				case Property.FontStyle:
					_output.SetValue(text.fontStyle);
					break;
				case Property.HorizontalOverflow:
					_output.SetValue(text.horizontalOverflow);
					break;
				case Property.LayoutPriority:
					_output.SetValue(text.layoutPriority);
					break;
				case Property.LineSpacing:
					_output.SetValue(text.lineSpacing);
					break;
				case Property.MainTexture:
					_output.SetValue(text.mainTexture);
					break;
				case Property.Maskable:
					_output.SetValue(text.maskable);
					break;
				case Property.Material:
					_output.SetValue(text.material);
					break;
				case Property.MaterialForRendering:
					_output.SetValue(text.materialForRendering);
					break;
				case Property.MinHeight:
					_output.SetValue(text.minHeight);
					break;
				case Property.MinWidth:
					_output.SetValue(text.minWidth);
					break;
				case Property.PixelsPerUnit:
					_output.SetValue(text.pixelsPerUnit);
					break;
				case Property.PreferredHeight:
					_output.SetValue(text.preferredHeight);
					break;
				case Property.PreferredWidth:
					_output.SetValue(text.preferredWidth);
					break;
				case Property.RectTransform:
					_output.SetValue(text.rectTransform);
					break;
				case Property.ResizeTextForBestFit:
					_output.SetValue(text.resizeTextForBestFit);
					break;
				case Property.ResizeTextMaxSize:
					_output.SetValue(text.resizeTextMaxSize);
					break;
				case Property.ResizeTextMinSize:
					_output.SetValue(text.resizeTextMinSize);
					break;
				case Property.SupportRichText:
					_output.SetValue(text.supportRichText);
					break;
				case Property.Text:
					_output.SetValue(text.text);
					break;
				case Property.VerticalOverflow:
					_output.SetValue(text.verticalOverflow);
					break;
				}
			}
		}
	}
}
