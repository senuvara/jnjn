using UnityEngine;

namespace GameFlow
{
	[AddComponentMenu("")]
	[Help("en", "Sets the value of a component of the rotation of the specified Transform.", null)]
	[Help("es", "Modifica el valor de una componente de la rotación del Transform especificado.", null)]
	public class SetRotationComponent : TransformAction
	{
		[SerializeField]
		private Vector3Component _component;

		[SerializeField]
		private Variable _componentVar;

		[SerializeField]
		private float _value = 1f;

		[SerializeField]
		private Variable _valueVar;

		public Vector3Component component => (Vector3Component)(object)_componentVar.GetValue(_component);

		public float value => _valueVar.GetValue(_value);

		public sealed override void Execute()
		{
			Execute(base.target, base.space, component, value, base.relative, base.multiplier);
		}

		public static void Execute(Transform t, Space space, Vector3Component component, float value, bool relative, float multiplier)
		{
			if (t == null)
			{
				return;
			}
			switch (space)
			{
			case Space.World:
			{
				Vector3 eulerAngles = t.eulerAngles;
				switch (component)
				{
				case Vector3Component.X:
					eulerAngles.x = (relative ? (eulerAngles.x + value * multiplier) : value);
					break;
				case Vector3Component.Y:
					eulerAngles.y = (relative ? (eulerAngles.y + value * multiplier) : value);
					break;
				case Vector3Component.Z:
					eulerAngles.z = (relative ? (eulerAngles.z + value * multiplier) : value);
					break;
				}
				t.eulerAngles = eulerAngles;
				break;
			}
			case Space.Self:
			{
				Vector3 localEulerAngles = t.localEulerAngles;
				switch (component)
				{
				case Vector3Component.X:
					localEulerAngles.x = (relative ? (localEulerAngles.x + value * multiplier) : value);
					break;
				case Vector3Component.Y:
					localEulerAngles.y = (relative ? (localEulerAngles.y + value * multiplier) : value);
					break;
				case Vector3Component.Z:
					localEulerAngles.z = (relative ? (localEulerAngles.z + value * multiplier) : value);
					break;
				}
				t.localEulerAngles = localEulerAngles;
				break;
			}
			}
		}
	}
}
