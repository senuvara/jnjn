using System;
using UnityEngine;

namespace GameFlow
{
	[Help("en", "Adds an item to the specified @GameFlow{List}.", null)]
	[Help("es", "Añade un elemento a la lista (@GameFlow{List}) especificada.", null)]
	[AddComponentMenu("")]
	public class AddItemToList : ListAction
	{
		public override void Execute()
		{
			List list = GetList();
			if (!(list == null))
			{
				switch (list.dataType)
				{
				case DataType.String:
				case DataType.Tag:
					list.AddValue(base.stringValue);
					break;
				case DataType.Integer:
				case DataType.Layer:
					list.AddValue(base.intValue);
					break;
				case DataType.Float:
					list.AddValue(base.floatValue);
					break;
				case DataType.Boolean:
				case DataType.Toggle:
					list.AddValue(base.boolValue);
					break;
				case DataType.Vector2:
					list.AddValue(base.vector2Value);
					break;
				case DataType.Vector3:
					list.AddValue(base.vector3Value);
					break;
				case DataType.Vector4:
					list.AddValue(base.vector4Value);
					break;
				case DataType.Rect:
					list.AddValue(base.rectValue);
					break;
				case DataType.Color:
					list.AddValue(base.colorValue);
					break;
				case DataType.Object:
					list.AddValue(base.objectValue);
					break;
				case DataType.Enum:
					list.AddValue((Enum)Enum.ToObject(list.type, base.enumIntValue));
					break;
				case DataType.AnimationCurve:
					list.AddValue(base.animationCurveValue);
					break;
				case DataType.Bounds:
					list.AddValue(base.boundsValue);
					break;
				case DataType.Quaternion:
					list.AddValue(base.quaternionValue);
					break;
				}
			}
		}
	}
}
