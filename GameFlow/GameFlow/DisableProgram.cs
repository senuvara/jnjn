using UnityEngine;

namespace GameFlow
{
	[AddComponentMenu("")]
	[Help("es", "Desactiva el programa (@GameFlow{Program}) especificado.", null)]
	[Help("en", "Disables the specified @GameFlow{Program}.", null)]
	public class DisableProgram : Action, IExecutableInEditor
	{
		[SerializeField]
		private Program _target;

		[SerializeField]
		private Variable _targetVar;

		public override void Execute()
		{
			Execute(_targetVar.GetValue(_target) as Program);
		}

		public static void Execute(Program program)
		{
			if (program != null)
			{
				program.enabled = false;
			}
		}
	}
}
