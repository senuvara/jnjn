using UnityEngine;

namespace GameFlow
{
	[Help("es", "Pone en reposo el objeto especificado.", null)]
	[Help("en", "Puts to sleep the specified object.", null)]
	[AddComponentMenu("")]
	public class Sleep : Action
	{
		[SerializeField]
		private Rigidbody _target;

		[SerializeField]
		private GameObject _targetObject;

		[SerializeField]
		private Variable _targetVar;

		protected override void OnReset()
		{
		}

		private void ExecutePre0_9_4()
		{
			Rigidbody rigidbody = _target;
			if (rigidbody == null)
			{
				rigidbody = (_targetVar.GetValueAsComponent(_target, typeof(Rigidbody)) as Rigidbody);
			}
			Execute(rigidbody);
		}

		public override void Execute()
		{
			int num = VersionUtils.VersionStringToInt(GetVersionLabel());
			if (num < VersionUtils.VersionStringToInt("0.9.4b"))
			{
				ExecutePre0_9_4();
				return;
			}
			Rigidbody rigidbody = _targetVar.GetValueAsComponent(_targetObject, typeof(Rigidbody)) as Rigidbody;
			if (rigidbody != null)
			{
				Execute(rigidbody);
				return;
			}
			Rigidbody2D rigidbody2D = _targetVar.GetValueAsComponent(_targetObject, typeof(Rigidbody2D)) as Rigidbody2D;
			if (rigidbody2D != null)
			{
				Execute(rigidbody2D);
			}
		}

		public static void Execute(Rigidbody rb)
		{
			if (rb != null)
			{
				rb.Sleep();
			}
		}

		public static void Execute(Rigidbody2D rb)
		{
			if (rb != null)
			{
				rb.Sleep();
			}
		}
	}
}
