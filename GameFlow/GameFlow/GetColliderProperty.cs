using UnityEngine;

namespace GameFlow
{
	[Help("es", "Lee una propiedad del componente @UnityManual{Collider} especificado.", null)]
	[Help("en", "Gets a property of the specified @UnityManual{Collider} component.", null)]
	[AddComponentMenu("")]
	public class GetColliderProperty : Action, IExecutableInEditor
	{
		public enum ColliderProperty
		{
			AttachedRigidbody,
			Bounds,
			Enabled,
			IsTrigger,
			Material,
			SharedMaterial,
			ContactOffset
		}

		public enum BoxColliderProperty
		{
			AttachedRigidbody,
			Bounds,
			Center,
			Enabled,
			IsTrigger,
			Material,
			SharedMaterial,
			Size,
			ContactOffset
		}

		public enum SphereColliderProperty
		{
			AttachedRigidbody,
			Bounds,
			Center,
			Enabled,
			IsTrigger,
			Material,
			Radius,
			SharedMaterial,
			ContactOffset
		}

		public enum CapsuleColliderProperty
		{
			AttachedRigidbody,
			Bounds,
			Center,
			Direction,
			Enabled,
			Height,
			IsTrigger,
			Material,
			Radius,
			SharedMaterial,
			ContactOffset
		}

		public enum MeshColliderProperty
		{
			AttachedRigidbody,
			Bounds,
			Convex,
			Enabled,
			IsTrigger,
			Material,
			SharedMaterial,
			SharedMesh,
			ContactOffset
		}

		[SerializeField]
		private Collider _source;

		[SerializeField]
		private Variable _sourceVar;

		[SerializeField]
		private ColliderProperty _baseProperty;

		[SerializeField]
		private BoxColliderProperty _boxProperty;

		[SerializeField]
		private SphereColliderProperty _sphereProperty;

		[SerializeField]
		private CapsuleColliderProperty _capsuleProperty;

		[SerializeField]
		private MeshColliderProperty _meshProperty;

		[SerializeField]
		private Variable _output;

		protected override void OnReset()
		{
			_source = base.gameObject.GetComponent<Collider>();
		}

		public override void Execute()
		{
			Collider collider = _source;
			if (collider == null)
			{
				collider = (_sourceVar.GetValueAsComponent(_source, typeof(Collider)) as Collider);
			}
			if (collider == null)
			{
				return;
			}
			BoxCollider boxCollider = collider as BoxCollider;
			if (boxCollider != null)
			{
				switch (_boxProperty)
				{
				case BoxColliderProperty.AttachedRigidbody:
					_output.SetValue(collider.attachedRigidbody);
					break;
				case BoxColliderProperty.Bounds:
					_output.SetValue(collider.bounds);
					break;
				case BoxColliderProperty.Center:
					_output.SetValue(boxCollider.center);
					break;
				case BoxColliderProperty.Enabled:
					_output.SetToggleValue(collider.enabled);
					break;
				case BoxColliderProperty.IsTrigger:
					_output.SetToggleValue(collider.isTrigger);
					break;
				case BoxColliderProperty.Material:
					_output.SetValue(collider.material);
					break;
				case BoxColliderProperty.SharedMaterial:
					_output.SetValue(collider.sharedMaterial);
					break;
				case BoxColliderProperty.Size:
					_output.SetValue(boxCollider.size);
					break;
				case BoxColliderProperty.ContactOffset:
					_output.SetValue(boxCollider.contactOffset);
					break;
				}
				return;
			}
			SphereCollider sphereCollider = collider as SphereCollider;
			if (sphereCollider != null)
			{
				switch (_sphereProperty)
				{
				case SphereColliderProperty.AttachedRigidbody:
					_output.SetValue(collider.attachedRigidbody);
					break;
				case SphereColliderProperty.Bounds:
					_output.SetValue(collider.bounds);
					break;
				case SphereColliderProperty.Center:
					_output.SetValue(boxCollider.center);
					break;
				case SphereColliderProperty.Enabled:
					_output.SetToggleValue(collider.enabled);
					break;
				case SphereColliderProperty.IsTrigger:
					_output.SetToggleValue(collider.isTrigger);
					break;
				case SphereColliderProperty.Material:
					_output.SetValue(collider.material);
					break;
				case SphereColliderProperty.Radius:
					_output.SetValue(sphereCollider.radius);
					break;
				case SphereColliderProperty.SharedMaterial:
					_output.SetValue(collider.sharedMaterial);
					break;
				case SphereColliderProperty.ContactOffset:
					_output.SetValue(sphereCollider.contactOffset);
					break;
				}
				return;
			}
			CapsuleCollider capsuleCollider = collider as CapsuleCollider;
			if (capsuleCollider != null)
			{
				switch (_capsuleProperty)
				{
				case CapsuleColliderProperty.AttachedRigidbody:
					_output.SetValue(collider.attachedRigidbody);
					break;
				case CapsuleColliderProperty.Bounds:
					_output.SetValue(collider.bounds);
					break;
				case CapsuleColliderProperty.Center:
					_output.SetValue(capsuleCollider.center);
					break;
				case CapsuleColliderProperty.Direction:
					_output.SetValue((Axis)capsuleCollider.direction);
					break;
				case CapsuleColliderProperty.Enabled:
					_output.SetToggleValue(collider.enabled);
					break;
				case CapsuleColliderProperty.Height:
					_output.SetValue(capsuleCollider.height);
					break;
				case CapsuleColliderProperty.IsTrigger:
					_output.SetToggleValue(collider.isTrigger);
					break;
				case CapsuleColliderProperty.Material:
					_output.SetValue(collider.material);
					break;
				case CapsuleColliderProperty.Radius:
					_output.SetValue(capsuleCollider.radius);
					break;
				case CapsuleColliderProperty.SharedMaterial:
					_output.SetValue(collider.sharedMaterial);
					break;
				case CapsuleColliderProperty.ContactOffset:
					_output.SetValue(capsuleCollider.contactOffset);
					break;
				}
				return;
			}
			MeshCollider meshCollider = collider as MeshCollider;
			if (meshCollider != null)
			{
				switch (_meshProperty)
				{
				case MeshColliderProperty.AttachedRigidbody:
					_output.SetValue(collider.attachedRigidbody);
					break;
				case MeshColliderProperty.Bounds:
					_output.SetValue(collider.bounds);
					break;
				case MeshColliderProperty.Convex:
					_output.SetValue(meshCollider.convex);
					break;
				case MeshColliderProperty.Enabled:
					_output.SetToggleValue(collider.enabled);
					break;
				case MeshColliderProperty.IsTrigger:
					_output.SetToggleValue(collider.isTrigger);
					break;
				case MeshColliderProperty.Material:
					_output.SetValue(collider.material);
					break;
				case MeshColliderProperty.SharedMaterial:
					_output.SetValue(collider.sharedMaterial);
					break;
				case MeshColliderProperty.SharedMesh:
					_output.SetValue(meshCollider.sharedMesh);
					break;
				case MeshColliderProperty.ContactOffset:
					_output.SetValue(collider.contactOffset);
					break;
				}
			}
		}
	}
}
