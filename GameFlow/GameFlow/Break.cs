using UnityEngine;

namespace GameFlow
{
	[Help("en", "Stops the execution of the current block of actions.", null)]
	[Help("es", "Detiene la ejecución del bloque de acciones actual.", null)]
	[AddComponentMenu("")]
	public class Break : Action, IExecutableInEditor
	{
		public override void Execute()
		{
			(container as IActionContainer)?.Break();
		}

		protected override void OnReset()
		{
			Collapse();
		}
	}
}
