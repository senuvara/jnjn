using UnityEngine;
using UnityEngine.Rendering;

namespace GameFlow
{
	[AddComponentMenu("")]
	[Help("es", "Modifica una propiedad del componente @UnityManual{SpriteRenderer} especificado.", null)]
	[Help("en", "Sets a property of the specified @UnityManual{SpriteRenderer}.", null)]
	public class SetSpriteRendererProperty : Action, IExecutableInEditor
	{
		public enum Property
		{
			Color,
			Enabled,
			LightmapIndex,
			LightmapScaleOffset,
			Material,
			Materials,
			ProbeAnchor,
			RealtimeLightmapScaleOffset,
			ReceiveShadows,
			ReflectionProbeUsage,
			ShadowCastingMode,
			SharedMaterial,
			SharedMaterials,
			SortingLayerID,
			SortingLayerName,
			SortingOrder,
			Sprite,
			LightProbeUsage
		}

		[SerializeField]
		private SpriteRenderer _target;

		[SerializeField]
		private Variable _targetVar;

		[SerializeField]
		private Property _property;

		[SerializeField]
		private Color _colorValue;

		[SerializeField]
		private bool _enabledValue;

		[SerializeField]
		private int _lightmapIndexValue;

		[SerializeField]
		private Vector4 _lightmapScaleOffsetValue;

		[SerializeField]
		private Material _materialValue;

		[SerializeField]
		private Transform _probeAnchorValue;

		[SerializeField]
		private Vector4 _realtimeLightmapScaleOffsetValue;

		[SerializeField]
		private bool _receiveShadowsValue;

		[SerializeField]
		private ReflectionProbeUsage _reflectionProbeUsageValue;

		[SerializeField]
		private ShadowCastingMode _shadowCastingModeValue;

		[SerializeField]
		private Material _sharedMaterialValue;

		[SerializeField]
		private int _sortingLayerIDValue;

		[SerializeField]
		private string _sortingLayerNameValue;

		[SerializeField]
		private int _sortingOrderValue;

		[SerializeField]
		private Sprite _spriteValue;

		[SerializeField]
		private LightProbeUsage _lightProbeUsageValue;

		[SerializeField]
		private Variable _valueVar;

		protected override void OnReset()
		{
			_target = base.gameObject.GetComponent<SpriteRenderer>();
		}

		public override void Setup()
		{
		}

		public override void Execute()
		{
			SpriteRenderer spriteRenderer = _targetVar.GetValueAsComponent(_target, typeof(SpriteRenderer)) as SpriteRenderer;
			if (!(spriteRenderer == null))
			{
				switch (_property)
				{
				case Property.Materials:
				case Property.SharedMaterials:
					break;
				case Property.Color:
					spriteRenderer.color = _valueVar.GetValue(_colorValue);
					break;
				case Property.Enabled:
					spriteRenderer.enabled = _valueVar.GetValue(_enabledValue);
					break;
				case Property.LightmapIndex:
					spriteRenderer.lightmapIndex = _valueVar.GetValue(_lightmapIndexValue);
					break;
				case Property.LightmapScaleOffset:
					spriteRenderer.lightmapScaleOffset = _valueVar.GetValue(_lightmapScaleOffsetValue);
					break;
				case Property.Material:
					spriteRenderer.material = (_valueVar.GetValue(_materialValue) as Material);
					break;
				case Property.ProbeAnchor:
					spriteRenderer.probeAnchor = (_valueVar.GetValueAsComponent(_probeAnchorValue, typeof(Transform)) as Transform);
					break;
				case Property.RealtimeLightmapScaleOffset:
					spriteRenderer.realtimeLightmapScaleOffset = _valueVar.GetValue(_realtimeLightmapScaleOffsetValue);
					break;
				case Property.ReceiveShadows:
					spriteRenderer.receiveShadows = _valueVar.GetValue(_receiveShadowsValue);
					break;
				case Property.ReflectionProbeUsage:
					spriteRenderer.reflectionProbeUsage = (ReflectionProbeUsage)(object)_valueVar.GetValue(_reflectionProbeUsageValue);
					break;
				case Property.ShadowCastingMode:
					spriteRenderer.shadowCastingMode = (ShadowCastingMode)(object)_valueVar.GetValue(_shadowCastingModeValue);
					break;
				case Property.SharedMaterial:
					spriteRenderer.sharedMaterial = (_valueVar.GetValue(_sharedMaterialValue) as Material);
					break;
				case Property.SortingLayerID:
					spriteRenderer.sortingLayerID = _valueVar.GetValue(_sortingLayerIDValue);
					break;
				case Property.SortingLayerName:
					spriteRenderer.sortingLayerName = _valueVar.GetValue(_sortingLayerNameValue);
					break;
				case Property.SortingOrder:
					spriteRenderer.sortingOrder = _valueVar.GetValue(_sortingOrderValue);
					break;
				case Property.Sprite:
					spriteRenderer.sprite = (_valueVar.GetValue(_spriteValue) as Sprite);
					break;
				case Property.LightProbeUsage:
					spriteRenderer.lightProbeUsage = (LightProbeUsage)(object)_valueVar.GetValue(_lightProbeUsageValue);
					break;
				}
			}
		}
	}
}
