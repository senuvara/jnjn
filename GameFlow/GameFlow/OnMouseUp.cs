using UnityEngine;

namespace GameFlow
{
	[Help("en", "Program that will be executed when any mouse button stops being pressed over an enabled Collider or GUIElement component in the listening range.", null)]
	[AddComponentMenu("GameFlow/Programs/On Mouse Up")]
	[DisallowMultipleComponent]
	[Help("es", "Programa que se ejecutará cuando se deje de pulsar cualquier botón del ratón estando su puntero sobre un componente Collider o GUIElement activado en el rango de escucha.", null)]
	public class OnMouseUp : OnMouseProgram
	{
		[LeftToggle]
		public bool onlyAsButton;

		private EventController[] _controllers;

		protected override void RegisterAsListener()
		{
			_controllers = RegisterAsListener(typeof(MouseUpEvent), typeof(MouseController), MouseController.AddController);
			if (base.listeningRange == EventListeningRange.SceneHierarchy)
			{
				MouseUpEvent.AddListener(this);
			}
		}

		protected override bool AcceptEvent(EventObject e)
		{
			MouseUpEvent mouseUpEvent = e as MouseUpEvent;
			return mouseUpEvent != null && mouseUpEvent.asButton == onlyAsButton;
		}

		protected override void UnRegisterAsListener()
		{
			MouseUpEvent.RemoveListener(this);
			UnRegisterAsListener(typeof(MouseUpEvent), typeof(MouseController));
		}

		private void OnTransformChildrenChanged()
		{
			if (base.listeningRange == EventListeningRange.GameObjectHierarchy)
			{
				UnsubscribeFrom(_controllers, typeof(MouseUpEvent));
				_controllers = EventController.GetControllersInHierarchyOf(typeof(MouseController), base.listeningTarget, MouseController.AddController);
				SubscribeTo(_controllers, typeof(MouseUpEvent));
			}
		}
	}
}
