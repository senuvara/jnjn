using System;
using UnityEngine;

namespace GameFlow
{
	public static class VariableExt
	{
		public static IVariableExtProxy proxy;

		public static void SetValue(this Variable variable, string value)
		{
			if (variable != null)
			{
				if (Application.isPlaying)
				{
					variable.stringValue = value;
				}
				else if (proxy != null)
				{
					proxy.SetValue(variable, value.Expanded());
				}
			}
		}

		public static void SetTagValue(this Variable variable, string value)
		{
			if (variable != null)
			{
				if (Application.isPlaying)
				{
					variable.tagValue = value;
				}
				else if (proxy != null)
				{
					proxy.SetTagValue(variable, value);
				}
			}
		}

		public static void SetValue(this Variable variable, int value)
		{
			if (variable != null)
			{
				if (Application.isPlaying)
				{
					variable.intValue = value;
				}
				else if (proxy != null)
				{
					proxy.SetValue(variable, value);
				}
			}
		}

		public static void SetLayerValue(this Variable variable, int value)
		{
			if (variable != null)
			{
				if (Application.isPlaying)
				{
					variable.layerValue = value;
				}
				else if (proxy != null)
				{
					proxy.SetLayerValue(variable, value);
				}
			}
		}

		public static void SetValue(this Variable variable, float value)
		{
			if (variable != null)
			{
				if (Application.isPlaying)
				{
					variable.floatValue = value;
				}
				else if (proxy != null)
				{
					proxy.SetValue(variable, value);
				}
			}
		}

		public static void SetValue(this Variable variable, bool value)
		{
			if (variable != null)
			{
				if (Application.isPlaying)
				{
					variable.boolValue = value;
				}
				else if (proxy != null)
				{
					proxy.SetValue(variable, value);
				}
			}
		}

		public static void SetToggleValue(this Variable variable, bool value)
		{
			if (variable != null)
			{
				if (Application.isPlaying)
				{
					variable.toggleValue = value;
				}
				else if (proxy != null)
				{
					proxy.SetToggleValue(variable, value);
				}
			}
		}

		public static void SetValue(this Variable variable, Vector2 value)
		{
			if (variable != null)
			{
				if (Application.isPlaying)
				{
					variable.vector2Value = value;
				}
				else if (proxy != null)
				{
					proxy.SetValue(variable, value);
				}
			}
		}

		public static void SetValue(this Variable variable, Vector3 value)
		{
			if (variable != null)
			{
				if (Application.isPlaying)
				{
					variable.vector3Value = value;
				}
				else if (proxy != null)
				{
					proxy.SetValue(variable, value);
				}
			}
		}

		public static void SetValue(this Variable variable, Rect value)
		{
			if (variable != null)
			{
				if (Application.isPlaying)
				{
					variable.rectValue = value;
				}
				else if (proxy != null)
				{
					proxy.SetValue(variable, value);
				}
			}
		}

		public static void SetValue(this Variable variable, Color value)
		{
			if (variable != null)
			{
				if (Application.isPlaying)
				{
					variable.colorValue = value;
				}
				else if (proxy != null)
				{
					proxy.SetValue(variable, value);
				}
			}
		}

		public static void SetValue(this Variable variable, UnityEngine.Object value, Type valueType)
		{
			if (variable != null)
			{
				if (Application.isPlaying)
				{
					variable.objectValue = value;
				}
				else if (proxy != null)
				{
					proxy.SetValue(variable, value, valueType);
				}
			}
		}

		public static void SetValue(this Variable variable, UnityEngine.Object value)
		{
			variable.SetValue(value, null);
		}

		public static void SetValue(this Variable variable, Enum value)
		{
			if (variable != null)
			{
				if (Application.isPlaying)
				{
					variable.enumValue = value;
				}
				else if (proxy != null)
				{
					proxy.SetValue(variable, value);
				}
			}
		}

		public static void SetValue(this Variable variable, Bounds value)
		{
			if (variable != null)
			{
				if (Application.isPlaying)
				{
					variable.boundsValue = value;
				}
				else if (proxy != null)
				{
					proxy.SetValue(variable, value);
				}
			}
		}

		public static void SetValue(this Variable variable, Quaternion value)
		{
			if (variable != null)
			{
				if (Application.isPlaying)
				{
					variable.quaternionValue = value;
				}
				else if (proxy != null)
				{
					proxy.SetValue(variable, value);
				}
			}
		}

		public static void SetValue(this Variable variable, Vector4 value)
		{
			if (variable != null)
			{
				if (Application.isPlaying)
				{
					variable.vector4Value = value;
				}
				else if (proxy != null)
				{
					proxy.SetValue(variable, value);
				}
			}
		}

		public static void SetValue(this Variable variable, AnimationCurve value)
		{
			if (variable != null)
			{
				if (Application.isPlaying)
				{
					variable.animationCurveValue = value;
				}
				else if (proxy != null)
				{
					proxy.SetValue(variable, value);
				}
			}
		}

		public static string GetValue(this Variable variable, string defaultValue, string format = null, UnityEngine.Object context = null)
		{
			if (variable != null)
			{
				if (format != null)
				{
					return variable.GetFormattedStringValue(format, context);
				}
				return variable.stringValue;
			}
			return defaultValue;
		}

		public static int GetValue(this Variable variable, int defaultValue)
		{
			if (variable != null)
			{
				return variable.intValue;
			}
			return defaultValue;
		}

		public static float GetValue(this Variable variable, float defaultValue)
		{
			if (variable != null)
			{
				return variable.floatValue;
			}
			return defaultValue;
		}

		public static bool GetValue(this Variable variable, bool defaultValue)
		{
			if (variable != null)
			{
				return variable.boolValue;
			}
			return defaultValue;
		}

		public static Vector2 GetValue(this Variable variable, Vector2 defaultValue)
		{
			if (variable != null)
			{
				return variable.vector2Value;
			}
			return defaultValue;
		}

		public static Vector3 GetValue(this Variable variable, Vector3 defaultValue)
		{
			if (variable != null)
			{
				return variable.vector3Value;
			}
			return defaultValue;
		}

		public static Rect GetValue(this Variable variable, Rect defaultValue)
		{
			if (variable != null)
			{
				return variable.rectValue;
			}
			return defaultValue;
		}

		public static Color GetValue(this Variable variable, Color defaultValue)
		{
			if (variable != null)
			{
				return variable.colorValue;
			}
			return defaultValue;
		}

		public static UnityEngine.Object GetValue(this Variable variable, UnityEngine.Object defaultValue)
		{
			if (variable != null)
			{
				return variable.objectValue;
			}
			return defaultValue;
		}

		public static Enum GetValue(this Variable variable, Enum defaultValue)
		{
			if (variable != null)
			{
				return variable.enumValue;
			}
			return defaultValue;
		}

		public static Bounds GetValue(this Variable variable, Bounds defaultValue)
		{
			if (variable != null)
			{
				return variable.boundsValue;
			}
			return defaultValue;
		}

		public static Quaternion GetValue(this Variable variable, Quaternion defaultValue)
		{
			if (variable != null)
			{
				return variable.quaternionValue;
			}
			return defaultValue;
		}

		public static Vector4 GetValue(this Variable variable, Vector4 defaultValue)
		{
			if (variable != null)
			{
				return variable.vector4Value;
			}
			return defaultValue;
		}

		public static AnimationCurve GetValue(this Variable variable, AnimationCurve defaultValue)
		{
			if (variable != null)
			{
				return variable.animationCurveValue;
			}
			return defaultValue;
		}

		public static Component GetValueAsComponent(this Variable variable, UnityEngine.Object defaultValue, Type componentType)
		{
			Component component = ((!(variable != null)) ? defaultValue : variable.objectValue) as Component;
			if (component == null)
			{
				GameObject gameObject = ((!(variable != null)) ? defaultValue : variable.objectValue) as GameObject;
				if (gameObject != null)
				{
					component = gameObject.GetComponent(componentType);
				}
			}
			return component;
		}

		public static object GetValueAsObject(this Variable variable)
		{
			if (variable == null)
			{
				return null;
			}
			switch (variable.dataType)
			{
			case DataType.String:
				return variable.stringValue;
			case DataType.Integer:
				return variable.intValue;
			case DataType.Float:
				return variable.floatValue;
			case DataType.Boolean:
				return variable.boolValue;
			case DataType.Vector2:
				return variable.vector2Value;
			case DataType.Vector3:
				return variable.vector3Value;
			case DataType.Rect:
				return variable.rectValue;
			case DataType.Color:
				return variable.colorValue;
			case DataType.Object:
				return variable.objectValue;
			case DataType.Enum:
				return variable.enumValue;
			case DataType.Toggle:
				return variable.toggleValue;
			case DataType.Tag:
				return variable.tagValue;
			case DataType.Layer:
				return variable.layerValue;
			case DataType.Bounds:
				return variable.boundsValue;
			case DataType.Quaternion:
				return variable.quaternionValue;
			case DataType.Vector4:
				return variable.vector4Value;
			case DataType.AnimationCurve:
				return variable.animationCurveValue;
			default:
				return null;
			}
		}

		public static string GetValueAsJSON(this Variable variable)
		{
			string result = string.Empty;
			if (variable != null)
			{
				switch (variable.dataType)
				{
				case DataType.Object:
					result = "Null";
					break;
				case DataType.Integer:
				case DataType.Float:
					result = variable.stringValue;
					break;
				case DataType.Boolean:
				case DataType.Toggle:
					result = variable.stringValue.ToLower();
					break;
				default:
					result = "\"" + variable.stringValue + "\"";
					break;
				}
			}
			return result;
		}
	}
}
