using UnityEngine;

namespace GameFlow
{
	public static class GizmosUtils
	{
		public static IGizmosUtilsProxy proxy;

		public static float GetHandleSize(Vector3 position)
		{
			if (proxy != null)
			{
				return proxy.GetHandleSize(position);
			}
			return 0f;
		}

		public static void SetHandlesColor(Color color)
		{
			if (proxy != null)
			{
				proxy.SetHandlesColor(color);
			}
		}

		public static void DrawLine(Vector3 p1, Vector3 p2, bool dotted = false)
		{
			if (proxy != null)
			{
				proxy.DrawLine(p1, p2, dotted);
			}
		}

		public static void DrawArrowCap(int controlID, Vector3 position, Quaternion rotation, float size)
		{
			if (proxy != null)
			{
				proxy.ArrowCap(controlID, position, rotation, size);
			}
		}

		public static void DrawSphereCap(int controlID, Vector3 position, Quaternion rotation, float size)
		{
			if (proxy != null)
			{
				proxy.SphereCap(controlID, position, rotation, size);
			}
		}

		public static void DrawConeCap(int controlID, Vector3 position, Quaternion rotation, float size)
		{
			if (proxy != null)
			{
				proxy.ConeCap(controlID, position, rotation, size);
			}
		}

		public static void DrawSelectableAreaForPointAt(Vector3 position, float radius)
		{
			if (proxy != null)
			{
				proxy.DrawSelectableAreaForPointAt(position, radius);
			}
		}

		public static void Draw3DCrossHairs(Vector3 position, float radius)
		{
			if (proxy != null)
			{
				proxy.Draw3DCrossHairs(position, radius);
			}
		}

		public static void DrawLabel(Vector3 position, string text, Color color, float offsetX = 0f, float offsetY = 0f)
		{
			if (proxy != null)
			{
				proxy.DrawLabel(position, text, color, offsetX, offsetY);
			}
		}

		public static void DrawBounds(Bounds bounds, Color color)
		{
			if (proxy != null)
			{
				proxy.DrawBounds(bounds, color);
			}
		}

		public static void DrawBoundsFrame(Bounds bounds, Color color)
		{
			if (proxy != null)
			{
				proxy.DrawBoundsFrame(bounds, color);
			}
		}

		public static void DrawRect(Rect rect, Color color)
		{
			if (proxy != null)
			{
				proxy.DrawRect(rect, color);
			}
		}

		public static Rect CalculateBoundsFrame(Bounds bounds)
		{
			if (proxy != null)
			{
				return CalculateBoundsFrame(bounds);
			}
			return RectExt.zero;
		}
	}
}
