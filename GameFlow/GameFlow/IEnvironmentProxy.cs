namespace GameFlow
{
	public interface IEnvironmentProxy
	{
		bool IsEditorPaused();
	}
}
