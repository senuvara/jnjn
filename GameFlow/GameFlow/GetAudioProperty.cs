using UnityEngine;

namespace GameFlow
{
	[Help("en", "Gets a general property of the audio system.", null)]
	[Help("es", "Lee una propiedad general del sistema de audio.", null)]
	[AddComponentMenu("")]
	public class GetAudioProperty : Action
	{
		public enum Property
		{
			Mute,
			MusicMute,
			FxMute,
			Volume
		}

		[SerializeField]
		private Property _property;

		[SerializeField]
		private Variable _output;

		public override void Execute()
		{
			if (!(_output == null))
			{
				switch (_property)
				{
				case Property.Mute:
					_output.SetValue(AudioController.instance.mute);
					break;
				case Property.MusicMute:
					_output.SetValue(AudioController.instance.musicMute);
					break;
				case Property.FxMute:
					_output.SetValue(AudioController.instance.fxMute);
					break;
				case Property.Volume:
					_output.SetValue(AudioController.instance.volume);
					break;
				}
			}
		}
	}
}
