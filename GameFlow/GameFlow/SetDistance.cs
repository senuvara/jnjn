using UnityEngine;

namespace GameFlow
{
	[Help("es", "Mueve un objeto con respecto a otro de forma que estén a la distancia especificada.", null)]
	[Help("en", "Moves a object so the distance to another object is the distance given.", null)]
	[AddComponentMenu("")]
	public class SetDistance : Action, IExecutableInEditor
	{
		[SerializeField]
		private Transform _target;

		[SerializeField]
		private Variable _targetVar;

		[SerializeField]
		private Transform _origin;

		[SerializeField]
		private Variable _originVar;

		[SerializeField]
		private float _distance;

		[SerializeField]
		private Variable _distanceVar;

		[SerializeField]
		private bool _relative;

		[SerializeField]
		private Variable _relativeVar;

		protected override void OnReset()
		{
			_target = base.gameObject.GetComponent<Transform>();
		}

		public override void Execute()
		{
			Transform transform = _targetVar.GetValueAsComponent(_target, typeof(Transform)) as Transform;
			if (!(transform == null))
			{
				Transform transform2 = _originVar.GetValueAsComponent(_origin, typeof(Transform)) as Transform;
				if (!(transform2 == null))
				{
					float value = _distanceVar.GetValue(_distance);
					float num = Vector3.Distance(transform2.position, transform.position);
					float maxDistanceDelta = (!_relativeVar.GetValue(_relative)) ? (num - value) : (0f - value);
					transform.position = Vector3.MoveTowards(transform.position, transform2.position, maxDistanceDelta);
				}
			}
		}
	}
}
