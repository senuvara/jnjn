using UnityEngine;

namespace GameFlow
{
	[AddComponentMenu("")]
	[Help("es", "Cambia una component del color especificado.", null)]
	[Help("en", "Sets a component of the specified color.", null)]
	public class SetColorComponent : Action
	{
		public enum ValueType
		{
			Integer,
			Float
		}

		[SerializeField]
		private Variable _variable;

		[SerializeField]
		private ColorComponent _component;

		[SerializeField]
		private Variable _componentVar;

		[SerializeField]
		private ValueType _valueType;

		[SerializeField]
		private int _value;

		[SerializeField]
		private float _floatValue;

		[SerializeField]
		private Variable _valueVar;

		protected ColorComponent component => (ColorComponent)(object)_componentVar.GetValue(_component);

		protected override void OnReset()
		{
		}

		public override void Execute()
		{
			if (_valueType == ValueType.Integer)
			{
				Execute(_variable, component, _valueVar.GetValue(_value));
			}
			else if (_valueType == ValueType.Float)
			{
				Execute(_variable, component, _valueVar.GetValue(_floatValue));
			}
		}

		public static void Execute(Variable variable, ColorComponent component, int value)
		{
			if (!(variable == null))
			{
				Color colorValue = variable.colorValue;
				switch (component)
				{
				case ColorComponent.R:
					colorValue.r = (float)value / 255f;
					break;
				case ColorComponent.G:
					colorValue.g = (float)value / 255f;
					break;
				case ColorComponent.B:
					colorValue.b = (float)value / 255f;
					break;
				case ColorComponent.A:
					colorValue.a = (float)value / 255f;
					break;
				}
				variable.SetValue(colorValue);
			}
		}

		public static void Execute(Variable variable, ColorComponent component, float value)
		{
			if (!(variable == null))
			{
				Color colorValue = variable.colorValue;
				switch (component)
				{
				case ColorComponent.R:
					colorValue.r = value;
					break;
				case ColorComponent.G:
					colorValue.g = value;
					break;
				case ColorComponent.B:
					colorValue.b = value;
					break;
				case ColorComponent.A:
					colorValue.a = value;
					break;
				}
				variable.SetValue(colorValue);
			}
		}
	}
}
