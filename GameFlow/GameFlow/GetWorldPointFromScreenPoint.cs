using UnityEngine;

namespace GameFlow
{
	[Help("en", "Gets a 3D world point from the specified 2D screen point.", null)]
	[AddComponentMenu("")]
	[Help("es", "Obtiene un punto 3D a partir del punto de pantalla 2D especificado.", null)]
	public class GetWorldPointFromScreenPoint : Action, IExecutableInEditor
	{
		[SerializeField]
		private Vector2 _position;

		[SerializeField]
		private Variable _positionVar;

		[SerializeField]
		private Variable _output;

		protected override void OnReset()
		{
		}

		public override void Setup()
		{
		}

		public override void Execute()
		{
			if (!(_output == null))
			{
				Vector2 value = _positionVar.GetValue(_position);
				UnityEngine.Ray ray = Camera.main.ScreenPointToRay(new Vector3(value.x, value.y, 0f));
				if (Physics.Raycast(ray, out UnityEngine.RaycastHit hitInfo))
				{
					_output.SetValue(hitInfo.point);
				}
			}
		}
	}
}
