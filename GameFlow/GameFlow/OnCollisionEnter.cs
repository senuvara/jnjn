using UnityEngine;

namespace GameFlow
{
	[DisallowMultipleComponent]
	[AddComponentMenu("GameFlow/Programs/On Collision Enter")]
	[Help("en", "Program that will be executed one time as soon as a Collider component in the listening range starts colliding with another component.", null)]
	[Help("es", "Programa que se ejecutará una vez tan pronto como un Collider en el rango de escucha comience a colisionar con otro componente.", null)]
	public class OnCollisionEnter : OnCollisionProgram
	{
		private EventController[] _controllers;

		protected override void RegisterAsListener()
		{
			_controllers = RegisterAsListener(typeof(CollisionEnterEvent), typeof(CollisionController), CollisionController.AddController);
			if (base.listeningRange == EventListeningRange.SceneHierarchy)
			{
				CollisionEnterEvent.AddListener(this);
			}
		}

		protected override void UnRegisterAsListener()
		{
			CollisionEnterEvent.RemoveListener(this);
			UnRegisterAsListener(typeof(CollisionEnterEvent), typeof(CollisionController));
		}

		private void OnTransformChildrenChanged()
		{
			if (base.listeningRange == EventListeningRange.GameObjectHierarchy)
			{
				UnsubscribeFrom(_controllers, typeof(CollisionEnterEvent));
				_controllers = EventController.GetControllersInHierarchyOf(typeof(CollisionController), base.listeningTarget, CollisionController.AddController);
				SubscribeTo(_controllers, typeof(CollisionEnterEvent));
			}
		}
	}
}
