using UnityEngine;

namespace GameFlow
{
	[Help("es", "Devuelve un valor @UnityAPI{Vector2} o @UnityAPI{Vector3} aleatorio.", null)]
	[Help("en", "Gets a random @UnityAPI{Vector2} or @UnityAPI{Vector3} value.", null)]
	[AddComponentMenu("")]
	public class GetRandomVector : Action, IExecutableInEditor
	{
		public enum OutputType
		{
			Vector3,
			Vector2
		}

		[SerializeField]
		private OutputType _outputType;

		[SerializeField]
		private Vector3 _min = Vector3.zero;

		[SerializeField]
		private Variable _minVar;

		[SerializeField]
		private Vector3 _max = new Vector3(1f, 1f, 1f);

		[SerializeField]
		private Variable _maxVar;

		[SerializeField]
		private Variable _output;

		public Vector3 min => _minVar.GetValue(_min);

		public Vector3 max => _maxVar.GetValue(_max);

		public override void Execute()
		{
			if (!(_output == null))
			{
				switch (_outputType)
				{
				case OutputType.Vector3:
					_output.SetValue(RandomUtils.GetRandomVector3(min, max));
					break;
				case OutputType.Vector2:
					_output.SetValue(RandomUtils.GetRandomVector2(min, max));
					break;
				}
			}
		}
	}
}
