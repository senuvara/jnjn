using UnityEngine;

namespace GameFlow
{
	[AddComponentMenu("")]
	[Help("en", "Sets a property of the specified @UnityManual{Camera}.", null)]
	[Help("es", "Modifica una propiedad de la @UnityManual{Camera} especificada.", null)]
	public class SetCameraProperty : Action, IExecutableInEditor
	{
		public enum Property
		{
			Aspect,
			BackgroundColor,
			ClearFlags,
			CullingMask,
			Depth,
			DepthTextureMode,
			Enabled,
			EventMask,
			FarClipPlane,
			FieldOfView,
			HDR,
			LayerCullDistances,
			LayerCullSpherical,
			NearClipPlane,
			Ortographic,
			OrtographicSize,
			PixelRect,
			Rect,
			RenderingPath,
			StereoConvergence,
			StereoSeparation,
			TargetTexture,
			TransparencySortMode,
			UseOcclusionCulling
		}

		[SerializeField]
		private Camera _target;

		[SerializeField]
		private Variable _targetVar;

		[SerializeField]
		private Property _property;

		[SerializeField]
		private float _floatValue;

		[SerializeField]
		private bool _boolValue;

		[SerializeField]
		private Color _colorValue;

		[SerializeField]
		private CameraClearFlags _clearFlagsValue;

		[SerializeField]
		private Rect _rectValue;

		[SerializeField]
		private Variable _valueVar;

		protected override void OnReset()
		{
			_target = Camera.main;
			_colorValue = Color.black;
		}

		public override void Execute()
		{
			Camera camera = _target;
			if (camera == null)
			{
				camera = (_targetVar.GetValueAsComponent(_target, typeof(Camera)) as Camera);
			}
			if (!(camera == null))
			{
				switch (_property)
				{
				case Property.CullingMask:
					break;
				case Property.DepthTextureMode:
					break;
				case Property.EventMask:
					break;
				case Property.LayerCullDistances:
					break;
				case Property.RenderingPath:
					break;
				case Property.TargetTexture:
					break;
				case Property.TransparencySortMode:
					break;
				case Property.Aspect:
					camera.aspect = _valueVar.GetValue(_floatValue);
					break;
				case Property.BackgroundColor:
					camera.backgroundColor = _valueVar.GetValue(_colorValue);
					break;
				case Property.ClearFlags:
					camera.clearFlags = (CameraClearFlags)(object)_valueVar.GetValue(_clearFlagsValue);
					break;
				case Property.Depth:
					camera.depth = _valueVar.GetValue(_floatValue);
					break;
				case Property.Enabled:
					camera.enabled = _valueVar.GetValue(_boolValue);
					break;
				case Property.FarClipPlane:
					camera.farClipPlane = _valueVar.GetValue(_floatValue);
					break;
				case Property.FieldOfView:
					camera.fieldOfView = _valueVar.GetValue(_floatValue);
					break;
				case Property.HDR:
					camera.hdr = _valueVar.GetValue(_boolValue);
					break;
				case Property.LayerCullSpherical:
					camera.layerCullSpherical = _valueVar.GetValue(_boolValue);
					break;
				case Property.NearClipPlane:
					camera.nearClipPlane = _valueVar.GetValue(_floatValue);
					break;
				case Property.Ortographic:
					camera.orthographic = _valueVar.GetValue(_boolValue);
					break;
				case Property.OrtographicSize:
					camera.orthographicSize = _valueVar.GetValue(_floatValue);
					break;
				case Property.PixelRect:
					camera.pixelRect = _valueVar.GetValue(_rectValue);
					break;
				case Property.Rect:
					camera.rect = _valueVar.GetValue(_rectValue);
					break;
				case Property.StereoConvergence:
					camera.stereoConvergence = _valueVar.GetValue(_floatValue);
					break;
				case Property.StereoSeparation:
					camera.stereoSeparation = _valueVar.GetValue(_floatValue);
					break;
				case Property.UseOcclusionCulling:
					camera.useOcclusionCulling = _valueVar.GetValue(_boolValue);
					break;
				}
			}
		}
	}
}
