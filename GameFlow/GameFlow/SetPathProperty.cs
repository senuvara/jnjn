using UnityEngine;

namespace GameFlow
{
	[AddComponentMenu("")]
	[Help("es", "Modifica una propiedad del componente @GameFlow{Path} especificado.", null)]
	[Help("en", "Sets a property of the specified @GameFlow{Path} component.", null)]
	public class SetPathProperty : Action, IExecutableInEditor
	{
		public enum Property
		{
			PathType,
			Subdivisions,
			Closed,
			Color
		}

		[SerializeField]
		private Path _target;

		[SerializeField]
		private Variable _targetVar;

		[SerializeField]
		private Property _property;

		[SerializeField]
		private PathType _pathTypeValue;

		[SerializeField]
		private int _intValue;

		[SerializeField]
		private bool _boolValue;

		[SerializeField]
		private Color _colorValue;

		[SerializeField]
		private Variable _valueVar;

		protected override void OnReset()
		{
		}

		public override void Execute()
		{
			Path path = _target;
			if (path == null)
			{
				path = (_targetVar.GetValueAsComponent(_target, typeof(Path)) as Path);
			}
			if (!(path == null))
			{
				switch (_property)
				{
				case Property.PathType:
					path.pathType = (PathType)(object)_valueVar.GetValue(_pathTypeValue);
					break;
				case Property.Subdivisions:
					path.subdivisions = _valueVar.GetValue(_intValue);
					break;
				case Property.Closed:
					path.closed = _valueVar.GetValue(_boolValue);
					break;
				case Property.Color:
					path.color = _valueVar.GetValue(_colorValue);
					break;
				}
			}
		}
	}
}
