using System;
using UnityEngine;

namespace GameFlow
{
	[Help("en", "A built-in @GameFlow{Variable} that returns the minute for the current time.", null)]
	[Help("es", "Una @GameFlow{Variable} integrada que devuelve el minuto de la hora actual.", null)]
	[AddComponentMenu("")]
	public class BuiltinMinute : BuiltinVariable
	{
		public override int intValue => DateTime.Now.Minute;

		public override string stringValue => Convert.ToString(DateTime.Now.Minute).PadLeft(2, '0');

		public override Type GetVariableType()
		{
			return typeof(int);
		}

		protected override string GetVariableId()
		{
			return "Minute";
		}
	}
}
