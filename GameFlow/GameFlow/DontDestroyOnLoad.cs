using System;
using UnityEngine;

namespace GameFlow
{
	[Help("en", "Makes the object target not be destroyed automatically when loading a new scene.", null)]
	[AddComponentMenu("")]
	[Help("es", "Hace que el objeto objetivo especificado no sea destruido al carga una nueva escena.", null)]
	public class DontDestroyOnLoad : Action
	{
		[SerializeField]
		private GameObject _target;

		[SerializeField]
		private Variable _targetVar;

		public GameObject target => _targetVar.GetValue(_target) as GameObject;

		protected override void OnReset()
		{
			_target = base.gameObject;
		}

		public override void Execute()
		{
			GameObject target = this.target;
			if (target != null)
			{
				UnityEngine.Object.DontDestroyOnLoad(target);
			}
		}

		public override Type GetPreferredContainerType()
		{
			return typeof(OnAwake);
		}
	}
}
