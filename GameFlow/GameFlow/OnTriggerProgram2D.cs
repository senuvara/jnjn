namespace GameFlow
{
	public abstract class OnTriggerProgram2D : EventProgram
	{
		private Parameter _sourceParam;

		private Parameter _otherParam;

		public Parameter sourceParam
		{
			get
			{
				if (_sourceParam == null)
				{
					_sourceParam = GetParameter("Source");
				}
				return _sourceParam;
			}
		}

		public Parameter otherParam
		{
			get
			{
				if (_otherParam == null)
				{
					_otherParam = GetParameter("Other");
				}
				return _otherParam;
			}
		}

		protected override void SetParameters(EventObject e)
		{
			TriggerEvent triggerEvent = e as TriggerEvent;
			sourceParam.SetValue(triggerEvent.source);
			otherParam.SetValue(triggerEvent.other);
		}
	}
}
