using System;
using System.Reflection;
using UnityEngine;

namespace GameFlow
{
	[AddComponentMenu("")]
	[Help("en", "Sets the value of the specified script variable using Reflection.", null)]
	[Help("es", "Establece el valor de la variable de script especificada usando Reflection.", null)]
	public class SetScriptProperty : ValueAction, IExecutableInEditor
	{
		[SerializeField]
		private MonoBehaviour _target;

		[SerializeField]
		private Variable _targetVar;

		[SerializeField]
		private string _property;

		[SerializeField]
		private Variable _propertyVar;

		public MonoBehaviour target => _targetVar.GetValue(_target) as MonoBehaviour;

		public string property => _propertyVar.GetValue(_property);

		public override void Execute()
		{
			if (target == null || property == null)
			{
				return;
			}
			FieldInfo field = target.GetType().GetField(property);
			if (field != null)
			{
				Type fieldType = field.FieldType;
				switch (fieldType.ToDataType())
				{
				case DataType.Toggle:
				case DataType.Tag:
				case DataType.Layer:
					break;
				case DataType.String:
					field.SetValue(target, base.stringValue);
					break;
				case DataType.Integer:
					field.SetValue(target, base.intValue);
					break;
				case DataType.Float:
					field.SetValue(target, base.floatValue);
					break;
				case DataType.Boolean:
					field.SetValue(target, base.boolValue);
					break;
				case DataType.Vector2:
					field.SetValue(target, base.vector2Value);
					break;
				case DataType.Vector3:
					field.SetValue(target, base.vector3Value);
					break;
				case DataType.Vector4:
					field.SetValue(target, base.vector4Value);
					break;
				case DataType.Rect:
					field.SetValue(target, base.rectValue);
					break;
				case DataType.Color:
					field.SetValue(target, base.colorValue);
					break;
				case DataType.Object:
					field.SetValue(target, base.objectValue);
					break;
				case DataType.Enum:
					field.SetValue(target, (Enum)Enum.ToObject(fieldType, base.enumIntValue));
					break;
				case DataType.AnimationCurve:
					field.SetValue(target, base.animationCurveValue);
					break;
				case DataType.Bounds:
					field.SetValue(target, base.boundsValue);
					break;
				case DataType.Quaternion:
					field.SetValue(target, base.quaternionValue);
					break;
				}
			}
		}

		private static FieldInfo GetField(MonoBehaviour target, string property, DataType dataType)
		{
			if (target == null)
			{
				return null;
			}
			if (property == null)
			{
				return null;
			}
			FieldInfo field = target.GetType().GetField(property);
			if (field == null)
			{
				return null;
			}
			Type fieldType = field.FieldType;
			if (fieldType.ToDataType() != dataType)
			{
				return null;
			}
			return field;
		}

		public static void Execute(MonoBehaviour target, string property, string value)
		{
			GetField(target, property, DataType.String)?.SetValue(target, value);
		}

		public static void Execute(MonoBehaviour target, string property, int value)
		{
			GetField(target, property, DataType.Integer)?.SetValue(target, value);
		}

		public static void Execute(MonoBehaviour target, string property, float value)
		{
			GetField(target, property, DataType.Float)?.SetValue(target, value);
		}

		public static void Execute(MonoBehaviour target, string property, bool value)
		{
			GetField(target, property, DataType.Boolean)?.SetValue(target, value);
		}

		public static void Execute(MonoBehaviour target, string property, Vector2 value)
		{
			GetField(target, property, DataType.Vector2)?.SetValue(target, value);
		}

		public static void Execute(MonoBehaviour target, string property, Vector3 value)
		{
			GetField(target, property, DataType.Vector3)?.SetValue(target, value);
		}

		public static void Execute(MonoBehaviour target, string property, Vector4 value)
		{
			GetField(target, property, DataType.Vector4)?.SetValue(target, value);
		}

		public static void Execute(MonoBehaviour target, string property, Rect value)
		{
			GetField(target, property, DataType.Rect)?.SetValue(target, value);
		}

		public static void Execute(MonoBehaviour target, string property, Color value)
		{
			GetField(target, property, DataType.Color)?.SetValue(target, value);
		}

		public static void Execute(MonoBehaviour target, string property, UnityEngine.Object value)
		{
			GetField(target, property, DataType.Object)?.SetValue(target, value);
		}

		public static void Execute(MonoBehaviour target, string property, Enum value)
		{
			GetField(target, property, DataType.Enum)?.SetValue(target, value);
		}

		public static void Execute(MonoBehaviour target, string property, AnimationCurve value)
		{
			GetField(target, property, DataType.AnimationCurve)?.SetValue(target, value);
		}

		public static void Execute(MonoBehaviour target, string property, Bounds value)
		{
			GetField(target, property, DataType.Bounds)?.SetValue(target, value);
		}

		public static void Execute(MonoBehaviour target, string property, Quaternion value)
		{
			GetField(target, property, DataType.Quaternion)?.SetValue(target, value);
		}
	}
}
