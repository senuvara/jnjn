using UnityEngine;

namespace GameFlow
{
	[AddComponentMenu("")]
	[Help("en", "Enables the specified Behaviour component.", null)]
	[Help("es", "Activa el componente de comportamiento (Behaviour) especificado.", null)]
	public class EnableBehaviour : Action, IExecutableInEditor
	{
		[SerializeField]
		private Behaviour _target;

		[SerializeField]
		private Variable _targetVar;

		public override void Execute()
		{
			Execute(_targetVar.GetValueAsComponent(_target, typeof(Behaviour)) as Behaviour);
		}

		public static void Execute(Behaviour behaviour)
		{
			if (behaviour != null)
			{
				behaviour.enabled = true;
			}
		}
	}
}
