using UnityEngine;

namespace GameFlow
{
	[Help("en", "Multiplies the numeric value of the specified @GameFlow{Variable}.", null)]
	[AddComponentMenu("")]
	[Help("es", "Multiplica el valor numérico de la @GameFlow{Variable} especificada.", null)]
	public class MultiplyVariableValue : VariableAction
	{
		protected override int GetDefaultIntValue()
		{
			return 1;
		}

		protected override float GetDefaultFloatValue()
		{
			return 1f;
		}

		public override void Execute()
		{
			Execute(base.variable, this);
		}

		public static void Execute(Variable variable, ValueAction valueAction)
		{
			if (variable == null || valueAction == null)
			{
				return;
			}
			Variable variable2 = (!variable.isIndirect) ? variable : variable.indirection;
			if (variable2 == null || variable2.isReadOnly)
			{
				return;
			}
			switch (variable2.dataType)
			{
			case DataType.Integer:
				variable2.intValue *= valueAction.intValue;
				break;
			case DataType.Float:
				variable2.floatValue *= valueAction.floatValue;
				break;
			case DataType.Vector2:
				switch (valueAction.GetDataType(DataType.Vector2))
				{
				case DataType.Boolean:
					break;
				case DataType.Integer:
				case DataType.Float:
					variable2.vector2Value *= valueAction.floatValue;
					break;
				case DataType.Vector2:
				case DataType.Vector3:
					variable2.vector2Value = variable2.vector2Value.Multiply(valueAction.vector2Value);
					break;
				}
				break;
			case DataType.Vector3:
				switch (valueAction.GetDataType(DataType.Vector3))
				{
				case DataType.Boolean:
					break;
				case DataType.Integer:
				case DataType.Float:
					variable2.vector3Value *= valueAction.floatValue;
					break;
				case DataType.Vector2:
				case DataType.Vector3:
					variable2.vector3Value = variable2.vector3Value.Multiply(valueAction.vector3Value);
					break;
				}
				break;
			case DataType.Rect:
				variable2.rectValue = variable2.rectValue.Multiply(valueAction.rectValue);
				break;
			case DataType.Bounds:
				variable2.boundsValue = variable2.boundsValue.Multiply(valueAction.boundsValue);
				break;
			case DataType.Vector4:
				switch (valueAction.GetDataType(DataType.Vector4))
				{
				case DataType.Integer:
				case DataType.Float:
					variable2.vector4Value *= valueAction.floatValue;
					break;
				case DataType.Vector2:
				case DataType.Vector3:
				case DataType.Vector4:
					variable2.vector4Value = variable2.vector4Value.Multiply(valueAction.vector4Value);
					break;
				}
				break;
			}
		}
	}
}
