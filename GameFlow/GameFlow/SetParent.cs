using UnityEngine;

namespace GameFlow
{
	[Help("es", "Asigna un nuevo objeto padre al objeto especificado.", null)]
	[Help("en", "Assigns a new parent to the specified object.", null)]
	[AddComponentMenu("")]
	public class SetParent : Action, IExecutableInEditor
	{
		[SerializeField]
		private GameObject _target;

		[SerializeField]
		private Variable _targetVar;

		[SerializeField]
		private GameObject _parent;

		[SerializeField]
		private Variable _parentVar;

		public GameObject target => _targetVar.GetValue(_target) as GameObject;

		public GameObject parent => _parentVar.GetValue(_parent) as GameObject;

		public override void Execute()
		{
			Execute(target, parent);
		}

		public static void Execute(GameObject target, GameObject parent)
		{
			if (target != null)
			{
				target.GetComponent<Transform>().parent = ((!(parent != null)) ? null : parent.GetComponent<Transform>());
			}
		}
	}
}
