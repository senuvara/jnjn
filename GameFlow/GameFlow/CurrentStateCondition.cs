using UnityEngine;

namespace GameFlow
{
	[Help("en", "Evaluates the specified @GameFlow{State}.", null)]
	[Help("es", "Evalúa el estado (@GameFlow{State}) especificado.", null)]
	[AddComponentMenu("")]
	public class CurrentStateCondition : Condition
	{
		public enum Comparison
		{
			IsCurrent,
			IsNotCurrent
		}

		[SerializeField]
		private Comparison _comparison;

		[SerializeField]
		private State _state;

		[SerializeField]
		private Variable _stateVar;

		public State state => _stateVar.GetValue(_state) as State;

		public override bool Evaluate()
		{
			return Evaluate(_comparison, state);
		}

		public static bool Evaluate(Comparison comparison, State state)
		{
			if (state == null)
			{
				return false;
			}
			StateMachine stateMachine = state.container as StateMachine;
			if (stateMachine == null)
			{
				return false;
			}
			switch (comparison)
			{
			case Comparison.IsCurrent:
				return stateMachine.currentState == state;
			case Comparison.IsNotCurrent:
				return stateMachine.currentState != state;
			default:
				return false;
			}
		}
	}
}
