using UnityEngine;

namespace GameFlow
{
	public class Log
	{
		private static string _header = "[<GameFlow>]  ";

		public static void Info(object message, Object context = null)
		{
			Debug.Log(string.Concat(_header, message, "\n"), context);
		}

		public static void Warning(object message, Object context = null)
		{
			Debug.LogWarning(string.Concat(_header, message, "\n"), context);
		}

		public static void Error(object message, Object context = null)
		{
			Debug.LogError(string.Concat(_header, message, "\n"), context);
		}

		public static void Obsolete(string obsolete, string replacement, Object context = null)
		{
			Warning(obsolete + " is OBSOLETE, please use " + replacement + " instead.\n", context);
		}
	}
}
