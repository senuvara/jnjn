using UnityEngine;

namespace GameFlow
{
	public class GameObjectEvent : EventObject
	{
		public GameObject source;
	}
}
