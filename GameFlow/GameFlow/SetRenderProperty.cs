using UnityEngine;

namespace GameFlow
{
	[Help("es", "Modifica una propiedad de los ajustes globales de Render.", null)]
	[AddComponentMenu("")]
	[Help("en", "Sets a property of the global Render Settings.", null)]
	public class SetRenderProperty : Action, IExecutableInEditor
	{
		public enum Property
		{
			AmbientLight,
			FlareFadeSpeed,
			FlareStrength,
			Fog,
			FogColor,
			FogDensity,
			FogEndDistance,
			FogMode,
			FogStartDistance,
			HaloStrength,
			Skybox
		}

		[SerializeField]
		private Property _property = Property.Fog;

		[SerializeField]
		private Color _colorValue = Color.white;

		[SerializeField]
		private float _floatValue = 1f;

		[SerializeField]
		private bool _toggleValue = true;

		[SerializeField]
		private FogMode _fogModeValue;

		[SerializeField]
		private Material _materialValue;

		[SerializeField]
		private Variable _valueVar;

		public override void Execute()
		{
			switch (_property)
			{
			case Property.AmbientLight:
				RenderSettings.ambientLight = _valueVar.GetValue(_colorValue);
				break;
			case Property.FlareFadeSpeed:
				RenderSettings.flareFadeSpeed = _valueVar.GetValue(_floatValue);
				break;
			case Property.FlareStrength:
				RenderSettings.flareStrength = _valueVar.GetValue(_floatValue);
				break;
			case Property.Fog:
				RenderSettings.fog = _valueVar.GetValue(_toggleValue);
				break;
			case Property.FogColor:
				RenderSettings.fogColor = _valueVar.GetValue(_colorValue);
				break;
			case Property.FogDensity:
				RenderSettings.fogDensity = _valueVar.GetValue(_floatValue);
				break;
			case Property.FogEndDistance:
				RenderSettings.fogEndDistance = _valueVar.GetValue(_floatValue);
				break;
			case Property.FogMode:
				RenderSettings.fogMode = (FogMode)(object)_valueVar.GetValue(_fogModeValue);
				break;
			case Property.FogStartDistance:
				RenderSettings.fogStartDistance = _valueVar.GetValue(_floatValue);
				break;
			case Property.HaloStrength:
				RenderSettings.haloStrength = _valueVar.GetValue(_floatValue);
				break;
			case Property.Skybox:
				RenderSettings.skybox = (_valueVar.GetValue(_materialValue) as Material);
				break;
			}
		}
	}
}
