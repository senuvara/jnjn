using UnityEngine;

namespace GameFlow
{
	[Help("es", "Obtiene el componente @UnityManual{RectTransform} del @UnityManual{GameObject} especificado.", null)]
	[Help("en", "Gets the @UnityManual{RectTransform} component of the specified @UnityManual{GameObject}.", null)]
	[AddComponentMenu("")]
	public class GetRectTransform : Action, IExecutableInEditor
	{
		[SerializeField]
		private GameObject _source;

		[SerializeField]
		private Variable _sourceVar;

		[SerializeField]
		private Variable _output;

		public GameObject source => _sourceVar.GetValue(_source) as GameObject;

		protected override void OnReset()
		{
			_source = base.gameObject;
		}

		public override void Execute()
		{
			if (!(_output == null))
			{
				GameObject source = this.source;
				if (!(source == null))
				{
					_output.SetValue(source.GetComponent<RectTransform>());
				}
			}
		}
	}
}
