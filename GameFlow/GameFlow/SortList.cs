using UnityEngine;

namespace GameFlow
{
	[Help("es", "Ordena los elementos de la lista (@GameFlow{List}) especificada.", null)]
	[Help("en", "Sorts the items in the specified @GameFlow{List}.", null)]
	[AddComponentMenu("")]
	public class SortList : ListAction
	{
		public enum SortingType
		{
			Ascending,
			Descending
		}

		public enum Vector2Criterion
		{
			XY,
			X,
			Y
		}

		public enum Vector3Criterion
		{
			XYZ,
			XY,
			XZ,
			YZ,
			X,
			Y,
			Z
		}

		public enum RectCriterion
		{
			PositionSize,
			Position,
			Size,
			X,
			Y,
			Width,
			Height
		}

		public enum ColorCriterion
		{
			RGBA,
			RGB,
			R,
			G,
			B,
			A
		}

		[SerializeField]
		private SortingType _sortingType;

		[SerializeField]
		private int _criterion;

		public override void Execute()
		{
			Execute(GetList(), _sortingType == SortingType.Descending, _criterion);
		}

		public static void Execute(List list, bool desc, int criterion)
		{
			if (!(list == null))
			{
				switch (list.dataType)
				{
				case DataType.Object:
					break;
				case DataType.String:
				case DataType.Tag:
					SortString(list, desc);
					break;
				case DataType.Integer:
				case DataType.Boolean:
				case DataType.Enum:
				case DataType.Toggle:
				case DataType.Layer:
					SortInteger(list, desc);
					break;
				case DataType.Float:
					SortFloat(list, desc);
					break;
				case DataType.Vector2:
					SortVector2(list, desc, (Vector2Criterion)criterion);
					break;
				case DataType.Vector3:
					SortVector3(list, desc, (Vector3Criterion)criterion);
					break;
				case DataType.Rect:
					SortRect(list, desc, (RectCriterion)criterion);
					break;
				case DataType.Color:
					SortColor(list, desc, (ColorCriterion)criterion);
					break;
				}
			}
		}

		private static void SortString(List list, bool desc)
		{
			int num = (!desc) ? 1 : (-1);
			for (int i = 0; i < list.Count; i++)
			{
				for (int j = i + 1; j < list.Count; j++)
				{
					if (list.GetStringAt(i).CompareTo(list.GetStringAt(j)) == num)
					{
						list.SwapValues(i, j);
					}
				}
			}
		}

		private static void SortInteger(List list, bool desc)
		{
			int num = (!desc) ? 1 : (-1);
			for (int i = 0; i < list.Count; i++)
			{
				for (int j = i + 1; j < list.Count; j++)
				{
					if (list.GetIntAt(i).CompareTo(list.GetIntAt(j)) == num)
					{
						list.SwapValues(i, j);
					}
				}
			}
		}

		private static void SortFloat(List list, bool desc)
		{
			int num = (!desc) ? 1 : (-1);
			for (int i = 0; i < list.Count; i++)
			{
				for (int j = i + 1; j < list.Count; j++)
				{
					if (list.GetFloatAt(i).CompareTo(list.GetFloatAt(j)) == num)
					{
						list.SwapValues(i, j);
					}
				}
			}
		}

		private static void SortVector2(List list, bool desc, Vector2Criterion criterion)
		{
			for (int i = 0; i < list.Count; i++)
			{
				for (int j = i + 1; j < list.Count; j++)
				{
					Vector2 vector2At = list.GetVector2At(i);
					Vector2 vector2At2 = list.GetVector2At(j);
					bool flag = false;
					switch (criterion)
					{
					case Vector2Criterion.XY:
						flag = (vector2At.x > vector2At2.x && vector2At.y > vector2At2.y);
						break;
					case Vector2Criterion.X:
						flag = (vector2At.x > vector2At2.x);
						break;
					case Vector2Criterion.Y:
						flag = (vector2At.y > vector2At2.y);
						break;
					}
					if (desc ? (!flag) : flag)
					{
						list.SwapValues(i, j);
					}
				}
			}
		}

		private static void SortVector3(List list, bool desc, Vector3Criterion criterion)
		{
			for (int i = 0; i < list.Count; i++)
			{
				for (int j = i + 1; j < list.Count; j++)
				{
					Vector3 vector3At = list.GetVector3At(i);
					Vector3 vector3At2 = list.GetVector3At(j);
					bool flag = false;
					switch (criterion)
					{
					case Vector3Criterion.XYZ:
						flag = (vector3At.x > vector3At2.x && vector3At.y > vector3At2.y && vector3At.z > vector3At2.z);
						break;
					case Vector3Criterion.XY:
						flag = (vector3At.x > vector3At2.x && vector3At.y > vector3At2.y);
						break;
					case Vector3Criterion.XZ:
						flag = (vector3At.x > vector3At2.x && vector3At.z > vector3At2.z);
						break;
					case Vector3Criterion.YZ:
						flag = (vector3At.y > vector3At2.y && vector3At.z > vector3At2.z);
						break;
					case Vector3Criterion.X:
						flag = (vector3At.x > vector3At2.x);
						break;
					case Vector3Criterion.Y:
						flag = (vector3At.y > vector3At2.y);
						break;
					case Vector3Criterion.Z:
						flag = (vector3At.z > vector3At2.z);
						break;
					}
					if (desc ? (!flag) : flag)
					{
						list.SwapValues(i, j);
					}
				}
			}
		}

		private static void SortRect(List list, bool desc, RectCriterion criterion)
		{
			for (int i = 0; i < list.Count; i++)
			{
				for (int j = i + 1; j < list.Count; j++)
				{
					Rect rectAt = list.GetRectAt(i);
					Rect rectAt2 = list.GetRectAt(j);
					bool flag = false;
					switch (criterion)
					{
					case RectCriterion.PositionSize:
						flag = (rectAt.x > rectAt2.x && rectAt.y > rectAt2.y && rectAt.width > rectAt2.width && rectAt.height > rectAt.height);
						break;
					case RectCriterion.Position:
						flag = (rectAt.x > rectAt2.x && rectAt.y > rectAt2.y);
						break;
					case RectCriterion.Size:
						flag = (rectAt.width > rectAt2.width && rectAt.height > rectAt.height);
						break;
					case RectCriterion.X:
						flag = (rectAt.x > rectAt2.x);
						break;
					case RectCriterion.Y:
						flag = (rectAt.y > rectAt2.y);
						break;
					case RectCriterion.Width:
						flag = (rectAt.width > rectAt2.width);
						break;
					case RectCriterion.Height:
						flag = (rectAt.height > rectAt.height);
						break;
					}
					if (desc ? (!flag) : flag)
					{
						list.SwapValues(i, j);
					}
				}
			}
		}

		private static void SortColor(List list, bool desc, ColorCriterion criterion)
		{
			for (int i = 0; i < list.Count; i++)
			{
				for (int j = i + 1; j < list.Count; j++)
				{
					Color colorAt = list.GetColorAt(i);
					Color colorAt2 = list.GetColorAt(j);
					bool flag = false;
					switch (criterion)
					{
					case ColorCriterion.RGBA:
						flag = (colorAt.r > colorAt2.r && colorAt.g > colorAt2.g && colorAt.b > colorAt2.b && colorAt.a > colorAt2.a);
						break;
					case ColorCriterion.RGB:
						flag = (colorAt.r > colorAt2.r && colorAt.g > colorAt2.g && colorAt.b > colorAt2.b);
						break;
					case ColorCriterion.R:
						flag = (colorAt.r > colorAt2.r);
						break;
					case ColorCriterion.G:
						flag = (colorAt.g > colorAt2.g);
						break;
					case ColorCriterion.B:
						flag = (colorAt.b > colorAt2.b);
						break;
					case ColorCriterion.A:
						flag = (colorAt.a > colorAt2.a);
						break;
					}
					if (desc ? (!flag) : flag)
					{
						list.SwapValues(i, j);
					}
				}
			}
		}
	}
}
