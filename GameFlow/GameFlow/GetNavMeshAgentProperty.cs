using UnityEngine;
using UnityEngine.AI;

namespace GameFlow
{
	[Help("es", "Lee una propiedad del componente @UnityManual{NavMesh} especificado.", null)]
	[AddComponentMenu("")]
	[Help("en", "Gets a property of the specified @UnityManual{NavMesh} component.", null)]
	public class GetNavMeshAgentProperty : Action, IExecutableInEditor
	{
		public enum Property
		{
			Acceleration,
			AngularSpeed,
			AreaMask,
			AutoBraking,
			AutoRepath,
			AutoTraverseOffMeshLink,
			AvoidancePriority,
			BaseOffset,
			CurrentOffMeshLinkData,
			DesiredVelocity,
			Destination,
			HasPath,
			Height,
			IsOnNavMesh,
			IsOnOffMeshLink,
			IsPathStale,
			NextOffMeshLinkData,
			NextPosition,
			ObstacleAvoidanceType,
			Path,
			PathEndPosition,
			PathPending,
			PathStatus,
			Radius,
			RemainingDistance,
			Speed,
			SteeringTarget,
			StoppingDistance,
			UpdatePosition,
			UpdateRotation,
			Velocity
		}

		[SerializeField]
		private NavMeshAgent _source;

		[SerializeField]
		private Variable _sourceVar;

		[SerializeField]
		private Property _property;

		[SerializeField]
		private Variable _output;

		protected override void OnReset()
		{
			_source = base.gameObject.GetComponent<NavMeshAgent>();
		}

		public override void Setup()
		{
		}

		public override void Execute()
		{
			NavMeshAgent navMeshAgent = _sourceVar.GetValueAsComponent(_source, typeof(NavMeshAgent)) as NavMeshAgent;
			if (!(navMeshAgent == null))
			{
				switch (_property)
				{
				case Property.CurrentOffMeshLinkData:
				case Property.NextOffMeshLinkData:
				case Property.Path:
					break;
				case Property.Acceleration:
					_output.SetValue(navMeshAgent.acceleration);
					break;
				case Property.AngularSpeed:
					_output.SetValue(navMeshAgent.angularSpeed);
					break;
				case Property.AreaMask:
					_output.SetValue(navMeshAgent.areaMask);
					break;
				case Property.AutoBraking:
					_output.SetValue(navMeshAgent.autoBraking);
					break;
				case Property.AutoRepath:
					_output.SetValue(navMeshAgent.autoRepath);
					break;
				case Property.AutoTraverseOffMeshLink:
					_output.SetValue(navMeshAgent.autoTraverseOffMeshLink);
					break;
				case Property.AvoidancePriority:
					_output.SetValue(navMeshAgent.avoidancePriority);
					break;
				case Property.BaseOffset:
					_output.SetValue(navMeshAgent.baseOffset);
					break;
				case Property.DesiredVelocity:
					_output.SetValue(navMeshAgent.desiredVelocity);
					break;
				case Property.Destination:
					_output.SetValue(navMeshAgent.destination);
					break;
				case Property.HasPath:
					_output.SetValue(navMeshAgent.hasPath);
					break;
				case Property.Height:
					_output.SetValue(navMeshAgent.height);
					break;
				case Property.IsOnNavMesh:
					_output.SetValue(navMeshAgent.isOnNavMesh);
					break;
				case Property.IsOnOffMeshLink:
					_output.SetValue(navMeshAgent.isOnOffMeshLink);
					break;
				case Property.IsPathStale:
					_output.SetValue(navMeshAgent.isPathStale);
					break;
				case Property.NextPosition:
					_output.SetValue(navMeshAgent.nextPosition);
					break;
				case Property.ObstacleAvoidanceType:
					_output.SetValue(navMeshAgent.obstacleAvoidanceType);
					break;
				case Property.PathEndPosition:
					_output.SetValue(navMeshAgent.pathEndPosition);
					break;
				case Property.PathPending:
					_output.SetValue(navMeshAgent.pathPending);
					break;
				case Property.PathStatus:
					_output.SetValue(navMeshAgent.pathStatus);
					break;
				case Property.Radius:
					_output.SetValue(navMeshAgent.radius);
					break;
				case Property.RemainingDistance:
					_output.SetValue(navMeshAgent.remainingDistance);
					break;
				case Property.Speed:
					_output.SetValue(navMeshAgent.speed);
					break;
				case Property.SteeringTarget:
					_output.SetValue(navMeshAgent.steeringTarget);
					break;
				case Property.StoppingDistance:
					_output.SetValue(navMeshAgent.stoppingDistance);
					break;
				case Property.UpdatePosition:
					_output.SetValue(navMeshAgent.updatePosition);
					break;
				case Property.UpdateRotation:
					_output.SetValue(navMeshAgent.updateRotation);
					break;
				case Property.Velocity:
					_output.SetValue(navMeshAgent.velocity);
					break;
				}
			}
		}
	}
}
