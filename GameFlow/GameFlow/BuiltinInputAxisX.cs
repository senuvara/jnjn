using System;
using UnityEngine;

namespace GameFlow
{
	[Help("es", "Una @GameFlow{Variable} integrada que devuelve el valor del eje virtual horizontal de entrada.", null)]
	[Help("en", "A built-in @GameFlow{Variable} that returns the current value of the virtual horizontal input axis.", null)]
	[AddComponentMenu("")]
	public class BuiltinInputAxisX : BuiltinVariable
	{
		public override float floatValue => (!Application.isPlaying) ? 0f : Input.GetAxis("Horizontal");

		public override Type GetVariableType()
		{
			return typeof(float);
		}

		protected override string GetVariableId()
		{
			return "Input Axis X";
		}
	}
}
