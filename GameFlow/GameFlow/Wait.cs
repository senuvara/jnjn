using UnityEngine;

namespace GameFlow
{
	[Help("es", "Espera durante el tiempo especificado.", null)]
	[Help("en", "Waits for the specified time interval.", null)]
	[AddComponentMenu("")]
	public class Wait : TimeAction
	{
	}
}
