using System;
using System.Collections.Generic;
using UnityEngine;

namespace GameFlow
{
	[AddComponentMenu("GameFlow/Tools/List")]
	[Help("es", "Define una lista dinámica de elementos del tipo especificado.", null)]
	[Help("en", "Defines a dynamic list of items of the specified type.", null)]
	public class List : BaseBehaviour, IData, IIdentifiable, IVariableFriendly
	{
		private enum Operation
		{
			Set,
			Insert,
			Add
		}

		public static readonly int BASE_INDEX = 0;

		public static IListProxy proxy = null;

		private static Type defaultType = typeof(GameObject);

		[SerializeField]
		private string _id = string.Empty;

		[SerializeField]
		private string _typeName = defaultType.FullName;

		private Type _type;

		[SerializeField]
		private DataType _dataType = DataType.Object;

		[SerializeField]
		private List<string> _stringValues = new List<string>();

		[SerializeField]
		private List<int> _intValues = new List<int>();

		[SerializeField]
		private List<float> _floatValues = new List<float>();

		[SerializeField]
		private List<Vector2> _vector2Values = new List<Vector2>();

		[SerializeField]
		private List<Vector3> _vector3Values = new List<Vector3>();

		[SerializeField]
		private List<Vector4> _vector4Values = new List<Vector4>();

		[SerializeField]
		private List<Rect> _rectValues = new List<Rect>();

		[SerializeField]
		private List<Color> _colorValues = new List<Color>();

		[SerializeField]
		private List<UnityEngine.Object> _objectValues = new List<UnityEngine.Object>();

		[SerializeField]
		private List<Bounds> _boundsValues = new List<Bounds>();

		[SerializeField]
		private List<AnimationCurve> _curveValues = new List<AnimationCurve>();

		[SerializeField]
		private List<Quaternion> _quaternionValues = new List<Quaternion>();

		[SerializeField]
		private List<Variable> _variables = new List<Variable>();

		[SerializeField]
		private bool _persistent;

		[SerializeField]
		private bool _titleFoldout = true;

		[SerializeField]
		private bool _itemsFoldout = true;

		[SerializeField]
		private bool _resetPerformed;

		[SerializeField]
		private bool _unfocus;

		public string id
		{
			get
			{
				return _id;
			}
			set
			{
				_id = value;
			}
		}

		public Type type
		{
			get
			{
				switch (_dataType)
				{
				case DataType.Object:
				case DataType.Enum:
					if (_typeName == null)
					{
						_typeName = defaultType.FullName;
						_type = defaultType;
					}
					else if (_type == null || _typeName != _type.FullName)
					{
						_type = TypeUtils.GetType(_typeName);
					}
					break;
				case DataType.String:
				case DataType.Tag:
					_type = typeof(string);
					break;
				case DataType.Integer:
				case DataType.Layer:
					_type = typeof(int);
					break;
				case DataType.Float:
					_type = typeof(float);
					break;
				case DataType.Boolean:
				case DataType.Toggle:
					_type = typeof(bool);
					break;
				case DataType.Vector2:
					_type = typeof(Vector2);
					break;
				case DataType.Vector3:
					_type = typeof(Vector3);
					break;
				case DataType.Vector4:
					_type = typeof(Vector4);
					break;
				case DataType.Rect:
					_type = typeof(Rect);
					break;
				case DataType.Color:
					_type = typeof(Color);
					break;
				case DataType.Bounds:
					_type = typeof(Bounds);
					break;
				case DataType.AnimationCurve:
					_type = typeof(AnimationCurve);
					break;
				case DataType.Quaternion:
					_type = typeof(Quaternion);
					break;
				}
				return _type;
			}
			set
			{
				CastTo(value);
			}
		}

		public DataType dataType => _dataType;

		public int Count => _variables.Count;

		public int NonNullObjectCount
		{
			get
			{
				int num = 0;
				for (int i = 0; i < Count; i++)
				{
					if (GetObjectAt(i) != null)
					{
						num++;
					}
				}
				return num;
			}
		}

		public bool isPersistent => _persistent;

		private void Awake()
		{
			proxy = null;
		}

		private void Start()
		{
		}

		private void Reset()
		{
			Type type = GetDefaultType();
			_typeName = ((type == null) ? defaultType.FullName : type.FullName);
			_dataType = type.ToDataType();
			OnReset();
		}

		protected override void OnReset()
		{
			if (_id == string.Empty)
			{
				_id = GetDefaultId();
			}
		}

		public virtual string GetDefaultId()
		{
			return GetType().Name + UnityEngine.Random.Range(1000, 9999);
		}

		public virtual Type GetDefaultType()
		{
			return defaultType;
		}

		public virtual void OnModify()
		{
		}

		public virtual string GetIdForControls()
		{
			return _id;
		}

		public virtual string GetIdForSelectors()
		{
			return _id;
		}

		public virtual string GetTypeForSelectors()
		{
			return "List";
		}

		public string GetStringAt(int index)
		{
			index -= BASE_INDEX;
			return _variables[index].GetValue(_stringValues[index]);
		}

		public int GetIntAt(int index)
		{
			index -= BASE_INDEX;
			return _variables[index].GetValue(_intValues[index]);
		}

		public float GetFloatAt(int index)
		{
			index -= BASE_INDEX;
			return _variables[index].GetValue(_floatValues[index]);
		}

		public bool GetBoolAt(int index)
		{
			index -= BASE_INDEX;
			return _variables[index].GetValue(Convert.ToBoolean(_intValues[index]));
		}

		public Vector2 GetVector2At(int index)
		{
			index -= BASE_INDEX;
			return _variables[index].GetValue(_vector2Values[index]);
		}

		public Vector3 GetVector3At(int index)
		{
			index -= BASE_INDEX;
			return _variables[index].GetValue(_vector3Values[index]);
		}

		public Vector3 GetVector4At(int index)
		{
			index -= BASE_INDEX;
			return _variables[index].GetValue(_vector4Values[index]);
		}

		public Rect GetRectAt(int index)
		{
			index -= BASE_INDEX;
			return _variables[index].GetValue(_rectValues[index]);
		}

		public Color GetColorAt(int index)
		{
			index -= BASE_INDEX;
			return _variables[index].GetValue(_colorValues[index]);
		}

		public Enum GetEnumAt(int index)
		{
			index -= BASE_INDEX;
			int value = _variables[index].GetValue(_intValues[index]);
			return (Enum)Enum.ToObject(type, value);
		}

		public UnityEngine.Object GetObjectAt(int index)
		{
			index -= BASE_INDEX;
			return _variables[index].GetValue(_objectValues[index]);
		}

		public AnimationCurve GetAnimationCurveAt(int index)
		{
			index -= BASE_INDEX;
			return _variables[index].GetValue(_curveValues[index]);
		}

		public Bounds GetBoundsAt(int index)
		{
			index -= BASE_INDEX;
			return _variables[index].GetValue(_boundsValues[index]);
		}

		public Quaternion GetQuaternionAt(int index)
		{
			index -= BASE_INDEX;
			return _variables[index].GetValue(_quaternionValues[index]);
		}

		public Variable GetVariableAt(int index)
		{
			index -= BASE_INDEX;
			return _variables[index];
		}

		public string GetJSONValueAt(int index)
		{
			switch (dataType)
			{
			case DataType.String:
				return "\"" + GetStringAt(index) + "\"";
			case DataType.Integer:
				return string.Empty + GetIntAt(index);
			case DataType.Float:
				return string.Empty + GetFloatAt(index);
			case DataType.Boolean:
			case DataType.Toggle:
				return (!GetBoolAt(index)) ? "false" : "true";
			default:
				return "null";
			}
		}

		public bool Contains(string value)
		{
			return _stringValues.Contains(value);
		}

		public bool Contains(int value)
		{
			return _intValues.Contains(value);
		}

		public bool Contains(float value)
		{
			return _floatValues.Contains(value);
		}

		public bool Contains(bool value)
		{
			return _intValues.Contains(value ? 1 : 0);
		}

		public bool Contains(Vector2 value)
		{
			return _vector2Values.Contains(value);
		}

		public bool Contains(Vector3 value)
		{
			return _vector3Values.Contains(value);
		}

		public bool Contains(Vector4 value)
		{
			return _vector4Values.Contains(value);
		}

		public bool Contains(Rect value)
		{
			return _rectValues.Contains(value);
		}

		public bool Contains(Color value)
		{
			return _colorValues.Contains(value);
		}

		public bool Contains(UnityEngine.Object value)
		{
			return _objectValues.Contains(value);
		}

		public bool Contains(Enum value)
		{
			return _intValues.Contains(Convert.ToInt32(value));
		}

		public bool Contains(AnimationCurve value)
		{
			return _curveValues.Contains(value);
		}

		public bool Contains(Bounds value)
		{
			return _boundsValues.Contains(value);
		}

		public bool Contains(Quaternion value)
		{
			return _quaternionValues.Contains(value);
		}

		public bool IsIndexValid(int index)
		{
			index -= BASE_INDEX;
			return index >= 0 && index < Count;
		}

		public int IndexOf(string value)
		{
			return IndexOf(value, 0);
		}

		public int IndexOf(string value, int fromIndex)
		{
			return _stringValues.IndexOf(value, fromIndex) + BASE_INDEX;
		}

		public int IndexOf(int value)
		{
			return IndexOf(value, 0);
		}

		public int IndexOf(int value, int fromIndex)
		{
			return _intValues.IndexOf(value, fromIndex) + BASE_INDEX;
		}

		public int IndexOf(float value)
		{
			return IndexOf(value, 0);
		}

		public int IndexOf(float value, int fromIndex)
		{
			return _floatValues.IndexOf(value, fromIndex) + BASE_INDEX;
		}

		public int IndexOf(bool value)
		{
			return IndexOf(value, 0);
		}

		public int IndexOf(bool value, int fromIndex)
		{
			return _intValues.IndexOf(value ? 1 : 0, fromIndex) + BASE_INDEX;
		}

		public int IndexOf(Vector2 value)
		{
			return IndexOf(value, 0);
		}

		public int IndexOf(Vector2 value, int fromIndex)
		{
			return _vector2Values.IndexOf(value, fromIndex) + BASE_INDEX;
		}

		public int IndexOf(Vector3 value)
		{
			return IndexOf(value, 0);
		}

		public int IndexOf(Vector3 value, int fromIndex)
		{
			return _vector3Values.IndexOf(value, fromIndex) + BASE_INDEX;
		}

		public int IndexOf(Vector4 value)
		{
			return IndexOf(value, 0);
		}

		public int IndexOf(Vector4 value, int fromIndex)
		{
			return _vector4Values.IndexOf(value, fromIndex) + BASE_INDEX;
		}

		public int IndexOf(Rect value)
		{
			return IndexOf(value, 0);
		}

		public int IndexOf(Rect value, int fromIndex)
		{
			return _rectValues.IndexOf(value, fromIndex) + BASE_INDEX;
		}

		public int IndexOf(Color value)
		{
			return IndexOf(value, 0);
		}

		public int IndexOf(Color value, int fromIndex)
		{
			return _colorValues.IndexOf(value, fromIndex) + BASE_INDEX;
		}

		public int IndexOf(UnityEngine.Object obj)
		{
			return IndexOf(obj, 0);
		}

		public int IndexOf(UnityEngine.Object value, int fromIndex)
		{
			Variable variable = value as Variable;
			return ((!(variable != null)) ? _objectValues.IndexOf(value, fromIndex) : _variables.IndexOf(variable, fromIndex)) + BASE_INDEX;
		}

		public int IndexOfNextNonNullObjectFrom(int index)
		{
			for (int i = index; i < _objectValues.Count; i++)
			{
				if (GetObjectAt(i) != null)
				{
					return i;
				}
			}
			return -1;
		}

		public int IndexOf(Enum value)
		{
			return IndexOf(value, 0);
		}

		public int IndexOf(Enum value, int fromIndex)
		{
			return _intValues.IndexOf(Convert.ToInt32(value), fromIndex) + BASE_INDEX;
		}

		public int IndexOf(AnimationCurve value)
		{
			return IndexOf(value, 0);
		}

		public int IndexOf(AnimationCurve value, int fromIndex)
		{
			return _curveValues.IndexOf(value, fromIndex) + BASE_INDEX;
		}

		public int IndexOf(Bounds value)
		{
			return IndexOf(value, 0);
		}

		public int IndexOf(Bounds value, int fromIndex)
		{
			return _boundsValues.IndexOf(value, fromIndex) + BASE_INDEX;
		}

		public int IndexOf(Quaternion value)
		{
			return IndexOf(value, 0);
		}

		public int IndexOf(Quaternion value, int fromIndex)
		{
			return _quaternionValues.IndexOf(value, fromIndex) + BASE_INDEX;
		}

		public void SetValueAt(string value, int index)
		{
			DoOperation(Operation.Set, value, index);
		}

		public void InsertValueAt(string value, int index)
		{
			DoOperation(Operation.Insert, value, index);
		}

		public void AddValue(string value)
		{
			DoOperation(Operation.Add, value, -1);
		}

		private void DoOperation(Operation op, string value, int index)
		{
			if (proxy != null)
			{
				switch (op)
				{
				case Operation.Set:
					proxy.SetValueAt(this, value, index);
					break;
				case Operation.Insert:
					proxy.InsertValueAt(this, value, index);
					break;
				case Operation.Add:
					proxy.AddValue(this, value);
					break;
				}
			}
			else
			{
				index -= BASE_INDEX;
				Variable variable = null;
				switch (op)
				{
				case Operation.Set:
					_stringValues[index] = value;
					_variables[index] = variable;
					break;
				case Operation.Insert:
					_stringValues.Insert(index, value);
					_variables.Insert(index, variable);
					break;
				case Operation.Add:
					_stringValues.Add(value);
					_variables.Add(variable);
					break;
				}
			}
			OnModify();
		}

		public void SetValueAt(int value, int index)
		{
			DoOperation(Operation.Set, value, index);
		}

		public void InsertValueAt(int value, int index)
		{
			DoOperation(Operation.Insert, value, index);
		}

		public void AddValue(int value)
		{
			DoOperation(Operation.Add, value, -1);
		}

		private void DoOperation(Operation op, int value, int index)
		{
			if (proxy != null)
			{
				switch (op)
				{
				case Operation.Set:
					proxy.SetValueAt(this, value, index);
					break;
				case Operation.Insert:
					proxy.InsertValueAt(this, value, index);
					break;
				case Operation.Add:
					proxy.AddValue(this, value);
					break;
				}
			}
			else
			{
				index -= BASE_INDEX;
				Variable variable = null;
				switch (op)
				{
				case Operation.Set:
					_intValues[index] = value;
					_variables[index] = variable;
					break;
				case Operation.Insert:
					_intValues.Insert(index, value);
					_variables.Insert(index, variable);
					break;
				case Operation.Add:
					_intValues.Add(value);
					_variables.Add(variable);
					break;
				}
			}
			OnModify();
		}

		public void SetValueAt(float value, int index)
		{
			DoOperation(Operation.Set, value, index);
		}

		public void InsertValueAt(float value, int index)
		{
			DoOperation(Operation.Insert, value, index);
		}

		public void AddValue(float value)
		{
			DoOperation(Operation.Add, value, -1);
		}

		private void DoOperation(Operation op, float value, int index)
		{
			if (proxy != null)
			{
				switch (op)
				{
				case Operation.Set:
					proxy.SetValueAt(this, value, index);
					break;
				case Operation.Insert:
					proxy.InsertValueAt(this, value, index);
					break;
				case Operation.Add:
					proxy.AddValue(this, value);
					break;
				}
			}
			else
			{
				index -= BASE_INDEX;
				Variable variable = null;
				switch (op)
				{
				case Operation.Set:
					_floatValues[index] = value;
					_variables[index] = variable;
					break;
				case Operation.Insert:
					_floatValues.Insert(index, value);
					_variables.Insert(index, variable);
					break;
				case Operation.Add:
					_floatValues.Add(value);
					_variables.Add(variable);
					break;
				}
			}
			OnModify();
		}

		public void SetValueAt(bool value, int index)
		{
			DoOperation(Operation.Set, value, index);
		}

		public void InsertValueAt(bool value, int index)
		{
			DoOperation(Operation.Insert, value, index);
		}

		public void AddValue(bool value)
		{
			DoOperation(Operation.Add, value, -1);
		}

		private void DoOperation(Operation op, bool value, int index)
		{
			int num = Convert.ToInt32(value);
			if (proxy != null)
			{
				switch (op)
				{
				case Operation.Set:
					proxy.SetValueAt(this, num, index);
					break;
				case Operation.Insert:
					proxy.InsertValueAt(this, num, index);
					break;
				case Operation.Add:
					proxy.AddValue(this, num);
					break;
				}
			}
			else
			{
				index -= BASE_INDEX;
				Variable variable = null;
				switch (op)
				{
				case Operation.Set:
					_intValues[index] = num;
					_variables[index] = variable;
					break;
				case Operation.Insert:
					_intValues.Insert(index, num);
					_variables.Insert(index, variable);
					break;
				case Operation.Add:
					_intValues.Add(num);
					_variables.Add(variable);
					break;
				}
			}
			OnModify();
		}

		public void SetValueAt(Vector2 value, int index)
		{
			DoOperation(Operation.Set, value, index);
		}

		public void InsertValueAt(Vector2 value, int index)
		{
			DoOperation(Operation.Insert, value, index);
		}

		public void AddValue(Vector2 value)
		{
			DoOperation(Operation.Add, value, -1);
		}

		private void DoOperation(Operation op, Vector2 value, int index)
		{
			if (proxy != null)
			{
				switch (op)
				{
				case Operation.Set:
					proxy.SetValueAt(this, value, index);
					break;
				case Operation.Insert:
					proxy.InsertValueAt(this, value, index);
					break;
				case Operation.Add:
					proxy.AddValue(this, value);
					break;
				}
			}
			else
			{
				index -= BASE_INDEX;
				Variable variable = null;
				switch (op)
				{
				case Operation.Set:
					_vector2Values[index] = value;
					_variables[index] = variable;
					break;
				case Operation.Insert:
					_vector2Values.Insert(index, value);
					_variables.Insert(index, variable);
					break;
				case Operation.Add:
					_vector2Values.Add(value);
					_variables.Add(variable);
					break;
				}
			}
			OnModify();
		}

		public void SetValueAt(Vector3 value, int index)
		{
			DoOperation(Operation.Set, value, index);
		}

		public void InsertValueAt(Vector3 value, int index)
		{
			DoOperation(Operation.Insert, value, index);
		}

		public void AddValue(Vector3 value)
		{
			DoOperation(Operation.Add, value, -1);
		}

		private void DoOperation(Operation op, Vector3 value, int index)
		{
			if (proxy != null)
			{
				switch (op)
				{
				case Operation.Set:
					proxy.SetValueAt(this, value, index);
					break;
				case Operation.Insert:
					proxy.InsertValueAt(this, value, index);
					break;
				case Operation.Add:
					proxy.AddValue(this, value);
					break;
				}
			}
			else
			{
				index -= BASE_INDEX;
				Variable variable = null;
				switch (op)
				{
				case Operation.Set:
					_vector3Values[index] = value;
					_variables[index] = variable;
					break;
				case Operation.Insert:
					_vector3Values.Insert(index, value);
					_variables.Insert(index, variable);
					break;
				case Operation.Add:
					_vector3Values.Add(value);
					_variables.Add(variable);
					break;
				}
			}
			OnModify();
		}

		public void SetValueAt(Vector4 value, int index)
		{
			DoOperation(Operation.Set, value, index);
		}

		public void InsertValueAt(Vector4 value, int index)
		{
			DoOperation(Operation.Insert, value, index);
		}

		public void AddValue(Vector4 value)
		{
			DoOperation(Operation.Add, value, -1);
		}

		private void DoOperation(Operation op, Vector4 value, int index)
		{
			if (proxy != null)
			{
				switch (op)
				{
				case Operation.Set:
					proxy.SetValueAt(this, value, index);
					break;
				case Operation.Insert:
					proxy.InsertValueAt(this, value, index);
					break;
				case Operation.Add:
					proxy.AddValue(this, value);
					break;
				}
			}
			else
			{
				index -= BASE_INDEX;
				Variable variable = null;
				switch (op)
				{
				case Operation.Set:
					_vector4Values[index] = value;
					_variables[index] = variable;
					break;
				case Operation.Insert:
					_vector4Values.Insert(index, value);
					_variables.Insert(index, variable);
					break;
				case Operation.Add:
					_vector4Values.Add(value);
					_variables.Add(variable);
					break;
				}
			}
			OnModify();
		}

		public void SetValueAt(Rect value, int index)
		{
			DoOperation(Operation.Set, value, index);
		}

		public void InsertValueAt(Rect value, int index)
		{
			DoOperation(Operation.Insert, value, index);
		}

		public void AddValue(Rect value)
		{
			DoOperation(Operation.Add, value, -1);
		}

		private void DoOperation(Operation op, Rect value, int index)
		{
			if (proxy != null)
			{
				switch (op)
				{
				case Operation.Set:
					proxy.SetValueAt(this, value, index);
					break;
				case Operation.Insert:
					proxy.InsertValueAt(this, value, index);
					break;
				case Operation.Add:
					proxy.AddValue(this, value);
					break;
				}
			}
			else
			{
				index -= BASE_INDEX;
				Variable variable = null;
				switch (op)
				{
				case Operation.Set:
					_rectValues[index] = value;
					_variables[index] = variable;
					break;
				case Operation.Insert:
					_rectValues.Insert(index, value);
					_variables.Insert(index, variable);
					break;
				case Operation.Add:
					_rectValues.Add(value);
					_variables.Add(variable);
					break;
				}
			}
			OnModify();
		}

		public void SetValueAt(Color value, int index)
		{
			DoOperation(Operation.Set, value, index);
		}

		public void InsertValueAt(Color value, int index)
		{
			DoOperation(Operation.Insert, value, index);
		}

		public void AddValue(Color value)
		{
			DoOperation(Operation.Add, value, -1);
		}

		private void DoOperation(Operation op, Color value, int index)
		{
			if (proxy != null)
			{
				switch (op)
				{
				case Operation.Set:
					proxy.SetValueAt(this, value, index);
					break;
				case Operation.Insert:
					proxy.InsertValueAt(this, value, index);
					break;
				case Operation.Add:
					proxy.AddValue(this, value);
					break;
				}
			}
			else
			{
				index -= BASE_INDEX;
				Variable variable = null;
				switch (op)
				{
				case Operation.Set:
					_colorValues[index] = value;
					_variables[index] = variable;
					break;
				case Operation.Insert:
					_colorValues.Insert(index, value);
					_variables.Insert(index, variable);
					break;
				case Operation.Add:
					_colorValues.Add(value);
					_variables.Add(variable);
					break;
				}
			}
			OnModify();
		}

		public void SetValueAt(UnityEngine.Object value, int index)
		{
			DoOperation(Operation.Set, value, index);
		}

		public void InsertValueAt(UnityEngine.Object value, int index)
		{
			DoOperation(Operation.Insert, value, index);
		}

		public void AddValue(UnityEngine.Object value)
		{
			DoOperation(Operation.Add, value, -1);
		}

		private void DoOperation(Operation op, UnityEngine.Object value, int index)
		{
			if (proxy != null)
			{
				switch (op)
				{
				case Operation.Set:
					proxy.SetValueAt(this, value, index);
					break;
				case Operation.Insert:
					proxy.InsertValueAt(this, value, index);
					break;
				case Operation.Add:
					proxy.AddValue(this, value);
					break;
				}
			}
			else
			{
				index -= BASE_INDEX;
				Variable variable = value as Variable;
				if (variable != null)
				{
					value = null;
				}
				switch (op)
				{
				case Operation.Set:
					if (value == null)
					{
						ScriptableObject scriptableObject = _objectValues[index] as ScriptableObject;
						if (scriptableObject != null)
						{
							UnityEngine.Object.Destroy(scriptableObject);
						}
					}
					_objectValues[index] = value;
					_variables[index] = variable;
					break;
				case Operation.Insert:
					_objectValues.Insert(index, value);
					_variables.Insert(index, variable);
					break;
				case Operation.Add:
					_objectValues.Add(value);
					_variables.Add(variable);
					break;
				}
			}
			OnModify();
		}

		public void SetValueAt(Enum value, int index)
		{
			DoOperation(Operation.Set, value, index);
		}

		public void InsertValueAt(Enum value, int index)
		{
			DoOperation(Operation.Insert, value, index);
		}

		public void AddValue(Enum value)
		{
			DoOperation(Operation.Add, value, -1);
		}

		private void DoOperation(Operation op, Enum value, int index)
		{
			if (proxy != null)
			{
				switch (op)
				{
				case Operation.Set:
					proxy.SetValueAt(this, value, index);
					break;
				case Operation.Insert:
					proxy.InsertValueAt(this, value, index);
					break;
				case Operation.Add:
					proxy.AddValue(this, value);
					break;
				}
			}
			else
			{
				index -= BASE_INDEX;
				int num = Convert.ToInt32(value);
				Variable variable = null;
				switch (op)
				{
				case Operation.Set:
					_intValues[index] = num;
					_variables[index] = variable;
					break;
				case Operation.Insert:
					_intValues.Insert(index, num);
					_variables.Insert(index, variable);
					break;
				case Operation.Add:
					_intValues.Add(num);
					_variables.Add(variable);
					break;
				}
			}
			OnModify();
		}

		public void SetValueAt(AnimationCurve value, int index)
		{
			DoOperation(Operation.Set, value, index);
		}

		public void InsertValueAt(AnimationCurve value, int index)
		{
			DoOperation(Operation.Insert, value, index);
		}

		public void AddValue(AnimationCurve value)
		{
			DoOperation(Operation.Add, value, -1);
		}

		private void DoOperation(Operation op, AnimationCurve value, int index)
		{
			if (proxy != null)
			{
				switch (op)
				{
				case Operation.Set:
					proxy.SetValueAt(this, value, index);
					break;
				case Operation.Insert:
					proxy.InsertValueAt(this, value, index);
					break;
				case Operation.Add:
					proxy.AddValue(this, value);
					break;
				}
			}
			else
			{
				index -= BASE_INDEX;
				Variable variable = null;
				switch (op)
				{
				case Operation.Set:
					_curveValues[index] = value;
					_variables[index] = variable;
					break;
				case Operation.Insert:
					_curveValues.Insert(index, value);
					_variables.Insert(index, variable);
					break;
				case Operation.Add:
					_curveValues.Add(value);
					_variables.Add(variable);
					break;
				}
			}
			OnModify();
		}

		public void SetValueAt(Bounds value, int index)
		{
			DoOperation(Operation.Set, value, index);
		}

		public void InsertValueAt(Bounds value, int index)
		{
			DoOperation(Operation.Insert, value, index);
		}

		public void AddValue(Bounds value)
		{
			DoOperation(Operation.Add, value, -1);
		}

		private void DoOperation(Operation op, Bounds value, int index)
		{
			if (proxy != null)
			{
				switch (op)
				{
				case Operation.Set:
					proxy.SetValueAt(this, value, index);
					break;
				case Operation.Insert:
					proxy.InsertValueAt(this, value, index);
					break;
				case Operation.Add:
					proxy.AddValue(this, value);
					break;
				}
			}
			else
			{
				index -= BASE_INDEX;
				Variable variable = null;
				switch (op)
				{
				case Operation.Set:
					_boundsValues[index] = value;
					_variables[index] = variable;
					break;
				case Operation.Insert:
					_boundsValues.Insert(index, value);
					_variables.Insert(index, variable);
					break;
				case Operation.Add:
					_boundsValues.Add(value);
					_variables.Add(variable);
					break;
				}
			}
			OnModify();
		}

		public void SetValueAt(Quaternion value, int index)
		{
			DoOperation(Operation.Set, value, index);
		}

		public void InsertValueAt(Quaternion value, int index)
		{
			DoOperation(Operation.Insert, value, index);
		}

		public void AddValue(Quaternion value)
		{
			DoOperation(Operation.Add, value, -1);
		}

		private void DoOperation(Operation op, Quaternion value, int index)
		{
			if (proxy != null)
			{
				switch (op)
				{
				case Operation.Set:
					proxy.SetValueAt(this, value, index);
					break;
				case Operation.Insert:
					proxy.InsertValueAt(this, value, index);
					break;
				case Operation.Add:
					proxy.AddValue(this, value);
					break;
				}
			}
			else
			{
				index -= BASE_INDEX;
				Variable variable = null;
				switch (op)
				{
				case Operation.Set:
					_quaternionValues[index] = value;
					_variables[index] = variable;
					break;
				case Operation.Insert:
					_quaternionValues.Insert(index, value);
					_variables.Insert(index, variable);
					break;
				case Operation.Add:
					_quaternionValues.Add(value);
					_variables.Add(variable);
					break;
				}
			}
			OnModify();
		}

		public void AddItem()
		{
			switch (dataType)
			{
			case DataType.String:
				AddValue(string.Empty);
				break;
			case DataType.Integer:
			case DataType.Boolean:
			case DataType.Enum:
			case DataType.Toggle:
			case DataType.Layer:
				AddValue(0);
				break;
			case DataType.Float:
				AddValue(0f);
				break;
			case DataType.Vector2:
				AddValue(Vector2.zero);
				break;
			case DataType.Vector3:
				AddValue(Vector3.zero);
				break;
			case DataType.Vector4:
				AddValue(Vector4.zero);
				break;
			case DataType.Rect:
				AddValue(RectExt.zero);
				break;
			case DataType.Color:
				AddValue(Color.black);
				break;
			case DataType.Object:
				AddValue((UnityEngine.Object)null);
				break;
			case DataType.Tag:
				AddValue("Untagged");
				break;
			case DataType.AnimationCurve:
				AddValue(new AnimationCurve());
				break;
			case DataType.Bounds:
				AddValue(BoundsExt.zero);
				break;
			case DataType.Quaternion:
				AddValue(Quaternion.identity);
				break;
			}
		}

		public void InsertItemAt(int index)
		{
			switch (dataType)
			{
			case DataType.String:
				InsertValueAt(string.Empty, index);
				break;
			case DataType.Integer:
			case DataType.Boolean:
			case DataType.Enum:
			case DataType.Toggle:
			case DataType.Layer:
				InsertValueAt(0, index);
				break;
			case DataType.Float:
				InsertValueAt(0f, index);
				break;
			case DataType.Vector2:
				InsertValueAt(Vector2.zero, index);
				break;
			case DataType.Vector3:
				InsertValueAt(Vector3.zero, index);
				break;
			case DataType.Vector4:
				InsertValueAt(Vector4.zero, index);
				break;
			case DataType.Rect:
				InsertValueAt(RectExt.zero, index);
				break;
			case DataType.Color:
				InsertValueAt(Color.black, index);
				break;
			case DataType.Object:
				InsertValueAt((UnityEngine.Object)null, index);
				break;
			case DataType.Tag:
				InsertValueAt("Untagged", index);
				break;
			case DataType.AnimationCurve:
				InsertValueAt(new AnimationCurve(), index);
				break;
			case DataType.Bounds:
				InsertValueAt(BoundsExt.zero, index);
				break;
			case DataType.Quaternion:
				InsertValueAt(Quaternion.identity, index);
				break;
			}
		}

		public void RemoveValueAt(int index)
		{
			RemoveValueAt(index, fireEvent: true);
		}

		private void RemoveValueAt(int index, bool fireEvent)
		{
			if (proxy != null)
			{
				proxy.RemoveValueAt(this, index);
			}
			else
			{
				index -= BASE_INDEX;
				switch (dataType)
				{
				case DataType.String:
				case DataType.Tag:
					_stringValues.RemoveAt(index);
					break;
				case DataType.Integer:
				case DataType.Boolean:
				case DataType.Enum:
				case DataType.Toggle:
				case DataType.Layer:
					_intValues.RemoveAt(index);
					break;
				case DataType.Float:
					_floatValues.RemoveAt(index);
					break;
				case DataType.Vector2:
					_vector2Values.RemoveAt(index);
					break;
				case DataType.Vector3:
					_vector3Values.RemoveAt(index);
					break;
				case DataType.Vector4:
					_vector4Values.RemoveAt(index);
					break;
				case DataType.Rect:
					_rectValues.RemoveAt(index);
					break;
				case DataType.Color:
					_colorValues.RemoveAt(index);
					break;
				case DataType.Object:
				{
					ScriptableObject scriptableObject = _objectValues[index] as ScriptableObject;
					if (scriptableObject != null)
					{
						UnityEngine.Object.Destroy(scriptableObject);
					}
					_objectValues.RemoveAt(index);
					break;
				}
				case DataType.AnimationCurve:
					_curveValues.RemoveAt(index);
					break;
				case DataType.Bounds:
					_boundsValues.RemoveAt(index);
					break;
				case DataType.Quaternion:
					_quaternionValues.RemoveAt(index);
					break;
				}
				_variables.RemoveAt(index);
			}
			if (fireEvent)
			{
				OnModify();
			}
		}

		public void Clear()
		{
			if (proxy != null)
			{
				proxy.Clear(this);
			}
			else
			{
				switch (dataType)
				{
				case DataType.String:
				case DataType.Tag:
					_stringValues.Clear();
					break;
				case DataType.Integer:
				case DataType.Boolean:
				case DataType.Enum:
				case DataType.Toggle:
				case DataType.Layer:
					_intValues.Clear();
					break;
				case DataType.Float:
					_floatValues.Clear();
					break;
				case DataType.Vector2:
					_vector2Values.Clear();
					break;
				case DataType.Vector3:
					_vector3Values.Clear();
					break;
				case DataType.Vector4:
					_vector4Values.Clear();
					break;
				case DataType.Rect:
					_rectValues.Clear();
					break;
				case DataType.Color:
					_colorValues.Clear();
					break;
				case DataType.Object:
				{
					for (int i = 0; i < _objectValues.Count; i++)
					{
						ScriptableObject scriptableObject = _objectValues[i] as ScriptableObject;
						if (scriptableObject != null)
						{
							UnityEngine.Object.Destroy(scriptableObject);
						}
					}
					_objectValues.Clear();
					break;
				}
				case DataType.AnimationCurve:
					_curveValues.Clear();
					break;
				case DataType.Bounds:
					_boundsValues.Clear();
					break;
				case DataType.Quaternion:
					_quaternionValues.Clear();
					break;
				}
				_variables.Clear();
			}
			OnModify();
		}

		public void ClearNulls()
		{
			if (proxy != null)
			{
				proxy.ClearNulls(this);
			}
			else
			{
				int num = Count;
				for (int i = 0; i < num; i++)
				{
					if (GetObjectAt(i) == null)
					{
						RemoveValueAt(i);
						num--;
						i--;
					}
				}
			}
			OnModify();
		}

		public void Remove(string value, bool removeAll = false)
		{
			bool flag = false;
			for (int num = IndexOf(value); num >= 0; num = IndexOf(value, num))
			{
				RemoveValueAt(num, fireEvent: false);
				flag = true;
				if (!removeAll || num >= Count)
				{
					break;
				}
			}
			if (flag)
			{
				OnModify();
			}
		}

		public void Remove(int value, bool removeAll = false)
		{
			bool flag = false;
			for (int num = IndexOf(value); num >= 0; num = IndexOf(value, num))
			{
				RemoveValueAt(num, fireEvent: false);
				flag = true;
				if (!removeAll || num >= Count)
				{
					break;
				}
			}
			if (flag)
			{
				OnModify();
			}
		}

		public void Remove(float value, bool removeAll = false)
		{
			bool flag = false;
			for (int num = IndexOf(value); num >= 0; num = IndexOf(value, num))
			{
				RemoveValueAt(num, fireEvent: false);
				flag = true;
				if (!removeAll || num >= Count)
				{
					break;
				}
			}
			if (flag)
			{
				OnModify();
			}
		}

		public void Remove(bool value, bool removeAll = false)
		{
			bool flag = false;
			for (int num = IndexOf(value); num >= 0; num = IndexOf(value, num))
			{
				RemoveValueAt(num, fireEvent: false);
				flag = true;
				if (!removeAll || num >= Count)
				{
					break;
				}
			}
			if (flag)
			{
				OnModify();
			}
		}

		public void Remove(Vector2 value, bool removeAll = false)
		{
			bool flag = false;
			for (int num = IndexOf(value); num >= 0; num = IndexOf(value, num))
			{
				RemoveValueAt(num, fireEvent: false);
				flag = true;
				if (!removeAll || num >= Count)
				{
					break;
				}
			}
			if (flag)
			{
				OnModify();
			}
		}

		public void Remove(Vector3 value, bool removeAll = false)
		{
			bool flag = false;
			for (int num = IndexOf(value); num >= 0; num = IndexOf(value, num))
			{
				RemoveValueAt(num, fireEvent: false);
				flag = true;
				if (!removeAll || num >= Count)
				{
					break;
				}
			}
			if (flag)
			{
				OnModify();
			}
		}

		public void Remove(Vector4 value, bool removeAll = false)
		{
			bool flag = false;
			for (int num = IndexOf(value); num >= 0; num = IndexOf(value, num))
			{
				RemoveValueAt(num, fireEvent: false);
				flag = true;
				if (!removeAll || num >= Count)
				{
					break;
				}
			}
			if (flag)
			{
				OnModify();
			}
		}

		public void Remove(Rect value, bool removeAll = false)
		{
			bool flag = false;
			for (int num = IndexOf(value); num >= 0; num = IndexOf(value, num))
			{
				RemoveValueAt(num, fireEvent: false);
				flag = true;
				if (!removeAll || num >= Count)
				{
					break;
				}
			}
			if (flag)
			{
				OnModify();
			}
		}

		public void Remove(Color value, bool removeAll = false)
		{
			bool flag = false;
			for (int num = IndexOf(value); num >= 0; num = IndexOf(value, num))
			{
				RemoveValueAt(num, fireEvent: false);
				flag = true;
				if (!removeAll || num >= Count)
				{
					break;
				}
			}
			if (flag)
			{
				OnModify();
			}
		}

		public void Remove(UnityEngine.Object value, bool removeAll = false)
		{
			bool flag = false;
			for (int num = IndexOf(value); num >= 0; num = IndexOf(value, num))
			{
				RemoveValueAt(num, fireEvent: false);
				flag = true;
				if (!removeAll || num >= Count)
				{
					break;
				}
			}
			if (flag)
			{
				OnModify();
			}
		}

		public void Remove(Enum value, bool removeAll = false)
		{
			bool flag = false;
			for (int num = IndexOf(value); num >= 0; num = IndexOf(value, num))
			{
				RemoveValueAt(num, fireEvent: false);
				flag = true;
				if (!removeAll || num >= Count)
				{
					break;
				}
			}
			if (flag)
			{
				OnModify();
			}
		}

		public void Remove(AnimationCurve value, bool removeAll = false)
		{
			bool flag = false;
			for (int num = IndexOf(value); num >= 0; num = IndexOf(value, num))
			{
				RemoveValueAt(num, fireEvent: false);
				flag = true;
				if (!removeAll || num >= Count)
				{
					break;
				}
			}
			if (flag)
			{
				OnModify();
			}
		}

		public void Remove(Bounds value, bool removeAll = false)
		{
			bool flag = false;
			for (int num = IndexOf(value); num >= 0; num = IndexOf(value, num))
			{
				RemoveValueAt(num, fireEvent: false);
				flag = true;
				if (!removeAll || num >= Count)
				{
					break;
				}
			}
			if (flag)
			{
				OnModify();
			}
		}

		public void Remove(Quaternion value, bool removeAll = false)
		{
			bool flag = false;
			for (int num = IndexOf(value); num >= 0; num = IndexOf(value, num))
			{
				RemoveValueAt(num, fireEvent: false);
				flag = true;
				if (!removeAll || num >= Count)
				{
					break;
				}
			}
			if (flag)
			{
				OnModify();
			}
		}

		private void CastTo(Type type)
		{
			_typeName = ((type == null) ? defaultType.FullName : type.FullName);
			_dataType = ((type == null) ? defaultType : type).ToDataType();
			Clear();
			OnModify();
		}

		public void SwapValues(int i, int j)
		{
			if (proxy != null)
			{
				proxy.SwapValues(this, i, j);
			}
			else
			{
				switch (dataType)
				{
				case DataType.String:
				case DataType.Tag:
					_stringValues.Swap(i, j);
					_variables.Swap(i, j);
					break;
				case DataType.Integer:
				case DataType.Boolean:
				case DataType.Enum:
				case DataType.Toggle:
				case DataType.Layer:
					_intValues.Swap(i, j);
					_variables.Swap(i, j);
					break;
				case DataType.Float:
					_floatValues.Swap(i, j);
					_variables.Swap(i, j);
					break;
				case DataType.Vector2:
					_vector2Values.Swap(i, j);
					_variables.Swap(i, j);
					break;
				case DataType.Vector3:
					_vector3Values.Swap(i, j);
					_variables.Swap(i, j);
					break;
				case DataType.Vector4:
					_vector4Values.Swap(i, j);
					_variables.Swap(i, j);
					break;
				case DataType.Rect:
					_rectValues.Swap(i, j);
					_variables.Swap(i, j);
					break;
				case DataType.Color:
					_colorValues.Swap(i, j);
					_variables.Swap(i, j);
					break;
				case DataType.Object:
					_objectValues.Swap(i, j);
					_variables.Swap(i, j);
					break;
				case DataType.AnimationCurve:
					_curveValues.Swap(i, j);
					_variables.Swap(i, j);
					break;
				case DataType.Bounds:
					_boundsValues.Swap(i, j);
					_variables.Swap(i, j);
					break;
				case DataType.Quaternion:
					_quaternionValues.Swap(i, j);
					_variables.Swap(i, j);
					break;
				}
			}
			OnModify();
		}
	}
}
