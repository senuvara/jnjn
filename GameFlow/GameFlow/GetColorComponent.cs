using UnityEngine;

namespace GameFlow
{
	[AddComponentMenu("")]
	[Help("en", "Gets a specified component of the given color.", null)]
	[Help("es", "Devuelve la componente especificada del color indicado.", null)]
	public class GetColorComponent : Action, IExecutableInEditor
	{
		public enum OutputType
		{
			Integer,
			Float
		}

		[SerializeField]
		private Color _color;

		[SerializeField]
		private Variable _colorVar;

		[SerializeField]
		private ColorComponent _component;

		[SerializeField]
		private Variable _componentVar;

		[SerializeField]
		private OutputType _outputType;

		[SerializeField]
		private Variable _output;

		protected Color color => _colorVar.GetValue(_color);

		protected ColorComponent component => (ColorComponent)(object)_componentVar.GetValue(_component);

		public override void Execute()
		{
			if (!(_output == null))
			{
				if (_outputType == OutputType.Integer)
				{
					_output.SetValue(GetInteger(color, component));
				}
				else if (_outputType == OutputType.Float)
				{
					_output.SetValue(GetFloat(color, component));
				}
			}
		}

		public static int GetInteger(Color color, ColorComponent component)
		{
			switch (component)
			{
			case ColorComponent.R:
				return Mathf.RoundToInt(color.r * 255f);
			case ColorComponent.G:
				return Mathf.RoundToInt(color.g * 255f);
			case ColorComponent.B:
				return Mathf.RoundToInt(color.b * 255f);
			case ColorComponent.A:
				return Mathf.RoundToInt(color.a * 255f);
			default:
				return 0;
			}
		}

		public static float GetFloat(Color color, ColorComponent component)
		{
			switch (component)
			{
			case ColorComponent.R:
				return color.r;
			case ColorComponent.G:
				return color.g;
			case ColorComponent.B:
				return color.b;
			case ColorComponent.A:
				return color.a;
			default:
				return 0f;
			}
		}
	}
}
