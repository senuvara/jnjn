using UnityEngine;

namespace GameFlow
{
	[Help("es", "Modifica una propiedad del componente @UnityManual{Rigidbody} especificado.", null)]
	[Help("en", "Sets a property of the specified @UnityManual{Rigidbody} component.", null)]
	[AddComponentMenu("")]
	public class SetRigidbodyProperty : Action, IExecutableInEditor
	{
		public enum Property
		{
			AngularDrag,
			AngularVelocity,
			CenterOfMass,
			CollisionDetectionMode,
			Constraints,
			DetectCollisions,
			Density,
			Drag,
			FreezeRotation,
			InertiaTensor,
			InertiaTensorRotation,
			Interpolation,
			IsKinematic,
			Mass,
			MaxAngularVelocity,
			Position,
			Rotation,
			SolverIterations,
			UseConeFriction,
			UseGravity,
			Velocity
		}

		[SerializeField]
		private Rigidbody _target;

		[SerializeField]
		private Variable _targetVar;

		[SerializeField]
		private Property _property;

		[SerializeField]
		private int _intValue;

		[SerializeField]
		private float _floatValue;

		[SerializeField]
		private bool _boolValue;

		[SerializeField]
		private Vector3 _vector3Value;

		[SerializeField]
		private CollisionDetectionMode _collisionDetectionModeValue;

		[SerializeField]
		private int _rigidbodyConstraintsValue;

		[SerializeField]
		private RigidbodyInterpolation _rigidbodyInterpolationValue;

		[SerializeField]
		private Variable _valueVar;

		protected override void OnReset()
		{
			_target = GetComponent<Rigidbody>();
		}

		public override void Setup()
		{
		}

		public override void Execute()
		{
			Rigidbody rigidbody = _target;
			if (rigidbody == null)
			{
				rigidbody = (_targetVar.GetValueAsComponent(_target, typeof(Rigidbody)) as Rigidbody);
			}
			if (!(rigidbody == null))
			{
				switch (_property)
				{
				case Property.UseConeFriction:
					break;
				case Property.AngularDrag:
					rigidbody.angularDrag = _valueVar.GetValue(_floatValue);
					break;
				case Property.AngularVelocity:
					rigidbody.angularVelocity = _valueVar.GetValue(_vector3Value);
					break;
				case Property.CenterOfMass:
					rigidbody.centerOfMass = _valueVar.GetValue(_vector3Value);
					break;
				case Property.CollisionDetectionMode:
					rigidbody.collisionDetectionMode = (CollisionDetectionMode)(object)_valueVar.GetValue(_collisionDetectionModeValue);
					break;
				case Property.Constraints:
					rigidbody.constraints = (RigidbodyConstraints)_valueVar.GetValue(_rigidbodyConstraintsValue);
					break;
				case Property.DetectCollisions:
					rigidbody.detectCollisions = _valueVar.GetValue(_boolValue);
					break;
				case Property.Density:
					rigidbody.SetDensity(_valueVar.GetValue(_floatValue));
					break;
				case Property.Drag:
					rigidbody.drag = _valueVar.GetValue(_floatValue);
					break;
				case Property.FreezeRotation:
					rigidbody.freezeRotation = _valueVar.GetValue(_boolValue);
					break;
				case Property.InertiaTensor:
					rigidbody.inertiaTensor = _valueVar.GetValue(_vector3Value);
					break;
				case Property.InertiaTensorRotation:
					rigidbody.inertiaTensorRotation = Quaternion.Euler(_valueVar.GetValue(_vector3Value));
					break;
				case Property.Interpolation:
					rigidbody.interpolation = (RigidbodyInterpolation)(object)_valueVar.GetValue(_rigidbodyInterpolationValue);
					break;
				case Property.IsKinematic:
					rigidbody.isKinematic = _valueVar.GetValue(_boolValue);
					break;
				case Property.Mass:
					rigidbody.mass = _valueVar.GetValue(_floatValue);
					break;
				case Property.MaxAngularVelocity:
					rigidbody.maxAngularVelocity = _valueVar.GetValue(_floatValue);
					break;
				case Property.Position:
					rigidbody.position = _valueVar.GetValue(_vector3Value);
					break;
				case Property.Rotation:
					rigidbody.rotation = Quaternion.Euler(_valueVar.GetValue(_vector3Value));
					break;
				case Property.SolverIterations:
					rigidbody.solverIterations = _valueVar.GetValue(_intValue);
					break;
				case Property.UseGravity:
					rigidbody.useGravity = _valueVar.GetValue(_boolValue);
					break;
				case Property.Velocity:
					rigidbody.velocity = _valueVar.GetValue(_vector3Value);
					break;
				}
			}
		}
	}
}
