using UnityEngine;

namespace GameFlow
{
	[Help("es", "Modifica una propiedad del componente @UnityManual{Rigidbody2D} especificado.", null)]
	[AddComponentMenu("")]
	[Help("en", "Sets a property of the specified @UnityManual{Rigidbody2D} component.", null)]
	public class SetRigidbody2DProperty : Action, IExecutableInEditor
	{
		public enum Property
		{
			AngularDrag,
			AngularVelocity,
			CenterOfMass,
			CollisionDetectionMode,
			Constraints,
			Drag,
			FreezeRotation,
			GravityScale,
			Inertia,
			Interpolation,
			IsKinematic,
			Mass,
			Position,
			Rotation,
			Simulated,
			SleepMode,
			Velocity
		}

		[SerializeField]
		private Rigidbody2D _target;

		[SerializeField]
		private Variable _targetVar;

		[SerializeField]
		private Property _property;

		[SerializeField]
		private float _angularDragValue;

		[SerializeField]
		private float _angularVelocityValue;

		[SerializeField]
		private Vector2 _centerOfMassValue;

		[SerializeField]
		private CollisionDetectionMode2D _collisionDetectionModeValue;

		[SerializeField]
		private int _constraintsValue;

		[SerializeField]
		private float _dragValue;

		[SerializeField]
		private bool _freezeRotationValue;

		[SerializeField]
		private float _gravityScaleValue;

		[SerializeField]
		private float _inertiaValue;

		[SerializeField]
		private RigidbodyInterpolation2D _interpolationValue;

		[SerializeField]
		private bool _isKinematicValue;

		[SerializeField]
		private float _massValue;

		[SerializeField]
		private Vector2 _positionValue;

		[SerializeField]
		private float _rotationValue;

		[SerializeField]
		private bool _simulatedValue;

		[SerializeField]
		private RigidbodySleepMode2D _sleepModeValue;

		[SerializeField]
		private Vector2 _velocityValue;

		[SerializeField]
		private Variable _valueVar;

		protected override void OnReset()
		{
			_target = base.gameObject.GetComponent<Rigidbody2D>();
		}

		public override void Setup()
		{
		}

		public override void Execute()
		{
			Rigidbody2D rigidbody2D = _targetVar.GetValueAsComponent(_target, typeof(Rigidbody2D)) as Rigidbody2D;
			if (!(rigidbody2D == null))
			{
				switch (_property)
				{
				case Property.AngularDrag:
					rigidbody2D.angularDrag = _valueVar.GetValue(_angularDragValue);
					break;
				case Property.AngularVelocity:
					rigidbody2D.angularVelocity = _valueVar.GetValue(_angularVelocityValue);
					break;
				case Property.CenterOfMass:
					rigidbody2D.centerOfMass = _valueVar.GetValue(_centerOfMassValue);
					break;
				case Property.CollisionDetectionMode:
					rigidbody2D.collisionDetectionMode = (CollisionDetectionMode2D)(object)_valueVar.GetValue(_collisionDetectionModeValue);
					break;
				case Property.Constraints:
					rigidbody2D.constraints = (RigidbodyConstraints2D)_valueVar.GetValue(_constraintsValue);
					break;
				case Property.Drag:
					rigidbody2D.drag = _valueVar.GetValue(_dragValue);
					break;
				case Property.FreezeRotation:
					rigidbody2D.freezeRotation = _valueVar.GetValue(_freezeRotationValue);
					break;
				case Property.GravityScale:
					rigidbody2D.gravityScale = _valueVar.GetValue(_gravityScaleValue);
					break;
				case Property.Inertia:
					rigidbody2D.inertia = _valueVar.GetValue(_inertiaValue);
					break;
				case Property.Interpolation:
					rigidbody2D.interpolation = (RigidbodyInterpolation2D)(object)_valueVar.GetValue(_interpolationValue);
					break;
				case Property.IsKinematic:
					rigidbody2D.isKinematic = _valueVar.GetValue(_isKinematicValue);
					break;
				case Property.Mass:
					rigidbody2D.mass = _valueVar.GetValue(_massValue);
					break;
				case Property.Position:
					rigidbody2D.position = _valueVar.GetValue(_positionValue);
					break;
				case Property.Rotation:
					rigidbody2D.rotation = _valueVar.GetValue(_rotationValue);
					break;
				case Property.Simulated:
					rigidbody2D.simulated = _valueVar.GetValue(_simulatedValue);
					break;
				case Property.SleepMode:
					rigidbody2D.sleepMode = (RigidbodySleepMode2D)(object)_valueVar.GetValue(_sleepModeValue);
					break;
				case Property.Velocity:
					rigidbody2D.velocity = _valueVar.GetValue(_velocityValue);
					break;
				}
			}
		}
	}
}
