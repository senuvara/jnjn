using System;
using UnityEngine;

namespace GameFlow
{
	[AddComponentMenu("")]
	[Help("es", "Elimina un elemento de la @GameFlow{List} especificada.", null)]
	[Help("en", "Removes an item from the specified @GameFlow{List}.", null)]
	public class RemoveItemFromList : ListAction
	{
		public enum RemovalType
		{
			ItemAt,
			FirstOccurrenceOf,
			AllOccurrencesOf
		}

		[SerializeField]
		private RemovalType _removal;

		public override void Execute()
		{
			List list = GetList();
			if (!(list != null))
			{
				return;
			}
			switch (_removal)
			{
			case RemovalType.ItemAt:
			{
				int index = GetIndex();
				if (list.IsIndexValid(index))
				{
					list.RemoveValueAt(index);
				}
				break;
			}
			case RemovalType.FirstOccurrenceOf:
			case RemovalType.AllOccurrencesOf:
			{
				bool removeAll = _removal == RemovalType.AllOccurrencesOf;
				switch (list.dataType)
				{
				case DataType.String:
				case DataType.Tag:
					list.Remove(base.stringValue, removeAll);
					break;
				case DataType.Integer:
				case DataType.Layer:
					list.Remove(base.intValue, removeAll);
					break;
				case DataType.Float:
					list.Remove(base.floatValue, removeAll);
					break;
				case DataType.Boolean:
				case DataType.Toggle:
					list.Remove(base.boolValue, removeAll);
					break;
				case DataType.Vector2:
					list.Remove(base.vector2Value, removeAll);
					break;
				case DataType.Vector3:
					list.Remove(base.vector3Value, removeAll);
					break;
				case DataType.Vector4:
					list.Remove(base.vector4Value, removeAll);
					break;
				case DataType.Rect:
					list.Remove(base.rectValue, removeAll);
					break;
				case DataType.Color:
					list.Remove(base.colorValue, removeAll);
					break;
				case DataType.Object:
					list.Remove(base.objectValue, removeAll);
					break;
				case DataType.Enum:
					list.Remove((Enum)Enum.ToObject(list.type, base.enumIntValue), removeAll);
					break;
				case DataType.AnimationCurve:
					list.Remove(base.animationCurveValue, removeAll);
					break;
				case DataType.Bounds:
					list.Remove(base.boundsValue, removeAll);
					break;
				case DataType.Quaternion:
					list.Remove(base.quaternionValue, removeAll);
					break;
				}
				break;
			}
			}
		}
	}
}
