using UnityEngine;

namespace GameFlow
{
	[Help("es", "Dibuja el @UnityManual{Collider} especificado.", null)]
	[Help("en", "Draws the specified @UnityManual{Collider}.", null)]
	[AddComponentMenu("")]
	public class DrawCollider : DrawGizmosAction
	{
		[SerializeField]
		private Collider _collider;

		[SerializeField]
		private Variable _colliderVar;

		[SerializeField]
		private Color _selectedColor = Color.clear;

		[SerializeField]
		private Variable _selectedColorVar;

		[SerializeField]
		private Color _unselectedColor = Color.white;

		[SerializeField]
		private Variable _unselectedColorVar;

		public Collider target => _colliderVar.GetValue(_collider) as Collider;

		public Color selectedColor => _selectedColorVar.GetValue(_selectedColor);

		public Color unselectedColor => _unselectedColorVar.GetValue(_unselectedColor);

		protected override void OnReset()
		{
			_collider = base.gameObject.GetComponent<Collider>();
		}
	}
}
