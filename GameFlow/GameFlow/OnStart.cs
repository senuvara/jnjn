using UnityEngine;

namespace GameFlow
{
	[DisallowMultipleComponent]
	[AddComponentMenu("GameFlow/Programs/On Start")]
	[Help("en", "Program that will be executed once the application starts if the @UnityManual{GameObject} is activated or, if it is not activated, the first time it gets activated.", null)]
	[Help("es", "Programa que se ejecutará cuando la aplicación arranque si el @UnityManual{GameObject} está activado o, si no está activado, la primera vez que se active.", null)]
	public class OnStart : Program
	{
		private void Start()
		{
			Restart();
		}
	}
}
