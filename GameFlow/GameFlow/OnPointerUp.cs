using UnityEngine;

namespace GameFlow
{
	[DisallowMultipleComponent]
	[Help("es", "Programa que se ejecutará cuando el puntero deje de estar presionado en el área de un componente seleccionable UI en el rango de escucha.", null)]
	[AddComponentMenu("GameFlow/Programs/On Pointer Up")]
	[Help("en", "Program that will be executed when the pointer stops being pressed in the area of a seletable UI component in the listening range.", null)]
	public class OnPointerUp : PointerEventProgram
	{
		private EventController[] _controllers;

		protected override void RegisterAsListener()
		{
			_controllers = RegisterAsListener(typeof(PointerUpEvent), typeof(UIEventController), UIEventController.AddController);
			if (base.listeningRange == EventListeningRange.SceneHierarchy)
			{
				PointerUpEvent.AddListener(this);
			}
		}

		protected override void UnRegisterAsListener()
		{
			PointerUpEvent.RemoveListener(this);
			UnRegisterAsListener(typeof(PointerUpEvent), typeof(UIEventController));
		}

		protected override void SetParameters(EventObject e)
		{
			PointerUpEvent pointerUpEvent = e as PointerUpEvent;
			base.sourceParam.SetValue(pointerUpEvent.source);
			base.positionParam.SetValue(pointerUpEvent.position);
		}

		private void OnTransformChildrenChanged()
		{
			if (base.listeningRange == EventListeningRange.GameObjectHierarchy)
			{
				UnsubscribeFrom(_controllers, typeof(PointerUpEvent));
				_controllers = EventController.GetControllersInHierarchyOf(typeof(UIEventController), base.listeningTarget, UIEventController.AddController);
				SubscribeTo(_controllers, typeof(PointerUpEvent));
			}
		}
	}
}
