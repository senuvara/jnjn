using UnityEngine;

namespace GameFlow
{
	[Help("es", "Reinicia y detiene el temporizador (@GameFlow{Timer}) especificado.", null)]
	[Help("en", "Resets and stops the specified @GameFlow{Timer}.", null)]
	[AddComponentMenu("")]
	public class ResetTimer : Action
	{
		[SerializeField]
		private Timer _target;

		[SerializeField]
		private Variable _targetVar;

		public override void Execute()
		{
			Timer timer = _targetVar.GetValue(_target) as Timer;
			if (timer != null)
			{
				timer.Stop(doReset: true);
			}
		}
	}
}
