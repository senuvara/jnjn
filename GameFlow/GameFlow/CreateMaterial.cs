using UnityEngine;

namespace GameFlow
{
	[Help("es", "Crea un nuevo @UnityManual{Material} asignándole el @UnityManual{Shader} especificado.", null)]
	[AddComponentMenu("")]
	[Help("en", "Creates a new @UnityManual{Material} with the specified @UnityManual{Shader} assigned.", null)]
	public class CreateMaterial : Action, IExecutableInEditor
	{
		[SerializeField]
		private string _name = "New Material";

		[SerializeField]
		private Variable _nameVar;

		[SerializeField]
		private Shader _shader;

		[SerializeField]
		private Variable _shaderVar;

		[SerializeField]
		private Variable _output;

		public string shaderName => _nameVar.GetValue(_name);

		public Shader shader => _shaderVar.GetValue(_shader) as Shader;

		public Variable output => _output;

		protected override void OnReset()
		{
			_shader = Shader.Find("Standard");
		}

		public override void Step()
		{
			Shader shader = this.shader;
			if (shader == null)
			{
				shader = Shader.Find("Standard");
			}
			Material material = new Material(shader);
			string shaderName = this.shaderName;
			if (shaderName != null && shaderName.Length > 0)
			{
				material.name = shaderName;
			}
			if (_output != null)
			{
				_output.SetValue(material);
			}
		}
	}
}
