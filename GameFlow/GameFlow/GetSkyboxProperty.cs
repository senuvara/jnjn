using UnityEngine;

namespace GameFlow
{
	[AddComponentMenu("")]
	[Help("en", "Gets a property of the specified @UnityManual{Skybox} component.", null)]
	[Help("es", "Lee una propiedad del componente @UnityManual{Skybox} especificado.", null)]
	public class GetSkyboxProperty : Action, IExecutableInEditor
	{
		public enum Property
		{
			Material
		}

		[SerializeField]
		private Skybox _source;

		[SerializeField]
		private Variable _sourceVar;

		[SerializeField]
		private Property _property;

		[SerializeField]
		private Variable _output;

		protected override void OnReset()
		{
			_source = base.gameObject.GetComponent<Skybox>();
		}

		public override void Setup()
		{
		}

		public override void Execute()
		{
			Skybox skybox = _sourceVar.GetValueAsComponent(_source, typeof(Skybox)) as Skybox;
			if (!(skybox == null) && _property == Property.Material)
			{
				_output.SetValue(skybox.material);
			}
		}
	}
}
