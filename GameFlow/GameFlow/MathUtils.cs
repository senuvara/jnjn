using System;
using UnityEngine;

namespace GameFlow
{
	public class MathUtils
	{
		public static float GetSumOf(float[] values)
		{
			float num = 0f;
			for (int i = 0; i < values.Length; i++)
			{
				num += values[i];
			}
			return num;
		}

		public static int GetSumOf(int[] values)
		{
			int num = 0;
			for (int i = 0; i < values.Length; i++)
			{
				num += values[i];
			}
			return num;
		}

		public static float Clamp(float value, float min, float max, bool angles = false)
		{
			if (angles)
			{
				value = AbsAngle(value);
				min = AbsAngle(min);
				max = AbsAngle(max);
				float num = Mathf.Abs(value - min);
				num = ((!(num > 180f)) ? num : Mathf.Abs(360f - num));
				float num2 = Mathf.Abs(value - max);
				num2 = ((!(num2 > 180f)) ? num2 : Mathf.Abs(360f - num2));
				if (min < max)
				{
					if (value > max || value < min)
					{
						value = ((!(num2 <= num)) ? min : max);
					}
				}
				else if (value > max && value < min)
				{
					value = ((!(num2 <= num)) ? min : max);
				}
				return value;
			}
			return Mathf.Clamp(value, min, max);
		}

		public static float AbsAngle(float angle)
		{
			angle %= 360f;
			angle = ((!(angle < 0f)) ? angle : (360f + angle));
			return angle;
		}

		private static float Angle(float a, float b)
		{
			float num = Mathf.Abs(AbsAngle(b) - AbsAngle(a));
			return (!(num > 180f)) ? num : Mathf.Abs(360f - num);
		}

		public static float Ease(float start, float end, float t, EasingType easing)
		{
			switch (easing)
			{
			case EasingType.Linear:
				return Mathf.Lerp(start, end, t);
			case EasingType.EaseInSine:
				end = EaseInSine(start, end, t);
				break;
			case EasingType.EaseOutSine:
				end = EaseOutSine(start, end, t);
				break;
			case EasingType.EaseInOutSine:
				end = EaseInOutSine(start, end, t);
				break;
			case EasingType.EaseInQuadratic:
				end = EaseInQuadratic(start, end, t);
				break;
			case EasingType.EaseOutQuadratic:
				end = EaseOutQuadratic(start, end, t);
				break;
			case EasingType.EaseInOutQuadratic:
				end = EaseInOutQuadratic(start, end, t);
				break;
			case EasingType.EaseInCubic:
				end = EaseInCubic(start, end, t);
				break;
			case EasingType.EaseOutCubic:
				end = EaseOutCubic(start, end, t);
				break;
			case EasingType.EaseInOutCubic:
				end = EaseInOutCubic(start, end, t);
				break;
			case EasingType.EaseInQuartic:
				end = EaseInQuartic(start, end, t);
				break;
			case EasingType.EaseOutQuartic:
				end = EaseOutQuartic(start, end, t);
				break;
			case EasingType.EaseInOutQuartic:
				end = EaseInOutQuartic(start, end, t);
				break;
			case EasingType.EaseInQuintic:
				end = EaseInQuintic(start, end, t);
				break;
			case EasingType.EaseOutQuintic:
				end = EaseOutQuintic(start, end, t);
				break;
			case EasingType.EaseInOutQuintic:
				end = EaseInOutQuintic(start, end, t);
				break;
			}
			return end;
		}

		public static Vector2 Ease(Vector2 start, Vector2 end, float t, EasingType easing)
		{
			switch (easing)
			{
			case EasingType.Linear:
				return Vector2.Lerp(start, end, t);
			case EasingType.EaseInSine:
				end.x = EaseInSine(start.x, end.x, t);
				end.y = EaseInSine(start.y, end.y, t);
				break;
			case EasingType.EaseOutSine:
				end.x = EaseOutSine(start.x, end.x, t);
				end.y = EaseOutSine(start.y, end.y, t);
				break;
			case EasingType.EaseInOutSine:
				end.x = EaseInOutSine(start.x, end.x, t);
				end.y = EaseInOutSine(start.y, end.y, t);
				break;
			case EasingType.EaseInQuadratic:
				end.x = EaseInQuadratic(start.x, end.x, t);
				end.y = EaseInQuadratic(start.y, end.y, t);
				break;
			case EasingType.EaseOutQuadratic:
				end.x = EaseOutQuadratic(start.x, end.x, t);
				end.y = EaseOutQuadratic(start.y, end.y, t);
				break;
			case EasingType.EaseInOutQuadratic:
				end.x = EaseInOutQuadratic(start.x, end.x, t);
				end.y = EaseInOutQuadratic(start.y, end.y, t);
				break;
			case EasingType.EaseInCubic:
				end.x = EaseInCubic(start.x, end.x, t);
				end.y = EaseInCubic(start.y, end.y, t);
				break;
			case EasingType.EaseOutCubic:
				end.x = EaseOutCubic(start.x, end.x, t);
				end.y = EaseOutCubic(start.y, end.y, t);
				break;
			case EasingType.EaseInOutCubic:
				end.x = EaseInOutCubic(start.x, end.x, t);
				end.y = EaseInOutCubic(start.y, end.y, t);
				break;
			case EasingType.EaseInQuartic:
				end.x = EaseInQuartic(start.x, end.x, t);
				end.y = EaseInQuartic(start.y, end.y, t);
				break;
			case EasingType.EaseOutQuartic:
				end.x = EaseOutQuartic(start.x, end.x, t);
				end.y = EaseOutQuartic(start.y, end.y, t);
				break;
			case EasingType.EaseInOutQuartic:
				end.x = EaseInOutQuartic(start.x, end.x, t);
				end.y = EaseInOutQuartic(start.y, end.y, t);
				break;
			case EasingType.EaseInQuintic:
				end.x = EaseInQuintic(start.x, end.x, t);
				end.y = EaseInQuintic(start.y, end.y, t);
				break;
			case EasingType.EaseOutQuintic:
				end.x = EaseOutQuintic(start.x, end.x, t);
				end.y = EaseOutQuintic(start.y, end.y, t);
				break;
			case EasingType.EaseInOutQuintic:
				end.x = EaseInOutQuintic(start.x, end.x, t);
				end.y = EaseInOutQuintic(start.y, end.y, t);
				break;
			}
			return end;
		}

		public static Vector3 Ease(Vector3 start, Vector3 end, float t, EasingType easing)
		{
			switch (easing)
			{
			case EasingType.Linear:
				return Vector3.Lerp(start, end, t);
			case EasingType.EaseInSine:
				end.x = EaseInSine(start.x, end.x, t);
				end.y = EaseInSine(start.y, end.y, t);
				end.z = EaseInSine(start.z, end.z, t);
				break;
			case EasingType.EaseOutSine:
				end.x = EaseOutSine(start.x, end.x, t);
				end.y = EaseOutSine(start.y, end.y, t);
				end.z = EaseOutSine(start.z, end.z, t);
				break;
			case EasingType.EaseInOutSine:
				end.x = EaseInOutSine(start.x, end.x, t);
				end.y = EaseInOutSine(start.y, end.y, t);
				end.z = EaseInOutSine(start.z, end.z, t);
				break;
			case EasingType.EaseInQuadratic:
				end.x = EaseInQuadratic(start.x, end.x, t);
				end.y = EaseInQuadratic(start.y, end.y, t);
				end.z = EaseInQuadratic(start.z, end.z, t);
				break;
			case EasingType.EaseOutQuadratic:
				end.x = EaseOutQuadratic(start.x, end.x, t);
				end.y = EaseOutQuadratic(start.y, end.y, t);
				end.z = EaseOutCubic(start.z, end.z, t);
				break;
			case EasingType.EaseInOutQuadratic:
				end.x = EaseInOutQuadratic(start.x, end.x, t);
				end.y = EaseInOutQuadratic(start.y, end.y, t);
				end.z = EaseInOutQuadratic(start.z, end.z, t);
				break;
			case EasingType.EaseInCubic:
				end.x = EaseInCubic(start.x, end.x, t);
				end.y = EaseInCubic(start.y, end.y, t);
				end.z = EaseInCubic(start.z, end.z, t);
				break;
			case EasingType.EaseOutCubic:
				end.x = EaseOutCubic(start.x, end.x, t);
				end.y = EaseOutCubic(start.y, end.y, t);
				end.z = EaseOutCubic(start.z, end.z, t);
				break;
			case EasingType.EaseInOutCubic:
				end.x = EaseInOutCubic(start.x, end.x, t);
				end.y = EaseInOutCubic(start.y, end.y, t);
				end.z = EaseInOutCubic(start.z, end.z, t);
				break;
			case EasingType.EaseInQuartic:
				end.x = EaseInQuartic(start.x, end.x, t);
				end.y = EaseInQuartic(start.y, end.y, t);
				end.z = EaseInQuartic(start.z, end.z, t);
				break;
			case EasingType.EaseOutQuartic:
				end.x = EaseOutQuartic(start.x, end.x, t);
				end.y = EaseOutQuartic(start.y, end.y, t);
				end.z = EaseOutQuartic(start.z, end.z, t);
				break;
			case EasingType.EaseInOutQuartic:
				end.x = EaseInOutQuartic(start.x, end.x, t);
				end.y = EaseInOutQuartic(start.y, end.y, t);
				end.z = EaseInOutQuartic(start.z, end.z, t);
				break;
			case EasingType.EaseInQuintic:
				end.x = EaseInQuintic(start.x, end.x, t);
				end.y = EaseInQuintic(start.y, end.y, t);
				end.z = EaseInQuintic(start.z, end.z, t);
				break;
			case EasingType.EaseOutQuintic:
				end.x = EaseOutQuintic(start.x, end.x, t);
				end.y = EaseOutQuintic(start.y, end.y, t);
				end.z = EaseOutQuintic(start.z, end.z, t);
				break;
			case EasingType.EaseInOutQuintic:
				end.x = EaseInOutQuintic(start.x, end.x, t);
				end.y = EaseInOutQuintic(start.y, end.y, t);
				end.z = EaseInOutQuintic(start.z, end.z, t);
				break;
			}
			return end;
		}

		public static Vector4 Ease(Vector4 start, Vector4 end, float t, EasingType easing)
		{
			switch (easing)
			{
			case EasingType.Linear:
				return Vector4.Lerp(start, end, t);
			case EasingType.EaseInSine:
				end.x = EaseInSine(start.x, end.x, t);
				end.y = EaseInSine(start.y, end.y, t);
				end.z = EaseInSine(start.z, end.z, t);
				end.w = EaseInSine(start.w, end.w, t);
				break;
			case EasingType.EaseOutSine:
				end.x = EaseOutSine(start.x, end.x, t);
				end.y = EaseOutSine(start.y, end.y, t);
				end.z = EaseOutSine(start.z, end.z, t);
				end.w = EaseOutSine(start.w, end.w, t);
				break;
			case EasingType.EaseInOutSine:
				end.x = EaseInOutSine(start.x, end.x, t);
				end.y = EaseInOutSine(start.y, end.y, t);
				end.z = EaseInOutSine(start.z, end.z, t);
				end.w = EaseInOutSine(start.w, end.w, t);
				break;
			case EasingType.EaseInQuadratic:
				end.x = EaseInQuadratic(start.x, end.x, t);
				end.y = EaseInQuadratic(start.y, end.y, t);
				end.z = EaseInQuadratic(start.z, end.z, t);
				end.w = EaseInQuadratic(start.w, end.w, t);
				break;
			case EasingType.EaseOutQuadratic:
				end.x = EaseOutQuadratic(start.x, end.x, t);
				end.y = EaseOutQuadratic(start.y, end.y, t);
				end.z = EaseOutCubic(start.z, end.z, t);
				end.w = EaseOutCubic(start.w, end.w, t);
				break;
			case EasingType.EaseInOutQuadratic:
				end.x = EaseInOutQuadratic(start.x, end.x, t);
				end.y = EaseInOutQuadratic(start.y, end.y, t);
				end.z = EaseInOutQuadratic(start.z, end.z, t);
				end.w = EaseInOutQuadratic(start.w, end.w, t);
				break;
			case EasingType.EaseInCubic:
				end.x = EaseInCubic(start.x, end.x, t);
				end.y = EaseInCubic(start.y, end.y, t);
				end.z = EaseInCubic(start.z, end.z, t);
				end.w = EaseInCubic(start.w, end.w, t);
				break;
			case EasingType.EaseOutCubic:
				end.x = EaseOutCubic(start.x, end.x, t);
				end.y = EaseOutCubic(start.y, end.y, t);
				end.z = EaseOutCubic(start.z, end.z, t);
				end.w = EaseOutCubic(start.w, end.w, t);
				break;
			case EasingType.EaseInOutCubic:
				end.x = EaseInOutCubic(start.x, end.x, t);
				end.y = EaseInOutCubic(start.y, end.y, t);
				end.z = EaseInOutCubic(start.z, end.z, t);
				end.w = EaseInOutCubic(start.w, end.w, t);
				break;
			case EasingType.EaseInQuartic:
				end.x = EaseInQuartic(start.x, end.x, t);
				end.y = EaseInQuartic(start.y, end.y, t);
				end.z = EaseInQuartic(start.z, end.z, t);
				end.w = EaseInQuartic(start.w, end.w, t);
				break;
			case EasingType.EaseOutQuartic:
				end.x = EaseOutQuartic(start.x, end.x, t);
				end.y = EaseOutQuartic(start.y, end.y, t);
				end.z = EaseOutQuartic(start.z, end.z, t);
				end.w = EaseOutQuartic(start.w, end.w, t);
				break;
			case EasingType.EaseInOutQuartic:
				end.x = EaseInOutQuartic(start.x, end.x, t);
				end.y = EaseInOutQuartic(start.y, end.y, t);
				end.z = EaseInOutQuartic(start.z, end.z, t);
				end.w = EaseInOutQuartic(start.w, end.w, t);
				break;
			case EasingType.EaseInQuintic:
				end.x = EaseInQuintic(start.x, end.x, t);
				end.y = EaseInQuintic(start.y, end.y, t);
				end.z = EaseInQuintic(start.z, end.z, t);
				end.w = EaseInQuintic(start.w, end.w, t);
				break;
			case EasingType.EaseOutQuintic:
				end.x = EaseOutQuintic(start.x, end.x, t);
				end.y = EaseOutQuintic(start.y, end.y, t);
				end.z = EaseOutQuintic(start.z, end.z, t);
				end.w = EaseOutQuintic(start.w, end.w, t);
				break;
			case EasingType.EaseInOutQuintic:
				end.x = EaseInOutQuintic(start.x, end.x, t);
				end.y = EaseInOutQuintic(start.y, end.y, t);
				end.z = EaseInOutQuintic(start.z, end.z, t);
				end.w = EaseInOutQuintic(start.w, end.w, t);
				break;
			}
			return end;
		}

		public static Color Ease(Color start, Color end, float t, EasingType easing)
		{
			switch (easing)
			{
			case EasingType.Linear:
				return Color.Lerp(start, end, t);
			case EasingType.EaseInSine:
				end.r = EaseInSine(start.r, end.r, t);
				end.g = EaseInSine(start.g, end.g, t);
				end.b = EaseInSine(start.b, end.b, t);
				end.a = EaseInSine(start.a, end.a, t);
				break;
			case EasingType.EaseOutSine:
				end.r = EaseOutSine(start.r, end.r, t);
				end.g = EaseOutSine(start.g, end.g, t);
				end.b = EaseOutSine(start.b, end.b, t);
				end.a = EaseOutSine(start.a, end.a, t);
				break;
			case EasingType.EaseInOutSine:
				end.r = EaseInOutSine(start.r, end.r, t);
				end.g = EaseInOutSine(start.g, end.g, t);
				end.b = EaseInOutSine(start.b, end.b, t);
				end.a = EaseInOutSine(start.a, end.a, t);
				break;
			case EasingType.EaseInQuadratic:
				end.r = EaseInQuadratic(start.r, end.r, t);
				end.g = EaseInQuadratic(start.g, end.g, t);
				end.b = EaseInQuadratic(start.b, end.b, t);
				end.a = EaseInQuadratic(start.a, end.a, t);
				break;
			case EasingType.EaseOutQuadratic:
				end.r = EaseOutQuadratic(start.r, end.r, t);
				end.g = EaseOutQuadratic(start.g, end.g, t);
				end.b = EaseOutCubic(start.b, end.b, t);
				end.a = EaseOutCubic(start.a, end.a, t);
				break;
			case EasingType.EaseInOutQuadratic:
				end.r = EaseInOutQuadratic(start.r, end.r, t);
				end.g = EaseInOutQuadratic(start.g, end.g, t);
				end.b = EaseInOutQuadratic(start.b, end.b, t);
				end.a = EaseInOutQuadratic(start.a, end.a, t);
				break;
			case EasingType.EaseInCubic:
				end.r = EaseInCubic(start.r, end.r, t);
				end.g = EaseInCubic(start.g, end.g, t);
				end.b = EaseInCubic(start.b, end.b, t);
				end.a = EaseInCubic(start.a, end.a, t);
				break;
			case EasingType.EaseOutCubic:
				end.r = EaseOutCubic(start.r, end.r, t);
				end.g = EaseOutCubic(start.g, end.g, t);
				end.b = EaseOutCubic(start.b, end.b, t);
				end.a = EaseOutCubic(start.a, end.a, t);
				break;
			case EasingType.EaseInOutCubic:
				end.r = EaseInOutCubic(start.r, end.r, t);
				end.g = EaseInOutCubic(start.g, end.g, t);
				end.b = EaseInOutCubic(start.b, end.b, t);
				end.a = EaseInOutCubic(start.a, end.a, t);
				break;
			case EasingType.EaseInQuartic:
				end.r = EaseInQuartic(start.r, end.r, t);
				end.g = EaseInQuartic(start.g, end.g, t);
				end.b = EaseInQuartic(start.b, end.b, t);
				end.a = EaseInQuartic(start.a, end.a, t);
				break;
			case EasingType.EaseOutQuartic:
				end.r = EaseOutQuartic(start.r, end.r, t);
				end.g = EaseOutQuartic(start.g, end.g, t);
				end.b = EaseOutQuartic(start.b, end.b, t);
				end.a = EaseOutQuartic(start.a, end.a, t);
				break;
			case EasingType.EaseInOutQuartic:
				end.r = EaseInOutQuartic(start.r, end.r, t);
				end.g = EaseInOutQuartic(start.g, end.g, t);
				end.b = EaseInOutQuartic(start.b, end.b, t);
				end.a = EaseInOutQuartic(start.a, end.a, t);
				break;
			case EasingType.EaseInQuintic:
				end.r = EaseInQuintic(start.r, end.r, t);
				end.g = EaseInQuintic(start.g, end.g, t);
				end.b = EaseInQuintic(start.b, end.b, t);
				end.a = EaseInQuintic(start.a, end.a, t);
				break;
			case EasingType.EaseOutQuintic:
				end.r = EaseOutQuintic(start.r, end.r, t);
				end.g = EaseOutQuintic(start.g, end.g, t);
				end.b = EaseOutQuintic(start.b, end.b, t);
				end.a = EaseOutQuintic(start.a, end.a, t);
				break;
			case EasingType.EaseInOutQuintic:
				end.r = EaseInOutQuintic(start.r, end.r, t);
				end.g = EaseInOutQuintic(start.g, end.g, t);
				end.b = EaseInOutQuintic(start.b, end.b, t);
				end.a = EaseInOutQuintic(start.a, end.a, t);
				break;
			}
			return end;
		}

		public static float EaseInSine(float start, float end, float t)
		{
			end -= start;
			return (0f - end) * Mathf.Cos(t * ((float)Math.PI / 2f)) + end + start;
		}

		public static float EaseOutSine(float start, float end, float t)
		{
			end -= start;
			return end * Mathf.Sin(t * ((float)Math.PI / 2f)) + start;
		}

		public static float EaseInOutSine(float start, float end, float t)
		{
			end -= start;
			return (0f - end) / 2f * (Mathf.Cos((float)Math.PI * t) - 1f) + start;
		}

		public static float EaseInQuadratic(float start, float end, float t)
		{
			end -= start;
			return end * t * t + start;
		}

		public static float EaseOutQuadratic(float start, float end, float t)
		{
			end -= start;
			return (0f - end) * t * (t - 2f) + start;
		}

		public static float EaseInOutQuadratic(float start, float end, float t)
		{
			t /= 0.5f;
			end -= start;
			if (t < 1f)
			{
				return end / 2f * t * t + start;
			}
			t -= 1f;
			return (0f - end) / 2f * (t * (t - 2f) - 1f) + start;
		}

		public static float EaseInCubic(float start, float end, float t)
		{
			end -= start;
			return end * t * t * t + start;
		}

		public static float EaseOutCubic(float start, float end, float t)
		{
			t -= 1f;
			end -= start;
			return end * (t * t * t + 1f) + start;
		}

		public static float EaseInOutCubic(float start, float end, float t)
		{
			t /= 0.5f;
			end -= start;
			if (t < 1f)
			{
				return end / 2f * t * t * t + start;
			}
			t -= 2f;
			return end / 2f * (t * t * t + 2f) + start;
		}

		public static float EaseInQuartic(float start, float end, float t)
		{
			end -= start;
			return end * t * t * t * t + start;
		}

		public static float EaseOutQuartic(float start, float end, float t)
		{
			t -= 1f;
			end -= start;
			return (0f - end) * (t * t * t * t - 1f) + start;
		}

		public static float EaseInOutQuartic(float start, float end, float t)
		{
			t /= 0.5f;
			end -= start;
			if (t < 1f)
			{
				return end / 2f * t * t * t * t + start;
			}
			t -= 2f;
			return (0f - end) / 2f * (t * t * t * t - 2f) + start;
		}

		public static float EaseInQuintic(float start, float end, float t)
		{
			end -= start;
			return end * t * t * t * t * t + start;
		}

		public static float EaseOutQuintic(float start, float end, float t)
		{
			t -= 1f;
			end -= start;
			return end * (t * t * t * t * t + 1f) + start;
		}

		public static float EaseInOutQuintic(float start, float end, float t)
		{
			t /= 0.5f;
			end -= start;
			if (t < 1f)
			{
				return end / 2f * t * t * t * t * t + start;
			}
			t -= 2f;
			return end / 2f * (t * t * t * t * t + 2f) + start;
		}

		public static float EaseDelta(float t, EasingType easing)
		{
			switch (easing)
			{
			case EasingType.EaseInSine:
				return EaseInSine(0f, 1f, t);
			case EasingType.EaseOutSine:
				return EaseOutSine(0f, 1f, t);
			case EasingType.EaseInOutSine:
				return EaseInOutSine(0f, 1f, t);
			case EasingType.EaseInQuadratic:
				return EaseInQuadratic(0f, 1f, t);
			case EasingType.EaseOutQuadratic:
				return EaseOutQuadratic(0f, 1f, t);
			case EasingType.EaseInOutQuadratic:
				return EaseInOutQuadratic(0f, 1f, t);
			case EasingType.EaseInCubic:
				return EaseInCubic(0f, 1f, t);
			case EasingType.EaseOutCubic:
				return EaseOutCubic(0f, 1f, t);
			case EasingType.EaseInOutCubic:
				return EaseInOutCubic(0f, 1f, t);
			case EasingType.EaseInQuartic:
				return EaseInQuartic(0f, 1f, t);
			case EasingType.EaseOutQuartic:
				return EaseOutQuartic(0f, 1f, t);
			case EasingType.EaseInOutQuartic:
				return EaseInOutQuartic(0f, 1f, t);
			case EasingType.EaseInQuintic:
				return EaseInQuintic(0f, 1f, t);
			case EasingType.EaseOutQuintic:
				return EaseOutQuintic(0f, 1f, t);
			case EasingType.EaseInOutQuintic:
				return EaseInOutQuintic(0f, 1f, t);
			default:
				return t;
			}
		}
	}
}
