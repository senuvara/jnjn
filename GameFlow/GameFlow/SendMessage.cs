using UnityEngine;
using UnityEngine.Serialization;

namespace GameFlow
{
	[Help("es", "Ejecuta el método especificado en cada MonoBehaviour del GameObject objetivo.", null)]
	[Help("en", "Calls the specified method on every MonoBehaviour of the target GameObject.", null)]
	[AddComponentMenu("")]
	public class SendMessage : Action
	{
		public enum Broadcasting
		{
			None,
			Children,
			Ancestors
		}

		[SerializeField]
		private GameObject _target;

		[SerializeField]
		private Variable _targetVar;

		[SerializeField]
		private string _method = string.Empty;

		[SerializeField]
		private Variable _methodVar;

		[SerializeField]
		[FormerlySerializedAs("_parameterVar")]
		private Variable _valueVar;

		[SerializeField]
		private Broadcasting _broadcasting;

		[SerializeField]
		private Variable _broadcastingVar;

		[SerializeField]
		private bool _requireReceiver;

		[SerializeField]
		private Variable _requireReceiverVar;

		public GameObject target => _targetVar.GetValue(_target) as GameObject;

		public string method => _methodVar.GetValue(_method);

		public object value => _valueVar.GetValueAsObject();

		public Broadcasting broadcasting => (Broadcasting)(object)_broadcastingVar.GetValue(_broadcasting);

		public bool requireReceiver => _requireReceiverVar.GetValue(_requireReceiver);

		private void Awake()
		{
		}

		protected override void OnReset()
		{
		}

		public override void Execute()
		{
			Execute(target, method, value, broadcasting, requireReceiver);
		}

		public static void Execute(GameObject target, string method, object parameterValue, Broadcasting broadcasting, bool requireReceiver)
		{
			if (!(target == null) && method != null && method.Length != 0)
			{
				SendMessageOptions options = (!requireReceiver) ? SendMessageOptions.DontRequireReceiver : SendMessageOptions.RequireReceiver;
				switch (broadcasting)
				{
				case Broadcasting.None:
					target.SendMessage(method, parameterValue, options);
					break;
				case Broadcasting.Children:
					target.BroadcastMessage(method, parameterValue, options);
					break;
				case Broadcasting.Ancestors:
					target.SendMessageUpwards(method, parameterValue, options);
					break;
				}
			}
		}
	}
}
