using UnityEngine;

namespace GameFlow
{
	[Help("en", "Copies the properties of the specified Material.", null)]
	[Help("es", "Copia las propiedades del Material especificado.", null)]
	[AddComponentMenu("")]
	public class CopyMaterial : Action
	{
		[SerializeField]
		private Material _source;

		[SerializeField]
		private Variable _sourceVar;

		[SerializeField]
		private Material _target;

		[SerializeField]
		private Variable _targetVar;

		public Material source => _sourceVar.GetValue(_source) as Material;

		public Material target => _targetVar.GetValue(_target) as Material;

		public override void Execute()
		{
			Execute(source, target);
		}

		public static void Execute(Material source, Material target)
		{
			if (!(source == null) && !(target == null) && !(source == target))
			{
				target.CopyPropertiesFromMaterial(source);
			}
		}
	}
}
