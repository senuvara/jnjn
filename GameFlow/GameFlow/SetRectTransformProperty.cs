using UnityEngine;

namespace GameFlow
{
	[Help("en", "Modifies the value of a property of the specified Rect Transform component.", null)]
	[AddComponentMenu("")]
	[Help("es", "Modifica el valor de una propiedad del componente Rect Transform especificado.", null)]
	public class SetRectTransformProperty : Action, IExecutableInEditor
	{
		public enum Property
		{
			AnchoredPosition,
			AnchoredPosition3D,
			AnchorMax,
			AnchorMin,
			Forward,
			HasChanged,
			LocalPosition,
			LocalRotation,
			LocalScale,
			OffsetMax,
			OffsetMin,
			Parent,
			Pivot,
			Position,
			Right,
			Rotation,
			SizeDelta,
			Up
		}

		[SerializeField]
		private RectTransform _target;

		[SerializeField]
		private Variable _targetVar;

		[SerializeField]
		private Property _property;

		[SerializeField]
		private Vector2 _anchoredPositionValue;

		[SerializeField]
		private Vector3 _anchoredPosition3DValue;

		[SerializeField]
		private Vector2 _anchorMaxValue;

		[SerializeField]
		private Vector2 _anchorMinValue;

		[SerializeField]
		private Vector3 _forwardValue;

		[SerializeField]
		private bool _hasChangedValue;

		[SerializeField]
		private Vector3 _localPositionValue;

		[SerializeField]
		private Vector3 _localRotationValue;

		[SerializeField]
		private Vector3 _localScaleValue;

		[SerializeField]
		private Vector2 _offsetMaxValue;

		[SerializeField]
		private Vector2 _offsetMinValue;

		[SerializeField]
		private Transform _parentValue;

		[SerializeField]
		private Vector2 _pivotValue;

		[SerializeField]
		private Vector3 _positionValue;

		[SerializeField]
		private Vector3 _rightValue;

		[SerializeField]
		private Vector3 _rotationValue;

		[SerializeField]
		private Vector2 _sizeDeltaValue;

		[SerializeField]
		private Vector3 _upValue;

		[SerializeField]
		private Variable _valueVar;

		public RectTransform target => _targetVar.GetValueAsComponent(_target, typeof(RectTransform)) as RectTransform;

		protected override void OnReset()
		{
			_target = base.gameObject.GetComponent<RectTransform>();
			if (!(_target == null))
			{
				_anchoredPositionValue = _target.anchoredPosition;
				_anchoredPosition3DValue = _target.anchoredPosition3D;
				_anchorMaxValue = _target.anchorMax;
				_anchorMinValue = _target.anchorMin;
				_forwardValue = _target.forward;
				_hasChangedValue = _target.hasChanged;
				_localPositionValue = _target.localPosition;
				_localRotationValue = _target.localEulerAngles;
				_localScaleValue = _target.localScale;
				_offsetMaxValue = _target.offsetMax;
				_offsetMinValue = _target.offsetMin;
				_parentValue = _target.parent;
				_pivotValue = _target.pivot;
				_positionValue = _target.position;
				_rightValue = _target.right;
				_rotationValue = _target.eulerAngles;
				_sizeDeltaValue = _target.sizeDelta;
				_upValue = _target.up;
			}
		}

		public override void Setup()
		{
		}

		public override void Execute()
		{
			RectTransform target = this.target;
			if (!(target == null))
			{
				switch (_property)
				{
				case Property.AnchoredPosition:
					target.anchoredPosition = _valueVar.GetValue(_anchoredPositionValue);
					break;
				case Property.AnchoredPosition3D:
					target.anchoredPosition3D = _valueVar.GetValue(_anchoredPosition3DValue);
					break;
				case Property.AnchorMax:
					target.anchorMax = _valueVar.GetValue(_anchorMaxValue);
					break;
				case Property.AnchorMin:
					target.anchorMin = _valueVar.GetValue(_anchorMinValue);
					break;
				case Property.Forward:
					target.forward = _valueVar.GetValue(_forwardValue);
					break;
				case Property.HasChanged:
					target.hasChanged = _valueVar.GetValue(_hasChangedValue);
					break;
				case Property.LocalPosition:
					target.localPosition = _valueVar.GetValue(_localPositionValue);
					break;
				case Property.LocalRotation:
					target.localEulerAngles = _valueVar.GetValue(_localRotationValue);
					break;
				case Property.LocalScale:
					target.localScale = _valueVar.GetValue(_localScaleValue);
					break;
				case Property.OffsetMax:
					target.offsetMax = _valueVar.GetValue(_offsetMaxValue);
					break;
				case Property.OffsetMin:
					target.offsetMin = _valueVar.GetValue(_offsetMinValue);
					break;
				case Property.Parent:
					target.parent = (_valueVar.GetValueAsComponent(_parentValue, typeof(Transform)) as Transform);
					break;
				case Property.Pivot:
					target.pivot = _valueVar.GetValue(_pivotValue);
					break;
				case Property.Position:
					target.position = _valueVar.GetValue(_positionValue);
					break;
				case Property.Right:
					target.right = _valueVar.GetValue(_rightValue);
					break;
				case Property.Rotation:
					target.eulerAngles = _valueVar.GetValue(_rotationValue);
					break;
				case Property.SizeDelta:
					target.sizeDelta = _valueVar.GetValue(_sizeDeltaValue);
					break;
				case Property.Up:
					target.up = _valueVar.GetValue(_upValue);
					break;
				}
			}
		}
	}
}
