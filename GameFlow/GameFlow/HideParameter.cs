using UnityEngine;

namespace GameFlow
{
	[AddComponentMenu("")]
	[Help("es", "Oculta el parámetro (@GameFlow{Parameter}) especificado.", null)]
	[Help("en", "Hides the specified @GameFlow{Parameter}.", null)]
	public class HideParameter : ParameterAction
	{
	}
}
