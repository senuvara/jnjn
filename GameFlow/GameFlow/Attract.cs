using UnityEngine;

namespace GameFlow
{
	[Help("es", "Atrae el @UnityManual{Rigidbody} objetivo especificado hacia el punto indicado.", null)]
	[AddComponentMenu("")]
	[Help("en", "Attracts the specified target @UnityManual{Rigidbody} towards the indicated point.", null)]
	public class Attract : Action, IExecutableInEditor
	{
		public enum PointType
		{
			Transform,
			Position
		}

		[SerializeField]
		private Rigidbody _target;

		[SerializeField]
		private Variable _targetVar;

		[SerializeField]
		private PointType _pointType;

		[SerializeField]
		private Transform _pointTransform;

		[SerializeField]
		private Variable _pointTransformVar;

		[SerializeField]
		private Vector3 _point;

		[SerializeField]
		private Variable _pointVar;

		[SerializeField]
		private float _acceleration = 10f;

		[SerializeField]
		private Variable _accelerationVar;

		[SerializeField]
		private bool _attenuated;

		[SerializeField]
		private Variable _attenuatedVar;

		public Rigidbody target => _targetVar.GetValueAsComponent(_target, typeof(Rigidbody)) as Rigidbody;

		public Transform pointTransform => _pointTransformVar.GetValueAsComponent(_pointTransform, typeof(Transform)) as Transform;

		public Vector3 point => _pointVar.GetValue(_point);

		public float acceleration => _accelerationVar.GetValue(_acceleration);

		public bool attenuated => _attenuatedVar.GetValue(_attenuated);

		protected override void OnReset()
		{
		}

		public override void Execute()
		{
			switch (_pointType)
			{
			case PointType.Transform:
			{
				Transform pointTransform = this.pointTransform;
				if (pointTransform != null)
				{
					Execute(target, pointTransform.position, acceleration, attenuated);
				}
				break;
			}
			case PointType.Position:
				Execute(target, point, acceleration, attenuated);
				break;
			}
		}

		public static void Execute(Rigidbody target, Vector3 point, float acceleration, bool attenuated)
		{
			if (!(target == null))
			{
				Vector3 force = (point - target.position).normalized * acceleration;
				if (attenuated)
				{
					float num = Vector3.Distance(point, target.position);
					num = Mathf.Clamp(num, 0.0001f, num);
					force /= num;
				}
				target.AddForce(force, ForceMode.Acceleration);
			}
		}
	}
}
