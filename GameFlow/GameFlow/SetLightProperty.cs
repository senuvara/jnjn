using UnityEngine;

namespace GameFlow
{
	[Help("es", "Modifica una propiedad del componente @UnityManual{Light} especificado.", null)]
	[AddComponentMenu("")]
	[Help("en", "Sets a property of the specified @UnityManual{Light}.", null)]
	public class SetLightProperty : Action, IExecutableInEditor
	{
		public enum Property
		{
			AlreadyLightmapped,
			AreaSize,
			BounceIntensity,
			Color,
			Cookie,
			CookieSize,
			CullingMask,
			Flare,
			Intensity,
			Range,
			RenderMode,
			ShadowBias,
			ShadowNormalBias,
			Shadows,
			ShadowStrength,
			SpotAngle,
			Type
		}

		[SerializeField]
		private Light _target;

		[SerializeField]
		private Variable _targetVar;

		[SerializeField]
		private Property _property = Property.Range;

		[SerializeField]
		private float _bounceIntensityValue;

		[SerializeField]
		private Color _colorValue;

		[SerializeField]
		private Texture _cookieValue;

		[SerializeField]
		private float _cookieSizeValue;

		[SerializeField]
		private LayerMask _cullingMaskValue;

		[SerializeField]
		private Flare _flareValue;

		[SerializeField]
		private float _intensityValue;

		[SerializeField]
		private float _rangeValue;

		[SerializeField]
		private LightRenderMode _renderModeValue;

		[SerializeField]
		private float _shadowBiasValue;

		[SerializeField]
		private float _shadowNormalBiasValue;

		[SerializeField]
		private LightShadows _shadowsValue;

		[SerializeField]
		private float _shadowStrengthValue;

		[SerializeField]
		private float _spotAngleValue;

		[SerializeField]
		private LightType _typeValue;

		[SerializeField]
		private Variable _valueVar;

		protected override void OnReset()
		{
			_target = base.gameObject.GetComponent<Light>();
		}

		public override void Setup()
		{
		}

		public override void Execute()
		{
			Light light = _targetVar.GetValueAsComponent(_target, typeof(Light)) as Light;
			if (!(light == null))
			{
				switch (_property)
				{
				case Property.BounceIntensity:
					light.bounceIntensity = _valueVar.GetValue(_bounceIntensityValue);
					break;
				case Property.Color:
					light.color = _valueVar.GetValue(_colorValue);
					break;
				case Property.Cookie:
					light.cookie = (_valueVar.GetValue(_cookieValue) as Texture);
					break;
				case Property.CookieSize:
					light.cookieSize = _valueVar.GetValue(_cookieSizeValue);
					break;
				case Property.CullingMask:
					light.cullingMask = _valueVar.GetValue(_cullingMaskValue);
					break;
				case Property.Flare:
					light.flare = (_valueVar.GetValue(_flareValue) as Flare);
					break;
				case Property.Intensity:
					light.intensity = _valueVar.GetValue(_intensityValue);
					break;
				case Property.Range:
					light.range = _valueVar.GetValue(_rangeValue);
					break;
				case Property.RenderMode:
					light.renderMode = (LightRenderMode)(object)_valueVar.GetValue(_renderModeValue);
					break;
				case Property.ShadowBias:
					light.shadowBias = _valueVar.GetValue(_shadowBiasValue);
					break;
				case Property.ShadowNormalBias:
					light.shadowNormalBias = _valueVar.GetValue(_shadowNormalBiasValue);
					break;
				case Property.Shadows:
					light.shadows = (LightShadows)(object)_valueVar.GetValue(_shadowsValue);
					break;
				case Property.ShadowStrength:
					light.shadowStrength = _valueVar.GetValue(_shadowStrengthValue);
					break;
				case Property.SpotAngle:
					light.spotAngle = _valueVar.GetValue(_spotAngleValue);
					break;
				case Property.Type:
					light.type = (LightType)(object)_valueVar.GetValue(_typeValue);
					break;
				}
			}
		}
	}
}
