using UnityEngine;

namespace GameFlow
{
	[Help("en", "Sets a property of the specified @UnityManual{CharacterController}.", null)]
	[Help("es", "Modifica una propiedad del componente @UnityManual{CharacterController} especificado.", null)]
	[AddComponentMenu("")]
	public class SetCharacterControllerProperty : Action, IExecutableInEditor
	{
		public enum Property
		{
			Center,
			ContactOffset,
			DetectCollisions,
			Enabled,
			Height,
			IsTrigger,
			Material,
			Radius,
			SharedMaterial,
			SlopeLimit,
			StepOffset,
			SkinWidth
		}

		[SerializeField]
		private CharacterController _target;

		[SerializeField]
		private Variable _targetVar;

		[SerializeField]
		private Property _property;

		[SerializeField]
		private Vector3 _centerValue;

		[SerializeField]
		private float _contactOffsetValue;

		[SerializeField]
		private bool _detectCollisionsValue;

		[SerializeField]
		private bool _enabledValue;

		[SerializeField]
		private float _heightValue;

		[SerializeField]
		private bool _isTriggerValue;

		[SerializeField]
		private PhysicMaterial _materialValue;

		[SerializeField]
		private float _radiusValue;

		[SerializeField]
		private PhysicMaterial _sharedMaterialValue;

		[SerializeField]
		private float _slopeLimitValue;

		[SerializeField]
		private float _stepOffsetValue;

		[SerializeField]
		private float _skinWidthValue;

		[SerializeField]
		private Variable _valueVar;

		protected override void OnReset()
		{
			_target = base.gameObject.GetComponent<CharacterController>();
		}

		public override void Setup()
		{
		}

		public override void Execute()
		{
			CharacterController characterController = _targetVar.GetValueAsComponent(_target, typeof(CharacterController)) as CharacterController;
			if (!(characterController == null))
			{
				switch (_property)
				{
				case Property.Center:
					characterController.center = _valueVar.GetValue(_centerValue);
					break;
				case Property.ContactOffset:
					characterController.contactOffset = _valueVar.GetValue(_contactOffsetValue);
					break;
				case Property.DetectCollisions:
					characterController.detectCollisions = _valueVar.GetValue(_detectCollisionsValue);
					break;
				case Property.Enabled:
					characterController.enabled = _valueVar.GetValue(_enabledValue);
					break;
				case Property.Height:
					characterController.height = _valueVar.GetValue(_heightValue);
					break;
				case Property.IsTrigger:
					characterController.isTrigger = _valueVar.GetValue(_isTriggerValue);
					break;
				case Property.Material:
					characterController.material = (_valueVar.GetValue(_materialValue) as PhysicMaterial);
					break;
				case Property.Radius:
					characterController.radius = _valueVar.GetValue(_radiusValue);
					break;
				case Property.SharedMaterial:
					characterController.sharedMaterial = (_valueVar.GetValue(_sharedMaterialValue) as PhysicMaterial);
					break;
				case Property.SlopeLimit:
					characterController.slopeLimit = _valueVar.GetValue(_slopeLimitValue);
					break;
				case Property.StepOffset:
					characterController.stepOffset = _valueVar.GetValue(_stepOffsetValue);
					break;
				case Property.SkinWidth:
					characterController.skinWidth = _valueVar.GetValue(_skinWidthValue);
					break;
				}
			}
		}
	}
}
