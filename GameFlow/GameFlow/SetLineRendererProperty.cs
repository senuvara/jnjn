using UnityEngine;
using UnityEngine.Rendering;

namespace GameFlow
{
	[Help("es", "Modifica una propiedad del componente @UnityManual{LineRenderer} especificado.", null)]
	[AddComponentMenu("")]
	[Help("en", "Sets a property of the specified @UnityManual{LineRenderer}.", null)]
	public class SetLineRendererProperty : Action, IExecutableInEditor
	{
		public enum Property
		{
			Enabled,
			LightmapIndex,
			LightmapScaleOffset,
			Material,
			Materials,
			ProbeAnchor,
			RealtimeLightmapScaleOffset,
			ReceiveShadows,
			ReflectionProbeUsage,
			ShadowCastingMode,
			SharedMaterial,
			SharedMaterials,
			SortingLayerID,
			SortingLayerName,
			SortingOrder,
			LightProbeUsage,
			UseWorldSpace
		}

		[SerializeField]
		private LineRenderer _target;

		[SerializeField]
		private Variable _targetVar;

		[SerializeField]
		private Property _property;

		[SerializeField]
		private bool _enabledValue;

		[SerializeField]
		private int _lightmapIndexValue;

		[SerializeField]
		private Vector4 _lightmapScaleOffsetValue;

		[SerializeField]
		private Material _materialValue;

		[SerializeField]
		private Transform _probeAnchorValue;

		[SerializeField]
		private Vector4 _realtimeLightmapScaleOffsetValue;

		[SerializeField]
		private bool _receiveShadowsValue;

		[SerializeField]
		private ReflectionProbeUsage _reflectionProbeUsageValue;

		[SerializeField]
		private ShadowCastingMode _shadowCastingModeValue;

		[SerializeField]
		private Material _sharedMaterialValue;

		[SerializeField]
		private int _sortingLayerIDValue;

		[SerializeField]
		private string _sortingLayerNameValue;

		[SerializeField]
		private int _sortingOrderValue;

		[SerializeField]
		private LightProbeUsage _lightProbeUsageValue;

		[SerializeField]
		private bool _useWorldSpaceValue;

		[SerializeField]
		private Variable _valueVar;

		protected override void OnReset()
		{
			_target = base.gameObject.GetComponent<LineRenderer>();
		}

		public override void Setup()
		{
		}

		public override void Execute()
		{
			LineRenderer lineRenderer = _targetVar.GetValueAsComponent(_target, typeof(LineRenderer)) as LineRenderer;
			if (!(lineRenderer == null))
			{
				switch (_property)
				{
				case Property.Materials:
				case Property.SharedMaterials:
					break;
				case Property.Enabled:
					lineRenderer.enabled = _valueVar.GetValue(_enabledValue);
					break;
				case Property.LightmapIndex:
					lineRenderer.lightmapIndex = _valueVar.GetValue(_lightmapIndexValue);
					break;
				case Property.LightmapScaleOffset:
					lineRenderer.lightmapScaleOffset = _valueVar.GetValue(_lightmapScaleOffsetValue);
					break;
				case Property.Material:
					lineRenderer.material = (_valueVar.GetValue(_materialValue) as Material);
					break;
				case Property.ProbeAnchor:
					lineRenderer.probeAnchor = (_valueVar.GetValueAsComponent(_probeAnchorValue, typeof(Transform)) as Transform);
					break;
				case Property.RealtimeLightmapScaleOffset:
					lineRenderer.realtimeLightmapScaleOffset = _valueVar.GetValue(_realtimeLightmapScaleOffsetValue);
					break;
				case Property.ReceiveShadows:
					lineRenderer.receiveShadows = _valueVar.GetValue(_receiveShadowsValue);
					break;
				case Property.ReflectionProbeUsage:
					lineRenderer.reflectionProbeUsage = (ReflectionProbeUsage)(object)_valueVar.GetValue(_reflectionProbeUsageValue);
					break;
				case Property.ShadowCastingMode:
					lineRenderer.shadowCastingMode = (ShadowCastingMode)(object)_valueVar.GetValue(_shadowCastingModeValue);
					break;
				case Property.SharedMaterial:
					lineRenderer.sharedMaterial = (_valueVar.GetValue(_sharedMaterialValue) as Material);
					break;
				case Property.SortingLayerID:
					lineRenderer.sortingLayerID = _valueVar.GetValue(_sortingLayerIDValue);
					break;
				case Property.SortingLayerName:
					lineRenderer.sortingLayerName = _valueVar.GetValue(_sortingLayerNameValue);
					break;
				case Property.SortingOrder:
					lineRenderer.sortingOrder = _valueVar.GetValue(_sortingOrderValue);
					break;
				case Property.LightProbeUsage:
					lineRenderer.lightProbeUsage = (LightProbeUsage)(object)_valueVar.GetValue(_lightProbeUsageValue);
					break;
				case Property.UseWorldSpace:
					lineRenderer.useWorldSpace = _valueVar.GetValue(_useWorldSpaceValue);
					break;
				}
			}
		}
	}
}
