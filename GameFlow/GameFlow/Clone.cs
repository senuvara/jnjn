using UnityEngine;

namespace GameFlow
{
	[Help("es", "Crea un duplicado o clon del @UnityManual{GameObject} especificado.", null)]
	[Help("en", "Creates a duplicate or clone of the specified @UnityManual{GameObject}.", null)]
	[AddComponentMenu("")]
	public class Clone : Action, IExecutableInEditor
	{
		public enum Positioning
		{
			OriginalPosition,
			SpecifiedPosition
		}

		public enum PositionType
		{
			Position,
			Transform
		}

		[SerializeField]
		private GameObject _original;

		[SerializeField]
		private Variable _originalVar;

		[SerializeField]
		private Positioning _clonePositioning;

		[SerializeField]
		private Variable _clonePositioningVar;

		[SerializeField]
		private PositionType _positionType;

		[SerializeField]
		private Vector3 _position;

		[SerializeField]
		private Variable _positionVar;

		[SerializeField]
		private Transform _transform;

		[SerializeField]
		private Variable _transformVar;

		[SerializeField]
		private bool _activate = true;

		[SerializeField]
		private Variable _output;

		public GameObject original => _originalVar.GetValue(_original) as GameObject;

		public Positioning clonePositioning => (Positioning)(object)_clonePositioningVar.GetValue(_clonePositioning);

		public Vector3 position
		{
			get
			{
				if (_positionType == PositionType.Position)
				{
					return _positionVar.GetValue(_position);
				}
				Transform transform = _transformVar.GetValueAsComponent(_transform, typeof(Transform)) as Transform;
				return transform.position;
			}
		}

		public bool activate => _activate;

		public Variable output => _output;

		public override void Step()
		{
			GameObject gameObject = _originalVar.GetValue(_original) as GameObject;
			GameObject gameObject2 = null;
			gameObject2 = ((clonePositioning != 0) ? Execute(gameObject, _activate, position) : Execute(gameObject, _activate));
			_output.SetValue(gameObject2);
			if (gameObject2 != null)
			{
				gameObject2.SetParent(gameObject.GetParent());
			}
		}

		public static GameObject Execute(GameObject source, bool activate)
		{
			GameObject result = null;
			if (source != null)
			{
				result = Execute(source, activate, source.GetComponent<Transform>().position);
			}
			return result;
		}

		public static GameObject Execute(GameObject source, bool activate, Vector3 position)
		{
			GameObject gameObject = null;
			if (source != null)
			{
				gameObject = Object.Instantiate(source, position, source.GetComponent<Transform>().rotation);
				OnActivate.RegisterInactiveAsListener(gameObject);
				if (activate)
				{
					gameObject.SetActive(value: true);
				}
				gameObject.name = source.name + " (clone)";
				gameObject.HideComponents();
			}
			return gameObject;
		}
	}
}
