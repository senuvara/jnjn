using UnityEngine;

namespace GameFlow
{
	[Help("es", "Añade los GameObjects seleccionados a la lista (@GameFlow{List}) especificada.", null)]
	[AddComponentMenu("")]
	[Help("en", "Adds the selected GameObjects to the specified @GameFlow{List}.", null)]
	public class AddSelectionToList : ListAction
	{
		[SerializeField]
		private bool _onlyActive;

		[SerializeField]
		private Variable _onlyActiveVar;

		public bool onlyActive => _onlyActiveVar.GetValue(_onlyActive);
	}
}
