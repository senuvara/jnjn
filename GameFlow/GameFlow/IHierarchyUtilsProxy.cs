using UnityEngine;

namespace GameFlow
{
	public interface IHierarchyUtilsProxy
	{
		bool SelectionContains(GameObject obj);

		bool SelectionContainsParentOf(GameObject obj);
	}
}
