using UnityEngine;
using UnityEngine.UI;

namespace GameFlow
{
	[AddComponentMenu("")]
	[Help("en", "Gets a property of the specified Toggle component.", null)]
	[Help("es", "Lee una propiedad del componente Toggle especificado.", null)]
	public class GetToggleProperty : Action, IExecutableInEditor
	{
		public enum Property
		{
			AnimationTriggers_DisabledTrigger,
			AnimationTriggers_HighlightedTrigger,
			AnimationTriggers_NormalTrigger,
			AnimationTriggers_PressedTrigger,
			Animator,
			Colors_ColorMultiplier,
			Colors_DisabledColor,
			Colors_FadeDuration,
			Colors_HighlightedColor,
			Colors_NormalColor,
			Colors_PressedColor,
			Group,
			Image,
			Interactable,
			IsOn,
			Navigation_Mode,
			Navigation_SelectOnDown,
			Navigation_SelectOnLeft,
			Navigation_SelectOnRight,
			Navigation_SelectOnUp,
			SpriteState_DisabledSprite,
			SpriteState_HighlightedSprite,
			SpriteState_PressedSprite,
			TargetGraphic,
			Transition
		}

		[SerializeField]
		private Toggle _source;

		[SerializeField]
		private Variable _sourceVar;

		[SerializeField]
		private Property _property;

		[SerializeField]
		private Variable _output;

		protected override void OnReset()
		{
			_source = base.gameObject.GetComponent<Toggle>();
		}

		public override void Setup()
		{
		}

		public override void Execute()
		{
			Toggle toggle = _sourceVar.GetValueAsComponent(_source, typeof(Toggle)) as Toggle;
			if (!(toggle == null))
			{
				switch (_property)
				{
				case Property.AnimationTriggers_DisabledTrigger:
					_output.SetValue(toggle.animationTriggers.disabledTrigger);
					break;
				case Property.AnimationTriggers_HighlightedTrigger:
					_output.SetValue(toggle.animationTriggers.highlightedTrigger);
					break;
				case Property.AnimationTriggers_NormalTrigger:
					_output.SetValue(toggle.animationTriggers.normalTrigger);
					break;
				case Property.AnimationTriggers_PressedTrigger:
					_output.SetValue(toggle.animationTriggers.pressedTrigger);
					break;
				case Property.Animator:
					_output.SetValue(toggle.animator);
					break;
				case Property.Colors_ColorMultiplier:
					_output.SetValue(toggle.colors.colorMultiplier);
					break;
				case Property.Colors_DisabledColor:
					_output.SetValue(toggle.colors.disabledColor);
					break;
				case Property.Colors_FadeDuration:
					_output.SetValue(toggle.colors.fadeDuration);
					break;
				case Property.Colors_HighlightedColor:
					_output.SetValue(toggle.colors.highlightedColor);
					break;
				case Property.Colors_NormalColor:
					_output.SetValue(toggle.colors.normalColor);
					break;
				case Property.Colors_PressedColor:
					_output.SetValue(toggle.colors.pressedColor);
					break;
				case Property.Group:
					_output.SetValue(toggle.group);
					break;
				case Property.Image:
					_output.SetValue(toggle.image);
					break;
				case Property.Interactable:
					_output.SetValue(toggle.interactable);
					break;
				case Property.IsOn:
					_output.SetValue(toggle.isOn);
					break;
				case Property.Navigation_Mode:
					_output.SetValue(toggle.navigation.mode);
					break;
				case Property.Navigation_SelectOnDown:
					_output.SetValue(toggle.navigation.selectOnDown);
					break;
				case Property.Navigation_SelectOnLeft:
					_output.SetValue(toggle.navigation.selectOnLeft);
					break;
				case Property.Navigation_SelectOnRight:
					_output.SetValue(toggle.navigation.selectOnRight);
					break;
				case Property.Navigation_SelectOnUp:
					_output.SetValue(toggle.navigation.selectOnUp);
					break;
				case Property.SpriteState_DisabledSprite:
					_output.SetValue(toggle.spriteState.disabledSprite);
					break;
				case Property.SpriteState_HighlightedSprite:
					_output.SetValue(toggle.spriteState.highlightedSprite);
					break;
				case Property.SpriteState_PressedSprite:
					_output.SetValue(toggle.spriteState.pressedSprite);
					break;
				case Property.TargetGraphic:
					_output.SetValue(toggle.targetGraphic);
					break;
				case Property.Transition:
					_output.SetValue(toggle.transition);
					break;
				}
			}
		}
	}
}
