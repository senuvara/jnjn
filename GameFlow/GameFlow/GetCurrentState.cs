using UnityEngine;

namespace GameFlow
{
	[Help("en", "Gets the current state of the specified @GameFlow{StateMachine}.", null)]
	[Help("es", "Obtiene el estado actual de la máquina de estados (@GameFlow{StateMachine}) especificada.", null)]
	[AddComponentMenu("")]
	public class GetCurrentState : Action, IExecutableInEditor
	{
		[SerializeField]
		private StateMachine _stateMachine;

		[SerializeField]
		private Variable _stateMachineVar;

		[SerializeField]
		private Variable _output;

		public StateMachine stateMachine => _stateMachineVar.GetValue(_stateMachine) as StateMachine;

		protected override void OnReset()
		{
			_stateMachine = base.gameObject.GetComponent<StateMachine>();
		}

		public override void Execute()
		{
			if (!(_output == null))
			{
				_output.SetValue(Execute(stateMachine));
			}
		}

		public static State Execute(StateMachine stateMachine)
		{
			if (stateMachine == null)
			{
				return null;
			}
			return stateMachine.currentState;
		}
	}
}
