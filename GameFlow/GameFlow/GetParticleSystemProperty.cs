using UnityEngine;

namespace GameFlow
{
	[Help("es", "Lee una propiedad del componente @UnityManual{ParticleSystem} especificado.", null)]
	[AddComponentMenu("")]
	[Help("en", "Gets a property of the specified @UnityManual{ParticleSystem} component.", null)]
	public class GetParticleSystemProperty : Action, IExecutableInEditor
	{
		public enum Property
		{
			Duration,
			EmissionRate,
			EmissionEnabled,
			GravityModifier,
			IsPaused,
			IsPlaying,
			IsStopped,
			Loop,
			MaxParticles,
			ParticleCount,
			PlaybackSpeed,
			PlayOnAwake,
			RandomSeed,
			SimulationSpace,
			StartColor,
			StartDelay,
			StartLifetime,
			StartRotation,
			StartSize,
			StartSpeed,
			Time,
			EmissionBurstCount,
			EmissionType
		}

		[SerializeField]
		private ParticleSystem _source;

		[SerializeField]
		private Variable _sourceVar;

		[SerializeField]
		private Property _property;

		[SerializeField]
		private Variable _output;

		protected override void OnReset()
		{
			_source = base.gameObject.GetComponent<ParticleSystem>();
		}

		public override void Setup()
		{
		}

		public override void Execute()
		{
			ParticleSystem particleSystem = _sourceVar.GetValueAsComponent(_source, typeof(ParticleSystem)) as ParticleSystem;
			if (!(particleSystem == null))
			{
				ParticleSystem.MainModule main = particleSystem.main;
				switch (_property)
				{
				case Property.EmissionType:
					break;
				case Property.Duration:
					_output.SetValue(main.duration);
					break;
				case Property.EmissionBurstCount:
					_output.SetValue(particleSystem.emission.burstCount);
					break;
				case Property.EmissionEnabled:
					_output.SetValue(particleSystem.emission.enabled);
					break;
				case Property.EmissionRate:
					_output.SetValue(particleSystem.emission.rateOverTime.constantMax);
					break;
				case Property.GravityModifier:
					_output.SetValue(main.gravityModifier.constantMin);
					break;
				case Property.IsPaused:
					_output.SetValue(particleSystem.isPaused);
					break;
				case Property.IsPlaying:
					_output.SetValue(particleSystem.isPlaying);
					break;
				case Property.IsStopped:
					_output.SetValue(particleSystem.isStopped);
					break;
				case Property.Loop:
					_output.SetValue(main.loop);
					break;
				case Property.MaxParticles:
					_output.SetValue(main.maxParticles);
					break;
				case Property.ParticleCount:
					_output.SetValue(particleSystem.particleCount);
					break;
				case Property.PlaybackSpeed:
					_output.SetValue(main.simulationSpeed);
					break;
				case Property.PlayOnAwake:
					_output.SetValue(main.playOnAwake);
					break;
				case Property.RandomSeed:
					_output.SetValue((int)particleSystem.randomSeed);
					break;
				case Property.SimulationSpace:
					_output.SetValue(main.simulationSpace);
					break;
				case Property.StartColor:
					_output.SetValue(main.startColor.Evaluate(0f));
					break;
				case Property.StartDelay:
					_output.SetValue(main.startDelay.Evaluate(0f));
					break;
				case Property.StartLifetime:
					_output.SetValue(main.startLifetime.Evaluate(0f));
					break;
				case Property.StartRotation:
					_output.SetValue(main.startRotation.Evaluate(0f));
					break;
				case Property.StartSize:
					_output.SetValue(main.startSize.Evaluate(0f));
					break;
				case Property.StartSpeed:
					_output.SetValue(main.startSpeed.Evaluate(0f));
					break;
				case Property.Time:
					_output.SetValue(particleSystem.time);
					break;
				}
			}
		}
	}
}
