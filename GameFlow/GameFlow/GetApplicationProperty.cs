using UnityEngine;

namespace GameFlow
{
	[Help("en", "Gets the value of a property of the application.", null)]
	[Help("es", "Lee el valor de una propiedad de la aplicación.", null)]
	[AddComponentMenu("")]
	public class GetApplicationProperty : Action, IExecutableInEditor
	{
		public enum Property
		{
			AbsoluteURL,
			BackgroundLoadingPriority,
			BundleIdentifier,
			CloudProjectId,
			CompanyName,
			DataPath,
			Genuine,
			GenuineCheckAvailable,
			InstallMode,
			InternetReachability,
			IsConsolePlatform,
			IsEditor,
			IsMobilePlatform,
			IsPlaying,
			IsWebPlayer,
			PersistentDataPath,
			Platform,
			ProductName,
			RunInBackground,
			SandboxType,
			SrcValue,
			StreamedBytes,
			StreamingAssetsPath,
			SystemLanguage,
			TargetFrameRate,
			TemporaryCachePath,
			UnityVersion,
			Version,
			WebSecurityEnabled
		}

		[SerializeField]
		private Property _property;

		[SerializeField]
		private Variable _output;

		protected override void OnReset()
		{
		}

		public override void Setup()
		{
		}

		public override void Execute()
		{
			switch (_property)
			{
			case Property.WebSecurityEnabled:
				break;
			case Property.AbsoluteURL:
				_output.SetValue(Application.absoluteURL);
				break;
			case Property.BackgroundLoadingPriority:
				_output.SetValue(Application.backgroundLoadingPriority);
				break;
			case Property.BundleIdentifier:
				_output.SetValue(Application.get_bundleIdentifier());
				break;
			case Property.CloudProjectId:
				_output.SetValue(Application.cloudProjectId);
				break;
			case Property.CompanyName:
				_output.SetValue(Application.companyName);
				break;
			case Property.DataPath:
				_output.SetValue(Application.dataPath);
				break;
			case Property.Genuine:
				_output.SetValue(Application.genuine);
				break;
			case Property.GenuineCheckAvailable:
				_output.SetValue(Application.genuineCheckAvailable);
				break;
			case Property.InstallMode:
				_output.SetValue(Application.installMode);
				break;
			case Property.InternetReachability:
				_output.SetValue(Application.internetReachability);
				break;
			case Property.IsConsolePlatform:
				_output.SetValue(Application.isConsolePlatform);
				break;
			case Property.IsEditor:
				_output.SetValue(Application.isEditor);
				break;
			case Property.IsMobilePlatform:
				_output.SetValue(Application.isMobilePlatform);
				break;
			case Property.IsPlaying:
				_output.SetValue(Application.isPlaying);
				break;
			case Property.IsWebPlayer:
				_output.SetValue(Application.isWebPlayer);
				break;
			case Property.PersistentDataPath:
				_output.SetValue(Application.persistentDataPath);
				break;
			case Property.Platform:
				_output.SetValue(Application.platform);
				break;
			case Property.ProductName:
				_output.SetValue(Application.productName);
				break;
			case Property.RunInBackground:
				_output.SetValue(Application.runInBackground);
				break;
			case Property.SandboxType:
				_output.SetValue(Application.sandboxType);
				break;
			case Property.SrcValue:
				_output.SetValue(Application.srcValue);
				break;
			case Property.StreamedBytes:
				_output.SetValue(Application.streamedBytes);
				break;
			case Property.StreamingAssetsPath:
				_output.SetValue(Application.streamingAssetsPath);
				break;
			case Property.SystemLanguage:
				_output.SetValue(Application.systemLanguage);
				break;
			case Property.TargetFrameRate:
				_output.SetValue(Application.targetFrameRate);
				break;
			case Property.TemporaryCachePath:
				_output.SetValue(Application.temporaryCachePath);
				break;
			case Property.UnityVersion:
				_output.SetValue(Application.unityVersion);
				break;
			case Property.Version:
				_output.SetValue(Application.version);
				break;
			}
		}
	}
}
