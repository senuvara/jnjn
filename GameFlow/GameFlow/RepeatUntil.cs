using System.Collections.Generic;
using UnityEngine;

namespace GameFlow
{
	[Help("en", "Executes the contained actions repeately until the conditions are met.", null)]
	[AddComponentMenu("")]
	[Help("es", "Ejecuta las acciones contenidas repetidamente hasta cumplir unas condiciones.", null)]
	public class RepeatUntil : Conditional
	{
		[SerializeField]
		private bool _untilFoldout = true;

		private bool _finished;

		public override void FirstStep()
		{
			_finished = false;
			sequence.FirstEnabledAction();
			Step();
		}

		public override void Execute()
		{
			if (!_finished && sequence.Finished())
			{
				_finished = Evaluate(defaultResult: true);
				if (!_finished)
				{
					sequence.FirstEnabledAction();
				}
			}
			if (_finished)
			{
				return;
			}
			Action currentAction = sequence.GetCurrentAction();
			if (currentAction != null && currentAction.enabledInEditor)
			{
				sequence.ExecuteCurrentAction();
			}
			else
			{
				sequence.NextEnabledAction();
				currentAction = sequence.GetCurrentAction();
				if (currentAction != null)
				{
					sequence.ExecuteCurrentAction();
				}
			}
			if (currentAction != null && ActionExecutor.DoesActionYield(currentAction))
			{
				ActionExecutor.Yield(currentAction);
				CheckFinished();
				sequence.NextEnabledAction();
				return;
			}
			while (!_finished && currentAction != null && currentAction.Finished())
			{
				sequence.NextEnabledAction();
				currentAction = sequence.GetCurrentAction();
				if (currentAction != null && currentAction.enabledInEditor)
				{
					sequence.ExecuteCurrentAction();
					if (ActionExecutor.DoesActionYield(currentAction))
					{
						ActionExecutor.Yield(currentAction);
						CheckFinished();
						sequence.NextEnabledAction();
						return;
					}
				}
			}
			CheckFinished();
		}

		private void CheckFinished()
		{
			if (sequence.Finished() && !_finished)
			{
				_finished = Evaluate(defaultResult: true);
				if (!_finished)
				{
					sequence.FirstEnabledAction();
				}
			}
		}

		public override bool Finished()
		{
			return _finished;
		}

		public override void Break()
		{
			_finished = true;
		}

		public override List<Block> GetBlocks()
		{
			List<Block> list = new List<Block>(GetActions().ToArray());
			list.AddRange(GetConditions().ToArray());
			return list;
		}
	}
}
