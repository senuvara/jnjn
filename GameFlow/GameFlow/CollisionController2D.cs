using System;
using System.Collections.Generic;
using UnityEngine;

namespace GameFlow
{
	[Help("es", "Controla los eventos de colisión en este @UnityManual{GameObject} para 2D.", null)]
	[Help("en", "Controls the collision events in this @UnityManual{GameObject} for 2D.", null)]
	[AddComponentMenu("")]
	public class CollisionController2D : EventController
	{
		[SerializeField]
		private bool _collisionEnterEnabled = true;

		[SerializeField]
		private bool _collisionExitEnabled = true;

		[SerializeField]
		private bool _collisionStayEnabled = true;

		private List<IEventListener> onCollisionEnterListeners = new List<IEventListener>();

		private List<IEventListener> onCollisionExitListeners = new List<IEventListener>();

		private List<IEventListener> onCollisionStayListeners = new List<IEventListener>();

		public static EventController AddController(GameObject gameObject)
		{
			Collider2D collider2D = gameObject.GetCollider2D(isTrigger: false);
			return (!(collider2D != null)) ? null : EventController.AddController(gameObject, typeof(CollisionController2D));
		}

		private void Awake()
		{
			base.enabled = ((base.enabled && _collisionEnterEnabled) || _collisionExitEnabled || _collisionStayEnabled);
		}

		public override void AddListener(IEventListener listener, Type eventType)
		{
			if (eventType == typeof(CollisionEnterEvent))
			{
				onCollisionEnterListeners.AddOnlyOnce(listener);
			}
			else if (eventType == typeof(CollisionExitEvent))
			{
				onCollisionExitListeners.AddOnlyOnce(listener);
			}
			else if (eventType == typeof(CollisionStayEvent))
			{
				onCollisionStayListeners.AddOnlyOnce(listener);
			}
		}

		public override void RemoveListener(IEventListener listener, Type eventType)
		{
			if (eventType == typeof(CollisionEnterEvent))
			{
				onCollisionEnterListeners.Remove(listener);
			}
			else if (eventType == typeof(CollisionExitEvent))
			{
				onCollisionExitListeners.Remove(listener);
			}
			else if (eventType == typeof(CollisionStayEvent))
			{
				onCollisionStayListeners.Remove(listener);
			}
		}

		private void OnCollisionEnter2D(Collision2D collision)
		{
			if (base.enabled && _collisionEnterEnabled)
			{
				CollisionEnterEvent collisionEnterEvent = new CollisionEnterEvent();
				InitCollisionEvent(collisionEnterEvent, collision);
				collisionEnterEvent.DispatchTo(onCollisionEnterListeners);
				collisionEnterEvent.DispatchToAll();
			}
		}

		private void OnCollisionExit2D(Collision2D collision)
		{
			if (base.enabled && _collisionExitEnabled)
			{
				CollisionExitEvent collisionExitEvent = new CollisionExitEvent();
				InitCollisionEvent(collisionExitEvent, collision);
				collisionExitEvent.DispatchTo(onCollisionExitListeners);
				collisionExitEvent.DispatchToAll();
			}
		}

		private void OnCollisionStay2D(Collision2D collision)
		{
			if (base.enabled && _collisionStayEnabled)
			{
				CollisionStayEvent collisionStayEvent = new CollisionStayEvent();
				InitCollisionEvent(collisionStayEvent, collision);
				collisionStayEvent.DispatchTo(onCollisionStayListeners);
				collisionStayEvent.DispatchToAll();
			}
		}

		private void InitCollisionEvent(CollisionEvent ce, Collision2D collision)
		{
			ce.source = base.gameObject;
			ce.other = collision.gameObject;
			ce.contactPoint = ((collision.contacts.Length <= 0) ? Vector2.zero : collision.contacts[0].point);
			ce.relativeVelocity = collision.relativeVelocity;
		}
	}
}
