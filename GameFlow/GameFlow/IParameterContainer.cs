using System.Collections.Generic;

namespace GameFlow
{
	public interface IParameterContainer : IBlockContainer
	{
		List<Parameter> GetParameters();

		void ParameterAdded(Parameter parameter);
	}
}
