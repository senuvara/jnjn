using System.Collections.Generic;

namespace GameFlow
{
	public class ExternalMessageEvent : EventObject
	{
		private static List<IEventListener> listeners = new List<IEventListener>();

		public DataType messageDataType;

		public string stringMessage;

		public int intMessage;

		public float floatMessage;

		public ExternalMessageEvent(string stringMessage)
		{
			this.stringMessage = stringMessage;
			messageDataType = DataType.String;
		}

		public ExternalMessageEvent(int intMessage)
		{
			this.intMessage = intMessage;
			messageDataType = DataType.Integer;
		}

		public ExternalMessageEvent(float floatMessage)
		{
			this.floatMessage = floatMessage;
			messageDataType = DataType.Float;
		}

		public override void DispatchToAll()
		{
			EventDispatcher.DispatchEventToListeners(this, listeners);
		}

		public static void AddListener(IEventListener listener)
		{
			listeners.AddOnlyOnce(listener);
		}

		public static void RemoveListener(IEventListener listener)
		{
			listeners.Remove(listener);
		}
	}
}
