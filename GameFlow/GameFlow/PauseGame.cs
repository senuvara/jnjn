using UnityEngine;

namespace GameFlow
{
	[Help("es", "Pone el juego en pausa.", null)]
	[AddComponentMenu("")]
	[Help("en", "Pauses the game.", null)]
	public class PauseGame : Action
	{
		private static LocalizedString alreadyPaused = LocalizedString.Create().Add("en", "Ignoring 'Game Pause' action: Game already paused.").Add("es", "Ignorando acción 'Game Pause': El juego ya está en pausa.");

		public override void Execute()
		{
			if (Game.paused)
			{
				Log.Warning(alreadyPaused);
			}
			Game.Pause();
		}
	}
}
