namespace GameFlow
{
	public class Progress
	{
		public static int minValue;

		public static int maxValue;

		public static int currentValue;

		public static string info;

		public static float delta => (float)currentValue / (float)(maxValue - minValue);
	}
}
