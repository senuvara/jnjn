using System;
using UnityEngine;

namespace GameFlow
{
	[Help("en", "A built-in @GameFlow{Variable} that returns the current value of the virtual vertical input axis.", null)]
	[AddComponentMenu("")]
	[Help("es", "Una @GameFlow{Variable} integrada que devuelve el valor del eje virtual vertical de entrada.", null)]
	public class BuiltinInputAxisY : BuiltinVariable
	{
		public override float floatValue => (!Application.isPlaying) ? 0f : Input.GetAxis("Vertical");

		public override Type GetVariableType()
		{
			return typeof(float);
		}

		protected override string GetVariableId()
		{
			return "Input Axis Y";
		}
	}
}
