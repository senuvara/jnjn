using UnityEngine;
using UnityEngine.UI;

namespace GameFlow
{
	[Help("es", "Lee una propiedad del componente Raw Image especificado.", null)]
	[AddComponentMenu("")]
	[Help("en", "Gets a property of the specified Raw Image component.", null)]
	public class GetRawImageProperty : Action, IExecutableInEditor
	{
		public enum Property
		{
			Canvas,
			CanvasRenderer,
			Color,
			DefaultMaterial,
			Depth,
			MainTexture,
			Maskable,
			Material,
			MaterialForRendering,
			RectTransform,
			Texture,
			UvRect
		}

		[SerializeField]
		private RawImage _source;

		[SerializeField]
		private Variable _sourceVar;

		[SerializeField]
		private Property _property;

		[SerializeField]
		private Variable _output;

		protected override void OnReset()
		{
			_source = base.gameObject.GetComponent<RawImage>();
		}

		public override void Setup()
		{
		}

		public override void Execute()
		{
			RawImage rawImage = _sourceVar.GetValueAsComponent(_source, typeof(RawImage)) as RawImage;
			if (!(rawImage == null))
			{
				switch (_property)
				{
				case Property.Canvas:
					_output.SetValue(rawImage.canvas);
					break;
				case Property.CanvasRenderer:
					_output.SetValue(rawImage.canvasRenderer);
					break;
				case Property.Color:
					_output.SetValue(rawImage.color);
					break;
				case Property.DefaultMaterial:
					_output.SetValue(rawImage.defaultMaterial);
					break;
				case Property.Depth:
					_output.SetValue(rawImage.depth);
					break;
				case Property.MainTexture:
					_output.SetValue(rawImage.mainTexture);
					break;
				case Property.Maskable:
					_output.SetValue(rawImage.maskable);
					break;
				case Property.Material:
					_output.SetValue(rawImage.material);
					break;
				case Property.MaterialForRendering:
					_output.SetValue(rawImage.materialForRendering);
					break;
				case Property.RectTransform:
					_output.SetValue(rawImage.rectTransform);
					break;
				case Property.Texture:
					_output.SetValue(rawImage.texture);
					break;
				case Property.UvRect:
					_output.SetValue(rawImage.uvRect);
					break;
				}
			}
		}
	}
}
