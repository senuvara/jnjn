using UnityEngine;
using UnityEngine.UI;

namespace GameFlow
{
	[AddComponentMenu("")]
	[Help("en", "Sets a property of the specified @UnityManual{Shadow}.", null)]
	[Help("es", "Modifica una propiedad del componente @UnityManual{Shadow} especificado.", null)]
	public class SetShadowProperty : Action, IExecutableInEditor
	{
		public enum Property
		{
			EffectColor,
			EffectDistance,
			UseGraphicAlpha
		}

		[SerializeField]
		private Shadow _target;

		[SerializeField]
		private Variable _targetVar;

		[SerializeField]
		private Property _property;

		[SerializeField]
		private Color _effectColorValue;

		[SerializeField]
		private Vector2 _effectDistanceValue;

		[SerializeField]
		private bool _useGraphicAlphaValue;

		[SerializeField]
		private Variable _valueVar;

		protected override void OnReset()
		{
			_target = base.gameObject.GetComponent<Shadow>();
		}

		public override void Setup()
		{
		}

		public override void Execute()
		{
			Shadow shadow = _targetVar.GetValueAsComponent(_target, typeof(Shadow)) as Shadow;
			if (!(shadow == null))
			{
				switch (_property)
				{
				case Property.EffectColor:
					shadow.effectColor = _valueVar.GetValue(_effectColorValue);
					break;
				case Property.EffectDistance:
					shadow.effectDistance = _valueVar.GetValue(_effectDistanceValue);
					break;
				case Property.UseGraphicAlpha:
					shadow.useGraphicAlpha = _valueVar.GetValue(_useGraphicAlphaValue);
					break;
				}
			}
		}
	}
}
