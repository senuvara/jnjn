using UnityEngine;

namespace GameFlow
{
	[AddComponentMenu("")]
	[Help("es", "Modifica la magnitud del vector especificado.", null)]
	[Help("en", "Sets the magnitude of the specified length.", null)]
	public class SetMagnitude : Action, IExecutableInEditor
	{
		[SerializeField]
		private Variable _variable;

		[SerializeField]
		private float _magnitude;

		[SerializeField]
		private Variable _magnitudeVar;

		public override void Execute()
		{
			if (!(_variable == null))
			{
				DataType dataType = _variable.dataType;
				float value = _magnitudeVar.GetValue(_magnitude);
				switch (dataType)
				{
				case DataType.Vector2:
				{
					Vector2 vector2Value = _variable.vector2Value;
					_variable.vector2Value = vector2Value.normalized * value;
					break;
				}
				case DataType.Vector3:
				{
					Vector3 vector3Value = _variable.vector3Value;
					_variable.vector3Value = vector3Value.normalized * value;
					break;
				}
				}
			}
		}
	}
}
