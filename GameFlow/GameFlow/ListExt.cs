using System.Collections.Generic;
using UnityEngine;

namespace GameFlow
{
	public static class ListExt
	{
		public static void AddOnlyOnce<T>(this List<T> list, T item)
		{
			if (!list.Contains(item))
			{
				list.Add(item);
			}
		}

		public static void Swap<T>(this IList<T> list, int firstIndex, int secondIndex)
		{
			if (firstIndex != secondIndex)
			{
				T value = list[firstIndex];
				list[firstIndex] = list[secondIndex];
				list[secondIndex] = value;
			}
		}

		public static void AddGameObjectHierarchy(this List list, GameObject go, bool onlyActive)
		{
			list.AddValue(go);
			Transform transform = go.transform;
			int childCount = transform.childCount;
			for (int i = 0; i < childCount; i++)
			{
				GameObject gameObject = transform.GetChild(i).gameObject;
				if (!onlyActive || gameObject.activeInHierarchy)
				{
					if (gameObject.transform.childCount > 0)
					{
						list.AddGameObjectHierarchy(gameObject, onlyActive);
					}
					else
					{
						list.AddValue(gameObject);
					}
				}
			}
		}
	}
}
