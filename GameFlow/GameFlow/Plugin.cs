using UnityEngine;

namespace GameFlow
{
	public class Plugin
	{
		private static string _version = "0.9.9b";

		private static int[] versionNumbers = VersionUtils.VersionStringToNumbers(version);

		private static GameObject control;

		public static string version => _version;

		public static int versionMajor => versionNumbers[0];

		public static int versionMinor => versionNumbers[1];

		public static int versionFix => versionNumbers[2];

		public static int versionInt => versionNumbers[0] * 1000 + versionNumbers[1] * 100 + versionNumbers[2];

		public static GameObject controlGameObject
		{
			get
			{
				if (!Application.isPlaying)
				{
					return null;
				}
				if (control == null)
				{
					control = new GameObject("[GameFlow]");
					control.hideFlags = HideFlags.HideInHierarchy;
					Object.DontDestroyOnLoad(control);
				}
				return control;
			}
		}
	}
}
