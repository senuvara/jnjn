using UnityEngine;

namespace GameFlow
{
	[Help("en", "Sets a property of the specified @UnityManual{AudioEchoFilter} component.", null)]
	[AddComponentMenu("")]
	[Help("es", "Modifica una propiedad del componente @UnityManual{AudioEchoFilter} especificado.", null)]
	public class SetAudioEchoFilterProperty : Action, IExecutableInEditor
	{
		public enum Property
		{
			DecayRatio,
			Delay,
			DryMix,
			WetMix
		}

		[SerializeField]
		private AudioEchoFilter _target;

		[SerializeField]
		private Variable _targetVar;

		[SerializeField]
		private Property _property;

		[SerializeField]
		private float _decayRatioValue;

		[SerializeField]
		private float _delayValue;

		[SerializeField]
		private float _dryMixValue;

		[SerializeField]
		private float _wetMixValue;

		[SerializeField]
		private Variable _valueVar;

		protected override void OnReset()
		{
			_target = base.gameObject.GetComponent<AudioEchoFilter>();
		}

		public override void Setup()
		{
		}

		public override void Execute()
		{
			AudioEchoFilter audioEchoFilter = _targetVar.GetValueAsComponent(_target, typeof(AudioEchoFilter)) as AudioEchoFilter;
			if (!(audioEchoFilter == null))
			{
				switch (_property)
				{
				case Property.DecayRatio:
					audioEchoFilter.decayRatio = _valueVar.GetValue(_decayRatioValue);
					break;
				case Property.Delay:
					audioEchoFilter.delay = _valueVar.GetValue(_delayValue);
					break;
				case Property.DryMix:
					audioEchoFilter.dryMix = _valueVar.GetValue(_dryMixValue);
					break;
				case Property.WetMix:
					audioEchoFilter.wetMix = _valueVar.GetValue(_wetMixValue);
					break;
				}
			}
		}
	}
}
