using UnityEngine;

namespace GameFlow
{
	[Help("en", "Evaluates whether any key is or has been just pressed.", null)]
	[AddComponentMenu("")]
	[Help("es", "Evalúa si está siendo o acaba de ser pulsada cualquier tecla.", null)]
	public class AnyKeyCondition : Condition
	{
		public enum Comparison
		{
			IsDown,
			IsJustDown
		}

		[SerializeField]
		private Comparison _comparison;

		public override bool Evaluate()
		{
			bool flag = false;
			bool flag2 = false;
			if (Input.anyKeyDown && !Input.GetMouseButtonDown(0) && !Input.GetMouseButtonDown(1) && !Input.GetMouseButtonDown(2))
			{
				flag = true;
				flag2 = true;
			}
			else if (Input.anyKey && !Input.GetMouseButton(0) && !Input.GetMouseButton(1) && !Input.GetMouseButton(2))
			{
				flag = false;
				flag2 = true;
			}
			switch (_comparison)
			{
			case Comparison.IsDown:
				return flag2;
			case Comparison.IsJustDown:
				return flag2 && flag;
			default:
				return false;
			}
		}
	}
}
