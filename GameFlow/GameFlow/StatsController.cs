using UnityEngine;

namespace GameFlow
{
	[AddComponentMenu("")]
	[DisallowMultipleComponent]
	public class StatsController : BaseBehaviour
	{
		private static StatsController _instance;

		private float _currentFPS;

		private float _maxFPS;

		private float _averageFPS;

		private float _lastInterval;

		private float _updateInterval = 0.25f;

		private int _frames;

		private int _samples;

		private float _avgsum;

		private static StatsController instance
		{
			get
			{
				Init();
				return _instance;
			}
		}

		public static float currentFPS => instance._currentFPS;

		public static float maxFPS => instance._maxFPS;

		public static float averageFPS => instance._averageFPS;

		private static void Init()
		{
			if (_instance == null)
			{
				GameObject gameObject = new GameObject("Stats Controller");
				gameObject.hideFlags = HideFlags.HideInHierarchy;
				_instance = gameObject.AddComponent<StatsController>();
			}
		}

		protected override void OnReset()
		{
			HideInInspector();
		}

		private void Start()
		{
			if (instance != this)
			{
				Object.Destroy(this);
				return;
			}
			_lastInterval = Time.realtimeSinceStartup;
			_frames = 0;
		}

		private void Update()
		{
			_frames++;
			float realtimeSinceStartup = Time.realtimeSinceStartup;
			if (realtimeSinceStartup > _lastInterval + _updateInterval)
			{
				_currentFPS = (float)_frames / (realtimeSinceStartup - _lastInterval);
				_frames = 0;
				_lastInterval = realtimeSinceStartup;
				if (_currentFPS > _maxFPS)
				{
					_maxFPS = _currentFPS;
				}
				_avgsum += _currentFPS;
				_averageFPS = _avgsum / (float)(++_samples);
			}
		}
	}
}
