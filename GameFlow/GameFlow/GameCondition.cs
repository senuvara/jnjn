using UnityEngine;

namespace GameFlow
{
	[Help("es", "Evalúa el estado general de juego (en pausa, terminado, etc.).", null)]
	[Help("en", "Evaluates the general game state (paused, over, etc.).", null)]
	[AddComponentMenu("")]
	public class GameCondition : Condition
	{
		public enum Comparison
		{
			IsStarted,
			IsNotStarted,
			IsPaused,
			IsNotPaused,
			IsOver,
			IsNotOver
		}

		[SerializeField]
		private Comparison _comparison;

		public override bool Evaluate()
		{
			switch (_comparison)
			{
			case Comparison.IsStarted:
				return Game.started;
			case Comparison.IsNotStarted:
				return !Game.started;
			case Comparison.IsPaused:
				return Game.paused;
			case Comparison.IsNotPaused:
				return !Game.paused;
			case Comparison.IsOver:
				return Game.over;
			case Comparison.IsNotOver:
				return !Game.over;
			default:
				return false;
			}
		}
	}
}
