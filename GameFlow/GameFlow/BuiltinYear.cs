using System;
using UnityEngine;

namespace GameFlow
{
	[Help("es", "Una @GameFlow{Variable} integrada que devuelve el año de la fecha actual.", null)]
	[Help("en", "A built-in @GameFlow{Variable} that returns the year for the current date.", null)]
	[AddComponentMenu("")]
	public class BuiltinYear : BuiltinVariable
	{
		public override int intValue => DateTime.Now.Year;

		public override Type GetVariableType()
		{
			return typeof(int);
		}

		protected override string GetVariableId()
		{
			return "Year";
		}
	}
}
