using UnityEngine;

namespace GameFlow
{
	[Help("es", "Establece el siguiente estado (@GameFlow{State}) al que saltar una vez que el actual finalice.", null)]
	[AddComponentMenu("")]
	[Help("en", "Sets the next @GameFlow{State} to jump after the current one finishes.", null)]
	public class SetNextState : Action, IExecutableInEditor
	{
		[SerializeField]
		private State _state;

		[SerializeField]
		private Variable _stateVar;

		public State state => _stateVar.GetValue(_state) as State;

		public override void Execute()
		{
			Execute(state);
		}

		public static void Execute(State state)
		{
			if (!(state == null))
			{
				StateMachine stateMachine = state.container as StateMachine;
				if (!(stateMachine == null))
				{
					stateMachine.currentState = state;
				}
			}
		}
	}
}
