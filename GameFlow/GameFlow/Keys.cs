using System.Collections.Generic;
using UnityEngine;

namespace GameFlow
{
	[Help("en", "Shows all the @GameFlow{Key} components defined in the inspected @UnityManual{GameObject}.", null)]
	[AddComponentMenu("")]
	[Help("es", "Muestra todos los componentes 'Tecla' (@GameFlow{Key}) definidos en el @UnityManual{GameObject}.", null)]
	[DisallowMultipleComponent]
	public class Keys : Container, IBlockContainer, IData, IKeyContainer
	{
		[SerializeField]
		private List<Key> _keys = new List<Key>();

		public List<Key> keys => _keys;

		public List<Key> GetKeys()
		{
			return _keys;
		}

		public virtual void KeyAdded(Key key)
		{
		}

		public override List<Block> GetBlocks()
		{
			return new List<Block>(_keys.ToArray());
		}

		public override bool Contains(Block block)
		{
			Key key = block as Key;
			if (key != null)
			{
				return _keys.Contains(key);
			}
			return false;
		}

		private void Update()
		{
			for (int i = 0; i < keys.Count; i++)
			{
				Key key = _keys[i];
				if (key == null || !key.enabledInEditor || (Game.paused && !key.enabledInPause))
				{
					continue;
				}
				if (Input.GetKeyDown(key.code))
				{
					key.justDown = true;
					key.isDown = true;
					key.justUp = Input.GetKeyUp(key.code);
					FireKeyDownEvent(key);
					continue;
				}
				if (Input.GetKey(key.code))
				{
					key.justDown = false;
					key.isDown = true;
					key.justUp = false;
					continue;
				}
				if (Input.GetKeyUp(key.code))
				{
					key.justDown = false;
					key.isDown = false;
					key.justUp = true;
					FireKeyUpEvent(key);
					continue;
				}
				if (key.justDown && key.justUp)
				{
					FireKeyUpEvent(key);
				}
				else
				{
					key.justUp = false;
				}
				key.justDown = false;
				key.isDown = false;
			}
		}

		private void FireKeyDownEvent(Key key)
		{
			KeyDownEvent keyDownEvent = new KeyDownEvent();
			keyDownEvent.key = key;
			keyDownEvent.DispatchToAll();
		}

		private void FireKeyUpEvent(Key key)
		{
			KeyUpEvent keyUpEvent = new KeyUpEvent();
			keyUpEvent.key = key;
			keyUpEvent.DispatchToAll();
		}
	}
}
