using UnityEngine;

namespace GameFlow
{
	[Help("en", "Evaluates properties of the specified @GameFlow{Timer} component.", null)]
	[Help("es", "Evalúa las propiedades del componente temporizador (@GameFlow{Timer}) especificado.", null)]
	[AddComponentMenu("")]
	public class TimerCondition : Condition
	{
		public enum Comparison
		{
			IsExpired,
			IsNotExpired,
			IsStopped,
			IsNotStopped
		}

		[SerializeField]
		private Timer _timer;

		[SerializeField]
		private Variable _timerVar;

		[SerializeField]
		private Comparison _comparison;

		public override bool Evaluate()
		{
			Timer timer = _timerVar.GetValue(_timer) as Timer;
			if (timer == null)
			{
				return false;
			}
			switch (_comparison)
			{
			case Comparison.IsExpired:
				return timer.expired;
			case Comparison.IsNotExpired:
				return !timer.expired;
			case Comparison.IsStopped:
				return timer.stopped;
			case Comparison.IsNotStopped:
				return !timer.stopped;
			default:
				return false;
			}
		}
	}
}
