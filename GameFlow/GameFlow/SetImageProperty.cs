using UnityEngine;
using UnityEngine.UI;

namespace GameFlow
{
	[Help("es", "Modifica el valor de una propiedad del componente Image especificado.", null)]
	[Help("en", "Modifies the value of a property of the specified Image component.", null)]
	[AddComponentMenu("")]
	public class SetImageProperty : Action, IExecutableInEditor
	{
		public enum Property
		{
			Color,
			AlphaHitTestMinimumThreshold,
			FillAmount,
			FillCenter,
			FillClockwise,
			FillMethod,
			FillOrigin,
			Maskable,
			Material,
			OverrideSprite,
			PreserveAspect,
			Sprite,
			Type
		}

		[SerializeField]
		private Image _target;

		[SerializeField]
		private Variable _targetVar;

		[SerializeField]
		private Property _property;

		[SerializeField]
		private Color _colorValue;

		[SerializeField]
		private float _eventAlphaThresholdValue;

		[SerializeField]
		private float _fillAmountValue;

		[SerializeField]
		private bool _fillCenterValue;

		[SerializeField]
		private bool _fillClockwiseValue;

		[SerializeField]
		private Image.FillMethod _fillMethodValue;

		[SerializeField]
		private int _fillOriginValue;

		[SerializeField]
		private bool _maskableValue;

		[SerializeField]
		private Material _materialValue;

		[SerializeField]
		private Sprite _overrideSpriteValue;

		[SerializeField]
		private bool _preserveAspectValue;

		[SerializeField]
		private Sprite _spriteValue;

		[SerializeField]
		private Image.Type _typeValue;

		[SerializeField]
		private Variable _valueVar;

		public Image target => _targetVar.GetValueAsComponent(_target, typeof(Image)) as Image;

		protected override void OnReset()
		{
			_target = base.gameObject.GetComponent<Image>();
		}

		public override void Setup()
		{
		}

		public override void Execute()
		{
			Image target = this.target;
			if (!(target == null))
			{
				switch (_property)
				{
				case Property.Color:
					target.color = _valueVar.GetValue(_colorValue);
					break;
				case Property.AlphaHitTestMinimumThreshold:
					target.alphaHitTestMinimumThreshold = _valueVar.GetValue(_eventAlphaThresholdValue);
					break;
				case Property.FillAmount:
					target.fillAmount = _valueVar.GetValue(_fillAmountValue);
					break;
				case Property.FillCenter:
					target.fillCenter = _valueVar.GetValue(_fillCenterValue);
					break;
				case Property.FillClockwise:
					target.fillClockwise = _valueVar.GetValue(_fillClockwiseValue);
					break;
				case Property.FillMethod:
					target.fillMethod = (Image.FillMethod)(object)_valueVar.GetValue(_fillMethodValue);
					break;
				case Property.FillOrigin:
					target.fillOrigin = _valueVar.GetValue(_fillOriginValue);
					break;
				case Property.Maskable:
					target.maskable = _valueVar.GetValue(_maskableValue);
					break;
				case Property.Material:
					target.material = (_valueVar.GetValue(_materialValue) as Material);
					break;
				case Property.OverrideSprite:
					target.overrideSprite = (_valueVar.GetValue(_overrideSpriteValue) as Sprite);
					break;
				case Property.PreserveAspect:
					target.preserveAspect = _valueVar.GetValue(_preserveAspectValue);
					break;
				case Property.Sprite:
					target.sprite = (_valueVar.GetValue(_spriteValue) as Sprite);
					break;
				case Property.Type:
					target.type = (Image.Type)(object)_valueVar.GetValue(_typeValue);
					break;
				}
			}
		}
	}
}
