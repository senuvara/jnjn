using UnityEngine;

namespace GameFlow
{
	[Help("en", "Sets the specified @GameFlow{State} as the current one in its @GameFlow{StateMachine}.", null)]
	[AddComponentMenu("")]
	[Help("es", "Establece el estado (@GameFlow{State}) especificado como el actual en su @GameFlow{StateMachine}.", null)]
	public class SetCurrentState : Action, IExecutableInEditor
	{
		[SerializeField]
		private State _state;

		[SerializeField]
		private Variable _stateVar;

		public State state => _stateVar.GetValue(_state) as State;

		public override void Execute()
		{
			Execute(state);
		}

		public static void Execute(State state)
		{
			if (state == null)
			{
				return;
			}
			StateMachine stateMachine = state.container as StateMachine;
			if (!(stateMachine == null))
			{
				if (stateMachine.currentState != null)
				{
					stateMachine.currentState.Break();
				}
				stateMachine.currentState = state;
			}
		}
	}
}
