using UnityEngine;

namespace GameFlow
{
	[Help("es", "Mueve el objeto especificado hasta que su posición alcance la de otro objeto.", null)]
	[Help("en", "Moves the specified object to match the position of another object.", null)]
	[AddComponentMenu("")]
	public class MoveTo : Move
	{
	}
}
