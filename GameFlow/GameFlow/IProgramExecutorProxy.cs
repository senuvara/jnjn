namespace GameFlow
{
	public interface IProgramExecutorProxy
	{
		void ExecuteProgram(Program program, bool yields, bool logElapsedTime, bool ignorePause);

		void StopExecution();

		bool IsPlaying();

		Program GetRunningProgram();
	}
}
