namespace GameFlow
{
	public abstract class PointerEventProgram : EventProgram
	{
		private Parameter _sourceParam;

		private Parameter _positionParam;

		public Parameter sourceParam
		{
			get
			{
				if (_sourceParam == null)
				{
					_sourceParam = GetParameter("Source");
				}
				return _sourceParam;
			}
		}

		public Parameter positionParam
		{
			get
			{
				if (_positionParam == null)
				{
					_positionParam = GetParameter("Position");
				}
				return _positionParam;
			}
		}

		protected override void CacheParameters()
		{
			CacheParameter(sourceParam);
			CacheParameter(positionParam);
		}
	}
}
