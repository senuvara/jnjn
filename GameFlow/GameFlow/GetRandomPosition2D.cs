using UnityEngine;

namespace GameFlow
{
	[Help("en", "Gets a random 2D position in the specified space.", null)]
	[AddComponentMenu("")]
	[Help("es", "Devuelve una posición 2D aleatoria en el espacio indicado.", null)]
	public class GetRandomPosition2D : Action, IExecutableInEditor
	{
		public enum Space
		{
			Local,
			World,
			Circle
		}

		[SerializeField]
		private Space _space = Space.World;

		[SerializeField]
		private Transform _transform;

		[SerializeField]
		private Variable _transformVar;

		[SerializeField]
		private Vector2 _min = Vector2.zero;

		[SerializeField]
		private Variable _minVar;

		[SerializeField]
		private Vector2 _max = new Vector2(1f, 1f);

		[SerializeField]
		private Variable _maxVar;

		[SerializeField]
		private Vector2 _center = Vector2.zero;

		[SerializeField]
		private Variable _centerVar;

		[SerializeField]
		private float _radius = 1f;

		[SerializeField]
		private Variable _radiusVar;

		[SerializeField]
		private Variable _output;

		public Vector2 min => _minVar.GetValue(_min);

		public Vector2 max => _maxVar.GetValue(_max);

		public Vector2 center => _centerVar.GetValue(_center);

		public float radius => _radiusVar.GetValue(_radius);

		public override void Execute()
		{
			if (_output == null)
			{
				return;
			}
			switch (_space)
			{
			case Space.Local:
			{
				Vector2 value = RandomUtils.GetRandomVector2(min, max);
				Transform transform = _transformVar.GetValueAsComponent(_transform, typeof(Transform)) as Transform;
				if (transform != null)
				{
					value = transform.position + transform.TransformDirection(value);
				}
				_output.SetValue(value);
				break;
			}
			case Space.World:
			{
				Vector2 value = RandomUtils.GetRandomVector2(min, max);
				_output.SetValue(value);
				break;
			}
			case Space.Circle:
			{
				Vector2 value = center + Random.insideUnitCircle * radius;
				_output.SetValue(value);
				break;
			}
			}
		}
	}
}
