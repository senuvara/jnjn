using System;
using UnityEngine;

namespace GameFlow
{
	[Help("es", "Define un parámetro del tipo especificado.", null)]
	[Help("en", "Defines a parameter of the specified type.", null)]
	[AddComponentMenu("GameFlow/Data/Parameter")]
	public class Parameter : Variable
	{
		[SerializeField]
		private string _label;

		[SerializeField]
		private bool _hidden;

		private void Reset()
		{
			HideInInspector();
			Disable();
			SetContainer();
			SetResetFlag();
			OnReset();
		}

		protected override void OnReset()
		{
			base.OnReset();
			_label = id;
		}

		public override string GetDefaultId()
		{
			return "Parameter" + UnityEngine.Random.Range(1000, 9999);
		}

		private void SetContainer()
		{
			if (container != null || !BlockInsertion.enabled || BlockInsertion.clipboard)
			{
				return;
			}
			IParameterContainer parameterContainer = BlockInsertion.target as IParameterContainer;
			if (parameterContainer == null)
			{
				parameterContainer = base.gameObject.GetComponent<Parameters>();
				if (parameterContainer == null)
				{
					parameterContainer = base.gameObject.AddComponent<Parameters>();
				}
				BlockInsertion.index = -1;
			}
			if (BlockInsertion.index < 0 || BlockInsertion.index >= parameterContainer.GetParameters().Count)
			{
				if (!parameterContainer.GetParameters().Contains(this))
				{
					parameterContainer.GetParameters().Add(this);
					parameterContainer.ParameterAdded(this);
					parameterContainer.SetDirty(dirty: true);
				}
			}
			else
			{
				parameterContainer.GetParameters().Insert(BlockInsertion.index, this);
				parameterContainer.ParameterAdded(this);
				parameterContainer.SetDirty(dirty: true);
			}
			container = (parameterContainer as UnityEngine.Object);
			BlockInsertion.Reset();
		}

		public override void AddListener(IEventListener listener, Type eventType)
		{
			if (eventType == typeof(ParameterChangeEvent))
			{
				_changeListeners.AddOnlyOnce((UnityEngine.Object)listener);
			}
		}

		public override void AddListener(IEventListener listener)
		{
			AddListener(listener, typeof(ParameterChangeEvent));
		}

		public override void RemoveListener(IEventListener listener, Type eventType)
		{
			if (eventType == typeof(ParameterChangeEvent))
			{
				_changeListeners.Remove((UnityEngine.Object)listener);
			}
		}

		public override void RemoveListener(IEventListener listener)
		{
			RemoveListener(listener, typeof(ParameterChangeEvent));
		}

		public override void DispatchChangeEvent()
		{
			ParameterChangeEvent parameterChangeEvent = new ParameterChangeEvent();
			parameterChangeEvent.parameter = this;
			parameterChangeEvent.DispatchTo(_changeListeners);
		}

		public override string GetTypeForSelectors()
		{
			return "Parameter";
		}
	}
}
