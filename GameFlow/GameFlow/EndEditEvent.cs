using System.Collections.Generic;
using UnityEngine;

namespace GameFlow
{
	public class EndEditEvent : UIEvent
	{
		private static List<IEventListener> listeners = new List<IEventListener>();

		public string value;

		public EndEditEvent(GameObject source, string value)
		{
			base.source = source;
			this.value = value;
		}

		public override void DispatchToAll()
		{
			EventDispatcher.DispatchEventToListeners(this, listeners);
		}

		public new static void AddListener(IEventListener listener)
		{
			listeners.AddOnlyOnce(listener);
		}

		public new static void RemoveListener(IEventListener listener)
		{
			listeners.Remove(listener);
		}
	}
}
