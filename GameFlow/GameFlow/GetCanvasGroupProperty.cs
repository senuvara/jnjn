using UnityEngine;

namespace GameFlow
{
	[Help("es", "Lee una propiedad del componente @UnityManual{CanvasGroup} especificado.", null)]
	[AddComponentMenu("")]
	[Help("en", "Gets a property of the specified @UnityManual{CanvasGroup} component.", null)]
	public class GetCanvasGroupProperty : Action, IExecutableInEditor
	{
		public enum Property
		{
			Alpha,
			BlocksRaycasts,
			IgnoreParentGroups,
			Interactable
		}

		[SerializeField]
		private CanvasGroup _source;

		[SerializeField]
		private Variable _sourceVar;

		[SerializeField]
		private Property _property;

		[SerializeField]
		private Variable _output;

		protected override void OnReset()
		{
			_source = base.gameObject.GetComponent<CanvasGroup>();
		}

		public override void Setup()
		{
		}

		public override void Execute()
		{
			CanvasGroup canvasGroup = _sourceVar.GetValueAsComponent(_source, typeof(CanvasGroup)) as CanvasGroup;
			if (!(canvasGroup == null))
			{
				switch (_property)
				{
				case Property.Alpha:
					_output.SetValue(canvasGroup.alpha);
					break;
				case Property.BlocksRaycasts:
					_output.SetValue(canvasGroup.blocksRaycasts);
					break;
				case Property.IgnoreParentGroups:
					_output.SetValue(canvasGroup.ignoreParentGroups);
					break;
				case Property.Interactable:
					_output.SetValue(canvasGroup.interactable);
					break;
				}
			}
		}
	}
}
