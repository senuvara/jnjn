using System;
using UnityEngine;

namespace GameFlow
{
	[Help("en", "A built-in @GameFlow{Variable} that returns the month number for the current date.", null)]
	[AddComponentMenu("")]
	[Help("es", "Una @GameFlow{Variable} integrada que devuelve el número del mes actual.", null)]
	public class BuiltinMonth : BuiltinVariable
	{
		public override int intValue => DateTime.Now.Month;

		public override Type GetVariableType()
		{
			return typeof(int);
		}

		protected override string GetVariableId()
		{
			return "Month";
		}
	}
}
