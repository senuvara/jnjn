using UnityEngine;

namespace GameFlow
{
	[Help("es", "Espera durante un intervalor de tiempo aleatorio.", null)]
	[Help("en", "Waits for a random time interval.", null)]
	[AddComponentMenu("")]
	public class RandomWait : TimeAction
	{
		[SerializeField]
		private float _min;

		[SerializeField]
		private Variable _minVar;

		[SerializeField]
		private float _max = 1f;

		[SerializeField]
		private Variable _maxVar;

		[SerializeField]
		private Variable _output;

		public override float GetDefaultDuration()
		{
			return Random.Range(_min, _max);
		}

		public override void FirstStep()
		{
			base.duration = Random.Range(_minVar.GetValue(_min), _maxVar.GetValue(_max));
			base.FirstStep();
		}

		public override bool Finished()
		{
			bool flag = base.Finished();
			if (flag && _output != null)
			{
				_output.SetValue(base.duration);
			}
			return flag;
		}
	}
}
