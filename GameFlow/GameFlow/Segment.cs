using UnityEngine;

namespace GameFlow
{
	public class Segment
	{
		public Vector3 startPosition
		{
			get;
			private set;
		}

		public Vector3 endPosition
		{
			get;
			private set;
		}

		public Quaternion startRotation
		{
			get;
			private set;
		}

		public Quaternion endRotation
		{
			get;
			private set;
		}

		public Vector3 startScale
		{
			get;
			private set;
		}

		public Vector3 endScale
		{
			get;
			private set;
		}

		public float length
		{
			get;
			private set;
		}

		public float offset
		{
			get;
			private set;
		}

		public float limit
		{
			get;
			private set;
		}

		public Segment(Vector3 startPosition, Vector3 endPosition, Quaternion startRotation, Quaternion endRotation, Vector3 startScale, Vector3 endScale, float offset)
		{
			this.startPosition = startPosition;
			this.endPosition = endPosition;
			this.startRotation = startRotation;
			this.endRotation = endRotation;
			this.startScale = startScale;
			this.endScale = endScale;
			this.offset = offset;
			length = Vector3.Distance(startPosition, endPosition);
			limit = offset + length;
		}

		public Vector3 GetPosition(float t)
		{
			return Vector3.Lerp(startPosition, endPosition, t);
		}

		public Quaternion GetRotation(float t)
		{
			return Quaternion.Slerp(startRotation, endRotation, t);
		}

		public Vector3 GetScale(float t)
		{
			return Vector3.Lerp(startScale, endScale, t);
		}
	}
}
