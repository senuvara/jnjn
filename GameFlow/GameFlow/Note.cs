using UnityEngine;

namespace GameFlow
{
	[AddComponentMenu("GameFlow/Tools/Note")]
	[Help("en", "Defines a text note in the Inspector.", null)]
	[Help("es", "Define una nota de texto en el inspector.", null)]
	public class Note : BaseBehaviour, ITool
	{
		[SerializeField]
		[TextArea(2, 1024)]
		private string _text = "This is an editable note.";

		[SerializeField]
		private bool _highlight = true;

		public string text
		{
			get
			{
				return _text;
			}
			set
			{
				_text = value;
			}
		}
	}
}
