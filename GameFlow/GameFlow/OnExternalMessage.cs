using UnityEngine;

namespace GameFlow
{
	[DisallowMultipleComponent]
	[Help("en", "Program that will be executed when a message from the web container is received.", null)]
	[AddComponentMenu("GameFlow/Programs/On External Message")]
	[Help("es", "Programa que se ejecutará cada vez que un mensaje del contenedor web sea recibido.", null)]
	public class OnExternalMessage : EventProgram
	{
		private Parameter _messageParam;

		protected Parameter messageParam
		{
			get
			{
				if (_messageParam == null)
				{
					_messageParam = GetParameter("Message");
				}
				return _messageParam;
			}
		}

		protected override void CacheParameters()
		{
			CacheParameter(messageParam);
		}

		protected override void OnAwake()
		{
			base.OnAwake();
			ExternalMessageController.Init();
		}

		protected override void RegisterAsListener()
		{
			ExternalMessageEvent.AddListener(this);
		}

		protected override void UnRegisterAsListener()
		{
			ExternalMessageEvent.RemoveListener(this);
		}

		protected override void SetParameters(EventObject e)
		{
			ExternalMessageEvent externalMessageEvent = e as ExternalMessageEvent;
			switch (externalMessageEvent.messageDataType)
			{
			case DataType.String:
				messageParam.SetValue(externalMessageEvent.stringMessage);
				break;
			case DataType.Integer:
				messageParam.SetValue(externalMessageEvent.intMessage);
				break;
			case DataType.Float:
				messageParam.SetValue(externalMessageEvent.floatMessage);
				break;
			}
		}
	}
}
