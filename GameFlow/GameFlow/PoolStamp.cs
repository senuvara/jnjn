using UnityEngine;

namespace GameFlow
{
	[AddComponentMenu("")]
	[DisallowMultipleComponent]
	public class PoolStamp : BaseBehaviour, IHidden
	{
		public Pool pool;

		protected override void OnReset()
		{
			HideInInspector();
		}
	}
}
