using UnityEngine;
using UnityEngine.UI;

namespace GameFlow
{
	[AddComponentMenu("")]
	[Help("es", "Modifica el valor de una propiedad del componente Canvas Scaler especificado.", null)]
	[Help("en", "Modifies the value of a property of the specified Canvas Scaler component.", null)]
	public class SetCanvasScalerProperty : Action, IExecutableInEditor
	{
		public enum Property
		{
			DefaultSpriteDPI,
			DynamicPixelsPerUnit,
			FallbackScreenDPI,
			MatchWidthOrHeight,
			PhysicalUnit,
			ReferencePixelsPerUnit,
			ReferenceResolution,
			ScaleFactor,
			ScreenMatchMode,
			UiScaleMode
		}

		[SerializeField]
		private CanvasScaler _target;

		[SerializeField]
		private Variable _targetVar;

		[SerializeField]
		private Property _property;

		[SerializeField]
		private float _defaultSpriteDPIValue;

		[SerializeField]
		private float _dynamicPixelsPerUnitValue;

		[SerializeField]
		private float _fallbackScreenDPIValue;

		[SerializeField]
		private float _matchWidthOrHeightValue;

		[SerializeField]
		private CanvasScaler.Unit _physicalUnitValue;

		[SerializeField]
		private float _referencePixelsPerUnitValue;

		[SerializeField]
		private Vector2 _referenceResolutionValue;

		[SerializeField]
		private float _scaleFactorValue;

		[SerializeField]
		private CanvasScaler.ScreenMatchMode _screenMatchModeValue;

		[SerializeField]
		private CanvasScaler.ScaleMode _uiScaleModeValue;

		[SerializeField]
		private Variable _valueVar;

		protected override void OnReset()
		{
			_target = base.gameObject.GetComponent<CanvasScaler>();
		}

		public override void Setup()
		{
		}

		public override void Execute()
		{
			CanvasScaler canvasScaler = _targetVar.GetValueAsComponent(_target, typeof(CanvasScaler)) as CanvasScaler;
			if (!(canvasScaler == null))
			{
				switch (_property)
				{
				case Property.DefaultSpriteDPI:
					canvasScaler.defaultSpriteDPI = _valueVar.GetValue(_defaultSpriteDPIValue);
					break;
				case Property.DynamicPixelsPerUnit:
					canvasScaler.dynamicPixelsPerUnit = _valueVar.GetValue(_dynamicPixelsPerUnitValue);
					break;
				case Property.FallbackScreenDPI:
					canvasScaler.fallbackScreenDPI = _valueVar.GetValue(_fallbackScreenDPIValue);
					break;
				case Property.MatchWidthOrHeight:
					canvasScaler.matchWidthOrHeight = _valueVar.GetValue(_matchWidthOrHeightValue);
					break;
				case Property.PhysicalUnit:
					canvasScaler.physicalUnit = (CanvasScaler.Unit)(object)_valueVar.GetValue(_physicalUnitValue);
					break;
				case Property.ReferencePixelsPerUnit:
					canvasScaler.referencePixelsPerUnit = _valueVar.GetValue(_referencePixelsPerUnitValue);
					break;
				case Property.ReferenceResolution:
					canvasScaler.referenceResolution = _valueVar.GetValue(_referenceResolutionValue);
					break;
				case Property.ScaleFactor:
					canvasScaler.scaleFactor = _valueVar.GetValue(_scaleFactorValue);
					break;
				case Property.ScreenMatchMode:
					canvasScaler.screenMatchMode = (CanvasScaler.ScreenMatchMode)(object)_valueVar.GetValue(_screenMatchModeValue);
					break;
				case Property.UiScaleMode:
					canvasScaler.uiScaleMode = (CanvasScaler.ScaleMode)(object)_valueVar.GetValue(_uiScaleModeValue);
					break;
				}
			}
		}
	}
}
