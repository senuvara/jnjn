using UnityEngine;

namespace GameFlow
{
	[AddComponentMenu("")]
	[Help("en", "Sets a general property of the application.", null)]
	[Help("es", "Modifica una propiedad general de la aplicación.", null)]
	public class SetApplicationProperty : Action, IExecutableInEditor
	{
		public enum Property
		{
			RunInBackground,
			TargetFrameRate
		}

		[SerializeField]
		private Property _property = Property.TargetFrameRate;

		[SerializeField]
		private bool _boolValue;

		[SerializeField]
		private int _intValue = 60;

		[SerializeField]
		private Variable _valueVar;

		protected override void OnReset()
		{
		}

		public override void Setup()
		{
		}

		public override void Execute()
		{
			switch (_property)
			{
			case Property.RunInBackground:
				Application.runInBackground = _valueVar.GetValue(_boolValue);
				break;
			case Property.TargetFrameRate:
				Application.targetFrameRate = _valueVar.GetValue(_intValue);
				QualitySettings.vSyncCount = 0;
				break;
			}
		}
	}
}
