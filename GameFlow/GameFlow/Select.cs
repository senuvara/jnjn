using UnityEngine;
using UnityEngine.UI;

namespace GameFlow
{
	[Help("es", "Selecciona el component UI especificado.", null)]
	[Help("en", "Selects the specified UI component.", null)]
	[AddComponentMenu("")]
	public class Select : Action
	{
		[SerializeField]
		private Selectable _target;

		[SerializeField]
		private Variable _targetVar;

		public Selectable target => _targetVar.GetValueAsComponent(_target, typeof(Selectable)) as Selectable;

		protected override void OnReset()
		{
			_target = base.gameObject.GetComponent<Selectable>();
		}

		public override void Execute()
		{
			Execute(target);
		}

		public static void Execute(Selectable target)
		{
			if (!(target == null))
			{
				target.Select();
			}
		}
	}
}
