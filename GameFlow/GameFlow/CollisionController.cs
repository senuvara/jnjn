using System;
using System.Collections.Generic;
using UnityEngine;

namespace GameFlow
{
	[Help("en", "Controls the collision events in this @UnityManual{GameObject}.", null)]
	[AddComponentMenu("")]
	[Help("es", "Controla los eventos de colisión en este @UnityManual{GameObject}.", null)]
	public class CollisionController : EventController
	{
		[SerializeField]
		private bool _collisionEnterEnabled = true;

		[SerializeField]
		private bool _collisionExitEnabled = true;

		[SerializeField]
		private bool _collisionStayEnabled = true;

		private List<IEventListener> onCollisionEnterListeners = new List<IEventListener>();

		private List<IEventListener> onCollisionExitListeners = new List<IEventListener>();

		private List<IEventListener> onCollisionStayListeners = new List<IEventListener>();

		public static EventController AddController(GameObject gameObject)
		{
			Collider collider = gameObject.GetCollider(isTrigger: false);
			return (!(collider != null)) ? null : EventController.AddController(gameObject, typeof(CollisionController));
		}

		private void Awake()
		{
			base.enabled = ((base.enabled && _collisionEnterEnabled) || _collisionExitEnabled || _collisionStayEnabled);
		}

		public override void AddListener(IEventListener listener, Type eventType)
		{
			if (eventType == typeof(CollisionEnterEvent))
			{
				onCollisionEnterListeners.AddOnlyOnce(listener);
			}
			else if (eventType == typeof(CollisionExitEvent))
			{
				onCollisionExitListeners.AddOnlyOnce(listener);
			}
			else if (eventType == typeof(CollisionStayEvent))
			{
				onCollisionStayListeners.AddOnlyOnce(listener);
			}
		}

		public override void RemoveListener(IEventListener listener, Type eventType)
		{
			if (eventType == typeof(CollisionEnterEvent))
			{
				onCollisionEnterListeners.Remove(listener);
			}
			else if (eventType == typeof(CollisionExitEvent))
			{
				onCollisionExitListeners.Remove(listener);
			}
			else if (eventType == typeof(CollisionStayEvent))
			{
				onCollisionStayListeners.Remove(listener);
			}
		}

		private void OnCollisionEnter(Collision collision)
		{
			if (base.enabled && _collisionEnterEnabled)
			{
				CollisionEnterEvent collisionEnterEvent = new CollisionEnterEvent();
				InitCollisionEvent(collisionEnterEvent, collision);
				collisionEnterEvent.DispatchTo(onCollisionEnterListeners);
				collisionEnterEvent.DispatchToAll();
			}
		}

		private void OnCollisionExit(Collision collision)
		{
			if (base.enabled && _collisionExitEnabled)
			{
				CollisionExitEvent collisionExitEvent = new CollisionExitEvent();
				InitCollisionEvent(collisionExitEvent, collision);
				collisionExitEvent.DispatchTo(onCollisionExitListeners);
				collisionExitEvent.DispatchToAll();
			}
		}

		private void OnCollisionStay(Collision collision)
		{
			if (base.enabled && _collisionStayEnabled)
			{
				CollisionStayEvent collisionStayEvent = new CollisionStayEvent();
				InitCollisionEvent(collisionStayEvent, collision);
				collisionStayEvent.DispatchTo(onCollisionStayListeners);
				collisionStayEvent.DispatchToAll();
			}
		}

		private void InitCollisionEvent(CollisionEvent ce, Collision collision)
		{
			ce.source = base.gameObject;
			ce.other = collision.gameObject;
			ce.contactPoint = ((collision.contacts.Length <= 0) ? Vector3.zero : collision.contacts[0].point);
			ce.relativeVelocity = collision.relativeVelocity;
		}
	}
}
