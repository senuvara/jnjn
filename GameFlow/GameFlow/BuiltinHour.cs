using System;
using UnityEngine;

namespace GameFlow
{
	[AddComponentMenu("")]
	[Help("en", "A built-in @GameFlow{Variable} that returns the hour for the current time.", null)]
	[Help("es", "Una @GameFlow{Variable} integrada que devuelve la hora actual.", null)]
	public class BuiltinHour : BuiltinVariable
	{
		public override int intValue => DateTime.Now.Hour;

		public override string stringValue => Convert.ToString(DateTime.Now.Hour).PadLeft(2, '0');

		public override Type GetVariableType()
		{
			return typeof(int);
		}

		protected override string GetVariableId()
		{
			return "Hour";
		}
	}
}
