namespace GameFlow
{
	public enum DataType
	{
		String,
		Integer,
		Float,
		Boolean,
		Vector2,
		Vector3,
		Rect,
		Color,
		Object,
		Enum,
		Toggle,
		Tag,
		Layer,
		Bounds,
		Quaternion,
		Vector4,
		AnimationCurve
	}
}
