using UnityEngine;
using UnityEngine.SceneManagement;

namespace GameFlow
{
	[AddComponentMenu("")]
	[Help("es", "Lee una propiedad de la escena actual.", null)]
	[Help("en", "Gets a property of the current scene.", null)]
	public class GetSceneProperty : Action, IExecutableInEditor
	{
		public enum Property
		{
			Index,
			Name
		}

		[SerializeField]
		private Property _property;

		[SerializeField]
		private Variable _output;

		public override void Execute()
		{
			if (!(_output == null))
			{
				Scene activeScene = SceneManager.GetActiveScene();
				switch (_property)
				{
				case Property.Index:
					_output.SetValue(activeScene.buildIndex);
					break;
				case Property.Name:
					_output.SetValue(activeScene.name);
					break;
				}
			}
		}
	}
}
