using UnityEngine;

namespace GameFlow
{
	[Help("en", "Program that will be executed every frame while a Collider component in the listening range is colliding with another component.", null)]
	[AddComponentMenu("GameFlow/Programs/On Collision Stay")]
	[DisallowMultipleComponent]
	[Help("es", "Programa que se ejecutará en cada fotograma mientras un Collider en el rango de escucha este colisionando con otro componente.", null)]
	public class OnCollisionStay : OnCollisionProgram
	{
		private EventController[] _controllers;

		protected override void RegisterAsListener()
		{
			_controllers = RegisterAsListener(typeof(CollisionStayEvent), typeof(CollisionController), CollisionController.AddController);
			if (base.listeningRange == EventListeningRange.SceneHierarchy)
			{
				CollisionStayEvent.AddListener(this);
			}
		}

		protected override void UnRegisterAsListener()
		{
			CollisionStayEvent.RemoveListener(this);
			UnRegisterAsListener(typeof(CollisionStayEvent), typeof(CollisionController));
		}

		private void OnTransformChildrenChanged()
		{
			if (base.listeningRange == EventListeningRange.GameObjectHierarchy)
			{
				UnsubscribeFrom(_controllers, typeof(CollisionStayEvent));
				_controllers = EventController.GetControllersInHierarchyOf(typeof(CollisionController), base.listeningTarget, CollisionController.AddController);
				SubscribeTo(_controllers, typeof(CollisionStayEvent));
			}
		}
	}
}
