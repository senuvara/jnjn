using UnityEngine;

namespace GameFlow
{
	[Help("en", "Modifies the value of a property of the specified GUIText component.", null)]
	[AddComponentMenu("")]
	[Help("es", "Modifica el valor de una propiedad del componente GUIText especificado.", null)]
	public class SetGUITextProperty : Action, IExecutableInEditor
	{
		public enum Property
		{
			Alignment,
			Anchor,
			Color,
			Enabled,
			Font,
			FontSize,
			FontStyle,
			LineSpacing,
			PixelOffset,
			RichText,
			ScreenRect,
			TabSize,
			Text
		}

		[SerializeField]
		private GUIText _target;

		[SerializeField]
		private Variable _targetVar;

		[SerializeField]
		private Property _property = Property.Text;

		[SerializeField]
		private TextAlignment _alignmentValue;

		[SerializeField]
		private TextAnchor _anchorValue;

		[SerializeField]
		private Color _colorValue = Color.white;

		[SerializeField]
		private bool _enabledValue = true;

		[SerializeField]
		private Font _fontValue;

		[SerializeField]
		private int _fontSizeValue;

		[SerializeField]
		private FontStyle _fontStyleValue;

		[SerializeField]
		private float _lineSpacingValue;

		[SerializeField]
		private Vector2 _pixelOffsetValue;

		[SerializeField]
		private bool _richTextValue;

		[SerializeField]
		private Rect _screenRectValue;

		[SerializeField]
		private float _tabSizeValue;

		[SerializeField]
		private string _textValue;

		[SerializeField]
		private Variable _valueVar;

		[SerializeField]
		private string _format;

		[SerializeField]
		private Variable _formatVar;

		public GUIText target => _targetVar.GetValueAsComponent(_target, typeof(GUIText)) as GUIText;

		public string format => _formatVar.GetValue(_format);

		protected override void OnReset()
		{
			GUIText component = GetComponent<GUIText>();
			if (component != null)
			{
				_target = component;
				_alignmentValue = component.alignment;
				_anchorValue = component.anchor;
				_colorValue = component.color;
				_enabledValue = component.enabled;
				_fontValue = component.font;
				_fontSizeValue = component.fontSize;
				_fontStyleValue = component.fontStyle;
				_lineSpacingValue = component.lineSpacing;
				_pixelOffsetValue = component.pixelOffset;
				_richTextValue = component.richText;
				_screenRectValue = component.GetScreenRect();
				_tabSizeValue = component.tabSize;
				_textValue = component.text;
			}
		}

		public override void Setup()
		{
		}

		public override void Execute()
		{
			GUIText target = this.target;
			if (target == null)
			{
				return;
			}
			switch (_property)
			{
			case Property.ScreenRect:
				break;
			case Property.Alignment:
				target.alignment = (TextAlignment)(object)_valueVar.GetValue(_alignmentValue);
				break;
			case Property.Anchor:
				target.anchor = (TextAnchor)(object)_valueVar.GetValue(_anchorValue);
				break;
			case Property.Color:
				target.color = _valueVar.GetValue(_colorValue);
				break;
			case Property.Enabled:
				target.enabled = _valueVar.GetValue(_enabledValue);
				break;
			case Property.Font:
				target.font = (_valueVar.GetValue(_fontValue) as Font);
				break;
			case Property.FontSize:
				target.fontSize = _valueVar.GetValue(_fontSizeValue);
				break;
			case Property.FontStyle:
				target.fontStyle = (FontStyle)(object)_valueVar.GetValue(_fontStyleValue);
				break;
			case Property.LineSpacing:
				target.lineSpacing = _valueVar.GetValue(_lineSpacingValue);
				break;
			case Property.PixelOffset:
				target.pixelOffset = _valueVar.GetValue(_pixelOffsetValue);
				break;
			case Property.RichText:
				target.richText = _valueVar.GetValue(_richTextValue);
				break;
			case Property.TabSize:
				target.tabSize = _valueVar.GetValue(_tabSizeValue);
				break;
			case Property.Text:
				if (_valueVar != null && _valueVar.IsNumeric())
				{
					target.text = _valueVar.GetValue(_textValue, format, base.gameObject).Expanded();
				}
				else
				{
					target.text = _valueVar.GetValue(_textValue).Expanded();
				}
				break;
			}
		}
	}
}
