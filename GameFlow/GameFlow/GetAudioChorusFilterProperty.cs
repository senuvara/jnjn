using UnityEngine;

namespace GameFlow
{
	[Help("es", "Lee una propiedad del componente @UnityManual{AudioChorusFilter} especificado.", null)]
	[Help("en", "Gets a property of the specified @UnityManual{AudioChorusFilter} component.", null)]
	[AddComponentMenu("")]
	public class GetAudioChorusFilterProperty : Action, IExecutableInEditor
	{
		public enum Property
		{
			Delay,
			Depth,
			DryMix,
			Rate,
			WetMix1,
			WetMix2,
			WetMix3
		}

		[SerializeField]
		private AudioChorusFilter _source;

		[SerializeField]
		private Variable _sourceVar;

		[SerializeField]
		private Property _property;

		[SerializeField]
		private Variable _output;

		protected override void OnReset()
		{
			_source = base.gameObject.GetComponent<AudioChorusFilter>();
		}

		public override void Setup()
		{
		}

		public override void Execute()
		{
			AudioChorusFilter audioChorusFilter = _sourceVar.GetValueAsComponent(_source, typeof(AudioChorusFilter)) as AudioChorusFilter;
			if (!(audioChorusFilter == null))
			{
				switch (_property)
				{
				case Property.Delay:
					_output.SetValue(audioChorusFilter.delay);
					break;
				case Property.Depth:
					_output.SetValue(audioChorusFilter.depth);
					break;
				case Property.DryMix:
					_output.SetValue(audioChorusFilter.dryMix);
					break;
				case Property.Rate:
					_output.SetValue(audioChorusFilter.rate);
					break;
				case Property.WetMix1:
					_output.SetValue(audioChorusFilter.wetMix1);
					break;
				case Property.WetMix2:
					_output.SetValue(audioChorusFilter.wetMix2);
					break;
				case Property.WetMix3:
					_output.SetValue(audioChorusFilter.wetMix3);
					break;
				}
			}
		}
	}
}
