using UnityEngine;

namespace GameFlow
{
	[AddComponentMenu("")]
	[Help("es", "Lee una propiedad del componente @UnityManual{MeshFilter} especificado.", null)]
	[Help("en", "Gets a property of the specified @UnityManual{MeshFilter} component.", null)]
	public class GetMeshFilterProperty : Action, IExecutableInEditor
	{
		public enum Property
		{
			Mesh,
			SharedMesh
		}

		[SerializeField]
		private MeshFilter _source;

		[SerializeField]
		private Variable _sourceVar;

		[SerializeField]
		private Property _property;

		[SerializeField]
		private Variable _output;

		protected override void OnReset()
		{
			_source = base.gameObject.GetComponent<MeshFilter>();
		}

		public override void Setup()
		{
		}

		public override void Execute()
		{
			MeshFilter meshFilter = _sourceVar.GetValueAsComponent(_source, typeof(MeshFilter)) as MeshFilter;
			if (!(meshFilter == null))
			{
				switch (_property)
				{
				case Property.Mesh:
					_output.SetValue(meshFilter.mesh);
					break;
				case Property.SharedMesh:
					_output.SetValue(meshFilter.sharedMesh);
					break;
				}
			}
		}
	}
}
