using UnityEngine;

namespace GameFlow
{
	[Help("en", "Cancels the specified web request.", null)]
	[Help("es", "Cancela la petición web especificada.", null)]
	[AddComponentMenu("")]
	public class CancelWebRequest : Action
	{
		[SerializeField]
		private WebRequest _request;

		[SerializeField]
		private Variable _requestVar;

		public override void Execute()
		{
			_request = (_requestVar.GetValue(_request) as WebRequest);
			if (_request != null)
			{
				_request.Cancel();
			}
		}
	}
}
