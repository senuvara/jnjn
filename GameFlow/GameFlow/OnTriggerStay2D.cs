using UnityEngine;

namespace GameFlow
{
	[AddComponentMenu("GameFlow/Programs/On Trigger Stay 2D")]
	[Help("en", "Program that will be executed every frame while a Collider 2D component configured as trigger in the listening range is being invaded by another component.", null)]
	[DisallowMultipleComponent]
	[Help("es", "Programa que se ejecutará cada fotograma que un componente Collider 2D configurado como disparador (trigger) en el rango de escucha esté siendo invadido por otro componente.", null)]
	public class OnTriggerStay2D : OnTriggerProgram2D
	{
		private EventController[] _controllers;

		protected override void RegisterAsListener()
		{
			_controllers = RegisterAsListener(typeof(TriggerStayEvent), typeof(TriggerController2D), TriggerController2D.AddController);
			if (base.listeningRange == EventListeningRange.SceneHierarchy)
			{
				TriggerStayEvent.AddListener(this);
			}
		}

		protected override void UnRegisterAsListener()
		{
			TriggerStayEvent.RemoveListener(this);
			UnRegisterAsListener(typeof(TriggerStayEvent), typeof(TriggerController2D));
		}

		private void OnTransformChildrenChanged()
		{
			if (base.listeningRange == EventListeningRange.GameObjectHierarchy)
			{
				UnsubscribeFrom(_controllers, typeof(TriggerStayEvent));
				_controllers = EventController.GetControllersInHierarchyOf(typeof(TriggerController2D), base.listeningTarget, TriggerController2D.AddController);
				SubscribeTo(_controllers, typeof(TriggerStayEvent));
			}
		}
	}
}
