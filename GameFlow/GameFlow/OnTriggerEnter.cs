using UnityEngine;

namespace GameFlow
{
	[DisallowMultipleComponent]
	[Help("es", "Programa que se ejecutará una vez en el momento que un componente Collider configurado como disparador (trigger) en el rango de escucha comience a ser invadido por otro componente.", null)]
	[Help("en", "Program that will be executed one time as soon as a Collider component configured as trigger in the listening range starts being invaded by another component.", null)]
	[AddComponentMenu("GameFlow/Programs/On Trigger Enter")]
	public class OnTriggerEnter : OnTriggerProgram
	{
		private EventController[] _controllers;

		protected override void RegisterAsListener()
		{
			_controllers = RegisterAsListener(typeof(TriggerEnterEvent), typeof(TriggerController), TriggerController.AddController);
			if (base.listeningRange == EventListeningRange.SceneHierarchy)
			{
				TriggerEnterEvent.AddListener(this);
			}
		}

		protected override void UnRegisterAsListener()
		{
			TriggerEnterEvent.RemoveListener(this);
			UnRegisterAsListener(typeof(TriggerEnterEvent), typeof(TriggerController));
		}

		private void OnTransformChildrenChanged()
		{
			if (base.listeningRange == EventListeningRange.GameObjectHierarchy)
			{
				UnsubscribeFrom(_controllers, typeof(TriggerEnterEvent));
				_controllers = EventController.GetControllersInHierarchyOf(typeof(TriggerController), base.listeningTarget, TriggerController.AddController);
				SubscribeTo(_controllers, typeof(TriggerEnterEvent));
			}
		}
	}
}
