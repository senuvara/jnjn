using System.Collections.Generic;
using UnityEngine;

namespace GameFlow
{
	[Help("en", "Shows all the @GameFlow{Ray} components defined in the inspected @UnityManual{GameObject}.", null)]
	[AddComponentMenu("")]
	[DisallowMultipleComponent]
	[Help("es", "Muestra todos los componentes @GameFlow{Ray} (Rayo) definidos en el @UnityManual{GameObject}.", null)]
	public class Rays : Container, IBlockContainer, IData, IRayContainer
	{
		[SerializeField]
		private List<Ray> _rays = new List<Ray>();

		public List<Ray> rays => _rays;

		public List<Ray> GetRays()
		{
			return _rays;
		}

		public virtual void RayAdded(Ray ray)
		{
		}

		public override List<Block> GetBlocks()
		{
			return new List<Block>(_rays.ToArray());
		}

		public override bool Contains(Block block)
		{
			Ray ray = block as Ray;
			if (ray != null)
			{
				return _rays.Contains(ray);
			}
			return false;
		}

		private void OnDrawGizmos()
		{
			if (HierarchyUtils.SelectionContains(base.gameObject))
			{
				return;
			}
			for (int i = 0; i < rays.Count; i++)
			{
				Ray ray = rays[i];
				if (ray != null && ray.enabledInEditor)
				{
					DrawRay(ray, ray.unselectedColor);
				}
			}
		}

		private void OnDrawGizmosSelected()
		{
			for (int i = 0; i < rays.Count; i++)
			{
				Ray ray = rays[i];
				if (ray != null && ray.enabledInEditor)
				{
					DrawRay(ray, ray.color);
				}
			}
		}

		public static void DrawRay(Ray ray, Color color)
		{
			if (!(ray == null) && color.a != 0f)
			{
				Vector3 direction = ray.direction;
				if (!(direction == Vector3.zero))
				{
					Gizmos.color = color;
					UnityEngine.Ray ray2 = ray.ToUnityRay();
					Debug.DrawRay(ray2.origin, ray2.direction.normalized * ray.length, color);
				}
			}
		}
	}
}
