using UnityEngine;

namespace GameFlow
{
	public interface IPrivatizable
	{
		bool IsPrivate();

		Object GetPrivatizer();
	}
}
