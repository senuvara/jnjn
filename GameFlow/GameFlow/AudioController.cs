using UnityEngine;
using UnityEngine.SceneManagement;

namespace GameFlow
{
	[AddComponentMenu("")]
	[Help("es", "Controla los eventos de audio.", null)]
	[Help("en", "Controls the audio events.", null)]
	[DisallowMultipleComponent]
	public class AudioController : BaseBehaviour, ITool
	{
		private static AudioController _instance;

		private bool _mute;

		private float _volume = 1f;

		private bool _fxMute;

		private bool _musicMute;

		private static AudioSource[] _fxSources;

		private static int _fxSourcesMax = 8;

		private static int _fxCursor = -1;

		private static AudioSource[] _musicSources;

		private static float[] _musicVolumes;

		private static int _musicSourcesMax = 2;

		private static int _musicCursor = -1;

		private TimerObject _timer;

		private AudioSource _mainMusic;

		private AudioListener _audioListener;

		private bool _fadingEnabled;

		private float _startVolume;

		private float _endVolume;

		public static AudioController instance
		{
			get
			{
				Init();
				return _instance;
			}
		}

		public bool mute
		{
			get
			{
				return _mute;
			}
			set
			{
				_mute = value;
				AudioListener.volume = ((!_mute) ? _volume : 0f);
			}
		}

		public float volume
		{
			get
			{
				return _volume;
			}
			set
			{
				_volume = value;
				if (!_mute)
				{
					AudioListener.volume = _volume;
				}
			}
		}

		public bool fxMute
		{
			get
			{
				return _fxMute;
			}
			set
			{
				_fxMute = value;
				SetSourcesMute(_fxSources, _fxMute);
			}
		}

		public bool musicMute
		{
			get
			{
				return _musicMute;
			}
			set
			{
				_musicMute = value;
				SetSourcesMute(_musicSources, _musicMute);
			}
		}

		private void Setup()
		{
			GetAudioListenerReference();
			_fxSources = AddAudioSources(Plugin.controlGameObject, _fxSourcesMax, fxMute);
			_musicSources = AddAudioSources(Plugin.controlGameObject, _musicSourcesMax, musicMute);
			_musicVolumes = new float[_musicSources.Length];
			CaptureMusicVolumes();
			_timer = new TimerObject(0f);
			SceneManager.sceneLoaded -= OnSceneLoaded;
			SceneManager.sceneLoaded += OnSceneLoaded;
		}

		private void GetAudioListenerReference()
		{
			if (_audioListener != null)
			{
				return;
			}
			Camera main = Camera.main;
			if (main != null)
			{
				_audioListener = main.gameObject.GetComponent<AudioListener>();
			}
			if (_audioListener == null)
			{
				AudioListener[] array = Object.FindObjectsOfType<AudioListener>();
				if (array.Length > 0)
				{
					_audioListener = array[0];
				}
			}
		}

		private void OnSceneLoaded(Scene scene, LoadSceneMode mode)
		{
			GetAudioListenerReference();
		}

		private void LateUpdate()
		{
			if (_audioListener != null)
			{
				base.gameObject.transform.position = _audioListener.gameObject.transform.position;
			}
		}

		protected override void OnReset()
		{
			HideInInspector();
		}

		[RuntimeInitializeOnLoadMethod]
		private static void Init()
		{
			if (_instance == null)
			{
				_instance = Plugin.controlGameObject.AddComponentOnlyOnce<AudioController>();
				_instance.Setup();
			}
		}

		private static AudioSource[] AddAudioSources(GameObject gameObject, int count, bool mute)
		{
			AudioSource[] array = new AudioSource[count];
			for (int i = 0; i < count; i++)
			{
				AudioSource audioSource = array[i] = gameObject.AddComponent<AudioSource>();
				if (audioSource != null)
				{
					audioSource.volume = 1f;
					audioSource.minDistance = 10f;
					ReflectionUtils.SetPropertyValue(typeof(AudioSource), audioSource, (Unity.versionMajor >= 5) ? "spatialBlend" : "panLevel", 0f);
					audioSource.mute = mute;
				}
			}
			return array;
		}

		private static void CaptureMusicVolumes()
		{
			for (int i = 0; i < _musicSources.Length; i++)
			{
				AudioSource audioSource = _musicSources[i];
				_musicVolumes[i] = audioSource.volume;
			}
		}

		public void PlayFX(AudioClip audioClip, float volume = 1f, bool loop = false)
		{
			_fxCursor = ((_fxCursor + 1 != _fxSourcesMax) ? (_fxCursor + 1) : 0);
			AudioSource audioSource = _fxSources[_fxCursor];
			if (!(audioSource == null) && audioSource.enabled && audioSource.gameObject.activeSelf)
			{
				audioSource.mute = fxMute;
				audioSource.loop = loop;
				audioSource.PlayOneShot(audioClip, ClampVolume(volume));
			}
		}

		public void PlayFXAtSource(AudioSource audioSource, float volume = 1f, bool loop = false)
		{
			if (audioSource != null && audioSource.enabled && audioSource.gameObject.activeSelf)
			{
				audioSource.volume = ClampVolume(volume);
				audioSource.mute = fxMute;
				audioSource.loop = loop;
				audioSource.Play();
			}
		}

		public void PlayMusic(AudioClip audioClip, float volume = 1f, bool loop = false, float fading = 0f)
		{
			_musicCursor = ((_musicCursor + 1 != _musicSourcesMax) ? (_musicCursor + 1) : 0);
			AudioSource audioSource = _musicSources[_musicCursor];
			if (!(audioSource == null) && audioSource.enabled && audioSource.gameObject.activeSelf)
			{
				audioSource.clip = audioClip;
				audioSource.mute = musicMute;
				audioSource.loop = loop;
				if (fading > 0f)
				{
					_mainMusic = audioSource;
					_startVolume = 0f;
					_endVolume = volume;
					_mainMusic.volume = 0f;
					EnableFading(fading);
				}
				else
				{
					_mainMusic = null;
					StopMusicSources();
					audioSource.volume = ClampVolume(volume);
				}
				audioSource.Play();
			}
		}

		private void StopMusicSources()
		{
			for (int i = 0; i < _musicSources.Length; i++)
			{
				AudioSource audioSource = _musicSources[i];
				if (audioSource != null && audioSource != _mainMusic && audioSource.isPlaying)
				{
					audioSource.Stop();
				}
			}
		}

		private void EnableFading(float fadeDuration)
		{
			CaptureMusicVolumes();
			_timer = new TimerObject(fadeDuration);
			_timer.Restart();
			_fadingEnabled = true;
		}

		public void StopMusic(float fadeDuration = 0f)
		{
			_mainMusic = null;
			if (fadeDuration > 0f)
			{
				EnableFading(fadeDuration);
				return;
			}
			StopMusicSources();
			_fadingEnabled = false;
		}

		private void Update()
		{
			if (!_fadingEnabled)
			{
				return;
			}
			for (int i = 0; i < _musicSources.Length; i++)
			{
				AudioSource audioSource = _musicSources[i];
				if (!(audioSource != null))
				{
					continue;
				}
				if (audioSource == _mainMusic)
				{
					audioSource.volume = Mathf.Lerp(_startVolume, _endVolume, _timer.elapsedTimeDelta);
				}
				else if (audioSource.isPlaying)
				{
					audioSource.volume = Mathf.Lerp(_musicVolumes[i], 0f, _timer.elapsedTimeDelta);
					if (Mathf.Approximately(audioSource.volume, 0f))
					{
						audioSource.Stop();
					}
				}
			}
			if (_timer.expired)
			{
				_timer = null;
				_fadingEnabled = false;
			}
		}

		private void SetSourcesMute(AudioSource[] sources, bool mute)
		{
			foreach (AudioSource audioSource in sources)
			{
				if (audioSource != null)
				{
					audioSource.mute = mute;
				}
			}
		}

		private static float ClampVolume(float volume)
		{
			return Mathf.Clamp(volume, 0f, 1f);
		}
	}
}
