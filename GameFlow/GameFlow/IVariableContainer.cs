using System.Collections.Generic;

namespace GameFlow
{
	public interface IVariableContainer : IBlockContainer
	{
		List<Variable> GetVariables();

		void VariableAdded(Variable variable);
	}
}
