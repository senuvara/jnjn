using UnityEngine;

namespace GameFlow
{
	[Help("en", "Gets a property of the specified @UnityManual{Transform} component.", null)]
	[Help("es", "Lee una propiedad del componente @UnityManual{Transform} especificado.", null)]
	[AddComponentMenu("")]
	public class GetTransformProperty : Action, IExecutableInEditor
	{
		public enum Property
		{
			LocalPositionX,
			LocalPositionY,
			LocalPositionZ,
			Sep1,
			WorldPositionX,
			WorldPositionY,
			WorldPositionZ,
			Sep2,
			LocalRotationX,
			LocalRotationY,
			LocalRotationZ,
			Sep3,
			WorldRotationX,
			WorldRotationY,
			WorldRotationZ,
			Sep4,
			LocalScaleX,
			LocalScaleY,
			LocalScaleZ,
			Sep5,
			WorldScaleX,
			WorldScaleY,
			WorldScaleZ,
			LocalPosition,
			WorldPosition,
			LocalRotation,
			WorldRotation,
			LocalScale,
			WorldScale
		}

		[SerializeField]
		private Transform _source;

		[SerializeField]
		private Variable _sourceVar;

		[SerializeField]
		private Property _property;

		[SerializeField]
		private Variable _output;

		public override void Execute()
		{
			if (_output == null)
			{
				return;
			}
			Transform transform = _source;
			if (transform == null)
			{
				transform = (_sourceVar.GetValueAsComponent(_source, typeof(Transform)) as Transform);
				if (transform == null)
				{
					return;
				}
			}
			switch (_property)
			{
			case Property.Sep1:
			case Property.Sep2:
			case Property.Sep3:
			case Property.Sep4:
			case Property.Sep5:
				break;
			case Property.LocalPosition:
				_output.SetValue(transform.localPosition);
				break;
			case Property.LocalPositionX:
			{
				Variable output18 = _output;
				Vector3 localPosition3 = transform.localPosition;
				output18.SetValue(localPosition3.x);
				break;
			}
			case Property.LocalPositionY:
			{
				Variable output17 = _output;
				Vector3 localPosition2 = transform.localPosition;
				output17.SetValue(localPosition2.y);
				break;
			}
			case Property.LocalPositionZ:
			{
				Variable output16 = _output;
				Vector3 localPosition = transform.localPosition;
				output16.SetValue(localPosition.z);
				break;
			}
			case Property.WorldPosition:
				_output.SetValue(transform.position);
				break;
			case Property.WorldPositionX:
			{
				Variable output15 = _output;
				Vector3 position3 = transform.position;
				output15.SetValue(position3.x);
				break;
			}
			case Property.WorldPositionY:
			{
				Variable output14 = _output;
				Vector3 position2 = transform.position;
				output14.SetValue(position2.y);
				break;
			}
			case Property.WorldPositionZ:
			{
				Variable output13 = _output;
				Vector3 position = transform.position;
				output13.SetValue(position.z);
				break;
			}
			case Property.LocalRotation:
				_output.SetValue(transform.localEulerAngles);
				break;
			case Property.LocalRotationX:
			{
				Variable output12 = _output;
				Vector3 localEulerAngles3 = transform.localEulerAngles;
				output12.SetValue(localEulerAngles3.x);
				break;
			}
			case Property.LocalRotationY:
			{
				Variable output11 = _output;
				Vector3 localEulerAngles2 = transform.localEulerAngles;
				output11.SetValue(localEulerAngles2.y);
				break;
			}
			case Property.LocalRotationZ:
			{
				Variable output10 = _output;
				Vector3 localEulerAngles = transform.localEulerAngles;
				output10.SetValue(localEulerAngles.z);
				break;
			}
			case Property.WorldRotation:
				_output.SetValue(transform.eulerAngles);
				break;
			case Property.WorldRotationX:
			{
				Variable output9 = _output;
				Vector3 eulerAngles3 = transform.eulerAngles;
				output9.SetValue(eulerAngles3.x);
				break;
			}
			case Property.WorldRotationY:
			{
				Variable output8 = _output;
				Vector3 eulerAngles2 = transform.eulerAngles;
				output8.SetValue(eulerAngles2.y);
				break;
			}
			case Property.WorldRotationZ:
			{
				Variable output7 = _output;
				Vector3 eulerAngles = transform.eulerAngles;
				output7.SetValue(eulerAngles.z);
				break;
			}
			case Property.LocalScale:
				_output.SetValue(transform.localScale);
				break;
			case Property.LocalScaleX:
			{
				Variable output6 = _output;
				Vector3 localScale3 = transform.localScale;
				output6.SetValue(localScale3.x);
				break;
			}
			case Property.LocalScaleY:
			{
				Variable output5 = _output;
				Vector3 localScale2 = transform.localScale;
				output5.SetValue(localScale2.y);
				break;
			}
			case Property.LocalScaleZ:
			{
				Variable output4 = _output;
				Vector3 localScale = transform.localScale;
				output4.SetValue(localScale.z);
				break;
			}
			case Property.WorldScale:
				_output.SetValue(transform.lossyScale);
				break;
			case Property.WorldScaleX:
			{
				Variable output3 = _output;
				Vector3 lossyScale3 = transform.lossyScale;
				output3.SetValue(lossyScale3.x);
				break;
			}
			case Property.WorldScaleY:
			{
				Variable output2 = _output;
				Vector3 lossyScale2 = transform.lossyScale;
				output2.SetValue(lossyScale2.y);
				break;
			}
			case Property.WorldScaleZ:
			{
				Variable output = _output;
				Vector3 lossyScale = transform.lossyScale;
				output.SetValue(lossyScale.z);
				break;
			}
			}
		}
	}
}
