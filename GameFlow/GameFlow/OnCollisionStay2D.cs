using UnityEngine;

namespace GameFlow
{
	[AddComponentMenu("GameFlow/Programs/On Collision Stay 2D")]
	[Help("en", "Program that will be executed every frame while a Collider 2D component in the listening range is colliding with another component.", null)]
	[Help("es", "Programa que se ejecutará en cada fotograma mientras un Collider 2D en el rango de escucha este colisionando con otro componente.", null)]
	[DisallowMultipleComponent]
	public class OnCollisionStay2D : OnCollisionProgram2D
	{
		private EventController[] _controllers;

		protected override void RegisterAsListener()
		{
			_controllers = RegisterAsListener(typeof(CollisionStayEvent), typeof(CollisionController2D), CollisionController2D.AddController);
			if (base.listeningRange == EventListeningRange.SceneHierarchy)
			{
				CollisionStayEvent.AddListener(this);
			}
		}

		protected override void UnRegisterAsListener()
		{
			CollisionStayEvent.RemoveListener(this);
			UnRegisterAsListener(typeof(CollisionStayEvent), typeof(CollisionController2D));
		}

		private void OnTransformChildrenChanged()
		{
			if (base.listeningRange == EventListeningRange.GameObjectHierarchy)
			{
				UnsubscribeFrom(_controllers, typeof(CollisionStayEvent));
				_controllers = EventController.GetControllersInHierarchyOf(typeof(CollisionController2D), base.listeningTarget, CollisionController2D.AddController);
				SubscribeTo(_controllers, typeof(CollisionStayEvent));
			}
		}
	}
}
