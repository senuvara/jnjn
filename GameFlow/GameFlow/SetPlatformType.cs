using UnityEngine;

namespace GameFlow
{
	[Help("es", "Establece el tipo de plataforma de ejecución.", null)]
	[Help("en", "Sets the runtime platform type.", null)]
	[AddComponentMenu("")]
	public class SetPlatformType : Action
	{
		[SerializeField]
		private PlatformType _type;

		[SerializeField]
		private Variable _typeVar;

		public override void Execute()
		{
			Platform.type = (PlatformType)(object)_typeVar.GetValue(_type);
		}
	}
}
