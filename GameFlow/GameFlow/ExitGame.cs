using UnityEngine;

namespace GameFlow
{
	[Help("es", "Cierra la aplicacion.", null)]
	[AddComponentMenu("")]
	[Help("en", "Quits the application.", null)]
	public class ExitGame : Action
	{
		public static IActionProxy proxy;

		public override void Execute()
		{
			if (Application.isPlaying)
			{
				if (proxy != null)
				{
					proxy.Execute(this);
				}
				else
				{
					Application.Quit();
				}
			}
		}
	}
}
