using System.Collections.Generic;
using UnityEngine;

namespace GameFlow
{
	[Help("es", "Ejecuta las acciones contenidas si el estado @GameFlow{State} es el actual en la máquina de estados (@GameFlow{StateMachine}) a la que pertenece.", null)]
	[AddComponentMenu("")]
	[Help("en", "Executes the contained actions if the current @GameFlow{State} is the current in the @GameFlow{StateMachine} it belongs to.", null)]
	public class OnState : Action, IActionContainer, IBlockContainer, IExecutableInEditor
	{
		[SerializeField]
		private State _state;

		[SerializeField]
		private Variable _stateVar;

		[SerializeField]
		private List<Action> _actions = new List<Action>();

		private ActionSequence _sequence;

		private bool _dirty;

		private bool _finished;

		public State state => _stateVar.GetValue(_state) as State;

		public override void Setup()
		{
			_sequence = new ActionSequence(_actions);
		}

		public override void FirstStep()
		{
			if (Evaluate(defaultResult: false))
			{
				_sequence.FirstEnabledAction();
				_finished = _sequence.Finished();
				if (!_finished)
				{
					Step();
				}
			}
			else
			{
				_finished = true;
			}
		}

		protected virtual bool Evaluate(bool defaultResult)
		{
			State state = this.state;
			if (state == null)
			{
				return defaultResult;
			}
			StateMachine stateMachine = state.container as StateMachine;
			if (stateMachine == null)
			{
				return defaultResult;
			}
			return stateMachine.currentState == state;
		}

		public override void Step()
		{
			if (_sequence.Finished())
			{
				_finished = true;
			}
			if (_finished)
			{
				return;
			}
			Action currentAction = _sequence.GetCurrentAction();
			if (currentAction != null && currentAction.enabledInEditor)
			{
				_sequence.ExecuteCurrentAction();
			}
			else
			{
				_sequence.NextEnabledAction();
				currentAction = _sequence.GetCurrentAction();
				if (currentAction != null)
				{
					_sequence.ExecuteCurrentAction();
				}
			}
			if (currentAction != null && ActionExecutor.DoesActionYield(currentAction))
			{
				ActionExecutor.Yield(currentAction);
				CheckFinished();
				_sequence.NextEnabledAction();
				return;
			}
			while (!_finished && currentAction != null && ActionExecutor.IsActionFinished(currentAction))
			{
				_sequence.NextEnabledAction();
				currentAction = _sequence.GetCurrentAction();
				if (currentAction != null && currentAction.enabledInEditor)
				{
					_sequence.ExecuteCurrentAction();
					if (ActionExecutor.DoesActionYield(currentAction))
					{
						ActionExecutor.Yield(currentAction);
						CheckFinished();
						_sequence.NextEnabledAction();
						return;
					}
				}
			}
			CheckFinished();
		}

		private void CheckFinished()
		{
			if (_sequence.Finished() && !_finished)
			{
				_finished = true;
			}
		}

		public override bool Finished()
		{
			return _finished;
		}

		public virtual List<Action> GetActions(int sectionIndex = 0)
		{
			return _actions;
		}

		public int GetActionCount(int sectionIndex = 0)
		{
			return ActionUtils.GetActionCount(this);
		}

		public virtual int GetActionSectionsCount()
		{
			return 1;
		}

		public virtual void ActionAdded(Action action, int sectionIndex = 0)
		{
		}

		public virtual void Break()
		{
		}

		public void SetDirty(bool dirty)
		{
			_dirty = dirty;
		}

		public bool IsDirty()
		{
			return _dirty;
		}

		public virtual List<Block> GetBlocks()
		{
			return new List<Block>(_actions.ToArray());
		}

		public virtual bool Contains(Block block)
		{
			if (block as Action != null)
			{
				return _actions.Contains(block as Action);
			}
			return false;
		}
	}
}
