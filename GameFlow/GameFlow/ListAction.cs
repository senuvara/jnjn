using UnityEngine;

namespace GameFlow
{
	public abstract class ListAction : Action, IExecutableInEditor
	{
		[SerializeField]
		private List _list;

		[SerializeField]
		private Variable _listVar;

		[SerializeField]
		private int _index;

		[SerializeField]
		private Variable _indexVar;

		[SerializeField]
		private string _stringValue;

		[SerializeField]
		private int _intValue;

		[SerializeField]
		private float _floatValue;

		[SerializeField]
		private bool _boolValue;

		[SerializeField]
		private Vector2 _vector2Value;

		[SerializeField]
		private Vector3 _vector3Value;

		[SerializeField]
		private Vector4 _vector4Value;

		[SerializeField]
		private Rect _rectValue;

		[SerializeField]
		private Color _colorValue;

		[SerializeField]
		private Object _objectValue;

		[SerializeField]
		private int _enumIntValue;

		[SerializeField]
		private AnimationCurve _animationCurveValue;

		[SerializeField]
		private Bounds _boundsValue;

		[SerializeField]
		private Quaternion _quaternionValue;

		[SerializeField]
		private Variable _valueVar;

		public List list => _listVar.GetValue(_list) as List;

		public int index => _indexVar.GetValue(_index);

		public string stringValue => _valueVar.GetValue(_stringValue);

		public int intValue => _valueVar.GetValue(_intValue);

		public float floatValue => _valueVar.GetValue(_floatValue);

		public bool boolValue => _valueVar.GetValue(_boolValue);

		public Vector2 vector2Value => _valueVar.GetValue(_vector2Value);

		public Vector3 vector3Value => _valueVar.GetValue(_vector3Value);

		public Vector4 vector4Value => _valueVar.GetValue(_vector4Value);

		public Rect rectValue => _valueVar.GetValue(_rectValue);

		public Color colorValue => _valueVar.GetValue(_colorValue);

		public Object objectValue => _valueVar.GetValue(_objectValue);

		public int enumIntValue => _valueVar.GetValue(_enumIntValue);

		public AnimationCurve animationCurveValue => _valueVar.GetValue(_animationCurveValue);

		public Bounds boundsValue => _valueVar.GetValue(_boundsValue);

		public Quaternion quaternionValue => _valueVar.GetValue(_quaternionValue);

		public Variable valueVar => _valueVar;

		protected override void OnReset()
		{
			_list = base.gameObject.GetComponent<List>();
		}

		protected List GetList()
		{
			return list;
		}

		protected int GetIndex()
		{
			return index;
		}
	}
}
