using System.Collections.Generic;

namespace GameFlow
{
	public interface IForceContainer : IBlockContainer
	{
		List<Force> GetForces();

		void ForceAdded(Force force);
	}
}
