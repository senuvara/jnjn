using UnityEngine;

namespace GameFlow
{
	[AddComponentMenu("")]
	[Help("en", "Gets a property of the specified @UnityManual{LensFlare} component.", null)]
	[Help("es", "Lee una propiedad del componente @UnityManual{LensFlare} especificado.", null)]
	public class GetLensFlareProperty : Action, IExecutableInEditor
	{
		public enum Property
		{
			Brightness,
			Color,
			FadeSpeed,
			Flare
		}

		[SerializeField]
		private LensFlare _source;

		[SerializeField]
		private Variable _sourceVar;

		[SerializeField]
		private Property _property;

		[SerializeField]
		private Variable _output;

		protected override void OnReset()
		{
			_source = base.gameObject.GetComponent<LensFlare>();
		}

		public override void Setup()
		{
		}

		public override void Execute()
		{
			LensFlare lensFlare = _sourceVar.GetValueAsComponent(_source, typeof(LensFlare)) as LensFlare;
			if (!(lensFlare == null))
			{
				switch (_property)
				{
				case Property.Brightness:
					_output.SetValue(lensFlare.brightness);
					break;
				case Property.Color:
					_output.SetValue(lensFlare.color);
					break;
				case Property.FadeSpeed:
					_output.SetValue(lensFlare.fadeSpeed);
					break;
				case Property.Flare:
					_output.SetValue(lensFlare.flare);
					break;
				}
			}
		}
	}
}
