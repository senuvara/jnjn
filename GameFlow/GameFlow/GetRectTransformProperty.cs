using UnityEngine;

namespace GameFlow
{
	[AddComponentMenu("")]
	[Help("es", "Lee una propiedad del componente Rect Transform especificado.", null)]
	[Help("en", "Gets a property of the specified Rect Transform component.", null)]
	public class GetRectTransformProperty : Action, IExecutableInEditor
	{
		public enum Property
		{
			AnchoredPosition,
			AnchoredPosition3D,
			AnchorMax,
			AnchorMin,
			ChildCount,
			Forward,
			HasChanged,
			LocalPosition,
			LocalRotation,
			LocalScale,
			LossyScale,
			OffsetMax,
			OffsetMin,
			Parent,
			Pivot,
			Position,
			Rect,
			Right,
			Root,
			Rotation,
			SizeDelta,
			Up
		}

		[SerializeField]
		private RectTransform _source;

		[SerializeField]
		private Variable _sourceVar;

		[SerializeField]
		private Property _property;

		[SerializeField]
		private Variable _output;

		protected override void OnReset()
		{
			_source = base.gameObject.GetComponent<RectTransform>();
		}

		public override void Setup()
		{
		}

		public override void Execute()
		{
			RectTransform rectTransform = _sourceVar.GetValueAsComponent(_source, typeof(RectTransform)) as RectTransform;
			if (!(rectTransform == null))
			{
				switch (_property)
				{
				case Property.AnchoredPosition:
					_output.SetValue(rectTransform.anchoredPosition);
					break;
				case Property.AnchoredPosition3D:
					_output.SetValue(rectTransform.anchoredPosition3D);
					break;
				case Property.AnchorMax:
					_output.SetValue(rectTransform.anchorMax);
					break;
				case Property.AnchorMin:
					_output.SetValue(rectTransform.anchorMin);
					break;
				case Property.ChildCount:
					_output.SetValue(rectTransform.childCount);
					break;
				case Property.Forward:
					_output.SetValue(rectTransform.forward);
					break;
				case Property.HasChanged:
					_output.SetValue(rectTransform.hasChanged);
					break;
				case Property.LocalPosition:
					_output.SetValue(rectTransform.localPosition);
					break;
				case Property.LocalRotation:
					_output.SetValue(rectTransform.localEulerAngles);
					break;
				case Property.LocalScale:
					_output.SetValue(rectTransform.localScale);
					break;
				case Property.LossyScale:
					_output.SetValue(rectTransform.lossyScale);
					break;
				case Property.OffsetMax:
					_output.SetValue(rectTransform.offsetMax);
					break;
				case Property.OffsetMin:
					_output.SetValue(rectTransform.offsetMin);
					break;
				case Property.Parent:
					_output.SetValue(rectTransform.parent);
					break;
				case Property.Pivot:
					_output.SetValue(rectTransform.pivot);
					break;
				case Property.Position:
					_output.SetValue(rectTransform.position);
					break;
				case Property.Rect:
					_output.SetValue(rectTransform.rect);
					break;
				case Property.Right:
					_output.SetValue(rectTransform.right);
					break;
				case Property.Root:
					_output.SetValue(rectTransform.root);
					break;
				case Property.Rotation:
					_output.SetValue(rectTransform.eulerAngles);
					break;
				case Property.SizeDelta:
					_output.SetValue(rectTransform.sizeDelta);
					break;
				case Property.Up:
					_output.SetValue(rectTransform.up);
					break;
				}
			}
		}
	}
}
