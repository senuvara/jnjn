using UnityEngine;

namespace GameFlow
{
	public static class ComponentExt
	{
		public static string GetId(this Component component, string nullText = "None")
		{
			string empty = string.Empty;
			if (component == null)
			{
				return nullText;
			}
			IIdentifiable identifiable = component as IIdentifiable;
			if (identifiable != null)
			{
				return component.gameObject.name + "<" + identifiable.GetIdForControls() + ">";
			}
			return component.name;
		}
	}
}
