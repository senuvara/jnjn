using UnityEngine;

namespace GameFlow
{
	[Help("es", "Activa todos los componentes de la lista (@GameFlow{List}) especificada.", null)]
	[Help("en", "Enables all the components of the specified @GameFlow{List}.", null)]
	[AddComponentMenu("")]
	public class EnableComponentsInList : ListAction
	{
		public override void Execute()
		{
			List list = GetList();
			if (list != null && list.dataType == DataType.Object)
			{
				for (int i = 0; i < list.Count; i++)
				{
					EnableComponent.Execute(list.GetObjectAt(i) as Component);
				}
			}
		}
	}
}
