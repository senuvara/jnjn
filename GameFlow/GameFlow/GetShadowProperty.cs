using UnityEngine;
using UnityEngine.UI;

namespace GameFlow
{
	[AddComponentMenu("")]
	[Help("es", "Lee una propiedad del componente @UnityManual{Shadow} especificado.", null)]
	[Help("en", "Gets a property of the specified @UnityManual{Shadow} component.", null)]
	public class GetShadowProperty : Action, IExecutableInEditor
	{
		public enum Property
		{
			EffectColor,
			EffectDistance,
			UseGraphicAlpha
		}

		[SerializeField]
		private Shadow _source;

		[SerializeField]
		private Variable _sourceVar;

		[SerializeField]
		private Property _property;

		[SerializeField]
		private Variable _output;

		protected override void OnReset()
		{
			_source = base.gameObject.GetComponent<Shadow>();
		}

		public override void Setup()
		{
		}

		public override void Execute()
		{
			Shadow shadow = _sourceVar.GetValueAsComponent(_source, typeof(Shadow)) as Shadow;
			if (!(shadow == null))
			{
				switch (_property)
				{
				case Property.EffectColor:
					_output.SetValue(shadow.effectColor);
					break;
				case Property.EffectDistance:
					_output.SetValue(shadow.effectDistance);
					break;
				case Property.UseGraphicAlpha:
					_output.SetValue(shadow.useGraphicAlpha);
					break;
				}
			}
		}
	}
}
