using UnityEngine;

namespace GameFlow
{
	[Help("en", "Rotates the target @UnityManual{Transform} around the specified pivot.", null)]
	[Help("es", "Rotat el @UnityManual{Transform} especificado alrededor de un pivote.", null)]
	[AddComponentMenu("")]
	public class Orbit : TimeAction, IExecutableInEditor
	{
		[SerializeField]
		private Transform _target;

		[SerializeField]
		private Variable _targetVar;

		[SerializeField]
		private Transform _pivot;

		[SerializeField]
		private Variable _pivotVar;

		[SerializeField]
		private Axis _axis = Axis.Y;

		[SerializeField]
		private Variable _axisVar;

		[SerializeField]
		private float _angle = 360f;

		[SerializeField]
		private Variable _angleVar;

		[SerializeField]
		private float _multiplier = 1f;

		[SerializeField]
		private Variable _multiplierVar;

		private bool _finished;

		private Transform _targetTransform;

		private Transform _pivotTransform;

		private float _toAngle;

		private Vector3 _axisDirection;

		private float _lastAngle;

		public Transform target => _targetVar.GetValueAsComponent(_target, typeof(Transform)) as Transform;

		public Transform pivot => _pivotVar.GetValueAsComponent(_pivot, typeof(Transform)) as Transform;

		public Axis axis => (Axis)(object)_axisVar.GetValue(_axis);

		public Vector3 axisDirection => TransformUtils.GetAxisDirection(axis);

		public float angle => _angleVar.GetValue(_angle);

		public float multiplier => _multiplierVar.GetValue(_multiplier);

		protected override void OnReset()
		{
			base.OnReset();
			_target = base.gameObject.GetComponent<Transform>();
		}

		public override void FirstStep()
		{
			_finished = false;
			_targetTransform = target;
			_pivotTransform = pivot;
			if (_targetTransform == null || _pivotTransform == null)
			{
				_finished = true;
			}
			else if (base.duration == 0f || !Application.isPlaying)
			{
				_targetTransform.RotateAround(_pivotTransform.position, axisDirection, angle * multiplier);
				_finished = true;
			}
			else
			{
				_toAngle = angle * multiplier;
				_axisDirection = axisDirection;
				base.FirstStep();
			}
		}

		public override void Step()
		{
			float elapsedTimeDelta = base.timer.elapsedTimeDelta;
			if (_targetTransform == null || _pivotTransform == null)
			{
				_finished = true;
				return;
			}
			float num = _toAngle * elapsedTimeDelta;
			float angle = num - _lastAngle;
			_targetTransform.RotateAround(_pivotTransform.position, _axisDirection, angle);
			_lastAngle = num;
		}

		public override bool Finished()
		{
			return _finished || base.Finished();
		}
	}
}
