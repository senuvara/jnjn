using UnityEngine;

namespace GameFlow
{
	[Help("en", "Gets a random number in the specified numeric range.", null)]
	[AddComponentMenu("")]
	[Help("es", "Devuelve un número aleatorio en el rango numérico especificado.", null)]
	public class GetRandomNumber : Action, IExecutableInEditor
	{
		public enum OutputType
		{
			Float,
			Integer
		}

		[SerializeField]
		private OutputType _outputType;

		[SerializeField]
		private float _min;

		[SerializeField]
		private Variable _minVar;

		[SerializeField]
		private float _max = 1f;

		[SerializeField]
		private Variable _maxVar;

		[SerializeField]
		private Variable _output;

		public override void Execute()
		{
			if (_output != null)
			{
				float value = _minVar.GetValue(_min);
				float value2 = _maxVar.GetValue(_max);
				switch (_outputType)
				{
				case OutputType.Float:
					_output.SetValue(Random.Range(value, value2));
					break;
				case OutputType.Integer:
					_output.SetValue(Random.Range((int)value, (int)value2 + 1));
					break;
				}
			}
		}
	}
}
