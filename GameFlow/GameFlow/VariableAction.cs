using UnityEngine;

namespace GameFlow
{
	public abstract class VariableAction : ValueAction, IExecutableInEditor
	{
		[SerializeField]
		private Variable _variable;

		public Variable variable => _variable;
	}
}
