using UnityEngine;

namespace GameFlow
{
	[AddComponentMenu("")]
	[Help("en", "Prints a message in the Console window.", null)]
	[Help("es", "Imprime un mensaje en la ventana de Consola.", null)]
	public class LogMessage : Action, IExecutableInEditor
	{
		public enum MessageType
		{
			Info,
			Warning,
			Error
		}

		[SerializeField]
		private MessageType _messageType;

		[SerializeField]
		private Variable _messageTypeVar;

		[SerializeField]
		private string _message = string.Empty;

		[SerializeField]
		private Variable _messageVar;

		protected MessageType messageType => (MessageType)(object)_messageTypeVar.GetValue(_messageType);

		protected string message => _messageVar.GetValue(_message).Expanded();

		private void Awake()
		{
			Object container = base.container;
			while (container as Action != null)
			{
				container = (container as Action).container;
			}
		}

		protected override void OnReset()
		{
		}

		public override void Execute()
		{
			switch (messageType)
			{
			case MessageType.Info:
				Debug.Log(message + "\n", base.gameObject);
				break;
			case MessageType.Warning:
				Debug.LogWarning(message + "\n", base.gameObject);
				break;
			case MessageType.Error:
				Debug.LogError(message + "\n", base.gameObject);
				break;
			}
		}
	}
}
