using UnityEngine;

namespace GameFlow
{
	[Help("en", "Gets a random @UnityAPI{Vector3} representing a rotation.", null)]
	[AddComponentMenu("")]
	[Help("es", "Devuelve un valor @UnityAPI{Vector3} aleatorio representando una rotación.", null)]
	public class GetRandomRotation : Action, IExecutableInEditor
	{
		[SerializeField]
		private Vector3 _min = new Vector3(-359.99f, -359.99f, -359.99f);

		[SerializeField]
		private Variable _minVar;

		[SerializeField]
		private Vector3 _max = new Vector3(360f, 360f, 360f);

		[SerializeField]
		private Variable _maxVar;

		[SerializeField]
		private Variable _output;

		public Vector3 min => _minVar.GetValue(_min);

		public Vector3 max => _maxVar.GetValue(_max);

		public override void Execute()
		{
			if (!(_output == null))
			{
				_output.SetValue(RandomUtils.GetRandomVector3(min, max));
			}
		}
	}
}
