using UnityEngine;

namespace GameFlow
{
	[Help("en", "Sets the velocity of the specified object.", null)]
	[AddComponentMenu("")]
	[Help("es", "Ajusta la velocidad del objeto especificado.", null)]
	public class SetVelocity : Action
	{
		[SerializeField]
		private Rigidbody _target;

		[SerializeField]
		private Variable _targetVar;

		[SerializeField]
		private Vector3 _velocity;

		[SerializeField]
		private Variable _velocityVar;

		protected Rigidbody target => _targetVar.GetValueAsComponent(_target, typeof(Rigidbody)) as Rigidbody;

		protected Vector3 velocity => _velocityVar.GetValue(_velocity);

		protected override void OnReset()
		{
			_target = base.gameObject.GetComponent<Rigidbody>();
		}

		public override void Execute()
		{
			Execute(target, velocity);
		}

		public static void Execute(Rigidbody rb, Vector3 velocity)
		{
			rb.velocity = velocity;
		}
	}
}
