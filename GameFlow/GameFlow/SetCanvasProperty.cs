using UnityEngine;

namespace GameFlow
{
	[Help("es", "Modifica el valor de una propiedad del componente @UnityManual{Canvas} especificado.", null)]
	[AddComponentMenu("")]
	[Help("en", "Modifies the value of a property of the specified @UnityManual{Canvas} component.", null)]
	public class SetCanvasProperty : Action, IExecutableInEditor
	{
		public enum Property
		{
			OverridePixelPerfect,
			OverrideSorting,
			PixelPerfect,
			PlaneDistance,
			ReferencePixelsPerUnit,
			RenderMode,
			ScaleFactor,
			SortingLayerID,
			SortingLayerName,
			SortingOrder,
			WorldCamera
		}

		[SerializeField]
		private Canvas _target;

		[SerializeField]
		private Variable _targetVar;

		[SerializeField]
		private Property _property;

		[SerializeField]
		private bool _overridePixelPerfectValue;

		[SerializeField]
		private bool _overrideSortingValue;

		[SerializeField]
		private bool _pixelPerfectValue;

		[SerializeField]
		private float _planeDistanceValue;

		[SerializeField]
		private float _referencePixelsPerUnitValue;

		[SerializeField]
		private RenderMode _renderModeValue;

		[SerializeField]
		private float _scaleFactorValue;

		[SerializeField]
		private int _sortingLayerIDValue;

		[SerializeField]
		private string _sortingLayerNameValue;

		[SerializeField]
		private int _sortingOrderValue;

		[SerializeField]
		private Camera _worldCameraValue;

		[SerializeField]
		private Variable _valueVar;

		public Canvas target => _targetVar.GetValueAsComponent(_target, typeof(Canvas)) as Canvas;

		protected override void OnReset()
		{
			_target = base.gameObject.GetComponent<Canvas>();
		}

		public override void Setup()
		{
		}

		public override void Execute()
		{
			Canvas target = this.target;
			if (!(target == null))
			{
				switch (_property)
				{
				case Property.OverridePixelPerfect:
					target.overridePixelPerfect = _valueVar.GetValue(_overridePixelPerfectValue);
					break;
				case Property.OverrideSorting:
					target.overrideSorting = _valueVar.GetValue(_overrideSortingValue);
					break;
				case Property.PixelPerfect:
					target.pixelPerfect = _valueVar.GetValue(_pixelPerfectValue);
					break;
				case Property.PlaneDistance:
					target.planeDistance = _valueVar.GetValue(_planeDistanceValue);
					break;
				case Property.ReferencePixelsPerUnit:
					target.referencePixelsPerUnit = _valueVar.GetValue(_referencePixelsPerUnitValue);
					break;
				case Property.RenderMode:
					target.renderMode = (RenderMode)(object)_valueVar.GetValue(_renderModeValue);
					break;
				case Property.ScaleFactor:
					target.scaleFactor = _valueVar.GetValue(_scaleFactorValue);
					break;
				case Property.SortingLayerID:
					target.sortingLayerID = _valueVar.GetValue(_sortingLayerIDValue);
					break;
				case Property.SortingLayerName:
					target.sortingLayerName = _valueVar.GetValue(_sortingLayerNameValue);
					break;
				case Property.SortingOrder:
					target.sortingOrder = _valueVar.GetValue(_sortingOrderValue);
					break;
				case Property.WorldCamera:
					target.worldCamera = (_valueVar.GetValueAsComponent(_worldCameraValue, typeof(Camera)) as Camera);
					break;
				}
			}
		}
	}
}
