using System;
using UnityEngine;

namespace GameFlow
{
	public static class GameObjectExt
	{
		public static Action AddAction(this GameObject gameObject, Type actionType)
		{
			return gameObject.AddComponent(actionType) as Action;
		}

		public static Condition AddCondition(this GameObject gameObject, Type conditionType)
		{
			return gameObject.AddComponent(conditionType) as Condition;
		}

		public static Variable AddVariable(this GameObject gameObject, DataType variableType)
		{
			Variable variable = gameObject.AddComponent(typeof(Variable)) as Variable;
			if (variable != null)
			{
				variable.dataType = variableType;
			}
			return variable;
		}

		public static void SetParent(this GameObject gameObject, GameObject parent)
		{
			if (gameObject != null && parent != null)
			{
				gameObject.GetComponent<Transform>().parent = parent.GetComponent<Transform>();
			}
		}

		public static GameObject GetParent(this GameObject gameObject)
		{
			GameObject result = null;
			if (gameObject != null)
			{
				Transform parent = gameObject.GetComponent<Transform>().parent;
				if (parent != null)
				{
					result = parent.gameObject;
				}
			}
			return result;
		}

		public static bool IsHiddenInHierarchy(this GameObject gameObject)
		{
			return ObjectUtils.IsHiddenInHierarchy(gameObject);
		}

		public static void HideComponents(this GameObject gameObject)
		{
			if (!Application.isEditor || gameObject == null || !Application.isEditor)
			{
				return;
			}
			Component[] components = gameObject.GetComponents<Component>();
			Component[] array = components;
			foreach (Component component in array)
			{
				if (component is IHidden)
				{
					ObjectUtils.HideInInspector(component);
				}
			}
		}

		public static T AddComponentOnlyOnce<T>(this GameObject gameObject) where T : Component
		{
			T val = gameObject.GetComponent<T>();
			if ((UnityEngine.Object)val == (UnityEngine.Object)null)
			{
				val = gameObject.AddComponent<T>();
			}
			return val;
		}

		public static Collider GetCollider(this GameObject gameObject, bool isTrigger)
		{
			Collider[] components = gameObject.GetComponents<Collider>();
			for (int i = 0; i < components.Length; i++)
			{
				if (components[i].isTrigger == isTrigger)
				{
					return components[i];
				}
			}
			return null;
		}

		public static Collider2D GetCollider2D(this GameObject gameObject, bool isTrigger)
		{
			Collider2D[] components = gameObject.GetComponents<Collider2D>();
			for (int i = 0; i < components.Length; i++)
			{
				if (components[i].isTrigger == isTrigger)
				{
					return components[i];
				}
			}
			return null;
		}
	}
}
