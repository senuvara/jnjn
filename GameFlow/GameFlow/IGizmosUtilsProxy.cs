using UnityEngine;

namespace GameFlow
{
	public interface IGizmosUtilsProxy
	{
		float GetHandleSize(Vector3 position);

		void SetHandlesColor(Color color);

		void DrawLine(Vector3 p1, Vector3 p2, bool dotted = false);

		void ArrowCap(int controlID, Vector3 position, Quaternion rotation, float size);

		void ConeCap(int controlID, Vector3 position, Quaternion rotation, float size);

		void SphereCap(int controlID, Vector3 position, Quaternion rotation, float size);

		void DrawSelectableAreaForPointAt(Vector3 position, float radius);

		void Draw3DCrossHairs(Vector3 position, float radius);

		void DrawLabel(Vector3 position, string text, Color color, float offsetX = 0f, float offsetY = 0f);

		void DrawBounds(Bounds bounds, Color color);

		void DrawBoundsFrame(Bounds bounds, Color color);

		Rect CalculateBoundsFrame(Bounds bounds);

		void DrawRect(Rect rect, Color color);
	}
}
