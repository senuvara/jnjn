using UnityEngine;

namespace GameFlow
{
	[Help("es", "Dibuja el marco 2D del cubo que delimita al objeto especificado.", null)]
	[AddComponentMenu("")]
	[Help("en", "Draws the 2D frame of the bounding cube of the specified Object.", null)]
	public class DrawBoundsFrame : DrawBounds
	{
	}
}
