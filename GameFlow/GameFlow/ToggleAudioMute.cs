using UnityEngine;

namespace GameFlow
{
	[AddComponentMenu("")]
	[Help("en", "Toggles audio mute on/off.", null)]
	[Help("es", "Enmudece el audio si no estaba enmudecido, o deja de enmudecerlo si ya lo estaba.", null)]
	public class ToggleAudioMute : Action
	{
		public enum Channel
		{
			Master,
			Music,
			FX
		}

		[SerializeField]
		private Channel _channel;

		public override void Execute()
		{
			switch (_channel)
			{
			case Channel.Master:
				AudioController.instance.mute = !AudioController.instance.mute;
				break;
			case Channel.Music:
				AudioController.instance.musicMute = !AudioController.instance.musicMute;
				break;
			case Channel.FX:
				AudioController.instance.fxMute = !AudioController.instance.fxMute;
				break;
			}
		}
	}
}
