namespace GameFlow
{
	public enum TimerType
	{
		Stopwatch,
		Countdown,
		Chrono
	}
}
