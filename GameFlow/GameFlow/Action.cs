using UnityEngine;

namespace GameFlow
{
	public abstract class Action : Block
	{
		private void Reset()
		{
			HideInInspector();
			Disable();
			SetContainer();
			SetResetFlag();
			OnReset();
		}

		private void SetContainer()
		{
			if (container != null || !BlockInsertion.enabled)
			{
				return;
			}
			IActionContainer actionContainer = BlockInsertion.target as IActionContainer;
			if (actionContainer != null)
			{
				int count = actionContainer.GetActions(BlockInsertion.sectionIndex).Count;
				if (BlockInsertion.index < 0 || BlockInsertion.index >= count)
				{
					actionContainer.GetActions(BlockInsertion.sectionIndex).Add(this);
				}
				else
				{
					actionContainer.GetActions(BlockInsertion.sectionIndex).Insert(BlockInsertion.index, this);
				}
				ActionAdded();
				actionContainer.ActionAdded(this);
				if (BlockInsertion.setDirty)
				{
					actionContainer.SetDirty(dirty: true);
				}
				container = (actionContainer as Object);
				BlockInsertion.Reset();
			}
		}

		public virtual void ActionAdded()
		{
		}

		private void Awake()
		{
		}

		public virtual void Setup()
		{
		}

		public virtual void FirstStep()
		{
			Step();
		}

		public virtual void Step()
		{
			Execute();
		}

		public virtual void Execute()
		{
		}

		public virtual void Finish()
		{
		}

		public virtual bool Finished()
		{
			return true;
		}

		public Object GetRootContainer()
		{
			Object container = base.container;
			while (container != null && container as Block != null)
			{
				container = (container as Block).container;
			}
			return container;
		}
	}
}
