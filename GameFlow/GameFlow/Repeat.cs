using UnityEngine;

namespace GameFlow
{
	[Help("en", "Executes the contained actions for the specified number of times.", null)]
	[Help("es", "Ejecuta las acciones contenidas el número de veces especificado.", null)]
	[AddComponentMenu("")]
	public class Repeat : While
	{
		[SerializeField]
		private int _times = 2;

		[SerializeField]
		private Variable _timesVar;

		private int _remaining;

		public int times => _timesVar.GetValue(_times);

		public override void FirstStep()
		{
			_remaining = times;
			base.FirstStep();
		}

		public override bool Evaluate(bool defaultResult)
		{
			_remaining--;
			return _remaining >= 0;
		}
	}
}
