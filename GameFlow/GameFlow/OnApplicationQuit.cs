using UnityEngine;

namespace GameFlow
{
	[DisallowMultipleComponent]
	[Help("es", "Programa que se ejecutará solo una vez antes de que la aplicación sea cerrada.", null)]
	[Help("en", "Program that will be executed only once, before the application is quit.", null)]
	[AddComponentMenu("GameFlow/Programs/On Application Quit")]
	public class OnApplicationQuit : EventProgram
	{
		protected override void OnAwake()
		{
			base.OnAwake();
			ApplicationEventController.Init();
		}

		protected override void RegisterAsListener()
		{
			ApplicationQuitEvent.AddListener(this);
		}

		protected override void UnRegisterAsListener()
		{
			ApplicationQuitEvent.RemoveListener(this);
		}
	}
}
