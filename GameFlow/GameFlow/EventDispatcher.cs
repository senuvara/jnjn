using System.Collections.Generic;
using UnityEngine;

namespace GameFlow
{
	public class EventDispatcher
	{
		public static void DispatchEventToListeners(EventObject eventObject, List<IEventListener> listeners)
		{
			int num = listeners?.Count ?? 0;
			for (int i = 0; i < num; i++)
			{
				IEventListener eventListener = listeners[i];
				if (eventListener != null && eventListener.IsListening())
				{
					eventListener.EventReceived(eventObject);
				}
			}
		}

		public static void DispatchEventToListeners(EventObject eventObject, List<Object> listeners)
		{
			int num = listeners?.Count ?? 0;
			for (int i = 0; i < num; i++)
			{
				IEventListener eventListener = listeners[i] as IEventListener;
				if (eventListener != null && eventListener.IsListening())
				{
					eventListener.EventReceived(eventObject);
				}
			}
		}
	}
}
