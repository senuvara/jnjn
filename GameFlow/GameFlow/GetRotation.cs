using UnityEngine;

namespace GameFlow
{
	[AddComponentMenu("")]
	[Help("es", "Obtiene la rotación (como ángulos Euler) del objeto especificado.", null)]
	[Help("en", "Gets the rotation (in Euler angles) of the specified object.", null)]
	public class GetRotation : Action, IExecutableInEditor
	{
		[SerializeField]
		private Transform _target;

		[SerializeField]
		private Variable _targetVar;

		[SerializeField]
		private Variable _output;

		protected override void OnReset()
		{
		}

		public override void Setup()
		{
		}

		public override void Execute()
		{
			if (!(_output == null))
			{
				Transform transform = _target;
				if (transform == null)
				{
					transform = (_targetVar.GetValueAsComponent(_target, typeof(Transform)) as Transform);
				}
				if (transform != null)
				{
					_output.SetValue(transform.rotation.eulerAngles);
				}
			}
		}
	}
}
