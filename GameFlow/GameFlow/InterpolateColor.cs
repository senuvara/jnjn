using UnityEngine;
using UnityEngine.UI;

namespace GameFlow
{
	[AddComponentMenu("")]
	[Help("es", "Interpola el color del @UnityManual{GameObject} especificado hasta que coincida con otro dado.", null)]
	[Help("en", "Interpolates the color of the specified @UnityManual{GameObject} to match another.", null)]
	public class InterpolateColor : TimeAction, IExecutableInEditor
	{
		[SerializeField]
		private GameObject _target;

		[SerializeField]
		private Variable _targetVar;

		[SerializeField]
		private Color _color = Color.clear;

		[SerializeField]
		private Variable _colorVar;

		private bool _finished;

		private Material _material;

		private Color _materialStartColor;

		private GUIText _guiText;

		private Color _guiTextStartColor;

		private GUITexture _guiTexture;

		private Color _guiTextureStartColor;

		private Text _text;

		private Color _textStartColor;

		private Image _image;

		private Color _imageStartColor;

		public GameObject target => _targetVar.GetValue(_target) as GameObject;

		public Color color => _colorVar.GetValue(_color);

		protected override void OnReset()
		{
			base.OnReset();
			_target = base.gameObject;
		}

		public override void FirstStep()
		{
			_finished = true;
			if (target == null)
			{
				return;
			}
			bool flag = base.duration == 0f || !Application.isPlaying;
			Renderer component = target.GetComponent<Renderer>();
			if (component != null)
			{
				_material = component.material;
				if (_material != null)
				{
					if (flag)
					{
						_material.color = color;
					}
					else
					{
						_materialStartColor = _material.color;
						_finished = false;
					}
				}
			}
			Text component2 = target.GetComponent<Text>();
			if (component2 != null)
			{
				if (flag)
				{
					component2.color = color;
				}
				else
				{
					_text = component2;
					_textStartColor = component2.color;
					_finished = false;
				}
			}
			Image component3 = target.GetComponent<Image>();
			if (component3 != null)
			{
				if (flag)
				{
					component3.color = color;
				}
				else
				{
					_image = component3;
					_imageStartColor = component3.color;
					_finished = false;
				}
			}
			GUIText component4 = target.GetComponent<GUIText>();
			if (component4 != null)
			{
				if (flag)
				{
					component4.color = color;
				}
				else
				{
					_guiText = component4;
					_guiTextStartColor = component4.color;
					_finished = false;
				}
			}
			GUITexture component5 = target.GetComponent<GUITexture>();
			if (component5 != null)
			{
				if (flag)
				{
					component5.color = color;
				}
				else
				{
					_guiTexture = component5;
					_guiTextureStartColor = component5.color;
					_finished = false;
				}
			}
			if (!_finished)
			{
				base.FirstStep();
			}
		}

		public override void Step()
		{
			float elapsedTimeDelta = base.timer.elapsedTimeDelta;
			if (_material != null)
			{
				_material.color = Color.Lerp(_materialStartColor, color, elapsedTimeDelta);
			}
			if (_text != null)
			{
				_text.color = Color.Lerp(_textStartColor, color, elapsedTimeDelta);
			}
			if (_image != null)
			{
				_image.color = Color.Lerp(_imageStartColor, color, elapsedTimeDelta);
			}
			if (_guiText != null)
			{
				_guiText.color = Color.Lerp(_guiTextStartColor, color, elapsedTimeDelta);
			}
			if (_guiTexture != null)
			{
				_guiTexture.color = Color.Lerp(_guiTextureStartColor, color, elapsedTimeDelta);
			}
		}

		public override bool Finished()
		{
			return _finished || base.Finished();
		}
	}
}
