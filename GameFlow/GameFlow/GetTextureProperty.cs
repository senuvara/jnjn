using UnityEngine;

namespace GameFlow
{
	[Help("en", "Gets a property of the specified Texture.", null)]
	[Help("es", "Lee una propiedad de la Textura especificada.", null)]
	[AddComponentMenu("")]
	public class GetTextureProperty : Action, IExecutableInEditor
	{
		public enum TextureProperty
		{
			AnisoLevel,
			FilterMode,
			Height,
			MipMapBias,
			Width,
			WrapMode
		}

		public enum Texture2DProperty
		{
			AnisoLevel,
			FilterMode,
			Height,
			MipMapBias,
			Width,
			WrapMode,
			Format,
			MipMapCount
		}

		public enum Texture3DProperty
		{
			AnisoLevel,
			FilterMode,
			Height,
			MipMapBias,
			Width,
			WrapMode,
			Depth,
			Format
		}

		public enum RenderTextureProperty
		{
			AnisoLevel,
			FilterMode,
			Height,
			MipMapBias,
			Width,
			WrapMode,
			Antialiasing,
			Depth,
			EnableRandomWrite,
			Format,
			GenerateMips,
			SRGB,
			UseMipMap,
			VolumeDepth
		}

		[SerializeField]
		private Texture _source;

		[SerializeField]
		private Variable _sourceVar;

		[SerializeField]
		private TextureProperty _textureProperty;

		[SerializeField]
		private Texture2DProperty _texture2dProperty;

		[SerializeField]
		private Texture3DProperty _texture3dProperty;

		[SerializeField]
		private RenderTextureProperty _renderProperty;

		[SerializeField]
		private Variable _output;

		public override void Execute()
		{
			Texture texture = _source;
			if (texture == null)
			{
				texture = (_sourceVar.GetValue(_source) as Texture);
			}
			if (texture == null)
			{
				return;
			}
			Texture2D texture2D = texture as Texture2D;
			if (texture2D != null)
			{
				switch (_texture2dProperty)
				{
				case Texture2DProperty.AnisoLevel:
					_output.SetValue(texture.anisoLevel);
					break;
				case Texture2DProperty.FilterMode:
					_output.SetValue(texture.filterMode);
					break;
				case Texture2DProperty.Height:
					_output.SetValue(texture.height);
					break;
				case Texture2DProperty.MipMapBias:
					_output.SetValue(texture.mipMapBias);
					break;
				case Texture2DProperty.Width:
					_output.SetValue(texture.width);
					break;
				case Texture2DProperty.WrapMode:
					_output.SetValue(texture.wrapMode);
					break;
				case Texture2DProperty.Format:
					_output.SetValue(texture2D.format);
					break;
				case Texture2DProperty.MipMapCount:
					_output.SetValue(texture2D.mipmapCount);
					break;
				}
				return;
			}
			Texture3D texture3D = texture as Texture3D;
			if (texture3D != null)
			{
				switch (_texture3dProperty)
				{
				case Texture3DProperty.AnisoLevel:
					_output.SetValue(texture.anisoLevel);
					break;
				case Texture3DProperty.FilterMode:
					_output.SetValue(texture.filterMode);
					break;
				case Texture3DProperty.Height:
					_output.SetValue(texture.height);
					break;
				case Texture3DProperty.MipMapBias:
					_output.SetValue(texture.mipMapBias);
					break;
				case Texture3DProperty.Width:
					_output.SetValue(texture.width);
					break;
				case Texture3DProperty.WrapMode:
					_output.SetValue(texture.wrapMode);
					break;
				case Texture3DProperty.Depth:
					_output.SetValue(texture3D.depth);
					break;
				case Texture3DProperty.Format:
					_output.SetValue(texture3D.format);
					break;
				}
				return;
			}
			RenderTexture renderTexture = texture as RenderTexture;
			if (renderTexture != null)
			{
				switch (_renderProperty)
				{
				case RenderTextureProperty.AnisoLevel:
					_output.SetValue(texture.anisoLevel);
					break;
				case RenderTextureProperty.FilterMode:
					_output.SetValue(texture.filterMode);
					break;
				case RenderTextureProperty.Height:
					_output.SetValue(texture.height);
					break;
				case RenderTextureProperty.MipMapBias:
					_output.SetValue(texture.mipMapBias);
					break;
				case RenderTextureProperty.Width:
					_output.SetValue(texture.width);
					break;
				case RenderTextureProperty.WrapMode:
					_output.SetValue(texture.wrapMode);
					break;
				case RenderTextureProperty.Antialiasing:
					_output.SetValue(renderTexture.antiAliasing);
					break;
				case RenderTextureProperty.Depth:
					_output.SetValue(renderTexture.depth);
					break;
				case RenderTextureProperty.EnableRandomWrite:
					_output.SetValue(renderTexture.enableRandomWrite);
					break;
				case RenderTextureProperty.Format:
					_output.SetValue(renderTexture.format);
					break;
				case RenderTextureProperty.GenerateMips:
					_output.SetValue(renderTexture.autoGenerateMips);
					break;
				case RenderTextureProperty.SRGB:
					_output.SetValue(renderTexture.sRGB);
					break;
				case RenderTextureProperty.UseMipMap:
					_output.SetValue(renderTexture.useMipMap);
					break;
				case RenderTextureProperty.VolumeDepth:
					_output.SetValue(renderTexture.volumeDepth);
					break;
				}
			}
		}
	}
}
