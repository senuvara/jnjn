using UnityEngine;

namespace GameFlow
{
	[Help("en", "Gets a property of the specified @UnityManual{Collider2D} component.", null)]
	[Help("es", "Lee una propiedad del componente @UnityManual{Collider2D} especificado.", null)]
	[AddComponentMenu("")]
	public class GetCollider2DProperty : Action, IExecutableInEditor
	{
		public enum BaseProperty
		{
			AttachedRigidbody,
			Bounds,
			Density,
			Enabled,
			IsTrigger,
			Offset,
			ShapeCount,
			SharedMaterial,
			UsedByEffector
		}

		public enum BoxProperty
		{
			AttachedRigidbody,
			Bounds,
			Density,
			Enabled,
			IsTrigger,
			Offset,
			ShapeCount,
			SharedMaterial,
			UsedByEffector,
			Size
		}

		public enum CircleProperty
		{
			AttachedRigidbody,
			Bounds,
			Density,
			Enabled,
			IsTrigger,
			Offset,
			ShapeCount,
			SharedMaterial,
			UsedByEffector,
			Radius
		}

		public enum EdgeProperty
		{
			AttachedRigidbody,
			Bounds,
			Density,
			Enabled,
			IsTrigger,
			Offset,
			ShapeCount,
			SharedMaterial,
			UsedByEffector,
			EdgeCount,
			PointCount
		}

		public enum PolygonProperty
		{
			AttachedRigidbody,
			Bounds,
			Density,
			Enabled,
			IsTrigger,
			Offset,
			ShapeCount,
			SharedMaterial,
			UsedByEffector,
			PathCount
		}

		[SerializeField]
		private Collider2D _source;

		[SerializeField]
		private Variable _sourceVar;

		[SerializeField]
		private BaseProperty _baseProperty;

		[SerializeField]
		private BoxProperty _boxProperty;

		[SerializeField]
		private CircleProperty _circleProperty;

		[SerializeField]
		private EdgeProperty _edgeProperty;

		[SerializeField]
		private PolygonProperty _polygonProperty;

		[SerializeField]
		private Variable _output;

		protected override void OnReset()
		{
			_source = base.gameObject.GetComponent<Collider2D>();
		}

		public override void Setup()
		{
		}

		public override void Execute()
		{
			Collider2D collider2D = _source;
			if (collider2D == null)
			{
				collider2D = (_sourceVar.GetValueAsComponent(_source, typeof(Collider2D)) as Collider2D);
			}
			if (collider2D == null)
			{
				return;
			}
			BoxCollider2D boxCollider2D = collider2D as BoxCollider2D;
			if (boxCollider2D != null)
			{
				switch (_boxProperty)
				{
				case BoxProperty.Density:
				case BoxProperty.UsedByEffector:
					break;
				case BoxProperty.AttachedRigidbody:
					_output.SetValue(collider2D.attachedRigidbody);
					break;
				case BoxProperty.Bounds:
					_output.SetValue(collider2D.bounds);
					break;
				case BoxProperty.Enabled:
					_output.SetToggleValue(collider2D.enabled);
					break;
				case BoxProperty.IsTrigger:
					_output.SetToggleValue(collider2D.isTrigger);
					break;
				case BoxProperty.Offset:
					_output.SetValue(collider2D.offset);
					break;
				case BoxProperty.ShapeCount:
					_output.SetValue(collider2D.shapeCount);
					break;
				case BoxProperty.SharedMaterial:
					_output.SetValue(collider2D.sharedMaterial);
					break;
				case BoxProperty.Size:
					_output.SetValue(boxCollider2D.size);
					break;
				}
				return;
			}
			CircleCollider2D circleCollider2D = collider2D as CircleCollider2D;
			if (circleCollider2D != null)
			{
				switch (_circleProperty)
				{
				case CircleProperty.Density:
				case CircleProperty.UsedByEffector:
					break;
				case CircleProperty.AttachedRigidbody:
					_output.SetValue(collider2D.attachedRigidbody);
					break;
				case CircleProperty.Bounds:
					_output.SetValue(collider2D.bounds);
					break;
				case CircleProperty.Enabled:
					_output.SetToggleValue(collider2D.enabled);
					break;
				case CircleProperty.IsTrigger:
					_output.SetToggleValue(collider2D.isTrigger);
					break;
				case CircleProperty.Offset:
					_output.SetValue(collider2D.offset);
					break;
				case CircleProperty.ShapeCount:
					_output.SetValue(collider2D.shapeCount);
					break;
				case CircleProperty.SharedMaterial:
					_output.SetValue(collider2D.sharedMaterial);
					break;
				case CircleProperty.Radius:
					_output.SetValue(circleCollider2D.radius);
					break;
				}
				return;
			}
			EdgeCollider2D edgeCollider2D = collider2D as EdgeCollider2D;
			if (edgeCollider2D != null)
			{
				switch (_edgeProperty)
				{
				case EdgeProperty.Density:
				case EdgeProperty.UsedByEffector:
					break;
				case EdgeProperty.AttachedRigidbody:
					_output.SetValue(collider2D.attachedRigidbody);
					break;
				case EdgeProperty.Bounds:
					_output.SetValue(collider2D.bounds);
					break;
				case EdgeProperty.Enabled:
					_output.SetToggleValue(collider2D.enabled);
					break;
				case EdgeProperty.IsTrigger:
					_output.SetToggleValue(collider2D.isTrigger);
					break;
				case EdgeProperty.Offset:
					_output.SetValue(collider2D.offset);
					break;
				case EdgeProperty.ShapeCount:
					_output.SetValue(collider2D.shapeCount);
					break;
				case EdgeProperty.SharedMaterial:
					_output.SetValue(collider2D.sharedMaterial);
					break;
				case EdgeProperty.PointCount:
					_output.SetValue(edgeCollider2D.pointCount);
					break;
				case EdgeProperty.EdgeCount:
					_output.SetValue(edgeCollider2D.edgeCount);
					break;
				}
				return;
			}
			PolygonCollider2D polygonCollider2D = collider2D as PolygonCollider2D;
			if (polygonCollider2D != null)
			{
				switch (_polygonProperty)
				{
				case PolygonProperty.Density:
				case PolygonProperty.UsedByEffector:
					break;
				case PolygonProperty.AttachedRigidbody:
					_output.SetValue(collider2D.attachedRigidbody);
					break;
				case PolygonProperty.Bounds:
					_output.SetValue(collider2D.bounds);
					break;
				case PolygonProperty.Enabled:
					_output.SetToggleValue(collider2D.enabled);
					break;
				case PolygonProperty.IsTrigger:
					_output.SetToggleValue(collider2D.isTrigger);
					break;
				case PolygonProperty.Offset:
					_output.SetValue(collider2D.offset);
					break;
				case PolygonProperty.ShapeCount:
					_output.SetValue(collider2D.shapeCount);
					break;
				case PolygonProperty.SharedMaterial:
					_output.SetValue(collider2D.sharedMaterial);
					break;
				case PolygonProperty.PathCount:
					_output.SetValue(polygonCollider2D.pathCount);
					break;
				}
			}
		}
	}
}
