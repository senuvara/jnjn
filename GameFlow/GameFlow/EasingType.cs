namespace GameFlow
{
	public enum EasingType
	{
		Linear,
		EaseInSine,
		EaseOutSine,
		EaseInOutSine,
		EaseInQuadratic,
		EaseOutQuadratic,
		EaseInOutQuadratic,
		EaseInCubic,
		EaseOutCubic,
		EaseInOutCubic,
		EaseInQuartic,
		EaseOutQuartic,
		EaseInOutQuartic,
		EaseInQuintic,
		EaseOutQuintic,
		EaseInOutQuintic
	}
}
