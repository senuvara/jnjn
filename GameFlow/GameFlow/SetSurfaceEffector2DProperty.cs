using UnityEngine;

namespace GameFlow
{
	[AddComponentMenu("")]
	[Help("es", "Modifica una propiedad del componente @UnityManual{SurfaceEffector2D} especificado.", null)]
	[Help("en", "Sets a property of the specified @UnityManual{SurfaceEffector2D}.", null)]
	public class SetSurfaceEffector2DProperty : Action, IExecutableInEditor
	{
		public enum Property
		{
			ColliderMask,
			ForceScale,
			Speed,
			SpeedVariation,
			UseBounce,
			UseColliderMask,
			UseContactForce,
			UseFriction
		}

		[SerializeField]
		private SurfaceEffector2D _target;

		[SerializeField]
		private Variable _targetVar;

		[SerializeField]
		private Property _property;

		[SerializeField]
		private LayerMask _colliderMaskValue;

		[SerializeField]
		private float _forceScaleValue;

		[SerializeField]
		private float _speedValue;

		[SerializeField]
		private float _speedVariationValue;

		[SerializeField]
		private bool _useBounceValue;

		[SerializeField]
		private bool _useColliderMaskValue;

		[SerializeField]
		private bool _useContactForceValue;

		[SerializeField]
		private bool _useFrictionValue;

		[SerializeField]
		private Variable _valueVar;

		protected override void OnReset()
		{
			_target = base.gameObject.GetComponent<SurfaceEffector2D>();
		}

		public override void Setup()
		{
		}

		public override void Execute()
		{
			SurfaceEffector2D surfaceEffector2D = _targetVar.GetValueAsComponent(_target, typeof(SurfaceEffector2D)) as SurfaceEffector2D;
			if (!(surfaceEffector2D == null))
			{
				switch (_property)
				{
				case Property.ColliderMask:
					surfaceEffector2D.colliderMask = _valueVar.GetValue(_colliderMaskValue);
					break;
				case Property.ForceScale:
					surfaceEffector2D.forceScale = _valueVar.GetValue(_forceScaleValue);
					break;
				case Property.Speed:
					surfaceEffector2D.speed = _valueVar.GetValue(_speedValue);
					break;
				case Property.SpeedVariation:
					surfaceEffector2D.speedVariation = _valueVar.GetValue(_speedVariationValue);
					break;
				case Property.UseBounce:
					surfaceEffector2D.useBounce = _valueVar.GetValue(_useBounceValue);
					break;
				case Property.UseColliderMask:
					surfaceEffector2D.useColliderMask = _valueVar.GetValue(_useColliderMaskValue);
					break;
				case Property.UseContactForce:
					surfaceEffector2D.useContactForce = _valueVar.GetValue(_useContactForceValue);
					break;
				case Property.UseFriction:
					surfaceEffector2D.useFriction = _valueVar.GetValue(_useFrictionValue);
					break;
				}
			}
		}
	}
}
