using UnityEngine;

namespace GameFlow
{
	[Help("es", "Obtiene la respuesta a la petición web especificada.", null)]
	[AddComponentMenu("")]
	[Help("en", "Gets the response to the specified web request.", null)]
	public class GetWebRequestResponse : Action
	{
		public enum ResponseType
		{
			Audio,
			Error,
			Image,
			Movie,
			Success,
			Text
		}

		[SerializeField]
		private WebRequest _source;

		[SerializeField]
		private Variable _sourceVar;

		[SerializeField]
		private ResponseType _responseType = ResponseType.Success;

		[SerializeField]
		private Variable _output;

		public override void Execute()
		{
			if (_output == null)
			{
				return;
			}
			WebRequest webRequest = _sourceVar.GetValue(_source) as WebRequest;
			if (!(webRequest == null))
			{
				switch (_responseType)
				{
				case ResponseType.Movie:
					break;
				case ResponseType.Audio:
					_output.SetValue(webRequest.responseAsAudio);
					break;
				case ResponseType.Error:
					_output.SetValue(webRequest.error);
					break;
				case ResponseType.Image:
					_output.SetValue(webRequest.responseAsImage);
					break;
				case ResponseType.Text:
					_output.SetValue(webRequest.responseAsText);
					break;
				case ResponseType.Success:
					_output.SetValue(webRequest.successful);
					break;
				}
			}
		}
	}
}
