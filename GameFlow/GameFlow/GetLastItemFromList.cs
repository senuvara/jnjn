using UnityEngine;

namespace GameFlow
{
	[AddComponentMenu("")]
	[Help("en", "Gets the last item of the specified @GameFlow{List}.", null)]
	[Help("es", "Devuelve el último elemento de la @GameFlow{List} especificada.", null)]
	public class GetLastItemFromList : ListAction
	{
		[SerializeField]
		private Variable _output;

		public override void Execute()
		{
			List list = GetList();
			if (list != null)
			{
				GetItemFromList.ExecuteWith(list, list.Count - 1, _output);
			}
		}
	}
}
