using UnityEngine;

namespace GameFlow
{
	[AddComponentMenu("")]
	[Help("es", "Evalúa el estado del componente @UnityManual{Animator} especificado.", null)]
	[Help("en", "Evaluates the state of the specified @UnityManual{Animator} component.", null)]
	public class AnimatorStateCondition : Condition
	{
		public enum Comparison
		{
			IsEqualTo,
			IsNotEqualTo
		}

		[SerializeField]
		private Animator _animator;

		[SerializeField]
		private Variable _animatorVar;

		[SerializeField]
		private Comparison _comparison;

		[SerializeField]
		private string _state;

		[SerializeField]
		private Variable _stateVar;

		public override bool Evaluate()
		{
			Animator animator = _animatorVar.GetValue(_animator) as Animator;
			if (animator == null)
			{
				return false;
			}
			switch (_comparison)
			{
			case Comparison.IsEqualTo:
				return animator.GetCurrentAnimatorStateInfo(0).IsName(_stateVar.GetValue(_state));
			case Comparison.IsNotEqualTo:
				return !animator.GetCurrentAnimatorStateInfo(0).IsName(_stateVar.GetValue(_state));
			default:
				return false;
			}
		}
	}
}
