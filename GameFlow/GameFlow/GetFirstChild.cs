using UnityEngine;

namespace GameFlow
{
	[AddComponentMenu("")]
	[Help("es", "Obtiene el primer objeto hijo del @UnityManual{GameObject} especificado.", null)]
	[Help("en", "Gets the first child of the specified @UnityManual{GameObject}.", null)]
	public class GetFirstChild : Action, IExecutableInEditor
	{
		[SerializeField]
		private GameObject _target;

		[SerializeField]
		private Variable _targetVar;

		[SerializeField]
		private Variable _output;

		protected override void OnReset()
		{
			_target = base.gameObject;
		}

		public override void Execute()
		{
			if (!(_output == null))
			{
				GameObject gameObject = _targetVar.GetValue(_target) as GameObject;
				if (!(gameObject == null) && gameObject.transform.childCount != 0)
				{
					_output.SetValue(gameObject.transform.GetChild(0).gameObject);
				}
			}
		}
	}
}
