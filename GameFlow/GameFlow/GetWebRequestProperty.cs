using UnityEngine;

namespace GameFlow
{
	[AddComponentMenu("")]
	[Help("es", "Lee una propiedad del objeto WebRequest especificado.", null)]
	[Help("en", "Gets a property of the specified WebRequest object.", null)]
	public class GetWebRequestProperty : Action
	{
		public enum Property
		{
			ContentType,
			DownloadProgress,
			Error,
			IsCancelled,
			IsDone,
			IsInProgress,
			Method,
			ResponseLength,
			Successful,
			UploadProgress,
			URL
		}

		[SerializeField]
		private WebRequest _source;

		[SerializeField]
		private Variable _sourceVar;

		[SerializeField]
		private Property _property;

		[SerializeField]
		private Variable _output;

		public override void Execute()
		{
			if (_output == null)
			{
				return;
			}
			WebRequest webRequest = _sourceVar.GetValue(_source) as WebRequest;
			if (!(webRequest == null))
			{
				switch (_property)
				{
				case Property.ContentType:
					_output.SetValue(webRequest.contentType);
					break;
				case Property.DownloadProgress:
					_output.SetValue(webRequest.downloadProgress);
					break;
				case Property.Error:
					_output.SetValue(webRequest.error);
					break;
				case Property.IsCancelled:
					_output.SetValue(webRequest.isCancelled);
					break;
				case Property.IsDone:
					_output.SetValue(webRequest.isDone);
					break;
				case Property.IsInProgress:
					_output.SetValue(webRequest.isInProgress);
					break;
				case Property.Method:
					_output.SetValue(webRequest.method);
					break;
				case Property.ResponseLength:
					_output.SetValue(webRequest.responseLength);
					break;
				case Property.Successful:
					_output.SetValue(webRequest.successful);
					break;
				case Property.UploadProgress:
					_output.SetValue(webRequest.uploadProgress);
					break;
				case Property.URL:
					_output.SetValue(webRequest.url);
					break;
				}
			}
		}
	}
}
