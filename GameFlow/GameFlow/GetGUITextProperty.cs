using UnityEngine;

namespace GameFlow
{
	[Help("es", "Lee una propiedad del componente GUIText especificado.", null)]
	[Help("en", "Gets a property of the specified GUIText component.", null)]
	[AddComponentMenu("")]
	public class GetGUITextProperty : Action, IExecutableInEditor
	{
		public enum Property
		{
			Alignment,
			Anchor,
			Color,
			Enabled,
			Font,
			FontSize,
			FontStyle,
			InstanceID,
			LineSpacing,
			Material,
			PixelOffset,
			RichText,
			ScreenRect,
			TabSize,
			Text
		}

		[SerializeField]
		private GUIText _target;

		[SerializeField]
		private Variable _targetVar;

		[SerializeField]
		private Property _property = Property.Text;

		[SerializeField]
		private Variable _output;

		protected override void OnReset()
		{
			_target = base.gameObject.GetComponent<GUIText>();
		}

		public override void Setup()
		{
		}

		public override void Execute()
		{
			GUIText gUIText = _targetVar.GetValueAsComponent(_target, typeof(GUIText)) as GUIText;
			if (!(gUIText == null))
			{
				switch (_property)
				{
				case Property.PixelOffset:
					break;
				case Property.Alignment:
					_output.SetValue(gUIText.alignment);
					break;
				case Property.Anchor:
					_output.SetValue(gUIText.anchor);
					break;
				case Property.Color:
					_output.SetValue(gUIText.color);
					break;
				case Property.Enabled:
					_output.SetValue(gUIText.enabled);
					break;
				case Property.Font:
					_output.SetValue(gUIText.font);
					break;
				case Property.FontSize:
					_output.SetValue(gUIText.fontSize);
					break;
				case Property.FontStyle:
					_output.SetValue(gUIText.fontStyle);
					break;
				case Property.InstanceID:
					_output.SetValue(gUIText.GetInstanceID());
					break;
				case Property.LineSpacing:
					_output.SetValue(gUIText.lineSpacing);
					break;
				case Property.Material:
					_output.SetValue(gUIText.material);
					break;
				case Property.RichText:
					_output.SetValue(gUIText.richText);
					break;
				case Property.ScreenRect:
					_output.SetValue(gUIText.GetScreenRect());
					break;
				case Property.TabSize:
					_output.SetValue(gUIText.tabSize);
					break;
				case Property.Text:
					_output.SetValue(gUIText.text);
					break;
				}
			}
		}
	}
}
