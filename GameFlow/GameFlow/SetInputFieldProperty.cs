using System;
using UnityEngine;
using UnityEngine.UI;

namespace GameFlow
{
	[Help("en", "Modifies the value of a property of the specified Input Field component.", null)]
	[AddComponentMenu("")]
	[Help("es", "Modifica el valor de una propiedad del componente Input Field especificado.", null)]
	public class SetInputFieldProperty : Action, IExecutableInEditor
	{
		public enum Property
		{
			AnimationTriggers_DisabledTrigger,
			AnimationTriggers_HighlightedTrigger,
			AnimationTriggers_NormalTrigger,
			AnimationTriggers_PressedTrigger,
			AsteriskChar,
			CaretBlinkRate,
			CaretPosition,
			CharacterLimit,
			CharacterValidation,
			Colors_ColorMultiplier,
			Colors_DisabledColor,
			Colors_FadeDuration,
			Colors_HighlightedColor,
			Colors_NormalColor,
			Colors_PressedColor,
			ContentType,
			Image,
			InputType,
			Interactable,
			KeyboardType,
			LineType,
			Navigation_Mode,
			Navigation_SelectOnDown,
			Navigation_SelectOnLeft,
			Navigation_SelectOnRight,
			Navigation_SelectOnUp,
			Placeholder,
			SelectionAnchorPosition,
			SelectionColor,
			SelectionFocusPosition,
			ShouldHideMobileInput,
			SpriteState_DisabledSprite,
			SpriteState_HighlightedSprite,
			SpriteState_PressedSprite,
			TargetGraphic,
			Text,
			TextComponent,
			Transition
		}

		[SerializeField]
		private InputField _target;

		[SerializeField]
		private Variable _targetVar;

		[SerializeField]
		private Property _property;

		[SerializeField]
		private string _disabledTriggerValue;

		[SerializeField]
		private string _highlightedTriggerValue;

		[SerializeField]
		private string _normalTriggerValue;

		[SerializeField]
		private string _pressedTriggerValue;

		[SerializeField]
		private string _asteriskCharValue;

		[SerializeField]
		private float _caretBlinkRateValue;

		[SerializeField]
		private int _caretPositionValue;

		[SerializeField]
		private int _characterLimitValue;

		[SerializeField]
		private InputField.CharacterValidation _characterValidationValue;

		[SerializeField]
		private float _colorMultiplierValue;

		[SerializeField]
		private Color _disabledColorValue;

		[SerializeField]
		private float _fadeDurationValue;

		[SerializeField]
		private Color _highlightedColorValue;

		[SerializeField]
		private Color _normalColorValue;

		[SerializeField]
		private Color _pressedColorValue;

		private ColorBlock colors;

		[SerializeField]
		private InputField.ContentType _contentTypeValue;

		[SerializeField]
		private Image _imageValue;

		[SerializeField]
		private InputField.InputType _inputTypeValue;

		[SerializeField]
		private bool _interactableValue;

		[SerializeField]
		private TouchScreenKeyboardType _keyboardTypeValue;

		[SerializeField]
		private InputField.LineType _lineTypeValue;

		[SerializeField]
		private Navigation.Mode _navigationModeValue;

		[SerializeField]
		private Selectable _selectOnDownValue;

		[SerializeField]
		private Selectable _selectOnLeftValue;

		[SerializeField]
		private Selectable _selectOnRightValue;

		[SerializeField]
		private Selectable _selectOnUpValue;

		private Navigation navigation;

		[SerializeField]
		private Graphic _placeholderValue;

		[SerializeField]
		private int _selectionAnchorPositionValue;

		[SerializeField]
		private Color _selectionColorValue;

		[SerializeField]
		private int _selectionFocusPositionValue;

		[SerializeField]
		private bool _shouldHideMobileInputValue;

		[SerializeField]
		private Sprite _disabledSpriteValue;

		[SerializeField]
		private Sprite _highlightedSpriteValue;

		[SerializeField]
		private Sprite _pressedSpriteValue;

		private SpriteState spriteState;

		[SerializeField]
		private Graphic _targetGraphicValue;

		[SerializeField]
		private string _textValue;

		[SerializeField]
		private Text _textComponentValue;

		[SerializeField]
		private Selectable.Transition _transitionValue;

		[SerializeField]
		private Variable _valueVar;

		public InputField target => _targetVar.GetValueAsComponent(_target, typeof(InputField)) as InputField;

		protected override void OnReset()
		{
			_target = base.gameObject.GetComponent<InputField>();
		}

		public override void Setup()
		{
		}

		public override void Execute()
		{
			InputField target = this.target;
			if (!(target == null))
			{
				switch (_property)
				{
				case Property.AnimationTriggers_DisabledTrigger:
					target.animationTriggers.disabledTrigger = _valueVar.GetValue(_disabledTriggerValue);
					break;
				case Property.AnimationTriggers_HighlightedTrigger:
					target.animationTriggers.highlightedTrigger = _valueVar.GetValue(_highlightedTriggerValue);
					break;
				case Property.AnimationTriggers_NormalTrigger:
					target.animationTriggers.normalTrigger = _valueVar.GetValue(_normalTriggerValue);
					break;
				case Property.AnimationTriggers_PressedTrigger:
					target.animationTriggers.pressedTrigger = _valueVar.GetValue(_pressedTriggerValue);
					break;
				case Property.AsteriskChar:
				{
					string value = _valueVar.GetValue(_asteriskCharValue);
					target.asteriskChar = Convert.ToChar((value == null || value.Length <= 0) ? " " : _valueVar.GetValue(_asteriskCharValue));
					break;
				}
				case Property.CaretBlinkRate:
					target.caretBlinkRate = _valueVar.GetValue(_caretBlinkRateValue);
					break;
				case Property.CaretPosition:
					target.caretPosition = _valueVar.GetValue(_caretPositionValue);
					break;
				case Property.CharacterLimit:
					target.characterLimit = _valueVar.GetValue(_characterLimitValue);
					break;
				case Property.CharacterValidation:
					target.characterValidation = (InputField.CharacterValidation)(object)_valueVar.GetValue(_characterValidationValue);
					break;
				case Property.Colors_ColorMultiplier:
					colors = default(ColorBlock);
					colors = target.colors;
					colors.colorMultiplier = _valueVar.GetValue(_colorMultiplierValue);
					target.colors = colors;
					break;
				case Property.Colors_DisabledColor:
					colors = default(ColorBlock);
					colors = target.colors;
					colors.disabledColor = _valueVar.GetValue(_disabledColorValue);
					target.colors = colors;
					break;
				case Property.Colors_FadeDuration:
					colors = default(ColorBlock);
					colors = target.colors;
					colors.fadeDuration = _valueVar.GetValue(_fadeDurationValue);
					target.colors = colors;
					break;
				case Property.Colors_HighlightedColor:
					colors = default(ColorBlock);
					colors = target.colors;
					colors.highlightedColor = _valueVar.GetValue(_highlightedColorValue);
					target.colors = colors;
					break;
				case Property.Colors_NormalColor:
					colors = default(ColorBlock);
					colors = target.colors;
					colors.normalColor = _valueVar.GetValue(_normalColorValue);
					target.colors = colors;
					break;
				case Property.Colors_PressedColor:
					colors = default(ColorBlock);
					colors = target.colors;
					colors.pressedColor = _valueVar.GetValue(_pressedColorValue);
					target.colors = colors;
					break;
				case Property.ContentType:
					target.contentType = (InputField.ContentType)(object)_valueVar.GetValue(_contentTypeValue);
					break;
				case Property.Image:
					target.image = (_valueVar.GetValueAsComponent(_imageValue, typeof(Image)) as Image);
					break;
				case Property.InputType:
					target.inputType = (InputField.InputType)(object)_valueVar.GetValue(_inputTypeValue);
					break;
				case Property.Interactable:
					target.interactable = _valueVar.GetValue(_interactableValue);
					break;
				case Property.KeyboardType:
					target.keyboardType = (TouchScreenKeyboardType)(object)_valueVar.GetValue(_keyboardTypeValue);
					break;
				case Property.LineType:
					target.lineType = (InputField.LineType)(object)_valueVar.GetValue(_lineTypeValue);
					break;
				case Property.Navigation_Mode:
					navigation = default(Navigation);
					navigation = target.navigation;
					navigation.mode = (Navigation.Mode)(object)_valueVar.GetValue(_navigationModeValue);
					target.navigation = navigation;
					break;
				case Property.Navigation_SelectOnDown:
					navigation = default(Navigation);
					navigation = target.navigation;
					navigation.selectOnDown = (_valueVar.GetValue(_selectOnDownValue) as Selectable);
					target.navigation = navigation;
					break;
				case Property.Navigation_SelectOnLeft:
					navigation = default(Navigation);
					navigation = target.navigation;
					navigation.selectOnLeft = (_valueVar.GetValue(_selectOnLeftValue) as Selectable);
					target.navigation = navigation;
					break;
				case Property.Navigation_SelectOnRight:
					navigation = default(Navigation);
					navigation = target.navigation;
					navigation.selectOnRight = (_valueVar.GetValue(_selectOnRightValue) as Selectable);
					target.navigation = navigation;
					break;
				case Property.Navigation_SelectOnUp:
					navigation = default(Navigation);
					navigation = target.navigation;
					navigation.selectOnUp = (_valueVar.GetValue(_selectOnUpValue) as Selectable);
					target.navigation = navigation;
					break;
				case Property.Placeholder:
					target.placeholder = (_valueVar.GetValueAsComponent(_placeholderValue, typeof(Graphic)) as Graphic);
					break;
				case Property.SelectionAnchorPosition:
					target.selectionAnchorPosition = _valueVar.GetValue(_selectionAnchorPositionValue);
					break;
				case Property.SelectionColor:
					target.selectionColor = _valueVar.GetValue(_selectionColorValue);
					break;
				case Property.SelectionFocusPosition:
					target.selectionFocusPosition = _valueVar.GetValue(_selectionFocusPositionValue);
					break;
				case Property.ShouldHideMobileInput:
					target.shouldHideMobileInput = _valueVar.GetValue(_shouldHideMobileInputValue);
					break;
				case Property.SpriteState_DisabledSprite:
					spriteState = default(SpriteState);
					spriteState = target.spriteState;
					spriteState.disabledSprite = (_valueVar.GetValue(_disabledSpriteValue) as Sprite);
					target.spriteState = spriteState;
					break;
				case Property.SpriteState_HighlightedSprite:
					spriteState = default(SpriteState);
					spriteState = target.spriteState;
					spriteState.highlightedSprite = (_valueVar.GetValue(_highlightedSpriteValue) as Sprite);
					target.spriteState = spriteState;
					break;
				case Property.SpriteState_PressedSprite:
					spriteState = default(SpriteState);
					spriteState = target.spriteState;
					spriteState.pressedSprite = (_valueVar.GetValue(_pressedSpriteValue) as Sprite);
					target.spriteState = spriteState;
					break;
				case Property.TargetGraphic:
					target.targetGraphic = (_valueVar.GetValueAsComponent(_targetGraphicValue, typeof(Graphics)) as Graphic);
					break;
				case Property.Text:
					target.text = _valueVar.GetValue(_textValue);
					break;
				case Property.TextComponent:
					target.textComponent = (_valueVar.GetValueAsComponent(_textComponentValue, typeof(Text)) as Text);
					break;
				case Property.Transition:
					target.transition = (Selectable.Transition)(object)_valueVar.GetValue(_transitionValue);
					break;
				}
			}
		}
	}
}
