using UnityEngine;

namespace GameFlow
{
	[DisallowMultipleComponent]
	[Help("es", "Programa que se ejecutará cuando la aplicación gane o pierda el foco.", null)]
	[AddComponentMenu("GameFlow/Programs/On Application Focus")]
	[Help("en", "Program that will be executed when the app gets or loses focus.", null)]
	public class OnApplicationFocus : EventProgram
	{
		private Parameter _isFocusedParam;

		protected Parameter isFocusedParam
		{
			get
			{
				if (_isFocusedParam == null)
				{
					_isFocusedParam = GetParameter("IsFocused");
				}
				return _isFocusedParam;
			}
		}

		protected override void CacheParameters()
		{
			CacheParameter(isFocusedParam);
		}

		protected override void OnAwake()
		{
			base.OnAwake();
			ApplicationEventController.Init();
		}

		protected override void RegisterAsListener()
		{
			ApplicationFocusEvent.AddListener(this);
		}

		protected override void UnRegisterAsListener()
		{
			ApplicationFocusEvent.RemoveListener(this);
		}

		protected override void SetParameters(EventObject e)
		{
			ApplicationFocusEvent applicationFocusEvent = e as ApplicationFocusEvent;
			isFocusedParam.SetValue(applicationFocusEvent.isFocused);
		}
	}
}
