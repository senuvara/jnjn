using UnityEngine;

namespace GameFlow
{
	[Help("en", "Adds a force to the specified target @UnityManual{Rigidbody2D} component.", null)]
	[Help("es", "Añade una fuerza al componente @UnityManual{Rigidbody2D} especificado como objetivo.", null)]
	[AddComponentMenu("")]
	public class AddForce2D : Action
	{
		public enum ForceType
		{
			Force,
			Vector2
		}

		[SerializeField]
		private Rigidbody2D _target;

		[SerializeField]
		private Variable _targetVar;

		[SerializeField]
		private ForceMode _mode;

		[SerializeField]
		private Variable _modeVar;

		[SerializeField]
		private ForceType _type;

		[SerializeField]
		private Vector2 _directionVector2;

		[SerializeField]
		private Variable _directionVar;

		[SerializeField]
		private Force _directionForce;

		[SerializeField]
		private bool _relative;

		[SerializeField]
		private Variable _relativeVar;

		[SerializeField]
		private float _multiplier = 1f;

		[SerializeField]
		private Variable _multiplierVar;

		public ForceMode2D mode => (ForceMode2D)(object)_modeVar.GetValue(_mode);

		public bool relative => _relativeVar.GetValue(_relative);

		public float multiplier => _multiplierVar.GetValue(_multiplier);

		public override void Execute()
		{
			Rigidbody2D rigidbody2D = _targetVar.GetValueAsComponent(_target, typeof(Rigidbody2D)) as Rigidbody2D;
			if (rigidbody2D == null)
			{
				return;
			}
			switch (_type)
			{
			case ForceType.Vector2:
				Execute(rigidbody2D, _directionVar.GetValue(_directionVector2), multiplier, mode, relative);
				break;
			case ForceType.Force:
			{
				Force force = _directionVar.GetValue(_directionForce) as Force;
				if (!(force == null))
				{
					Execute(rigidbody2D, force.direction, force.magnitude * multiplier, mode, relative);
				}
				break;
			}
			}
		}

		public static void Execute(Rigidbody2D rb, Vector2 direction, float magnitude, ForceMode2D mode, bool relative)
		{
			if (relative)
			{
				rb.AddRelativeForce(direction * magnitude, mode);
			}
			else
			{
				rb.AddForce(direction * magnitude, mode);
			}
		}
	}
}
