using UnityEngine;

namespace GameFlow
{
	[Help("en", "Gets a random point in any Collider of the specified @GameFlow{List}.", null)]
	[Help("es", "Devuelve un punto aleatorio en cualquier Collider de la @GameFlow{List} especificada.", null)]
	[AddComponentMenu("")]
	public class GetRandomPointInColliderList : ListAction
	{
		[SerializeField]
		private Variable _output;

		public override void Setup()
		{
		}

		public override void Execute()
		{
			if (!(_output != null))
			{
				return;
			}
			List list = GetList();
			Collider collider = null;
			if (list != null && list.Count > 0)
			{
				int index = Random.Range(0, list.Count);
				collider = (list.GetObjectAt(index) as Collider);
			}
			BoxCollider boxCollider = collider as BoxCollider;
			if (boxCollider != null)
			{
				_output.SetValue(RandomUtils.GetRandomPointInCollider(boxCollider));
				return;
			}
			SphereCollider sphereCollider = collider as SphereCollider;
			if (sphereCollider != null)
			{
				_output.SetValue(RandomUtils.GetRandomPointInCollider(sphereCollider));
			}
		}
	}
}
