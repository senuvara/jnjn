using UnityEngine;

namespace GameFlow
{
	[Help("es", "Guarda una captura de pantalla como archivo en formato PNG.", null)]
	[AddComponentMenu("")]
	[Help("en", "Captures and saves a screenshot as PNG file.", null)]
	public class CaptureScreenshot : Action, IExecutableInEditor
	{
		[SerializeField]
		private string _path = string.Empty;

		[SerializeField]
		private Variable _pathVar;

		[SerializeField]
		private int _size = 1;

		[SerializeField]
		private Variable _sizeVar;

		protected string url => _pathVar.GetValue(_path).Expanded();

		protected int size => _sizeVar.GetValue(_size);

		public override void Execute()
		{
			Execute(url, size);
		}

		public static void Execute(string path, int size = 1)
		{
			if (path != null && path.Length != 0)
			{
				ScreenCapture.CaptureScreenshot(path, Mathf.Clamp(size, 1, 8));
			}
		}
	}
}
