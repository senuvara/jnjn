using UnityEngine;

namespace GameFlow
{
	[Help("en", "Gets a property of the specified @UnityManual{Component}.", null)]
	[AddComponentMenu("")]
	[Help("es", "Lee una propiedad del @UnityManual{Component} especificado.", null)]
	public class GetComponentProperty : Action, IExecutableInEditor
	{
		public enum Property
		{
			GameObject,
			InstanceID
		}

		[SerializeField]
		private Component _source;

		[SerializeField]
		private Variable _sourceVar;

		[SerializeField]
		private Property _property;

		[SerializeField]
		private Variable _output;

		public Component source => _sourceVar.GetValue(_source) as Component;

		public override void Execute()
		{
			if (_output == null)
			{
				return;
			}
			Component source = this.source;
			if (!(source == null))
			{
				switch (_property)
				{
				case Property.GameObject:
					_output.SetValue(source.gameObject);
					break;
				case Property.InstanceID:
					_output.SetValue(source.GetInstanceID());
					break;
				}
			}
		}
	}
}
