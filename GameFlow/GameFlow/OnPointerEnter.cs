using UnityEngine;

namespace GameFlow
{
	[Help("en", "Program that will be executed when the pointer enters the area of a seletable UI component in the listening range.", null)]
	[AddComponentMenu("GameFlow/Programs/On Pointer Enter")]
	[Help("es", "Programa que se ejecutará cuando el puntero entre en el área de un componente seleccionable UI en el rango de escucha.", null)]
	[DisallowMultipleComponent]
	public class OnPointerEnter : PointerEventProgram
	{
		private EventController[] _controllers;

		protected override void RegisterAsListener()
		{
			_controllers = RegisterAsListener(typeof(PointerEnterEvent), typeof(UIEventController), UIEventController.AddController);
			if (base.listeningRange == EventListeningRange.SceneHierarchy)
			{
				PointerEnterEvent.AddListener(this);
			}
		}

		protected override void UnRegisterAsListener()
		{
			PointerEnterEvent.RemoveListener(this);
			UnRegisterAsListener(typeof(PointerEnterEvent), typeof(UIEventController));
		}

		protected override void SetParameters(EventObject e)
		{
			PointerEnterEvent pointerEnterEvent = e as PointerEnterEvent;
			base.sourceParam.SetValue(pointerEnterEvent.source);
			base.positionParam.SetValue(pointerEnterEvent.position);
		}

		private void OnTransformChildrenChanged()
		{
			if (base.listeningRange == EventListeningRange.GameObjectHierarchy)
			{
				UnsubscribeFrom(_controllers, typeof(PointerEnterEvent));
				_controllers = EventController.GetControllersInHierarchyOf(typeof(UIEventController), base.listeningTarget, UIEventController.AddController);
				SubscribeTo(_controllers, typeof(PointerEnterEvent));
			}
		}
	}
}
