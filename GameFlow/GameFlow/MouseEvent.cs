using System.Collections.Generic;
using UnityEngine;

namespace GameFlow
{
	public class MouseEvent : EventObject
	{
		private static List<IEventListener> listeners = new List<IEventListener>();

		public GameObject source;

		public Vector2 position;

		public override void DispatchToAll()
		{
			EventDispatcher.DispatchEventToListeners(this, listeners);
		}

		public static void AddListener(IEventListener listener)
		{
			listeners.AddOnlyOnce(listener);
		}

		public static void RemoveListener(IEventListener listener)
		{
			listeners.Remove(listener);
		}
	}
}
