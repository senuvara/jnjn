using UnityEngine;

namespace GameFlow
{
	[AddComponentMenu("GameFlow/Programs/On Pointer Down")]
	[Help("en", "Program that will be executed when the pointer is down in the area of a seletable UI component in the listening range.", null)]
	[Help("es", "Programa que se ejecutará cuando el puntero esté presionado en el área de un componente seleccionable UI en el rango de escucha.", null)]
	[DisallowMultipleComponent]
	public class OnPointerDown : PointerEventProgram
	{
		private EventController[] _controllers;

		protected override void RegisterAsListener()
		{
			_controllers = RegisterAsListener(typeof(PointerDownEvent), typeof(UIEventController), UIEventController.AddController);
			if (base.listeningRange == EventListeningRange.SceneHierarchy)
			{
				PointerDownEvent.AddListener(this);
			}
		}

		protected override void UnRegisterAsListener()
		{
			PointerDownEvent.RemoveListener(this);
			UnRegisterAsListener(typeof(PointerDownEvent), typeof(UIEventController));
		}

		protected override void SetParameters(EventObject e)
		{
			PointerDownEvent pointerDownEvent = e as PointerDownEvent;
			base.sourceParam.SetValue(pointerDownEvent.source);
			base.positionParam.SetValue(pointerDownEvent.position);
		}

		private void OnTransformChildrenChanged()
		{
			if (base.listeningRange == EventListeningRange.GameObjectHierarchy)
			{
				UnsubscribeFrom(_controllers, typeof(PointerDownEvent));
				_controllers = EventController.GetControllersInHierarchyOf(typeof(UIEventController), base.listeningTarget, UIEventController.AddController);
				SubscribeTo(_controllers, typeof(PointerDownEvent));
			}
		}
	}
}
