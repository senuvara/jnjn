using UnityEngine;

namespace GameFlow
{
	[AddComponentMenu("")]
	[Help("es", "Obtiene el vector de dirección que conecta dos puntos.", null)]
	[Help("en", "Gets the direction Vector that connects two points.", null)]
	public class GetDirection : Action, IExecutableInEditor
	{
		public enum InputType
		{
			Vector3,
			Transform
		}

		[SerializeField]
		private int _inputType1;

		[SerializeField]
		private Vector3 _v1;

		[SerializeField]
		private Transform _t1;

		[SerializeField]
		private Variable _p1Var;

		[SerializeField]
		private int _inputType2;

		[SerializeField]
		private Transform _t2;

		[SerializeField]
		private Vector3 _v2;

		[SerializeField]
		private Variable _p2Var;

		[SerializeField]
		private Variable _output;

		public override void Execute()
		{
			if (_output == null)
			{
				return;
			}
			Vector3 b = Vector3.zero;
			switch (_inputType1)
			{
			case 0:
				b = _p1Var.GetValue(_v1);
				break;
			case 1:
			{
				Transform transform = _p1Var.GetValueAsComponent(_t1, typeof(Transform)) as Transform;
				if (transform == null)
				{
					return;
				}
				b = transform.position;
				break;
			}
			}
			Vector3 a = Vector3.zero;
			switch (_inputType2)
			{
			case 0:
				a = _p1Var.GetValue(_v2);
				break;
			case 1:
			{
				Transform transform2 = _p2Var.GetValueAsComponent(_t2, typeof(Transform)) as Transform;
				if (transform2 == null)
				{
					return;
				}
				a = transform2.position;
				break;
			}
			}
			Vector3 value = a - b;
			_output.SetValue(value);
		}
	}
}
