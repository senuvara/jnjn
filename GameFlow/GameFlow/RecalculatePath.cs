using UnityEngine;

namespace GameFlow
{
	[Help("es", "Recalcula la trayectoria (@GameFlow{Path}) especificada.", null)]
	[Help("en", "Recalculates the specified @GameFlow{Path}.", null)]
	[AddComponentMenu("")]
	public class RecalculatePath : Action, IExecutableInEditor
	{
		[SerializeField]
		private Path _path;

		[SerializeField]
		private Variable _pathVar;

		protected Path path => _pathVar.GetValueAsComponent(_path, typeof(Path)) as Path;

		protected override void OnReset()
		{
			_path = base.gameObject.GetComponent<Path>();
		}

		public override void Execute()
		{
			Execute(path);
		}

		public static void Execute(Path path)
		{
			if (!(path == null))
			{
				path.SetDirty();
			}
		}
	}
}
