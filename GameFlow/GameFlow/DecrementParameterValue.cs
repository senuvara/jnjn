using UnityEngine;

namespace GameFlow
{
	[Help("es", "Decrementa el valor numérico del @GameFlow{Parameter} especificado.", null)]
	[AddComponentMenu("")]
	[Help("en", "Decrements the numeric value of the specified @GameFlow{Parameter}.", null)]
	public class DecrementParameterValue : ParameterAction
	{
		protected override int GetDefaultIntValue()
		{
			return 1;
		}

		protected override float GetDefaultFloatValue()
		{
			return 1f;
		}

		public override void Execute()
		{
			Execute(base.parameter, this);
		}

		public static void Execute(Parameter parameter, ValueAction valueAction)
		{
			DecrementVariableValue.Execute(parameter, valueAction);
		}
	}
}
