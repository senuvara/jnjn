using UnityEngine;

namespace GameFlow
{
	public static class BlockUtils
	{
		public static bool IsChildOf(this Block block, Object container)
		{
			if (block == null || container == null)
			{
				return false;
			}
			Block block2 = block.container as Block;
			while (block2 != null)
			{
				if (block2 == container)
				{
					return true;
				}
				block2 = (block2.container as Block);
			}
			return false;
		}
	}
}
