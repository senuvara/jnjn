using UnityEngine;

namespace GameFlow
{
	[Help("en", "Gets a property of the specified @UnityManual{SpriteRenderer} component.", null)]
	[AddComponentMenu("")]
	[Help("es", "Lee una propiedad del componente @UnityManual{SpriteRenderer} especificado.", null)]
	public class GetSpriteRendererProperty : Action, IExecutableInEditor
	{
		public enum Property
		{
			Bounds,
			Color,
			Enabled,
			IsPartOfStaticBatch,
			IsVisible,
			LightmapIndex,
			LightmapScaleOffset,
			LocalToWorldMatrix,
			Material,
			Materials,
			ProbeAnchor,
			RealtimeLightmapIndex,
			RealtimeLightmapScaleOffset,
			ReceiveShadows,
			ReflectionProbeUsage,
			ShadowCastingMode,
			SharedMaterial,
			SharedMaterials,
			SortingLayerID,
			SortingLayerName,
			SortingOrder,
			Sprite,
			LightProbeUsage,
			WorldToLocalMatrix
		}

		[SerializeField]
		private SpriteRenderer _source;

		[SerializeField]
		private Variable _sourceVar;

		[SerializeField]
		private Property _property;

		[SerializeField]
		private Variable _output;

		protected override void OnReset()
		{
			_source = base.gameObject.GetComponent<SpriteRenderer>();
		}

		public override void Setup()
		{
		}

		public override void Execute()
		{
			SpriteRenderer spriteRenderer = _sourceVar.GetValueAsComponent(_source, typeof(SpriteRenderer)) as SpriteRenderer;
			if (!(spriteRenderer == null))
			{
				switch (_property)
				{
				case Property.LocalToWorldMatrix:
				case Property.Materials:
				case Property.SharedMaterials:
					break;
				case Property.Bounds:
					_output.SetValue(spriteRenderer.bounds);
					break;
				case Property.Color:
					_output.SetValue(spriteRenderer.color);
					break;
				case Property.Enabled:
					_output.SetValue(spriteRenderer.enabled);
					break;
				case Property.IsPartOfStaticBatch:
					_output.SetValue(spriteRenderer.isPartOfStaticBatch);
					break;
				case Property.IsVisible:
					_output.SetValue(spriteRenderer.isVisible);
					break;
				case Property.LightmapIndex:
					_output.SetValue(spriteRenderer.lightmapIndex);
					break;
				case Property.LightmapScaleOffset:
					_output.SetValue(spriteRenderer.lightmapScaleOffset);
					break;
				case Property.Material:
					_output.SetValue(spriteRenderer.material);
					break;
				case Property.ProbeAnchor:
					_output.SetValue(spriteRenderer.probeAnchor);
					break;
				case Property.RealtimeLightmapIndex:
					_output.SetValue(spriteRenderer.realtimeLightmapIndex);
					break;
				case Property.RealtimeLightmapScaleOffset:
					_output.SetValue(spriteRenderer.realtimeLightmapScaleOffset);
					break;
				case Property.ReceiveShadows:
					_output.SetValue(spriteRenderer.receiveShadows);
					break;
				case Property.ReflectionProbeUsage:
					_output.SetValue(spriteRenderer.reflectionProbeUsage);
					break;
				case Property.ShadowCastingMode:
					_output.SetValue(spriteRenderer.shadowCastingMode);
					break;
				case Property.SharedMaterial:
					_output.SetValue(spriteRenderer.sharedMaterial);
					break;
				case Property.SortingLayerID:
					_output.SetValue(spriteRenderer.sortingLayerID);
					break;
				case Property.SortingLayerName:
					_output.SetValue(spriteRenderer.sortingLayerName);
					break;
				case Property.SortingOrder:
					_output.SetValue(spriteRenderer.sortingOrder);
					break;
				case Property.Sprite:
					_output.SetValue(spriteRenderer.sprite);
					break;
				case Property.LightProbeUsage:
					_output.SetValue(spriteRenderer.lightProbeUsage);
					break;
				}
			}
		}
	}
}
