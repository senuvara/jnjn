using UnityEngine;

namespace GameFlow
{
	[AddComponentMenu("")]
	[Help("es", "Obtiene el valor actual del eje de entrada especificado.", null)]
	[Help("en", "Gets the current value of the specified input axis.", null)]
	public class GetInputAxisValue : Action, IExecutableInEditor
	{
		[SerializeField]
		private string _axis = "Horizontal";

		[SerializeField]
		private Variable _axisVar;

		[SerializeField]
		private bool _raw;

		[SerializeField]
		private Variable _rawVar;

		[SerializeField]
		private Variable _output;

		private static LocalizedString invalidAxis = LocalizedString.Create().Add("en", "The specified Axis is not defined in Input settings.").Add("es", "El eje especificado no aparece definido en los ajustes de entrada.");

		public string axis => _axisVar.GetValue(_axis);

		public bool raw => _rawVar.GetValue(_raw);

		protected override void OnReset()
		{
		}

		public override void Execute()
		{
			if (!(_output == null))
			{
				try
				{
					_output.SetValue(Execute(axis, raw));
				}
				catch
				{
					Log.Error(invalidAxis, this);
				}
			}
		}

		public static float Execute(string axis, bool raw)
		{
			return (!raw) ? Input.GetAxis(axis) : Input.GetAxisRaw(axis);
		}
	}
}
