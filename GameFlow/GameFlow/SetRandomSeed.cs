using UnityEngine;

namespace GameFlow
{
	[Help("es", "Establece el número semilla para el generador de números aleatorios.", null)]
	[AddComponentMenu("")]
	[Help("en", "Sets the seed for the random number generator.", null)]
	public class SetRandomSeed : Action, IExecutableInEditor
	{
		[SerializeField]
		private int _seed;

		[SerializeField]
		private Variable _seedVar;

		protected override void OnReset()
		{
			_seed = Random.Range(0, int.MaxValue);
		}

		public override void Execute()
		{
			Random.InitState(_seedVar.GetValue(_seed));
		}
	}
}
