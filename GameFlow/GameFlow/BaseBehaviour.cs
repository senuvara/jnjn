using UnityEngine;

namespace GameFlow
{
	public abstract class BaseBehaviour : MonoBehaviour
	{
		[SerializeField]
		private bool _initialized;

		[SerializeField]
		private bool _reset;

		[SerializeField]
		private string _version = Plugin.version;

		[SerializeField]
		private bool _moved;

		public bool initialized => _initialized;

		public bool reset => _reset;

		private void Reset()
		{
			_reset = true;
			OnReset();
		}

		protected void SetResetFlag()
		{
			_reset = true;
		}

		public void ClearResetFlag()
		{
			_initialized = true;
			_reset = false;
		}

		protected virtual void OnReset()
		{
		}

		private void Awake()
		{
			OnAwake();
		}

		protected virtual void OnAwake()
		{
		}

		protected void Disable()
		{
			base.enabled = false;
		}

		protected bool IsHiddenInInspector()
		{
			return ObjectUtils.IsHiddenInInspector(this);
		}

		protected bool IsEditable()
		{
			return ObjectUtils.IsEditable(this);
		}

		protected void HideInInspector()
		{
			ObjectUtils.HideInInspector(this);
		}

		protected void MakeEditable()
		{
			ObjectUtils.MakeEditable(this);
		}

		protected void MakeNotEditable()
		{
			ObjectUtils.MakeNotEditable(this);
		}

		protected string GetVersionLabel()
		{
			return _version;
		}
	}
}
