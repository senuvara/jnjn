using UnityEngine;

namespace GameFlow
{
	[AddComponentMenu("")]
	[Help("en", "Limits the value of the specified @GameFlow{Parameter}.", null)]
	[Help("es", "Limita el valor del parámetro (@GameFlow{Parameter}) especificado.", null)]
	public class LimitParameterValue : LimitValueAction, IExecutableInEditor
	{
		[SerializeField]
		private Parameter _parameter;

		public Parameter parameter => _parameter;

		public override void Execute()
		{
			Execute(parameter, this);
		}

		public static void Execute(Parameter parameter, LimitValueAction limitValueAction)
		{
			LimitVariableValue.Execute(parameter, limitValueAction);
		}
	}
}
