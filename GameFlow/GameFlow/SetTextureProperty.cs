using UnityEngine;

namespace GameFlow
{
	[Help("en", "Sets a property of the specified Texture.", null)]
	[AddComponentMenu("")]
	[Help("es", "Modifica una propiedad de la Textura especificada.", null)]
	public class SetTextureProperty : Action, IExecutableInEditor
	{
		public enum TextureProperty
		{
			AnisoLevel,
			FilterMode,
			MipMapBias,
			WrapMode
		}

		public enum Texture2DProperty
		{
			AnisoLevel,
			FilterMode,
			MipMapBias,
			WrapMode
		}

		public enum Texture3DProperty
		{
			AnisoLevel,
			FilterMode,
			MipMapBias,
			WrapMode
		}

		public enum RenderTextureProperty
		{
			AnisoLevel,
			FilterMode,
			MipMapBias,
			WrapMode,
			Antialiasing,
			Depth,
			EnableRandomWrite,
			Format,
			GenerateMips,
			UseMipMap,
			VolumeDepth
		}

		[SerializeField]
		private Texture _target;

		[SerializeField]
		private Variable _targetVar;

		[SerializeField]
		private TextureProperty _textureProperty;

		[SerializeField]
		private Texture2DProperty _texture2dProperty;

		[SerializeField]
		private Texture3DProperty _texture3dProperty;

		[SerializeField]
		private RenderTextureProperty _renderProperty;

		[SerializeField]
		private bool _boolValue;

		[SerializeField]
		private int _intValue;

		[SerializeField]
		private FilterMode _filterModeValue;

		[SerializeField]
		private RenderTextureFormat _formatValue;

		[SerializeField]
		private TextureWrapMode _wrapModeValue;

		[SerializeField]
		private Variable _valueVar;

		public override void Execute()
		{
			Texture texture = _target;
			if (texture == null)
			{
				texture = (_targetVar.GetValue(_target) as Texture);
			}
			if (texture == null)
			{
				return;
			}
			Texture2D x = texture as Texture2D;
			if (x != null)
			{
				switch (_texture2dProperty)
				{
				case Texture2DProperty.AnisoLevel:
					texture.anisoLevel = _valueVar.GetValue(_intValue);
					break;
				case Texture2DProperty.FilterMode:
					texture.filterMode = (FilterMode)(object)_valueVar.GetValue(_filterModeValue);
					break;
				case Texture2DProperty.MipMapBias:
					texture.mipMapBias = _valueVar.GetValue(_intValue);
					break;
				case Texture2DProperty.WrapMode:
					texture.wrapMode = (TextureWrapMode)(object)_valueVar.GetValue(_wrapModeValue);
					break;
				}
				return;
			}
			Texture3D x2 = texture as Texture3D;
			if (x2 != null)
			{
				switch (_texture3dProperty)
				{
				case Texture3DProperty.AnisoLevel:
					texture.anisoLevel = _valueVar.GetValue(_intValue);
					break;
				case Texture3DProperty.FilterMode:
					texture.filterMode = (FilterMode)(object)_valueVar.GetValue(_filterModeValue);
					break;
				case Texture3DProperty.MipMapBias:
					texture.mipMapBias = _valueVar.GetValue(_intValue);
					break;
				case Texture3DProperty.WrapMode:
					texture.wrapMode = (TextureWrapMode)(object)_valueVar.GetValue(_wrapModeValue);
					break;
				}
				return;
			}
			RenderTexture renderTexture = texture as RenderTexture;
			if (renderTexture != null)
			{
				switch (_renderProperty)
				{
				case RenderTextureProperty.AnisoLevel:
					texture.anisoLevel = _valueVar.GetValue(_intValue);
					break;
				case RenderTextureProperty.FilterMode:
					texture.filterMode = (FilterMode)(object)_valueVar.GetValue(_filterModeValue);
					break;
				case RenderTextureProperty.MipMapBias:
					texture.mipMapBias = _valueVar.GetValue(_intValue);
					break;
				case RenderTextureProperty.WrapMode:
					texture.wrapMode = (TextureWrapMode)(object)_valueVar.GetValue(_wrapModeValue);
					break;
				case RenderTextureProperty.Antialiasing:
					renderTexture.antiAliasing = _valueVar.GetValue(_intValue);
					break;
				case RenderTextureProperty.Depth:
					renderTexture.depth = _valueVar.GetValue(_intValue);
					break;
				case RenderTextureProperty.EnableRandomWrite:
					renderTexture.enableRandomWrite = _valueVar.GetValue(_boolValue);
					break;
				case RenderTextureProperty.Format:
					renderTexture.format = (RenderTextureFormat)(object)_valueVar.GetValue(_formatValue);
					break;
				case RenderTextureProperty.GenerateMips:
					renderTexture.autoGenerateMips = _valueVar.GetValue(_boolValue);
					break;
				case RenderTextureProperty.UseMipMap:
					renderTexture.useMipMap = _valueVar.GetValue(_boolValue);
					break;
				case RenderTextureProperty.VolumeDepth:
					renderTexture.volumeDepth = _valueVar.GetValue(_intValue);
					break;
				}
			}
		}
	}
}
