using System;
using UnityEngine;

namespace GameFlow
{
	[AddComponentMenu("")]
	[Help("es", "Obtiene un componente del @UnityManual{GameObject} especificado.", null)]
	[Help("en", "Gets a component of the specified @UnityManual{GameObject}.", null)]
	public class GetComponent : Action, IExecutableInEditor
	{
		private static Type defaultType = typeof(Transform);

		[SerializeField]
		private GameObject _source;

		[SerializeField]
		private Variable _sourceVar;

		[SerializeField]
		private string _typeName = defaultType.FullName;

		private Type _type;

		[SerializeField]
		private Variable _output;

		public Type type
		{
			get
			{
				if (_typeName == null)
				{
					_typeName = defaultType.FullName;
					_type = defaultType;
				}
				else if (_type == null || _typeName != _type.FullName)
				{
					_type = TypeUtils.GetType(_typeName);
				}
				return _type;
			}
		}

		protected override void OnReset()
		{
			_source = base.gameObject;
		}

		public override void Execute()
		{
			if (!(_output == null))
			{
				GameObject gameObject = _sourceVar.GetValue(_source) as GameObject;
				if (!(gameObject == null) && type != null)
				{
					Component component = gameObject.GetComponent(type);
					_output.SetValue(component, type);
				}
			}
		}
	}
}
