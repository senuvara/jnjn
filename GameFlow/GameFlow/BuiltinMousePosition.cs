using System;
using UnityEngine;

namespace GameFlow
{
	[Help("es", "Una @GameFlow{Variable} integrada que devuelve la posición actual del ratón.", null)]
	[AddComponentMenu("")]
	[Help("en", "A built-in @GameFlow{Variable} that returns the current position of the mouse cursor.", null)]
	public class BuiltinMousePosition : BuiltinVariable
	{
		public override Vector2 vector2Value => (!Application.isPlaying) ? Vector3.zero : Input.mousePosition;

		public override Type GetVariableType()
		{
			return typeof(Vector2);
		}

		protected override string GetVariableId()
		{
			return "Mouse Position";
		}
	}
}
