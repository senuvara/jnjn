using System;
using UnityEngine;

namespace GameFlow
{
	[Help("en", "A built-in @GameFlow{Variable} that returns the default language.", null)]
	[Help("es", "Una @GameFlow{Variable} integrada que devuelve el idioma por defecto.", null)]
	[AddComponentMenu("")]
	public class BuiltinDefaultLanguage : BuiltinVariable
	{
		public override Enum enumValue => LocalizationSettings.GetDefaultLanguage().systemLanguage;

		public override Type GetVariableType()
		{
			return typeof(SystemLanguage);
		}

		protected override string GetVariableId()
		{
			return "Default Language";
		}
	}
}
