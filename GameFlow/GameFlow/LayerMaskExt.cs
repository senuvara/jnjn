using UnityEngine;

namespace GameFlow
{
	public static class LayerMaskExt
	{
		public static bool IsLayerSet(this LayerMask layerMask, int layer)
		{
			return ((1 << layer) & layerMask.value) != 0;
		}
	}
}
