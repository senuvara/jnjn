using UnityEngine;

namespace GameFlow
{
	[AddComponentMenu("")]
	[Help("es", "Evalúa el número de elementos de la lista especificada.", null)]
	[Help("en", "Evaluates the count of items of the specified list.", null)]
	public class ListCountCondition : Condition
	{
		public enum Comparison
		{
			IsEqualTo,
			IsNotEqualTo,
			IsGreaterThan,
			IsGreaterThanOrEqualTo,
			IsLessThan,
			IsLessThanOrEqualTo
		}

		[SerializeField]
		private List _list;

		[SerializeField]
		private Variable _listVar;

		[SerializeField]
		private Comparison _comparison;

		[SerializeField]
		private int _intValue;

		[SerializeField]
		private Variable _valueVar;

		public int intValue => _valueVar.GetValue(_intValue);

		public Variable valueVar => _valueVar;

		public override bool Evaluate()
		{
			List list = _listVar.GetValue(_list) as List;
			if (list == null)
			{
				return false;
			}
			int value = _valueVar.GetValue(_intValue);
			switch (_comparison)
			{
			case Comparison.IsEqualTo:
				return list.Count == value;
			case Comparison.IsNotEqualTo:
				return list.Count != value;
			case Comparison.IsGreaterThan:
				return list.Count > value;
			case Comparison.IsGreaterThanOrEqualTo:
				return list.Count >= value;
			case Comparison.IsLessThan:
				return list.Count < value;
			case Comparison.IsLessThanOrEqualTo:
				return list.Count <= value;
			default:
				return false;
			}
		}
	}
}
