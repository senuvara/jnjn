using UnityEngine;

namespace GameFlow
{
	[AddComponentMenu("")]
	[Help("es", "Interpola el valor de la @GameFlow{Variable} especificada.", null)]
	[Help("en", "Interpolates the value of the specified @GameFlow{Variable}.", null)]
	public class InterpolateValue : TimeAction, IExecutableInEditor
	{
		[SerializeField]
		private Variable _variable;

		[SerializeField]
		private int _fromIntValue;

		[SerializeField]
		private float _fromFloatValue;

		[SerializeField]
		private Vector2 _fromVector2Value;

		[SerializeField]
		private Vector3 _fromVector3Value;

		[SerializeField]
		private Vector4 _fromVector4Value;

		[SerializeField]
		private Color _fromColorValue;

		[SerializeField]
		private int _toIntValue;

		[SerializeField]
		private float _toFloatValue;

		[SerializeField]
		private Vector2 _toVector2Value;

		[SerializeField]
		private Vector3 _toVector3Value;

		[SerializeField]
		private Vector4 _toVector4Value;

		[SerializeField]
		private Color _toColorValue;

		[SerializeField]
		private Variable _toValueVar;

		[SerializeField]
		private EasingType _easingType;

		private bool _finished;

		private Variable _resolved;

		public int toIntValue => _toValueVar.GetValue(_toIntValue);

		public float toFloatValue => _toValueVar.GetValue(_toFloatValue);

		public Vector2 toVector2Value => _toValueVar.GetValue(_toVector2Value);

		public Vector3 toVector3Value => _toValueVar.GetValue(_toVector3Value);

		public Vector4 toVector4Value => _toValueVar.GetValue(_toVector4Value);

		public Color toColorValue => _toValueVar.GetValue(_toColorValue);

		public override void FirstStep()
		{
			_finished = true;
			if (_variable == null)
			{
				return;
			}
			_resolved = ((!_variable.isIndirect) ? _variable : _variable.indirection);
			if (_resolved == null || _resolved.isReadOnly)
			{
				return;
			}
			if (base.duration == 0f || !Application.isPlaying)
			{
				InstantInterpolation();
				return;
			}
			_finished = false;
			switch (_resolved.dataType)
			{
			case DataType.Integer:
				_fromIntValue = _resolved.intValue;
				break;
			case DataType.Float:
				_fromFloatValue = _resolved.floatValue;
				break;
			case DataType.Vector2:
				_fromVector2Value = _resolved.vector2Value;
				break;
			case DataType.Vector3:
				_fromVector3Value = _resolved.vector3Value;
				break;
			case DataType.Vector4:
				_fromVector4Value = _resolved.vector4Value;
				break;
			case DataType.Color:
				_fromColorValue = _resolved.colorValue;
				break;
			}
			base.FirstStep();
		}

		private void InstantInterpolation()
		{
			switch (_resolved.dataType)
			{
			case DataType.Integer:
				_resolved.intValue = toIntValue;
				break;
			case DataType.Float:
				_resolved.floatValue = toFloatValue;
				break;
			case DataType.Vector2:
				_resolved.vector2Value = toVector2Value;
				break;
			case DataType.Vector3:
				_resolved.vector3Value = toVector3Value;
				break;
			case DataType.Vector4:
				_resolved.vector4Value = toVector4Value;
				break;
			case DataType.Color:
				_resolved.colorValue = toColorValue;
				break;
			}
		}

		public override void Step()
		{
			float elapsedTimeDelta = base.timer.elapsedTimeDelta;
			switch (_resolved.dataType)
			{
			case DataType.Integer:
				_resolved.intValue = (int)MathUtils.Ease(_fromIntValue, toIntValue, elapsedTimeDelta, _easingType);
				break;
			case DataType.Float:
				_resolved.floatValue = MathUtils.Ease(_fromFloatValue, toFloatValue, elapsedTimeDelta, _easingType);
				break;
			case DataType.Vector2:
				_resolved.vector2Value = MathUtils.Ease(_fromVector2Value, toVector2Value, elapsedTimeDelta, _easingType);
				break;
			case DataType.Vector3:
				_resolved.vector3Value = MathUtils.Ease(_fromVector3Value, toVector3Value, elapsedTimeDelta, _easingType);
				break;
			case DataType.Vector4:
				_resolved.vector4Value = MathUtils.Ease(_fromVector4Value, toVector4Value, elapsedTimeDelta, _easingType);
				break;
			case DataType.Color:
				_resolved.colorValue = MathUtils.Ease(_fromColorValue, toColorValue, elapsedTimeDelta, _easingType);
				break;
			}
		}

		public override bool Finished()
		{
			return _finished || base.Finished();
		}
	}
}
