using System;
using System.Text.RegularExpressions;

namespace GameFlow
{
	public static class VersionUtils
	{
		private static string[] delimiters = new string[1]
		{
			"."
		};

		private static Regex digitsOnly = new Regex("[^\\d]");

		public static int[] VersionStringToNumbers(string version)
		{
			string[] array = version.Split(delimiters, StringSplitOptions.RemoveEmptyEntries);
			int[] array2 = new int[3];
			for (int i = 0; i < array2.Length; i++)
			{
				try
				{
					array2[i] = ((array.Length > i) ? int.Parse(digitsOnly.Replace(array[i], string.Empty)) : 0);
				}
				catch
				{
				}
			}
			return array2;
		}

		public static int VersionStringToInt(string version)
		{
			int[] array = VersionStringToNumbers(version);
			return array[0] * 1000 + array[1] * 100 + array[2];
		}

		public static bool IsBetaVersion(string version)
		{
			if (version == null)
			{
				return false;
			}
			return version.EndsWith("b") || version.EndsWith("B");
		}
	}
}
