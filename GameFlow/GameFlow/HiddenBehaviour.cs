using UnityEngine;

namespace GameFlow
{
	public abstract class HiddenBehaviour : MonoBehaviour
	{
		private void Reset()
		{
			base.hideFlags = HideFlags.HideInInspector;
			OnReset();
		}

		public virtual void OnReset()
		{
		}
	}
}
