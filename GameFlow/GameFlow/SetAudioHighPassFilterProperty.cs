using UnityEngine;

namespace GameFlow
{
	[AddComponentMenu("")]
	[Help("es", "Modifica una propiedad del componente @UnityManual{AudioHighPassFilter} especificado.", null)]
	[Help("en", "Sets a property of the specified @UnityManual{AudioHighPassFilter} component.", null)]
	public class SetAudioHighPassFilterProperty : Action, IExecutableInEditor
	{
		public enum Property
		{
			CutoffFrequency,
			HighpassResonanceQ
		}

		[SerializeField]
		private AudioHighPassFilter _target;

		[SerializeField]
		private Variable _targetVar;

		[SerializeField]
		private Property _property;

		[SerializeField]
		private float _cutoffFrequencyValue;

		[SerializeField]
		private float _highpassResonanceQValue;

		[SerializeField]
		private Variable _valueVar;

		protected override void OnReset()
		{
			_target = base.gameObject.GetComponent<AudioHighPassFilter>();
		}

		public override void Setup()
		{
		}

		public override void Execute()
		{
			AudioHighPassFilter audioHighPassFilter = _targetVar.GetValueAsComponent(_target, typeof(AudioHighPassFilter)) as AudioHighPassFilter;
			if (!(audioHighPassFilter == null))
			{
				switch (_property)
				{
				case Property.CutoffFrequency:
					audioHighPassFilter.cutoffFrequency = _valueVar.GetValue(_cutoffFrequencyValue);
					break;
				case Property.HighpassResonanceQ:
					audioHighPassFilter.highpassResonanceQ = _valueVar.GetValue(_highpassResonanceQValue);
					break;
				}
			}
		}
	}
}
