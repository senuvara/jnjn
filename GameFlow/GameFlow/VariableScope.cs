namespace GameFlow
{
	public enum VariableScope
	{
		GameObject,
		GameObjectHierarchy,
		All
	}
}
