using System.Text;
using UnityEngine;

namespace GameFlow
{
	[Help("en", "Generates the JSON representation for the data contained in the specified @GameFlow{List}.", null)]
	[AddComponentMenu("")]
	[Help("es", "Genera la representación JSON de los datos contenidos en la lista (@GameFlow{List}) especificada.", null)]
	public class GetJSONFromList : ListAction
	{
		public enum Format
		{
			Object,
			Array
		}

		[SerializeField]
		private Variable _output;

		[SerializeField]
		private Format _format;

		[SerializeField]
		private bool _useListId = true;

		[SerializeField]
		private bool _pretty = true;

		public override void Execute()
		{
			if (!(_output == null))
			{
				string value = Execute(base.list, _format, _useListId, _pretty);
				_output.SetValue(value);
			}
		}

		public static string Execute(List list, Format format, bool useListId, bool pretty)
		{
			string text = (!pretty) ? string.Empty : "\n";
			string text2 = (!pretty) ? string.Empty : " ";
			StringBuilder stringBuilder = new StringBuilder();
			switch (format)
			{
			case Format.Object:
			{
				if (list == null)
				{
					return "{" + text2 + "}";
				}
				if (useListId)
				{
					stringBuilder.Append("\"" + list.id + "\"");
					stringBuilder.Append(":" + text2);
				}
				stringBuilder.Append("{" + text);
				int count = list.Count;
				for (int i = 0; i < count; i++)
				{
					Variable variableAt2 = list.GetVariableAt(i);
					if (variableAt2 != null)
					{
						stringBuilder.Append(text2 + text2 + "\"" + variableAt2.id + "\":" + text2);
						stringBuilder.Append(variableAt2.GetValueAsJSON());
					}
					else
					{
						stringBuilder.Append(text2 + text2 + "\"" + i + "\":" + text2);
						stringBuilder.Append(list.GetJSONValueAt(i));
					}
					stringBuilder.Append((i >= count - 1) ? text : ("," + text2 + text));
				}
				stringBuilder.Append("}");
				break;
			}
			case Format.Array:
			{
				if (list == null)
				{
					return "[" + text2 + "]";
				}
				if (useListId)
				{
					stringBuilder.Append("\"" + list.id + "\"");
					stringBuilder.Append(":" + text2);
				}
				stringBuilder.Append("[" + text);
				int count = list.Count;
				for (int i = 0; i < count; i++)
				{
					stringBuilder.Append(text2 + text2 + "{" + text2);
					Variable variableAt = list.GetVariableAt(i);
					stringBuilder.Append("\"" + i + "\":" + text2);
					if (variableAt != null)
					{
						stringBuilder.Append(variableAt.GetValueAsJSON());
					}
					else
					{
						stringBuilder.Append(list.GetJSONValueAt(i));
					}
					stringBuilder.Append((i >= count - 1) ? (text2 + "}" + text) : (text2 + "}," + text));
				}
				stringBuilder.Append("]");
				break;
			}
			}
			return stringBuilder.ToString();
		}
	}
}
