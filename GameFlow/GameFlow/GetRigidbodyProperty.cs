using UnityEngine;

namespace GameFlow
{
	[Help("en", "Gets a property of the specified @UnityManual{Rigidbody} component.", null)]
	[AddComponentMenu("")]
	[Help("es", "Lee una propiedad del componente @UnityManual{Rigidbody} especificado.", null)]
	public class GetRigidbodyProperty : Action, IExecutableInEditor
	{
		public enum Property
		{
			AngularDrag,
			AngularVelocity,
			CenterOfMass,
			CollisionDetectionMode,
			Constraints,
			DetectCollisions,
			Drag,
			FreezeRotation,
			InertiaTensor,
			InertiaTensorRotation,
			Interpolation,
			IsKinematic,
			IsSleeping,
			Mass,
			MaxAngularVelocity,
			Position,
			Rotation,
			SolverIterations,
			UseConeFriction,
			UseGravity,
			Velocity,
			WorldCenterOfMass
		}

		[SerializeField]
		private Rigidbody _source;

		[SerializeField]
		private Variable _sourceVar;

		[SerializeField]
		private Property _property;

		[SerializeField]
		private Variable _output;

		protected override void OnReset()
		{
			_source = GetComponent<Rigidbody>();
		}

		public override void Setup()
		{
		}

		public override void Execute()
		{
			Rigidbody rigidbody = _source;
			if (rigidbody == null)
			{
				rigidbody = (_sourceVar.GetValueAsComponent(_source, typeof(Rigidbody)) as Rigidbody);
			}
			if (!(rigidbody == null))
			{
				switch (_property)
				{
				case Property.UseConeFriction:
					break;
				case Property.AngularDrag:
					_output.SetValue(rigidbody.angularDrag);
					break;
				case Property.AngularVelocity:
					_output.SetValue(rigidbody.angularVelocity);
					break;
				case Property.CenterOfMass:
					_output.SetValue(rigidbody.centerOfMass);
					break;
				case Property.CollisionDetectionMode:
					_output.SetValue(rigidbody.collisionDetectionMode);
					break;
				case Property.Constraints:
					_output.SetValue(rigidbody.constraints);
					break;
				case Property.DetectCollisions:
					_output.SetValue(rigidbody.detectCollisions);
					break;
				case Property.Drag:
					_output.SetValue(rigidbody.drag);
					break;
				case Property.FreezeRotation:
					_output.SetValue(rigidbody.freezeRotation);
					break;
				case Property.InertiaTensor:
					_output.SetValue(rigidbody.inertiaTensor);
					break;
				case Property.InertiaTensorRotation:
					_output.SetValue(rigidbody.inertiaTensorRotation.eulerAngles);
					break;
				case Property.Interpolation:
					_output.SetValue(rigidbody.interpolation);
					break;
				case Property.IsKinematic:
					_output.SetValue(rigidbody.isKinematic);
					break;
				case Property.IsSleeping:
					_output.SetValue(rigidbody.IsSleeping());
					break;
				case Property.Mass:
					_output.SetValue(rigidbody.mass);
					break;
				case Property.MaxAngularVelocity:
					_output.SetValue(rigidbody.maxAngularVelocity);
					break;
				case Property.Position:
					_output.SetValue(rigidbody.position);
					break;
				case Property.Rotation:
					_output.SetValue(rigidbody.rotation.eulerAngles);
					break;
				case Property.SolverIterations:
					_output.SetValue(rigidbody.solverIterations);
					break;
				case Property.UseGravity:
					_output.SetValue(rigidbody.useGravity);
					break;
				case Property.Velocity:
					_output.SetValue(rigidbody.velocity);
					break;
				case Property.WorldCenterOfMass:
					_output.SetValue(rigidbody.worldCenterOfMass);
					break;
				}
			}
		}
	}
}
