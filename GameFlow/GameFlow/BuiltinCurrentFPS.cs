using System;
using UnityEngine;

namespace GameFlow
{
	[Help("es", "Una @GameFlow{Variable} integrada que devuelve el número de FPS (Fotogramas Por Segundo) al que está corriendo la aplicación o juego actualmente.", null)]
	[AddComponentMenu("")]
	[Help("en", "A built-in @GameFlow{Variable} that returns the current FPS (Frames Per Second) the app/game is running at.", null)]
	public class BuiltinCurrentFPS : BuiltinVariable
	{
		public override float floatValue => (!Application.isPlaying) ? 0f : StatsController.currentFPS;

		public override Type GetVariableType()
		{
			return typeof(float);
		}

		protected override string GetVariableId()
		{
			return "Current FPS";
		}
	}
}
