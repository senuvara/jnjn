using UnityEngine;

namespace GameFlow
{
	[AddComponentMenu("")]
	[Help("en", "Multiplies the numeric value of the specified @GameFlow{Parameter}.", null)]
	[Help("es", "Multiplica el valor numérico del @GameFlow{Parameter} especificado.", null)]
	public class MultiplyParameterValue : ParameterAction
	{
		protected override int GetDefaultIntValue()
		{
			return 1;
		}

		protected override float GetDefaultFloatValue()
		{
			return 1f;
		}

		public override void Execute()
		{
			Execute(base.parameter, this);
		}

		public static void Execute(Parameter parameter, ValueAction valueAction)
		{
			MultiplyVariableValue.Execute(parameter, valueAction);
		}
	}
}
