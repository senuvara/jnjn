using System;
using UnityEngine;

namespace GameFlow
{
	[AddComponentMenu("")]
	[Help("es", "Agrega un componente al @UnityManual{GameObject} especificado.", null)]
	[Help("en", "Adds a component to the specified @UnityManual{GameObject}.", null)]
	public class AddComponent : Action, IExecutableInEditor
	{
		private static Type defaultType = typeof(Transform);

		[SerializeField]
		private GameObject _target;

		[SerializeField]
		private Variable _targetVar;

		[SerializeField]
		private string _typeName = defaultType.FullName;

		private Type _type;

		[SerializeField]
		private Variable _output;

		public GameObject target => _targetVar.GetValue(_target) as GameObject;

		public Type type
		{
			get
			{
				if (_typeName == null)
				{
					_typeName = defaultType.FullName;
					_type = defaultType;
				}
				else if (_type == null || _typeName != _type.FullName)
				{
					_type = TypeUtils.GetType(_typeName);
				}
				return _type;
			}
		}

		protected override void OnReset()
		{
			_target = base.gameObject;
		}

		public override void Execute()
		{
			GameObject gameObject = _targetVar.GetValue(_target) as GameObject;
			if (!(gameObject == null) && type != null)
			{
				Component value = gameObject.AddComponent(type);
				_output.SetValue(value, type);
			}
		}
	}
}
