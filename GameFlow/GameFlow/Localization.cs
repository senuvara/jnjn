using System;
using System.Collections.Generic;
using UnityEngine;

namespace GameFlow
{
	[Help("en", "Defines a localized value.", null)]
	[Help("es", "Define un valor localizado.", null)]
	[AddComponentMenu("GameFlow/Data/Localization")]
	public class Localization : Variable
	{
		public enum Backing
		{
			None
		}

		public static Language currentLanguage;

		[SerializeField]
		private Backing _backing;

		[SerializeField]
		private float _elementHeight = 40f;

		[TextArea]
		[SerializeField]
		private string _text;

		[SerializeField]
		private List<SystemLanguage> _languages = new List<SystemLanguage>();

		[SerializeField]
		private List<string> _stringValues;

		[SerializeField]
		private List<int> _intValues;

		[SerializeField]
		private List<float> _floatValues;

		[SerializeField]
		private List<Vector2> _vector2Values;

		[SerializeField]
		private List<Vector3> _vector3Values;

		[SerializeField]
		private List<Vector4> _vector4Values;

		[SerializeField]
		private List<Rect> _rectValues;

		[SerializeField]
		private List<Color> _colorValues;

		[SerializeField]
		private List<UnityEngine.Object> _objectValues;

		[SerializeField]
		private List<AnimationCurve> _curveValues;

		[SerializeField]
		private List<Bounds> _boundsValues;

		[SerializeField]
		private List<Quaternion> _quaternionValues;

		public string key => id;

		public List<SystemLanguage> languages => _languages;

		private List<string> stringValues
		{
			get
			{
				if (_stringValues == null)
				{
					_stringValues = new List<string>();
				}
				return _stringValues;
			}
		}

		private List<int> intValues
		{
			get
			{
				if (_intValues == null)
				{
					_intValues = new List<int>();
				}
				return _intValues;
			}
		}

		private List<float> floatValues
		{
			get
			{
				if (_floatValues == null)
				{
					_floatValues = new List<float>();
				}
				return _floatValues;
			}
		}

		private List<Vector2> vector2Values
		{
			get
			{
				if (_vector2Values == null)
				{
					_vector2Values = new List<Vector2>();
				}
				return _vector2Values;
			}
		}

		private List<Vector3> vector3Values
		{
			get
			{
				if (_vector3Values == null)
				{
					_vector3Values = new List<Vector3>();
				}
				return _vector3Values;
			}
		}

		private List<Vector4> vector4Values
		{
			get
			{
				if (_vector4Values == null)
				{
					_vector4Values = new List<Vector4>();
				}
				return _vector4Values;
			}
		}

		private List<Rect> rectValues
		{
			get
			{
				if (_rectValues == null)
				{
					_rectValues = new List<Rect>();
				}
				return _rectValues;
			}
		}

		private List<Color> colorValues
		{
			get
			{
				if (_colorValues == null)
				{
					_colorValues = new List<Color>();
				}
				return _colorValues;
			}
		}

		private List<UnityEngine.Object> objectValues
		{
			get
			{
				if (_objectValues == null)
				{
					_objectValues = new List<UnityEngine.Object>();
				}
				return _objectValues;
			}
		}

		private List<AnimationCurve> curveValues
		{
			get
			{
				if (_curveValues == null)
				{
					_curveValues = new List<AnimationCurve>();
				}
				return _curveValues;
			}
		}

		private List<Bounds> boundsValues
		{
			get
			{
				if (_boundsValues == null)
				{
					_boundsValues = new List<Bounds>();
				}
				return _boundsValues;
			}
		}

		private List<Quaternion> quaternionValues
		{
			get
			{
				if (_quaternionValues == null)
				{
					_quaternionValues = new List<Quaternion>();
				}
				return _quaternionValues;
			}
		}

		public override string stringValue
		{
			get
			{
				if (base.dataType == DataType.String)
				{
					int num = (stringValues.Count != 0) ? GetLanguageIndex() : (-1);
					return (num >= 0) ? stringValues[num] : GetIdForControls();
				}
				return base.stringValue;
			}
		}

		public override int intValue
		{
			get
			{
				if (base.dataType == DataType.Integer)
				{
					int num = (intValues.Count != 0) ? GetLanguageIndex() : (-1);
					return (num >= 0) ? intValues[num] : 0;
				}
				return base.intValue;
			}
		}

		public override float floatValue
		{
			get
			{
				if (base.dataType == DataType.Float)
				{
					int num = (floatValues.Count != 0) ? GetLanguageIndex() : (-1);
					return (num >= 0) ? floatValues[num] : 0f;
				}
				return base.floatValue;
			}
		}

		public override bool boolValue
		{
			get
			{
				if (base.dataType == DataType.Boolean)
				{
					int num = (intValues.Count != 0) ? GetLanguageIndex() : (-1);
					return num >= 0 && intValues[num] != 0;
				}
				return base.boolValue;
			}
		}

		public override Vector2 vector2Value
		{
			get
			{
				if (base.dataType == DataType.Vector2)
				{
					int num = (vector2Values.Count != 0) ? GetLanguageIndex() : (-1);
					return (num >= 0) ? vector2Values[num] : Vector2.zero;
				}
				return base.vector2Value;
			}
		}

		public override Vector3 vector3Value
		{
			get
			{
				if (base.dataType == DataType.Vector3)
				{
					int num = (vector3Values.Count != 0) ? GetLanguageIndex() : (-1);
					return (num >= 0) ? vector3Values[num] : Vector3.zero;
				}
				return base.vector3Value;
			}
		}

		public override Vector4 vector4Value
		{
			get
			{
				if (base.dataType == DataType.Vector4)
				{
					int num = (vector4Values.Count != 0) ? GetLanguageIndex() : (-1);
					return (num >= 0) ? vector4Values[num] : Vector4.zero;
				}
				return base.vector4Value;
			}
		}

		public override Rect rectValue
		{
			get
			{
				if (base.dataType == DataType.Rect)
				{
					int num = (rectValues.Count != 0) ? GetLanguageIndex() : (-1);
					return (num >= 0) ? rectValues[num] : RectExt.zero;
				}
				return base.rectValue;
			}
		}

		public override Color colorValue
		{
			get
			{
				if (base.dataType == DataType.Color)
				{
					int num = (colorValues.Count != 0) ? GetLanguageIndex() : (-1);
					return (num >= 0) ? colorValues[num] : Color.black;
				}
				return base.colorValue;
			}
		}

		public override UnityEngine.Object objectValue
		{
			get
			{
				if (base.dataType == DataType.Object)
				{
					int num = (objectValues.Count != 0) ? GetLanguageIndex() : (-1);
					return (num >= 0) ? objectValues[num] : null;
				}
				return base.objectValue;
			}
		}

		public override Enum enumValue
		{
			get
			{
				if (base.dataType == DataType.Object)
				{
					int num = (objectValues.Count != 0) ? GetLanguageIndex() : (-1);
					return (Enum)((num >= 0) ? ((Enum)Enum.ToObject(base.type, intValues[num])) : ((object)None.None));
				}
				return base.enumValue;
			}
		}

		public override bool toggleValue
		{
			get
			{
				if (base.dataType == DataType.Toggle)
				{
					int num = (intValues.Count != 0) ? GetLanguageIndex() : (-1);
					return num >= 0 && intValues[num] != 0;
				}
				return base.toggleValue;
			}
		}

		public override string tagValue => stringValue;

		public override int layerValue => intValue;

		public override AnimationCurve animationCurveValue
		{
			get
			{
				if (base.dataType == DataType.AnimationCurve)
				{
					int num = (curveValues.Count != 0) ? GetLanguageIndex() : (-1);
					return (num >= 0) ? curveValues[num] : new AnimationCurve();
				}
				return base.animationCurveValue;
			}
		}

		public override Bounds boundsValue
		{
			get
			{
				if (base.dataType == DataType.Bounds)
				{
					int num = (boundsValues.Count != 0) ? GetLanguageIndex() : (-1);
					return (num >= 0) ? boundsValues[num] : BoundsExt.zero;
				}
				return base.boundsValue;
			}
		}

		public override Quaternion quaternionValue
		{
			get
			{
				if (base.dataType == DataType.Quaternion)
				{
					int num = (quaternionValues.Count != 0) ? GetLanguageIndex() : (-1);
					return (num >= 0) ? quaternionValues[num] : Quaternion.identity;
				}
				return base.quaternionValue;
			}
		}

		private int GetLanguageIndex()
		{
			SystemLanguage item = (currentLanguage == null) ? GameSettings.language.systemLanguage : currentLanguage.systemLanguage;
			int num = _languages.IndexOf(item);
			if (num < 0)
			{
				num = _languages.IndexOf(LocalizationSettings.GetDefaultLanguage().systemLanguage);
			}
			return num;
		}

		public void AddItem()
		{
			_languages.Add(SystemLanguage.English);
			switch (base.dataType)
			{
			case DataType.String:
				stringValues.Add(string.Empty);
				break;
			case DataType.Integer:
			case DataType.Boolean:
			case DataType.Enum:
			case DataType.Toggle:
			case DataType.Layer:
				intValues.Add(0);
				break;
			case DataType.Float:
				floatValues.Add(0f);
				break;
			case DataType.Vector2:
				vector2Values.Add(Vector2.zero);
				break;
			case DataType.Vector3:
				vector3Values.Add(Vector3.zero);
				break;
			case DataType.Vector4:
				vector3Values.Add(Vector4.zero);
				break;
			case DataType.Rect:
				rectValues.Add(RectExt.zero);
				break;
			case DataType.Color:
				colorValues.Add(Color.black);
				break;
			case DataType.Object:
				objectValues.Add(null);
				break;
			case DataType.Tag:
				stringValues.Add("Untagged");
				break;
			case DataType.AnimationCurve:
				curveValues.Add(new AnimationCurve());
				break;
			case DataType.Bounds:
				boundsValues.Add(BoundsExt.zero);
				break;
			case DataType.Quaternion:
				quaternionValues.Add(Quaternion.identity);
				break;
			}
		}

		private void Reset()
		{
			HideInInspector();
			Disable();
			SetContainer();
			SetResetFlag();
			MakeReadOnly();
			OnReset();
		}

		protected override void OnReset()
		{
			base.OnReset();
		}

		public override string GetDefaultId()
		{
			return "Localization" + UnityEngine.Random.Range(1000, 9999);
		}

		private void SetContainer()
		{
			if (container != null || !BlockInsertion.enabled || BlockInsertion.clipboard)
			{
				return;
			}
			ILocalizationContainer localizationContainer = BlockInsertion.target as ILocalizationContainer;
			if (localizationContainer == null)
			{
				localizationContainer = base.gameObject.GetComponent<Localizations>();
				if (localizationContainer == null)
				{
					localizationContainer = base.gameObject.AddComponent<Localizations>();
				}
				BlockInsertion.index = -1;
			}
			if (BlockInsertion.index < 0 || BlockInsertion.index >= localizationContainer.GetLocalizations().Count)
			{
				if (!localizationContainer.Contains(this))
				{
					localizationContainer.GetLocalizations().Add(this);
					localizationContainer.LocalizationAdded(this);
					localizationContainer.SetDirty(dirty: true);
				}
			}
			else
			{
				localizationContainer.GetLocalizations().Insert(BlockInsertion.index, this);
				localizationContainer.LocalizationAdded(this);
				localizationContainer.SetDirty(dirty: true);
			}
			container = (localizationContainer as UnityEngine.Object);
			BlockInsertion.Reset();
		}

		public override string GetTypeForSelectors()
		{
			return "Localization";
		}
	}
}
