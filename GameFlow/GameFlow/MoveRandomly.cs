using UnityEngine;

namespace GameFlow
{
	[Help("en", "Moves the specified object to a random position.", null)]
	[AddComponentMenu("")]
	[Help("es", "Mueve el objeto especificado hasta una posición aleatoria.", null)]
	public class MoveRandomly : Move
	{
		[SerializeField]
		private Vector3 _min = new Vector3(-5f, -5f, -5f);

		[SerializeField]
		private Variable _minVar;

		[SerializeField]
		private Vector3 _max = new Vector3(5f, 5f, 5f);

		[SerializeField]
		private Variable _maxVar;

		public Vector3 min => _minVar.GetValue(_min);

		public Vector3 max => _maxVar.GetValue(_max);

		protected override void OnReset()
		{
			base.OnReset();
			base.positionTargetType = TargetType.Vector3;
		}

		protected override void SetupTargetValues()
		{
			Vector3 randomVector = RandomUtils.GetRandomVector3(min, max);
			if (base.relative)
			{
				toPosition = _t1.position + randomVector;
			}
			else
			{
				toPosition = randomVector;
			}
		}
	}
}
