using UnityEngine;

namespace GameFlow
{
	[Help("en", "Gets the magnitude of the specified vector.", null)]
	[Help("es", "Devuelve la magnitud del vector especificado.", null)]
	[AddComponentMenu("")]
	public class GetMagnitude : Action, IExecutableInEditor
	{
		public enum InputType
		{
			Vector2,
			Vector3
		}

		[SerializeField]
		private int _inputType = 1;

		[SerializeField]
		private Vector2 _vector2;

		[SerializeField]
		private Variable _vector2Var;

		[SerializeField]
		private Vector3 _vector3;

		[SerializeField]
		private Variable _vector3Var;

		[SerializeField]
		private Variable _output;

		public override void Execute()
		{
			if (!(_output == null))
			{
				switch (_inputType)
				{
				case 0:
				{
					Vector2 value2 = _vector2Var.GetValue(_vector2);
					_output.floatValue = value2.magnitude;
					break;
				}
				case 1:
				{
					Vector3 value = _vector3Var.GetValue(_vector3);
					_output.floatValue = value.magnitude;
					break;
				}
				}
			}
		}
	}
}
