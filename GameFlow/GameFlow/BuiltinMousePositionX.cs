using System;
using UnityEngine;

namespace GameFlow
{
	[AddComponentMenu("")]
	[Help("es", "Una @GameFlow{Variable} integrada que devuelve la coord. X de la posición actual del ratón.", null)]
	[Help("en", "A built-in @GameFlow{Variable} that returns the X coord. of the current position of the mouse cursor.", null)]
	public class BuiltinMousePositionX : BuiltinVariable
	{
		public override float floatValue
		{
			get
			{
				float result;
				if (Application.isPlaying)
				{
					Vector3 mousePosition = Input.mousePosition;
					result = mousePosition.x;
				}
				else
				{
					result = 0f;
				}
				return result;
			}
		}

		public override Type GetVariableType()
		{
			return typeof(float);
		}

		protected override string GetVariableId()
		{
			return "Mouse Position X";
		}
	}
}
