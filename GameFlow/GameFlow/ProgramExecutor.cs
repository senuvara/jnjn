namespace GameFlow
{
	public class ProgramExecutor
	{
		public static IProgramExecutorProxy proxy;

		public static bool isPlaying
		{
			get
			{
				if (proxy != null)
				{
					return proxy.IsPlaying();
				}
				return false;
			}
		}

		public static Program runningProgram
		{
			get
			{
				if (proxy != null)
				{
					return proxy.GetRunningProgram();
				}
				return null;
			}
		}

		public static void ExecuteProgram(Program program, bool yields = true, bool logElapsedTime = true, bool ignorePause = false)
		{
			if (proxy != null)
			{
				proxy.ExecuteProgram(program, yields, logElapsedTime, ignorePause);
			}
		}

		public static void StopExecution()
		{
			if (proxy != null)
			{
				proxy.StopExecution();
			}
		}
	}
}
