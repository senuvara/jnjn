using System.Collections.Generic;

namespace GameFlow
{
	public class ActionSequence
	{
		private List<Action> _actions;

		private int _last = -1;

		private int _current;

		public ActionSequence(List<Action> actions)
		{
			_actions = actions;
			for (int i = 0; i < actions.Count; i++)
			{
				if (actions[i] != null)
				{
					actions[i].Setup();
				}
			}
		}

		public void FirstEnabledAction()
		{
			_last = -1;
			NextEnabledActionFrom(0);
		}

		private void NextEnabledActionFrom(int index)
		{
			int i;
			for (i = ((index >= _actions.Count) ? _actions.Count : index); i < _actions.Count && (!(_actions[i] != null) || !_actions[i].enabledInEditor); i++)
			{
			}
			_current = i;
		}

		public void NextEnabledAction()
		{
			NextEnabledActionFrom(_current + 1);
		}

		public bool IsNewAction()
		{
			return _current != _last;
		}

		public void ExecuteCurrentAction()
		{
			if (_current < _actions.Count)
			{
				Action action = _actions[_current];
				if (_current != _last)
				{
					ActionExecutor.ExecuteActionFirstStep(action);
					_last = _current;
				}
				else if (!ActionExecutor.ExecuteActionStep(action))
				{
					_last = _current;
				}
			}
		}

		public void MarkCurrentActionAsExecuted()
		{
			if (_current < _actions.Count)
			{
				_last = _current;
			}
		}

		public Action GetCurrentAction()
		{
			if (_current < _actions.Count)
			{
				return _actions[_current];
			}
			return null;
		}

		public bool Finished()
		{
			return _current == _actions.Count;
		}
	}
}
