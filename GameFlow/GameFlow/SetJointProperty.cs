using UnityEngine;

namespace GameFlow
{
	[AddComponentMenu("")]
	[Help("en", "Sets a property of the specified Joint component.", null)]
	[Help("es", "Modifica una propiedad del componente Joint especificado.", null)]
	public class SetJointProperty : Action, IExecutableInEditor
	{
		public enum JointProperty
		{
			Anchor,
			AutoConfigureConnectedAnchor,
			Axis,
			BreakForce,
			BreakTorque,
			ConnectedAnchor,
			ConnectedBody,
			EnableCollision,
			EnablePreprocessing
		}

		public enum CharacterJointProperty
		{
			Anchor,
			AutoConfigureConnectedAnchor,
			Axis,
			BreakForce,
			BreakTorque,
			ConnectedAnchor,
			ConnectedBody,
			EnableCollision,
			EnablePreprocessing,
			EnableProjection,
			HighTwistLimit_Bounciness,
			HighTwistLimit_ContactDistance,
			HighTwistLimit_Limit,
			LowTwistLimit_Bounciness,
			LowTwistLimit_ContactDistance,
			LowTwistLimit_Limit,
			ProjectionAngle,
			ProjectionDistance,
			Swing1Limit_Bounciness,
			Swing1Limit_ContactDistance,
			Swing1Limit_Limit,
			Swing2Limit_Bounciness,
			Swing2Limit_ContactDistance,
			Swing2Limit_Limit,
			SwingAxis,
			SwingLimitSpring_Damper,
			SwingLimitSpring_Spring,
			TwistLimitSpring_Damper,
			TwistLimitSpring_Spring
		}

		public enum HingeJointProperty
		{
			Anchor,
			AutoConfigureConnectedAnchor,
			Axis,
			BreakForce,
			BreakTorque,
			ConnectedAnchor,
			ConnectedBody,
			EnableCollision,
			EnablePreprocessing,
			Limits_BounceMinVelocity,
			Limits_Bounciness,
			Limits_ContactDistance,
			Limits_Max,
			Limits_Min,
			Motor_Force,
			Motor_FreeSpin,
			Motor_TargetVelocity,
			Spring_Damper,
			Spring_Spring,
			Spring_TargetPosition,
			UseLimits,
			UseMotor,
			UseSpring
		}

		public enum SpringJointProperty
		{
			Anchor,
			AutoConfigureConnectedAnchor,
			Axis,
			BreakForce,
			BreakTorque,
			ConnectedAnchor,
			ConnectedBody,
			EnableCollision,
			EnablePreprocessing,
			Damper,
			MaxDistance,
			MinDistance,
			Spring,
			Tolerance
		}

		public enum FixedJointProperty
		{
			Anchor,
			AutoConfigureConnectedAnchor,
			Axis,
			BreakForce,
			BreakTorque,
			ConnectedAnchor,
			ConnectedBody,
			EnableCollision,
			EnablePreprocessing
		}

		public enum ConfigJointProperty
		{
			Anchor,
			AutoConfigureConnectedAnchor,
			Axis,
			BreakForce,
			BreakTorque,
			ConnectedAnchor,
			ConnectedBody,
			EnableCollision,
			EnablePreprocessing,
			AngularXDrive_MaximumForce,
			AngularXDrive_PositionDamper,
			AngularXDrive_PositionSpring,
			AngularXLimitSpring_Damper,
			AngularXLimitSpring_Spring,
			AngularXMotion,
			AngularYLimit_Bounciness,
			AngularYLimit_ContactDistance,
			AngularYLimit_Limit,
			AngularYMotion,
			AngularYZDrive_MaximumForce,
			AngularYZDrive_PositionDamper,
			AngularYZDrive_PositionSpring,
			AngularYZLimitSpring_Damper,
			AngularYZLimitSpring_Spring,
			AngularZLimit_Bounciness,
			AngularZLimit_ContactDistance,
			AngularZLimit_Limit,
			AngularZMotion,
			ConfiguredInWorldSpace,
			HighAngularXLimit_Bounciness,
			HighAngularXLimit_ContactDistance,
			HighAngularXLimit_Limit,
			LinearLimit_Bounciness,
			LinearLimit_ContactDistance,
			LinearLimit_Limit,
			LinearLimitSpring_Damper,
			LinearLimitSpring_Spring,
			LowAngularXLimit_Bounciness,
			LowAngularXLimit_ContactDistance,
			LowAngularXLimit_Limit,
			ProjectionAngle,
			ProjectionDistance,
			ProjectionMode,
			RotationDriveMode,
			SecondaryAxis,
			SlerpDrive_MaximumForce,
			SlerpDrive_PositionDamper,
			SlerpDrive_PositionSpring,
			SwapBodies,
			TargetAngularVelocity,
			TargetPosition,
			TargetRotation,
			TargetVelocity,
			XDrive_MaximumForce,
			XDrive_PositionDamper,
			XDrive_PositionSpring,
			XMotion,
			YDrive_MaximumForce,
			YDrive_PositionDamper,
			YDrive_PositionSpring,
			YMotion,
			ZDrive_MaximumForce,
			ZDrive_PositionDamper,
			ZDrive_PositionSpring,
			ZMotion
		}

		[SerializeField]
		private Joint _target;

		[SerializeField]
		private Variable _targetVar;

		[SerializeField]
		private JointProperty _jointProperty;

		[SerializeField]
		private CharacterJointProperty _characterProperty;

		[SerializeField]
		private HingeJointProperty _hingeProperty;

		[SerializeField]
		private SpringJointProperty _springProperty;

		[SerializeField]
		private FixedJointProperty _fixedProperty;

		[SerializeField]
		private ConfigJointProperty _configProperty;

		[SerializeField]
		private Vector3 _vector3Value;

		[SerializeField]
		private bool _boolValue;

		[SerializeField]
		private Rigidbody _rigidbodyValue;

		[SerializeField]
		private float _floatValue;

		[SerializeField]
		private Quaternion _quaternionValue;

		[SerializeField]
		private ConfigurableJointMotion _motionValue;

		[SerializeField]
		private JointProjectionMode _jointProjectionModeValue;

		[SerializeField]
		private RotationDriveMode _rotationDriveModeValue;

		[SerializeField]
		private Variable _valueVar;

		protected override void OnReset()
		{
			_target = base.gameObject.GetComponent<Joint>();
		}

		public override void Execute()
		{
			Joint joint = _target;
			if (joint == null)
			{
				joint = (_targetVar.GetValueAsComponent(_target, typeof(Joint)) as Joint);
			}
			if (joint == null)
			{
				return;
			}
			CharacterJoint characterJoint = joint as CharacterJoint;
			if (characterJoint != null)
			{
				switch (_characterProperty)
				{
				case CharacterJointProperty.Anchor:
					joint.anchor = _valueVar.GetValue(_vector3Value);
					break;
				case CharacterJointProperty.AutoConfigureConnectedAnchor:
					joint.autoConfigureConnectedAnchor = _valueVar.GetValue(_boolValue);
					break;
				case CharacterJointProperty.Axis:
					joint.axis = _valueVar.GetValue(_vector3Value);
					break;
				case CharacterJointProperty.BreakForce:
					joint.breakForce = _valueVar.GetValue(_floatValue);
					break;
				case CharacterJointProperty.BreakTorque:
					joint.breakTorque = _valueVar.GetValue(_floatValue);
					break;
				case CharacterJointProperty.ConnectedAnchor:
					joint.connectedAnchor = _valueVar.GetValue(_vector3Value);
					break;
				case CharacterJointProperty.ConnectedBody:
					joint.connectedBody = (_valueVar.GetValueAsComponent(_rigidbodyValue, typeof(Rigidbody)) as Rigidbody);
					break;
				case CharacterJointProperty.EnableCollision:
					joint.enableCollision = _valueVar.GetValue(_boolValue);
					break;
				case CharacterJointProperty.EnablePreprocessing:
					joint.enablePreprocessing = _valueVar.GetValue(_boolValue);
					break;
				case CharacterJointProperty.EnableProjection:
					characterJoint.enableProjection = _valueVar.GetValue(_boolValue);
					break;
				case CharacterJointProperty.HighTwistLimit_Bounciness:
				{
					SoftJointLimit swing2Limit = characterJoint.highTwistLimit;
					swing2Limit.bounciness = _valueVar.GetValue(_floatValue);
					characterJoint.highTwistLimit = swing2Limit;
					break;
				}
				case CharacterJointProperty.HighTwistLimit_ContactDistance:
				{
					SoftJointLimit swing2Limit = characterJoint.highTwistLimit;
					swing2Limit.contactDistance = _valueVar.GetValue(_floatValue);
					characterJoint.highTwistLimit = swing2Limit;
					break;
				}
				case CharacterJointProperty.HighTwistLimit_Limit:
				{
					SoftJointLimit swing2Limit = characterJoint.highTwistLimit;
					swing2Limit.limit = _valueVar.GetValue(_floatValue);
					characterJoint.highTwistLimit = swing2Limit;
					break;
				}
				case CharacterJointProperty.LowTwistLimit_Bounciness:
				{
					SoftJointLimit swing2Limit = characterJoint.lowTwistLimit;
					swing2Limit.bounciness = _valueVar.GetValue(_floatValue);
					characterJoint.lowTwistLimit = swing2Limit;
					break;
				}
				case CharacterJointProperty.LowTwistLimit_ContactDistance:
				{
					SoftJointLimit swing2Limit = characterJoint.lowTwistLimit;
					swing2Limit.contactDistance = _valueVar.GetValue(_floatValue);
					characterJoint.lowTwistLimit = swing2Limit;
					break;
				}
				case CharacterJointProperty.LowTwistLimit_Limit:
				{
					SoftJointLimit swing2Limit = characterJoint.lowTwistLimit;
					swing2Limit.limit = _valueVar.GetValue(_floatValue);
					characterJoint.lowTwistLimit = swing2Limit;
					break;
				}
				case CharacterJointProperty.ProjectionAngle:
					characterJoint.projectionAngle = _valueVar.GetValue(_floatValue);
					break;
				case CharacterJointProperty.ProjectionDistance:
					characterJoint.projectionDistance = _valueVar.GetValue(_floatValue);
					break;
				case CharacterJointProperty.Swing1Limit_Bounciness:
				{
					SoftJointLimit swing2Limit = characterJoint.swing1Limit;
					swing2Limit.bounciness = _valueVar.GetValue(_floatValue);
					characterJoint.swing1Limit = swing2Limit;
					break;
				}
				case CharacterJointProperty.Swing1Limit_ContactDistance:
				{
					SoftJointLimit swing2Limit = characterJoint.swing1Limit;
					swing2Limit.contactDistance = _valueVar.GetValue(_floatValue);
					characterJoint.swing1Limit = swing2Limit;
					break;
				}
				case CharacterJointProperty.Swing1Limit_Limit:
				{
					SoftJointLimit swing2Limit = characterJoint.swing1Limit;
					swing2Limit.limit = _valueVar.GetValue(_floatValue);
					characterJoint.swing1Limit = swing2Limit;
					break;
				}
				case CharacterJointProperty.Swing2Limit_Bounciness:
				{
					SoftJointLimit swing2Limit = characterJoint.swing2Limit;
					swing2Limit.bounciness = _valueVar.GetValue(_floatValue);
					characterJoint.swing2Limit = swing2Limit;
					break;
				}
				case CharacterJointProperty.Swing2Limit_ContactDistance:
				{
					SoftJointLimit swing2Limit = characterJoint.swing2Limit;
					swing2Limit.contactDistance = _valueVar.GetValue(_floatValue);
					characterJoint.swing2Limit = swing2Limit;
					break;
				}
				case CharacterJointProperty.Swing2Limit_Limit:
				{
					SoftJointLimit swing2Limit = characterJoint.swing2Limit;
					swing2Limit.limit = _valueVar.GetValue(_floatValue);
					characterJoint.swing2Limit = swing2Limit;
					break;
				}
				case CharacterJointProperty.SwingAxis:
					characterJoint.swingAxis = _valueVar.GetValue(_vector3Value);
					break;
				case CharacterJointProperty.SwingLimitSpring_Damper:
				{
					SoftJointLimitSpring twistLimitSpring = characterJoint.swingLimitSpring;
					twistLimitSpring.damper = _valueVar.GetValue(_floatValue);
					characterJoint.swingLimitSpring = twistLimitSpring;
					break;
				}
				case CharacterJointProperty.SwingLimitSpring_Spring:
				{
					SoftJointLimitSpring twistLimitSpring = characterJoint.swingLimitSpring;
					twistLimitSpring.spring = _valueVar.GetValue(_floatValue);
					characterJoint.swingLimitSpring = twistLimitSpring;
					break;
				}
				case CharacterJointProperty.TwistLimitSpring_Damper:
				{
					SoftJointLimitSpring twistLimitSpring = characterJoint.twistLimitSpring;
					twistLimitSpring.damper = _valueVar.GetValue(_floatValue);
					characterJoint.twistLimitSpring = twistLimitSpring;
					break;
				}
				case CharacterJointProperty.TwistLimitSpring_Spring:
				{
					SoftJointLimitSpring twistLimitSpring = characterJoint.twistLimitSpring;
					twistLimitSpring.spring = _valueVar.GetValue(_floatValue);
					characterJoint.twistLimitSpring = twistLimitSpring;
					break;
				}
				}
				return;
			}
			HingeJoint hingeJoint = joint as HingeJoint;
			if (hingeJoint != null)
			{
				JointSpring spring;
				switch (_hingeProperty)
				{
				case HingeJointProperty.Anchor:
					joint.anchor = _valueVar.GetValue(_vector3Value);
					break;
				case HingeJointProperty.AutoConfigureConnectedAnchor:
					joint.autoConfigureConnectedAnchor = _valueVar.GetValue(_boolValue);
					break;
				case HingeJointProperty.Axis:
					joint.axis = _valueVar.GetValue(_vector3Value);
					break;
				case HingeJointProperty.BreakForce:
					joint.breakForce = _valueVar.GetValue(_floatValue);
					break;
				case HingeJointProperty.BreakTorque:
					joint.breakTorque = _valueVar.GetValue(_floatValue);
					break;
				case HingeJointProperty.ConnectedAnchor:
					joint.connectedAnchor = _valueVar.GetValue(_vector3Value);
					break;
				case HingeJointProperty.ConnectedBody:
					joint.connectedBody = (_valueVar.GetValueAsComponent(_rigidbodyValue, typeof(Rigidbody)) as Rigidbody);
					break;
				case HingeJointProperty.EnableCollision:
					joint.enableCollision = _valueVar.GetValue(_boolValue);
					break;
				case HingeJointProperty.EnablePreprocessing:
					joint.enablePreprocessing = _valueVar.GetValue(_boolValue);
					break;
				case HingeJointProperty.Limits_BounceMinVelocity:
				{
					JointLimits limits = hingeJoint.limits;
					limits.bounceMinVelocity = _valueVar.GetValue(_floatValue);
					hingeJoint.limits = limits;
					break;
				}
				case HingeJointProperty.Limits_Bounciness:
				{
					JointLimits limits = hingeJoint.limits;
					limits.bounciness = _valueVar.GetValue(_floatValue);
					hingeJoint.limits = limits;
					break;
				}
				case HingeJointProperty.Limits_ContactDistance:
				{
					JointLimits limits = hingeJoint.limits;
					limits.contactDistance = _valueVar.GetValue(_floatValue);
					hingeJoint.limits = limits;
					break;
				}
				case HingeJointProperty.Limits_Max:
				{
					JointLimits limits = hingeJoint.limits;
					limits.max = _valueVar.GetValue(_floatValue);
					hingeJoint.limits = limits;
					break;
				}
				case HingeJointProperty.Limits_Min:
				{
					JointLimits limits = hingeJoint.limits;
					limits.min = _valueVar.GetValue(_floatValue);
					hingeJoint.limits = limits;
					break;
				}
				case HingeJointProperty.Motor_Force:
				{
					JointMotor motor = hingeJoint.motor;
					motor.force = _valueVar.GetValue(_floatValue);
					hingeJoint.motor = motor;
					break;
				}
				case HingeJointProperty.Motor_FreeSpin:
				{
					JointMotor motor = hingeJoint.motor;
					motor.freeSpin = _valueVar.GetValue(_boolValue);
					hingeJoint.motor = motor;
					break;
				}
				case HingeJointProperty.Motor_TargetVelocity:
				{
					JointMotor motor = hingeJoint.motor;
					motor.targetVelocity = _valueVar.GetValue(_floatValue);
					hingeJoint.motor = motor;
					break;
				}
				case HingeJointProperty.Spring_Damper:
					spring = hingeJoint.spring;
					spring.damper = _valueVar.GetValue(_floatValue);
					hingeJoint.spring = spring;
					break;
				case HingeJointProperty.Spring_Spring:
					spring = hingeJoint.spring;
					spring.spring = _valueVar.GetValue(_floatValue);
					hingeJoint.spring = spring;
					break;
				case HingeJointProperty.Spring_TargetPosition:
					spring = hingeJoint.spring;
					spring.targetPosition = _valueVar.GetValue(_floatValue);
					hingeJoint.spring = spring;
					break;
				case HingeJointProperty.UseLimits:
					hingeJoint.useLimits = _valueVar.GetValue(_boolValue);
					break;
				case HingeJointProperty.UseMotor:
					hingeJoint.useMotor = _valueVar.GetValue(_boolValue);
					break;
				case HingeJointProperty.UseSpring:
					hingeJoint.useSpring = _valueVar.GetValue(_boolValue);
					break;
				}
				return;
			}
			SpringJoint springJoint = joint as SpringJoint;
			if (springJoint != null)
			{
				switch (_springProperty)
				{
				case SpringJointProperty.Anchor:
					joint.anchor = _valueVar.GetValue(_vector3Value);
					break;
				case SpringJointProperty.AutoConfigureConnectedAnchor:
					joint.autoConfigureConnectedAnchor = _valueVar.GetValue(_boolValue);
					break;
				case SpringJointProperty.Axis:
					joint.axis = _valueVar.GetValue(_vector3Value);
					break;
				case SpringJointProperty.BreakForce:
					joint.breakForce = _valueVar.GetValue(_floatValue);
					break;
				case SpringJointProperty.BreakTorque:
					joint.breakTorque = _valueVar.GetValue(_floatValue);
					break;
				case SpringJointProperty.ConnectedAnchor:
					joint.connectedAnchor = _valueVar.GetValue(_vector3Value);
					break;
				case SpringJointProperty.ConnectedBody:
					joint.connectedBody = (_valueVar.GetValueAsComponent(_rigidbodyValue, typeof(Rigidbody)) as Rigidbody);
					break;
				case SpringJointProperty.EnableCollision:
					joint.enableCollision = _valueVar.GetValue(_boolValue);
					break;
				case SpringJointProperty.EnablePreprocessing:
					joint.enablePreprocessing = _valueVar.GetValue(_boolValue);
					break;
				case SpringJointProperty.Damper:
					springJoint.damper = _valueVar.GetValue(_floatValue);
					break;
				case SpringJointProperty.MaxDistance:
					springJoint.maxDistance = _valueVar.GetValue(_floatValue);
					break;
				case SpringJointProperty.MinDistance:
					springJoint.minDistance = _valueVar.GetValue(_floatValue);
					break;
				case SpringJointProperty.Spring:
					springJoint.spring = _valueVar.GetValue(_floatValue);
					break;
				case SpringJointProperty.Tolerance:
					springJoint.tolerance = _valueVar.GetValue(_floatValue);
					break;
				}
				return;
			}
			FixedJoint x = joint as FixedJoint;
			if (x != null)
			{
				switch (_fixedProperty)
				{
				case FixedJointProperty.Anchor:
					joint.anchor = _valueVar.GetValue(_vector3Value);
					break;
				case FixedJointProperty.AutoConfigureConnectedAnchor:
					joint.autoConfigureConnectedAnchor = _valueVar.GetValue(_boolValue);
					break;
				case FixedJointProperty.Axis:
					joint.axis = _valueVar.GetValue(_vector3Value);
					break;
				case FixedJointProperty.BreakForce:
					joint.breakForce = _valueVar.GetValue(_floatValue);
					break;
				case FixedJointProperty.BreakTorque:
					joint.breakTorque = _valueVar.GetValue(_floatValue);
					break;
				case FixedJointProperty.ConnectedAnchor:
					joint.connectedAnchor = _valueVar.GetValue(_vector3Value);
					break;
				case FixedJointProperty.ConnectedBody:
					joint.connectedBody = (_valueVar.GetValueAsComponent(_rigidbodyValue, typeof(Rigidbody)) as Rigidbody);
					break;
				case FixedJointProperty.EnableCollision:
					joint.enableCollision = _valueVar.GetValue(_boolValue);
					break;
				case FixedJointProperty.EnablePreprocessing:
					joint.enablePreprocessing = _valueVar.GetValue(_boolValue);
					break;
				}
			}
			ConfigurableJoint configurableJoint = joint as ConfigurableJoint;
			if (configurableJoint != null)
			{
				switch (_configProperty)
				{
				case ConfigJointProperty.Anchor:
					joint.anchor = _valueVar.GetValue(_vector3Value);
					break;
				case ConfigJointProperty.AutoConfigureConnectedAnchor:
					joint.autoConfigureConnectedAnchor = _valueVar.GetValue(_boolValue);
					break;
				case ConfigJointProperty.Axis:
					joint.axis = _valueVar.GetValue(_vector3Value);
					break;
				case ConfigJointProperty.BreakForce:
					joint.breakForce = _valueVar.GetValue(_floatValue);
					break;
				case ConfigJointProperty.BreakTorque:
					joint.breakTorque = _valueVar.GetValue(_floatValue);
					break;
				case ConfigJointProperty.ConnectedAnchor:
					joint.connectedAnchor = _valueVar.GetValue(_vector3Value);
					break;
				case ConfigJointProperty.ConnectedBody:
					joint.connectedBody = (_valueVar.GetValueAsComponent(_rigidbodyValue, typeof(Rigidbody)) as Rigidbody);
					break;
				case ConfigJointProperty.EnableCollision:
					joint.enableCollision = _valueVar.GetValue(_boolValue);
					break;
				case ConfigJointProperty.EnablePreprocessing:
					joint.enablePreprocessing = _valueVar.GetValue(_boolValue);
					break;
				case ConfigJointProperty.AngularXDrive_MaximumForce:
				{
					JointDrive zDrive = configurableJoint.angularXDrive;
					zDrive.maximumForce = _valueVar.GetValue(_floatValue);
					configurableJoint.angularXDrive = zDrive;
					break;
				}
				case ConfigJointProperty.AngularXDrive_PositionDamper:
				{
					JointDrive zDrive = configurableJoint.angularXDrive;
					zDrive.positionDamper = _valueVar.GetValue(_floatValue);
					configurableJoint.angularXDrive = zDrive;
					break;
				}
				case ConfigJointProperty.AngularXDrive_PositionSpring:
				{
					JointDrive zDrive = configurableJoint.angularXDrive;
					zDrive.positionSpring = _valueVar.GetValue(_floatValue);
					configurableJoint.angularXDrive = zDrive;
					break;
				}
				case ConfigJointProperty.AngularXLimitSpring_Damper:
				{
					SoftJointLimitSpring twistLimitSpring = configurableJoint.angularXLimitSpring;
					twistLimitSpring.damper = _valueVar.GetValue(_floatValue);
					configurableJoint.angularXLimitSpring = twistLimitSpring;
					break;
				}
				case ConfigJointProperty.AngularXLimitSpring_Spring:
				{
					SoftJointLimitSpring twistLimitSpring = configurableJoint.angularXLimitSpring;
					twistLimitSpring.spring = _valueVar.GetValue(_floatValue);
					configurableJoint.angularXLimitSpring = twistLimitSpring;
					break;
				}
				case ConfigJointProperty.AngularXMotion:
					configurableJoint.angularXMotion = (ConfigurableJointMotion)_valueVar.GetValue(_floatValue);
					break;
				case ConfigJointProperty.AngularYLimit_Bounciness:
				{
					SoftJointLimit swing2Limit = configurableJoint.angularYLimit;
					swing2Limit.bounciness = _valueVar.GetValue(_floatValue);
					configurableJoint.angularYLimit = swing2Limit;
					break;
				}
				case ConfigJointProperty.AngularYLimit_ContactDistance:
				{
					SoftJointLimit swing2Limit = configurableJoint.angularYLimit;
					swing2Limit.contactDistance = _valueVar.GetValue(_floatValue);
					configurableJoint.angularYLimit = swing2Limit;
					break;
				}
				case ConfigJointProperty.AngularYLimit_Limit:
				{
					SoftJointLimit swing2Limit = configurableJoint.angularYLimit;
					swing2Limit.limit = _valueVar.GetValue(_floatValue);
					configurableJoint.angularYLimit = swing2Limit;
					break;
				}
				case ConfigJointProperty.AngularYMotion:
					configurableJoint.angularYMotion = (ConfigurableJointMotion)(object)_valueVar.GetValue(_motionValue);
					break;
				case ConfigJointProperty.AngularYZDrive_MaximumForce:
				{
					JointDrive zDrive = configurableJoint.angularYZDrive;
					zDrive.maximumForce = _valueVar.GetValue(_floatValue);
					configurableJoint.angularYZDrive = zDrive;
					break;
				}
				case ConfigJointProperty.AngularYZDrive_PositionDamper:
				{
					JointDrive zDrive = configurableJoint.angularYZDrive;
					zDrive.positionDamper = _valueVar.GetValue(_floatValue);
					configurableJoint.angularYZDrive = zDrive;
					break;
				}
				case ConfigJointProperty.AngularYZDrive_PositionSpring:
				{
					JointDrive zDrive = configurableJoint.angularYZDrive;
					zDrive.positionSpring = _valueVar.GetValue(_floatValue);
					configurableJoint.angularYZDrive = zDrive;
					break;
				}
				case ConfigJointProperty.AngularYZLimitSpring_Damper:
				{
					SoftJointLimitSpring twistLimitSpring = configurableJoint.angularYZLimitSpring;
					twistLimitSpring.damper = _valueVar.GetValue(_floatValue);
					configurableJoint.angularYZLimitSpring = twistLimitSpring;
					break;
				}
				case ConfigJointProperty.AngularYZLimitSpring_Spring:
				{
					SoftJointLimitSpring twistLimitSpring = configurableJoint.angularYZLimitSpring;
					twistLimitSpring.spring = _valueVar.GetValue(_floatValue);
					configurableJoint.angularYZLimitSpring = twistLimitSpring;
					break;
				}
				case ConfigJointProperty.AngularZLimit_Bounciness:
				{
					SoftJointLimit swing2Limit = configurableJoint.angularZLimit;
					swing2Limit.bounciness = _valueVar.GetValue(_floatValue);
					configurableJoint.angularZLimit = swing2Limit;
					break;
				}
				case ConfigJointProperty.AngularZLimit_ContactDistance:
				{
					SoftJointLimit swing2Limit = configurableJoint.angularZLimit;
					swing2Limit.contactDistance = _valueVar.GetValue(_floatValue);
					configurableJoint.angularZLimit = swing2Limit;
					break;
				}
				case ConfigJointProperty.AngularZLimit_Limit:
				{
					SoftJointLimit swing2Limit = configurableJoint.angularZLimit;
					swing2Limit.limit = _valueVar.GetValue(_floatValue);
					configurableJoint.angularZLimit = swing2Limit;
					break;
				}
				case ConfigJointProperty.AngularZMotion:
					configurableJoint.angularZMotion = (ConfigurableJointMotion)(object)_valueVar.GetValue(_motionValue);
					break;
				case ConfigJointProperty.ConfiguredInWorldSpace:
					configurableJoint.configuredInWorldSpace = _valueVar.GetValue(_boolValue);
					break;
				case ConfigJointProperty.HighAngularXLimit_Bounciness:
				{
					SoftJointLimit swing2Limit = configurableJoint.highAngularXLimit;
					swing2Limit.bounciness = _valueVar.GetValue(_floatValue);
					configurableJoint.highAngularXLimit = swing2Limit;
					break;
				}
				case ConfigJointProperty.HighAngularXLimit_ContactDistance:
				{
					SoftJointLimit swing2Limit = configurableJoint.highAngularXLimit;
					swing2Limit.contactDistance = _valueVar.GetValue(_floatValue);
					configurableJoint.highAngularXLimit = swing2Limit;
					break;
				}
				case ConfigJointProperty.HighAngularXLimit_Limit:
				{
					SoftJointLimit swing2Limit = configurableJoint.highAngularXLimit;
					swing2Limit.limit = _valueVar.GetValue(_floatValue);
					configurableJoint.highAngularXLimit = swing2Limit;
					break;
				}
				case ConfigJointProperty.LinearLimit_Bounciness:
				{
					SoftJointLimit swing2Limit = configurableJoint.linearLimit;
					swing2Limit.bounciness = _valueVar.GetValue(_floatValue);
					configurableJoint.linearLimit = swing2Limit;
					break;
				}
				case ConfigJointProperty.LinearLimit_ContactDistance:
				{
					SoftJointLimit swing2Limit = configurableJoint.linearLimit;
					swing2Limit.contactDistance = _valueVar.GetValue(_floatValue);
					configurableJoint.linearLimit = swing2Limit;
					break;
				}
				case ConfigJointProperty.LinearLimit_Limit:
				{
					SoftJointLimit swing2Limit = configurableJoint.linearLimit;
					swing2Limit.limit = _valueVar.GetValue(_floatValue);
					configurableJoint.linearLimit = swing2Limit;
					break;
				}
				case ConfigJointProperty.LinearLimitSpring_Damper:
				{
					SoftJointLimitSpring twistLimitSpring = configurableJoint.linearLimitSpring;
					twistLimitSpring.damper = _valueVar.GetValue(_floatValue);
					configurableJoint.linearLimitSpring = twistLimitSpring;
					break;
				}
				case ConfigJointProperty.LinearLimitSpring_Spring:
				{
					SoftJointLimitSpring twistLimitSpring = configurableJoint.linearLimitSpring;
					twistLimitSpring.spring = _valueVar.GetValue(_floatValue);
					configurableJoint.linearLimitSpring = twistLimitSpring;
					break;
				}
				case ConfigJointProperty.LowAngularXLimit_Bounciness:
				{
					SoftJointLimit swing2Limit = configurableJoint.lowAngularXLimit;
					swing2Limit.bounciness = _valueVar.GetValue(_floatValue);
					configurableJoint.lowAngularXLimit = swing2Limit;
					break;
				}
				case ConfigJointProperty.LowAngularXLimit_ContactDistance:
				{
					SoftJointLimit swing2Limit = configurableJoint.lowAngularXLimit;
					swing2Limit.contactDistance = _valueVar.GetValue(_floatValue);
					configurableJoint.lowAngularXLimit = swing2Limit;
					break;
				}
				case ConfigJointProperty.LowAngularXLimit_Limit:
				{
					SoftJointLimit swing2Limit = configurableJoint.lowAngularXLimit;
					swing2Limit.limit = _valueVar.GetValue(_floatValue);
					configurableJoint.lowAngularXLimit = swing2Limit;
					break;
				}
				case ConfigJointProperty.ProjectionAngle:
					configurableJoint.projectionAngle = _valueVar.GetValue(_floatValue);
					break;
				case ConfigJointProperty.ProjectionDistance:
					configurableJoint.projectionDistance = _valueVar.GetValue(_floatValue);
					break;
				case ConfigJointProperty.ProjectionMode:
					configurableJoint.projectionMode = (JointProjectionMode)(object)_valueVar.GetValue(_jointProjectionModeValue);
					break;
				case ConfigJointProperty.RotationDriveMode:
					configurableJoint.rotationDriveMode = (RotationDriveMode)(object)_valueVar.GetValue(_rotationDriveModeValue);
					break;
				case ConfigJointProperty.SecondaryAxis:
					configurableJoint.secondaryAxis = _valueVar.GetValue(_vector3Value);
					break;
				case ConfigJointProperty.SlerpDrive_MaximumForce:
				{
					JointDrive zDrive = configurableJoint.slerpDrive;
					zDrive.maximumForce = _valueVar.GetValue(_floatValue);
					configurableJoint.slerpDrive = zDrive;
					break;
				}
				case ConfigJointProperty.SlerpDrive_PositionDamper:
				{
					JointDrive zDrive = configurableJoint.slerpDrive;
					zDrive.positionDamper = _valueVar.GetValue(_floatValue);
					configurableJoint.slerpDrive = zDrive;
					break;
				}
				case ConfigJointProperty.SlerpDrive_PositionSpring:
				{
					JointDrive zDrive = configurableJoint.slerpDrive;
					zDrive.positionSpring = _valueVar.GetValue(_floatValue);
					configurableJoint.slerpDrive = zDrive;
					break;
				}
				case ConfigJointProperty.SwapBodies:
					configurableJoint.swapBodies = _valueVar.GetValue(_boolValue);
					break;
				case ConfigJointProperty.TargetAngularVelocity:
					configurableJoint.targetAngularVelocity = _valueVar.GetValue(_vector3Value);
					break;
				case ConfigJointProperty.TargetPosition:
					configurableJoint.targetPosition = _valueVar.GetValue(_vector3Value);
					break;
				case ConfigJointProperty.TargetRotation:
					configurableJoint.targetRotation = _valueVar.GetValue(_quaternionValue);
					break;
				case ConfigJointProperty.TargetVelocity:
					configurableJoint.targetVelocity = _valueVar.GetValue(_vector3Value);
					break;
				case ConfigJointProperty.XDrive_MaximumForce:
				{
					JointDrive zDrive = configurableJoint.xDrive;
					zDrive.maximumForce = _valueVar.GetValue(_floatValue);
					configurableJoint.xDrive = zDrive;
					break;
				}
				case ConfigJointProperty.XDrive_PositionDamper:
				{
					JointDrive zDrive = configurableJoint.xDrive;
					zDrive.positionDamper = _valueVar.GetValue(_floatValue);
					configurableJoint.xDrive = zDrive;
					break;
				}
				case ConfigJointProperty.XDrive_PositionSpring:
				{
					JointDrive zDrive = configurableJoint.xDrive;
					zDrive.positionSpring = _valueVar.GetValue(_floatValue);
					configurableJoint.xDrive = zDrive;
					break;
				}
				case ConfigJointProperty.XMotion:
					configurableJoint.xMotion = (ConfigurableJointMotion)(object)_valueVar.GetValue(_motionValue);
					break;
				case ConfigJointProperty.YDrive_MaximumForce:
				{
					JointDrive zDrive = configurableJoint.yDrive;
					zDrive.maximumForce = _valueVar.GetValue(_floatValue);
					configurableJoint.yDrive = zDrive;
					break;
				}
				case ConfigJointProperty.YDrive_PositionDamper:
				{
					JointDrive zDrive = configurableJoint.yDrive;
					zDrive.positionDamper = _valueVar.GetValue(_floatValue);
					configurableJoint.yDrive = zDrive;
					break;
				}
				case ConfigJointProperty.YDrive_PositionSpring:
				{
					JointDrive zDrive = configurableJoint.yDrive;
					zDrive.positionSpring = _valueVar.GetValue(_floatValue);
					configurableJoint.yDrive = zDrive;
					break;
				}
				case ConfigJointProperty.YMotion:
					configurableJoint.yMotion = (ConfigurableJointMotion)(object)_valueVar.GetValue(_motionValue);
					break;
				case ConfigJointProperty.ZDrive_MaximumForce:
				{
					JointDrive zDrive = configurableJoint.zDrive;
					zDrive.maximumForce = _valueVar.GetValue(_floatValue);
					configurableJoint.zDrive = zDrive;
					break;
				}
				case ConfigJointProperty.ZDrive_PositionDamper:
				{
					JointDrive zDrive = configurableJoint.zDrive;
					zDrive.positionDamper = _valueVar.GetValue(_floatValue);
					configurableJoint.zDrive = zDrive;
					break;
				}
				case ConfigJointProperty.ZDrive_PositionSpring:
				{
					JointDrive zDrive = configurableJoint.zDrive;
					zDrive.positionSpring = _valueVar.GetValue(_floatValue);
					configurableJoint.zDrive = zDrive;
					break;
				}
				case ConfigJointProperty.ZMotion:
					configurableJoint.zMotion = (ConfigurableJointMotion)(object)_valueVar.GetValue(_motionValue);
					break;
				}
			}
		}
	}
}
