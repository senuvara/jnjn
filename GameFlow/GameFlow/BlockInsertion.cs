using System.Collections.Generic;
using UnityEngine;

namespace GameFlow
{
	public class BlockInsertion
	{
		public static Object target = null;

		public static int sectionIndex = 0;

		public static int index = -1;

		public static bool enabled = true;

		public static bool clipboard;

		public static bool setDirty = true;

		private static Stack<Object> _targetStack = new Stack<Object>();

		private static Stack<int> _sectionIndexStack = new Stack<int>();

		private static Stack<int> _indexStack = new Stack<int>();

		public static void Reset()
		{
			target = null;
			sectionIndex = 0;
			index = -1;
			enabled = true;
			setDirty = true;
		}

		public static void Push()
		{
			_targetStack.Push(target);
			_sectionIndexStack.Push(sectionIndex);
			_indexStack.Push(index);
		}

		public static void Pop()
		{
			target = _targetStack.Pop();
			sectionIndex = _sectionIndexStack.Pop();
			index = _indexStack.Pop();
		}
	}
}
