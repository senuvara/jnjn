using System;
using System.Collections.Generic;
using System.Reflection;
using UnityEngine;

namespace GameFlow
{
	[AddComponentMenu("")]
	[Help("en", "Executes the specified method using Reflection.", null)]
	[Help("es", "Ejecuta el método especificado usando Reflection.", null)]
	public class InvokeMethod : Action, IExecutableInEditor
	{
		[SerializeField]
		private Component _target;

		[SerializeField]
		private Variable _targetVar;

		[SerializeField]
		private string _methodName;

		[SerializeField]
		private int _methodIndex;

		[SerializeField]
		private List<string> _strings = new List<string>();

		[SerializeField]
		private List<int> _ints = new List<int>();

		[SerializeField]
		private List<float> _floats = new List<float>();

		[SerializeField]
		private List<Vector2> _vector2s = new List<Vector2>();

		[SerializeField]
		private List<Vector3> _vector3s = new List<Vector3>();

		[SerializeField]
		private List<Vector4> _vector4s = new List<Vector4>();

		[SerializeField]
		private List<Rect> _rects = new List<Rect>();

		[SerializeField]
		private List<Color> _colors = new List<Color>();

		[SerializeField]
		private List<UnityEngine.Object> _objects = new List<UnityEngine.Object>();

		[SerializeField]
		private List<AnimationCurve> _curves = new List<AnimationCurve>();

		[SerializeField]
		private List<Bounds> _bounds = new List<Bounds>();

		[SerializeField]
		private List<Quaternion> _quaternions = new List<Quaternion>();

		[SerializeField]
		private List<Variable> _parameters = new List<Variable>();

		[SerializeField]
		private Variable _output;

		private Component _lastTarget;

		private MethodInfo _method;

		private object[] _parameterValues;

		private Type[] _paramTypes;

		private DataType[] _paramDataTypes;

		public Component target => _targetVar.GetValue(_target) as Component;

		private void Awake()
		{
			_lastTarget = target;
			Refresh();
		}

		private void Refresh()
		{
			_method = null;
			MethodInfo[] methods = ReflectionUtils.GetMethods(_lastTarget);
			if (methods == null)
			{
				return;
			}
			for (int i = 0; i < methods.Length; i++)
			{
				ParameterInfo[] parameters = methods[i].GetParameters();
				string text = (parameters.Length <= 0) ? string.Empty : " ( ";
				if (parameters.Length > 0)
				{
					for (int j = 0; j < parameters.Length; j++)
					{
						string name = TypeUtils.GetName(parameters[j].ParameterType);
						text = text + name + ((j >= parameters.Length - 1) ? string.Empty : ", ");
					}
					text += " )";
				}
				if (_methodName == methods[i].Name + text)
				{
					_method = methods[i];
					break;
				}
			}
			if (_method == null)
			{
				return;
			}
			ParameterInfo[] parameters2 = _method.GetParameters();
			if (parameters2.Length > 0)
			{
				_parameterValues = new object[parameters2.Length];
				_paramTypes = new Type[parameters2.Length];
				_paramDataTypes = new DataType[parameters2.Length];
				for (int k = 0; k < parameters2.Length; k++)
				{
					_paramTypes[k] = parameters2[k].ParameterType;
					_paramDataTypes[k] = parameters2[k].ParameterType.ToDataType();
				}
			}
		}

		public override void Execute()
		{
			Component target = this.target;
			if (target != _lastTarget)
			{
				if (_lastTarget != null && !_lastTarget.GetType().IsAssignableFrom(target.GetType()))
				{
					return;
				}
				_lastTarget = target;
				Refresh();
			}
			if (_method == null)
			{
				return;
			}
			if (_strings.Count == 0)
			{
				for (int i = 0; i < _parameters.Count; i++)
				{
					_parameterValues[i] = ((!(_parameters[i] != null)) ? null : _parameters[i].ToSystemObject());
				}
			}
			else
			{
				for (int j = 0; j < _parameters.Count; j++)
				{
					switch (_paramDataTypes[j])
					{
					case DataType.String:
					case DataType.Tag:
						_parameterValues[j] = _parameters[j].GetValue(_strings[j]);
						break;
					case DataType.Integer:
					case DataType.Layer:
						_parameterValues[j] = _parameters[j].GetValue(_ints[j]);
						break;
					case DataType.Float:
						_parameterValues[j] = _parameters[j].GetValue(_floats[j]);
						break;
					case DataType.Boolean:
					case DataType.Toggle:
						_parameterValues[j] = (_parameters[j].GetValue(_ints[j]) != 0);
						break;
					case DataType.Vector2:
						_parameterValues[j] = _parameters[j].GetValue(_vector2s[j]);
						break;
					case DataType.Vector3:
						_parameterValues[j] = _parameters[j].GetValue(_vector3s[j]);
						break;
					case DataType.Vector4:
						_parameterValues[j] = _parameters[j].GetValue(_vector4s[j]);
						break;
					case DataType.Rect:
						_parameterValues[j] = _parameters[j].GetValue(_rects[j]);
						break;
					case DataType.Color:
						_parameterValues[j] = _parameters[j].GetValue(_colors[j]);
						break;
					case DataType.Object:
						_parameterValues[j] = _parameters[j].GetValue(_objects[j]);
						break;
					case DataType.Enum:
						_parameterValues[j] = _parameters[j].GetValue((Enum)Enum.ToObject(_paramTypes[j], Convert.ToInt32(_ints[j])));
						break;
					case DataType.AnimationCurve:
						_parameterValues[j] = _parameters[j].GetValue(_curves[j]);
						break;
					case DataType.Bounds:
						_parameterValues[j] = _parameters[j].GetValue(_bounds[j]);
						break;
					case DataType.Quaternion:
						_parameterValues[j] = _parameters[j].GetValue(_quaternions[j]);
						break;
					}
				}
			}
			Execute(_lastTarget, _method, _parameterValues, _output);
		}

		public static void Execute(Component target, MethodInfo method, object[] parameterValues, Variable output)
		{
			if (!(target == null) && method != null)
			{
				try
				{
					object obj = method.Invoke(target, parameterValues);
					if (!(output == null))
					{
						switch (output.dataType)
						{
						case DataType.String:
							output.SetValue(Convert.ToString(obj));
							break;
						case DataType.Tag:
							output.SetTagValue(Convert.ToString(obj));
							break;
						case DataType.Integer:
							output.SetValue(Convert.ToInt32(obj));
							break;
						case DataType.Layer:
							output.SetLayerValue(Convert.ToInt32(obj));
							break;
						case DataType.Float:
							output.SetValue((float)Convert.ToDouble(obj));
							break;
						case DataType.Boolean:
							output.SetValue(Convert.ToBoolean(obj));
							break;
						case DataType.Toggle:
							output.SetToggleValue(Convert.ToBoolean(obj));
							break;
						case DataType.Vector2:
							output.SetValue((Vector2)obj);
							break;
						case DataType.Vector3:
							output.SetValue((Vector3)obj);
							break;
						case DataType.Vector4:
							output.SetValue((Vector4)obj);
							break;
						case DataType.Rect:
							output.SetValue((Rect)obj);
							break;
						case DataType.Color:
							output.SetValue((Color)obj);
							break;
						case DataType.Object:
							output.SetValue((UnityEngine.Object)obj, output.type);
							break;
						case DataType.Enum:
							output.SetValue((Enum)Enum.ToObject(output.type, Convert.ToInt32(obj)));
							break;
						case DataType.AnimationCurve:
							output.SetValue((AnimationCurve)obj);
							break;
						case DataType.Bounds:
							output.SetValue((Bounds)obj);
							break;
						case DataType.Quaternion:
							output.SetValue((Quaternion)obj);
							break;
						}
					}
				}
				catch (Exception message)
				{
					ParameterInfo[] parameters = method.GetParameters();
					bool flag = false;
					for (int i = 0; i < parameters.Length; i++)
					{
						if (parameters[i].ParameterType != parameterValues[i].GetType())
						{
							flag = true;
							Log.Error("ERROR: Failed to convert Parameter \"" + parameters[i].Name.SplitCamelCase() + "\"   >>  Expected Type: " + TypeUtils.GetName(parameters[i].ParameterType) + "   >>  Current Type: " + TypeUtils.GetName(parameterValues[i].GetType()));
						}
					}
					if (!flag)
					{
						Debug.LogError(message);
					}
				}
			}
		}
	}
}
