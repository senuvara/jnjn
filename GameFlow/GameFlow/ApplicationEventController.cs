using UnityEngine;

namespace GameFlow
{
	[AddComponentMenu("")]
	[DisallowMultipleComponent]
	[Help("en", "Controls the application events.", null)]
	[Help("es", "Controla los eventos de aplicación.", null)]
	public class ApplicationEventController : EventController
	{
		[SerializeField]
		private bool _applicationInitEnabled = true;

		[SerializeField]
		private bool _applicationFocusEnabled = true;

		[SerializeField]
		private bool _applicationPauseEnabled = true;

		[SerializeField]
		private bool _applicationQuitEnabled = true;

		private static bool _initialized;

		public static void Init()
		{
			Plugin.controlGameObject.AddComponentOnlyOnce<ApplicationEventController>();
		}

		private void Awake()
		{
			base.enabled = ((base.enabled && _applicationInitEnabled) || _applicationFocusEnabled || _applicationPauseEnabled || _applicationQuitEnabled);
		}

		private void OnApplicationFocus(bool focusStatus)
		{
			if (base.enabled && _applicationFocusEnabled)
			{
				ApplicationFocusEvent applicationFocusEvent = new ApplicationFocusEvent(focusStatus);
				applicationFocusEvent.DispatchToAll();
			}
		}

		private void OnApplicationPause(bool pauseStatus)
		{
			if (base.enabled && _applicationPauseEnabled)
			{
				if (!pauseStatus && !_initialized)
				{
					FireApplicationInit();
					return;
				}
				ApplicationPauseEvent applicationPauseEvent = new ApplicationPauseEvent(pauseStatus);
				applicationPauseEvent.DispatchToAll();
			}
		}

		private void OnApplicationQuit()
		{
			if (base.enabled && _applicationQuitEnabled)
			{
				ApplicationQuitEvent applicationQuitEvent = new ApplicationQuitEvent();
				applicationQuitEvent.DispatchToAll();
			}
		}

		private void FireApplicationInit()
		{
			_initialized = true;
			ApplicationInitEvent applicationInitEvent = new ApplicationInitEvent();
			applicationInitEvent.DispatchToAll();
		}
	}
}
