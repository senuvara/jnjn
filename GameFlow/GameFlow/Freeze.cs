using UnityEngine;

namespace GameFlow
{
	[Help("es", "Congela el componente @UnityManual{Transform} especificado durante el tiempo especificado.", null)]
	[Help("en", "Freezes the specified @UnityManual{Transform} component during the specified time.", null)]
	[AddComponentMenu("")]
	public class Freeze : TimeAction
	{
		[SerializeField]
		private Transform _target;

		[SerializeField]
		private Variable _targetVar;

		[SerializeField]
		private bool _freezePosition = true;

		[SerializeField]
		private Variable _freezePositionVar;

		[SerializeField]
		private bool _freezeRotation = true;

		[SerializeField]
		private Variable _freezeRotationVar;

		[SerializeField]
		private bool _freezeScale = true;

		[SerializeField]
		private Variable _freezeScaleVar;

		private Transform _transform;

		private Vector3 startPosition;

		private Quaternion startRotation;

		private Vector3 startScale;

		private bool _finished;

		public Transform target => _targetVar.GetValueAsComponent(_target, typeof(Transform)) as Transform;

		public bool freezePosition => _freezePositionVar.GetValue(_freezePosition);

		public bool freezeRotation => _freezeRotationVar.GetValue(_freezeRotation);

		public bool freezeScale => _freezeScaleVar.GetValue(_freezeScale);

		protected override void OnReset()
		{
			base.OnReset();
			_target = base.gameObject.GetComponent<Transform>();
		}

		public override void FirstStep()
		{
			_finished = false;
			_transform = target;
			if (_transform == null)
			{
				Finish();
				_finished = true;
				return;
			}
			startPosition = _transform.position;
			startRotation = _transform.rotation;
			startScale = _transform.localScale;
			base.FirstStep();
		}

		public override void Step()
		{
			if (_transform == null)
			{
				_finished = true;
				return;
			}
			if (freezePosition)
			{
				_transform.position = startPosition;
			}
			if (freezeRotation)
			{
				_transform.rotation = startRotation;
			}
			if (freezeScale)
			{
				_transform.localScale = startScale;
			}
		}

		public override bool Finished()
		{
			return _finished || base.Finished();
		}
	}
}
