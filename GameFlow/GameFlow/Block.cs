using System;
using UnityEngine;
using UnityEngine.Serialization;

namespace GameFlow
{
	public abstract class Block : BaseBehaviour, IHidden
	{
		[SerializeField]
		[FormerlySerializedAs("foldout")]
		private bool _foldout = true;

		[HideInInspector]
		public bool enabledInEditor = true;

		[HideInInspector]
		public UnityEngine.Object container;

		[HideInInspector]
		public bool DoNotDestroyOrphan;

		[HideInInspector]
		public ScriptableObject editor;

		public bool collapsed => !_foldout;

		private void Reset()
		{
			HideInInspector();
			Disable();
			SetResetFlag();
			OnReset();
		}

		private void OnValidate()
		{
			HideInInspector();
		}

		public virtual Type GetPreferredContainerType()
		{
			return typeof(OnStart);
		}

		public void PerformReset()
		{
			OnReset();
		}

		public void Collapse()
		{
			_foldout = false;
		}

		public void Expand()
		{
			_foldout = true;
		}
	}
}
