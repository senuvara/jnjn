using UnityEngine;

namespace GameFlow
{
	[AddComponentMenu("")]
	[Help("es", "Devuelve el primer impacto resultado de lanzar el rayo (@GameFlow{Ray}) especificado.", null)]
	[Help("en", "Gets the first hit after casting the specified @GameFlow{Ray}.", null)]
	public class GetFirstRaycastHit : Action
	{
		public enum OutputType
		{
			Collider,
			GameObject,
			RaycastHit,
			Rigidbody,
			Transform
		}

		[SerializeField]
		private Ray _ray;

		[SerializeField]
		private Variable _rayVar;

		[SerializeField]
		private LayerMask _layers;

		[SerializeField]
		private OutputType _outputType = OutputType.GameObject;

		[SerializeField]
		private Variable _output;

		protected override void OnReset()
		{
			_layers = 65531;
		}

		public override void Execute()
		{
			if (!(_output == null))
			{
				_output.SetValue(Execute(_rayVar.GetValue(_ray) as Ray, _layers, _outputType));
			}
		}

		public static Object Execute(Ray ray, LayerMask layers, OutputType outputType)
		{
			if (ray == null)
			{
				return null;
			}
			UnityEngine.Ray ray2 = ray.ToUnityRay();
			UnityEngine.RaycastHit hitInfo = default(UnityEngine.RaycastHit);
			bool flag = Physics.Raycast(ray2, out hitInfo, ray.length, layers);
			switch (outputType)
			{
			case OutputType.Collider:
				return (!flag) ? null : hitInfo.collider;
			case OutputType.GameObject:
				return (!flag) ? null : hitInfo.collider.gameObject;
			case OutputType.RaycastHit:
				return RaycastHit.CreateInstance(hitInfo);
			case OutputType.Rigidbody:
				return (!flag) ? null : hitInfo.rigidbody;
			case OutputType.Transform:
				return (!flag) ? null : hitInfo.transform;
			default:
				return null;
			}
		}
	}
}
