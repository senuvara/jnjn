using UnityEngine;
using UnityEngine.UI;

namespace GameFlow
{
	[Help("en", "Get a property of the specified Scrollbar component.", null)]
	[AddComponentMenu("")]
	[Help("es", "Lee una propiedad del componente Scrollbar especificado.", null)]
	public class GetScrollbarProperty : Action, IExecutableInEditor
	{
		public enum Property
		{
			AnimationTriggers_DisabledTrigger,
			AnimationTriggers_HighlightedTrigger,
			AnimationTriggers_NormalTrigger,
			AnimationTriggers_PressedTrigger,
			Animator,
			Colors_ColorMultiplier,
			Colors_DisabledColor,
			Colors_FadeDuration,
			Colors_HighlightedColor,
			Colors_NormalColor,
			Colors_PressedColor,
			Direction,
			HandleRect,
			Image,
			Interactable,
			Navigation_Mode,
			Navigation_SelectOnDown,
			Navigation_SelectOnLeft,
			Navigation_SelectOnRight,
			Navigation_SelectOnUp,
			NumberOfSteps,
			Size,
			SpriteState_DisabledSprite,
			SpriteState_HighlightedSprite,
			SpriteState_PressedSprite,
			TargetGraphic,
			Transition,
			Value
		}

		[SerializeField]
		private Scrollbar _source;

		[SerializeField]
		private Variable _sourceVar;

		[SerializeField]
		private Property _property;

		[SerializeField]
		private Variable _output;

		protected override void OnReset()
		{
			_source = base.gameObject.GetComponent<Scrollbar>();
		}

		public override void Setup()
		{
		}

		public override void Execute()
		{
			Scrollbar scrollbar = _sourceVar.GetValueAsComponent(_source, typeof(Scrollbar)) as Scrollbar;
			if (!(scrollbar == null))
			{
				switch (_property)
				{
				case Property.AnimationTriggers_DisabledTrigger:
					_output.SetValue(scrollbar.animationTriggers.disabledTrigger);
					break;
				case Property.AnimationTriggers_HighlightedTrigger:
					_output.SetValue(scrollbar.animationTriggers.highlightedTrigger);
					break;
				case Property.AnimationTriggers_NormalTrigger:
					_output.SetValue(scrollbar.animationTriggers.normalTrigger);
					break;
				case Property.AnimationTriggers_PressedTrigger:
					_output.SetValue(scrollbar.animationTriggers.pressedTrigger);
					break;
				case Property.Animator:
					_output.SetValue(scrollbar.animator);
					break;
				case Property.Colors_ColorMultiplier:
					_output.SetValue(scrollbar.colors.colorMultiplier);
					break;
				case Property.Colors_DisabledColor:
					_output.SetValue(scrollbar.colors.disabledColor);
					break;
				case Property.Colors_FadeDuration:
					_output.SetValue(scrollbar.colors.fadeDuration);
					break;
				case Property.Colors_HighlightedColor:
					_output.SetValue(scrollbar.colors.highlightedColor);
					break;
				case Property.Colors_NormalColor:
					_output.SetValue(scrollbar.colors.normalColor);
					break;
				case Property.Colors_PressedColor:
					_output.SetValue(scrollbar.colors.pressedColor);
					break;
				case Property.Direction:
					_output.SetValue(scrollbar.direction);
					break;
				case Property.HandleRect:
					_output.SetValue(scrollbar.handleRect);
					break;
				case Property.Image:
					_output.SetValue(scrollbar.image);
					break;
				case Property.Interactable:
					_output.SetValue(scrollbar.interactable);
					break;
				case Property.Navigation_Mode:
					_output.SetValue(scrollbar.navigation.mode);
					break;
				case Property.Navigation_SelectOnDown:
					_output.SetValue(scrollbar.navigation.selectOnDown);
					break;
				case Property.Navigation_SelectOnLeft:
					_output.SetValue(scrollbar.navigation.selectOnLeft);
					break;
				case Property.Navigation_SelectOnRight:
					_output.SetValue(scrollbar.navigation.selectOnRight);
					break;
				case Property.Navigation_SelectOnUp:
					_output.SetValue(scrollbar.navigation.selectOnUp);
					break;
				case Property.NumberOfSteps:
					_output.SetValue(scrollbar.numberOfSteps);
					break;
				case Property.Size:
					_output.SetValue(scrollbar.size);
					break;
				case Property.SpriteState_DisabledSprite:
					_output.SetValue(scrollbar.spriteState.disabledSprite);
					break;
				case Property.SpriteState_HighlightedSprite:
					_output.SetValue(scrollbar.spriteState.highlightedSprite);
					break;
				case Property.SpriteState_PressedSprite:
					_output.SetValue(scrollbar.spriteState.pressedSprite);
					break;
				case Property.TargetGraphic:
					_output.SetValue(scrollbar.targetGraphic);
					break;
				case Property.Transition:
					_output.SetValue(scrollbar.transition);
					break;
				case Property.Value:
					_output.SetValue(scrollbar.value);
					break;
				}
			}
		}
	}
}
