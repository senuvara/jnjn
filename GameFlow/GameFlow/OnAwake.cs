using UnityEngine;

namespace GameFlow
{
	[AddComponentMenu("GameFlow/Programs/On Awake")]
	[DisallowMultipleComponent]
	[Help("en", "Program that will be executed once the @UnityManual{GameObject} is initialized.", null)]
	[Help("es", "Programa que se ejecutará una vez el @UnityManual{GameObject} sea inicializado.", null)]
	public class OnAwake : Program
	{
		private void Awake()
		{
			if (base.enabled)
			{
				ActionExecutor.proxy = null;
				Restart();
			}
		}

		private void Update()
		{
		}
	}
}
