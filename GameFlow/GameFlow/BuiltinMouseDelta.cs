using System;
using UnityEngine;

namespace GameFlow
{
	[AddComponentMenu("")]
	[Help("es", "Una @GameFlow{Variable} integrada que devuelve el delta (movimiento relativo) de la posición del ratón.", null)]
	[Help("en", "A built-in @GameFlow{Variable} that returns the delta (relative movement) position of the mouse cursor.", null)]
	public class BuiltinMouseDelta : BuiltinVariable
	{
		public override Vector2 vector2Value => (!Application.isPlaying) ? Vector2.zero : MouseDeltaController.delta;

		public override Type GetVariableType()
		{
			return typeof(Vector2);
		}

		protected override string GetVariableId()
		{
			return "Mouse Delta";
		}

		[RuntimeInitializeOnLoadMethod]
		private static void Init()
		{
			Plugin.controlGameObject.AddComponentOnlyOnce<MouseDeltaController>();
		}
	}
}
