using UnityEngine;

namespace GameFlow
{
	[AddComponentMenu("")]
	[Help("en", "Modifies the values of a property of the specified @UnityManual{GameObject}.", null)]
	[Help("es", "Modifica el valor de una propiedad del @UnityManual{GameObject} especificado.", null)]
	public class SetGameObjectProperty : Action, IExecutableInEditor
	{
		public enum Property
		{
			Active,
			Layer,
			Name,
			Tag
		}

		[SerializeField]
		private GameObject _target;

		[SerializeField]
		private Variable _targetVar;

		[SerializeField]
		private Property _property;

		[SerializeField]
		private bool _activeValue = true;

		[SerializeField]
		private int _layerValue;

		[SerializeField]
		private string _nameValue = "GameObject";

		[SerializeField]
		private string _tagValue = "Untagged";

		[SerializeField]
		private Variable _valueVar;

		public override void Setup()
		{
			OnActivate.RegisterAllInactiveAsListeners();
		}

		public override void Execute()
		{
			GameObject gameObject = _targetVar.GetValue(_target) as GameObject;
			if (!(gameObject == null))
			{
				switch (_property)
				{
				case Property.Active:
					gameObject.SetActive(_valueVar.GetValue(_activeValue));
					break;
				case Property.Layer:
					gameObject.layer = _valueVar.GetValue(_layerValue);
					break;
				case Property.Name:
					gameObject.name = _valueVar.GetValue(_nameValue);
					break;
				case Property.Tag:
					gameObject.tag = _valueVar.GetValue(_tagValue);
					break;
				}
			}
		}
	}
}
