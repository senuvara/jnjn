using UnityEngine;

namespace GameFlow
{
	[Help("es", "Programa que se ejecutará cada vez que un @UnityManual{GameObject} en el rango de escucha sea activado.", null)]
	[DisallowMultipleComponent]
	[AddComponentMenu("GameFlow/Programs/On Activate")]
	[Help("en", "Program that will be executed every time a @UnityManual{GameObject} in the listening range gets activated.", null)]
	public class OnActivate : OnActivationProgram
	{
		private EventController[] _controllers;

		public static void RegisterAllInactiveAsListeners()
		{
			OnActivate[] array = Resources.FindObjectsOfTypeAll<OnActivate>();
			OnActivate[] array2 = array;
			foreach (OnActivate onActivate in array2)
			{
				if (!onActivate.gameObject.activeSelf)
				{
					onActivate.RegisterAsListener();
				}
			}
		}

		public static void RegisterInactiveAsListener(GameObject gameObject)
		{
			if (!gameObject.activeSelf)
			{
				OnActivate component = gameObject.GetComponent<OnActivate>();
				if (component != null)
				{
					component.RegisterAsListener();
				}
			}
		}

		protected override void RegisterAsListener()
		{
			_controllers = RegisterAsListener(typeof(ActivationEvent), typeof(ActivationController), ActivationController.AddController);
			if (base.listeningRange == EventListeningRange.SceneHierarchy)
			{
				ActivationEvent.AddListener(this);
			}
		}

		protected override void SetParameters(EventObject e)
		{
			ActivationEvent activationEvent = e as ActivationEvent;
			base.sourceParam.SetValue(activationEvent.source);
		}

		protected override void UnRegisterAsListener()
		{
			ActivationEvent.RemoveListener(this);
			UnRegisterAsListener(typeof(ActivationEvent), typeof(ActivationController));
		}

		private void OnTransformChildrenChanged()
		{
			if (base.listeningRange == EventListeningRange.GameObjectHierarchy)
			{
				UnsubscribeFrom(_controllers, typeof(ActivationEvent));
				_controllers = EventController.GetControllersInHierarchyOf(typeof(ActivationController), base.listeningTarget, ActivationController.AddController);
				SubscribeTo(_controllers, typeof(ActivationEvent));
			}
		}
	}
}
