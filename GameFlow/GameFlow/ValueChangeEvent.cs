using System.Collections.Generic;
using UnityEngine;

namespace GameFlow
{
	public class ValueChangeEvent : UIEvent
	{
		private static List<IEventListener> listeners = new List<IEventListener>();

		public DataType valueDataType;

		public string stringValue;

		public bool toggleValue;

		public float floatValue;

		public ValueChangeEvent(GameObject source, string stringValue)
		{
			base.source = source;
			valueDataType = DataType.String;
			this.stringValue = stringValue;
		}

		public ValueChangeEvent(GameObject source, bool toggleValue)
		{
			base.source = source;
			valueDataType = DataType.Toggle;
			this.toggleValue = toggleValue;
		}

		public ValueChangeEvent(GameObject source, float floatValue)
		{
			base.source = source;
			valueDataType = DataType.Float;
			this.floatValue = floatValue;
		}

		public override void DispatchToAll()
		{
			EventDispatcher.DispatchEventToListeners(this, listeners);
		}

		public new static void AddListener(IEventListener listener)
		{
			listeners.AddOnlyOnce(listener);
		}

		public new static void RemoveListener(IEventListener listener)
		{
			listeners.Remove(listener);
		}
	}
}
