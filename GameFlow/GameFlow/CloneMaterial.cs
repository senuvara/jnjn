using UnityEngine;

namespace GameFlow
{
	[Help("en", "Creates a clone of the specified Material.", null)]
	[Help("es", "Crea un clon del Material especificado.", null)]
	[AddComponentMenu("")]
	public class CloneMaterial : Action
	{
		[SerializeField]
		private Material _source;

		[SerializeField]
		private Variable _sourceVar;

		[SerializeField]
		private Variable _output;

		public Material source => _sourceVar.GetValue(_source) as Material;

		public override void Execute()
		{
			Material value = Execute(source);
			_output.SetValue(value);
		}

		public static Material Execute(Material source)
		{
			if (source == null)
			{
				return null;
			}
			Material material = new Material(source);
			if (material != null)
			{
				string name = material.name;
				material.name = name + " (Clone" + material.GetInstanceID() + ")";
			}
			return material;
		}
	}
}
