using UnityEngine;

namespace GameFlow
{
	[Help("en", "Gets a property of the specified @GameFlow{Timer}.", null)]
	[Help("es", "Lee una propiedad del @GameFlow{Timer} especificado.", null)]
	[AddComponentMenu("")]
	public class GetTimerProperty : Action, IExecutableInEditor
	{
		public enum Property
		{
			AutoRestart,
			Duration,
			ElapsedTime,
			IgnorePause,
			IsExpired,
			IsStopped,
			Type
		}

		[SerializeField]
		private Timer _timer;

		[SerializeField]
		private Variable _timerVar;

		[SerializeField]
		private Property _property = Property.ElapsedTime;

		[SerializeField]
		private Variable _output;

		public override void Execute()
		{
			if (_output == null)
			{
				return;
			}
			Timer timer = _timerVar.GetValue(_timer) as Timer;
			if (!(timer == null))
			{
				switch (_property)
				{
				case Property.AutoRestart:
					_output.SetToggleValue(timer.autoRestart);
					break;
				case Property.Duration:
					_output.SetValue(timer.duration);
					break;
				case Property.ElapsedTime:
					_output.SetValue(timer.elapsedTime);
					break;
				case Property.IgnorePause:
					_output.SetValue(timer.ignorePause);
					break;
				case Property.IsExpired:
					_output.SetValue(timer.expired);
					break;
				case Property.IsStopped:
					_output.SetValue(timer.stopped);
					break;
				case Property.Type:
					_output.SetValue(timer.type);
					break;
				}
			}
		}
	}
}
