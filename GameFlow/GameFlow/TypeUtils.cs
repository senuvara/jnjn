using System;
using System.Collections.Generic;
using System.Reflection;

namespace GameFlow
{
	public static class TypeUtils
	{
		public static Type GetType(string typeName)
		{
			if (typeName == null || typeName.Trim().Length == 0)
			{
				return null;
			}
			Type type = Type.GetType(typeName);
			if (type != null)
			{
				return type;
			}
			Assembly[] assemblies = AppDomain.CurrentDomain.GetAssemblies();
			foreach (Assembly assembly in assemblies)
			{
				type = assembly.GetType(typeName);
				if (type != null)
				{
					return type;
				}
			}
			return null;
		}

		public static List<Type> GetTypesInNamespace(string nameSpace)
		{
			List<Type> list = new List<Type>();
			if (nameSpace == null)
			{
				return list;
			}
			Assembly[] assemblies = AppDomain.CurrentDomain.GetAssemblies();
			foreach (Assembly assembly in assemblies)
			{
				Type[] types = assembly.GetTypes();
				foreach (Type type in types)
				{
					if (type.Namespace != null && type.Namespace.Equals(nameSpace))
					{
						list.Add(type);
					}
				}
			}
			return list;
		}

		public static List<Type> GetTypesExtendingBaseTypeInAssembly(Type baseType, Assembly assembly)
		{
			List<Type> list = new List<Type>();
			Type[] types = assembly.GetTypes();
			foreach (Type type in types)
			{
				if (type != null && type != baseType && baseType.IsAssignableFrom(type))
				{
					list.Add(type);
				}
			}
			return list;
		}

		public static List<Type> GetEnumTypesInAssembly(Assembly assembly)
		{
			List<Type> list = new List<Type>();
			Type[] types = assembly.GetTypes();
			foreach (Type type in types)
			{
				if (type.IsEnum && type.IsVisible && !type.IsNested && !type.IsNestedPrivate && type.Namespace != null && !type.Namespace.Contains("Internal"))
				{
					list.Add(type);
				}
			}
			return list;
		}

		public static List<Type> GetEnumTypesInProjectScripts()
		{
			List<Type> list = new List<Type>();
			Assembly[] assemblies = AppDomain.CurrentDomain.GetAssemblies();
			foreach (Assembly assembly in assemblies)
			{
				if (!assembly.FullName.StartsWith("Assembly-") && assembly != Assembly.GetExecutingAssembly())
				{
					continue;
				}
				Type[] types = assembly.GetTypes();
				foreach (Type type in types)
				{
					if (type.IsEnum && !type.IsNested && !type.IsNestedPrivate)
					{
						list.Add(type);
					}
				}
			}
			return list;
		}

		public static string GetName(Type type)
		{
			if (type == null)
			{
				return string.Empty;
			}
			if (type == typeof(int) || type == typeof(uint) || type == typeof(ushort) || type == typeof(short) || type == typeof(uint) || type == typeof(int) || type == typeof(ulong) || type == typeof(long))
			{
				return "Integer";
			}
			if (type == typeof(float) || type == typeof(double) || type == typeof(float))
			{
				return "Float";
			}
			return type.Name;
		}

		public static bool IsAnyOf(this Type type, Type[] types)
		{
			if (type == null || types == null)
			{
				return false;
			}
			for (int i = 0; i < types.Length; i++)
			{
				if (type == types[i])
				{
					return true;
				}
			}
			return false;
		}
	}
}
