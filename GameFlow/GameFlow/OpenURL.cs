using UnityEngine;

namespace GameFlow
{
	[AddComponentMenu("")]
	[Help("en", "Opens the specified Url in the default browser.", null)]
	[Help("es", "Abre la Url especificada en el navegador por defecto.", null)]
	public class OpenURL : Action, IExecutableInEditor
	{
		[SerializeField]
		private string _url = "http://google.com";

		[SerializeField]
		private Variable _urlVar;

		protected string url => _urlVar.GetValue(_url).Expanded();

		public override void Execute()
		{
			Execute(url);
		}

		public static void Execute(string url)
		{
			if (url != null && url.Length != 0)
			{
				Application.OpenURL(url);
			}
		}
	}
}
