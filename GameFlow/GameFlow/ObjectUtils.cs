using UnityEngine;

namespace GameFlow
{
	public class ObjectUtils
	{
		public static bool IsHiddenInInspector(Object o)
		{
			return (o.hideFlags & HideFlags.HideInInspector) != 0;
		}

		public static bool IsHiddenInHierarchy(Object o)
		{
			return (o.hideFlags & HideFlags.HideInHierarchy) != 0;
		}

		public static bool IsEditable(Object o)
		{
			return (o.hideFlags & HideFlags.NotEditable) == 0;
		}

		public static void HideInInspector(Object o)
		{
			o.hideFlags |= HideFlags.HideInInspector;
		}

		public static void MakeEditable(Object o)
		{
			o.hideFlags &= ~HideFlags.NotEditable;
		}

		public static void MakeNotEditable(Object o)
		{
			o.hideFlags |= HideFlags.NotEditable;
		}
	}
}
