using System.Collections.Generic;
using UnityEngine;

namespace GameFlow
{
	[AddComponentMenu("")]
	[Help("en", "Shows all the @GameFlow{Force} components defined in the inspected @UnityManual{GameObject}.", null)]
	[DisallowMultipleComponent]
	[Help("es", "Muestra todos los componentes Fuerza (@GameFlow{Force}) definidos en el @UnityManual{GameObject}.", null)]
	public class Forces : Container, IBlockContainer, IData, IForceContainer
	{
		[SerializeField]
		private List<Force> _forces = new List<Force>();

		public List<Force> forces => _forces;

		public List<Force> GetForces()
		{
			return _forces;
		}

		public virtual void ForceAdded(Force force)
		{
		}

		public override List<Block> GetBlocks()
		{
			return new List<Block>(_forces.ToArray());
		}

		public override bool Contains(Block block)
		{
			Force force = block as Force;
			if (force != null)
			{
				return _forces.Contains(force);
			}
			return false;
		}

		private void OnDrawGizmos()
		{
			if (HierarchyUtils.SelectionContains(base.gameObject))
			{
				return;
			}
			for (int i = 0; i < forces.Count; i++)
			{
				Force force = forces[i];
				if (force != null && force.enabledInEditor)
				{
					DrawForce(force, force.unselectedColor);
				}
			}
		}

		private void OnDrawGizmosSelected()
		{
			for (int i = 0; i < forces.Count; i++)
			{
				Force force = forces[i];
				if (force != null && force.enabledInEditor)
				{
					DrawForce(force, force.color);
				}
			}
		}

		public static void DrawForce(Force force, Color color)
		{
			if (force == null || color.a == 0f)
			{
				return;
			}
			Vector3 direction = force.direction;
			if (!(direction == Vector3.zero))
			{
				Transform component = force.gameObject.GetComponent<Transform>();
				Quaternion rotation = Quaternion.LookRotation(direction);
				Vector3 position = component.position;
				Vector3 vector = position + direction.normalized * force.magnitude / force.scale;
				Vector3 vector2 = Vector3.zero;
				switch (force.anchor)
				{
				case Force.ForceAnchor.Middle:
					vector2 = (vector - position) / 2f;
					break;
				case Force.ForceAnchor.End:
					vector2 = vector - position;
					break;
				}
				position -= vector2;
				vector -= vector2;
				float handleSize = GizmosUtils.GetHandleSize(vector);
				Gizmos.color = color;
				Gizmos.DrawLine(position, vector - direction.normalized * handleSize * 0.23f);
				GizmosUtils.SetHandlesColor(color);
				GizmosUtils.DrawConeCap(0, vector - direction.normalized * handleSize * 0.13f, rotation, handleSize * 0.23f);
				DrawPickableArea(force);
			}
		}

		private static void DrawPickableArea(Force force)
		{
			if (force == null)
			{
				return;
			}
			Vector3 direction = force.direction;
			if (!(direction == Vector3.zero))
			{
				Transform component = force.gameObject.GetComponent<Transform>();
				float handleSize = GizmosUtils.GetHandleSize(component.position);
				Gizmos.matrix = component.localToWorldMatrix;
				Gizmos.color = Color.clear;
				Vector3 zero = Vector3.zero;
				Vector3 vector = component.TransformDirection(force.direction / force.scale);
				Vector3 vector2 = Vector3.zero;
				switch (force.anchor)
				{
				case Force.ForceAnchor.Middle:
					vector2 = (vector - zero) / 2f;
					break;
				case Force.ForceAnchor.End:
					vector2 = vector - zero;
					break;
				}
				zero -= vector2;
				vector -= vector2;
				Gizmos.DrawSphere(vector, handleSize * 0.26f);
			}
		}

		private static void DrawLabel(Force force, Color color)
		{
			Vector3 direction = force.direction;
			if (!(direction == Vector3.zero))
			{
				Vector3 position = force.GetComponent<Transform>().position;
				GizmosUtils.DrawLabel(position + direction / force.scale, force.id, color, 0f, -40f);
			}
		}
	}
}
