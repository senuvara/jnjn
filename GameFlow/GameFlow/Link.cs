using UnityEngine;

namespace GameFlow
{
	[Help("es", "Define un enlace con un GameObject.", null)]
	[Help("en", "Defines a link with a GameObject.", null)]
	[AddComponentMenu("GameFlow/Tools/Link")]
	public class Link : Block, IData
	{
		[SerializeField]
		private string _title = string.Empty;

		[SerializeField]
		private Variable _titleVar;

		[TextArea(2, 64)]
		[SerializeField]
		private string _description = string.Empty;

		[SerializeField]
		private Variable _descriptionVar;

		[SerializeField]
		private GameObject _target;

		public string title => _titleVar.GetValue(_title);

		public string description => _descriptionVar.GetValue(_description);

		public GameObject target => _target;

		protected override void OnReset()
		{
			SetContainer();
			if (_title == string.Empty)
			{
				_title = "Link" + Random.Range(1000, 9999);
			}
		}

		private void SetContainer()
		{
			if (container != null || !BlockInsertion.enabled || BlockInsertion.clipboard)
			{
				return;
			}
			ILinkContainer linkContainer = BlockInsertion.target as ILinkContainer;
			if (linkContainer == null)
			{
				linkContainer = base.gameObject.GetComponent<Overview>();
				if (linkContainer == null)
				{
					linkContainer = base.gameObject.AddComponent<Overview>();
				}
				BlockInsertion.index = -1;
			}
			if (BlockInsertion.index < 0 || BlockInsertion.index >= linkContainer.GetLinks().Count)
			{
				if (!linkContainer.Contains(this))
				{
					linkContainer.GetLinks().Add(this);
					linkContainer.LinkAdded(this);
					linkContainer.SetDirty(dirty: true);
				}
			}
			else
			{
				linkContainer.GetLinks().Insert(BlockInsertion.index, this);
				linkContainer.LinkAdded(this);
				linkContainer.SetDirty(dirty: true);
			}
			container = (linkContainer as Object);
			BlockInsertion.Reset();
		}

		private void OnValidate()
		{
			HideInInspector();
		}
	}
}
