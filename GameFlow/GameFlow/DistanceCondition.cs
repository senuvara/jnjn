using UnityEngine;

namespace GameFlow
{
	[Help("en", "Evaluates the distance between two objects.", null)]
	[Help("es", "Evalúa la distancia entre dos objetos.", null)]
	[AddComponentMenu("")]
	public class DistanceCondition : Condition
	{
		public enum Comparison
		{
			IsEqualTo,
			IsNotEqualTo,
			IsGreaterThan,
			IsGreaterThanOrEqualTo,
			IsLessThan,
			IsLessThanOrEqualTo
		}

		[SerializeField]
		private Transform _t1;

		[SerializeField]
		private Variable _t1Var;

		[SerializeField]
		private Transform _t2;

		[SerializeField]
		private Variable _t2Var;

		[SerializeField]
		private float _floatValue;

		[SerializeField]
		private Variable _floatValueVar;

		[SerializeField]
		private Comparison _comparison;

		public override bool Evaluate()
		{
			Transform transform = _t1Var.GetValueAsComponent(_t1, typeof(Transform)) as Transform;
			Transform transform2 = _t2Var.GetValueAsComponent(_t2, typeof(Transform)) as Transform;
			if (transform == null || transform2 == null)
			{
				return false;
			}
			float num = Vector3.Distance(transform.position, transform2.position);
			float value = _floatValueVar.GetValue(_floatValue);
			switch (_comparison)
			{
			case Comparison.IsEqualTo:
				return Mathf.Approximately(num, value);
			case Comparison.IsNotEqualTo:
				return !Mathf.Approximately(num, value);
			case Comparison.IsGreaterThan:
				return num > value;
			case Comparison.IsGreaterThanOrEqualTo:
				return num >= value;
			case Comparison.IsLessThan:
				return num < value;
			case Comparison.IsLessThanOrEqualTo:
				return num <= value;
			default:
				return false;
			}
		}
	}
}
