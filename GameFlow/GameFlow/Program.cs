using System;
using System.Collections.Generic;
using UnityEngine;

namespace GameFlow
{
	[Help("en", "Defines a sequence of actions that will stay idle until explicitly executed with the @GameFlow{ExecuteProgram} action.", null)]
	[AddComponentMenu("GameFlow/Programs/Program")]
	[Help("es", "Define una secuencia de acciones que se mantendrá inactiva hasta que sea explícitamente ejecutada por medio de una acción @GameFlow{ExecuteProgram}.", null)]
	public class Program : Container, IActionContainer, IBlockContainer, IEventDispatcher, IIdentifiable, IInspectionable, IParameterContainer
	{
		[SerializeField]
		private string _id = string.Empty;

		[SerializeField]
		private List<Action> _actions = new List<Action>();

		[SerializeField]
		private List<Parameter> _parameters = new List<Parameter>();

		[SerializeField]
		private bool _showParameters;

		[SerializeField]
		private bool _actionsFoldout = true;

		private ActionSequence sequence;

		private List<IEventListener> finishListeners = new List<IEventListener>();

		public string id => _id;

		public bool finished
		{
			get;
			private set;
		}

		public bool running
		{
			get;
			private set;
		}

		public bool yieldBroken
		{
			get;
			private set;
		}

		public bool ignorePause
		{
			get;
			set;
		}

		protected override void OnReset()
		{
			if (_id == string.Empty)
			{
				_id = GetDefaultId();
			}
		}

		public virtual string GetDefaultId()
		{
			return "Program" + UnityEngine.Random.Range(1000, 9999);
		}

		private void Awake()
		{
			ActionExecutor.proxy = null;
			if (GetType() == typeof(Program) || GetType() == typeof(Macro))
			{
				base.enabled = false;
			}
			OnAwake();
		}

		public void Restart()
		{
			if (sequence == null || !Application.isPlaying)
			{
				sequence = new ActionSequence(_actions);
			}
			sequence.FirstEnabledAction();
			finished = sequence.Finished();
			if (!finished)
			{
				Step(ignorePause);
			}
		}

		private void Update()
		{
			Step(ignorePause);
		}

		public void Execute()
		{
			base.enabled = true;
			Restart();
		}

		public virtual void Step(bool ignorePause = false)
		{
			if ((!ignorePause && (Game.paused || Environment.IsEditorPaused())) || sequence == null)
			{
				return;
			}
			if (sequence.Finished())
			{
				finished = true;
			}
			if (!finished)
			{
				running = true;
				yieldBroken = false;
				bool flag = ExecuteCurrentAction();
				while (!flag)
				{
					flag = ExecuteCurrentAction();
				}
			}
		}

		private bool ExecuteCurrentAction()
		{
			bool flag = true;
			Action currentAction = sequence.GetCurrentAction();
			while (currentAction == null || !currentAction.enabledInEditor)
			{
				sequence.NextEnabledAction();
				if (sequence.Finished())
				{
					Finish();
					return flag;
				}
				currentAction = sequence.GetCurrentAction();
			}
			sequence.ExecuteCurrentAction();
			if (ActionExecutor.IsActionFinished(currentAction))
			{
				sequence.NextEnabledAction();
				if (finished)
				{
					return flag;
				}
				if (sequence.Finished())
				{
					Finish();
					return flag;
				}
				if (ActionExecutor.DoesActionYield(currentAction))
				{
					Yield();
					return flag;
				}
				return !flag;
			}
			return flag;
		}

		private void CheckFinished()
		{
			if (!finished && sequence.Finished())
			{
				Finish();
			}
		}

		public void Finish()
		{
			if (!finished)
			{
				finished = true;
				running = false;
				if (WillFireEventOnProgramFinished())
				{
					ProgramFinishEvent programFinishEvent = new ProgramFinishEvent(this);
					programFinishEvent.DispatchTo(finishListeners);
					programFinishEvent.DispatchToAll();
				}
			}
			if (GetType() == typeof(Program))
			{
				base.enabled = false;
			}
		}

		public virtual void Break()
		{
			Finish();
		}

		public virtual void Yield()
		{
			yieldBroken = true;
		}

		public override List<Block> GetBlocks()
		{
			return new List<Block>(_actions.ToArray());
		}

		public virtual List<Action> GetActions(int sectionIndex = 0)
		{
			return _actions;
		}

		public virtual int GetActionCount(int sectionIndex = 0)
		{
			int num = _actions.Count;
			foreach (Action action in _actions)
			{
				if (action is IActionContainer)
				{
					num += ActionUtils.GetActionCount(action as IActionContainer);
				}
			}
			return num;
		}

		public virtual int GetActionSectionsCount()
		{
			return 1;
		}

		public virtual void ActionAdded(Action action, int sectionIndex = 0)
		{
		}

		protected virtual bool WillFireEventOnProgramFinished()
		{
			return true;
		}

		public virtual List<Parameter> GetParameters()
		{
			return _parameters;
		}

		public virtual void ParameterAdded(Parameter parameter)
		{
		}

		public Parameter GetParameter(string id)
		{
			foreach (Parameter parameter in GetParameters())
			{
				if (parameter != null && parameter.id.Equals(id))
				{
					return parameter;
				}
			}
			return null;
		}

		public override bool Contains(Block block)
		{
			Action action = block as Action;
			if (action != null)
			{
				return _actions.Contains(action);
			}
			return false;
		}

		public void AddListener(IEventListener listener, Type eventType)
		{
			if (eventType == typeof(ProgramFinishEvent) && !finishListeners.Contains(listener))
			{
				finishListeners.Add(listener);
			}
		}

		public void RemoveListener(IEventListener listener, Type eventType)
		{
			if (eventType == typeof(ProgramFinishEvent) && finishListeners.Contains(listener))
			{
				finishListeners.Remove(listener);
			}
		}

		public virtual string GetIdForControls()
		{
			return (GetType() != typeof(Program)) ? GetType().Name.SplitCamelCase() : id;
		}

		public virtual string GetIdForSelectors()
		{
			return GetIdForControls();
		}

		public virtual string GetTypeForSelectors()
		{
			return "Program";
		}
	}
}
