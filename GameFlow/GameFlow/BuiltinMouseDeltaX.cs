using System;
using UnityEngine;

namespace GameFlow
{
	[Help("en", "A built-in @GameFlow{Variable} that returns the delta (relative movement) for the X coordinate of the mouse.", null)]
	[Help("es", "Una @GameFlow{Variable} integrada que devuelve el valor delta (movimiento relativo) de la coordenada X del raton.", null)]
	[AddComponentMenu("")]
	public class BuiltinMouseDeltaX : BuiltinVariable
	{
		public override float floatValue => (!Application.isPlaying) ? 0f : MouseDeltaController.delta.x;

		public override Type GetVariableType()
		{
			return typeof(float);
		}

		protected override string GetVariableId()
		{
			return "Mouse Delta X";
		}

		[RuntimeInitializeOnLoadMethod]
		private static void Init()
		{
			Plugin.controlGameObject.AddComponentOnlyOnce<MouseDeltaController>();
		}
	}
}
