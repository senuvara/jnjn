using UnityEngine;

namespace GameFlow
{
	[Help("en", "Program that will be executed when the game gets resume after a notification from @GameFlow{ResumeGame}.", null)]
	[DisallowMultipleComponent]
	[AddComponentMenu("GameFlow/Programs/On Game Resume")]
	[Help("es", "Programa que se ejecutará cuando el juego se reanude tras una notificación por parte de @GameFlow{ResumeGame}.", null)]
	public class OnGameResume : EventProgram
	{
		protected override void RegisterAsListener()
		{
			GameResumeEvent.AddListener(this);
		}

		protected override void UnRegisterAsListener()
		{
			GameResumeEvent.RemoveListener(this);
		}
	}
}
