using UnityEngine;

namespace GameFlow
{
	public static class GUITextExt
	{
		public static Vector2 GetTextSize(this GUIText guiText)
		{
			if (guiText == null)
			{
				return Vector2.zero;
			}
			GUIStyle gUIStyle = new GUIStyle();
			gUIStyle.font = guiText.font;
			gUIStyle.fontSize = guiText.fontSize;
			gUIStyle.fontStyle = guiText.fontStyle;
			return gUIStyle.CalcSize(new GUIContent(guiText.text));
		}
	}
}
