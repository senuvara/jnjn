using UnityEngine;

namespace GameFlow
{
	[AddComponentMenu("")]
	[Help("en", "Gets a property of the specified @UnityManual{GameObject}.", null)]
	[Help("es", "Lee una propiedad del @UnityManual{GameObject} especificado.", null)]
	public class GetGameObjectProperty : Action, IExecutableInEditor
	{
		public enum Property
		{
			Active,
			InstanceID,
			IsStatic,
			Layer,
			Name,
			Tag,
			Transform
		}

		[SerializeField]
		private GameObject _target;

		[SerializeField]
		private Variable _targetVar;

		[SerializeField]
		private Property _property;

		[SerializeField]
		private Variable _output;

		public Property property => _property;

		protected override void OnReset()
		{
		}

		public override void Setup()
		{
		}

		public override void Execute()
		{
			if (_output == null)
			{
				return;
			}
			GameObject gameObject = _targetVar.GetValue(_target) as GameObject;
			if (!(gameObject == null))
			{
				switch (_property)
				{
				case Property.Active:
					_output.SetToggleValue(gameObject.activeInHierarchy);
					break;
				case Property.InstanceID:
					_output.SetValue(gameObject.GetInstanceID());
					break;
				case Property.IsStatic:
					_output.SetToggleValue(gameObject.isStatic);
					break;
				case Property.Layer:
					_output.SetLayerValue(gameObject.layer);
					break;
				case Property.Name:
					_output.SetValue(gameObject.name);
					break;
				case Property.Tag:
					_output.SetTagValue(gameObject.tag);
					break;
				case Property.Transform:
					_output.SetValue(gameObject.GetComponent<Transform>());
					break;
				}
			}
		}
	}
}
