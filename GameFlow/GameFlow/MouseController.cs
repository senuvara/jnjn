using System;
using System.Collections.Generic;
using UnityEngine;

namespace GameFlow
{
	[Help("es", "Controla los eventos de raton en este @UnityManual{GameObject}.", null)]
	[DisallowMultipleComponent]
	[AddComponentMenu("")]
	[Help("en", "Controls the mouse events in this @UnityManual{GameObject}.", null)]
	public class MouseController : EventController
	{
		[SerializeField]
		private bool _mouseDownEnabled = true;

		[SerializeField]
		private bool _mouseDragEnabled = true;

		[SerializeField]
		private bool _mouseEnterEnabled = true;

		[SerializeField]
		private bool _mouseExitEnabled = true;

		[SerializeField]
		private bool _mouseOverEnabled = true;

		[SerializeField]
		private bool _mouseUpEnabled = true;

		private List<IEventListener> onMouseDownListeners = new List<IEventListener>();

		private List<IEventListener> onMouseDragListeners = new List<IEventListener>();

		private List<IEventListener> onMouseEnterListeners = new List<IEventListener>();

		private List<IEventListener> onMouseExitListeners = new List<IEventListener>();

		private List<IEventListener> onMouseOverListeners = new List<IEventListener>();

		private List<IEventListener> onMouseUpListeners = new List<IEventListener>();

		private static bool mouseDown;

		private static Vector2 lastPosition;

		public static EventController AddController(GameObject gameObject)
		{
			return (!(gameObject.GetComponent<GUIElement>() != null) && !(gameObject.GetComponent<Collider>() != null) && !(gameObject.GetComponent<Collider2D>() != null)) ? null : EventController.AddController(gameObject, typeof(MouseController));
		}

		private void Awake()
		{
			base.enabled = ((base.enabled && _mouseDownEnabled) || _mouseDragEnabled || _mouseEnterEnabled || _mouseExitEnabled || _mouseOverEnabled || _mouseUpEnabled);
		}

		public override void AddListener(IEventListener listener, Type eventType)
		{
			if (eventType == typeof(MouseDownEvent))
			{
				onMouseDownListeners.AddOnlyOnce(listener);
			}
			else if (eventType == typeof(MouseDragEvent))
			{
				onMouseDragListeners.AddOnlyOnce(listener);
			}
			else if (eventType == typeof(MouseEnterEvent))
			{
				onMouseEnterListeners.AddOnlyOnce(listener);
			}
			else if (eventType == typeof(MouseExitEvent))
			{
				onMouseExitListeners.AddOnlyOnce(listener);
			}
			else if (eventType == typeof(MouseOverEvent))
			{
				onMouseOverListeners.AddOnlyOnce(listener);
			}
			else if (eventType == typeof(MouseUpEvent))
			{
				onMouseUpListeners.AddOnlyOnce(listener);
			}
		}

		public override void RemoveListener(IEventListener listener, Type eventType)
		{
			if (eventType == typeof(MouseDownEvent))
			{
				onMouseDownListeners.Remove(listener);
			}
			else if (eventType == typeof(MouseDragEvent))
			{
				onMouseDragListeners.Remove(listener);
			}
			else if (eventType == typeof(MouseEnterEvent))
			{
				onMouseEnterListeners.Remove(listener);
			}
			else if (eventType == typeof(MouseExitEvent))
			{
				onMouseExitListeners.Remove(listener);
			}
			else if (eventType == typeof(MouseOverEvent))
			{
				onMouseOverListeners.Remove(listener);
			}
			else if (eventType == typeof(MouseUpEvent))
			{
				onMouseUpListeners.Remove(listener);
			}
		}

		private void OnMouseDown()
		{
			if (!mouseDown)
			{
				lastPosition = Input.mousePosition;
				mouseDown = true;
			}
			if (base.enabled && _mouseDownEnabled)
			{
				MouseDownEvent mouseDownEvent = new MouseDownEvent();
				InitMouseEvent(mouseDownEvent);
				mouseDownEvent.DispatchTo(onMouseDownListeners);
				mouseDownEvent.DispatchToAll();
			}
		}

		private void OnMouseDrag()
		{
			Vector2 delta = (Vector2)Input.mousePosition - lastPosition;
			lastPosition = Input.mousePosition;
			if (base.enabled && _mouseDragEnabled)
			{
				MouseDragEvent mouseDragEvent = new MouseDragEvent();
				InitMouseEvent(mouseDragEvent);
				mouseDragEvent.delta = delta;
				mouseDragEvent.DispatchTo(onMouseDragListeners);
				mouseDragEvent.DispatchToAll();
			}
		}

		private void OnMouseEnter()
		{
			if (base.enabled && _mouseEnterEnabled)
			{
				MouseEnterEvent mouseEnterEvent = new MouseEnterEvent();
				InitMouseEvent(mouseEnterEvent);
				mouseEnterEvent.DispatchTo(onMouseEnterListeners);
				mouseEnterEvent.DispatchToAll();
			}
		}

		private void OnMouseExit()
		{
			if (base.enabled && _mouseExitEnabled)
			{
				MouseExitEvent mouseExitEvent = new MouseExitEvent();
				InitMouseEvent(mouseExitEvent);
				mouseExitEvent.DispatchTo(onMouseExitListeners);
				mouseExitEvent.DispatchToAll();
			}
		}

		private void OnMouseOver()
		{
			if (base.enabled && _mouseOverEnabled)
			{
				MouseOverEvent mouseOverEvent = new MouseOverEvent();
				InitMouseEvent(mouseOverEvent);
				mouseOverEvent.DispatchTo(onMouseOverListeners);
				mouseOverEvent.DispatchToAll();
			}
		}

		private void OnMouseUp()
		{
			if (mouseDown)
			{
				mouseDown = false;
			}
			if (base.enabled && _mouseUpEnabled)
			{
				MouseUpEvent mouseUpEvent = new MouseUpEvent();
				InitMouseEvent(mouseUpEvent);
				mouseUpEvent.DispatchTo(onMouseUpListeners);
				mouseUpEvent.DispatchToAll();
			}
		}

		private void OnMouseUpAsButton()
		{
			if (base.enabled && _mouseUpEnabled)
			{
				MouseUpEvent mouseUpEvent = new MouseUpEvent();
				InitMouseEvent(mouseUpEvent);
				mouseUpEvent.asButton = true;
				mouseUpEvent.DispatchTo(onMouseUpListeners);
				mouseUpEvent.DispatchToAll();
			}
		}

		private void InitMouseEvent(MouseEvent me)
		{
			me.source = base.gameObject;
			me.position = Input.mousePosition;
		}
	}
}
