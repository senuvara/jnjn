namespace GameFlow
{
	public interface IYielder
	{
		void Yield();
	}
}
