using System.Collections.Generic;

namespace GameFlow
{
	public class ApplicationInitEvent : ApplicationEvent
	{
		private static List<IEventListener> listeners = new List<IEventListener>();

		public override void DispatchToAll()
		{
			EventDispatcher.DispatchEventToListeners(this, listeners);
		}

		public new static void AddListener(IEventListener listener)
		{
			listeners.AddOnlyOnce(listener);
		}

		public new static void RemoveListener(IEventListener listener)
		{
			listeners.Remove(listener);
		}
	}
}
