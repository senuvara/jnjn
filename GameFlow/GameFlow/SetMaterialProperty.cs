using UnityEngine;

namespace GameFlow
{
	[Help("en", "Sets a property of the specified Material.", null)]
	[AddComponentMenu("")]
	[Help("es", "Modifica una propiedad del Material especificado.", null)]
	public class SetMaterialProperty : Action, IExecutableInEditor
	{
		public enum Property
		{
			Color,
			GlobalIlluminationFlags,
			MainTexture,
			MainTextureOffset,
			MainTextureScale,
			NamedColor,
			NamedFloat,
			NamedInteger,
			NamedTag,
			NamedTexture,
			NamedTextureOffset,
			NamedTextureScale,
			NamedVector,
			RenderQueue,
			Shader
		}

		[SerializeField]
		private Material _target;

		[SerializeField]
		private Variable _targetVar;

		[SerializeField]
		private Property _property;

		[SerializeField]
		private string _propertyName;

		[SerializeField]
		private Variable _propertyNameVar;

		[SerializeField]
		private int _intValue;

		[SerializeField]
		private float _floatValue;

		[SerializeField]
		private Color _colorValue;

		[SerializeField]
		private Vector2 _vector2Value;

		[SerializeField]
		private Vector2 _vector4Value;

		[SerializeField]
		private string _stringValue;

		[SerializeField]
		private Texture _textureValue;

		[SerializeField]
		private Shader _shaderValue;

		[SerializeField]
		private int _flagsValue;

		[SerializeField]
		private string _textureName;

		[SerializeField]
		private Variable _textureNameVar;

		[SerializeField]
		private Variable _valueVar;

		public override void Execute()
		{
			Material material = _target;
			if (material == null)
			{
				material = (_targetVar.GetValue(_target) as Material);
			}
			if (material == null)
			{
				return;
			}
			switch (_property)
			{
			case Property.Color:
				material.color = _valueVar.GetValue(_colorValue);
				break;
			case Property.GlobalIlluminationFlags:
				material.globalIlluminationFlags = (MaterialGlobalIlluminationFlags)_valueVar.GetValue(_flagsValue);
				break;
			case Property.MainTexture:
				material.mainTexture = (_valueVar.GetValue(_textureValue) as Texture);
				break;
			case Property.MainTextureOffset:
				material.mainTextureOffset = _valueVar.GetValue(_vector2Value);
				break;
			case Property.MainTextureScale:
				material.mainTextureScale = _valueVar.GetValue(_vector2Value);
				break;
			case Property.NamedTexture:
			case Property.NamedTextureOffset:
			case Property.NamedTextureScale:
			{
				string value2 = _textureNameVar.GetValue(_textureName);
				if (value2 != null && value2.Length != 0)
				{
					switch (_property)
					{
					case Property.NamedTexture:
						material.SetTexture(value2, _valueVar.GetValue(_textureValue) as Texture);
						break;
					case Property.NamedTextureOffset:
						material.SetTextureOffset(value2, _valueVar.GetValue(_vector2Value));
						break;
					case Property.NamedTextureScale:
						material.SetTextureScale(value2, _valueVar.GetValue(_vector2Value));
						break;
					}
				}
				break;
			}
			case Property.RenderQueue:
				material.renderQueue = _valueVar.GetValue(_intValue);
				break;
			case Property.Shader:
				material.shader = (_valueVar.GetValue(_shaderValue) as Shader);
				break;
			case Property.NamedColor:
			case Property.NamedFloat:
			case Property.NamedInteger:
			case Property.NamedTag:
			case Property.NamedVector:
			{
				string value = _propertyNameVar.GetValue(_propertyName);
				if (value != null && value.Length != 0)
				{
					switch (_property)
					{
					case Property.NamedTexture:
					case Property.NamedTextureOffset:
					case Property.NamedTextureScale:
						break;
					case Property.NamedColor:
						material.SetColor(value, _valueVar.GetValue(_colorValue));
						break;
					case Property.NamedFloat:
						material.SetFloat(value, _valueVar.GetValue(_floatValue));
						break;
					case Property.NamedInteger:
						material.SetInt(value, _valueVar.GetValue(_intValue));
						break;
					case Property.NamedTag:
						material.SetOverrideTag(value, _valueVar.GetValue(_stringValue));
						break;
					case Property.NamedVector:
						material.SetVector(value, _valueVar.GetValue(_vector4Value));
						break;
					}
				}
				break;
			}
			}
		}
	}
}
