using System;
using UnityEngine;

namespace GameFlow
{
	[AddComponentMenu("")]
	[Help("en", "Gets the fractional part of the specified numeric value.", null)]
	[Help("es", "Devuelve la parte fraccional del valor numérico especificado.", null)]
	public class GetFractionalPart : Action, IExecutableInEditor
	{
		[SerializeField]
		private float _value;

		[SerializeField]
		private Variable _valueVar;

		[SerializeField]
		private Variable _output;

		[SerializeField]
		private int _decimals = 2;

		[SerializeField]
		private bool _asInt;

		public float value => _valueVar.GetValue(_value);

		public override void Execute()
		{
			if (_output != null)
			{
				float num = Execute(value, _decimals, _asInt);
				if (_asInt)
				{
					_output.SetValue((int)num);
				}
				else
				{
					_output.SetValue(num);
				}
			}
		}

		public static float Execute(float value, int decimals, bool asInt)
		{
			float num = value - ((!(value >= 0f)) ? Mathf.Ceil(value) : Mathf.Floor(value));
			num = (float)Math.Round(num, decimals);
			return (!asInt) ? num : (num * Mathf.Pow(10f, decimals));
		}
	}
}
