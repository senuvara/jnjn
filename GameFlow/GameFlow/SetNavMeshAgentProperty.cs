using UnityEngine;
using UnityEngine.AI;

namespace GameFlow
{
	[AddComponentMenu("")]
	[Help("es", "Modifica una propiedad del componente @UnityManual{NavMeshAgent} especificado.", null)]
	[Help("en", "Sets a property of the specified @UnityManual{NavMeshAgent}.", null)]
	public class SetNavMeshAgentProperty : Action, IExecutableInEditor
	{
		public enum Property
		{
			Acceleration,
			AngularSpeed,
			AreaMask,
			AutoBraking,
			AutoRepath,
			AutoTraverseOffMeshLink,
			AvoidancePriority,
			BaseOffset,
			Destination,
			Height,
			NextPosition,
			ObstacleAvoidanceType,
			Path,
			Radius,
			Speed,
			StoppingDistance,
			UpdatePosition,
			UpdateRotation,
			Velocity
		}

		[SerializeField]
		private NavMeshAgent _target;

		[SerializeField]
		private Variable _targetVar;

		[SerializeField]
		private Property _property;

		[SerializeField]
		private float _accelerationValue;

		[SerializeField]
		private float _angularSpeedValue;

		[SerializeField]
		private int _areaMaskValue;

		[SerializeField]
		private bool _autoBrakingValue;

		[SerializeField]
		private bool _autoRepathValue;

		[SerializeField]
		private bool _autoTraverseOffMeshLinkValue;

		[SerializeField]
		private int _avoidancePriorityValue;

		[SerializeField]
		private float _baseOffsetValue;

		[SerializeField]
		private Vector3 _destinationValue;

		[SerializeField]
		private float _heightValue;

		[SerializeField]
		private Vector3 _nextPositionValue;

		[SerializeField]
		private ObstacleAvoidanceType _obstacleAvoidanceTypeValue;

		[SerializeField]
		private float _radiusValue;

		[SerializeField]
		private float _speedValue;

		[SerializeField]
		private float _stoppingDistanceValue;

		[SerializeField]
		private bool _updatePositionValue;

		[SerializeField]
		private bool _updateRotationValue;

		[SerializeField]
		private Vector3 _velocityValue;

		[SerializeField]
		private Variable _valueVar;

		protected override void OnReset()
		{
			_target = base.gameObject.GetComponent<NavMeshAgent>();
		}

		public override void Setup()
		{
		}

		public override void Execute()
		{
			NavMeshAgent navMeshAgent = _targetVar.GetValueAsComponent(_target, typeof(NavMeshAgent)) as NavMeshAgent;
			if (!(navMeshAgent == null))
			{
				switch (_property)
				{
				case Property.Path:
					break;
				case Property.Acceleration:
					navMeshAgent.acceleration = _valueVar.GetValue(_accelerationValue);
					break;
				case Property.AngularSpeed:
					navMeshAgent.angularSpeed = _valueVar.GetValue(_angularSpeedValue);
					break;
				case Property.AreaMask:
					navMeshAgent.areaMask = _valueVar.GetValue(_areaMaskValue);
					break;
				case Property.AutoBraking:
					navMeshAgent.autoBraking = _valueVar.GetValue(_autoBrakingValue);
					break;
				case Property.AutoRepath:
					navMeshAgent.autoRepath = _valueVar.GetValue(_autoRepathValue);
					break;
				case Property.AutoTraverseOffMeshLink:
					navMeshAgent.autoTraverseOffMeshLink = _valueVar.GetValue(_autoTraverseOffMeshLinkValue);
					break;
				case Property.AvoidancePriority:
					navMeshAgent.avoidancePriority = _valueVar.GetValue(_avoidancePriorityValue);
					break;
				case Property.BaseOffset:
					navMeshAgent.baseOffset = _valueVar.GetValue(_baseOffsetValue);
					break;
				case Property.Destination:
					navMeshAgent.destination = _valueVar.GetValue(_destinationValue);
					break;
				case Property.Height:
					navMeshAgent.height = _valueVar.GetValue(_heightValue);
					break;
				case Property.NextPosition:
					navMeshAgent.nextPosition = _valueVar.GetValue(_nextPositionValue);
					break;
				case Property.ObstacleAvoidanceType:
					navMeshAgent.obstacleAvoidanceType = (ObstacleAvoidanceType)(object)_valueVar.GetValue(_obstacleAvoidanceTypeValue);
					break;
				case Property.Radius:
					navMeshAgent.radius = _valueVar.GetValue(_radiusValue);
					break;
				case Property.Speed:
					navMeshAgent.speed = _valueVar.GetValue(_speedValue);
					break;
				case Property.StoppingDistance:
					navMeshAgent.stoppingDistance = _valueVar.GetValue(_stoppingDistanceValue);
					break;
				case Property.UpdatePosition:
					navMeshAgent.updatePosition = _valueVar.GetValue(_updatePositionValue);
					break;
				case Property.UpdateRotation:
					navMeshAgent.updateRotation = _valueVar.GetValue(_updateRotationValue);
					break;
				case Property.Velocity:
					navMeshAgent.velocity = _valueVar.GetValue(_velocityValue);
					break;
				}
			}
		}
	}
}
