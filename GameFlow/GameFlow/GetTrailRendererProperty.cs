using UnityEngine;

namespace GameFlow
{
	[AddComponentMenu("")]
	[Help("en", "Gets a property of the specified @UnityManual{TrailRenderer} component.", null)]
	[Help("es", "Lee una propiedad del componente @UnityManual{TrailRenderer} especificado.", null)]
	public class GetTrailRendererProperty : Action, IExecutableInEditor
	{
		public enum Property
		{
			Autodestruct,
			Bounds,
			Enabled,
			EndWidth,
			IsPartOfStaticBatch,
			IsVisible,
			LightmapIndex,
			LightmapScaleOffset,
			LocalToWorldMatrix,
			Material,
			Materials,
			ProbeAnchor,
			RealtimeLightmapIndex,
			RealtimeLightmapScaleOffset,
			ReceiveShadows,
			ReflectionProbeUsage,
			ShadowCastingMode,
			SharedMaterial,
			SharedMaterials,
			SortingLayerID,
			SortingLayerName,
			SortingOrder,
			StartWidth,
			Time,
			LightProbeUsage,
			WorldToLocalMatrix
		}

		[SerializeField]
		private TrailRenderer _source;

		[SerializeField]
		private Variable _sourceVar;

		[SerializeField]
		private Property _property;

		[SerializeField]
		private Variable _output;

		protected override void OnReset()
		{
			_source = base.gameObject.GetComponent<TrailRenderer>();
		}

		public override void Setup()
		{
		}

		public override void Execute()
		{
			TrailRenderer trailRenderer = _sourceVar.GetValueAsComponent(_source, typeof(TrailRenderer)) as TrailRenderer;
			if (!(trailRenderer == null))
			{
				switch (_property)
				{
				case Property.LocalToWorldMatrix:
				case Property.Materials:
				case Property.SharedMaterials:
					break;
				case Property.Autodestruct:
					_output.SetValue(trailRenderer.autodestruct);
					break;
				case Property.Bounds:
					_output.SetValue(trailRenderer.bounds);
					break;
				case Property.Enabled:
					_output.SetValue(trailRenderer.enabled);
					break;
				case Property.EndWidth:
					_output.SetValue(trailRenderer.endWidth);
					break;
				case Property.IsPartOfStaticBatch:
					_output.SetValue(trailRenderer.isPartOfStaticBatch);
					break;
				case Property.IsVisible:
					_output.SetValue(trailRenderer.isVisible);
					break;
				case Property.LightmapIndex:
					_output.SetValue(trailRenderer.lightmapIndex);
					break;
				case Property.LightmapScaleOffset:
					_output.SetValue(trailRenderer.lightmapScaleOffset);
					break;
				case Property.Material:
					_output.SetValue(trailRenderer.material);
					break;
				case Property.ProbeAnchor:
					_output.SetValue(trailRenderer.probeAnchor);
					break;
				case Property.RealtimeLightmapIndex:
					_output.SetValue(trailRenderer.realtimeLightmapIndex);
					break;
				case Property.RealtimeLightmapScaleOffset:
					_output.SetValue(trailRenderer.realtimeLightmapScaleOffset);
					break;
				case Property.ReceiveShadows:
					_output.SetValue(trailRenderer.receiveShadows);
					break;
				case Property.ReflectionProbeUsage:
					_output.SetValue(trailRenderer.reflectionProbeUsage);
					break;
				case Property.ShadowCastingMode:
					_output.SetValue(trailRenderer.shadowCastingMode);
					break;
				case Property.SharedMaterial:
					_output.SetValue(trailRenderer.sharedMaterial);
					break;
				case Property.SortingLayerID:
					_output.SetValue(trailRenderer.sortingLayerID);
					break;
				case Property.SortingLayerName:
					_output.SetValue(trailRenderer.sortingLayerName);
					break;
				case Property.SortingOrder:
					_output.SetValue(trailRenderer.sortingOrder);
					break;
				case Property.StartWidth:
					_output.SetValue(trailRenderer.startWidth);
					break;
				case Property.Time:
					_output.SetValue(trailRenderer.time);
					break;
				case Property.LightProbeUsage:
					_output.SetValue(trailRenderer.lightProbeUsage);
					break;
				}
			}
		}
	}
}
