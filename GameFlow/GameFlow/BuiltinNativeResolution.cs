using System;
using UnityEngine;

namespace GameFlow
{
	[Help("en", "A built-in @GameFlow{Variable} that returns the native resolution of the current screen.", null)]
	[Help("es", "Una @GameFlow{Variable} integrada que devuelve la resolución nativa de la pantalla en uso.", null)]
	[AddComponentMenu("")]
	public class BuiltinNativeResolution : BuiltinVariable
	{
		public override Vector2 vector2Value => new Vector2(Screen.currentResolution.width, Screen.currentResolution.height);

		public override Type GetVariableType()
		{
			return typeof(Vector2);
		}

		protected override string GetVariableId()
		{
			return "Native Resolution";
		}
	}
}
