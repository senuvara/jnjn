using UnityEngine;
using UnityEngine.UI;

namespace GameFlow
{
	[Help("en", "Gets a property of the specified Image component.", null)]
	[Help("es", "Lee una propiedad del componente Image especificado.", null)]
	[AddComponentMenu("")]
	public class GetImageProperty : Action, IExecutableInEditor
	{
		public enum Property
		{
			Canvas,
			CanvasRenderer,
			Color,
			DefaultMaterial,
			Depth,
			AlphaHitTestMinimumThreshold,
			FillAmount,
			FillCenter,
			FillClockwise,
			FillMethod,
			FillOrigin,
			FlexibleHeight,
			FlexibleWidth,
			HasBorder,
			LayoutPriority,
			MainTexture,
			Maskable,
			Material,
			MaterialForRendering,
			MinHeight,
			MinWidth,
			OverrideSprite,
			PixelsPerUnit,
			PreferredHeight,
			PreferredWidth,
			PreserveAspect,
			RectTransform,
			Sprite,
			Type
		}

		[SerializeField]
		private Image _source;

		[SerializeField]
		private Variable _sourceVar;

		[SerializeField]
		private Property _property = Property.Color;

		[SerializeField]
		private Variable _output;

		protected override void OnReset()
		{
			_source = base.gameObject.GetComponent<Image>();
		}

		public override void Setup()
		{
		}

		public override void Execute()
		{
			Image image = _sourceVar.GetValueAsComponent(_source, typeof(Image)) as Image;
			if (!(image == null))
			{
				switch (_property)
				{
				case Property.Canvas:
					_output.SetValue(image.canvas);
					break;
				case Property.CanvasRenderer:
					_output.SetValue(image.canvasRenderer);
					break;
				case Property.Color:
					_output.SetValue(image.color);
					break;
				case Property.DefaultMaterial:
					_output.SetValue(image.defaultMaterial);
					break;
				case Property.Depth:
					_output.SetValue(image.depth);
					break;
				case Property.AlphaHitTestMinimumThreshold:
					_output.SetValue(image.alphaHitTestMinimumThreshold);
					break;
				case Property.FillAmount:
					_output.SetValue(image.fillAmount);
					break;
				case Property.FillCenter:
					_output.SetValue(image.fillCenter);
					break;
				case Property.FillClockwise:
					_output.SetValue(image.fillClockwise);
					break;
				case Property.FillMethod:
					_output.SetValue(image.fillMethod);
					break;
				case Property.FillOrigin:
					_output.SetValue(image.fillOrigin);
					break;
				case Property.FlexibleHeight:
					_output.SetValue(image.flexibleHeight);
					break;
				case Property.FlexibleWidth:
					_output.SetValue(image.flexibleWidth);
					break;
				case Property.HasBorder:
					_output.SetValue(image.hasBorder);
					break;
				case Property.LayoutPriority:
					_output.SetValue(image.layoutPriority);
					break;
				case Property.MainTexture:
					_output.SetValue(image.mainTexture);
					break;
				case Property.Maskable:
					_output.SetValue(image.maskable);
					break;
				case Property.Material:
					_output.SetValue(image.material);
					break;
				case Property.MaterialForRendering:
					_output.SetValue(image.materialForRendering);
					break;
				case Property.MinHeight:
					_output.SetValue(image.minHeight);
					break;
				case Property.MinWidth:
					_output.SetValue(image.minWidth);
					break;
				case Property.OverrideSprite:
					_output.SetValue(image.overrideSprite);
					break;
				case Property.PixelsPerUnit:
					_output.SetValue(image.pixelsPerUnit);
					break;
				case Property.PreferredHeight:
					_output.SetValue(image.preferredHeight);
					break;
				case Property.PreferredWidth:
					_output.SetValue(image.preferredWidth);
					break;
				case Property.PreserveAspect:
					_output.SetValue(image.preserveAspect);
					break;
				case Property.RectTransform:
					_output.SetValue(image.rectTransform);
					break;
				case Property.Sprite:
					_output.SetValue(image.sprite);
					break;
				case Property.Type:
					_output.SetValue(image.type);
					break;
				}
			}
		}
	}
}
