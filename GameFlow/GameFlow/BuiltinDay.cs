using System;
using UnityEngine;

namespace GameFlow
{
	[AddComponentMenu("")]
	[Help("en", "A built-in @GameFlow{Variable} that returns the day of the month for the current date.", null)]
	[Help("es", "Una @GameFlow{Variable} integrada que devuelve el día del mes de la fecha actual.", null)]
	public class BuiltinDay : BuiltinVariable
	{
		public override int intValue => DateTime.Now.Day;

		public override Type GetVariableType()
		{
			return typeof(int);
		}

		protected override string GetVariableId()
		{
			return "Day";
		}
	}
}
