using UnityEngine;

namespace GameFlow
{
	[Help("en", "Program that will be executed when an UI component in the listening range is deselected.", null)]
	[Help("es", "Programa que se ejecutará cuando un componente UI en el rango de escucha deje de estar seleccionado.", null)]
	[DisallowMultipleComponent]
	[AddComponentMenu("GameFlow/Programs/On Deselect")]
	public class OnDeselect : EventProgram
	{
		private Parameter _sourceParam;

		private EventController[] _controllers;

		public Parameter sourceParam
		{
			get
			{
				if (_sourceParam == null)
				{
					_sourceParam = GetParameter("Source");
				}
				return _sourceParam;
			}
		}

		protected override void CacheParameters()
		{
			CacheParameter(sourceParam);
		}

		protected override void RegisterAsListener()
		{
			_controllers = RegisterAsListener(typeof(DeselectEvent), typeof(UIEventController), UIEventController.AddController);
			if (base.listeningRange == EventListeningRange.SceneHierarchy)
			{
				DeselectEvent.AddListener(this);
			}
		}

		protected override void UnRegisterAsListener()
		{
			DeselectEvent.RemoveListener(this);
			UnRegisterAsListener(typeof(DeselectEvent), typeof(UIEventController));
		}

		protected override void SetParameters(EventObject e)
		{
			DeselectEvent deselectEvent = e as DeselectEvent;
			sourceParam.SetValue(deselectEvent.source);
		}

		private void OnTransformChildrenChanged()
		{
			if (base.listeningRange == EventListeningRange.GameObjectHierarchy)
			{
				UnsubscribeFrom(_controllers, typeof(DeselectEvent));
				_controllers = EventController.GetControllersInHierarchyOf(typeof(UIEventController), base.listeningTarget, UIEventController.AddController);
				SubscribeTo(_controllers, typeof(DeselectEvent));
			}
		}
	}
}
