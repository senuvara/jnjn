using UnityEngine;

namespace GameFlow
{
	[Help("es", "Establece el idioma a utilizar para propósitos de localización.", null)]
	[AddComponentMenu("")]
	[Help("en", "Sets the current language for localization purposes.", null)]
	public class SetLanguage : Action, IExecutableInEditor
	{
		[SerializeField]
		private SystemLanguage _language;

		[SerializeField]
		private Variable _languageVar;

		public SystemLanguage language => (SystemLanguage)(object)_languageVar.GetValue(_language);

		protected override void OnReset()
		{
			_language = LocalizationSettings.GetDefaultLanguage().systemLanguage;
		}

		public override void Execute()
		{
			GameSettings.language = Language.GetInstance(language);
		}
	}
}
