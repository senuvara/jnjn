using UnityEngine;

namespace GameFlow
{
	public abstract class ParameterAction : ValueAction, IExecutableInEditor
	{
		[SerializeField]
		private Parameter _parameter;

		public Parameter parameter => _parameter;
	}
}
