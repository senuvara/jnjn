using System;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.EventSystems;
using UnityEngine.UI;

namespace GameFlow
{
	[Help("es", "Controla los eventos de interfaz de usuario en este @UnityManual{GameObject}.", null)]
	[Help("en", "Controls the user interface events in this @UnityManual{GameObject}.", null)]
	[DisallowMultipleComponent]
	[AddComponentMenu("")]
	public class UIEventController : EventController, IPointerDownHandler, IEventSystemHandler, IPointerEnterHandler, IPointerExitHandler, IPointerUpHandler, IPointerClickHandler, ISelectHandler, IDeselectHandler, IMoveHandler, ISubmitHandler
	{
		private bool _clickEnabled = true;

		private bool _pointerDownEnabled = true;

		private bool _pointerEnterEnabled = true;

		private bool _pointerExitEnabled = true;

		private bool _pointerUpEnabled = true;

		private bool _valueChangeEnabled = true;

		private bool _endEditEnabled = true;

		private bool _selectEnabled = true;

		private bool _deselectEnabled = true;

		private List<IEventListener> onClickListeners = new List<IEventListener>();

		private List<IEventListener> onPointerDownListeners = new List<IEventListener>();

		private List<IEventListener> onPointerEnterListeners = new List<IEventListener>();

		private List<IEventListener> onPointerExitListeners = new List<IEventListener>();

		private List<IEventListener> onPointerUpListeners = new List<IEventListener>();

		private List<IEventListener> onValueChangeListeners = new List<IEventListener>();

		private List<IEventListener> onEndEditListeners = new List<IEventListener>();

		private List<IEventListener> onSelectListeners = new List<IEventListener>();

		private List<IEventListener> onDeselectListeners = new List<IEventListener>();

		public static EventController AddController(GameObject gameObject)
		{
			return (!(gameObject.GetComponent<Selectable>() != null) && !gameObject.GetComponent<InputField>() && !gameObject.GetComponent<Toggle>() && !gameObject.GetComponent<Slider>() && !gameObject.GetComponent<Scrollbar>()) ? null : EventController.AddController(gameObject, typeof(UIEventController));
		}

		private void Start()
		{
		}

		private void Awake()
		{
			InputField component = base.gameObject.GetComponent<InputField>();
			if (component != null)
			{
				component.onValueChanged.AddListener(OnValueChange);
				component.onEndEdit.AddListener(OnEndEdit);
			}
			Toggle component2 = base.gameObject.GetComponent<Toggle>();
			if (component2 != null)
			{
				component2.onValueChanged.AddListener(OnValueChange);
			}
			Slider component3 = base.gameObject.GetComponent<Slider>();
			if (component3 != null)
			{
				component3.onValueChanged.AddListener(OnValueChange);
			}
			Scrollbar component4 = base.gameObject.GetComponent<Scrollbar>();
			if (component4 != null)
			{
				component4.onValueChanged.AddListener(OnValueChange);
			}
		}

		public override void AddListener(IEventListener listener, Type eventType)
		{
			if (eventType == typeof(ClickEvent))
			{
				if (base.gameObject.GetComponent<Selectable>() != null)
				{
					onClickListeners.AddOnlyOnce(listener);
				}
			}
			else if (eventType == typeof(PointerDownEvent))
			{
				if (base.gameObject.GetComponent<Selectable>() != null)
				{
					onPointerDownListeners.AddOnlyOnce(listener);
				}
			}
			else if (eventType == typeof(PointerEnterEvent))
			{
				if (base.gameObject.GetComponent<Selectable>() != null)
				{
					onPointerEnterListeners.AddOnlyOnce(listener);
				}
			}
			else if (eventType == typeof(PointerExitEvent))
			{
				if (base.gameObject.GetComponent<Selectable>() != null)
				{
					onPointerExitListeners.AddOnlyOnce(listener);
				}
			}
			else if (eventType == typeof(PointerUpEvent))
			{
				if (base.gameObject.GetComponent<Selectable>() != null)
				{
					onPointerUpListeners.AddOnlyOnce(listener);
				}
			}
			else if (eventType == typeof(ValueChangeEvent))
			{
				if (base.gameObject.GetComponent<InputField>() != null)
				{
					onValueChangeListeners.AddOnlyOnce(listener);
				}
				else if (base.gameObject.GetComponent<Toggle>() != null)
				{
					onValueChangeListeners.AddOnlyOnce(listener);
				}
				else if (base.gameObject.GetComponent<Slider>() != null)
				{
					onValueChangeListeners.AddOnlyOnce(listener);
				}
				else if (base.gameObject.GetComponent<Scrollbar>() != null)
				{
					onValueChangeListeners.AddOnlyOnce(listener);
				}
			}
			else if (eventType == typeof(EndEditEvent))
			{
				if (base.gameObject.GetComponent<InputField>() != null)
				{
					onEndEditListeners.AddOnlyOnce(listener);
				}
			}
			else if (eventType == typeof(SelectEvent))
			{
				if (base.gameObject.GetComponent<Selectable>() != null)
				{
					onSelectListeners.AddOnlyOnce(listener);
				}
			}
			else if (eventType == typeof(DeselectEvent) && base.gameObject.GetComponent<Selectable>() != null)
			{
				onDeselectListeners.AddOnlyOnce(listener);
			}
		}

		public override void RemoveListener(IEventListener listener, Type eventType)
		{
			if (eventType == typeof(ClickEvent))
			{
				onClickListeners.Remove(listener);
			}
		}

		public void OnPointerClick(PointerEventData eventData)
		{
			if (base.enabled && _clickEnabled)
			{
				ClickEvent clickEvent = new ClickEvent(base.gameObject);
				clickEvent.position = eventData.position;
				clickEvent.DispatchTo(onClickListeners);
				clickEvent.DispatchToAll();
			}
		}

		public void OnPointerDown(PointerEventData eventData)
		{
			if (base.enabled && _pointerDownEnabled)
			{
				PointerDownEvent pointerDownEvent = new PointerDownEvent(base.gameObject);
				pointerDownEvent.position = eventData.position;
				pointerDownEvent.DispatchTo(onPointerDownListeners);
				pointerDownEvent.DispatchToAll();
			}
		}

		public void OnPointerEnter(PointerEventData eventData)
		{
			if (base.enabled && _pointerEnterEnabled)
			{
				PointerEnterEvent pointerEnterEvent = new PointerEnterEvent(base.gameObject);
				pointerEnterEvent.position = eventData.position;
				pointerEnterEvent.DispatchTo(onPointerEnterListeners);
				pointerEnterEvent.DispatchToAll();
			}
		}

		public void OnPointerExit(PointerEventData eventData)
		{
			if (base.enabled && _pointerExitEnabled)
			{
				PointerExitEvent pointerExitEvent = new PointerExitEvent(base.gameObject);
				pointerExitEvent.position = eventData.position;
				pointerExitEvent.DispatchTo(onPointerExitListeners);
				pointerExitEvent.DispatchToAll();
			}
		}

		public void OnPointerUp(PointerEventData eventData)
		{
			if (base.enabled && _pointerUpEnabled)
			{
				PointerUpEvent pointerUpEvent = new PointerUpEvent(base.gameObject);
				pointerUpEvent.position = eventData.position;
				pointerUpEvent.DispatchTo(onPointerUpListeners);
				pointerUpEvent.DispatchToAll();
			}
		}

		private void OnValueChange(string stringValue)
		{
			if (base.enabled && _valueChangeEnabled)
			{
				ValueChangeEvent valueChangeEvent = new ValueChangeEvent(base.gameObject, stringValue);
				valueChangeEvent.DispatchTo(onValueChangeListeners);
				valueChangeEvent.DispatchToAll();
			}
		}

		private void OnValueChange(bool toggleValue)
		{
			if (base.enabled && _valueChangeEnabled)
			{
				ValueChangeEvent valueChangeEvent = new ValueChangeEvent(base.gameObject, toggleValue);
				valueChangeEvent.DispatchTo(onValueChangeListeners);
				valueChangeEvent.DispatchToAll();
			}
		}

		private void OnValueChange(float floatValue)
		{
			if (base.enabled && _valueChangeEnabled)
			{
				ValueChangeEvent valueChangeEvent = new ValueChangeEvent(base.gameObject, floatValue);
				valueChangeEvent.DispatchTo(onValueChangeListeners);
				valueChangeEvent.DispatchToAll();
			}
		}

		private void OnEndEdit(string stringValue)
		{
			if (base.enabled && _endEditEnabled)
			{
				EndEditEvent endEditEvent = new EndEditEvent(base.gameObject, stringValue);
				endEditEvent.DispatchTo(onEndEditListeners);
				endEditEvent.DispatchToAll();
			}
		}

		public void OnSelect(BaseEventData eventData)
		{
			if (base.enabled && _selectEnabled)
			{
				SelectEvent selectEvent = new SelectEvent(base.gameObject);
				selectEvent.DispatchTo(onSelectListeners);
				selectEvent.DispatchToAll();
			}
		}

		public void OnDeselect(BaseEventData eventData)
		{
			if (base.enabled && _deselectEnabled)
			{
				DeselectEvent deselectEvent = new DeselectEvent(base.gameObject);
				deselectEvent.DispatchTo(onDeselectListeners);
				deselectEvent.DispatchToAll();
			}
		}

		public void OnMove(AxisEventData eventData)
		{
		}

		public void OnSubmit(BaseEventData eventData)
		{
			if (base.enabled && _clickEnabled)
			{
				ClickEvent clickEvent = new ClickEvent(base.gameObject);
				clickEvent.DispatchTo(onClickListeners);
				clickEvent.DispatchToAll();
			}
		}

		private void InitUIEvent(UIEvent e)
		{
			e.source = base.gameObject;
		}
	}
}
