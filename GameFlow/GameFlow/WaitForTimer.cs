using UnityEngine;

namespace GameFlow
{
	[Help("en", "Waits the specified @GameFlow{Timer} for expiration.", null)]
	[Help("es", "Espera hasta que el @GameFlow{Timer} especificado haya terminado.", null)]
	[AddComponentMenu("")]
	public class WaitForTimer : Action
	{
		[SerializeField]
		private Timer _target;

		[SerializeField]
		private Variable _targetVar;

		public Timer timer => _targetVar.GetValue(_target) as Timer;

		public override bool Finished()
		{
			return timer != null && timer.expired;
		}
	}
}
