using UnityEngine;
using UnityEngine.UI;

namespace GameFlow
{
	[Help("es", "Hace desaparecer gradualmente el @UnityManual{GameObject} especificado.", null)]
	[Help("en", "Makes the specified @UnityManual{GameObject} disappear gradually.", null)]
	[AddComponentMenu("")]
	public class FadeOut : TimeAction, IExecutableInEditor
	{
		[SerializeField]
		private GameObject _target;

		[SerializeField]
		private Variable _targetVar;

		private bool _finished;

		private float _endAlpha;

		private CanvasGroup _canvasGroup;

		private float _canvasGroupAlpha;

		private Material _material;

		private float _materialStartAlpha;

		private GUIText _guiText;

		private float _guiTextStartAlpha;

		private GUITexture _guiTexture;

		private float _guiTextureStartAlpha;

		private Text _text;

		private float _textStartAlpha;

		private Image _image;

		private float _imageStartAlpha;

		public GameObject target => _targetVar.GetValue(_target) as GameObject;

		protected override void OnReset()
		{
			base.OnReset();
			_target = base.gameObject;
		}

		public virtual float GetEndAlpha()
		{
			return 0f;
		}

		public override void FirstStep()
		{
			_endAlpha = GetEndAlpha();
			_finished = true;
			if (target == null)
			{
				return;
			}
			bool flag = base.duration == 0f || !Application.isPlaying;
			CanvasGroup component = target.GetComponent<CanvasGroup>();
			if (component != null)
			{
				if (flag)
				{
					component.alpha = _endAlpha;
				}
				else
				{
					_canvasGroup = component;
					_canvasGroupAlpha = component.alpha;
					_finished = false;
				}
			}
			else
			{
				Renderer component2 = target.GetComponent<Renderer>();
				Color color;
				if (component2 != null)
				{
					_material = component2.material;
					if (_material != null)
					{
						if (flag)
						{
							color = _material.color;
							color.a = _endAlpha;
							_material.color = color;
						}
						else
						{
							Color color2 = _material.color;
							_materialStartAlpha = color2.a;
							_finished = false;
						}
					}
				}
				Text component3 = target.GetComponent<Text>();
				if (component3 != null)
				{
					if (flag)
					{
						color = component3.color;
						color.a = _endAlpha;
						component3.color = color;
					}
					else
					{
						_text = component3;
						Color color3 = component3.color;
						_textStartAlpha = color3.a;
						_finished = false;
					}
				}
				Image component4 = target.GetComponent<Image>();
				if (component4 != null)
				{
					if (flag)
					{
						color = component4.color;
						color.a = _endAlpha;
						component4.color = color;
					}
					else
					{
						_image = component4;
						Color color4 = component4.color;
						_imageStartAlpha = color4.a;
						_finished = false;
					}
				}
				GUIText component5 = target.GetComponent<GUIText>();
				if (component5 != null)
				{
					if (flag)
					{
						color = component5.color;
						color.a = _endAlpha;
						component5.color = color;
					}
					else
					{
						_guiText = component5;
						Color color5 = component5.color;
						_guiTextStartAlpha = color5.a;
						_finished = false;
					}
				}
				GUITexture component6 = target.GetComponent<GUITexture>();
				if (component6 != null)
				{
					if (flag)
					{
						color = component6.color;
						color.a = _endAlpha;
						component6.color = color;
					}
					else
					{
						_guiTexture = component6;
						Color color6 = component6.color;
						_guiTextureStartAlpha = color6.a;
						_finished = false;
					}
				}
			}
			if (!_finished)
			{
				base.FirstStep();
			}
		}

		public override void Step()
		{
			float elapsedTimeDelta = base.timer.elapsedTimeDelta;
			if (_canvasGroup != null)
			{
				_canvasGroup.alpha = Mathf.Lerp(_canvasGroupAlpha, _endAlpha, elapsedTimeDelta);
				return;
			}
			Color color;
			if (_material != null)
			{
				color = _material.color;
				color.a = Mathf.Lerp(_materialStartAlpha, _endAlpha, elapsedTimeDelta);
				_material.color = color;
			}
			if (_text != null)
			{
				color = _text.color;
				color.a = Mathf.Lerp(_textStartAlpha, _endAlpha, elapsedTimeDelta);
				_text.color = color;
			}
			if (_image != null)
			{
				color = _image.color;
				color.a = Mathf.Lerp(_imageStartAlpha, _endAlpha, elapsedTimeDelta);
				_image.color = color;
			}
			if (_guiText != null)
			{
				color = _guiText.color;
				color.a = Mathf.Lerp(_guiTextStartAlpha, _endAlpha, elapsedTimeDelta);
				_guiText.color = color;
			}
			if (_guiTexture != null)
			{
				color = _guiTexture.color;
				color.a = Mathf.Lerp(_guiTextureStartAlpha, _endAlpha, elapsedTimeDelta);
				_guiTexture.color = color;
			}
		}

		public override bool Finished()
		{
			return _finished || base.Finished();
		}
	}
}
