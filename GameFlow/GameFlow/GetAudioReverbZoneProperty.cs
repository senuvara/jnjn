using UnityEngine;

namespace GameFlow
{
	[AddComponentMenu("")]
	[Help("en", "Gets a property of the specified @UnityManual{AudioReverbZone} component.", null)]
	[Help("es", "Lee una propiedad del componente @UnityManual{AudioReverbZone} especificado.", null)]
	public class GetAudioReverbZoneProperty : Action, IExecutableInEditor
	{
		public enum Property
		{
			DecayHFRatio,
			DecayTime,
			Density,
			Diffusion,
			HFReference,
			LFReference,
			MaxDistance,
			MinDistance,
			Reflections,
			ReflectionsDelay,
			Reverb,
			ReverbDelay,
			ReverbPreset,
			Room,
			RoomHF,
			RoomLF,
			RoomRolloffFactor
		}

		[SerializeField]
		private AudioReverbZone _source;

		[SerializeField]
		private Variable _sourceVar;

		[SerializeField]
		private Property _property;

		[SerializeField]
		private Variable _output;

		protected override void OnReset()
		{
			_source = base.gameObject.GetComponent<AudioReverbZone>();
		}

		public override void Setup()
		{
		}

		public override void Execute()
		{
			AudioReverbZone audioReverbZone = _sourceVar.GetValueAsComponent(_source, typeof(AudioReverbZone)) as AudioReverbZone;
			if (!(audioReverbZone == null))
			{
				switch (_property)
				{
				case Property.DecayHFRatio:
					_output.SetValue(audioReverbZone.decayHFRatio);
					break;
				case Property.DecayTime:
					_output.SetValue(audioReverbZone.decayTime);
					break;
				case Property.Density:
					_output.SetValue(audioReverbZone.density);
					break;
				case Property.Diffusion:
					_output.SetValue(audioReverbZone.diffusion);
					break;
				case Property.HFReference:
					_output.SetValue(audioReverbZone.HFReference);
					break;
				case Property.LFReference:
					_output.SetValue(audioReverbZone.LFReference);
					break;
				case Property.MaxDistance:
					_output.SetValue(audioReverbZone.maxDistance);
					break;
				case Property.MinDistance:
					_output.SetValue(audioReverbZone.minDistance);
					break;
				case Property.Reflections:
					_output.SetValue(audioReverbZone.reflections);
					break;
				case Property.ReflectionsDelay:
					_output.SetValue(audioReverbZone.reflectionsDelay);
					break;
				case Property.Reverb:
					_output.SetValue(audioReverbZone.reverb);
					break;
				case Property.ReverbDelay:
					_output.SetValue(audioReverbZone.reverbDelay);
					break;
				case Property.ReverbPreset:
					_output.SetValue(audioReverbZone.reverbPreset);
					break;
				case Property.Room:
					_output.SetValue(audioReverbZone.room);
					break;
				case Property.RoomHF:
					_output.SetValue(audioReverbZone.roomHF);
					break;
				case Property.RoomLF:
					_output.SetValue(audioReverbZone.roomLF);
					break;
				case Property.RoomRolloffFactor:
					_output.SetValue(audioReverbZone.roomRolloffFactor);
					break;
				}
			}
		}
	}
}
