using UnityEngine;

namespace GameFlow
{
	[Help("en", "Sets a property of the specified @GameFlow{Program}.", null)]
	[Help("es", "Modifica una propiedad del programa (@GameFlow{Program}) especificado.", null)]
	[AddComponentMenu("")]
	public class SetProgramProperty : Action, IExecutableInEditor
	{
		public enum Property
		{
			IgnorePause
		}

		[SerializeField]
		private Program _target;

		[SerializeField]
		private Variable _targetVar;

		[SerializeField]
		private Property _property;

		[SerializeField]
		private bool _ignorePauseValue;

		[SerializeField]
		private Variable _valueVar;

		protected override void OnReset()
		{
			_target = base.gameObject.GetComponent<Program>();
		}

		public override void Setup()
		{
		}

		public override void Execute()
		{
			Program program = _targetVar.GetValueAsComponent(_target, typeof(Program)) as Program;
			if (!(program == null) && _property == Property.IgnorePause)
			{
				program.ignorePause = _valueVar.GetValue(_ignorePauseValue);
			}
		}
	}
}
