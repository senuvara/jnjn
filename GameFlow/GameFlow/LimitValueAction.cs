using UnityEngine;

namespace GameFlow
{
	public abstract class LimitValueAction : ValueAction, IExecutableInEditor
	{
		[SerializeField]
		private int _maxIntValue;

		[SerializeField]
		private float _maxFloatValue;

		[SerializeField]
		private Rect _maxRectValue;

		[SerializeField]
		private Vector2 _maxVector2Value;

		[SerializeField]
		private Vector3 _maxVector3Value;

		[SerializeField]
		private Variable _maxValueVar;

		public int maxIntValue => _maxValueVar.GetValue(_maxIntValue);

		public float maxFloatValue => _maxValueVar.GetValue(_maxFloatValue);

		public Rect maxRectValue => _maxValueVar.GetValue(_maxRectValue);

		public Vector2 maxVector2Value => _maxValueVar.GetValue(_maxVector2Value);

		public Vector3 maxVector3Value => _maxValueVar.GetValue(_maxVector3Value);

		public Variable maxValueVar => _maxValueVar;
	}
}
