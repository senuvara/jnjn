using UnityEngine;
using UnityEngine.UI;

namespace GameFlow
{
	[Help("en", "Gets a property of the specified Slider component.", null)]
	[Help("es", "Lee una propiedad del componente Slider especificado.", null)]
	[AddComponentMenu("")]
	public class GetSliderProperty : Action, IExecutableInEditor
	{
		public enum Property
		{
			AnimationTriggers_DisabledTrigger,
			AnimationTriggers_HighlightedTrigger,
			AnimationTriggers_NormalTrigger,
			AnimationTriggers_PressedTrigger,
			Animator,
			Colors_ColorMultiplier,
			Colors_DisabledColor,
			Colors_FadeDuration,
			Colors_HighlightedColor,
			Colors_NormalColor,
			Colors_PressedColor,
			Direction,
			FillRect,
			HandleRect,
			Image,
			Interactable,
			MaxValue,
			MinValue,
			Navigation_Mode,
			Navigation_SelectOnDown,
			Navigation_SelectOnLeft,
			Navigation_SelectOnRight,
			Navigation_SelectOnUp,
			NormalizedValue,
			SpriteState_DisabledSprite,
			SpriteState_HighlightedSprite,
			SpriteState_PressedSprite,
			TargetGraphic,
			Transition,
			Value,
			WholeNumbers
		}

		[SerializeField]
		private Slider _source;

		[SerializeField]
		private Variable _sourceVar;

		[SerializeField]
		private Property _property;

		[SerializeField]
		private Variable _output;

		protected override void OnReset()
		{
			_source = base.gameObject.GetComponent<Slider>();
		}

		public override void Setup()
		{
		}

		public override void Execute()
		{
			Slider slider = _sourceVar.GetValueAsComponent(_source, typeof(Slider)) as Slider;
			if (!(slider == null))
			{
				switch (_property)
				{
				case Property.AnimationTriggers_DisabledTrigger:
					_output.SetValue(slider.animationTriggers.disabledTrigger);
					break;
				case Property.AnimationTriggers_HighlightedTrigger:
					_output.SetValue(slider.animationTriggers.highlightedTrigger);
					break;
				case Property.AnimationTriggers_NormalTrigger:
					_output.SetValue(slider.animationTriggers.normalTrigger);
					break;
				case Property.AnimationTriggers_PressedTrigger:
					_output.SetValue(slider.animationTriggers.pressedTrigger);
					break;
				case Property.Animator:
					_output.SetValue(slider.animator);
					break;
				case Property.Colors_ColorMultiplier:
					_output.SetValue(slider.colors.colorMultiplier);
					break;
				case Property.Colors_DisabledColor:
					_output.SetValue(slider.colors.disabledColor);
					break;
				case Property.Colors_FadeDuration:
					_output.SetValue(slider.colors.fadeDuration);
					break;
				case Property.Colors_HighlightedColor:
					_output.SetValue(slider.colors.highlightedColor);
					break;
				case Property.Colors_NormalColor:
					_output.SetValue(slider.colors.normalColor);
					break;
				case Property.Colors_PressedColor:
					_output.SetValue(slider.colors.pressedColor);
					break;
				case Property.Direction:
					_output.SetValue(slider.direction);
					break;
				case Property.FillRect:
					_output.SetValue(slider.fillRect);
					break;
				case Property.HandleRect:
					_output.SetValue(slider.handleRect);
					break;
				case Property.Image:
					_output.SetValue(slider.image);
					break;
				case Property.Interactable:
					_output.SetValue(slider.interactable);
					break;
				case Property.MaxValue:
					_output.SetValue(slider.maxValue);
					break;
				case Property.MinValue:
					_output.SetValue(slider.minValue);
					break;
				case Property.Navigation_Mode:
					_output.SetValue(slider.navigation.mode);
					break;
				case Property.Navigation_SelectOnDown:
					_output.SetValue(slider.navigation.selectOnDown);
					break;
				case Property.Navigation_SelectOnLeft:
					_output.SetValue(slider.navigation.selectOnLeft);
					break;
				case Property.Navigation_SelectOnRight:
					_output.SetValue(slider.navigation.selectOnRight);
					break;
				case Property.Navigation_SelectOnUp:
					_output.SetValue(slider.navigation.selectOnUp);
					break;
				case Property.NormalizedValue:
					_output.SetValue(slider.normalizedValue);
					break;
				case Property.SpriteState_DisabledSprite:
					_output.SetValue(slider.spriteState.disabledSprite);
					break;
				case Property.SpriteState_HighlightedSprite:
					_output.SetValue(slider.spriteState.highlightedSprite);
					break;
				case Property.SpriteState_PressedSprite:
					_output.SetValue(slider.spriteState.pressedSprite);
					break;
				case Property.TargetGraphic:
					_output.SetValue(slider.targetGraphic);
					break;
				case Property.Transition:
					_output.SetValue(slider.transition);
					break;
				case Property.Value:
					_output.SetValue(slider.value);
					break;
				case Property.WholeNumbers:
					_output.SetValue(slider.wholeNumbers);
					break;
				}
			}
		}
	}
}
