using UnityEngine;

namespace GameFlow
{
	public static class HierarchyUtils
	{
		public static IHierarchyUtilsProxy proxy;

		public static bool SelectionContains(GameObject go)
		{
			if (proxy != null)
			{
				return proxy.SelectionContains(go);
			}
			return false;
		}

		public static bool SelectionContainsParentOf(GameObject go)
		{
			if (proxy != null)
			{
				return proxy.SelectionContainsParentOf(go);
			}
			return false;
		}
	}
}
