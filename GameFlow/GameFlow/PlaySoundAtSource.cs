using UnityEngine;

namespace GameFlow
{
	[AddComponentMenu("")]
	[Help("en", "Plays the sound at the specified @UnityManual{AudioSource}.", null)]
	[Help("es", "Reproduce el sonido ubicado en el @UnityManual{AudioSource} especificado.", null)]
	public class PlaySoundAtSource : Action
	{
		[SerializeField]
		private AudioSource _audioSource;

		[SerializeField]
		private Variable _audioSourceVar;

		[SerializeField]
		private float _volume = 1f;

		[SerializeField]
		private Variable _volumeVar;

		[SerializeField]
		private bool _loop;

		public override void Execute()
		{
			AudioController.instance.PlayFXAtSource(_audioSourceVar.GetValue(_audioSource) as AudioSource, _volumeVar.GetValue(_volume), _loop);
		}
	}
}
