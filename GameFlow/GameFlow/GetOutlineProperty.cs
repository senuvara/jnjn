using UnityEngine;
using UnityEngine.UI;

namespace GameFlow
{
	[Help("es", "Lee una propiedad del componente @UnityManual{Outline} especificado.", null)]
	[AddComponentMenu("")]
	[Help("en", "Gets a property of the specified @UnityManual{Outline} component.", null)]
	public class GetOutlineProperty : Action, IExecutableInEditor
	{
		public enum Property
		{
			EffectColor,
			EffectDistance,
			UseGraphicAlpha
		}

		[SerializeField]
		private Outline _source;

		[SerializeField]
		private Variable _sourceVar;

		[SerializeField]
		private Property _property;

		[SerializeField]
		private Variable _output;

		protected override void OnReset()
		{
			_source = base.gameObject.GetComponent<Outline>();
		}

		public override void Setup()
		{
		}

		public override void Execute()
		{
			Outline outline = _sourceVar.GetValueAsComponent(_source, typeof(Outline)) as Outline;
			if (!(outline == null))
			{
				switch (_property)
				{
				case Property.EffectColor:
					_output.SetValue(outline.effectColor);
					break;
				case Property.EffectDistance:
					_output.SetValue(outline.effectDistance);
					break;
				case Property.UseGraphicAlpha:
					_output.SetValue(outline.useGraphicAlpha);
					break;
				}
			}
		}
	}
}
