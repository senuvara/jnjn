using UnityEngine;

namespace GameFlow
{
	public class RaycastHit : ScriptableObject, IVariableFriendly
	{
		[SerializeField]
		private Vector3 _barycenter;

		[SerializeField]
		private Collider _collider;

		[SerializeField]
		private float _distance;

		[SerializeField]
		private GameObject _gameObject;

		[SerializeField]
		private Vector3 _impactPoint;

		[SerializeField]
		private Vector2 _lightmapCoord;

		[SerializeField]
		private Vector3 _normal;

		[SerializeField]
		private Rigidbody _rigidbody;

		[SerializeField]
		private Vector2 _textureCoord;

		[SerializeField]
		private Vector2 _textureCoord2;

		[SerializeField]
		private Transform _transform;

		[SerializeField]
		private int _triangleIndex;

		public Vector3 barycenter => _barycenter;

		public Collider collider => _collider;

		public float distance => _distance;

		public GameObject gameObject => _gameObject;

		public Vector3 impactPoint => _impactPoint;

		public Vector2 lightmapCoord => _lightmapCoord;

		public Vector3 normal => _normal;

		public Rigidbody rigidbody => _rigidbody;

		public Vector2 textureCoord => _textureCoord;

		public Vector2 textureCoord2 => _textureCoord2;

		public Transform transform => _transform;

		public int triangleIndex => _triangleIndex;

		public static RaycastHit CreateInstance(UnityEngine.RaycastHit hit)
		{
			RaycastHit raycastHit = ScriptableObject.CreateInstance<RaycastHit>();
			raycastHit.SetProperties(hit);
			return raycastHit;
		}

		public void SetProperties(UnityEngine.RaycastHit hit)
		{
			_barycenter = hit.barycentricCoordinate;
			_collider = hit.collider;
			_distance = hit.distance;
			_gameObject = ((!(hit.collider != null)) ? null : hit.collider.gameObject);
			_impactPoint = hit.point;
			_lightmapCoord = ((!(hit.collider != null)) ? Vector2.zero : hit.lightmapCoord);
			_normal = hit.normal;
			_rigidbody = hit.rigidbody;
			_textureCoord = hit.textureCoord;
			_textureCoord2 = hit.textureCoord2;
			_transform = hit.transform;
			_triangleIndex = hit.triangleIndex;
		}
	}
}
