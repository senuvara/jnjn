using System.Collections.Generic;
using UnityEngine;

namespace GameFlow
{
	public abstract class Container : BaseBehaviour, IBlockContainer
	{
		[HideInInspector]
		public int stamp;

		[HideInInspector]
		public int stampMax;

		private bool dirty;

		private void Reset()
		{
			SetResetFlag();
			OnReset();
		}

		public void SetDirty(bool dirty)
		{
			this.dirty = dirty;
		}

		public bool IsDirty()
		{
			return dirty;
		}

		public virtual List<Block> GetBlocks()
		{
			return null;
		}

		public virtual bool Contains(Block block)
		{
			return false;
		}
	}
}
