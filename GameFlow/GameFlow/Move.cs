using UnityEngine;

namespace GameFlow
{
	[AddComponentMenu("")]
	[Help("en", "Moves the specified object to match the given target position.", null)]
	[Help("es", "Mueve el objeto especificado hasta que alcance la posición indicada.", null)]
	public class Move : Interpolate
	{
		protected override void OnReset()
		{
			base.OnReset();
			base.interpolation = Interpolation.Position;
			base.positionTargetType = TargetType.Transform;
		}

		public override void Step()
		{
			float elapsedTimeDelta = base.timer.elapsedTimeDelta;
			if (_t1 == null)
			{
				_finished = true;
				return;
			}
			switch (base.positionTargetType)
			{
			case TargetType.Vector3:
				switch (base.space)
				{
				case Space.World:
					_t1.position = MathUtils.Ease(startPosition, toPosition, elapsedTimeDelta, base.easingType);
					break;
				case Space.Self:
					_t1.localPosition = MathUtils.Ease(startPosition, toPosition, elapsedTimeDelta, base.easingType);
					break;
				}
				break;
			case TargetType.Transform:
				if (_t2 == null)
				{
					_finished = true;
				}
				else
				{
					_t1.position = MathUtils.Ease(startPosition, _t2.position, elapsedTimeDelta, base.easingType);
				}
				break;
			}
		}
	}
}
