using System;
using UnityEngine;

namespace GameFlow
{
	[Help("es", "Modifica el elemento en el índice dado de la @GameFlow{List} especificada.", null)]
	[Help("en", "Sets the item at the given index in the specified @GameFlow{List}.", null)]
	[AddComponentMenu("")]
	public class SetItemInList : ListAction
	{
		public override void Execute()
		{
			List list = GetList();
			if (list == null)
			{
				return;
			}
			int index = GetIndex();
			if (list.IsIndexValid(index))
			{
				switch (list.dataType)
				{
				case DataType.String:
				case DataType.Tag:
					list.SetValueAt(base.stringValue, index);
					break;
				case DataType.Integer:
				case DataType.Layer:
					list.SetValueAt(base.intValue, index);
					break;
				case DataType.Float:
					list.SetValueAt(base.floatValue, index);
					break;
				case DataType.Boolean:
				case DataType.Toggle:
					list.SetValueAt(base.boolValue, index);
					break;
				case DataType.Vector2:
					list.SetValueAt(base.vector2Value, index);
					break;
				case DataType.Vector3:
					list.SetValueAt(base.vector3Value, index);
					break;
				case DataType.Rect:
					list.SetValueAt(base.rectValue, index);
					break;
				case DataType.Color:
					list.SetValueAt(base.colorValue, index);
					break;
				case DataType.Object:
					list.SetValueAt(base.objectValue, index);
					break;
				case DataType.Enum:
					list.SetValueAt((Enum)Enum.ToObject(list.type, base.enumIntValue), index);
					break;
				}
			}
		}
	}
}
