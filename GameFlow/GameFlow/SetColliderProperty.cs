using UnityEngine;

namespace GameFlow
{
	[Help("es", "Modifica una propiedad del componente @UnityManual{Collider} especificado.", null)]
	[AddComponentMenu("")]
	[Help("en", "Sets a property of the specified @UnityManual{Collider}.", null)]
	public class SetColliderProperty : Action, IExecutableInEditor
	{
		public enum ColliderProperty
		{
			Enabled,
			IsTrigger,
			Material,
			SharedMaterial,
			ContactOffset
		}

		public enum BoxColliderProperty
		{
			Center,
			Enabled,
			IsTrigger,
			Material,
			SharedMaterial,
			Size,
			ContactOffset
		}

		public enum SphereColliderProperty
		{
			Center,
			Enabled,
			IsTrigger,
			Material,
			Radius,
			SharedMaterial,
			ContactOffset
		}

		public enum CapsuleColliderProperty
		{
			Center,
			Direction,
			Enabled,
			Height,
			IsTrigger,
			Material,
			Radius,
			SharedMaterial,
			ContactOffset
		}

		public enum MeshColliderProperty
		{
			Convex,
			Enabled,
			IsTrigger,
			Material,
			SharedMaterial,
			SharedMesh,
			ContactOffset
		}

		[SerializeField]
		private Collider _target;

		[SerializeField]
		private Variable _targetVar;

		[SerializeField]
		private ColliderProperty _baseProperty;

		[SerializeField]
		private BoxColliderProperty _boxProperty;

		[SerializeField]
		private SphereColliderProperty _sphereProperty;

		[SerializeField]
		private CapsuleColliderProperty _capsuleProperty;

		[SerializeField]
		private MeshColliderProperty _meshProperty;

		[SerializeField]
		private Vector3 _vector3Value;

		[SerializeField]
		private bool _boolValue;

		[SerializeField]
		private PhysicMaterial _materialValue;

		[SerializeField]
		private float _floatValue;

		[SerializeField]
		private Axis _axisValue;

		[SerializeField]
		private Mesh _meshValue;

		[SerializeField]
		private Variable _valueVar;

		protected override void OnReset()
		{
			_target = base.gameObject.GetComponent<Collider>();
		}

		public override void Execute()
		{
			Collider collider = _target;
			if (collider == null)
			{
				collider = (_targetVar.GetValueAsComponent(_target, typeof(Collider)) as Collider);
			}
			if (collider == null)
			{
				return;
			}
			BoxCollider boxCollider = collider as BoxCollider;
			if (boxCollider != null)
			{
				switch (_boxProperty)
				{
				case BoxColliderProperty.Center:
					boxCollider.center = _valueVar.GetValue(_vector3Value);
					break;
				case BoxColliderProperty.Enabled:
					collider.enabled = _valueVar.GetValue(_boolValue);
					break;
				case BoxColliderProperty.IsTrigger:
					collider.isTrigger = _valueVar.GetValue(_boolValue);
					break;
				case BoxColliderProperty.Material:
					collider.material = (_valueVar.GetValue(_materialValue) as PhysicMaterial);
					break;
				case BoxColliderProperty.SharedMaterial:
					collider.sharedMaterial = (_valueVar.GetValue(_materialValue) as PhysicMaterial);
					break;
				case BoxColliderProperty.Size:
					boxCollider.size = _valueVar.GetValue(_vector3Value);
					break;
				case BoxColliderProperty.ContactOffset:
					boxCollider.contactOffset = _valueVar.GetValue(_floatValue);
					break;
				}
				return;
			}
			SphereCollider sphereCollider = collider as SphereCollider;
			if (sphereCollider != null)
			{
				switch (_sphereProperty)
				{
				case SphereColliderProperty.Center:
					sphereCollider.center = _valueVar.GetValue(_vector3Value);
					break;
				case SphereColliderProperty.Enabled:
					collider.enabled = _valueVar.GetValue(_boolValue);
					break;
				case SphereColliderProperty.IsTrigger:
					collider.isTrigger = _valueVar.GetValue(_boolValue);
					break;
				case SphereColliderProperty.Material:
					collider.material = (_valueVar.GetValue(_materialValue) as PhysicMaterial);
					break;
				case SphereColliderProperty.Radius:
					sphereCollider.radius = _valueVar.GetValue(_floatValue);
					break;
				case SphereColliderProperty.SharedMaterial:
					collider.sharedMaterial = (_valueVar.GetValue(_materialValue) as PhysicMaterial);
					break;
				case SphereColliderProperty.ContactOffset:
					sphereCollider.contactOffset = _valueVar.GetValue(_floatValue);
					break;
				}
				return;
			}
			CapsuleCollider capsuleCollider = collider as CapsuleCollider;
			if (capsuleCollider != null)
			{
				switch (_capsuleProperty)
				{
				case CapsuleColliderProperty.Center:
					capsuleCollider.center = _valueVar.GetValue(_vector3Value);
					break;
				case CapsuleColliderProperty.Direction:
					capsuleCollider.direction = (int)(Axis)(object)_valueVar.GetValue(_axisValue);
					break;
				case CapsuleColliderProperty.Enabled:
					collider.enabled = _valueVar.GetValue(_boolValue);
					break;
				case CapsuleColliderProperty.Height:
					capsuleCollider.height = _valueVar.GetValue(_floatValue);
					break;
				case CapsuleColliderProperty.IsTrigger:
					collider.isTrigger = _valueVar.GetValue(_boolValue);
					break;
				case CapsuleColliderProperty.Material:
					collider.material = (_valueVar.GetValue(_materialValue) as PhysicMaterial);
					break;
				case CapsuleColliderProperty.Radius:
					capsuleCollider.radius = _valueVar.GetValue(_floatValue);
					break;
				case CapsuleColliderProperty.SharedMaterial:
					collider.sharedMaterial = (_valueVar.GetValue(_materialValue) as PhysicMaterial);
					break;
				case CapsuleColliderProperty.ContactOffset:
					capsuleCollider.contactOffset = _valueVar.GetValue(_floatValue);
					break;
				}
				return;
			}
			MeshCollider meshCollider = collider as MeshCollider;
			if (meshCollider != null)
			{
				switch (_meshProperty)
				{
				case MeshColliderProperty.Convex:
					meshCollider.convex = _valueVar.GetValue(_boolValue);
					break;
				case MeshColliderProperty.Enabled:
					collider.enabled = _valueVar.GetValue(_boolValue);
					break;
				case MeshColliderProperty.IsTrigger:
					collider.isTrigger = _valueVar.GetValue(_boolValue);
					break;
				case MeshColliderProperty.Material:
					collider.material = (_valueVar.GetValue(_materialValue) as PhysicMaterial);
					break;
				case MeshColliderProperty.SharedMaterial:
					collider.sharedMaterial = (_valueVar.GetValue(_materialValue) as PhysicMaterial);
					break;
				case MeshColliderProperty.SharedMesh:
					meshCollider.sharedMesh = (_valueVar.GetValue(_meshValue) as Mesh);
					break;
				case MeshColliderProperty.ContactOffset:
					meshCollider.contactOffset = _valueVar.GetValue(_floatValue);
					break;
				}
			}
		}
	}
}
