using UnityEngine;

namespace GameFlow
{
	public static class ParsingUtils
	{
		private static char[] separators = new char[1]
		{
			'|'
		};

		public static string ToParseable(this string s)
		{
			if (s == null || s.Length == 0)
			{
				return string.Empty;
			}
			return s.Replace(", ", separators[0].ToString());
		}

		public static Vector2 ParseVector2(string s)
		{
			string[] array = s.Replace("(", string.Empty).Replace(")", string.Empty).Split(separators);
			float x = 0f;
			if (array.Length >= 1)
			{
				x = float.Parse(array[0].Trim());
			}
			float y = 0f;
			if (array.Length >= 2)
			{
				y = float.Parse(array[1].Trim());
			}
			return new Vector2(x, y);
		}

		public static Vector3 ParseVector3(string s)
		{
			string[] array = s.Replace("(", string.Empty).Replace(")", string.Empty).Split(separators);
			float x = 0f;
			if (array.Length >= 1)
			{
				x = float.Parse(array[0].Trim());
			}
			float y = 0f;
			if (array.Length >= 2)
			{
				y = float.Parse(array[1].Trim());
			}
			float z = 0f;
			if (array.Length >= 3)
			{
				z = float.Parse(array[2].Trim());
			}
			return new Vector3(x, y, z);
		}

		public static Rect ParseRect(string s)
		{
			string[] array = s.Replace("(x:", string.Empty).Replace("y:", string.Empty).Replace("width:", string.Empty)
				.Replace("height:", string.Empty)
				.Replace(")", string.Empty)
				.Split(separators);
			float x = 0f;
			if (array.Length >= 1)
			{
				x = float.Parse(array[0].Trim());
			}
			float y = 0f;
			if (array.Length >= 2)
			{
				y = float.Parse(array[1].Trim());
			}
			float width = 0f;
			if (array.Length >= 3)
			{
				width = float.Parse(array[2].Trim());
			}
			float height = 0f;
			if (array.Length >= 4)
			{
				height = float.Parse(array[3].Trim());
			}
			return new Rect(x, y, width, height);
		}

		public static Color ParseColor(string s)
		{
			string[] array = s.Replace("RGBA(", string.Empty).Replace(")", string.Empty).Split(separators);
			float r = 0f;
			if (array.Length >= 1)
			{
				r = float.Parse(array[0].Trim());
			}
			float g = 0f;
			if (array.Length >= 2)
			{
				g = float.Parse(array[1].Trim());
			}
			float b = 0f;
			if (array.Length >= 3)
			{
				b = float.Parse(array[2].Trim());
			}
			float a = 0f;
			if (array.Length >= 4)
			{
				a = float.Parse(array[3].Trim());
			}
			return new Color(r, g, b, a);
		}

		public static Bounds ParseBounds(string s)
		{
			string[] array = s.Replace("Center: (", string.Empty).Replace("Extents: (", string.Empty).Replace(")", string.Empty)
				.Split(separators);
			Vector3 zero = Vector3.zero;
			Vector3 zero2 = Vector3.zero;
			float x = 0f;
			if (array.Length >= 1)
			{
				x = float.Parse(array[0].Trim());
			}
			float y = 0f;
			if (array.Length >= 2)
			{
				y = float.Parse(array[1].Trim());
			}
			float z = 0f;
			if (array.Length >= 3)
			{
				z = float.Parse(array[2].Trim());
			}
			zero = new Vector3(x, y, z);
			x = 0f;
			if (array.Length >= 4)
			{
				x = float.Parse(array[3].Trim());
			}
			y = 0f;
			if (array.Length >= 5)
			{
				y = float.Parse(array[4].Trim());
			}
			z = 0f;
			if (array.Length >= 6)
			{
				z = float.Parse(array[5].Trim());
			}
			zero2 = new Vector3(x, y, z);
			return new Bounds(zero, zero2);
		}

		public static Vector4 ParseVector4(string s)
		{
			string[] array = s.Replace("(", string.Empty).Replace(")", string.Empty).Split(separators);
			float x = 0f;
			if (array.Length >= 1)
			{
				x = float.Parse(array[0].Trim());
			}
			float y = 0f;
			if (array.Length >= 2)
			{
				y = float.Parse(array[1].Trim());
			}
			float z = 0f;
			if (array.Length >= 3)
			{
				z = float.Parse(array[2].Trim());
			}
			float w = 0f;
			if (array.Length >= 4)
			{
				w = float.Parse(array[3].Trim());
			}
			return new Vector4(x, y, z, w);
		}

		public static Quaternion ParseQuaternion(string s)
		{
			Vector4 vector = ParseVector4(s);
			return new Quaternion(vector.x, vector.y, vector.z, vector.w);
		}
	}
}
