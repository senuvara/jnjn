using UnityEngine;

namespace GameFlow
{
	[Help("es", "Invierte el estado de activacion del @UnityManual{GameObject} especificado.", null)]
	[Help("en", "Toggles the activation state of the specified @UnityManual{GameObject}.", null)]
	[AddComponentMenu("")]
	public class ToggleGameObjectActivation : Action, IExecutableInEditor
	{
		[SerializeField]
		private GameObject _target;

		[SerializeField]
		private Variable _targetVar;

		public GameObject target => _targetVar.GetValue(_target) as GameObject;

		public override void Execute()
		{
			GameObject target = this.target;
			if (target != null)
			{
				target.SetActive(!target.activeSelf);
			}
		}
	}
}
