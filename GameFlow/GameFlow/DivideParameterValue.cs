using UnityEngine;

namespace GameFlow
{
	[Help("es", "Divide el valor numérico del @GameFlow{Parameter} especificado.", null)]
	[Help("en", "Divides the numeric value of the specified @GameFlow{Parameter}.", null)]
	[AddComponentMenu("")]
	public class DivideParameterValue : ParameterAction
	{
		protected override int GetDefaultIntValue()
		{
			return 2;
		}

		protected override float GetDefaultFloatValue()
		{
			return 2f;
		}

		public override void Execute()
		{
			Execute(base.parameter, this);
		}

		public static void Execute(Parameter parameter, ValueAction valueAction)
		{
			DivideVariableValue.Execute(parameter, valueAction);
		}
	}
}
