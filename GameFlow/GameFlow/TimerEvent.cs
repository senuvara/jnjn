using System.Collections.Generic;

namespace GameFlow
{
	public class TimerEvent : EventObject
	{
		private static List<IEventListener> listeners = new List<IEventListener>();

		public Timer source
		{
			get;
			private set;
		}

		public TimerEvent(Timer source)
		{
			this.source = source;
		}

		public override void DispatchToAll()
		{
			EventDispatcher.DispatchEventToListeners(this, listeners);
		}

		public static void AddListener(IEventListener listener)
		{
			listeners.AddOnlyOnce(listener);
		}

		public static void RemoveListener(IEventListener listener)
		{
			listeners.Remove(listener);
		}
	}
}
