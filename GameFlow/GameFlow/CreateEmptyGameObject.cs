using UnityEngine;

namespace GameFlow
{
	[Help("en", "Creates a new empty @UnityManual{GameObject} with the specified name.", null)]
	[AddComponentMenu("")]
	[Help("es", "Crea un nuevo @UnityManual{GameObject} vacío con el nombre especificado.", null)]
	public class CreateEmptyGameObject : Action, IExecutableInEditor
	{
		[SerializeField]
		private string _objName = "GameObject";

		[SerializeField]
		private Variable _objNameVar;

		[SerializeField]
		private Variable _output;

		public string objName => _objNameVar.GetValue(_objName);

		public Variable output => _output;

		public override void Step()
		{
			GameObject value = new GameObject(objName);
			if (_output != null)
			{
				_output.SetValue(value);
			}
		}
	}
}
