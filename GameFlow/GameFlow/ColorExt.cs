using UnityEngine;

namespace GameFlow
{
	public static class ColorExt
	{
		public static int GetRGBA(this Color32 color)
		{
			return (color.r << 24) | (color.g << 16) | (color.b << 8) | color.a;
		}

		public static int GetRGBA(this Color color)
		{
			Color32 color2 = color;
			return (color2.r << 24) | (color2.g << 16) | (color2.b << 8) | color2.a;
		}

		public static string ToHexRGBA(this Color color)
		{
			return color.GetRGBA().ToString("X");
		}

		public static bool EqualsRGBA(this Color color, Color other)
		{
			Color32 color2 = color;
			Color32 color3 = other;
			return color2.r == color3.r && color2.g == color3.g && color2.b == color3.b && color2.a == color3.a;
		}

		public static void CopyRGBA(this Color color, Color other)
		{
			Color32 color2 = color;
			Color32 color3 = other;
			color2.r = color3.r;
			color2.g = color3.g;
			color2.b = color3.b;
			color2.a = color3.a;
		}

		public static bool SameTo(this Color color, Color other)
		{
			return Mathf.Abs(color.r - other.r) <= 0.01f && Mathf.Abs(color.g - other.g) <= 0.01f && Mathf.Abs(color.b - other.b) <= 0.01f && Mathf.Abs(color.a - other.a) <= 0.01f;
		}
	}
}
