using System.Collections.Generic;
using UnityEngine;

namespace GameFlow
{
	[AddComponentMenu("")]
	[Help("en", "Shows all the @GameFlow{Variable} blocks defined in the inspected @UnityManual{GameObject}.", null)]
	[Help("es", "Muestra todos los bloques @GameFlow{Variable} definidos en el @UnityManual{GameObject} inspeccionado.", null)]
	[DisallowMultipleComponent]
	public class Variables : Container, IBlockContainer, IData, IVariableContainer
	{
		[SerializeField]
		private List<Variable> _variables = new List<Variable>();

		private static Dictionary<string, Variable> variableDictionary;

		public List<Variable> GetVariables()
		{
			return _variables;
		}

		public virtual void VariableAdded(Variable variable)
		{
		}

		public override List<Block> GetBlocks()
		{
			return new List<Block>(_variables.ToArray());
		}

		public override bool Contains(Block block)
		{
			Variable variable = block as Variable;
			if (variable != null)
			{
				return _variables.Contains(variable);
			}
			return false;
		}

		private static void InitDictionary()
		{
			if (variableDictionary != null)
			{
				return;
			}
			variableDictionary = new Dictionary<string, Variable>();
			Variable[] array = Resources.FindObjectsOfTypeAll<Variable>();
			Variable[] array2 = array;
			foreach (Variable variable in array2)
			{
				if (variable != null && !variable.isPrivate && !variableDictionary.ContainsKey(variable.id))
				{
					variableDictionary.Add(variable.id, variable);
				}
			}
		}

		public static Variable GetVariableById(string id)
		{
			//Discarded unreachable code: IL_0023, IL_0030
			if (id == null)
			{
				id = string.Empty;
			}
			InitDictionary();
			try
			{
				return variableDictionary[id];
			}
			catch
			{
				return null;
			}
		}

		public static Variable GetVariableById(string id, GameObject go, bool recursive = false)
		{
			if (id == null)
			{
				id = string.Empty;
			}
			if (go == null)
			{
				return null;
			}
			Variable[] array = recursive ? go.GetComponentsInChildren<Variable>() : go.GetComponents<Variable>();
			foreach (Variable variable in array)
			{
				if (variable != null && variable.id == id)
				{
					return variable;
				}
			}
			return null;
		}

		public static Variable[] GetAllVariables()
		{
			InitDictionary();
			return new List<Variable>(variableDictionary.Values).ToArray();
		}
	}
}
