namespace GameFlow
{
	public abstract class OnMouseProgram : EventProgram
	{
		private Parameter _sourceParam;

		private Parameter _positionParam;

		protected Parameter sourceParam
		{
			get
			{
				if (_sourceParam == null)
				{
					_sourceParam = GetParameter("Source");
				}
				return _sourceParam;
			}
		}

		protected Parameter positionParam
		{
			get
			{
				if (_positionParam == null)
				{
					_positionParam = GetParameter("Mouse Position");
				}
				return _positionParam;
			}
		}

		protected override void CacheParameters()
		{
			CacheParameter(sourceParam);
			CacheParameter(positionParam);
		}

		protected override void SetParameters(EventObject e)
		{
			MouseEvent mouseEvent = e as MouseEvent;
			sourceParam.SetValue(mouseEvent.source);
			positionParam.SetValue(mouseEvent.position);
		}
	}
}
