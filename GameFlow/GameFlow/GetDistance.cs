using UnityEngine;

namespace GameFlow
{
	[Help("es", "Obtiene la distancia entre dos objetos.", null)]
	[Help("en", "Gets the distance between two objects.", null)]
	[AddComponentMenu("")]
	public class GetDistance : Action, IExecutableInEditor
	{
		[SerializeField]
		private Transform _t1;

		[SerializeField]
		private Variable _t1Var;

		[SerializeField]
		private Transform _t2;

		[SerializeField]
		private Variable _t2Var;

		[SerializeField]
		private Variable _output;

		public override void Execute()
		{
			if (_output == null)
			{
				return;
			}
			Transform transform = _t1Var.GetValueAsComponent(_t1, typeof(Transform)) as Transform;
			if (!(transform == null))
			{
				Transform transform2 = _t2Var.GetValueAsComponent(_t2, typeof(Transform)) as Transform;
				if (!(transform2 == null))
				{
					float value = Vector3.Distance(transform.position, transform2.position);
					_output.SetValue(value);
				}
			}
		}
	}
}
