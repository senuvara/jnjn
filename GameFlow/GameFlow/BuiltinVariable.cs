using System;

namespace GameFlow
{
	public abstract class BuiltinVariable : Variable, IHideInSelector
	{
		public sealed override bool isReadOnly => true;

		public sealed override string id => GetVariableId();

		public abstract Type GetVariableType();

		protected abstract string GetVariableId();

		protected override void OnReset()
		{
			base.type = GetVariableType();
		}

		public override string GetIdForControls()
		{
			return GetVariableId();
		}

		public override string GetIdForSelectors()
		{
			return GetVariableId();
		}

		public override string GetTypeForSelectors()
		{
			return "Built-in Variable";
		}
	}
}
