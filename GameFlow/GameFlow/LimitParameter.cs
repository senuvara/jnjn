using UnityEngine;

namespace GameFlow
{
	[Help("es", "Limita el valor del parámetro (@GameFlow{Parameter}) especificado.", null)]
	[AddComponentMenu("")]
	[Help("en", "Limits the value of the specified @GameFlow{Parameter}.", null)]
	public class LimitParameter : LimitParameterValue
	{
	}
}
