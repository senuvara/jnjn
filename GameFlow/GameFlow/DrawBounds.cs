using UnityEngine;

namespace GameFlow
{
	[Help("en", "Draws the bounding box of the specified @UnityManual{Transform}.", null)]
	[AddComponentMenu("")]
	[Help("es", "Dibuja la caja que delimita al @UnityManual{Transform} especificado.", null)]
	public class DrawBounds : DrawGizmosAction
	{
		public enum TargetType
		{
			Transform,
			Mesh,
			Collider
		}

		[SerializeField]
		private TargetType _targetType;

		[SerializeField]
		private Transform _transform;

		[SerializeField]
		private Variable _transformVar;

		[SerializeField]
		private MeshRenderer _mesh;

		[SerializeField]
		private Variable _meshVar;

		[SerializeField]
		private Collider _collider;

		[SerializeField]
		private Variable _colliderVar;

		[SerializeField]
		private Color _selectedColor = new Color(1f, 1f, 0f, 0.75f);

		[SerializeField]
		private Variable _selectedColorVar;

		[SerializeField]
		private Color _unselectedColor = new Color(1f, 1f, 0f, 0.25f);

		[SerializeField]
		private Variable _unselectedColorVar;

		public TargetType targetType => _targetType;

		public new Transform transform => _transformVar.GetValueAsComponent(_transform, typeof(Transform)) as Transform;

		public MeshRenderer mesh => _meshVar.GetValueAsComponent(_mesh, typeof(MeshRenderer)) as MeshRenderer;

		public Collider target => _colliderVar.GetValueAsComponent(_collider, typeof(Collider)) as Collider;

		public Color selectedColor => _selectedColorVar.GetValue(_selectedColor);

		public Color unselectedColor => _unselectedColorVar.GetValue(_unselectedColor);

		protected override void OnReset()
		{
			_transform = base.gameObject.GetComponent<Transform>();
			_collider = base.gameObject.GetComponent<Collider>();
			_mesh = base.gameObject.GetComponent<MeshRenderer>();
		}
	}
}
