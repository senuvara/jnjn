using UnityEngine;

namespace GameFlow
{
	[Help("en", "Destroys the objects contained in the specified @GameFlow{List}.", null)]
	[Help("es", "Destruye los objetos contenidos en la @GameFlow{List} especificada.", null)]
	[AddComponentMenu("")]
	public class DestroyGameObjectsInList : ListAction, IExecutableInEditor
	{
		public override void Execute()
		{
			Execute(base.list);
		}

		public static void Execute(List list)
		{
			if (list == null || list.dataType != DataType.Object)
			{
				return;
			}
			for (int i = 0; i < list.Count; i++)
			{
				GameObject gameObject = list.GetObjectAt(i) as GameObject;
				if (gameObject != null)
				{
					GameFlow.Destroy.Execute(gameObject);
				}
			}
		}
	}
}
