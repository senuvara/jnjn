using UnityEngine;

namespace GameFlow
{
	[AddComponentMenu("")]
	[Help("en", "Adds the indicated GameObject and its hierarchy to the specified @GameFlow{List}.", null)]
	[Help("es", "Añade el GameObject indicado y su jerarquía a la lista (@GameFlow{List}) especificada.", null)]
	public class AddHierarchyToList : ListAction
	{
		[SerializeField]
		private GameObject _hierarchyRoot;

		[SerializeField]
		private Variable _hierarchyRootVar;

		[SerializeField]
		private bool _onlyActive;

		[SerializeField]
		private Variable _onlyActiveVar;

		public GameObject hierarchyRoot => _hierarchyRootVar.GetValue(_hierarchyRoot) as GameObject;

		public bool onlyActive => _onlyActiveVar.GetValue(_onlyActive);

		public override void Execute()
		{
			List list = GetList();
			if (!(list == null))
			{
				GameObject hierarchyRoot = this.hierarchyRoot;
				if (!(hierarchyRoot == null) && (!onlyActive || hierarchyRoot.activeInHierarchy))
				{
					list.AddGameObjectHierarchy(hierarchyRoot, onlyActive);
				}
			}
		}
	}
}
