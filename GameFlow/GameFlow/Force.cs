using UnityEngine;

namespace GameFlow
{
	[AddComponentMenu("GameFlow/Tools/Force")]
	[Help("es", "Define una fuerza.", null)]
	[Help("en", "Defines a force.", null)]
	public class Force : Block, IIdentifiable, ITool, IVariableFriendly
	{
		public enum ForceAnchor
		{
			Start,
			Middle,
			End
		}

		[SerializeField]
		private string _id = string.Empty;

		[SerializeField]
		private float _magnitude = 1f;

		[SerializeField]
		private Variable _magnitudeVar;

		[SerializeField]
		private Vector3 _direction = new Vector3(0f, 0f, 1f);

		[SerializeField]
		private Variable _directionVar;

		[SerializeField]
		private Color _color = Color.cyan;

		private Color _unselectedColor;

		[SerializeField]
		private float _scale = 1f;

		[SerializeField]
		private ForceAnchor _anchor;

		public string id => _id;

		public float magnitude
		{
			get
			{
				return _magnitudeVar.GetValue(_magnitude);
			}
			set
			{
				_magnitudeVar = null;
				_magnitude = value;
			}
		}

		public Vector3 direction
		{
			get
			{
				return base.transform.TransformDirection(_directionVar.GetValue(_direction));
			}
			set
			{
				_directionVar = null;
				_direction = value;
			}
		}

		public Color color
		{
			get
			{
				return _color;
			}
			set
			{
				_color = value;
				RecalcUnselectedColor();
			}
		}

		public Color unselectedColor => _unselectedColor;

		public float scale => _scale;

		public ForceAnchor anchor => _anchor;

		protected override void OnReset()
		{
			base.enabled = false;
			SetContainer();
			if (_id == string.Empty)
			{
				_id = "Force" + Random.Range(1000, 9999);
			}
			RecalcUnselectedColor();
		}

		private void SetContainer()
		{
			if (container != null || !BlockInsertion.enabled || BlockInsertion.clipboard)
			{
				return;
			}
			IForceContainer forceContainer = BlockInsertion.target as IForceContainer;
			if (forceContainer == null)
			{
				forceContainer = base.gameObject.GetComponent<Forces>();
				if (forceContainer == null)
				{
					forceContainer = base.gameObject.AddComponent<Forces>();
				}
				BlockInsertion.index = -1;
			}
			if (BlockInsertion.index < 0 || BlockInsertion.index >= forceContainer.GetForces().Count)
			{
				if (!forceContainer.Contains(this))
				{
					forceContainer.GetForces().Add(this);
					forceContainer.ForceAdded(this);
					forceContainer.SetDirty(dirty: true);
				}
			}
			else
			{
				forceContainer.GetForces().Insert(BlockInsertion.index, this);
				forceContainer.ForceAdded(this);
				forceContainer.SetDirty(dirty: true);
			}
			container = (forceContainer as Object);
			BlockInsertion.Reset();
		}

		private void Start()
		{
		}

		private void OnValidate()
		{
			RecalcUnselectedColor();
		}

		private void RecalcUnselectedColor()
		{
			_unselectedColor = ColorUtils.CreateColor(_color, 0.3f);
		}

		public string GetIdForControls()
		{
			return _id;
		}

		public string GetIdForSelectors()
		{
			return _id;
		}

		public virtual string GetTypeForSelectors()
		{
			return "Force";
		}
	}
}
