using System;
using System.Collections.Generic;
using UnityEngine;

namespace GameFlow
{
	[Help("en", "Defines a path or trajectory from a list of points.", null)]
	[AddComponentMenu("GameFlow/Tools/Path")]
	[Help("es", "Define una ruta o trayectoria a partir de una lista de puntos.", null)]
	public class Path : List, ITool
	{
		[SerializeField]
		private PathType _pathType;

		[SerializeField]
		private int _subdivisions = 10;

		[SerializeField]
		private bool _closed;

		[SerializeField]
		private Color _color = Color.yellow;

		[SerializeField]
		private bool _showLabels = true;

		[SerializeField]
		private bool _settingsFoldout = true;

		[SerializeField]
		private bool _pointsFoldout = true;

		private Color _unselColor;

		private bool _dirty;

		private List<Transform> _transformList = new List<Transform>();

		private Transform[] _transforms;

		private static float POINT_RADIUS = 0.075f;

		private static float CROSSHAIRS_RADIUS = POINT_RADIUS * 2.5f;

		private Segment[] _segments = new Segment[0];

		private float _length;

		private TransformSnapshot[] _snapshots;

		public PathType pathType
		{
			get
			{
				return _pathType;
			}
			set
			{
				_pathType = value;
			}
		}

		public int subdivisions
		{
			get
			{
				return _subdivisions;
			}
			set
			{
				_subdivisions = value;
			}
		}

		public bool closed
		{
			get
			{
				return _closed;
			}
			set
			{
				_closed = value;
			}
		}

		public Color color
		{
			get
			{
				return _color;
			}
			set
			{
				_color = value;
			}
		}

		public Segment[] segments
		{
			get
			{
				if (IsDirty())
				{
					Recalculate();
				}
				return _segments;
			}
		}

		public float length
		{
			get
			{
				if (IsDirty())
				{
					Recalculate();
				}
				return _length;
			}
		}

		public override Type GetDefaultType()
		{
			return typeof(Transform);
		}

		private void Awake()
		{
			List.proxy = null;
		}

		private void OnValidate()
		{
			_unselColor = ColorUtils.CreateColor(_color, 0.25f);
		}

		public Transform GetTransformAt(int index)
		{
			return GetObjectAt(index) as Transform;
		}

		private void OnDrawGizmos()
		{
			if (!HierarchyUtils.SelectionContains(base.gameObject))
			{
				DrawPath(_unselColor);
				DrawPointMarkers(_unselColor, hightlightSelected: true, _color);
			}
		}

		private void OnDrawGizmosSelected()
		{
			DrawPath(_color);
			DrawPointMarkers(_color, hightlightSelected: false, _color);
		}

		private void DrawPath(Color color)
		{
			Gizmos.color = color;
			for (int i = 0; i < segments.Length; i++)
			{
				Segment segment = segments[i];
				Gizmos.DrawLine(segment.startPosition, segment.endPosition);
			}
		}

		private void DrawPointMarkers(Color color, bool hightlightSelected, Color selColor)
		{
			for (int num = IndexOfNextNonNullObjectFrom(0); num >= 0; num = IndexOfNextNonNullObjectFrom(num + 1))
			{
				Transform transformAt = GetTransformAt(num);
				if (transformAt != null)
				{
					bool flag = HierarchyUtils.SelectionContains(transformAt.gameObject);
					Gizmos.color = ((!hightlightSelected || !flag) ? color : selColor);
					GizmosUtils.DrawSelectableAreaForPointAt(transformAt.position, POINT_RADIUS);
					GizmosUtils.Draw3DCrossHairs(transformAt.position, CROSSHAIRS_RADIUS);
					if (!hightlightSelected || flag)
					{
						GizmosUtils.SetHandlesColor(Color.magenta);
						float handleSize = GizmosUtils.GetHandleSize(transformAt.position);
						GizmosUtils.DrawArrowCap(0, transformAt.position, transformAt.rotation, handleSize);
					}
					if (!_showLabels)
					{
					}
				}
			}
		}

		private bool IsDirty()
		{
			return _dirty || (base.Count > 0 && _segments.Length == 0) || (!Application.isPlaying && AnySnapshotDiffers());
		}

		private void Recalculate()
		{
			FilterNonNullTransforms();
			switch (_pathType)
			{
			case PathType.Spline:
				CalcSplineSegments();
				break;
			case PathType.Linear:
				CalcLinearSegments();
				break;
			}
			CalcTotalLength();
			if (!Application.isPlaying)
			{
				CreateSnapshots();
			}
			_dirty = false;
		}

		private void FilterNonNullTransforms()
		{
			_transformList.Clear();
			for (int num = IndexOfNextNonNullObjectFrom(0); num >= 0; num = IndexOfNextNonNullObjectFrom(num + 1))
			{
				_transformList.Add(GetTransformAt(num));
			}
			_transforms = _transformList.ToArray();
		}

		private void CreateSnapshots()
		{
			_snapshots = new TransformSnapshot[_transforms.Length];
			for (int i = 0; i < _snapshots.Length; i++)
			{
				_snapshots[i] = new TransformSnapshot(_transforms[i]);
			}
		}

		private bool AnySnapshotDiffers()
		{
			if (_snapshots == null)
			{
				return false;
			}
			for (int i = 0; i < _snapshots.Length; i++)
			{
				if (_snapshots[i].Differs())
				{
					return true;
				}
			}
			return false;
		}

		private int GetSegmentCount()
		{
			int num = _transforms.Length;
			if (num >= 3)
			{
				return (!_closed) ? (num - 1) : num;
			}
			return (num > 1) ? (num - 1) : 0;
		}

		private void CalcSplineSegments()
		{
			int num = GetSegmentCount() * _subdivisions;
			_segments = new Segment[num];
			int num2 = _transforms.Length;
			float num3 = 0f;
			if (num2 == 2)
			{
				CalcSplineSection(0, num3, 0, 0, 1, 1);
			}
			else if (num2 > 2)
			{
				int section = 0;
				num3 = (_closed ? (num3 + CalcSplineSection(section++, num3, num2 - 1, 0, 1, 2)) : (num3 + CalcSplineSection(section++, num3, 0, 0, 1, 2)));
				for (int i = 1; i < num2 - 2; i++)
				{
					num3 += CalcSplineSection(section++, num3, i - 1, i, i + 1, i + 2);
				}
				if (!_closed)
				{
					CalcSplineSection(section, num3, num2 - 3, num2 - 2, num2 - 1, num2 - 1);
					return;
				}
				num3 += CalcSplineSection(section++, num3, num2 - 3, num2 - 2, num2 - 1, 0);
				CalcSplineSection(section, num3, num2 - 2, num2 - 1, 0, 1);
			}
		}

		private float CalcSplineSection(int section, float offset, int pi1, int pi2, int pi3, int pi4)
		{
			Vector3 position = _transforms[pi1].position;
			Vector3 position2 = _transforms[pi2].position;
			Quaternion rotation = _transforms[pi2].rotation;
			Vector3 localScale = _transforms[pi2].localScale;
			Vector3 position3 = _transforms[pi3].position;
			Quaternion rotation2 = _transforms[pi3].rotation;
			Vector3 localScale2 = _transforms[pi3].localScale;
			Vector3 position4 = _transforms[pi4].position;
			float num = offset;
			float num2 = 0f;
			float num3 = 1f / (float)_subdivisions;
			int num4 = section * _subdivisions;
			Vector3 startPosition = position2;
			Quaternion startRotation = rotation;
			Vector3 startScale = localScale;
			for (int i = 0; i < _subdivisions - 1; i++)
			{
				num2 += num3;
				Vector3 catmullRomSplinePoint = PathUtils.GetCatmullRomSplinePoint(position, position2, position3, position4, num2);
				Quaternion quaternion = Quaternion.Slerp(rotation, rotation2, num2);
				Vector3 vector = Vector3.Lerp(localScale, localScale2, num2);
				_segments[num4 + i] = new Segment(startPosition, catmullRomSplinePoint, startRotation, quaternion, startScale, vector, offset);
				offset += _segments[num4 + i].length;
				startPosition = catmullRomSplinePoint;
				startRotation = quaternion;
				startScale = vector;
			}
			_segments[num4 + _subdivisions - 1] = new Segment(startPosition, position3, startRotation, rotation2, startScale, localScale2, offset);
			offset += _segments[num4 + _subdivisions - 1].length;
			return offset - num;
		}

		private void CalcLinearSegments()
		{
			int segmentCount = GetSegmentCount();
			_segments = new Segment[segmentCount];
			if (segmentCount == 1)
			{
				Transform transform = _transforms[0];
				Transform transform2 = _transforms[1];
				_segments[0] = new Segment(transform.position, transform2.position, transform.rotation, transform2.rotation, transform.localScale, transform2.localScale, 0f);
			}
			else if (segmentCount > 1)
			{
				int num = 0;
				int num2 = _transforms.Length - 1;
				float num3 = 0f;
				for (int i = 0; i < num2; i++)
				{
					Transform transform3 = _transforms[i];
					Transform transform4 = _transforms[i + 1];
					_segments[num] = new Segment(transform3.position, transform4.position, transform3.rotation, transform4.rotation, transform3.localScale, transform4.localScale, num3);
					num3 += _segments[num++].length;
				}
				if (_closed)
				{
					Transform transform5 = _transforms[num2];
					Transform transform6 = _transforms[0];
					_segments[num] = new Segment(transform5.position, transform6.position, transform5.rotation, transform6.rotation, transform5.localScale, transform6.localScale, num3);
				}
			}
		}

		private void CalcTotalLength()
		{
			_length = 0f;
			for (int i = 0; i < _segments.Length; i++)
			{
				_length += _segments[i].length;
			}
		}

		public Segment GetSegment(float t)
		{
			float num = length * Mathf.Clamp01(t);
			if (IsDirty())
			{
				Recalculate();
			}
			for (int i = 0; i < segments.Length; i++)
			{
				Segment segment = _segments[i];
				if (num <= segment.limit)
				{
					return segment;
				}
			}
			return null;
		}

		public void SetDirty()
		{
			_dirty = true;
		}

		public override string GetTypeForSelectors()
		{
			return "Path";
		}

		public void AddPoint(Transform t)
		{
			AddValue(t);
		}
	}
}
