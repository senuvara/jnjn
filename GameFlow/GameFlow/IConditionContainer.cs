using System.Collections.Generic;

namespace GameFlow
{
	public interface IConditionContainer : IBlockContainer
	{
		List<Condition> GetConditions();

		void ConditionAdded(Condition condition);
	}
}
