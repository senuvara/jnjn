using UnityEngine;

namespace GameFlow
{
	[AddComponentMenu("")]
	[Help("es", "Lee una propiedad del componente @UnityManual{AudioReverbFilter} especificado.", null)]
	[Help("en", "Gets a property of the specified @UnityManual{AudioReverbFilter} component.", null)]
	public class GetAudioReverbFilterProperty : Action, IExecutableInEditor
	{
		public enum Property
		{
			DecayHFRatio,
			DecayTime,
			Density,
			Diffusion,
			DryLevel,
			HfReference,
			LfReference,
			ReflectionsDelay,
			ReflectionsLevel,
			ReverbDelay,
			ReverbLevel,
			ReverbPreset,
			Room,
			RoomHF,
			RoomLF,
			RoomRolloff
		}

		[SerializeField]
		private AudioReverbFilter _source;

		[SerializeField]
		private Variable _sourceVar;

		[SerializeField]
		private Property _property;

		[SerializeField]
		private Variable _output;

		protected override void OnReset()
		{
			_source = base.gameObject.GetComponent<AudioReverbFilter>();
		}

		public override void Setup()
		{
		}

		public override void Execute()
		{
			AudioReverbFilter audioReverbFilter = _sourceVar.GetValueAsComponent(_source, typeof(AudioReverbFilter)) as AudioReverbFilter;
			if (!(audioReverbFilter == null))
			{
				switch (_property)
				{
				case Property.DecayHFRatio:
					_output.SetValue(audioReverbFilter.decayHFRatio);
					break;
				case Property.DecayTime:
					_output.SetValue(audioReverbFilter.decayTime);
					break;
				case Property.Density:
					_output.SetValue(audioReverbFilter.density);
					break;
				case Property.Diffusion:
					_output.SetValue(audioReverbFilter.diffusion);
					break;
				case Property.DryLevel:
					_output.SetValue(audioReverbFilter.dryLevel);
					break;
				case Property.HfReference:
					_output.SetValue(audioReverbFilter.hfReference);
					break;
				case Property.LfReference:
					_output.SetValue(audioReverbFilter.lfReference);
					break;
				case Property.ReflectionsDelay:
					_output.SetValue(audioReverbFilter.reflectionsDelay);
					break;
				case Property.ReflectionsLevel:
					_output.SetValue(audioReverbFilter.reflectionsLevel);
					break;
				case Property.ReverbDelay:
					_output.SetValue(audioReverbFilter.reverbDelay);
					break;
				case Property.ReverbLevel:
					_output.SetValue(audioReverbFilter.reverbLevel);
					break;
				case Property.ReverbPreset:
					_output.SetValue(audioReverbFilter.reverbPreset);
					break;
				case Property.Room:
					_output.SetValue(audioReverbFilter.room);
					break;
				case Property.RoomHF:
					_output.SetValue(audioReverbFilter.roomHF);
					break;
				case Property.RoomLF:
					_output.SetValue(audioReverbFilter.roomLF);
					break;
				case Property.RoomRolloff:
					_output.SetValue(audioReverbFilter.get_roomRolloff());
					break;
				}
			}
		}
	}
}
