using UnityEngine;

namespace GameFlow
{
	[Help("es", "Lee una propiedad del componente @UnityManual{Projector} especificado.", null)]
	[Help("en", "Gets a property of the specified @UnityManual{Projector} component.", null)]
	[AddComponentMenu("")]
	public class GetProjectorProperty : Action, IExecutableInEditor
	{
		public enum Property
		{
			AspectRatio,
			FarClipPlane,
			FieldOfView,
			IgnoreLayers,
			Material,
			NearClipPlane,
			Orthographic,
			OrthographicSize
		}

		[SerializeField]
		private Projector _source;

		[SerializeField]
		private Variable _sourceVar;

		[SerializeField]
		private Property _property;

		[SerializeField]
		private Variable _output;

		protected override void OnReset()
		{
			_source = base.gameObject.GetComponent<Projector>();
		}

		public override void Setup()
		{
		}

		public override void Execute()
		{
			Projector projector = _sourceVar.GetValueAsComponent(_source, typeof(Projector)) as Projector;
			if (!(projector == null))
			{
				switch (_property)
				{
				case Property.AspectRatio:
					_output.SetValue(projector.aspectRatio);
					break;
				case Property.FarClipPlane:
					_output.SetValue(projector.farClipPlane);
					break;
				case Property.FieldOfView:
					_output.SetValue(projector.fieldOfView);
					break;
				case Property.IgnoreLayers:
					_output.SetValue(projector.ignoreLayers);
					break;
				case Property.Material:
					_output.SetValue(projector.material);
					break;
				case Property.NearClipPlane:
					_output.SetValue(projector.nearClipPlane);
					break;
				case Property.Orthographic:
					_output.SetValue(projector.orthographic);
					break;
				case Property.OrthographicSize:
					_output.SetValue(projector.orthographicSize);
					break;
				}
			}
		}
	}
}
