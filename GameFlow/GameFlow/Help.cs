using System;

namespace GameFlow
{
	[AttributeUsage(AttributeTargets.Class, AllowMultiple = true)]
	public class Help : Attribute
	{
		public string code
		{
			get;
			private set;
		}

		public string text
		{
			get;
			private set;
		}

		public string url
		{
			get;
			private set;
		}

		public Help(string code, string text, string url = null)
		{
			this.code = code;
			this.text = text;
			this.url = url;
		}
	}
}
