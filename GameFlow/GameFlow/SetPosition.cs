using UnityEngine;

namespace GameFlow
{
	[Help("es", "Modifica la posición 3D del objeto especificado.", null)]
	[Help("en", "Sets the 3D world position of the specified object.", null)]
	[AddComponentMenu("")]
	public class SetPosition : TransformAction
	{
		[SerializeField]
		private Vector3 _position;

		[SerializeField]
		private Variable _positionVar;

		public Vector3 position => _positionVar.GetValue(_position);

		public override void Execute()
		{
			Execute(base.target, base.space, position, base.relative, base.multiplier, base.mask);
		}

		public static void Execute(Transform t, Space space, Vector3 position, bool relative, float multiplier, Vector3 mask)
		{
			if (t == null)
			{
				return;
			}
			if (relative)
			{
				t.position = t.position.Masked(t.position + position * multiplier, mask);
				return;
			}
			switch (space)
			{
			case Space.World:
				t.position = t.position.Masked(position, mask);
				break;
			case Space.Self:
				t.localPosition = t.localPosition.Masked(position, mask);
				break;
			}
		}
	}
}
