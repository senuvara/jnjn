using UnityEngine;

namespace GameFlow
{
	[AddComponentMenu("GameFlow/Programs/On Game Over")]
	[DisallowMultipleComponent]
	[Help("es", "Programa que se ejecutará cuando se reciba una notificación por parte de @GameFlow{GameOver}.", null)]
	[Help("en", "Program that will be executed when a notification from @GameFlow{GameOver} is received.", null)]
	public class OnGameOver : EventProgram
	{
		protected override void RegisterAsListener()
		{
			GameOverEvent.AddListener(this);
		}

		protected override void UnRegisterAsListener()
		{
			GameOverEvent.RemoveListener(this);
		}
	}
}
