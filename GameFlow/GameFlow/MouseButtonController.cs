using UnityEngine;

namespace GameFlow
{
	[Help("es", "Controla los botones del ratón.", null)]
	[Help("en", "Controls the mouse buttons.", null)]
	[DisallowMultipleComponent]
	[AddComponentMenu("")]
	public class MouseButtonController : BaseBehaviour, ITool
	{
		private static MouseButtonController _instance;

		private bool[] _isDown = new bool[3];

		private bool[] _isJustDown = new bool[3];

		private bool[] _isJustUp = new bool[3];

		private float[] _justTimestamp = new float[3];

		private static MouseButtonController instance
		{
			get
			{
				Init();
				return _instance;
			}
		}

		protected override void OnReset()
		{
			HideInInspector();
		}

		private static void Init()
		{
			if (_instance == null)
			{
				GameObject gameObject = new GameObject("Mouse Button Controller");
				gameObject.hideFlags = HideFlags.HideInHierarchy;
				_instance = gameObject.AddComponent<MouseButtonController>();
			}
		}

		public static bool IsButtonDown(MouseButton mouseButton)
		{
			return instance._isDown[(int)mouseButton];
		}

		public static bool IsButtonJustDown(MouseButton mouseButton)
		{
			return instance._isJustDown[(int)mouseButton];
		}

		public static bool IsButtonJustUp(MouseButton mouseButton)
		{
			return instance._isJustUp[(int)mouseButton];
		}

		public static bool IsButtonUp(MouseButton mouseButton)
		{
			return !instance._isDown[(int)mouseButton];
		}

		private void Update()
		{
			float time = Time.time;
			bool flag = false;
			for (int i = 0; i < 3; i++)
			{
				_isDown[i] = Input.GetMouseButton(i);
				_isJustDown[i] = Input.GetMouseButtonDown(i);
				if (_isJustDown[i])
				{
					_justTimestamp[i] = Time.time;
					flag = true;
				}
				_isJustUp[i] = Input.GetMouseButtonUp(i);
			}
			if (!flag)
			{
				return;
			}
			for (int j = 0; j < 3; j++)
			{
				if (!_isJustDown[j])
				{
					float num = time - _justTimestamp[j];
					if (num <= 0.05f)
					{
						_isJustDown[j] = true;
						_isJustUp[j] = false;
						_isDown[j] = true;
					}
				}
			}
		}
	}
}
