using System.Collections;

namespace GameFlow
{
	public class Comparers
	{
		public class StringComparer : IComparer
		{
			public bool descendent;

			int IComparer.Compare(object obj1, object obj2)
			{
				string text = (string)obj1;
				string text2 = (string)obj2;
				return descendent ? text2.CompareTo(text) : text.CompareTo(text2);
			}
		}

		private static StringComparer _stringComparer;

		public static StringComparer stringComparer
		{
			get
			{
				if (_stringComparer == null)
				{
					_stringComparer = new StringComparer();
				}
				return _stringComparer;
			}
		}
	}
}
