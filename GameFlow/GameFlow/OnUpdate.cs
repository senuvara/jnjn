using UnityEngine;

namespace GameFlow
{
	[Help("en", "Program that will be executed, if enabled, every frame.", null)]
	[AddComponentMenu("GameFlow/Programs/On Update")]
	[Help("es", "Programa que se ejecutará, si está activo, en cada fotograma.", null)]
	[DisallowMultipleComponent]
	public class OnUpdate : Program
	{
		private void Update()
		{
			Restart();
			Step();
		}

		protected override bool WillFireEventOnProgramFinished()
		{
			return false;
		}
	}
}
