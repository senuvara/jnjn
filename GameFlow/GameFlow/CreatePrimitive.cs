using UnityEngine;

namespace GameFlow
{
	[AddComponentMenu("")]
	[Help("es", "Crea un nuevo @UnityManual{GameObject} de tipo primitiva con el nombre especificado.", null)]
	[Help("en", "Creates a new primitive-type @UnityManual{GameObject} with the specified name.", null)]
	public class CreatePrimitive : Action, IExecutableInEditor
	{
		[SerializeField]
		private PrimitiveType _primitiveType = PrimitiveType.Cube;

		[SerializeField]
		private Variable _primitiveTypeVar;

		[SerializeField]
		private Variable _output;

		public PrimitiveType primitiveType => (PrimitiveType)(object)_primitiveTypeVar.GetValue(_primitiveType);

		public Variable output => _output;

		public override void Step()
		{
			GameObject value = GameObject.CreatePrimitive(primitiveType);
			if (_output != null)
			{
				_output.SetValue(value);
			}
		}
	}
}
