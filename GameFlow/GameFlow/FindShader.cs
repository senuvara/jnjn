using UnityEngine;

namespace GameFlow
{
	[Help("en", "Finds a Shader with the given name.", null)]
	[AddComponentMenu("")]
	[Help("es", "Busca un Shader con el nombre dado.", null)]
	public class FindShader : Action, IExecutableInEditor
	{
		[SerializeField]
		private string _name;

		[SerializeField]
		private Variable _nameVar;

		[SerializeField]
		private Variable _output;

		public override void Execute()
		{
			if (!(_output == null))
			{
				Shader value = Execute(_nameVar.GetValue(_name));
				_output.SetValue(value, typeof(Shader));
			}
		}

		public static Shader Execute(string name)
		{
			if (name == null || name.Length == 0)
			{
				return null;
			}
			return Shader.Find(name);
		}
	}
}
