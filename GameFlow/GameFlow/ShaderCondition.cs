using UnityEngine;

namespace GameFlow
{
	[AddComponentMenu("")]
	[Help("en", "Evaluates whether the specified Shader is supported.", null)]
	[Help("es", "Evalúa si el Shader especificado está soportado.", null)]
	public class ShaderCondition : Condition
	{
		public enum Comparison
		{
			IsSupported,
			IsNotSupported
		}

		[SerializeField]
		private Shader _shader;

		[SerializeField]
		private Variable _shaderVar;

		[SerializeField]
		private Comparison _comparison;

		public override bool Evaluate()
		{
			Shader shader = _shaderVar.GetValue(_shader) as Shader;
			if (shader == null)
			{
				return false;
			}
			switch (_comparison)
			{
			case Comparison.IsSupported:
				return shader.isSupported;
			case Comparison.IsNotSupported:
				return !shader.isSupported;
			default:
				return false;
			}
		}
	}
}
