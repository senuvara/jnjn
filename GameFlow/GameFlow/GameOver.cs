using UnityEngine;

namespace GameFlow
{
	[Help("en", "Notifies the game is over.", null)]
	[AddComponentMenu("")]
	[Help("es", "Notifica que el juego ha terminado.", null)]
	public class GameOver : Action
	{
		private static LocalizedString alreadyOver = LocalizedString.Create().Add("en", "Ignoring 'Game Over' action: Game is already over.").Add("es", "Ignorando accion 'Game Over': El juego ya ha terminado.");

		private static LocalizedString nonStarted = LocalizedString.Create().Add("en", "Ignoring 'Game Over' action: Game is not started.").Add("es", "Ignorando accion 'Game Over': El juego aún no ha sido iniciado.");

		public override void Execute()
		{
			if (Game.over)
			{
				Log.Warning(alreadyOver, base.gameObject);
			}
			else if (!Game.started)
			{
				Log.Warning(nonStarted, base.gameObject);
			}
			Game.Finish();
		}
	}
}
