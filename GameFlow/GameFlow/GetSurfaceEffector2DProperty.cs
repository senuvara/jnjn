using UnityEngine;

namespace GameFlow
{
	[Help("en", "Gets a property of the specified @UnityManual{SurfaceEffector2D} component.", null)]
	[AddComponentMenu("")]
	[Help("es", "Lee una propiedad del componente @UnityManual{SurfaceEffector2D} especificado.", null)]
	public class GetSurfaceEffector2DProperty : Action, IExecutableInEditor
	{
		public enum Property
		{
			ColliderMask,
			ForceScale,
			Speed,
			SpeedVariation,
			UseBounce,
			UseColliderMask,
			UseContactForce,
			UseFriction
		}

		[SerializeField]
		private SurfaceEffector2D _source;

		[SerializeField]
		private Variable _sourceVar;

		[SerializeField]
		private Property _property;

		[SerializeField]
		private Variable _output;

		protected override void OnReset()
		{
			_source = base.gameObject.GetComponent<SurfaceEffector2D>();
		}

		public override void Setup()
		{
		}

		public override void Execute()
		{
			SurfaceEffector2D surfaceEffector2D = _sourceVar.GetValueAsComponent(_source, typeof(SurfaceEffector2D)) as SurfaceEffector2D;
			if (!(surfaceEffector2D == null))
			{
				switch (_property)
				{
				case Property.ColliderMask:
					_output.SetValue(surfaceEffector2D.colliderMask);
					break;
				case Property.ForceScale:
					_output.SetValue(surfaceEffector2D.forceScale);
					break;
				case Property.Speed:
					_output.SetValue(surfaceEffector2D.speed);
					break;
				case Property.SpeedVariation:
					_output.SetValue(surfaceEffector2D.speedVariation);
					break;
				case Property.UseBounce:
					_output.SetValue(surfaceEffector2D.useBounce);
					break;
				case Property.UseColliderMask:
					_output.SetValue(surfaceEffector2D.useColliderMask);
					break;
				case Property.UseContactForce:
					_output.SetValue(surfaceEffector2D.useContactForce);
					break;
				case Property.UseFriction:
					_output.SetValue(surfaceEffector2D.useFriction);
					break;
				}
			}
		}
	}
}
