using UnityEngine;

namespace GameFlow
{
	public static class RectExt
	{
		public static Rect zero = new Rect(0f, 0f, 0f, 0f);

		public static bool IsMouseOver(this Rect rect)
		{
			return rect.Contains(Event.current.mousePosition);
		}

		public static Rect Add(this Rect rect, RectOffset offset)
		{
			rect.Set(rect.x + (float)offset.left, rect.y + (float)offset.top, rect.width - (float)offset.left + (float)offset.right, rect.height - (float)offset.top + (float)offset.bottom);
			return rect;
		}

		public static Rect Increase(this Rect rect, float width, float height)
		{
			rect.Set(rect.x, rect.y, rect.width + width, rect.height + height);
			return rect;
		}

		public static Rect Move(this Rect rect, float left, float top, bool relative = true)
		{
			if (relative)
			{
				rect.Set(rect.x + left, rect.y + top, rect.width, rect.height);
			}
			else
			{
				rect.Set(left, top, rect.width, rect.height);
			}
			return rect;
		}

		public static Rect Add(this Rect rect, Rect other)
		{
			rect.Set(rect.x + other.x, rect.y + other.y, rect.width + other.width, rect.height + other.height);
			return rect;
		}

		public static Rect Substract(this Rect rect, Rect other)
		{
			rect.Set(rect.x - other.x, rect.y - other.y, rect.width - other.width, rect.height - other.height);
			return rect;
		}

		public static Rect Multiply(this Rect rect, Rect multiplier)
		{
			return new Rect(rect.x * multiplier.x, rect.y * multiplier.y, rect.width * multiplier.width, rect.height * multiplier.height);
		}

		public static Rect Divide(this Rect rect, Rect multiplier)
		{
			return new Rect(rect.x / multiplier.x, rect.y / multiplier.y, rect.width / multiplier.width, rect.height / multiplier.height);
		}

		public static Rect GetRectTrimmedToText(this Rect rect, string text, GUIStyle style)
		{
			Vector2 vector = style.CalcSize(new GUIContent(text));
			Rect result = rect;
			result.width = vector.x;
			return result;
		}

		public static Rect GetIndentedRect(this Rect rect, float leftIndent = 0f, float rightIndent = 0f)
		{
			Rect result = rect;
			result.x += leftIndent;
			result.width -= leftIndent;
			result.width -= rightIndent;
			return result;
		}
	}
}
