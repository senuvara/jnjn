using UnityEngine;

namespace GameFlow
{
	[Help("es", "Activa el programa (@GameFlow{Program}) especificado.", null)]
	[Help("en", "Enables the specified @GameFlow{Program}.", null)]
	[AddComponentMenu("")]
	public class EnableProgram : Action, IExecutableInEditor
	{
		[SerializeField]
		private Program _target;

		[SerializeField]
		private Variable _targetVar;

		public override void Execute()
		{
			Execute(_targetVar.GetValue(_target) as Program);
		}

		public static void Execute(Program program)
		{
			if (program != null)
			{
				program.enabled = true;
			}
		}
	}
}
