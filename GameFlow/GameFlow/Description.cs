using UnityEngine;

namespace GameFlow
{
	[AddComponentMenu("GameFlow/Tools/Description")]
	[Help("es", "Define un texto de descripción en el inspector.", null)]
	[Help("en", "Defines a description text in the Inspector.", null)]
	public class Description : BaseBehaviour, ITool
	{
		[SerializeField]
		[TextArea(2, 1024)]
		private string _text = "Description here.";

		public string text
		{
			get
			{
				return _text;
			}
			set
			{
				_text = value;
			}
		}
	}
}
