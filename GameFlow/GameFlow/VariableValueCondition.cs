using System;
using UnityEngine;

namespace GameFlow
{
	[AddComponentMenu("")]
	[Help("es", "Evalúa el valor de la variable especificada.", null)]
	[Help("en", "Evaluates the value of the specified variable.", null)]
	public class VariableValueCondition : Condition
	{
		public enum Comparison
		{
			IsEqualTo,
			IsNotEqualTo,
			IsGreaterThan,
			IsGreaterThanOrEqualTo,
			IsLessThan,
			IsLessThanOrEqualTo
		}

		public enum EqualComparison
		{
			IsEqualTo,
			IsNotEqualTo
		}

		public enum ObjectComparison
		{
			IsEqualTo,
			IsNotEqualTo,
			IsNone,
			IsNotNone
		}

		public enum StringComparison
		{
			IsEqualTo,
			IsNotEqualTo,
			IsGreaterThan,
			IsGreaterThanOrEqualTo,
			IsLessThan,
			IsLessThanOrEqualTo,
			Contains,
			StartsWith,
			EndsWith,
			IsEmpty,
			IsNotEmpty
		}

		[SerializeField]
		private Variable _variable;

		[SerializeField]
		private Comparison _comparison;

		[SerializeField]
		private EqualComparison _equalComparison;

		[SerializeField]
		private ObjectComparison _objectComparison;

		[SerializeField]
		private StringComparison _stringComparison;

		[SerializeField]
		private string _stringValue;

		[SerializeField]
		private int _intValue;

		[SerializeField]
		private float _floatValue;

		[SerializeField]
		private bool _boolValue;

		[SerializeField]
		private Rect _rectValue;

		[SerializeField]
		private Vector2 _vector2Value;

		[SerializeField]
		private Vector3 _vector3Value;

		[SerializeField]
		private Color _colorValue;

		[SerializeField]
		private UnityEngine.Object _objectValue;

		[SerializeField]
		private int _enumIntValue;

		[SerializeField]
		private Bounds _boundsValue;

		[SerializeField]
		private Quaternion _quaternionValue;

		[SerializeField]
		private Vector4 _vector4Value;

		[SerializeField]
		private Variable _valueVar;

		[SerializeField]
		private bool _caseSensitive = true;

		public override bool Evaluate()
		{
			if (_variable == null)
			{
				return false;
			}
			Variable variable = (!_variable.isIndirect) ? _variable : _variable.indirection;
			if (variable == null)
			{
				return false;
			}
			switch (variable.dataType)
			{
			case DataType.String:
			{
				string value8 = _valueVar.GetValue(_stringValue);
				switch (_stringComparison)
				{
				case StringComparison.IsEqualTo:
					return string.Equals(variable.stringValue, value8, (!_caseSensitive) ? System.StringComparison.CurrentCultureIgnoreCase : System.StringComparison.CurrentCulture);
				case StringComparison.IsNotEqualTo:
					return !string.Equals(variable.stringValue, value8, (!_caseSensitive) ? System.StringComparison.CurrentCultureIgnoreCase : System.StringComparison.CurrentCulture);
				case StringComparison.IsGreaterThan:
					return variable.stringValue.CompareTo(value8) > 0;
				case StringComparison.IsGreaterThanOrEqualTo:
					return variable.stringValue.CompareTo(value8) >= 0;
				case StringComparison.IsLessThan:
					return variable.stringValue.CompareTo(value8) < 0;
				case StringComparison.IsLessThanOrEqualTo:
					return variable.stringValue.CompareTo(value8) <= 0;
				case StringComparison.Contains:
					return variable.stringValue.Contains(value8);
				case StringComparison.StartsWith:
					return variable.stringValue.StartsWith(value8);
				case StringComparison.EndsWith:
					return variable.stringValue.EndsWith(value8);
				case StringComparison.IsEmpty:
					return variable.stringValue.Length == 0;
				case StringComparison.IsNotEmpty:
					return variable.stringValue.Length > 0;
				}
				break;
			}
			case DataType.Tag:
			{
				string value7 = _valueVar.GetValue(_stringValue);
				switch (_equalComparison)
				{
				case EqualComparison.IsEqualTo:
					return variable.stringValue.CompareTo(value7) == 0;
				case EqualComparison.IsNotEqualTo:
					return variable.stringValue.CompareTo(value7) != 0;
				}
				break;
			}
			case DataType.Integer:
			{
				int value9 = _valueVar.GetValue(_intValue);
				switch (_comparison)
				{
				case Comparison.IsEqualTo:
					return variable.intValue == value9;
				case Comparison.IsNotEqualTo:
					return variable.intValue != value9;
				case Comparison.IsGreaterThan:
					return variable.intValue > value9;
				case Comparison.IsGreaterThanOrEqualTo:
					return variable.intValue >= value9;
				case Comparison.IsLessThan:
					return variable.intValue < value9;
				case Comparison.IsLessThanOrEqualTo:
					return variable.intValue <= value9;
				}
				break;
			}
			case DataType.Layer:
			{
				int value5 = _valueVar.GetValue(_intValue);
				switch (_equalComparison)
				{
				case EqualComparison.IsEqualTo:
					return variable.intValue == value5;
				case EqualComparison.IsNotEqualTo:
					return variable.intValue != value5;
				}
				break;
			}
			case DataType.Float:
			{
				float value4 = _valueVar.GetValue(_floatValue);
				switch (_comparison)
				{
				case Comparison.IsEqualTo:
					return variable.floatValue == value4;
				case Comparison.IsNotEqualTo:
					return variable.floatValue != value4;
				case Comparison.IsGreaterThan:
					return variable.floatValue > value4;
				case Comparison.IsGreaterThanOrEqualTo:
					return variable.floatValue >= value4;
				case Comparison.IsLessThan:
					return variable.floatValue < value4;
				case Comparison.IsLessThanOrEqualTo:
					return variable.floatValue <= value4;
				}
				break;
			}
			case DataType.Boolean:
			case DataType.Toggle:
			{
				bool value6 = _valueVar.GetValue(_boolValue);
				switch (_equalComparison)
				{
				case EqualComparison.IsEqualTo:
					return variable.boolValue == value6;
				case EqualComparison.IsNotEqualTo:
					return variable.boolValue != value6;
				}
				break;
			}
			case DataType.Vector2:
			{
				Vector2 value13 = _valueVar.GetValue(_vector2Value);
				switch (_equalComparison)
				{
				case EqualComparison.IsEqualTo:
					return variable.vector2Value == value13;
				case EqualComparison.IsNotEqualTo:
					return variable.vector2Value != value13;
				}
				break;
			}
			case DataType.Vector3:
			{
				Vector3 value12 = _valueVar.GetValue(_vector3Value);
				switch (_equalComparison)
				{
				case EqualComparison.IsEqualTo:
					return variable.vector3Value == value12;
				case EqualComparison.IsNotEqualTo:
					return variable.vector3Value != value12;
				}
				break;
			}
			case DataType.Rect:
			{
				Rect value11 = _valueVar.GetValue(_rectValue);
				switch (_equalComparison)
				{
				case EqualComparison.IsEqualTo:
					return variable.rectValue == value11;
				case EqualComparison.IsNotEqualTo:
					return variable.rectValue != value11;
				}
				break;
			}
			case DataType.Color:
			{
				Color value10 = _valueVar.GetValue(_colorValue);
				switch (_equalComparison)
				{
				case EqualComparison.IsEqualTo:
					return (Vector4)variable.colorValue == (Vector4)value10;
				case EqualComparison.IsNotEqualTo:
					return (Vector4)variable.colorValue != (Vector4)value10;
				}
				break;
			}
			case DataType.Object:
			{
				bool flag2 = variable.type == typeof(Variable) || variable.type == typeof(Parameter);
				switch (_objectComparison)
				{
				case ObjectComparison.IsEqualTo:
					if (flag2)
					{
						return variable.objectValue == _valueVar;
					}
					return variable.objectValue == _valueVar.GetValue(_objectValue);
				case ObjectComparison.IsNotEqualTo:
					if (flag2)
					{
						return variable.objectValue != _valueVar;
					}
					return variable.objectValue != _valueVar.GetValue(_objectValue);
				case ObjectComparison.IsNone:
					return variable.objectValue == null;
				case ObjectComparison.IsNotNone:
					return variable.objectValue != null;
				}
				break;
			}
			case DataType.Enum:
			{
				int num = 0;
				bool flag = true;
				if (_valueVar == null)
				{
					num = _enumIntValue;
				}
				else
				{
					flag = (variable.type == _valueVar.type);
					num = Convert.ToInt32(_valueVar.enumValue);
				}
				switch (_equalComparison)
				{
				case EqualComparison.IsEqualTo:
					return flag && Convert.ToInt32(variable.enumValue) == num;
				case EqualComparison.IsNotEqualTo:
					return !flag || Convert.ToInt32(variable.enumValue) != num;
				}
				break;
			}
			case DataType.Bounds:
			{
				Bounds value3 = _valueVar.GetValue(_boundsValue);
				switch (_equalComparison)
				{
				case EqualComparison.IsEqualTo:
					return variable.boundsValue == value3;
				case EqualComparison.IsNotEqualTo:
					return variable.boundsValue != value3;
				}
				break;
			}
			case DataType.Quaternion:
			{
				Quaternion value2 = _valueVar.GetValue(_quaternionValue);
				switch (_equalComparison)
				{
				case EqualComparison.IsEqualTo:
					return variable.quaternionValue == value2;
				case EqualComparison.IsNotEqualTo:
					return variable.quaternionValue != value2;
				}
				break;
			}
			case DataType.Vector4:
			{
				Vector4 value = _valueVar.GetValue(_vector4Value);
				switch (_equalComparison)
				{
				case EqualComparison.IsEqualTo:
					return variable.vector4Value == value;
				case EqualComparison.IsNotEqualTo:
					return variable.vector4Value != value;
				}
				break;
			}
			}
			return false;
		}
	}
}
