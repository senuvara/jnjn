using System.Text;
using System.Text.RegularExpressions;
using UnityEngine;

namespace GameFlow
{
	public static class StringUtils
	{
		public static string FirstToUpper(this string s)
		{
			if (s == null || s.Length == 0)
			{
				return s;
			}
			if (s.Length == 1)
			{
				return s.ToUpper();
			}
			return s.Substring(0, 1).ToUpper() + s.Substring(1);
		}

		public static string SplitCamelCase(this string s)
		{
			string text = Regex.Replace(s.FirstToUpper(), "([A-Z])", "$1");
			bool flag = false;
			StringBuilder stringBuilder = new StringBuilder();
			int num = 0;
			for (int i = 0; i < text.Length; i++)
			{
				string text2 = text.Substring(i, 1);
				if (text2[0] >= "A"[0] && text2[0] <= "Z"[0])
				{
					num++;
					if (flag)
					{
						stringBuilder.Append(" ");
						flag = false;
					}
				}
				else
				{
					flag = true;
					if (num > 1)
					{
						stringBuilder.Insert(i, " ");
					}
					num = 0;
				}
				stringBuilder.Append(text2);
			}
			stringBuilder.Replace("Current State Condition", "State Condition");
			stringBuilder.Replace("Game Object", "GameObject");
			stringBuilder.Replace("Audio Source", "AudioSource");
			stringBuilder.Replace("GUI Text", "GUIText");
			stringBuilder.Replace("3 D", " 3D");
			stringBuilder.Replace("2  D", " 2D ");
			stringBuilder.Replace("2 D", " 2D ");
			stringBuilder.Replace("Uv ", "UV ");
			return stringBuilder.ToString().Trim();
		}

		public static string TrimRight(this string s, int length)
		{
			return s.Substring(0, s.Length - length);
		}

		public static string ReplaceTypeNames(this string s)
		{
			s = s.Replace("Game Object", "GameObject");
			return s;
		}

		public static string ReplaceAcutes(this string s)
		{
			s = s.Replace("á", "ú");
			s = s.Replace("é", "ú");
			s = s.Replace("í", "ú");
			s = s.Replace("ó", "ú");
			s = s.Replace("ú", "ú");
			return s;
		}

		public static GUIContent ToGUIContent(this string s)
		{
			return new GUIContent(s);
		}

		public static EditorMessage ToMessage(this string s)
		{
			return EditorMessage.Info(s);
		}

		public static EditorMessage ToWarningMessage(this string s)
		{
			return EditorMessage.Warning(s);
		}

		public static EditorMessage ToErrorMessage(this string s)
		{
			return EditorMessage.Error(s);
		}

		public static string Expanded(this string s)
		{
			if (s == null || s.Length == 0)
			{
				return string.Empty;
			}
			return Regex.Replace(s, "\\$\\{(.*?)\\}", ExpandVariable);
		}

		private static string ExpandVariable(Match match)
		{
			string id = match.Value.Substring(2, match.Value.Length - 3);
			Variable variableById = Variables.GetVariableById(id);
			if (variableById != null)
			{
				return variableById.stringValue;
			}
			return match.Value;
		}

		public static string Inverted(this string s)
		{
			int length = s.Length;
			StringBuilder stringBuilder = new StringBuilder();
			for (int num = length - 1; num >= 0; num--)
			{
				string value = s.Substring(num, 1);
				stringBuilder.Append(value);
			}
			return stringBuilder.ToString();
		}

		public static byte[] ToByteArray(this string s)
		{
			return Encoding.UTF8.GetBytes(s);
		}
	}
}
