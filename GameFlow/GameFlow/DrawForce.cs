using UnityEngine;

namespace GameFlow
{
	[AddComponentMenu("")]
	[Help("en", "Draws the specified @GameFlow{Force}.", null)]
	[Help("es", "Dibuja la fuerza (@GameFlow{Force}) especificada.", null)]
	public class DrawForce : DrawGizmosAction
	{
		[SerializeField]
		private Force _force;

		[SerializeField]
		private Variable _forceVar;

		[SerializeField]
		private Color _selectedColor = Color.clear;

		[SerializeField]
		private Variable _selectedColorVar;

		[SerializeField]
		private Color _unselectedColor = Color.white;

		[SerializeField]
		private Variable _unselectedColorVar;

		public Force force => _forceVar.GetValue(_force) as Force;

		public Color selectedColor => _selectedColorVar.GetValue(_selectedColor);

		public Color unselectedColor => _unselectedColorVar.GetValue(_unselectedColor);

		protected override void OnReset()
		{
			_force = base.gameObject.GetComponent<Force>();
		}
	}
}
