using System;
using UnityEngine;

namespace GameFlow
{
	[Help("es", "Una @GameFlow{Variable} integrada que devuelve la coord. Y de la posición actual del ratón.", null)]
	[AddComponentMenu("")]
	[Help("en", "A built-in @GameFlow{Variable} that returns the Y coord. of the current position of the mouse cursor.", null)]
	public class BuiltinMousePositionY : BuiltinVariable
	{
		public override float floatValue
		{
			get
			{
				float result;
				if (Application.isPlaying)
				{
					Vector3 mousePosition = Input.mousePosition;
					result = mousePosition.y;
				}
				else
				{
					result = 0f;
				}
				return result;
			}
		}

		public override Type GetVariableType()
		{
			return typeof(float);
		}

		protected override string GetVariableId()
		{
			return "Mouse Position Y";
		}
	}
}
