using UnityEngine;

namespace GameFlow
{
	[Help("es", "Modifica una propiedad del componente @UnityManual{Platform} especificado.", null)]
	[AddComponentMenu("")]
	[Help("en", "Sets a property of the specified @UnityManual{Platform}.", null)]
	public class SetPlatformEffector2DProperty : Action, IExecutableInEditor
	{
		public enum Property
		{
			ColliderMask,
			SideAngleVariance,
			UseColliderMask,
			UseOneWay,
			UseSideBounce,
			UseSideFriction
		}

		[SerializeField]
		private PlatformEffector2D _target;

		[SerializeField]
		private Variable _targetVar;

		[SerializeField]
		private Property _property;

		[SerializeField]
		private LayerMask _colliderMaskValue;

		[SerializeField]
		private float _sideAngleVarianceValue;

		[SerializeField]
		private bool _useColliderMaskValue;

		[SerializeField]
		private bool _useOneWayValue;

		[SerializeField]
		private bool _useSideBounceValue;

		[SerializeField]
		private bool _useSideFrictionValue;

		[SerializeField]
		private Variable _valueVar;

		protected override void OnReset()
		{
			_target = base.gameObject.GetComponent<PlatformEffector2D>();
		}

		public override void Setup()
		{
		}

		public override void Execute()
		{
			PlatformEffector2D platformEffector2D = _targetVar.GetValueAsComponent(_target, typeof(PlatformEffector2D)) as PlatformEffector2D;
			if (!(platformEffector2D == null))
			{
				switch (_property)
				{
				case Property.ColliderMask:
					platformEffector2D.colliderMask = _valueVar.GetValue(_colliderMaskValue);
					break;
				case Property.SideAngleVariance:
					platformEffector2D.sideArc = _valueVar.GetValue(_sideAngleVarianceValue);
					break;
				case Property.UseColliderMask:
					platformEffector2D.useColliderMask = _valueVar.GetValue(_useColliderMaskValue);
					break;
				case Property.UseOneWay:
					platformEffector2D.useOneWay = _valueVar.GetValue(_useOneWayValue);
					break;
				case Property.UseSideBounce:
					platformEffector2D.useSideBounce = _valueVar.GetValue(_useSideBounceValue);
					break;
				case Property.UseSideFriction:
					platformEffector2D.useSideFriction = _valueVar.GetValue(_useSideFrictionValue);
					break;
				}
			}
		}
	}
}
