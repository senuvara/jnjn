namespace GameFlow
{
	public abstract class OnActivationProgram : EventProgram
	{
		private Parameter _sourceParam;

		protected Parameter sourceParam
		{
			get
			{
				if (_sourceParam == null)
				{
					_sourceParam = GetParameter("Source");
				}
				return _sourceParam;
			}
		}

		protected override void CacheParameters()
		{
			CacheParameter(sourceParam);
		}
	}
}
