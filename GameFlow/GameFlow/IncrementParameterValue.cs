using UnityEngine;

namespace GameFlow
{
	[Help("en", "Increments the numeric value of the specified @GameFlow{Parameter}.", null)]
	[AddComponentMenu("")]
	[Help("es", "Incrementa el valor numérico del @GameFlow{Parameter} especificado.", null)]
	public class IncrementParameterValue : ParameterAction
	{
		protected override int GetDefaultIntValue()
		{
			return 1;
		}

		protected override float GetDefaultFloatValue()
		{
			return 1f;
		}

		public override void Execute()
		{
			Execute(base.parameter, this);
		}

		public static void Execute(Parameter parameter, ValueAction valueAction)
		{
			IncrementVariableValue.Execute(parameter, valueAction);
		}
	}
}
