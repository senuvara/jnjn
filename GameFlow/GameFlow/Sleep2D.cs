using UnityEngine;

namespace GameFlow
{
	[Help("en", "Puts to sleep the specified 2D object.", null)]
	[Help("es", "Pone en reposo el objeto 2D especificado.", null)]
	[AddComponentMenu("")]
	public class Sleep2D : Action
	{
		[SerializeField]
		private Rigidbody2D _target;

		[SerializeField]
		private Variable _targetVar;

		public override void Setup()
		{
		}

		public override void Execute()
		{
			Rigidbody2D rigidbody2D = _target;
			if (rigidbody2D == null)
			{
				rigidbody2D = (_targetVar.GetValueAsComponent(_target, typeof(Rigidbody2D)) as Rigidbody2D);
			}
			Execute(rigidbody2D);
		}

		public static void Execute(Rigidbody2D rb)
		{
			if (rb != null)
			{
				rb.Sleep();
			}
		}
	}
}
