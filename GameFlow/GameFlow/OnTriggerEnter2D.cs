using UnityEngine;

namespace GameFlow
{
	[DisallowMultipleComponent]
	[AddComponentMenu("GameFlow/Programs/On Trigger Enter 2D")]
	[Help("es", "Programa que se ejecutará una vez en el momento que un componente Collider 2D configurado como disparador (trigger) en el rango de escucha comience a ser invadido por otro componente.", null)]
	[Help("en", "Program that will be executed one time as soon as a Collider 2D component configured as trigger in the listening range starts being invaded by another component.", null)]
	public class OnTriggerEnter2D : OnTriggerProgram2D
	{
		private EventController[] _controllers;

		protected override void RegisterAsListener()
		{
			_controllers = RegisterAsListener(typeof(TriggerEnterEvent), typeof(TriggerController2D), TriggerController2D.AddController);
			if (base.listeningRange == EventListeningRange.SceneHierarchy)
			{
				TriggerEnterEvent.AddListener(this);
			}
		}

		protected override void UnRegisterAsListener()
		{
			TriggerEnterEvent.RemoveListener(this);
			UnRegisterAsListener(typeof(TriggerEnterEvent), typeof(TriggerController2D));
		}

		private void OnTransformChildrenChanged()
		{
			if (base.listeningRange == EventListeningRange.GameObjectHierarchy)
			{
				UnsubscribeFrom(_controllers, typeof(TriggerEnterEvent));
				_controllers = EventController.GetControllersInHierarchyOf(typeof(TriggerController2D), base.listeningTarget, TriggerController2D.AddController);
				SubscribeTo(_controllers, typeof(TriggerEnterEvent));
			}
		}
	}
}
