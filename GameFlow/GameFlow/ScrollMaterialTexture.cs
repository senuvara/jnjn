using UnityEngine;

namespace GameFlow
{
	[Help("en", "Changes the offset of a Texture of the specified Material.", null)]
	[AddComponentMenu("")]
	[Help("es", "Modifica el offset de una Textura del Material especificado.", null)]
	public class ScrollMaterialTexture : Action
	{
		public enum TargetTexture
		{
			MainTexture,
			NamedTexture
		}

		[SerializeField]
		private Material _material;

		[SerializeField]
		private Variable _materialVar;

		[SerializeField]
		private TargetTexture _target;

		[SerializeField]
		private string _textureName;

		[SerializeField]
		private Variable _textureNameVar;

		[SerializeField]
		private Vector2 _offset;

		[SerializeField]
		private Variable _offsetVar;

		[SerializeField]
		private float _multiplier = 1f;

		[SerializeField]
		private Variable _multiplierVar;

		public override void Execute()
		{
			Material material = _materialVar.GetValue(_material) as Material;
			if (material == null)
			{
				return;
			}
			Texture texture = null;
			switch (_target)
			{
			case TargetTexture.MainTexture:
				texture = material.mainTexture;
				break;
			case TargetTexture.NamedTexture:
			{
				string value = _textureNameVar.GetValue(_textureName);
				if (material.HasProperty(value))
				{
					texture = material.GetTexture(value);
				}
				break;
			}
			}
			if (!(texture == null))
			{
				Vector2 vector = _offsetVar.GetValue(_offset) * _multiplierVar.GetValue(_multiplier);
				float num = texture.width;
				float num2 = texture.height;
				Vector2 vector2 = material.mainTextureOffset += new Vector2(vector.x / num, vector.y / num2);
			}
		}
	}
}
