using UnityEngine;

namespace GameFlow
{
	[AddComponentMenu("")]
	[Help("en", "Sets a global shader keyword.", null)]
	[Help("es", "Habilita una palabra clave para todos los shaders.", null)]
	public class EnableShaderKeyword : Action, IExecutableInEditor
	{
		public enum Scope
		{
			Global,
			Material
		}

		[SerializeField]
		private string _keyword;

		[SerializeField]
		private Variable _keywordVar;

		[SerializeField]
		private Scope _scope;

		[SerializeField]
		private Material _material;

		[SerializeField]
		private Variable _materialVar;

		public override void Execute()
		{
			string value = _keywordVar.GetValue(_keyword);
			switch (_scope)
			{
			case Scope.Global:
				Execute(value);
				break;
			case Scope.Material:
				Execute(value, _materialVar.GetValue(_material) as Material);
				break;
			}
		}

		public static void Execute(string keyword)
		{
			if (keyword != null && keyword.Length != 0)
			{
				Shader.EnableKeyword(keyword);
			}
		}

		public static void Execute(string keyword, Material material)
		{
			if (keyword != null && keyword.Length != 0 && !(material == null))
			{
				material.EnableKeyword(keyword);
			}
		}
	}
}
