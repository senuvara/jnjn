using UnityEngine;

namespace GameFlow
{
	[Help("es", "Evalúa la capa del @UnityManual{GameObject} especificado.", null)]
	[AddComponentMenu("")]
	[Help("en", "Evaluates the Layer of the specified @UnityManual{GameObject}.", null)]
	public class LayerCondition : Condition
	{
		public enum Comparison
		{
			IsEqualTo,
			IsNotEqualTo
		}

		[SerializeField]
		private GameObject _gameObject;

		[SerializeField]
		private Variable _gameObjectVar;

		[SerializeField]
		private Comparison _comparison;

		[SerializeField]
		private int _layer;

		[SerializeField]
		private Variable _layerVar;

		public override bool Evaluate()
		{
			GameObject gameObject = _gameObjectVar.GetValue(_gameObject) as GameObject;
			if (gameObject == null)
			{
				return false;
			}
			switch (_comparison)
			{
			case Comparison.IsEqualTo:
				return gameObject.layer == _layerVar.GetValue(_layer);
			case Comparison.IsNotEqualTo:
				return gameObject.layer != _layerVar.GetValue(_layer);
			default:
				return false;
			}
		}
	}
}
