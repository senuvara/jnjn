using System;
using UnityEngine;

namespace GameFlow
{
	[Help("es", "Confina un objeto dentro del espacio definido por el componente Collider especificado.", null)]
	[AddComponentMenu("")]
	[Help("en", "Confines an object into the space defined by the specified Collider componente.", null)]
	public class Confine : Action, IExecutableInEditor
	{
		[SerializeField]
		private Transform _target;

		[SerializeField]
		private Variable _targetVar;

		[SerializeField]
		private Collider _fence;

		[SerializeField]
		private Variable _fenceVar;

		protected override void OnReset()
		{
		}

		public override void Step()
		{
			Transform transform = _targetVar.GetValueAsComponent(_target, typeof(Transform)) as Transform;
			Collider fence = _fenceVar.GetValueAsComponent(_fence, typeof(Collider)) as Collider;
			Execute(transform, fence);
		}

		public static void Execute(Transform transform, Collider fence)
		{
			if (!(transform == null) && !(fence == null))
			{
				Bounds bounds = new Bounds(transform.position, transform.lossyScale);
				Bounds bounds2 = fence.bounds;
				if (!bounds2.Contains(bounds.max))
				{
					Bounds bounds3 = new Bounds(bounds2.center, bounds2.size);
					bounds3.Encapsulate(bounds.max);
					Vector3 vector = bounds3.size - bounds2.size;
					transform.position -= vector;
				}
				if (!bounds2.Contains(bounds.min))
				{
					Bounds bounds4 = new Bounds(bounds2.center, bounds2.size);
					bounds4.Encapsulate(bounds.min);
					Vector3 vector2 = bounds4.size - bounds2.size;
					transform.position += vector2;
				}
			}
		}

		public override Type GetPreferredContainerType()
		{
			return typeof(OnLateUpdate);
		}
	}
}
