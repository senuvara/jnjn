namespace GameFlow
{
	public class ArrayUtils
	{
		public static int IndexOf<T>(T[] array, T value)
		{
			for (int i = 0; i < array.Length; i++)
			{
				if (array[i].Equals(value))
				{
					return i;
				}
			}
			return -1;
		}
	}
}
