using UnityEngine;
using UnityEngine.AI;

namespace GameFlow
{
	[Help("es", "Modifica una propiedad del componente @UnityManual{OffMeshLink} especificado.", null)]
	[Help("en", "Sets a property of the specified @UnityManual{OffMeshLink}.", null)]
	[AddComponentMenu("")]
	public class SetOffMeshLinkProperty : Action, IExecutableInEditor
	{
		public enum Property
		{
			Activated,
			Area,
			AutoUpdatePositions,
			BiDirectional,
			CostOverride,
			EndTransform,
			StartTransform
		}

		[SerializeField]
		private OffMeshLink _target;

		[SerializeField]
		private Variable _targetVar;

		[SerializeField]
		private Property _property;

		[SerializeField]
		private bool _activatedValue;

		[SerializeField]
		private int _areaValue;

		[SerializeField]
		private bool _autoUpdatePositionsValue;

		[SerializeField]
		private bool _biDirectionalValue;

		[SerializeField]
		private float _costOverrideValue;

		[SerializeField]
		private Transform _endTransformValue;

		[SerializeField]
		private Transform _startTransformValue;

		[SerializeField]
		private Variable _valueVar;

		protected override void OnReset()
		{
			_target = base.gameObject.GetComponent<OffMeshLink>();
		}

		public override void Setup()
		{
		}

		public override void Execute()
		{
			OffMeshLink offMeshLink = _targetVar.GetValueAsComponent(_target, typeof(OffMeshLink)) as OffMeshLink;
			if (!(offMeshLink == null))
			{
				switch (_property)
				{
				case Property.Activated:
					offMeshLink.activated = _valueVar.GetValue(_activatedValue);
					break;
				case Property.Area:
					offMeshLink.area = _valueVar.GetValue(_areaValue);
					break;
				case Property.AutoUpdatePositions:
					offMeshLink.autoUpdatePositions = _valueVar.GetValue(_autoUpdatePositionsValue);
					break;
				case Property.BiDirectional:
					offMeshLink.biDirectional = _valueVar.GetValue(_biDirectionalValue);
					break;
				case Property.CostOverride:
					offMeshLink.costOverride = _valueVar.GetValue(_costOverrideValue);
					break;
				case Property.EndTransform:
					offMeshLink.endTransform = (_valueVar.GetValueAsComponent(_endTransformValue, typeof(Transform)) as Transform);
					break;
				case Property.StartTransform:
					offMeshLink.startTransform = (_valueVar.GetValueAsComponent(_startTransformValue, typeof(Transform)) as Transform);
					break;
				}
			}
		}
	}
}
