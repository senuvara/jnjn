using UnityEngine;

namespace GameFlow
{
	[Help("es", "Lee una componente del vector especificado.", null)]
	[AddComponentMenu("")]
	[Help("en", "Gets a component of the specified vector.", null)]
	public class GetVectorComponent : Action, IExecutableInEditor
	{
		public enum VectorType
		{
			Vector2,
			Vector3
		}

		[SerializeField]
		private VectorType _vectorType = VectorType.Vector3;

		[SerializeField]
		private Vector2 _vector2;

		[SerializeField]
		private Variable _vector2Var;

		[SerializeField]
		private Vector2Component _vector2Component;

		[SerializeField]
		private Vector3 _vector3;

		[SerializeField]
		private Variable _vector3Var;

		[SerializeField]
		private Vector3Component _vector3Component;

		[SerializeField]
		private Variable _output;

		public override void Execute()
		{
			if (_output == null)
			{
				return;
			}
			switch (_vectorType)
			{
			case VectorType.Vector2:
			{
				Vector2 value2 = _vector2Var.GetValue(_vector2);
				switch (_vector2Component)
				{
				case Vector2Component.X:
					_output.floatValue = value2.x;
					break;
				case Vector2Component.Y:
					_output.floatValue = value2.y;
					break;
				}
				break;
			}
			case VectorType.Vector3:
			{
				Vector3 value = _vector3Var.GetValue(_vector3);
				switch (_vector3Component)
				{
				case Vector3Component.X:
					_output.floatValue = value.x;
					break;
				case Vector3Component.Y:
					_output.floatValue = value.y;
					break;
				case Vector3Component.Z:
					_output.floatValue = value.z;
					break;
				}
				break;
			}
			}
		}
	}
}
