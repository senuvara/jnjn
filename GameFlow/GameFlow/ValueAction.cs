using System;
using UnityEngine;

namespace GameFlow
{
	public abstract class ValueAction : Action
	{
		[SerializeField]
		private string _stringValue;

		[SerializeField]
		private int _intValue;

		[SerializeField]
		private float _floatValue;

		[SerializeField]
		private bool _boolValue;

		[SerializeField]
		private Rect _rectValue;

		[SerializeField]
		private Vector2 _vector2Value;

		[SerializeField]
		private Vector3 _vector3Value;

		[SerializeField]
		private Color _colorValue;

		[SerializeField]
		private int _r;

		[SerializeField]
		private Variable _rVar;

		[SerializeField]
		private int _g;

		[SerializeField]
		private Variable _gVar;

		[SerializeField]
		private int _b;

		[SerializeField]
		private Variable _bVar;

		[SerializeField]
		private int _a;

		[SerializeField]
		private Variable _aVar;

		[SerializeField]
		private UnityEngine.Object _objectValue;

		[SerializeField]
		private int _enumIntValue;

		[SerializeField]
		private AnimationCurve _animationCurveValue;

		[SerializeField]
		private Bounds _boundsValue;

		[SerializeField]
		private Quaternion _quaternionValue;

		[SerializeField]
		private Vector4 _vector4Value;

		[SerializeField]
		private Variable _valueVar;

		public string stringValue => _valueVar.GetValue(_stringValue);

		public int intValue => _valueVar.GetValue(_intValue);

		public float floatValue => _valueVar.GetValue(_floatValue);

		public bool boolValue => _valueVar.GetValue(_boolValue);

		public Rect rectValue => _valueVar.GetValue(_rectValue);

		public Vector2 vector2Value => _valueVar.GetValue(_vector2Value);

		public Vector3 vector3Value => _valueVar.GetValue(_vector3Value);

		public Color colorValue
		{
			get
			{
				if (_valueVar != null)
				{
					return _valueVar.GetValue(_colorValue);
				}
				return ColorUtils.CreateColor(r, g, b, a);
			}
		}

		protected int r => _rVar.GetValue(_r);

		protected int g => _gVar.GetValue(_g);

		protected int b => _bVar.GetValue(_b);

		protected int a => _aVar.GetValue(_a);

		public UnityEngine.Object objectValue => _valueVar.GetValue(_objectValue);

		public int enumIntValue => (!(_valueVar != null)) ? _enumIntValue : Convert.ToInt32(_valueVar.enumValue);

		public AnimationCurve animationCurveValue => _valueVar.GetValue(_animationCurveValue);

		public Bounds boundsValue => _valueVar.GetValue(_boundsValue);

		public Quaternion quaternionValue => _valueVar.GetValue(_quaternionValue);

		public Vector4 vector4Value => _valueVar.GetValue(_vector4Value);

		public Variable valueVar => _valueVar;

		protected override void OnReset()
		{
			_stringValue = GetDefaultStringValue();
			_intValue = GetDefaultIntValue();
			_floatValue = GetDefaultFloatValue();
			_boolValue = GetDefaultBoolValue();
			_vector2Value = GetDefaultVector2Value();
			_vector3Value = GetDefaultVector3Value();
			_rectValue = GetDefaultRectValue();
			_colorValue = GetDefaultColorValue();
			_objectValue = GetDefaultObjectValue();
			_enumIntValue = Convert.ToInt32(GetDefaultEnumValue());
			_animationCurveValue = GetDefaultAnimationCurveValue();
			_boundsValue = GetDefaultBoundsValue();
			_quaternionValue = GetDefaultQuaternionValue();
			_vector4Value = GetDefaultVector4Value();
		}

		protected virtual string GetDefaultStringValue()
		{
			return string.Empty;
		}

		protected virtual int GetDefaultIntValue()
		{
			return 0;
		}

		protected virtual float GetDefaultFloatValue()
		{
			return 0f;
		}

		protected virtual bool GetDefaultBoolValue()
		{
			return false;
		}

		protected virtual Vector2 GetDefaultVector2Value()
		{
			return Vector2.zero;
		}

		protected virtual Vector3 GetDefaultVector3Value()
		{
			return Vector3.zero;
		}

		protected virtual Rect GetDefaultRectValue()
		{
			return RectExt.zero;
		}

		protected virtual Color GetDefaultColorValue()
		{
			return Color.black;
		}

		protected virtual UnityEngine.Object GetDefaultObjectValue()
		{
			return null;
		}

		protected virtual Enum GetDefaultEnumValue()
		{
			return None.None;
		}

		protected virtual AnimationCurve GetDefaultAnimationCurveValue()
		{
			return new AnimationCurve();
		}

		protected virtual Bounds GetDefaultBoundsValue()
		{
			return new Bounds(Vector3.zero, Vector3.zero);
		}

		protected virtual Quaternion GetDefaultQuaternionValue()
		{
			return Quaternion.identity;
		}

		protected virtual Vector4 GetDefaultVector4Value()
		{
			return Vector4.zero;
		}

		public DataType GetDataType(DataType defaultDataType)
		{
			return (!(_valueVar != null)) ? defaultDataType : _valueVar.dataType;
		}
	}
}
