using UnityEngine;

namespace GameFlow
{
	[Help("en", "Gets a property of the specified Joint 2D component.", null)]
	[Help("es", "Lee una propiedad del componente Joint 2D especificado.", null)]
	[AddComponentMenu("")]
	public class GetJoint2DProperty : Action, IExecutableInEditor
	{
		public enum JointProperty
		{
			BreakForce,
			BreakTorque,
			ConnectedBody,
			EnableCollision,
			ReactionForce,
			ReactionTorque
		}

		public enum DistanceJointProperty
		{
			BreakForce,
			BreakTorque,
			ConnectedBody,
			EnableCollision,
			ReactionForce,
			ReactionTorque,
			Anchor,
			AutoConfigureConnectedAnchor,
			ConnectedAnchor,
			AutoConfigureDistance,
			Distance
		}

		public enum FixedJointProperty
		{
			BreakForce,
			BreakTorque,
			ConnectedBody,
			EnableCollision,
			ReactionForce,
			ReactionTorque,
			Anchor,
			AutoConfigureConnectedAnchor,
			ConnectedAnchor,
			DampingRatio,
			Frequency,
			ReferenceAngle
		}

		public enum FrictionJointProperty
		{
			BreakForce,
			BreakTorque,
			ConnectedBody,
			EnableCollision,
			ReactionForce,
			ReactionTorque,
			Anchor,
			AutoConfigureConnectedAnchor,
			ConnectedAnchor,
			MaxForce,
			MaxTorque
		}

		public enum HingeJointProperty
		{
			BreakForce,
			BreakTorque,
			ConnectedBody,
			EnableCollision,
			ReactionForce,
			ReactionTorque,
			Anchor,
			AutoConfigureConnectedAnchor,
			ConnectedAnchor,
			JointAngle,
			JointSpeed,
			Limits_Max,
			Limits_Min,
			LimitState,
			Motor_MaxTorque,
			Motor_Speed,
			ReferenceAngle,
			UseLimits,
			UseMotor
		}

		public enum RelativeJointProperty
		{
			BreakForce,
			BreakTorque,
			ConnectedBody,
			EnableCollision,
			ReactionForce,
			ReactionTorque,
			AngularOffset,
			AutoConfigureOffset,
			CorrectionScale,
			LinearOffset,
			MaxForce,
			MaxTorque,
			Target
		}

		public enum SliderJointProperty
		{
			BreakForce,
			BreakTorque,
			ConnectedBody,
			EnableCollision,
			ReactionForce,
			ReactionTorque,
			Anchor,
			AutoConfigureConnectedAnchor,
			ConnectedAnchor,
			Angle,
			AutoConfigureAngle,
			JointSpeed,
			JointTranslation,
			Limits_Max,
			Limits_Min,
			LimitState,
			Motor_MaxTorque,
			Motor_Speed,
			ReferenceAngle,
			UseLimits,
			UseMotor
		}

		public enum SpringJointProperty
		{
			BreakForce,
			BreakTorque,
			ConnectedBody,
			EnableCollision,
			ReactionForce,
			ReactionTorque,
			Anchor,
			AutoConfigureConnectedAnchor,
			ConnectedAnchor,
			AutoConfigureDistance,
			DampingRatio,
			Distance,
			Frequency
		}

		public enum TargetJointProperty
		{
			BreakForce,
			BreakTorque,
			ConnectedBody,
			EnableCollision,
			ReactionForce,
			ReactionTorque,
			Anchor,
			AutoConfigureTarget,
			DampingRatio,
			Frequency,
			MaxForce,
			Target
		}

		public enum WheelJointProperty
		{
			BreakForce,
			BreakTorque,
			ConnectedBody,
			EnableCollision,
			ReactionForce,
			ReactionTorque,
			Anchor,
			AutoConfigureConnectedAnchor,
			ConnectedAnchor,
			JointSpeed,
			JointTranslation,
			Motor_MaxTorque,
			Motor_Speed,
			Suspension_Angle,
			Suspension_DampingRatio,
			Suspension_Frequency,
			UseMotor
		}

		[SerializeField]
		private Joint2D _source;

		[SerializeField]
		private Variable _sourceVar;

		[SerializeField]
		private JointProperty _jointProperty;

		[SerializeField]
		private DistanceJointProperty _distanceProperty;

		[SerializeField]
		private FixedJointProperty _fixedProperty;

		[SerializeField]
		private FrictionJointProperty _frictionProperty;

		[SerializeField]
		private HingeJointProperty _hingeProperty;

		[SerializeField]
		private RelativeJointProperty _relativeProperty;

		[SerializeField]
		private SliderJointProperty _sliderProperty;

		[SerializeField]
		private SpringJointProperty _springProperty;

		[SerializeField]
		private TargetJointProperty _targetProperty;

		[SerializeField]
		private WheelJointProperty _wheelProperty;

		[SerializeField]
		private Variable _output;

		protected override void OnReset()
		{
			_source = base.gameObject.GetComponent<Joint2D>();
		}

		public override void Execute()
		{
			Joint2D joint2D = _source;
			if (joint2D == null)
			{
				joint2D = (_sourceVar.GetValueAsComponent(_source, typeof(Joint2D)) as Joint2D);
			}
			if (joint2D == null)
			{
				return;
			}
			DistanceJoint2D distanceJoint2D = joint2D as DistanceJoint2D;
			if (distanceJoint2D != null)
			{
				switch (_distanceProperty)
				{
				case DistanceJointProperty.BreakForce:
					_output.SetValue(distanceJoint2D.breakForce);
					break;
				case DistanceJointProperty.BreakTorque:
					_output.SetValue(distanceJoint2D.breakTorque);
					break;
				case DistanceJointProperty.ConnectedBody:
					_output.SetValue(distanceJoint2D.connectedBody);
					break;
				case DistanceJointProperty.EnableCollision:
					_output.SetToggleValue(distanceJoint2D.enableCollision);
					break;
				case DistanceJointProperty.ReactionForce:
					_output.SetValue(distanceJoint2D.reactionForce);
					break;
				case DistanceJointProperty.ReactionTorque:
					_output.SetValue(distanceJoint2D.reactionTorque);
					break;
				case DistanceJointProperty.Anchor:
					_output.SetValue(distanceJoint2D.anchor);
					break;
				case DistanceJointProperty.AutoConfigureConnectedAnchor:
					_output.SetToggleValue(distanceJoint2D.autoConfigureConnectedAnchor);
					break;
				case DistanceJointProperty.ConnectedAnchor:
					_output.SetValue(distanceJoint2D.connectedAnchor);
					break;
				case DistanceJointProperty.AutoConfigureDistance:
					_output.SetToggleValue(distanceJoint2D.autoConfigureDistance);
					break;
				case DistanceJointProperty.Distance:
					_output.SetValue(distanceJoint2D.distance);
					break;
				}
				return;
			}
			FixedJoint2D fixedJoint2D = joint2D as FixedJoint2D;
			if (fixedJoint2D != null)
			{
				switch (_fixedProperty)
				{
				case FixedJointProperty.BreakForce:
					_output.SetValue(fixedJoint2D.breakForce);
					break;
				case FixedJointProperty.BreakTorque:
					_output.SetValue(fixedJoint2D.breakTorque);
					break;
				case FixedJointProperty.ConnectedBody:
					_output.SetValue(fixedJoint2D.connectedBody);
					break;
				case FixedJointProperty.EnableCollision:
					_output.SetToggleValue(fixedJoint2D.enableCollision);
					break;
				case FixedJointProperty.ReactionForce:
					_output.SetValue(fixedJoint2D.reactionForce);
					break;
				case FixedJointProperty.ReactionTorque:
					_output.SetValue(fixedJoint2D.reactionTorque);
					break;
				case FixedJointProperty.Anchor:
					_output.SetValue(fixedJoint2D.anchor);
					break;
				case FixedJointProperty.AutoConfigureConnectedAnchor:
					_output.SetToggleValue(fixedJoint2D.autoConfigureConnectedAnchor);
					break;
				case FixedJointProperty.ConnectedAnchor:
					_output.SetValue(fixedJoint2D.connectedAnchor);
					break;
				case FixedJointProperty.DampingRatio:
					_output.SetValue(fixedJoint2D.dampingRatio);
					break;
				case FixedJointProperty.Frequency:
					_output.SetValue(fixedJoint2D.frequency);
					break;
				case FixedJointProperty.ReferenceAngle:
					_output.SetValue(fixedJoint2D.referenceAngle);
					break;
				}
				return;
			}
			FrictionJoint2D frictionJoint2D = joint2D as FrictionJoint2D;
			if (frictionJoint2D != null)
			{
				switch (_frictionProperty)
				{
				case FrictionJointProperty.BreakForce:
					_output.SetValue(frictionJoint2D.breakForce);
					break;
				case FrictionJointProperty.BreakTorque:
					_output.SetValue(frictionJoint2D.breakTorque);
					break;
				case FrictionJointProperty.ConnectedBody:
					_output.SetValue(frictionJoint2D.connectedBody);
					break;
				case FrictionJointProperty.EnableCollision:
					_output.SetToggleValue(frictionJoint2D.enableCollision);
					break;
				case FrictionJointProperty.ReactionForce:
					_output.SetValue(frictionJoint2D.reactionForce);
					break;
				case FrictionJointProperty.ReactionTorque:
					_output.SetValue(frictionJoint2D.reactionTorque);
					break;
				case FrictionJointProperty.Anchor:
					_output.SetValue(frictionJoint2D.anchor);
					break;
				case FrictionJointProperty.AutoConfigureConnectedAnchor:
					_output.SetToggleValue(frictionJoint2D.autoConfigureConnectedAnchor);
					break;
				case FrictionJointProperty.ConnectedAnchor:
					_output.SetValue(frictionJoint2D.connectedAnchor);
					break;
				}
				return;
			}
			HingeJoint2D hingeJoint2D = joint2D as HingeJoint2D;
			if (hingeJoint2D != null)
			{
				switch (_hingeProperty)
				{
				case HingeJointProperty.BreakForce:
					_output.SetValue(hingeJoint2D.breakForce);
					break;
				case HingeJointProperty.BreakTorque:
					_output.SetValue(hingeJoint2D.breakTorque);
					break;
				case HingeJointProperty.ConnectedBody:
					_output.SetValue(hingeJoint2D.connectedBody);
					break;
				case HingeJointProperty.EnableCollision:
					_output.SetToggleValue(hingeJoint2D.enableCollision);
					break;
				case HingeJointProperty.ReactionForce:
					_output.SetValue(hingeJoint2D.reactionForce);
					break;
				case HingeJointProperty.ReactionTorque:
					_output.SetValue(hingeJoint2D.reactionTorque);
					break;
				case HingeJointProperty.Anchor:
					_output.SetValue(hingeJoint2D.anchor);
					break;
				case HingeJointProperty.AutoConfigureConnectedAnchor:
					_output.SetToggleValue(hingeJoint2D.autoConfigureConnectedAnchor);
					break;
				case HingeJointProperty.ConnectedAnchor:
					_output.SetValue(hingeJoint2D.connectedAnchor);
					break;
				case HingeJointProperty.JointAngle:
					_output.SetValue(hingeJoint2D.jointAngle);
					break;
				case HingeJointProperty.JointSpeed:
					_output.SetValue(hingeJoint2D.jointSpeed);
					break;
				case HingeJointProperty.Limits_Max:
					_output.SetValue(hingeJoint2D.limits.max);
					break;
				case HingeJointProperty.Limits_Min:
					_output.SetValue(hingeJoint2D.limits.min);
					break;
				case HingeJointProperty.LimitState:
					_output.SetValue(hingeJoint2D.limitState);
					break;
				case HingeJointProperty.Motor_MaxTorque:
					_output.SetValue(hingeJoint2D.motor.maxMotorTorque);
					break;
				case HingeJointProperty.Motor_Speed:
					_output.SetValue(hingeJoint2D.motor.motorSpeed);
					break;
				case HingeJointProperty.ReferenceAngle:
					_output.SetValue(hingeJoint2D.referenceAngle);
					break;
				case HingeJointProperty.UseLimits:
					_output.SetToggleValue(hingeJoint2D.useLimits);
					break;
				case HingeJointProperty.UseMotor:
					_output.SetToggleValue(hingeJoint2D.useMotor);
					break;
				}
				return;
			}
			RelativeJoint2D relativeJoint2D = joint2D as RelativeJoint2D;
			if (relativeJoint2D != null)
			{
				switch (_relativeProperty)
				{
				case RelativeJointProperty.BreakForce:
					_output.SetValue(relativeJoint2D.breakForce);
					break;
				case RelativeJointProperty.BreakTorque:
					_output.SetValue(relativeJoint2D.breakTorque);
					break;
				case RelativeJointProperty.ConnectedBody:
					_output.SetValue(relativeJoint2D.connectedBody);
					break;
				case RelativeJointProperty.EnableCollision:
					_output.SetToggleValue(relativeJoint2D.enableCollision);
					break;
				case RelativeJointProperty.ReactionForce:
					_output.SetValue(relativeJoint2D.reactionForce);
					break;
				case RelativeJointProperty.ReactionTorque:
					_output.SetValue(relativeJoint2D.reactionTorque);
					break;
				case RelativeJointProperty.AngularOffset:
					_output.SetValue(relativeJoint2D.breakForce);
					break;
				case RelativeJointProperty.AutoConfigureOffset:
					_output.SetToggleValue(relativeJoint2D.autoConfigureOffset);
					break;
				case RelativeJointProperty.CorrectionScale:
					_output.SetValue(relativeJoint2D.correctionScale);
					break;
				case RelativeJointProperty.LinearOffset:
					_output.SetValue(relativeJoint2D.linearOffset);
					break;
				case RelativeJointProperty.MaxForce:
					_output.SetValue(relativeJoint2D.maxForce);
					break;
				case RelativeJointProperty.MaxTorque:
					_output.SetValue(relativeJoint2D.maxTorque);
					break;
				case RelativeJointProperty.Target:
					_output.SetValue(relativeJoint2D.target);
					break;
				}
				return;
			}
			SliderJoint2D sliderJoint2D = joint2D as SliderJoint2D;
			if (sliderJoint2D != null)
			{
				switch (_sliderProperty)
				{
				case SliderJointProperty.BreakForce:
					_output.SetValue(sliderJoint2D.breakForce);
					break;
				case SliderJointProperty.BreakTorque:
					_output.SetValue(sliderJoint2D.breakTorque);
					break;
				case SliderJointProperty.ConnectedBody:
					_output.SetValue(sliderJoint2D.connectedBody);
					break;
				case SliderJointProperty.EnableCollision:
					_output.SetToggleValue(sliderJoint2D.enableCollision);
					break;
				case SliderJointProperty.ReactionForce:
					_output.SetValue(sliderJoint2D.reactionForce);
					break;
				case SliderJointProperty.ReactionTorque:
					_output.SetValue(sliderJoint2D.reactionTorque);
					break;
				case SliderJointProperty.Anchor:
					_output.SetValue(sliderJoint2D.anchor);
					break;
				case SliderJointProperty.AutoConfigureConnectedAnchor:
					_output.SetToggleValue(sliderJoint2D.autoConfigureConnectedAnchor);
					break;
				case SliderJointProperty.ConnectedAnchor:
					_output.SetValue(sliderJoint2D.connectedAnchor);
					break;
				case SliderJointProperty.Angle:
					_output.SetValue(sliderJoint2D.angle);
					break;
				case SliderJointProperty.AutoConfigureAngle:
					_output.SetToggleValue(sliderJoint2D.autoConfigureAngle);
					break;
				case SliderJointProperty.JointSpeed:
					_output.SetValue(sliderJoint2D.jointSpeed);
					break;
				case SliderJointProperty.JointTranslation:
					_output.SetValue(sliderJoint2D.jointTranslation);
					break;
				case SliderJointProperty.Limits_Max:
					_output.SetValue(sliderJoint2D.limits.max);
					break;
				case SliderJointProperty.Limits_Min:
					_output.SetValue(sliderJoint2D.limits.min);
					break;
				case SliderJointProperty.LimitState:
					_output.SetValue(sliderJoint2D.limitState);
					break;
				case SliderJointProperty.Motor_MaxTorque:
					_output.SetValue(sliderJoint2D.motor.maxMotorTorque);
					break;
				case SliderJointProperty.Motor_Speed:
					_output.SetValue(sliderJoint2D.motor.motorSpeed);
					break;
				case SliderJointProperty.ReferenceAngle:
					_output.SetValue(sliderJoint2D.referenceAngle);
					break;
				case SliderJointProperty.UseLimits:
					_output.SetToggleValue(sliderJoint2D.useLimits);
					break;
				case SliderJointProperty.UseMotor:
					_output.SetToggleValue(sliderJoint2D.useMotor);
					break;
				}
				return;
			}
			SpringJoint2D springJoint2D = joint2D as SpringJoint2D;
			if (springJoint2D != null)
			{
				switch (_springProperty)
				{
				case SpringJointProperty.BreakForce:
					_output.SetValue(springJoint2D.breakForce);
					break;
				case SpringJointProperty.BreakTorque:
					_output.SetValue(springJoint2D.breakTorque);
					break;
				case SpringJointProperty.ConnectedBody:
					_output.SetValue(springJoint2D.connectedBody);
					break;
				case SpringJointProperty.EnableCollision:
					_output.SetToggleValue(springJoint2D.enableCollision);
					break;
				case SpringJointProperty.ReactionForce:
					_output.SetValue(springJoint2D.reactionForce);
					break;
				case SpringJointProperty.ReactionTorque:
					_output.SetValue(springJoint2D.reactionTorque);
					break;
				case SpringJointProperty.Anchor:
					_output.SetValue(springJoint2D.anchor);
					break;
				case SpringJointProperty.AutoConfigureConnectedAnchor:
					_output.SetToggleValue(springJoint2D.autoConfigureConnectedAnchor);
					break;
				case SpringJointProperty.ConnectedAnchor:
					_output.SetValue(springJoint2D.connectedAnchor);
					break;
				case SpringJointProperty.AutoConfigureDistance:
					_output.SetToggleValue(springJoint2D.autoConfigureDistance);
					break;
				case SpringJointProperty.DampingRatio:
					_output.SetValue(springJoint2D.dampingRatio);
					break;
				case SpringJointProperty.Distance:
					_output.SetValue(springJoint2D.distance);
					break;
				case SpringJointProperty.Frequency:
					_output.SetValue(springJoint2D.frequency);
					break;
				}
				return;
			}
			TargetJoint2D targetJoint2D = joint2D as TargetJoint2D;
			if (targetJoint2D != null)
			{
				switch (_targetProperty)
				{
				case TargetJointProperty.BreakForce:
					_output.SetValue(targetJoint2D.breakForce);
					break;
				case TargetJointProperty.BreakTorque:
					_output.SetValue(targetJoint2D.breakTorque);
					break;
				case TargetJointProperty.ConnectedBody:
					_output.SetValue(targetJoint2D.connectedBody);
					break;
				case TargetJointProperty.EnableCollision:
					_output.SetToggleValue(targetJoint2D.enableCollision);
					break;
				case TargetJointProperty.ReactionForce:
					_output.SetValue(targetJoint2D.reactionForce);
					break;
				case TargetJointProperty.ReactionTorque:
					_output.SetValue(targetJoint2D.reactionTorque);
					break;
				case TargetJointProperty.Anchor:
					_output.SetValue(targetJoint2D.anchor);
					break;
				case TargetJointProperty.AutoConfigureTarget:
					_output.SetToggleValue(targetJoint2D.autoConfigureTarget);
					break;
				case TargetJointProperty.DampingRatio:
					_output.SetValue(targetJoint2D.dampingRatio);
					break;
				case TargetJointProperty.Frequency:
					_output.SetValue(targetJoint2D.frequency);
					break;
				case TargetJointProperty.MaxForce:
					_output.SetValue(targetJoint2D.maxForce);
					break;
				case TargetJointProperty.Target:
					_output.SetValue(targetJoint2D.target);
					break;
				}
				return;
			}
			WheelJoint2D wheelJoint2D = joint2D as WheelJoint2D;
			if (wheelJoint2D != null)
			{
				switch (_wheelProperty)
				{
				case WheelJointProperty.BreakForce:
					_output.SetValue(wheelJoint2D.breakForce);
					break;
				case WheelJointProperty.BreakTorque:
					_output.SetValue(wheelJoint2D.breakTorque);
					break;
				case WheelJointProperty.ConnectedBody:
					_output.SetValue(wheelJoint2D.connectedBody);
					break;
				case WheelJointProperty.EnableCollision:
					_output.SetToggleValue(wheelJoint2D.enableCollision);
					break;
				case WheelJointProperty.ReactionForce:
					_output.SetValue(wheelJoint2D.reactionForce);
					break;
				case WheelJointProperty.ReactionTorque:
					_output.SetValue(wheelJoint2D.reactionTorque);
					break;
				case WheelJointProperty.Anchor:
					_output.SetValue(wheelJoint2D.anchor);
					break;
				case WheelJointProperty.AutoConfigureConnectedAnchor:
					_output.SetValue(wheelJoint2D.autoConfigureConnectedAnchor);
					break;
				case WheelJointProperty.ConnectedAnchor:
					_output.SetValue(wheelJoint2D.connectedAnchor);
					break;
				case WheelJointProperty.JointSpeed:
					_output.SetValue(wheelJoint2D.jointSpeed);
					break;
				case WheelJointProperty.JointTranslation:
					_output.SetValue(wheelJoint2D.jointTranslation);
					break;
				case WheelJointProperty.Motor_MaxTorque:
					_output.SetValue(wheelJoint2D.motor.maxMotorTorque);
					break;
				case WheelJointProperty.Motor_Speed:
					_output.SetValue(wheelJoint2D.motor.motorSpeed);
					break;
				case WheelJointProperty.Suspension_Angle:
					_output.SetValue(wheelJoint2D.suspension.angle);
					break;
				case WheelJointProperty.Suspension_DampingRatio:
					_output.SetValue(wheelJoint2D.suspension.dampingRatio);
					break;
				case WheelJointProperty.Suspension_Frequency:
					_output.SetValue(wheelJoint2D.suspension.frequency);
					break;
				case WheelJointProperty.UseMotor:
					_output.SetToggleValue(wheelJoint2D.useMotor);
					break;
				}
			}
		}
	}
}
