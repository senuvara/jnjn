using UnityEngine;

namespace GameFlow
{
	[AddComponentMenu("")]
	[Help("es", "Lee una propiedad del componente @UnityManual{PointEffector2D} especificado.", null)]
	[Help("en", "Gets a property of the specified @UnityManual{PointEffector2D} component.", null)]
	public class GetPointEffector2DProperty : Action, IExecutableInEditor
	{
		public enum Property
		{
			AngularDrag,
			ColliderMask,
			DistanceScale,
			Drag,
			ForceMagnitude,
			ForceMode,
			ForceSource,
			ForceTarget,
			ForceVariation,
			UseColliderMask
		}

		[SerializeField]
		private PointEffector2D _source;

		[SerializeField]
		private Variable _sourceVar;

		[SerializeField]
		private Property _property;

		[SerializeField]
		private Variable _output;

		protected override void OnReset()
		{
			_source = base.gameObject.GetComponent<PointEffector2D>();
		}

		public override void Setup()
		{
		}

		public override void Execute()
		{
			PointEffector2D pointEffector2D = _sourceVar.GetValueAsComponent(_source, typeof(PointEffector2D)) as PointEffector2D;
			if (!(pointEffector2D == null))
			{
				switch (_property)
				{
				case Property.AngularDrag:
					_output.SetValue(pointEffector2D.angularDrag);
					break;
				case Property.ColliderMask:
					_output.SetValue(pointEffector2D.colliderMask);
					break;
				case Property.DistanceScale:
					_output.SetValue(pointEffector2D.distanceScale);
					break;
				case Property.Drag:
					_output.SetValue(pointEffector2D.drag);
					break;
				case Property.ForceMagnitude:
					_output.SetValue(pointEffector2D.forceMagnitude);
					break;
				case Property.ForceMode:
					_output.SetValue(pointEffector2D.forceMode);
					break;
				case Property.ForceSource:
					_output.SetValue(pointEffector2D.forceSource);
					break;
				case Property.ForceTarget:
					_output.SetValue(pointEffector2D.forceTarget);
					break;
				case Property.ForceVariation:
					_output.SetValue(pointEffector2D.forceVariation);
					break;
				case Property.UseColliderMask:
					_output.SetValue(pointEffector2D.useColliderMask);
					break;
				}
			}
		}
	}
}
