using UnityEngine;

namespace GameFlow
{
	[Help("es", "Comienza a reproducir la animación (@UnityManual{Animation} o @UnityManual{Animator}) especificada.", null)]
	[AddComponentMenu("")]
	[Help("en", "Starts the playback of the specified animation (@UnityManual{Animation} or @UnityManual{Animator}).", null)]
	public class PlayAnimation : Action
	{
		public enum TargetType
		{
			Animation,
			Animator
		}

		[SerializeField]
		private TargetType _targetType;

		[SerializeField]
		private Animation _anim;

		[SerializeField]
		private Variable _animVar;

		[SerializeField]
		private WrapMode _wrapMode;

		[SerializeField]
		private float _speed = 1f;

		[SerializeField]
		private Variable _speedVar;

		[SerializeField]
		private AnimationClip _clip;

		[SerializeField]
		private Variable _clipVar;

		[SerializeField]
		private string _clipName = string.Empty;

		[SerializeField]
		private int _clipIndex;

		[SerializeField]
		private Animator _animator;

		[SerializeField]
		private Variable _animatorVar;

		[SerializeField]
		private string _stateName = string.Empty;

		[SerializeField]
		private Variable _stateNameVar;

		[SerializeField]
		private float _transitionDuration;

		[SerializeField]
		private Variable _transitionDurationVar;

		[SerializeField]
		private bool _restart;

		[SerializeField]
		private Variable _restartVar;

		public Animation target => _animVar.GetValueAsComponent(_anim, typeof(Animation)) as Animation;

		public AnimationClip clip => _clipVar.GetValue(_clip) as AnimationClip;

		public Animator animator => _animatorVar.GetValueAsComponent(_animator, typeof(Animator)) as Animator;

		public string state => _stateNameVar.GetValue(_stateName);

		public float transitionDuration => _transitionDurationVar.GetValue(_transitionDuration);

		public bool restart => _restartVar.GetValue(_restart);

		public override void Execute()
		{
			switch (_targetType)
			{
			case TargetType.Animation:
				Execute(target, clip, _wrapMode, _speedVar.GetValue(_speed));
				break;
			case TargetType.Animator:
				Execute(animator, state, transitionDuration, restart);
				break;
			}
		}

		public static void Execute(Animation animation, AnimationClip clip, WrapMode wrapMode, float speed)
		{
			if (!(animation == null) && !(clip == null))
			{
				animation.SetAnimationSpeed(speed);
				animation.wrapMode = wrapMode;
				animation.Play(clip.name);
			}
		}

		public static void Execute(Animator animator, string state, float transitionDuration, bool restart)
		{
			if (!(animator == null) && (restart || !animator.GetCurrentAnimatorStateInfo(0).IsName(state)))
			{
				animator.CrossFade(state, transitionDuration, 0, 0f);
			}
		}
	}
}
