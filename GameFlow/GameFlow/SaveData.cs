using UnityEngine;

namespace GameFlow
{
	[Help("en", "Saves the persistent data.", null)]
	[Help("es", "Guarda los datos persistentes.", null)]
	[AddComponentMenu("")]
	public class SaveData : Action, IExecutableInEditor
	{
		public override void Execute()
		{
			PlayerPrefs.Save();
		}
	}
}
