using System.Collections.Generic;
using UnityEngine;

namespace GameFlow
{
	[Help("es", "Programa que se ejecutará en cuanto un componente temporizador (@GameFlow{Timer}) en el rango de escucha llegue a su fin.", null)]
	[AddComponentMenu("GameFlow/Programs/On Timer Expire")]
	[Help("en", "Program that will be executed once a @GameFlow{Timer} component in the listening range gets expired.", null)]
	[DisallowMultipleComponent]
	public class OnTimerExpire : EventProgram
	{
		private Parameter _sourceParam;

		private Timer[] _timers;

		protected Parameter sourceParam
		{
			get
			{
				if (_sourceParam == null)
				{
					_sourceParam = GetParameter("Source");
				}
				return _sourceParam;
			}
		}

		protected override void RegisterAsListener()
		{
			switch (base.listeningRange)
			{
			case EventListeningRange.GameObject:
				if (base.listeningTarget != null)
				{
					SubscribeTo(base.listeningTarget.GetComponents<Timer>());
				}
				break;
			case EventListeningRange.GameObjectHierarchy:
				if (base.listeningTarget != null)
				{
					_timers = SubscribeTo(base.listeningTarget.GetComponentsInChildren<Timer>(includeInactive: true));
				}
				break;
			case EventListeningRange.SceneHierarchy:
				TimerExpireEvent.AddListener(this);
				break;
			case EventListeningRange.List:
			{
				if (!(base.listeningList != null))
				{
					break;
				}
				for (int i = 0; i < base.listeningList.Count; i++)
				{
					GameObject gameObject = base.listeningList.GetObjectAt(i) as GameObject;
					if (gameObject != null)
					{
						SubscribeTo(gameObject.GetComponents<Timer>());
					}
				}
				break;
			}
			}
		}

		private Timer[] SubscribeTo(Timer[] timers)
		{
			List<Timer> list = new List<Timer>();
			for (int i = 0; i < timers.Length; i++)
			{
				IEventDispatcher eventDispatcher = timers[i];
				if (eventDispatcher != null)
				{
					eventDispatcher.AddListener(this, typeof(TimerExpireEvent));
					list.Add(timers[i]);
				}
			}
			return list.ToArray();
		}

		protected override void SetParameters(EventObject e)
		{
			TimerExpireEvent timerExpireEvent = e as TimerExpireEvent;
			sourceParam.SetValue(timerExpireEvent.source);
		}

		protected override void UnRegisterAsListener()
		{
			switch (base.listeningRange)
			{
			case EventListeningRange.GameObject:
				if (base.listeningTarget != null)
				{
					UnsubscribeFrom(base.listeningTarget.GetComponents<Timer>(), typeof(TimerExpireEvent));
				}
				break;
			case EventListeningRange.GameObjectHierarchy:
				if (base.listeningTarget != null)
				{
					UnsubscribeFrom(base.listeningTarget.GetComponentsInChildren<Timer>(includeInactive: true), typeof(TimerExpireEvent));
				}
				break;
			case EventListeningRange.SceneHierarchy:
				TimerExpireEvent.RemoveListener(this);
				break;
			case EventListeningRange.List:
			{
				if (!(base.listeningList != null))
				{
					break;
				}
				for (int i = 0; i < base.listeningList.Count; i++)
				{
					GameObject gameObject = base.listeningList.GetObjectAt(i) as GameObject;
					if (gameObject != null)
					{
						UnsubscribeFrom(gameObject.GetComponents<Timer>(), typeof(TimerExpireEvent));
					}
				}
				break;
			}
			}
		}

		public override void EventReceived(EventObject e)
		{
			if (AcceptEvent(e))
			{
				SetParameters(e);
				Timer timer = sourceParam.objectValue as Timer;
				if (timer != null)
				{
					base.ignorePause = timer.ignorePause;
				}
				Restart();
			}
		}

		private void OnTransformChildrenChanged()
		{
			if (base.listeningRange == EventListeningRange.GameObjectHierarchy)
			{
				UnsubscribeFrom(_timers, typeof(TimerExpireEvent));
				_timers = SubscribeTo(base.listeningTarget.GetComponentsInChildren<Timer>(includeInactive: true));
				SubscribeTo(_timers);
			}
		}
	}
}
