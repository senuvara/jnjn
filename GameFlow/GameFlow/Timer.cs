using System;
using System.Collections.Generic;
using UnityEngine;

namespace GameFlow
{
	[Help("es", "Dispara eventos de temporizador una sola vez o en intervalos de tiempo regulares..", null)]
	[DisallowMultipleComponent]
	[AddComponentMenu("GameFlow/Tools/Timer")]
	[Help("en", "Triggers timer events only once or at regular time intervals.", null)]
	public class Timer : BaseBehaviour, IEventDispatcher, IIdentifiable, ITool, IVariableFriendly
	{
		[SerializeField]
		public TimerType _type;

		[SerializeField]
		public Variable _typeVar;

		[SerializeField]
		public float _duration = 1f;

		[SerializeField]
		public Variable _durationVar;

		[SerializeField]
		public bool _ignorePause;

		[SerializeField]
		public Variable _ignorePauseVar;

		[SerializeField]
		private bool _autoRestart;

		[SerializeField]
		private Variable _autoRestartVar;

		[SerializeField]
		private bool _preview = true;

		private bool _stopped;

		private bool _finished;

		private TimerObject _timer;

		private List<IEventListener> expireListeners = new List<IEventListener>();

		public TimerType type
		{
			get
			{
				return (TimerType)(object)_typeVar.GetValue(_type);
			}
			set
			{
				_type = value;
			}
		}

		public float duration
		{
			get
			{
				return _durationVar.GetValue(_duration);
			}
			set
			{
				if (_timer != null)
				{
					_timer.duration = value;
				}
				_duration = value;
			}
		}

		public bool ignorePause
		{
			get
			{
				return _ignorePauseVar.GetValue(_ignorePause);
			}
			set
			{
				_ignorePause = value;
			}
		}

		public bool autoRestart
		{
			get
			{
				return _autoRestartVar.GetValue(_autoRestart);
			}
			set
			{
				_autoRestart = value;
			}
		}

		public bool expired => _timer != null && _timer.expired;

		public bool stopped => _stopped;

		public bool finished => _finished;

		public float elapsedTime => (_timer == null) ? 0f : _timer.elapsedTime;

		public float remainingTime => (_timer == null) ? 0f : (_timer.duration - _timer.elapsedTime);

		private void Awake()
		{
			_timer = new TimerObject(_durationVar.GetValue(_duration), _ignorePause);
		}

		private void Start()
		{
			if (!stopped)
			{
				_timer.Restart();
			}
		}

		private void Update()
		{
			if (_stopped || _timer.stopped || _type == TimerType.Chrono)
			{
				return;
			}
			_timer.duration = _durationVar.GetValue(_duration);
			if (_timer.expired)
			{
				_finished = true;
				_timer.Finish();
				DispatchExpireEvent();
				if (_autoRestart && !_stopped)
				{
					_finished = false;
					_timer.Restart();
				}
			}
		}

		private void DispatchExpireEvent()
		{
			TimerExpireEvent timerExpireEvent = new TimerExpireEvent(this);
			timerExpireEvent.DispatchTo(expireListeners);
			timerExpireEvent.DispatchToAll();
		}

		private void OnDisable()
		{
			Stop();
		}

		private void OnEnable()
		{
			Resume();
		}

		public void Restart()
		{
			_finished = false;
			_stopped = false;
			if (base.enabled)
			{
				_timer.Restart();
			}
			else
			{
				_timer.Reset();
			}
		}

		public void Stop(bool doReset = false)
		{
			_stopped = true;
			if (doReset)
			{
				_timer.Reset();
			}
			else
			{
				_timer.Stop();
			}
		}

		public void Resume()
		{
			_stopped = false;
			_timer.Resume();
		}

		public string GetIdForControls()
		{
			return "Timer";
		}

		public string GetIdForSelectors()
		{
			return "Timer";
		}

		public virtual string GetTypeForSelectors()
		{
			return "Timer";
		}

		public void AddListener(IEventListener listener, Type eventType)
		{
			if (eventType == typeof(TimerExpireEvent) && !expireListeners.Contains(listener))
			{
				expireListeners.Add(listener);
			}
		}

		public void RemoveListener(IEventListener listener, Type eventType)
		{
			if (eventType == typeof(TimerExpireEvent) && expireListeners.Contains(listener))
			{
				expireListeners.Remove(listener);
			}
		}
	}
}
