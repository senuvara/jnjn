using UnityEngine;

namespace GameFlow
{
	[Help("es", "Programa que se ejecutará cuando el puntero del ratón deje de estar sobre un componente Collider o GUIElement activado en el rango de escucha.", null)]
	[DisallowMultipleComponent]
	[AddComponentMenu("GameFlow/Programs/On Mouse Exit")]
	[Help("en", "Program that will be executed when the mouse is not longer over an enabled Collider or GUIElement component in the listening range.", null)]
	public class OnMouseExit : OnMouseProgram
	{
		private EventController[] _controllers;

		protected override void RegisterAsListener()
		{
			_controllers = RegisterAsListener(typeof(MouseExitEvent), typeof(MouseController), MouseController.AddController);
			if (base.listeningRange == EventListeningRange.SceneHierarchy)
			{
				MouseExitEvent.AddListener(this);
			}
		}

		protected override void UnRegisterAsListener()
		{
			MouseExitEvent.RemoveListener(this);
			UnRegisterAsListener(typeof(MouseExitEvent), typeof(MouseController));
		}

		private void OnTransformChildrenChanged()
		{
			if (base.listeningRange == EventListeningRange.GameObjectHierarchy)
			{
				UnsubscribeFrom(_controllers, typeof(MouseExitEvent));
				_controllers = EventController.GetControllersInHierarchyOf(typeof(MouseController), base.listeningTarget, MouseController.AddController);
				SubscribeTo(_controllers, typeof(MouseExitEvent));
			}
		}
	}
}
