using System;
using UnityEngine;

namespace GameFlow
{
	[Serializable]
	public class LocalizationSettings : ScriptableObject
	{
		[SerializeField]
		private SystemLanguage _systemLanguage = SystemLanguage.English;

		private static Language _language;

		public static Language GetDefaultLanguage()
		{
			if (_language == null)
			{
				LocalizationSettings localizationSettings = Resources.Load<LocalizationSettings>("Localization");
				if (localizationSettings == null)
				{
					_language = Language.GetInstance(SystemLanguage.English);
				}
				else
				{
					_language = Language.GetInstance(localizationSettings._systemLanguage);
				}
			}
			return _language;
		}
	}
}
