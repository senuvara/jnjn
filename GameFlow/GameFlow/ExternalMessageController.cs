using UnityEngine;

namespace GameFlow
{
	[AddComponentMenu("")]
	public class ExternalMessageController : EventController
	{
		public static void Init()
		{
			Plugin.controlGameObject.AddComponentOnlyOnce<ExternalMessageController>();
		}

		private void String(string message)
		{
			ExternalMessageEvent externalMessageEvent = new ExternalMessageEvent(message);
			externalMessageEvent.DispatchToAll();
		}

		private void Integer(int message)
		{
			ExternalMessageEvent externalMessageEvent = new ExternalMessageEvent(message);
			externalMessageEvent.DispatchToAll();
		}

		private void Float(float message)
		{
			ExternalMessageEvent externalMessageEvent = new ExternalMessageEvent(message);
			externalMessageEvent.DispatchToAll();
		}
	}
}
