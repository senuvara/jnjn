using UnityEngine;
using UnityEngine.Rendering;

namespace GameFlow
{
	[AddComponentMenu("")]
	[Help("es", "Modifica una propiedad del componente @UnityManual{MeshRenderer} especificado.", null)]
	[Help("en", "Sets a property of the specified @UnityManual{MeshRenderer}.", null)]
	public class SetMeshRendererProperty : Action, IExecutableInEditor
	{
		public enum Property
		{
			AdditionalVertexStreams,
			Enabled,
			LightmapIndex,
			LightmapScaleOffset,
			Material,
			Materials,
			ProbeAnchor,
			RealtimeLightmapScaleOffset,
			ReceiveShadows,
			ReflectionProbeUsage,
			ShadowCastingMode,
			SharedMaterial,
			SharedMaterials,
			SortingLayerID,
			SortingLayerName,
			SortingOrder,
			LightProbeUsage
		}

		[SerializeField]
		private MeshRenderer _target;

		[SerializeField]
		private Variable _targetVar;

		[SerializeField]
		private Property _property;

		[SerializeField]
		private Mesh _additionalVertexStreamsValue;

		[SerializeField]
		private bool _enabledValue;

		[SerializeField]
		private int _lightmapIndexValue;

		[SerializeField]
		private Vector4 _lightmapScaleOffsetValue;

		[SerializeField]
		private Material _materialValue;

		[SerializeField]
		private Transform _probeAnchorValue;

		[SerializeField]
		private Vector4 _realtimeLightmapScaleOffsetValue;

		[SerializeField]
		private bool _receiveShadowsValue;

		[SerializeField]
		private ReflectionProbeUsage _reflectionProbeUsageValue;

		[SerializeField]
		private ShadowCastingMode _shadowCastingModeValue;

		[SerializeField]
		private Material _sharedMaterialValue;

		[SerializeField]
		private int _sortingLayerIDValue;

		[SerializeField]
		private string _sortingLayerNameValue;

		[SerializeField]
		private int _sortingOrderValue;

		[SerializeField]
		private LightProbeUsage _lightProbeUsageValue;

		[SerializeField]
		private Variable _valueVar;

		protected override void OnReset()
		{
			_target = base.gameObject.GetComponent<MeshRenderer>();
		}

		public override void Setup()
		{
		}

		public override void Execute()
		{
			MeshRenderer meshRenderer = _targetVar.GetValueAsComponent(_target, typeof(MeshRenderer)) as MeshRenderer;
			if (!(meshRenderer == null))
			{
				switch (_property)
				{
				case Property.Materials:
				case Property.SharedMaterials:
					break;
				case Property.AdditionalVertexStreams:
					meshRenderer.additionalVertexStreams = (_valueVar.GetValue(_additionalVertexStreamsValue) as Mesh);
					break;
				case Property.Enabled:
					meshRenderer.enabled = _valueVar.GetValue(_enabledValue);
					break;
				case Property.LightmapIndex:
					meshRenderer.lightmapIndex = _valueVar.GetValue(_lightmapIndexValue);
					break;
				case Property.LightmapScaleOffset:
					meshRenderer.lightmapScaleOffset = _valueVar.GetValue(_lightmapScaleOffsetValue);
					break;
				case Property.Material:
					meshRenderer.material = (_valueVar.GetValue(_materialValue) as Material);
					break;
				case Property.ProbeAnchor:
					meshRenderer.probeAnchor = (_valueVar.GetValueAsComponent(_probeAnchorValue, typeof(Transform)) as Transform);
					break;
				case Property.RealtimeLightmapScaleOffset:
					meshRenderer.realtimeLightmapScaleOffset = _valueVar.GetValue(_realtimeLightmapScaleOffsetValue);
					break;
				case Property.ReceiveShadows:
					meshRenderer.receiveShadows = _valueVar.GetValue(_receiveShadowsValue);
					break;
				case Property.ReflectionProbeUsage:
					meshRenderer.reflectionProbeUsage = (ReflectionProbeUsage)(object)_valueVar.GetValue(_reflectionProbeUsageValue);
					break;
				case Property.ShadowCastingMode:
					meshRenderer.shadowCastingMode = (ShadowCastingMode)(object)_valueVar.GetValue(_shadowCastingModeValue);
					break;
				case Property.SharedMaterial:
					meshRenderer.sharedMaterial = (_valueVar.GetValue(_sharedMaterialValue) as Material);
					break;
				case Property.SortingLayerID:
					meshRenderer.sortingLayerID = _valueVar.GetValue(_sortingLayerIDValue);
					break;
				case Property.SortingLayerName:
					meshRenderer.sortingLayerName = _valueVar.GetValue(_sortingLayerNameValue);
					break;
				case Property.SortingOrder:
					meshRenderer.sortingOrder = _valueVar.GetValue(_sortingOrderValue);
					break;
				case Property.LightProbeUsage:
					meshRenderer.lightProbeUsage = (LightProbeUsage)(object)_valueVar.GetValue(_lightProbeUsageValue);
					break;
				}
			}
		}
	}
}
