using UnityEngine;

namespace GameFlow
{
	[Help("es", "Modifica el valor de una propiedad del @GameFlow{Timer} especificado.", null)]
	[AddComponentMenu("")]
	[Help("en", "Modifies the values of a property of the specified @GameFlow{Timer}.", null)]
	public class SetTimerProperty : Action, IExecutableInEditor
	{
		public enum Property
		{
			AutoRestart,
			Duration,
			IgnorePause,
			Type
		}

		[SerializeField]
		private Timer _timer;

		[SerializeField]
		private Variable _timerVar;

		[SerializeField]
		private Property _property;

		[SerializeField]
		private bool _boolValue = true;

		[SerializeField]
		private float _floatValue;

		[SerializeField]
		private TimerType _typeValue;

		[SerializeField]
		private Variable _valueVar;

		public override void Setup()
		{
			OnActivate.RegisterAllInactiveAsListeners();
		}

		public override void Execute()
		{
			Timer timer = _timerVar.GetValue(_timer) as Timer;
			if (!(timer == null))
			{
				switch (_property)
				{
				case Property.AutoRestart:
					timer.autoRestart = _valueVar.GetValue(_boolValue);
					break;
				case Property.Duration:
					timer.duration = _valueVar.GetValue(_floatValue);
					break;
				case Property.IgnorePause:
					timer.ignorePause = _valueVar.GetValue(_boolValue);
					break;
				case Property.Type:
					timer.type = (TimerType)(object)_valueVar.GetValue(_typeValue);
					break;
				}
			}
		}
	}
}
