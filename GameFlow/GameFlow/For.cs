using UnityEngine;

namespace GameFlow
{
	[AddComponentMenu("")]
	[Help("es", "Ejecuta las acciones contenidas un número de veces especificado mediante un rango numérico.", null)]
	[Help("en", "Executes the contained actions for a number of times specified by a numeric range.", null)]
	public class For : While
	{
		[SerializeField]
		private Variable _variable;

		[SerializeField]
		private int _startValueInt = 1;

		[SerializeField]
		private float _startValueFloat = 1f;

		[SerializeField]
		private Variable _startValueVar;

		[SerializeField]
		private int _endValueInt = 10;

		[SerializeField]
		private float _endValueFloat = 10f;

		[SerializeField]
		private Variable _endValueVar;

		[SerializeField]
		private int _incrementInt = 1;

		[SerializeField]
		private float _incrementFloat = 1f;

		[SerializeField]
		private Variable _incrementVar;

		private bool firstIteration = true;

		public override void FirstStep()
		{
			if (_variable != null)
			{
				ResetCounter();
			}
			base.FirstStep();
		}

		private void ResetCounter()
		{
			switch (_variable.dataType)
			{
			case DataType.Integer:
				_variable.intValue = _startValueVar.GetValue(_startValueInt);
				break;
			case DataType.Float:
				_variable.floatValue = _startValueVar.GetValue(_startValueFloat);
				break;
			}
			firstIteration = true;
		}

		private void UpdateCounter()
		{
			if (firstIteration)
			{
				firstIteration = false;
				return;
			}
			switch (_variable.dataType)
			{
			case DataType.Integer:
				_variable.intValue += _incrementVar.GetValue(_incrementInt);
				break;
			case DataType.Float:
				_variable.floatValue += _incrementVar.GetValue(_incrementFloat);
				break;
			}
		}

		public override bool Evaluate(bool defaultResult)
		{
			if (_variable == null)
			{
				return defaultResult;
			}
			bool result = defaultResult;
			switch (_variable.dataType)
			{
			case DataType.Integer:
				result = (_variable.intValue != _endValueVar.GetValue(_endValueInt));
				break;
			case DataType.Float:
				result = !Mathf.Approximately(_variable.floatValue, _endValueVar.GetValue(_endValueFloat));
				break;
			}
			UpdateCounter();
			return result;
		}
	}
}
