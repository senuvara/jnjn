using UnityEngine;

namespace GameFlow
{
	[Help("en", "Initializes the progress indicator.", null)]
	[AddComponentMenu("")]
	[Help("es", "Inicializa el indicador de progreso.", null)]
	public class SetupProgress : Action, IExecutableInEditor
	{
		[SerializeField]
		private int _max;

		[SerializeField]
		private Variable _maxVar;

		[SerializeField]
		private string _info;

		[SerializeField]
		private Variable _infoVar;

		public override void Execute()
		{
			Progress.maxValue = _maxVar.GetValue(_max);
			Progress.currentValue = 0;
			Progress.info = _infoVar.GetValue(_info);
		}
	}
}
