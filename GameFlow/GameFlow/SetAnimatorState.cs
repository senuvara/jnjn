using UnityEngine;

namespace GameFlow
{
	[AddComponentMenu("")]
	[Help("en", "Changes the state of the specified @UnityManual{Animator}.", null)]
	[Help("es", "Cambia el estado del @UnityManual{Animator} especificado.", null)]
	public class SetAnimatorState : Action, IExecutableInEditor
	{
		[SerializeField]
		private Animator _target;

		[SerializeField]
		private Variable _targetVar;

		[SerializeField]
		private string _state;

		[SerializeField]
		private Variable _stateVar;

		[SerializeField]
		private float _transitionDuration;

		[SerializeField]
		private Variable _transitionDurationVar;

		[SerializeField]
		private bool _restart;

		[SerializeField]
		private Variable _restartVar;

		public Animator animator => _targetVar.GetValueAsComponent(_target, typeof(Animator)) as Animator;

		public string state => _stateVar.GetValue(_state);

		public float transitionDuration => _transitionDurationVar.GetValue(_transitionDuration);

		public bool restart => _restartVar.GetValue(_restart);

		public override void Execute()
		{
			Execute(animator, state, transitionDuration, restart);
		}

		public static void Execute(Animator animator, string state, float transitionDuration, bool restart)
		{
			if (!(animator == null) && (restart || !animator.GetCurrentAnimatorStateInfo(0).IsName(state)))
			{
				animator.CrossFade(state, transitionDuration, 0, 0f);
			}
		}
	}
}
