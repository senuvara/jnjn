using UnityEngine;

namespace GameFlow
{
	[AddComponentMenu("")]
	[Help("es", "Modifica una de las componentes del vector guardado en la @GameFlow{Variable} especificada.", null)]
	[Help("en", "Sets the value of a component of the vector stored in the specified @GameFlow{Variable}.", null)]
	public class SetVectorComponent : Action, IExecutableInEditor
	{
		[SerializeField]
		private Variable _variable;

		[SerializeField]
		private Vector3Component _component;

		[SerializeField]
		private Variable _componentVar;

		[SerializeField]
		private float _value;

		[SerializeField]
		private Variable _valueVar;

		public Vector3Component component => (Vector3Component)(object)_componentVar.GetValue(_component);

		public float value => _valueVar.GetValue(_value);

		public override void Setup()
		{
		}

		public override void Execute()
		{
			if (!(_variable == null))
			{
				Vector3 vector3Value = _variable.vector3Value;
				switch (component)
				{
				case Vector3Component.X:
					vector3Value.x = value;
					break;
				case Vector3Component.Y:
					vector3Value.y = value;
					break;
				case Vector3Component.Z:
					vector3Value.z = value;
					break;
				}
				_variable.SetValue(vector3Value);
			}
		}
	}
}
