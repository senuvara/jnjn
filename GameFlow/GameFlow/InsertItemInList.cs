using System;
using UnityEngine;

namespace GameFlow
{
	[AddComponentMenu("")]
	[Help("en", "Inserts an item in the given index of the specified @GameFlow{List}.", null)]
	[Help("es", "Inserta un elemento en el índice dado de la @GameFlow{List} especificada.", null)]
	public class InsertItemInList : ListAction
	{
		public override void Execute()
		{
			List list = GetList();
			if (list == null)
			{
				return;
			}
			int index = GetIndex();
			if (index >= List.BASE_INDEX && (index < list.Count || list.Count <= 0))
			{
				switch (list.dataType)
				{
				case DataType.String:
				case DataType.Tag:
					list.InsertValueAt(base.stringValue, index);
					break;
				case DataType.Integer:
				case DataType.Layer:
					list.InsertValueAt(base.intValue, index);
					break;
				case DataType.Float:
					list.InsertValueAt(base.floatValue, index);
					break;
				case DataType.Boolean:
				case DataType.Toggle:
					list.InsertValueAt(base.boolValue, index);
					break;
				case DataType.Vector2:
					list.InsertValueAt(base.vector2Value, index);
					break;
				case DataType.Vector3:
					list.InsertValueAt(base.vector3Value, index);
					break;
				case DataType.Vector4:
					list.InsertValueAt(base.vector4Value, index);
					break;
				case DataType.Rect:
					list.InsertValueAt(base.rectValue, index);
					break;
				case DataType.Color:
					list.InsertValueAt(base.colorValue, index);
					break;
				case DataType.Object:
					list.InsertValueAt(base.objectValue, index);
					break;
				case DataType.Enum:
					list.InsertValueAt((Enum)Enum.ToObject(list.type, base.enumIntValue), index);
					break;
				case DataType.AnimationCurve:
					list.InsertValueAt(base.animationCurveValue, index);
					break;
				case DataType.Bounds:
					list.InsertValueAt(base.boundsValue, index);
					break;
				case DataType.Quaternion:
					list.InsertValueAt(base.quaternionValue, index);
					break;
				}
			}
		}
	}
}
