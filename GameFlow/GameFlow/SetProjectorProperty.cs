using UnityEngine;

namespace GameFlow
{
	[AddComponentMenu("")]
	[Help("es", "Modifica una propiedad del componente @UnityManual{Projector} especificado.", null)]
	[Help("en", "Sets a property of the specified @UnityManual{Projector}.", null)]
	public class SetProjectorProperty : Action, IExecutableInEditor
	{
		public enum Property
		{
			AspectRatio,
			FarClipPlane,
			FieldOfView,
			IgnoreLayers,
			Material,
			NearClipPlane,
			Orthographic,
			OrthographicSize
		}

		[SerializeField]
		private Projector _target;

		[SerializeField]
		private Variable _targetVar;

		[SerializeField]
		private Property _property;

		[SerializeField]
		private float _aspectRatioValue;

		[SerializeField]
		private float _farClipPlaneValue;

		[SerializeField]
		private float _fieldOfViewValue;

		[SerializeField]
		private LayerMask _ignoreLayersValue;

		[SerializeField]
		private Material _materialValue;

		[SerializeField]
		private float _nearClipPlaneValue;

		[SerializeField]
		private bool _orthographicValue;

		[SerializeField]
		private float _orthographicSizeValue;

		[SerializeField]
		private Variable _valueVar;

		protected override void OnReset()
		{
			_target = base.gameObject.GetComponent<Projector>();
		}

		public override void Setup()
		{
		}

		public override void Execute()
		{
			Projector projector = _targetVar.GetValueAsComponent(_target, typeof(Projector)) as Projector;
			if (!(projector == null))
			{
				switch (_property)
				{
				case Property.AspectRatio:
					projector.aspectRatio = _valueVar.GetValue(_aspectRatioValue);
					break;
				case Property.FarClipPlane:
					projector.farClipPlane = _valueVar.GetValue(_farClipPlaneValue);
					break;
				case Property.FieldOfView:
					projector.fieldOfView = _valueVar.GetValue(_fieldOfViewValue);
					break;
				case Property.IgnoreLayers:
					projector.ignoreLayers = _valueVar.GetValue(_ignoreLayersValue);
					break;
				case Property.Material:
					projector.material = (_valueVar.GetValue(_materialValue) as Material);
					break;
				case Property.NearClipPlane:
					projector.nearClipPlane = _valueVar.GetValue(_nearClipPlaneValue);
					break;
				case Property.Orthographic:
					projector.orthographic = _valueVar.GetValue(_orthographicValue);
					break;
				case Property.OrthographicSize:
					projector.orthographicSize = _valueVar.GetValue(_orthographicSizeValue);
					break;
				}
			}
		}
	}
}
