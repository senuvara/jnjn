using System.Collections.Generic;

namespace GameFlow
{
	public class KeyboardEvent : EventObject
	{
		private static List<IEventListener> listeners = new List<IEventListener>();

		public Key key;

		public override void DispatchToAll()
		{
			EventDispatcher.DispatchEventToListeners(this, listeners);
		}

		public static void AddListener(IEventListener listener)
		{
			listeners.AddOnlyOnce(listener);
		}

		public static void RemoveListener(IEventListener listener)
		{
			listeners.Remove(listener);
		}
	}
}
