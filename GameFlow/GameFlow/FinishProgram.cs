using UnityEngine;

namespace GameFlow
{
	[Help("en", "Finishes the specified @GameFlow{Program}.", null)]
	[AddComponentMenu("")]
	[Help("es", "Finaliza el programa (@GameFlow{Program}) especificado.", null)]
	public class FinishProgram : Action, IExecutableInEditor
	{
		[SerializeField]
		private Program _program;

		[SerializeField]
		private Variable _programVar;

		protected Program program => _programVar.GetValue(_program) as Program;

		protected override void OnReset()
		{
		}

		public override void Execute()
		{
			Execute(program);
		}

		public static void Execute(Program program)
		{
			if (!(program == null))
			{
				program.Finish();
			}
		}
	}
}
