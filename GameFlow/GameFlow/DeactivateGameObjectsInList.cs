using UnityEngine;

namespace GameFlow
{
	[Help("en", "Deactivates the objects contained in the specified @GameFlow{List}.", null)]
	[Help("es", "Desactiva los objetos contenidos en la @GameFlow{List} especificada.", null)]
	[AddComponentMenu("")]
	public class DeactivateGameObjectsInList : ListAction
	{
		public override void Execute()
		{
			List list = GetList();
			if (!(list != null) || list.dataType != DataType.Object)
			{
				return;
			}
			for (int i = 0; i < list.Count; i++)
			{
				GameObject gameObject = list.GetObjectAt(i) as GameObject;
				if (gameObject != null)
				{
					gameObject.SetActive(value: false);
				}
			}
		}
	}
}
