using UnityEngine;

namespace GameFlow
{
	[Help("es", "Lee una propiedad del componente Joint especificado.", null)]
	[Help("en", "Gets a property of the specified Joint component.", null)]
	[AddComponentMenu("")]
	public class GetJointProperty : Action, IExecutableInEditor
	{
		public enum JointProperty
		{
			Anchor,
			AutoConfigureConnectedAnchor,
			Axis,
			BreakForce,
			BreakTorque,
			ConnectedAnchor,
			ConnectedBody,
			EnableCollision,
			EnablePreprocessing
		}

		public enum CharacterJointProperty
		{
			Anchor,
			AutoConfigureConnectedAnchor,
			Axis,
			BreakForce,
			BreakTorque,
			ConnectedAnchor,
			ConnectedBody,
			EnableCollision,
			EnablePreprocessing,
			EnableProjection,
			HighTwistLimit_Bounciness,
			HighTwistLimit_ContactDistance,
			HighTwistLimit_Limit,
			LowTwistLimit_Bounciness,
			LowTwistLimit_ContactDistance,
			LowTwistLimit_Limit,
			ProjectionAngle,
			ProjectionDistance,
			Swing1Limit_Bounciness,
			Swing1Limit_ContactDistance,
			Swing1Limit_Limit,
			Swing2Limit_Bounciness,
			Swing2Limit_ContactDistance,
			Swing2Limit_Limit,
			SwingAxis,
			SwingLimitSpring_Damper,
			SwingLimitSpring_Spring,
			TwistLimitSpring_Damper,
			TwistLimitSpring_Spring
		}

		public enum HingeJointProperty
		{
			Anchor,
			AutoConfigureConnectedAnchor,
			Axis,
			BreakForce,
			BreakTorque,
			ConnectedAnchor,
			ConnectedBody,
			EnableCollision,
			EnablePreprocessing,
			Angle,
			Limits_BounceMinVelocity,
			Limits_Bounciness,
			Limits_ContactDistance,
			Limits_Max,
			Limits_Min,
			Motor_Force,
			Motor_FreeSpin,
			Motor_TargetVelocity,
			Spring_Damper,
			Spring_Spring,
			Spring_TargetPosition,
			UseLimits,
			UseMotor,
			UseSpring,
			Velocity
		}

		public enum SpringJointProperty
		{
			Anchor,
			AutoConfigureConnectedAnchor,
			Axis,
			BreakForce,
			BreakTorque,
			ConnectedAnchor,
			ConnectedBody,
			EnableCollision,
			EnablePreprocessing,
			Damper,
			MaxDistance,
			MinDistance,
			Spring,
			Tolerance
		}

		public enum FixedJointProperty
		{
			Anchor,
			AutoConfigureConnectedAnchor,
			Axis,
			BreakForce,
			BreakTorque,
			ConnectedAnchor,
			ConnectedBody,
			EnableCollision,
			EnablePreprocessing
		}

		public enum ConfigJointProperty
		{
			Anchor,
			AutoConfigureConnectedAnchor,
			Axis,
			BreakForce,
			BreakTorque,
			ConnectedAnchor,
			ConnectedBody,
			EnableCollision,
			EnablePreprocessing,
			AngularXDrive_MaximumForce,
			AngularXDrive_PositionDamper,
			AngularXDrive_PositionSpring,
			AngularXLimitSpring_Damper,
			AngularXLimitSpring_Spring,
			AngularXMotion,
			AngularYLimit_Bounciness,
			AngularYLimit_ContactDistance,
			AngularYLimit_Limit,
			AngularYMotion,
			AngularYZDrive_MaximumForce,
			AngularYZDrive_PositionDamper,
			AngularYZDrive_PositionSpring,
			AngularYZLimitSpring_Damper,
			AngularYZLimitSpring_Spring,
			AngularZLimit_Bounciness,
			AngularZLimit_ContactDistance,
			AngularZLimit_Limit,
			AngularZMotion,
			ConfiguredInWorldSpace,
			HighAngularXLimit_Bounciness,
			HighAngularXLimit_ContactDistance,
			HighAngularXLimit_Limit,
			LinearLimit_Bounciness,
			LinearLimit_ContactDistance,
			LinearLimit_Limit,
			LinearLimitSpring_Damper,
			LinearLimitSpring_Spring,
			LowAngularXLimit_Bounciness,
			LowAngularXLimit_ContactDistance,
			LowAngularXLimit_Limit,
			ProjectionAngle,
			ProjectionDistance,
			ProjectionMode,
			RotationDriveMode,
			SecondaryAxis,
			SlerpDrive_MaximumForce,
			SlerpDrive_PositionDamper,
			SlerpDrive_PositionSpring,
			SwapBodies,
			TargetAngularVelocity,
			TargetPosition,
			TargetRotation,
			TargetVelocity,
			XDrive_MaximumForce,
			XDrive_PositionDamper,
			XDrive_PositionSpring,
			XMotion,
			YDrive_MaximumForce,
			YDrive_PositionDamper,
			YDrive_PositionSpring,
			YMotion,
			ZDrive_MaximumForce,
			ZDrive_PositionDamper,
			ZDrive_PositionSpring,
			ZMotion
		}

		[SerializeField]
		private Joint _source;

		[SerializeField]
		private Variable _sourceVar;

		[SerializeField]
		private JointProperty _jointProperty;

		[SerializeField]
		private CharacterJointProperty _characterProperty;

		[SerializeField]
		private HingeJointProperty _hingeProperty;

		[SerializeField]
		private SpringJointProperty _springProperty;

		[SerializeField]
		private FixedJointProperty _fixedProperty;

		[SerializeField]
		private ConfigJointProperty _configProperty;

		[SerializeField]
		private Variable _output;

		protected override void OnReset()
		{
			_source = base.gameObject.GetComponent<Joint>();
		}

		public override void Execute()
		{
			Joint joint = _source;
			if (joint == null)
			{
				joint = (_sourceVar.GetValueAsComponent(_source, typeof(Joint)) as Joint);
			}
			if (joint == null)
			{
				return;
			}
			CharacterJoint characterJoint = joint as CharacterJoint;
			if (characterJoint != null)
			{
				switch (_characterProperty)
				{
				case CharacterJointProperty.Anchor:
					_output.SetValue(joint.anchor);
					break;
				case CharacterJointProperty.AutoConfigureConnectedAnchor:
					_output.SetValue(joint.autoConfigureConnectedAnchor);
					break;
				case CharacterJointProperty.Axis:
					_output.SetValue(joint.axis);
					break;
				case CharacterJointProperty.BreakForce:
					_output.SetValue(joint.breakForce);
					break;
				case CharacterJointProperty.BreakTorque:
					_output.SetValue(joint.breakTorque);
					break;
				case CharacterJointProperty.ConnectedAnchor:
					_output.SetValue(joint.connectedAnchor);
					break;
				case CharacterJointProperty.ConnectedBody:
					_output.SetValue(joint.connectedBody);
					break;
				case CharacterJointProperty.EnableCollision:
					_output.SetToggleValue(joint.enableCollision);
					break;
				case CharacterJointProperty.EnablePreprocessing:
					_output.SetToggleValue(joint.enablePreprocessing);
					break;
				case CharacterJointProperty.EnableProjection:
					_output.SetToggleValue(characterJoint.enableProjection);
					break;
				case CharacterJointProperty.HighTwistLimit_Bounciness:
					_output.SetValue(characterJoint.highTwistLimit.bounciness);
					break;
				case CharacterJointProperty.HighTwistLimit_ContactDistance:
					_output.SetValue(characterJoint.highTwistLimit.contactDistance);
					break;
				case CharacterJointProperty.HighTwistLimit_Limit:
					_output.SetValue(characterJoint.highTwistLimit.limit);
					break;
				case CharacterJointProperty.LowTwistLimit_Bounciness:
					_output.SetValue(characterJoint.lowTwistLimit.bounciness);
					break;
				case CharacterJointProperty.LowTwistLimit_ContactDistance:
					_output.SetValue(characterJoint.lowTwistLimit.contactDistance);
					break;
				case CharacterJointProperty.LowTwistLimit_Limit:
					_output.SetValue(characterJoint.lowTwistLimit.limit);
					break;
				case CharacterJointProperty.ProjectionAngle:
					_output.SetValue(characterJoint.projectionAngle);
					break;
				case CharacterJointProperty.ProjectionDistance:
					_output.SetValue(characterJoint.projectionDistance);
					break;
				case CharacterJointProperty.Swing1Limit_Bounciness:
					_output.SetValue(characterJoint.swing1Limit.bounciness);
					break;
				case CharacterJointProperty.Swing1Limit_ContactDistance:
					_output.SetValue(characterJoint.swing1Limit.contactDistance);
					break;
				case CharacterJointProperty.Swing1Limit_Limit:
					_output.SetValue(characterJoint.swing1Limit.limit);
					break;
				case CharacterJointProperty.Swing2Limit_Bounciness:
					_output.SetValue(characterJoint.swing2Limit.bounciness);
					break;
				case CharacterJointProperty.Swing2Limit_ContactDistance:
					_output.SetValue(characterJoint.swing2Limit.contactDistance);
					break;
				case CharacterJointProperty.Swing2Limit_Limit:
					_output.SetValue(characterJoint.swing2Limit.limit);
					break;
				case CharacterJointProperty.SwingAxis:
					_output.SetValue(characterJoint.swingAxis);
					break;
				case CharacterJointProperty.SwingLimitSpring_Damper:
					_output.SetValue(characterJoint.swingLimitSpring.damper);
					break;
				case CharacterJointProperty.SwingLimitSpring_Spring:
					_output.SetValue(characterJoint.swingLimitSpring.spring);
					break;
				case CharacterJointProperty.TwistLimitSpring_Damper:
					_output.SetValue(characterJoint.twistLimitSpring.damper);
					break;
				case CharacterJointProperty.TwistLimitSpring_Spring:
					_output.SetValue(characterJoint.twistLimitSpring.spring);
					break;
				}
				return;
			}
			HingeJoint hingeJoint = joint as HingeJoint;
			if (hingeJoint != null)
			{
				switch (_hingeProperty)
				{
				case HingeJointProperty.Anchor:
					_output.SetValue(joint.anchor);
					break;
				case HingeJointProperty.AutoConfigureConnectedAnchor:
					_output.SetValue(joint.autoConfigureConnectedAnchor);
					break;
				case HingeJointProperty.Axis:
					_output.SetValue(joint.axis);
					break;
				case HingeJointProperty.BreakForce:
					_output.SetValue(joint.breakForce);
					break;
				case HingeJointProperty.BreakTorque:
					_output.SetValue(joint.breakTorque);
					break;
				case HingeJointProperty.ConnectedAnchor:
					_output.SetValue(joint.connectedAnchor);
					break;
				case HingeJointProperty.ConnectedBody:
					_output.SetValue(joint.connectedBody);
					break;
				case HingeJointProperty.EnableCollision:
					_output.SetToggleValue(joint.enableCollision);
					break;
				case HingeJointProperty.EnablePreprocessing:
					_output.SetToggleValue(joint.enablePreprocessing);
					break;
				case HingeJointProperty.Angle:
					_output.SetValue(hingeJoint.angle);
					break;
				case HingeJointProperty.Limits_BounceMinVelocity:
					_output.SetValue(hingeJoint.limits.bounceMinVelocity);
					break;
				case HingeJointProperty.Limits_Bounciness:
					_output.SetValue(hingeJoint.limits.bounciness);
					break;
				case HingeJointProperty.Limits_ContactDistance:
					_output.SetValue(hingeJoint.limits.contactDistance);
					break;
				case HingeJointProperty.Limits_Max:
					_output.SetValue(hingeJoint.limits.max);
					break;
				case HingeJointProperty.Limits_Min:
					_output.SetValue(hingeJoint.limits.min);
					break;
				case HingeJointProperty.Motor_Force:
					_output.SetValue(hingeJoint.motor.force);
					break;
				case HingeJointProperty.Motor_FreeSpin:
					_output.SetValue(hingeJoint.motor.freeSpin);
					break;
				case HingeJointProperty.Motor_TargetVelocity:
					_output.SetValue(hingeJoint.motor.targetVelocity);
					break;
				case HingeJointProperty.Spring_Damper:
				{
					Variable output3 = _output;
					JointSpring spring3 = hingeJoint.spring;
					output3.SetValue(spring3.damper);
					break;
				}
				case HingeJointProperty.Spring_Spring:
				{
					Variable output2 = _output;
					JointSpring spring2 = hingeJoint.spring;
					output2.SetValue(spring2.spring);
					break;
				}
				case HingeJointProperty.Spring_TargetPosition:
				{
					Variable output = _output;
					JointSpring spring = hingeJoint.spring;
					output.SetValue(spring.targetPosition);
					break;
				}
				case HingeJointProperty.UseLimits:
					_output.SetToggleValue(hingeJoint.useLimits);
					break;
				case HingeJointProperty.UseMotor:
					_output.SetToggleValue(hingeJoint.useMotor);
					break;
				case HingeJointProperty.UseSpring:
					_output.SetToggleValue(hingeJoint.useSpring);
					break;
				case HingeJointProperty.Velocity:
					_output.SetValue(hingeJoint.velocity);
					break;
				}
				return;
			}
			SpringJoint springJoint = joint as SpringJoint;
			if (springJoint != null)
			{
				switch (_springProperty)
				{
				case SpringJointProperty.Anchor:
					_output.SetValue(joint.anchor);
					break;
				case SpringJointProperty.AutoConfigureConnectedAnchor:
					_output.SetValue(joint.autoConfigureConnectedAnchor);
					break;
				case SpringJointProperty.Axis:
					_output.SetValue(joint.axis);
					break;
				case SpringJointProperty.BreakForce:
					_output.SetValue(joint.breakForce);
					break;
				case SpringJointProperty.BreakTorque:
					_output.SetValue(joint.breakTorque);
					break;
				case SpringJointProperty.ConnectedAnchor:
					_output.SetValue(joint.connectedAnchor);
					break;
				case SpringJointProperty.ConnectedBody:
					_output.SetValue(joint.connectedBody);
					break;
				case SpringJointProperty.EnableCollision:
					_output.SetToggleValue(joint.enableCollision);
					break;
				case SpringJointProperty.EnablePreprocessing:
					_output.SetToggleValue(joint.enablePreprocessing);
					break;
				case SpringJointProperty.Damper:
					_output.SetValue(springJoint.damper);
					break;
				case SpringJointProperty.MaxDistance:
					_output.SetValue(springJoint.maxDistance);
					break;
				case SpringJointProperty.MinDistance:
					_output.SetValue(springJoint.minDistance);
					break;
				case SpringJointProperty.Spring:
					_output.SetValue(springJoint.spring);
					break;
				case SpringJointProperty.Tolerance:
					_output.SetValue(springJoint.tolerance);
					break;
				}
				return;
			}
			FixedJoint x = joint as FixedJoint;
			if (x != null)
			{
				switch (_fixedProperty)
				{
				case FixedJointProperty.Anchor:
					_output.SetValue(joint.anchor);
					break;
				case FixedJointProperty.AutoConfigureConnectedAnchor:
					_output.SetValue(joint.autoConfigureConnectedAnchor);
					break;
				case FixedJointProperty.Axis:
					_output.SetValue(joint.axis);
					break;
				case FixedJointProperty.BreakForce:
					_output.SetValue(joint.breakForce);
					break;
				case FixedJointProperty.BreakTorque:
					_output.SetValue(joint.breakTorque);
					break;
				case FixedJointProperty.ConnectedAnchor:
					_output.SetValue(joint.connectedAnchor);
					break;
				case FixedJointProperty.ConnectedBody:
					_output.SetValue(joint.connectedBody);
					break;
				case FixedJointProperty.EnableCollision:
					_output.SetToggleValue(joint.enableCollision);
					break;
				case FixedJointProperty.EnablePreprocessing:
					_output.SetToggleValue(joint.enablePreprocessing);
					break;
				}
				return;
			}
			ConfigurableJoint configurableJoint = joint as ConfigurableJoint;
			if (configurableJoint != null)
			{
				switch (_configProperty)
				{
				case ConfigJointProperty.Anchor:
					_output.SetValue(joint.anchor);
					break;
				case ConfigJointProperty.AutoConfigureConnectedAnchor:
					_output.SetValue(joint.autoConfigureConnectedAnchor);
					break;
				case ConfigJointProperty.Axis:
					_output.SetValue(joint.axis);
					break;
				case ConfigJointProperty.BreakForce:
					_output.SetValue(joint.breakForce);
					break;
				case ConfigJointProperty.BreakTorque:
					_output.SetValue(joint.breakTorque);
					break;
				case ConfigJointProperty.ConnectedAnchor:
					_output.SetValue(joint.connectedAnchor);
					break;
				case ConfigJointProperty.ConnectedBody:
					_output.SetValue(joint.connectedBody);
					break;
				case ConfigJointProperty.EnableCollision:
					_output.SetToggleValue(joint.enableCollision);
					break;
				case ConfigJointProperty.EnablePreprocessing:
					_output.SetToggleValue(joint.enablePreprocessing);
					break;
				case ConfigJointProperty.AngularXDrive_MaximumForce:
					_output.SetValue(configurableJoint.angularXDrive.maximumForce);
					break;
				case ConfigJointProperty.AngularXDrive_PositionDamper:
					_output.SetValue(configurableJoint.angularXDrive.positionDamper);
					break;
				case ConfigJointProperty.AngularXDrive_PositionSpring:
					_output.SetValue(configurableJoint.angularXDrive.positionSpring);
					break;
				case ConfigJointProperty.AngularXLimitSpring_Damper:
					_output.SetValue(configurableJoint.angularXLimitSpring.damper);
					break;
				case ConfigJointProperty.AngularXLimitSpring_Spring:
					_output.SetValue(configurableJoint.angularXLimitSpring.spring);
					break;
				case ConfigJointProperty.AngularXMotion:
					_output.SetValue(configurableJoint.angularXMotion);
					break;
				case ConfigJointProperty.AngularYLimit_Bounciness:
					_output.SetValue(configurableJoint.angularYLimit.bounciness);
					break;
				case ConfigJointProperty.AngularYLimit_ContactDistance:
					_output.SetValue(configurableJoint.angularYLimit.contactDistance);
					break;
				case ConfigJointProperty.AngularYLimit_Limit:
					_output.SetValue(configurableJoint.angularYLimit.limit);
					break;
				case ConfigJointProperty.AngularYMotion:
					_output.SetValue(configurableJoint.angularYMotion);
					break;
				case ConfigJointProperty.AngularYZDrive_MaximumForce:
					_output.SetValue(configurableJoint.angularYZDrive.maximumForce);
					break;
				case ConfigJointProperty.AngularYZDrive_PositionDamper:
					_output.SetValue(configurableJoint.angularYZDrive.positionDamper);
					break;
				case ConfigJointProperty.AngularYZDrive_PositionSpring:
					_output.SetValue(configurableJoint.angularYZDrive.positionSpring);
					break;
				case ConfigJointProperty.AngularYZLimitSpring_Damper:
					_output.SetValue(configurableJoint.angularYZLimitSpring.damper);
					break;
				case ConfigJointProperty.AngularYZLimitSpring_Spring:
					_output.SetValue(configurableJoint.angularYZLimitSpring.spring);
					break;
				case ConfigJointProperty.AngularZLimit_Bounciness:
					_output.SetValue(configurableJoint.angularZLimit.bounciness);
					break;
				case ConfigJointProperty.AngularZLimit_ContactDistance:
					_output.SetValue(configurableJoint.angularZLimit.contactDistance);
					break;
				case ConfigJointProperty.AngularZLimit_Limit:
					_output.SetValue(configurableJoint.angularZLimit.limit);
					break;
				case ConfigJointProperty.AngularZMotion:
					_output.SetValue(configurableJoint.angularZMotion);
					break;
				case ConfigJointProperty.ConfiguredInWorldSpace:
					_output.SetToggleValue(configurableJoint.configuredInWorldSpace);
					break;
				case ConfigJointProperty.HighAngularXLimit_Bounciness:
					_output.SetValue(configurableJoint.highAngularXLimit.bounciness);
					break;
				case ConfigJointProperty.HighAngularXLimit_ContactDistance:
					_output.SetValue(configurableJoint.highAngularXLimit.contactDistance);
					break;
				case ConfigJointProperty.HighAngularXLimit_Limit:
					_output.SetValue(configurableJoint.highAngularXLimit.limit);
					break;
				case ConfigJointProperty.LinearLimit_Bounciness:
					_output.SetValue(configurableJoint.linearLimit.bounciness);
					break;
				case ConfigJointProperty.LinearLimit_ContactDistance:
					_output.SetValue(configurableJoint.linearLimit.contactDistance);
					break;
				case ConfigJointProperty.LinearLimit_Limit:
					_output.SetValue(configurableJoint.linearLimit.limit);
					break;
				case ConfigJointProperty.LinearLimitSpring_Damper:
					_output.SetValue(configurableJoint.linearLimitSpring.damper);
					break;
				case ConfigJointProperty.LinearLimitSpring_Spring:
					_output.SetValue(configurableJoint.linearLimitSpring.spring);
					break;
				case ConfigJointProperty.LowAngularXLimit_Bounciness:
					_output.SetValue(configurableJoint.lowAngularXLimit.bounciness);
					break;
				case ConfigJointProperty.LowAngularXLimit_ContactDistance:
					_output.SetValue(configurableJoint.lowAngularXLimit.contactDistance);
					break;
				case ConfigJointProperty.LowAngularXLimit_Limit:
					_output.SetValue(configurableJoint.lowAngularXLimit.limit);
					break;
				case ConfigJointProperty.ProjectionAngle:
					_output.SetValue(configurableJoint.projectionAngle);
					break;
				case ConfigJointProperty.ProjectionDistance:
					_output.SetValue(configurableJoint.projectionDistance);
					break;
				case ConfigJointProperty.ProjectionMode:
					_output.SetValue(configurableJoint.projectionMode);
					break;
				case ConfigJointProperty.RotationDriveMode:
					_output.SetValue(configurableJoint.rotationDriveMode);
					break;
				case ConfigJointProperty.SecondaryAxis:
					_output.SetValue(configurableJoint.secondaryAxis);
					break;
				case ConfigJointProperty.SlerpDrive_MaximumForce:
					_output.SetValue(configurableJoint.slerpDrive.maximumForce);
					break;
				case ConfigJointProperty.SlerpDrive_PositionDamper:
					_output.SetValue(configurableJoint.slerpDrive.positionDamper);
					break;
				case ConfigJointProperty.SlerpDrive_PositionSpring:
					_output.SetValue(configurableJoint.slerpDrive.positionSpring);
					break;
				case ConfigJointProperty.SwapBodies:
					_output.SetValue(configurableJoint.swapBodies);
					break;
				case ConfigJointProperty.TargetAngularVelocity:
					_output.SetValue(configurableJoint.targetAngularVelocity);
					break;
				case ConfigJointProperty.TargetPosition:
					_output.SetValue(configurableJoint.targetPosition);
					break;
				case ConfigJointProperty.TargetRotation:
					_output.SetValue(configurableJoint.targetRotation);
					break;
				case ConfigJointProperty.TargetVelocity:
					_output.SetValue(configurableJoint.targetVelocity);
					break;
				case ConfigJointProperty.XDrive_MaximumForce:
					_output.SetValue(configurableJoint.xDrive.maximumForce);
					break;
				case ConfigJointProperty.XDrive_PositionDamper:
					_output.SetValue(configurableJoint.xDrive.positionDamper);
					break;
				case ConfigJointProperty.XDrive_PositionSpring:
					_output.SetValue(configurableJoint.xDrive.positionSpring);
					break;
				case ConfigJointProperty.XMotion:
					_output.SetValue(configurableJoint.xMotion);
					break;
				case ConfigJointProperty.YDrive_MaximumForce:
					_output.SetValue(configurableJoint.yDrive.maximumForce);
					break;
				case ConfigJointProperty.YDrive_PositionDamper:
					_output.SetValue(configurableJoint.yDrive.positionDamper);
					break;
				case ConfigJointProperty.YDrive_PositionSpring:
					_output.SetValue(configurableJoint.yDrive.positionSpring);
					break;
				case ConfigJointProperty.YMotion:
					_output.SetValue(configurableJoint.yMotion);
					break;
				case ConfigJointProperty.ZDrive_MaximumForce:
					_output.SetValue(configurableJoint.zDrive.maximumForce);
					break;
				case ConfigJointProperty.ZDrive_PositionDamper:
					_output.SetValue(configurableJoint.zDrive.positionDamper);
					break;
				case ConfigJointProperty.ZDrive_PositionSpring:
					_output.SetValue(configurableJoint.zDrive.positionSpring);
					break;
				case ConfigJointProperty.ZMotion:
					_output.SetValue(configurableJoint.zMotion);
					break;
				}
			}
		}
	}
}
