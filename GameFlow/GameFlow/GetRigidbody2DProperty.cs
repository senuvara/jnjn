using UnityEngine;

namespace GameFlow
{
	[Help("en", "Gets a property of the specified @UnityManual{Rigidbody2D} component.", null)]
	[AddComponentMenu("")]
	[Help("es", "Lee una propiedad del componente @UnityManual{Rigidbody2D} especificado.", null)]
	public class GetRigidbody2DProperty : Action, IExecutableInEditor
	{
		public enum Property
		{
			AngularDrag,
			AngularVelocity,
			CenterOfMass,
			CollisionDetectionMode,
			Constraints,
			Drag,
			FreezeRotation,
			GravityScale,
			Inertia,
			Interpolation,
			IsKinematic,
			Mass,
			Position,
			Rotation,
			Simulated,
			SleepMode,
			Velocity,
			WorldCenterOfMass
		}

		[SerializeField]
		private Rigidbody2D _source;

		[SerializeField]
		private Variable _sourceVar;

		[SerializeField]
		private Property _property;

		[SerializeField]
		private Variable _output;

		protected override void OnReset()
		{
			_source = base.gameObject.GetComponent<Rigidbody2D>();
		}

		public override void Setup()
		{
		}

		public override void Execute()
		{
			Rigidbody2D rigidbody2D = _sourceVar.GetValueAsComponent(_source, typeof(Rigidbody2D)) as Rigidbody2D;
			if (!(rigidbody2D == null))
			{
				switch (_property)
				{
				case Property.AngularDrag:
					_output.SetValue(rigidbody2D.angularDrag);
					break;
				case Property.AngularVelocity:
					_output.SetValue(rigidbody2D.angularVelocity);
					break;
				case Property.CenterOfMass:
					_output.SetValue(rigidbody2D.centerOfMass);
					break;
				case Property.CollisionDetectionMode:
					_output.SetValue(rigidbody2D.collisionDetectionMode);
					break;
				case Property.Constraints:
					_output.SetValue(rigidbody2D.constraints);
					break;
				case Property.Drag:
					_output.SetValue(rigidbody2D.drag);
					break;
				case Property.FreezeRotation:
					_output.SetValue(rigidbody2D.freezeRotation);
					break;
				case Property.GravityScale:
					_output.SetValue(rigidbody2D.gravityScale);
					break;
				case Property.Inertia:
					_output.SetValue(rigidbody2D.inertia);
					break;
				case Property.Interpolation:
					_output.SetValue(rigidbody2D.interpolation);
					break;
				case Property.IsKinematic:
					_output.SetValue(rigidbody2D.isKinematic);
					break;
				case Property.Mass:
					_output.SetValue(rigidbody2D.mass);
					break;
				case Property.Position:
					_output.SetValue(rigidbody2D.position);
					break;
				case Property.Rotation:
					_output.SetValue(rigidbody2D.rotation);
					break;
				case Property.Simulated:
					_output.SetValue(rigidbody2D.simulated);
					break;
				case Property.SleepMode:
					_output.SetValue(rigidbody2D.sleepMode);
					break;
				case Property.Velocity:
					_output.SetValue(rigidbody2D.velocity);
					break;
				case Property.WorldCenterOfMass:
					_output.SetValue(rigidbody2D.worldCenterOfMass);
					break;
				}
			}
		}
	}
}
