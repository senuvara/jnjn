using UnityEngine;

namespace GameFlow
{
	[AddComponentMenu("")]
	[Help("es", "Devuelve el punto de impacto resultado de lanzar el rayo (@GameFlow{Ray}) especificado.", null)]
	[Help("en", "Gets the impact point after casting the specified @GameFlow{Ray}.", null)]
	public class GetImpactPoint : Action
	{
		[SerializeField]
		private Ray _ray;

		[SerializeField]
		private Variable _rayVar;

		[SerializeField]
		private LayerMask _layers;

		[SerializeField]
		private Space _space;

		[SerializeField]
		private Variable _spaceVar;

		[SerializeField]
		private Variable _output;

		protected override void OnReset()
		{
			_layers = 65531;
		}

		public override void Execute()
		{
			if (_output == null)
			{
				return;
			}
			Ray ray = _rayVar.GetValue(_ray) as Ray;
			if (ray == null)
			{
				return;
			}
			UnityEngine.Ray ray2 = ray.ToUnityRay();
			UnityEngine.RaycastHit[] array = Physics.RaycastAll(ray2, ray.length, _layers);
			if (array.Length != 0)
			{
				Space space = (Space)(object)_spaceVar.GetValue(_space);
				Vector3 point = array[0].point;
				if (space == Space.World)
				{
					_output.SetValue(point);
				}
				else
				{
					_output.SetValue(array[0].transform.InverseTransformPoint(point));
				}
			}
		}

		public static Vector3 Execute(Ray ray, LayerMask layers, Space space, Vector3 noImpact)
		{
			if (ray == null)
			{
				return noImpact;
			}
			UnityEngine.Ray ray2 = ray.ToUnityRay();
			UnityEngine.RaycastHit[] array = Physics.RaycastAll(ray2, ray.length, layers);
			if (array.Length == 0)
			{
				return noImpact;
			}
			Vector3 point = array[0].point;
			if (space == Space.World)
			{
				return point;
			}
			return array[0].transform.InverseTransformPoint(point);
		}
	}
}
