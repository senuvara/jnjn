using UnityEngine;

namespace GameFlow
{
	[Help("en", "Program to be executed when the gizmos of the @UnityManual{GameObject} draw in the Editor.", null)]
	[DisallowMultipleComponent]
	[AddComponentMenu("GameFlow/Programs/On Draw Gizmos")]
	[Help("es", "Programa que se ejecutará cuando los gizmos del @UnityManual{GameObject} se dibujen en el Editor.", null)]
	public class OnDrawGizmos : Program
	{
		private void Update()
		{
		}
	}
}
