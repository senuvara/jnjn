using UnityEngine;

namespace GameFlow
{
	[Help("es", "Modifica el valor de una componente del tamaño del Transform especificado.", null)]
	[Help("en", "Sets the value of a component of the scale of the specified Transform.", null)]
	[AddComponentMenu("")]
	public class SetScaleComponent : TransformAction
	{
		[SerializeField]
		private Vector3Component _component;

		[SerializeField]
		private Variable _componentVar;

		[SerializeField]
		private float _value = 1f;

		[SerializeField]
		private Variable _valueVar;

		public Vector3Component component => (Vector3Component)(object)_componentVar.GetValue(_component);

		public float value => _valueVar.GetValue(_value);

		public sealed override void Execute()
		{
			Execute(base.target, component, value, base.relative, base.multiplier);
		}

		public static void Execute(Transform t, Vector3Component component, float value, bool relative, float multiplier)
		{
			if (!(t == null))
			{
				Vector3 localScale = t.localScale;
				switch (component)
				{
				case Vector3Component.X:
					localScale.x = (relative ? (localScale.x + value * multiplier) : value);
					break;
				case Vector3Component.Y:
					localScale.y = (relative ? (localScale.y + value * multiplier) : value);
					break;
				case Vector3Component.Z:
					localScale.z = (relative ? (localScale.z + value * multiplier) : value);
					break;
				}
				t.localScale = localScale;
			}
		}
	}
}
