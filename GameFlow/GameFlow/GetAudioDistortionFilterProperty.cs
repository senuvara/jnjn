using UnityEngine;

namespace GameFlow
{
	[Help("es", "Lee una propiedad del componente @UnityManual{AudioDistortionFilter} especificado.", null)]
	[Help("en", "Gets a property of the specified @UnityManual{AudioDistortionFilter} component.", null)]
	[AddComponentMenu("")]
	public class GetAudioDistortionFilterProperty : Action, IExecutableInEditor
	{
		public enum Property
		{
			DistortionLevel
		}

		[SerializeField]
		private AudioDistortionFilter _source;

		[SerializeField]
		private Variable _sourceVar;

		[SerializeField]
		private Property _property;

		[SerializeField]
		private Variable _output;

		protected override void OnReset()
		{
			_source = base.gameObject.GetComponent<AudioDistortionFilter>();
		}

		public override void Setup()
		{
		}

		public override void Execute()
		{
			AudioDistortionFilter audioDistortionFilter = _sourceVar.GetValueAsComponent(_source, typeof(AudioDistortionFilter)) as AudioDistortionFilter;
			if (!(audioDistortionFilter == null) && _property == Property.DistortionLevel)
			{
				_output.SetValue(audioDistortionFilter.distortionLevel);
			}
		}
	}
}
