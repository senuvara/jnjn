using System.Collections.Generic;
using UnityEngine;

namespace GameFlow
{
	public class EventObject
	{
		public virtual void DispatchTo(IEventListener listener)
		{
			if (listener != null && listener.IsListening())
			{
				listener.EventReceived(this);
			}
		}

		public virtual void DispatchToAll()
		{
		}

		public void DispatchTo(List<IEventListener> listeners)
		{
			EventDispatcher.DispatchEventToListeners(this, listeners);
		}

		public void DispatchTo(List<Object> listeners)
		{
			EventDispatcher.DispatchEventToListeners(this, listeners);
		}
	}
}
