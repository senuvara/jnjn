using System;
using System.Collections;
using System.Collections.Generic;
using UnityEngine;

namespace GameFlow
{
	public static class AnimationUtils
	{
		public static AnimationState GetFirstAnimationState(this Animation animation)
		{
			AnimationState result = null;
			IEnumerator enumerator = animation.GetEnumerator();
			try
			{
				if (enumerator.MoveNext())
				{
					return (AnimationState)enumerator.Current;
				}
				return result;
			}
			finally
			{
				IDisposable disposable = enumerator as IDisposable;
				if (disposable != null)
				{
					disposable.Dispose();
				}
			}
		}

		public static void SetAnimationTime(this Animation animation, float t)
		{
			AnimationState firstAnimationState = animation.GetFirstAnimationState();
			if (firstAnimationState != null)
			{
				firstAnimationState.time = t;
			}
		}

		public static void SetAnimationSpeed(this Animation animation, float speed)
		{
			foreach (AnimationState item in animation)
			{
				item.speed = speed;
			}
		}

		public static float GetAnimationTime(this Animation animation)
		{
			float result = -1f;
			AnimationState firstAnimationState = animation.GetFirstAnimationState();
			if (firstAnimationState != null)
			{
				result = firstAnimationState.time;
			}
			return result;
		}

		public static float GetAnimationDuration(this Animation animation)
		{
			AnimationState firstAnimationState = animation.GetFirstAnimationState();
			float result = -1f;
			if (firstAnimationState != null)
			{
				result = firstAnimationState.length;
			}
			return result;
		}

		public static AnimationState[] GetAnimationStates(this Animation animation)
		{
			List<AnimationState> list = new List<AnimationState>();
			foreach (AnimationState item in animation)
			{
				list.Add(item);
			}
			return list.ToArray();
		}

		public static string[] GetAnimationClipNames(this Animation animation)
		{
			List<string> list = new List<string>();
			foreach (AnimationState item in animation)
			{
				if (item != null && item.clip != null)
				{
					list.Add(item.clip.name);
				}
			}
			return list.ToArray();
		}

		public static int GetIndexOfClipInAnimation(this Animation animation, AnimationClip clip)
		{
			int num = -1;
			foreach (AnimationState item in animation)
			{
				num++;
				if (item != null && clip == item.clip)
				{
					return num;
				}
			}
			return num;
		}
	}
}
