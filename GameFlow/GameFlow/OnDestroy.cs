using UnityEngine;

namespace GameFlow
{
	[AddComponentMenu("GameFlow/Programs/On Destroy")]
	[DisallowMultipleComponent]
	[Help("es", "Programa que se ejecutará una vez el @UnityManual{GameObject} sea destruido.", null)]
	[Help("en", "Program that will be executed once the @UnityManual{GameObject} is destroyed.", null)]
	public class OnDestroy : OnDestroyProgram
	{
	}
}
