using System.Collections.Generic;

namespace GameFlow
{
	public interface ILocalizationContainer : IBlockContainer
	{
		List<Localization> GetLocalizations();

		void LocalizationAdded(Localization localization);
	}
}
