using UnityEngine;

namespace GameFlow
{
	[Help("en", "Flips the specified Transform.", null)]
	[AddComponentMenu("")]
	[Help("es", "Cambia la orientación del Transform especificado.", null)]
	public class Flip : TransformAction
	{
		[SerializeField]
		private Axis _axis;

		[SerializeField]
		private Variable _axisVar;

		public Axis axis => (Axis)(object)_axisVar.GetValue(_axis);

		public sealed override void Execute()
		{
			Execute(base.target, axis);
		}

		public static void Execute(Transform t, Axis axis)
		{
			if (!(t == null))
			{
				Vector3 localScale = t.localScale;
				switch (axis)
				{
				case Axis.X:
					localScale.x *= -1f;
					break;
				case Axis.Y:
					localScale.y *= -1f;
					break;
				case Axis.Z:
					localScale.z *= -1f;
					break;
				}
				t.localScale = localScale;
			}
		}
	}
}
