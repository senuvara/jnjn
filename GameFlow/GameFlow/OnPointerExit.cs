using UnityEngine;

namespace GameFlow
{
	[AddComponentMenu("GameFlow/Programs/On Pointer Exit")]
	[DisallowMultipleComponent]
	[Help("en", "Program that will be executed when the pointer exits the area of a seletable UI component in the listening range.", null)]
	[Help("es", "Programa que se ejecutará cuando el puntero salga del área de un componente seleccionable UI en el rango de escucha.", null)]
	public class OnPointerExit : PointerEventProgram
	{
		private EventController[] _controllers;

		protected override void RegisterAsListener()
		{
			_controllers = RegisterAsListener(typeof(PointerExitEvent), typeof(UIEventController), UIEventController.AddController);
			if (base.listeningRange == EventListeningRange.SceneHierarchy)
			{
				PointerExitEvent.AddListener(this);
			}
		}

		protected override void UnRegisterAsListener()
		{
			PointerExitEvent.RemoveListener(this);
			UnRegisterAsListener(typeof(PointerExitEvent), typeof(UIEventController));
		}

		protected override void SetParameters(EventObject e)
		{
			PointerExitEvent pointerExitEvent = e as PointerExitEvent;
			base.sourceParam.SetValue(pointerExitEvent.source);
			base.positionParam.SetValue(pointerExitEvent.position);
		}

		private void OnTransformChildrenChanged()
		{
			if (base.listeningRange == EventListeningRange.GameObjectHierarchy)
			{
				UnsubscribeFrom(_controllers, typeof(PointerExitEvent));
				_controllers = EventController.GetControllersInHierarchyOf(typeof(UIEventController), base.listeningTarget, UIEventController.AddController);
				SubscribeTo(_controllers, typeof(PointerExitEvent));
			}
		}
	}
}
