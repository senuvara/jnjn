using UnityEngine;

namespace GameFlow
{
	[AddComponentMenu("")]
	[Help("es", "Modifica el valor de una propiedad del componente @UnityManual{CanvasGroup} especificado.", null)]
	[Help("en", "Modifies the value of a property of the specified @UnityManual{CanvasGroup} component.", null)]
	public class SetCanvasGroupProperty : Action, IExecutableInEditor
	{
		public enum Property
		{
			Alpha,
			BlocksRaycasts,
			IgnoreParentGroups,
			Interactable
		}

		[SerializeField]
		private CanvasGroup _target;

		[SerializeField]
		private Variable _targetVar;

		[SerializeField]
		private Property _property;

		[SerializeField]
		private float _alphaValue;

		[SerializeField]
		private bool _blocksRaycastsValue;

		[SerializeField]
		private bool _ignoreParentGroupsValue;

		[SerializeField]
		private bool _interactableValue;

		[SerializeField]
		private Variable _valueVar;

		protected override void OnReset()
		{
			_target = base.gameObject.GetComponent<CanvasGroup>();
		}

		public override void Setup()
		{
		}

		public override void Execute()
		{
			CanvasGroup canvasGroup = _targetVar.GetValueAsComponent(_target, typeof(CanvasGroup)) as CanvasGroup;
			if (!(canvasGroup == null))
			{
				switch (_property)
				{
				case Property.Alpha:
					canvasGroup.alpha = _valueVar.GetValue(_alphaValue);
					break;
				case Property.BlocksRaycasts:
					canvasGroup.blocksRaycasts = _valueVar.GetValue(_blocksRaycastsValue);
					break;
				case Property.IgnoreParentGroups:
					canvasGroup.ignoreParentGroups = _valueVar.GetValue(_ignoreParentGroupsValue);
					break;
				case Property.Interactable:
					canvasGroup.interactable = _valueVar.GetValue(_interactableValue);
					break;
				}
			}
		}
	}
}
