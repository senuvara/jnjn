using UnityEngine;

namespace GameFlow
{
	[Help("es", "Evalúa si el rayo (@GameFlow{Ray}) indicado intersecta el objeto especificado.", null)]
	[Help("en", "Evaluates whether the indicated @GameFlow{Ray} intersects the specified object.", null)]
	[AddComponentMenu("")]
	public class RayCondition : Condition
	{
		public enum Comparison
		{
			AnyCollider,
			NoCollider,
			Collider
		}

		[SerializeField]
		private Ray _ray;

		[SerializeField]
		private Variable _rayVar;

		[SerializeField]
		private Comparison _comparison = Comparison.Collider;

		[SerializeField]
		private Collider _collider;

		[SerializeField]
		private Variable _colliderVar;

		[SerializeField]
		private LayerMask _layers;

		protected override void OnReset()
		{
			_ray = base.gameObject.GetComponent<Ray>();
			_layers = 65531;
		}

		public override bool Evaluate()
		{
			switch (_comparison)
			{
			case Comparison.AnyCollider:
			case Comparison.NoCollider:
				return EvaluateAny();
			case Comparison.Collider:
				return EvaluateColliderType();
			default:
				return false;
			}
		}

		private bool EvaluateAny()
		{
			Ray ray = _rayVar.GetValueAsComponent(_ray, typeof(Ray)) as Ray;
			if (ray == null)
			{
				return (_comparison != 0) ? true : false;
			}
			UnityEngine.Ray ray2 = ray.ToUnityRay();
			if (ray2.direction == Vector3.zero)
			{
				return (_comparison != 0) ? true : false;
			}
			UnityEngine.RaycastHit[] array = Physics.RaycastAll(ray2, ray.length, _layers);
			bool flag = array.Length > 0;
			return (_comparison != 0) ? (!flag) : flag;
		}

		private bool EvaluateColliderType()
		{
			Collider collider = _colliderVar.GetValueAsComponent(_collider, typeof(Collider)) as Collider;
			if (_collider == null)
			{
				return false;
			}
			Ray ray = _rayVar.GetValueAsComponent(_ray, typeof(Ray)) as Ray;
			if (ray == null)
			{
				return false;
			}
			UnityEngine.Ray ray2 = ray.ToUnityRay();
			if (ray2.direction == Vector3.zero)
			{
				return false;
			}
			UnityEngine.RaycastHit hitInfo;
			return collider.Raycast(ray2, out hitInfo, ray.length);
		}
	}
}
