using UnityEngine;

namespace GameFlow
{
	public class ColorUtils
	{
		public static Color CreateColor(int rgba)
		{
			byte r = (byte)((rgba & 4278190080u) >> 24);
			byte g = (byte)((rgba & 0xFF0000) >> 16);
			byte b = (byte)((rgba & 0xFF00) >> 8);
			byte a = (byte)(rgba & 0xFF);
			return new Color32(r, g, b, a);
		}

		public static int GetColorRGBA(int r, int g, int b, int a)
		{
			return (r << 24) | (g << 16) | (b << 8) | a;
		}

		public static Color CreateColor(int r, int g, int b, int a = 255)
		{
			return new Color32((byte)r, (byte)g, (byte)b, (byte)a);
		}

		public static Color CreateColor(Color color, float alpha)
		{
			return new Color(color.r, color.g, color.b, alpha);
		}
	}
}
