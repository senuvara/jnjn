using UnityEngine;

namespace GameFlow
{
	[Help("en", "Gets a property of the specified @UnityManual{AreaEffector2D} component.", null)]
	[AddComponentMenu("")]
	[Help("es", "Lee una propiedad del componente @UnityManual{AreaEffector2D} especificado.", null)]
	public class GetAreaEffector2DProperty : Action, IExecutableInEditor
	{
		public enum Property
		{
			AngularDrag,
			ColliderMask,
			Drag,
			ForceAngle,
			ForceMagnitude,
			ForceTarget,
			ForceVariation,
			UseColliderMask,
			UseGlobalAngle
		}

		[SerializeField]
		private AreaEffector2D _source;

		[SerializeField]
		private Variable _sourceVar;

		[SerializeField]
		private Property _property;

		[SerializeField]
		private Variable _output;

		protected override void OnReset()
		{
			_source = base.gameObject.GetComponent<AreaEffector2D>();
		}

		public override void Setup()
		{
		}

		public override void Execute()
		{
			AreaEffector2D areaEffector2D = _sourceVar.GetValueAsComponent(_source, typeof(AreaEffector2D)) as AreaEffector2D;
			if (!(areaEffector2D == null))
			{
				switch (_property)
				{
				case Property.AngularDrag:
					_output.SetValue(areaEffector2D.angularDrag);
					break;
				case Property.ColliderMask:
					_output.SetValue(areaEffector2D.colliderMask);
					break;
				case Property.Drag:
					_output.SetValue(areaEffector2D.drag);
					break;
				case Property.ForceAngle:
					_output.SetValue(areaEffector2D.forceAngle);
					break;
				case Property.ForceMagnitude:
					_output.SetValue(areaEffector2D.forceMagnitude);
					break;
				case Property.ForceTarget:
					_output.SetValue(areaEffector2D.forceTarget);
					break;
				case Property.ForceVariation:
					_output.SetValue(areaEffector2D.forceVariation);
					break;
				case Property.UseColliderMask:
					_output.SetValue(areaEffector2D.useColliderMask);
					break;
				case Property.UseGlobalAngle:
					_output.SetValue(areaEffector2D.useGlobalAngle);
					break;
				}
			}
		}
	}
}
