using UnityEngine;

namespace GameFlow
{
	[Help("es", "Ajusta la posición 3D de un objeto a partir de su posición 2D de pantalla.", null)]
	[AddComponentMenu("")]
	[Help("en", "Sets the 3D world position of the specified object given its 2D screen position.", null)]
	public class SetPositionFromScreenPoint : Action, IExecutableInEditor
	{
		[SerializeField]
		private Transform _target;

		[SerializeField]
		private Variable _targetVar;

		[SerializeField]
		private Vector2 _screenPosition;

		[SerializeField]
		private Variable _screenPositionVar;

		[SerializeField]
		private bool _relative;

		[SerializeField]
		private float _multiplier = 1f;

		[SerializeField]
		private Variable _multiplierVar;

		protected Transform target => _targetVar.GetValueAsComponent(_target, typeof(Transform)) as Transform;

		protected Vector2 screenPosition => _screenPositionVar.GetValue(_screenPosition);

		protected bool relative => _relative;

		protected float multiplier => _multiplierVar.GetValue(_multiplier);

		protected override void OnReset()
		{
			_target = base.gameObject.GetComponent<Transform>();
		}

		public override void Execute()
		{
			Execute(target, screenPosition, relative, multiplier);
		}

		public static void Execute(Transform t, Vector2 screenPosition, bool relative, float multiplier)
		{
			if (!(t == null))
			{
				Camera main = Camera.main;
				if (!(main == null))
				{
					Vector3 vector = main.WorldToScreenPoint(t.position);
					Vector3 position = relative ? new Vector3(vector.x + screenPosition.x * multiplier, vector.y + screenPosition.y * multiplier, vector.z) : new Vector3(screenPosition.x, screenPosition.y, vector.z);
					t.position = main.ScreenToWorldPoint(position);
				}
			}
		}
	}
}
