using System;
using UnityEngine;

namespace GameFlow
{
	[AddComponentMenu("")]
	[Help("en", "Starts the execution of the specified @GameFlow{Program}.", null)]
	[Help("es", "Inicia la ejecucion del programa (@GameFlow{Program}) especificado.", null)]
	public class ExecuteProgram : Action, IExecutableInEditor
	{
		[SerializeField]
		private Program _program;

		[SerializeField]
		private Variable _programVar;

		[SerializeField]
		private bool _restart = true;

		[SerializeField]
		private Variable _restartVar;

		[SerializeField]
		private bool _wait = true;

		[SerializeField]
		private Variable _waitVar;

		[SerializeField]
		private bool _ignorePause;

		[SerializeField]
		private Variable _ignorePauseVar;

		private Program _runningProgram;

		private static Type[] _noWaitTypes = new Type[4]
		{
			typeof(OnUpdate),
			typeof(OnFixedUpdate),
			typeof(OnLateUpdate),
			typeof(StateMachine)
		};

		public Program program => _programVar.GetValue(_program) as Program;

		protected bool restart => _restartVar.GetValue(_restart);

		protected bool wait => _waitVar.GetValue(_wait);

		protected bool ignorePause => _ignorePauseVar.GetValue(_ignorePause);

		public override void FirstStep()
		{
			UnityEngine.Object rootContainer = GetRootContainer();
			if (rootContainer != null && rootContainer.GetType().IsAnyOf(_noWaitTypes))
			{
				_wait = false;
			}
			_runningProgram = null;
			if (!(program == null) && (!program.running || restart || wait))
			{
				_runningProgram = program;
				_runningProgram.ignorePause = ignorePause;
				_runningProgram.Execute();
			}
		}

		public override bool Finished()
		{
			if (_runningProgram == null || !wait)
			{
				return true;
			}
			if (_runningProgram.finished)
			{
				_runningProgram = null;
				return true;
			}
			return false;
		}
	}
}
