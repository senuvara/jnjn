using UnityEngine;

namespace GameFlow
{
	[Help("es", "Crea una instancia del Prefab especificado.", null)]
	[AddComponentMenu("")]
	[Help("en", "Creates an instance of the specified Prefab.", null)]
	public class Instantiate : Action, IExecutableInEditor
	{
		public enum Positioning
		{
			OriginalPosition,
			SpecifiedPosition
		}

		public enum PositionType
		{
			Position,
			Transform
		}

		[SerializeField]
		private GameObject _original;

		[SerializeField]
		private Variable _originalVar;

		[SerializeField]
		private Positioning _positioning;

		[SerializeField]
		private Variable _positioningVar;

		[SerializeField]
		private PositionType _positionType;

		[SerializeField]
		private Vector3 _position;

		[SerializeField]
		private Variable _positionVar;

		[SerializeField]
		private Transform _transform;

		[SerializeField]
		private Variable _transformVar;

		[SerializeField]
		private bool _activate = true;

		[SerializeField]
		private Variable _output;

		public GameObject original => _originalVar.GetValue(_original) as GameObject;

		public Positioning positioning => (Positioning)(object)_positioningVar.GetValue(_positioning);

		public Vector3 position
		{
			get
			{
				if (_positionType == PositionType.Position)
				{
					return _positionVar.GetValue(_position);
				}
				Transform transform = _transformVar.GetValueAsComponent(_transform, typeof(Transform)) as Transform;
				return transform.position;
			}
		}

		public bool activate => _activate;

		public Variable output => _output;

		public override void Step()
		{
			GameObject source = _originalVar.GetValue(_original) as GameObject;
			GameObject gameObject = null;
			VariableExt.SetValue(value: (positioning != 0) ? Execute(source, _activate, position) : Execute(source, _activate), variable: _output);
		}

		public static GameObject Execute(GameObject source, bool activate)
		{
			GameObject result = null;
			if (source != null)
			{
				result = Execute(source, activate, source.GetComponent<Transform>().position);
			}
			return result;
		}

		public static GameObject Execute(GameObject source, bool activate, Vector3 position)
		{
			GameObject gameObject = null;
			if (source != null)
			{
				gameObject = Object.Instantiate(source, position, source.GetComponent<Transform>().rotation);
				OnActivate.RegisterInactiveAsListener(gameObject);
				if (activate)
				{
					gameObject.SetActive(value: true);
				}
				gameObject.name = source.name;
				gameObject.HideComponents();
			}
			return gameObject;
		}
	}
}
