using UnityEngine;

namespace GameFlow
{
	[AddComponentMenu("")]
	[Help("en", "Resumes the game if it was paused, otherwise pauses the game.", null)]
	[Help("es", "Reanuda el juego si estaba en pausa, en caso contrario lo pone en pausa.", null)]
	public class TogglePause : Action
	{
		public override void Execute()
		{
			Game.TogglePause();
		}
	}
}
