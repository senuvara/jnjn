using UnityEngine;

namespace GameFlow
{
	[Help("es", "Ejecuta las acciones contenidas durante el tiempo especificado.", null)]
	[AddComponentMenu("")]
	[Help("en", "Executes the contained actions during the specified duration.", null)]
	public class During : While
	{
		[SerializeField]
		private float _duration;

		[SerializeField]
		private Variable _durationVar;

		private TimerObject _timer;

		public float duration => _durationVar.GetValue(_duration);

		public override void Setup()
		{
			base.Setup();
			if (_timer == null)
			{
				_timer = new TimerObject(0f);
			}
		}

		private void StartTimer()
		{
			if (_timer != null)
			{
				_timer.duration = _durationVar.GetValue(_duration);
				_timer.Restart();
			}
		}

		public override void FirstStep()
		{
			StartTimer();
			base.FirstStep();
		}

		public override bool Evaluate(bool defaultResult)
		{
			return !_timer.expired;
		}
	}
}
