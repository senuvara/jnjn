using UnityEngine;

namespace GameFlow
{
	[Help("es", "Envía un comando (@GameFlow{Command}) a todos los objetos contenidos en la @GameFlow{List} especificada.", null)]
	[Help("en", "Sends a @GameFlow{Command} to all objects contained in the specified @GameFlow{List}.", null)]
	[AddComponentMenu("")]
	public class SendCommandToList : ListAction
	{
		public enum Order
		{
			FromFirstToLast,
			FromLastToFirst
		}

		[SerializeField]
		private string _commandId;

		[SerializeField]
		private Variable _commandIdVar;

		[SerializeField]
		private bool _restart = true;

		[SerializeField]
		private Variable _restartVar;

		[SerializeField]
		private Order _order;

		protected string commandId => _commandIdVar.GetValue(_commandId);

		protected bool restart => _restartVar.GetValue(_restart);

		public override void Execute()
		{
			Execute(GetList(), commandId, restart, _order);
		}

		public static void Execute(List list, string commandId, bool restart, Order order = Order.FromFirstToLast)
		{
			if (!(list != null) || list.dataType != DataType.Object)
			{
				return;
			}
			if (order == Order.FromFirstToLast)
			{
				for (int i = 0; i < list.Count; i++)
				{
					GameObject gameObject = list.GetObjectAt(i) as GameObject;
					if (gameObject != null)
					{
						SendCommand.Execute(gameObject, commandId, restart);
					}
				}
				return;
			}
			for (int num = list.Count - 1; num >= 0; num--)
			{
				GameObject gameObject2 = list.GetObjectAt(num) as GameObject;
				if (gameObject2 != null)
				{
					SendCommand.Execute(gameObject2, commandId, restart);
				}
			}
		}
	}
}
