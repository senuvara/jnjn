using UnityEngine;

namespace GameFlow
{
	[Help("es", "Devuelve la posición 3D en el mundo del objeto especificado.", null)]
	[Help("en", "Gets the 3D world position of the specified object.", null)]
	[AddComponentMenu("")]
	public class GetPosition : Action, IExecutableInEditor
	{
		[SerializeField]
		private Transform _target;

		[SerializeField]
		private Variable _targetVar;

		[SerializeField]
		private Variable _output;

		protected override void OnReset()
		{
		}

		public override void Setup()
		{
		}

		public override void Execute()
		{
			if (!(_output == null))
			{
				Transform transform = _target;
				if (transform == null)
				{
					transform = (_targetVar.GetValueAsComponent(_target, typeof(Transform)) as Transform);
				}
				if (transform != null)
				{
					_output.SetValue(transform.position);
				}
			}
		}
	}
}
