using System;

namespace GameFlow
{
	[AttributeUsage(AttributeTargets.Field)]
	public class ParameterType : Attribute
	{
		public Type type
		{
			get;
			private set;
		}

		public ParameterType(Type type)
		{
			this.type = type;
		}
	}
}
