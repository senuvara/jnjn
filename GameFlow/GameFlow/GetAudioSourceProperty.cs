using UnityEngine;

namespace GameFlow
{
	[Help("en", "Gets a property of the specified @UnityManual{AudioSource} component.", null)]
	[AddComponentMenu("")]
	[Help("es", "Lee una propiedad del componente @UnityManual{AudioSource} especificado.", null)]
	public class GetAudioSourceProperty : Action, IExecutableInEditor
	{
		public enum Property
		{
			BypassEffects,
			BypassListenerEffects,
			BypassReverbZones,
			Clip,
			DopplerLevel,
			IgnoreListenerPause,
			IgnoreListenerVolume,
			IsPlaying,
			Loop,
			MaxDistance,
			MinDistance,
			Mute,
			PanStereo,
			Pitch,
			PlayOnAwake,
			Priority,
			ReverbZoneMix,
			RolloffMode,
			SpatialBlend,
			Spatialize,
			Spread,
			Time,
			TimeSamples,
			VelocityUpdateMode,
			Volume
		}

		[SerializeField]
		private AudioSource _source;

		[SerializeField]
		private Variable _sourceVar;

		[SerializeField]
		private Property _property = Property.Mute;

		[SerializeField]
		private Variable _output;

		protected override void OnReset()
		{
			_source = base.gameObject.GetComponent<AudioSource>();
		}

		public override void Setup()
		{
		}

		public override void Execute()
		{
			AudioSource audioSource = _sourceVar.GetValueAsComponent(_source, typeof(AudioSource)) as AudioSource;
			if (!(audioSource == null))
			{
				switch (_property)
				{
				case Property.PanStereo:
					break;
				case Property.ReverbZoneMix:
					break;
				case Property.SpatialBlend:
					break;
				case Property.Spatialize:
					break;
				case Property.BypassEffects:
					_output.SetValue(audioSource.bypassEffects);
					break;
				case Property.BypassListenerEffects:
					_output.SetValue(audioSource.bypassListenerEffects);
					break;
				case Property.BypassReverbZones:
					_output.SetValue(audioSource.bypassReverbZones);
					break;
				case Property.Clip:
					_output.SetValue(audioSource.clip);
					break;
				case Property.DopplerLevel:
					_output.SetValue(audioSource.dopplerLevel);
					break;
				case Property.IgnoreListenerPause:
					_output.SetValue(audioSource.ignoreListenerPause);
					break;
				case Property.IgnoreListenerVolume:
					_output.SetValue(audioSource.ignoreListenerVolume);
					break;
				case Property.IsPlaying:
					_output.SetValue(audioSource.isPlaying);
					break;
				case Property.Loop:
					_output.SetValue(audioSource.loop);
					break;
				case Property.MaxDistance:
					_output.SetValue(audioSource.maxDistance);
					break;
				case Property.MinDistance:
					_output.SetValue(audioSource.minDistance);
					break;
				case Property.Mute:
					_output.SetValue(audioSource.mute);
					break;
				case Property.Pitch:
					_output.SetValue(audioSource.pitch);
					break;
				case Property.PlayOnAwake:
					_output.SetValue(audioSource.playOnAwake);
					break;
				case Property.Priority:
					_output.SetValue(audioSource.priority);
					break;
				case Property.RolloffMode:
					_output.SetValue(audioSource.rolloffMode);
					break;
				case Property.Spread:
					_output.SetValue(audioSource.spread);
					break;
				case Property.Time:
					_output.SetValue(audioSource.time);
					break;
				case Property.TimeSamples:
					_output.SetValue(audioSource.timeSamples);
					break;
				case Property.VelocityUpdateMode:
					_output.SetValue(audioSource.velocityUpdateMode);
					break;
				case Property.Volume:
					_output.SetValue(audioSource.volume);
					break;
				}
			}
		}
	}
}
