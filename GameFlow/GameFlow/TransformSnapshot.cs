using UnityEngine;

namespace GameFlow
{
	public class TransformSnapshot
	{
		private Transform _transform;

		private Vector3 _position;

		private Vector3 _rotation;

		private Vector3 _scale;

		public TransformSnapshot(Transform t)
		{
			_transform = t;
			Update();
		}

		public void Update()
		{
			if (_transform != null)
			{
				_position = _transform.position;
				_rotation = _transform.eulerAngles;
				_scale = _transform.localScale;
			}
		}

		public bool Differs()
		{
			if (_transform == null)
			{
				return true;
			}
			return _transform.position != _position || _transform.eulerAngles != _rotation || _transform.localScale != _scale;
		}
	}
}
