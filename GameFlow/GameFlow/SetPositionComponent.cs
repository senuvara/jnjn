using UnityEngine;

namespace GameFlow
{
	[AddComponentMenu("")]
	[Help("es", "Modifica el valor de una componente de la posición del Transform especificado.", null)]
	[Help("en", "Sets the value of a component of the position of the specified Transform.", null)]
	public class SetPositionComponent : TransformAction
	{
		[SerializeField]
		private Vector3Component _component;

		[SerializeField]
		private Variable _componentVar;

		[SerializeField]
		private float _value = 1f;

		[SerializeField]
		private Variable _valueVar;

		public Vector3Component component => (Vector3Component)(object)_componentVar.GetValue(_component);

		public float value => _valueVar.GetValue(_value);

		public sealed override void Execute()
		{
			Execute(base.target, base.space, component, value, base.relative, base.multiplier);
		}

		public static void Execute(Transform t, Space space, Vector3Component component, float value, bool relative, float multiplier)
		{
			if (t == null)
			{
				return;
			}
			switch (space)
			{
			case Space.World:
			{
				Vector3 position = t.position;
				switch (component)
				{
				case Vector3Component.X:
					position.x = (relative ? (position.x + value * multiplier) : value);
					break;
				case Vector3Component.Y:
					position.y = (relative ? (position.y + value * multiplier) : value);
					break;
				case Vector3Component.Z:
					position.z = (relative ? (position.z + value * multiplier) : value);
					break;
				}
				t.position = position;
				break;
			}
			case Space.Self:
			{
				Vector3 localPosition = t.localPosition;
				switch (component)
				{
				case Vector3Component.X:
					localPosition.x = (relative ? (localPosition.x + value * multiplier) : value);
					break;
				case Vector3Component.Y:
					localPosition.y = (relative ? (localPosition.y + value * multiplier) : value);
					break;
				case Vector3Component.Z:
					localPosition.z = (relative ? (localPosition.z + value * multiplier) : value);
					break;
				}
				t.localPosition = localPosition;
				break;
			}
			}
		}
	}
}
