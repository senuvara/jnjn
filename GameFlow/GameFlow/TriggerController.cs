using System;
using System.Collections.Generic;
using UnityEngine;

namespace GameFlow
{
	[Help("en", "Controls the triggering events in this @UnityManual{GameObject}.", null)]
	[AddComponentMenu("")]
	[Help("es", "Controla los eventos de disparadores en este @UnityManual{GameObject}.", null)]
	public class TriggerController : EventController
	{
		[SerializeField]
		private bool _triggerEnterEnabled = true;

		[SerializeField]
		private bool _triggerExitEnabled = true;

		[SerializeField]
		private bool _triggerStayEnabled = true;

		private List<IEventListener> _enterListeners = new List<IEventListener>();

		private List<IEventListener> _exitListeners = new List<IEventListener>();

		private List<IEventListener> _stayListeners = new List<IEventListener>();

		public static EventController AddController(GameObject gameObject)
		{
			Collider collider = gameObject.GetCollider(isTrigger: true);
			return (!(collider != null)) ? null : EventController.AddController(gameObject, typeof(TriggerController));
		}

		private void Awake()
		{
			base.enabled = ((base.enabled && _triggerEnterEnabled) || _triggerExitEnabled || _triggerStayEnabled);
		}

		public override void AddListener(IEventListener listener, Type eventType)
		{
			if (eventType == typeof(TriggerEnterEvent))
			{
				_enterListeners.AddOnlyOnce(listener);
			}
			else if (eventType == typeof(TriggerExitEvent))
			{
				_exitListeners.AddOnlyOnce(listener);
			}
			else if (eventType == typeof(TriggerStayEvent))
			{
				_stayListeners.AddOnlyOnce(listener);
			}
		}

		public override void RemoveListener(IEventListener listener, Type eventType)
		{
			if (eventType == typeof(TriggerEnterEvent))
			{
				_enterListeners.Remove(listener);
			}
			else if (eventType == typeof(TriggerExitEvent))
			{
				_exitListeners.Remove(listener);
			}
			else if (eventType == typeof(TriggerStayEvent))
			{
				_stayListeners.Remove(listener);
			}
		}

		private void OnTriggerEnter(Collider other)
		{
			if (base.enabled && _triggerEnterEnabled)
			{
				TriggerEnterEvent triggerEnterEvent = new TriggerEnterEvent();
				InitEvent(triggerEnterEvent, other);
				triggerEnterEvent.DispatchTo(_enterListeners);
				triggerEnterEvent.DispatchToAll();
			}
		}

		private void OnTriggerExit(Collider other)
		{
			if (base.enabled && _triggerExitEnabled)
			{
				TriggerExitEvent triggerExitEvent = new TriggerExitEvent();
				InitEvent(triggerExitEvent, other);
				triggerExitEvent.DispatchTo(_exitListeners);
				triggerExitEvent.DispatchToAll();
			}
		}

		private void OnTriggerStay(Collider other)
		{
			if (base.enabled && _triggerStayEnabled)
			{
				TriggerStayEvent triggerStayEvent = new TriggerStayEvent();
				InitEvent(triggerStayEvent, other);
				triggerStayEvent.DispatchTo(_stayListeners);
				triggerStayEvent.DispatchToAll();
			}
		}

		private void InitEvent(TriggerEvent te, Collider other)
		{
			te.source = base.gameObject;
			te.other = other.gameObject;
		}
	}
}
