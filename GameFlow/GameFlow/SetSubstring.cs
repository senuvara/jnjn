using UnityEngine;

namespace GameFlow
{
	[Help("es", "Modifica una parte de la cadena almacenada en la @GameFlow{Variable} especificada.", null)]
	[AddComponentMenu("")]
	[Help("en", "Sets a part of the string stored in the specified @GameFlow{Variable}.", null)]
	public class SetSubstring : Action, IExecutableInEditor
	{
		[SerializeField]
		private string _newString;

		[SerializeField]
		private Variable _newStringVar;

		[SerializeField]
		private int _atIndex;

		[SerializeField]
		private Variable _atIndexVar;

		[SerializeField]
		private Variable _variable;

		private static LocalizedString invalidIndex = LocalizedString.Create().Add("en", "Set Substring > Invalid Index.").Add("es", "Set Substring > Índice no válido.");

		public override void Execute()
		{
			Execute(_newStringVar.GetValue(_newString), _atIndexVar.GetValue(_atIndex), _variable, container);
		}

		public static void Execute(string newString, int atIndex, Variable variable, Object context = null)
		{
			if (!(variable == null))
			{
				string value = variable.GetValue(string.Empty);
				if (atIndex < 0 || atIndex >= value.Length)
				{
					Log.Warning(invalidIndex, context);
					return;
				}
				int num = (atIndex + newString.Length > value.Length) ? (value.Length - atIndex) : newString.Length;
				string text = value.Remove(atIndex, num);
				text = text.Insert(atIndex, newString.Substring(0, num));
				variable.SetValue(text);
			}
		}
	}
}
