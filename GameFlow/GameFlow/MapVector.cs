using UnityEngine;

namespace GameFlow
{
	[Help("es", "Modifica una de las componentes del vector guardado en la @GameFlow{Variable} especificada.", null)]
	[AddComponentMenu("")]
	[Help("en", "Maps the components of the vector stored in the specified @GameFlow{Variable}.", null)]
	public class MapVector : Action, IExecutableInEditor
	{
		public enum InputType
		{
			Vector2,
			Vector3
		}

		public enum OutputType
		{
			Vector2,
			Vector3
		}

		public enum Vector2Mapping
		{
			X,
			Y,
			Ignore
		}

		public enum Vector3Mapping
		{
			X,
			Y,
			Z,
			Ignore
		}

		[SerializeField]
		private InputType _inputType;

		[SerializeField]
		private Vector2 _vector2Value;

		[SerializeField]
		private Vector3 _vector3Value;

		[SerializeField]
		private Variable _valueVar;

		[SerializeField]
		private OutputType _outputType = OutputType.Vector3;

		[SerializeField]
		private Vector2Mapping _mapXToV2;

		[SerializeField]
		private Vector2Mapping _mapYToV2 = Vector2Mapping.Ignore;

		[SerializeField]
		private Vector2Mapping _mapZToV2 = Vector2Mapping.Y;

		[SerializeField]
		private Vector3Mapping _mapXToV3;

		[SerializeField]
		private Vector3Mapping _mapYToV3 = Vector3Mapping.Ignore;

		[SerializeField]
		private Vector3Mapping _mapZToV3 = Vector3Mapping.Z;

		[SerializeField]
		private Variable _output;

		public override void Execute()
		{
			if (_output == null)
			{
				return;
			}
			switch (_outputType)
			{
			case OutputType.Vector2:
			{
				Vector2 vector2Value = _output.vector2Value;
				switch (_inputType)
				{
				case InputType.Vector2:
				{
					Vector2 value2 = _valueVar.GetValue(_vector2Value);
					if (_mapXToV2 != Vector2Mapping.Ignore)
					{
						vector2Value.x = value2[(int)_mapXToV2];
					}
					if (_mapYToV2 != Vector2Mapping.Ignore)
					{
						vector2Value.y = value2[(int)_mapYToV2];
					}
					break;
				}
				case InputType.Vector3:
				{
					Vector3 value = _valueVar.GetValue(_vector3Value);
					if (_mapXToV3 != Vector3Mapping.Ignore)
					{
						vector2Value.x = value[(int)_mapXToV3];
					}
					if (_mapYToV3 != Vector3Mapping.Ignore)
					{
						vector2Value.y = value[(int)_mapYToV3];
					}
					break;
				}
				}
				_output.vector2Value = vector2Value;
				break;
			}
			case OutputType.Vector3:
			{
				Vector3 vector3Value = _output.vector3Value;
				switch (_inputType)
				{
				case InputType.Vector2:
				{
					Vector2 value2 = _valueVar.GetValue(_vector2Value);
					if (_mapXToV2 != Vector2Mapping.Ignore)
					{
						vector3Value.x = value2[(int)_mapXToV2];
					}
					if (_mapYToV2 != Vector2Mapping.Ignore)
					{
						vector3Value.y = value2[(int)_mapYToV2];
					}
					if (_mapZToV2 != Vector2Mapping.Ignore)
					{
						vector3Value.z = value2[(int)_mapZToV2];
					}
					break;
				}
				case InputType.Vector3:
				{
					Vector3 value = _valueVar.GetValue(_vector3Value);
					if (_mapXToV3 != Vector3Mapping.Ignore)
					{
						vector3Value.x = value[(int)_mapXToV3];
					}
					if (_mapYToV3 != Vector3Mapping.Ignore)
					{
						vector3Value.y = value[(int)_mapYToV3];
					}
					if (_mapZToV3 != Vector3Mapping.Ignore)
					{
						vector3Value.z = value[(int)_mapZToV3];
					}
					break;
				}
				}
				_output.vector3Value = vector3Value;
				break;
			}
			}
		}
	}
}
