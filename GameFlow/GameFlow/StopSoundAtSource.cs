using UnityEngine;

namespace GameFlow
{
	[AddComponentMenu("")]
	[Help("es", "Deja de reproducir el sonido asociado al @GameFlow{AudioSource} especificado.", null)]
	[Help("en", "Stops the playback of the sound at the specified @GameFlow{AudioSource}.", null)]
	public class StopSoundAtSource : Action
	{
		[SerializeField]
		private AudioSource _audioSource;

		[SerializeField]
		private Variable _audioSourceVar;

		public override void Execute()
		{
			AudioSource audioSource = _audioSourceVar.GetValue(_audioSource) as AudioSource;
			if (audioSource != null)
			{
				audioSource.Stop();
			}
		}
	}
}
