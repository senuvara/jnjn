using UnityEngine;

namespace GameFlow
{
	[Help("es", "Obtiene el último objeto hijo del @UnityManual{GameObject} especificado.", null)]
	[AddComponentMenu("")]
	[Help("en", "Gets the last child of the specified @UnityManual{GameObject}.", null)]
	public class GetLastChild : Action, IExecutableInEditor
	{
		[SerializeField]
		private GameObject _target;

		[SerializeField]
		private Variable _targetVar;

		[SerializeField]
		private Variable _output;

		protected override void OnReset()
		{
			_target = base.gameObject;
		}

		public override void Execute()
		{
			if (_output == null)
			{
				return;
			}
			GameObject gameObject = _targetVar.GetValue(_target) as GameObject;
			if (!(gameObject == null))
			{
				int childCount = gameObject.transform.childCount;
				if (childCount != 0)
				{
					_output.SetValue(gameObject.transform.GetChild(childCount - 1).gameObject);
				}
			}
		}
	}
}
