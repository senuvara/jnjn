using UnityEngine;

namespace GameFlow
{
	[Help("es", "Modifica una propiedad general del sistema de audio.", null)]
	[AddComponentMenu("")]
	[Help("en", "Sets a general property of the audio system.", null)]
	public class SetAudioProperty : Action
	{
		public enum Property
		{
			Mute,
			MusicMute,
			FxMute,
			Volume
		}

		[SerializeField]
		private Property _property;

		[SerializeField]
		private bool _mute;

		[SerializeField]
		private bool _musicMute;

		[SerializeField]
		private bool _fxMute;

		[SerializeField]
		private float _volume;

		[SerializeField]
		private Variable _valueVar;

		protected override void OnReset()
		{
		}

		public override void Execute()
		{
			switch (_property)
			{
			case Property.Mute:
				AudioController.instance.mute = _valueVar.GetValue(_mute);
				break;
			case Property.MusicMute:
				AudioController.instance.musicMute = _valueVar.GetValue(_musicMute);
				break;
			case Property.FxMute:
				AudioController.instance.fxMute = _valueVar.GetValue(_fxMute);
				break;
			case Property.Volume:
				AudioController.instance.volume = _valueVar.GetValue(_volume);
				break;
			}
		}
	}
}
