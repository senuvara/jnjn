using UnityEngine;

namespace GameFlow
{
	[Help("en", "Evaluates the specified conditions.", null)]
	[Help("es", "Evalúa las condiciones especificadas.", null)]
	[AddComponentMenu("")]
	public class Evaluate : EvaluateConditions
	{
	}
}
