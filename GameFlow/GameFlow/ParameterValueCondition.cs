using UnityEngine;

namespace GameFlow
{
	[Help("en", "Evaluates the value of the specified @GameFlow{Parameter}.", null)]
	[AddComponentMenu("")]
	[Help("es", "Evalúa el valor del parámetro (@GameFlow{Parameter}) especificado.", null)]
	public class ParameterValueCondition : VariableValueCondition
	{
	}
}
