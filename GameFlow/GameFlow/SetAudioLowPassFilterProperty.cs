using UnityEngine;

namespace GameFlow
{
	[Help("en", "Sets a property of the specified @UnityManual{AudioLowPassFilter} component.", null)]
	[AddComponentMenu("")]
	[Help("es", "Modifica una propiedad del componente @UnityManual{AudioLowPassFilter} especificado.", null)]
	public class SetAudioLowPassFilterProperty : Action, IExecutableInEditor
	{
		public enum Property
		{
			CustomCutoffCurve,
			CutoffFrequency,
			LowpassResonanceQ
		}

		[SerializeField]
		private AudioLowPassFilter _target;

		[SerializeField]
		private Variable _targetVar;

		[SerializeField]
		private Property _property;

		[SerializeField]
		private AnimationCurve _customCutoffCurveValue;

		[SerializeField]
		private float _cutoffFrequencyValue;

		[SerializeField]
		private float _lowpassResonanceQValue;

		[SerializeField]
		private Variable _valueVar;

		protected override void OnReset()
		{
			_target = base.gameObject.GetComponent<AudioLowPassFilter>();
		}

		public override void Setup()
		{
		}

		public override void Execute()
		{
			AudioLowPassFilter audioLowPassFilter = _targetVar.GetValueAsComponent(_target, typeof(AudioLowPassFilter)) as AudioLowPassFilter;
			if (!(audioLowPassFilter == null))
			{
				switch (_property)
				{
				case Property.CustomCutoffCurve:
					audioLowPassFilter.customCutoffCurve = _valueVar.GetValue(_customCutoffCurveValue);
					break;
				case Property.CutoffFrequency:
					audioLowPassFilter.cutoffFrequency = _valueVar.GetValue(_cutoffFrequencyValue);
					break;
				case Property.LowpassResonanceQ:
					audioLowPassFilter.lowpassResonanceQ = _valueVar.GetValue(_lowpassResonanceQValue);
					break;
				}
			}
		}
	}
}
