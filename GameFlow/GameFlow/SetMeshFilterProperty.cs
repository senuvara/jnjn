using UnityEngine;

namespace GameFlow
{
	[Help("es", "Modifica una propiedad del componente @UnityManual{MeshFilter} especificado.", null)]
	[Help("en", "Sets a property of the specified @UnityManual{MeshFilter}.", null)]
	[AddComponentMenu("")]
	public class SetMeshFilterProperty : Action, IExecutableInEditor
	{
		public enum Property
		{
			Mesh,
			SharedMesh
		}

		[SerializeField]
		private MeshFilter _target;

		[SerializeField]
		private Variable _targetVar;

		[SerializeField]
		private Property _property;

		[SerializeField]
		private Mesh _meshValue;

		[SerializeField]
		private Mesh _sharedMeshValue;

		[SerializeField]
		private Variable _valueVar;

		protected override void OnReset()
		{
			_target = base.gameObject.GetComponent<MeshFilter>();
		}

		public override void Setup()
		{
		}

		public override void Execute()
		{
			MeshFilter meshFilter = _targetVar.GetValueAsComponent(_target, typeof(MeshFilter)) as MeshFilter;
			if (!(meshFilter == null))
			{
				switch (_property)
				{
				case Property.Mesh:
					meshFilter.mesh = (_valueVar.GetValue(_meshValue) as Mesh);
					break;
				case Property.SharedMesh:
					meshFilter.sharedMesh = (_valueVar.GetValue(_sharedMeshValue) as Mesh);
					break;
				}
			}
		}
	}
}
