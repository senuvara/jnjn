namespace GameFlow
{
	public enum MessageType
	{
		Info,
		Warning,
		Error
	}
}
