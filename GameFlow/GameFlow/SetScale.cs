using UnityEngine;

namespace GameFlow
{
	[Help("es", "Modifica el tamaño del objeto especificado.", null)]
	[Help("en", "Sets the scale of the specified object.", null)]
	[AddComponentMenu("")]
	public class SetScale : TransformAction
	{
		public enum ScaleInputType
		{
			Vector3,
			Factor
		}

		[SerializeField]
		private ScaleInputType _inputType;

		[SerializeField]
		private Variable _inputTypeVar;

		[SerializeField]
		private Vector3 _scale;

		[SerializeField]
		private Variable _scaleVar;

		[SerializeField]
		private float _factor;

		[SerializeField]
		private Variable _factorVar;

		public ScaleInputType inputType => (ScaleInputType)(object)_inputTypeVar.GetValue(_inputType);

		public Vector3 scale => _scaleVar.GetValue(_scale);

		public float factor => _factorVar.GetValue(_factor);

		public override void Execute()
		{
			switch (inputType)
			{
			case ScaleInputType.Vector3:
				Execute(base.target, scale, base.relative, base.multiplier, base.mask);
				break;
			case ScaleInputType.Factor:
				Execute(base.target, factor, base.mask);
				break;
			}
		}

		public static void Execute(Transform t, Vector3 scale, bool relative, float multiplier, Vector3 mask)
		{
			if (!(t == null))
			{
				Vector3 value = (!relative) ? scale : (t.localScale + scale * multiplier);
				t.localScale = t.localScale.Masked(value, mask);
			}
		}

		public static void Execute(Transform t, float factor, Vector3 mask)
		{
			if (!(t == null))
			{
				Vector3 value = t.localScale * factor;
				t.localScale = t.localScale.Masked(value, mask);
			}
		}
	}
}
