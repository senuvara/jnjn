using UnityEngine;

namespace GameFlow
{
	[Help("en", "Evaluates general audio properties.", null)]
	[AddComponentMenu("")]
	[Help("es", "Evalúa propiedades generales de audio.", null)]
	public class AudioCondition : Condition
	{
		public enum Comparison
		{
			IsMuted,
			IsNotMuted
		}

		[SerializeField]
		private Comparison _comparison;

		public override bool Evaluate()
		{
			switch (_comparison)
			{
			case Comparison.IsMuted:
				return AudioController.instance.mute;
			case Comparison.IsNotMuted:
				return !AudioController.instance.mute;
			default:
				return false;
			}
		}
	}
}
