using UnityEngine;

namespace GameFlow
{
	[Help("en", "Gets a 2D screen point from the specified 3D world point.", null)]
	[AddComponentMenu("")]
	[Help("es", "Obtiene un punto 2D de pantalla a partir del punto 3D especificado.", null)]
	public class GetScreenPointFromWorldPoint : Action, IExecutableInEditor
	{
		[SerializeField]
		private Vector3 _position;

		[SerializeField]
		private Variable _positionVar;

		[SerializeField]
		private Variable _output;

		public Vector3 position => _positionVar.GetValue(_position);

		public override void Execute()
		{
			if (!(_output == null))
			{
				_output.SetValue(Execute(position));
			}
		}

		public static Vector2 Execute(Vector3 position)
		{
			if (Camera.main == null)
			{
				return Vector2.zero;
			}
			return Camera.main.WorldToScreenPoint(position);
		}
	}
}
