using UnityEngine;
using UnityEngine.AI;

namespace GameFlow
{
	[Help("en", "Sets the destination of the specified @UnityManual{NavMeshAgent} component.", null)]
	[AddComponentMenu("")]
	[Help("es", "Fija el destino del componente @UnityManual{NavMeshAgent} especificado.", null)]
	public class SetNavMeshAgentDestination : Action
	{
		public enum DestinationType
		{
			Transform,
			Vector3
		}

		[SerializeField]
		private NavMeshAgent _agent;

		[SerializeField]
		private Variable _agentVar;

		[SerializeField]
		private DestinationType _destinationType;

		[SerializeField]
		private Transform _destinationTransform;

		[SerializeField]
		private Vector3 _destinationVector3;

		[SerializeField]
		private Variable _destinationVar;

		public NavMeshAgent agent => _agentVar.GetValue(_agent) as NavMeshAgent;

		public Transform destinationTransform => _destinationVar.GetValueAsComponent(_destinationTransform, typeof(Transform)) as Transform;

		public Vector3 destinationVector3 => _destinationVar.GetValue(_destinationVector3);

		protected override void OnReset()
		{
			_agent = (base.gameObject.GetComponent(typeof(NavMeshAgent)) as NavMeshAgent);
		}

		public override void Execute()
		{
			switch (_destinationType)
			{
			case DestinationType.Transform:
				Execute(agent, destinationTransform);
				break;
			case DestinationType.Vector3:
				Execute(agent, destinationVector3);
				break;
			}
		}

		public static void Execute(NavMeshAgent agent, Transform destination)
		{
			if (agent != null && destination != null)
			{
				agent.SetDestination(destination.position);
			}
		}

		public static void Execute(NavMeshAgent agent, Vector3 destination)
		{
			if (agent != null)
			{
				agent.SetDestination(destination);
			}
		}
	}
}
