using System.Collections.Generic;

namespace GameFlow
{
	public interface ILinkContainer : IBlockContainer
	{
		List<Link> GetLinks();

		void LinkAdded(Link link);
	}
}
