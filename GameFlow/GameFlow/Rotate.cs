using UnityEngine;

namespace GameFlow
{
	[Help("en", "Rotates the specified object to match the given target rotation.", null)]
	[AddComponentMenu("")]
	[Help("es", "Rota el objeto especificado hasta que alcance la rotación destino indicada.", null)]
	public class Rotate : Interpolate
	{
		protected override void OnReset()
		{
			base.OnReset();
			base.relative = true;
			base.interpolation = Interpolation.Rotation;
			base.rotationTargetType = TargetType.Transform;
		}

		public override void Step()
		{
			float elapsedTimeDelta = base.timer.elapsedTimeDelta;
			if (_t1 == null)
			{
				_finished = true;
				return;
			}
			switch (base.rotationTargetType)
			{
			case TargetType.Vector3:
				switch (base.space)
				{
				case Space.World:
					_t1.rotation = Quaternion.Slerp(startRotation, toRotation, MathUtils.EaseDelta(elapsedTimeDelta, base.easingType));
					break;
				case Space.Self:
					_t1.localRotation = Quaternion.Slerp(startRotation, toRotation, MathUtils.EaseDelta(elapsedTimeDelta, base.easingType));
					break;
				}
				break;
			case TargetType.Transform:
				if (_t2 == null)
				{
					_finished = true;
				}
				else
				{
					_t1.rotation = Quaternion.Slerp(startRotation, _t2.rotation, MathUtils.EaseDelta(elapsedTimeDelta, base.easingType));
				}
				break;
			}
		}
	}
}
