using UnityEngine;

namespace GameFlow
{
	[Help("es", "Modifica una propiedad de los ajustes globales de Física.", null)]
	[Help("en", "Sets a property of the global Physics Settings.", null)]
	[AddComponentMenu("")]
	public class SetPhysicsProperty : Action, IExecutableInEditor
	{
		public enum Property
		{
			BounceThreshold,
			Gravity,
			DefaultSolverIterations
		}

		[SerializeField]
		private Property _property = Property.Gravity;

		[SerializeField]
		private float _floatValue;

		[SerializeField]
		private Vector3 _vector3Value = new Vector3(0f, -9.81f, 0f);

		[SerializeField]
		private int _intValue;

		[SerializeField]
		private Variable _valueVar;

		public override void Execute()
		{
			switch (_property)
			{
			case Property.BounceThreshold:
				Physics.bounceThreshold = _valueVar.GetValue(_floatValue);
				break;
			case Property.Gravity:
				Physics.gravity = _valueVar.GetValue(_vector3Value);
				break;
			case Property.DefaultSolverIterations:
				Physics.defaultSolverIterations = _valueVar.GetValue(_intValue);
				break;
			}
		}
	}
}
