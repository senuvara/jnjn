using UnityEngine;

namespace GameFlow
{
	[AddComponentMenu("")]
	[Help("es", "Activa todos los componentes de tipo Behaviour de la lista (@GameFlow{List}) especificada.", null)]
	[Help("en", "Enables all the Behaviour components found in the specified @GameFlow{List}.", null)]
	public class EnableBehavioursInList : ListAction
	{
		public override void Execute()
		{
			List list = GetList();
			if (list != null && list.dataType == DataType.Object)
			{
				for (int i = 0; i < list.Count; i++)
				{
					EnableBehaviour.Execute(list.GetObjectAt(i) as Behaviour);
				}
			}
		}
	}
}
