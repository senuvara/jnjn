using UnityEngine;

namespace GameFlow
{
	[AddComponentMenu("")]
	[Help("en", "Evaluates properties of the specified @UnityManual{GameObject}.", null)]
	[Help("es", "Evalúa propiedades del @UnityManual{GameObject} especificado.", null)]
	public class GameObjectCondition : Condition
	{
		public enum Comparison
		{
			IsActive,
			IsNotActive,
			IsThis,
			IsNotThis,
			IsTaggedAs,
			IsNotTaggedAs,
			IsInLayer,
			IsNotInLayer,
			IsEqualTo,
			IsNotEqualTo
		}

		[SerializeField]
		private GameObject _gameObject;

		[SerializeField]
		private Variable _gameObjectVar;

		[SerializeField]
		private Comparison _comparison;

		[SerializeField]
		private int _layer;

		[SerializeField]
		private Variable _layerVar;

		[SerializeField]
		private string _tag = "Untagged";

		[SerializeField]
		private Variable _tagVar;

		[SerializeField]
		private GameObject _other;

		[SerializeField]
		private Variable _otherVar;

		public override bool Evaluate()
		{
			GameObject gameObject = _gameObjectVar.GetValue(_gameObject) as GameObject;
			if (gameObject == null)
			{
				return false;
			}
			switch (_comparison)
			{
			case Comparison.IsActive:
				return gameObject.activeInHierarchy;
			case Comparison.IsNotActive:
				return !gameObject.activeInHierarchy;
			case Comparison.IsThis:
				return gameObject == base.gameObject;
			case Comparison.IsNotThis:
				return gameObject != base.gameObject;
			case Comparison.IsTaggedAs:
				return gameObject.tag.Equals(_tagVar.GetValue(_tag));
			case Comparison.IsNotTaggedAs:
				return !gameObject.tag.Equals(_tagVar.GetValue(_tag));
			case Comparison.IsInLayer:
				return gameObject.layer == _layerVar.GetValue(_layer);
			case Comparison.IsNotInLayer:
				return gameObject.layer != _layerVar.GetValue(_layer);
			case Comparison.IsEqualTo:
				return gameObject.Equals(_otherVar.GetValue(_other) as GameObject);
			case Comparison.IsNotEqualTo:
				return !gameObject.Equals(_otherVar.GetValue(_other) as GameObject);
			default:
				return false;
			}
		}
	}
}
