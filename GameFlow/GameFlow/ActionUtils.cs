using System.Collections.Generic;
using UnityEngine;

namespace GameFlow
{
	public static class ActionUtils
	{
		public static Program GetProgram(this Action action)
		{
			if (action == null)
			{
				return null;
			}
			Object container = action.container;
			while (container != null && container as Action != null)
			{
				container = (container as Action).container;
			}
			return container as Program;
		}

		public static int GetActionCount(IActionContainer actionContainer)
		{
			int num = 0;
			int actionSectionsCount = actionContainer.GetActionSectionsCount();
			for (int i = 0; i < actionSectionsCount; i++)
			{
				List<Action> actions = actionContainer.GetActions(i);
				num += actions.Count;
				foreach (Action item in actions)
				{
					if (item is IActionContainer)
					{
						num += (item as IActionContainer).GetActionCount();
					}
				}
			}
			return num;
		}
	}
}
