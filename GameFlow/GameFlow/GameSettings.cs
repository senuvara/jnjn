using UnityEngine;

namespace GameFlow
{
	public class GameSettings
	{
		private static string LANGUAGE_KEY = "GameFlow.GameSettings.language";

		private static bool initialized;

		private static Language _language;

		public static Language language
		{
			get
			{
				if (!initialized)
				{
					Init();
				}
				return _language;
			}
			set
			{
				if (value == null)
				{
					value = LocalizationSettings.GetDefaultLanguage();
				}
				PlayerPrefs.SetString(LANGUAGE_KEY, value.code);
				_language = value;
				new LanguageEvent(_language).DispatchToAll();
			}
		}

		public static void Init()
		{
			if (!initialized)
			{
				string @string = PlayerPrefs.GetString(LANGUAGE_KEY, LanguageUtils.systemLanguage.code);
				_language = Language.GetInstance(@string);
				initialized = true;
				new LanguageEvent(_language).DispatchToAll();
			}
		}
	}
}
