using UnityEngine;

namespace GameFlow
{
	[Help("es", "Modifica una propiedad del componente @UnityManual{AudioDistortionFilter} especificado.", null)]
	[AddComponentMenu("")]
	[Help("en", "Sets a property of the specified @UnityManual{AudioDistortionFilter} component.", null)]
	public class SetAudioDistortionFilterProperty : Action, IExecutableInEditor
	{
		public enum Property
		{
			DistortionLevel
		}

		[SerializeField]
		private AudioDistortionFilter _target;

		[SerializeField]
		private Variable _targetVar;

		[SerializeField]
		private Property _property;

		[SerializeField]
		private float _distortionLevelValue;

		[SerializeField]
		private Variable _valueVar;

		protected override void OnReset()
		{
			_target = base.gameObject.GetComponent<AudioDistortionFilter>();
		}

		public override void Setup()
		{
		}

		public override void Execute()
		{
			AudioDistortionFilter audioDistortionFilter = _targetVar.GetValueAsComponent(_target, typeof(AudioDistortionFilter)) as AudioDistortionFilter;
			if (!(audioDistortionFilter == null) && _property == Property.DistortionLevel)
			{
				audioDistortionFilter.distortionLevel = _valueVar.GetValue(_distortionLevelValue);
			}
		}
	}
}
