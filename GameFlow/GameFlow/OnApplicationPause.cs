using UnityEngine;

namespace GameFlow
{
	[DisallowMultipleComponent]
	[Help("es", "Programa que se ejecutará cuando la aplicación entre o salga en pausa.", null)]
	[AddComponentMenu("GameFlow/Programs/On Application Pause")]
	[Help("en", "Program that will be executed when the app pauses or resumes from pause.", null)]
	public class OnApplicationPause : EventProgram
	{
		private Parameter _isPausedParam;

		protected Parameter isPausedParam
		{
			get
			{
				if (_isPausedParam == null)
				{
					_isPausedParam = GetParameter("IsPaused");
				}
				return _isPausedParam;
			}
		}

		protected override void CacheParameters()
		{
			CacheParameter(isPausedParam);
		}

		protected override void OnAwake()
		{
			base.OnAwake();
			ApplicationEventController.Init();
		}

		protected override void RegisterAsListener()
		{
			ApplicationPauseEvent.AddListener(this);
		}

		protected override void UnRegisterAsListener()
		{
			ApplicationPauseEvent.RemoveListener(this);
		}

		protected override void SetParameters(EventObject e)
		{
			ApplicationPauseEvent applicationPauseEvent = e as ApplicationPauseEvent;
			isPausedParam.SetValue(applicationPauseEvent.isPaused);
		}
	}
}
