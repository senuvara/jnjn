using UnityEngine;

namespace GameFlow
{
	[AddComponentMenu("")]
	[Help("en", "Sets the material of the specified object.", null)]
	[Help("es", "Cambia el material del objeto especificado.", null)]
	public class SetMaterial : Action, IExecutableInEditor
	{
		[SerializeField]
		private Renderer _target;

		[SerializeField]
		private Variable _targetVar;

		[SerializeField]
		private Material _material;

		[SerializeField]
		private Variable _materialVar;

		public Renderer target => _targetVar.GetValueAsComponent(_target, typeof(Renderer)) as Renderer;

		protected Material material => _materialVar.GetValue(_material) as Material;

		protected override void OnReset()
		{
			_target = base.gameObject.GetComponent<Renderer>();
		}

		public override void Execute()
		{
			Execute(target, material);
		}

		public static void Execute(Renderer renderer, Material material)
		{
			if (!(renderer == null))
			{
				renderer.sharedMaterial = material;
			}
		}
	}
}
