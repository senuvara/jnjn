using UnityEngine;

namespace GameFlow
{
	[Help("en", "Attracts the specified target @UnityManual{Rigidbody2D} towards the indicated point.", null)]
	[AddComponentMenu("")]
	[Help("es", "Atrae el @UnityManual{Rigidbody2D} objetivo especificado hacia el punto indicado.", null)]
	public class Attract2D : Action, IExecutableInEditor
	{
		public enum PointType
		{
			Transform,
			Position
		}

		[SerializeField]
		private Rigidbody2D _target;

		[SerializeField]
		private Variable _targetVar;

		[SerializeField]
		private PointType _pointType;

		[SerializeField]
		private Transform _pointTransform;

		[SerializeField]
		private Variable _pointTransformVar;

		[SerializeField]
		private Vector2 _point;

		[SerializeField]
		private Variable _pointVar;

		[SerializeField]
		private float _acceleration = 10f;

		[SerializeField]
		private Variable _accelerationVar;

		[SerializeField]
		private bool _attenuated;

		[SerializeField]
		private Variable _attenuatedVar;

		public Rigidbody2D target => _targetVar.GetValueAsComponent(_target, typeof(Rigidbody2D)) as Rigidbody2D;

		public Transform pointTransform => _pointTransformVar.GetValueAsComponent(_pointTransform, typeof(Transform)) as Transform;

		public Vector2 point => _pointVar.GetValue(_point);

		public float acceleration => _accelerationVar.GetValue(_acceleration);

		public bool attenuated => _attenuatedVar.GetValue(_attenuated);

		protected override void OnReset()
		{
		}

		public override void Execute()
		{
			switch (_pointType)
			{
			case PointType.Transform:
			{
				Transform pointTransform = this.pointTransform;
				if (pointTransform != null)
				{
					Execute(target, pointTransform.position, acceleration, attenuated);
				}
				break;
			}
			case PointType.Position:
				Execute(target, point, acceleration, attenuated);
				break;
			}
		}

		public static void Execute(Rigidbody2D target, Vector2 point, float acceleration, bool attenuated)
		{
			if (!(target == null))
			{
				Vector2 force = (point - target.position).normalized * acceleration;
				if (attenuated)
				{
					float num = Vector2.Distance(point, target.position);
					num = Mathf.Clamp(num, 0.0001f, num);
					force /= num;
				}
				target.AddForce(force, ForceMode2D.Force);
			}
		}
	}
}
