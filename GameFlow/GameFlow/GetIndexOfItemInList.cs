using UnityEngine;

namespace GameFlow
{
	[AddComponentMenu("")]
	[Help("en", "Gets the index of the given item in the specified @GameFlow{List}.", null)]
	[Help("es", "Devuelve el índice del elemento dado en la @GameFlow{List} especificada.", null)]
	public class GetIndexOfItemInList : ListAction
	{
		[SerializeField]
		private Variable _output;

		public override void Execute()
		{
			if (!(base.list == null) && !(_output == null))
			{
				switch (base.list.dataType)
				{
				case DataType.String:
				case DataType.Tag:
					_output.SetValue(base.list.IndexOf(base.stringValue));
					break;
				case DataType.Integer:
				case DataType.Layer:
					_output.SetValue(base.list.IndexOf(base.intValue));
					break;
				case DataType.Float:
					_output.SetValue(base.list.IndexOf(base.floatValue));
					break;
				case DataType.Boolean:
				case DataType.Toggle:
					_output.SetValue(base.list.IndexOf(base.boolValue));
					break;
				case DataType.Vector2:
					_output.SetValue(base.list.IndexOf(base.vector2Value));
					break;
				case DataType.Vector3:
					_output.SetValue(base.list.IndexOf(base.vector3Value));
					break;
				case DataType.Rect:
					_output.SetValue(base.list.IndexOf(base.rectValue));
					break;
				case DataType.Color:
					_output.SetValue(base.list.IndexOf(base.colorValue));
					break;
				case DataType.Object:
					_output.SetValue(base.list.IndexOf(base.objectValue));
					break;
				case DataType.Enum:
					_output.SetValue(base.list.IndexOf(base.enumIntValue));
					break;
				default:
					_output.SetValue(-1);
					break;
				}
			}
		}
	}
}
