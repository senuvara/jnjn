using UnityEngine;

namespace GameFlow
{
	[Help("en", "Gets a property of the specified GUITexture component.", null)]
	[AddComponentMenu("")]
	[Help("es", "Lee una propiedad del componente GUITexture especificado.", null)]
	public class GetGUITextureProperty : Action, IExecutableInEditor
	{
		public enum Property
		{
			Border,
			Color,
			Enabled,
			InstanceID,
			PixelInset,
			ScreenRect,
			Texture
		}

		[SerializeField]
		private GUITexture _target;

		[SerializeField]
		private Variable _targetVar;

		[SerializeField]
		private Property _property = Property.Texture;

		[SerializeField]
		private Variable _output;

		protected override void OnReset()
		{
			_target = GetComponent<GUITexture>();
		}

		public override void Setup()
		{
		}

		public override void Execute()
		{
			GUITexture gUITexture = _targetVar.GetValueAsComponent(_target, typeof(GUITexture)) as GUITexture;
			if (!(gUITexture == null))
			{
				switch (_property)
				{
				case Property.Border:
					break;
				case Property.Color:
					_output.SetValue(gUITexture.color);
					break;
				case Property.Enabled:
					_output.SetValue(gUITexture.enabled);
					break;
				case Property.InstanceID:
					_output.SetValue(gUITexture.GetInstanceID());
					break;
				case Property.PixelInset:
					_output.SetValue(gUITexture.pixelInset);
					break;
				case Property.ScreenRect:
					_output.SetValue(gUITexture.GetScreenRect());
					break;
				case Property.Texture:
					_output.SetValue(gUITexture.texture);
					break;
				}
			}
		}
	}
}
