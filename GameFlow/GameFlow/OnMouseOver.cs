using UnityEngine;

namespace GameFlow
{
	[Help("es", "Programa que se ejecutará cuando se mueva el puntero del ratón sobre un componente Collider o GUIElement activado en el rango de escucha.", null)]
	[DisallowMultipleComponent]
	[AddComponentMenu("GameFlow/Programs/On Mouse Over")]
	[Help("en", "Program that will be executed when the mouse is moved over an enabled Collider or GUIElement component in the listening range.", null)]
	public class OnMouseOver : OnMouseProgram
	{
		private EventController[] _controllers;

		protected override void RegisterAsListener()
		{
			_controllers = RegisterAsListener(typeof(MouseOverEvent), typeof(MouseController), MouseController.AddController);
			if (base.listeningRange == EventListeningRange.SceneHierarchy)
			{
				MouseOverEvent.AddListener(this);
			}
		}

		protected override void UnRegisterAsListener()
		{
			MouseOverEvent.RemoveListener(this);
			UnRegisterAsListener(typeof(MouseOverEvent), typeof(MouseController));
		}

		private void OnTransformChildrenChanged()
		{
			if (base.listeningRange == EventListeningRange.GameObjectHierarchy)
			{
				UnsubscribeFrom(_controllers, typeof(MouseOverEvent));
				_controllers = EventController.GetControllersInHierarchyOf(typeof(MouseController), base.listeningTarget, MouseController.AddController);
				SubscribeTo(_controllers, typeof(MouseOverEvent));
			}
		}
	}
}
