using UnityEngine;

namespace GameFlow
{
	[Help("en", "Program that will be executed when the game gets paused after a notification from @GameFlow{PauseGame}.", null)]
	[Help("es", "Programa que se ejecutará cuando el juego entre en pausa tras una notificación por parte de @GameFlow{PauseGame}.", null)]
	[DisallowMultipleComponent]
	[AddComponentMenu("GameFlow/Programs/On Game Pause")]
	public class OnGamePause : EventProgram
	{
		protected override void RegisterAsListener()
		{
			base.ignorePause = true;
			GamePauseEvent.AddListener(this);
		}

		protected override void UnRegisterAsListener()
		{
			GamePauseEvent.RemoveListener(this);
		}
	}
}
