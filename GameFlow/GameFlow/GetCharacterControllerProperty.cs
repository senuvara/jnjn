using UnityEngine;

namespace GameFlow
{
	[AddComponentMenu("")]
	[Help("es", "Lee una propiedad del componente @UnityManual{CharacterController} especificado.", null)]
	[Help("en", "Gets a property of the specified @UnityManual{CharacterController} component.", null)]
	public class GetCharacterControllerProperty : Action, IExecutableInEditor
	{
		public enum Property
		{
			AttachedRigidbody,
			Bounds,
			Center,
			CollisionFlags,
			ContactOffset,
			DetectCollisions,
			Enabled,
			Height,
			IsGrounded,
			IsTrigger,
			Material,
			Radius,
			SharedMaterial,
			SlopeLimit,
			StepOffset,
			Velocity,
			SkinWidth
		}

		[SerializeField]
		private CharacterController _source;

		[SerializeField]
		private Variable _sourceVar;

		[SerializeField]
		private Property _property;

		[SerializeField]
		private Variable _output;

		protected override void OnReset()
		{
			_source = base.gameObject.GetComponent<CharacterController>();
		}

		public override void Setup()
		{
		}

		public override void Execute()
		{
			CharacterController characterController = _sourceVar.GetValueAsComponent(_source, typeof(CharacterController)) as CharacterController;
			if (!(characterController == null))
			{
				switch (_property)
				{
				case Property.AttachedRigidbody:
					_output.SetValue(characterController.attachedRigidbody);
					break;
				case Property.Bounds:
					_output.SetValue(characterController.bounds);
					break;
				case Property.Center:
					_output.SetValue(characterController.center);
					break;
				case Property.CollisionFlags:
					_output.SetValue(characterController.collisionFlags);
					break;
				case Property.ContactOffset:
					_output.SetValue(characterController.contactOffset);
					break;
				case Property.DetectCollisions:
					_output.SetValue(characterController.detectCollisions);
					break;
				case Property.Enabled:
					_output.SetValue(characterController.enabled);
					break;
				case Property.Height:
					_output.SetValue(characterController.height);
					break;
				case Property.IsGrounded:
					_output.SetValue(characterController.isGrounded);
					break;
				case Property.IsTrigger:
					_output.SetValue(characterController.isTrigger);
					break;
				case Property.Material:
					_output.SetValue(characterController.material);
					break;
				case Property.Radius:
					_output.SetValue(characterController.radius);
					break;
				case Property.SharedMaterial:
					_output.SetValue(characterController.sharedMaterial);
					break;
				case Property.SlopeLimit:
					_output.SetValue(characterController.slopeLimit);
					break;
				case Property.StepOffset:
					_output.SetValue(characterController.stepOffset);
					break;
				case Property.Velocity:
					_output.SetValue(characterController.velocity);
					break;
				case Property.SkinWidth:
					_output.SetValue(characterController.skinWidth);
					break;
				}
			}
		}
	}
}
