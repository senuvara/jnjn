using UnityEngine;

namespace GameFlow
{
	[AddComponentMenu("")]
	[Help("en", "Gets a property of the global Render Settings.", null)]
	[Help("es", "Lee una propiedad de los ajustes globales de Render.", null)]
	public class GetRenderProperty : Action, IExecutableInEditor
	{
		public enum Property
		{
			AmbientLight,
			FlareFadeSpeed,
			FlareStrength,
			Fog,
			FogColor,
			FogDensity,
			FogEndDistance,
			FogMode,
			FogStartDistance,
			HaloStrength,
			Skybox
		}

		[SerializeField]
		private Property _property = Property.Fog;

		[SerializeField]
		private Variable _output;

		protected override void OnReset()
		{
		}

		public override void Execute()
		{
			switch (_property)
			{
			case Property.AmbientLight:
				_output.SetValue(RenderSettings.ambientLight);
				break;
			case Property.FlareFadeSpeed:
				_output.SetValue(RenderSettings.flareFadeSpeed);
				break;
			case Property.FlareStrength:
				_output.SetValue(RenderSettings.flareStrength);
				break;
			case Property.Fog:
				_output.SetToggleValue(RenderSettings.fog);
				break;
			case Property.FogColor:
				_output.SetValue(RenderSettings.fogColor);
				break;
			case Property.FogDensity:
				_output.SetValue(RenderSettings.fogDensity);
				break;
			case Property.FogEndDistance:
				_output.SetValue(RenderSettings.fogEndDistance);
				break;
			case Property.FogMode:
				_output.SetValue(RenderSettings.fogMode);
				break;
			case Property.FogStartDistance:
				_output.SetValue(RenderSettings.fogStartDistance);
				break;
			case Property.HaloStrength:
				_output.SetValue(RenderSettings.haloStrength);
				break;
			case Property.Skybox:
				_output.SetValue(RenderSettings.skybox);
				break;
			}
		}
	}
}
