using UnityEngine;

namespace GameFlow
{
	[Help("en", "Gets a property of the global Physics Settings.", null)]
	[AddComponentMenu("")]
	[Help("es", "Lee una propiedad de los ajustes globales de Física.", null)]
	public class GetPhysicsProperty : Action, IExecutableInEditor
	{
		public enum Property
		{
			BounceThreshold,
			Gravity,
			DefaultSolverIterations
		}

		[SerializeField]
		private Property _property = Property.Gravity;

		[SerializeField]
		private Variable _output;

		protected override void OnReset()
		{
		}

		public override void Execute()
		{
			switch (_property)
			{
			case Property.BounceThreshold:
				_output.SetValue(Physics.bounceThreshold);
				break;
			case Property.Gravity:
				_output.SetValue(Physics.gravity);
				break;
			case Property.DefaultSolverIterations:
				_output.SetValue(Physics.defaultSolverIterations);
				break;
			}
		}
	}
}
