using UnityEngine;
using UnityEngine.UI;

namespace GameFlow
{
	[Help("es", "Lee una propiedad del componente @UnityManual{Button} especificado.", null)]
	[Help("en", "Gets a property of the specified @UnityManual{Button} component.", null)]
	[AddComponentMenu("")]
	public class GetButtonProperty : Action, IExecutableInEditor
	{
		public enum Property
		{
			AnimationTriggers_DisabledTrigger,
			AnimationTriggers_HighlightedTrigger,
			AnimationTriggers_NormalTrigger,
			AnimationTriggers_PressedTrigger,
			Animator,
			Colors_ColorMultiplier,
			Colors_DisabledColor,
			Colors_FadeDuration,
			Colors_HighlightedColor,
			Colors_NormalColor,
			Colors_PressedColor,
			Image,
			Interactable,
			Navigation_Mode,
			Navigation_SelectOnDown,
			Navigation_SelectOnLeft,
			Navigation_SelectOnRight,
			Navigation_SelectOnUp,
			SpriteState_DisabledSprite,
			SpriteState_HighlightedSprite,
			SpriteState_PressedSprite,
			TargetGraphic,
			Transition
		}

		[SerializeField]
		private Button _source;

		[SerializeField]
		private Variable _sourceVar;

		[SerializeField]
		private Property _property;

		[SerializeField]
		private Variable _output;

		protected override void OnReset()
		{
			_source = base.gameObject.GetComponent<Button>();
		}

		public override void Setup()
		{
		}

		public override void Execute()
		{
			Button button = _sourceVar.GetValueAsComponent(_source, typeof(Button)) as Button;
			if (!(button == null))
			{
				switch (_property)
				{
				case Property.AnimationTriggers_DisabledTrigger:
					_output.SetValue(button.animationTriggers.disabledTrigger);
					break;
				case Property.AnimationTriggers_HighlightedTrigger:
					_output.SetValue(button.animationTriggers.highlightedTrigger);
					break;
				case Property.AnimationTriggers_NormalTrigger:
					_output.SetValue(button.animationTriggers.normalTrigger);
					break;
				case Property.AnimationTriggers_PressedTrigger:
					_output.SetValue(button.animationTriggers.pressedTrigger);
					break;
				case Property.Animator:
					_output.SetValue(button.animator);
					break;
				case Property.Colors_ColorMultiplier:
					_output.SetValue(button.colors.colorMultiplier);
					break;
				case Property.Colors_DisabledColor:
					_output.SetValue(button.colors.disabledColor);
					break;
				case Property.Colors_FadeDuration:
					_output.SetValue(button.colors.fadeDuration);
					break;
				case Property.Colors_HighlightedColor:
					_output.SetValue(button.colors.highlightedColor);
					break;
				case Property.Colors_NormalColor:
					_output.SetValue(button.colors.normalColor);
					break;
				case Property.Colors_PressedColor:
					_output.SetValue(button.colors.pressedColor);
					break;
				case Property.Image:
					_output.SetValue(button.image);
					break;
				case Property.Interactable:
					_output.SetValue(button.interactable);
					break;
				case Property.Navigation_Mode:
					_output.SetValue(button.navigation.mode);
					break;
				case Property.Navigation_SelectOnDown:
					_output.SetValue(button.navigation.selectOnDown);
					break;
				case Property.Navigation_SelectOnLeft:
					_output.SetValue(button.navigation.selectOnLeft);
					break;
				case Property.Navigation_SelectOnRight:
					_output.SetValue(button.navigation.selectOnRight);
					break;
				case Property.Navigation_SelectOnUp:
					_output.SetValue(button.navigation.selectOnUp);
					break;
				case Property.SpriteState_DisabledSprite:
					_output.SetValue(button.spriteState.disabledSprite);
					break;
				case Property.SpriteState_HighlightedSprite:
					_output.SetValue(button.spriteState.highlightedSprite);
					break;
				case Property.SpriteState_PressedSprite:
					_output.SetValue(button.spriteState.pressedSprite);
					break;
				case Property.TargetGraphic:
					_output.SetValue(button.targetGraphic);
					break;
				case Property.Transition:
					_output.SetValue(button.transition);
					break;
				}
			}
		}
	}
}
