using System.Collections.Generic;

namespace GameFlow
{
	public interface IRayContainer : IBlockContainer
	{
		List<Ray> GetRays();

		void RayAdded(Ray ray);
	}
}
