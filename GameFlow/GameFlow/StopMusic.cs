using UnityEngine;

namespace GameFlow
{
	[AddComponentMenu("")]
	[Help("en", "Stops the music with optional volume fading.", null)]
	[Help("es", "Detiene la música con fading de volumen opcional.", null)]
	public class StopMusic : Action
	{
		[SerializeField]
		private float _fading;

		[SerializeField]
		private Variable _fadingVar;

		public override void Execute()
		{
			AudioController.instance.StopMusic(_fadingVar.GetValue(_fading));
		}
	}
}
