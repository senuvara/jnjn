using UnityEngine;

namespace GameFlow
{
	[Help("es", "Modifica una propiedad del componente @UnityManual{ParticleSystem} especificado.", null)]
	[AddComponentMenu("")]
	[Help("en", "Sets a property of the specified @UnityManual{ParticleSystem}.", null)]
	public class SetParticleSystemProperty : Action, IExecutableInEditor
	{
		public enum Property
		{
			EmissionRate,
			EmissionEnabled,
			GravityModifier,
			Loop,
			MaxParticles,
			PlaybackSpeed,
			PlayOnAwake,
			RandomSeed,
			SimulationSpace,
			StartColor,
			StartDelay,
			StartLifetime,
			StartRotation,
			StartSize,
			StartSpeed,
			Time,
			EmissionType
		}

		[SerializeField]
		private ParticleSystem _target;

		[SerializeField]
		private Variable _targetVar;

		[SerializeField]
		private Property _property;

		[SerializeField]
		private float _emissionRateValue;

		[SerializeField]
		private bool _enableEmissionValue;

		[SerializeField]
		private float _gravityModifierValue;

		[SerializeField]
		private bool _loopValue;

		[SerializeField]
		private int _maxParticlesValue;

		[SerializeField]
		private float _playbackSpeedValue;

		[SerializeField]
		private bool _playOnAwakeValue;

		[SerializeField]
		private int _randomSeedValue;

		[SerializeField]
		private ParticleSystemSimulationSpace _simulationSpaceValue;

		[SerializeField]
		private Color _startColorValue;

		[SerializeField]
		private float _startDelayValue;

		[SerializeField]
		private float _startLifetimeValue;

		[SerializeField]
		private float _startRotationValue;

		[SerializeField]
		private float _startSizeValue;

		[SerializeField]
		private float _startSpeedValue;

		[SerializeField]
		private float _timeValue;

		[SerializeField]
		private Variable _valueVar;

		protected override void OnReset()
		{
			_target = base.gameObject.GetComponent<ParticleSystem>();
		}

		public override void Setup()
		{
		}

		public override void Execute()
		{
			ParticleSystem particleSystem = _targetVar.GetValueAsComponent(_target, typeof(ParticleSystem)) as ParticleSystem;
			if (!(particleSystem == null))
			{
				ParticleSystem.MainModule main = particleSystem.main;
				switch (_property)
				{
				case Property.EmissionType:
					break;
				case Property.EmissionRate:
				{
					ParticleSystem.EmissionModule emission = particleSystem.emission;
					emission.rateOverTime = new ParticleSystem.MinMaxCurve(_valueVar.GetValue(_emissionRateValue));
					break;
				}
				case Property.EmissionEnabled:
				{
					ParticleSystem.EmissionModule emission = particleSystem.emission;
					emission.enabled = _valueVar.GetValue(_enableEmissionValue);
					break;
				}
				case Property.GravityModifier:
					main.gravityModifier = _valueVar.GetValue(_gravityModifierValue);
					break;
				case Property.Loop:
					main.loop = _valueVar.GetValue(_loopValue);
					break;
				case Property.MaxParticles:
					main.maxParticles = _valueVar.GetValue(_maxParticlesValue);
					break;
				case Property.PlaybackSpeed:
					main.simulationSpeed = _valueVar.GetValue(_playbackSpeedValue);
					break;
				case Property.PlayOnAwake:
					main.playOnAwake = _valueVar.GetValue(_playOnAwakeValue);
					break;
				case Property.RandomSeed:
					particleSystem.randomSeed = (uint)_valueVar.GetValue(_randomSeedValue);
					break;
				case Property.SimulationSpace:
					main.simulationSpace = (ParticleSystemSimulationSpace)(object)_valueVar.GetValue(_simulationSpaceValue);
					break;
				case Property.StartColor:
					main.startColor = _valueVar.GetValue(_startColorValue);
					break;
				case Property.StartDelay:
					main.startDelay = _valueVar.GetValue(_startDelayValue);
					break;
				case Property.StartLifetime:
					main.startLifetime = _valueVar.GetValue(_startLifetimeValue);
					break;
				case Property.StartRotation:
					main.startRotation = _valueVar.GetValue(_startRotationValue);
					break;
				case Property.StartSize:
					main.startSize = _valueVar.GetValue(_startSizeValue);
					break;
				case Property.StartSpeed:
					main.startSpeed = _valueVar.GetValue(_startSpeedValue);
					break;
				case Property.Time:
					particleSystem.time = _valueVar.GetValue(_timeValue);
					break;
				}
			}
		}
	}
}
