using System.Collections.Generic;
using UnityEngine;

namespace GameFlow
{
	[Help("en", "Executes the contained actions repeately while the conditions are met.", null)]
	[AddComponentMenu("")]
	[Help("es", "Ejecuta repetidamente las acciones contenidas mientras las condiciones se cumplan.", null)]
	public class While : Conditional
	{
		private bool _finished;

		public override void FirstStep()
		{
			_finished = !Evaluate(defaultResult: false);
			if (!_finished)
			{
				sequence.FirstEnabledAction();
				Step();
			}
		}

		public override void Step()
		{
			if (!_finished && sequence.Finished())
			{
				_finished = !Evaluate(defaultResult: false);
				if (!_finished)
				{
					sequence.FirstEnabledAction();
				}
			}
			if (_finished)
			{
				return;
			}
			Action currentAction = sequence.GetCurrentAction();
			if (currentAction != null && currentAction.enabledInEditor)
			{
				sequence.ExecuteCurrentAction();
			}
			else
			{
				sequence.NextEnabledAction();
				currentAction = sequence.GetCurrentAction();
				if (currentAction != null)
				{
					sequence.ExecuteCurrentAction();
				}
			}
			if (currentAction != null && ActionExecutor.DoesActionYield(currentAction))
			{
				ActionExecutor.Yield(currentAction);
				CheckFinished();
				sequence.NextEnabledAction();
				return;
			}
			while (!_finished && currentAction != null && ActionExecutor.IsActionFinished(currentAction))
			{
				sequence.NextEnabledAction();
				currentAction = sequence.GetCurrentAction();
				if (currentAction != null && currentAction.enabledInEditor)
				{
					sequence.ExecuteCurrentAction();
					if (ActionExecutor.DoesActionYield(currentAction))
					{
						ActionExecutor.Yield(currentAction);
						CheckFinished();
						sequence.NextEnabledAction();
						return;
					}
				}
			}
			CheckFinished();
		}

		private void CheckFinished()
		{
			if (!_finished && sequence.Finished())
			{
				_finished = !Evaluate(defaultResult: false);
				if (!_finished)
				{
					sequence.FirstEnabledAction();
				}
			}
		}

		public override bool Finished()
		{
			return _finished;
		}

		public override void Break()
		{
			_finished = true;
		}

		public override List<Block> GetBlocks()
		{
			List<Block> list = new List<Block>(GetActions().ToArray());
			list.AddRange(GetConditions().ToArray());
			return list;
		}
	}
}
