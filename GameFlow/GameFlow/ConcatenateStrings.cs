using UnityEngine;

namespace GameFlow
{
	[AddComponentMenu("")]
	[Help("es", "Concatena dos cadenas y guarda el resultado en la @GameFlow{Variable} de salida especificada.", null)]
	[Help("en", "Concatenates two strings and saves the result in the specified output @GameFlow{Variable}.", null)]
	public class ConcatenateStrings : Action, IExecutableInEditor
	{
		[SerializeField]
		private Variable _string1;

		[SerializeField]
		private Variable _string2;

		[SerializeField]
		private Variable _output;

		public override void Execute()
		{
			if (!(_string1 == null) && !(_string2 == null) && !(_output == null))
			{
				_output.SetValue(_string1.GetValue(string.Empty) + _string2.GetValue(string.Empty));
			}
		}
	}
}
