using UnityEngine;
using UnityEngine.SceneManagement;

namespace GameFlow
{
	[Help("en", "Loads the scene with the specified order number.", null)]
	[Help("es", "Carga la escena con el número de orden especificado.", null)]
	[AddComponentMenu("")]
	public class LoadScene : Action
	{
		[SerializeField]
		private int _scene;

		[SerializeField]
		private Variable _sceneVar;

		[SerializeField]
		private bool _additive;

		[SerializeField]
		private bool _asynchronous;

		public override void Execute()
		{
			int num = _sceneVar.GetValue(_scene);
			if (num < 0)
			{
				num = SceneManager.sceneCount + num;
			}
			PlayerPrefs.Save();
			LoadSceneMode mode = _additive ? LoadSceneMode.Additive : LoadSceneMode.Single;
			if (_asynchronous)
			{
				SceneManager.LoadSceneAsync(num, mode);
			}
			else
			{
				SceneManager.LoadScene(num, mode);
			}
		}
	}
}
