using UnityEngine;

namespace GameFlow
{
	[AddComponentMenu("")]
	[Help("en", "Gets an offset of the specified object.", null)]
	[Help("es", "Devuelve una posición relativa al objeto especificado.", null)]
	public class GetOffset : Action, IExecutableInEditor
	{
		[SerializeField]
		private Transform _origin;

		[SerializeField]
		private Variable _originVar;

		[SerializeField]
		private Vector3 _offset;

		[SerializeField]
		private Variable _offsetVar;

		[SerializeField]
		private Variable _output;

		public Transform origin => _originVar.GetValueAsComponent(_origin, typeof(Transform)) as Transform;

		public Vector3 offset => _offsetVar.GetValue(_offset);

		protected override void OnReset()
		{
			_origin = base.gameObject.GetComponent<Transform>();
		}

		public override void Execute()
		{
			if (!(_output == null))
			{
				Transform origin = this.origin;
				if (!(origin == null))
				{
					_output.SetValue(this.origin.position + offset);
				}
			}
		}
	}
}
