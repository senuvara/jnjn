using UnityEngine;

namespace GameFlow
{
	[AddComponentMenu("")]
	[Help("en", "Toggles the boolean-value of the specified @GameFlow{Parameter}.", null)]
	[Help("es", "Cambia el valor booleano del @GameFlow{Parameter} especificado por su contrario.", null)]
	public class ToggleParameterValue : ParameterAction
	{
		public override void Execute()
		{
			ToggleVariableValue.Execute(base.parameter);
		}
	}
}
