using System;
using System.Collections.Generic;
using UnityEngine;

namespace GameFlow
{
	[Help("es", "Controla los eventos de disparadores en este @UnityManual{GameObject} para 2D.", null)]
	[AddComponentMenu("")]
	[Help("en", "Controls the triggering events in this @UnityManual{GameObject} for 2D.", null)]
	public class TriggerController2D : EventController
	{
		[SerializeField]
		private bool _triggerEnterEnabled = true;

		[SerializeField]
		private bool _triggerExitEnabled = true;

		[SerializeField]
		private bool _triggerStayEnabled = true;

		private List<IEventListener> _enterListeners = new List<IEventListener>();

		private List<IEventListener> _exitListeners = new List<IEventListener>();

		private List<IEventListener> _stayListeners = new List<IEventListener>();

		public static EventController AddController(GameObject gameObject)
		{
			Collider2D collider2D = gameObject.GetCollider2D(isTrigger: true);
			return (!(collider2D != null)) ? null : EventController.AddController(gameObject, typeof(TriggerController2D));
		}

		private void Awake()
		{
			base.enabled = ((base.enabled && _triggerEnterEnabled) || _triggerExitEnabled || _triggerStayEnabled);
		}

		public override void AddListener(IEventListener listener, Type eventType)
		{
			if (eventType == typeof(TriggerEnterEvent))
			{
				_enterListeners.AddOnlyOnce(listener);
			}
			else if (eventType == typeof(TriggerExitEvent))
			{
				_exitListeners.AddOnlyOnce(listener);
			}
			else if (eventType == typeof(TriggerStayEvent))
			{
				_stayListeners.AddOnlyOnce(listener);
			}
		}

		public override void RemoveListener(IEventListener listener, Type eventType)
		{
			if (eventType == typeof(TriggerEnterEvent))
			{
				_enterListeners.Remove(listener);
			}
			else if (eventType == typeof(TriggerExitEvent))
			{
				_exitListeners.Remove(listener);
			}
			else if (eventType == typeof(TriggerStayEvent))
			{
				_stayListeners.Remove(listener);
			}
		}

		private void OnTriggerEnter2D(Collider2D other)
		{
			if (base.enabled && _triggerEnterEnabled)
			{
				TriggerEnterEvent triggerEnterEvent = new TriggerEnterEvent();
				InitEvent(triggerEnterEvent, other);
				triggerEnterEvent.DispatchTo(_enterListeners);
				triggerEnterEvent.DispatchToAll();
			}
		}

		private void OnTriggerExit2D(Collider2D other)
		{
			if (base.enabled && _triggerExitEnabled)
			{
				TriggerExitEvent triggerExitEvent = new TriggerExitEvent();
				InitEvent(triggerExitEvent, other);
				triggerExitEvent.DispatchTo(_exitListeners);
				triggerExitEvent.DispatchToAll();
			}
		}

		private void OnTriggerStay2D(Collider2D other)
		{
			if (base.enabled && _triggerStayEnabled)
			{
				TriggerStayEvent triggerStayEvent = new TriggerStayEvent();
				InitEvent(triggerStayEvent, other);
				triggerStayEvent.DispatchTo(_stayListeners);
				triggerStayEvent.DispatchToAll();
			}
		}

		private void InitEvent(TriggerEvent te, Collider2D other)
		{
			te.source = base.gameObject;
			te.other = other.gameObject;
		}
	}
}
