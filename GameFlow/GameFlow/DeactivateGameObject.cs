using UnityEngine;

namespace GameFlow
{
	[AddComponentMenu("")]
	[Help("es", "Activa el @UnityManual{GameObject} especificado.", null)]
	[Help("en", "Deactivates the specified @UnityManual{GameObject}.", null)]
	public class DeactivateGameObject : Action, IExecutableInEditor
	{
		[SerializeField]
		private GameObject _target;

		[SerializeField]
		private Variable _targetVar;

		public GameObject target => _targetVar.GetValue(_target) as GameObject;

		public override void Execute()
		{
			GameObject target = this.target;
			if (target != null)
			{
				target.SetActive(value: false);
			}
		}
	}
}
