using UnityEngine;

namespace GameFlow
{
	[Help("es", "Desactiva el componente de comportamiento (Behaviour) especificado.", null)]
	[AddComponentMenu("")]
	[Help("en", "Disables the specified Behaviour component.", null)]
	public class DisableBehaviour : Action, IExecutableInEditor
	{
		[SerializeField]
		private Behaviour _target;

		[SerializeField]
		private Variable _targetVar;

		public override void Execute()
		{
			Execute(_targetVar.GetValueAsComponent(_target, typeof(Behaviour)) as Behaviour);
		}

		public static void Execute(Behaviour behaviour)
		{
			if (behaviour != null)
			{
				behaviour.enabled = false;
			}
		}
	}
}
