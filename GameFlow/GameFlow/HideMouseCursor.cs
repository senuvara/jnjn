using UnityEngine;

namespace GameFlow
{
	[AddComponentMenu("")]
	[Help("es", "Oculta el puntero del ratón.", null)]
	[Help("en", "Hides the mouse cursor.", null)]
	public class HideMouseCursor : Action
	{
		public override void Execute()
		{
			Cursor.visible = false;
		}
	}
}
