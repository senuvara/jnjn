using UnityEngine;

namespace GameFlow
{
	[Help("en", "Creates and returns a web request object.", null)]
	[Help("es", "Crea y devuelve un objeto de petición web.", null)]
	[AddComponentMenu("")]
	public class CreateWebRequest : Action
	{
		public enum ContentType
		{
			Custom,
			Text,
			HTML,
			CSS,
			JSON,
			ZIP,
			PDF,
			GIF,
			JPEG,
			PNG
		}

		[SerializeField]
		private string _url;

		[SerializeField]
		private Variable _urlVar;

		[SerializeField]
		private WebRequest.Method _method;

		[SerializeField]
		private ContentType _contentType = ContentType.Text;

		[SerializeField]
		private string _customContentType = string.Empty;

		[SerializeField]
		private Variable _data;

		[SerializeField]
		private Variable _output;

		private static string[] _contentTypes = new string[16]
		{
			string.Empty,
			"text/plain",
			"text/html",
			"text/css",
			"application/json",
			"application/zip",
			"application/pdf",
			"image/gif",
			"image/jpg",
			"image/png",
			"video/mpeg",
			"video/mp4",
			"video/quicktime",
			"audio/mpeg",
			"audio/mp4",
			"audio/ogg"
		};

		public override void Execute()
		{
			if (_output == null)
			{
				return;
			}
			string value = _urlVar.GetValue(_url);
			switch (_method)
			{
			case WebRequest.Method.GET:
				_output.SetValue(WebRequest.CreateGetRequest(value));
				break;
			case WebRequest.Method.POST:
				if (!(_data == null))
				{
					byte[] data = new byte[0];
					DataType dataType = _data.dataType;
					if (dataType != DataType.Object)
					{
						data = _data.stringValue.ToByteArray();
					}
					string contentType = (_contentType != 0) ? _contentTypes[(int)_contentType] : _customContentType;
					_output.SetValue(WebRequest.CreatePostRequest(value, data, contentType));
				}
				break;
			}
		}
	}
}
