using UnityEngine;

namespace GameFlow
{
	[AddComponentMenu("")]
	[Help("es", "Desactiva todos los componentes de la lista (@GameFlow{List}) especificada.", null)]
	[Help("en", "Disables all the components of the specified @GameFlow{List}.", null)]
	public class DisableComponentsInList : ListAction
	{
		public override void Execute()
		{
			List list = GetList();
			if (list != null && list.dataType == DataType.Object)
			{
				for (int i = 0; i < list.Count; i++)
				{
					DisableComponent.Execute(list.GetObjectAt(i) as Component);
				}
			}
		}
	}
}
