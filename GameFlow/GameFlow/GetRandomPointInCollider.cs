using UnityEngine;

namespace GameFlow
{
	[Help("en", "Gets a random point inside the space of the specified Collider.", null)]
	[Help("es", "Devuelve un punto aleatorio dentro del espacio del Collider.", null)]
	[AddComponentMenu("")]
	public class GetRandomPointInCollider : Action, IExecutableInEditor
	{
		[SerializeField]
		private Collider _target;

		[SerializeField]
		private Variable _targetVar;

		[SerializeField]
		private Variable _output;

		public override void Setup()
		{
		}

		public override void Execute()
		{
			Collider collider = _targetVar.GetValueAsComponent(_target, typeof(Collider)) as Collider;
			BoxCollider boxCollider = collider as BoxCollider;
			if (boxCollider != null)
			{
				_output.SetValue(RandomUtils.GetRandomPointInCollider(boxCollider));
				return;
			}
			SphereCollider sphereCollider = collider as SphereCollider;
			if (sphereCollider != null)
			{
				_output.SetValue(RandomUtils.GetRandomPointInCollider(sphereCollider));
			}
		}
	}
}
