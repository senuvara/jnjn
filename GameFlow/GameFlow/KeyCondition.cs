using UnityEngine;

namespace GameFlow
{
	[Help("en", "Evaluates whether the specified @GameFlow{Key} is or has been pressed.", null)]
	[AddComponentMenu("")]
	[Help("es", "Evalúa si la tecla (@GameFlow{Key}) especificada está siendo o ha sido pulsada.", null)]
	public class KeyCondition : Condition
	{
		public enum Comparison
		{
			IsDown,
			IsJustDown,
			IsUp,
			IsJustUp
		}

		[SerializeField]
		private Key _key;

		[SerializeField]
		private Variable _keyVar;

		[SerializeField]
		private Comparison _comparison;

		public override bool Evaluate()
		{
			Key key = _keyVar.GetValue(_key) as Key;
			if (key == null)
			{
				return false;
			}
			switch (_comparison)
			{
			case Comparison.IsDown:
				return key.isDown && !key.justDown;
			case Comparison.IsJustDown:
				return key.isDown && key.justDown;
			case Comparison.IsUp:
				return !key.isDown && !key.justUp;
			case Comparison.IsJustUp:
				return !key.isDown && key.justUp;
			default:
				return false;
			}
		}
	}
}
