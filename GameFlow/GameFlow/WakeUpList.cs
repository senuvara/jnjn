using UnityEngine;

namespace GameFlow
{
	[AddComponentMenu("")]
	[Help("en", "Wakes up all objects (2D or 3D) in the specified @GameFlow{List}.", null)]
	[Help("es", "Despierta los objetos (sean 2D o 3D) de la @GameFlow{List} especificada.", null)]
	public class WakeUpList : ListAction
	{
		public override void Execute()
		{
			List list = GetList();
			if (!(list != null) || list.dataType != DataType.Object)
			{
				return;
			}
			for (int i = 0; i < list.Count; i++)
			{
				Rigidbody rigidbody = list.GetObjectAt(i) as Rigidbody;
				if (rigidbody != null)
				{
					WakeUp.Execute(rigidbody);
					continue;
				}
				Rigidbody2D rigidbody2D = list.GetObjectAt(i) as Rigidbody2D;
				if (rigidbody2D != null)
				{
					WakeUp2D.Execute(rigidbody2D);
				}
			}
		}
	}
}
