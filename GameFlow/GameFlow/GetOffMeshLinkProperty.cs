using UnityEngine;
using UnityEngine.AI;

namespace GameFlow
{
	[AddComponentMenu("")]
	[Help("en", "Gets a property of the specified @UnityManual{OffMeshLink} component.", null)]
	[Help("es", "Lee una propiedad del componente @UnityManual{OffMeshLink} especificado.", null)]
	public class GetOffMeshLinkProperty : Action, IExecutableInEditor
	{
		public enum Property
		{
			Activated,
			Area,
			AutoUpdatePositions,
			BiDirectional,
			CostOverride,
			EndTransform,
			Occupied,
			StartTransform
		}

		[SerializeField]
		private OffMeshLink _source;

		[SerializeField]
		private Variable _sourceVar;

		[SerializeField]
		private Property _property;

		[SerializeField]
		private Variable _output;

		protected override void OnReset()
		{
			_source = base.gameObject.GetComponent<OffMeshLink>();
		}

		public override void Setup()
		{
		}

		public override void Execute()
		{
			OffMeshLink offMeshLink = _sourceVar.GetValueAsComponent(_source, typeof(OffMeshLink)) as OffMeshLink;
			if (!(offMeshLink == null))
			{
				switch (_property)
				{
				case Property.Activated:
					_output.SetValue(offMeshLink.activated);
					break;
				case Property.Area:
					_output.SetValue(offMeshLink.area);
					break;
				case Property.AutoUpdatePositions:
					_output.SetValue(offMeshLink.autoUpdatePositions);
					break;
				case Property.BiDirectional:
					_output.SetValue(offMeshLink.biDirectional);
					break;
				case Property.CostOverride:
					_output.SetValue(offMeshLink.costOverride);
					break;
				case Property.EndTransform:
					_output.SetValue(offMeshLink.endTransform);
					break;
				case Property.Occupied:
					_output.SetValue(offMeshLink.occupied);
					break;
				case Property.StartTransform:
					_output.SetValue(offMeshLink.startTransform);
					break;
				}
			}
		}
	}
}
