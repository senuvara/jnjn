using UnityEngine;

namespace GameFlow
{
	[Help("en", "Modifies the values of a property of the specified @GameFlow{Ray}.", null)]
	[Help("es", "Modifica el valor de una propiedad del rayo (@GameFlow{Ray}) especificada.", null)]
	[AddComponentMenu("")]
	public class SetRayProperty : Action, IExecutableInEditor
	{
		public enum Property
		{
			Color,
			Direction,
			Enabled,
			Length,
			Origin
		}

		[SerializeField]
		private Ray _ray;

		[SerializeField]
		private Variable _rayVar;

		[SerializeField]
		private Property _property = Property.Direction;

		[SerializeField]
		private Color _colorValue = Color.red;

		[SerializeField]
		private Transform _transformValue;

		[SerializeField]
		private Vector3 _vector2Value;

		[SerializeField]
		private Vector3 _vector3Value;

		[SerializeField]
		private bool _boolValue;

		[SerializeField]
		private float _floatValue = 1f;

		[SerializeField]
		private Variable _valueVar;

		[SerializeField]
		private Ray.DirectionType _directionType = Ray.DirectionType.LocalPoint;

		[SerializeField]
		private Ray.OriginType _originType;

		public override void Setup()
		{
		}

		public override void Execute()
		{
			Ray ray = _rayVar.GetValue(_ray) as Ray;
			if (ray == null)
			{
				return;
			}
			switch (_property)
			{
			case Property.Color:
				ray.color = _valueVar.GetValue(_colorValue);
				break;
			case Property.Enabled:
				ray.enabledInEditor = _valueVar.GetValue(_boolValue);
				break;
			case Property.Direction:
				switch (_directionType)
				{
				case Ray.DirectionType.Transform:
				{
					Transform directionTransform = _valueVar.GetValueAsComponent(_transformValue, typeof(Transform)) as Transform;
					ray.SetDirectionTransform(directionTransform);
					break;
				}
				case Ray.DirectionType.WorldPoint:
					ray.SetDirectionWorldPoint(_valueVar.GetValue(_vector3Value));
					break;
				case Ray.DirectionType.LocalPoint:
					ray.SetDirectionLocalPoint(_valueVar.GetValue(_vector3Value));
					break;
				case Ray.DirectionType.WorldDirection:
					ray.SetDirectionWorldDirection(_valueVar.GetValue(_vector3Value));
					break;
				case Ray.DirectionType.LocalDirection:
					ray.SetDirectionLocalDirection(_valueVar.GetValue(_vector3Value));
					break;
				case Ray.DirectionType.ScreenPoint:
					ray.SetDirectionScreenPoint(_valueVar.GetValue(_vector2Value));
					break;
				case Ray.DirectionType.ViewportPoint:
					ray.SetDirectionViewportPoint(_valueVar.GetValue(_vector2Value));
					break;
				}
				break;
			case Property.Length:
				ray.length = _valueVar.GetValue(_floatValue);
				break;
			case Property.Origin:
				switch (_originType)
				{
				case Ray.OriginType.Transform:
				{
					Transform origin = _valueVar.GetValueAsComponent(_transformValue, typeof(Transform)) as Transform;
					ray.SetOrigin(origin);
					break;
				}
				case Ray.OriginType.WorldPoint:
					ray.SetOrigin(_valueVar.GetValue(_vector3Value));
					break;
				}
				break;
			}
		}
	}
}
