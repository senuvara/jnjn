using UnityEngine;

namespace GameFlow
{
	[AddComponentMenu("")]
	[Help("es", "Devuelve el número de elementos en la @GameFlow{List} especificada.", null)]
	[Help("en", "Gets the number of items in the specified @GameFlow{List}.", null)]
	public class GetListCount : ListAction
	{
		[SerializeField]
		private Variable _output;

		public override void Execute()
		{
			ExecuteWith(GetList(), _output);
		}

		public static void ExecuteWith(List list, Variable output)
		{
			if (!(list == null) && !(output == null))
			{
				output.SetValue(list.Count);
			}
		}
	}
}
