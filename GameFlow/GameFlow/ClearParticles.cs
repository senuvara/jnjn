using UnityEngine;

namespace GameFlow
{
	[Help("es", "Elimina todas las partículas del @UnityManual{ParticleSystem} o ParticleEmitter especificado.", null)]
	[AddComponentMenu("")]
	[Help("en", "Removes all particles from the specified @UnityManual{ParticleSystem} or ParticleEmitter.", null)]
	public class ClearParticles : Action
	{
		public enum TargetType
		{
			System,
			Emitter
		}

		[SerializeField]
		private TargetType _targetType;

		[SerializeField]
		private ParticleSystem _targetSystem;

		[SerializeField]
		private Variable _targetVar;

		protected override void OnReset()
		{
		}

		public override void Execute()
		{
			switch (_targetType)
			{
			case TargetType.System:
			{
				ParticleSystem particleSystem = _targetVar.GetValue(_targetSystem) as ParticleSystem;
				if (particleSystem != null)
				{
					particleSystem.Clear();
				}
				break;
			}
			}
		}
	}
}
