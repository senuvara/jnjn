using UnityEngine;

namespace GameFlow
{
	[AddComponentMenu("")]
	[Help("en", "Gets a random item from the specified @GameFlow{List}.", null)]
	[Help("es", "Devuelve un elemento aleatorio de la @GameFlow{List} especificada.", null)]
	public class GetRandomItemFromList : ListAction
	{
		[SerializeField]
		private Variable _output;

		public override void Execute()
		{
			List list = GetList();
			if (list != null && list.Count > 0)
			{
				int index = Random.Range(0, list.Count);
				GetItemFromList.ExecuteWith(list, index, _output);
			}
		}
	}
}
