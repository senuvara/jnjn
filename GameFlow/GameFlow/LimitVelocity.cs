using UnityEngine;

namespace GameFlow
{
	[Help("en", "Limits the velocity of the specified 3D object.", null)]
	[AddComponentMenu("")]
	[Help("es", "Limita la velocidad del objeto 3D especificado.", null)]
	public class LimitVelocity : Action
	{
		[SerializeField]
		private Rigidbody _target;

		[SerializeField]
		private Variable _targetVar;

		[SerializeField]
		private float _min;

		[SerializeField]
		private Variable _minVar;

		[SerializeField]
		private float _max = 1f;

		[SerializeField]
		private Variable _maxVar;

		protected override void OnReset()
		{
			_target = GetComponent<Rigidbody>();
		}

		public override void Execute()
		{
			Rigidbody rb = _targetVar.GetValueAsComponent(_target, typeof(Rigidbody)) as Rigidbody;
			Execute(rb, _minVar.GetValue(_min), _maxVar.GetValue(_max));
		}

		public static void Execute(Rigidbody rb, float min, float max)
		{
			if (!(rb == null) && !Mathf.Approximately(rb.velocity.magnitude, 0f))
			{
				float d = Mathf.Clamp(rb.velocity.magnitude, min, max);
				rb.velocity = rb.velocity.normalized * d;
			}
		}
	}
}
