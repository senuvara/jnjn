using UnityEngine;

namespace GameFlow
{
	[Help("es", "Ejecuta el comando (@GameFlow{Command}) especificado.", null)]
	[Help("en", "Executes the specified @GameFlow{Command}.", null)]
	[AddComponentMenu("")]
	public class ExecuteCommand : Action
	{
		[SerializeField]
		private Command _command;

		[SerializeField]
		private Variable _commandVar;

		[SerializeField]
		private bool _restart = true;

		[SerializeField]
		private Variable _restartVar;

		[SerializeField]
		private bool _wait = true;

		[SerializeField]
		private Variable _waitVar;

		private Program _runningProgram;

		public Command command => _commandVar.GetValue(_command) as Command;

		public bool restart => _restartVar.GetValue(_restart);

		public bool wait => _waitVar.GetValue(_wait);

		public override void FirstStep()
		{
			Command command = this.command;
			if (!(command == null))
			{
				Program program = command.program;
				_runningProgram = null;
				if (!(program == null) && (!program.running || restart || wait))
				{
					_runningProgram = program;
					program.Execute();
				}
			}
		}

		public override bool Finished()
		{
			if (_runningProgram == null || !wait)
			{
				return true;
			}
			if (_runningProgram.finished)
			{
				_runningProgram = null;
				return true;
			}
			return false;
		}

		public static void Execute(Command command, bool restart)
		{
			if (!(command == null))
			{
				Program program = command.program;
				if (!(program == null) && (!program.running || restart))
				{
					program.Execute();
				}
			}
		}
	}
}
