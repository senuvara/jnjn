using System.Collections.Generic;
using UnityEngine;

namespace GameFlow
{
	[Help("es", "Programa que ejecutará, si está activado, la secuencia de acciones asociada a su estado actual.", null)]
	[Help("en", "Program that will execute, while enabled, the action sequence associated to its current state.", null)]
	[AddComponentMenu("GameFlow/Tools/State Machine")]
	[DisallowMultipleComponent]
	public class StateMachine : Container, IBlockContainer, IStateContainer
	{
		private static LocalizedString invalidState = LocalizedString.Create().Add("en", "The specified State '%1' does not belong to State Machine '%2'.").Add("es", "El estado especificado '%1' no pertenece a la máquina de estados '%2'.");

		private static LocalizedString noState = LocalizedString.Create().Add("en", "No State specified.").Add("es", "No se ha especificado un estado.");

		[SerializeField]
		private bool _settingsFoldout = true;

		[SerializeField]
		private bool _statesFoldout = true;

		[SerializeField]
		private string _id = string.Empty;

		[SerializeField]
		private List<State> _states = new List<State>();

		[SerializeField]
		private State _startState;

		[SerializeField]
		private State _currentState;

		private State _lastState;

		public string id => _id;

		public List<State> states => _states;

		public State startState
		{
			get
			{
				if (_startState == null && _states.Count > 0)
				{
					_startState = _states[0];
				}
				return _startState;
			}
			set
			{
				if (value == null)
				{
					Log.Error(noState.ToString(), base.gameObject);
				}
				else if (!_states.Contains(value))
				{
					Log.Error(invalidState.ToString().Replace("%1", value.GetId()).Replace("%2", this.GetId()), base.gameObject);
				}
				else
				{
					_startState = value;
				}
			}
		}

		public State currentState
		{
			get
			{
				return _currentState;
			}
			set
			{
				if (value == null)
				{
					_currentState = value;
				}
				else if (!_states.Contains(value))
				{
					Log.Error(invalidState.ToString().Replace("%1", value.GetId()).Replace("%2", this.GetId()), base.gameObject);
				}
				else
				{
					_currentState = value;
				}
			}
		}

		protected override void OnReset()
		{
			if (_id == string.Empty)
			{
				_id = GetDefaultId();
			}
		}

		protected virtual string GetDefaultId()
		{
			return "StateMachine" + Random.Range(1000, 9999);
		}

		private void Awake()
		{
			base.useGUILayout = false;
		}

		private void Start()
		{
			Restart();
		}

		public void Restart()
		{
			_currentState = _startState;
		}

		private void Update()
		{
			if (_currentState != null)
			{
				if (_lastState != _currentState)
				{
					_currentState.Restart();
				}
				else
				{
					_currentState.Execute();
				}
			}
			_lastState = _currentState;
		}

		public List<State> GetStates()
		{
			return _states;
		}

		public virtual void StateAdded(State state)
		{
			if (_states.Count == 1)
			{
				_startState = state;
			}
		}

		public override List<Block> GetBlocks()
		{
			return new List<Block>(_states.ToArray());
		}

		public override bool Contains(Block block)
		{
			State state = block as State;
			if (state != null)
			{
				return _states.Contains(state);
			}
			return false;
		}
	}
}
