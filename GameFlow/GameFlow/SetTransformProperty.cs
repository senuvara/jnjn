using UnityEngine;

namespace GameFlow
{
	[Help("en", "Sets a property of the specified @UnityManual{Transform} component.", null)]
	[Help("es", "Modifica una propiedad del componente @UnityManual{Transform} especificado.", null)]
	[AddComponentMenu("")]
	public class SetTransformProperty : Action, IExecutableInEditor
	{
		public enum Property
		{
			LocalPositionX,
			LocalPositionY,
			LocalPositionZ,
			Sep1,
			WorldPositionX,
			WorldPositionY,
			WorldPositionZ,
			Sep2,
			LocalRotationX,
			LocalRotationY,
			LocalRotationZ,
			Sep3,
			WorldRotationX,
			WorldRotationY,
			WorldRotationZ,
			Sep4,
			LocalScaleX,
			LocalScaleY,
			LocalScaleZ,
			LocalPosition,
			WorldPosition,
			LocalRotation,
			WorldRotation,
			LocalScale
		}

		[SerializeField]
		private Transform _target;

		[SerializeField]
		private Variable _targetVar;

		[SerializeField]
		private Property _property;

		[SerializeField]
		private float _value;

		[SerializeField]
		private Vector3 _vector3Value;

		[SerializeField]
		private Variable _valueVar;

		[SerializeField]
		private bool _relative;

		[SerializeField]
		private Variable _relativeVar;

		public bool relative => _relativeVar.GetValue(_relative);

		public override void Execute()
		{
			Transform transform = _target;
			if (transform == null)
			{
				transform = (_targetVar.GetValueAsComponent(_target, typeof(Transform)) as Transform);
				if (transform == null)
				{
					return;
				}
			}
			Vector3 vector;
			switch (_property)
			{
			case Property.Sep1:
			case Property.Sep2:
			case Property.Sep3:
			case Property.Sep4:
				break;
			case Property.LocalPosition:
				vector = _valueVar.GetValue(_vector3Value);
				transform.localPosition = ((!relative) ? vector : (transform.localPosition + vector));
				break;
			case Property.LocalPositionX:
			{
				float value = _valueVar.GetValue(_value);
				vector = transform.localPosition;
				vector.x = ((!relative) ? value : (vector.x + value));
				transform.localPosition = vector;
				break;
			}
			case Property.LocalPositionY:
			{
				float value = _valueVar.GetValue(_value);
				vector = transform.localPosition;
				vector.y = ((!relative) ? value : (vector.y + value));
				transform.localPosition = vector;
				break;
			}
			case Property.LocalPositionZ:
			{
				float value = _valueVar.GetValue(_value);
				vector = transform.localPosition;
				vector.z = ((!relative) ? value : (vector.z + value));
				transform.localPosition = vector;
				break;
			}
			case Property.WorldPosition:
				vector = _valueVar.GetValue(_vector3Value);
				transform.position = ((!relative) ? vector : (transform.position + vector));
				break;
			case Property.WorldPositionX:
			{
				float value = _valueVar.GetValue(_value);
				vector = transform.position;
				vector.x = ((!relative) ? value : (vector.x + value));
				transform.position = vector;
				break;
			}
			case Property.WorldPositionY:
			{
				float value = _valueVar.GetValue(_value);
				vector = transform.position;
				vector.y = ((!relative) ? value : (vector.y + value));
				transform.position = vector;
				break;
			}
			case Property.WorldPositionZ:
			{
				float value = _valueVar.GetValue(_value);
				vector = transform.position;
				vector.z = ((!relative) ? value : (vector.z + value));
				transform.position = vector;
				break;
			}
			case Property.LocalRotation:
				vector = _valueVar.GetValue(_vector3Value);
				transform.localEulerAngles = ((!relative) ? vector : (transform.localEulerAngles + vector));
				break;
			case Property.LocalRotationX:
			{
				float value = _valueVar.GetValue(_value);
				vector = transform.localEulerAngles;
				vector.x = ((!relative) ? value : (vector.x + value));
				transform.localEulerAngles = vector;
				break;
			}
			case Property.LocalRotationY:
			{
				float value = _valueVar.GetValue(_value);
				vector = transform.localEulerAngles;
				vector.y = ((!relative) ? value : (vector.y + value));
				transform.localEulerAngles = vector;
				break;
			}
			case Property.LocalRotationZ:
			{
				float value = _valueVar.GetValue(_value);
				vector = transform.localEulerAngles;
				vector.z = ((!relative) ? value : (vector.z + value));
				transform.localEulerAngles = vector;
				break;
			}
			case Property.WorldRotation:
				vector = _valueVar.GetValue(_vector3Value);
				transform.eulerAngles = ((!relative) ? vector : (transform.eulerAngles + vector));
				break;
			case Property.WorldRotationX:
			{
				float value = _valueVar.GetValue(_value);
				vector = transform.eulerAngles;
				vector.x = ((!relative) ? value : (vector.x + value));
				transform.eulerAngles = vector;
				break;
			}
			case Property.WorldRotationY:
			{
				float value = _valueVar.GetValue(_value);
				vector = transform.eulerAngles;
				vector.y = ((!relative) ? value : (vector.y + value));
				transform.eulerAngles = vector;
				break;
			}
			case Property.WorldRotationZ:
			{
				float value = _valueVar.GetValue(_value);
				vector = transform.eulerAngles;
				vector.z = ((!relative) ? value : (vector.z + value));
				transform.eulerAngles = vector;
				break;
			}
			case Property.LocalScale:
				vector = _valueVar.GetValue(_vector3Value);
				transform.localScale = ((!relative) ? vector : (transform.localScale + vector));
				break;
			case Property.LocalScaleX:
			{
				float value = _valueVar.GetValue(_value);
				vector = transform.localScale;
				vector.x = ((!relative) ? value : (vector.x + value));
				transform.localScale = vector;
				break;
			}
			case Property.LocalScaleY:
			{
				float value = _valueVar.GetValue(_value);
				vector = transform.localScale;
				vector.y = ((!relative) ? value : (vector.y + value));
				transform.localScale = vector;
				break;
			}
			case Property.LocalScaleZ:
			{
				float value = _valueVar.GetValue(_value);
				vector = transform.localScale;
				vector.z = ((!relative) ? value : (vector.z + value));
				transform.localScale = vector;
				break;
			}
			}
		}
	}
}
