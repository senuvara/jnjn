using UnityEngine;
using UnityEngine.UI;

namespace GameFlow
{
	[Help("en", "Modifies the value of a property of the specified Text component.", null)]
	[Help("es", "Modifica el valor de una propiedad del componente Text especificado.", null)]
	[AddComponentMenu("")]
	public class SetTextProperty : Action, IExecutableInEditor
	{
		public enum Property
		{
			Alignment,
			Color,
			Font,
			FontSize,
			FontStyle,
			HorizontalOverflow,
			LineSpacing,
			Maskable,
			Material,
			ResizeTextForBestFit,
			ResizeTextMaxSize,
			ResizeTextMinSize,
			SupportRichText,
			Text,
			VerticalOverflow
		}

		[SerializeField]
		private Text _target;

		[SerializeField]
		private Variable _targetVar;

		[SerializeField]
		private Property _property = Property.Text;

		[SerializeField]
		private TextAnchor _alignmentValue;

		[SerializeField]
		private Color _colorValue = Color.white;

		[SerializeField]
		private Font _fontValue;

		[SerializeField]
		private int _fontSizeValue;

		[SerializeField]
		private FontStyle _fontStyleValue;

		[SerializeField]
		private HorizontalWrapMode _horizontalOverflowValue;

		[SerializeField]
		private float _lineSpacingValue;

		[SerializeField]
		private bool _maskableValue;

		[SerializeField]
		private Material _materialValue;

		[SerializeField]
		private bool _resizeTextForBestFitValue;

		[SerializeField]
		private int _resizeTextMaxSizeValue;

		[SerializeField]
		private int _resizeTextMinSizeValue;

		[SerializeField]
		private bool _supportRichTextValue;

		[SerializeField]
		private string _textValue;

		[SerializeField]
		private VerticalWrapMode _verticalOverflowValue;

		[SerializeField]
		private Variable _valueVar;

		[SerializeField]
		private string _format;

		[SerializeField]
		private Variable _formatVar;

		public Text target => _targetVar.GetValueAsComponent(_target, typeof(Text)) as Text;

		public string format => _formatVar.GetValue(_format);

		protected override void OnReset()
		{
			Text component = GetComponent<Text>();
			if (component != null)
			{
				_target = component;
				_alignmentValue = component.alignment;
				_colorValue = component.color;
				_fontValue = component.font;
				_fontSizeValue = component.fontSize;
				_fontStyleValue = component.fontStyle;
				_horizontalOverflowValue = component.horizontalOverflow;
				_lineSpacingValue = component.lineSpacing;
				_maskableValue = component.maskable;
				_materialValue = null;
				_resizeTextForBestFitValue = component.resizeTextForBestFit;
				_resizeTextMaxSizeValue = component.resizeTextMaxSize;
				_resizeTextMinSizeValue = component.resizeTextMinSize;
				_supportRichTextValue = component.supportRichText;
				_textValue = component.text;
				_verticalOverflowValue = component.verticalOverflow;
			}
		}

		public override void Execute()
		{
			Text target = this.target;
			if (target == null)
			{
				return;
			}
			switch (_property)
			{
			case Property.Alignment:
				target.alignment = (TextAnchor)(object)_valueVar.GetValue(_alignmentValue);
				break;
			case Property.Color:
				target.color = _valueVar.GetValue(_colorValue);
				break;
			case Property.Font:
				target.font = (_valueVar.GetValue(_fontValue) as Font);
				break;
			case Property.FontSize:
				target.fontSize = _valueVar.GetValue(_fontSizeValue);
				break;
			case Property.FontStyle:
				target.fontStyle = (FontStyle)(object)_valueVar.GetValue(_fontStyleValue);
				break;
			case Property.HorizontalOverflow:
				target.horizontalOverflow = (HorizontalWrapMode)(object)_valueVar.GetValue(_horizontalOverflowValue);
				break;
			case Property.LineSpacing:
				target.lineSpacing = _valueVar.GetValue(_lineSpacingValue);
				break;
			case Property.Maskable:
				target.maskable = _valueVar.GetValue(_maskableValue);
				break;
			case Property.Material:
				target.material = (_valueVar.GetValue(_materialValue) as Material);
				break;
			case Property.ResizeTextForBestFit:
				target.resizeTextForBestFit = _valueVar.GetValue(_resizeTextForBestFitValue);
				break;
			case Property.ResizeTextMaxSize:
				target.resizeTextMaxSize = _valueVar.GetValue(_resizeTextMaxSizeValue);
				break;
			case Property.ResizeTextMinSize:
				target.resizeTextMinSize = _valueVar.GetValue(_resizeTextMinSizeValue);
				break;
			case Property.SupportRichText:
				target.supportRichText = _valueVar.GetValue(_supportRichTextValue);
				break;
			case Property.Text:
				if (_valueVar != null && _valueVar.IsNumeric())
				{
					target.text = _valueVar.GetValue(_textValue, format, base.gameObject).Expanded();
				}
				else
				{
					target.text = _valueVar.GetValue(_textValue).Expanded();
				}
				break;
			case Property.VerticalOverflow:
				target.verticalOverflow = (VerticalWrapMode)(object)_valueVar.GetValue(_verticalOverflowValue);
				break;
			}
		}
	}
}
