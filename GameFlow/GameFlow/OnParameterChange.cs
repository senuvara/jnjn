using UnityEngine;

namespace GameFlow
{
	[Help("en", "Program that will be executed when a @GameFlow{Parameter} of the GameObject changes.", null)]
	[AddComponentMenu("GameFlow/Programs/On Parameter Change")]
	[Help("es", "Programa que se ejecutará cuando un parámetro (@GameFlow{Parameter}) del GameObject sea modificado.", null)]
	[DisallowMultipleComponent]
	public class OnParameterChange : EventProgram
	{
		public static IEventListenerProxy proxy;

		private Parameter _changedParam;

		public Parameter changedParam
		{
			get
			{
				if (_changedParam == null)
				{
					_changedParam = GetParameter("Changed Parameter");
				}
				return _changedParam;
			}
		}

		protected override void CacheParameters()
		{
			CacheParameter(changedParam);
		}

		protected override void SetParameters(EventObject e)
		{
			ParameterChangeEvent parameterChangeEvent = e as ParameterChangeEvent;
			changedParam.SetValue(parameterChangeEvent.parameter);
		}

		public override void EventReceived(EventObject e)
		{
			if (AcceptEvent(e))
			{
				SetParameters(e);
				if (proxy != null)
				{
					proxy.EventReceivedByListener(e, this);
				}
				else
				{
					Restart();
				}
			}
		}
	}
}
