using UnityEngine;

namespace GameFlow
{
	[Help("en", "Program that will be executed one time as soon as a Collider 2D component in the listening range starts colliding with another component.", null)]
	[AddComponentMenu("GameFlow/Programs/On Collision Enter 2D")]
	[Help("es", "Programa que se ejecutará una vez tan pronto como un Collider 2D en el rango de escucha comience a colisionar con otro componente.", null)]
	[DisallowMultipleComponent]
	public class OnCollisionEnter2D : OnCollisionProgram2D
	{
		private EventController[] _controllers;

		protected override void RegisterAsListener()
		{
			_controllers = RegisterAsListener(typeof(CollisionEnterEvent), typeof(CollisionController2D), CollisionController2D.AddController);
			if (base.listeningRange == EventListeningRange.SceneHierarchy)
			{
				CollisionEnterEvent.AddListener(this);
			}
		}

		protected override void UnRegisterAsListener()
		{
			CollisionEnterEvent.RemoveListener(this);
			UnRegisterAsListener(typeof(CollisionEnterEvent), typeof(CollisionController2D));
		}

		private void OnTransformChildrenChanged()
		{
			if (base.listeningRange == EventListeningRange.GameObjectHierarchy)
			{
				UnsubscribeFrom(_controllers, typeof(CollisionEnterEvent));
				_controllers = EventController.GetControllersInHierarchyOf(typeof(CollisionController2D), base.listeningTarget, CollisionController2D.AddController);
				SubscribeTo(_controllers, typeof(CollisionEnterEvent));
			}
		}
	}
}
