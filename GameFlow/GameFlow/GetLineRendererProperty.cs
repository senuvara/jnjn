using UnityEngine;

namespace GameFlow
{
	[Help("es", "Lee una propiedad del componente @UnityManual{LineRenderer} especificado.", null)]
	[AddComponentMenu("")]
	[Help("en", "Gets a property of the specified @UnityManual{LineRenderer} component.", null)]
	public class GetLineRendererProperty : Action, IExecutableInEditor
	{
		public enum Property
		{
			Bounds,
			Enabled,
			IsPartOfStaticBatch,
			IsVisible,
			LightmapIndex,
			LightmapScaleOffset,
			LocalToWorldMatrix,
			Material,
			Materials,
			ProbeAnchor,
			RealtimeLightmapIndex,
			RealtimeLightmapScaleOffset,
			ReceiveShadows,
			ReflectionProbeUsage,
			ShadowCastingMode,
			SharedMaterial,
			SharedMaterials,
			SortingLayerID,
			SortingLayerName,
			SortingOrder,
			LightProbeUsage,
			UseWorldSpace,
			WorldToLocalMatrix
		}

		[SerializeField]
		private LineRenderer _source;

		[SerializeField]
		private Variable _sourceVar;

		[SerializeField]
		private Property _property;

		[SerializeField]
		private Variable _output;

		protected override void OnReset()
		{
			_source = base.gameObject.GetComponent<LineRenderer>();
		}

		public override void Setup()
		{
		}

		public override void Execute()
		{
			LineRenderer lineRenderer = _sourceVar.GetValueAsComponent(_source, typeof(LineRenderer)) as LineRenderer;
			if (!(lineRenderer == null))
			{
				switch (_property)
				{
				case Property.LocalToWorldMatrix:
				case Property.Materials:
				case Property.SharedMaterials:
					break;
				case Property.Bounds:
					_output.SetValue(lineRenderer.bounds);
					break;
				case Property.Enabled:
					_output.SetValue(lineRenderer.enabled);
					break;
				case Property.IsPartOfStaticBatch:
					_output.SetValue(lineRenderer.isPartOfStaticBatch);
					break;
				case Property.IsVisible:
					_output.SetValue(lineRenderer.isVisible);
					break;
				case Property.LightmapIndex:
					_output.SetValue(lineRenderer.lightmapIndex);
					break;
				case Property.LightmapScaleOffset:
					_output.SetValue(lineRenderer.lightmapScaleOffset);
					break;
				case Property.Material:
					_output.SetValue(lineRenderer.material);
					break;
				case Property.ProbeAnchor:
					_output.SetValue(lineRenderer.probeAnchor);
					break;
				case Property.RealtimeLightmapIndex:
					_output.SetValue(lineRenderer.realtimeLightmapIndex);
					break;
				case Property.RealtimeLightmapScaleOffset:
					_output.SetValue(lineRenderer.realtimeLightmapScaleOffset);
					break;
				case Property.ReceiveShadows:
					_output.SetValue(lineRenderer.receiveShadows);
					break;
				case Property.ReflectionProbeUsage:
					_output.SetValue(lineRenderer.reflectionProbeUsage);
					break;
				case Property.ShadowCastingMode:
					_output.SetValue(lineRenderer.shadowCastingMode);
					break;
				case Property.SharedMaterial:
					_output.SetValue(lineRenderer.sharedMaterial);
					break;
				case Property.SortingLayerID:
					_output.SetValue(lineRenderer.sortingLayerID);
					break;
				case Property.SortingLayerName:
					_output.SetValue(lineRenderer.sortingLayerName);
					break;
				case Property.SortingOrder:
					_output.SetValue(lineRenderer.sortingOrder);
					break;
				case Property.LightProbeUsage:
					_output.SetValue(lineRenderer.lightProbeUsage);
					break;
				case Property.UseWorldSpace:
					_output.SetValue(lineRenderer.useWorldSpace);
					break;
				}
			}
		}
	}
}
