using UnityEngine;

namespace GameFlow
{
	[AddComponentMenu("")]
	[Help("en", "Gets a property of the specified Canvas component.", null)]
	[Help("es", "Lee una propiedad del componente Canvas especificado.", null)]
	public class GetCanvasProperty : Action, IExecutableInEditor
	{
		public enum Property
		{
			CachedSortingLayerValue,
			IsRootCanvas,
			OverridePixelPerfect,
			OverrideSorting,
			PixelPerfect,
			PixelRect,
			PlaneDistance,
			ReferencePixelsPerUnit,
			RenderMode,
			RenderOrder,
			ScaleFactor,
			SortingLayerID,
			SortingLayerName,
			SortingOrder,
			WorldCamera
		}

		[SerializeField]
		private Canvas _source;

		[SerializeField]
		private Variable _sourceVar;

		[SerializeField]
		private Property _property;

		[SerializeField]
		private Variable _output;

		protected override void OnReset()
		{
			_source = base.gameObject.GetComponent<Canvas>();
		}

		public override void Setup()
		{
		}

		public override void Execute()
		{
			Canvas canvas = _sourceVar.GetValueAsComponent(_source, typeof(Canvas)) as Canvas;
			if (!(canvas == null))
			{
				switch (_property)
				{
				case Property.CachedSortingLayerValue:
					_output.SetValue(canvas.cachedSortingLayerValue);
					break;
				case Property.IsRootCanvas:
					_output.SetValue(canvas.isRootCanvas);
					break;
				case Property.OverridePixelPerfect:
					_output.SetValue(canvas.overridePixelPerfect);
					break;
				case Property.OverrideSorting:
					_output.SetValue(canvas.overrideSorting);
					break;
				case Property.PixelPerfect:
					_output.SetValue(canvas.pixelPerfect);
					break;
				case Property.PixelRect:
					_output.SetValue(canvas.pixelRect);
					break;
				case Property.PlaneDistance:
					_output.SetValue(canvas.planeDistance);
					break;
				case Property.ReferencePixelsPerUnit:
					_output.SetValue(canvas.referencePixelsPerUnit);
					break;
				case Property.RenderMode:
					_output.SetValue(canvas.renderMode);
					break;
				case Property.RenderOrder:
					_output.SetValue(canvas.renderOrder);
					break;
				case Property.ScaleFactor:
					_output.SetValue(canvas.scaleFactor);
					break;
				case Property.SortingLayerID:
					_output.SetValue(canvas.sortingLayerID);
					break;
				case Property.SortingLayerName:
					_output.SetValue(canvas.sortingLayerName);
					break;
				case Property.SortingOrder:
					_output.SetValue(canvas.sortingOrder);
					break;
				case Property.WorldCamera:
					_output.SetValue(canvas.worldCamera);
					break;
				}
			}
		}
	}
}
