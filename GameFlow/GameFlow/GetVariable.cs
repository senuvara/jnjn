using UnityEngine;

namespace GameFlow
{
	[Help("en", "Gets a variable with the specified id in the indicated scope.", null)]
	[Help("es", "Obtiene una variable con el id indicado en el ámbito especificado.", null)]
	[AddComponentMenu("")]
	public class GetVariable : Action, IExecutableInEditor
	{
		[SerializeField]
		private VariableScope _scope = VariableScope.All;

		[SerializeField]
		private GameObject _source;

		[SerializeField]
		private Variable _sourceVar;

		[SerializeField]
		private string _id = string.Empty;

		[SerializeField]
		private Variable _idVar;

		[SerializeField]
		private Variable _output;

		public VariableScope scope => _scope;

		public GameObject source => _sourceVar.GetValue(_source) as GameObject;

		public string id => _idVar.GetValue(_id);

		protected override void OnReset()
		{
			_source = base.gameObject;
		}

		public override void Execute()
		{
			if (!(_output == null))
			{
				Variable variable = null;
				switch (scope)
				{
				case VariableScope.GameObject:
					variable = Variables.GetVariableById(id, source);
					break;
				case VariableScope.GameObjectHierarchy:
					variable = Variables.GetVariableById(id, source, recursive: true);
					break;
				case VariableScope.All:
					variable = Variables.GetVariableById(id);
					break;
				}
				_output.SetValue(variable, (!(variable != null)) ? typeof(Variable) : variable.GetType());
			}
		}
	}
}
