using System.Collections.Generic;
using UnityEngine;

namespace GameFlow
{
	[Help("en", "Defines an overview of the scene.", null)]
	[AddComponentMenu("GameFlow/Tools/Overview")]
	[DisallowMultipleComponent]
	[Help("es", "Define un resumen general de la escena.", null)]
	public class Overview : Container, IBlockContainer, ITool, ILinkContainer
	{
		[SerializeField]
		private bool _editorMode = true;

		[SerializeField]
		private List<Link> _links = new List<Link>();

		public List<Link> GetLinks()
		{
			return _links;
		}

		public virtual void LinkAdded(Link link)
		{
		}

		public override List<Block> GetBlocks()
		{
			return new List<Block>(_links.ToArray());
		}

		public override bool Contains(Block block)
		{
			Link link = block as Link;
			if (link != null)
			{
				return _links.Contains(link);
			}
			return false;
		}

		private void Start()
		{
		}
	}
}
