using System.IO;
using UnityEngine;

namespace GameFlow
{
	public class TextureUtils
	{
		public static Texture2D CreateTexture2D(int width, int height, int[] rgbaPixels)
		{
			Texture2D texture2D = new Texture2D(width, height);
			Color32[] array = new Color32[rgbaPixels.Length];
			for (int i = 0; i < rgbaPixels.Length; i++)
			{
				byte r = (byte)((rgbaPixels[i] & 4278190080u) >> 24);
				byte g = (byte)((rgbaPixels[i] & 0xFF0000) >> 16);
				byte b = (byte)((rgbaPixels[i] & 0xFF00) >> 8);
				byte a = (byte)(rgbaPixels[i] & 0xFF);
				array[i] = new Color32(r, g, b, a);
			}
			texture2D.SetPixels32(array);
			texture2D.Apply(updateMipmaps: false);
			texture2D.hideFlags = HideFlags.DontSave;
			return texture2D;
		}

		public static void MakePixelsTransparent(int backColor, int[] rgbaPixels, bool oneMinus)
		{
			for (int i = 0; i < rgbaPixels.Length; i++)
			{
				if (rgbaPixels[i] == backColor)
				{
					rgbaPixels[i] = 0;
					continue;
				}
				byte b = (byte)((rgbaPixels[i] & 4278190080u) >> 24);
				byte b2 = (byte)((rgbaPixels[i] & 0xFF0000) >> 16);
				byte b3 = (byte)((rgbaPixels[i] & 0xFF00) >> 8);
				byte b4 = (byte)(255f * ((Color)new Color32(b, b2, b3, byte.MaxValue)).gamma.grayscale);
				byte b5 = (byte)((!oneMinus) ? (b4 + 34) : (255 - b4 + 64));
				rgbaPixels[i] = ((b << 24) | (b2 << 16) | (b3 << 8) | b5);
			}
		}

		public static Texture2D CreateColorTexture(int width, int height, Color color)
		{
			Texture2D texture2D = new Texture2D(width, height);
			Color32[] array = new Color32[width * height];
			for (int i = 0; i < array.Length; i++)
			{
				array[i] = color;
			}
			texture2D.SetPixels32(array);
			texture2D.Apply(updateMipmaps: false);
			texture2D.hideFlags = HideFlags.DontSave;
			return texture2D;
		}

		public static Texture2D CreateBoxTexture(int width, int height, Color backColor, Color borderColor, int borderSize = 1)
		{
			Texture2D texture2D = new Texture2D(width, height);
			Color32[] array = new Color32[width * height];
			int num = 0;
			for (int i = 0; i < borderSize; i++)
			{
				for (int j = 0; j < width; j++)
				{
					array[num++] = borderColor;
				}
			}
			for (int k = borderSize; k < height - borderSize; k++)
			{
				for (int l = 0; l < borderSize; l++)
				{
					array[num++] = borderColor;
				}
				for (int m = borderSize; m < width - borderSize; m++)
				{
					array[num++] = backColor;
				}
				for (int n = 0; n < borderSize; n++)
				{
					array[num++] = borderColor;
				}
			}
			for (int num2 = 0; num2 < borderSize; num2++)
			{
				for (int num3 = 0; num3 < width; num3++)
				{
					array[num++] = borderColor;
				}
			}
			texture2D.SetPixels32(array);
			texture2D.Apply(updateMipmaps: false);
			texture2D.hideFlags = HideFlags.DontSave;
			return texture2D;
		}

		public static Texture2D CreateCopy(Texture2D source)
		{
			Texture2D texture2D = new Texture2D(source.width, source.height);
			Color32[] pixels = source.GetPixels32();
			texture2D.SetPixels32(pixels);
			texture2D.Apply(updateMipmaps: false);
			texture2D.hideFlags = HideFlags.DontSave;
			return texture2D;
		}

		public static void SaveTextureAsFile(Texture2D texture, string filePath)
		{
			byte[] bytes = texture.EncodeToPNG();
			File.WriteAllBytes(filePath, bytes);
		}

		public static void SaveTextureAsAsset(Texture2D texture, string assetPath)
		{
			SaveTextureAsFile(texture, Application.dataPath + "/" + assetPath);
		}
	}
}
