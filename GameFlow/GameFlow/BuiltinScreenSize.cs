using System;
using UnityEngine;

namespace GameFlow
{
	[AddComponentMenu("")]
	[Help("en", "A built-in @GameFlow{Variable} that returns the current screen size.", null)]
	[Help("es", "Una @GameFlow{Variable} integrada que devuelve la resolución actual de la pantalla.", null)]
	public class BuiltinScreenSize : BuiltinVariable
	{
		public override Vector2 vector2Value => new Vector2(Screen.width, Screen.height);

		public override Type GetVariableType()
		{
			return typeof(Vector2);
		}

		protected override string GetVariableId()
		{
			return "Screen Size";
		}
	}
}
