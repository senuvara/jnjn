using UnityEngine;

namespace GameFlow
{
	[AddComponentMenu("")]
	[Help("en", "Gets a new replaced version of the text stored in the specified @GameFlow{Variable}.", null)]
	[Help("es", "Devuelve una versión reemplazada del texto guardado en la @GameFlow{Variable}.", null)]
	public class ReplaceInString : Action, IExecutableInEditor
	{
		[SerializeField]
		private string _replace;

		[SerializeField]
		private Variable _replaceVar;

		[SerializeField]
		private string _with;

		[SerializeField]
		private Variable _withVar;

		[SerializeField]
		private Variable _variable;

		public override void Execute()
		{
			Execute(_replaceVar.GetValue(_replace), _withVar.GetValue(_with), _variable);
		}

		public static void Execute(string replace, string with, Variable variable)
		{
			if (!(variable == null))
			{
				variable.SetValue(variable.GetValue(string.Empty).Replace(replace, with));
			}
		}
	}
}
