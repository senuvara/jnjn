using System;
using System.Collections.Generic;
using UnityEngine;

namespace GameFlow
{
	public abstract class EventProgram : Program, IEventListener
	{
		[SerializeField]
		private EventListeningRange _listeningRange;

		[SerializeField]
		private GameObject _listeningTarget;

		[SerializeField]
		private List _listeningList;

		public EventListeningRange listeningRange => _listeningRange;

		public GameObject listeningTarget
		{
			get
			{
				if (_listeningTarget == null && Application.isPlaying)
				{
					return base.gameObject;
				}
				return _listeningTarget;
			}
		}

		public List listeningList => _listeningList;

		protected override void OnReset()
		{
			base.OnReset();
			_listeningTarget = base.gameObject;
			_listeningList = base.gameObject.GetComponent<List>();
		}

		protected override void OnAwake()
		{
			RegisterAsListener();
			CacheParameters();
		}

		protected virtual void RegisterAsListener()
		{
		}

		protected virtual void CacheParameters()
		{
		}

		protected void CacheParameter(Parameter param)
		{
		}

		protected EventController[] RegisterAsListener(Type eventType, Type controllerType, EventController.CreateControllerDelegate CreateController)
		{
			EventController[] result = null;
			switch (listeningRange)
			{
			case EventListeningRange.GameObject:
			{
				EventController controllerInGameObject = EventController.GetControllerInGameObject(controllerType, listeningTarget, CreateController);
				if (controllerInGameObject != null)
				{
					controllerInGameObject.AddListener(this, eventType);
					result = new EventController[1]
					{
						controllerInGameObject
					};
				}
				break;
			}
			case EventListeningRange.GameObjectHierarchy:
				result = EventController.GetControllersInHierarchyOf(controllerType, listeningTarget, CreateController);
				result = SubscribeTo(result, eventType);
				break;
			case EventListeningRange.SceneHierarchy:
				result = EventController.GetControllersInScene(controllerType, CreateController);
				result = new EventController[0];
				break;
			case EventListeningRange.List:
				result = EventController.GetControllersInList(controllerType, listeningList, CreateController);
				result = SubscribeTo(result, eventType);
				break;
			}
			return result;
		}

		protected EventController[] SubscribeTo(EventController[] controllers, Type eventType)
		{
			List<EventController> list = new List<EventController>();
			foreach (IEventDispatcher eventDispatcher in controllers)
			{
				if (eventDispatcher != null)
				{
					eventDispatcher.AddListener(this, eventType);
					list.Add(eventDispatcher as EventController);
				}
			}
			return list.ToArray();
		}

		protected void UnsubscribeFrom(UnityEngine.Object[] objects, Type eventType)
		{
			foreach (UnityEngine.Object @object in objects)
			{
				(@object as IEventDispatcher)?.RemoveListener(this, eventType);
			}
		}

		protected void UnRegisterAsListener(Type eventType, Type controllerType)
		{
			switch (listeningRange)
			{
			case EventListeningRange.GameObject:
				EventController.RemoveListenerFrom(this, eventType, EventController.GetControllerInGameObject(controllerType, listeningTarget));
				break;
			case EventListeningRange.GameObjectHierarchy:
				EventController.RemoveListenerFrom(this, eventType, EventController.GetControllersInHierarchyOf(controllerType, listeningTarget));
				break;
			case EventListeningRange.SceneHierarchy:
				EventController.RemoveListenerFrom(this, eventType, EventController.GetControllersInScene(controllerType));
				break;
			case EventListeningRange.List:
				EventController.RemoveListenerFrom(this, eventType, EventController.GetControllersInList(controllerType, listeningList));
				break;
			}
		}

		public bool IsListening()
		{
			return this != null && base.enabled;
		}

		public virtual void EventReceived(EventObject e)
		{
			if (AcceptEvent(e))
			{
				SetParameters(e);
				Restart();
			}
		}

		protected virtual bool AcceptEvent(EventObject e)
		{
			return true;
		}

		protected virtual void SetParameters(EventObject e)
		{
		}

		public override bool Contains(Block block)
		{
			if (block != null)
			{
				if (block as Parameter != null)
				{
					return GetParameters().Contains(block as Parameter);
				}
				if (block as Action != null)
				{
					return GetActions().Contains(block as Action);
				}
			}
			return false;
		}

		private void OnDestroy()
		{
			UnRegisterAsListener();
		}

		protected virtual void UnRegisterAsListener()
		{
		}
	}
}
