using UnityEngine;

namespace GameFlow
{
	[AddComponentMenu("")]
	[Help("en", "Evaluates the Tag of the specified @UnityManual{GameObject}.", null)]
	[Help("es", "Evalúa la etiqueta del @UnityManual{GameObject} especificado.", null)]
	public class TagCondition : Condition
	{
		public enum Comparison
		{
			IsEqualTo,
			IsNotEqualTo
		}

		[SerializeField]
		private GameObject _gameObject;

		[SerializeField]
		private Variable _gameObjectVar;

		[SerializeField]
		private Comparison _comparison;

		[SerializeField]
		private string _tag = "Untagged";

		[SerializeField]
		private Variable _tagVar;

		public override bool Evaluate()
		{
			GameObject gameObject = _gameObjectVar.GetValue(_gameObject) as GameObject;
			if (gameObject == null)
			{
				return false;
			}
			switch (_comparison)
			{
			case Comparison.IsEqualTo:
				return gameObject.tag.Equals(_tagVar.GetValue(_tag));
			case Comparison.IsNotEqualTo:
				return !gameObject.tag.Equals(_tagVar.GetValue(_tag));
			default:
				return false;
			}
		}
	}
}
