using UnityEngine;

namespace GameFlow
{
	[Help("es", "Reinicia el temporizador (@GameFlow{Timer}) especificado.", null)]
	[Help("en", "Restarts the specified @GameFlow{Timer}.", null)]
	[AddComponentMenu("")]
	public class RestartTimer : Action
	{
		[SerializeField]
		private Timer _target;

		[SerializeField]
		private Variable _targetVar;

		public override void Execute()
		{
			Timer timer = _targetVar.GetValue(_target) as Timer;
			if (timer != null)
			{
				timer.Restart();
			}
		}
	}
}
