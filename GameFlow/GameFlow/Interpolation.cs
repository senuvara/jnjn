using System;

namespace GameFlow
{
	[Flags]
	public enum Interpolation
	{
		Position = 0x1,
		Rotation = 0x2,
		Scale = 0x4
	}
}
