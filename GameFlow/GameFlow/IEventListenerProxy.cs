namespace GameFlow
{
	public interface IEventListenerProxy
	{
		void EventReceivedByListener(EventObject e, IEventListener listener);
	}
}
