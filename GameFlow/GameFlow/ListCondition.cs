using System;
using UnityEngine;

namespace GameFlow
{
	[Help("en", "Evaluates properties and content of the specified list.", null)]
	[AddComponentMenu("")]
	[Help("es", "Evalúa propiedades y contenido de la lista especificada.", null)]
	public class ListCondition : Condition
	{
		public enum Comparison
		{
			IsEmpty,
			IsNotEmpty,
			Contains,
			DoesNotContain
		}

		[SerializeField]
		private List _list;

		[SerializeField]
		private Variable _listVar;

		[SerializeField]
		private Comparison _comparison;

		[SerializeField]
		private string _stringValue;

		[SerializeField]
		private int _intValue;

		[SerializeField]
		private float _floatValue;

		[SerializeField]
		private bool _boolValue;

		[SerializeField]
		private Vector2 _vector2Value;

		[SerializeField]
		private Vector3 _vector3Value;

		[SerializeField]
		private Vector4 _vector4Value;

		[SerializeField]
		private Rect _rectValue;

		[SerializeField]
		private Color _colorValue;

		[SerializeField]
		private UnityEngine.Object _objectValue;

		[SerializeField]
		private int _enumIntValue;

		[SerializeField]
		private AnimationCurve _curveValue;

		[SerializeField]
		private Bounds _boundsValue;

		[SerializeField]
		private Quaternion _quaternionValue;

		[SerializeField]
		private Variable _valueVar;

		public override bool Evaluate()
		{
			List list = _listVar.GetValue(_list) as List;
			if (list == null)
			{
				return false;
			}
			switch (_comparison)
			{
			case Comparison.IsEmpty:
				return list.Count == 0;
			case Comparison.IsNotEmpty:
				return list.Count != 0;
			case Comparison.Contains:
				return ValueIsContainedInList(list);
			case Comparison.DoesNotContain:
				return !ValueIsContainedInList(list);
			default:
				return false;
			}
		}

		private bool ValueIsContainedInList(List list)
		{
			switch (list.dataType)
			{
			case DataType.String:
			case DataType.Tag:
				return list.Contains(_valueVar.GetValue(_stringValue));
			case DataType.Integer:
			case DataType.Layer:
				return list.Contains(_valueVar.GetValue(_intValue));
			case DataType.Float:
				return list.Contains(_valueVar.GetValue(_floatValue));
			case DataType.Boolean:
			case DataType.Toggle:
				return list.Contains(_valueVar.GetValue(_boolValue));
			case DataType.Vector2:
				return list.Contains(_valueVar.GetValue(_vector2Value));
			case DataType.Vector3:
				return list.Contains(_valueVar.GetValue(_vector3Value));
			case DataType.Vector4:
				return list.Contains(_valueVar.GetValue(_vector4Value));
			case DataType.Rect:
				return list.Contains(_valueVar.GetValue(_rectValue));
			case DataType.Color:
				return list.Contains(_valueVar.GetValue(_colorValue));
			case DataType.Object:
				return list.Contains(_valueVar.GetValue(_objectValue));
			case DataType.Enum:
				return list.Contains((Enum)Enum.ToObject(list.type, _valueVar.GetValue(_enumIntValue)));
			case DataType.AnimationCurve:
				return list.Contains(_valueVar.GetValue(_curveValue));
			case DataType.Bounds:
				return list.Contains(_valueVar.GetValue(_boundsValue));
			case DataType.Quaternion:
				return list.Contains(_valueVar.GetValue(_quaternionValue));
			default:
				return false;
			}
		}
	}
}
