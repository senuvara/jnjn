using System;
using UnityEngine;

namespace GameFlow
{
	[Help("en", "A built-in @GameFlow{Variable} that returns the second for the current time.", null)]
	[AddComponentMenu("")]
	[Help("es", "Una @GameFlow{Variable} integrada que devuelve el segundo de la hora actual.", null)]
	public class BuiltinSecond : BuiltinVariable
	{
		public override int intValue => DateTime.Now.Second;

		public override string stringValue => Convert.ToString(DateTime.Now.Second).PadLeft(2, '0');

		public override Type GetVariableType()
		{
			return typeof(int);
		}

		protected override string GetVariableId()
		{
			return "Second";
		}
	}
}
