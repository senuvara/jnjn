namespace GameFlow
{
	public interface IEventListener
	{
		void EventReceived(EventObject e);

		bool IsListening();
	}
}
