using System;
using System.Collections.Generic;
using UnityEngine;

namespace GameFlow
{
	public abstract class EventController : BaseBehaviour, IEventDispatcher, IHidden, ITool
	{
		public delegate EventController CreateControllerDelegate(GameObject gameObject);

		private static GameObject[] gameObjects;

		public static EventController GetControllerInGameObject(Type controllerType, GameObject gameObject, CreateControllerDelegate CreateController = null)
		{
			EventController eventController = null;
			if (gameObject != null && !gameObject.IsHiddenInHierarchy())
			{
				eventController = (gameObject.GetComponent(controllerType) as EventController);
				if (eventController == null && CreateController != null)
				{
					eventController = CreateController(gameObject);
				}
			}
			return eventController;
		}

		public static EventController[] GetControllersInHierarchyOf(Type controllerType, GameObject gameObject, CreateControllerDelegate CreateEventController = null)
		{
			List<EventController> list = new List<EventController>();
			if (gameObject != null)
			{
				GameObject parent = gameObject.GetParent();
				Transform[] componentsInChildren = ((!(parent == null)) ? parent : gameObject).GetComponentsInChildren<Transform>(includeInactive: true);
				Transform[] array = componentsInChildren;
				foreach (Transform transform in array)
				{
					EventController controllerInGameObject = GetControllerInGameObject(controllerType, transform.gameObject, CreateEventController);
					if (controllerInGameObject != null)
					{
						list.Add(controllerInGameObject);
					}
				}
			}
			return list.ToArray();
		}

		public static GameObject[] GetGameObjectsInScene()
		{
			if (gameObjects == null)
			{
				gameObjects = UnityEngine.Object.FindObjectsOfType<GameObject>();
			}
			return gameObjects;
		}

		public static EventController[] GetControllersInScene(Type controllerType, CreateControllerDelegate CreateEventController = null)
		{
			List<EventController> list = new List<EventController>();
			GameObject[] gameObjectsInScene = GetGameObjectsInScene();
			GameObject[] array = gameObjectsInScene;
			foreach (GameObject gameObject in array)
			{
				EventController controllerInGameObject = GetControllerInGameObject(controllerType, gameObject, CreateEventController);
				if (controllerInGameObject != null)
				{
					list.Add(controllerInGameObject);
				}
			}
			return list.ToArray();
		}

		public static EventController[] GetControllersInList(Type controllerType, List list, CreateControllerDelegate CreateEventController = null)
		{
			List<EventController> list2 = new List<EventController>();
			if (list != null)
			{
				for (int i = 0; i < list.Count; i++)
				{
					GameObject gameObject = list.GetObjectAt(i) as GameObject;
					EventController controllerInGameObject = GetControllerInGameObject(controllerType, gameObject, CreateEventController);
					if (controllerInGameObject != null)
					{
						list2.Add(controllerInGameObject);
					}
				}
			}
			return list2.ToArray();
		}

		public static EventController AddController(GameObject gameObject, Type controllerType, bool hideController = true)
		{
			EventController eventController = gameObject.AddComponent(controllerType) as EventController;
			if (eventController != null && hideController)
			{
				eventController.hideFlags = HideFlags.HideInInspector;
			}
			return eventController;
		}

		public static bool AddListenerTo(IEventListener listener, Type eventType, EventController controller)
		{
			if (controller != null)
			{
				controller.AddListener(listener, eventType);
				return true;
			}
			return false;
		}

		public static bool AddListenerTo(IEventListener listener, Type eventType, EventController[] controllers)
		{
			bool result = false;
			for (int i = 0; i < controllers.Length; i++)
			{
				if (controllers[i] != null)
				{
					controllers[i].AddListener(listener, eventType);
					result = true;
				}
			}
			return result;
		}

		public static void RemoveListenerFrom(IEventListener listener, Type eventType, EventController controller)
		{
			if (controller != null)
			{
				controller.RemoveListener(listener, eventType);
			}
		}

		public static void RemoveListenerFrom(IEventListener listener, Type eventType, EventController[] controllers)
		{
			for (int i = 0; i < controllers.Length; i++)
			{
				if (controllers[i] != null)
				{
					controllers[i].RemoveListener(listener, eventType);
				}
			}
		}

		public virtual void AddListener(IEventListener listener, Type eventType)
		{
		}

		public virtual void RemoveListener(IEventListener listener, Type eventType)
		{
		}

		private void Start()
		{
		}
	}
}
