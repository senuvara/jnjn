using UnityEngine;

namespace GameFlow
{
	[AddComponentMenu("")]
	[Help("en", "Releases all the objects in the specified @GameFlow{Pool}.", null)]
	[Help("es", "Libera todos los objetos alojados en el @GameFlow{Pool} especificado.", null)]
	public class ResetPool : Action
	{
		[SerializeField]
		private Pool _pool;

		[SerializeField]
		private Variable _poolVar;

		public override void Execute()
		{
			Pool pool = _poolVar.GetValue(_pool) as Pool;
			if (pool != null)
			{
				pool.ReleaseAll();
			}
		}
	}
}
