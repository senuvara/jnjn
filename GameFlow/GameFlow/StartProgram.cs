using UnityEngine;

namespace GameFlow
{
	[Help("en", "Restarts the specified @GameFlow{Program}.", null)]
	[Help("es", "Reinicia el programa (@GameFlow{Program}) especificado.", null)]
	[AddComponentMenu("")]
	public class StartProgram : RestartProgram
	{
	}
}
