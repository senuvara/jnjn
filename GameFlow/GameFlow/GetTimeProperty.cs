using UnityEngine;

namespace GameFlow
{
	[Help("en", "Gets the value of a time-related property.", null)]
	[Help("es", "Lee el valor de una propiedad relativa al tiempo.", null)]
	[AddComponentMenu("")]
	public class GetTimeProperty : Action, IExecutableInEditor
	{
		public enum Property
		{
			CaptureFramerate,
			DeltaTime,
			FixedDeltaTime,
			FixedTime,
			FrameCount,
			FrameBeginTime,
			MaximumDeltaTime,
			RealtimeSinceStartup,
			RenderedFrameCount,
			SmoothDeltaTime,
			TimeScale,
			TimeSinceLevelLoad,
			UnscaledDeltaTime,
			UnscaledTime
		}

		[SerializeField]
		private Property _property;

		[SerializeField]
		private Variable _output;

		public override void Execute()
		{
			switch (_property)
			{
			case Property.CaptureFramerate:
				_output.SetValue(Time.captureFramerate);
				break;
			case Property.DeltaTime:
				_output.SetValue(Time.deltaTime);
				break;
			case Property.FixedDeltaTime:
				_output.SetValue(Time.fixedDeltaTime);
				break;
			case Property.FixedTime:
				_output.SetValue(Time.fixedTime);
				break;
			case Property.FrameBeginTime:
				_output.SetValue(Time.time);
				break;
			case Property.FrameCount:
				_output.SetValue(Time.frameCount);
				break;
			case Property.MaximumDeltaTime:
				_output.SetValue(Time.maximumDeltaTime);
				break;
			case Property.RealtimeSinceStartup:
				_output.SetValue(Time.realtimeSinceStartup);
				break;
			case Property.RenderedFrameCount:
				_output.SetValue(Time.renderedFrameCount);
				break;
			case Property.SmoothDeltaTime:
				_output.SetValue(Time.smoothDeltaTime);
				break;
			case Property.TimeScale:
				_output.SetValue(Time.timeScale);
				break;
			case Property.TimeSinceLevelLoad:
				_output.SetValue(Time.timeSinceLevelLoad);
				break;
			case Property.UnscaledDeltaTime:
				_output.SetValue(Time.unscaledDeltaTime);
				break;
			case Property.UnscaledTime:
				_output.SetValue(Time.unscaledTime);
				break;
			}
		}
	}
}
