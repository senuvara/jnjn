using System;
using UnityEngine;

namespace GameFlow
{
	public interface IVariableExtProxy
	{
		void SetValue(Variable variable, string value);

		void SetTagValue(Variable variable, string value);

		void SetValue(Variable variable, int value);

		void SetLayerValue(Variable variable, int value);

		void SetValue(Variable variable, float value);

		void SetValue(Variable variable, bool value);

		void SetToggleValue(Variable variable, bool value);

		void SetValue(Variable variable, Vector2 value);

		void SetValue(Variable variable, Vector3 value);

		void SetValue(Variable variable, Vector4 value);

		void SetValue(Variable variable, Rect value);

		void SetValue(Variable variable, Color value);

		void SetValue(Variable variable, UnityEngine.Object value, Type valueType);

		void SetValue(Variable variable, UnityEngine.Object value);

		void SetValue(Variable variable, Enum value);

		void SetValue(Variable variable, Bounds value);

		void SetValue(Variable variable, Quaternion value);

		void SetValue(Variable variable, AnimationCurve value);
	}
}
