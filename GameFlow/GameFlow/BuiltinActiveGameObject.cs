using System;
using UnityEngine;

namespace GameFlow
{
	[AddComponentMenu("")]
	[Help("es", "Una @GameFlow{Variable} integrada que devuelve el @UnityManual{GameObject} activo (el mostrado en el inspector).", null)]
	[Help("en", "A built-in @GameFlow{Variable} that returns the active @UnityManual{GameObject} (the one shown in the inspector).", null)]
	public class BuiltinActiveGameObject : BuiltinVariable
	{
		public delegate GameObject GetActiveGameObjectDelegate();

		public static GetActiveGameObjectDelegate Delegate;

		public override UnityEngine.Object objectValue
		{
			get
			{
				if (!Application.isPlaying && ProgramExecutor.isPlaying && Delegate != null)
				{
					return Delegate();
				}
				return null;
			}
		}

		public override Type GetVariableType()
		{
			return typeof(GameObject);
		}

		protected override string GetVariableId()
		{
			return "Active GameObject";
		}
	}
}
