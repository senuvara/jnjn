using UnityEngine;

namespace GameFlow
{
	[AddComponentMenu("GameFlow/Programs/On Deactivate")]
	[Help("en", "Program that will be executed every time a @UnityManual{GameObject} in the listening range gets deactivated.", null)]
	[DisallowMultipleComponent]
	[Help("es", "Programa que se ejecutará cada vez que un @UnityManual{GameObject} en el rango de escucha sea desactivado.", null)]
	public class OnDeactivate : OnActivationProgram
	{
		private EventController[] _controllers;

		protected override void RegisterAsListener()
		{
			_controllers = RegisterAsListener(typeof(DeactivationEvent), typeof(ActivationController), ActivationController.AddController);
			if (base.listeningRange == EventListeningRange.SceneHierarchy)
			{
				DeactivationEvent.AddListener(this);
			}
		}

		protected override void SetParameters(EventObject e)
		{
			DeactivationEvent deactivationEvent = e as DeactivationEvent;
			base.sourceParam.SetValue(deactivationEvent.source);
		}

		protected override void UnRegisterAsListener()
		{
			DeactivationEvent.RemoveListener(this);
			UnRegisterAsListener(typeof(DeactivationEvent), typeof(ActivationController));
		}

		private void OnTransformChildrenChanged()
		{
			if (base.listeningRange == EventListeningRange.GameObjectHierarchy)
			{
				UnsubscribeFrom(_controllers, typeof(DeactivationEvent));
				_controllers = EventController.GetControllersInHierarchyOf(typeof(ActivationController), base.listeningTarget, ActivationController.AddController);
				SubscribeTo(_controllers, typeof(DeactivationEvent));
			}
		}
	}
}
