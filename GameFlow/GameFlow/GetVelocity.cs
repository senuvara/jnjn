using UnityEngine;

namespace GameFlow
{
	[Help("es", "Obtiene la velocidad del objeto que contiene al @UnityManual{Rigidbody} especificado.", null)]
	[Help("en", "Gets the velocity of the object containing the specified @UnityManual{Rigidbody} component.", null)]
	[AddComponentMenu("")]
	public class GetVelocity : Action, IExecutableInEditor
	{
		[SerializeField]
		private Rigidbody _target;

		[SerializeField]
		private Variable _targetVar;

		[SerializeField]
		private Variable _output;

		protected override void OnReset()
		{
		}

		public override void Setup()
		{
		}

		public override void Execute()
		{
			if (!(_output == null))
			{
				Rigidbody rigidbody = _target;
				if (rigidbody == null)
				{
					rigidbody = (_targetVar.GetValueAsComponent(_target, typeof(Rigidbody)) as Rigidbody);
				}
				if (rigidbody != null)
				{
					_output.SetValue(rigidbody.velocity);
				}
			}
		}
	}
}
