using UnityEngine;

namespace GameFlow
{
	[AddComponentMenu("")]
	[Help("es", "Reinicia el programa (@GameFlow{Program}) especificado.", null)]
	[Help("en", "Restarts the specified @GameFlow{Program}.", null)]
	public class RestartProgram : Action, IExecutableInEditor
	{
		[SerializeField]
		private Program _target;

		[SerializeField]
		private Variable _targetVar;

		[SerializeField]
		private bool _preEnableProgram = true;

		protected override void OnReset()
		{
		}

		public override void Execute()
		{
			Execute(_targetVar.GetValue(_target) as Program, _preEnableProgram);
		}

		public static void Execute(Program program, bool preEnableProgram)
		{
			if ((bool)program)
			{
				if (preEnableProgram)
				{
					program.enabled = true;
				}
				program.Restart();
			}
		}
	}
}
