using UnityEngine;

namespace GameFlow
{
	[Help("en", "Program that will be executed when a notification from @GameFlow{StartGame} is received.", null)]
	[Help("es", "Programa que se ejecutará cuando se reciba una notificación por parte de @GameFlow{StartGame}.", null)]
	[AddComponentMenu("GameFlow/Programs/On Game Start")]
	[DisallowMultipleComponent]
	public class OnGameStart : EventProgram
	{
		protected override void RegisterAsListener()
		{
			GameStartEvent.AddListener(this);
		}

		protected override void UnRegisterAsListener()
		{
			GameStartEvent.RemoveListener(this);
		}
	}
}
