using UnityEngine;

namespace GameFlow
{
	[AddComponentMenu("")]
	[DisallowMultipleComponent]
	public class MouseDeltaController : BaseBehaviour
	{
		public static Vector2 delta;

		private static Vector2 _lastPosition;

		protected override void OnReset()
		{
			HideInInspector();
		}

		private void Start()
		{
			_lastPosition = Input.mousePosition;
		}

		private void Update()
		{
			delta = (Vector2)Input.mousePosition - _lastPosition;
			_lastPosition = Input.mousePosition;
		}
	}
}
