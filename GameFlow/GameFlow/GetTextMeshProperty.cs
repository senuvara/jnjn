using UnityEngine;

namespace GameFlow
{
	[Help("es", "Lee una propiedad del componente @UnityManual{TextMesh} especificado.", null)]
	[Help("en", "Gets a property of the specified @UnityManual{TextMesh} component.", null)]
	[AddComponentMenu("")]
	public class GetTextMeshProperty : Action, IExecutableInEditor
	{
		public enum Property
		{
			Alignment,
			Anchor,
			CharacterSize,
			Color,
			Font,
			FontSize,
			FontStyle,
			LineSpacing,
			OffsetZ,
			RichText,
			TabSize,
			Text
		}

		[SerializeField]
		private TextMesh _source;

		[SerializeField]
		private Variable _sourceVar;

		[SerializeField]
		private Property _property = Property.Text;

		[SerializeField]
		private Variable _output;

		protected override void OnReset()
		{
			_source = base.gameObject.GetComponent<TextMesh>();
		}

		public override void Setup()
		{
		}

		public override void Execute()
		{
			TextMesh textMesh = _sourceVar.GetValueAsComponent(_source, typeof(TextMesh)) as TextMesh;
			if (!(textMesh == null))
			{
				switch (_property)
				{
				case Property.Alignment:
					_output.SetValue(textMesh.alignment);
					break;
				case Property.Anchor:
					_output.SetValue(textMesh.anchor);
					break;
				case Property.CharacterSize:
					_output.SetValue(textMesh.characterSize);
					break;
				case Property.Color:
					_output.SetValue(textMesh.color);
					break;
				case Property.Font:
					_output.SetValue(textMesh.font);
					break;
				case Property.FontSize:
					_output.SetValue(textMesh.fontSize);
					break;
				case Property.FontStyle:
					_output.SetValue(textMesh.fontStyle);
					break;
				case Property.LineSpacing:
					_output.SetValue(textMesh.lineSpacing);
					break;
				case Property.OffsetZ:
					_output.SetValue(textMesh.offsetZ);
					break;
				case Property.RichText:
					_output.SetValue(textMesh.richText);
					break;
				case Property.TabSize:
					_output.SetValue(textMesh.tabSize);
					break;
				case Property.Text:
					_output.SetValue(textMesh.text);
					break;
				}
			}
		}
	}
}
