using UnityEngine;

namespace GameFlow
{
	[Help("es", "Reproduce el sonido especificado como si estuviese delante de la cámara.", null)]
	[AddComponentMenu("")]
	[Help("en", "Plays the specified sound as it was in front of the camera.", null)]
	public class PlaySound : Action
	{
		[SerializeField]
		private AudioClip _audioClip;

		[SerializeField]
		private Variable _audioClipVar;

		[SerializeField]
		private float _volume = 1f;

		[SerializeField]
		private Variable _volumeVar;

		public override void Execute()
		{
			AudioController.instance.PlayFX(_audioClipVar.GetValue(_audioClip) as AudioClip, _volumeVar.GetValue(_volume));
		}
	}
}
