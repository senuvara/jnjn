using System;
using UnityEngine;

namespace GameFlow
{
	public interface IListProxy
	{
		void SetValueAt(List list, string value, int index);

		void SetValueAt(List list, int value, int index);

		void SetValueAt(List list, float value, int index);

		void SetValueAt(List list, bool value, int index);

		void SetValueAt(List list, Vector2 value, int index);

		void SetValueAt(List list, Vector3 value, int index);

		void SetValueAt(List list, Vector4 value, int index);

		void SetValueAt(List list, Rect value, int index);

		void SetValueAt(List list, Color value, int index);

		void SetValueAt(List list, UnityEngine.Object value, int index);

		void SetValueAt(List list, Enum value, int index);

		void SetValueAt(List list, AnimationCurve value, int index);

		void SetValueAt(List list, Bounds value, int index);

		void SetValueAt(List list, Quaternion value, int index);

		void InsertValueAt(List list, string value, int index);

		void InsertValueAt(List list, int value, int index);

		void InsertValueAt(List list, float value, int index);

		void InsertValueAt(List list, bool value, int index);

		void InsertValueAt(List list, Vector2 value, int index);

		void InsertValueAt(List list, Vector3 value, int index);

		void InsertValueAt(List list, Vector4 value, int index);

		void InsertValueAt(List list, Rect value, int index);

		void InsertValueAt(List list, Color value, int index);

		void InsertValueAt(List list, UnityEngine.Object value, int index);

		void InsertValueAt(List list, Enum value, int index);

		void InsertValueAt(List list, AnimationCurve value, int index);

		void InsertValueAt(List list, Bounds value, int index);

		void InsertValueAt(List list, Quaternion value, int index);

		void AddValue(List list, string value);

		void AddValue(List list, int value);

		void AddValue(List list, float value);

		void AddValue(List list, bool value);

		void AddValue(List list, Vector2 value);

		void AddValue(List list, Vector3 value);

		void AddValue(List list, Vector4 value);

		void AddValue(List list, Rect value);

		void AddValue(List list, Color value);

		void AddValue(List list, UnityEngine.Object value);

		void AddValue(List list, Enum value);

		void AddValue(List list, AnimationCurve value);

		void AddValue(List list, Bounds value);

		void AddValue(List list, Quaternion value);

		void RemoveValueAt(List list, int index);

		void Clear(List list);

		void ClearNulls(List list);

		void SwapValues(List list, int index1, int index2);
	}
}
