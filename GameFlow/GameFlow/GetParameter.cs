using UnityEngine;

namespace GameFlow
{
	[Help("en", "Gets a parameter with the specified id in the indicated scope.", null)]
	[AddComponentMenu("")]
	[Help("es", "Obtiene un parámetro con el id indicado en el ámbito especificado.", null)]
	public class GetParameter : Action, IExecutableInEditor
	{
		[SerializeField]
		private VariableScope _scope = VariableScope.All;

		[SerializeField]
		private GameObject _source;

		[SerializeField]
		private Variable _sourceVar;

		[SerializeField]
		private string _id = string.Empty;

		[SerializeField]
		private Variable _idVar;

		[SerializeField]
		private Variable _output;

		public VariableScope scope => _scope;

		public GameObject source => _sourceVar.GetValue(_source) as GameObject;

		public string id => _idVar.GetValue(_id);

		protected override void OnReset()
		{
			_source = base.gameObject;
		}

		public override void Execute()
		{
			if (!(_output == null))
			{
				Parameter parameter = null;
				switch (scope)
				{
				case VariableScope.GameObject:
					parameter = Parameters.GetParameterById(id, source);
					break;
				case VariableScope.GameObjectHierarchy:
					parameter = Parameters.GetParameterById(id, source, recursive: true);
					break;
				case VariableScope.All:
					parameter = Parameters.GetParameterById(id);
					break;
				}
				_output.SetValue(parameter, (!(parameter != null)) ? typeof(Parameter) : parameter.GetType());
			}
		}
	}
}
