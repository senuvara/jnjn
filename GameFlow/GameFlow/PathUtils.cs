using UnityEngine;

namespace GameFlow
{
	public static class PathUtils
	{
		public static Vector3 GetCatmullRomSplinePoint(Vector3 p1, Vector3 p2, Vector3 p3, Vector3 p4, float t)
		{
			float d = ((0f - t + 2f) * t - 1f) * t * 0.5f;
			float d2 = ((3f * t - 5f) * t * t + 2f) * 0.5f;
			float d3 = ((-3f * t + 4f) * t + 1f) * t * 0.5f;
			float d4 = (t - 1f) * t * t * 0.5f;
			return p1 * d + p2 * d2 + p3 * d3 + p4 * d4;
		}

		public static Vector3 GetPositionAtTime(Path path, float time, float duration, Direction direction = Direction.Forward)
		{
			float num = (!(duration > 0f)) ? 1f : duration;
			float num2 = time / num;
			num2 = ((direction != 0) ? (1f - num2) : num2);
			Segment segment = path.GetSegment(num2);
			float t = (num2 * path.length - segment.offset) / segment.length;
			return segment.GetPosition(t);
		}

		public static Vector3 GetPositionAtDistance(Path path, float distance, Direction direction = Direction.Forward)
		{
			float num = distance / path.length;
			num = ((direction != 0) ? (1f - num) : num);
			Segment segment = path.GetSegment(num);
			float t = (num * path.length - segment.offset) / segment.length;
			return segment.GetPosition(t);
		}
	}
}
