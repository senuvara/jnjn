using UnityEngine;
using UnityEngine.UI;

namespace GameFlow
{
	[Help("es", "Devuelve el color del material por defecto del objeto especificado.", null)]
	[Help("en", "Gets the color of the default material of the specified object.", null)]
	[AddComponentMenu("")]
	public class GetColor : Action, IExecutableInEditor
	{
		[SerializeField]
		private GameObject _source;

		[SerializeField]
		private Variable _sourceVar;

		[SerializeField]
		private Variable _output;

		public GameObject source => _sourceVar.GetValue(_source) as GameObject;

		protected override void OnReset()
		{
			_source = base.gameObject;
		}

		public override void Execute()
		{
			if (!(_output == null))
			{
				_output.SetValue(Execute(source));
			}
		}

		public static Color Execute(GameObject source)
		{
			if (source == null)
			{
				return Color.black;
			}
			Renderer component = source.GetComponent<Renderer>();
			if (component != null)
			{
				return component.material.color;
			}
			Text component2 = source.GetComponent<Text>();
			if (component2 != null)
			{
				return component2.color;
			}
			Image component3 = source.GetComponent<Image>();
			if (component3 != null)
			{
				return component3.color;
			}
			GUIText component4 = source.GetComponent<GUIText>();
			if (component4 != null)
			{
				return component4.color;
			}
			GUITexture component5 = source.GetComponent<GUITexture>();
			if (component5 != null)
			{
				return component5.color;
			}
			return Color.black;
		}
	}
}
