using UnityEngine;

namespace GameFlow
{
	[Help("en", "Increments the numeric value of the specified @GameFlow{Variable}.", null)]
	[AddComponentMenu("")]
	[Help("es", "Incrementa el valor numérico de la @GameFlow{Variable} especificada.", null)]
	public class IncrementVariableValue : VariableAction
	{
		protected override int GetDefaultIntValue()
		{
			return 1;
		}

		protected override float GetDefaultFloatValue()
		{
			return 1f;
		}

		public override void Execute()
		{
			Execute(base.variable, this);
		}

		public static void Execute(Variable variable, ValueAction valueAction)
		{
			if (variable == null || valueAction == null)
			{
				return;
			}
			Variable variable2 = (!variable.isIndirect) ? variable : variable.indirection;
			if (!(variable2 == null) && !variable2.isReadOnly)
			{
				switch (variable2.dataType)
				{
				case DataType.Integer:
					variable2.intValue += valueAction.intValue;
					break;
				case DataType.Float:
					variable2.floatValue += valueAction.floatValue;
					break;
				case DataType.Vector2:
					variable2.vector2Value += valueAction.vector2Value;
					break;
				case DataType.Vector3:
					variable2.vector3Value += valueAction.vector3Value;
					break;
				case DataType.Rect:
					variable2.rectValue = variable2.rectValue.Add(valueAction.rectValue);
					break;
				case DataType.Bounds:
					variable2.boundsValue = variable2.boundsValue.Add(valueAction.boundsValue);
					break;
				case DataType.Vector4:
					variable2.vector4Value += valueAction.vector4Value;
					break;
				}
			}
		}
	}
}
