using UnityEngine;

namespace GameFlow
{
	public static class BoundsExt
	{
		public static Bounds zero = new Bounds(Vector3.zero, Vector3.zero);

		public static Rect CalculateFrame(this Bounds bounds)
		{
			return GizmosUtils.CalculateBoundsFrame(bounds);
		}

		public static Bounds Add(this Bounds bounds, Bounds other)
		{
			bounds.center += other.center;
			bounds.extents += other.extents;
			return bounds;
		}

		public static Bounds Substract(this Bounds bounds, Bounds other)
		{
			bounds.center -= other.center;
			bounds.extents -= other.extents;
			return bounds;
		}

		public static Bounds Multiply(this Bounds bounds, Bounds multiplier)
		{
			bounds.center = Vector3.Scale(bounds.center, multiplier.center);
			bounds.extents = Vector3.Scale(bounds.extents, multiplier.extents);
			return bounds;
		}

		public static Bounds Divide(this Bounds bounds, Bounds divisor)
		{
			Vector3 center = bounds.center;
			Vector3 center2 = divisor.center;
			bounds.center = new Vector3(center.x / center2.x, center.y / center2.y, center.z / center2.z);
			Vector3 extents = bounds.extents;
			Vector3 extents2 = divisor.extents;
			bounds.extents = new Vector3(extents.x / extents2.x, extents.y / extents2.y, extents.z / extents2.z);
			return bounds;
		}
	}
}
