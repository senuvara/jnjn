using UnityEngine;

namespace GameFlow
{
	[AddComponentMenu("")]
	[Help("es", "Evalúa la expresión dada en la página web contenedora.", null)]
	[Help("en", "Evaluates the given expression in the containing web page.", null)]
	public class EvaluateExternal : Action
	{
		[SerializeField]
		private string _expression = "alert(\"Hi!\")";

		[SerializeField]
		private Variable _expressionVar;

		private void Awake()
		{
		}

		protected override void OnReset()
		{
		}

		public override void Execute()
		{
			Execute(_expressionVar.GetValue(_expression));
		}

		public static void Execute(string expression)
		{
			if (expression != null)
			{
				Application.ExternalEval(expression.Expanded());
			}
		}
	}
}
