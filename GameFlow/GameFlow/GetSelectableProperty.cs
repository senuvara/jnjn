using UnityEngine;
using UnityEngine.UI;

namespace GameFlow
{
	[Help("en", "Gets a property of the specified @UnityManual{Selectable} component.", null)]
	[AddComponentMenu("")]
	[Help("es", "Lee una propiedad del componente @UnityManual{Selectable} especificado.", null)]
	public class GetSelectableProperty : Action, IExecutableInEditor
	{
		public enum Property
		{
			AnimationTriggers_DisabledTrigger,
			AnimationTriggers_HighlightedTrigger,
			AnimationTriggers_NormalTrigger,
			AnimationTriggers_PressedTrigger,
			Animator,
			Colors_ColorMultiplier,
			Colors_DisabledColor,
			Colors_FadeDuration,
			Colors_HighlightedColor,
			Colors_NormalColor,
			Colors_PressedColor,
			Image,
			Interactable,
			Navigation_Mode,
			Navigation_SelectOnDown,
			Navigation_SelectOnLeft,
			Navigation_SelectOnRight,
			Navigation_SelectOnUp,
			SpriteState_DisabledSprite,
			SpriteState_HighlightedSprite,
			SpriteState_PressedSprite,
			TargetGraphic,
			Transition
		}

		[SerializeField]
		private Selectable _source;

		[SerializeField]
		private Variable _sourceVar;

		[SerializeField]
		private Property _property;

		[SerializeField]
		private Variable _output;

		protected override void OnReset()
		{
			_source = base.gameObject.GetComponent<Selectable>();
		}

		public override void Setup()
		{
		}

		public override void Execute()
		{
			Selectable selectable = _sourceVar.GetValueAsComponent(_source, typeof(Selectable)) as Selectable;
			if (!(selectable == null))
			{
				switch (_property)
				{
				case Property.AnimationTriggers_DisabledTrigger:
					_output.SetValue(selectable.animationTriggers.disabledTrigger);
					break;
				case Property.AnimationTriggers_HighlightedTrigger:
					_output.SetValue(selectable.animationTriggers.highlightedTrigger);
					break;
				case Property.AnimationTriggers_NormalTrigger:
					_output.SetValue(selectable.animationTriggers.normalTrigger);
					break;
				case Property.AnimationTriggers_PressedTrigger:
					_output.SetValue(selectable.animationTriggers.pressedTrigger);
					break;
				case Property.Animator:
					_output.SetValue(selectable.animator);
					break;
				case Property.Colors_ColorMultiplier:
					_output.SetValue(selectable.colors.colorMultiplier);
					break;
				case Property.Colors_DisabledColor:
					_output.SetValue(selectable.colors.disabledColor);
					break;
				case Property.Colors_FadeDuration:
					_output.SetValue(selectable.colors.fadeDuration);
					break;
				case Property.Colors_HighlightedColor:
					_output.SetValue(selectable.colors.highlightedColor);
					break;
				case Property.Colors_NormalColor:
					_output.SetValue(selectable.colors.normalColor);
					break;
				case Property.Colors_PressedColor:
					_output.SetValue(selectable.colors.pressedColor);
					break;
				case Property.Image:
					_output.SetValue(selectable.image);
					break;
				case Property.Interactable:
					_output.SetValue(selectable.interactable);
					break;
				case Property.Navigation_Mode:
					_output.SetValue(selectable.navigation.mode);
					break;
				case Property.Navigation_SelectOnDown:
					_output.SetValue(selectable.navigation.selectOnDown);
					break;
				case Property.Navigation_SelectOnLeft:
					_output.SetValue(selectable.navigation.selectOnLeft);
					break;
				case Property.Navigation_SelectOnRight:
					_output.SetValue(selectable.navigation.selectOnRight);
					break;
				case Property.Navigation_SelectOnUp:
					_output.SetValue(selectable.navigation.selectOnUp);
					break;
				case Property.SpriteState_DisabledSprite:
					_output.SetValue(selectable.spriteState.disabledSprite);
					break;
				case Property.SpriteState_HighlightedSprite:
					_output.SetValue(selectable.spriteState.highlightedSprite);
					break;
				case Property.SpriteState_PressedSprite:
					_output.SetValue(selectable.spriteState.pressedSprite);
					break;
				case Property.TargetGraphic:
					_output.SetValue(selectable.targetGraphic);
					break;
				case Property.Transition:
					_output.SetValue(selectable.transition);
					break;
				}
			}
		}
	}
}
