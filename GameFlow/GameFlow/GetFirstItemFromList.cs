using UnityEngine;

namespace GameFlow
{
	[AddComponentMenu("")]
	[Help("es", "Devuelve el primer elemento de la @GameFlow{List} especificada.", null)]
	[Help("en", "Gets the first item of the specified @GameFlow{List}.", null)]
	public class GetFirstItemFromList : ListAction
	{
		[SerializeField]
		private Variable _output;

		public override void Execute()
		{
			GetItemFromList.ExecuteWith(GetList(), 0, _output);
		}
	}
}
