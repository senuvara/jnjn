namespace GameFlow
{
	public enum VectorType
	{
		Vector2,
		Vector3,
		Vector4
	}
}
