using System.Collections.Generic;

namespace GameFlow
{
	public interface ICommandContainer : IBlockContainer
	{
		List<Command> GetCommands();

		void CommandAdded(Command command);
	}
}
