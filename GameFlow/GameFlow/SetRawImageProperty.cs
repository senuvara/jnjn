using UnityEngine;
using UnityEngine.UI;

namespace GameFlow
{
	[Help("en", "Modifies the value of a property of the specified Raw Image component.", null)]
	[Help("es", "Modifica el valor de una propiedad del componente Raw Image especificado.", null)]
	[AddComponentMenu("")]
	public class SetRawImageProperty : Action, IExecutableInEditor
	{
		public enum Property
		{
			Color,
			Maskable,
			Material,
			Texture,
			UvRect
		}

		[SerializeField]
		private RawImage _target;

		[SerializeField]
		private Variable _targetVar;

		[SerializeField]
		private Property _property;

		[SerializeField]
		private Color _colorValue;

		[SerializeField]
		private bool _maskableValue;

		[SerializeField]
		private Material _materialValue;

		[SerializeField]
		private Texture _textureValue;

		[SerializeField]
		private Rect _uvRectValue;

		[SerializeField]
		private Variable _valueVar;

		public RawImage target => _targetVar.GetValueAsComponent(_target, typeof(RawImage)) as RawImage;

		protected override void OnReset()
		{
			_target = base.gameObject.GetComponent<RawImage>();
		}

		public override void Setup()
		{
		}

		public override void Execute()
		{
			RawImage target = this.target;
			if (!(target == null))
			{
				switch (_property)
				{
				case Property.Color:
					target.color = _valueVar.GetValue(_colorValue);
					break;
				case Property.Maskable:
					target.maskable = _valueVar.GetValue(_maskableValue);
					break;
				case Property.Material:
					target.material = (_valueVar.GetValue(_materialValue) as Material);
					break;
				case Property.Texture:
					target.texture = (_valueVar.GetValue(_textureValue) as Texture);
					break;
				case Property.UvRect:
					target.uvRect = _valueVar.GetValue(_uvRectValue);
					break;
				}
			}
		}
	}
}
