using UnityEngine;

namespace GameFlow
{
	[Help("en", "Stops the specified @GameFlow{Timer}.", null)]
	[Help("es", "Detiene el temporizador @GameFlow{Timer} especificado.", null)]
	[AddComponentMenu("")]
	public class StopTimer : Action
	{
		[SerializeField]
		private Timer _target;

		[SerializeField]
		private Variable _targetVar;

		public override void Execute()
		{
			Timer timer = _targetVar.GetValue(_target) as Timer;
			if (timer != null)
			{
				timer.Stop();
			}
		}
	}
}
