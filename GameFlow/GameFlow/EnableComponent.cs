using UnityEngine;

namespace GameFlow
{
	[AddComponentMenu("")]
	[Help("en", "Enables the specified Component.", null)]
	[Help("es", "Habilita el componente especificado.", null)]
	public class EnableComponent : Action, IExecutableInEditor
	{
		[SerializeField]
		private Component _component;

		[SerializeField]
		private Variable _componentVar;

		public override void Execute()
		{
			Execute(_componentVar.GetValueAsComponent(_component, typeof(Component)));
		}

		public static void Execute(Component component)
		{
			if (component == null)
			{
				return;
			}
			Behaviour behaviour = component as Behaviour;
			if (behaviour != null)
			{
				behaviour.enabled = true;
				return;
			}
			Collider collider = component as Collider;
			if (collider != null)
			{
				collider.enabled = true;
				return;
			}
			Renderer renderer = component as Renderer;
			if (renderer != null)
			{
				renderer.enabled = true;
				return;
			}
			Cloth cloth = component as Cloth;
			if (cloth != null)
			{
				cloth.enabled = true;
			}
		}
	}
}
