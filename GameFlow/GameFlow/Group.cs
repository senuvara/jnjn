using System.Collections.Generic;
using UnityEngine;

namespace GameFlow
{
	[Help("es", "Ejecuta todas las acciones activadas y contenidas en el grupo de manera simultánea.", null)]
	[AddComponentMenu("")]
	[Help("en", "Executes all enabled actions in the group simultaneously.", null)]
	public class Group : Action, IActionContainer, IBlockContainer, IExecutableInEditor
	{
		[SerializeField]
		private List<Action> _actions = new List<Action>();

		private bool dirty;

		public List<Action> GetActions(int sectionIndex = 0)
		{
			return _actions;
		}

		public int GetActionCount(int sectionIndex = 0)
		{
			return ActionUtils.GetActionCount(this);
		}

		public int GetActionSectionsCount()
		{
			return 1;
		}

		public void ActionAdded(Action action, int sectionIndex = 0)
		{
		}

		public override void Setup()
		{
			foreach (Action action in _actions)
			{
				ActionExecutor.SetupAction(action);
			}
		}

		public override void FirstStep()
		{
			foreach (Action action in _actions)
			{
				if (action.enabledInEditor)
				{
					ActionExecutor.ExecuteActionFirstStep(action);
				}
			}
		}

		public override void Step()
		{
			foreach (Action action in _actions)
			{
				if (action.enabledInEditor && !ActionExecutor.IsActionFinished(action))
				{
					ActionExecutor.ExecuteActionStep(action);
				}
			}
		}

		public override bool Finished()
		{
			foreach (Action action in _actions)
			{
				if (action.enabledInEditor && !ActionExecutor.IsActionFinished(action))
				{
					return false;
				}
			}
			return true;
		}

		public void Break()
		{
			(container as IActionContainer)?.Break();
		}

		public void SetDirty(bool dirty)
		{
			this.dirty = dirty;
		}

		public bool IsDirty()
		{
			return dirty;
		}

		public bool Contains(Block block)
		{
			Action action = block as Action;
			if (action != null)
			{
				return _actions.Contains(action);
			}
			return false;
		}

		public List<Block> GetBlocks()
		{
			return new List<Block>(_actions.ToArray());
		}
	}
}
