using UnityEngine;

namespace GameFlow
{
	[Help("en", "Sets a property of the specified @UnityManual{AudioChorusFilter} component.", null)]
	[Help("es", "Modifica una propiedad del componente @UnityManual{AudioChorusFilter} especificado.", null)]
	[AddComponentMenu("")]
	public class SetAudioChorusFilterProperty : Action, IExecutableInEditor
	{
		public enum Property
		{
			Delay,
			Depth,
			DryMix,
			Rate,
			WetMix1,
			WetMix2,
			WetMix3
		}

		[SerializeField]
		private AudioChorusFilter _target;

		[SerializeField]
		private Variable _targetVar;

		[SerializeField]
		private Property _property;

		[SerializeField]
		private float _delayValue;

		[SerializeField]
		private float _depthValue;

		[SerializeField]
		private float _dryMixValue;

		[SerializeField]
		private float _rateValue;

		[SerializeField]
		private float _wetMix1Value;

		[SerializeField]
		private float _wetMix2Value;

		[SerializeField]
		private float _wetMix3Value;

		[SerializeField]
		private Variable _valueVar;

		protected override void OnReset()
		{
			_target = base.gameObject.GetComponent<AudioChorusFilter>();
		}

		public override void Setup()
		{
		}

		public override void Execute()
		{
			AudioChorusFilter audioChorusFilter = _targetVar.GetValueAsComponent(_target, typeof(AudioChorusFilter)) as AudioChorusFilter;
			if (!(audioChorusFilter == null))
			{
				switch (_property)
				{
				case Property.Delay:
					audioChorusFilter.delay = _valueVar.GetValue(_delayValue);
					break;
				case Property.Depth:
					audioChorusFilter.depth = _valueVar.GetValue(_depthValue);
					break;
				case Property.DryMix:
					audioChorusFilter.dryMix = _valueVar.GetValue(_dryMixValue);
					break;
				case Property.Rate:
					audioChorusFilter.rate = _valueVar.GetValue(_rateValue);
					break;
				case Property.WetMix1:
					audioChorusFilter.wetMix1 = _valueVar.GetValue(_wetMix1Value);
					break;
				case Property.WetMix2:
					audioChorusFilter.wetMix2 = _valueVar.GetValue(_wetMix2Value);
					break;
				case Property.WetMix3:
					audioChorusFilter.wetMix3 = _valueVar.GetValue(_wetMix3Value);
					break;
				}
			}
		}
	}
}
