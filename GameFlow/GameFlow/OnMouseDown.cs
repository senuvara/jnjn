using UnityEngine;

namespace GameFlow
{
	[DisallowMultipleComponent]
	[AddComponentMenu("GameFlow/Programs/On Mouse Down")]
	[Help("es", "Programa que se ejecutará cuando se pulse cualquier botón del ratón estando su puntero sobre un componente Collider o GUIElement activado en el rango de escucha.", null)]
	[Help("en", "Program that will be executed when any mouse button is pressed over an enabled Collider or GUIElement component in the listening range.", null)]
	public class OnMouseDown : OnMouseProgram
	{
		private EventController[] _controllers;

		protected override void RegisterAsListener()
		{
			_controllers = RegisterAsListener(typeof(MouseDownEvent), typeof(MouseController), MouseController.AddController);
			if (base.listeningRange == EventListeningRange.SceneHierarchy)
			{
				MouseDownEvent.AddListener(this);
			}
		}

		protected override void UnRegisterAsListener()
		{
			MouseDownEvent.RemoveListener(this);
			UnRegisterAsListener(typeof(MouseDownEvent), typeof(MouseController));
		}

		private void OnTransformChildrenChanged()
		{
			if (base.listeningRange == EventListeningRange.GameObjectHierarchy)
			{
				UnsubscribeFrom(_controllers, typeof(MouseDownEvent));
				_controllers = EventController.GetControllersInHierarchyOf(typeof(MouseController), base.listeningTarget, MouseController.AddController);
				SubscribeTo(_controllers, typeof(MouseDownEvent));
			}
		}
	}
}
