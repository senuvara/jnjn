using UnityEngine;

namespace GameFlow
{
	public static class Unity
	{
		private static int[] versionNumbers = VersionUtils.VersionStringToNumbers(Application.unityVersion);

		public static int versionMajor => versionNumbers[0];

		public static int versionMinor => versionNumbers[1];

		public static int versionFix => versionNumbers[2];

		public static int versionInt => versionNumbers[0] * 100 + versionNumbers[1] * 10 + versionNumbers[2];
	}
}
