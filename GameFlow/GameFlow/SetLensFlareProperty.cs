using UnityEngine;

namespace GameFlow
{
	[AddComponentMenu("")]
	[Help("en", "Sets a property of the specified @UnityManual{LensFlare}.", null)]
	[Help("es", "Modifica una propiedad del componente @UnityManual{LensFlare} especificado.", null)]
	public class SetLensFlareProperty : Action, IExecutableInEditor
	{
		public enum Property
		{
			Brightness,
			Color,
			FadeSpeed,
			Flare
		}

		[SerializeField]
		private LensFlare _target;

		[SerializeField]
		private Variable _targetVar;

		[SerializeField]
		private Property _property;

		[SerializeField]
		private float _brightnessValue;

		[SerializeField]
		private Color _colorValue;

		[SerializeField]
		private float _fadeSpeedValue;

		[SerializeField]
		private Flare _flareValue;

		[SerializeField]
		private Variable _valueVar;

		protected override void OnReset()
		{
			_target = base.gameObject.GetComponent<LensFlare>();
		}

		public override void Setup()
		{
		}

		public override void Execute()
		{
			LensFlare lensFlare = _targetVar.GetValueAsComponent(_target, typeof(LensFlare)) as LensFlare;
			if (!(lensFlare == null))
			{
				switch (_property)
				{
				case Property.Brightness:
					lensFlare.brightness = _valueVar.GetValue(_brightnessValue);
					break;
				case Property.Color:
					lensFlare.color = _valueVar.GetValue(_colorValue);
					break;
				case Property.FadeSpeed:
					lensFlare.fadeSpeed = _valueVar.GetValue(_fadeSpeedValue);
					break;
				case Property.Flare:
					lensFlare.flare = (_valueVar.GetValue(_flareValue) as Flare);
					break;
				}
			}
		}
	}
}
