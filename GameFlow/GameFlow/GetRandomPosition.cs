using UnityEngine;

namespace GameFlow
{
	[Help("en", "Gets a random 3D position in the specified space.", null)]
	[AddComponentMenu("")]
	[Help("es", "Devuelve una posición 3D aleatoria en el espacio indicado.", null)]
	public class GetRandomPosition : Action, IExecutableInEditor
	{
		public enum Space
		{
			Local,
			World,
			Sphere,
			SphereSurface
		}

		[SerializeField]
		private Space _space = Space.World;

		[SerializeField]
		private Transform _transform;

		[SerializeField]
		private Variable _transformVar;

		[SerializeField]
		private Vector3 _min = Vector3.zero;

		[SerializeField]
		private Variable _minVar;

		[SerializeField]
		private Vector3 _max = new Vector3(1f, 1f, 1f);

		[SerializeField]
		private Variable _maxVar;

		[SerializeField]
		private Vector3 _center = Vector3.zero;

		[SerializeField]
		private Variable _centerVar;

		[SerializeField]
		private float _radius = 1f;

		[SerializeField]
		private Variable _radiusVar;

		[SerializeField]
		private Variable _output;

		public Vector3 min => _minVar.GetValue(_min);

		public Vector3 max => _maxVar.GetValue(_max);

		public Vector3 center => _centerVar.GetValue(_center);

		public float radius => _radiusVar.GetValue(_radius);

		public override void Execute()
		{
			if (_output == null)
			{
				return;
			}
			switch (_space)
			{
			case Space.Local:
			{
				Vector3 value = RandomUtils.GetRandomVector3(min, max);
				Transform transform = _transformVar.GetValueAsComponent(_transform, typeof(Transform)) as Transform;
				if (transform != null)
				{
					value = transform.position + transform.TransformDirection(value);
				}
				_output.SetValue(value);
				break;
			}
			case Space.World:
			{
				Vector3 value = RandomUtils.GetRandomVector3(min, max);
				_output.SetValue(value);
				break;
			}
			case Space.Sphere:
			{
				Vector3 value = center + Random.insideUnitSphere * radius;
				_output.SetValue(value);
				break;
			}
			case Space.SphereSurface:
			{
				Vector3 value = center + Random.onUnitSphere * radius;
				_output.SetValue(value);
				break;
			}
			}
		}
	}
}
