using UnityEngine;

namespace GameFlow
{
	[Help("en", "Gets the maximum number of objects in the specified @GameFlow{Pool} component.", null)]
	[Help("es", "Devuelve el número máximo de objetos en el @GameFlow{Pool} especificado.", null)]
	[AddComponentMenu("")]
	public class GetPoolCapacity : Action, IExecutableInEditor
	{
		[SerializeField]
		private Pool _pool;

		[SerializeField]
		private Variable _poolVar;

		[SerializeField]
		private Variable _output;

		public override void Execute()
		{
			ExecuteWith(_poolVar.GetValue(_pool) as Pool, _output);
		}

		public static void ExecuteWith(Pool pool, Variable output)
		{
			if (!(pool == null) && !(output == null))
			{
				output.SetValue(pool.capacity);
			}
		}
	}
}
