using UnityEngine;

namespace GameFlow
{
	[Help("es", "Lee una propiedad del componente @UnityManual{PlatformEffector2D} especificado.", null)]
	[AddComponentMenu("")]
	[Help("en", "Gets a property of the specified @UnityManual{PlatformEffector2D} component.", null)]
	public class GetPlatformEffector2DProperty : Action, IExecutableInEditor
	{
		public enum Property
		{
			ColliderMask,
			SideAngleVariance,
			UseColliderMask,
			UseOneWay,
			UseSideBounce,
			UseSideFriction
		}

		[SerializeField]
		private PlatformEffector2D _source;

		[SerializeField]
		private Variable _sourceVar;

		[SerializeField]
		private Property _property;

		[SerializeField]
		private Variable _output;

		protected override void OnReset()
		{
			_source = base.gameObject.GetComponent<PlatformEffector2D>();
		}

		public override void Setup()
		{
		}

		public override void Execute()
		{
			PlatformEffector2D platformEffector2D = _sourceVar.GetValueAsComponent(_source, typeof(PlatformEffector2D)) as PlatformEffector2D;
			if (!(platformEffector2D == null))
			{
				switch (_property)
				{
				case Property.ColliderMask:
					_output.SetValue(platformEffector2D.colliderMask);
					break;
				case Property.SideAngleVariance:
					_output.SetValue(platformEffector2D.sideArc);
					break;
				case Property.UseColliderMask:
					_output.SetValue(platformEffector2D.useColliderMask);
					break;
				case Property.UseOneWay:
					_output.SetValue(platformEffector2D.useOneWay);
					break;
				case Property.UseSideBounce:
					_output.SetValue(platformEffector2D.useSideBounce);
					break;
				case Property.UseSideFriction:
					_output.SetValue(platformEffector2D.useSideFriction);
					break;
				}
			}
		}
	}
}
