using UnityEngine;
using UnityEngine.AI;

namespace GameFlow
{
	[Help("en", "Sets a property of the specified @UnityManual{NavMeshObstacle}.", null)]
	[Help("es", "Modifica una propiedad del componente @UnityManual{NavMeshObstacle} especificado.", null)]
	[AddComponentMenu("")]
	public class SetNavMeshObstacleProperty : Action, IExecutableInEditor
	{
		public enum Property
		{
			CarveOnlyStationary,
			Carving,
			CarvingMoveThreshold,
			CarvingTimeToStationary,
			Center,
			Height,
			Radius,
			Shape,
			Size,
			Velocity
		}

		[SerializeField]
		private NavMeshObstacle _target;

		[SerializeField]
		private Variable _targetVar;

		[SerializeField]
		private Property _property;

		[SerializeField]
		private bool _carveOnlyStationaryValue;

		[SerializeField]
		private bool _carvingValue;

		[SerializeField]
		private float _carvingMoveThresholdValue;

		[SerializeField]
		private float _carvingTimeToStationaryValue;

		[SerializeField]
		private Vector3 _centerValue;

		[SerializeField]
		private float _heightValue;

		[SerializeField]
		private float _radiusValue;

		[SerializeField]
		private NavMeshObstacleShape _shapeValue;

		[SerializeField]
		private Vector3 _sizeValue;

		[SerializeField]
		private Vector3 _velocityValue;

		[SerializeField]
		private Variable _valueVar;

		protected override void OnReset()
		{
			_target = base.gameObject.GetComponent<NavMeshObstacle>();
		}

		public override void Setup()
		{
		}

		public override void Execute()
		{
			NavMeshObstacle navMeshObstacle = _targetVar.GetValueAsComponent(_target, typeof(NavMeshObstacle)) as NavMeshObstacle;
			if (!(navMeshObstacle == null))
			{
				switch (_property)
				{
				case Property.CarveOnlyStationary:
					navMeshObstacle.carveOnlyStationary = _valueVar.GetValue(_carveOnlyStationaryValue);
					break;
				case Property.Carving:
					navMeshObstacle.carving = _valueVar.GetValue(_carvingValue);
					break;
				case Property.CarvingMoveThreshold:
					navMeshObstacle.carvingMoveThreshold = _valueVar.GetValue(_carvingMoveThresholdValue);
					break;
				case Property.CarvingTimeToStationary:
					navMeshObstacle.carvingTimeToStationary = _valueVar.GetValue(_carvingTimeToStationaryValue);
					break;
				case Property.Center:
					navMeshObstacle.center = _valueVar.GetValue(_centerValue);
					break;
				case Property.Height:
					navMeshObstacle.height = _valueVar.GetValue(_heightValue);
					break;
				case Property.Radius:
					navMeshObstacle.radius = _valueVar.GetValue(_radiusValue);
					break;
				case Property.Shape:
					navMeshObstacle.shape = (NavMeshObstacleShape)(object)_valueVar.GetValue(_shapeValue);
					break;
				case Property.Size:
					navMeshObstacle.size = _valueVar.GetValue(_sizeValue);
					break;
				case Property.Velocity:
					navMeshObstacle.velocity = _valueVar.GetValue(_velocityValue);
					break;
				}
			}
		}
	}
}
