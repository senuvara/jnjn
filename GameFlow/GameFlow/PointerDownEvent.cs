using System.Collections.Generic;
using UnityEngine;

namespace GameFlow
{
	public class PointerDownEvent : UIEvent
	{
		private static List<IEventListener> listeners = new List<IEventListener>();

		public Vector2 position;

		public PointerDownEvent(GameObject source)
		{
			base.source = source;
		}

		public override void DispatchToAll()
		{
			EventDispatcher.DispatchEventToListeners(this, listeners);
		}

		public new static void AddListener(IEventListener listener)
		{
			listeners.AddOnlyOnce(listener);
		}

		public new static void RemoveListener(IEventListener listener)
		{
			listeners.Remove(listener);
		}
	}
}
