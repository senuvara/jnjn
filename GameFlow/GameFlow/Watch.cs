using System.Collections.Generic;

namespace GameFlow
{
	public class Watch
	{
		public static Dictionary<string, string> values = new Dictionary<string, string>();

		public static void Log(string key, string value)
		{
			values[key] = value;
		}

		public static void Log(string key, bool value)
		{
			values[key] = ((!value) ? "False" : "True");
		}

		public static void Log(string key, int value)
		{
			values[key] = string.Empty + value;
		}

		public static void Log(string key, float value)
		{
			values[key] = string.Empty + value;
		}
	}
}
