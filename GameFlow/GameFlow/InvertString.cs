using UnityEngine;

namespace GameFlow
{
	[Help("en", "Inverts the text stored in the specified @GameFlow{Variable}.", null)]
	[Help("es", "Invierte el texto guardado en la @GameFlow{Variable} especificada.", null)]
	[AddComponentMenu("")]
	public class InvertString : Action, IExecutableInEditor
	{
		[SerializeField]
		private Variable _variable;

		public override void Execute()
		{
			Execute(_variable);
		}

		public static void Execute(Variable variable)
		{
			if (!(variable == null))
			{
				Variable variable2 = (!variable.isIndirect) ? variable : variable.indirection;
				if (!(variable2 == null))
				{
					variable2.SetValue(variable2.GetValue(string.Empty).Inverted());
				}
			}
		}
	}
}
