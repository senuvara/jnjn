using UnityEngine;

namespace GameFlow
{
	[Help("en", "Notifies the start of the game.", null)]
	[Help("es", "Notifica el comienzo del juego.", null)]
	[AddComponentMenu("")]
	public class StartGame : Action
	{
		private static LocalizedString alreadyStarted = LocalizedString.Create().Add("en", "Ignoring Start Game action: Game already started.").Add("es", "Ignorando acción Start Game: El juego ya está iniciado.");

		public override void Execute()
		{
			if (Game.started)
			{
				Log.Warning(alreadyStarted, base.gameObject);
			}
			Game.Start();
		}
	}
}
