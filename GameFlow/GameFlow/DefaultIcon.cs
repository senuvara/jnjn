using System;

namespace GameFlow
{
	[AttributeUsage(AttributeTargets.Class)]
	public class DefaultIcon : Attribute
	{
	}
}
