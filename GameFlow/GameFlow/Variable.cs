using System;
using System.Collections.Generic;
using UnityEngine;

namespace GameFlow
{
	[AddComponentMenu("GameFlow/Data/Variable")]
	[Help("en", "Defines a variable of the specified type.", null)]
	[Help("es", "Define una variable del tipo especificado.", null)]
	public class Variable : Block, IData, IEventDispatcher, IIdentifiable, IPrivatizable, IVariableFriendly
	{
		[SerializeField]
		private string _id = string.Empty;

		[SerializeField]
		private DataType _dataType;

		[SerializeField]
		private string _typeName = string.Empty;

		private Type _type;

		[SerializeField]
		private string _defaultStringValue = string.Empty;

		[SerializeField]
		private int _defaultIntValue;

		[SerializeField]
		private float _defaultFloatValue;

		[SerializeField]
		private bool _defaultBoolValue;

		[SerializeField]
		private Vector2 _defaultVector2Value;

		[SerializeField]
		private Vector3 _defaultVector3Value;

		[SerializeField]
		private Vector4 _defaultVector4Value;

		[SerializeField]
		private Rect _defaultRectValue;

		[SerializeField]
		private Color _defaultColorValue;

		[SerializeField]
		private int _defaultEnumIntValue;

		[SerializeField]
		private Bounds _defaultBoundsValue;

		[SerializeField]
		private Quaternion _defaultQuaternionValue;

		[SerializeField]
		private string _stringValue = string.Empty;

		[SerializeField]
		private int _intValue;

		[SerializeField]
		private float _floatValue;

		[SerializeField]
		private bool _boolValue = true;

		[SerializeField]
		private Vector2 _vector2Value = Vector2.zero;

		[SerializeField]
		private Vector3 _vector3Value = Vector3.zero;

		[SerializeField]
		private Rect _rectValue = RectExt.zero;

		[SerializeField]
		private Color _colorValue = Color.black;

		[SerializeField]
		private int _r;

		[SerializeField]
		private int _g;

		[SerializeField]
		private int _b;

		[SerializeField]
		private int _a;

		[SerializeField]
		private UnityEngine.Object _objectValue;

		[SerializeField]
		private int _enumIntValue;

		[SerializeField]
		private bool _enumCached;

		private Enum _enumValue;

		[SerializeField]
		private Bounds _boundsValue = new Bounds(Vector3.zero, Vector3.zero);

		[SerializeField]
		private Quaternion _quaternionValue = Quaternion.identity;

		[SerializeField]
		private Vector4 _vector4Value = Vector4.zero;

		[SerializeField]
		private AnimationCurve _animationCurveValue = new AnimationCurve();

		[SerializeField]
		private bool _readOnly;

		[SerializeField]
		private bool _private;

		[SerializeField]
		private bool _persistent;

		[SerializeField]
		private bool _indirect;

		[SerializeField]
		private bool _isVariant;

		[SerializeField]
		private bool _showDescription;

		[TextArea(1, 8)]
		[SerializeField]
		private string _description = "<Description>";

		[SerializeField]
		protected List<UnityEngine.Object> _changeListeners = new List<UnityEngine.Object>();

		private static int _initStamp;

		private int _stamp;

		private static LocalizedString _invalidFormat = LocalizedString.Create().Add("en", "Invalid format.").Add("es", "Formato no válido.");

		public virtual string id
		{
			get
			{
				return (!_indirect) ? _id : ("*" + _id);
			}
			set
			{
				_id = value;
			}
		}

		public DataType dataType
		{
			get
			{
				return _dataType;
			}
			set
			{
				_dataType = value;
				if (_dataType != DataType.Object)
				{
					_indirect = false;
				}
				_isVariant = false;
			}
		}

		public Type type
		{
			get
			{
				switch (_dataType)
				{
				case DataType.Enum:
					if (_type == null || _typeName != _type.FullName)
					{
						_type = TypeUtils.GetType(_typeName);
					}
					if (_type == null)
					{
						_type = typeof(None);
						_typeName = _type.FullName;
					}
					break;
				case DataType.Object:
					if (_type == null || _typeName != _type.FullName)
					{
						_type = TypeUtils.GetType(_typeName);
					}
					if (_type == null)
					{
						_type = typeof(GameObject);
						_typeName = _type.FullName;
					}
					break;
				case DataType.String:
				case DataType.Tag:
					_type = typeof(string);
					break;
				case DataType.Integer:
				case DataType.Layer:
					_type = typeof(int);
					break;
				case DataType.Float:
					_type = typeof(float);
					break;
				case DataType.Boolean:
				case DataType.Toggle:
					_type = typeof(bool);
					break;
				case DataType.Vector2:
					_type = typeof(Vector2);
					break;
				case DataType.Vector3:
					_type = typeof(Vector3);
					break;
				case DataType.Rect:
					_type = typeof(Rect);
					break;
				case DataType.Color:
					_type = typeof(Color);
					break;
				case DataType.Quaternion:
					_type = typeof(Quaternion);
					break;
				case DataType.Bounds:
					_type = typeof(Bounds);
					break;
				case DataType.Vector4:
					_type = typeof(Vector4);
					break;
				case DataType.AnimationCurve:
					_type = typeof(AnimationCurve);
					break;
				default:
					_type = typeof(string);
					break;
				}
				return _type;
			}
			set
			{
				if (value == null)
				{
					return;
				}
				DataType dataType = value.ToDataType();
				if (CheckTypeChange(dataType, value))
				{
					_type = value;
					_typeName = string.Empty;
					_isVariant = false;
					_dataType = dataType;
					DataType dataType2 = _dataType;
					if (dataType2 == DataType.Object || dataType2 == DataType.Enum)
					{
						_typeName = value.FullName;
					}
				}
			}
		}

		public virtual string stringValue
		{
			get
			{
				DoAwake();
				switch (_dataType)
				{
				case DataType.String:
				case DataType.Tag:
					return _stringValue;
				case DataType.Integer:
					return Convert.ToString(intValue);
				case DataType.Float:
					return Convert.ToString(floatValue);
				case DataType.Boolean:
				case DataType.Toggle:
					return Convert.ToString(boolValue);
				case DataType.Vector2:
					return vector2Value.ToString();
				case DataType.Vector3:
					return vector3Value.ToString();
				case DataType.Rect:
					return rectValue.ToString();
				case DataType.Color:
					return colorValue.ToString();
				case DataType.Object:
					if (_indirect)
					{
						Variable variable = objectValue as Variable;
						if (variable != null)
						{
							return variable.stringValue;
						}
					}
					return (!(objectValue != null)) ? "Null" : objectValue.ToString();
				case DataType.Enum:
					return enumValue.ToString();
				case DataType.Layer:
					return (intValue < 0 || intValue >= 32) ? string.Empty : LayerMask.LayerToName(intValue);
				default:
					return string.Empty;
				}
			}
			set
			{
				DoAwake();
				if (!Application.isPlaying)
				{
					this.SetValue(value);
					return;
				}
				if (_indirect)
				{
					Variable variable = objectValue as Variable;
					if (variable != null)
					{
						variable.stringValue = value;
						return;
					}
				}
				if (CheckTypeChange(DataType.String, null))
				{
					_dataType = DataType.String;
					_isVariant = false;
					_stringValue = value.Expanded();
					if (_persistent)
					{
						PlayerPrefs.SetString(_id, _stringValue);
						PlayerPrefs.Save();
					}
					DispatchChangeEvent();
				}
			}
		}

		public virtual int intValue
		{
			get
			{
				//Discarded unreachable code: IL_0064, IL_0071
				DoAwake();
				switch (_dataType)
				{
				case DataType.Integer:
				case DataType.Layer:
					return _intValue;
				case DataType.String:
				case DataType.Tag:
					try
					{
						return Convert.ToInt32(stringValue);
					}
					catch
					{
						return 0;
					}
				case DataType.Float:
					return Convert.ToInt32(floatValue);
				case DataType.Boolean:
				case DataType.Toggle:
					return Convert.ToInt32(boolValue);
				case DataType.Vector2:
				{
					Vector2 vector2Value2 = this.vector2Value;
					return Convert.ToInt32(vector2Value2.x);
				}
				case DataType.Vector3:
				{
					Vector2 vector2Value = this.vector2Value;
					return Convert.ToInt32(vector2Value.x);
				}
				case DataType.Rect:
					return Convert.ToInt32(rectValue.x);
				case DataType.Object:
					if (_indirect)
					{
						Variable variable = objectValue as Variable;
						if (variable != null)
						{
							return variable.intValue;
						}
					}
					break;
				case DataType.Enum:
					return _enumIntValue;
				}
				return 0;
			}
			set
			{
				DoAwake();
				if (!Application.isPlaying)
				{
					this.SetValue(value);
					return;
				}
				if (_indirect)
				{
					Variable variable = objectValue as Variable;
					if (variable != null)
					{
						variable.intValue = value;
						return;
					}
				}
				if (CheckTypeChange(DataType.Integer, null))
				{
					_dataType = DataType.Integer;
					_isVariant = false;
					_intValue = value;
					if (_persistent)
					{
						PlayerPrefs.SetInt(_id, value);
						PlayerPrefs.Save();
					}
					DispatchChangeEvent();
				}
			}
		}

		public virtual float floatValue
		{
			get
			{
				//Discarded unreachable code: IL_0064, IL_0075
				DoAwake();
				switch (_dataType)
				{
				case DataType.Float:
					return _floatValue;
				case DataType.String:
				case DataType.Tag:
					try
					{
						return Convert.ToSingle(stringValue);
					}
					catch
					{
						return 0f;
					}
				case DataType.Integer:
				case DataType.Layer:
					return Convert.ToSingle(intValue);
				case DataType.Boolean:
				case DataType.Toggle:
					return Convert.ToSingle(boolValue);
				case DataType.Vector2:
				{
					Vector2 vector2Value = this.vector2Value;
					return vector2Value.x;
				}
				case DataType.Vector3:
				{
					Vector3 vector3Value = this.vector3Value;
					return vector3Value.x;
				}
				case DataType.Rect:
					return rectValue.x;
				case DataType.Object:
					if (_indirect)
					{
						Variable variable = objectValue as Variable;
						if (variable != null)
						{
							return variable.floatValue;
						}
					}
					break;
				}
				return 0f;
			}
			set
			{
				DoAwake();
				if (!Application.isPlaying)
				{
					this.SetValue(value);
					return;
				}
				if (_indirect)
				{
					Variable variable = objectValue as Variable;
					if (variable != null)
					{
						variable.floatValue = value;
						return;
					}
				}
				if (CheckTypeChange(DataType.Float, null))
				{
					_dataType = DataType.Float;
					_isVariant = false;
					_floatValue = value;
					if (_persistent)
					{
						PlayerPrefs.SetFloat(_id, value);
						PlayerPrefs.Save();
					}
					DispatchChangeEvent();
				}
			}
		}

		public virtual bool boolValue
		{
			get
			{
				DoAwake();
				switch (_dataType)
				{
				case DataType.Boolean:
				case DataType.Toggle:
					return _boolValue;
				case DataType.String:
				case DataType.Tag:
					return stringValue == "True";
				case DataType.Integer:
				case DataType.Layer:
					return Convert.ToBoolean(intValue);
				case DataType.Float:
					return Convert.ToBoolean(floatValue);
				case DataType.Object:
					if (_indirect)
					{
						Variable variable = objectValue as Variable;
						if (variable != null)
						{
							return variable.boolValue;
						}
					}
					break;
				}
				return false;
			}
			set
			{
				DoAwake();
				if (!Application.isPlaying)
				{
					this.SetValue(value);
					return;
				}
				if (_indirect)
				{
					Variable variable = objectValue as Variable;
					if (variable != null)
					{
						variable.boolValue = value;
						return;
					}
				}
				if (CheckTypeChange(DataType.Boolean, null))
				{
					_dataType = DataType.Boolean;
					_isVariant = false;
					_boolValue = value;
					if (_persistent)
					{
						PlayerPrefs.SetString(_id, (!value) ? "False" : "True");
						PlayerPrefs.Save();
					}
					DispatchChangeEvent();
				}
			}
		}

		public virtual Vector2 vector2Value
		{
			get
			{
				//Discarded unreachable code: IL_006e, IL_007f
				DoAwake();
				switch (_dataType)
				{
				case DataType.Vector2:
					return _vector2Value;
				case DataType.String:
				case DataType.Tag:
					try
					{
						return new Vector2(Convert.ToSingle(stringValue), 0f);
					}
					catch
					{
						return Vector2.zero;
					}
				case DataType.Integer:
				case DataType.Layer:
					return new Vector2(intValue, 0f);
				case DataType.Float:
					return new Vector2(floatValue, 0f);
				case DataType.Vector3:
				{
					Vector3 vector3Value = this.vector3Value;
					float x = vector3Value.x;
					Vector3 vector3Value2 = this.vector3Value;
					return new Vector2(x, vector3Value2.y);
				}
				case DataType.Rect:
					return new Vector2(rectValue.x, rectValue.y);
				case DataType.Color:
				{
					Color colorValue = this.colorValue;
					float r = colorValue.r;
					Color colorValue2 = this.colorValue;
					return new Vector2(r, colorValue2.g);
				}
				case DataType.Object:
					if (_indirect)
					{
						Variable variable = objectValue as Variable;
						if (variable != null)
						{
							return variable.vector2Value;
						}
					}
					break;
				}
				return Vector2.zero;
			}
			set
			{
				DoAwake();
				if (!Application.isPlaying)
				{
					this.SetValue(value);
					return;
				}
				if (_indirect)
				{
					Variable variable = objectValue as Variable;
					if (variable != null)
					{
						variable.vector2Value = value;
						return;
					}
				}
				if (CheckTypeChange(DataType.Vector2, null))
				{
					_dataType = DataType.Vector2;
					_isVariant = false;
					_vector2Value = value;
					if (_persistent)
					{
						PlayerPrefs.SetString(_id, value.ToString().ToParseable());
						PlayerPrefs.Save();
					}
					DispatchChangeEvent();
				}
			}
		}

		public virtual Vector3 vector3Value
		{
			get
			{
				//Discarded unreachable code: IL_0073, IL_0084
				DoAwake();
				switch (_dataType)
				{
				case DataType.Vector3:
					return _vector3Value;
				case DataType.String:
				case DataType.Tag:
					try
					{
						return new Vector3(Convert.ToSingle(stringValue), 0f, 0f);
					}
					catch
					{
						return Vector3.zero;
					}
				case DataType.Integer:
				case DataType.Layer:
					return new Vector3(intValue, 0f, 0f);
				case DataType.Float:
					return new Vector3(floatValue, 0f, 0f);
				case DataType.Vector2:
				{
					Vector2 vector2Value = this.vector2Value;
					float x = vector2Value.x;
					Vector2 vector2Value2 = this.vector2Value;
					return new Vector3(x, vector2Value2.y, 0f);
				}
				case DataType.Rect:
					return new Vector3(rectValue.x, rectValue.y, rectValue.width);
				case DataType.Color:
				{
					Color colorValue = this.colorValue;
					float r = colorValue.r;
					Color colorValue2 = this.colorValue;
					float g = colorValue2.g;
					Color colorValue3 = this.colorValue;
					return new Vector3(r, g, colorValue3.b);
				}
				case DataType.Object:
					if (_indirect)
					{
						Variable variable = objectValue as Variable;
						if (variable != null)
						{
							return variable.vector3Value;
						}
					}
					break;
				}
				return Vector3.zero;
			}
			set
			{
				DoAwake();
				if (!Application.isPlaying)
				{
					this.SetValue(value);
					return;
				}
				if (_indirect)
				{
					Variable variable = objectValue as Variable;
					if (variable != null)
					{
						variable.vector3Value = value;
						return;
					}
				}
				if (CheckTypeChange(DataType.Vector3, null))
				{
					_dataType = DataType.Vector3;
					_isVariant = false;
					_vector3Value = value;
					if (_persistent)
					{
						PlayerPrefs.SetString(_id, value.ToString().ToParseable());
						PlayerPrefs.Save();
					}
					DispatchChangeEvent();
				}
			}
		}

		public virtual Rect rectValue
		{
			get
			{
				//Discarded unreachable code: IL_0078, IL_0089
				DoAwake();
				switch (_dataType)
				{
				case DataType.Rect:
					return _rectValue;
				case DataType.String:
				case DataType.Tag:
					try
					{
						return new Rect(Convert.ToSingle(stringValue), 0f, 0f, 0f);
					}
					catch
					{
						return RectExt.zero;
					}
				case DataType.Integer:
				case DataType.Layer:
					return new Rect(intValue, 0f, 0f, 0f);
				case DataType.Float:
					return new Rect(floatValue, 0f, 0f, 0f);
				case DataType.Vector2:
				{
					Vector2 vector2Value = this.vector2Value;
					float x2 = vector2Value.x;
					Vector2 vector2Value2 = this.vector2Value;
					return new Rect(x2, vector2Value2.y, 0f, 0f);
				}
				case DataType.Vector3:
				{
					Vector3 vector3Value = this.vector3Value;
					float x = vector3Value.x;
					Vector3 vector3Value2 = this.vector3Value;
					float y = vector3Value2.y;
					Vector3 vector3Value3 = this.vector3Value;
					return new Rect(x, y, vector3Value3.z, 0f);
				}
				case DataType.Object:
					if (_indirect)
					{
						Variable variable = objectValue as Variable;
						if (variable != null)
						{
							return variable.rectValue;
						}
					}
					break;
				}
				return RectExt.zero;
			}
			set
			{
				DoAwake();
				if (!Application.isPlaying)
				{
					this.SetValue(value);
					return;
				}
				if (_indirect)
				{
					Variable variable = objectValue as Variable;
					if (variable != null)
					{
						variable.rectValue = value;
						return;
					}
				}
				if (CheckTypeChange(DataType.Rect, null))
				{
					_dataType = DataType.Rect;
					_isVariant = false;
					_rectValue = value;
					if (_persistent)
					{
						PlayerPrefs.SetString(_id, value.ToString().ToParseable());
						PlayerPrefs.Save();
					}
					DispatchChangeEvent();
				}
			}
		}

		public virtual Color colorValue
		{
			get
			{
				DoAwake();
				switch (_dataType)
				{
				case DataType.Color:
					return _colorValue;
				case DataType.Integer:
				case DataType.Layer:
					return new Color(intValue, 0f, 0f);
				case DataType.Float:
					return new Color(floatValue, 0f, 0f);
				case DataType.Vector2:
				{
					Vector2 vector2Value = this.vector2Value;
					float x2 = vector2Value.x;
					Vector2 vector2Value2 = this.vector2Value;
					return new Color(x2, vector2Value2.y, 0f);
				}
				case DataType.Vector3:
				{
					Vector3 vector3Value = this.vector3Value;
					float x = vector3Value.x;
					Vector3 vector3Value2 = this.vector3Value;
					float y = vector3Value2.y;
					Vector3 vector3Value3 = this.vector3Value;
					return new Color(x, y, vector3Value3.z);
				}
				case DataType.Object:
					if (_indirect)
					{
						Variable variable = objectValue as Variable;
						if (variable != null)
						{
							return variable.colorValue;
						}
					}
					break;
				}
				return Color.black;
			}
			set
			{
				DoAwake();
				if (!Application.isPlaying)
				{
					this.SetValue(value);
					return;
				}
				if (_indirect)
				{
					Variable variable = objectValue as Variable;
					if (variable != null)
					{
						variable.colorValue = value;
						return;
					}
				}
				if (CheckTypeChange(DataType.Color, null))
				{
					_dataType = DataType.Color;
					_isVariant = false;
					_colorValue = value;
					if (_persistent)
					{
						PlayerPrefs.SetString(_id, value.ToString().ToParseable());
						PlayerPrefs.Save();
					}
					DispatchChangeEvent();
				}
			}
		}

		public virtual UnityEngine.Object objectValue
		{
			get
			{
				DataType dataType = _dataType;
				if (dataType == DataType.Object)
				{
					return _objectValue;
				}
				return null;
			}
			set
			{
				if (!Application.isPlaying)
				{
					this.SetValue(value);
				}
				else if (CheckTypeChange(DataType.Object, (!(value != null)) ? null : value.GetType()))
				{
					_dataType = DataType.Object;
					_isVariant = false;
					_objectValue = value;
					DispatchChangeEvent();
				}
			}
		}

		public virtual Enum enumValue
		{
			get
			{
				DoAwake();
				switch (_dataType)
				{
				case DataType.Enum:
					if (!_enumCached || _enumValue == null)
					{
						_enumValue = (Enum)Enum.ToObject(type, _enumIntValue);
						_enumCached = true;
					}
					return _enumValue;
				case DataType.Object:
					if (_indirect)
					{
						Variable variable = objectValue as Variable;
						if (variable != null)
						{
							return variable.enumValue;
						}
					}
					break;
				}
				return None.None;
			}
			set
			{
				DoAwake();
				if (!Application.isPlaying)
				{
					this.SetValue(value);
					return;
				}
				if (_indirect)
				{
					Variable variable = objectValue as Variable;
					if (variable != null)
					{
						variable.enumValue = value;
						return;
					}
				}
				if (CheckTypeChange(DataType.Enum, value?.GetType()))
				{
					_dataType = DataType.Enum;
					_isVariant = false;
					_enumIntValue = Convert.ToInt32(value);
					type = value.GetType();
					_enumValue = (Enum)Enum.ToObject(type, _enumIntValue);
					_enumCached = true;
					if (_persistent)
					{
						PlayerPrefs.SetInt(_id, _enumIntValue);
						PlayerPrefs.Save();
					}
					DispatchChangeEvent();
				}
			}
		}

		public virtual bool toggleValue
		{
			get
			{
				return boolValue;
			}
			set
			{
				DoAwake();
				if (!Application.isPlaying)
				{
					this.SetToggleValue(value);
					return;
				}
				if (_indirect)
				{
					Variable variable = objectValue as Variable;
					if (variable != null)
					{
						variable.toggleValue = value;
						return;
					}
				}
				if (CheckTypeChange(DataType.Toggle, null))
				{
					_dataType = DataType.Toggle;
					_isVariant = false;
					_boolValue = value;
					if (_persistent)
					{
						PlayerPrefs.SetString(_id, (!value) ? "False" : "True");
						PlayerPrefs.Save();
					}
					DispatchChangeEvent();
				}
			}
		}

		public virtual string tagValue
		{
			get
			{
				return stringValue;
			}
			set
			{
				DoAwake();
				if (!Application.isPlaying)
				{
					this.SetTagValue(value);
					return;
				}
				if (_indirect)
				{
					Variable variable = objectValue as Variable;
					if (variable != null)
					{
						variable.tagValue = value;
						return;
					}
				}
				if (CheckTypeChange(DataType.Tag, null))
				{
					_dataType = DataType.Tag;
					_isVariant = false;
					_stringValue = value;
					if (_persistent)
					{
						PlayerPrefs.SetString(_id, value);
						PlayerPrefs.Save();
					}
					DispatchChangeEvent();
				}
			}
		}

		public virtual int layerValue
		{
			get
			{
				return intValue;
			}
			set
			{
				DoAwake();
				if (!Application.isPlaying)
				{
					this.SetLayerValue(value);
					return;
				}
				if (_indirect)
				{
					Variable variable = objectValue as Variable;
					if (variable != null)
					{
						variable.layerValue = value;
						return;
					}
				}
				if (CheckTypeChange(DataType.Layer, null))
				{
					_dataType = DataType.Layer;
					_isVariant = false;
					_intValue = value;
					if (_persistent)
					{
						PlayerPrefs.SetInt(_id, value);
						PlayerPrefs.Save();
					}
					DispatchChangeEvent();
				}
			}
		}

		public virtual Bounds boundsValue
		{
			get
			{
				DoAwake();
				switch (_dataType)
				{
				case DataType.Bounds:
					return _boundsValue;
				case DataType.Vector3:
					return new Bounds(vector3Value, Vector3.zero);
				case DataType.Object:
					if (_indirect)
					{
						Variable variable = objectValue as Variable;
						if (variable != null)
						{
							return variable.boundsValue;
						}
					}
					break;
				}
				return new Bounds(Vector3.zero, Vector3.zero);
			}
			set
			{
				DoAwake();
				if (!Application.isPlaying)
				{
					this.SetValue(value);
					return;
				}
				if (_indirect)
				{
					Variable variable = objectValue as Variable;
					if (variable != null)
					{
						variable.boundsValue = value;
						return;
					}
				}
				if (CheckTypeChange(DataType.Bounds, null))
				{
					_dataType = DataType.Bounds;
					_isVariant = false;
					_boundsValue = value;
					if (_persistent)
					{
						PlayerPrefs.SetString(_id, value.ToString().ToParseable());
						PlayerPrefs.Save();
					}
					DispatchChangeEvent();
				}
			}
		}

		public virtual Quaternion quaternionValue
		{
			get
			{
				//Discarded unreachable code: IL_0084, IL_0095
				DoAwake();
				switch (_dataType)
				{
				case DataType.Quaternion:
					return _quaternionValue;
				case DataType.String:
				case DataType.Tag:
					try
					{
						return new Quaternion(Convert.ToSingle(stringValue), 0f, 0f, 0f);
					}
					catch
					{
						return Quaternion.identity;
					}
				case DataType.Integer:
				case DataType.Layer:
					return new Quaternion(intValue, 0f, 0f, 0f);
				case DataType.Float:
					return new Quaternion(floatValue, 0f, 0f, 0f);
				case DataType.Vector2:
				{
					Vector2 vector2Value = this.vector2Value;
					float x3 = vector2Value.x;
					Vector2 vector2Value2 = this.vector2Value;
					return new Quaternion(x3, vector2Value2.y, 0f, 0f);
				}
				case DataType.Vector3:
				{
					Vector3 vector3Value = this.vector3Value;
					float x2 = vector3Value.x;
					Vector3 vector3Value2 = this.vector3Value;
					float y2 = vector3Value2.y;
					Vector3 vector3Value3 = this.vector3Value;
					return new Quaternion(x2, y2, vector3Value3.z, 0f);
				}
				case DataType.Vector4:
				{
					Vector4 vector4Value = this.vector4Value;
					float x = vector4Value.x;
					Vector4 vector4Value2 = this.vector4Value;
					float y = vector4Value2.y;
					Vector4 vector4Value3 = this.vector4Value;
					float z = vector4Value3.z;
					Vector4 vector4Value4 = this.vector4Value;
					return new Quaternion(x, y, z, vector4Value4.w);
				}
				case DataType.Rect:
					return new Quaternion(rectValue.x, rectValue.y, rectValue.width, rectValue.height);
				case DataType.Color:
				{
					Color colorValue = this.colorValue;
					float r = colorValue.r;
					Color colorValue2 = this.colorValue;
					float g = colorValue2.g;
					Color colorValue3 = this.colorValue;
					float b = colorValue3.b;
					Color colorValue4 = this.colorValue;
					return new Quaternion(r, g, b, colorValue4.a);
				}
				case DataType.Object:
					if (_indirect)
					{
						Variable variable = objectValue as Variable;
						if (variable != null)
						{
							return variable.quaternionValue;
						}
					}
					break;
				}
				return Quaternion.identity;
			}
			set
			{
				DoAwake();
				if (!Application.isPlaying)
				{
					this.SetValue(value);
					return;
				}
				if (_indirect)
				{
					Variable variable = objectValue as Variable;
					if (variable != null)
					{
						variable.quaternionValue = value;
						return;
					}
				}
				if (CheckTypeChange(DataType.Quaternion, null))
				{
					_dataType = DataType.Quaternion;
					_isVariant = false;
					_quaternionValue = value;
					if (_persistent)
					{
						PlayerPrefs.SetString(_id, value.ToString().ToParseable());
						PlayerPrefs.Save();
					}
					DispatchChangeEvent();
				}
			}
		}

		public virtual Vector4 vector4Value
		{
			get
			{
				//Discarded unreachable code: IL_0084, IL_0095
				DoAwake();
				switch (_dataType)
				{
				case DataType.Vector4:
					return _vector4Value;
				case DataType.String:
				case DataType.Tag:
					try
					{
						return new Vector4(Convert.ToSingle(stringValue), 0f, 0f, 0f);
					}
					catch
					{
						return Vector4.zero;
					}
				case DataType.Integer:
				case DataType.Layer:
					return new Vector4(intValue, 0f, 0f, 0f);
				case DataType.Float:
					return new Vector4(floatValue, 0f, 0f, 0f);
				case DataType.Vector2:
				{
					Vector2 vector2Value = this.vector2Value;
					float x3 = vector2Value.x;
					Vector2 vector2Value2 = this.vector2Value;
					return new Vector4(x3, vector2Value2.y, 0f, 0f);
				}
				case DataType.Vector3:
				{
					Vector3 vector3Value = this.vector3Value;
					float x2 = vector3Value.x;
					Vector3 vector3Value2 = this.vector3Value;
					float y2 = vector3Value2.y;
					Vector3 vector3Value3 = this.vector3Value;
					return new Vector4(x2, y2, vector3Value3.z, 0f);
				}
				case DataType.Rect:
					return new Vector4(rectValue.x, rectValue.y, rectValue.width, rectValue.height);
				case DataType.Color:
				{
					Color colorValue = this.colorValue;
					float r = colorValue.r;
					Color colorValue2 = this.colorValue;
					float g = colorValue2.g;
					Color colorValue3 = this.colorValue;
					float b = colorValue3.b;
					Color colorValue4 = this.colorValue;
					return new Vector4(r, g, b, colorValue4.a);
				}
				case DataType.Quaternion:
				{
					Quaternion quaternionValue = this.quaternionValue;
					float x = quaternionValue.x;
					Quaternion quaternionValue2 = this.quaternionValue;
					float y = quaternionValue2.y;
					Quaternion quaternionValue3 = this.quaternionValue;
					float z = quaternionValue3.z;
					Quaternion quaternionValue4 = this.quaternionValue;
					return new Vector4(x, y, z, quaternionValue4.w);
				}
				case DataType.Object:
					if (_indirect)
					{
						Variable variable = objectValue as Variable;
						if (variable != null)
						{
							return variable.vector4Value;
						}
					}
					break;
				}
				return Vector4.zero;
			}
			set
			{
				DoAwake();
				if (!Application.isPlaying)
				{
					this.SetValue(value);
					return;
				}
				if (_indirect)
				{
					Variable variable = objectValue as Variable;
					if (variable != null)
					{
						variable.vector4Value = value;
						return;
					}
				}
				if (CheckTypeChange(DataType.Vector4, null))
				{
					_dataType = DataType.Vector4;
					_isVariant = false;
					_vector4Value = value;
					if (_persistent)
					{
						PlayerPrefs.SetString(_id, value.ToString().ToParseable());
						PlayerPrefs.Save();
					}
					DispatchChangeEvent();
				}
			}
		}

		public virtual AnimationCurve animationCurveValue
		{
			get
			{
				switch (_dataType)
				{
				case DataType.AnimationCurve:
					return _animationCurveValue;
				case DataType.Object:
					if (_indirect)
					{
						Variable variable = objectValue as Variable;
						if (variable != null)
						{
							return variable.animationCurveValue;
						}
					}
					break;
				}
				return new AnimationCurve();
			}
			set
			{
				if (!Application.isPlaying)
				{
					this.SetValue(value);
					return;
				}
				if (_indirect)
				{
					Variable variable = objectValue as Variable;
					if (variable != null)
					{
						variable.animationCurveValue = value;
						return;
					}
				}
				if (CheckTypeChange(DataType.AnimationCurve, null))
				{
					_dataType = DataType.AnimationCurve;
					_isVariant = false;
					_animationCurveValue = value;
					DispatchChangeEvent();
				}
			}
		}

		public virtual bool isReadOnly => _readOnly;

		public bool isPrivate
		{
			get
			{
				return _private;
			}
			set
			{
				_private = value;
			}
		}

		public bool isPersistent => _persistent;

		public bool isIndirect => _indirect;

		public Variable indirection => (!_indirect) ? null : (objectValue as Variable);

		public bool isVariant
		{
			get
			{
				return _isVariant;
			}
			set
			{
				_isVariant = value;
			}
		}

		public string description
		{
			get
			{
				return _description;
			}
			set
			{
				_description = value;
				_showDescription = true;
			}
		}

		private bool CheckTypeChange(DataType newDataType, Type newType)
		{
			bool flag = true;
			if (_persistent)
			{
				if (newDataType != _dataType)
				{
					DataType dataType = _dataType;
					flag = ((dataType == DataType.Boolean || dataType == DataType.Toggle) && (newDataType == DataType.Boolean || newDataType == DataType.Toggle));
				}
				else
				{
					DataType dataType = _dataType;
					if (dataType == DataType.Object || dataType == DataType.Enum)
					{
						flag = (newType == _type);
					}
				}
				if (!flag)
				{
					Log.Warning("[" + base.gameObject.name + "<" + _id + ">] Variable value assignment failed : The Type of a persistent Variable should not be changed.");
				}
			}
			return flag;
		}

		private static int GetSessionStamp()
		{
			if (_initStamp == 0)
			{
				_initStamp = (int)Time.realtimeSinceStartup;
			}
			return _initStamp;
		}

		private void Reset()
		{
			HideInInspector();
			Disable();
			SetContainer();
			SetResetFlag();
			OnReset();
		}

		protected override void OnReset()
		{
			if (_id == string.Empty)
			{
				_id = GetDefaultId();
			}
		}

		public virtual string GetDefaultId()
		{
			return "Variable" + UnityEngine.Random.Range(1000, 9999);
		}

		private void DoAwake()
		{
			if (_stamp != GetSessionStamp())
			{
				if (_persistent && !PlayerPrefs.HasKey(_id))
				{
					ResetValue();
				}
				Load();
				_stamp = GetSessionStamp();
			}
		}

		private void SetContainer()
		{
			if (container != null || !BlockInsertion.enabled || BlockInsertion.clipboard)
			{
				return;
			}
			IVariableContainer variableContainer = BlockInsertion.target as IVariableContainer;
			if (variableContainer == null)
			{
				variableContainer = base.gameObject.GetComponent<Variables>();
				if (variableContainer == null)
				{
					variableContainer = base.gameObject.AddComponent<Variables>();
				}
				BlockInsertion.index = -1;
			}
			if (BlockInsertion.index < 0 || BlockInsertion.index >= variableContainer.GetVariables().Count)
			{
				if (!variableContainer.GetVariables().Contains(this))
				{
					variableContainer.GetVariables().Add(this);
					variableContainer.VariableAdded(this);
					variableContainer.SetDirty(dirty: true);
				}
			}
			else
			{
				variableContainer.GetVariables().Insert(BlockInsertion.index, this);
				variableContainer.VariableAdded(this);
				variableContainer.SetDirty(dirty: true);
			}
			container = (variableContainer as UnityEngine.Object);
			BlockInsertion.Reset();
		}

		public object ToSystemObject()
		{
			switch (_dataType)
			{
			case DataType.String:
			case DataType.Tag:
				return _stringValue;
			case DataType.Integer:
			case DataType.Layer:
				return _intValue;
			case DataType.Float:
				return _floatValue;
			case DataType.Boolean:
			case DataType.Toggle:
				return _boolValue;
			case DataType.Vector2:
				return _vector2Value;
			case DataType.Vector3:
				return _vector3Value;
			case DataType.Rect:
				return _rectValue;
			case DataType.Color:
				return _colorValue;
			case DataType.Enum:
				return _enumValue;
			case DataType.Object:
				return _objectValue;
			case DataType.Bounds:
				return _boundsValue;
			case DataType.Quaternion:
				return _quaternionValue;
			case DataType.Vector4:
				return _vector4Value;
			case DataType.AnimationCurve:
				return _animationCurveValue;
			default:
				return null;
			}
		}

		public string GetFormattedStringValue(string format, UnityEngine.Object context = null)
		{
			//Discarded unreachable code: IL_0229
			DoAwake();
			if (format == null || format.Length == 0)
			{
				return stringValue;
			}
			try
			{
				switch (_dataType)
				{
				case DataType.String:
				case DataType.Tag:
					return string.Format(format, _stringValue);
				case DataType.Integer:
					return string.Format(format, intValue);
				case DataType.Float:
					return string.Format(format, floatValue);
				case DataType.Boolean:
				case DataType.Toggle:
					return string.Format(format, boolValue);
				case DataType.Vector2:
					return string.Format(format, vector2Value);
				case DataType.Vector3:
					return string.Format(format, vector3Value);
				case DataType.Rect:
					return string.Format(format, rectValue);
				case DataType.Color:
					return string.Format(format, colorValue);
				case DataType.Object:
					if (_indirect)
					{
						Variable variable = objectValue as Variable;
						if (variable != null)
						{
							return variable.GetFormattedStringValue(format);
						}
					}
					return string.Format(format, objectValue);
				case DataType.Enum:
					return string.Format(format, enumValue);
				case DataType.Layer:
					return string.Format(format, (intValue < 0 || intValue >= 32) ? string.Empty : LayerMask.LayerToName(intValue));
				case DataType.Bounds:
					return string.Format(format, boundsValue);
				case DataType.Quaternion:
					return string.Format(format, quaternionValue);
				case DataType.Vector4:
					return string.Format(format, vector4Value);
				case DataType.AnimationCurve:
					return string.Format(format, animationCurveValue);
				}
			}
			catch
			{
				Log.Warning(_invalidFormat, context);
				return string.Empty;
			}
			return string.Format(format, string.Empty);
		}

		public virtual string GetIdForControls()
		{
			return id;
		}

		public virtual string GetIdForSelectors()
		{
			return id;
		}

		public virtual string GetTypeForSelectors()
		{
			return "Variable";
		}

		public void ResetValue()
		{
			if (_persistent)
			{
				switch (_dataType)
				{
				case DataType.Object:
					break;
				case DataType.String:
				case DataType.Tag:
					PlayerPrefs.SetString(_id, _defaultStringValue);
					break;
				case DataType.Integer:
				case DataType.Layer:
					PlayerPrefs.SetInt(_id, _defaultIntValue);
					break;
				case DataType.Float:
					PlayerPrefs.SetFloat(_id, _defaultFloatValue);
					break;
				case DataType.Boolean:
				case DataType.Toggle:
					PlayerPrefs.SetString(_id, (!_defaultBoolValue) ? "False" : "True");
					break;
				case DataType.Vector2:
					PlayerPrefs.SetString(_id, _defaultVector2Value.ToString().ToParseable());
					break;
				case DataType.Vector3:
					PlayerPrefs.SetString(_id, _defaultVector3Value.ToString().ToParseable());
					break;
				case DataType.Vector4:
					PlayerPrefs.SetString(_id, _defaultVector4Value.ToString().ToParseable());
					break;
				case DataType.Rect:
					PlayerPrefs.SetString(_id, _defaultRectValue.ToString().ToParseable());
					break;
				case DataType.Color:
					PlayerPrefs.SetString(_id, _defaultColorValue.ToString().ToParseable());
					break;
				case DataType.Enum:
					PlayerPrefs.SetInt(_id, _defaultEnumIntValue);
					break;
				case DataType.Bounds:
					PlayerPrefs.SetString(_id, _defaultBoundsValue.ToString().ToParseable());
					break;
				case DataType.Quaternion:
					PlayerPrefs.SetString(_id, _defaultQuaternionValue.ToString().ToParseable());
					break;
				}
			}
		}

		public void Load()
		{
			if (_persistent)
			{
				switch (_dataType)
				{
				case DataType.Object:
					break;
				case DataType.String:
				case DataType.Tag:
					_stringValue = PlayerPrefs.GetString(_id, _defaultStringValue);
					break;
				case DataType.Integer:
				case DataType.Layer:
					_intValue = PlayerPrefs.GetInt(_id, _defaultIntValue);
					break;
				case DataType.Float:
					_floatValue = PlayerPrefs.GetFloat(_id, _defaultFloatValue);
					break;
				case DataType.Boolean:
				case DataType.Toggle:
					_boolValue = (PlayerPrefs.GetString(_id, (!_defaultBoolValue) ? "False" : "True") == "True");
					break;
				case DataType.Vector2:
					_vector2Value = ParsingUtils.ParseVector2(PlayerPrefs.GetString(_id, _defaultVector2Value.ToString().ToParseable()));
					break;
				case DataType.Vector3:
					_vector3Value = ParsingUtils.ParseVector3(PlayerPrefs.GetString(_id, _defaultVector3Value.ToString().ToParseable()));
					break;
				case DataType.Vector4:
					_vector4Value = ParsingUtils.ParseVector4(PlayerPrefs.GetString(_id, _defaultVector4Value.ToString().ToParseable()));
					break;
				case DataType.Rect:
					_rectValue = ParsingUtils.ParseRect(PlayerPrefs.GetString(_id, _defaultRectValue.ToString().ToParseable()));
					break;
				case DataType.Color:
					_colorValue = ParsingUtils.ParseColor(PlayerPrefs.GetString(_id, _defaultColorValue.ToString().ToParseable()));
					break;
				case DataType.Enum:
					_defaultEnumIntValue = PlayerPrefs.GetInt(_id, _enumIntValue);
					break;
				case DataType.Bounds:
					_boundsValue = ParsingUtils.ParseBounds(PlayerPrefs.GetString(_id, _defaultBoundsValue.ToString().ToParseable()));
					break;
				case DataType.Quaternion:
					_quaternionValue = ParsingUtils.ParseQuaternion(PlayerPrefs.GetString(_id, _defaultQuaternionValue.ToString().ToParseable()));
					break;
				}
			}
		}

		public virtual void AddListener(IEventListener listener, Type eventType)
		{
			if (eventType == typeof(VariableChangeEvent))
			{
				_changeListeners.AddOnlyOnce((UnityEngine.Object)listener);
			}
		}

		public virtual void AddListener(IEventListener listener)
		{
			AddListener(listener, typeof(VariableChangeEvent));
		}

		public virtual void RemoveListener(IEventListener listener, Type eventType)
		{
			if (eventType == typeof(VariableChangeEvent))
			{
				_changeListeners.Remove((UnityEngine.Object)listener);
			}
		}

		public virtual void RemoveListener(IEventListener listener)
		{
			RemoveListener(listener, typeof(VariableChangeEvent));
		}

		public virtual void DispatchChangeEvent()
		{
			VariableChangeEvent variableChangeEvent = new VariableChangeEvent();
			variableChangeEvent.variable = this;
			variableChangeEvent.DispatchTo(_changeListeners);
		}

		private void OnValidate()
		{
			HideInInspector();
		}

		public virtual bool IsPrivate()
		{
			return isPrivate;
		}

		public virtual UnityEngine.Object GetPrivatizer()
		{
			return container;
		}

		public override string ToString()
		{
			return $"{base.gameObject.name}<{_id}> ({GetType().FullName})";
		}

		public void MakeReadOnly()
		{
			_readOnly = true;
		}

		public void MakeIndirect()
		{
			_indirect = true;
		}

		public bool IsNumeric()
		{
			return _dataType == DataType.Integer || _dataType == DataType.Float;
		}
	}
}
