using UnityEngine;

namespace GameFlow
{
	[DisallowMultipleComponent]
	[AddComponentMenu("GameFlow/Programs/On Trigger Exit 2D")]
	[Help("en", "Program that will be executed on time as soon as a Collider 2D component configured as trigger in the listening range stops being invaded by another component.", null)]
	[Help("es", "Programa que se ejecutará en el primer fotograma que un componente Collider 2D configurado como disparador (trigger) en el rango de escucha deje de estar invadido por otro componente.", null)]
	public class OnTriggerExit2D : OnTriggerProgram2D
	{
		private EventController[] _controllers;

		protected override void RegisterAsListener()
		{
			_controllers = RegisterAsListener(typeof(TriggerExitEvent), typeof(TriggerController2D), TriggerController2D.AddController);
			if (base.listeningRange == EventListeningRange.SceneHierarchy)
			{
				TriggerExitEvent.AddListener(this);
			}
		}

		protected override void UnRegisterAsListener()
		{
			TriggerExitEvent.RemoveListener(this);
			UnRegisterAsListener(typeof(TriggerExitEvent), typeof(TriggerController2D));
		}

		private void OnTransformChildrenChanged()
		{
			if (base.listeningRange == EventListeningRange.GameObjectHierarchy)
			{
				UnsubscribeFrom(_controllers, typeof(TriggerExitEvent));
				_controllers = EventController.GetControllersInHierarchyOf(typeof(TriggerController2D), base.listeningTarget, TriggerController2D.AddController);
				SubscribeTo(_controllers, typeof(TriggerExitEvent));
			}
		}
	}
}
