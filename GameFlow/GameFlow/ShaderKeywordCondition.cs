using UnityEngine;

namespace GameFlow
{
	[Help("en", "Evaluates whether the specified Shader keyword is enabled.", null)]
	[AddComponentMenu("")]
	[Help("es", "Evalúa si la palabra clave de Shader especificada está habilitada.", null)]
	public class ShaderKeywordCondition : Condition
	{
		public enum Comparison
		{
			IsEnabled,
			IsNotEnabled
		}

		public enum Scope
		{
			Global,
			Material
		}

		[SerializeField]
		private string _keyword;

		[SerializeField]
		private Variable _keywordVar;

		[SerializeField]
		private Comparison _comparison;

		[SerializeField]
		private Scope _scope;

		[SerializeField]
		private Material _material;

		[SerializeField]
		private Variable _materialVar;

		public override bool Evaluate()
		{
			string value = _keywordVar.GetValue(_keyword);
			if (value == null || value.Length == 0)
			{
				return false;
			}
			switch (_scope)
			{
			case Scope.Global:
				switch (_comparison)
				{
				case Comparison.IsEnabled:
					return Shader.IsKeywordEnabled(value);
				case Comparison.IsNotEnabled:
					return !Shader.IsKeywordEnabled(value);
				}
				break;
			case Scope.Material:
			{
				Material material = _materialVar.GetValue(_material) as Material;
				if (material == null)
				{
					return false;
				}
				switch (_comparison)
				{
				case Comparison.IsEnabled:
					return material.IsKeywordEnabled(value);
				case Comparison.IsNotEnabled:
					return !material.IsKeywordEnabled(value);
				}
				break;
			}
			}
			return false;
		}
	}
}
