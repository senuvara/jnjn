using UnityEngine;

namespace GameFlow
{
	[Help("es", "Programa que se ejecutará cuando el puntero del ratón entre en el área de un componente Collider o GUIElement activado en el rango de escucha.", null)]
	[Help("en", "Program that will be executed when the mouse enters the area of an enabled Collider or GUIElement component in the listening range.", null)]
	[DisallowMultipleComponent]
	[AddComponentMenu("GameFlow/Programs/On Mouse Enter")]
	public class OnMouseEnter : OnMouseProgram
	{
		private EventController[] _controllers;

		protected override void RegisterAsListener()
		{
			_controllers = RegisterAsListener(typeof(MouseEnterEvent), typeof(MouseController), MouseController.AddController);
			if (base.listeningRange == EventListeningRange.SceneHierarchy)
			{
				MouseEnterEvent.AddListener(this);
			}
		}

		protected override void UnRegisterAsListener()
		{
			MouseEnterEvent.RemoveListener(this);
			UnRegisterAsListener(typeof(MouseEnterEvent), typeof(MouseController));
		}

		private void OnTransformChildrenChanged()
		{
			if (base.listeningRange == EventListeningRange.GameObjectHierarchy)
			{
				UnsubscribeFrom(_controllers, typeof(MouseEnterEvent));
				_controllers = EventController.GetControllersInHierarchyOf(typeof(MouseController), base.listeningTarget, MouseController.AddController);
				SubscribeTo(_controllers, typeof(MouseEnterEvent));
			}
		}
	}
}
