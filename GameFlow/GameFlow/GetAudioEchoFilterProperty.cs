using UnityEngine;

namespace GameFlow
{
	[AddComponentMenu("")]
	[Help("en", "Gets a property of the specified @UnityManual{AudioEchoFilter} component.", null)]
	[Help("es", "Lee una propiedad del componente @UnityManual{AudioEchoFilter} especificado.", null)]
	public class GetAudioEchoFilterProperty : Action, IExecutableInEditor
	{
		public enum Property
		{
			DecayRatio,
			Delay,
			DryMix,
			WetMix
		}

		[SerializeField]
		private AudioEchoFilter _source;

		[SerializeField]
		private Variable _sourceVar;

		[SerializeField]
		private Property _property;

		[SerializeField]
		private Variable _output;

		protected override void OnReset()
		{
			_source = base.gameObject.GetComponent<AudioEchoFilter>();
		}

		public override void Setup()
		{
		}

		public override void Execute()
		{
			AudioEchoFilter audioEchoFilter = _sourceVar.GetValueAsComponent(_source, typeof(AudioEchoFilter)) as AudioEchoFilter;
			if (!(audioEchoFilter == null))
			{
				switch (_property)
				{
				case Property.DecayRatio:
					_output.SetValue(audioEchoFilter.decayRatio);
					break;
				case Property.Delay:
					_output.SetValue(audioEchoFilter.delay);
					break;
				case Property.DryMix:
					_output.SetValue(audioEchoFilter.dryMix);
					break;
				case Property.WetMix:
					_output.SetValue(audioEchoFilter.wetMix);
					break;
				}
			}
		}
	}
}
