using UnityEngine;

namespace GameFlow
{
	[Help("es", "Invierte el estado de activación del @UnityManual{Component} especificado.", null)]
	[Help("en", "Toggles the enablement state of the specified @UnityManual{Component}.", null)]
	[AddComponentMenu("")]
	public class ToggleComponentEnablement : Action, IExecutableInEditor
	{
		[SerializeField]
		private Component _target;

		[SerializeField]
		private Variable _targetVar;

		protected Component target => _targetVar.GetValue(_target) as Component;

		public override void Execute()
		{
			Execute(target);
		}

		public static void Execute(Component component)
		{
			if (component == null)
			{
				return;
			}
			Behaviour behaviour = component as Behaviour;
			if (behaviour != null)
			{
				behaviour.enabled = !behaviour.enabled;
				return;
			}
			Collider collider = component as Collider;
			if (collider != null)
			{
				collider.enabled = !collider.enabled;
				return;
			}
			Renderer renderer = component as Renderer;
			if (renderer != null)
			{
				renderer.enabled = !renderer.enabled;
				return;
			}
			Cloth cloth = component as Cloth;
			if (cloth != null)
			{
				cloth.enabled = !cloth.enabled;
			}
		}
	}
}
