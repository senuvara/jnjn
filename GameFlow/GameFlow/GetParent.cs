using UnityEngine;

namespace GameFlow
{
	[AddComponentMenu("")]
	[Help("en", "Gets the parent of the specified @UnityManual{GameObject}.", null)]
	[Help("es", "Obtiene el padre del @UnityManual{GameObject} especificado.", null)]
	public class GetParent : Action, IExecutableInEditor
	{
		[SerializeField]
		private GameObject _target;

		[SerializeField]
		private Variable _targetVar;

		[SerializeField]
		private Variable _output;

		protected override void OnReset()
		{
			_target = base.gameObject;
		}

		public override void Execute()
		{
			if (!(_output == null))
			{
				GameObject gameObject = _targetVar.GetValue(_target) as GameObject;
				_output.SetValue((!(gameObject != null)) ? null : gameObject.GetParent());
			}
		}
	}
}
