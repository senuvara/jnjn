namespace GameFlow
{
	public interface IActionProxy
	{
		void Execute(Action action);
	}
}
