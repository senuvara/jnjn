using UnityEngine;

namespace GameFlow
{
	[AddComponentMenu("")]
	[Help("en", "Sets a property of the specified @UnityManual{PointEffector2D}.", null)]
	[Help("es", "Modifica una propiedad del componente @UnityManual{PointEffector2D} especificado.", null)]
	public class SetPointEffector2DProperty : Action, IExecutableInEditor
	{
		public enum Property
		{
			AngularDrag,
			ColliderMask,
			DistanceScale,
			Drag,
			ForceMagnitude,
			ForceMode,
			ForceSource,
			ForceTarget,
			ForceVariation,
			UseColliderMask
		}

		[SerializeField]
		private PointEffector2D _target;

		[SerializeField]
		private Variable _targetVar;

		[SerializeField]
		private Property _property;

		[SerializeField]
		private float _angularDragValue;

		[SerializeField]
		private LayerMask _colliderMaskValue;

		[SerializeField]
		private float _distanceScaleValue;

		[SerializeField]
		private float _dragValue;

		[SerializeField]
		private float _forceMagnitudeValue;

		[SerializeField]
		private EffectorForceMode2D _forceModeValue;

		[SerializeField]
		private EffectorSelection2D _forceSourceValue;

		[SerializeField]
		private EffectorSelection2D _forceTargetValue;

		[SerializeField]
		private float _forceVariationValue;

		[SerializeField]
		private bool _useColliderMaskValue;

		[SerializeField]
		private Variable _valueVar;

		protected override void OnReset()
		{
			_target = base.gameObject.GetComponent<PointEffector2D>();
		}

		public override void Setup()
		{
		}

		public override void Execute()
		{
			PointEffector2D pointEffector2D = _targetVar.GetValueAsComponent(_target, typeof(PointEffector2D)) as PointEffector2D;
			if (!(pointEffector2D == null))
			{
				switch (_property)
				{
				case Property.AngularDrag:
					pointEffector2D.angularDrag = _valueVar.GetValue(_angularDragValue);
					break;
				case Property.ColliderMask:
					pointEffector2D.colliderMask = _valueVar.GetValue(_colliderMaskValue);
					break;
				case Property.DistanceScale:
					pointEffector2D.distanceScale = _valueVar.GetValue(_distanceScaleValue);
					break;
				case Property.Drag:
					pointEffector2D.drag = _valueVar.GetValue(_dragValue);
					break;
				case Property.ForceMagnitude:
					pointEffector2D.forceMagnitude = _valueVar.GetValue(_forceMagnitudeValue);
					break;
				case Property.ForceMode:
					pointEffector2D.forceMode = (EffectorForceMode2D)(object)_valueVar.GetValue(_forceModeValue);
					break;
				case Property.ForceSource:
					pointEffector2D.forceSource = (EffectorSelection2D)(object)_valueVar.GetValue(_forceSourceValue);
					break;
				case Property.ForceTarget:
					pointEffector2D.forceTarget = (EffectorSelection2D)(object)_valueVar.GetValue(_forceTargetValue);
					break;
				case Property.ForceVariation:
					pointEffector2D.forceVariation = _valueVar.GetValue(_forceVariationValue);
					break;
				case Property.UseColliderMask:
					pointEffector2D.useColliderMask = _valueVar.GetValue(_useColliderMaskValue);
					break;
				}
			}
		}
	}
}
