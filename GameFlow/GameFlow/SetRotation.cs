using UnityEngine;

namespace GameFlow
{
	[Help("es", "Ajusta la rotación del objecto especificado.", null)]
	[AddComponentMenu("")]
	[Help("en", "Sets the rotation of the specified object.", null)]
	public class SetRotation : TransformAction
	{
		[SerializeField]
		private Vector3 _rotation;

		[SerializeField]
		private Variable _rotationVar;

		public Vector3 rotation => _rotationVar.GetValue(_rotation);

		public override void Execute()
		{
			Execute(base.target, base.space, rotation, base.relative, base.multiplier, base.mask);
		}

		public static void Execute(Transform t, Space space, Vector3 rotation, bool relative, float multiplier, Vector3 mask)
		{
			if (t == null)
			{
				return;
			}
			if (relative)
			{
				rotation = rotation.Masked(Vector3.zero, mask.ToInverseMask());
				t.Rotate(rotation * multiplier, space);
				return;
			}
			switch (space)
			{
			case Space.World:
				t.eulerAngles = t.eulerAngles.Masked(rotation, mask);
				break;
			case Space.Self:
				t.localEulerAngles = t.localEulerAngles.Masked(rotation, mask);
				break;
			}
		}
	}
}
