using System;
using UnityEngine;

namespace GameFlow
{
	public class LayerUtils
	{
		private static long lastUpdateTick;

		private static string[] layerNames;

		public static string[] GetLayerNames()
		{
			if (layerNames == null || DateTime.Now.Ticks - lastUpdateTick > 10000000)
			{
				lastUpdateTick = DateTime.Now.Ticks;
				layerNames = new string[32];
				for (int i = 0; i < 32; i++)
				{
					string text = LayerMask.LayerToName(i);
					if (text == string.Empty)
					{
						text = ((i > 7) ? "User Layer " : "Builtin Layer ") + i;
					}
					layerNames[i] = text;
				}
			}
			return layerNames;
		}
	}
}
