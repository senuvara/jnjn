using UnityEngine;

namespace GameFlow
{
	[Help("en", "Defines a key that can be queried for state.", null)]
	[Help("es", "Define una tecla cuyo estado puede consultarse.", null)]
	[AddComponentMenu("GameFlow/Tools/Key")]
	public class Key : Block, IData, IIdentifiable, IVariableFriendly
	{
		[SerializeField]
		private string _id = string.Empty;

		[SerializeField]
		private KeyCode _code;

		[SerializeField]
		private Variable _codeVar;

		[SerializeField]
		private bool _enabledInPause;

		[SerializeField]
		private Variable _enabledInPauseVar;

		public bool isDown;

		public bool justDown;

		public bool justUp;

		public string id => _id;

		public KeyCode code => (KeyCode)(object)_codeVar.GetValue(_code);

		public bool enabledInPause => _enabledInPauseVar.GetValue(_enabledInPause);

		private void Awake()
		{
		}

		private void Start()
		{
		}

		protected override void OnReset()
		{
			SetContainer();
			if (_id == string.Empty)
			{
				_id = "Key" + Random.Range(1000, 9999);
			}
		}

		private void SetContainer()
		{
			if (container != null || !BlockInsertion.enabled || BlockInsertion.clipboard)
			{
				return;
			}
			IKeyContainer keyContainer = BlockInsertion.target as IKeyContainer;
			if (keyContainer == null)
			{
				keyContainer = base.gameObject.GetComponent<Keys>();
				if (keyContainer == null)
				{
					keyContainer = base.gameObject.AddComponent<Keys>();
				}
				BlockInsertion.index = -1;
			}
			if (BlockInsertion.index < 0 || BlockInsertion.index >= keyContainer.GetKeys().Count)
			{
				if (!keyContainer.Contains(this))
				{
					keyContainer.GetKeys().Add(this);
					keyContainer.KeyAdded(this);
					keyContainer.SetDirty(dirty: true);
				}
			}
			else
			{
				keyContainer.GetKeys().Insert(BlockInsertion.index, this);
				keyContainer.KeyAdded(this);
				keyContainer.SetDirty(dirty: true);
			}
			container = (keyContainer as Object);
			BlockInsertion.Reset();
		}

		public string GetIdForControls()
		{
			return _id;
		}

		public string GetIdForSelectors()
		{
			return _id;
		}

		public virtual string GetTypeForSelectors()
		{
			return "Key";
		}

		private void OnValidate()
		{
			HideInInspector();
		}
	}
}
