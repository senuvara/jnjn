using System;
using System.Collections.Generic;
using UnityEngine;

namespace GameFlow
{
	[AddComponentMenu("")]
	[Help("es", "Controla los eventos de activación en este @UnityManual{GameObject}.", null)]
	[Help("en", "Controls the activation events in this @UnityManual{GameObject}.", null)]
	public class ActivationController : EventController
	{
		[SerializeField]
		private bool _activateEnabled = true;

		[SerializeField]
		private bool _deactivateEnabled = true;

		private List<IEventListener> onActivateListeners = new List<IEventListener>();

		private List<IEventListener> onDeactivateListeners = new List<IEventListener>();

		public static EventController AddController(GameObject gameObject)
		{
			return EventController.AddController(gameObject, typeof(ActivationController));
		}

		private void Awake()
		{
			base.enabled = ((base.enabled && _activateEnabled) || _deactivateEnabled);
		}

		public override void AddListener(IEventListener listener, Type eventType)
		{
			if (eventType == typeof(ActivationEvent))
			{
				onActivateListeners.AddOnlyOnce(listener);
			}
			else if (eventType == typeof(DeactivationEvent))
			{
				onDeactivateListeners.AddOnlyOnce(listener);
			}
		}

		public override void RemoveListener(IEventListener listener, Type eventType)
		{
			if (eventType == typeof(ActivationEvent))
			{
				onActivateListeners.Remove(listener);
			}
			else if (eventType == typeof(DeactivationEvent))
			{
				onDeactivateListeners.Remove(listener);
			}
		}

		private void OnEnable()
		{
			if (_activateEnabled)
			{
				ActivationEvent activationEvent = new ActivationEvent();
				activationEvent.source = base.gameObject;
				activationEvent.DispatchTo(onActivateListeners);
				activationEvent.DispatchToAll();
			}
		}

		private void OnDisable()
		{
			if (_deactivateEnabled)
			{
				DeactivationEvent deactivationEvent = new DeactivationEvent();
				deactivationEvent.source = base.gameObject;
				deactivationEvent.DispatchTo(onDeactivateListeners);
				deactivationEvent.DispatchToAll();
			}
		}
	}
}
