using UnityEngine;

namespace GameFlow
{
	[Help("en", "Gets the angle between two objects.", null)]
	[AddComponentMenu("")]
	[Help("es", "Obtiene el ángulo entre dos objetos.", null)]
	public class GetAngle : Action, IExecutableInEditor
	{
		[SerializeField]
		private Transform _t1;

		[SerializeField]
		private Variable _t1Var;

		[SerializeField]
		private TransformUtils.Direction _direction;

		[SerializeField]
		private Variable _directionVar;

		[SerializeField]
		private Transform _t2;

		[SerializeField]
		private Variable _t2Var;

		[SerializeField]
		private Variable _output;

		protected override void OnReset()
		{
			_t1 = base.gameObject.GetComponent<Transform>();
		}

		public override void Execute()
		{
			if (!(_output == null))
			{
				Transform transform = _t1Var.GetValueAsComponent(_t1, typeof(Transform)) as Transform;
				Transform transform2 = _t2Var.GetValueAsComponent(_t2, typeof(Transform)) as Transform;
				if (!(transform == null) && !(transform2 == null))
				{
					TransformUtils.Direction direction = (TransformUtils.Direction)(object)_directionVar.GetValue(_direction);
					float value = Vector3.Angle(transform.position - transform2.position, transform2.GetDirection(direction));
					_output.SetValue(value);
				}
			}
		}
	}
}
