using UnityEngine;

namespace GameFlow
{
	[Help("es", "Cambia el puntero del ratón a la textura especificada.", null)]
	[AddComponentMenu("")]
	[Help("en", "Changes the mouse cursor to the specified texture.", null)]
	public class SetMouseCursor : Action
	{
		public enum CursorType
		{
			Texture,
			Default
		}

		[SerializeField]
		private CursorType _cursorType;

		[SerializeField]
		private Texture2D _texture;

		[SerializeField]
		public Variable _textureVar;

		[SerializeField]
		private Vector2 _hotspot;

		[SerializeField]
		public Variable _hotspotVar;

		public override void Execute()
		{
			Execute((_cursorType != 0) ? null : (_textureVar.GetValue(_texture) as Texture2D), _hotspotVar.GetValue(_hotspot));
		}

		public static void Execute(Texture2D texture, Vector2 hotspot)
		{
			Cursor.SetCursor(texture, hotspot, CursorMode.Auto);
			Cursor.SetCursor(texture, hotspot, CursorMode.Auto);
		}
	}
}
