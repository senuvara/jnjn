using UnityEngine;

namespace GameFlow
{
	[AddComponentMenu("")]
	[Help("en", "Pauses the Unity Editor.", null)]
	[Help("es", "Pone en pausa el editor de Unity.", null)]
	public class PauseEditor : Action, IYielder
	{
		public static IActionProxy proxy;

		public override void Execute()
		{
			if (proxy != null)
			{
				proxy.Execute(this);
			}
		}

		public void Yield()
		{
		}
	}
}
