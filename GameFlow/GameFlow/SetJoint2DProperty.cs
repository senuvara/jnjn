using UnityEngine;

namespace GameFlow
{
	[Help("es", "Modifica una propiedad del componente Joint 2D especificado.", null)]
	[AddComponentMenu("")]
	[Help("en", "Sets a property of the specified Joint 2D component.", null)]
	public class SetJoint2DProperty : Action, IExecutableInEditor
	{
		public enum JointProperty
		{
			BreakForce,
			BreakTorque,
			ConnectedBody,
			EnableCollision
		}

		public enum DistanceJointProperty
		{
			BreakForce,
			BreakTorque,
			ConnectedBody,
			EnableCollision,
			Anchor,
			AutoConfigureConnectedAnchor,
			ConnectedAnchor,
			AutoConfigureDistance,
			Distance
		}

		public enum FixedJointProperty
		{
			BreakForce,
			BreakTorque,
			ConnectedBody,
			EnableCollision,
			Anchor,
			AutoConfigureConnectedAnchor,
			ConnectedAnchor,
			DampingRatio,
			Frequency
		}

		public enum FrictionJointProperty
		{
			BreakForce,
			BreakTorque,
			ConnectedBody,
			EnableCollision,
			Anchor,
			AutoConfigureConnectedAnchor,
			ConnectedAnchor,
			MaxForce,
			MaxTorque
		}

		public enum HingeJointProperty
		{
			BreakForce,
			BreakTorque,
			ConnectedBody,
			EnableCollision,
			Anchor,
			AutoConfigureConnectedAnchor,
			ConnectedAnchor,
			Limits_Max,
			Limits_Min,
			Motor_MaxTorque,
			Motor_Speed,
			UseLimits,
			UseMotor
		}

		public enum RelativeJointProperty
		{
			BreakForce,
			BreakTorque,
			ConnectedBody,
			EnableCollision,
			AngularOffset,
			AutoConfigureOffset,
			CorrectionScale,
			LinearOffset,
			MaxForce,
			MaxTorque
		}

		public enum SliderJointProperty
		{
			BreakForce,
			BreakTorque,
			ConnectedBody,
			EnableCollision,
			Anchor,
			AutoConfigureConnectedAnchor,
			ConnectedAnchor,
			Angle,
			AutoConfigureAngle,
			Limits_Max,
			Limits_Min,
			Motor_MaxTorque,
			Motor_Speed,
			UseLimits,
			UseMotor
		}

		public enum SpringJointProperty
		{
			BreakForce,
			BreakTorque,
			ConnectedBody,
			EnableCollision,
			Anchor,
			AutoConfigureConnectedAnchor,
			ConnectedAnchor,
			AutoConfigureDistance,
			DampingRatio,
			Distance,
			Frequency
		}

		public enum TargetJointProperty
		{
			BreakForce,
			BreakTorque,
			ConnectedBody,
			EnableCollision,
			Anchor,
			AutoConfigureTarget,
			DampingRatio,
			Frequency,
			MaxForce,
			Target
		}

		public enum WheelJointProperty
		{
			BreakForce,
			BreakTorque,
			ConnectedBody,
			EnableCollision,
			Anchor,
			AutoConfigureConnectedAnchor,
			ConnectedAnchor,
			Motor_MaxTorque,
			Motor_Speed,
			Suspension_Angle,
			Suspension_DampingRatio,
			Suspension_Frequency,
			UseMotor
		}

		[SerializeField]
		private Joint2D _target;

		[SerializeField]
		private Variable _targetVar;

		[SerializeField]
		private JointProperty _jointProperty;

		[SerializeField]
		private DistanceJointProperty _distanceProperty;

		[SerializeField]
		private FixedJointProperty _fixedProperty;

		[SerializeField]
		private FrictionJointProperty _frictionProperty;

		[SerializeField]
		private HingeJointProperty _hingeProperty;

		[SerializeField]
		private RelativeJointProperty _relativeProperty;

		[SerializeField]
		private SliderJointProperty _sliderProperty;

		[SerializeField]
		private SpringJointProperty _springProperty;

		[SerializeField]
		private TargetJointProperty _targetProperty;

		[SerializeField]
		private WheelJointProperty _wheelProperty;

		[SerializeField]
		private Vector2 _vector2Value;

		[SerializeField]
		private bool _boolValue;

		[SerializeField]
		private Rigidbody2D _rigidbodyValue;

		[SerializeField]
		private float _floatValue;

		[SerializeField]
		private Variable _valueVar;

		protected override void OnReset()
		{
			_target = base.gameObject.GetComponent<Joint2D>();
		}

		public override void Execute()
		{
			Joint2D joint2D = _target;
			if (joint2D == null)
			{
				joint2D = (_targetVar.GetValueAsComponent(_target, typeof(Joint2D)) as Joint2D);
			}
			if (joint2D == null)
			{
				return;
			}
			DistanceJoint2D distanceJoint2D = joint2D as DistanceJoint2D;
			if (distanceJoint2D != null)
			{
				switch (_distanceProperty)
				{
				case DistanceJointProperty.BreakForce:
					distanceJoint2D.breakForce = _valueVar.GetValue(_floatValue);
					break;
				case DistanceJointProperty.BreakTorque:
					distanceJoint2D.breakTorque = _valueVar.GetValue(_floatValue);
					break;
				case DistanceJointProperty.ConnectedBody:
					distanceJoint2D.connectedBody = (_valueVar.GetValueAsComponent(_rigidbodyValue, typeof(Rigidbody2D)) as Rigidbody2D);
					break;
				case DistanceJointProperty.EnableCollision:
					distanceJoint2D.enableCollision = _valueVar.GetValue(_boolValue);
					break;
				case DistanceJointProperty.Anchor:
					distanceJoint2D.anchor = _valueVar.GetValue(_vector2Value);
					break;
				case DistanceJointProperty.AutoConfigureConnectedAnchor:
					distanceJoint2D.autoConfigureConnectedAnchor = _valueVar.GetValue(_boolValue);
					break;
				case DistanceJointProperty.ConnectedAnchor:
					distanceJoint2D.connectedAnchor = _valueVar.GetValue(_vector2Value);
					break;
				case DistanceJointProperty.AutoConfigureDistance:
					distanceJoint2D.autoConfigureDistance = _valueVar.GetValue(_boolValue);
					break;
				case DistanceJointProperty.Distance:
					distanceJoint2D.distance = _valueVar.GetValue(_floatValue);
					break;
				}
				return;
			}
			FixedJoint2D fixedJoint2D = joint2D as FixedJoint2D;
			if (fixedJoint2D != null)
			{
				switch (_fixedProperty)
				{
				case FixedJointProperty.BreakForce:
					fixedJoint2D.breakForce = _valueVar.GetValue(_floatValue);
					break;
				case FixedJointProperty.BreakTorque:
					fixedJoint2D.breakTorque = _valueVar.GetValue(_floatValue);
					break;
				case FixedJointProperty.ConnectedBody:
					fixedJoint2D.connectedBody = (_valueVar.GetValueAsComponent(_rigidbodyValue, typeof(Rigidbody2D)) as Rigidbody2D);
					break;
				case FixedJointProperty.EnableCollision:
					fixedJoint2D.enableCollision = _valueVar.GetValue(_boolValue);
					break;
				case FixedJointProperty.Anchor:
					fixedJoint2D.anchor = _valueVar.GetValue(_vector2Value);
					break;
				case FixedJointProperty.AutoConfigureConnectedAnchor:
					fixedJoint2D.autoConfigureConnectedAnchor = _valueVar.GetValue(_boolValue);
					break;
				case FixedJointProperty.ConnectedAnchor:
					fixedJoint2D.connectedAnchor = _valueVar.GetValue(_vector2Value);
					break;
				case FixedJointProperty.DampingRatio:
					fixedJoint2D.dampingRatio = _valueVar.GetValue(_floatValue);
					break;
				case FixedJointProperty.Frequency:
					fixedJoint2D.frequency = _valueVar.GetValue(_floatValue);
					break;
				}
				return;
			}
			FrictionJoint2D frictionJoint2D = joint2D as FrictionJoint2D;
			if (frictionJoint2D != null)
			{
				switch (_frictionProperty)
				{
				case FrictionJointProperty.BreakForce:
					frictionJoint2D.breakForce = _valueVar.GetValue(_floatValue);
					break;
				case FrictionJointProperty.BreakTorque:
					frictionJoint2D.breakTorque = _valueVar.GetValue(_floatValue);
					break;
				case FrictionJointProperty.ConnectedBody:
					frictionJoint2D.connectedBody = (_valueVar.GetValueAsComponent(_rigidbodyValue, typeof(Rigidbody2D)) as Rigidbody2D);
					break;
				case FrictionJointProperty.EnableCollision:
					frictionJoint2D.enableCollision = _valueVar.GetValue(_boolValue);
					break;
				case FrictionJointProperty.Anchor:
					frictionJoint2D.anchor = _valueVar.GetValue(_vector2Value);
					break;
				case FrictionJointProperty.AutoConfigureConnectedAnchor:
					frictionJoint2D.autoConfigureConnectedAnchor = _valueVar.GetValue(_boolValue);
					break;
				case FrictionJointProperty.ConnectedAnchor:
					frictionJoint2D.connectedAnchor = _valueVar.GetValue(_vector2Value);
					break;
				case FrictionJointProperty.MaxForce:
					frictionJoint2D.maxForce = _valueVar.GetValue(_floatValue);
					break;
				case FrictionJointProperty.MaxTorque:
					frictionJoint2D.maxTorque = _valueVar.GetValue(_floatValue);
					break;
				}
				return;
			}
			HingeJoint2D hingeJoint2D = joint2D as HingeJoint2D;
			if (hingeJoint2D != null)
			{
				switch (_hingeProperty)
				{
				case HingeJointProperty.BreakForce:
					hingeJoint2D.breakForce = _valueVar.GetValue(_floatValue);
					break;
				case HingeJointProperty.BreakTorque:
					hingeJoint2D.breakTorque = _valueVar.GetValue(_floatValue);
					break;
				case HingeJointProperty.ConnectedBody:
					hingeJoint2D.connectedBody = (_valueVar.GetValueAsComponent(_rigidbodyValue, typeof(Rigidbody2D)) as Rigidbody2D);
					break;
				case HingeJointProperty.EnableCollision:
					hingeJoint2D.enableCollision = _valueVar.GetValue(_boolValue);
					break;
				case HingeJointProperty.Anchor:
					hingeJoint2D.anchor = _valueVar.GetValue(_vector2Value);
					break;
				case HingeJointProperty.AutoConfigureConnectedAnchor:
					hingeJoint2D.autoConfigureConnectedAnchor = _valueVar.GetValue(_boolValue);
					break;
				case HingeJointProperty.ConnectedAnchor:
					hingeJoint2D.connectedAnchor = _valueVar.GetValue(_vector2Value);
					break;
				case HingeJointProperty.Limits_Max:
				{
					JointAngleLimits2D limits = hingeJoint2D.limits;
					limits.max = _valueVar.GetValue(_floatValue);
					hingeJoint2D.limits = limits;
					break;
				}
				case HingeJointProperty.Limits_Min:
				{
					JointAngleLimits2D limits = hingeJoint2D.limits;
					limits.min = _valueVar.GetValue(_floatValue);
					hingeJoint2D.limits = limits;
					break;
				}
				case HingeJointProperty.Motor_MaxTorque:
				{
					JointMotor2D motor = hingeJoint2D.motor;
					motor.maxMotorTorque = _valueVar.GetValue(_floatValue);
					hingeJoint2D.motor = motor;
					break;
				}
				case HingeJointProperty.Motor_Speed:
				{
					JointMotor2D motor = hingeJoint2D.motor;
					motor.motorSpeed = _valueVar.GetValue(_floatValue);
					hingeJoint2D.motor = motor;
					break;
				}
				case HingeJointProperty.UseLimits:
					hingeJoint2D.useLimits = _valueVar.GetValue(_boolValue);
					break;
				case HingeJointProperty.UseMotor:
					hingeJoint2D.useMotor = _valueVar.GetValue(_boolValue);
					break;
				}
				return;
			}
			RelativeJoint2D relativeJoint2D = joint2D as RelativeJoint2D;
			if (relativeJoint2D != null)
			{
				switch (_relativeProperty)
				{
				case RelativeJointProperty.BreakForce:
					relativeJoint2D.breakForce = _valueVar.GetValue(_floatValue);
					break;
				case RelativeJointProperty.BreakTorque:
					relativeJoint2D.breakTorque = _valueVar.GetValue(_floatValue);
					break;
				case RelativeJointProperty.ConnectedBody:
					relativeJoint2D.connectedBody = (_valueVar.GetValueAsComponent(_rigidbodyValue, typeof(Rigidbody2D)) as Rigidbody2D);
					break;
				case RelativeJointProperty.EnableCollision:
					relativeJoint2D.enableCollision = _valueVar.GetValue(_boolValue);
					break;
				case RelativeJointProperty.AngularOffset:
					relativeJoint2D.angularOffset = _valueVar.GetValue(_floatValue);
					break;
				case RelativeJointProperty.AutoConfigureOffset:
					relativeJoint2D.autoConfigureOffset = _valueVar.GetValue(_boolValue);
					break;
				case RelativeJointProperty.CorrectionScale:
					relativeJoint2D.correctionScale = _valueVar.GetValue(_floatValue);
					break;
				case RelativeJointProperty.LinearOffset:
					relativeJoint2D.linearOffset = _valueVar.GetValue(_vector2Value);
					break;
				case RelativeJointProperty.MaxForce:
					relativeJoint2D.maxForce = _valueVar.GetValue(_floatValue);
					break;
				case RelativeJointProperty.MaxTorque:
					relativeJoint2D.maxTorque = _valueVar.GetValue(_floatValue);
					break;
				}
				return;
			}
			SliderJoint2D sliderJoint2D = joint2D as SliderJoint2D;
			if (sliderJoint2D != null)
			{
				switch (_sliderProperty)
				{
				case SliderJointProperty.BreakForce:
					sliderJoint2D.breakForce = _valueVar.GetValue(_floatValue);
					break;
				case SliderJointProperty.BreakTorque:
					sliderJoint2D.breakTorque = _valueVar.GetValue(_floatValue);
					break;
				case SliderJointProperty.ConnectedBody:
					sliderJoint2D.connectedBody = (_valueVar.GetValueAsComponent(_rigidbodyValue, typeof(Rigidbody2D)) as Rigidbody2D);
					break;
				case SliderJointProperty.EnableCollision:
					sliderJoint2D.enableCollision = _valueVar.GetValue(_boolValue);
					break;
				case SliderJointProperty.Anchor:
					sliderJoint2D.anchor = _valueVar.GetValue(_vector2Value);
					break;
				case SliderJointProperty.AutoConfigureConnectedAnchor:
					sliderJoint2D.autoConfigureConnectedAnchor = _valueVar.GetValue(_boolValue);
					break;
				case SliderJointProperty.ConnectedAnchor:
					sliderJoint2D.connectedAnchor = _valueVar.GetValue(_vector2Value);
					break;
				case SliderJointProperty.Angle:
					sliderJoint2D.angle = _valueVar.GetValue(_floatValue);
					break;
				case SliderJointProperty.AutoConfigureAngle:
					sliderJoint2D.autoConfigureConnectedAnchor = _valueVar.GetValue(_boolValue);
					break;
				case SliderJointProperty.Limits_Max:
				{
					JointTranslationLimits2D limits2 = sliderJoint2D.limits;
					limits2.max = _valueVar.GetValue(_floatValue);
					sliderJoint2D.limits = limits2;
					break;
				}
				case SliderJointProperty.Limits_Min:
				{
					JointTranslationLimits2D limits2 = sliderJoint2D.limits;
					limits2.min = _valueVar.GetValue(_floatValue);
					sliderJoint2D.limits = limits2;
					break;
				}
				case SliderJointProperty.Motor_MaxTorque:
				{
					JointMotor2D motor = sliderJoint2D.motor;
					motor.maxMotorTorque = _valueVar.GetValue(_floatValue);
					sliderJoint2D.motor = motor;
					break;
				}
				case SliderJointProperty.Motor_Speed:
				{
					JointMotor2D motor = sliderJoint2D.motor;
					motor.motorSpeed = _valueVar.GetValue(_floatValue);
					sliderJoint2D.motor = motor;
					break;
				}
				case SliderJointProperty.UseLimits:
					sliderJoint2D.useLimits = _valueVar.GetValue(_boolValue);
					break;
				case SliderJointProperty.UseMotor:
					sliderJoint2D.useMotor = _valueVar.GetValue(_boolValue);
					break;
				}
				return;
			}
			SpringJoint2D springJoint2D = joint2D as SpringJoint2D;
			if (springJoint2D != null)
			{
				switch (_springProperty)
				{
				case SpringJointProperty.Distance:
					break;
				case SpringJointProperty.BreakForce:
					springJoint2D.breakForce = _valueVar.GetValue(_floatValue);
					break;
				case SpringJointProperty.BreakTorque:
					springJoint2D.breakTorque = _valueVar.GetValue(_floatValue);
					break;
				case SpringJointProperty.ConnectedBody:
					springJoint2D.connectedBody = (_valueVar.GetValueAsComponent(_rigidbodyValue, typeof(Rigidbody2D)) as Rigidbody2D);
					break;
				case SpringJointProperty.EnableCollision:
					springJoint2D.enableCollision = _valueVar.GetValue(_boolValue);
					break;
				case SpringJointProperty.Anchor:
					springJoint2D.anchor = _valueVar.GetValue(_vector2Value);
					break;
				case SpringJointProperty.AutoConfigureConnectedAnchor:
					springJoint2D.autoConfigureConnectedAnchor = _valueVar.GetValue(_boolValue);
					break;
				case SpringJointProperty.ConnectedAnchor:
					springJoint2D.connectedAnchor = _valueVar.GetValue(_vector2Value);
					break;
				case SpringJointProperty.AutoConfigureDistance:
					springJoint2D.autoConfigureDistance = _valueVar.GetValue(_boolValue);
					break;
				case SpringJointProperty.DampingRatio:
					springJoint2D.dampingRatio = _valueVar.GetValue(_floatValue);
					break;
				case SpringJointProperty.Frequency:
					springJoint2D.frequency = _valueVar.GetValue(_floatValue);
					break;
				}
				return;
			}
			TargetJoint2D targetJoint2D = joint2D as TargetJoint2D;
			if (targetJoint2D != null)
			{
				switch (_targetProperty)
				{
				case TargetJointProperty.MaxForce:
					break;
				case TargetJointProperty.BreakForce:
					targetJoint2D.breakForce = _valueVar.GetValue(_floatValue);
					break;
				case TargetJointProperty.BreakTorque:
					targetJoint2D.breakTorque = _valueVar.GetValue(_floatValue);
					break;
				case TargetJointProperty.ConnectedBody:
					targetJoint2D.connectedBody = (_valueVar.GetValueAsComponent(_rigidbodyValue, typeof(Rigidbody2D)) as Rigidbody2D);
					break;
				case TargetJointProperty.EnableCollision:
					targetJoint2D.enableCollision = _valueVar.GetValue(_boolValue);
					break;
				case TargetJointProperty.Anchor:
					targetJoint2D.anchor = _valueVar.GetValue(_vector2Value);
					break;
				case TargetJointProperty.AutoConfigureTarget:
					targetJoint2D.autoConfigureTarget = _valueVar.GetValue(_boolValue);
					break;
				case TargetJointProperty.DampingRatio:
					targetJoint2D.dampingRatio = _valueVar.GetValue(_floatValue);
					break;
				case TargetJointProperty.Frequency:
					targetJoint2D.frequency = _valueVar.GetValue(_floatValue);
					break;
				case TargetJointProperty.Target:
					targetJoint2D.target = _valueVar.GetValue(_vector2Value);
					break;
				}
				return;
			}
			WheelJoint2D wheelJoint2D = joint2D as WheelJoint2D;
			if (wheelJoint2D != null)
			{
				switch (_wheelProperty)
				{
				case WheelJointProperty.BreakForce:
					wheelJoint2D.breakForce = _valueVar.GetValue(_floatValue);
					break;
				case WheelJointProperty.BreakTorque:
					wheelJoint2D.breakTorque = _valueVar.GetValue(_floatValue);
					break;
				case WheelJointProperty.ConnectedBody:
					wheelJoint2D.connectedBody = (_valueVar.GetValueAsComponent(_rigidbodyValue, typeof(Rigidbody2D)) as Rigidbody2D);
					break;
				case WheelJointProperty.EnableCollision:
					wheelJoint2D.enableCollision = _valueVar.GetValue(_boolValue);
					break;
				case WheelJointProperty.Anchor:
					wheelJoint2D.anchor = _valueVar.GetValue(_vector2Value);
					break;
				case WheelJointProperty.AutoConfigureConnectedAnchor:
					wheelJoint2D.autoConfigureConnectedAnchor = _valueVar.GetValue(_boolValue);
					break;
				case WheelJointProperty.ConnectedAnchor:
					wheelJoint2D.connectedAnchor = _valueVar.GetValue(_vector2Value);
					break;
				case WheelJointProperty.Motor_MaxTorque:
				{
					JointMotor2D motor = sliderJoint2D.motor;
					motor.maxMotorTorque = _valueVar.GetValue(_floatValue);
					wheelJoint2D.motor = motor;
					break;
				}
				case WheelJointProperty.Motor_Speed:
				{
					JointMotor2D motor = sliderJoint2D.motor;
					motor.motorSpeed = _valueVar.GetValue(_floatValue);
					wheelJoint2D.motor = motor;
					break;
				}
				case WheelJointProperty.Suspension_Angle:
				{
					JointSuspension2D suspension = wheelJoint2D.suspension;
					suspension.angle = _valueVar.GetValue(_floatValue);
					wheelJoint2D.suspension = suspension;
					break;
				}
				case WheelJointProperty.Suspension_DampingRatio:
				{
					JointSuspension2D suspension = wheelJoint2D.suspension;
					suspension.dampingRatio = _valueVar.GetValue(_floatValue);
					wheelJoint2D.suspension = suspension;
					break;
				}
				case WheelJointProperty.Suspension_Frequency:
				{
					JointSuspension2D suspension = wheelJoint2D.suspension;
					suspension.frequency = _valueVar.GetValue(_floatValue);
					wheelJoint2D.suspension = suspension;
					break;
				}
				case WheelJointProperty.UseMotor:
					wheelJoint2D.useMotor = _valueVar.GetValue(_boolValue);
					break;
				}
			}
		}
	}
}
