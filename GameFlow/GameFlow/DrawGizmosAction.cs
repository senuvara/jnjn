using System;

namespace GameFlow
{
	public abstract class DrawGizmosAction : Action, IExecutableInEditor
	{
		public override Type GetPreferredContainerType()
		{
			return typeof(OnDrawGizmos);
		}
	}
}
