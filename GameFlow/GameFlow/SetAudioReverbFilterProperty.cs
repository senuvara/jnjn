using UnityEngine;

namespace GameFlow
{
	[AddComponentMenu("")]
	[Help("es", "Modifica una propiedad del componente @UnityManual{AudioReverbFilter} especificado.", null)]
	[Help("en", "Sets a property of the specified @UnityManual{AudioReverbFilter} component.", null)]
	public class SetAudioReverbFilterProperty : Action, IExecutableInEditor
	{
		public enum Property
		{
			DecayHFRatio,
			DecayTime,
			Density,
			Diffusion,
			DryLevel,
			HfReference,
			LfReference,
			ReflectionsDelay,
			ReflectionsLevel,
			ReverbDelay,
			ReverbLevel,
			ReverbPreset,
			Room,
			RoomHF,
			RoomLF,
			RoomRolloff
		}

		[SerializeField]
		private AudioReverbFilter _target;

		[SerializeField]
		private Variable _targetVar;

		[SerializeField]
		private Property _property;

		[SerializeField]
		private float _decayHFRatioValue;

		[SerializeField]
		private float _decayTimeValue;

		[SerializeField]
		private float _densityValue;

		[SerializeField]
		private float _diffusionValue;

		[SerializeField]
		private float _dryLevelValue;

		[SerializeField]
		private float _hfReferenceValue;

		[SerializeField]
		private float _lfReferenceValue;

		[SerializeField]
		private float _reflectionsDelayValue;

		[SerializeField]
		private float _reflectionsLevelValue;

		[SerializeField]
		private float _reverbDelayValue;

		[SerializeField]
		private float _reverbLevelValue;

		[SerializeField]
		private AudioReverbPreset _reverbPresetValue;

		[SerializeField]
		private float _roomValue;

		[SerializeField]
		private float _roomHFValue;

		[SerializeField]
		private float _roomLFValue;

		[SerializeField]
		private float _roomRolloffValue;

		[SerializeField]
		private Variable _valueVar;

		protected override void OnReset()
		{
			_target = base.gameObject.GetComponent<AudioReverbFilter>();
		}

		public override void Setup()
		{
		}

		public override void Execute()
		{
			AudioReverbFilter audioReverbFilter = _targetVar.GetValueAsComponent(_target, typeof(AudioReverbFilter)) as AudioReverbFilter;
			if (!(audioReverbFilter == null))
			{
				switch (_property)
				{
				case Property.DecayHFRatio:
					audioReverbFilter.decayHFRatio = _valueVar.GetValue(_decayHFRatioValue);
					break;
				case Property.DecayTime:
					audioReverbFilter.decayTime = _valueVar.GetValue(_decayTimeValue);
					break;
				case Property.Density:
					audioReverbFilter.density = _valueVar.GetValue(_densityValue);
					break;
				case Property.Diffusion:
					audioReverbFilter.diffusion = _valueVar.GetValue(_diffusionValue);
					break;
				case Property.DryLevel:
					audioReverbFilter.dryLevel = _valueVar.GetValue(_dryLevelValue);
					break;
				case Property.HfReference:
					audioReverbFilter.hfReference = _valueVar.GetValue(_hfReferenceValue);
					break;
				case Property.LfReference:
					audioReverbFilter.lfReference = _valueVar.GetValue(_lfReferenceValue);
					break;
				case Property.ReflectionsDelay:
					audioReverbFilter.reflectionsDelay = _valueVar.GetValue(_reflectionsDelayValue);
					break;
				case Property.ReflectionsLevel:
					audioReverbFilter.reflectionsLevel = _valueVar.GetValue(_reflectionsLevelValue);
					break;
				case Property.ReverbDelay:
					audioReverbFilter.reverbDelay = _valueVar.GetValue(_reverbDelayValue);
					break;
				case Property.ReverbLevel:
					audioReverbFilter.reverbLevel = _valueVar.GetValue(_reverbLevelValue);
					break;
				case Property.ReverbPreset:
					audioReverbFilter.reverbPreset = (AudioReverbPreset)(object)_valueVar.GetValue(_reverbPresetValue);
					break;
				case Property.Room:
					audioReverbFilter.room = _valueVar.GetValue(_roomValue);
					break;
				case Property.RoomHF:
					audioReverbFilter.roomHF = _valueVar.GetValue(_roomHFValue);
					break;
				case Property.RoomLF:
					audioReverbFilter.roomLF = _valueVar.GetValue(_roomLFValue);
					break;
				case Property.RoomRolloff:
					audioReverbFilter.set_roomRolloff(_valueVar.GetValue(_roomRolloffValue));
					break;
				}
			}
		}
	}
}
