using UnityEngine;

namespace GameFlow
{
	[Help("es", "Evalúa la plataforma sobre la que se está ejecutando la aplicación.", null)]
	[Help("en", "Evaluates the platform the application is running on.", null)]
	[AddComponentMenu("")]
	public class PlatformCondition : Condition
	{
		public enum Comparison
		{
			Is,
			IsNot,
			TypeIs,
			TypeIsNot
		}

		[SerializeField]
		private Comparison _comparison;

		[SerializeField]
		private RuntimePlatform _platform;

		[SerializeField]
		private Variable _platformVar;

		[SerializeField]
		private PlatformType _type;

		[SerializeField]
		private Variable _typeVar;

		public override bool Evaluate()
		{
			switch (_comparison)
			{
			case Comparison.Is:
				return Platform.current == (RuntimePlatform)(object)_platformVar.GetValue(_platform);
			case Comparison.IsNot:
				return Platform.current != (RuntimePlatform)(object)_platformVar.GetValue(_platform);
			case Comparison.TypeIs:
				return Platform.type == (PlatformType)(object)_typeVar.GetValue(_type);
			case Comparison.TypeIsNot:
				return Platform.type != (PlatformType)(object)_typeVar.GetValue(_type);
			default:
				return false;
			}
		}
	}
}
