using UnityEngine;

namespace GameFlow
{
	[AddComponentMenu("")]
	[Help("en", "Resumes the game if it was paused.", null)]
	[Help("es", "Reanuda el juego si estaba en pausa.", null)]
	public class ResumeGame : Action
	{
		private static LocalizedString nonPaused = LocalizedString.Create().Add("en", "Ignoring 'Resume Game' action: Game is not paused.").Add("es", "Ignorando acción 'Resume Game': El juego no está en pausa.");

		public override void Execute()
		{
			if (!Game.paused)
			{
				Log.Warning(nonPaused, base.gameObject);
			}
			Game.Resume();
		}
	}
}
