using UnityEngine;

namespace GameFlow
{
	[AddComponentMenu("")]
	[Help("es", "Envía el comando (@GameFlow{Command}) con la Id especificada a un GameObject y lo ejecuta (si existe).", null)]
	[Help("en", "Sends the @GameFlow{Command} with the specified Id to a target GameObject and executes it (if exists).", null)]
	public class SendCommand : Action
	{
		[SerializeField]
		private GameObject _target;

		[SerializeField]
		private Variable _targetVar;

		[SerializeField]
		private int _commandIndex;

		[SerializeField]
		private string _commandId;

		[SerializeField]
		private Variable _commandIdVar;

		[SerializeField]
		private bool _restart = true;

		[SerializeField]
		private Variable _restartVar;

		[SerializeField]
		private bool _wait = true;

		[SerializeField]
		private Variable _waitVar;

		private GameObject _lastTarget;

		private string _lastCommandId;

		private Command _cachedCommand;

		private Program _runningProgram;

		public GameObject target => _targetVar.GetValue(_target) as GameObject;

		public string commandId => _commandIdVar.GetValue(_commandId);

		public bool restart => _restartVar.GetValue(_restart);

		public bool wait => _waitVar.GetValue(_wait);

		public override void FirstStep()
		{
			GameObject target = this.target;
			if (target == null)
			{
				return;
			}
			string commandId = this.commandId;
			if (_lastTarget != target || _lastCommandId != commandId)
			{
				_cachedCommand = Commands.GetCommandById(commandId, target);
				_lastTarget = target;
				_lastCommandId = commandId;
			}
			if (!(_cachedCommand == null))
			{
				Program program = _cachedCommand.program;
				_runningProgram = null;
				if (!(program == null) && (!program.running || restart || wait))
				{
					_runningProgram = program;
					program.Execute();
				}
			}
		}

		public override bool Finished()
		{
			if (_runningProgram == null || !wait)
			{
				return true;
			}
			if (_runningProgram.finished)
			{
				_runningProgram = null;
				return true;
			}
			return false;
		}

		public static void Execute(GameObject target, string commandId, bool restart)
		{
			if (target == null)
			{
				return;
			}
			Command commandById = Commands.GetCommandById(commandId, target);
			if (!(commandById == null))
			{
				Program program = commandById.program;
				if (!(program == null) && (!program.running || restart))
				{
					program.Execute();
				}
			}
		}
	}
}
