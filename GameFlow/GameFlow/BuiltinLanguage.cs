using System;
using UnityEngine;

namespace GameFlow
{
	[Help("es", "Una @GameFlow{Variable} integrada que devuelve el idioma en uso.", null)]
	[Help("en", "A built-in @GameFlow{Variable} that returns the current language.", null)]
	[AddComponentMenu("")]
	public class BuiltinLanguage : BuiltinVariable
	{
		public override Enum enumValue => GameSettings.language.systemLanguage;

		public override Type GetVariableType()
		{
			return typeof(SystemLanguage);
		}

		protected override string GetVariableId()
		{
			return "Language";
		}
	}
}
