namespace GameFlow
{
	public class Environment
	{
		public static IEnvironmentProxy proxy;

		public static bool IsEditorPaused()
		{
			return proxy != null && proxy.IsEditorPaused();
		}
	}
}
