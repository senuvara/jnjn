using UnityEngine;
using UnityEngine.Rendering;

namespace GameFlow
{
	[Help("es", "Modifica una propiedad del componente @UnityManual{TrailRenderer} especificado.", null)]
	[Help("en", "Sets a property of the specified @UnityManual{TrailRenderer}.", null)]
	[AddComponentMenu("")]
	public class SetTrailRendererProperty : Action, IExecutableInEditor
	{
		public enum Property
		{
			Autodestruct,
			Enabled,
			EndWidth,
			LightmapIndex,
			LightmapScaleOffset,
			Material,
			Materials,
			ProbeAnchor,
			RealtimeLightmapScaleOffset,
			ReceiveShadows,
			ReflectionProbeUsage,
			ShadowCastingMode,
			SharedMaterial,
			SharedMaterials,
			SortingLayerID,
			SortingLayerName,
			SortingOrder,
			StartWidth,
			Time,
			LightProbeUsage
		}

		[SerializeField]
		private TrailRenderer _target;

		[SerializeField]
		private Variable _targetVar;

		[SerializeField]
		private Property _property;

		[SerializeField]
		private bool _autodestructValue;

		[SerializeField]
		private bool _enabledValue;

		[SerializeField]
		private float _endWidthValue;

		[SerializeField]
		private int _lightmapIndexValue;

		[SerializeField]
		private Vector4 _lightmapScaleOffsetValue;

		[SerializeField]
		private Material _materialValue;

		[SerializeField]
		private Transform _probeAnchorValue;

		[SerializeField]
		private Vector4 _realtimeLightmapScaleOffsetValue;

		[SerializeField]
		private bool _receiveShadowsValue;

		[SerializeField]
		private ReflectionProbeUsage _reflectionProbeUsageValue;

		[SerializeField]
		private ShadowCastingMode _shadowCastingModeValue;

		[SerializeField]
		private Material _sharedMaterialValue;

		[SerializeField]
		private int _sortingLayerIDValue;

		[SerializeField]
		private string _sortingLayerNameValue;

		[SerializeField]
		private int _sortingOrderValue;

		[SerializeField]
		private float _startWidthValue;

		[SerializeField]
		private float _timeValue;

		[SerializeField]
		private LightProbeUsage _lightProbeUsageValue;

		[SerializeField]
		private Variable _valueVar;

		protected override void OnReset()
		{
			_target = base.gameObject.GetComponent<TrailRenderer>();
		}

		public override void Setup()
		{
		}

		public override void Execute()
		{
			TrailRenderer trailRenderer = _targetVar.GetValueAsComponent(_target, typeof(TrailRenderer)) as TrailRenderer;
			if (!(trailRenderer == null))
			{
				switch (_property)
				{
				case Property.Materials:
				case Property.SharedMaterials:
					break;
				case Property.Autodestruct:
					trailRenderer.autodestruct = _valueVar.GetValue(_autodestructValue);
					break;
				case Property.Enabled:
					trailRenderer.enabled = _valueVar.GetValue(_enabledValue);
					break;
				case Property.EndWidth:
					trailRenderer.endWidth = _valueVar.GetValue(_endWidthValue);
					break;
				case Property.LightmapIndex:
					trailRenderer.lightmapIndex = _valueVar.GetValue(_lightmapIndexValue);
					break;
				case Property.LightmapScaleOffset:
					trailRenderer.lightmapScaleOffset = _valueVar.GetValue(_lightmapScaleOffsetValue);
					break;
				case Property.Material:
					trailRenderer.material = (_valueVar.GetValue(_materialValue) as Material);
					break;
				case Property.ProbeAnchor:
					trailRenderer.probeAnchor = (_valueVar.GetValueAsComponent(_probeAnchorValue, typeof(Transform)) as Transform);
					break;
				case Property.RealtimeLightmapScaleOffset:
					trailRenderer.realtimeLightmapScaleOffset = _valueVar.GetValue(_realtimeLightmapScaleOffsetValue);
					break;
				case Property.ReceiveShadows:
					trailRenderer.receiveShadows = _valueVar.GetValue(_receiveShadowsValue);
					break;
				case Property.ReflectionProbeUsage:
					trailRenderer.reflectionProbeUsage = (ReflectionProbeUsage)(object)_valueVar.GetValue(_reflectionProbeUsageValue);
					break;
				case Property.ShadowCastingMode:
					trailRenderer.shadowCastingMode = (ShadowCastingMode)(object)_valueVar.GetValue(_shadowCastingModeValue);
					break;
				case Property.SharedMaterial:
					trailRenderer.sharedMaterial = (_valueVar.GetValue(_sharedMaterialValue) as Material);
					break;
				case Property.SortingLayerID:
					trailRenderer.sortingLayerID = _valueVar.GetValue(_sortingLayerIDValue);
					break;
				case Property.SortingLayerName:
					trailRenderer.sortingLayerName = _valueVar.GetValue(_sortingLayerNameValue);
					break;
				case Property.SortingOrder:
					trailRenderer.sortingOrder = _valueVar.GetValue(_sortingOrderValue);
					break;
				case Property.StartWidth:
					trailRenderer.startWidth = _valueVar.GetValue(_startWidthValue);
					break;
				case Property.Time:
					trailRenderer.time = _valueVar.GetValue(_timeValue);
					break;
				case Property.LightProbeUsage:
					trailRenderer.lightProbeUsage = (LightProbeUsage)(object)_valueVar.GetValue(_lightProbeUsageValue);
					break;
				}
			}
		}
	}
}
