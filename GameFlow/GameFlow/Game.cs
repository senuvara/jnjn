using UnityEngine;

namespace GameFlow
{
	public class Game
	{
		private static float prePauseTimeScale;

		public static bool started
		{
			get;
			private set;
		}

		public static bool paused
		{
			get;
			private set;
		}

		public static bool over
		{
			get;
			private set;
		}

		public static bool Pause()
		{
			if (!paused)
			{
				prePauseTimeScale = Time.timeScale;
				Time.timeScale = 0f;
				AudioListener.pause = true;
				paused = true;
				new GamePauseEvent().DispatchToAll();
				return true;
			}
			return false;
		}

		public static bool Resume()
		{
			if (paused)
			{
				Time.timeScale = prePauseTimeScale;
				AudioListener.pause = false;
				paused = false;
				new GameResumeEvent().DispatchToAll();
				return true;
			}
			return false;
		}

		public static void TogglePause()
		{
			if (!paused)
			{
				Pause();
			}
			else
			{
				Resume();
			}
		}

		public static bool Start()
		{
			if (!started)
			{
				started = true;
				over = false;
				new GameStartEvent().DispatchToAll();
				return true;
			}
			return false;
		}

		public static bool Finish()
		{
			if (started)
			{
				started = false;
				over = true;
				new GameOverEvent().DispatchToAll();
				return true;
			}
			return false;
		}
	}
}
