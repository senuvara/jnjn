using UnityEngine;

namespace GameFlow
{
	[DisallowMultipleComponent]
	[AddComponentMenu("GameFlow/Programs/On Language Set")]
	[Help("en", "Program that will be executed when the app language is set or changed.", null)]
	[Help("es", "Programa que se ejecutará cuando se establezca o cambie el idioma activo.", null)]
	public class OnLanguageSet : EventProgram
	{
		protected override void OnAwake()
		{
			base.OnAwake();
			GameSettings.Init();
		}

		protected override void RegisterAsListener()
		{
			LanguageEvent.AddListener(this);
		}

		protected override void UnRegisterAsListener()
		{
			LanguageEvent.RemoveListener(this);
		}
	}
}
