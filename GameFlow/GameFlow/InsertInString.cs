using UnityEngine;

namespace GameFlow
{
	[Help("es", "Inserta texto en la @GameFlow{Variable} especificada.", null)]
	[Help("en", "Inserts text in the specified @GameFlow{Variable}.", null)]
	[AddComponentMenu("")]
	public class InsertInString : Action, IExecutableInEditor
	{
		[SerializeField]
		private string _insert;

		[SerializeField]
		private Variable _insertVar;

		[SerializeField]
		private int _index;

		[SerializeField]
		private Variable _indexVar;

		[SerializeField]
		private Variable _variable;

		public override void Execute()
		{
			Execute(_insertVar.GetValue(_insert), _indexVar.GetValue(_index), _variable);
		}

		public static void Execute(string text, int index, Variable variable)
		{
			if (!(variable == null))
			{
				Variable variable2 = (!variable.isIndirect) ? variable : variable.indirection;
				if (!(variable2 == null))
				{
					variable2.SetValue(variable2.GetValue(string.Empty).Insert(index, text));
				}
			}
		}
	}
}
