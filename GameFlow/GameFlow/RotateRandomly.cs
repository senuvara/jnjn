using UnityEngine;

namespace GameFlow
{
	[Help("en", "Rotates the specified object randomly.", null)]
	[Help("es", "Rota el objeto especificado aleatoriamente.", null)]
	[AddComponentMenu("")]
	public class RotateRandomly : Rotate
	{
		[SerializeField]
		private Vector3 _min = new Vector3(-359.99f, -359.99f, -359.99f);

		[SerializeField]
		private Variable _minVar;

		[SerializeField]
		private Vector3 _max = new Vector3(359.99f, 359.99f, 359.99f);

		[SerializeField]
		private Variable _maxVar;

		public Vector3 min => _minVar.GetValue(_min);

		public Vector3 max => _maxVar.GetValue(_max);

		protected override void OnReset()
		{
			base.OnReset();
			base.relative = true;
			base.rotationTargetType = TargetType.Vector3;
		}

		protected override void SetupTargetValues()
		{
			Quaternion quaternion = Quaternion.Euler(RandomUtils.GetRandomVector3(min, max));
			if (base.relative)
			{
				toRotation = _t1.rotation * quaternion;
			}
			else
			{
				toRotation = quaternion;
			}
		}
	}
}
