using System.Collections.Generic;
using UnityEngine;

namespace GameFlow
{
	[Help("en", "Shows all the @GameFlow{Localization}s defined in the inspected @UnityManual{GameObject}.", null)]
	[Help("es", "Muestra todas las localizaciones (@GameFlow{Localization}) definidos en el @UnityManual{GameObject}.", null)]
	[AddComponentMenu("")]
	public class Localizations : Container, IBlockContainer, IData, ILocalizationContainer
	{
		[SerializeField]
		private List<Localization> _localizations = new List<Localization>();

		private static Dictionary<string, Localization> localizationDictionary;

		public List<Localization> localizations => _localizations;

		public List<Localization> GetLocalizations()
		{
			return _localizations;
		}

		public virtual void LocalizationAdded(Localization localization)
		{
		}

		public override List<Block> GetBlocks()
		{
			return new List<Block>(_localizations.ToArray());
		}

		public override bool Contains(Block block)
		{
			Localization localization = block as Localization;
			if (localization != null)
			{
				return _localizations.Contains(localization);
			}
			return false;
		}

		private static void InitDictionary()
		{
			if (localizationDictionary != null)
			{
				return;
			}
			localizationDictionary = new Dictionary<string, Localization>();
			Localization[] array = Resources.FindObjectsOfTypeAll<Localization>();
			Localization[] array2 = array;
			foreach (Localization localization in array2)
			{
				if (localization != null && !localizationDictionary.ContainsKey(localization.id))
				{
					localizationDictionary.Add(localization.id, localization);
				}
			}
		}

		public static Localization GetLocalizationById(string id)
		{
			//Discarded unreachable code: IL_0023, IL_0030
			if (id == null)
			{
				id = string.Empty;
			}
			InitDictionary();
			try
			{
				return localizationDictionary[id];
			}
			catch
			{
				return null;
			}
		}

		public static Localization[] GetAllLocalizations()
		{
			InitDictionary();
			return new List<Localization>(localizationDictionary.Values).ToArray();
		}
	}
}
