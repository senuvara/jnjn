using UnityEngine;

namespace GameFlow
{
	[Help("es", "Modifica una propiedad del componente @UnityManual{Skybox} especificado.", null)]
	[AddComponentMenu("")]
	[Help("en", "Sets a property of the specified @UnityManual{Skybox}.", null)]
	public class SetSkyboxProperty : Action, IExecutableInEditor
	{
		public enum Property
		{
			Material
		}

		[SerializeField]
		private Skybox _target;

		[SerializeField]
		private Variable _targetVar;

		[SerializeField]
		private Property _property;

		[SerializeField]
		private Material _materialValue;

		[SerializeField]
		private Variable _valueVar;

		protected override void OnReset()
		{
			_target = base.gameObject.GetComponent<Skybox>();
		}

		public override void Setup()
		{
		}

		public override void Execute()
		{
			Skybox skybox = _targetVar.GetValueAsComponent(_target, typeof(Skybox)) as Skybox;
			if (!(skybox == null) && _property == Property.Material)
			{
				skybox.material = (_valueVar.GetValue(_materialValue) as Material);
			}
		}
	}
}
