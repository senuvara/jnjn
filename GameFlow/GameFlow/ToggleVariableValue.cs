using UnityEngine;

namespace GameFlow
{
	[AddComponentMenu("")]
	[Help("en", "Toggles the boolean-value of the specified @GameFlow{Variable}.", null)]
	[Help("es", "Cambia el valor booleano de una @GameFlow{Variable} especificada por su contrario.", null)]
	public class ToggleVariableValue : VariableAction
	{
		public override void Execute()
		{
			Execute(base.variable);
		}

		public static void Execute(Variable variable)
		{
			if (variable == null)
			{
				return;
			}
			Variable variable2 = (!variable.isIndirect) ? variable : variable.indirection;
			if (!(variable2 == null) && !variable2.isReadOnly)
			{
				switch (variable2.dataType)
				{
				case DataType.Boolean:
					variable2.boolValue = !variable2.boolValue;
					break;
				case DataType.Toggle:
					variable2.toggleValue = !variable2.toggleValue;
					break;
				}
			}
		}
	}
}
