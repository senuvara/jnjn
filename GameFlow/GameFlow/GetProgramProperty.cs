using UnityEngine;

namespace GameFlow
{
	[Help("es", "Lee una propiedad del programa (@UnityManual{Program}) especificado.", null)]
	[AddComponentMenu("")]
	[Help("en", "Gets a property of the specified @GameFlow{Program}.", null)]
	public class GetProgramProperty : Action, IExecutableInEditor
	{
		public enum Property
		{
			Finished,
			Id,
			IgnorePause,
			Running
		}

		[SerializeField]
		private Program _source;

		[SerializeField]
		private Variable _sourceVar;

		[SerializeField]
		private Property _property;

		[SerializeField]
		private Variable _output;

		protected override void OnReset()
		{
			_source = base.gameObject.GetComponent<Program>();
		}

		public override void Setup()
		{
		}

		public override void Execute()
		{
			Program program = _sourceVar.GetValueAsComponent(_source, typeof(Program)) as Program;
			if (!(program == null))
			{
				switch (_property)
				{
				case Property.Finished:
					_output.SetValue(program.finished);
					break;
				case Property.Id:
					_output.SetValue(program.id);
					break;
				case Property.IgnorePause:
					_output.SetValue(program.ignorePause);
					break;
				case Property.Running:
					_output.SetValue(program.running);
					break;
				}
			}
		}
	}
}
