using UnityEngine;

namespace GameFlow
{
	[Help("es", "Evalúa el tipo de dispositivo en el que se esta ejecutando la aplicación.", null)]
	[Help("en", "Evaluates the kind of device the application is running on.", null)]
	[AddComponentMenu("")]
	public class DeviceTypeCondition : Condition
	{
		public enum Comparison
		{
			IsDesktop,
			IsNotDesktop,
			IsMobile,
			IsNotMobile,
			IsConsole,
			IsNotConsole
		}

		[SerializeField]
		private Comparison _comparison;

		public override bool Evaluate()
		{
			switch (_comparison)
			{
			case Comparison.IsDesktop:
				return SystemInfo.deviceType == DeviceType.Desktop;
			case Comparison.IsNotDesktop:
				return SystemInfo.deviceType != DeviceType.Desktop;
			case Comparison.IsMobile:
				return SystemInfo.deviceType == DeviceType.Handheld;
			case Comparison.IsNotMobile:
				return SystemInfo.deviceType != DeviceType.Handheld;
			case Comparison.IsConsole:
				return SystemInfo.deviceType == DeviceType.Console;
			case Comparison.IsNotConsole:
				return SystemInfo.deviceType != DeviceType.Console;
			default:
				return false;
			}
		}
	}
}
