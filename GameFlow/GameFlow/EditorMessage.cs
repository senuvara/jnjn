namespace GameFlow
{
	public class EditorMessage
	{
		private string _text;

		public string[] parameters;

		public MessageType type
		{
			get;
			private set;
		}

		public string text
		{
			get
			{
				//Discarded unreachable code: IL_002d, IL_003f
				try
				{
					return (parameters != null) ? string.Format(_text, parameters) : _text;
				}
				catch
				{
					return _text;
				}
			}
		}

		public EditorMessage(MessageType type, string text)
		{
			this.type = type;
			_text = text;
		}

		public static EditorMessage Info(string text)
		{
			return new EditorMessage(MessageType.Info, text);
		}

		public static EditorMessage Warning(string text)
		{
			return new EditorMessage(MessageType.Warning, text);
		}

		public static EditorMessage Error(string text)
		{
			return new EditorMessage(MessageType.Error, text);
		}
	}
}
