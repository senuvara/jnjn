using UnityEngine;
using UnityEngine.UI;

namespace GameFlow
{
	[Help("en", "Sets a property of the specified @UnityManual{Outline}.", null)]
	[Help("es", "Modifica una propiedad del componente @UnityManual{Outline} especificado.", null)]
	[AddComponentMenu("")]
	public class SetOutlineProperty : Action, IExecutableInEditor
	{
		public enum Property
		{
			EffectColor,
			EffectDistance,
			UseGraphicAlpha
		}

		[SerializeField]
		private Outline _target;

		[SerializeField]
		private Variable _targetVar;

		[SerializeField]
		private Property _property;

		[SerializeField]
		private Color _effectColorValue;

		[SerializeField]
		private Vector2 _effectDistanceValue;

		[SerializeField]
		private bool _useGraphicAlphaValue;

		[SerializeField]
		private Variable _valueVar;

		protected override void OnReset()
		{
			_target = base.gameObject.GetComponent<Outline>();
		}

		public override void Setup()
		{
		}

		public override void Execute()
		{
			Outline outline = _targetVar.GetValueAsComponent(_target, typeof(Outline)) as Outline;
			if (!(outline == null))
			{
				switch (_property)
				{
				case Property.EffectColor:
					outline.effectColor = _valueVar.GetValue(_effectColorValue);
					break;
				case Property.EffectDistance:
					outline.effectDistance = _valueVar.GetValue(_effectDistanceValue);
					break;
				case Property.UseGraphicAlpha:
					outline.useGraphicAlpha = _valueVar.GetValue(_useGraphicAlphaValue);
					break;
				}
			}
		}
	}
}
