using System;
using System.Reflection;
using UnityEngine;

namespace GameFlow
{
	[Help("es", "Obtiene el valor de la variable de script especificada usando Reflection.", null)]
	[AddComponentMenu("")]
	[Help("en", "Gets the value of the specified script variable using Reflection.", null)]
	public class GetScriptProperty : Action, IExecutableInEditor
	{
		[SerializeField]
		private MonoBehaviour _target;

		[SerializeField]
		private Variable _targetVar;

		[SerializeField]
		private string _property;

		[SerializeField]
		private Variable _propertyVar;

		[SerializeField]
		private Variable _output;

		public MonoBehaviour target => _targetVar.GetValue(_target) as MonoBehaviour;

		public string property => _propertyVar.GetValue(_property);

		public override void Execute()
		{
			Execute(target, property, _output);
		}

		public static void Execute(MonoBehaviour target, string property, Variable output)
		{
			if (target == null || property == null || output == null)
			{
				return;
			}
			FieldInfo field = target.GetType().GetField(property);
			if (field != null)
			{
				Type fieldType = field.FieldType;
				DataType dataType = fieldType.ToDataType();
				object value = field.GetValue(target);
				switch (dataType)
				{
				case DataType.String:
					output.SetValue(Convert.ToString(value));
					break;
				case DataType.Tag:
					output.SetTagValue(Convert.ToString(value));
					break;
				case DataType.Integer:
					output.SetValue(Convert.ToInt32(value));
					break;
				case DataType.Layer:
					output.SetLayerValue(Convert.ToInt32(value));
					break;
				case DataType.Float:
					output.SetValue((float)Convert.ToDouble(value));
					break;
				case DataType.Boolean:
					output.SetValue(Convert.ToBoolean(value));
					break;
				case DataType.Toggle:
					output.SetToggleValue(Convert.ToBoolean(value));
					break;
				case DataType.Vector2:
					output.SetValue((Vector2)value);
					break;
				case DataType.Vector3:
					output.SetValue((Vector3)value);
					break;
				case DataType.Vector4:
					output.SetValue((Vector4)value);
					break;
				case DataType.Rect:
					output.SetValue((Rect)value);
					break;
				case DataType.Color:
					output.SetValue((Color)value);
					break;
				case DataType.Object:
					output.SetValue((UnityEngine.Object)value, fieldType);
					break;
				case DataType.Enum:
					output.SetValue((Enum)Enum.ToObject(fieldType, Convert.ToInt32(value)));
					break;
				case DataType.AnimationCurve:
					output.SetValue((AnimationCurve)value);
					break;
				case DataType.Bounds:
					output.SetValue((Bounds)value);
					break;
				case DataType.Quaternion:
					output.SetValue((Quaternion)value);
					break;
				}
			}
		}
	}
}
