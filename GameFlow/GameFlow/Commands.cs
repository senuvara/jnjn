using System.Collections.Generic;
using UnityEngine;

namespace GameFlow
{
	[DisallowMultipleComponent]
	[AddComponentMenu("")]
	[Help("en", "Shows all the @GameFlow{Command} blocks defined in the inspected @UnityManual{GameObject}.", null)]
	[Help("es", "Muestra todos los bloques @GameFlow{Command} definidos en el @UnityManual{GameObject} inspeccionado.", null)]
	public class Commands : Container, IBlockContainer, ICommandContainer, ITool
	{
		[SerializeField]
		private List<Command> _commands = new List<Command>();

		[SerializeField]
		private bool _editorMode = true;

		public List<Command> GetCommands()
		{
			return _commands;
		}

		public virtual void CommandAdded(Command command)
		{
		}

		public override List<Block> GetBlocks()
		{
			return new List<Block>(_commands.ToArray());
		}

		public override bool Contains(Block block)
		{
			Command command = block as Command;
			if (command != null)
			{
				return _commands.Contains(command);
			}
			return false;
		}

		public static Command GetCommandById(string id, GameObject go)
		{
			if (id == null || go == null)
			{
				return null;
			}
			Command[] components = go.GetComponents<Command>();
			foreach (Command command in components)
			{
				if (command != null && command.id == id)
				{
					return command;
				}
			}
			return null;
		}
	}
}
