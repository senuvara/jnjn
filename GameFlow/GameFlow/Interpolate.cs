using UnityEngine;

namespace GameFlow
{
	[Help("es", "Interpola el componente @UnityManual{Transform} especificado hasta encajar con otro dado.", null)]
	[AddComponentMenu("")]
	[Help("en", "Interpolates the specified @UnityManual{Transform} component to match another.", null)]
	public class Interpolate : TimeAction, IExecutableInEditor
	{
		public enum TargetType
		{
			Vector3,
			Transform
		}

		[SerializeField]
		private Transform _target;

		[SerializeField]
		private Variable _targetVar;

		[SerializeField]
		private Transform _toTarget;

		[SerializeField]
		private Variable _toTargetVar;

		[SerializeField]
		private TargetType _positionTargetType = TargetType.Transform;

		[SerializeField]
		private Vector3 _toPosition;

		[SerializeField]
		private Variable _toPositionVar;

		[SerializeField]
		private TargetType _rotationTargetType = TargetType.Transform;

		[SerializeField]
		private Vector3 _toRotation;

		[SerializeField]
		private Variable _toRotationVar;

		[SerializeField]
		private TargetType _scaleTargetType = TargetType.Transform;

		[SerializeField]
		private Vector3 _toScale = new Vector3(1f, 1f, 1f);

		[SerializeField]
		private Variable _toScaleVar;

		[SerializeField]
		private bool _relative;

		[SerializeField]
		private Space _space;

		[SerializeField]
		private Variable _spaceVar;

		[SerializeField]
		private float _multiplier = 1f;

		[SerializeField]
		private Variable _multiplierVar;

		[SerializeField]
		private int _interpolation = 7;

		[SerializeField]
		private Variable _interpolationVar;

		[SerializeField]
		private EasingType _easingType;

		protected bool _finished;

		protected Transform _t1;

		protected Transform _t2;

		protected Vector3 startPosition;

		protected Quaternion startRotation;

		protected Vector3 startScale;

		protected bool interpolatePosition;

		protected bool interpolateRotation;

		protected bool interpolateScale;

		protected Vector3 toPosition;

		protected Quaternion toRotation;

		protected Vector3 toScale;

		public Transform target => _targetVar.GetValueAsComponent(_target, typeof(Transform)) as Transform;

		public Transform toTarget => _toTargetVar.GetValueAsComponent(_toTarget, typeof(Transform)) as Transform;

		protected TargetType positionTargetType
		{
			get
			{
				return _positionTargetType;
			}
			set
			{
				_positionTargetType = value;
			}
		}

		protected TargetType rotationTargetType
		{
			get
			{
				return _rotationTargetType;
			}
			set
			{
				_rotationTargetType = value;
			}
		}

		protected TargetType scaleTargetType
		{
			get
			{
				return _scaleTargetType;
			}
			set
			{
				_scaleTargetType = value;
			}
		}

		protected bool relative
		{
			get
			{
				return _relative;
			}
			set
			{
				_relative = value;
			}
		}

		public Space space => (Space)(object)_spaceVar.GetValue(_space);

		public float multiplier => _multiplierVar.GetValue(_multiplier);

		public Interpolation interpolation
		{
			get
			{
				return (Interpolation)_interpolationVar.GetValue(_interpolation);
			}
			set
			{
				_interpolation = (int)value;
				_interpolationVar = null;
			}
		}

		protected EasingType easingType
		{
			get
			{
				return _easingType;
			}
			set
			{
				_easingType = value;
			}
		}

		protected override void OnReset()
		{
			base.OnReset();
			_target = base.gameObject.GetComponent<Transform>();
		}

		protected virtual void SetupTargetValues()
		{
			if (relative)
			{
				toPosition = _t1.position + _toPositionVar.GetValue(_toPosition);
				toRotation = _t1.rotation * Quaternion.Euler(_toRotationVar.GetValue(_toRotation));
				toScale = _t1.localScale + _toScaleVar.GetValue(_toScale);
			}
			else
			{
				toPosition = _toPositionVar.GetValue(_toPosition);
				toRotation = Quaternion.Euler(_toRotationVar.GetValue(_toRotation));
				toScale = _toScaleVar.GetValue(_toScale);
			}
		}

		public override void FirstStep()
		{
			_finished = false;
			_t1 = target;
			if (_t1 == null)
			{
				_finished = true;
				return;
			}
			_t2 = toTarget;
			SetupTargetValues();
			interpolatePosition = ((interpolation & Interpolation.Position) != 0);
			interpolateRotation = ((interpolation & Interpolation.Rotation) != 0);
			interpolateScale = ((interpolation & Interpolation.Scale) != 0);
			if (base.duration == 0f || !Application.isPlaying)
			{
				if (interpolatePosition)
				{
					switch (positionTargetType)
					{
					case TargetType.Vector3:
						switch (space)
						{
						case Space.World:
							_t1.position = toPosition;
							break;
						case Space.Self:
							_t1.localPosition = toPosition;
							break;
						}
						break;
					case TargetType.Transform:
						if (_t2 != null)
						{
							_t1.position = _t2.position;
						}
						break;
					}
				}
				if (interpolateRotation)
				{
					switch (rotationTargetType)
					{
					case TargetType.Vector3:
						switch (space)
						{
						case Space.World:
							_t1.rotation = toRotation;
							break;
						case Space.Self:
							_t1.localRotation = toRotation;
							break;
						}
						break;
					case TargetType.Transform:
						if (_t2 != null)
						{
							_t1.rotation = _t2.rotation;
						}
						break;
					}
				}
				if (interpolateScale)
				{
					switch (scaleTargetType)
					{
					case TargetType.Vector3:
						_t1.localScale = toScale;
						break;
					case TargetType.Transform:
						if (_t2 != null)
						{
							_t1.localScale = _t2.localScale;
						}
						break;
					}
				}
				_finished = true;
			}
			else
			{
				switch (space)
				{
				case Space.World:
					startPosition = _t1.position;
					startRotation = _t1.rotation;
					startScale = _t1.localScale;
					break;
				case Space.Self:
					startPosition = _t1.localPosition;
					startRotation = _t1.localRotation;
					startScale = _t1.localScale;
					break;
				}
				base.FirstStep();
			}
		}

		public override void Step()
		{
			float elapsedTimeDelta = base.timer.elapsedTimeDelta;
			if (_t1 == null || _t2 == null)
			{
				_finished = true;
				return;
			}
			if (interpolatePosition)
			{
				_t1.position = MathUtils.Ease(startPosition, _t2.position, elapsedTimeDelta, easingType);
			}
			if (interpolateRotation)
			{
				_t1.rotation = Quaternion.Slerp(startRotation, _t2.rotation, MathUtils.EaseDelta(elapsedTimeDelta, easingType));
			}
			if (interpolateScale)
			{
				_t1.localScale = MathUtils.Ease(startScale, _t2.localScale, elapsedTimeDelta, easingType);
			}
		}

		public override bool Finished()
		{
			return _finished || base.Finished();
		}
	}
}
