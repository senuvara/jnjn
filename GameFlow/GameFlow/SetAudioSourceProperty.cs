using UnityEngine;

namespace GameFlow
{
	[Help("es", "Modifica una propiedad del componente @UnityManual{AudioSource} especificado.", null)]
	[Help("en", "Sets a property of the specified @UnityManual{AudioSource} component.", null)]
	[AddComponentMenu("")]
	public class SetAudioSourceProperty : Action, IExecutableInEditor
	{
		public enum Property
		{
			BypassEffects,
			BypassListenerEffects,
			BypassReverbZones,
			Clip,
			DopplerLevel,
			IgnoreListenerPause,
			IgnoreListenerVolume,
			Loop,
			MaxDistance,
			MinDistance,
			Mute,
			PanStereo,
			PanLevel,
			Pitch,
			PlayOnAwake,
			Priority,
			ReverbZoneMix,
			RolloffMode,
			SpatialBlend,
			Spatialize,
			Spread,
			Time,
			TimeSamples,
			VelocityUpdateMode,
			Volume
		}

		[SerializeField]
		private AudioSource _target;

		[SerializeField]
		private Variable _targetVar;

		[SerializeField]
		private Property _property = Property.Mute;

		[SerializeField]
		private bool _bypassEffectsValue;

		[SerializeField]
		private bool _bypassListenerEffectsValue;

		[SerializeField]
		private bool _bypassReverbZonesValue;

		[SerializeField]
		private AudioClip _clipValue;

		[SerializeField]
		private float _dopplerLevelValue;

		[SerializeField]
		private bool _ignoreListenerPauseValue;

		[SerializeField]
		private bool _ignoreListenerVolumeValue;

		[SerializeField]
		private bool _loopValue;

		[SerializeField]
		private float _maxDistanceValue;

		[SerializeField]
		private float _minDistanceValue;

		[SerializeField]
		private bool _muteValue;

		[SerializeField]
		private float _pitchValue;

		[SerializeField]
		private bool _playOnAwakeValue;

		[SerializeField]
		private int _priorityValue;

		[SerializeField]
		private AudioRolloffMode _rolloffModeValue;

		[SerializeField]
		private float _spreadValue;

		[SerializeField]
		private float _timeValue;

		[SerializeField]
		private int _timeSamplesValue;

		[SerializeField]
		private AudioVelocityUpdateMode _velocityUpdateModeValue;

		[SerializeField]
		private float _volumeValue;

		[SerializeField]
		private Variable _valueVar;

		protected override void OnReset()
		{
			_target = base.gameObject.GetComponent<AudioSource>();
		}

		public override void Setup()
		{
		}

		public override void Execute()
		{
			AudioSource audioSource = _targetVar.GetValueAsComponent(_target, typeof(AudioSource)) as AudioSource;
			if (!(audioSource == null))
			{
				switch (_property)
				{
				case Property.PanStereo:
					break;
				case Property.PanLevel:
					break;
				case Property.ReverbZoneMix:
					break;
				case Property.SpatialBlend:
					break;
				case Property.Spatialize:
					break;
				case Property.BypassEffects:
					audioSource.bypassEffects = _valueVar.GetValue(_bypassEffectsValue);
					break;
				case Property.BypassListenerEffects:
					audioSource.bypassListenerEffects = _valueVar.GetValue(_bypassListenerEffectsValue);
					break;
				case Property.BypassReverbZones:
					audioSource.bypassReverbZones = _valueVar.GetValue(_bypassReverbZonesValue);
					break;
				case Property.Clip:
					audioSource.clip = (_valueVar.GetValue(_clipValue) as AudioClip);
					break;
				case Property.DopplerLevel:
					audioSource.dopplerLevel = _valueVar.GetValue(_dopplerLevelValue);
					break;
				case Property.IgnoreListenerPause:
					audioSource.ignoreListenerPause = _valueVar.GetValue(_ignoreListenerPauseValue);
					break;
				case Property.IgnoreListenerVolume:
					audioSource.ignoreListenerVolume = _valueVar.GetValue(_ignoreListenerVolumeValue);
					break;
				case Property.Loop:
					audioSource.loop = _valueVar.GetValue(_loopValue);
					break;
				case Property.MaxDistance:
					audioSource.maxDistance = _valueVar.GetValue(_maxDistanceValue);
					break;
				case Property.MinDistance:
					audioSource.minDistance = _valueVar.GetValue(_minDistanceValue);
					break;
				case Property.Mute:
					audioSource.mute = _valueVar.GetValue(_muteValue);
					break;
				case Property.Pitch:
					audioSource.pitch = _valueVar.GetValue(_pitchValue);
					break;
				case Property.PlayOnAwake:
					audioSource.playOnAwake = _valueVar.GetValue(_playOnAwakeValue);
					break;
				case Property.Priority:
					audioSource.priority = _valueVar.GetValue(_priorityValue);
					break;
				case Property.RolloffMode:
					audioSource.rolloffMode = (AudioRolloffMode)(object)_valueVar.GetValue(_rolloffModeValue);
					break;
				case Property.Spread:
					audioSource.spread = _valueVar.GetValue(_spreadValue);
					break;
				case Property.Time:
					audioSource.time = _valueVar.GetValue(_timeValue);
					break;
				case Property.TimeSamples:
					audioSource.timeSamples = _valueVar.GetValue(_timeSamplesValue);
					break;
				case Property.VelocityUpdateMode:
					audioSource.velocityUpdateMode = (AudioVelocityUpdateMode)(object)_valueVar.GetValue(_velocityUpdateModeValue);
					break;
				case Property.Volume:
					audioSource.volume = _valueVar.GetValue(_volumeValue);
					break;
				}
			}
		}
	}
}
