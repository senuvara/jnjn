using UnityEngine;

namespace GameFlow
{
	[AddComponentMenu("")]
	[Help("es", "Ajusta la escala de tiempo para lograr efectos de des/aceleracion durante el juego.", null)]
	[Help("en", "Sets the time scale for de/acceleration effects during the game.", null)]
	public class SetTimeScale : Action, IExecutableInEditor
	{
		[SerializeField]
		private float _scale;

		[SerializeField]
		private Variable _scaleVar;

		protected override void OnReset()
		{
			_scale = Time.timeScale;
		}

		public override void Execute()
		{
			Time.timeScale = _scaleVar.GetValue(_scale);
		}
	}
}
