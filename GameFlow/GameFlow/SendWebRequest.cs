using UnityEngine;

namespace GameFlow
{
	[AddComponentMenu("")]
	[Help("en", "Sends the specified web request and optionally waits for a response.", null)]
	[Help("es", "Envía la petición web especificada y espera (opcionalmente) hasta que sea respondida.", null)]
	public class SendWebRequest : Action
	{
		public enum ResponseType
		{
			Audio,
			Error,
			Image,
			Movie,
			Success,
			Text
		}

		[SerializeField]
		private WebRequest _request;

		[SerializeField]
		private Variable _requestVar;

		[SerializeField]
		private bool _await = true;

		[SerializeField]
		private float _timeout = 10f;

		[SerializeField]
		private ResponseType _responseType = ResponseType.Success;

		[SerializeField]
		private Variable _output;

		private float _realtimeStart;

		public override void FirstStep()
		{
			_request = (_requestVar.GetValue(_request) as WebRequest);
			if (_request != null)
			{
				_request.Send();
				if (_await)
				{
					_realtimeStart = Time.realtimeSinceStartup;
				}
			}
		}

		public override bool Finished()
		{
			if (_request == null)
			{
				return true;
			}
			if (!_await)
			{
				return true;
			}
			bool isDone = _request.isDone;
			if (!isDone && Time.realtimeSinceStartup - _realtimeStart >= _timeout)
			{
				_request.Cancel();
				return true;
			}
			if (isDone)
			{
				switch (_responseType)
				{
				case ResponseType.Audio:
					_output.SetValue(_request.responseAsAudio);
					break;
				case ResponseType.Error:
					_output.SetValue(_request.error);
					break;
				case ResponseType.Image:
					_output.SetValue(_request.responseAsImage);
					break;
				case ResponseType.Success:
					_output.SetValue(_request.successful);
					break;
				case ResponseType.Text:
					_output.SetValue(_request.responseAsText);
					break;
				}
			}
			return isDone;
		}
	}
}
