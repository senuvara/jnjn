using UnityEngine;

namespace GameFlow
{
	[AddComponentMenu("")]
	[Help("en", "Evaluates the angle between two objects.", null)]
	[Help("es", "Evalúa el ángulo entre dos objetos.", null)]
	public class AngleCondition : Condition
	{
		public enum Comparison
		{
			IsEqualTo,
			IsNotEqualTo,
			IsGreaterThan,
			IsGreaterThanOrEqualTo,
			IsLessThan,
			IsLessThanOrEqualTo
		}

		[SerializeField]
		private Transform _t1;

		[SerializeField]
		private Variable _t1Var;

		[SerializeField]
		private TransformUtils.Direction _direction;

		[SerializeField]
		private Variable _directionVar;

		[SerializeField]
		private Transform _t2;

		[SerializeField]
		private Variable _t2Var;

		[SerializeField]
		private float _floatValue;

		[SerializeField]
		private Variable _floatValueVar;

		[SerializeField]
		private Comparison _comparison;

		protected override void OnReset()
		{
			_t1 = base.gameObject.GetComponent<Transform>();
		}

		public override bool Evaluate()
		{
			Transform transform = _t1Var.GetValueAsComponent(_t1, typeof(Transform)) as Transform;
			Transform transform2 = _t2Var.GetValueAsComponent(_t2, typeof(Transform)) as Transform;
			if (transform == null || transform2 == null)
			{
				return false;
			}
			TransformUtils.Direction direction = (TransformUtils.Direction)(object)_directionVar.GetValue(_direction);
			float num = Vector3.Angle(transform2.position - transform.position, transform.GetDirection(direction));
			float value = _floatValueVar.GetValue(_floatValue);
			switch (_comparison)
			{
			case Comparison.IsEqualTo:
				return Mathf.Approximately(num, value);
			case Comparison.IsNotEqualTo:
				return !Mathf.Approximately(num, value);
			case Comparison.IsGreaterThan:
				return num > value;
			case Comparison.IsGreaterThanOrEqualTo:
				return num >= value;
			case Comparison.IsLessThan:
				return num < value;
			case Comparison.IsLessThanOrEqualTo:
				return num <= value;
			default:
				return false;
			}
		}
	}
}
