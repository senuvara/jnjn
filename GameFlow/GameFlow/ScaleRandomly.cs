using UnityEngine;

namespace GameFlow
{
	[Help("es", "Escala el objeto especificado hasta un tamaño aleatorio.", null)]
	[AddComponentMenu("")]
	[Help("en", "Scales the specified object to a random scale.", null)]
	public class ScaleRandomly : Scale
	{
		[SerializeField]
		private Vector3 _min = new Vector3(0.01f, 0.01f, 0.01f);

		[SerializeField]
		private Variable _minVar;

		[SerializeField]
		private Vector3 _max = new Vector3(5f, 5f, 5f);

		[SerializeField]
		private Variable _maxVar;

		public Vector3 min => _minVar.GetValue(_min);

		public Vector3 max => _maxVar.GetValue(_max);

		protected override void OnReset()
		{
			base.OnReset();
			base.scaleTargetType = TargetType.Vector3;
		}

		protected override void SetupTargetValues()
		{
			Vector3 randomVector = RandomUtils.GetRandomVector3(min, max);
			toScale = ((!base.relative) ? randomVector : (_t1.localScale + randomVector));
		}
	}
}
