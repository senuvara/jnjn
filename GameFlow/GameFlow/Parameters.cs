using System.Collections.Generic;
using UnityEngine;

namespace GameFlow
{
	[AddComponentMenu("")]
	[Help("en", "Shows all the @GameFlow{Parameter} blocks defined in the inspected @UnityManual{GameObject}.", null)]
	[Help("es", "Muestra todos los bloques @GameFlow{Parameter} definidos en el @UnityManual{GameObject} inspeccionado.", null)]
	[DisallowMultipleComponent]
	public class Parameters : Container, IBlockContainer, IData, IParameterContainer
	{
		[SerializeField]
		private List<Parameter> _parameters = new List<Parameter>();

		[SerializeField]
		private bool _actionsFoldout = true;

		[SerializeField]
		private bool _editorMode;

		private static Dictionary<string, Parameter> parameterDictionary;

		public List<Parameter> GetParameters()
		{
			return _parameters;
		}

		public virtual void ParameterAdded(Parameter parameter)
		{
			OnParameterChange[] componentsInChildren = base.gameObject.GetComponentsInChildren<OnParameterChange>();
			foreach (OnParameterChange onParameterChange in componentsInChildren)
			{
				if (onParameterChange != null)
				{
					parameter.AddListener(onParameterChange);
				}
			}
		}

		public override List<Block> GetBlocks()
		{
			return new List<Block>(_parameters.ToArray());
		}

		public override bool Contains(Block block)
		{
			Parameter parameter = block as Parameter;
			if (parameter != null)
			{
				return _parameters.Contains(parameter);
			}
			return false;
		}

		private static void InitDictionary()
		{
			if (parameterDictionary != null)
			{
				return;
			}
			parameterDictionary = new Dictionary<string, Parameter>();
			Parameter[] array = Resources.FindObjectsOfTypeAll<Parameter>();
			Parameter[] array2 = array;
			foreach (Parameter parameter in array2)
			{
				if (parameter != null && !parameter.isPrivate && !parameterDictionary.ContainsKey(parameter.id))
				{
					parameterDictionary.Add(parameter.id, parameter);
				}
			}
		}

		public static Parameter GetParameterById(string id)
		{
			//Discarded unreachable code: IL_0023, IL_0030
			if (id == null)
			{
				id = string.Empty;
			}
			InitDictionary();
			try
			{
				return parameterDictionary[id];
			}
			catch
			{
				return null;
			}
		}

		public static Parameter GetParameterById(string id, GameObject go, bool recursive = false)
		{
			if (id == null)
			{
				id = string.Empty;
			}
			if (go == null)
			{
				return null;
			}
			Parameter[] array = recursive ? go.GetComponentsInChildren<Parameter>() : go.GetComponents<Parameter>();
			foreach (Parameter parameter in array)
			{
				if (parameter != null && parameter.id == id)
				{
					return parameter;
				}
			}
			return null;
		}

		public static Parameter[] GetAllParameters()
		{
			InitDictionary();
			return new List<Parameter>(parameterDictionary.Values).ToArray();
		}
	}
}
