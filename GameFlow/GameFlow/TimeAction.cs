using UnityEngine;

namespace GameFlow
{
	public abstract class TimeAction : Action
	{
		[SerializeField]
		private float _duration;

		[SerializeField]
		private Variable _durationVar;

		private TimerObject _timer;

		public float duration
		{
			get
			{
				return _durationVar.GetValue(_duration);
			}
			protected set
			{
				_duration = value;
				_durationVar = null;
			}
		}

		public bool isInstantaneous => _durationVar == null && _duration == 0f;

		public TimerObject timer => _timer;

		public float elapsedTime => _timer.elapsedTime;

		public bool expired => _timer.expired;

		protected override void OnReset()
		{
			_duration = GetDefaultDuration();
		}

		public virtual float GetDefaultDuration()
		{
			return 1f;
		}

		public override void Setup()
		{
			Program program = GetRootContainer() as Program;
			if (program != null)
			{
				bool flag = program as OnUpdate != null;
				flag |= (program as OnFixedUpdate != null);
				flag |= (program as OnLateUpdate != null);
				flag |= (program as OnAwake != null);
				if (flag | (program as OnDestroy != null))
				{
					_duration = 0f;
				}
			}
		}

		protected void StartTimer()
		{
			Program program = GetRootContainer() as Program;
			_timer = new TimerObject(_durationVar.GetValue(_duration), program != null && program.ignorePause);
			_timer.Restart();
		}

		public override void FirstStep()
		{
			StartTimer();
			Step();
		}

		public override bool Finished()
		{
			if (_timer == null)
			{
				return true;
			}
			return _timer.expired;
		}
	}
}
