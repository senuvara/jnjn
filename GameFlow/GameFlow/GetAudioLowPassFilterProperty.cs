using UnityEngine;

namespace GameFlow
{
	[AddComponentMenu("")]
	[Help("es", "Lee una propiedad del componente @UnityManual{AudioLowPassFilter} especificado.", null)]
	[Help("en", "Gets a property of the specified @UnityManual{AudioLowPassFilter} component.", null)]
	public class GetAudioLowPassFilterProperty : Action, IExecutableInEditor
	{
		public enum Property
		{
			CustomCutoffCurve,
			CutoffFrequency,
			LowpassResonanceQ
		}

		[SerializeField]
		private AudioLowPassFilter _source;

		[SerializeField]
		private Variable _sourceVar;

		[SerializeField]
		private Property _property;

		[SerializeField]
		private Variable _output;

		protected override void OnReset()
		{
			_source = base.gameObject.GetComponent<AudioLowPassFilter>();
		}

		public override void Setup()
		{
		}

		public override void Execute()
		{
			AudioLowPassFilter audioLowPassFilter = _sourceVar.GetValueAsComponent(_source, typeof(AudioLowPassFilter)) as AudioLowPassFilter;
			if (!(audioLowPassFilter == null))
			{
				switch (_property)
				{
				case Property.CustomCutoffCurve:
					_output.SetValue(audioLowPassFilter.customCutoffCurve);
					break;
				case Property.CutoffFrequency:
					_output.SetValue(audioLowPassFilter.cutoffFrequency);
					break;
				case Property.LowpassResonanceQ:
					_output.SetValue(audioLowPassFilter.lowpassResonanceQ);
					break;
				}
			}
		}
	}
}
