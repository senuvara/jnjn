using UnityEngine;

namespace GameFlow
{
	[Help("en", "Sets a property of the specified @UnityManual{AreaEffector2D} component.", null)]
	[Help("es", "Modifica una propiedad del componente @UnityManual{AreaEffector2D} especificado.", null)]
	[AddComponentMenu("")]
	public class SetAreaEffector2DProperty : Action, IExecutableInEditor
	{
		public enum Property
		{
			AngularDrag,
			ColliderMask,
			Drag,
			ForceAngle,
			ForceMagnitude,
			ForceTarget,
			ForceVariation,
			UseColliderMask,
			UseGlobalAngle
		}

		[SerializeField]
		private AreaEffector2D _target;

		[SerializeField]
		private Variable _targetVar;

		[SerializeField]
		private Property _property;

		[SerializeField]
		private float _angularDragValue;

		[SerializeField]
		private LayerMask _colliderMaskValue;

		[SerializeField]
		private float _dragValue;

		[SerializeField]
		private float _forceAngleValue;

		[SerializeField]
		private float _forceMagnitudeValue;

		[SerializeField]
		private EffectorSelection2D _forceTargetValue;

		[SerializeField]
		private float _forceVariationValue;

		[SerializeField]
		private bool _useColliderMaskValue;

		[SerializeField]
		private bool _useGlobalAngleValue;

		[SerializeField]
		private Variable _valueVar;

		protected override void OnReset()
		{
			_target = base.gameObject.GetComponent<AreaEffector2D>();
		}

		public override void Setup()
		{
		}

		public override void Execute()
		{
			AreaEffector2D areaEffector2D = _targetVar.GetValueAsComponent(_target, typeof(AreaEffector2D)) as AreaEffector2D;
			if (!(areaEffector2D == null))
			{
				switch (_property)
				{
				case Property.AngularDrag:
					areaEffector2D.angularDrag = _valueVar.GetValue(_angularDragValue);
					break;
				case Property.ColliderMask:
					areaEffector2D.colliderMask = _valueVar.GetValue(_colliderMaskValue);
					break;
				case Property.Drag:
					areaEffector2D.drag = _valueVar.GetValue(_dragValue);
					break;
				case Property.ForceAngle:
					areaEffector2D.forceAngle = _valueVar.GetValue(_forceAngleValue);
					break;
				case Property.ForceMagnitude:
					areaEffector2D.forceMagnitude = _valueVar.GetValue(_forceMagnitudeValue);
					break;
				case Property.ForceTarget:
					areaEffector2D.forceTarget = (EffectorSelection2D)(object)_valueVar.GetValue(_forceTargetValue);
					break;
				case Property.ForceVariation:
					areaEffector2D.forceVariation = _valueVar.GetValue(_forceVariationValue);
					break;
				case Property.UseColliderMask:
					areaEffector2D.useColliderMask = _valueVar.GetValue(_useColliderMaskValue);
					break;
				case Property.UseGlobalAngle:
					areaEffector2D.useGlobalAngle = _valueVar.GetValue(_useGlobalAngleValue);
					break;
				}
			}
		}
	}
}
