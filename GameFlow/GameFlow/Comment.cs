using UnityEngine;

namespace GameFlow
{
	[Help("es", "Añade un comentario de programa como nota de texto. Esta acción no realiza operación.", null)]
	[Help("en", "Adds a program comment as a text note. This is a no-op action.", null)]
	[AddComponentMenu("")]
	public class Comment : Action, IExecutableInEditor
	{
		[TextArea(1, 1024)]
		[SerializeField]
		private string _text = "This is an editable comment.";
	}
}
