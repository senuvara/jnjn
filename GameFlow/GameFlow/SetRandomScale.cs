using UnityEngine;

namespace GameFlow
{
	[AddComponentMenu("")]
	[Help("es", "Ajusta la escala del objetivo especificado a un valor @UnityAPI{Vector3} aleatorio.", null)]
	[Help("en", "Sets the scale of the specified target to a random @UnityAPI{Vector3} value.", null)]
	public class SetRandomScale : TransformAction
	{
		[SerializeField]
		private Vector3 _min = Vector3.zero;

		[SerializeField]
		private Variable _minVar;

		[SerializeField]
		private Vector3 _max = new Vector3(10f, 10f, 10f);

		[SerializeField]
		private Variable _maxVar;

		public Vector3 min => _minVar.GetValue(_min);

		public Vector3 max => _maxVar.GetValue(_max);

		public override void Execute()
		{
			Transform target = base.target;
			if (!(target == null))
			{
				Execute(target, min, max, base.relative, base.multiplier, base.mask);
			}
		}

		public static void Execute(Transform t, Vector3 min, Vector3 max, bool relative, float multiplier, Vector3 mask)
		{
			if (!(t == null))
			{
				float x = Random.Range(min.x, max.x);
				float y = Random.Range(min.y, max.y);
				float z = Random.Range(min.z, max.z);
				SetScale.Execute(t, new Vector3(x, y, z), relative, multiplier, mask);
			}
		}
	}
}
