using UnityEngine;

namespace GameFlow
{
	[Help("en", "Evaluates the state of the specified web request.", null)]
	[AddComponentMenu("")]
	[Help("es", "Evalúa el estado de la petición web (Web Request) especificada.", null)]
	public class WebRequestCondition : Condition
	{
		public enum Comparison
		{
			IsInProgress,
			IsNotInProgress,
			IsDone,
			IsNotDone,
			IsCancelled,
			IsNotCancelled,
			IsSuccessful,
			IsNotSuccessful,
			IsNone,
			IsNotNone
		}

		[SerializeField]
		private WebRequest _request;

		[SerializeField]
		private Variable _requestVar;

		[SerializeField]
		private Comparison _comparison = Comparison.IsSuccessful;

		public override bool Evaluate()
		{
			WebRequest webRequest = _requestVar.GetValue(_request) as WebRequest;
			switch (_comparison)
			{
			case Comparison.IsInProgress:
				return webRequest != null && webRequest.isInProgress;
			case Comparison.IsNotInProgress:
				return webRequest != null && !webRequest.isInProgress;
			case Comparison.IsDone:
				return webRequest != null && webRequest.isDone;
			case Comparison.IsNotDone:
				return webRequest != null && !webRequest.isDone;
			case Comparison.IsCancelled:
				return webRequest != null && webRequest.isCancelled;
			case Comparison.IsNotCancelled:
				return webRequest != null && !webRequest.isCancelled;
			case Comparison.IsSuccessful:
				return webRequest != null && webRequest.successful;
			case Comparison.IsNotSuccessful:
				return webRequest != null && !webRequest.successful;
			case Comparison.IsNone:
				return webRequest == null;
			case Comparison.IsNotNone:
				return webRequest != null;
			default:
				return false;
			}
		}
	}
}
