using UnityEngine;

namespace GameFlow
{
	[Help("es", "Establece la plataforma de ejecución.", null)]
	[AddComponentMenu("")]
	[Help("en", "Sets the runtime platform.", null)]
	public class SetPlatform : Action
	{
		[SerializeField]
		private RuntimePlatform _platform;

		[SerializeField]
		private Variable _platformVar;

		protected override void OnReset()
		{
			_platform = Application.platform;
		}

		public override void Execute()
		{
			Platform.current = (RuntimePlatform)(object)_platformVar.GetValue(_platform);
		}
	}
}
