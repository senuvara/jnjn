using UnityEngine;

namespace GameFlow
{
	[AddComponentMenu("")]
	[Help("es", "Borra la consola de depuración.", null)]
	[Help("en", "Clears the debug console.", null)]
	public class ClearConsole : Action, IExecutableInEditor
	{
	}
}
