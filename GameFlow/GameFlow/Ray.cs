using UnityEngine;

namespace GameFlow
{
	[Help("en", "Defines a ray.", null)]
	[Help("es", "Define un rayo.", null)]
	[AddComponentMenu("GameFlow/Tools/Ray")]
	public class Ray : Block, IIdentifiable, ITool, IVariableFriendly
	{
		public enum OriginType
		{
			Transform,
			WorldPoint
		}

		public enum DirectionType
		{
			Transform,
			WorldPoint,
			LocalPoint,
			ScreenPoint,
			ViewportPoint,
			WorldDirection,
			LocalDirection
		}

		[SerializeField]
		private string _id = string.Empty;

		[SerializeField]
		private OriginType _originType;

		[SerializeField]
		private Transform _originTransform;

		[SerializeField]
		private Vector3 _originVector3;

		[SerializeField]
		private Variable _originVar;

		[SerializeField]
		private DirectionType _directionType = DirectionType.LocalPoint;

		[SerializeField]
		private Transform _directionTransform;

		[SerializeField]
		private Vector2 _directionVector2;

		[SerializeField]
		private Vector3 _directionVector3 = Vector3.forward;

		[SerializeField]
		private Variable _directionVar;

		[SerializeField]
		private float _length = 10f;

		[SerializeField]
		private Variable _lengthVar;

		[SerializeField]
		private Color _color = Color.red;

		private Color _unselectedColor;

		public string id => _id;

		public Vector3 origin
		{
			get
			{
				Vector3 result = Vector3.zero;
				switch (_originType)
				{
				case OriginType.Transform:
				{
					Transform transform = _originVar.GetValueAsComponent(_originTransform, typeof(Transform)) as Transform;
					if (transform != null)
					{
						result = transform.position;
					}
					break;
				}
				case OriginType.WorldPoint:
					result = _originVar.GetValue(_originVector3);
					break;
				}
				return result;
			}
		}

		public Vector3 direction
		{
			get
			{
				Vector3 result = Vector3.zero;
				switch (_directionType)
				{
				case DirectionType.Transform:
				{
					Transform transform = _directionVar.GetValueAsComponent(_directionTransform, typeof(Transform)) as Transform;
					if (transform != null)
					{
						result = (transform.position - origin).normalized;
					}
					break;
				}
				case DirectionType.WorldPoint:
					result = (_directionVar.GetValue(_directionVector3) - origin).normalized;
					break;
				case DirectionType.LocalPoint:
					if (_originType == OriginType.Transform)
					{
						Transform transform = _originVar.GetValueAsComponent(_originTransform, typeof(Transform)) as Transform;
						if (transform != null)
						{
							result = (transform.TransformPoint(_directionVector3) - origin).normalized;
						}
					}
					break;
				case DirectionType.ScreenPoint:
				{
					Camera main = Camera.main;
					if (main != null)
					{
						result = main.ScreenPointToRay(_directionVar.GetValue(_directionVector2)).direction;
					}
					break;
				}
				case DirectionType.ViewportPoint:
				{
					Camera main = Camera.main;
					if (main != null)
					{
						result = main.ViewportPointToRay(_directionVar.GetValue(_directionVector2)).direction;
					}
					break;
				}
				case DirectionType.WorldDirection:
					result = _directionVar.GetValue(_directionVector3).normalized;
					break;
				case DirectionType.LocalDirection:
					if (_originType == OriginType.Transform)
					{
						Transform transform = _originVar.GetValueAsComponent(_originTransform, typeof(Transform)) as Transform;
						if (transform != null)
						{
							return transform.TransformDirection(_directionVector3);
						}
					}
					break;
				}
				return result;
			}
		}

		public float length
		{
			get
			{
				return _lengthVar.GetValue(_length);
			}
			set
			{
				_lengthVar = null;
				_length = value;
			}
		}

		public Color color
		{
			get
			{
				return _color;
			}
			set
			{
				_color = value;
				RecalcUnselectedColor();
			}
		}

		public Color unselectedColor => _unselectedColor;

		protected override void OnReset()
		{
			base.enabled = false;
			SetContainer();
			if (_id == string.Empty)
			{
				_id = "Ray" + Random.Range(1000, 9999);
			}
			RecalcUnselectedColor();
			if (Camera.main != null)
			{
				_originTransform = Camera.main.transform;
			}
		}

		private void SetContainer()
		{
			if (container != null || !BlockInsertion.enabled || BlockInsertion.clipboard)
			{
				return;
			}
			IRayContainer rayContainer = BlockInsertion.target as IRayContainer;
			if (rayContainer == null)
			{
				rayContainer = base.gameObject.GetComponent<Rays>();
				if (rayContainer == null)
				{
					rayContainer = base.gameObject.AddComponent<Rays>();
				}
				BlockInsertion.index = -1;
			}
			if (BlockInsertion.index < 0 || BlockInsertion.index >= rayContainer.GetRays().Count)
			{
				if (!rayContainer.Contains(this))
				{
					rayContainer.GetRays().Add(this);
					rayContainer.RayAdded(this);
					rayContainer.SetDirty(dirty: true);
				}
			}
			else
			{
				rayContainer.GetRays().Insert(BlockInsertion.index, this);
				rayContainer.RayAdded(this);
				rayContainer.SetDirty(dirty: true);
			}
			container = (rayContainer as Object);
			BlockInsertion.Reset();
		}

		private void Start()
		{
		}

		private void OnValidate()
		{
			RecalcUnselectedColor();
		}

		private void RecalcUnselectedColor()
		{
			_unselectedColor = ColorUtils.CreateColor(_color, 0.3f);
		}

		public string GetIdForControls()
		{
			return _id;
		}

		public string GetIdForSelectors()
		{
			return _id;
		}

		public virtual string GetTypeForSelectors()
		{
			return "Ray";
		}

		public UnityEngine.Ray ToUnityRay()
		{
			Camera main = Camera.main;
			if (main != null && main.orthographic)
			{
				switch (_directionType)
				{
				case DirectionType.ScreenPoint:
					return main.ScreenPointToRay(_directionVar.GetValue(_directionVector2));
				case DirectionType.ViewportPoint:
					return main.ViewportPointToRay(_directionVar.GetValue(_directionVector2));
				}
			}
			return new UnityEngine.Ray(origin, direction);
		}

		public void SetOrigin(Transform transform)
		{
			_originType = OriginType.Transform;
			_originTransform = transform;
		}

		public void SetOrigin(Vector3 point)
		{
			_originType = OriginType.WorldPoint;
			_originTransform = null;
			_originVector3 = point;
		}

		public Transform GetOriginAsTransform()
		{
			return _originTransform;
		}

		public Vector3 GetOriginAsWorldPoint()
		{
			return _originVector3;
		}

		public void SetDirectionTransform(Transform transform)
		{
			_directionType = DirectionType.Transform;
			_directionTransform = transform;
		}

		public void SetDirectionWorldPoint(Vector3 point)
		{
			_directionType = DirectionType.WorldPoint;
			_directionTransform = null;
			_directionVector3 = point;
		}

		public void SetDirectionLocalPoint(Vector3 point)
		{
			_directionType = DirectionType.LocalPoint;
			_directionTransform = null;
			_directionVector3 = point;
		}

		public void SetDirectionScreenPoint(Vector2 point)
		{
			_directionType = DirectionType.ScreenPoint;
			_directionTransform = null;
			_directionVector2 = point;
		}

		public void SetDirectionViewportPoint(Vector2 point)
		{
			_directionType = DirectionType.ViewportPoint;
			_directionTransform = null;
			_directionVector2 = point;
		}

		public void SetDirectionWorldDirection(Vector3 direction)
		{
			_directionType = DirectionType.WorldDirection;
			_directionTransform = null;
			_directionVector3 = direction;
		}

		public void SetDirectionLocalDirection(Vector3 direction)
		{
			_directionType = DirectionType.LocalDirection;
			_directionTransform = null;
			_directionVector3 = direction;
		}
	}
}
