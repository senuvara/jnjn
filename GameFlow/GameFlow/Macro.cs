using UnityEngine;

namespace GameFlow
{
	[Help("es", "Define una secuencia de acciones que puede ser ejecutada explícitamente en el Editor por medio de un atajo de teclado.", null)]
	[DisallowMultipleComponent]
	[AddComponentMenu("GameFlow/Programs/Macro")]
	[Help("en", "Defines a sequence of actions that can be assigned a keyboard shortcut and can be explicitly executed in Editor.", null)]
	public class Macro : Program, IEventListener, IExecutableInEditor
	{
		private void Awake()
		{
			RegisterAsListener();
		}

		protected virtual bool RegisterAsListener()
		{
			return false;
		}

		public bool IsListening()
		{
			return base.enabled;
		}

		public virtual void EventReceived(EventObject e)
		{
		}

		protected virtual bool AcceptEvent(EventObject e)
		{
			return true;
		}

		protected virtual void SetParameters(EventObject e)
		{
		}

		private void OnDestroy()
		{
			UnRegisterAsListener();
		}

		protected virtual void UnRegisterAsListener()
		{
		}

		public override string GetDefaultId()
		{
			return "Macro";
		}

		public override string GetIdForControls()
		{
			return base.id;
		}

		public override string GetTypeForSelectors()
		{
			return "Macro";
		}
	}
}
