using UnityEngine;
using UnityEngine.UI;

namespace GameFlow
{
	[Help("es", "Lee una propiedad del componente Input Field especificado.", null)]
	[AddComponentMenu("")]
	[Help("en", "Gets a property of the specified Input Field component.", null)]
	public class GetInputFieldProperty : Action, IExecutableInEditor
	{
		public enum Property
		{
			AnimationTriggers_DisabledTrigger,
			AnimationTriggers_HighlightedTrigger,
			AnimationTriggers_NormalTrigger,
			AnimationTriggers_PressedTrigger,
			Animator,
			AsteriskChar,
			CaretBlinkRate,
			CaretPosition,
			CharacterLimit,
			CharacterValidation,
			Colors_ColorMultiplier,
			Colors_DisabledColor,
			Colors_FadeDuration,
			Colors_HighlightedColor,
			Colors_NormalColor,
			Colors_PressedColor,
			ContentType,
			Image,
			InputType,
			Interactable,
			IsFocused,
			KeyboardType,
			LineType,
			MultiLine,
			Navigation_Mode,
			Navigation_SelectOnDown,
			Navigation_SelectOnLeft,
			Navigation_SelectOnRight,
			Navigation_SelectOnUp,
			Placeholder,
			SelectionAnchorPosition,
			SelectionColor,
			SelectionFocusPosition,
			ShouldHideMobileInput,
			SpriteState_DisabledSprite,
			SpriteState_HighlightedSprite,
			SpriteState_PressedSprite,
			TargetGraphic,
			Text,
			TextComponent,
			Transition,
			WasCanceled
		}

		[SerializeField]
		private InputField _source;

		[SerializeField]
		private Variable _sourceVar;

		[SerializeField]
		private Property _property;

		[SerializeField]
		private Variable _output;

		protected override void OnReset()
		{
			_source = base.gameObject.GetComponent<InputField>();
		}

		public override void Setup()
		{
		}

		public override void Execute()
		{
			InputField inputField = _sourceVar.GetValueAsComponent(_source, typeof(InputField)) as InputField;
			if (!(inputField == null))
			{
				switch (_property)
				{
				case Property.AnimationTriggers_DisabledTrigger:
					_output.SetValue(inputField.animationTriggers.disabledTrigger);
					break;
				case Property.AnimationTriggers_HighlightedTrigger:
					_output.SetValue(inputField.animationTriggers.highlightedTrigger);
					break;
				case Property.AnimationTriggers_NormalTrigger:
					_output.SetValue(inputField.animationTriggers.normalTrigger);
					break;
				case Property.AnimationTriggers_PressedTrigger:
					_output.SetValue(inputField.animationTriggers.pressedTrigger);
					break;
				case Property.Animator:
					_output.SetValue(inputField.animator);
					break;
				case Property.AsteriskChar:
					_output.SetValue(inputField.asteriskChar.ToString());
					break;
				case Property.CaretBlinkRate:
					_output.SetValue(inputField.caretBlinkRate);
					break;
				case Property.CaretPosition:
					_output.SetValue(inputField.caretPosition);
					break;
				case Property.CharacterLimit:
					_output.SetValue(inputField.characterLimit);
					break;
				case Property.CharacterValidation:
					_output.SetValue(inputField.characterValidation);
					break;
				case Property.Colors_ColorMultiplier:
					_output.SetValue(inputField.colors.colorMultiplier);
					break;
				case Property.Colors_DisabledColor:
					_output.SetValue(inputField.colors.disabledColor);
					break;
				case Property.Colors_FadeDuration:
					_output.SetValue(inputField.colors.fadeDuration);
					break;
				case Property.Colors_HighlightedColor:
					_output.SetValue(inputField.colors.highlightedColor);
					break;
				case Property.Colors_NormalColor:
					_output.SetValue(inputField.colors.normalColor);
					break;
				case Property.Colors_PressedColor:
					_output.SetValue(inputField.colors.pressedColor);
					break;
				case Property.ContentType:
					_output.SetValue(inputField.contentType);
					break;
				case Property.Image:
					_output.SetValue(inputField.image);
					break;
				case Property.InputType:
					_output.SetValue(inputField.inputType);
					break;
				case Property.Interactable:
					_output.SetValue(inputField.interactable);
					break;
				case Property.IsFocused:
					_output.SetValue(inputField.isFocused);
					break;
				case Property.KeyboardType:
					_output.SetValue(inputField.keyboardType);
					break;
				case Property.LineType:
					_output.SetValue(inputField.lineType);
					break;
				case Property.MultiLine:
					_output.SetValue(inputField.multiLine);
					break;
				case Property.Navigation_Mode:
					_output.SetValue(inputField.navigation.mode);
					break;
				case Property.Navigation_SelectOnDown:
					_output.SetValue(inputField.navigation.selectOnDown);
					break;
				case Property.Navigation_SelectOnLeft:
					_output.SetValue(inputField.navigation.selectOnLeft);
					break;
				case Property.Navigation_SelectOnRight:
					_output.SetValue(inputField.navigation.selectOnRight);
					break;
				case Property.Navigation_SelectOnUp:
					_output.SetValue(inputField.navigation.selectOnUp);
					break;
				case Property.Placeholder:
					_output.SetValue(inputField.placeholder);
					break;
				case Property.SelectionAnchorPosition:
					_output.SetValue(inputField.selectionAnchorPosition);
					break;
				case Property.SelectionColor:
					_output.SetValue(inputField.selectionColor);
					break;
				case Property.SelectionFocusPosition:
					_output.SetValue(inputField.selectionFocusPosition);
					break;
				case Property.ShouldHideMobileInput:
					_output.SetValue(inputField.shouldHideMobileInput);
					break;
				case Property.SpriteState_DisabledSprite:
					_output.SetValue(inputField.spriteState.disabledSprite);
					break;
				case Property.SpriteState_HighlightedSprite:
					_output.SetValue(inputField.spriteState.highlightedSprite);
					break;
				case Property.SpriteState_PressedSprite:
					_output.SetValue(inputField.spriteState.pressedSprite);
					break;
				case Property.TargetGraphic:
					_output.SetValue(inputField.targetGraphic);
					break;
				case Property.Text:
					_output.SetValue(inputField.text);
					break;
				case Property.TextComponent:
					_output.SetValue(inputField.textComponent);
					break;
				case Property.Transition:
					_output.SetValue(inputField.transition);
					break;
				case Property.WasCanceled:
					_output.SetValue(inputField.wasCanceled);
					break;
				}
			}
		}
	}
}
