using UnityEngine;

namespace GameFlow
{
	[Help("es", "Pide un objeto que no esté en uso al @GameFlow{Pool} especificado.", null)]
	[AddComponentMenu("")]
	[Help("en", "Requests a free object to the specified @GameFlow{Pool}.", null)]
	public class GetObjectFromPool : Action
	{
		[SerializeField]
		private Pool _pool;

		[SerializeField]
		private Variable _poolVar;

		[SerializeField]
		private Variable _output;

		public override void Execute()
		{
			Execute(_poolVar.GetValue(_pool) as Pool, _output);
		}

		public static void Execute(Pool pool, Variable output)
		{
			if (!(pool == null) && !(output == null))
			{
				output.SetValue(pool.GetObject());
			}
		}
	}
}
