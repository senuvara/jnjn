using UnityEngine;

namespace GameFlow
{
	public abstract class TransformAction : Action, IExecutableInEditor
	{
		[SerializeField]
		private Transform _target;

		[SerializeField]
		private Variable _targetVar;

		[SerializeField]
		private Space _space;

		[SerializeField]
		private Variable _spaceVar;

		[SerializeField]
		private Vector3 _mask = Vector3.one;

		[SerializeField]
		private Variable _maskVar;

		[SerializeField]
		private bool _relative;

		[SerializeField]
		private Variable _relativeVar;

		[SerializeField]
		private float _multiplier = 1f;

		[SerializeField]
		private Variable _multiplierVar;

		public Transform target => _targetVar.GetValueAsComponent(_target, typeof(Transform)) as Transform;

		public Space space => (Space)(object)_spaceVar.GetValue(_space);

		public Vector3 mask => _maskVar.GetValue(_mask);

		public bool relative
		{
			get
			{
				return _relativeVar.GetValue(_relative);
			}
			protected set
			{
				_relative = value;
			}
		}

		public float multiplier => _multiplierVar.GetValue(_multiplier);

		protected override void OnReset()
		{
			_target = base.gameObject.transform;
		}
	}
}
