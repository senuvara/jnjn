using UnityEngine;

namespace GameFlow
{
	[AddComponentMenu("")]
	[Help("es", "Ejecuta las acciones contenidas para cada elemento de la @GameFlow{List} especificada.", null)]
	[Help("en", "Executes the contained actions for each item of the specified @GameFlow{List}.", null)]
	public class ForEach : While
	{
		public enum Order
		{
			FromFirstToLast,
			FromLastToFirst
		}

		[SerializeField]
		private List _list;

		[SerializeField]
		private Variable _listVar;

		[SerializeField]
		private Order _order;

		[SerializeField]
		private Variable _output;

		[SerializeField]
		private int _index;

		[SerializeField]
		private int _endIndex;

		private List _cachedList;

		protected List list => _listVar.GetValue(_list) as List;

		public override void FirstStep()
		{
			_cachedList = list;
			if (_cachedList != null)
			{
				if (_order == Order.FromFirstToLast)
				{
					_index = 0;
					_endIndex = _cachedList.Count - 1;
				}
				else
				{
					_index = _cachedList.Count - 1;
					_endIndex = 0;
				}
			}
			base.FirstStep();
		}

		public override bool Evaluate(bool defaultResult)
		{
			if (_cachedList == null)
			{
				return defaultResult;
			}
			if (!_cachedList.IsIndexValid(_index))
			{
				return defaultResult;
			}
			if ((_order != 0) ? (_index < _endIndex) : (_index > _endIndex))
			{
				return false;
			}
			switch (_cachedList.dataType)
			{
			case DataType.String:
				_output.SetValue(_cachedList.GetStringAt(_index));
				break;
			case DataType.Tag:
				_output.SetTagValue(_cachedList.GetStringAt(_index));
				break;
			case DataType.Integer:
				_output.SetValue(_cachedList.GetIntAt(_index));
				break;
			case DataType.Layer:
				_output.SetLayerValue(_cachedList.GetIntAt(_index));
				break;
			case DataType.Float:
				_output.SetValue(_cachedList.GetFloatAt(_index));
				break;
			case DataType.Boolean:
				_output.SetValue(_cachedList.GetBoolAt(_index));
				break;
			case DataType.Toggle:
				_output.SetToggleValue(_cachedList.GetBoolAt(_index));
				break;
			case DataType.Vector2:
				_output.SetValue(_cachedList.GetVector2At(_index));
				break;
			case DataType.Vector3:
				_output.SetValue(_cachedList.GetVector3At(_index));
				break;
			case DataType.Rect:
				_output.SetValue(_cachedList.GetRectAt(_index));
				break;
			case DataType.Color:
				_output.SetValue(_cachedList.GetColorAt(_index));
				break;
			case DataType.Object:
				_output.SetValue(_cachedList.GetObjectAt(_index));
				break;
			case DataType.Enum:
				_output.SetValue(_cachedList.GetEnumAt(_index));
				break;
			}
			_index += ((_order == Order.FromFirstToLast) ? 1 : (-1));
			return true;
		}
	}
}
