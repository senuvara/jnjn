using System.Collections.Generic;

namespace GameFlow
{
	public interface IActionContainer : IBlockContainer
	{
		List<Action> GetActions(int sectionIndex = 0);

		int GetActionCount(int sectionIndex = 0);

		int GetActionSectionsCount();

		void ActionAdded(Action action, int sectionIndex = 0);

		void Break();
	}
}
