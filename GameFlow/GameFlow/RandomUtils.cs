using UnityEngine;

namespace GameFlow
{
	public static class RandomUtils
	{
		public static Vector2 GetRandomVector2(Vector2 min, Vector2 max)
		{
			float x = Random.Range(min.x, max.x);
			float y = Random.Range(min.y, max.y);
			return new Vector2(x, y);
		}

		public static Vector3 GetRandomVector3(Vector3 min, Vector3 max)
		{
			float x = Random.Range(min.x, max.x);
			float y = Random.Range(min.y, max.y);
			float z = Random.Range(min.z, max.z);
			return new Vector3(x, y, z);
		}

		public static Vector3 GetRandomPointInCollider(BoxCollider bc)
		{
			if (bc == null)
			{
				return Vector3.zero;
			}
			Transform component = bc.gameObject.GetComponent<Transform>();
			Vector3 vector = bc.size / 2f;
			float num = Random.Range(0f - vector.x, vector.x);
			float num2 = Random.Range(0f - vector.y, vector.y);
			float num3 = Random.Range(0f - vector.z, vector.z);
			Vector3 center = bc.center;
			float x = center.x + num;
			Vector3 center2 = bc.center;
			float y = center2.y + num2;
			Vector3 center3 = bc.center;
			return component.TransformPoint(x, y, center3.z + num3);
		}

		public static Vector3 GetRandomPointInCollider(SphereCollider sc)
		{
			if (sc == null)
			{
				return Vector3.zero;
			}
			Transform component = sc.gameObject.GetComponent<Transform>();
			Vector3 a = component.TransformPoint(sc.center);
			Vector3 lossyScale = component.lossyScale;
			float a2 = Mathf.Abs(lossyScale.x);
			Vector3 lossyScale2 = component.lossyScale;
			float a3 = Mathf.Max(a2, Mathf.Abs(lossyScale2.y));
			Vector3 lossyScale3 = component.lossyScale;
			float d = Mathf.Max(a3, Mathf.Abs(lossyScale3.z));
			Vector3 b = Random.insideUnitSphere * sc.radius * d;
			return a + b;
		}
	}
}
