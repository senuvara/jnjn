using UnityEngine;

namespace GameFlow
{
	[Help("es", "Transforma el punto especificado del espacio local del @UnityManual{Transform} dado al espacio del mundo.", null)]
	[Help("en", "Transforms the specified point from local space of the given @UnityManual{Transform} to world space.", null)]
	[AddComponentMenu("")]
	public class GetTransformPoint : Action, IExecutableInEditor
	{
		[SerializeField]
		private Transform _target;

		[SerializeField]
		private Variable _targetVar;

		[SerializeField]
		private Vector3 _vector3Value;

		[SerializeField]
		private Variable _valueVar;

		[SerializeField]
		private bool _inverse;

		[SerializeField]
		private Variable _inverseVar;

		[SerializeField]
		private Variable _output;

		protected override void OnReset()
		{
			_target = base.gameObject.GetComponent<Transform>();
		}

		public override void Execute()
		{
			if (_output == null)
			{
				return;
			}
			Transform transform = _target;
			if (transform == null)
			{
				transform = (_targetVar.GetValueAsComponent(_target, typeof(Transform)) as Transform);
				if (transform == null)
				{
					return;
				}
			}
			Vector3 value = _valueVar.GetValue(_vector3Value);
			if (_inverseVar.GetValue(_inverse))
			{
				_output.SetValue(transform.InverseTransformPoint(value));
			}
			else
			{
				_output.SetValue(transform.TransformPoint(value));
			}
		}
	}
}
