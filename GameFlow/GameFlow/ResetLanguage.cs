using UnityEngine;

namespace GameFlow
{
	[Help("es", "Establece el idioma por defecto como el idioma a usar para localización.", null)]
	[Help("en", "Sets the default language as the current for localization purposes.", null)]
	[AddComponentMenu("")]
	public class ResetLanguage : Action, IExecutableInEditor
	{
		public override void Execute()
		{
			GameSettings.language = LocalizationSettings.GetDefaultLanguage();
		}
	}
}
