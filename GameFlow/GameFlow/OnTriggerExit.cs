using UnityEngine;

namespace GameFlow
{
	[DisallowMultipleComponent]
	[AddComponentMenu("GameFlow/Programs/On Trigger Exit")]
	[Help("en", "Program that will be executed on time as soon as a Collider component configured as trigger in the listening range stops being invaded by another component.", null)]
	[Help("es", "Programa que se ejecutará en el primer fotograma que un componente Collider configurado como disparador (trigger) en el rango de escucha deje de estar invadido por otro componente.", null)]
	public class OnTriggerExit : OnTriggerProgram
	{
		private EventController[] _controllers;

		protected override void RegisterAsListener()
		{
			_controllers = RegisterAsListener(typeof(TriggerExitEvent), typeof(TriggerController), TriggerController.AddController);
			if (base.listeningRange == EventListeningRange.SceneHierarchy)
			{
				TriggerExitEvent.AddListener(this);
			}
		}

		protected override void UnRegisterAsListener()
		{
			TriggerExitEvent.RemoveListener(this);
			UnRegisterAsListener(typeof(TriggerExitEvent), typeof(TriggerController));
		}

		private void OnTransformChildrenChanged()
		{
			if (base.listeningRange == EventListeningRange.GameObjectHierarchy)
			{
				UnsubscribeFrom(_controllers, typeof(TriggerExitEvent));
				_controllers = EventController.GetControllersInHierarchyOf(typeof(TriggerController), base.listeningTarget, TriggerController.AddController);
				SubscribeTo(_controllers, typeof(TriggerExitEvent));
			}
		}
	}
}
