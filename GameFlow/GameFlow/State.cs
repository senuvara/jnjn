using System.Collections.Generic;
using UnityEngine;

namespace GameFlow
{
	[AddComponentMenu("GameFlow/Tools/State")]
	[Help("es", "Define un estado que podrá ser usado en una Máquina de Estados (@GameFlow{StateMachine}).", null)]
	[Help("en", "Defines a state that can be used in a @GameFlow{StateMachine}.", null)]
	public class State : Block, IActionContainer, IBlockContainer, IIdentifiable, IVariableFriendly
	{
		[SerializeField]
		private string _id = string.Empty;

		[SerializeField]
		private List<Action> _actions = new List<Action>();

		private bool _dirty;

		private ActionSequence sequence;

		private bool _finished;

		public string id => _id;

		private void Awake()
		{
			base.enabled = false;
			sequence = new ActionSequence(_actions);
		}

		protected override void OnReset()
		{
			base.enabled = false;
			SetContainer();
			if (_id == string.Empty)
			{
				_id = "State" + Random.Range(1000, 9999);
			}
		}

		private void SetContainer()
		{
			if (container != null || !BlockInsertion.enabled || BlockInsertion.clipboard)
			{
				return;
			}
			IStateContainer stateContainer = BlockInsertion.target as IStateContainer;
			if (stateContainer == null)
			{
				stateContainer = base.gameObject.GetComponent<StateMachine>();
				if (stateContainer == null)
				{
					stateContainer = base.gameObject.AddComponent<StateMachine>();
				}
				BlockInsertion.index = -1;
			}
			if (BlockInsertion.index < 0 || BlockInsertion.index >= stateContainer.GetStates().Count)
			{
				if (!stateContainer.Contains(this))
				{
					stateContainer.GetStates().Add(this);
					stateContainer.StateAdded(this);
					stateContainer.SetDirty(dirty: true);
				}
			}
			else
			{
				stateContainer.GetStates().Insert(BlockInsertion.index, this);
				stateContainer.StateAdded(this);
				stateContainer.SetDirty(dirty: true);
			}
			container = (stateContainer as Object);
			BlockInsertion.Reset();
		}

		public void Restart()
		{
			ResetSequence();
			Execute();
		}

		public void Execute()
		{
			if (!_finished && sequence.Finished())
			{
				_finished = true;
			}
			if (_finished)
			{
				ResetSequence();
				return;
			}
			bool flag = ExecuteCurrentAction();
			while (!flag)
			{
				flag = ExecuteCurrentAction();
			}
		}

		private bool ExecuteCurrentAction()
		{
			bool flag = true;
			Action currentAction = sequence.GetCurrentAction();
			while (currentAction == null || !currentAction.enabledInEditor)
			{
				sequence.NextEnabledAction();
				if (sequence.Finished())
				{
					ResetSequence();
					return flag;
				}
				currentAction = sequence.GetCurrentAction();
			}
			sequence.ExecuteCurrentAction();
			if (ActionExecutor.IsActionFinished(currentAction))
			{
				sequence.NextEnabledAction();
				if (_finished)
				{
					return flag;
				}
				if (sequence.Finished())
				{
					ResetSequence();
					return flag;
				}
				return !flag;
			}
			return flag;
		}

		private void ResetSequence()
		{
			sequence.FirstEnabledAction();
			_finished = false;
		}

		public virtual List<Action> GetActions(int sectionIndex = 0)
		{
			return _actions;
		}

		public int GetActionCount(int sectionIndex = 0)
		{
			return ActionUtils.GetActionCount(this);
		}

		public virtual int GetActionSectionsCount()
		{
			return 1;
		}

		public virtual void ActionAdded(Action action, int sectionIndex = 0)
		{
		}

		public virtual void Break()
		{
			_finished = true;
			StateMachine stateMachine = container as StateMachine;
			if (stateMachine != null)
			{
				stateMachine.currentState = null;
			}
		}

		public void SetDirty(bool dirty)
		{
			_dirty = dirty;
		}

		public bool IsDirty()
		{
			return _dirty;
		}

		public List<Block> GetBlocks()
		{
			return new List<Block>(_actions.ToArray());
		}

		public virtual bool Contains(Block block)
		{
			if (block as Action != null)
			{
				return _actions.Contains(block as Action);
			}
			return false;
		}

		public string GetIdForControls()
		{
			return _id;
		}

		public string GetIdForSelectors()
		{
			return _id;
		}

		public virtual string GetTypeForSelectors()
		{
			return "State";
		}
	}
}
