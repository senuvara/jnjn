using UnityEngine;

namespace GameFlow
{
	[Help("es", "Devuelve la posición del punto situado a la distancia dada a lo largo del rayo (@GameFlow{Ray}) especificado.", null)]
	[Help("en", "Returns the position of the point at the given distance along the specified @GameFlow{Ray}.", null)]
	[AddComponentMenu("")]
	public class GetRayPoint : Action, IExecutableInEditor
	{
		[SerializeField]
		private Ray _ray;

		[SerializeField]
		private Variable _rayVar;

		[SerializeField]
		private float _distance = 10f;

		[SerializeField]
		private Variable _distanceVar;

		[SerializeField]
		private Variable _output;

		public override void Execute()
		{
			if (!(_output == null))
			{
				Ray ray = _rayVar.GetValue(_ray) as Ray;
				if (!(ray == null))
				{
					float value = _distanceVar.GetValue(_distance);
					_output.SetValue(ray.ToUnityRay().GetPoint(value));
				}
			}
		}
	}
}
