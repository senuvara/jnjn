using System.Collections.Generic;
using UnityEngine;

namespace GameFlow
{
	[Help("es", "Ejecuta condicionalmente las acciones contenidas.", null)]
	[Help("en", "Executes the contained actions conditionally.", null)]
	[AddComponentMenu("")]
	public class If : Conditional
	{
		private bool _finished;

		[SerializeField]
		private List<Action> _elseActions = new List<Action>();

		protected ActionSequence elseSequence;

		[SerializeField]
		private bool _elseVisible;

		private ActionSequence _activeSequence;

		public override List<Action> GetActions(int sectionIndex = 0)
		{
			switch (sectionIndex)
			{
			case 1:
				return base.GetActions();
			case 2:
				return _elseActions;
			default:
				return null;
			}
		}

		public override int GetActionSectionsCount()
		{
			return 2;
		}

		public sealed override void Setup()
		{
			base.Setup();
			elseSequence = new ActionSequence(_elseActions);
		}

		public override void FirstStep()
		{
			_activeSequence = ((!Evaluate(defaultResult: false)) ? elseSequence : sequence);
			_activeSequence.FirstEnabledAction();
			_finished = _activeSequence.Finished();
			if (!_finished)
			{
				Step();
			}
		}

		public override void Step()
		{
			if (_activeSequence.Finished())
			{
				_finished = true;
			}
			if (_finished)
			{
				return;
			}
			Action currentAction = _activeSequence.GetCurrentAction();
			if (currentAction != null && currentAction.enabledInEditor)
			{
				_activeSequence.ExecuteCurrentAction();
			}
			else
			{
				_activeSequence.NextEnabledAction();
				currentAction = _activeSequence.GetCurrentAction();
				if (currentAction != null)
				{
					_activeSequence.ExecuteCurrentAction();
				}
			}
			if (currentAction != null && ActionExecutor.DoesActionYield(currentAction))
			{
				ActionExecutor.Yield(currentAction);
				CheckFinished();
				_activeSequence.NextEnabledAction();
				return;
			}
			while (!_finished && currentAction != null && ActionExecutor.IsActionFinished(currentAction))
			{
				_activeSequence.NextEnabledAction();
				currentAction = _activeSequence.GetCurrentAction();
				if (currentAction != null && currentAction.enabledInEditor)
				{
					_activeSequence.ExecuteCurrentAction();
					if (ActionExecutor.DoesActionYield(currentAction))
					{
						ActionExecutor.Yield(currentAction);
						CheckFinished();
						_activeSequence.NextEnabledAction();
						return;
					}
				}
			}
			CheckFinished();
		}

		private void CheckFinished()
		{
			if (_activeSequence.Finished() && !_finished)
			{
				_finished = true;
			}
		}

		public override bool Finished()
		{
			return _finished;
		}

		public override void Break()
		{
			_finished = true;
			(container as IActionContainer)?.Break();
		}

		public override bool Contains(Block block)
		{
			bool flag = base.Contains(block);
			if (!flag && block as Action != null)
			{
				flag = _elseActions.Contains(block as Action);
			}
			return flag;
		}

		public override List<Block> GetBlocks()
		{
			List<Block> list = new List<Block>(GetActions().ToArray());
			list.AddRange(GetActions(1).ToArray());
			list.AddRange(GetConditions().ToArray());
			return list;
		}
	}
}
