using UnityEngine;

namespace GameFlow
{
	[Help("es", "Deshabilita el componente especificado.", null)]
	[AddComponentMenu("")]
	[Help("en", "Disables the specified Component.", null)]
	public class DisableComponent : Action, IExecutableInEditor
	{
		[SerializeField]
		private Component _component;

		[SerializeField]
		private Variable _componentVar;

		public override void Execute()
		{
			Execute(_componentVar.GetValueAsComponent(_component, typeof(Component)));
		}

		public static void Execute(Component component)
		{
			if (component == null)
			{
				return;
			}
			Behaviour behaviour = component as Behaviour;
			if (behaviour != null)
			{
				behaviour.enabled = false;
				return;
			}
			Collider collider = component as Collider;
			if (collider != null)
			{
				collider.enabled = false;
				return;
			}
			Renderer renderer = component as Renderer;
			if (renderer != null)
			{
				renderer.enabled = false;
				return;
			}
			Cloth cloth = component as Cloth;
			if (cloth != null)
			{
				cloth.enabled = false;
			}
		}
	}
}
