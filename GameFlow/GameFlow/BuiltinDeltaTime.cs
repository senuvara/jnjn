using System;
using UnityEngine;

namespace GameFlow
{
	[Help("es", "Una @GameFlow{Variable} integrada que devuelve el tiempo en segundos que tardó en completarse el último fotograma.", null)]
	[AddComponentMenu("")]
	[Help("en", "A built-in @GameFlow{Variable} that returns the time in seconds it took to complete the last frame.", null)]
	public class BuiltinDeltaTime : BuiltinVariable
	{
		public override float floatValue => (!Application.isPlaying) ? 0f : Time.deltaTime;

		public override Type GetVariableType()
		{
			return typeof(float);
		}

		protected override string GetVariableId()
		{
			return "Delta Time";
		}
	}
}
