using UnityEngine;

namespace GameFlow
{
	[AddComponentMenu("")]
	[Help("es", "Añade a una lista (@GameFlow{List}) dada los impactos resultantes de lanzar el rayo (@GameFlow{Ray}) especificado.", null)]
	[Help("en", "Adds to a given @GameFlow{List} the resulting hits after casting the specified @GameFlow{Ray}.", null)]
	public class GetRaycastHits : ListAction
	{
		public enum OutputType
		{
			Colliders,
			Distances,
			GameObjects,
			ImpactPoints,
			RaycastHits,
			Rigidbodies,
			Transforms
		}

		[SerializeField]
		private Ray _ray;

		[SerializeField]
		private Variable _rayVar;

		[SerializeField]
		private LayerMask _layers;

		[SerializeField]
		private OutputType _outputType = OutputType.GameObjects;

		protected override void OnReset()
		{
			_layers = 65531;
		}

		public override void Execute()
		{
			Execute(_rayVar.GetValue(_ray) as Ray, _layers, base.list, _outputType);
		}

		public static void Execute(Ray ray, LayerMask layers, List list, OutputType outputType)
		{
			if (ray == null || list == null)
			{
				return;
			}
			UnityEngine.Ray ray2 = ray.ToUnityRay();
			UnityEngine.RaycastHit[] array = Physics.RaycastAll(ray2, ray.length, layers);
			switch (outputType)
			{
			case OutputType.Colliders:
			{
				list.type = typeof(Collider);
				for (int i = 0; i < array.Length; i++)
				{
					list.AddValue(array[i].collider);
				}
				break;
			}
			case OutputType.Distances:
			{
				list.type = typeof(float);
				for (int i = 0; i < array.Length; i++)
				{
					list.AddValue(array[i].distance);
				}
				break;
			}
			case OutputType.GameObjects:
			{
				list.type = typeof(GameObject);
				for (int i = 0; i < array.Length; i++)
				{
					list.AddValue(array[i].collider.gameObject);
				}
				break;
			}
			case OutputType.ImpactPoints:
			{
				list.type = typeof(Vector3);
				for (int i = 0; i < array.Length; i++)
				{
					list.AddValue(array[i].point);
				}
				break;
			}
			case OutputType.RaycastHits:
			{
				list.type = typeof(RaycastHit);
				for (int i = 0; i < array.Length; i++)
				{
					RaycastHit value = RaycastHit.CreateInstance(array[i]);
					list.AddValue(value);
				}
				break;
			}
			case OutputType.Rigidbodies:
			{
				list.type = typeof(Rigidbody);
				for (int i = 0; i < array.Length; i++)
				{
					list.AddValue(array[i].rigidbody);
				}
				break;
			}
			case OutputType.Transforms:
			{
				list.type = typeof(Transform);
				for (int i = 0; i < array.Length; i++)
				{
					list.AddValue(array[i].transform);
				}
				break;
			}
			}
		}
	}
}
