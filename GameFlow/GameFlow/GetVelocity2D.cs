using UnityEngine;

namespace GameFlow
{
	[Help("es", "Obtiene la velocidad del objeto que contiene al @UnityManual{Rigidbody2D} especificado.", null)]
	[Help("en", "Gets the velocity of the object containing the specified @UnityManual{Rigidbody2D} component.", null)]
	[AddComponentMenu("")]
	public class GetVelocity2D : Action, IExecutableInEditor
	{
		[SerializeField]
		private Rigidbody2D _source;

		[SerializeField]
		private Variable _sourceVar;

		[SerializeField]
		private Variable _output;

		protected override void OnReset()
		{
			_source = base.gameObject.GetComponent<Rigidbody2D>();
		}

		public override void Execute()
		{
			if (!(_output == null))
			{
				Rigidbody2D rigidbody2D = _sourceVar.GetValueAsComponent(_source, typeof(Rigidbody2D)) as Rigidbody2D;
				if (rigidbody2D != null)
				{
					_output.SetValue(rigidbody2D.velocity);
				}
			}
		}
	}
}
