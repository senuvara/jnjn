using System.Collections.Generic;
using UnityEngine;

namespace GameFlow
{
	[Help("es", "Programa que se ejecutará cuando un @GameFlow{Program} en el rango de escucha finalice.", null)]
	[AddComponentMenu("GameFlow/Programs/On Program Finish")]
	[DisallowMultipleComponent]
	[Help("en", "Program that will be executed when a @GameFlow{Program} in the listening range finishes.", null)]
	public class OnProgramFinish : EventProgram
	{
		private Parameter _programParam;

		private Program[] _programs;

		protected Parameter programParam
		{
			get
			{
				if (_programParam == null)
				{
					_programParam = GetParameter("Program");
				}
				return _programParam;
			}
		}

		protected override void RegisterAsListener()
		{
			switch (base.listeningRange)
			{
			case EventListeningRange.GameObject:
				if (base.listeningTarget != null)
				{
					SubscribeTo(base.listeningTarget.GetComponents<Program>());
				}
				break;
			case EventListeningRange.GameObjectHierarchy:
				if (base.listeningTarget != null)
				{
					_programs = SubscribeTo(base.listeningTarget.GetComponentsInChildren<Program>(includeInactive: true));
				}
				break;
			case EventListeningRange.SceneHierarchy:
				ProgramFinishEvent.AddListener(this);
				break;
			case EventListeningRange.List:
			{
				if (!(base.listeningList != null))
				{
					break;
				}
				for (int i = 0; i < base.listeningList.Count; i++)
				{
					GameObject gameObject = base.listeningList.GetObjectAt(i) as GameObject;
					if (gameObject != null)
					{
						SubscribeTo(gameObject.GetComponents<Program>());
					}
				}
				break;
			}
			}
		}

		private Program[] SubscribeTo(Program[] programs)
		{
			List<Program> list = new List<Program>();
			for (int i = 0; i < programs.Length; i++)
			{
				IEventDispatcher eventDispatcher = programs[i];
				if (eventDispatcher != null && eventDispatcher as EventProgram != this)
				{
					eventDispatcher.AddListener(this, typeof(ProgramFinishEvent));
					list.Add(programs[i]);
				}
			}
			return list.ToArray();
		}

		protected override void SetParameters(EventObject e)
		{
			ProgramFinishEvent programFinishEvent = e as ProgramFinishEvent;
			programParam.SetValue(programFinishEvent.program);
		}

		protected override bool WillFireEventOnProgramFinished()
		{
			return false;
		}

		protected override void UnRegisterAsListener()
		{
			switch (base.listeningRange)
			{
			case EventListeningRange.GameObject:
				if (base.listeningTarget != null)
				{
					UnsubscribeFrom(base.listeningTarget.GetComponents<Program>(), typeof(ProgramFinishEvent));
				}
				break;
			case EventListeningRange.GameObjectHierarchy:
				if (base.listeningTarget != null)
				{
					UnsubscribeFrom(base.listeningTarget.GetComponentsInChildren<Program>(includeInactive: true), typeof(ProgramFinishEvent));
				}
				break;
			case EventListeningRange.SceneHierarchy:
				ProgramFinishEvent.RemoveListener(this);
				break;
			case EventListeningRange.List:
			{
				if (!(base.listeningList != null))
				{
					break;
				}
				for (int i = 0; i < base.listeningList.Count; i++)
				{
					GameObject gameObject = base.listeningList.GetObjectAt(i) as GameObject;
					if (gameObject != null)
					{
						UnsubscribeFrom(gameObject.GetComponents<Program>(), typeof(ProgramFinishEvent));
					}
				}
				break;
			}
			}
		}

		private void OnTransformChildrenChanged()
		{
			if (base.listeningRange == EventListeningRange.GameObjectHierarchy)
			{
				UnsubscribeFrom(_programs, typeof(ProgramFinishEvent));
				_programs = SubscribeTo(base.listeningTarget.GetComponentsInChildren<Program>(includeInactive: true));
				SubscribeTo(_programs);
			}
		}
	}
}
