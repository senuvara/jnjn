using UnityEngine;

namespace GameFlow
{
	[AddComponentMenu("")]
	[Help("es", "Reanuda el temporizador (@GameFlow{Timer}) especificado.", null)]
	[Help("en", "Resumes the specified @GameFlow{Timer}.", null)]
	public class ResumeTimer : Action
	{
		[SerializeField]
		private Timer _target;

		[SerializeField]
		private Variable _targetVar;

		public override void Execute()
		{
			Timer timer = _targetVar.GetValue(_target) as Timer;
			if (timer != null)
			{
				timer.Resume();
			}
		}
	}
}
