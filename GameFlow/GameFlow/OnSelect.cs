using UnityEngine;

namespace GameFlow
{
	[AddComponentMenu("GameFlow/Programs/On Select")]
	[DisallowMultipleComponent]
	[Help("es", "Programa que se ejecutará cuando un componente UI en el rango de escucha sea seleccionado.", null)]
	[Help("en", "Program that will be executed when an UI component in the listening range is selected.", null)]
	public class OnSelect : EventProgram
	{
		private Parameter _sourceParam;

		private EventController[] _controllers;

		public Parameter sourceParam
		{
			get
			{
				if (_sourceParam == null)
				{
					_sourceParam = GetParameter("Source");
				}
				return _sourceParam;
			}
		}

		protected override void CacheParameters()
		{
			CacheParameter(sourceParam);
		}

		protected override void RegisterAsListener()
		{
			_controllers = RegisterAsListener(typeof(SelectEvent), typeof(UIEventController), UIEventController.AddController);
			if (base.listeningRange == EventListeningRange.SceneHierarchy)
			{
				SelectEvent.AddListener(this);
			}
		}

		protected override void UnRegisterAsListener()
		{
			SelectEvent.RemoveListener(this);
			UnRegisterAsListener(typeof(SelectEvent), typeof(UIEventController));
		}

		protected override void SetParameters(EventObject e)
		{
			SelectEvent selectEvent = e as SelectEvent;
			sourceParam.SetValue(selectEvent.source);
		}

		private void OnTransformChildrenChanged()
		{
			if (base.listeningRange == EventListeningRange.GameObjectHierarchy)
			{
				UnsubscribeFrom(_controllers, typeof(SelectEvent));
				_controllers = EventController.GetControllersInHierarchyOf(typeof(UIEventController), base.listeningTarget, UIEventController.AddController);
				SubscribeTo(_controllers, typeof(SelectEvent));
			}
		}
	}
}
