using UnityEngine;

namespace GameFlow
{
	[AddComponentMenu("")]
	[Help("en", "Sets a property of the specified @UnityManual{ConstantForce2D}.", null)]
	[Help("es", "Modifica una propiedad del componente @UnityManual{ConstantForce2D} especificado.", null)]
	public class SetConstantForce2DProperty : Action, IExecutableInEditor
	{
		public enum Property
		{
			Force,
			RelativeForce,
			Torque
		}

		[SerializeField]
		private ConstantForce2D _target;

		[SerializeField]
		private Variable _targetVar;

		[SerializeField]
		private Property _property;

		[SerializeField]
		private Vector2 _forceValue;

		[SerializeField]
		private Vector2 _relativeForceValue;

		[SerializeField]
		private float _torqueValue;

		[SerializeField]
		private Variable _valueVar;

		protected override void OnReset()
		{
			_target = base.gameObject.GetComponent<ConstantForce2D>();
		}

		public override void Setup()
		{
		}

		public override void Execute()
		{
			ConstantForce2D constantForce2D = _targetVar.GetValueAsComponent(_target, typeof(ConstantForce2D)) as ConstantForce2D;
			if (!(constantForce2D == null))
			{
				switch (_property)
				{
				case Property.Force:
					constantForce2D.force = _valueVar.GetValue(_forceValue);
					break;
				case Property.RelativeForce:
					constantForce2D.relativeForce = _valueVar.GetValue(_relativeForceValue);
					break;
				case Property.Torque:
					constantForce2D.torque = _valueVar.GetValue(_torqueValue);
					break;
				}
			}
		}
	}
}
