using System.Collections.Generic;

namespace GameFlow
{
	public interface IKeyContainer : IBlockContainer
	{
		List<Key> GetKeys();

		void KeyAdded(Key key);
	}
}
