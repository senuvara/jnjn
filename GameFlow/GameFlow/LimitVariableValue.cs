using UnityEngine;

namespace GameFlow
{
	[Help("es", "Limita el valor de la @GameFlow{Variable} especificada.", null)]
	[AddComponentMenu("")]
	[Help("en", "Limits the value of the specified @GameFlow{Variable}.", null)]
	public class LimitVariableValue : LimitValueAction
	{
		[SerializeField]
		private Variable _variable;

		public Variable variable => _variable;

		public override void Execute()
		{
			Execute(variable, this);
		}

		public static void Execute(Variable variable, LimitValueAction action)
		{
			if (variable == null || action == null)
			{
				return;
			}
			Variable variable2 = (!variable.isIndirect) ? variable : variable.indirection;
			if (!(variable2 == null) && !variable2.isReadOnly)
			{
				switch (variable2.dataType)
				{
				case DataType.Boolean:
					break;
				case DataType.Integer:
					variable2.intValue = Mathf.Clamp(variable2.intValue, action.intValue, action.maxIntValue);
					break;
				case DataType.Float:
					variable2.floatValue = Mathf.Clamp(variable2.floatValue, action.floatValue, action.maxFloatValue);
					break;
				case DataType.Vector2:
				{
					Vector2 vector2Value = variable2.vector2Value;
					float x3 = vector2Value.x;
					Vector2 vector2Value2 = action.vector2Value;
					float x4 = vector2Value2.x;
					Vector2 maxVector2Value = action.maxVector2Value;
					Vector2 vector2Value3 = default(Vector2);
					vector2Value3.x = Mathf.Clamp(x3, x4, maxVector2Value.x);
					Vector2 vector2Value4 = variable2.vector2Value;
					float y3 = vector2Value4.y;
					Vector2 vector2Value5 = action.vector2Value;
					float y4 = vector2Value5.y;
					Vector2 maxVector2Value2 = action.maxVector2Value;
					vector2Value3.y = Mathf.Clamp(y3, y4, maxVector2Value2.y);
					variable2.vector2Value = vector2Value3;
					break;
				}
				case DataType.Vector3:
				{
					Vector3 vector3Value = variable2.vector3Value;
					float x = vector3Value.x;
					Vector3 vector3Value2 = action.vector3Value;
					float x2 = vector3Value2.x;
					Vector3 maxVector3Value = action.maxVector3Value;
					Vector3 vector3Value3 = default(Vector3);
					vector3Value3.x = Mathf.Clamp(x, x2, maxVector3Value.x);
					Vector3 vector3Value4 = variable2.vector3Value;
					float y = vector3Value4.y;
					Vector3 vector3Value5 = action.vector3Value;
					float y2 = vector3Value5.y;
					Vector3 maxVector3Value2 = action.maxVector3Value;
					vector3Value3.y = Mathf.Clamp(y, y2, maxVector3Value2.y);
					Vector3 vector3Value6 = variable2.vector3Value;
					float z = vector3Value6.z;
					Vector3 vector3Value7 = action.vector3Value;
					float z2 = vector3Value7.z;
					Vector3 maxVector3Value3 = action.maxVector3Value;
					vector3Value3.z = Mathf.Clamp(z, z2, maxVector3Value3.z);
					variable2.vector3Value = vector3Value3;
					break;
				}
				case DataType.Rect:
				{
					Rect zero = RectExt.zero;
					zero.x = Mathf.Clamp(variable2.rectValue.x, action.maxRectValue.x, action.maxRectValue.x);
					zero.y = Mathf.Clamp(variable2.rectValue.y, action.maxRectValue.y, action.maxRectValue.y);
					zero.width = Mathf.Clamp(variable2.rectValue.width, action.maxRectValue.width, action.maxRectValue.width);
					zero.height = Mathf.Clamp(variable2.rectValue.height, action.maxRectValue.height, action.maxRectValue.height);
					variable2.rectValue = zero;
					break;
				}
				}
			}
		}
	}
}
