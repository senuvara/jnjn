using UnityEngine;

namespace GameFlow
{
	[Help("en", "Sets the velocity of the specified 2D object.", null)]
	[AddComponentMenu("")]
	[Help("es", "Ajusta la velocidad del objeto 2D especificado.", null)]
	public class SetVelocity2D : Action
	{
		[SerializeField]
		private Rigidbody2D _target;

		[SerializeField]
		private Variable _targetVar;

		[SerializeField]
		private Vector2 _velocity;

		[SerializeField]
		private Variable _velocityVar;

		protected Rigidbody2D target => _targetVar.GetValueAsComponent(_target, typeof(Rigidbody2D)) as Rigidbody2D;

		protected Vector2 velocity => _velocityVar.GetValue(_velocity);

		protected override void OnReset()
		{
			_target = base.gameObject.GetComponent<Rigidbody2D>();
		}

		public override void Execute()
		{
			Execute(target, velocity);
		}

		public static void Execute(Rigidbody2D rb, Vector2 velocity)
		{
			rb.velocity = velocity;
		}
	}
}
