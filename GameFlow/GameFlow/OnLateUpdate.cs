using UnityEngine;

namespace GameFlow
{
	[Help("es", "Programa que se ejecutará, si esta activom, en cada fotograma después de la ejecucion de todos los programas @GameFlow{OnUpdate}.", null)]
	[AddComponentMenu("GameFlow/Programs/On Late Update")]
	[DisallowMultipleComponent]
	[Help("en", "Program that will be executed, if enabled, every frame after the execution of all @GameFlow{OnUpdate} programs.", null)]
	public class OnLateUpdate : Program
	{
		private void Update()
		{
		}

		private void LateUpdate()
		{
			Restart();
			Step();
		}

		protected override bool WillFireEventOnProgramFinished()
		{
			return false;
		}
	}
}
