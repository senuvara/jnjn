using UnityEngine;

namespace GameFlow
{
	[Help("es", "Lee una propiedad del rayo (@GameFlow{Ray}) especificado.", null)]
	[Help("en", "Gets a property of the specified @GameFlow{Ray}.", null)]
	[AddComponentMenu("")]
	public class GetRayProperty : Action, IExecutableInEditor
	{
		public enum Property
		{
			Color,
			Direction,
			Enabled,
			End,
			Id,
			Length,
			Origin
		}

		[SerializeField]
		private Ray _ray;

		[SerializeField]
		private Variable _rayVar;

		[SerializeField]
		private Property _property = Property.Direction;

		[SerializeField]
		private Variable _output;

		public override void Execute()
		{
			if (_output == null)
			{
				return;
			}
			Ray ray = _rayVar.GetValue(_ray) as Ray;
			if (!(ray == null))
			{
				switch (_property)
				{
				case Property.Color:
					_output.SetValue(ray.color);
					break;
				case Property.Direction:
					_output.SetValue(ray.direction);
					break;
				case Property.Enabled:
					_output.SetValue(ray.enabledInEditor);
					break;
				case Property.End:
					_output.SetValue(ray.origin + ray.direction.normalized * ray.length);
					break;
				case Property.Id:
					_output.SetValue(ray.id);
					break;
				case Property.Length:
					_output.SetValue(ray.length);
					break;
				case Property.Origin:
					_output.SetValue(ray.origin);
					break;
				}
			}
		}
	}
}
