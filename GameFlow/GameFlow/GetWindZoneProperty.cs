using UnityEngine;

namespace GameFlow
{
	[Help("es", "Lee una propiedad del componente @UnityManual{WindZone} especificado.", null)]
	[AddComponentMenu("")]
	[Help("en", "Gets a property of the specified @UnityManual{WindZone} component.", null)]
	public class GetWindZoneProperty : Action, IExecutableInEditor
	{
		public enum Property
		{
			Mode,
			Radius,
			WindMain,
			WindPulseFrequency,
			WindPulseMagnitude,
			WindTurbulence
		}

		[SerializeField]
		private WindZone _source;

		[SerializeField]
		private Variable _sourceVar;

		[SerializeField]
		private Property _property;

		[SerializeField]
		private Variable _output;

		protected override void OnReset()
		{
			_source = base.gameObject.GetComponent<WindZone>();
		}

		public override void Setup()
		{
		}

		public override void Execute()
		{
			WindZone windZone = _sourceVar.GetValueAsComponent(_source, typeof(WindZone)) as WindZone;
			if (!(windZone == null))
			{
				switch (_property)
				{
				case Property.Mode:
					_output.SetValue(windZone.mode);
					break;
				case Property.Radius:
					_output.SetValue(windZone.radius);
					break;
				case Property.WindMain:
					_output.SetValue(windZone.windMain);
					break;
				case Property.WindPulseFrequency:
					_output.SetValue(windZone.windPulseFrequency);
					break;
				case Property.WindPulseMagnitude:
					_output.SetValue(windZone.windPulseMagnitude);
					break;
				case Property.WindTurbulence:
					_output.SetValue(windZone.windTurbulence);
					break;
				}
			}
		}
	}
}
