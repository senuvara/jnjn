using System.Collections.Generic;

namespace GameFlow
{
	public class LanguageEvent : EventObject
	{
		private static List<IEventListener> listeners = new List<IEventListener>();

		public Language language;

		public LanguageEvent(Language language)
		{
			this.language = language;
		}

		public override void DispatchToAll()
		{
			EventDispatcher.DispatchEventToListeners(this, listeners);
		}

		public static void AddListener(IEventListener listener)
		{
			listeners.AddOnlyOnce(listener);
		}

		public static void RemoveListener(IEventListener listener)
		{
			listeners.Remove(listener);
		}
	}
}
