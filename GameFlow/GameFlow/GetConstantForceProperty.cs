using UnityEngine;

namespace GameFlow
{
	[Help("en", "Gets a property of the specified @UnityManual{ConstantForce} component.", null)]
	[Help("es", "Lee una propiedad del componente @UnityManual{ConstantForce} especificado.", null)]
	[AddComponentMenu("")]
	public class GetConstantForceProperty : Action, IExecutableInEditor
	{
		public enum Property
		{
			Force,
			RelativeForce,
			RelativeTorque,
			Torque
		}

		[SerializeField]
		private ConstantForce _source;

		[SerializeField]
		private Variable _sourceVar;

		[SerializeField]
		private Property _property;

		[SerializeField]
		private Variable _output;

		protected override void OnReset()
		{
			_source = base.gameObject.GetComponent<ConstantForce>();
		}

		public override void Setup()
		{
		}

		public override void Execute()
		{
			ConstantForce constantForce = _sourceVar.GetValueAsComponent(_source, typeof(ConstantForce)) as ConstantForce;
			if (!(constantForce == null))
			{
				switch (_property)
				{
				case Property.Force:
					_output.SetValue(constantForce.force);
					break;
				case Property.RelativeForce:
					_output.SetValue(constantForce.relativeForce);
					break;
				case Property.RelativeTorque:
					_output.SetValue(constantForce.relativeTorque);
					break;
				case Property.Torque:
					_output.SetValue(constantForce.torque);
					break;
				}
			}
		}
	}
}
