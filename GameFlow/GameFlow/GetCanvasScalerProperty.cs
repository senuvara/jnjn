using UnityEngine;
using UnityEngine.UI;

namespace GameFlow
{
	[Help("en", "Gets a property of the specified Canvas Scaler component.", null)]
	[Help("es", "Lee una propiedad del componente Canvas Scaler especificado.", null)]
	[AddComponentMenu("")]
	public class GetCanvasScalerProperty : Action, IExecutableInEditor
	{
		public enum Property
		{
			DefaultSpriteDPI,
			DynamicPixelsPerUnit,
			FallbackScreenDPI,
			MatchWidthOrHeight,
			PhysicalUnit,
			ReferencePixelsPerUnit,
			ReferenceResolution,
			ScaleFactor,
			ScreenMatchMode,
			UiScaleMode
		}

		[SerializeField]
		private CanvasScaler _source;

		[SerializeField]
		private Variable _sourceVar;

		[SerializeField]
		private Property _property;

		[SerializeField]
		private Variable _output;

		protected override void OnReset()
		{
			_source = base.gameObject.GetComponent<CanvasScaler>();
		}

		public override void Setup()
		{
		}

		public override void Execute()
		{
			CanvasScaler canvasScaler = _sourceVar.GetValueAsComponent(_source, typeof(CanvasScaler)) as CanvasScaler;
			if (!(canvasScaler == null))
			{
				switch (_property)
				{
				case Property.DefaultSpriteDPI:
					_output.SetValue(canvasScaler.defaultSpriteDPI);
					break;
				case Property.DynamicPixelsPerUnit:
					_output.SetValue(canvasScaler.dynamicPixelsPerUnit);
					break;
				case Property.FallbackScreenDPI:
					_output.SetValue(canvasScaler.fallbackScreenDPI);
					break;
				case Property.MatchWidthOrHeight:
					_output.SetValue(canvasScaler.matchWidthOrHeight);
					break;
				case Property.PhysicalUnit:
					_output.SetValue(canvasScaler.physicalUnit);
					break;
				case Property.ReferencePixelsPerUnit:
					_output.SetValue(canvasScaler.referencePixelsPerUnit);
					break;
				case Property.ReferenceResolution:
					_output.SetValue(canvasScaler.referenceResolution);
					break;
				case Property.ScaleFactor:
					_output.SetValue(canvasScaler.scaleFactor);
					break;
				case Property.ScreenMatchMode:
					_output.SetValue(canvasScaler.screenMatchMode);
					break;
				case Property.UiScaleMode:
					_output.SetValue(canvasScaler.uiScaleMode);
					break;
				}
			}
		}
	}
}
