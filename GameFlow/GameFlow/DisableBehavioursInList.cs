using UnityEngine;

namespace GameFlow
{
	[Help("es", "Desactiva todos los componentes de tipo Behaviour de la lista (@GameFlow{List}) especificada.", null)]
	[AddComponentMenu("")]
	[Help("en", "Disables all the Behaviour components found in the specified @GameFlow{List}.", null)]
	public class DisableBehavioursInList : ListAction
	{
		public override void Execute()
		{
			List list = GetList();
			if (list != null && list.dataType == DataType.Object)
			{
				for (int i = 0; i < list.Count; i++)
				{
					DisableBehaviour.Execute(list.GetObjectAt(i) as Behaviour);
				}
			}
		}
	}
}
