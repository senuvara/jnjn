using System.Collections.Generic;

namespace GameFlow
{
	public class LocalizedString
	{
		private Dictionary<string, string> localizations = new Dictionary<string, string>();

		public static LocalizedString Create()
		{
			return new LocalizedString();
		}

		public LocalizedString Add(string code, string text)
		{
			if (!localizations.ContainsKey(code))
			{
				localizations.Add(code, text);
			}
			return this;
		}

		public string ToString(string code, string defaultText = "")
		{
			if (!localizations.ContainsKey(code))
			{
				return defaultText;
			}
			return localizations[code];
		}

		public string ToString(string defaultText)
		{
			return ToString(LanguageUtils.systemLanguage.code, defaultText);
		}

		public override string ToString()
		{
			return ToString(LanguageUtils.systemLanguage.code, string.Empty);
		}
	}
}
