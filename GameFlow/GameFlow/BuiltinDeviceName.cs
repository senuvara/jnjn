using System;
using UnityEngine;

namespace GameFlow
{
	[Help("en", "A built-in @GameFlow{Variable} that returns name of the current device.", null)]
	[Help("es", "Una @GameFlow{Variable} integrada que devuelve el nombre del dispositivo en uso.", null)]
	[AddComponentMenu("")]
	public class BuiltinDeviceName : BuiltinVariable
	{
		public override string stringValue => SystemInfo.deviceName;

		public override Type GetVariableType()
		{
			return typeof(string);
		}

		protected override string GetVariableId()
		{
			return "Device Name";
		}
	}
}
