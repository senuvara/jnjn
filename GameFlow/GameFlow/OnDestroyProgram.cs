namespace GameFlow
{
	public abstract class OnDestroyProgram : Program
	{
		private void OnDestroy()
		{
			PoolStamp component = base.gameObject.GetComponent<PoolStamp>();
			if (component == null || component.pool == null)
			{
				Restart();
			}
		}
	}
}
