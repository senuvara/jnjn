using UnityEngine;

namespace GameFlow
{
	[AddComponentMenu("")]
	[Help("es", "Devuelve el primer material del objeto especificado.", null)]
	[Help("en", "Gets the first material of the specified object.", null)]
	public class GetMaterial : Action, IExecutableInEditor
	{
		[SerializeField]
		private Renderer _source;

		[SerializeField]
		private Variable _sourceVar;

		[SerializeField]
		private Variable _output;

		public Renderer source => _sourceVar.GetValueAsComponent(_source, typeof(Renderer)) as Renderer;

		protected override void OnReset()
		{
			_source = base.gameObject.GetComponent<Renderer>();
		}

		public override void Execute()
		{
			if (!(_output == null))
			{
				_output.SetValue(Execute(source));
			}
		}

		public static Material Execute(Renderer renderer)
		{
			if (renderer == null)
			{
				return null;
			}
			return renderer.sharedMaterial;
		}
	}
}
