using UnityEngine;

namespace GameFlow
{
	[Help("es", "Modifica el valor de una propiedad de la fuerza (@GameFlow{Force}) especificada.", null)]
	[AddComponentMenu("")]
	[Help("en", "Modifies the values of a property of the specified @GameFlow{Force}.", null)]
	public class SetForceProperty : Action, IExecutableInEditor
	{
		public enum Property
		{
			Color,
			Direction,
			Magnitude
		}

		[SerializeField]
		private Force _force;

		[SerializeField]
		private Variable _forceVar;

		[SerializeField]
		private Property _property = Property.Direction;

		[SerializeField]
		private Color _colorValue = Color.yellow;

		[SerializeField]
		private Vector3 _vector3Value;

		[SerializeField]
		private float _floatValue = 1f;

		[SerializeField]
		private Variable _valueVar;

		public override void Setup()
		{
			OnActivate.RegisterAllInactiveAsListeners();
		}

		public override void Execute()
		{
			Force force = _forceVar.GetValue(_force) as Force;
			if (!(force == null))
			{
				switch (_property)
				{
				case Property.Color:
					force.color = _valueVar.GetValue(_colorValue);
					break;
				case Property.Direction:
					force.direction = _valueVar.GetValue(_vector3Value);
					break;
				case Property.Magnitude:
					force.magnitude = _valueVar.GetValue(_floatValue);
					break;
				}
			}
		}
	}
}
