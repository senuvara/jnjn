using UnityEngine;

namespace GameFlow
{
	[AddComponentMenu("")]
	[Help("es", "Hace aparecer gradualmente el @UnityManual{GameObject} especificado.", null)]
	[Help("en", "Makes the specified @UnityManual{GameObject} appear gradually.", null)]
	public class FadeIn : FadeOut
	{
		public override float GetEndAlpha()
		{
			return 1f;
		}
	}
}
