using System;
using UnityEngine;

namespace GameFlow
{
	[AddComponentMenu("")]
	[Help("en", "Gets an offset in a circle centered on the specified object.", null)]
	[Help("es", "Devuelve una posición relativa en un círculo centrado en el objeto especificado.", null)]
	public class GetRadialOffset : Action, IExecutableInEditor
	{
		[SerializeField]
		private Transform _center;

		[SerializeField]
		private Variable _centerVar;

		[SerializeField]
		private float _radius;

		[SerializeField]
		private Variable _radiusVar;

		[SerializeField]
		private Axis _axis = Axis.Y;

		[SerializeField]
		private Variable _axisVar;

		[SerializeField]
		private float _angle;

		[SerializeField]
		private Variable _angleVar;

		[SerializeField]
		private Space _space;

		[SerializeField]
		private Variable _spaceVar;

		[SerializeField]
		private Variable _output;

		public Transform center => _centerVar.GetValueAsComponent(_center, typeof(Transform)) as Transform;

		public float radius => _radiusVar.GetValue(_radius);

		public Axis axis => (Axis)(object)_axisVar.GetValue(_axis);

		public float angle => _angleVar.GetValue(_angle);

		public Space space => (Space)(object)_spaceVar.GetValue(_space);

		protected override void OnReset()
		{
			_center = base.gameObject.GetComponent<Transform>();
		}

		public override void Execute()
		{
			if (_output == null)
			{
				return;
			}
			Transform center = this.center;
			if (!(center == null))
			{
				float radius = this.radius;
				Vector3 vector = Vector3.zero;
				float num = Mathf.Sin(angle * ((float)Math.PI / 180f)) * radius;
				float num2 = Mathf.Cos(angle * ((float)Math.PI / 180f)) * radius;
				switch (axis)
				{
				case Axis.X:
					vector = new Vector3(0f, num2, 0f - num);
					break;
				case Axis.Y:
					vector = new Vector3(num, 0f, num2);
					break;
				case Axis.Z:
					vector = new Vector3(num, num2, 0f);
					break;
				}
				vector += this.center.position;
				if (space == Space.Self)
				{
					vector = center.TransformPoint(vector);
				}
				_output.SetValue(vector);
			}
		}
	}
}
