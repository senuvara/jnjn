using UnityEngine;

namespace GameFlow
{
	[Help("es", "Limita la rotación del Transform especificado.", null)]
	[AddComponentMenu("")]
	[Help("en", "Clamps the rotation of the specified Transform.", null)]
	public class ClampRotation : TransformAction
	{
		[SerializeField]
		private Vector3 _minValue;

		[SerializeField]
		private Variable _minValueVar;

		[SerializeField]
		private Vector3 _maxValue;

		[SerializeField]
		private Variable _maxValueVar;

		public Vector3 minValue => _minValueVar.GetValue(_minValue);

		public Vector3 maxValue => _maxValueVar.GetValue(_maxValue);

		public override void Execute()
		{
			Transform target = base.target;
			if (!(target == null))
			{
				Execute(target, base.space, minValue, maxValue, base.mask);
			}
		}

		public static void Execute(Transform t, Space space, Vector3 min, Vector3 max, Vector3 mask)
		{
			if (!(t == null))
			{
				switch (space)
				{
				case Space.World:
				{
					Vector3 value = t.eulerAngles.Clamped(min, max, angles: true);
					t.eulerAngles = t.eulerAngles.Masked(value, mask);
					break;
				}
				case Space.Self:
				{
					Vector3 value = t.localEulerAngles.Clamped(min, max, angles: true);
					t.localEulerAngles = t.localEulerAngles.Masked(value, mask);
					break;
				}
				}
			}
		}
	}
}
