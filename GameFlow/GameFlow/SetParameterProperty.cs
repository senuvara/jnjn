using UnityEngine;

namespace GameFlow
{
	[AddComponentMenu("")]
	[Help("es", "Modifica una propiedad del parámetro (@GameFlow{Parameter}) especificado.", null)]
	[Help("en", "Sets a property of the specified @GameFlow{Parameter}.", null)]
	public class SetParameterProperty : ParameterAction
	{
		public enum Property
		{
			Hidden,
			Label,
			Visible
		}

		[SerializeField]
		private Property _property;
	}
}
