using UnityEngine;

namespace GameFlow
{
	[AddComponentMenu("")]
	[Help("es", "Muestra el puntero del ratón.", null)]
	[Help("en", "Shows the mouse cursor.", null)]
	public class ShowMouseCursor : Action
	{
		public override void Execute()
		{
			Cursor.visible = true;
		}
	}
}
