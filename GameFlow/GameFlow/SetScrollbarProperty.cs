using UnityEngine;
using UnityEngine.UI;

namespace GameFlow
{
	[AddComponentMenu("")]
	[Help("es", "Modifica el valor de una propiedad del componente Scrollbar especificado.", null)]
	[Help("en", "Modifies the value of a property of the specified Scrollbar component.", null)]
	public class SetScrollbarProperty : Action, IExecutableInEditor
	{
		public enum Property
		{
			AnimationTriggers_DisabledTrigger,
			AnimationTriggers_HighlightedTrigger,
			AnimationTriggers_NormalTrigger,
			AnimationTriggers_PressedTrigger,
			Colors_ColorMultiplier,
			Colors_DisabledColor,
			Colors_FadeDuration,
			Colors_HighlightedColor,
			Colors_NormalColor,
			Colors_PressedColor,
			Direction,
			HandleRect,
			Image,
			Interactable,
			Navigation_Mode,
			Navigation_SelectOnDown,
			Navigation_SelectOnLeft,
			Navigation_SelectOnRight,
			Navigation_SelectOnUp,
			NumberOfSteps,
			Size,
			SpriteState_DisabledSprite,
			SpriteState_HighlightedSprite,
			SpriteState_PressedSprite,
			TargetGraphic,
			Transition,
			Value
		}

		[SerializeField]
		private Scrollbar _target;

		[SerializeField]
		private Variable _targetVar;

		[SerializeField]
		private Property _property;

		[SerializeField]
		private string _disabledTriggerValue;

		[SerializeField]
		private string _highlightedTriggerValue;

		[SerializeField]
		private string _normalTriggerValue;

		[SerializeField]
		private string _pressedTriggerValue;

		[SerializeField]
		private float _colorMultiplierValue;

		[SerializeField]
		private Color _disabledColorValue;

		[SerializeField]
		private float _fadeDurationValue;

		[SerializeField]
		private Color _highlightedColorValue;

		[SerializeField]
		private Color _normalColorValue;

		[SerializeField]
		private Color _pressedColorValue;

		private ColorBlock colors;

		[SerializeField]
		private Scrollbar.Direction _directionValue;

		[SerializeField]
		private RectTransform _handleRectValue;

		[SerializeField]
		private Image _imageValue;

		[SerializeField]
		private bool _interactableValue;

		[SerializeField]
		private Navigation.Mode _navigationModeValue;

		[SerializeField]
		private Selectable _selectOnDownValue;

		[SerializeField]
		private Selectable _selectOnLeftValue;

		[SerializeField]
		private Selectable _selectOnRightValue;

		[SerializeField]
		private Selectable _selectOnUpValue;

		private Navigation navigation;

		[SerializeField]
		private int _numberOfStepsValue;

		[SerializeField]
		private float _sizeValue;

		[SerializeField]
		private Sprite _disabledSpriteValue;

		[SerializeField]
		private Sprite _highlightedSpriteValue;

		[SerializeField]
		private Sprite _pressedSpriteValue;

		private SpriteState spriteState;

		[SerializeField]
		private Graphic _targetGraphicValue;

		[SerializeField]
		private Selectable.Transition _transitionValue;

		[SerializeField]
		private float _valueValue;

		[SerializeField]
		private Variable _valueVar;

		public Scrollbar target => _targetVar.GetValueAsComponent(_target, typeof(Scrollbar)) as Scrollbar;

		protected override void OnReset()
		{
			_target = base.gameObject.GetComponent<Scrollbar>();
		}

		public override void Setup()
		{
		}

		public override void Execute()
		{
			Scrollbar target = this.target;
			if (!(target == null))
			{
				switch (_property)
				{
				case Property.AnimationTriggers_DisabledTrigger:
					target.animationTriggers.disabledTrigger = _valueVar.GetValue(_disabledTriggerValue);
					break;
				case Property.AnimationTriggers_HighlightedTrigger:
					target.animationTriggers.highlightedTrigger = _valueVar.GetValue(_highlightedTriggerValue);
					break;
				case Property.AnimationTriggers_NormalTrigger:
					target.animationTriggers.normalTrigger = _valueVar.GetValue(_normalTriggerValue);
					break;
				case Property.AnimationTriggers_PressedTrigger:
					target.animationTriggers.pressedTrigger = _valueVar.GetValue(_pressedTriggerValue);
					break;
				case Property.Colors_ColorMultiplier:
					colors = default(ColorBlock);
					colors = target.colors;
					colors.colorMultiplier = _valueVar.GetValue(_colorMultiplierValue);
					target.colors = colors;
					break;
				case Property.Colors_DisabledColor:
					colors = default(ColorBlock);
					colors = target.colors;
					colors.disabledColor = _valueVar.GetValue(_disabledColorValue);
					target.colors = colors;
					break;
				case Property.Colors_FadeDuration:
					colors = default(ColorBlock);
					colors = target.colors;
					colors.fadeDuration = _valueVar.GetValue(_fadeDurationValue);
					target.colors = colors;
					break;
				case Property.Colors_HighlightedColor:
					colors = default(ColorBlock);
					colors = target.colors;
					colors.highlightedColor = _valueVar.GetValue(_highlightedColorValue);
					target.colors = colors;
					break;
				case Property.Colors_NormalColor:
					colors = default(ColorBlock);
					colors = target.colors;
					colors.normalColor = _valueVar.GetValue(_normalColorValue);
					target.colors = colors;
					break;
				case Property.Colors_PressedColor:
					colors = default(ColorBlock);
					colors = target.colors;
					colors.pressedColor = _valueVar.GetValue(_pressedColorValue);
					target.colors = colors;
					break;
				case Property.Direction:
					target.direction = (Scrollbar.Direction)(object)_valueVar.GetValue(_directionValue);
					break;
				case Property.HandleRect:
					target.handleRect = (_valueVar.GetValueAsComponent(_handleRectValue, typeof(RectTransform)) as RectTransform);
					break;
				case Property.Image:
					target.image = (_valueVar.GetValueAsComponent(_imageValue, typeof(Image)) as Image);
					break;
				case Property.Interactable:
					target.interactable = _valueVar.GetValue(_interactableValue);
					break;
				case Property.Navigation_Mode:
					navigation = default(Navigation);
					navigation = target.navigation;
					navigation.mode = (Navigation.Mode)(object)_valueVar.GetValue(_navigationModeValue);
					target.navigation = navigation;
					break;
				case Property.Navigation_SelectOnDown:
					navigation = default(Navigation);
					navigation = target.navigation;
					navigation.selectOnDown = (_valueVar.GetValue(_selectOnDownValue) as Selectable);
					target.navigation = navigation;
					break;
				case Property.Navigation_SelectOnLeft:
					navigation = default(Navigation);
					navigation = target.navigation;
					navigation.selectOnLeft = (_valueVar.GetValue(_selectOnLeftValue) as Selectable);
					target.navigation = navigation;
					break;
				case Property.Navigation_SelectOnRight:
					navigation = default(Navigation);
					navigation = target.navigation;
					navigation.selectOnRight = (_valueVar.GetValue(_selectOnRightValue) as Selectable);
					target.navigation = navigation;
					break;
				case Property.Navigation_SelectOnUp:
					navigation = default(Navigation);
					navigation = target.navigation;
					navigation.selectOnUp = (_valueVar.GetValue(_selectOnUpValue) as Selectable);
					target.navigation = navigation;
					break;
				case Property.NumberOfSteps:
					target.numberOfSteps = _valueVar.GetValue(_numberOfStepsValue);
					break;
				case Property.Size:
					target.size = _valueVar.GetValue(_sizeValue);
					break;
				case Property.SpriteState_DisabledSprite:
					spriteState = default(SpriteState);
					spriteState = target.spriteState;
					spriteState.disabledSprite = (_valueVar.GetValue(_disabledSpriteValue) as Sprite);
					target.spriteState = spriteState;
					break;
				case Property.SpriteState_HighlightedSprite:
					spriteState = default(SpriteState);
					spriteState = target.spriteState;
					spriteState.highlightedSprite = (_valueVar.GetValue(_highlightedSpriteValue) as Sprite);
					target.spriteState = spriteState;
					break;
				case Property.SpriteState_PressedSprite:
					spriteState = default(SpriteState);
					spriteState = target.spriteState;
					spriteState.pressedSprite = (_valueVar.GetValue(_pressedSpriteValue) as Sprite);
					target.spriteState = spriteState;
					break;
				case Property.TargetGraphic:
					target.targetGraphic = (_valueVar.GetValueAsComponent(_targetGraphicValue, typeof(Graphic)) as Graphic);
					break;
				case Property.Transition:
					target.transition = (Selectable.Transition)(object)_valueVar.GetValue(_transitionValue);
					break;
				case Property.Value:
					target.value = _valueVar.GetValue(_valueValue);
					break;
				}
			}
		}
	}
}
