using System;
using UnityEngine;

namespace GameFlow
{
	[Help("es", "Modifica el valor de la @GameFlow{Variable} especificada.", null)]
	[Help("en", "Sets the value of the specified @GameFlow{Variable}.", null)]
	[AddComponentMenu("")]
	public class SetVariableValue : VariableAction
	{
		[SerializeField]
		private string _format;

		[SerializeField]
		private Variable _formatVar;

		public string format => _formatVar.GetValue(_format);

		public override void Execute()
		{
			Execute(base.variable, this, base.gameObject, format);
		}

		public static void Execute(Variable variable, ValueAction action, GameObject context = null, string format = "")
		{
			if (variable == null || action == null)
			{
				return;
			}
			Variable variable2 = (!variable.isIndirect) ? variable : variable.indirection;
			if (variable2 == null)
			{
				return;
			}
			switch (variable2.dataType)
			{
			case DataType.String:
			{
				Variable valueVar = action.valueVar;
				if (valueVar != null && valueVar.IsNumeric())
				{
					variable2.stringValue = action.valueVar.GetValue(action.stringValue, format, context).Expanded();
				}
				else
				{
					variable2.stringValue = action.valueVar.GetValue(action.stringValue).Expanded();
				}
				break;
			}
			case DataType.Tag:
				variable2.tagValue = action.stringValue;
				break;
			case DataType.Integer:
				variable2.intValue = action.intValue;
				break;
			case DataType.Layer:
				variable2.layerValue = action.intValue;
				break;
			case DataType.Float:
				variable2.floatValue = action.floatValue;
				break;
			case DataType.Boolean:
				variable2.boolValue = action.boolValue;
				break;
			case DataType.Toggle:
				variable2.toggleValue = action.boolValue;
				break;
			case DataType.Vector2:
				variable2.vector2Value = action.vector2Value;
				break;
			case DataType.Vector3:
				variable2.vector3Value = action.vector3Value;
				break;
			case DataType.Rect:
				variable2.rectValue = action.rectValue;
				break;
			case DataType.Color:
				variable2.colorValue = action.colorValue;
				break;
			case DataType.Object:
				variable2.objectValue = action.objectValue;
				break;
			case DataType.Enum:
			{
				Type enumType = (!(action.valueVar == null)) ? action.valueVar.type : variable2.type;
				variable2.enumValue = (Enum)Enum.ToObject(enumType, action.enumIntValue);
				break;
			}
			case DataType.AnimationCurve:
				variable2.animationCurveValue = action.animationCurveValue;
				break;
			case DataType.Bounds:
				variable2.boundsValue = action.boundsValue;
				break;
			case DataType.Quaternion:
				variable2.quaternionValue = action.quaternionValue;
				break;
			case DataType.Vector4:
				variable2.vector4Value = action.vector4Value;
				break;
			}
		}
	}
}
