using UnityEngine;

namespace GameFlow
{
	[AddComponentMenu("")]
	[Help("en", "Gets a property of the specified @GameFlow{Force}.", null)]
	[Help("es", "Lee una propiedad de la fuerza (@GameFlow{Force}) especificada.", null)]
	public class GetForceProperty : Action, IExecutableInEditor
	{
		public enum Property
		{
			Color,
			Direction,
			Id,
			Magnitude
		}

		[SerializeField]
		private Force _force;

		[SerializeField]
		private Variable _forceVar;

		[SerializeField]
		private Property _property = Property.Direction;

		[SerializeField]
		private Variable _output;

		public override void Execute()
		{
			if (_output == null)
			{
				return;
			}
			Force force = _forceVar.GetValue(_force) as Force;
			if (!(force == null))
			{
				switch (_property)
				{
				case Property.Color:
					_output.SetValue(force.color);
					break;
				case Property.Direction:
					_output.SetValue(force.direction);
					break;
				case Property.Id:
					_output.SetValue(force.id);
					break;
				case Property.Magnitude:
					_output.SetValue(force.magnitude);
					break;
				}
			}
		}
	}
}
