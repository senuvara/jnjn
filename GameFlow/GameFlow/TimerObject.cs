using UnityEngine;

namespace GameFlow
{
	public class TimerObject
	{
		public bool ignorePause;

		private float _duration;

		private float _elapsedTime;

		private float _startTime;

		private float _durationDelta;

		public bool stopped
		{
			get;
			private set;
		}

		public float duration
		{
			get
			{
				return _duration;
			}
			set
			{
				_duration = value;
				_durationDelta = ((!(value > 0f)) ? 1f : value);
			}
		}

		public float elapsedTime
		{
			get
			{
				if (!stopped)
				{
					_elapsedTime = _currentTime - _startTime + Time.deltaTime / 2f;
				}
				return _elapsedTime;
			}
		}

		public float elapsedTimeDelta => elapsedTime / _durationDelta;

		public bool expired => duration >= 0f && elapsedTime >= duration;

		private float _currentTime => (!ignorePause) ? Time.time : Time.realtimeSinceStartup;

		public TimerObject(float duration = 0f, bool ignorePause = false)
		{
			this.duration = duration;
			this.ignorePause = ignorePause;
			stopped = true;
		}

		public void Reset()
		{
			_elapsedTime = 0f;
			stopped = true;
		}

		public void Restart()
		{
			_startTime = _currentTime;
			stopped = false;
		}

		public void Resume()
		{
			_startTime = _currentTime - ((!stopped) ? 0f : _elapsedTime);
			stopped = false;
		}

		public void Stop()
		{
			_elapsedTime = _currentTime - _startTime;
			stopped = true;
		}

		public void Finish()
		{
			stopped = true;
			_elapsedTime = _duration;
		}
	}
}
