using UnityEngine;

namespace GameFlow
{
	[DisallowMultipleComponent]
	[Help("en", "Program that will be executed when an UI component is clicked in the listening range.", null)]
	[Help("es", "Programa que se ejecutará cuando se haga click sobre un componente UI en el rango de escucha.", null)]
	[AddComponentMenu("GameFlow/Programs/On Click")]
	public class OnClick : PointerEventProgram
	{
		private EventController[] _controllers;

		protected override void RegisterAsListener()
		{
			_controllers = RegisterAsListener(typeof(ClickEvent), typeof(UIEventController), UIEventController.AddController);
			if (base.listeningRange == EventListeningRange.SceneHierarchy)
			{
				ClickEvent.AddListener(this);
			}
		}

		protected override void UnRegisterAsListener()
		{
			ClickEvent.RemoveListener(this);
			UnRegisterAsListener(typeof(ClickEvent), typeof(UIEventController));
		}

		protected override void SetParameters(EventObject e)
		{
			ClickEvent clickEvent = e as ClickEvent;
			base.sourceParam.SetValue(clickEvent.source);
			base.positionParam.SetValue(clickEvent.position);
		}

		private void OnTransformChildrenChanged()
		{
			if (base.listeningRange == EventListeningRange.GameObjectHierarchy)
			{
				UnsubscribeFrom(_controllers, typeof(ClickEvent));
				_controllers = EventController.GetControllersInHierarchyOf(typeof(UIEventController), base.listeningTarget, UIEventController.AddController);
				SubscribeTo(_controllers, typeof(ClickEvent));
			}
		}
	}
}
