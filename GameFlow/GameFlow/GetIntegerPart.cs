using UnityEngine;

namespace GameFlow
{
	[Help("en", "Gets the integer part of the specified numeric value.", null)]
	[Help("es", "Devuelve la parte entera del valor numérico especificado.", null)]
	[AddComponentMenu("")]
	public class GetIntegerPart : Action, IExecutableInEditor
	{
		[SerializeField]
		private float _value;

		[SerializeField]
		private Variable _valueVar;

		[SerializeField]
		private Variable _output;

		public float value => _valueVar.GetValue(_value);

		public override void Execute()
		{
			if (_output != null)
			{
				float value = this.value;
				_output.SetValue((!(value >= 0f)) ? Mathf.CeilToInt(value) : Mathf.FloorToInt(value));
			}
		}
	}
}
