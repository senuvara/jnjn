using UnityEngine;

namespace GameFlow
{
	[AddComponentMenu("")]
	[Help("es", "Fija una rotación relativa para el @UnityManual{Transform} especificado alrededor de un pivote.", null)]
	[Help("en", "Sets a relative rotation of the target @UnityManual{Transform} around the specified pivot.", null)]
	public class SetOrbitRotation : TransformAction
	{
		[SerializeField]
		private Transform _pivot;

		[SerializeField]
		private Variable _pivotVar;

		[SerializeField]
		private Axis _axis = Axis.Y;

		[SerializeField]
		private Variable _axisVar;

		[SerializeField]
		private float _angle;

		[SerializeField]
		private Variable _angleVar;

		public Transform pivot => _pivotVar.GetValueAsComponent(_pivot, typeof(Transform)) as Transform;

		public Axis axis => (Axis)(object)_axisVar.GetValue(_axis);

		public Vector3 axisDirection => TransformUtils.GetAxisDirection(axis);

		public float angle => _angleVar.GetValue(_angle);

		protected override void OnReset()
		{
			base.OnReset();
			base.relative = true;
		}

		public override void Execute()
		{
			Transform pivot = this.pivot;
			if (!(pivot == null))
			{
				Execute(base.target, pivot.position, axisDirection, angle, base.multiplier);
			}
		}

		public static void Execute(Transform t, Vector3 pivotPosition, Vector3 axisDirection, float angle, float multiplier)
		{
			if (!(t == null))
			{
				t.RotateAround(pivotPosition, axisDirection, angle * multiplier);
			}
		}
	}
}
