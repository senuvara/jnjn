using UnityEngine;

namespace GameFlow
{
	[AddComponentMenu("")]
	[Help("en", "Gets a property of the specified @UnityManual{MeshRenderer} component.", null)]
	[Help("es", "Lee una propiedad del componente @UnityManual{MeshRenderer} especificado.", null)]
	public class GetMeshRendererProperty : Action, IExecutableInEditor
	{
		public enum Property
		{
			AdditionalVertexStreams,
			Bounds,
			Enabled,
			IsPartOfStaticBatch,
			IsVisible,
			LightmapIndex,
			LightmapScaleOffset,
			LocalToWorldMatrix,
			Material,
			Materials,
			ProbeAnchor,
			RealtimeLightmapIndex,
			RealtimeLightmapScaleOffset,
			ReceiveShadows,
			ReflectionProbeUsage,
			ShadowCastingMode,
			SharedMaterial,
			SharedMaterials,
			SortingLayerID,
			SortingLayerName,
			SortingOrder,
			LightProbeUsage,
			WorldToLocalMatrix
		}

		[SerializeField]
		private MeshRenderer _source;

		[SerializeField]
		private Variable _sourceVar;

		[SerializeField]
		private Property _property;

		[SerializeField]
		private Variable _output;

		protected override void OnReset()
		{
			_source = base.gameObject.GetComponent<MeshRenderer>();
		}

		public override void Setup()
		{
		}

		public override void Execute()
		{
			MeshRenderer meshRenderer = _sourceVar.GetValueAsComponent(_source, typeof(MeshRenderer)) as MeshRenderer;
			if (!(meshRenderer == null))
			{
				switch (_property)
				{
				case Property.LocalToWorldMatrix:
				case Property.Materials:
				case Property.SharedMaterials:
					break;
				case Property.AdditionalVertexStreams:
					_output.SetValue(meshRenderer.additionalVertexStreams);
					break;
				case Property.Bounds:
					_output.SetValue(meshRenderer.bounds);
					break;
				case Property.Enabled:
					_output.SetValue(meshRenderer.enabled);
					break;
				case Property.IsPartOfStaticBatch:
					_output.SetValue(meshRenderer.isPartOfStaticBatch);
					break;
				case Property.IsVisible:
					_output.SetValue(meshRenderer.isVisible);
					break;
				case Property.LightmapIndex:
					_output.SetValue(meshRenderer.lightmapIndex);
					break;
				case Property.LightmapScaleOffset:
					_output.SetValue(meshRenderer.lightmapScaleOffset);
					break;
				case Property.Material:
					_output.SetValue(meshRenderer.material);
					break;
				case Property.ProbeAnchor:
					_output.SetValue(meshRenderer.probeAnchor);
					break;
				case Property.RealtimeLightmapIndex:
					_output.SetValue(meshRenderer.realtimeLightmapIndex);
					break;
				case Property.RealtimeLightmapScaleOffset:
					_output.SetValue(meshRenderer.realtimeLightmapScaleOffset);
					break;
				case Property.ReceiveShadows:
					_output.SetValue(meshRenderer.receiveShadows);
					break;
				case Property.ReflectionProbeUsage:
					_output.SetValue(meshRenderer.reflectionProbeUsage);
					break;
				case Property.ShadowCastingMode:
					_output.SetValue(meshRenderer.shadowCastingMode);
					break;
				case Property.SharedMaterial:
					_output.SetValue(meshRenderer.sharedMaterial);
					break;
				case Property.SortingLayerID:
					_output.SetValue(meshRenderer.sortingLayerID);
					break;
				case Property.SortingLayerName:
					_output.SetValue(meshRenderer.sortingLayerName);
					break;
				case Property.SortingOrder:
					_output.SetValue(meshRenderer.sortingOrder);
					break;
				case Property.LightProbeUsage:
					_output.SetValue(meshRenderer.lightProbeUsage);
					break;
				}
			}
		}
	}
}
