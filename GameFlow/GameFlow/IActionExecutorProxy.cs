namespace GameFlow
{
	public interface IActionExecutorProxy
	{
		void SetupAction(Action action);

		bool ExecuteActionFirstStep(Action action);

		bool ExecuteActionStep(Action action);

		bool IsActionFinished(Action action);

		bool DoesActionYield(Action action);
	}
}
