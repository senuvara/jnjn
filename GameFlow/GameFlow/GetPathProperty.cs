using UnityEngine;

namespace GameFlow
{
	[AddComponentMenu("")]
	[Help("es", "Lee una propiedad del componente @GameFlow{Path} especificado.", null)]
	[Help("en", "Gets a property of the specified @GameFlow{Path} component.", null)]
	public class GetPathProperty : Action, IExecutableInEditor
	{
		public enum Property
		{
			PathType,
			Subdivisions,
			Closed,
			Color,
			Length,
			Points
		}

		[SerializeField]
		private Path _target;

		[SerializeField]
		private Variable _targetVar;

		[SerializeField]
		private Property _property;

		[SerializeField]
		private Variable _output;

		protected override void OnReset()
		{
		}

		public override void Setup()
		{
		}

		public override void Execute()
		{
			if (_output == null)
			{
				return;
			}
			Path path = _target;
			if (path == null)
			{
				path = (_targetVar.GetValueAsComponent(_target, typeof(Path)) as Path);
			}
			if (!(path == null))
			{
				switch (_property)
				{
				case Property.PathType:
					_output.SetValue(path.pathType);
					break;
				case Property.Subdivisions:
					_output.SetValue(path.subdivisions);
					break;
				case Property.Closed:
					_output.SetValue(path.closed);
					break;
				case Property.Color:
					_output.SetValue(path.color);
					break;
				case Property.Length:
					_output.SetValue(path.length);
					break;
				case Property.Points:
					_output.SetValue(path.NonNullObjectCount);
					break;
				}
			}
		}
	}
}
