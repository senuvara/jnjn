using UnityEngine;

namespace GameFlow
{
	[Help("es", "Destruye el @UnityManual{GameObject} o @UnityManual{Component} especificado.", null)]
	[Help("en", "Destroys the specified @UnityManual{GameObject} or @UnityManual{Component}.", null)]
	[AddComponentMenu("")]
	public class Destroy : Action, IExecutableInEditor
	{
		public enum TargetType
		{
			GameObject,
			Component
		}

		[SerializeField]
		private TargetType _targetType;

		[SerializeField]
		private Object _target;

		[SerializeField]
		private Variable _targetVar;

		public Object target => _targetVar.GetValue(_target);

		public override void Execute()
		{
			Execute(target);
		}

		public static void Execute(Object target)
		{
			if (target == null)
			{
				return;
			}
			GameObject gameObject = target as GameObject;
			if (gameObject != null)
			{
				PoolStamp component = gameObject.GetComponent<PoolStamp>();
				if (component != null && component.pool != null)
				{
					component.pool.ReleaseObject(gameObject);
					OnDestroyProgram component2 = gameObject.GetComponent<OnDestroyProgram>();
					if (component2 != null)
					{
						component2.Execute();
					}
					return;
				}
			}
			Object.DestroyObject(target);
		}
	}
}
