namespace GameFlow
{
	public interface IIdentifiable
	{
		string GetIdForControls();

		string GetIdForSelectors();

		string GetTypeForSelectors();
	}
}
