using UnityEngine;

namespace GameFlow
{
	public class ActionExecutor
	{
		public static IActionExecutorProxy proxy;

		public static void SetupAction(Action action)
		{
			if (proxy != null)
			{
				proxy.SetupAction(action);
			}
			action.Setup();
		}

		public static bool ExecuteActionFirstStep(Action action)
		{
			bool result = true;
			if (proxy != null)
			{
				return proxy.ExecuteActionFirstStep(action);
			}
			action.FirstStep();
			return result;
		}

		public static bool ExecuteActionStep(Action action)
		{
			bool result = true;
			if (proxy != null)
			{
				return proxy.ExecuteActionStep(action);
			}
			action.Step();
			return result;
		}

		public static bool IsActionFinished(Action action)
		{
			if (proxy != null)
			{
				return proxy.IsActionFinished(action);
			}
			return action.Finished();
		}

		public static bool DoesActionYield(Action action)
		{
			if (proxy != null)
			{
				return proxy.DoesActionYield(action);
			}
			return action is IYielder;
		}

		public static void Yield(Action action)
		{
			if (action is IYielder)
			{
				Object rootContainer = action.GetRootContainer();
				(rootContainer as IYielder)?.Yield();
			}
		}
	}
}
