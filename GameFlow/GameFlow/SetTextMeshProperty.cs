using UnityEngine;

namespace GameFlow
{
	[AddComponentMenu("")]
	[Help("en", "Sets a property of the specified @UnityManual{TextMesh}.", null)]
	[Help("es", "Modifica una propiedad del componente @UnityManual{TextMesh} especificado.", null)]
	public class SetTextMeshProperty : Action, IExecutableInEditor
	{
		public enum Property
		{
			Alignment,
			Anchor,
			CharacterSize,
			Color,
			Font,
			FontSize,
			FontStyle,
			LineSpacing,
			OffsetZ,
			RichText,
			TabSize,
			Text
		}

		[SerializeField]
		private TextMesh _target;

		[SerializeField]
		private Variable _targetVar;

		[SerializeField]
		private Property _property = Property.Text;

		[SerializeField]
		private TextAlignment _alignmentValue;

		[SerializeField]
		private TextAnchor _anchorValue;

		[SerializeField]
		private float _characterSizeValue;

		[SerializeField]
		private Color _colorValue;

		[SerializeField]
		private Font _fontValue;

		[SerializeField]
		private int _fontSizeValue;

		[SerializeField]
		private FontStyle _fontStyleValue;

		[SerializeField]
		private float _lineSpacingValue;

		[SerializeField]
		private float _offsetZValue;

		[SerializeField]
		private bool _richTextValue;

		[SerializeField]
		private float _tabSizeValue;

		[SerializeField]
		private string _textValue;

		[SerializeField]
		private Variable _valueVar;

		protected override void OnReset()
		{
			_target = base.gameObject.GetComponent<TextMesh>();
		}

		public override void Setup()
		{
		}

		public override void Execute()
		{
			TextMesh textMesh = _targetVar.GetValueAsComponent(_target, typeof(TextMesh)) as TextMesh;
			if (!(textMesh == null))
			{
				switch (_property)
				{
				case Property.Alignment:
					textMesh.alignment = (TextAlignment)(object)_valueVar.GetValue(_alignmentValue);
					break;
				case Property.Anchor:
					textMesh.anchor = (TextAnchor)(object)_valueVar.GetValue(_anchorValue);
					break;
				case Property.CharacterSize:
					textMesh.characterSize = _valueVar.GetValue(_characterSizeValue);
					break;
				case Property.Color:
					textMesh.color = _valueVar.GetValue(_colorValue);
					break;
				case Property.Font:
					textMesh.font = (_valueVar.GetValue(_fontValue) as Font);
					break;
				case Property.FontSize:
					textMesh.fontSize = _valueVar.GetValue(_fontSizeValue);
					break;
				case Property.FontStyle:
					textMesh.fontStyle = (FontStyle)(object)_valueVar.GetValue(_fontStyleValue);
					break;
				case Property.LineSpacing:
					textMesh.lineSpacing = _valueVar.GetValue(_lineSpacingValue);
					break;
				case Property.OffsetZ:
					textMesh.offsetZ = _valueVar.GetValue(_offsetZValue);
					break;
				case Property.RichText:
					textMesh.richText = _valueVar.GetValue(_richTextValue);
					break;
				case Property.TabSize:
					textMesh.tabSize = _valueVar.GetValue(_tabSizeValue);
					break;
				case Property.Text:
					textMesh.text = _valueVar.GetValue(_textValue);
					break;
				}
			}
		}
	}
}
