using UnityEngine;

namespace GameFlow
{
	[AddComponentMenu("")]
	[Help("es", "Pone en reposo todos los objetos (sean 2D o 3D) de la @GameFlow{List} especificada.", null)]
	[Help("en", "Puts to sleep all objects (2D or 3D) in the specified @GameFlow{List}.", null)]
	public class SleepList : ListAction
	{
		public override void Execute()
		{
			List list = GetList();
			if (!(list != null) || list.dataType != DataType.Object)
			{
				return;
			}
			for (int i = 0; i < list.Count; i++)
			{
				Rigidbody rigidbody = list.GetObjectAt(i) as Rigidbody;
				if (rigidbody != null)
				{
					Sleep.Execute(rigidbody);
					continue;
				}
				Rigidbody2D rigidbody2D = list.GetObjectAt(i) as Rigidbody2D;
				if (rigidbody2D != null)
				{
					Sleep2D.Execute(rigidbody2D);
				}
			}
		}
	}
}
