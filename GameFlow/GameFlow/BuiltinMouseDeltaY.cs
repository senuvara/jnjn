using System;
using UnityEngine;

namespace GameFlow
{
	[Help("en", "A built-in @GameFlow{Variable} that returns the delta (relative movement) for the Y coordinate of the mouse.", null)]
	[Help("es", "Una @GameFlow{Variable} integrada que devuelve el valor delta (movimiento relativo) de la coordenada Y del raton.", null)]
	[AddComponentMenu("")]
	public class BuiltinMouseDeltaY : BuiltinVariable
	{
		public override float floatValue => (!Application.isPlaying) ? 0f : MouseDeltaController.delta.y;

		public override Type GetVariableType()
		{
			return typeof(float);
		}

		protected override string GetVariableId()
		{
			return "Mouse Delta Y";
		}

		[RuntimeInitializeOnLoadMethod]
		private static void Init()
		{
			Plugin.controlGameObject.AddComponentOnlyOnce<MouseDeltaController>();
		}
	}
}
