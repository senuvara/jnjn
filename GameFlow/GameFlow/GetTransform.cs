using UnityEngine;

namespace GameFlow
{
	[AddComponentMenu("")]
	[Help("es", "Obtiene el componente @UnityManual{Transform} del @UnityManual{GameObject} especificado.", null)]
	[Help("en", "Gets the @UnityManual{Transform} component of the specified @UnityManual{GameObject}.", null)]
	public class GetTransform : Action, IExecutableInEditor
	{
		[SerializeField]
		private GameObject _target;

		[SerializeField]
		private Variable _targetVar;

		[SerializeField]
		private Variable _output;

		private Transform _transform;

		protected override void OnReset()
		{
		}

		public override void Setup()
		{
			if (_target != null)
			{
				_transform = _target.GetComponent<Transform>();
			}
		}

		public override void Execute()
		{
			if (_output == null)
			{
				return;
			}
			if (_transform != null)
			{
				_output.SetValue(_transform);
				return;
			}
			GameObject gameObject = _targetVar.GetValue(_target) as GameObject;
			if (gameObject != null)
			{
				_output.SetValue(gameObject.GetComponent<Transform>());
			}
		}
	}
}
