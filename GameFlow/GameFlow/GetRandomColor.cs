using UnityEngine;

namespace GameFlow
{
	[Help("en", "Gets a random @UnityAPI{Color} between the specified colors.", null)]
	[Help("es", "Devuelve un @UnityAPI{Color} aleatorio entre los colores especificados.", null)]
	[AddComponentMenu("")]
	public class GetRandomColor : Action, IExecutableInEditor
	{
		[SerializeField]
		private Color _min = Color.black;

		[SerializeField]
		private Variable _minVar;

		[SerializeField]
		private Color _max = Color.white;

		[SerializeField]
		private Variable _maxVar;

		[SerializeField]
		private Variable _output;

		public override void Execute()
		{
			if (!(_output == null))
			{
				Color value = _minVar.GetValue(_min);
				Color value2 = _maxVar.GetValue(_max);
				float r = Random.Range(value.r, value2.r);
				float g = Random.Range(value.g, value2.g);
				float b = Random.Range(value.b, value2.b);
				float a = Random.Range(value.a, value2.a);
				_output.SetValue(new Color(r, g, b, a));
			}
		}
	}
}
