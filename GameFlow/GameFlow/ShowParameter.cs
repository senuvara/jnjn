using UnityEngine;

namespace GameFlow
{
	[AddComponentMenu("")]
	[Help("en", "Shows the specified @GameFlow{Parameter}.", null)]
	[Help("es", "Muestra el parámetro (@GameFlow{Parameter}) especificado.", null)]
	public class ShowParameter : ParameterAction
	{
	}
}
