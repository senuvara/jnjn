using UnityEngine;
using UnityEngine.UI;

namespace GameFlow
{
	[AddComponentMenu("")]
	public class SetSelectableProperty : Action, IExecutableInEditor
	{
		public enum Property
		{
			AnimationTriggers_DisabledTrigger,
			AnimationTriggers_HighlightedTrigger,
			AnimationTriggers_NormalTrigger,
			AnimationTriggers_PressedTrigger,
			Colors_ColorMultiplier,
			Colors_DisabledColor,
			Colors_FadeDuration,
			Colors_HighlightedColor,
			Colors_NormalColor,
			Colors_PressedColor,
			Image,
			Interactable,
			Navigation_Mode,
			Navigation_SelectOnDown,
			Navigation_SelectOnLeft,
			Navigation_SelectOnRight,
			Navigation_SelectOnUp,
			SpriteState_DisabledSprite,
			SpriteState_HighlightedSprite,
			SpriteState_PressedSprite,
			TargetGraphic,
			Transition
		}

		[SerializeField]
		private Selectable _target;

		[SerializeField]
		private Variable _targetVar;

		[SerializeField]
		private Property _property;

		[SerializeField]
		private string _disabledTriggerValue;

		[SerializeField]
		private string _highlightedTriggerValue;

		[SerializeField]
		private string _normalTriggerValue;

		[SerializeField]
		private string _pressedTriggerValue;

		[SerializeField]
		private float _colorMultiplierValue;

		[SerializeField]
		private Color _disabledColorValue;

		[SerializeField]
		private float _fadeDurationValue;

		[SerializeField]
		private Color _highlightedColorValue;

		[SerializeField]
		private Color _normalColorValue;

		[SerializeField]
		private Color _pressedColorValue;

		private ColorBlock colors;

		[SerializeField]
		private Image _imageValue;

		[SerializeField]
		private bool _interactableValue;

		[SerializeField]
		private Navigation.Mode _navigationModeValue;

		[SerializeField]
		private Selectable _selectOnDownValue;

		[SerializeField]
		private Selectable _selectOnLeftValue;

		[SerializeField]
		private Selectable _selectOnRightValue;

		[SerializeField]
		private Selectable _selectOnUpValue;

		private Navigation navigation;

		[SerializeField]
		private Sprite _disabledSpriteValue;

		[SerializeField]
		private Sprite _highlightedSpriteValue;

		[SerializeField]
		private Sprite _pressedSpriteValue;

		private SpriteState spriteState;

		[SerializeField]
		private Graphic _targetGraphicValue;

		[SerializeField]
		private Selectable.Transition _transitionValue;

		[SerializeField]
		private Variable _valueVar;

		protected override void OnReset()
		{
			_target = base.gameObject.GetComponent<Selectable>();
		}

		public override void Setup()
		{
		}

		public override void Execute()
		{
			Selectable selectable = _targetVar.GetValueAsComponent(_target, typeof(Selectable)) as Selectable;
			if (!(selectable == null))
			{
				switch (_property)
				{
				case Property.AnimationTriggers_DisabledTrigger:
					selectable.animationTriggers.disabledTrigger = _valueVar.GetValue(_disabledTriggerValue);
					break;
				case Property.AnimationTriggers_HighlightedTrigger:
					selectable.animationTriggers.highlightedTrigger = _valueVar.GetValue(_highlightedTriggerValue);
					break;
				case Property.AnimationTriggers_NormalTrigger:
					selectable.animationTriggers.normalTrigger = _valueVar.GetValue(_normalTriggerValue);
					break;
				case Property.AnimationTriggers_PressedTrigger:
					selectable.animationTriggers.pressedTrigger = _valueVar.GetValue(_pressedTriggerValue);
					break;
				case Property.Colors_ColorMultiplier:
					colors = default(ColorBlock);
					colors = selectable.colors;
					colors.colorMultiplier = _valueVar.GetValue(_colorMultiplierValue);
					selectable.colors = colors;
					break;
				case Property.Colors_DisabledColor:
					colors = default(ColorBlock);
					colors = selectable.colors;
					colors.disabledColor = _valueVar.GetValue(_disabledColorValue);
					selectable.colors = colors;
					break;
				case Property.Colors_FadeDuration:
					colors = default(ColorBlock);
					colors = selectable.colors;
					colors.fadeDuration = _valueVar.GetValue(_fadeDurationValue);
					selectable.colors = colors;
					break;
				case Property.Colors_HighlightedColor:
					colors = default(ColorBlock);
					colors = selectable.colors;
					colors.highlightedColor = _valueVar.GetValue(_highlightedColorValue);
					selectable.colors = colors;
					break;
				case Property.Colors_NormalColor:
					colors = default(ColorBlock);
					colors = selectable.colors;
					colors.normalColor = _valueVar.GetValue(_normalColorValue);
					selectable.colors = colors;
					break;
				case Property.Colors_PressedColor:
					colors = default(ColorBlock);
					colors = selectable.colors;
					colors.pressedColor = _valueVar.GetValue(_pressedColorValue);
					selectable.colors = colors;
					break;
				case Property.Image:
					selectable.image = (_valueVar.GetValueAsComponent(_imageValue, typeof(Image)) as Image);
					break;
				case Property.Interactable:
					selectable.interactable = _valueVar.GetValue(_interactableValue);
					break;
				case Property.Navigation_Mode:
					navigation = default(Navigation);
					navigation = selectable.navigation;
					navigation.mode = (Navigation.Mode)(object)_valueVar.GetValue(_navigationModeValue);
					selectable.navigation = navigation;
					break;
				case Property.Navigation_SelectOnDown:
					navigation = default(Navigation);
					navigation = selectable.navigation;
					navigation.selectOnDown = (_valueVar.GetValue(_selectOnDownValue) as Selectable);
					selectable.navigation = navigation;
					break;
				case Property.Navigation_SelectOnLeft:
					navigation = default(Navigation);
					navigation = selectable.navigation;
					navigation.selectOnLeft = (_valueVar.GetValue(_selectOnLeftValue) as Selectable);
					selectable.navigation = navigation;
					break;
				case Property.Navigation_SelectOnRight:
					navigation = default(Navigation);
					navigation = selectable.navigation;
					navigation.selectOnRight = (_valueVar.GetValue(_selectOnRightValue) as Selectable);
					selectable.navigation = navigation;
					break;
				case Property.Navigation_SelectOnUp:
					navigation = default(Navigation);
					navigation = selectable.navigation;
					navigation.selectOnUp = (_valueVar.GetValue(_selectOnUpValue) as Selectable);
					selectable.navigation = navigation;
					break;
				case Property.SpriteState_DisabledSprite:
					spriteState = default(SpriteState);
					spriteState = selectable.spriteState;
					spriteState.disabledSprite = (_valueVar.GetValue(_disabledSpriteValue) as Sprite);
					selectable.spriteState = spriteState;
					break;
				case Property.SpriteState_HighlightedSprite:
					spriteState = default(SpriteState);
					spriteState = selectable.spriteState;
					spriteState.highlightedSprite = (_valueVar.GetValue(_highlightedSpriteValue) as Sprite);
					selectable.spriteState = spriteState;
					break;
				case Property.SpriteState_PressedSprite:
					spriteState = default(SpriteState);
					spriteState = selectable.spriteState;
					spriteState.pressedSprite = (_valueVar.GetValue(_pressedSpriteValue) as Sprite);
					selectable.spriteState = spriteState;
					break;
				case Property.TargetGraphic:
					selectable.targetGraphic = (_valueVar.GetValueAsComponent(_targetGraphicValue, typeof(Graphic)) as Graphic);
					break;
				case Property.Transition:
					selectable.transition = (Selectable.Transition)(object)_valueVar.GetValue(_transitionValue);
					break;
				}
			}
		}
	}
}
