using UnityEngine;

namespace GameFlow
{
	[Help("es", "Deshabilita el componente Collider especificado.", null)]
	[Help("en", "Disables the specified Collider component.", null)]
	[AddComponentMenu("")]
	public class DisableCollider : Action, IExecutableInEditor
	{
		[SerializeField]
		private Collider _target;

		[SerializeField]
		private Variable _targetVar;

		public override void Execute()
		{
			Execute(_targetVar.GetValueAsComponent(_target, typeof(Collider)) as Collider);
		}

		public static void Execute(Collider collider)
		{
			if (collider != null)
			{
				collider.enabled = false;
			}
		}
	}
}
