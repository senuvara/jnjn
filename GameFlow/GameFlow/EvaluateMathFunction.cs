using System;
using UnityEngine;

namespace GameFlow
{
	[AddComponentMenu("")]
	[Help("es", "Evalúa la función matemática elegida.", null)]
	[Help("en", "Evaluates the selected mathematical function.", null)]
	public class EvaluateMathFunction : Action, IExecutableInEditor
	{
		public enum Function
		{
			Abs,
			Acos,
			Asin,
			Atan,
			Atan2,
			Ceil,
			Cos,
			Exp,
			Floor,
			Log,
			Max,
			Min,
			Pow,
			Round,
			Sign,
			Sin,
			Sqrt,
			Tan
		}

		public enum AngleUnit
		{
			Radians,
			Degrees
		}

		[SerializeField]
		private Function _function;

		[SerializeField]
		private float _value1;

		[SerializeField]
		private Variable _value1Var;

		[SerializeField]
		private float _value2;

		[SerializeField]
		private Variable _value2Var;

		[SerializeField]
		private AngleUnit _angleUnit;

		[SerializeField]
		private Variable _output;

		public float value1 => _value1Var.GetValue(_value1);

		public float value2 => _value2Var.GetValue(_value2);

		public override void Execute()
		{
			if (_output != null)
			{
				DataType dataType = _output.dataType;
				if (dataType == DataType.Integer)
				{
					_output.SetValue((int)Execute(_function, value1, value2, _angleUnit));
				}
				else
				{
					_output.SetValue(Execute(_function, value1, value2, _angleUnit));
				}
			}
		}

		public static float Execute(Function function, float value1, float value2 = 0f, AngleUnit angleUnit = AngleUnit.Radians)
		{
			switch (function)
			{
			case Function.Abs:
				return Mathf.Abs(value1);
			case Function.Acos:
			{
				float num = (angleUnit != AngleUnit.Degrees) ? 1f : 57.29578f;
				return (float)Math.Round(Mathf.Acos(value1) * num, 4);
			}
			case Function.Asin:
			{
				float num = (angleUnit != AngleUnit.Degrees) ? 1f : 57.29578f;
				return (float)Math.Round(Mathf.Asin(value1) * num, 4);
			}
			case Function.Atan:
			{
				float num = (angleUnit != AngleUnit.Degrees) ? 1f : 57.29578f;
				return (float)Math.Round(Mathf.Atan(value1) * num, 4);
			}
			case Function.Atan2:
			{
				float num = (angleUnit != AngleUnit.Degrees) ? 1f : 57.29578f;
				return (float)Math.Round(Mathf.Atan2(value1, value2) * num, 4);
			}
			case Function.Ceil:
				return Mathf.Ceil(value1);
			case Function.Cos:
			{
				float num = (angleUnit != AngleUnit.Degrees) ? 1f : ((float)Math.PI / 180f);
				return (float)Math.Round(Mathf.Cos(value1 * num), 6);
			}
			case Function.Exp:
				return Mathf.Exp(value1);
			case Function.Floor:
				return Mathf.Floor(value1);
			case Function.Log:
				return Mathf.Log(value1, value2);
			case Function.Max:
				return Mathf.Max(value1, value2);
			case Function.Min:
				return Mathf.Min(value1, value2);
			case Function.Pow:
				return Mathf.Pow(value1, value2);
			case Function.Round:
				return Mathf.Round(value1);
			case Function.Sign:
				return Mathf.Sign(value1);
			case Function.Sin:
			{
				float num = (angleUnit != AngleUnit.Degrees) ? 1f : ((float)Math.PI / 180f);
				return (float)Math.Round(Mathf.Sin(value1 * num), 6);
			}
			case Function.Sqrt:
				return Mathf.Sqrt(value1);
			case Function.Tan:
			{
				float num = (angleUnit != AngleUnit.Degrees) ? 1f : ((float)Math.PI / 180f);
				return (float)Math.Round(Mathf.Tan(value1 * num), 6);
			}
			default:
				return 0f;
			}
		}
	}
}
