using UnityEngine;

namespace GameFlow
{
	[Help("es", "Despierta al componente @UnityManual{Rigidbody2D} especificado.", null)]
	[AddComponentMenu("")]
	[Help("en", "Wakes up the specified @UnityManual{Rigidbody2D} component.", null)]
	public class WakeUp2D : Action
	{
		[SerializeField]
		private Rigidbody2D _target;

		[SerializeField]
		private Variable _targetVar;

		public override void Setup()
		{
		}

		public override void Execute()
		{
			Rigidbody2D rigidbody2D = _target;
			if (rigidbody2D == null)
			{
				rigidbody2D = (_targetVar.GetValueAsComponent(_target, typeof(Rigidbody2D)) as Rigidbody2D);
			}
			Execute(rigidbody2D);
		}

		public static void Execute(Rigidbody2D rb)
		{
			if (rb != null)
			{
				rb.WakeUp();
			}
		}
	}
}
