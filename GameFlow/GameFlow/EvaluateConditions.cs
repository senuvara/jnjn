using System.Collections.Generic;
using UnityEngine;

namespace GameFlow
{
	[Help("es", "Evalúa las condiciones especificadas.", null)]
	[AddComponentMenu("")]
	[Help("en", "Evaluates the specified conditions.", null)]
	public class EvaluateConditions : Action, IBlockContainer, IConditionContainer, IExecutableInEditor
	{
		[SerializeField]
		private List<Condition> _conditions = new List<Condition>();

		[SerializeField]
		private Variable _output;

		private bool _dirty;

		public List<Condition> GetConditions()
		{
			return _conditions;
		}

		public virtual void ConditionAdded(Condition condition)
		{
		}

		public virtual bool Evaluate(bool defaultResult)
		{
			List<Condition> conditions = GetConditions();
			if (conditions == null)
			{
				Log.Warning("List of conditions should not be null.", base.gameObject);
				return defaultResult;
			}
			if (conditions.Count == 0)
			{
				return defaultResult;
			}
			int num = 0;
			bool flag = conditions[num].Evaluate();
			Connective connective = conditions[num].connective;
			for (num++; num < conditions.Count; num++)
			{
				switch (connective)
				{
				case Connective.And:
					flag = (flag && conditions[num].Evaluate());
					break;
				case Connective.Or:
					flag = (flag || conditions[num].Evaluate());
					break;
				}
				connective = conditions[num].connective;
			}
			return flag;
		}

		public override void Execute()
		{
			bool value = Evaluate(defaultResult: false);
			if (_output != null)
			{
				_output.SetValue(value);
			}
		}

		public void SetDirty(bool dirty)
		{
			_dirty = dirty;
		}

		public bool IsDirty()
		{
			return _dirty;
		}

		public List<Block> GetBlocks()
		{
			return null;
		}

		public virtual bool Contains(Block block)
		{
			if (block != null && block as Condition != null)
			{
				return _conditions.Contains(block as Condition);
			}
			return false;
		}
	}
}
