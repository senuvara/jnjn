using System;
using System.Collections;
using System.Collections.Generic;
using System.Reflection;
using UnityEngine;

namespace GameFlow
{
	public static class ReflectionUtils
	{
		public class MethodComparer : IComparer
		{
			int IComparer.Compare(object x, object y)
			{
				MethodInfo methodInfo = x as MethodInfo;
				string a = (methodInfo == null) ? string.Empty : methodInfo.Name;
				MethodInfo methodInfo2 = y as MethodInfo;
				string b = (methodInfo2 == null) ? string.Empty : methodInfo2.Name;
				return new CaseInsensitiveComparer().Compare(a, b);
			}
		}

		public class PropertyComparer : IComparer
		{
			int IComparer.Compare(object x, object y)
			{
				PropertyInfo propertyInfo = x as PropertyInfo;
				string a = (propertyInfo == null) ? string.Empty : propertyInfo.Name;
				PropertyInfo propertyInfo2 = y as PropertyInfo;
				string b = (propertyInfo2 == null) ? string.Empty : propertyInfo2.Name;
				return new CaseInsensitiveComparer().Compare(a, b);
			}
		}

		public class FieldComparer : IComparer
		{
			int IComparer.Compare(object x, object y)
			{
				FieldInfo fieldInfo = x as FieldInfo;
				string a = (fieldInfo == null) ? string.Empty : fieldInfo.Name;
				FieldInfo fieldInfo2 = y as FieldInfo;
				string b = (fieldInfo2 == null) ? string.Empty : fieldInfo2.Name;
				return new CaseInsensitiveComparer().Compare(a, b);
			}
		}

		private static Type[] _supportedTypes = new Type[7]
		{
			typeof(Vector2),
			typeof(Vector3),
			typeof(Vector4),
			typeof(Rect),
			typeof(Bounds),
			typeof(Color),
			typeof(Quaternion)
		};

		public static object Invoke(Type type, string methodName, object[] parameters)
		{
			Type[] array = new Type[(parameters != null) ? parameters.Length : 0];
			for (int i = 0; i < array.Length; i++)
			{
				array[i] = parameters[i].GetType();
			}
			BindingFlags bindingAttr = BindingFlags.Static | BindingFlags.Public | BindingFlags.NonPublic;
			MethodInfo method = type.GetMethod(methodName, bindingAttr, null, array, null);
			if (method == null)
			{
				return null;
			}
			if (method.IsStatic)
			{
				return method.Invoke(null, parameters);
			}
			bindingAttr = (BindingFlags.DeclaredOnly | BindingFlags.Instance | BindingFlags.Public | BindingFlags.NonPublic | BindingFlags.CreateInstance);
			object obj = type.InvokeMember(null, bindingAttr, null, null, new object[0]);
			return method.Invoke(obj, parameters);
		}

		public static object Invoke(object obj, string methodName, object[] parameters)
		{
			Type[] array = new Type[(parameters != null) ? parameters.Length : 0];
			for (int i = 0; i < array.Length; i++)
			{
				array[i] = parameters[i].GetType();
			}
			Type type = obj.GetType();
			BindingFlags bindingAttr = BindingFlags.Instance | BindingFlags.Public | BindingFlags.NonPublic;
			return type.GetMethod(methodName, bindingAttr, null, array, null)?.Invoke(obj, parameters);
		}

		public static void SetPropertyValue(Type type, object obj, string propertyName, object value)
		{
			type?.GetProperty(propertyName)?.SetValue(obj, value, null);
		}

		public static object GetPropertyValue(Type type, object obj, string propertyName)
		{
			return type?.GetProperty(propertyName)?.GetValue(obj, null);
		}

		public static object GetFieldValue(Type type, object obj, string propertyName)
		{
			return type?.GetField(propertyName, BindingFlags.Instance | BindingFlags.Static | BindingFlags.Public | BindingFlags.NonPublic)?.GetValue(obj);
		}

		public static MethodInfo[] GetMethods(Type type, bool includeStatic = true, bool sorted = true)
		{
			if (type == null)
			{
				return new MethodInfo[0];
			}
			BindingFlags bindingFlags = BindingFlags.Instance | BindingFlags.Public | BindingFlags.InvokeMethod;
			if (includeStatic)
			{
				bindingFlags |= BindingFlags.Static;
			}
			MethodInfo[] methods = type.GetMethods(bindingFlags);
			if (sorted)
			{
				Array.Sort(methods, new MethodComparer());
			}
			List<MethodInfo> list = new List<MethodInfo>();
			bool flag = type.IsSubclassOf(typeof(MonoBehaviour));
			if (flag && type.Namespace == "GameFlow")
			{
				return new MethodInfo[0];
			}
			Type c = (!flag) ? typeof(UnityEngine.Object) : typeof(MonoBehaviour);
			for (int i = 0; i < methods.Length; i++)
			{
				if (methods[i].DeclaringType.IsSubclassOf(c) && IsSupported(methods[i]))
				{
					list.Add(methods[i]);
				}
			}
			return list.ToArray();
		}

		private static bool IsSupported(MethodInfo method)
		{
			if (method.IsSpecialName)
			{
				return false;
			}
			if (method.ReturnType != typeof(void) && !IsParameterTypeSupported(method.ReturnType))
			{
				return false;
			}
			ParameterInfo[] parameters = method.GetParameters();
			if (parameters.Length > 0)
			{
				for (int i = 0; i < parameters.Length; i++)
				{
					Type parameterType = parameters[i].ParameterType;
					if (!IsParameterTypeSupported(parameterType) || parameters[i].IsOut)
					{
						return false;
					}
				}
			}
			return true;
		}

		private static bool IsParameterTypeSupported(Type type)
		{
			if ((type.IsValueType && !type.IsPrimitive && !type.IsEnum && !type.IsAnyOf(_supportedTypes)) || type.IsArray || type == typeof(object) || type == typeof(Type) || type == typeof(UnityEngine.Object) || typeof(ICollection).IsAssignableFrom(type))
			{
				return false;
			}
			return true;
		}

		public static MethodInfo[] GetMethods(Component component, bool includeStatic = true)
		{
			if (component == null)
			{
				return new MethodInfo[0];
			}
			return GetMethods(component.GetType(), includeStatic);
		}

		public static bool IsObsolete(this PropertyInfo property, bool logMessage = false)
		{
			object[] customAttributes = property.GetCustomAttributes(inherit: true);
			for (int i = 0; i < customAttributes.Length; i++)
			{
				if (customAttributes[i].GetType() == typeof(ObsoleteAttribute))
				{
					if (logMessage)
					{
						Debug.Log(property.Name);
						Debug.LogWarning(">>> " + (customAttributes[i] as ObsoleteAttribute).Message);
					}
					return true;
				}
			}
			return false;
		}

		public static FieldInfo[] GetFields(MonoBehaviour script, bool includeStatic = true)
		{
			if (script == null)
			{
				return new FieldInfo[0];
			}
			return GetFields(script.GetType(), includeStatic);
		}

		public static FieldInfo[] GetFields(Type type, bool includeStatic = true, bool sorted = true)
		{
			if (type == null)
			{
				return new FieldInfo[0];
			}
			if (type.IsSubclassOf(typeof(MonoBehaviour)) && type.Namespace == "GameFlow")
			{
				return new FieldInfo[0];
			}
			BindingFlags bindingFlags = BindingFlags.Instance | BindingFlags.Public;
			if (includeStatic)
			{
				bindingFlags |= BindingFlags.Static;
			}
			FieldInfo[] fields = type.GetFields(bindingFlags);
			if (sorted)
			{
				Array.Sort(fields, new FieldComparer());
			}
			List<FieldInfo> list = new List<FieldInfo>();
			for (int i = 0; i < fields.Length; i++)
			{
				if (fields[i].DeclaringType.IsSubclassOf(typeof(MonoBehaviour)))
				{
					list.Add(fields[i]);
				}
			}
			return list.ToArray();
		}
	}
}
