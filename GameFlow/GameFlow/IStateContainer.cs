using System.Collections.Generic;

namespace GameFlow
{
	public interface IStateContainer : IBlockContainer
	{
		List<State> GetStates();

		void StateAdded(State state);
	}
}
