using UnityEngine;

namespace GameFlow
{
	[Help("en", "Removes all items in the specified @GameFlow{List}.", null)]
	[AddComponentMenu("")]
	[Help("es", "Elimina todos los elementos de la lista (@GameFlow{List}) especificada.", null)]
	public class ClearList : ListAction
	{
		[SerializeField]
		private bool _onlyNulls;

		public override void Execute()
		{
			Execute(GetList(), _onlyNulls);
		}

		public static void Execute(List list, bool onlyNulls)
		{
			if (!(list == null))
			{
				if (onlyNulls && list.dataType == DataType.Object)
				{
					list.ClearNulls();
				}
				else
				{
					list.Clear();
				}
			}
		}
	}
}
