using UnityEngine;

namespace GameFlow
{
	[Help("es", "Lee una propiedad del objeto RaycastHit especificado.", null)]
	[Help("en", "Gets a property of the specified RaycastHit object.", null)]
	[AddComponentMenu("")]
	public class GetRaycastHitProperty : Action, IExecutableInEditor
	{
		public enum Property
		{
			Barycenter,
			Collider,
			Distance,
			GameObject,
			ImpactPoint,
			LightmapCoord,
			Normal,
			Rigidbody,
			TextureCoord,
			TextureCoord2,
			Transform,
			TriangleIndex,
			LocalImpactPoint
		}

		[SerializeField]
		private RaycastHit _source;

		[SerializeField]
		private Variable _sourceVar;

		[SerializeField]
		private Property _property;

		[SerializeField]
		private Variable _output;

		public override void Execute()
		{
			if (_output == null)
			{
				return;
			}
			RaycastHit raycastHit = _sourceVar.GetValue(_source) as RaycastHit;
			if (!(raycastHit == null))
			{
				switch (_property)
				{
				case Property.Barycenter:
					_output.SetValue(raycastHit.barycenter);
					break;
				case Property.Collider:
					_output.SetValue(raycastHit.collider);
					break;
				case Property.Distance:
					_output.SetValue(raycastHit.distance);
					break;
				case Property.GameObject:
					_output.SetValue(raycastHit.gameObject);
					break;
				case Property.ImpactPoint:
					_output.SetValue(raycastHit.impactPoint);
					break;
				case Property.LightmapCoord:
					_output.SetValue(raycastHit.lightmapCoord);
					break;
				case Property.Normal:
					_output.SetValue(raycastHit.normal);
					break;
				case Property.Rigidbody:
					_output.SetValue(raycastHit.rigidbody);
					break;
				case Property.TextureCoord:
					_output.SetValue(raycastHit.textureCoord);
					break;
				case Property.TextureCoord2:
					_output.SetValue(raycastHit.textureCoord2);
					break;
				case Property.Transform:
					_output.SetValue(raycastHit.transform);
					break;
				case Property.TriangleIndex:
					_output.SetValue(raycastHit.triangleIndex);
					break;
				case Property.LocalImpactPoint:
				{
					Transform transform = raycastHit.transform;
					_output.SetValue(transform.InverseTransformPoint(raycastHit.impactPoint));
					break;
				}
				}
			}
		}
	}
}
