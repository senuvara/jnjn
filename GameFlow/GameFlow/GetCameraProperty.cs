using UnityEngine;

namespace GameFlow
{
	[AddComponentMenu("")]
	[Help("en", "Gets a property of the specified @UnityManual{Camera} component.", null)]
	[Help("es", "Lee una propiedad del componente @UnityManual{Camera} especificado.", null)]
	public class GetCameraProperty : Action, IExecutableInEditor
	{
		public enum Property
		{
			ActualRenderingPath,
			Aspect,
			BackgroundColor,
			ClearFlags,
			CullingMask,
			Depth,
			DepthTextureMode,
			Enabled,
			EventMask,
			FarClipPlane,
			FieldOfView,
			HDR,
			LayerCullDistances,
			LayerCullSpherical,
			NearClipPlane,
			Ortographic,
			OrtographicSize,
			PixelHeight,
			PixelRect,
			PixelWidth,
			Rect,
			RenderingPath,
			StereoConvergence,
			StereoEnabled,
			StereoSeparation,
			TargetTexture,
			TransparencySortMode,
			UseOcclusionCulling,
			Velocity
		}

		[SerializeField]
		private Camera _target;

		[SerializeField]
		private Variable _targetVar;

		[SerializeField]
		private Property _property;

		[SerializeField]
		private Variable _output;

		protected override void OnReset()
		{
			_target = Camera.main;
		}

		public override void Setup()
		{
		}

		public override void Execute()
		{
			Camera camera = _target;
			if (camera == null)
			{
				camera = (_targetVar.GetValueAsComponent(_target, typeof(Camera)) as Camera);
			}
			if (!(camera == null))
			{
				switch (_property)
				{
				case Property.LayerCullDistances:
					break;
				case Property.ActualRenderingPath:
					_output.SetValue(camera.actualRenderingPath);
					break;
				case Property.Aspect:
					_output.SetValue(camera.aspect);
					break;
				case Property.BackgroundColor:
					_output.SetValue(camera.backgroundColor);
					break;
				case Property.ClearFlags:
					_output.SetValue(camera.clearFlags);
					break;
				case Property.CullingMask:
					_output.SetValue(camera.cullingMask);
					break;
				case Property.Depth:
					_output.SetValue(camera.depth);
					break;
				case Property.DepthTextureMode:
					_output.SetValue(camera.depthTextureMode);
					break;
				case Property.Enabled:
					_output.SetValue(camera.enabled);
					break;
				case Property.EventMask:
					_output.SetValue(camera.eventMask);
					break;
				case Property.FarClipPlane:
					_output.SetValue(camera.farClipPlane);
					break;
				case Property.FieldOfView:
					_output.SetValue(camera.fieldOfView);
					break;
				case Property.HDR:
					_output.SetValue(camera.hdr);
					break;
				case Property.LayerCullSpherical:
					_output.SetValue(camera.layerCullSpherical);
					break;
				case Property.NearClipPlane:
					_output.SetValue(camera.nearClipPlane);
					break;
				case Property.Ortographic:
					_output.SetValue(camera.orthographic);
					break;
				case Property.OrtographicSize:
					_output.SetValue(camera.orthographicSize);
					break;
				case Property.PixelHeight:
					_output.SetValue((float)ReflectionUtils.GetPropertyValue(typeof(Camera), camera, "pixelHeight"));
					break;
				case Property.PixelRect:
					_output.SetValue(camera.pixelRect);
					break;
				case Property.PixelWidth:
					_output.SetValue((float)ReflectionUtils.GetPropertyValue(typeof(Camera), camera, "pixelWidth"));
					break;
				case Property.Rect:
					_output.SetValue(camera.rect);
					break;
				case Property.RenderingPath:
					_output.SetValue(camera.renderingPath);
					break;
				case Property.StereoConvergence:
					_output.SetValue(camera.stereoConvergence);
					break;
				case Property.StereoEnabled:
					_output.SetValue(camera.stereoEnabled);
					break;
				case Property.StereoSeparation:
					_output.SetValue(camera.stereoSeparation);
					break;
				case Property.TargetTexture:
					_output.SetValue(camera.targetTexture);
					break;
				case Property.TransparencySortMode:
					_output.SetValue(camera.transparencySortMode);
					break;
				case Property.UseOcclusionCulling:
					_output.SetValue(camera.useOcclusionCulling);
					break;
				case Property.Velocity:
					_output.SetValue(camera.velocity);
					break;
				}
			}
		}
	}
}
