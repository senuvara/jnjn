using UnityEngine;

namespace GameFlow
{
	[Help("en", "Gets a property of the specified @UnityManual{Light} component.", null)]
	[AddComponentMenu("")]
	[Help("es", "Lee una propiedad del componente @UnityManual{Light} especificado.", null)]
	public class GetLightProperty : Action, IExecutableInEditor
	{
		public enum Property
		{
			IsBaked,
			AreaSize,
			BounceIntensity,
			Color,
			CommandBufferCount,
			Cookie,
			CookieSize,
			CullingMask,
			Flare,
			Intensity,
			Range,
			RenderMode,
			ShadowBias,
			ShadowNormalBias,
			Shadows,
			ShadowStrength,
			SpotAngle,
			Type
		}

		[SerializeField]
		private Light _source;

		[SerializeField]
		private Variable _sourceVar;

		[SerializeField]
		private Property _property = Property.Range;

		[SerializeField]
		private Variable _output;

		protected override void OnReset()
		{
			_source = base.gameObject.GetComponent<Light>();
		}

		public override void Setup()
		{
		}

		public override void Execute()
		{
			Light light = _sourceVar.GetValueAsComponent(_source, typeof(Light)) as Light;
			if (!(light == null))
			{
				switch (_property)
				{
				case Property.AreaSize:
					break;
				case Property.IsBaked:
					_output.SetValue(light.get_isBaked());
					break;
				case Property.BounceIntensity:
					_output.SetValue(light.bounceIntensity);
					break;
				case Property.Color:
					_output.SetValue(light.color);
					break;
				case Property.CommandBufferCount:
					_output.SetValue(light.commandBufferCount);
					break;
				case Property.Cookie:
					_output.SetValue(light.cookie);
					break;
				case Property.CookieSize:
					_output.SetValue(light.cookieSize);
					break;
				case Property.CullingMask:
					_output.SetValue(light.cullingMask);
					break;
				case Property.Flare:
					_output.SetValue(light.flare);
					break;
				case Property.Intensity:
					_output.SetValue(light.intensity);
					break;
				case Property.Range:
					_output.SetValue(light.range);
					break;
				case Property.RenderMode:
					_output.SetValue(light.renderMode);
					break;
				case Property.ShadowBias:
					_output.SetValue(light.shadowBias);
					break;
				case Property.ShadowNormalBias:
					_output.SetValue(light.shadowNormalBias);
					break;
				case Property.Shadows:
					_output.SetValue(light.shadows);
					break;
				case Property.ShadowStrength:
					_output.SetValue(light.shadowStrength);
					break;
				case Property.SpotAngle:
					_output.SetValue(light.spotAngle);
					break;
				case Property.Type:
					_output.SetValue(light.type);
					break;
				}
			}
		}
	}
}
