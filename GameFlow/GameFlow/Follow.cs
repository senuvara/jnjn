using System;
using UnityEngine;

namespace GameFlow
{
	[Help("es", "Hace que un objeto especificado siga a otro.", null)]
	[AddComponentMenu("")]
	[Help("en", "Makes a specified object follow another.", null)]
	public class Follow : Action
	{
		[SerializeField]
		private Transform _follower;

		[SerializeField]
		private Variable _followerVar;

		[SerializeField]
		private Transform _target;

		[SerializeField]
		private Variable _targetVar;

		private Vector3 offset;

		private Transform lastFollower;

		private Transform lastTarget;

		private Vector3 followerLastPosition;

		protected override void OnReset()
		{
			_follower = base.gameObject.transform;
		}

		public override void Execute()
		{
			Transform transform = _followerVar.GetValueAsComponent(_follower, typeof(Transform)) as Transform;
			if (transform == null)
			{
				lastFollower = transform;
				return;
			}
			Transform transform2 = _targetVar.GetValueAsComponent(_target, typeof(Transform)) as Transform;
			if (transform2 == null)
			{
				lastTarget = transform2;
				return;
			}
			if (transform != lastFollower || transform2 != lastTarget || transform.position != followerLastPosition)
			{
				offset = transform2.position - transform.position;
			}
			Execute(transform, transform2, offset);
			lastFollower = transform;
			lastTarget = transform2;
			followerLastPosition = lastFollower.position;
		}

		public static void Execute(Transform follower, Transform target, Vector3 offset)
		{
			if (!(follower == null) && !(target == null))
			{
				follower.position = target.position - offset;
			}
		}

		public override Type GetPreferredContainerType()
		{
			if (base.gameObject.GetComponent<Camera>() != null)
			{
				return typeof(OnLateUpdate);
			}
			return typeof(OnStart);
		}
	}
}
