using System;
using UnityEngine;

namespace GameFlow
{
	[Help("es", "Una @GameFlow{Variable} integrada que devuelve la primera Cámara activada cuyo tag es 'MainCamera'.", null)]
	[Help("en", "A built-in @GameFlow{Variable} that returns the first enabled Camera tagged as 'MainCamera'.", null)]
	[AddComponentMenu("")]
	public class BuiltinMainCamera : BuiltinVariable
	{
		public override UnityEngine.Object objectValue => Camera.main;

		public override Type GetVariableType()
		{
			return typeof(Camera);
		}

		protected override string GetVariableId()
		{
			return "Main Camera";
		}
	}
}
