using UnityEngine;

namespace GameFlow
{
	[AddComponentMenu("")]
	[Help("en", "Gets a child of the specified @UnityManual{GameObject}.", null)]
	[Help("es", "Obtiene un hijo del @UnityManual{GameObject} especificado.", null)]
	public class GetChild : Action, IExecutableInEditor
	{
		[SerializeField]
		private GameObject _target;

		[SerializeField]
		private Variable _targetVar;

		[SerializeField]
		private int _childIndex;

		[SerializeField]
		private Variable _childIndexVar;

		[SerializeField]
		private Variable _output;

		protected override void OnReset()
		{
			_target = base.gameObject;
		}

		public override void Execute()
		{
			if (_output == null)
			{
				return;
			}
			GameObject gameObject = _targetVar.GetValue(_target) as GameObject;
			if (!(gameObject == null))
			{
				int childCount = gameObject.transform.childCount;
				if (childCount != 0)
				{
					int value = _childIndexVar.GetValue(_childIndex);
					value = Mathf.Clamp(value, 0, childCount);
					_output.SetValue(gameObject.transform.GetChild(value).gameObject);
				}
			}
		}
	}
}
