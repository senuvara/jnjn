using UnityEngine;

public class BaseObject
{
	public static void Log(string format, object context, params object[] parameters)
	{
		Debug.Log(string.Format(format, parameters), context as Object);
	}

	public static void Log(string format, params object[] parameters)
	{
		Debug.Log(string.Format(format, parameters));
	}

	public static void Log(string s)
	{
		Debug.Log(s);
	}
}
