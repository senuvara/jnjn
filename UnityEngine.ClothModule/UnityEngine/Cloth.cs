using System;
using System.Collections.Generic;
using System.Runtime.CompilerServices;
using UnityEngine.Internal;
using UnityEngine.Scripting;

namespace UnityEngine
{
	/// <summary>
	///   <para>The Cloth class provides an interface to cloth simulation physics.</para>
	/// </summary>
	[NativeClass("Unity::Cloth")]
	[RequireComponent(typeof(Transform), typeof(SkinnedMeshRenderer))]
	public sealed class Cloth : Component
	{
		/// <summary>
		///   <para>Cloth's sleep threshold.</para>
		/// </summary>
		public float sleepThreshold
		{
			[MethodImpl(MethodImplOptions.InternalCall)]
			[GeneratedByOldBindingsGenerator]
			get;
			[MethodImpl(MethodImplOptions.InternalCall)]
			[GeneratedByOldBindingsGenerator]
			set;
		}

		/// <summary>
		///   <para>Bending stiffness of the cloth.</para>
		/// </summary>
		public float bendingStiffness
		{
			[MethodImpl(MethodImplOptions.InternalCall)]
			[GeneratedByOldBindingsGenerator]
			get;
			[MethodImpl(MethodImplOptions.InternalCall)]
			[GeneratedByOldBindingsGenerator]
			set;
		}

		/// <summary>
		///   <para>Stretching stiffness of the cloth.</para>
		/// </summary>
		public float stretchingStiffness
		{
			[MethodImpl(MethodImplOptions.InternalCall)]
			[GeneratedByOldBindingsGenerator]
			get;
			[MethodImpl(MethodImplOptions.InternalCall)]
			[GeneratedByOldBindingsGenerator]
			set;
		}

		/// <summary>
		///   <para>Damp cloth motion.</para>
		/// </summary>
		public float damping
		{
			[MethodImpl(MethodImplOptions.InternalCall)]
			[GeneratedByOldBindingsGenerator]
			get;
			[MethodImpl(MethodImplOptions.InternalCall)]
			[GeneratedByOldBindingsGenerator]
			set;
		}

		/// <summary>
		///   <para>A constant, external acceleration applied to the cloth.</para>
		/// </summary>
		public Vector3 externalAcceleration
		{
			get
			{
				INTERNAL_get_externalAcceleration(out Vector3 value);
				return value;
			}
			set
			{
				INTERNAL_set_externalAcceleration(ref value);
			}
		}

		/// <summary>
		///   <para>A random, external acceleration applied to the cloth.</para>
		/// </summary>
		public Vector3 randomAcceleration
		{
			get
			{
				INTERNAL_get_randomAcceleration(out Vector3 value);
				return value;
			}
			set
			{
				INTERNAL_set_randomAcceleration(ref value);
			}
		}

		/// <summary>
		///   <para>Should gravity affect the cloth simulation?</para>
		/// </summary>
		public bool useGravity
		{
			[MethodImpl(MethodImplOptions.InternalCall)]
			[GeneratedByOldBindingsGenerator]
			get;
			[MethodImpl(MethodImplOptions.InternalCall)]
			[GeneratedByOldBindingsGenerator]
			set;
		}

		[Obsolete("Deprecated. Cloth.selfCollisions is no longer supported since Unity 5.0.", true)]
		public bool selfCollision
		{
			[MethodImpl(MethodImplOptions.InternalCall)]
			[GeneratedByOldBindingsGenerator]
			get;
			[MethodImpl(MethodImplOptions.InternalCall)]
			[GeneratedByOldBindingsGenerator]
			set;
		}

		/// <summary>
		///   <para>Is this cloth enabled?</para>
		/// </summary>
		public bool enabled
		{
			[MethodImpl(MethodImplOptions.InternalCall)]
			[GeneratedByOldBindingsGenerator]
			get;
			[MethodImpl(MethodImplOptions.InternalCall)]
			[GeneratedByOldBindingsGenerator]
			set;
		}

		/// <summary>
		///   <para>The current vertex positions of the cloth object.</para>
		/// </summary>
		public Vector3[] vertices
		{
			[MethodImpl(MethodImplOptions.InternalCall)]
			[GeneratedByOldBindingsGenerator]
			get;
		}

		/// <summary>
		///   <para>The current normals of the cloth object.</para>
		/// </summary>
		public Vector3[] normals
		{
			[MethodImpl(MethodImplOptions.InternalCall)]
			[GeneratedByOldBindingsGenerator]
			get;
		}

		/// <summary>
		///   <para>The friction of the cloth when colliding with the character.</para>
		/// </summary>
		public float friction
		{
			[MethodImpl(MethodImplOptions.InternalCall)]
			[GeneratedByOldBindingsGenerator]
			get;
			[MethodImpl(MethodImplOptions.InternalCall)]
			[GeneratedByOldBindingsGenerator]
			set;
		}

		/// <summary>
		///   <para>How much to increase mass of colliding particles.</para>
		/// </summary>
		public float collisionMassScale
		{
			[MethodImpl(MethodImplOptions.InternalCall)]
			[GeneratedByOldBindingsGenerator]
			get;
			[MethodImpl(MethodImplOptions.InternalCall)]
			[GeneratedByOldBindingsGenerator]
			set;
		}

		[Obsolete("useContinuousCollision is no longer supported, use enableContinuousCollision instead")]
		public float useContinuousCollision
		{
			[MethodImpl(MethodImplOptions.InternalCall)]
			[GeneratedByOldBindingsGenerator]
			get;
			[MethodImpl(MethodImplOptions.InternalCall)]
			[GeneratedByOldBindingsGenerator]
			set;
		}

		/// <summary>
		///   <para>Enable continuous collision to improve collision stability.</para>
		/// </summary>
		public bool enableContinuousCollision
		{
			[MethodImpl(MethodImplOptions.InternalCall)]
			[GeneratedByOldBindingsGenerator]
			get;
			[MethodImpl(MethodImplOptions.InternalCall)]
			[GeneratedByOldBindingsGenerator]
			set;
		}

		/// <summary>
		///   <para>Add one virtual particle per triangle to improve collision stability.</para>
		/// </summary>
		public float useVirtualParticles
		{
			[MethodImpl(MethodImplOptions.InternalCall)]
			[GeneratedByOldBindingsGenerator]
			get;
			[MethodImpl(MethodImplOptions.InternalCall)]
			[GeneratedByOldBindingsGenerator]
			set;
		}

		/// <summary>
		///   <para>The cloth skinning coefficients used to set up how the cloth interacts with the skinned mesh.</para>
		/// </summary>
		public ClothSkinningCoefficient[] coefficients
		{
			[MethodImpl(MethodImplOptions.InternalCall)]
			[GeneratedByOldBindingsGenerator]
			get;
			[MethodImpl(MethodImplOptions.InternalCall)]
			[GeneratedByOldBindingsGenerator]
			set;
		}

		/// <summary>
		///   <para>How much world-space movement of the character will affect cloth vertices.</para>
		/// </summary>
		public float worldVelocityScale
		{
			[MethodImpl(MethodImplOptions.InternalCall)]
			[GeneratedByOldBindingsGenerator]
			get;
			[MethodImpl(MethodImplOptions.InternalCall)]
			[GeneratedByOldBindingsGenerator]
			set;
		}

		/// <summary>
		///   <para>How much world-space acceleration of the character will affect cloth vertices.</para>
		/// </summary>
		public float worldAccelerationScale
		{
			[MethodImpl(MethodImplOptions.InternalCall)]
			[GeneratedByOldBindingsGenerator]
			get;
			[MethodImpl(MethodImplOptions.InternalCall)]
			[GeneratedByOldBindingsGenerator]
			set;
		}

		[Obsolete("Parameter solverFrequency is obsolete and no longer supported. Please use clothSolverFrequency instead.")]
		public bool solverFrequency
		{
			get
			{
				return (clothSolverFrequency > 0f) ? true : false;
			}
			set
			{
				clothSolverFrequency = ((!value) ? 0f : 120f);
			}
		}

		/// <summary>
		///   <para>Number of cloth solver iterations per second.</para>
		/// </summary>
		public float clothSolverFrequency
		{
			[MethodImpl(MethodImplOptions.InternalCall)]
			[GeneratedByOldBindingsGenerator]
			get;
			[MethodImpl(MethodImplOptions.InternalCall)]
			[GeneratedByOldBindingsGenerator]
			set;
		}

		/// <summary>
		///   <para>An array of CapsuleColliders which this Cloth instance should collide with.</para>
		/// </summary>
		public CapsuleCollider[] capsuleColliders
		{
			[MethodImpl(MethodImplOptions.InternalCall)]
			[GeneratedByOldBindingsGenerator]
			get;
			[MethodImpl(MethodImplOptions.InternalCall)]
			[GeneratedByOldBindingsGenerator]
			set;
		}

		/// <summary>
		///   <para>An array of ClothSphereColliderPairs which this Cloth instance should collide with.</para>
		/// </summary>
		public ClothSphereColliderPair[] sphereColliders
		{
			[MethodImpl(MethodImplOptions.InternalCall)]
			[GeneratedByOldBindingsGenerator]
			get;
			[MethodImpl(MethodImplOptions.InternalCall)]
			[GeneratedByOldBindingsGenerator]
			set;
		}

		/// <summary>
		///   <para>Use Tether Anchors.</para>
		/// </summary>
		public bool useTethers
		{
			[MethodImpl(MethodImplOptions.InternalCall)]
			[GeneratedByOldBindingsGenerator]
			get;
			[MethodImpl(MethodImplOptions.InternalCall)]
			[GeneratedByOldBindingsGenerator]
			set;
		}

		/// <summary>
		///   <para>Sets the stiffness frequency parameter.</para>
		/// </summary>
		public float stiffnessFrequency
		{
			[MethodImpl(MethodImplOptions.InternalCall)]
			[GeneratedByOldBindingsGenerator]
			get;
			[MethodImpl(MethodImplOptions.InternalCall)]
			[GeneratedByOldBindingsGenerator]
			set;
		}

		/// <summary>
		///   <para>Minimum distance at which two cloth particles repel each other (default: 0.0).</para>
		/// </summary>
		public float selfCollisionDistance
		{
			[MethodImpl(MethodImplOptions.InternalCall)]
			[GeneratedByOldBindingsGenerator]
			get;
			[MethodImpl(MethodImplOptions.InternalCall)]
			[GeneratedByOldBindingsGenerator]
			set;
		}

		/// <summary>
		///   <para>Self-collision stiffness defines how strong the separating impulse should be for colliding particles.</para>
		/// </summary>
		public float selfCollisionStiffness
		{
			[MethodImpl(MethodImplOptions.InternalCall)]
			[GeneratedByOldBindingsGenerator]
			get;
			[MethodImpl(MethodImplOptions.InternalCall)]
			[GeneratedByOldBindingsGenerator]
			set;
		}

		[MethodImpl(MethodImplOptions.InternalCall)]
		[GeneratedByOldBindingsGenerator]
		private extern void INTERNAL_get_externalAcceleration(out Vector3 value);

		[MethodImpl(MethodImplOptions.InternalCall)]
		[GeneratedByOldBindingsGenerator]
		private extern void INTERNAL_set_externalAcceleration(ref Vector3 value);

		[MethodImpl(MethodImplOptions.InternalCall)]
		[GeneratedByOldBindingsGenerator]
		private extern void INTERNAL_get_randomAcceleration(out Vector3 value);

		[MethodImpl(MethodImplOptions.InternalCall)]
		[GeneratedByOldBindingsGenerator]
		private extern void INTERNAL_set_randomAcceleration(ref Vector3 value);

		/// <summary>
		///   <para>Clear the pending transform changes from affecting the cloth simulation.</para>
		/// </summary>
		public void ClearTransformMotion()
		{
			INTERNAL_CALL_ClearTransformMotion(this);
		}

		[MethodImpl(MethodImplOptions.InternalCall)]
		[GeneratedByOldBindingsGenerator]
		private static extern void INTERNAL_CALL_ClearTransformMotion(Cloth self);

		/// <summary>
		///   <para>Fade the cloth simulation in or out.</para>
		/// </summary>
		/// <param name="enabled">Fading enabled or not.</param>
		/// <param name="interpolationTime"></param>
		[MethodImpl(MethodImplOptions.InternalCall)]
		[GeneratedByOldBindingsGenerator]
		public extern void SetEnabledFading(bool enabled, [DefaultValue("0.5f")] float interpolationTime);

		[ExcludeFromDocs]
		public void SetEnabledFading(bool enabled)
		{
			float interpolationTime = 0.5f;
			SetEnabledFading(enabled, interpolationTime);
		}

		[MethodImpl(MethodImplOptions.InternalCall)]
		[GeneratedByOldBindingsGenerator]
		internal extern void GetVirtualParticleIndicesMono(object indicesOutList);

		[MethodImpl(MethodImplOptions.InternalCall)]
		[GeneratedByOldBindingsGenerator]
		internal extern void SetVirtualParticleIndicesMono(object indicesInList);

		[MethodImpl(MethodImplOptions.InternalCall)]
		[GeneratedByOldBindingsGenerator]
		internal extern void GetVirtualParticleWeightsMono(object weightsOutList);

		[MethodImpl(MethodImplOptions.InternalCall)]
		[GeneratedByOldBindingsGenerator]
		internal extern void SetVirtualParticleWeightsMono(object weightsInList);

		[MethodImpl(MethodImplOptions.InternalCall)]
		[GeneratedByOldBindingsGenerator]
		internal extern void GetSelfAndInterCollisionIndicesMono(object indicesOutList);

		[MethodImpl(MethodImplOptions.InternalCall)]
		[GeneratedByOldBindingsGenerator]
		internal extern void SetSelfAndInterCollisionIndicesMono(object indicesInList);

		public void GetVirtualParticleIndices(List<uint> indices)
		{
			if (indices == null)
			{
				throw new ArgumentNullException("indices");
			}
			GetVirtualParticleIndicesMono(indices);
		}

		public void SetVirtualParticleIndices(List<uint> indices)
		{
			if (indices == null)
			{
				throw new ArgumentNullException("indices");
			}
			SetVirtualParticleIndicesMono(indices);
		}

		public void GetVirtualParticleWeights(List<Vector3> weights)
		{
			if (weights == null)
			{
				throw new ArgumentNullException("weights");
			}
			GetVirtualParticleWeightsMono(weights);
		}

		public void SetVirtualParticleWeights(List<Vector3> weights)
		{
			if (weights == null)
			{
				throw new ArgumentNullException("weights");
			}
			SetVirtualParticleWeightsMono(weights);
		}

		public void GetSelfAndInterCollisionIndices(List<uint> indices)
		{
			if (indices == null)
			{
				throw new ArgumentNullException("indices");
			}
			GetSelfAndInterCollisionIndicesMono(indices);
		}

		public void SetSelfAndInterCollisionIndices(List<uint> indices)
		{
			if (indices == null)
			{
				throw new ArgumentNullException("indices");
			}
			SetSelfAndInterCollisionIndicesMono(indices);
		}
	}
}
