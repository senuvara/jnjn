using UnityEngine;

[AddComponentMenu("Visualizer Studio/Modifiers/Game Object Property Modifier")]
public class VisGameObjectPropertyModifier : VisBasePropertyModifier
{
	public new static class Defaults
	{
		public const GameObjectProperty targetProperty = GameObjectProperty.UniformScale;
	}

	public GameObjectProperty targetProperty = GameObjectProperty.UniformScale;

	public override void Reset()
	{
		base.Reset();
		targetProperty = GameObjectProperty.UniformScale;
	}

	public override void Start()
	{
		base.Start();
	}

	public override void SetProperty(float propertyValue)
	{
		VisPropertyHelper.SetGameObjectProperty(base.gameObject, targetProperty, propertyValue);
	}
}
