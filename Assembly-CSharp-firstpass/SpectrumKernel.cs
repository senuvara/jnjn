using UnityEngine;

public class SpectrumKernel : MonoBehaviour
{
	public static float[] spects;

	public static float threshold = 3f;

	private void Awake()
	{
		spects = new float[1024];
	}

	private void Update()
	{
		AudioListener.GetSpectrumData(spects, 0, FFTWindow.BlackmanHarris);
	}
}
