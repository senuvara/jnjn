using UnityEngine;

public class SpectrumPartVelZ : MonoBehaviour
{
	public ParticleSystem partEmitter;

	public int audioChannel = 3;

	public float audioSensibility = 0.1f;

	public float particleVelocity = 50f;

	private void Update()
	{
		ParticleSystem.MainModule main = partEmitter.main;
		ParticleSystem.VelocityOverLifetimeModule velocityOverLifetime = partEmitter.velocityOverLifetime;
		if (SpectrumKernel.spects[audioChannel] * SpectrumKernel.threshold >= audioSensibility)
		{
			velocityOverLifetime.zMultiplier = particleVelocity;
		}
		else
		{
			velocityOverLifetime.zMultiplier = 0f;
		}
	}
}
