using UnityEngine;

[AddComponentMenu("Visualizer Studio/Triggers/Game Object Property Trigger")]
public class VisGameObjectPropertyTrigger : VisBasePropertyTrigger
{
	public new static class Defaults
	{
		public const GameObjectProperty targetProperty = GameObjectProperty.UniformScale;
	}

	public GameObjectProperty targetProperty = GameObjectProperty.UniformScale;

	public override void Reset()
	{
		base.Reset();
		targetProperty = GameObjectProperty.UniformScale;
	}

	public override void Start()
	{
		base.Start();
	}

	public override void SetProperty(float propertyValue)
	{
		VisPropertyHelper.SetGameObjectProperty(base.gameObject, targetProperty, propertyValue);
	}
}
