using UnityEngine;

public static class SA_Extensions_Bounds
{
	public static Vector3 GetVertex(this Bounds bounds, SA_VertexX x, SA_VertexY y, SA_VertexZ z)
	{
		Vector3 center = bounds.center;
		switch (x)
		{
		case SA_VertexX.Right:
		{
			float x3 = center.x;
			Vector3 extents2 = bounds.extents;
			center.x = x3 - extents2.x;
			break;
		}
		case SA_VertexX.Left:
		{
			float x2 = center.x;
			Vector3 extents = bounds.extents;
			center.x = x2 + extents.x;
			break;
		}
		}
		switch (y)
		{
		case SA_VertexY.Bottom:
		{
			float y3 = center.y;
			Vector3 extents4 = bounds.extents;
			center.y = y3 - extents4.y;
			break;
		}
		case SA_VertexY.Top:
		{
			float y2 = center.y;
			Vector3 extents3 = bounds.extents;
			center.y = y2 + extents3.y;
			break;
		}
		}
		switch (z)
		{
		case SA_VertexZ.Back:
		{
			float z3 = center.z;
			Vector3 extents6 = bounds.extents;
			center.z = z3 - extents6.z;
			break;
		}
		case SA_VertexZ.Front:
		{
			float z2 = center.z;
			Vector3 extents5 = bounds.extents;
			center.z = z2 + extents5.z;
			break;
		}
		}
		return center;
	}

	public static Bounds CalculateBounds(GameObject obj)
	{
		bool flag = false;
		Bounds result = new Bounds(Vector3.zero, Vector3.zero);
		Renderer[] componentsInChildren = obj.GetComponentsInChildren<Renderer>();
		Renderer component = obj.GetComponent<Renderer>();
		if (component != null)
		{
			result = component.bounds;
			flag = true;
		}
		Renderer[] array = componentsInChildren;
		foreach (Renderer renderer in array)
		{
			if (!flag)
			{
				result = renderer.bounds;
				flag = true;
			}
			else
			{
				result.Encapsulate(renderer.bounds);
			}
		}
		return result;
	}
}
