using SA.Common.Pattern;

namespace SA.IOSNative.UIKit
{
	public class NativeReceiver : Singleton<NativeReceiver>
	{
		public void Init()
		{
		}

		private void CalendarPickerClosed(string time)
		{
			Calendar.DatePicked(time);
		}

		private void DateTimePickerClosed(string time)
		{
			DateTimePicker.PickerClosed(time);
		}

		private void DateTimePickerDateChanged(string time)
		{
			DateTimePicker.DateChangedEvent(time);
		}
	}
}
