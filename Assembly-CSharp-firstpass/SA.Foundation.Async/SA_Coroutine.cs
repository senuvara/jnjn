using SA.Foundation.Patterns;
using System;
using System.Collections;
using System.Collections.Generic;
using UnityEngine;

namespace SA.Foundation.Async
{
	public static class SA_Coroutine
	{
		private class SA_InternalCoroutine : SA_Singleton<SA_InternalCoroutine>
		{
			public Coroutine MonoStartCoroutine(IEnumerator routine)
			{
				return StartCoroutine(routine);
			}

			public void MonoStopCoroutine(IEnumerator routine)
			{
				StopCoroutine(routine);
			}

			public Coroutine StartInstruction(YieldInstruction instruction, Action action)
			{
				return StartCoroutine(RunActionAfterInstruction(instruction, action));
			}

			private IEnumerator RunActionAfterInstruction(YieldInstruction instruction, Action action)
			{
				yield return instruction;
				action();
			}
		}

		private class CoroutineQueue : SA_Singleton<CoroutineQueue>
		{
			private Queue<IEnumerator> m_coroutineQueue = new Queue<IEnumerator>();

			protected override void Awake()
			{
				base.Awake();
				StartCoroutine(CoroutineLoop());
			}

			public void AddCoroutine(IEnumerator coroutine)
			{
				m_coroutineQueue.Enqueue(coroutine);
			}

			private IEnumerator CoroutineLoop()
			{
				while (true)
				{
					if (m_coroutineQueue.Count > 0)
					{
						yield return m_coroutineQueue.Dequeue();
					}
					else
					{
						yield return new WaitForEndOfFrame();
					}
				}
			}
		}

		public static Coroutine Start(IEnumerator routine)
		{
			return SA_Singleton<SA_InternalCoroutine>.Instance.MonoStartCoroutine(routine);
		}

		public static void Stop(IEnumerator routine)
		{
			SA_Singleton<SA_InternalCoroutine>.Instance.MonoStopCoroutine(routine);
		}

		public static void AddCoroutineToQueue(IEnumerator coroutine)
		{
			SA_Singleton<CoroutineQueue>.Instance.AddCoroutine(coroutine);
		}

		public static Coroutine WaitForEndOfFrame(Action action)
		{
			return SA_Singleton<SA_InternalCoroutine>.Instance.StartInstruction(new WaitForEndOfFrame(), action);
		}

		public static Coroutine WaitForFixedUpdate(Action action)
		{
			return SA_Singleton<SA_InternalCoroutine>.Instance.StartInstruction(new WaitForFixedUpdate(), action);
		}

		public static Coroutine WaitForSeconds(float seconds, Action action)
		{
			return SA_Singleton<SA_InternalCoroutine>.Instance.StartInstruction(new WaitForSeconds(seconds), action);
		}

		public static Coroutine WaitForSecondsRandom(float min, float max, Action action)
		{
			float seconds = UnityEngine.Random.Range(min, max);
			return WaitForSeconds(seconds, action);
		}
	}
}
