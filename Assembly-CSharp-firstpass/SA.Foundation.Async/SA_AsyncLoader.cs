using System;
using System.Collections;
using UnityEngine;

namespace SA.Foundation.Async
{
	public static class SA_AsyncLoader
	{
		public static void LoadWebTexture(string url, Action<Texture2D> callback)
		{
			SA_WWWTextureLoader sA_WWWTextureLoader = SA_WWWTextureLoader.Create();
			sA_WWWTextureLoader.OnLoad += callback;
			sA_WWWTextureLoader.LoadTexture(url);
		}

		public static void LoadResource<T>(string path, Action<T> callback) where T : UnityEngine.Object
		{
			SA_Coroutine.Start(ResourceLoadCoroutine(path, callback));
		}

		private static IEnumerator ResourceLoadCoroutine<T>(string path, Action<T> callback) where T : UnityEngine.Object
		{
			ResourceRequest request = Resources.LoadAsync<GameObject>(path);
			yield return request;
			if (request.asset == null)
			{
				Debug.LogWarning("Resource not found at path: " + path);
				callback((T)null);
			}
			else
			{
				T obj = UnityEngine.Object.Instantiate(request.asset) as T;
				callback(obj);
			}
		}
	}
}
