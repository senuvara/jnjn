using UnityEngine;

[AddComponentMenu("Visualizer Studio/Triggers/Emit Particles Trigger")]
public class VisEmitParticlesTrigger : VisBaseTrigger
{
	public new static class Defaults
	{
	}

	public int emitCount = 1;

	public int emitCountVariance;

	public override void Reset()
	{
		base.Reset();
	}

	public override void Start()
	{
		base.Start();
	}

	public override void OnTriggered(float current, float previous, float difference, float adjustedDifference)
	{
		ParticleSystem componentInChildren = GetComponentInChildren<ParticleSystem>();
		if (componentInChildren != null)
		{
			componentInChildren.Emit(emitCount + Random.Range(-emitCountVariance, emitCountVariance));
		}
	}
}
