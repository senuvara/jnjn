using System.Collections.Generic;
using UnityEngine;

[AddComponentMenu("Visualizer Studio/Modifiers/Target Modifier")]
public class VisTargetModifier : VisBaseModifier
{
	public List<GameObject> targetGameObjects = new List<GameObject>();

	public event ValueUpdatedDelegate ValueUpdated;

	public override void Start()
	{
		base.Start();
		CreateAllDelegates();
	}

	public override void OnDestroy()
	{
		base.OnDestroy();
		DestroyAllDelegates();
	}

	public void CreateAllDelegates()
	{
		for (int i = 0; i < targetGameObjects.Count; i++)
		{
			if (!(targetGameObjects[i] != null))
			{
				continue;
			}
			MonoBehaviour[] components = targetGameObjects[i].GetComponents<MonoBehaviour>();
			for (int j = 0; j < components.Length; j++)
			{
				if (components[j] != this && components[j] is IVisModifierTarget)
				{
					IVisModifierTarget visModifierTarget = components[j] as IVisModifierTarget;
					if (visModifierTarget != null)
					{
						ValueUpdated += visModifierTarget.OnValueUpdated;
					}
				}
			}
		}
	}

	public void DestroyAllDelegates()
	{
		for (int i = 0; i < targetGameObjects.Count; i++)
		{
			if (!(targetGameObjects[i] != null))
			{
				continue;
			}
			MonoBehaviour[] components = targetGameObjects[i].GetComponents<MonoBehaviour>();
			for (int j = 0; j < components.Length; j++)
			{
				if (components[j] != this && components[j] is IVisModifierTarget)
				{
					IVisModifierTarget visModifierTarget = components[j] as IVisModifierTarget;
					if (visModifierTarget != null)
					{
						ValueUpdated -= visModifierTarget.OnValueUpdated;
					}
				}
			}
		}
	}

	public void AddGameObject(GameObject gameObject)
	{
		if (!(gameObject != null) || targetGameObjects.Contains(gameObject))
		{
			return;
		}
		if (gameObject == this)
		{
			Debug.LogWarning("You are adding a game object to a modifier that is the parent of this modifier.  This may have unexpected results.");
		}
		targetGameObjects.Add(gameObject);
		MonoBehaviour[] components = gameObject.GetComponents<MonoBehaviour>();
		for (int i = 0; i < components.Length; i++)
		{
			if (components[i] != this && components[i] is IVisModifierTarget)
			{
				IVisModifierTarget visModifierTarget = components[i] as IVisModifierTarget;
				if (visModifierTarget != null)
				{
					ValueUpdated += visModifierTarget.OnValueUpdated;
				}
			}
		}
	}

	public void RemoveGameObject(GameObject gameObject)
	{
		if (!(gameObject != null) || !targetGameObjects.Contains(gameObject))
		{
			return;
		}
		targetGameObjects.Remove(gameObject);
		MonoBehaviour[] components = gameObject.GetComponents<MonoBehaviour>();
		for (int i = 0; i < components.Length; i++)
		{
			if (components[i] != this && components[i] is IVisModifierTarget)
			{
				IVisModifierTarget visModifierTarget = components[i] as IVisModifierTarget;
				if (visModifierTarget != null)
				{
					ValueUpdated -= visModifierTarget.OnValueUpdated;
				}
			}
		}
	}

	public override void OnValueUpdated(float current, float previous, float difference, float adjustedDifference)
	{
		if (this.ValueUpdated != null)
		{
			this.ValueUpdated(base.Controller.GetCurrentValue(), base.Controller.GetPreviousValue(), base.Controller.GetValueDifference(), base.Controller.GetAdjustedValueDifference());
		}
	}
}
