public interface IVisModifierTarget
{
	void OnValueUpdated(float current, float previous, float difference, float adjustedDifference);
}
