using System.Collections.Generic;
using UnityEngine;

[AddComponentMenu("Visualizer Studio/Data Group")]
public sealed class VisDataGroup : MonoBehaviour, IVisManagerTarget
{
	public static class Defaults
	{
		public static int dataGroupNameCounter;

		public const string dataGroupName = "Default";

		public const VisDataSource dataSource = VisDataSource.Spectrum;

		public const int numberSubDataGroups = 5;

		public const int frequencyRangeStartIndex = 0;

		public const int frequencyRangeEndIndex = 256;

		public const float boost = 1f;

		public const float cutoff = 1f;
	}

	[SerializeField]
	private VisManager m_oVisManager;

	[HideInInspector]
	[SerializeField]
	private string m_szLastVisManagerName;

	public string dataGroupName = string.Empty;

	public VisDataSource dataSource = VisDataSource.Spectrum;

	public int numberSubDataGroups = 5;

	public int frequencyRangeStartIndex;

	public int frequencyRangeEndIndex = 256;

	public float boost = 1f;

	public float cutoff = 1f;

	public Color debugColor = Color.white;

	private VisDataContainer[] m_aoDataContainers = new VisDataContainer[5];

	private List<VisSubDataGroup> m_oSubDataGroups = new List<VisSubDataGroup>();

	public VisManager Manager
	{
		get
		{
			return m_oVisManager;
		}
		set
		{
			if (m_oVisManager != null)
			{
				m_oVisManager.RemoveDataGroup(this);
			}
			if (value != null)
			{
				value.AddDataGroup(this);
			}
			m_oVisManager = value;
			if (m_oVisManager != null)
			{
				m_szLastVisManagerName = m_oVisManager.name;
			}
			else
			{
				m_szLastVisManagerName = null;
			}
		}
	}

	public string LastManagerName => m_szLastVisManagerName;

	public void Reset()
	{
		dataGroupName = "Default" + ++Defaults.dataGroupNameCounter;
		dataSource = VisDataSource.Spectrum;
		numberSubDataGroups = 5;
		frequencyRangeStartIndex = 0;
		frequencyRangeEndIndex = 256;
		boost = 1f;
		cutoff = 1f;
	}

	public void Awake()
	{
		if (m_oVisManager == null)
		{
			m_oVisManager = GetComponent<VisManager>();
			if (m_oVisManager == null)
			{
				Object[] array = Object.FindObjectsOfType(typeof(VisManager));
				for (int i = 0; i < array.Length; i++)
				{
					VisManager visManager = array[i] as VisManager;
					if (visManager.enabled)
					{
						m_oVisManager = visManager;
						break;
					}
				}
			}
		}
		ValidateManager(displayWarning: true);
		EnsureRegistered();
		if (m_oVisManager == null)
		{
			Debug.LogError("This Data Group does not have a VisManager assigned to it, nor could it find an active VisManager. In order to function, this Data Group needs a VisManager!");
		}
	}

	public void Start()
	{
		boost = VisHelper.Validate(boost, 0.001f, 10000f, 1f, this, "boost", error: false);
		cutoff = VisHelper.Validate(cutoff, 0.001f, 10000f, 1f, this, "cutoff", error: false);
		for (int i = 0; i < m_aoDataContainers.Length; i++)
		{
			m_aoDataContainers[i] = new VisDataContainer();
		}
		if (m_oVisManager != null)
		{
			frequencyRangeEndIndex = VisHelper.Validate(frequencyRangeEndIndex, 0, (int)m_oVisManager.windowSize, 0, this, "frequencyRangeEndIndex", error: true);
			frequencyRangeStartIndex = VisHelper.Validate(frequencyRangeStartIndex, 0, frequencyRangeEndIndex, 0, this, "frequencyRangeStartIndex", error: true);
			numberSubDataGroups = VisHelper.Validate(numberSubDataGroups, 1, frequencyRangeEndIndex - frequencyRangeStartIndex + 1, 1, this, "numberSubDataGroups", error: true);
			int num = frequencyRangeEndIndex - frequencyRangeStartIndex + 1;
			int num2 = Mathf.RoundToInt((float)num / (float)numberSubDataGroups);
			if (num2 <= 0)
			{
				num2 = 1;
			}
			int num3 = num - num2 * (num / num2);
			for (int j = frequencyRangeStartIndex; j <= frequencyRangeEndIndex - num2; j += num2)
			{
				int startIndex = j;
				int endIndex = j + num2 - 1;
				m_oSubDataGroups.Add(new VisSubDataGroup(startIndex, endIndex));
			}
			if (num3 > 0 && m_oSubDataGroups.Count > 0)
			{
				m_oSubDataGroups[m_oSubDataGroups.Count - 1].frequencyRangeEndIndex += num3;
			}
		}
		else
		{
			Debug.LogError("This data group must have an assigned VisManager.");
		}
	}

	public void OnDestroy()
	{
		if (m_oVisManager != null)
		{
			m_oVisManager.RemoveDataGroup(this);
		}
	}

	public bool ValidateManager(bool displayWarning)
	{
		if (m_oVisManager != null && m_oVisManager.gameObject != base.gameObject)
		{
			if (displayWarning)
			{
				Debug.LogWarning("This Data Group (" + dataGroupName + ") is in a different Game Object than it's Manager (" + m_oVisManager.name + ").  Please make sure it is attached to the same Game Object to prevent issues.", this);
			}
			return false;
		}
		return true;
	}

	public void EnsureRegistered()
	{
		if (m_oVisManager != null)
		{
			m_oVisManager.AddDataGroup(this);
		}
	}

	private void Update()
	{
		if (!(m_oVisManager != null))
		{
			return;
		}
		float[] spectrum = (dataSource != VisDataSource.Spectrum) ? m_oVisManager.GetRawAudioData() : m_oVisManager.GetSpectrumData();
		for (int i = 0; i < m_oSubDataGroups.Count; i++)
		{
			m_oSubDataGroups[i].Update(spectrum);
		}
		for (int j = 0; j < m_aoDataContainers.Length; j++)
		{
			m_aoDataContainers[j].UpdatePreviousValues();
			m_aoDataContainers[j].ResetCurrentValues();
			for (int k = 0; k < m_oSubDataGroups.Count; k++)
			{
				float value = m_oSubDataGroups[k].GetValue((VisDataValueType)j);
				m_aoDataContainers[j].sum += value;
				if (Mathf.Abs(value) < m_aoDataContainers[j].minimum)
				{
					m_aoDataContainers[j].minimum = value;
				}
				if (Mathf.Abs(value) > m_aoDataContainers[j].maximum)
				{
					m_aoDataContainers[j].maximum = value;
				}
			}
			m_aoDataContainers[j].average = m_aoDataContainers[j].sum / (float)m_oSubDataGroups.Count;
			m_aoDataContainers[j].median = (m_aoDataContainers[j].minimum + m_aoDataContainers[j].maximum) * 0.5f;
			m_aoDataContainers[j].ApplyBoostAndCutoff(boost, cutoff);
			m_aoDataContainers[j].UpdateValueDifferences();
		}
	}

	public float GetValue(VisDataValueType finalValueType, VisDataValueType subValueType)
	{
		return m_aoDataContainers[(int)subValueType].GetValue(finalValueType);
	}

	public float GetPreviousValue(VisDataValueType finalValueType, VisDataValueType subValueType)
	{
		return m_aoDataContainers[(int)subValueType].GetPreviousValue(finalValueType);
	}

	public float GetValueDifference(VisDataValueType finalValueType, VisDataValueType subValueType)
	{
		return m_aoDataContainers[(int)subValueType].GetValueDifference(finalValueType);
	}

	public Rect DisplayDebugGUI(int x, int y, int barWidth, int barHeight, int separation, Texture debugTexture)
	{
		if (debugTexture != null)
		{
			int num = 20;
			int num2 = 5;
			int num3 = barWidth * m_oSubDataGroups.Count;
			int num4 = num3 * 5 + separation * 4 + num2 * 2;
			int num5 = barHeight + num2 * 2 + num * 4;
			Rect rect = new Rect(x - num2, y - num2, num4, num5);
			float num6 = (!(m_oVisManager != null)) ? 0f : ((float)frequencyRangeStartIndex * m_oVisManager.FrequencyResolution);
			float num7 = (!(m_oVisManager != null)) ? 0f : ((float)(frequencyRangeEndIndex + 1) * m_oVisManager.FrequencyResolution);
			GUI.BeginGroup(rect);
			GUI.color = new Color(0f, 0f, 0f, 0.5f);
			GUI.DrawTexture(new Rect(0f, 0f, rect.width, rect.height), debugTexture);
			GUI.color = Color.white;
			GUI.Label(new Rect(num2, num2, 200f, num + 3), "Data Group: \"" + dataGroupName + "\"");
			GUI.Label(new Rect(num2, num2 + num, 200f, num + 3), "Range: " + num6.ToString("F1") + " Hz to " + num7.ToString("F1") + " Hz");
			GUI.Label(new Rect(num2, num2 + num * 2, 200f, num + 3), "Boost: " + boost + "    Cutoff: " + cutoff);
			for (int i = 0; i <= 4; i++)
			{
				for (int j = 0; j < m_oSubDataGroups.Count; j++)
				{
					float value = m_oSubDataGroups[j].GetValue((VisDataValueType)i);
					value *= boost;
					value = Mathf.Clamp(value, (!(value < 0f)) ? 0f : (0f - cutoff), (!(value < 0f)) ? cutoff : 0f);
					float num8 = Mathf.Abs(value) / cutoff;
					GUI.color = debugColor;
					if (dataSource == VisDataSource.Spectrum)
					{
						int num9 = (int)((float)(barHeight - 1) * num8) + 1;
						GUI.DrawTexture(new Rect(num2 + barWidth * j + i * (num3 + separation), num * 3 + num2 + barHeight - num9, barWidth, num9), debugTexture);
						continue;
					}
					int num10 = (int)((float)(barHeight / 2 - 1) * num8) + 1;
					if (value < 0f)
					{
						GUI.DrawTexture(new Rect(num2 + barWidth * j + i * (num3 + separation), num * 3 + num2 + barHeight / 2, barWidth, num10), debugTexture);
					}
					else
					{
						GUI.DrawTexture(new Rect(num2 + barWidth * j + i * (num3 + separation), num * 3 + num2 + barHeight / 2 - num10 + 1, barWidth, num10), debugTexture);
					}
				}
				string text = "avg";
				switch (i)
				{
				case 1:
					text = "med";
					break;
				case 2:
					text = "sum";
					break;
				case 3:
					text = "min";
					break;
				case 4:
					text = "max";
					break;
				}
				GUI.color = Color.white;
				GUI.Label(new Rect(num2 + i * (num3 + separation), num2 + barHeight + num * 3, 200f, num + 3), text);
			}
			GUI.color = Color.white;
			GUI.DrawTexture(new Rect(0f, 0f, num4, 1f), debugTexture);
			GUI.DrawTexture(new Rect(0f, num5 - 1, num4, 1f), debugTexture);
			GUI.DrawTexture(new Rect(0f, 0f, 1f, num5), debugTexture);
			GUI.DrawTexture(new Rect(num4 - 1, 0f, 1f, num5), debugTexture);
			GUI.EndGroup();
			return rect;
		}
		return new Rect(0f, 0f, 0f, 0f);
	}

	public override string ToString()
	{
		return "VisDataGroup \"" + dataGroupName + "\"";
	}

	public static bool RestoreVisDataGroupTarget(IVisDataGroupTarget target)
	{
		if (target.DataGroup == null && target.LastDataGroupName != null && target.LastDataGroupName.Length > 0)
		{
			VisManager visManager = null;
			if (target is IVisManagerTarget)
			{
				visManager = (target as IVisManagerTarget).Manager;
			}
			if (visManager != null)
			{
				for (int i = 0; i < visManager.DataGroups.Count; i++)
				{
					if (visManager.DataGroups[i].dataGroupName == target.LastDataGroupName)
					{
						target.DataGroup = visManager.DataGroups[i];
						return true;
					}
				}
			}
		}
		return false;
	}
}
