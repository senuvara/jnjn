using UnityEngine;

[AddComponentMenu("Visualizer Studio/Triggers/Message Trigger")]
public class VisMessageTrigger : VisTargetTrigger
{
	public new static class Defaults
	{
		public const string messageName = "TriggerMessage";

		public const ControllerSourceValue messageParameter = ControllerSourceValue.Current;
	}

	public enum ControllerSourceValue
	{
		None,
		Current,
		Previous,
		Difference
	}

	public string messageName = "TriggerMessage";

	public ControllerSourceValue messageParameter = ControllerSourceValue.Current;

	public override void Reset()
	{
		base.Reset();
		messageName = "TriggerMessage";
		messageParameter = ControllerSourceValue.Current;
	}

	public override void Start()
	{
		base.Start();
	}

	public override void Update()
	{
		base.Update();
	}

	public override void OnTriggered(float current, float previous, float difference, float adjustedDifference)
	{
		base.OnTriggered(current, previous, difference, adjustedDifference);
		for (int i = 0; i < targetGameObjects.Count; i++)
		{
			switch (messageParameter)
			{
			case ControllerSourceValue.None:
				targetGameObjects[i].SendMessage(messageName);
				break;
			case ControllerSourceValue.Current:
				targetGameObjects[i].SendMessage(messageName, current);
				break;
			case ControllerSourceValue.Previous:
				targetGameObjects[i].SendMessage(messageName, previous);
				break;
			case ControllerSourceValue.Difference:
				targetGameObjects[i].SendMessage(messageName, adjustedDifference);
				break;
			}
		}
	}
}
