using UnityEngine;

public static class SA_Extensions_Vector3
{
	public static Vector3 Reset(this Vector3 vec)
	{
		return Vector3.zero;
	}

	public static Vector3 ResetXCoord(this Vector3 vec)
	{
		Vector3 result = vec;
		result.x = 0f;
		return result;
	}

	public static Vector3 ResetYCoord(this Vector3 vec)
	{
		Vector3 result = vec;
		result.y = 0f;
		return result;
	}

	public static Vector3 ResetZCoord(this Vector3 vec)
	{
		Vector3 result = vec;
		result.z = 0f;
		return result;
	}

	public static Vector3 Average(this Vector3[] f)
	{
		Vector3 zero = Vector3.zero;
		int num = 0;
		for (int i = 0; i < f.Length - 1; i++)
		{
			if (f[i] != Vector3.zero)
			{
				zero += f[i];
				num++;
			}
		}
		num = ((num <= 0) ? 1 : num);
		zero.x /= num;
		zero.y /= num;
		zero.z /= num;
		return zero;
	}

	public static void Add(this Vector3[] v3, Vector3 value)
	{
		for (int num = v3.Length - 1; num > 0; num--)
		{
			v3[num] = v3[num - 1];
		}
		v3[0] = value;
	}
}
