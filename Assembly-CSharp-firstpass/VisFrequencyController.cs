using UnityEngine;

[AddComponentMenu("Visualizer Studio/Controllers/Frequency Controller")]
public class VisFrequencyController : VisBaseController, IVisDataGroupTarget
{
	public new static class Defaults
	{
		public const VisDataValueType subValueType = VisDataValueType.Sum;

		public const VisDataValueType finalValueType = VisDataValueType.Average;
	}

	[SerializeField]
	private VisDataGroup dataGroup;

	[HideInInspector]
	[SerializeField]
	private string m_szLastDataGroupName;

	public VisDataValueType subValueType = VisDataValueType.Sum;

	public VisDataValueType finalValueType;

	public VisDataGroup DataGroup
	{
		get
		{
			return dataGroup;
		}
		set
		{
			dataGroup = value;
			if (dataGroup != null)
			{
				m_szLastDataGroupName = dataGroup.dataGroupName;
			}
			else
			{
				m_szLastDataGroupName = null;
			}
		}
	}

	public string LastDataGroupName => m_szLastDataGroupName;

	public override void Reset()
	{
		base.Reset();
		subValueType = VisDataValueType.Sum;
		finalValueType = VisDataValueType.Average;
	}

	public override void Start()
	{
		base.Start();
		if (base.Manager == null)
		{
			Debug.LogError("This controller must have a VisManager assigned to it in order to function.");
		}
		if (dataGroup == null)
		{
			Debug.LogError("This controller must have a VisDataGroup assigned to it in order to function. Please double check the spelling of the target data group.");
		}
	}

	public override float GetCustomControllerValue()
	{
		if (dataGroup != null)
		{
			return dataGroup.GetValue(finalValueType, subValueType);
		}
		return base.GetCustomControllerValue();
	}

	public override Rect DisplayDebugGUI(int x, int y, int barWidth, int barHeight, int separation, Texture debugTexture)
	{
		if (debugTexture != null)
		{
			int num = 150;
			int num2 = 20;
			int num3 = 5;
			int num4 = Mathf.Max(barWidth, num) + num3 * 2;
			int num5 = num3 * 2 + num2 * 5 + barHeight;
			Rect rect = new Rect(x - num3, y - num3, num4, num5);
			GUI.BeginGroup(rect);
			GUI.color = new Color(0f, 0f, 0f, 0.5f);
			GUI.DrawTexture(new Rect(0f, 0f, rect.width, rect.height), debugTexture);
			GUI.color = Color.white;
			GUI.Label(new Rect(num3, num3, num, num2 + 3), "Controller: \"" + controllerName + "\"");
			GUI.Label(new Rect(num3, num3 + num2, num, num2 + 3), "Data Group: \"" + ((!(dataGroup != null)) ? "NONE" : dataGroup.dataGroupName) + "\"");
			GUI.Label(new Rect(num3, num3 + num2 * 2, num, num2 + 3), string.Concat("Sub Type: \"", subValueType, "\""));
			GUI.Label(new Rect(num3, num3 + num2 * 3, num, num2 + 3), string.Concat("Final Type: \"", finalValueType, "\""));
			GUI.Label(new Rect(num3, num3 + num2 * 4, num, num2 + 3), "VALUE: " + GetCurrentValue().ToString("F4"));
			float num6 = (m_fValue - base.MinValue) / (base.MaxValue - base.MinValue) * 0.975f + 0.025f;
			if (dataGroup != null)
			{
				GUI.color = dataGroup.debugColor;
			}
			GUI.DrawTexture(new Rect(num3, num3 + num2 * 5, (int)((float)barWidth * num6), barHeight), debugTexture);
			GUI.color = Color.white;
			GUI.DrawTexture(new Rect(0f, 0f, num4, 1f), debugTexture);
			GUI.DrawTexture(new Rect(0f, num5 - 1, num4, 1f), debugTexture);
			GUI.DrawTexture(new Rect(0f, 0f, 1f, num5), debugTexture);
			GUI.DrawTexture(new Rect(num4 - 1, 0f, 1f, num5), debugTexture);
			GUI.EndGroup();
			return rect;
		}
		return new Rect(0f, 0f, 0f, 0f);
	}
}
