using System;
using System.Collections.Generic;
using System.Text.RegularExpressions;

public static class SA_Extensions_Uri
{
	private static readonly Regex _regex = new Regex("[?|&]([\\w\\.]+)=([^?|^&]+)");

	public static Dictionary<string, string> ParseQueryString(this Uri uri)
	{
		Match match = _regex.Match(uri.PathAndQuery);
		Dictionary<string, string> dictionary = new Dictionary<string, string>();
		while (match.Success)
		{
			dictionary.Add(match.Groups[1].Value, match.Groups[2].Value);
			match = match.NextMatch();
		}
		return dictionary;
	}
}
