using UnityEngine;

[AddComponentMenu("Visualizer Studio/Triggers/Add Force Trigger")]
public class VisAddForceTrigger : VisBaseTrigger
{
	public new static class Defaults
	{
		public const ControllerSourceValue controllerValue = ControllerSourceValue.Current;

		public const float minControllerValue = 0f;

		public const float maxControllerValue = 1f;

		public const float minForceValue = 10f;

		public const float maxForceValue = 200f;

		public const bool invertValue = false;

		public const bool randomValue = false;

		public static readonly Vector3 forceDirection = Vector3.up;

		public const ForceMode forceMode = ForceMode.Impulse;
	}

	public ControllerSourceValue controllerValue;

	public float minControllerValue;

	public float maxControllerValue = 1f;

	public float minForceValue = 10f;

	public float maxForceValue = 200f;

	public bool invertValue;

	public bool randomValue;

	public Vector3 forceDirection = Defaults.forceDirection;

	public ForceMode forceMode = ForceMode.Impulse;

	public override void Reset()
	{
		base.Reset();
	}

	public override void Start()
	{
		base.Start();
	}

	public override void OnTriggered(float current, float previous, float difference, float adjustedDifference)
	{
		Rigidbody component = GetComponent<Rigidbody>();
		if (component != null)
		{
			Vector3 normalized = forceDirection.normalized;
			float sourceValue = current;
			if (controllerValue == ControllerSourceValue.Previous)
			{
				sourceValue = previous;
			}
			else if (controllerValue == ControllerSourceValue.Difference)
			{
				sourceValue = adjustedDifference;
			}
			float d = VisHelper.ConvertBetweenRanges(sourceValue, minControllerValue, maxControllerValue, minForceValue, maxForceValue, invertValue);
			component.AddForce(normalized * d, forceMode);
		}
	}
}
