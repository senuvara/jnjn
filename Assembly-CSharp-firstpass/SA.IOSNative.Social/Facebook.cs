using SA.Common.Data;
using SA.Common.Models;
using SA.Common.Pattern;
using System;
using System.Collections.Generic;
using UnityEngine;

namespace SA.IOSNative.Social
{
	public static class Facebook
	{
		private class NativeListener : Singleton<NativeListener>
		{
			private void OnFacebookPostFailed()
			{
				Result obj = new Result(new Error());
				Facebook.OnPostResult(obj);
			}

			private void OnFacebookPostSuccess()
			{
				Result obj = new Result();
				Facebook.OnPostResult(obj);
			}
		}

		private static class Internal
		{
			public static void ISN_FbPost(string text, string url, string encodedMedia)
			{
			}
		}

		public static event Action OnPostStart;

		public static event Action<Result> OnPostResult;

		static Facebook()
		{
			Facebook.OnPostStart = delegate
			{
			};
			Facebook.OnPostResult = delegate
			{
			};
			Singleton<NativeListener>.Instantiate();
		}

		public static void Post(string text, Action<Result> callback = null)
		{
			Post(text, null, new Texture2D[0], callback);
		}

		public static void Post(Texture2D image, Action<Result> callback = null)
		{
			Post(null, null, new Texture2D[1]
			{
				image
			}, callback);
		}

		public static void Post(Texture2D[] images, Action<Result> callback = null)
		{
			Post(null, null, images, callback);
		}

		public static void Post(string text, string url, Action<Result> callback = null)
		{
			Post(text, url, new Texture2D[0], callback);
		}

		public static void Post(string text, Texture2D image, Action<Result> callback = null)
		{
			Post(text, null, new Texture2D[1]
			{
				image
			}, callback);
		}

		public static void Post(string text, Texture2D[] images, Action<Result> callback = null)
		{
			Post(text, null, images, callback);
		}

		public static void Post(string text, string url, Texture2D image, Action<Result> callback = null)
		{
			Post(text, url, new Texture2D[1]
			{
				image
			}, callback);
		}

		public static void Post(string text, string url, Texture2D[] images, Action<Result> callback = null)
		{
			if (url == null)
			{
				url = string.Empty;
			}
			if (text == null)
			{
				text = string.Empty;
			}
			if (images == null)
			{
				images = new Texture2D[0];
			}
			if (callback != null)
			{
				OnPostResult += callback;
			}
			if (Application.platform == RuntimePlatform.IPhonePlayer)
			{
				Facebook.OnPostStart();
			}
			List<string> list = new List<string>();
			Texture2D[] array = images;
			foreach (Texture2D tex in array)
			{
				byte[] inArray = tex.EncodeToPNG();
				list.Add(Convert.ToBase64String(inArray));
			}
			string encodedMedia = Converter.SerializeArray(list.ToArray());
			Internal.ISN_FbPost(text, url, encodedMedia);
		}
	}
}
