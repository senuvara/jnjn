using SA.Common.Data;
using SA.Common.Models;
using System;
using System.Collections.Generic;
using UnityEngine;

namespace SA.IOSNative.Social
{
	public static class Meida
	{
		private static class Internal
		{
			public static void ISN_MediaShare(string text, string encodedMedia)
			{
			}
		}

		public static event Action<Result> OnShareResult;

		public static void Share(string message, Action<Result> callback = null)
		{
			Share(message, new Texture2D[0], callback);
		}

		public static void Share(Texture2D image, Action<Result> callback = null)
		{
			Share(null, new Texture2D[1]
			{
				image
			}, callback);
		}

		public static void Share(Texture2D[] images, Action<Result> callback = null)
		{
			Share(null, images, callback);
		}

		public static void Share(string message, Texture2D image, Action<Result> callback = null)
		{
			Share(message, new Texture2D[1]
			{
				image
			}, callback);
		}

		public static void Share(string message, Texture2D[] images, Action<Result> callback = null)
		{
			if (message == null)
			{
				message = string.Empty;
			}
			if (callback != null)
			{
				OnShareResult += callback;
			}
			List<string> list = new List<string>();
			foreach (Texture2D tex in images)
			{
				byte[] inArray = tex.EncodeToPNG();
				list.Add(Convert.ToBase64String(inArray));
			}
			string encodedMedia = Converter.SerializeArray(list.ToArray());
			Internal.ISN_MediaShare(message, encodedMedia);
		}

		static Meida()
		{
			Meida.OnShareResult = delegate
			{
			};
		}
	}
}
