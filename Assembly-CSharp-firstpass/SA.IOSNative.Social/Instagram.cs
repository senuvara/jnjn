using SA.Common.Models;
using SA.Common.Pattern;
using System;
using UnityEngine;

namespace SA.IOSNative.Social
{
	public static class Instagram
	{
		private class NativeListener : Singleton<NativeListener>
		{
			private void OnInstaPostSuccess()
			{
				Result obj = new Result();
				Instagram.OnPostResult(obj);
			}

			private void OnInstaPostFailed(string data)
			{
				int code = Convert.ToInt32(data);
				Error error = new Error(code, "Posting Failed");
				Result obj = new Result(error);
				Instagram.OnPostResult(obj);
			}
		}

		private static class Internal
		{
			public static void ISN_InstaShare(string encodedMedia, string message)
			{
			}
		}

		public static event Action OnPostStart;

		public static event Action<Result> OnPostResult;

		static Instagram()
		{
			Instagram.OnPostStart = delegate
			{
			};
			Instagram.OnPostResult = delegate
			{
			};
			Singleton<NativeListener>.Instantiate();
		}

		public static void Post(Texture2D image, Action<Result> callback = null)
		{
			Post(image, null, callback);
		}

		public static void Post(Texture2D image, string message, Action<Result> callback = null)
		{
			if (message == null)
			{
				message = string.Empty;
			}
			if (callback != null)
			{
				OnPostResult += callback;
			}
			if (Application.platform == RuntimePlatform.IPhonePlayer)
			{
				Instagram.OnPostStart();
			}
			byte[] inArray = image.EncodeToPNG();
			string encodedMedia = Convert.ToBase64String(inArray);
			Internal.ISN_InstaShare(encodedMedia, message);
		}
	}
}
