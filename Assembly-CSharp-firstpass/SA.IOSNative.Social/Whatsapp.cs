using System;
using UnityEngine;

namespace SA.IOSNative.Social
{
	public static class Whatsapp
	{
		private static class Internal
		{
			public static void ISN_WP_ShareText(string message)
			{
			}

			public static void ISN_WP_ShareMedia(string encodedMedia)
			{
			}
		}

		public static void Post(string message)
		{
			Internal.ISN_WP_ShareText(message);
		}

		public static void Post(Texture2D image)
		{
			byte[] inArray = image.EncodeToPNG();
			string encodedMedia = Convert.ToBase64String(inArray);
			Internal.ISN_WP_ShareMedia(encodedMedia);
		}
	}
}
