using SA.Common.Data;
using SA.Common.Models;
using SA.Common.Pattern;
using System;
using System.Collections.Generic;
using UnityEngine;

namespace SA.IOSNative.Social
{
	public static class Mail
	{
		private class NativeListener : Singleton<NativeListener>
		{
			private void OnMailFailed()
			{
				Result obj = new Result(new Error());
				Mail.OnSendMailResult(obj);
			}

			private void OnMailSuccess()
			{
				Result obj = new Result();
				Mail.OnSendMailResult(obj);
			}
		}

		private static class Internal
		{
			public static void ISN_SendMail(string subject, string body, string recipients, string encodedMedia)
			{
			}
		}

		public static event Action<Result> OnSendMailResult;

		public static void Send(string subject, string body, string recipient, Action<Result> callback = null)
		{
			Send(subject, body, new string[1]
			{
				recipient
			}, new Texture2D[0], callback);
		}

		public static void Send(string subject, string body, string recipient, Texture2D image, Action<Result> callback = null)
		{
			Send(subject, body, new string[1]
			{
				recipient
			}, new Texture2D[1]
			{
				image
			}, callback);
		}

		public static void Send(string subject, string body, string recipient, Texture2D[] images, Action<Result> callback = null)
		{
			Send(subject, body, new string[1]
			{
				recipient
			}, images, callback);
		}

		public static void Send(string subject, string body, string[] recipients, Action<Result> callback = null)
		{
			Send(subject, body, recipients, new Texture2D[0], callback);
		}

		public static void Send(string subject, string body, string[] recipients, Texture2D image, Action<Result> callback = null)
		{
			Send(subject, body, recipients, new Texture2D[1]
			{
				image
			}, callback);
		}

		public static void Send(string subject, string body, string[] recipients, Texture2D[] images, Action<Result> callback = null)
		{
			if (subject == null)
			{
				subject = string.Empty;
			}
			if (body == null)
			{
				body = string.Empty;
			}
			if (recipients == null)
			{
				recipients = new string[0];
			}
			if (images == null)
			{
				images = new Texture2D[0];
			}
			if (callback != null)
			{
				OnSendMailResult += callback;
			}
			string recipients2 = Converter.SerializeArray(recipients);
			List<string> list = new List<string>();
			Texture2D[] array = images;
			foreach (Texture2D tex in array)
			{
				byte[] inArray = tex.EncodeToPNG();
				list.Add(Convert.ToBase64String(inArray));
			}
			string encodedMedia = Converter.SerializeArray(list.ToArray());
			Internal.ISN_SendMail(subject, body, recipients2, encodedMedia);
		}

		static Mail()
		{
			Mail.OnSendMailResult = delegate
			{
			};
		}
	}
}
