using SA.Common.Data;
using SA.Common.Models;
using SA.Common.Pattern;
using System;
using System.Collections.Generic;
using UnityEngine;

namespace SA.IOSNative.Social
{
	public static class Twitter
	{
		private class NativeListener : Singleton<NativeListener>
		{
			private void OnTwitterPostFailed()
			{
				Result obj = new Result(new Error());
				Twitter.OnPostResult(obj);
			}

			private void OnTwitterPostSuccess()
			{
				Result obj = new Result();
				Twitter.OnPostResult(obj);
			}
		}

		private static class Internal
		{
			public static void ISN_TwPost(string text, string url, string encodedMedia)
			{
			}

			public static void ISN_TwPostGIF(string text, string url)
			{
			}
		}

		public static event Action OnPostStart;

		public static event Action<Result> OnPostResult;

		static Twitter()
		{
			Twitter.OnPostStart = delegate
			{
			};
			Twitter.OnPostResult = delegate
			{
			};
			Singleton<NativeListener>.Instantiate();
		}

		public static void Post(string text, Action<Result> callback = null)
		{
			Post(text, null, new Texture2D[0], callback);
		}

		public static void Post(Texture2D image, Action<Result> callback = null)
		{
			Post(null, null, new Texture2D[1]
			{
				image
			}, callback);
		}

		public static void Post(Texture2D[] images, Action<Result> callback = null)
		{
			Post(null, null, images, callback);
		}

		public static void Post(string text, string url, Action<Result> callback = null)
		{
			Post(text, url, new Texture2D[0], callback);
		}

		public static void Post(string text, Texture2D image, Action<Result> callback = null)
		{
			Post(text, null, new Texture2D[1]
			{
				image
			}, callback);
		}

		public static void Post(string text, Texture2D[] images, Action<Result> callback = null)
		{
			Post(text, null, images, callback);
		}

		public static void Post(string text, string url, Texture2D image, Action<Result> callback = null)
		{
			Post(text, url, new Texture2D[1]
			{
				image
			}, callback);
		}

		public static void Post(string text, string url, Texture2D[] images, Action<Result> callback = null)
		{
			if (url == null)
			{
				url = string.Empty;
			}
			if (text == null)
			{
				text = string.Empty;
			}
			if (images == null)
			{
				images = new Texture2D[0];
			}
			if (callback != null)
			{
				OnPostResult += callback;
			}
			if (Application.platform == RuntimePlatform.IPhonePlayer)
			{
				Twitter.OnPostStart();
			}
			List<string> list = new List<string>();
			Texture2D[] array = images;
			foreach (Texture2D tex in array)
			{
				byte[] inArray = tex.EncodeToPNG();
				list.Add(Convert.ToBase64String(inArray));
			}
			string encodedMedia = Converter.SerializeArray(list.ToArray());
			Internal.ISN_TwPost(text, url, encodedMedia);
		}

		public static void PostGif(string text, string url)
		{
			Internal.ISN_TwPostGIF(text, url);
		}
	}
}
