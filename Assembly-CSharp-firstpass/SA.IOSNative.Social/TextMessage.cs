using SA.Common.Data;
using SA.Common.Pattern;
using System;
using System.Collections.Generic;
using UnityEngine;

namespace SA.IOSNative.Social
{
	public static class TextMessage
	{
		private class NativeListener : Singleton<NativeListener>
		{
			private void OnTextMessageComposeResult(string data)
			{
				int obj = Convert.ToInt32(data);
				TextMessage.OnTextMessageResult((TextMessageComposeResult)obj);
				TextMessage.OnTextMessageResult = delegate
				{
				};
			}
		}

		private static class Internal
		{
			public static void ISN_SendTextMessage(string body, string recipients, string encodedMedia)
			{
			}
		}

		public static event Action<TextMessageComposeResult> OnTextMessageResult;

		public static void Send(string body, string recipient, Action<TextMessageComposeResult> callback = null)
		{
			Send(body, new string[1]
			{
				recipient
			}, new Texture2D[0], callback);
		}

		public static void Send(string body, string recipient, Texture2D image, Action<TextMessageComposeResult> callback = null)
		{
			Send(body, new string[1]
			{
				recipient
			}, new Texture2D[1]
			{
				image
			}, callback);
		}

		public static void Send(string body, string recipient, Texture2D[] images, Action<TextMessageComposeResult> callback = null)
		{
			Send(body, new string[1]
			{
				recipient
			}, images, callback);
		}

		public static void Send(string body, string[] recipients, Action<TextMessageComposeResult> callback = null)
		{
			Send(body, recipients, new Texture2D[0], callback);
		}

		public static void Send(string body, string[] recipients, Texture2D image, Action<TextMessageComposeResult> callback = null)
		{
			Send(body, recipients, new Texture2D[1]
			{
				image
			}, callback);
		}

		public static void Send(string body, string[] recipients, Texture2D[] images, Action<TextMessageComposeResult> callback = null)
		{
			if (body == null)
			{
				body = string.Empty;
			}
			if (recipients == null)
			{
				recipients = new string[0];
			}
			if (images == null)
			{
				images = new Texture2D[0];
			}
			if (callback != null)
			{
				OnTextMessageResult += callback;
			}
			string recipients2 = Converter.SerializeArray(recipients);
			List<string> list = new List<string>();
			Texture2D[] array = images;
			foreach (Texture2D tex in array)
			{
				byte[] inArray = tex.EncodeToPNG();
				list.Add(Convert.ToBase64String(inArray));
			}
			string encodedMedia = Converter.SerializeArray(list.ToArray());
			Internal.ISN_SendTextMessage(body, recipients2, encodedMedia);
		}

		static TextMessage()
		{
			TextMessage.OnTextMessageResult = delegate
			{
			};
		}
	}
}
