using System;
using UnityEngine;

namespace CodeStage.AntiCheat.Detectors
{
	[Obsolete("Please use ACTkDetectorBase instead.", true)]
	public class ActDetectorBase : MonoBehaviour
	{
	}
}
