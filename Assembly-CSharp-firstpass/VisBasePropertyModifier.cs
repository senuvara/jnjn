using UnityEngine;

public abstract class VisBasePropertyModifier : VisBaseModifier
{
	public new static class Defaults
	{
		public const ControllerSourceValue controllerSourceValue = ControllerSourceValue.Current;

		public const float minControllerValue = 0f;

		public const float maxControllerValue = 1f;

		public const float minPropertyValue = -1f;

		public const float maxPropertyValue = 1f;

		public const bool invertValue = false;

		public const bool randomValue = true;
	}

	public ControllerSourceValue controllerSourceValue;

	public float minControllerValue;

	public float maxControllerValue = 1f;

	public float minPropertyValue = -1f;

	public float maxPropertyValue = 1f;

	public bool invertValue;

	public override void Reset()
	{
		base.Reset();
		controllerSourceValue = ControllerSourceValue.Current;
		minControllerValue = 0f;
		maxControllerValue = 1f;
		minPropertyValue = -1f;
		maxPropertyValue = 1f;
	}

	public override void Start()
	{
		base.Start();
		maxControllerValue = VisHelper.Validate(maxControllerValue, -1000000f, 1000000f, 1f, this, "maxControllerValue", error: false);
		minControllerValue = VisHelper.Validate(minControllerValue, -1000000f, maxControllerValue, Mathf.Min(0f, maxControllerValue), this, "minControllerValue", error: false);
		maxPropertyValue = VisHelper.Validate(maxPropertyValue, -1000000f, 1000000f, 1f, this, "maxPropertyValue", error: false);
		minPropertyValue = VisHelper.Validate(minPropertyValue, -1000000f, maxPropertyValue, Mathf.Min(-1f, maxPropertyValue), this, "minPropertyValue", error: false);
	}

	public override void OnValueUpdated(float current, float previous, float difference, float adjustedDifference)
	{
		float sourceValue = current;
		if (controllerSourceValue == ControllerSourceValue.Previous)
		{
			sourceValue = previous;
		}
		else if (controllerSourceValue == ControllerSourceValue.Difference)
		{
			sourceValue = adjustedDifference;
		}
		float property = VisHelper.ConvertBetweenRanges(sourceValue, minControllerValue, maxControllerValue, minPropertyValue, maxPropertyValue, invertValue);
		SetProperty(property);
	}

	public abstract void SetProperty(float propertyValue);
}
