using SA.Foundation.Utility;
using System;
using UnityEngine;

namespace SA.Foundation.UtilitiesEditor
{
	public static class SA_AssetDatabase
	{
		private class SA_AssetDatabaseProxy
		{
			public static void Create(UnityEngine.Object asset, string path)
			{
				path = FixPath(path);
			}

			public static bool Copy(string path, string newPath)
			{
				path = FixPath(path);
				newPath = FixPath(newPath);
				return false;
			}

			public static string Move(string oldPath, string newPath)
			{
				oldPath = FixPath(oldPath);
				newPath = FixPath(newPath);
				return string.Empty;
			}

			public static bool Delete(string path)
			{
				path = FixPath(path);
				return false;
			}

			public static void CreateFolder(string parentFolder, string newFolderName)
			{
				parentFolder = FixPath(parentFolder);
			}

			public static string GetAssetPath(UnityEngine.Object assetObject)
			{
				return string.Empty;
			}

			public static T LoadAssetAtPath<T>(string assetPath) where T : UnityEngine.Object
			{
				return (T)null;
			}

			private static string FixPath(string path)
			{
				path = "Assets/" + path;
				if (path.EndsWith("/", StringComparison.CurrentCulture))
				{
					path = path.Substring(0, path.Length - 1);
				}
				return path;
			}
		}

		public static void CreateAsset(UnityEngine.Object asset, string path)
		{
			path = FixRelativePath(path);
			SA_AssetDatabaseProxy.Create(asset, path);
		}

		public static string GetProjectFolderRelativePath(UnityEngine.Object assetObject)
		{
			return SA_AssetDatabaseProxy.GetAssetPath(assetObject);
		}

		public static string GetAssetFolderRelativePath(UnityEngine.Object assetObject)
		{
			return FixRelativePath(SA_AssetDatabaseProxy.GetAssetPath(assetObject), validateFoldersPath: false);
		}

		public static string GetAbsoluteAssetPath(UnityEngine.Object assetObject)
		{
			string assetFolderRelativePath = GetAssetFolderRelativePath(assetObject);
			return SA_PathUtil.ConvertRelativeToAbsolutePath(assetFolderRelativePath);
		}

		public static bool CopyAsset(string path, string newPath)
		{
			newPath = FixRelativePath(newPath);
			return SA_AssetDatabaseProxy.Copy(path, newPath);
		}

		public static string MoveAsset(string oldPath, string newPath)
		{
			oldPath = FixRelativePath(oldPath);
			newPath = FixRelativePath(newPath);
			return SA_AssetDatabaseProxy.Move(oldPath, newPath);
		}

		public static bool DeleteAsset(string path)
		{
			path = FixRelativePath(path);
			return SA_AssetDatabaseProxy.Delete(path);
		}

		public static T LoadAssetAtPath<T>(string assetPath) where T : UnityEngine.Object
		{
			assetPath = FixRelativePath(assetPath);
			return SA_AssetDatabaseProxy.LoadAssetAtPath<T>(assetPath);
		}

		public static bool IsDirectoryExists(string path)
		{
			return SA_PathUtil.IsDirectoryExists(path);
		}

		private static string FixRelativePath(string path, bool validateFoldersPath = true)
		{
			path = SA_PathUtil.FixRelativePath(path);
			if (validateFoldersPath)
			{
				ValidateFoldersPath(path);
			}
			return path;
		}

		private static void ValidateFoldersPath(string path)
		{
			string parentFolder = string.Empty;
			foreach (string item in SA_PathUtil.GetDirectoriesOutOfPath(path))
			{
				if (!IsDirectoryExists(item))
				{
					string pathDirectoryName = SA_PathUtil.GetPathDirectoryName(item);
					SA_AssetDatabaseProxy.CreateFolder(parentFolder, pathDirectoryName);
				}
				parentFolder = item;
			}
		}
	}
}
