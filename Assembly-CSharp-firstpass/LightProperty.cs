public enum LightProperty
{
	ColorRed,
	ColorGreen,
	ColorBlue,
	ColorRGB,
	ColorAlpha,
	Intensity,
	Range,
	SpotAngle,
	ShadowBias,
	ShadowSoftness_DEPRECATED,
	ShadowSoftnessFade_DEPRECATED,
	ShadowStrength
}
