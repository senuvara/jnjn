using UnityEngine;

public abstract class VisBasePropertyTrigger : VisBaseTrigger
{
	public new static class Defaults
	{
		public const ControllerSourceValue controllerSourceValue = ControllerSourceValue.Current;

		public const float minControllerValue = 0f;

		public const float maxControllerValue = 1f;

		public const float minPropertyValue = -1f;

		public const float maxPropertyValue = 1f;

		public const bool invertValue = false;

		public const bool randomValue = true;

		public const bool limitIncreaseRate = false;

		public const float increaseRate = 1f;

		public const bool limitDecreaseRate = false;

		public const float decreaseRate = 1f;

		public const bool returnToRest = false;

		public const float restValue = 0f;
	}

	public ControllerSourceValue controllerSourceValue;

	public float minControllerValue;

	public float maxControllerValue = 1f;

	public float minPropertyValue = -1f;

	public float maxPropertyValue = 1f;

	public bool invertValue;

	public bool randomValue = true;

	public bool limitIncreaseRate;

	public float increaseRate = 1f;

	public bool limitDecreaseRate;

	public float decreaseRate = 1f;

	public bool returnToRest;

	public float restValue;

	protected float m_fTargetPropertyValue = float.PositiveInfinity;

	protected float m_fCurrentPropertyValue = float.PositiveInfinity;

	public override void Reset()
	{
		base.Reset();
		controllerSourceValue = ControllerSourceValue.Current;
		minControllerValue = 0f;
		maxControllerValue = 1f;
		minPropertyValue = -1f;
		maxPropertyValue = 1f;
		limitIncreaseRate = false;
		increaseRate = 1f;
		limitDecreaseRate = false;
		decreaseRate = 1f;
	}

	public override void Start()
	{
		base.Start();
		maxControllerValue = VisHelper.Validate(maxControllerValue, -1000000f, 1000000f, 1f, this, "maxControllerValue", error: false);
		minControllerValue = VisHelper.Validate(minControllerValue, -1000000f, maxControllerValue, Mathf.Min(0f, maxControllerValue), this, "minControllerValue", error: false);
		maxPropertyValue = VisHelper.Validate(maxPropertyValue, -1000000f, 1000000f, 1f, this, "maxPropertyValue", error: false);
		minPropertyValue = VisHelper.Validate(minPropertyValue, -1000000f, maxPropertyValue, Mathf.Min(-1f, maxPropertyValue), this, "minPropertyValue", error: false);
		increaseRate = VisHelper.Validate(increaseRate, 1E-05f, 10000f, 1f, this, "increaseRate", error: false);
		decreaseRate = VisHelper.Validate(decreaseRate, 1E-05f, 10000f, 1f, this, "decreaseRate", error: false);
		if (returnToRest && !limitIncreaseRate && !limitDecreaseRate)
		{
			Debug.LogWarning("If \"Return to Rest\" is true, you must have \"Limit Increase Rate\" or \"Limit Decrease Rate\" set to true. Resetting it to false!");
			returnToRest = false;
		}
	}

	public override void Update()
	{
		base.Update();
		if (!float.IsPositiveInfinity(m_fCurrentPropertyValue))
		{
			if (m_fCurrentPropertyValue != m_fTargetPropertyValue)
			{
				if (limitIncreaseRate && m_fCurrentPropertyValue < m_fTargetPropertyValue)
				{
					m_fCurrentPropertyValue += increaseRate * Time.deltaTime;
					if (m_fCurrentPropertyValue > m_fTargetPropertyValue)
					{
						m_fCurrentPropertyValue = m_fTargetPropertyValue;
					}
					SetProperty(m_fCurrentPropertyValue);
				}
				else if (limitDecreaseRate && m_fCurrentPropertyValue > m_fTargetPropertyValue)
				{
					m_fCurrentPropertyValue -= decreaseRate * Time.deltaTime;
					if (m_fCurrentPropertyValue < m_fTargetPropertyValue)
					{
						m_fCurrentPropertyValue = m_fTargetPropertyValue;
					}
					SetProperty(m_fCurrentPropertyValue);
				}
				else
				{
					m_fCurrentPropertyValue = m_fTargetPropertyValue;
					SetProperty(m_fCurrentPropertyValue);
				}
			}
			else if (returnToRest && m_fCurrentPropertyValue != restValue)
			{
				m_fTargetPropertyValue = restValue;
			}
		}
		else if (returnToRest)
		{
			m_fCurrentPropertyValue = (m_fTargetPropertyValue = restValue);
			SetProperty(m_fCurrentPropertyValue);
		}
	}

	public override void OnTriggered(float current, float previous, float difference, float adjustedDifference)
	{
		if (randomValue)
		{
			m_fTargetPropertyValue = Random.Range(minPropertyValue, maxPropertyValue);
		}
		else
		{
			float sourceValue = current;
			if (controllerSourceValue == ControllerSourceValue.Previous)
			{
				sourceValue = previous;
			}
			else if (controllerSourceValue == ControllerSourceValue.Difference)
			{
				sourceValue = adjustedDifference;
			}
			m_fTargetPropertyValue = VisHelper.ConvertBetweenRanges(sourceValue, minControllerValue, maxControllerValue, minPropertyValue, maxPropertyValue, invertValue);
		}
		if (float.IsPositiveInfinity(m_fCurrentPropertyValue))
		{
			m_fCurrentPropertyValue = m_fTargetPropertyValue;
			SetProperty(m_fCurrentPropertyValue);
		}
		else if (limitIncreaseRate && m_fCurrentPropertyValue < m_fTargetPropertyValue)
		{
			SetProperty(m_fCurrentPropertyValue);
		}
		else if (limitDecreaseRate && m_fCurrentPropertyValue > m_fTargetPropertyValue)
		{
			SetProperty(m_fCurrentPropertyValue);
		}
		else
		{
			m_fCurrentPropertyValue = m_fTargetPropertyValue;
			SetProperty(m_fCurrentPropertyValue);
		}
	}

	public abstract void SetProperty(float propertyValue);
}
