using UnityEngine;

[AddComponentMenu("Visualizer Studio/Triggers/Light Property Trigger")]
public class VisLightPropertyTrigger : VisBasePropertyTrigger
{
	public new static class Defaults
	{
		public const LightProperty targetProperty = LightProperty.Intensity;
	}

	public LightProperty targetProperty = LightProperty.Intensity;

	public override void Reset()
	{
		base.Reset();
		targetProperty = LightProperty.Intensity;
	}

	public override void Start()
	{
		base.Start();
	}

	public override void SetProperty(float propertyValue)
	{
		VisPropertyHelper.SetLightProperty(GetComponent<Light>(), targetProperty, propertyValue);
	}
}
