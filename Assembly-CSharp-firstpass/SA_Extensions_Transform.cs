using UnityEngine;

public static class SA_Extensions_Transform
{
	public static void SetGlobalScale(this Transform transform, Vector3 globalScale)
	{
		transform.localScale = Vector3.one;
		float x = globalScale.x;
		Vector3 lossyScale = transform.lossyScale;
		float x2 = x / lossyScale.x;
		float y = globalScale.y;
		Vector3 lossyScale2 = transform.lossyScale;
		float y2 = y / lossyScale2.y;
		float z = globalScale.z;
		Vector3 lossyScale3 = transform.lossyScale;
		transform.localScale = new Vector3(x2, y2, z / lossyScale3.z);
	}

	public static void Reset(this Transform t)
	{
		t.localScale = Vector3.one;
		t.localPosition = Vector3.zero;
		t.localRotation = Quaternion.identity;
	}

	public static Transform Clear(this Transform transform)
	{
		if (transform.childCount == 0)
		{
			return transform;
		}
		Transform[] componentsInChildren = transform.GetComponentsInChildren<Transform>();
		Transform[] array = componentsInChildren;
		foreach (Transform transform2 in array)
		{
			if (transform2 != transform && transform2 != null)
			{
				Object.DestroyImmediate(transform2.gameObject);
			}
		}
		return transform;
	}

	public static Transform FindOrCreateChild(this Transform target, string name)
	{
		Transform transform = target.Find(name);
		if (transform == null)
		{
			transform = new GameObject(name).transform;
			transform.parent = target;
			transform.Reset();
		}
		return transform;
	}

	public static Bounds GetRendererBounds(this Transform t)
	{
		return t.gameObject.GetRendererBounds();
	}

	public static Vector3 GetVertex(this Transform t, SA_VertexX x, SA_VertexY y, SA_VertexZ z)
	{
		return t.gameObject.GetVertex(x, y, z);
	}
}
