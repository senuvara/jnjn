using System;

namespace com.taptap.license.v2
{
	public class TapLicense
	{
		public delegate void LicenseEventHandler();

		public delegate void DLCEventHandler(DLCResult result);

		private static TapLicense singleton;

		private volatile bool inited;

		private static object syncRoot = new object();

		private LicenseEventHandler licenseHandler;

		private DLCEventHandler dlcHandler;

		public static TapLicense INSTANCE
		{
			get
			{
				lock (syncRoot)
				{
					if (singleton == null)
					{
						singleton = new TapLicense();
					}
				}
				return singleton;
			}
		}

		public void checkLicense()
		{
			if (!inited)
			{
				inited = true;
				TapListener.init();
			}
		}

		public void checkLicense(LicenseEventHandler handler)
		{
			if (!inited)
			{
				inited = true;
				TapListener.init();
				licenseHandler = handler;
			}
		}

		public void initDLCManager(DLCEventHandler handler)
		{
			if (!inited)
			{
				throw new ArgumentException("TapTap call checkLicense first");
			}
			dlcHandler = handler;
		}

		public void initDLCManager(DLCEventHandler handler, bool checkOnce, string publicKey)
		{
			if (!inited)
			{
				throw new ArgumentException("TapTap call checkLicense first");
			}
			dlcHandler = handler;
		}

		public void queryDLC(string sku)
		{
			if (!inited)
			{
				throw new ArgumentException("TapTap call checkLicense first");
			}
		}

		public void purchaseDLC(string sku)
		{
			if (!inited)
			{
				throw new ArgumentException("TapTap call checkLicense first");
			}
		}

		public void onLicenseSuccess()
		{
			if (licenseHandler != null)
			{
				licenseHandler();
			}
		}

		public void onDLCCallback(DLCResult result)
		{
			if (dlcHandler != null)
			{
				dlcHandler(result);
			}
		}
	}
}
