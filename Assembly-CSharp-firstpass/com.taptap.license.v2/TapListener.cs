using UnityEngine;

namespace com.taptap.license.v2
{
	public class TapListener : MonoBehaviour
	{
		public static volatile bool inited;

		public static void init()
		{
			if (!inited)
			{
				inited = true;
				GameObject gameObject = new GameObject();
				gameObject.name = "TapUnityIab";
				gameObject.AddComponent<TapListener>();
				Object.DontDestroyOnLoad(gameObject);
			}
		}

		private void Start()
		{
		}

		private void Update()
		{
		}

		private void onLicenseSuccess()
		{
			TapLicense.INSTANCE.onLicenseSuccess();
		}

		private void onDLCCallback(string result)
		{
			DLCResult result2 = JsonUtility.FromJson<DLCResult>(result);
			TapLicense.INSTANCE.onDLCCallback(result2);
		}
	}
}
