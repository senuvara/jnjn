using System;

namespace com.taptap.license.v2
{
	[Serializable]
	public class DLCResult
	{
		public string sku;

		public int code;
	}
}
