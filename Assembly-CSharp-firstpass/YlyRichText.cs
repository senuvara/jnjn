using System;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.EventSystems;
using UnityEngine.UI;

[AddComponentMenu("UI/YlyRichText", 11)]
[RequireComponent(typeof(RectTransform))]
public class YlyRichText : MaskableGraphic, ILayoutElement, IPointerClickHandler, IBeginDragHandler, IEndDragHandler, IPointerDownHandler, IPointerUpHandler, IEventSystemHandler
{
	private enum CharType
	{
		Normal,
		UnderWire,
		DeleteWire
	}

	private enum WireType
	{
		UnderWire,
		DeleteWire
	}

	private class CharData
	{
		public CharType cType;

		public UIVertex[] vertices = new UIVertex[4];
	}

	private class AssetData
	{
		public string assetPath;

		public int aType;

		public UnityEngine.Object obj;

		public float x;

		public float y;

		public float width;

		public float height;
	}

	private class WireData
	{
		public WireType wireType;

		public float beginX;

		public float endX;

		public Color32 uColor = Color.blue;

		public int fontSize;
	}

	private class LinkData
	{
		public Rect rect = new Rect(0f, 0f, 0f, 0f);

		public string arg;
	}

	private class LineData
	{
		public float x;

		public float y;

		public float z;

		public float width;

		public float height;

		public float charMaxHeight;

		public float charMinY;

		public float charMaxY;

		public YlyRichText ylyRichText;

		public List<CharData> charDatas = new List<CharData>();

		public List<AssetData> assetDatas = new List<AssetData>();

		public List<LinkData> linkDatas = new List<LinkData>();

		public void AddCharData(char cstr, CharacterInfo ci, float charBeginX, float charEndX, Color32 topColor, Color32 bottomColor)
		{
			int minY = ci.minY;
			int maxY = ci.maxY;
			if ((float)minY < charMinY)
			{
				charMinY = minY;
			}
			if ((float)maxY > charMaxY)
			{
				charMaxY = maxY;
			}
			float num = charMaxY - charMinY;
			if (num > charMaxHeight)
			{
				charMaxHeight = num;
			}
			if (charMaxHeight > height)
			{
				height = charMaxHeight;
			}
			if (cstr != '\n')
			{
				UIVertex uIVertex = default(UIVertex);
				uIVertex.position = new Vector3(charBeginX, maxY, z);
				uIVertex.color = topColor;
				uIVertex.uv0 = ci.uvTopLeft;
				UIVertex uIVertex2 = default(UIVertex);
				uIVertex2.position = new Vector3(charEndX, maxY, z);
				uIVertex2.color = topColor;
				uIVertex2.uv0 = ci.uvTopRight;
				UIVertex uIVertex3 = default(UIVertex);
				uIVertex3.position = new Vector3(charEndX, minY, z);
				uIVertex3.color = bottomColor;
				uIVertex3.uv0 = ci.uvBottomRight;
				UIVertex uIVertex4 = default(UIVertex);
				uIVertex4.position = new Vector3(charBeginX, minY, z);
				uIVertex4.color = bottomColor;
				uIVertex4.uv0 = ci.uvBottomLeft;
				CharData charData = new CharData();
				charData.vertices[0] = uIVertex;
				charData.vertices[1] = uIVertex2;
				charData.vertices[2] = uIVertex3;
				charData.vertices[3] = uIVertex4;
				charDatas.Add(charData);
			}
		}

		public bool AddWireData(WireData wireData)
		{
			if (wireData == null || wireData.beginX > wireData.endX)
			{
				return false;
			}
			ylyRichText.font.GetCharacterInfo('_', out CharacterInfo info, wireData.fontSize, FontStyle.Normal);
			int minY = info.minY;
			int maxY = info.maxY;
			maxY -= minY;
			minY = 0;
			Vector2 uvTopRight = info.uvTopRight;
			float num = uvTopRight.x;
			Vector2 uvBottomLeft = info.uvBottomLeft;
			float num2 = Mathf.Abs(num - uvBottomLeft.x) / 2f;
			UIVertex uIVertex = default(UIVertex);
			uIVertex.position = new Vector3(wireData.beginX, maxY, z);
			uIVertex.color = wireData.uColor;
			Vector2 uvBottomRight = info.uvBottomRight;
			float num3 = uvBottomRight.x + num2;
			Vector2 uvBottomRight2 = info.uvBottomRight;
			uIVertex.uv0 = new Vector2(num3, uvBottomRight2.y);
			UIVertex uIVertex2 = default(UIVertex);
			uIVertex2.position = new Vector3(wireData.endX, maxY, z);
			uIVertex2.color = wireData.uColor;
			Vector2 uvTopRight2 = info.uvTopRight;
			float num4 = uvTopRight2.x - num2;
			Vector2 uvTopRight3 = info.uvTopRight;
			uIVertex2.uv0 = new Vector2(num4, uvTopRight3.y);
			UIVertex uIVertex3 = default(UIVertex);
			uIVertex3.position = new Vector3(wireData.endX, minY, z);
			uIVertex3.color = wireData.uColor;
			Vector2 uvTopLeft = info.uvTopLeft;
			float num5 = uvTopLeft.x - num2;
			Vector2 uvTopLeft2 = info.uvTopLeft;
			uIVertex3.uv0 = new Vector2(num5, uvTopLeft2.y);
			UIVertex uIVertex4 = default(UIVertex);
			uIVertex4.position = new Vector3(wireData.beginX, minY, z);
			uIVertex4.color = wireData.uColor;
			Vector2 uvBottomLeft2 = info.uvBottomLeft;
			float num6 = uvBottomLeft2.x + num2;
			Vector2 uvBottomLeft3 = info.uvBottomLeft;
			uIVertex4.uv0 = new Vector2(num6, uvBottomLeft3.y);
			CharData charData = new CharData();
			if (wireData.wireType == WireType.UnderWire)
			{
				charData.cType = CharType.UnderWire;
			}
			else if (wireData.wireType == WireType.DeleteWire)
			{
				charData.cType = CharType.DeleteWire;
			}
			charData.vertices[0] = uIVertex;
			charData.vertices[1] = uIVertex2;
			charData.vertices[2] = uIVertex3;
			charData.vertices[3] = uIVertex4;
			charDatas.Add(charData);
			return true;
		}

		public void AddAssetData(AssetData assetData, float emoteX)
		{
			if (assetData != null)
			{
				assetData.x = emoteX;
				if (assetData.height > height)
				{
					height = assetData.height;
				}
				assetDatas.Add(assetData);
			}
		}

		public void AddLinkData(LinkData linkData, float endX)
		{
			if (linkData != null)
			{
				linkData.rect.xMax = endX;
				linkDatas.Add(linkData);
			}
		}

		public void OnFinish()
		{
			float num = ylyRichText.rectTransform.rect.width;
			float num2 = ylyRichText.rectTransform.rect.height;
			int count = charDatas.Count;
			float num3 = x;
			float num4 = 0f;
			float num5 = y - height / 2f;
			float num6 = y - height;
			float num7 = charMaxHeight / 2f;
			float num8 = Mathf.Abs(charMinY);
			for (int i = 0; i < count; i++)
			{
				num4 = ((charDatas[i].cType != CharType.UnderWire) ? ((charDatas[i].cType != CharType.DeleteWire) ? (num6 + num8) : (num6 + num7)) : num6);
				charDatas[i].vertices[0].position.x = num3 + charDatas[i].vertices[0].position.x;
				charDatas[i].vertices[1].position.x = num3 + charDatas[i].vertices[1].position.x;
				charDatas[i].vertices[2].position.x = num3 + charDatas[i].vertices[2].position.x;
				charDatas[i].vertices[3].position.x = num3 + charDatas[i].vertices[3].position.x;
				charDatas[i].vertices[0].position.y = num4 + charDatas[i].vertices[0].position.y;
				charDatas[i].vertices[1].position.y = num4 + charDatas[i].vertices[1].position.y;
				charDatas[i].vertices[2].position.y = num4 + charDatas[i].vertices[2].position.y;
				charDatas[i].vertices[3].position.y = num4 + charDatas[i].vertices[3].position.y;
			}
			num4 = num6;
			count = assetDatas.Count;
			for (int j = 0; j < count; j++)
			{
				assetDatas[j].x = num3 + assetDatas[j].x + assetDatas[j].width / 2f;
				assetDatas[j].y = num4 + assetDatas[j].height / 2f;
			}
			num4 = num6;
			count = linkDatas.Count;
			for (int k = 0; k < count; k++)
			{
				linkDatas[k].rect.xMin = num3 + linkDatas[k].rect.xMin;
				linkDatas[k].rect.xMax = num3 + linkDatas[k].rect.xMax;
				linkDatas[k].rect.yMin = num4;
				linkDatas[k].rect.yMax = num4 + charMaxHeight;
			}
		}

		public void OnPopulateCharMesh(ref VertexHelper vh, ref int j)
		{
			int count = charDatas.Count;
			bool flag = false;
			Vector3[] array = new Vector3[4]
			{
				new Vector3(1f, 1f, 0f),
				new Vector3(1f, -1f, 0f),
				new Vector3(-1f, -1f, 0f),
				new Vector3(-1f, 1f, 0f)
			};
			for (int i = 0; i < count; i++)
			{
				flag = false;
				if (charDatas[i].cType == CharType.Normal)
				{
					flag = true;
				}
				if (ylyRichText.isNeedOutLine && flag)
				{
					for (int k = 0; k < 4; k++)
					{
						vh.AddVert(charDatas[i].vertices[0].position + array[k], ylyRichText.outLineColor, charDatas[i].vertices[0].uv0);
						vh.AddVert(charDatas[i].vertices[1].position + array[k], ylyRichText.outLineColor, charDatas[i].vertices[1].uv0);
						vh.AddVert(charDatas[i].vertices[2].position + array[k], ylyRichText.outLineColor, charDatas[i].vertices[2].uv0);
						vh.AddVert(charDatas[i].vertices[3].position + array[k], ylyRichText.outLineColor, charDatas[i].vertices[3].uv0);
						vh.AddTriangle(4 * j, 4 * j + 1, 4 * j + 2);
						vh.AddTriangle(4 * j, 4 * j + 2, 4 * j + 3);
						j++;
					}
				}
				vh.AddVert(charDatas[i].vertices[0].position, charDatas[i].vertices[0].color, charDatas[i].vertices[0].uv0);
				vh.AddVert(charDatas[i].vertices[1].position, charDatas[i].vertices[1].color, charDatas[i].vertices[1].uv0);
				vh.AddVert(charDatas[i].vertices[2].position, charDatas[i].vertices[2].color, charDatas[i].vertices[2].uv0);
				vh.AddVert(charDatas[i].vertices[3].position, charDatas[i].vertices[3].color, charDatas[i].vertices[3].uv0);
				vh.AddTriangle(4 * j, 4 * j + 1, 4 * j + 2);
				vh.AddTriangle(4 * j, 4 * j + 2, 4 * j + 3);
				j++;
			}
		}
	}

	private class CalcCharData
	{
		public int i;

		public int blockType;

		public int blockLen;

		public float charX;

		public float charW;

		public int lastBlockType = -1;

		public Color sclr;

		public Color eclr;

		public float advanceX;

		public float offcharX;

		public FontStyle fontStyle;

		public int fontSize;

		public bool isSpaceOrTabChar;

		public WireData underWireData;

		public WireData deleteWireData;

		public AssetData assetData;

		public LinkData linkData;

		public LineData linedata;
	}

	public YlyDelegateUtil.StringDelegate onLinkClick;

	public static string emotePngPathFormat = "Assets/YlyRichText/Atlas/i{0}.png";

	public static string animEmotePrefabPathFormat = "Assets/YlyRichText/Prefabs/i{0}.prefab";

	public static string cIconPngPathFormat = "Assets/YlyRichText/Atlas/Icon/{0}.png";

	[TextArea(3, 10)]
	[SerializeField]
	[Tooltip("文本内容")]
	private string m_Text = string.Empty;

	[SerializeField]
	[Tooltip("字体")]
	private static Font m_Font;

	[SerializeField]
	[Tooltip("字体大小")]
	private int m_FontSize = 28;

	[SerializeField]
	[Tooltip("行高")]
	private float m_LineHeght = 40f;

	[SerializeField]
	[Tooltip("行之间间隔")]
	private float m_LineSpacing;

	[SerializeField]
	[Tooltip("字符之间的横向间隔")]
	private float m_OffCharX = 8f;

	[SerializeField]
	[Tooltip("是否自定义宽度自动换行")]
	private bool m_IsCustomWidthToNewLine;

	[SerializeField]
	[Tooltip("自定义宽度自动换行")]
	private float m_CustomWidthToNewLine = 100f;

	[SerializeField]
	[Tooltip("是否需要描边，注意：描边会比原来多4倍顶点，unity限制每个mesh最多65000个顶点，所以，能尽量不用就不用吧")]
	private bool m_IsNeedOutLine;

	[SerializeField]
	[Tooltip("描边颜色")]
	private Color32 m_OutLineColor = Color.black;

	[SerializeField]
	[Tooltip("颜色")]
	private Color32 m_TColor = Color.black;

	[SerializeField]
	[Tooltip("是否自适应宽高")]
	private bool m_IsAutoAdaptiveWidthHeight;

	[SerializeField]
	[Tooltip("横向Wrap模式")]
	private HorizontalWrapMode m_HorizontalOverflow;

	[SerializeField]
	[Tooltip("纵向Wrap模式")]
	private VerticalWrapMode m_VerticalOverflow;

	[SerializeField]
	[Tooltip("对齐方式")]
	private TextAnchor m_Alignment;

	[SerializeField]
	[Tooltip("是否启用rich text模式")]
	private bool m_EnableRichText = true;

	private List<LineData> m_Lines = new List<LineData>();

	private Dictionary<string, AssetData> m_AssetDataDict = new Dictionary<string, AssetData>();

	private float m_PreferredHeight;

	private float m_PreferredWidth;

	private int[] m_StrMask = new int[0];

	private Dictionary<int, string> m_ParamDict = new Dictionary<int, string>();

	private Dictionary<string, int> m_PreLoadAssetDic = new Dictionary<string, int>();

	private string m_ParsedText = string.Empty;

	private string m_RefHeightGoName = "YlyRichTextRefHeightGo";

	private bool m_IsFontTextureDirty;

	private bool m_IsPopulateMeshDone;

	private bool m_IsDrag;

	public Font font
	{
		get
		{
			return m_Font;
		}
		set
		{
			m_Font = value;
			UpdateLines();
		}
	}

	public override Color color
	{
		get
		{
			return m_TColor;
		}
		set
		{
			m_TColor = value;
			ParseText();
			UpdateLines();
		}
	}

	public string text
	{
		get
		{
			return m_Text;
		}
		set
		{
			m_Text = value;
			ParseText();
			UpdateLines();
			LayoutRebuilder.ForceRebuildLayoutImmediate(base.transform.parent.GetComponent<RectTransform>());
		}
	}

	public int fontSize
	{
		get
		{
			return m_FontSize;
		}
		set
		{
			m_FontSize = value;
			if (m_FontSize <= 0)
			{
				m_FontSize = 1;
			}
			UpdateLines();
		}
	}

	public float lineHeght
	{
		get
		{
			return m_LineHeght;
		}
		set
		{
			m_LineHeght = value;
			UpdateLines();
		}
	}

	public float lineSpacing
	{
		get
		{
			return m_LineSpacing;
		}
		set
		{
			m_LineSpacing = value;
			UpdateLines();
		}
	}

	public float offCharX
	{
		get
		{
			return m_OffCharX;
		}
		set
		{
			m_OffCharX = value;
			UpdateLines();
		}
	}

	public bool isAutoAdaptiveWidthHeight
	{
		get
		{
			return m_IsAutoAdaptiveWidthHeight;
		}
		set
		{
			m_IsAutoAdaptiveWidthHeight = value;
			UpdateLines();
		}
	}

	public bool isCustomWidthToNewLine
	{
		get
		{
			return m_IsCustomWidthToNewLine;
		}
		set
		{
			m_IsCustomWidthToNewLine = value;
			UpdateLines();
		}
	}

	public float customWidthToNewLine
	{
		get
		{
			return m_CustomWidthToNewLine;
		}
		set
		{
			m_CustomWidthToNewLine = value;
			UpdateLines();
		}
	}

	public bool isNeedOutLine
	{
		get
		{
			return m_IsNeedOutLine;
		}
		set
		{
			m_IsNeedOutLine = value;
			UpdateLines();
		}
	}

	public Color32 outLineColor
	{
		get
		{
			return m_OutLineColor;
		}
		set
		{
			m_OutLineColor = value;
			if (m_IsNeedOutLine)
			{
				UpdateLines();
			}
		}
	}

	public HorizontalWrapMode horizontalOverflow
	{
		get
		{
			return m_HorizontalOverflow;
		}
		set
		{
			m_HorizontalOverflow = value;
			UpdateLines();
		}
	}

	public VerticalWrapMode verticalOverflow
	{
		get
		{
			return m_VerticalOverflow;
		}
		set
		{
			m_VerticalOverflow = value;
			UpdateLines();
		}
	}

	public bool enableRichText
	{
		get
		{
			return m_EnableRichText;
		}
		set
		{
			m_EnableRichText = value;
			ParseText();
			UpdateLines();
		}
	}

	public TextAnchor alignment
	{
		get
		{
			return m_Alignment;
		}
		set
		{
			m_Alignment = value;
			UpdateLines();
		}
	}

	public override Texture mainTexture
	{
		get
		{
			if (m_Font != null && m_Font.material != null && m_Font.material.mainTexture != null)
			{
				return m_Font.material.mainTexture;
			}
			if (m_Material != null)
			{
				return m_Material.mainTexture;
			}
			return base.mainTexture;
		}
	}

	public virtual float flexibleHeight => -1f;

	public virtual float flexibleWidth => -1f;

	public virtual int layoutPriority => 0;

	public virtual float minHeight => 0f;

	public virtual float minWidth => 0f;

	public virtual float preferredHeight => m_PreferredHeight;

	public virtual float preferredWidth => m_PreferredWidth;

	protected YlyRichText()
	{
		base.useLegacyMeshGeneration = false;
	}

	public virtual void CalculateLayoutInputHorizontal()
	{
	}

	public virtual void CalculateLayoutInputVertical()
	{
	}

	protected override void Start()
	{
		base.Start();
		if (m_Font == null)
		{
			m_Font = (Resources.GetBuiltinResource(typeof(Font), "Arial.ttf") as Font);
		}
		Font.textureRebuilt += FontTextureRebuilt;
		text = m_Text;
	}

	public void OnPointerClick(PointerEventData eventData)
	{
		if (onLinkClick == null || eventData == null || m_IsDrag)
		{
			return;
		}
		RectTransformUtility.ScreenPointToLocalPointInRectangle(base.rectTransform, eventData.position, Camera.main, out Vector2 localPoint);
		for (int i = 0; i < m_Lines.Count; i++)
		{
			int count = m_Lines[i].linkDatas.Count;
			for (int j = 0; j < count; j++)
			{
				if (m_Lines[i].linkDatas[j].rect.Contains(localPoint))
				{
					onLinkClick(m_Lines[i].linkDatas[j].arg);
					return;
				}
			}
		}
	}

	public void OnPointerDown(PointerEventData eventData)
	{
	}

	public void OnPointerUp(PointerEventData eventData)
	{
	}

	public void OnBeginDrag(PointerEventData eventData)
	{
		m_IsDrag = true;
	}

	public void OnEndDrag(PointerEventData eventData)
	{
		m_IsDrag = false;
	}

	protected override void OnPopulateMesh(VertexHelper vh)
	{
		vh.Clear();
		int j = 0;
		for (int i = 0; i < m_Lines.Count; i++)
		{
			m_Lines[i].OnPopulateCharMesh(ref vh, ref j);
		}
		m_IsPopulateMeshDone = true;
	}

	protected override void OnEnable()
	{
		base.OnEnable();
	}

	protected override void OnDisable()
	{
		base.OnDisable();
	}

	protected override void OnRectTransformDimensionsChange()
	{
		UpdateLines();
		base.OnRectTransformDimensionsChange();
	}

	private bool CalcColorBegin(CalcCharData ccData)
	{
		if (m_ParamDict.ContainsKey(ccData.i))
		{
			string text = m_ParamDict[ccData.i];
			if (ccData.blockLen == 16)
			{
				float r = (float)Convert.ToInt16(text.Substring(0, 2), 16) / 255f;
				float g = (float)Convert.ToInt16(text.Substring(2, 2), 16) / 255f;
				float b = (float)Convert.ToInt16(text.Substring(4, 2), 16) / 255f;
				float a = (float)Convert.ToInt16(text.Substring(6, 2), 16) / 255f;
				ccData.sclr = (ccData.eclr = new Color(r, g, b, a));
			}
			else if (ccData.blockLen == 24)
			{
				float r2 = (float)Convert.ToInt16(text.Substring(0, 2), 16) / 255f;
				float g2 = (float)Convert.ToInt16(text.Substring(2, 2), 16) / 255f;
				float b2 = (float)Convert.ToInt16(text.Substring(4, 2), 16) / 255f;
				float a2 = (float)Convert.ToInt16(text.Substring(6, 2), 16) / 255f;
				ccData.sclr = new Color(r2, g2, b2, a2);
				float r3 = (float)Convert.ToInt16(text.Substring(8, 2), 16) / 255f;
				float g3 = (float)Convert.ToInt16(text.Substring(10, 2), 16) / 255f;
				float b3 = (float)Convert.ToInt16(text.Substring(12, 2), 16) / 255f;
				float a3 = (float)Convert.ToInt16(text.Substring(14, 2), 16) / 255f;
				ccData.eclr = new Color(r3, g3, b3, a3);
			}
			if (ccData.underWireData != null)
			{
				ccData.underWireData.endX = ccData.charX - m_OffCharX;
				if (ccData.linedata.AddWireData(ccData.underWireData))
				{
					ccData.underWireData = new WireData();
					ccData.underWireData.beginX = ccData.charX - m_OffCharX;
				}
				else
				{
					ccData.underWireData = new WireData();
					ccData.underWireData.beginX = ccData.charX;
				}
				ccData.underWireData.uColor = ccData.sclr;
			}
			ccData.i += ccData.blockLen - 1;
			ccData.lastBlockType = ccData.blockType;
			return true;
		}
		return false;
	}

	private void CalcColorEnd(CalcCharData ccData)
	{
		ccData.sclr = (ccData.eclr = m_TColor);
		if (ccData.underWireData != null)
		{
			ccData.underWireData.endX = ccData.charX - m_OffCharX;
			if (ccData.linedata.AddWireData(ccData.underWireData))
			{
				ccData.underWireData = new WireData();
				ccData.underWireData.beginX = ccData.charX - m_OffCharX;
			}
			else
			{
				ccData.underWireData = new WireData();
				ccData.underWireData.beginX = ccData.charX;
			}
			ccData.underWireData.uColor = ccData.sclr;
		}
		ccData.i += ccData.blockLen - 1;
		ccData.lastBlockType = ccData.blockType;
	}

	private void CalcUnderLineBegin(CalcCharData ccData)
	{
		ccData.underWireData = new WireData();
		ccData.underWireData.fontSize = ccData.fontSize;
		ccData.underWireData.beginX = ccData.charX;
		ccData.underWireData.uColor = ccData.sclr;
		ccData.i += ccData.blockLen - 1;
		ccData.lastBlockType = ccData.blockType;
	}

	private bool CalcUnderLineEnd(CalcCharData ccData)
	{
		if (ccData.underWireData != null)
		{
			ccData.underWireData.endX = ccData.charX - m_OffCharX;
			ccData.linedata.AddWireData(ccData.underWireData);
			ccData.underWireData = null;
			ccData.i += ccData.blockLen - 1;
			ccData.lastBlockType = ccData.blockType;
			return true;
		}
		return false;
	}

	private void CalcDelLineBegin(CalcCharData ccData)
	{
		ccData.deleteWireData = new WireData();
		ccData.deleteWireData.fontSize = ccData.fontSize;
		ccData.deleteWireData.wireType = WireType.DeleteWire;
		ccData.deleteWireData.beginX = ccData.charX;
		ccData.deleteWireData.uColor = m_TColor;
		ccData.i += ccData.blockLen - 1;
		ccData.lastBlockType = ccData.blockType;
	}

	private void CalcDelLineEnd(CalcCharData ccData)
	{
		ccData.deleteWireData.endX = ccData.charX - m_OffCharX;
		ccData.linedata.AddWireData(ccData.deleteWireData);
		ccData.deleteWireData = null;
		ccData.i += ccData.blockLen - 1;
		ccData.lastBlockType = ccData.blockType;
	}

	private bool CalcLinkBegin(CalcCharData ccData)
	{
		if (m_ParamDict.ContainsKey(ccData.i))
		{
			string arg = m_ParamDict[ccData.i];
			ccData.linkData = new LinkData();
			ccData.linkData.arg = arg;
			ccData.linkData.rect.xMin = ccData.charX;
			ccData.i += ccData.blockLen - 1;
			ccData.lastBlockType = ccData.blockType;
			return true;
		}
		return false;
	}

	private bool CalcLinkEnd(CalcCharData ccData)
	{
		if (ccData.linkData != null)
		{
			ccData.linedata.AddLinkData(ccData.linkData, ccData.charX - m_OffCharX);
			ccData.linkData = null;
			ccData.sclr = (ccData.eclr = m_TColor);
			ccData.i += ccData.blockLen - 1;
			ccData.lastBlockType = ccData.blockType;
			return true;
		}
		return false;
	}

	private void CalcBoldBegin(CalcCharData ccData)
	{
		if (ccData.fontStyle == FontStyle.Italic)
		{
			ccData.fontStyle = FontStyle.BoldAndItalic;
		}
		else
		{
			ccData.fontStyle = FontStyle.Bold;
		}
		ccData.i += ccData.blockLen - 1;
		ccData.lastBlockType = ccData.blockType;
	}

	private void CalcBoldEnd(CalcCharData ccData)
	{
		if (ccData.fontStyle == FontStyle.BoldAndItalic || ccData.fontStyle == FontStyle.Italic)
		{
			ccData.fontStyle = FontStyle.Italic;
		}
		else
		{
			ccData.fontStyle = FontStyle.Normal;
		}
		ccData.i += ccData.blockLen - 1;
		ccData.lastBlockType = ccData.blockType;
	}

	private void CalcItalicBegin(CalcCharData ccData)
	{
		if (ccData.fontStyle == FontStyle.Bold)
		{
			ccData.fontStyle = FontStyle.BoldAndItalic;
		}
		else
		{
			ccData.fontStyle = FontStyle.Italic;
		}
		ccData.i += ccData.blockLen - 1;
		ccData.lastBlockType = ccData.blockType;
	}

	private void CalcItalicEnd(CalcCharData ccData)
	{
		if (ccData.fontStyle == FontStyle.BoldAndItalic || ccData.fontStyle == FontStyle.Bold)
		{
			ccData.fontStyle = FontStyle.Bold;
		}
		else
		{
			ccData.fontStyle = FontStyle.Normal;
		}
		ccData.i += ccData.blockLen - 1;
		ccData.lastBlockType = ccData.blockType;
	}

	private bool CalcFontSizeBegin(CalcCharData ccData)
	{
		int result = -1;
		if (m_ParamDict.ContainsKey(ccData.i) && int.TryParse(m_ParamDict[ccData.i], out result))
		{
			ccData.fontSize = result;
			ccData.i += ccData.blockLen - 1;
			ccData.lastBlockType = ccData.blockType;
			return true;
		}
		return false;
	}

	private void CalcFontSizeEnd(CalcCharData ccData)
	{
		ccData.fontSize = m_FontSize;
		ccData.i += ccData.blockLen - 1;
		ccData.lastBlockType = ccData.blockType;
	}

	private void CalcEmote(CalcCharData ccData, ref CharacterInfo ci)
	{
		int result = -1;
		bool flag = false;
		if (m_ParamDict.ContainsKey(ccData.i) && int.TryParse(m_ParamDict[ccData.i], out result))
		{
			if (YlyRichTextParser.GetEmoteType(result) == 10100)
			{
				string text = string.Format(animEmotePrefabPathFormat, m_ParamDict[ccData.i]);
				if (m_AssetDataDict.ContainsKey(text))
				{
					ccData.assetData = new AssetData();
					ccData.assetData.assetPath = text;
					ccData.assetData.aType = 10100;
					ccData.assetData.obj = m_AssetDataDict[text].obj;
					ccData.assetData.width = m_AssetDataDict[text].width;
					ccData.assetData.height = m_AssetDataDict[text].height;
					ccData.charW = ccData.assetData.width;
					flag = true;
				}
			}
			else
			{
				string text2 = string.Format(emotePngPathFormat, m_ParamDict[ccData.i]);
				if (m_AssetDataDict.ContainsKey(text2))
				{
					ccData.assetData = new AssetData();
					ccData.assetData.assetPath = text2;
					ccData.assetData.aType = 10000;
					ccData.assetData.obj = m_AssetDataDict[text2].obj;
					ccData.assetData.width = m_AssetDataDict[text2].width;
					ccData.assetData.height = m_AssetDataDict[text2].height;
					ccData.charW = ccData.assetData.width;
					flag = true;
				}
			}
		}
		if (!flag)
		{
			ccData.charW = ci.advance;
		}
	}

	private void CalcPic(CalcCharData ccData, ref CharacterInfo ci)
	{
		if (m_ParamDict.ContainsKey(ccData.i) && m_AssetDataDict.ContainsKey(m_ParamDict[ccData.i]))
		{
			string text = m_ParamDict[ccData.i];
			ccData.assetData = new AssetData();
			ccData.assetData.assetPath = text;
			ccData.assetData.aType = 10200;
			ccData.assetData.obj = m_AssetDataDict[text].obj;
			ccData.assetData.width = m_AssetDataDict[text].width;
			ccData.assetData.height = m_AssetDataDict[text].height;
			ccData.charW = ccData.assetData.width;
		}
		else
		{
			ccData.charW = ci.advance;
		}
	}

	private void CalcCIcon(CalcCharData ccData, ref CharacterInfo ci)
	{
		bool flag = false;
		if (m_ParamDict.ContainsKey(ccData.i))
		{
			string text = string.Format(cIconPngPathFormat, m_ParamDict[ccData.i]);
			if (m_AssetDataDict.ContainsKey(text))
			{
				ccData.assetData = new AssetData();
				ccData.assetData.assetPath = text;
				ccData.assetData.aType = 10300;
				ccData.assetData.obj = m_AssetDataDict[text].obj;
				ccData.assetData.width = m_AssetDataDict[text].width;
				ccData.assetData.height = m_AssetDataDict[text].height;
				ccData.charW = ccData.assetData.width;
				flag = true;
			}
		}
		if (!flag)
		{
			ccData.charW = ci.advance;
		}
	}

	private void CalcNormalChar(CalcCharData ccData, ref CharacterInfo ci)
	{
		if (m_ParsedText[ccData.i] == ' ' || m_ParsedText[ccData.i] == '\t')
		{
			ccData.isSpaceOrTabChar = true;
		}
		else
		{
			ccData.isSpaceOrTabChar = false;
		}
		ccData.charW = ci.advance;
	}

	private bool CalcNewLine(CalcCharData ccData, int strLen, ref float totalWidth, ref float totalHeight)
	{
		float num = base.rectTransform.rect.width;
		if (m_IsCustomWidthToNewLine)
		{
			num = m_CustomWidthToNewLine;
		}
		if ((m_HorizontalOverflow == HorizontalWrapMode.Wrap && totalWidth - m_OffCharX >= num) || m_ParsedText[ccData.i] == '\n')
		{
			ccData.linedata.width = Mathf.Max(totalWidth - ccData.advanceX - m_OffCharX, 0f);
			if (ccData.underWireData != null)
			{
				Color32 uColor = ccData.underWireData.uColor;
				ccData.underWireData.endX = ccData.charX - m_OffCharX;
				ccData.linedata.AddWireData(ccData.underWireData);
				ccData.underWireData = new WireData();
				ccData.underWireData.fontSize = ccData.fontSize;
				ccData.underWireData.beginX = 0f;
				ccData.underWireData.uColor = uColor;
			}
			if (ccData.deleteWireData != null)
			{
				Color32 uColor2 = ccData.deleteWireData.uColor;
				ccData.deleteWireData.endX = ccData.charX - m_OffCharX;
				ccData.linedata.AddWireData(ccData.deleteWireData);
				ccData.deleteWireData = new WireData();
				ccData.deleteWireData.fontSize = ccData.fontSize;
				ccData.deleteWireData.wireType = WireType.DeleteWire;
				ccData.deleteWireData.beginX = 0f;
				ccData.deleteWireData.uColor = uColor2;
			}
			if (ccData.linkData != null)
			{
				string arg = ccData.linkData.arg;
				ccData.linedata.AddLinkData(ccData.linkData, ccData.charX - m_OffCharX);
				ccData.linkData = new LinkData();
				ccData.linkData.arg = arg;
				ccData.linkData.rect.xMin = 0f;
			}
			totalHeight += ccData.linedata.height;
			if (m_VerticalOverflow == VerticalWrapMode.Truncate && totalHeight > base.rectTransform.rect.height)
			{
				return true;
			}
			totalHeight += m_LineSpacing;
			if (ccData.linedata.width > m_PreferredWidth)
			{
				m_PreferredWidth = ccData.linedata.width;
			}
			ccData.charX = 0f;
			if (m_ParsedText[ccData.i] == '\n')
			{
				totalWidth = 0f;
				ccData.offcharX = 0f;
				ccData.charW = 0f;
			}
			else
			{
				totalWidth = ccData.advanceX;
			}
			m_Lines.Add(ccData.linedata);
			if (ccData.i < strLen - 1)
			{
				ccData.linedata = new LineData();
				ccData.linedata.ylyRichText = this;
				ccData.linedata.height = m_LineHeght;
			}
		}
		return false;
	}

	private void CalcAlign()
	{
		Vector2 pivot = base.rectTransform.pivot;
		float num = (0f - pivot.x) * base.rectTransform.rect.width;
		Vector2 pivot2 = base.rectTransform.pivot;
		float num2 = (1f - pivot2.y) * base.rectTransform.rect.height;
		float x = 0f;
		float num3 = 0f;
		switch (m_Alignment)
		{
		case TextAnchor.UpperLeft:
		case TextAnchor.UpperCenter:
		case TextAnchor.UpperRight:
			num3 = num2;
			break;
		case TextAnchor.MiddleLeft:
		case TextAnchor.MiddleCenter:
		case TextAnchor.MiddleRight:
			num3 = Mathf.Min(num2, num2 - (base.rectTransform.rect.height - m_PreferredHeight) / 2f);
			break;
		case TextAnchor.LowerLeft:
		case TextAnchor.LowerCenter:
		case TextAnchor.LowerRight:
			num3 = Mathf.Min(num2, num2 - (base.rectTransform.rect.height - m_PreferredHeight));
			break;
		}
		for (int i = 0; i < m_Lines.Count; i++)
		{
			switch (m_Alignment)
			{
			case TextAnchor.UpperLeft:
			case TextAnchor.MiddleLeft:
			case TextAnchor.LowerLeft:
				x = num;
				break;
			case TextAnchor.UpperCenter:
			case TextAnchor.MiddleCenter:
			case TextAnchor.LowerCenter:
				x = num + (base.rectTransform.rect.width - m_Lines[i].width) / 2f;
				break;
			case TextAnchor.UpperRight:
			case TextAnchor.MiddleRight:
			case TextAnchor.LowerRight:
				x = num + (base.rectTransform.rect.width - m_Lines[i].width);
				break;
			}
			m_Lines[i].x = x;
			m_Lines[i].y = num3;
			m_Lines[i].OnFinish();
			num3 = num3 - m_Lines[i].height - m_LineSpacing;
		}
	}

	private void UpdateLines()
	{
		m_Lines.Clear();
		m_PreferredWidth = 0f;
		m_PreferredHeight = 0f;
		if (m_Font == null)
		{
			return;
		}
		float totalWidth = 0f;
		float totalHeight = 0f;
		int length = m_ParsedText.Length;
		m_Font.RequestCharactersInTexture("▇_", m_FontSize, FontStyle.Normal);
		CalcCharData calcCharData = new CalcCharData();
		calcCharData.fontSize = m_FontSize;
		calcCharData.fontStyle = FontStyle.Normal;
		calcCharData.sclr = m_TColor;
		calcCharData.eclr = m_TColor;
		if (length > 0)
		{
			calcCharData.linedata = new LineData();
			calcCharData.linedata.ylyRichText = this;
			calcCharData.linedata.height = m_LineHeght;
		}
		calcCharData.i = 0;
		for (; calcCharData.i < length; calcCharData.i++)
		{
			calcCharData.blockType = m_StrMask[calcCharData.i] / 100 * 100;
			calcCharData.blockLen = m_StrMask[calcCharData.i] - calcCharData.blockType;
			if (calcCharData.blockType == 20000)
			{
				if (CalcColorBegin(calcCharData))
				{
					continue;
				}
			}
			else
			{
				if (calcCharData.blockType == 20400)
				{
					CalcColorEnd(calcCharData);
					continue;
				}
				if (calcCharData.blockType == 20200)
				{
					CalcUnderLineBegin(calcCharData);
					continue;
				}
				if (calcCharData.blockType == 20600)
				{
					if (CalcUnderLineEnd(calcCharData))
					{
						continue;
					}
				}
				else
				{
					if (calcCharData.blockType == 21300)
					{
						CalcDelLineBegin(calcCharData);
						continue;
					}
					if (calcCharData.blockType == 21400)
					{
						CalcDelLineEnd(calcCharData);
						continue;
					}
					if (calcCharData.blockType == 20100)
					{
						if (CalcLinkBegin(calcCharData))
						{
							continue;
						}
					}
					else if (calcCharData.blockType == 20300)
					{
						if (CalcLinkEnd(calcCharData))
						{
							continue;
						}
					}
					else
					{
						if (calcCharData.blockType == 20700)
						{
							CalcBoldBegin(calcCharData);
							continue;
						}
						if (calcCharData.blockType == 20800)
						{
							CalcBoldEnd(calcCharData);
							continue;
						}
						if (calcCharData.blockType == 20900)
						{
							CalcItalicBegin(calcCharData);
							continue;
						}
						if (calcCharData.blockType == 21000)
						{
							CalcItalicEnd(calcCharData);
							continue;
						}
						if (calcCharData.blockType == 21100)
						{
							if (CalcFontSizeBegin(calcCharData))
							{
								continue;
							}
						}
						else if (calcCharData.blockType == 21200)
						{
							CalcFontSizeEnd(calcCharData);
							continue;
						}
					}
				}
			}
			m_Font.RequestCharactersInTexture(m_ParsedText[calcCharData.i].ToString(), calcCharData.fontSize, calcCharData.fontStyle);
			m_Font.GetCharacterInfo(m_ParsedText[calcCharData.i], out CharacterInfo info, calcCharData.fontSize, calcCharData.fontStyle);
			if (calcCharData.blockType == 10000)
			{
				CalcEmote(calcCharData, ref info);
			}
			else if (calcCharData.blockType == 10200)
			{
				CalcPic(calcCharData, ref info);
			}
			else if (calcCharData.blockType == 10300)
			{
				CalcCIcon(calcCharData, ref info);
			}
			else
			{
				CalcNormalChar(calcCharData, ref info);
			}
			calcCharData.advanceX = calcCharData.charW + m_OffCharX;
			calcCharData.offcharX = m_OffCharX;
			totalWidth += calcCharData.advanceX;
			if (CalcNewLine(calcCharData, length, ref totalWidth, ref totalHeight))
			{
				break;
			}
			if (calcCharData.assetData != null)
			{
				calcCharData.linedata.AddAssetData(calcCharData.assetData, calcCharData.charX);
				calcCharData.assetData = null;
				calcCharData.i += calcCharData.blockLen - 1;
				calcCharData.lastBlockType = calcCharData.blockType;
			}
			else
			{
				if (!calcCharData.isSpaceOrTabChar)
				{
					calcCharData.linedata.AddCharData(m_ParsedText[calcCharData.i], info, calcCharData.charX + (float)info.minX, calcCharData.charX + (float)info.maxX, calcCharData.sclr, calcCharData.eclr);
				}
				calcCharData.lastBlockType = 0;
			}
			calcCharData.charX += calcCharData.charW + calcCharData.offcharX;
		}
		m_PreferredHeight = Mathf.Max(totalHeight - m_LineSpacing, 0f);
		CalcAlign();
		calcCharData = null;
		SetVerticesDirty();
	}

	private void FontTextureRebuilt(Font changedFont)
	{
		if (!(m_Font == null) && !(m_Font != changedFont))
		{
			m_IsFontTextureDirty = true;
		}
	}

	private void PreLoadAsset()
	{
		m_AssetDataDict.Clear();
		float num = 0f;
		float num2 = 0f;
		RectTransform rectTransform = (RectTransform)base.transform.Find(m_RefHeightGoName);
		List<string> list = new List<string>(m_PreLoadAssetDic.Keys);
		for (int i = 0; i < list.Count; i++)
		{
			AssetData assetData = null;
			if (m_PreLoadAssetDic[list[i]] == 10100)
			{
				GameObject gameObject = LoadAsset(list[i], typeof(GameObject)) as GameObject;
				if (gameObject == null)
				{
					continue;
				}
				Vector2 sizeDelta = gameObject.GetComponent<RectTransform>().sizeDelta;
				num = sizeDelta.x;
				Vector2 sizeDelta2 = gameObject.GetComponent<RectTransform>().sizeDelta;
				num2 = sizeDelta2.y;
				assetData = new AssetData();
				assetData.obj = gameObject;
			}
			else if (m_PreLoadAssetDic[list[i]] == 10000 || m_PreLoadAssetDic[list[i]] == 10200 || m_PreLoadAssetDic[list[i]] == 10300)
			{
				Sprite sprite = LoadAsset(list[i], typeof(Sprite)) as Sprite;
				if (sprite == null)
				{
					continue;
				}
				num = sprite.rect.width;
				num2 = sprite.rect.height;
				assetData = new AssetData();
				assetData.obj = sprite;
			}
			if (assetData != null && !m_AssetDataDict.ContainsKey(list[i]))
			{
				assetData.assetPath = list[i];
				assetData.width = num;
				assetData.height = num2;
				if (rectTransform != null)
				{
					AssetData assetData2 = assetData;
					Vector2 sizeDelta3 = rectTransform.sizeDelta;
					assetData2.height = sizeDelta3.y;
					assetData.width = assetData.height / num2 * num;
				}
				m_AssetDataDict.Add(list[i], assetData);
			}
		}
	}

	private void ParseText()
	{
		if (string.IsNullOrEmpty(m_Text))
		{
			m_ParsedText = string.Empty;
			return;
		}
		m_ParsedText = YlyRichTextParser.Parse(m_Text, out m_StrMask, out m_ParamDict, out m_PreLoadAssetDic, m_EnableRichText);
		PreLoadAsset();
	}

	private void UpdateAssetGo()
	{
		ClearAssetGo();
		int num = 0;
		GameObject gameObject = null;
		for (int i = 0; i < m_Lines.Count; i++)
		{
			num = m_Lines[i].assetDatas.Count;
			for (int j = 0; j < num; j++)
			{
				if (m_Lines[i].assetDatas[j].aType == 10100)
				{
					gameObject = (UnityEngine.Object.Instantiate(m_Lines[i].assetDatas[j].obj) as GameObject);
				}
				else if (m_Lines[i].assetDatas[j].aType == 10000 || m_Lines[i].assetDatas[j].aType == 10200 || m_Lines[i].assetDatas[j].aType == 10300)
				{
					gameObject = new GameObject();
					gameObject.AddComponent<RectTransform>();
					Image image = gameObject.AddComponent<Image>();
					image.sprite = (m_Lines[i].assetDatas[j].obj as Sprite);
				}
				if (gameObject != null)
				{
					gameObject.AddComponent<YlyAssetIdentify>();
					gameObject.transform.SetParent(base.transform);
					RectTransform component = gameObject.GetComponent<RectTransform>();
					component.pivot = new Vector2(0.5f, 0.5f);
					component.anchorMax = new Vector2(0f, 1f);
					component.anchorMin = new Vector2(0f, 1f);
					component.localScale = new Vector3(1f, 1f, 1f);
					component.localPosition = new Vector3(m_Lines[i].assetDatas[j].x, m_Lines[i].assetDatas[j].y, m_Lines[i].z);
					component.sizeDelta = new Vector2(m_Lines[i].assetDatas[j].width, m_Lines[i].assetDatas[j].height);
				}
			}
		}
		if (m_IsAutoAdaptiveWidthHeight && (m_HorizontalOverflow == HorizontalWrapMode.Overflow || m_VerticalOverflow == VerticalWrapMode.Overflow))
		{
			if (m_HorizontalOverflow == HorizontalWrapMode.Overflow && m_VerticalOverflow == VerticalWrapMode.Overflow)
			{
				base.rectTransform.sizeDelta = new Vector2(m_PreferredWidth, m_PreferredHeight);
			}
			if (m_HorizontalOverflow == HorizontalWrapMode.Overflow && m_VerticalOverflow == VerticalWrapMode.Truncate)
			{
				RectTransform rectTransform = base.rectTransform;
				float preferredWidth = m_PreferredWidth;
				Vector2 sizeDelta = base.rectTransform.sizeDelta;
				rectTransform.sizeDelta = new Vector2(preferredWidth, sizeDelta.y);
			}
			if (m_HorizontalOverflow == HorizontalWrapMode.Wrap && m_VerticalOverflow == VerticalWrapMode.Overflow)
			{
				RectTransform rectTransform2 = base.rectTransform;
				Vector2 sizeDelta2 = base.rectTransform.sizeDelta;
				rectTransform2.sizeDelta = new Vector2(sizeDelta2.x, m_PreferredHeight);
			}
		}
	}

	private void ClearAssetGo()
	{
		YlyAssetIdentify[] componentsInChildren = base.gameObject.GetComponentsInChildren<YlyAssetIdentify>();
		for (int i = 0; i < componentsInChildren.Length; i++)
		{
			if (componentsInChildren[i].gameObject != base.gameObject)
			{
				UnityEngine.Object.Destroy(componentsInChildren[i].gameObject);
			}
		}
	}

	private UnityEngine.Object LoadAsset(string assetPath, Type assetType)
	{
		UnityEngine.Object @object = null;
		if (@object == null)
		{
			Debug.Log("===============YlyRichText.LoadAsset asset not exit!!! 资源不存在！！！：" + assetPath);
		}
		return @object;
	}

	private void Update()
	{
		if (m_IsPopulateMeshDone)
		{
			m_IsPopulateMeshDone = false;
			UpdateAssetGo();
		}
		if (m_IsFontTextureDirty)
		{
			m_IsFontTextureDirty = false;
			UpdateLines();
		}
	}

	protected override void OnDestroy()
	{
		Font.textureRebuilt -= FontTextureRebuilt;
		ClearAssetGo();
		m_Lines.Clear();
		m_AssetDataDict.Clear();
		onLinkClick = null;
	}
}
