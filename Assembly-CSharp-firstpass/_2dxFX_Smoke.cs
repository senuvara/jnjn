using System;
using UnityEngine;
using UnityEngine.UI;

[Serializable]
[ExecuteInEditMode]
[AddComponentMenu("2DxFX/Standard/Smoke")]
public class _2dxFX_Smoke : MonoBehaviour
{
	[HideInInspector]
	public Material ForceMaterial;

	[HideInInspector]
	public bool ActiveChange = true;

	private string shader = "2DxFX/Standard/Smoke";

	[HideInInspector]
	[Range(0f, 1f)]
	public float _Alpha = 1f;

	[HideInInspector]
	public Texture2D __MainTex2;

	[HideInInspector]
	[Range(64f, 256f)]
	public float _Value1 = 64f;

	[HideInInspector]
	[Range(0f, 1f)]
	public float _Value2 = 1f;

	[HideInInspector]
	[Range(0f, 1f)]
	public float _Value3 = 1f;

	[HideInInspector]
	public float _Value4;

	[HideInInspector]
	public Color _Color1 = new Color(1f, 0f, 1f, 1f);

	[HideInInspector]
	public Color _Color2 = new Color(1f, 1f, 1f, 1f);

	[HideInInspector]
	public bool _AutoScrollX;

	[HideInInspector]
	[Range(0f, 10f)]
	public float _AutoScrollSpeedX;

	[HideInInspector]
	public bool _AutoScrollY;

	[HideInInspector]
	[Range(0f, 10f)]
	public float _AutoScrollSpeedY;

	[HideInInspector]
	private float _AutoScrollCountX;

	[HideInInspector]
	private float _AutoScrollCountY;

	[HideInInspector]
	public int ShaderChange;

	private Material tempMaterial;

	private Material defaultMaterial;

	private Image CanvasImage;

	private void Awake()
	{
		if (base.gameObject.GetComponent<Image>() != null)
		{
			CanvasImage = base.gameObject.GetComponent<Image>();
		}
	}

	private void Start()
	{
		__MainTex2 = (Resources.Load("_2dxFX_SmokeTXT") as Texture2D);
		ShaderChange = 0;
		if (base.gameObject.GetComponent<SpriteRenderer>() != null)
		{
			GetComponent<Renderer>().sharedMaterial.SetTexture("_MainTex2", __MainTex2);
		}
		else if (base.gameObject.GetComponent<Image>() != null)
		{
			CanvasImage.material.SetTexture("_MainTex2", __MainTex2);
		}
	}

	public void CallUpdate()
	{
		Update();
	}

	private void Update()
	{
		if (base.gameObject.GetComponent<Image>() != null && CanvasImage == null)
		{
			CanvasImage = base.gameObject.GetComponent<Image>();
		}
		if (ShaderChange == 0 && ForceMaterial != null)
		{
			ShaderChange = 1;
			if (tempMaterial != null)
			{
				UnityEngine.Object.DestroyImmediate(tempMaterial);
			}
			if (base.gameObject.GetComponent<SpriteRenderer>() != null)
			{
				GetComponent<Renderer>().sharedMaterial = ForceMaterial;
			}
			else if (base.gameObject.GetComponent<Image>() != null)
			{
				CanvasImage.material = ForceMaterial;
			}
			ForceMaterial.hideFlags = HideFlags.None;
			ForceMaterial.shader = Shader.Find(shader);
		}
		if (ForceMaterial == null && ShaderChange == 1)
		{
			if (tempMaterial != null)
			{
				UnityEngine.Object.DestroyImmediate(tempMaterial);
			}
			tempMaterial = new Material(Shader.Find(shader));
			tempMaterial.hideFlags = HideFlags.None;
			if (base.gameObject.GetComponent<SpriteRenderer>() != null)
			{
				GetComponent<Renderer>().sharedMaterial = tempMaterial;
			}
			else if (base.gameObject.GetComponent<Image>() != null)
			{
				CanvasImage.material = tempMaterial;
			}
			ShaderChange = 0;
		}
		if (!ActiveChange)
		{
			return;
		}
		if (base.gameObject.GetComponent<SpriteRenderer>() != null)
		{
			GetComponent<Renderer>().sharedMaterial.SetFloat("_Alpha", 1f - _Alpha);
			GetComponent<Renderer>().sharedMaterial.SetFloat("_Value1", _Value1);
			if (_Value2 == 1f)
			{
				_Value2 = 0.995f;
			}
			GetComponent<Renderer>().sharedMaterial.SetFloat("_Value2", _Value2);
			GetComponent<Renderer>().sharedMaterial.SetFloat("_Value3", _Value3);
			GetComponent<Renderer>().sharedMaterial.SetFloat("_Value4", _Value4);
			GetComponent<Renderer>().sharedMaterial.SetColor("_Color1", _Color1);
			GetComponent<Renderer>().sharedMaterial.SetColor("_Color2", _Color2);
		}
		else if (base.gameObject.GetComponent<Image>() != null)
		{
			CanvasImage.material.SetFloat("_Alpha", 1f - _Alpha);
			CanvasImage.material.SetFloat("_Value1", _Value1);
			if (_Value2 == 1f)
			{
				_Value2 = 0.995f;
			}
			CanvasImage.material.SetFloat("_Value2", _Value2);
			CanvasImage.material.SetFloat("_Value3", _Value3);
			CanvasImage.material.SetFloat("_Value4", _Value4);
			CanvasImage.material.SetColor("_Color1", _Color1);
			CanvasImage.material.SetColor("_Color2", _Color2);
		}
	}

	private void OnDestroy()
	{
		if (base.gameObject.GetComponent<Image>() != null && CanvasImage == null)
		{
			CanvasImage = base.gameObject.GetComponent<Image>();
		}
		if (Application.isPlaying || !Application.isEditor)
		{
			return;
		}
		if (tempMaterial != null)
		{
			UnityEngine.Object.DestroyImmediate(tempMaterial);
		}
		if (base.gameObject.activeSelf && defaultMaterial != null)
		{
			if (base.gameObject.GetComponent<SpriteRenderer>() != null)
			{
				GetComponent<Renderer>().sharedMaterial = defaultMaterial;
				GetComponent<Renderer>().sharedMaterial.hideFlags = HideFlags.None;
			}
			else if (base.gameObject.GetComponent<Image>() != null)
			{
				CanvasImage.material = defaultMaterial;
				CanvasImage.material.hideFlags = HideFlags.None;
			}
		}
	}

	private void OnDisable()
	{
		if (base.gameObject.GetComponent<Image>() != null && CanvasImage == null)
		{
			CanvasImage = base.gameObject.GetComponent<Image>();
		}
		if (base.gameObject.activeSelf && defaultMaterial != null)
		{
			if (base.gameObject.GetComponent<SpriteRenderer>() != null)
			{
				GetComponent<Renderer>().sharedMaterial = defaultMaterial;
				GetComponent<Renderer>().sharedMaterial.hideFlags = HideFlags.None;
			}
			else if (base.gameObject.GetComponent<Image>() != null)
			{
				CanvasImage.material = defaultMaterial;
				CanvasImage.material.hideFlags = HideFlags.None;
			}
		}
	}

	private void OnEnable()
	{
		if (base.gameObject.GetComponent<Image>() != null && CanvasImage == null)
		{
			CanvasImage = base.gameObject.GetComponent<Image>();
		}
		if (defaultMaterial == null)
		{
			defaultMaterial = new Material(Shader.Find("Sprites/Default"));
		}
		if (ForceMaterial == null)
		{
			ActiveChange = true;
			tempMaterial = new Material(Shader.Find(shader));
			tempMaterial.hideFlags = HideFlags.None;
			if (base.gameObject.GetComponent<SpriteRenderer>() != null)
			{
				GetComponent<Renderer>().sharedMaterial = tempMaterial;
			}
			else if (base.gameObject.GetComponent<Image>() != null)
			{
				CanvasImage.material = tempMaterial;
			}
			__MainTex2 = (Resources.Load("_2dxFX_SmokeTXT") as Texture2D);
		}
		else
		{
			ForceMaterial.shader = Shader.Find(shader);
			ForceMaterial.hideFlags = HideFlags.None;
			if (base.gameObject.GetComponent<SpriteRenderer>() != null)
			{
				GetComponent<Renderer>().sharedMaterial = ForceMaterial;
			}
			else if (base.gameObject.GetComponent<Image>() != null)
			{
				CanvasImage.material = ForceMaterial;
			}
			__MainTex2 = (Resources.Load("_2dxFX_SmokeTXT") as Texture2D);
		}
		if ((bool)__MainTex2)
		{
			__MainTex2.wrapMode = TextureWrapMode.Repeat;
			if (base.gameObject.GetComponent<SpriteRenderer>() != null)
			{
				GetComponent<Renderer>().sharedMaterial.SetTexture("_MainTex2", __MainTex2);
			}
			else if (base.gameObject.GetComponent<Image>() != null)
			{
				CanvasImage.material.SetTexture("_MainTex2", __MainTex2);
			}
		}
	}
}
