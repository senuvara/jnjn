using System;
using UnityEngine;

namespace SA.Foundation.Templates
{
	[Serializable]
	public class SA_Result
	{
		[SerializeField]
		protected SA_Error m_error;

		[SerializeField]
		protected string m_requestId = string.Empty;

		public SA_Error Error => m_error;

		public bool HasError
		{
			get
			{
				if (m_error == null || string.IsNullOrEmpty(m_error.Message))
				{
					return false;
				}
				return true;
			}
		}

		public bool IsSucceeded => !HasError;

		public bool IsFailed => HasError;

		public string RequestId
		{
			get
			{
				return m_requestId;
			}
			set
			{
				m_requestId = value;
			}
		}

		public SA_Result()
		{
		}

		public SA_Result(SA_Error error)
		{
			m_error = error;
		}
	}
}
