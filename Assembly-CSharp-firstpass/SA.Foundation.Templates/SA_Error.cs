using System;
using UnityEngine;

namespace SA.Foundation.Templates
{
	[Serializable]
	public class SA_Error
	{
		[SerializeField]
		private int m_code;

		[SerializeField]
		private string m_message = string.Empty;

		public int Code => m_code;

		public string Message => m_message;

		public SA_Error(int code, string message = "")
		{
			m_code = code;
			m_message = message;
		}
	}
}
