using System.Security.Cryptography;
using System.Text;

namespace SA.Foundation.Cryptography
{
	public static class SA_HMAC
	{
		public static string Hash(string key, string data)
		{
			byte[] bytes = Encoding.UTF8.GetBytes(key);
			using (HMACSHA256 hMACSHA = new HMACSHA256(bytes))
			{
				hMACSHA.ComputeHash(Encoding.UTF8.GetBytes(data));
				byte[] hash = hMACSHA.Hash;
				string text = string.Empty;
				for (int i = 0; i < hash.Length; i++)
				{
					text += hash[i].ToString("X2");
				}
				return text.ToLower();
			}
		}
	}
}
