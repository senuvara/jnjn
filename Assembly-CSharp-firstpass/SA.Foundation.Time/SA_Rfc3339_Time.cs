using System;
using System.Globalization;

namespace SA.Foundation.Time
{
	public static class SA_Rfc3339_Time
	{
		private static string[] _rfc3339Formats = new string[0];

		private const string Rfc3339Format = "yyyy-MM-dd'T'HH:mm:ssK";

		private const string MinRfc339Value = "0001-01-01T00:00:00Z";

		private static string[] DateTimePatterns
		{
			get
			{
				if (_rfc3339Formats.Length > 0)
				{
					return _rfc3339Formats;
				}
				_rfc3339Formats = new string[11];
				_rfc3339Formats[0] = "yyyy'-'MM'-'dd'T'HH':'mm':'ss'.'fffffffK";
				_rfc3339Formats[1] = "yyyy'-'MM'-'dd'T'HH':'mm':'ss'.'ffffffK";
				_rfc3339Formats[2] = "yyyy'-'MM'-'dd'T'HH':'mm':'ss'.'fffffK";
				_rfc3339Formats[3] = "yyyy'-'MM'-'dd'T'HH':'mm':'ss'.'ffffK";
				_rfc3339Formats[4] = "yyyy'-'MM'-'dd'T'HH':'mm':'ss'.'fffK";
				_rfc3339Formats[5] = "yyyy'-'MM'-'dd'T'HH':'mm':'ss'.'ffK";
				_rfc3339Formats[6] = "yyyy'-'MM'-'dd'T'HH':'mm':'ss'.'fK";
				_rfc3339Formats[7] = "yyyy'-'MM'-'dd'T'HH':'mm':'ssK";
				_rfc3339Formats[8] = "yyyy'-'MM'-'dd'T'HH':'mm':'ss'.'fffffffK";
				_rfc3339Formats[9] = DateTimeFormatInfo.InvariantInfo.UniversalSortableDateTimePattern;
				_rfc3339Formats[10] = DateTimeFormatInfo.InvariantInfo.SortableDateTimePattern;
				return _rfc3339Formats;
			}
		}

		public static string ToRfc3339(DateTime dateTime)
		{
			if (dateTime == DateTime.MinValue)
			{
				return "0001-01-01T00:00:00Z";
			}
			return dateTime.ToString("yyyy-MM-dd'T'HH:mm:ssK", DateTimeFormatInfo.InvariantInfo);
		}

		public static bool TryParseRfc3339(string Rfc3339Time, out DateTime result)
		{
			bool result2 = false;
			result = DateTime.Now;
			if (!string.IsNullOrEmpty(Rfc3339Time) && DateTime.TryParseExact(Rfc3339Time, DateTimePatterns, DateTimeFormatInfo.InvariantInfo, DateTimeStyles.AdjustToUniversal, out DateTime result3))
			{
				result = DateTime.SpecifyKind(result3, DateTimeKind.Utc);
				result = result.ToLocalTime();
				result2 = true;
			}
			return result2;
		}
	}
}
