using System;
using System.Collections.Generic;

namespace SA.Foundation.Time
{
	public static class SA_Time
	{
		private static Dictionary<string, long> s_timers = new Dictionary<string, long>();

		public static int CurrentTimeUTC => (int)DateTime.UtcNow.Subtract(new DateTime(1970, 1, 1)).TotalSeconds;

		public static void StartTimer(string name)
		{
			s_timers[name] = DateTime.Now.Ticks;
		}

		public static float GetTime(string name)
		{
			if (s_timers.ContainsKey(name))
			{
				long num = DateTime.Now.Ticks - s_timers[name];
				return (float)num / 1E+07f;
			}
			return 0f;
		}
	}
}
