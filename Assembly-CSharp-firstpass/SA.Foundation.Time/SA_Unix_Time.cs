using System;

namespace SA.Foundation.Time
{
	public static class SA_Unix_Time
	{
		public static DateTime ToDateTime(long timestamp)
		{
			return new DateTime(1970, 1, 1, 0, 0, 0, 0, DateTimeKind.Utc).AddSeconds(timestamp);
		}

		public static long ToUnixTime(DateTime date)
		{
			DateTime d = new DateTime(1970, 1, 1, 0, 0, 0, 0, DateTimeKind.Utc);
			return (long)(date.ToUniversalTime() - d).TotalSeconds;
		}
	}
}
