using CodeStage.AntiCheat.ObscuredTypes;
using System.Text;
using UnityEngine;

namespace CodeStage.AntiCheat.Examples
{
	public class ObscuredTypesExamples : MonoBehaviour
	{
		[Header("Regular variables")]
		public string regularString = "I'm regular string";

		public int regularInt = 1987;

		public float regularFloat = 2013.05237f;

		public Vector3 regularVector3 = new Vector3(10.5f, 11.5f, 12.5f);

		[Header("Obscured (secure) variables")]
		public ObscuredString obscuredString = "I'm obscured string";

		public ObscuredInt obscuredInt = 1987;

		public ObscuredFloat obscuredFloat = 2013.05237f;

		public ObscuredVector3 obscuredVector3 = new Vector3(10.5f, 11.5f, 12.5f);

		public ObscuredBool obscuredBool = true;

		public ObscuredLong obscuredLong = 945678987654123345L;

		public ObscuredDouble obscuredDouble = 9.45678987654;

		public ObscuredVector2 obscuredVector2 = new Vector2(8.5f, 9.5f);

		public ObscuredDecimal obscuredDecimal = 503.4521m;

		public ObscuredVector2Int obscuredVector2Int = new Vector2Int(8, 9);

		public ObscuredVector3Int obscuredVector3Int = new Vector3Int(15, 16, 17);

		private readonly StringBuilder logBuilder = new StringBuilder();

		private void Awake()
		{
			ObscuredStringExample();
			ObscuredIntExample();
		}

		private void Start()
		{
			logBuilder.Length = 0;
			logBuilder.AppendLine("ObscuredDecimal value from inspector: " + obscuredDecimal);
			logBuilder.AppendLine("ObscuredBool value from inspector: " + obscuredBool);
			logBuilder.AppendLine("ObscuredLong value from inspector: " + obscuredLong);
			logBuilder.AppendLine("ObscuredDouble value from inspector: " + obscuredDouble);
			logBuilder.AppendLine("ObscuredVector2 value from inspector: " + obscuredVector2);
			logBuilder.AppendLine("ObscuredVector2Int value from inspector: " + obscuredVector2Int);
			logBuilder.AppendLine("ObscuredVector3Int value from inspector: " + obscuredVector3Int);
			Debug.Log(logBuilder);
			Invoke("RandomizeObscuredVars", Random.Range(1f, 10f));
		}

		private void RandomizeObscuredVars()
		{
			obscuredInt.RandomizeCryptoKey();
			obscuredFloat.RandomizeCryptoKey();
			obscuredString.RandomizeCryptoKey();
			obscuredVector3.RandomizeCryptoKey();
			Invoke("RandomizeObscuredVars", Random.Range(1f, 10f));
		}

		private void ObscuredStringExample()
		{
			ObscuredString.SetNewCryptoKey("I LOVE MY GIRLz");
			string text = "the Goscurry is not a lie ;)";
			ObscuredString obscuredString = text;
			string encrypted = obscuredString.GetEncrypted();
			ObscuredString obscuredString2 = ObscuredString.FromEncrypted(encrypted);
			logBuilder.Length = 0;
			logBuilder.AppendLine("[ ObscuredString example ]");
			logBuilder.AppendLine("Original value:\n" + text);
			logBuilder.AppendLine("Obscured value in memory:\n" + obscuredString2.GetEncrypted());
			Debug.Log(logBuilder);
		}

		private void ObscuredIntExample()
		{
			int value = 5;
			ObscuredInt value2 = value;
			ObscuredInt.SetNewCryptoKey(666);
			value2.ApplyNewCryptoKey();
			value = value2;
			value2 = value;
			value2 = (int)value2 - 2;
			value2 = (int)value2 + value + 10;
			value2 = (int)value2 / 2;
			++value2;
			--value2;
			logBuilder.Length = 0;
			logBuilder.AppendLine("[ ObscuredInt example ]");
			logBuilder.AppendLine("Original lives count: " + value);
			logBuilder.AppendLine("Obscured lives count in memory: " + ((ObscuredInt)value).GetEncrypted());
			logBuilder.AppendLine(string.Concat("Lives count after few operations with obscured value: ", value2, " (", value2.ToString("X"), "h)"));
			Debug.Log(logBuilder);
		}
	}
}
