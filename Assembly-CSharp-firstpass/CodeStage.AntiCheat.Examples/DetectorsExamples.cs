using CodeStage.AntiCheat.Detectors;
using UnityEngine;

namespace CodeStage.AntiCheat.Examples
{
	public class DetectorsExamples : MonoBehaviour
	{
		internal bool injectionDetected;

		internal bool speedHackDetected;

		internal bool wrongTimeDetected;

		internal bool timeCheatingDetected;

		internal bool obscuredTypeCheatDetected;

		internal bool wallHackCheatDetected;

		public void OnSpeedHackDetected()
		{
			speedHackDetected = true;
			Debug.Log("Speed hack Detected!");
		}

		public void OnTimeCheatingDetected()
		{
			timeCheatingDetected = true;
			Debug.Log("Time cheating Detected!");
		}

		public void OnInjectionDetected()
		{
			injectionDetected = true;
			Debug.Log("Injection Detected!");
		}

		public void OnInjectionDetectedWithCause(string cause)
		{
			injectionDetected = true;
			Debug.Log("Injection Detected! Cause: " + cause);
		}

		public void OnObscuredTypeCheatingDetected()
		{
			obscuredTypeCheatDetected = true;
			Debug.Log("Obscured Vars Cheating Detected!");
		}

		public void OnWallHackDetected()
		{
			wallHackCheatDetected = true;
			Debug.Log("Wall hack Detected!");
		}

		public void OnTimeCheatChecked(TimeCheatingDetector.CheckResult checkResult, TimeCheatingDetector.ErrorKind errorKind)
		{
			switch (checkResult)
			{
			case TimeCheatingDetector.CheckResult.CheckPassed:
				Debug.Log("TimeCheatingDetector: Check passed, time seems to be fine.");
				break;
			case TimeCheatingDetector.CheckResult.WrongTimeDetected:
				wrongTimeDetected = true;
				Debug.LogWarning("TimeCheatingDetector: Wrong time Detected!");
				break;
			case TimeCheatingDetector.CheckResult.CheatDetected:
				timeCheatingDetected = true;
				Debug.LogWarning("TimeCheatingDetector: Time cheating Detected!");
				break;
			case TimeCheatingDetector.CheckResult.Unknown:
			case TimeCheatingDetector.CheckResult.Error:
				Debug.LogError("TimeCheatingDetector: some error occured: " + errorKind);
				break;
			default:
				Debug.LogError("TimeCheatingDetector: incorrect CheckResult value!");
				break;
			}
		}

		private void Start()
		{
			SpeedHackDetectorExample();
			InjectionDetectorExample();
			ObscuredCheatingDetectorExample();
			TimeCheatingDetectorExample();
		}

		private void SpeedHackDetectorExample()
		{
		}

		private void InjectionDetectorExample()
		{
		}

		private void ObscuredCheatingDetectorExample()
		{
		}

		private void TimeCheatingDetectorExample()
		{
			TimeCheatingDetector.Instance.CheatChecked += OnTimeCheatChecked;
		}

		internal void ForceTimeCheatingDetectorCheck()
		{
			if (TimeCheatingDetector.Instance != null && !TimeCheatingDetector.Instance.IsCheckingForCheat)
			{
				TimeCheatingDetector.Instance.ForceCheck();
			}
		}
	}
}
