using CodeStage.AntiCheat.Detectors;
using CodeStage.AntiCheat.ObscuredTypes;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Reflection;
using UnityEngine;

namespace CodeStage.AntiCheat.Examples
{
	[AddComponentMenu("")]
	public class ExamplesGUI : MonoBehaviour
	{
		private enum ExamplePage
		{
			ObscuredTypes,
			ObscuredPrefs,
			Detectors
		}

		private const string RedColor = "#FF4040";

		private const string YellowColor = "#E9D604";

		private const string GreenColor = "#02C85F";

		[Header("Examples")]
		public ObscuredTypesExamples obscuredTypesExamples;

		public ObscuredPrefsExamples obscuredPrefsExamples;

		public DetectorsExamples detectorsExamples;

		private readonly string[] tabs = new string[3]
		{
			"Variables protection",
			"Saves protection",
			"Cheating detectors"
		};

		private ExamplePage currentPage;

		private string allSimpleObscuredTypes;

		private ObscuredPrefs.DeviceLockLevel savesLock;

		private GUIStyle centeredStyle;

		private void OnGUI()
		{
			if (centeredStyle == null)
			{
				centeredStyle = new GUIStyle(GUI.skin.label)
				{
					alignment = TextAnchor.UpperCenter
				};
			}
			GUILayout.BeginArea(new Rect(10f, 5f, Screen.width - 20, Screen.height - 10));
			GUILayout.Label("<color=\"#0287C8\"><b>Anti-Cheat Toolkit Sandbox</b></color>", centeredStyle);
			GUILayout.Label("Here you can overview common ACTk features and try to cheat something yourself.", centeredStyle);
			GUILayout.Space(5f);
			currentPage = (ExamplePage)GUILayout.Toolbar((int)currentPage, tabs);
			switch (currentPage)
			{
			case ExamplePage.ObscuredTypes:
				DrawObscuredTypesPage();
				break;
			case ExamplePage.ObscuredPrefs:
				DrawObscuredPrefsPage();
				break;
			case ExamplePage.Detectors:
				DrawDetectorsPage();
				break;
			}
			GUILayout.EndArea();
		}

		private void DrawObscuredTypesPage()
		{
			GUILayout.Label("ACTk offers own collection of the secure types to let you protect your variables from <b>ANY</b> memory hacking tools (Cheat Engine, ArtMoney, GameCIH, Game Guardian, etc.).");
			GUILayout.Space(5f);
			using (new GUILayout.HorizontalScope())
			{
				GUILayout.Label("<b>Obscured types:</b>\n<color=\"#75C4EB\">" + GetAllSimpleObscuredTypes() + "</color>", GUILayout.MinWidth(130f));
				GUILayout.Space(10f);
				using (new GUILayout.VerticalScope(GUI.skin.box))
				{
					GUILayout.Label("Below you can try to cheat few variables of the regular types and their obscured (secure) analogues (you may change initial values from Tester object inspector):");
					GUILayout.Space(10f);
					using (new GUILayout.HorizontalScope())
					{
						GUILayout.Label("<b>int:</b> " + obscuredTypesExamples.regularInt, GUILayout.Width(250f));
						if (GUILayout.Button("Add random value"))
						{
							obscuredTypesExamples.regularInt += UnityEngine.Random.Range(1, 100);
						}
						if (GUILayout.Button("Reset"))
						{
							obscuredTypesExamples.regularInt = 0;
						}
					}
					using (new GUILayout.HorizontalScope())
					{
						GUILayout.Label("<b>ObscuredInt:</b> " + obscuredTypesExamples.obscuredInt, GUILayout.Width(250f));
						if (GUILayout.Button("Add random value"))
						{
							ObscuredTypesExamples obj = obscuredTypesExamples;
							obj.obscuredInt = (int)obj.obscuredInt + UnityEngine.Random.Range(1, 100);
						}
						if (GUILayout.Button("Reset"))
						{
							obscuredTypesExamples.obscuredInt = 0;
						}
					}
					GUILayout.Space(10f);
					using (new GUILayout.HorizontalScope())
					{
						GUILayout.Label("<b>float:</b> " + obscuredTypesExamples.regularFloat, GUILayout.Width(250f));
						if (GUILayout.Button("Add random value"))
						{
							obscuredTypesExamples.regularFloat += UnityEngine.Random.Range(1f, 100f);
						}
						if (GUILayout.Button("Reset"))
						{
							obscuredTypesExamples.regularFloat = 0f;
						}
					}
					using (new GUILayout.HorizontalScope())
					{
						GUILayout.Label("<b>ObscuredFloat:</b> " + obscuredTypesExamples.obscuredFloat, GUILayout.Width(250f));
						if (GUILayout.Button("Add random value"))
						{
							ObscuredTypesExamples obj2 = obscuredTypesExamples;
							obj2.obscuredFloat = (float)obj2.obscuredFloat + UnityEngine.Random.Range(1f, 100f);
						}
						if (GUILayout.Button("Reset"))
						{
							obscuredTypesExamples.obscuredFloat = 0f;
						}
					}
					GUILayout.Space(10f);
					using (new GUILayout.HorizontalScope())
					{
						GUILayout.Label("<b>Vector3:</b> " + obscuredTypesExamples.regularVector3, GUILayout.Width(250f));
						if (GUILayout.Button("Add random value"))
						{
							obscuredTypesExamples.regularVector3 += UnityEngine.Random.insideUnitSphere;
						}
						if (GUILayout.Button("Reset"))
						{
							obscuredTypesExamples.regularVector3 = Vector3.zero;
						}
					}
					using (new GUILayout.HorizontalScope())
					{
						GUILayout.Label("<b>ObscuredVector3:</b> " + obscuredTypesExamples.obscuredVector3, GUILayout.Width(250f));
						if (GUILayout.Button("Add random value"))
						{
							obscuredTypesExamples.obscuredVector3 += UnityEngine.Random.insideUnitSphere;
						}
						if (GUILayout.Button("Reset"))
						{
							obscuredTypesExamples.obscuredVector3 = Vector3.zero;
						}
					}
				}
			}
		}

		private void DrawObscuredPrefsPage()
		{
			GUILayout.Label("ACTk has secure layer for the PlayerPrefs: <color=\"#75C4EB\">ObscuredPrefs</color>. It protects data from view, detects any cheating attempts, optionally locks data to the current device and supports additional data types.");
			GUILayout.Space(5f);
			using (new GUILayout.HorizontalScope())
			{
				GUILayout.Label("<b>Supported types:</b>\n" + GetAllObscuredPrefsDataTypes(), GUILayout.MinWidth(130f));
				using (new GUILayout.VerticalScope(GUI.skin.box))
				{
					GUILayout.Label("Below you can try to cheat both regular PlayerPrefs and secure ObscuredPrefs:");
					using (new GUILayout.VerticalScope())
					{
						GUILayout.Label("<color=\"#FF4040\"><b>PlayerPrefs:</b></color>\neasy to cheat, only 3 supported types", centeredStyle);
						GUILayout.Space(5f);
						if (string.IsNullOrEmpty(obscuredPrefsExamples.regularPrefs))
						{
							obscuredPrefsExamples.LoadRegularPrefs();
						}
						using (new GUILayout.HorizontalScope())
						{
							GUILayout.Label(obscuredPrefsExamples.regularPrefs, GUILayout.Width(270f));
							using (new GUILayout.VerticalScope())
							{
								using (new GUILayout.HorizontalScope())
								{
									if (GUILayout.Button("Save"))
									{
										obscuredPrefsExamples.SaveRegularPrefs();
									}
									if (GUILayout.Button("Load"))
									{
										obscuredPrefsExamples.LoadRegularPrefs();
									}
								}
								if (GUILayout.Button("Delete"))
								{
									obscuredPrefsExamples.DeleteRegularPrefs();
								}
							}
						}
					}
					GUILayout.Space(5f);
					using (new GUILayout.VerticalScope())
					{
						GUILayout.Label("<color=\"#02C85F\"><b>ObscuredPrefs:</b></color>\nsecure, lot of additional types and extra options", centeredStyle);
						GUILayout.Space(5f);
						if (string.IsNullOrEmpty(obscuredPrefsExamples.obscuredPrefs))
						{
							obscuredPrefsExamples.LoadObscuredPrefs();
						}
						using (new GUILayout.HorizontalScope())
						{
							GUILayout.Label(obscuredPrefsExamples.obscuredPrefs, GUILayout.Width(270f));
							using (new GUILayout.VerticalScope())
							{
								using (new GUILayout.HorizontalScope())
								{
									if (GUILayout.Button("Save"))
									{
										obscuredPrefsExamples.SaveObscuredPrefs();
									}
									if (GUILayout.Button("Load"))
									{
										obscuredPrefsExamples.LoadObscuredPrefs();
									}
								}
								if (GUILayout.Button("Delete"))
								{
									obscuredPrefsExamples.DeleteObscuredPrefs();
								}
								using (new GUILayout.HorizontalScope())
								{
									GUILayout.Label("LockToDevice level");
								}
								savesLock = (ObscuredPrefs.DeviceLockLevel)GUILayout.SelectionGrid((int)savesLock, Enum.GetNames(typeof(ObscuredPrefs.DeviceLockLevel)), 3);
								obscuredPrefsExamples.LockObscuredPrefsToDevice(savesLock);
								GUILayout.Space(5f);
								using (new GUILayout.HorizontalScope())
								{
									obscuredPrefsExamples.PreservePlayerPrefs = GUILayout.Toggle(obscuredPrefsExamples.PreservePlayerPrefs, "preservePlayerPrefs");
								}
								using (new GUILayout.HorizontalScope())
								{
									obscuredPrefsExamples.EmergencyMode = GUILayout.Toggle(obscuredPrefsExamples.EmergencyMode, "emergencyMode");
								}
								using (new GUILayout.HorizontalScope())
								{
									obscuredPrefsExamples.ReadForeignSaves = GUILayout.Toggle(obscuredPrefsExamples.ReadForeignSaves, "readForeignSaves");
								}
								GUILayout.Space(5f);
								GUILayout.Label("<color=\"" + ((!obscuredPrefsExamples.savesAlterationDetected) ? "#02C85F" : "#FF4040") + "\">Saves modification detected: " + obscuredPrefsExamples.savesAlterationDetected + "</color>");
								GUILayout.Label("<color=\"" + ((!obscuredPrefsExamples.foreignSavesDetected) ? "#02C85F" : "#FF4040") + "\">Foreign saves detected: " + obscuredPrefsExamples.foreignSavesDetected + "</color>");
							}
						}
					}
					GUILayout.Space(5f);
				}
			}
		}

		private void DrawDetectorsPage()
		{
			GUILayout.Label("ACTk is able to detect some types of cheating to let you take action on the cheating players. This example scene has all possible detectors and all of them are automatically start on scene start.");
			GUILayout.Space(5f);
			using (new GUILayout.VerticalScope(GUI.skin.box))
			{
				GUILayout.Label("<b>Speed Hack Detector</b>");
				GUILayout.Label("Allows to detect speed hack applied from Cheat Engine, Game Guardian and such.");
				GUILayout.Label("Running: " + SpeedHackDetector.Instance.IsRunning + "\n<color=\"" + ((!detectorsExamples.speedHackDetected) ? "#02C85F" : "#FF4040") + "\">Detected: " + detectorsExamples.speedHackDetected.ToString().ToLower() + "</color>");
				GUILayout.Space(10f);
				using (new GUILayout.HorizontalScope())
				{
					GUILayout.Label("<b>Time Cheating Detector</b> (updates once per 1 min by default)");
					if (GUILayout.Button("Force check", GUILayout.Width(100f)))
					{
						detectorsExamples.ForceTimeCheatingDetectorCheck();
					}
				}
				GUILayout.Label("Allows to detect system time change to cheat some long-term processes (building progress, etc.).");
				if (detectorsExamples.wrongTimeDetected && !detectorsExamples.timeCheatingDetected)
				{
					GUILayout.Label("Running: " + TimeCheatingDetector.Instance.IsRunning + "\n<color=\"#E9D604\">Wrong time detected (diff more than: " + TimeCheatingDetector.Instance.wrongTimeThreshold.ToString().ToLower() + " minutes)</color>");
				}
				else
				{
					GUILayout.Label("Running: " + TimeCheatingDetector.Instance.IsRunning + "\n<color=\"" + ((!detectorsExamples.timeCheatingDetected) ? "#02C85F" : "#FF4040") + "\">Detected: " + detectorsExamples.timeCheatingDetected.ToString().ToLower() + "</color>");
				}
				GUILayout.Space(10f);
				GUILayout.Label("<b>Obscured Cheating Detector</b>");
				GUILayout.Label("Detects cheating of any Obscured type (except ObscuredPrefs, it has own detection features) used in project.");
				GUILayout.Label("Running: " + ObscuredCheatingDetector.Instance.IsRunning + "\n<color=\"" + ((!detectorsExamples.obscuredTypeCheatDetected) ? "#02C85F" : "#FF4040") + "\">Detected: " + detectorsExamples.obscuredTypeCheatDetected.ToString().ToLower() + "</color>");
				GUILayout.Space(10f);
				GUILayout.Label("<b>WallHack Detector</b>");
				GUILayout.Label("Detects common types of wall hack cheating: walking through the walls (Rigidbody and CharacterController modules), shooting through the walls (Raycast module), looking through the walls (Wireframe module).");
				GUILayout.Label("Running: " + WallHackDetector.Instance.IsRunning + "\n<color=\"" + ((!detectorsExamples.wallHackCheatDetected) ? "#02C85F" : "#FF4040") + "\">Detected: " + detectorsExamples.wallHackCheatDetected.ToString().ToLower() + "</color>");
				GUILayout.Space(10f);
				GUILayout.Label("<b>Injection Detector</b>");
				GUILayout.Label("Allows to detect foreign managed assemblies in your application.");
				GUILayout.Label("Running: " + (InjectionDetector.Instance != null && InjectionDetector.Instance.IsRunning) + "\n<color=\"" + ((!detectorsExamples.injectionDetected) ? "#02C85F" : "#FF4040") + "\">Detected: " + detectorsExamples.injectionDetected.ToString().ToLower() + "</color>");
			}
		}

		private string GetAllSimpleObscuredTypes()
		{
			string result = "Can't use reflection here, sorry :(";
			string types = string.Empty;
			if (string.IsNullOrEmpty(allSimpleObscuredTypes))
			{
				Assembly assembly = typeof(ObscuredInt).Assembly;
				IEnumerable<Type> source = from t in assembly.GetTypes()
					where t.IsPublic && t.Namespace == "CodeStage.AntiCheat.ObscuredTypes" && t.Name != "ObscuredPrefs"
					select t;
				source.ToList().ForEach(delegate(Type t)
				{
					if (types.Length > 0)
					{
						types = types + "\n" + t.Name;
					}
					else
					{
						types += t.Name;
					}
				});
				if (!string.IsNullOrEmpty(types))
				{
					result = types;
					allSimpleObscuredTypes = types;
				}
				else
				{
					allSimpleObscuredTypes = result;
				}
			}
			else
			{
				result = allSimpleObscuredTypes;
			}
			return result;
		}

		private string GetAllObscuredPrefsDataTypes()
		{
			return "int\nfloat\nstring\n<color=\"#75C4EB\">uint\ndouble\ndecimal\nlong\nulong\nbool\nbyte[]\nVector2\nVector3\nQuaternion\nColor\nRect</color>";
		}
	}
}
