using CodeStage.AntiCheat.ObscuredTypes;
using UnityEngine;

namespace CodeStage.AntiCheat.Examples
{
	public class ObscuredPrefsExamples : MonoBehaviour
	{
		private const string PrefsString = "name";

		private const string PrefsInt = "money";

		private const string PrefsFloat = "lifeBar";

		private const string PrefsBool = "gameComplete";

		private const string PrefsUint = "demoUint";

		private const string PrefsLong = "demoLong";

		private const string PrefsDouble = "demoDouble";

		private const string PrefsVector2 = "demoVector2";

		private const string PrefsVector3 = "demoVector3";

		private const string PrefsQuaternion = "demoQuaternion";

		private const string PrefsRect = "demoRect";

		private const string PrefsColor = "demoColor";

		private const string PrefsByteArray = "demoByteArray";

		public string prefsEncryptionKey = "change me!";

		internal string regularPrefs;

		internal string obscuredPrefs;

		internal bool savesAlterationDetected;

		internal bool foreignSavesDetected;

		internal bool PreservePlayerPrefs
		{
			get
			{
				return ObscuredPrefs.preservePlayerPrefs;
			}
			set
			{
				ObscuredPrefs.preservePlayerPrefs = value;
			}
		}

		internal bool EmergencyMode
		{
			get
			{
				return ObscuredPrefs.emergencyMode;
			}
			set
			{
				ObscuredPrefs.emergencyMode = value;
			}
		}

		internal bool ReadForeignSaves
		{
			get
			{
				return ObscuredPrefs.readForeignSaves;
			}
			set
			{
				ObscuredPrefs.readForeignSaves = value;
			}
		}

		private void Awake()
		{
			ObscuredPrefs.CryptoKey = prefsEncryptionKey;
			ObscuredPrefs.OnAlterationDetected += SavesAlterationDetected;
			ObscuredPrefs.OnPossibleForeignSavesDetected += ForeignSavesDetected;
		}

		private void OnDestroy()
		{
			DeleteRegularPrefs();
			DeleteObscuredPrefs();
		}

		private void SavesAlterationDetected()
		{
			savesAlterationDetected = true;
		}

		private void ForeignSavesDetected()
		{
			foreignSavesDetected = true;
		}

		internal void LoadRegularPrefs()
		{
			regularPrefs = "int: " + PlayerPrefs.GetInt("money", -1) + "\n";
			string text = regularPrefs;
			regularPrefs = text + "float: " + PlayerPrefs.GetFloat("lifeBar", -1f) + "\n";
			regularPrefs = regularPrefs + "string: " + PlayerPrefs.GetString("name", "No saved PlayerPrefs!");
		}

		internal void SaveRegularPrefs()
		{
			PlayerPrefs.SetInt("money", 456);
			PlayerPrefs.SetFloat("lifeBar", 456.789f);
			PlayerPrefs.SetString("name", "Hey, there!");
			PlayerPrefs.Save();
		}

		internal void DeleteRegularPrefs()
		{
			PlayerPrefs.DeleteKey("money");
			PlayerPrefs.DeleteKey("lifeBar");
			PlayerPrefs.DeleteKey("name");
			PlayerPrefs.Save();
		}

		internal void LockObscuredPrefsToDevice(ObscuredPrefs.DeviceLockLevel level)
		{
			ObscuredPrefs.lockToDevice = level;
		}

		internal void LoadObscuredPrefs()
		{
			obscuredPrefs = "int: " + ObscuredPrefs.GetInt("money", -1) + "\n";
			string text = obscuredPrefs;
			obscuredPrefs = text + "float: " + ObscuredPrefs.GetFloat("lifeBar", -1f) + "\n";
			obscuredPrefs = obscuredPrefs + "string: " + ObscuredPrefs.GetString("name", "No saved ObscuredPrefs!") + "\n";
			text = obscuredPrefs;
			obscuredPrefs = text + "bool: " + ObscuredPrefs.GetBool("gameComplete", defaultValue: false) + "\n";
			text = obscuredPrefs;
			obscuredPrefs = text + "uint: " + ObscuredPrefs.GetUInt("demoUint", 0u) + "\n";
			text = obscuredPrefs;
			obscuredPrefs = text + "long: " + ObscuredPrefs.GetLong("demoLong", -1L) + "\n";
			text = obscuredPrefs;
			obscuredPrefs = text + "double: " + ObscuredPrefs.GetDouble("demoDouble", -1.0) + "\n";
			text = obscuredPrefs;
			obscuredPrefs = string.Concat(text, "Vector2: ", ObscuredPrefs.GetVector2("demoVector2", Vector2.zero), "\n");
			text = obscuredPrefs;
			obscuredPrefs = string.Concat(text, "Vector3: ", ObscuredPrefs.GetVector3("demoVector3", Vector3.zero), "\n");
			text = obscuredPrefs;
			obscuredPrefs = string.Concat(text, "Quaternion: ", ObscuredPrefs.GetQuaternion("demoQuaternion", Quaternion.identity), "\n");
			text = obscuredPrefs;
			obscuredPrefs = string.Concat(text, "Rect: ", ObscuredPrefs.GetRect("demoRect", new Rect(0f, 0f, 0f, 0f)), "\n");
			text = obscuredPrefs;
			obscuredPrefs = string.Concat(text, "Color: ", ObscuredPrefs.GetColor("demoColor", Color.black), "\n");
			byte[] byteArray = ObscuredPrefs.GetByteArray("demoByteArray", 0, 4);
			text = obscuredPrefs;
			obscuredPrefs = text + "byte[]: {" + byteArray[0] + "," + byteArray[1] + "," + byteArray[2] + "," + byteArray[3] + "}";
		}

		internal void SaveObscuredPrefs()
		{
			ObscuredPrefs.SetInt("money", 123);
			ObscuredPrefs.SetFloat("lifeBar", 123.456f);
			ObscuredPrefs.SetString("name", "Goscurry is not a lie ;)");
			ObscuredPrefs.SetBool("gameComplete", value: true);
			ObscuredPrefs.SetUInt("demoUint", 1234567891u);
			ObscuredPrefs.SetLong("demoLong", 1234567891234567890L);
			ObscuredPrefs.SetDouble("demoDouble", 1.234567890123456);
			ObscuredPrefs.SetVector2("demoVector2", Vector2.one);
			ObscuredPrefs.SetVector3("demoVector3", Vector3.one);
			ObscuredPrefs.SetQuaternion("demoQuaternion", Quaternion.Euler(new Vector3(10f, 20f, 30f)));
			ObscuredPrefs.SetRect("demoRect", new Rect(1.5f, 2.6f, 3.7f, 4.8f));
			ObscuredPrefs.SetColor("demoColor", Color.red);
			ObscuredPrefs.SetByteArray("demoByteArray", new byte[4]
			{
				44,
				104,
				43,
				32
			});
			ObscuredPrefs.Save();
		}

		internal void DeleteObscuredPrefs()
		{
			ObscuredPrefs.DeleteKey("money");
			ObscuredPrefs.DeleteKey("lifeBar");
			ObscuredPrefs.DeleteKey("name");
			ObscuredPrefs.DeleteKey("gameComplete");
			ObscuredPrefs.DeleteKey("demoUint");
			ObscuredPrefs.DeleteKey("demoLong");
			ObscuredPrefs.DeleteKey("demoDouble");
			ObscuredPrefs.DeleteKey("demoVector2");
			ObscuredPrefs.DeleteKey("demoVector3");
			ObscuredPrefs.DeleteKey("demoQuaternion");
			ObscuredPrefs.DeleteKey("demoRect");
			ObscuredPrefs.DeleteKey("demoColor");
			ObscuredPrefs.DeleteKey("demoByteArray");
			ObscuredPrefs.Save();
		}
	}
}
