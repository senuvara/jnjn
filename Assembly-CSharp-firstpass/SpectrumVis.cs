using UnityEngine;

public class SpectrumVis : MonoBehaviour
{
	public enum axisStrech
	{
		dx,
		dy,
		dz,
		dyAndDz,
		all
	}

	public enum channelColour
	{
		red,
		green,
		blue,
		all
	}

	public GameObject[] cubes;

	public Color barColor;

	public float sizePower = 20f;

	public axisStrech stretchAxis = axisStrech.dy;

	public channelColour currentChannel;

	private float currentRed;

	private float currentGreen;

	private float currentBlue;

	public float colorPower = 12f;

	private void Start()
	{
		currentRed = barColor.r;
		currentGreen = barColor.g;
		currentBlue = barColor.b;
	}

	private void Update()
	{
		for (int i = 0; i < cubes.Length; i++)
		{
			Vector3 localScale = cubes[i].transform.localScale;
			float b = SpectrumKernel.spects[i] * sizePower;
			if (stretchAxis == axisStrech.dx)
			{
				localScale.x = Mathf.Lerp(localScale.x, b, Time.deltaTime * sizePower);
			}
			if (stretchAxis == axisStrech.dy)
			{
				localScale.y = Mathf.Lerp(localScale.y, b, Time.deltaTime * sizePower);
			}
			if (stretchAxis == axisStrech.dz)
			{
				localScale.z = Mathf.Lerp(localScale.z, b, Time.deltaTime * sizePower);
			}
			if (stretchAxis == axisStrech.dyAndDz)
			{
				localScale.y = Mathf.Lerp(localScale.y, b, Time.deltaTime * sizePower);
				localScale.z = Mathf.Lerp(localScale.z, b, Time.deltaTime * sizePower);
			}
			if (stretchAxis == axisStrech.all)
			{
				localScale.x = Mathf.Lerp(localScale.x, b, Time.deltaTime * sizePower);
				localScale.y = Mathf.Lerp(localScale.y, b, Time.deltaTime * sizePower);
				localScale.z = Mathf.Lerp(localScale.z, b, Time.deltaTime * sizePower);
			}
			cubes[i].transform.localScale = localScale;
			float num = SpectrumKernel.spects[i] * colorPower;
			if (currentChannel == channelColour.red)
			{
				barColor.r = currentRed + num;
			}
			if (currentChannel == channelColour.green)
			{
				barColor.g = currentGreen + num;
			}
			if (currentChannel == channelColour.blue)
			{
				barColor.b = currentBlue + num;
			}
			if (currentChannel == channelColour.all)
			{
				barColor.b = currentBlue + num;
				barColor.g = currentGreen + num;
				barColor.r = currentRed + num;
			}
			cubes[i].GetComponent<Renderer>().material.color = barColor;
			cubes[i].GetComponent<Renderer>().material.SetColor("_TintColor", barColor);
		}
	}
}
