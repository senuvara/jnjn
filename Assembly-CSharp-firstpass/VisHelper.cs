using UnityEngine;

public static class VisHelper
{
	public static int Validate(int currentValue, int minValue, int maxValue, int defaultValue, Object obj, string fieldName, bool error)
	{
		if (currentValue < minValue || currentValue > maxValue)
		{
			string message = obj.ToString() + " has its data \"" + fieldName + "\" out of its valid data range of " + minValue + " to " + maxValue + ".  Defaulting to " + defaultValue + ".";
			if (error)
			{
				Debug.LogError(message, obj);
			}
			else
			{
				Debug.LogWarning(message, obj);
			}
			return defaultValue;
		}
		return currentValue;
	}

	public static float Validate(float currentValue, float minValue, float maxValue, float defaultValue, Object obj, string fieldName, bool error)
	{
		if (currentValue < minValue || currentValue > maxValue)
		{
			string message = obj.ToString() + " has its data \"" + fieldName + "\" out of its valid data range of " + minValue + " to " + maxValue + ".  Defaulting to " + defaultValue + ".";
			if (error)
			{
				Debug.LogError(message, obj);
			}
			else
			{
				Debug.LogWarning(message, obj);
			}
			return defaultValue;
		}
		return currentValue;
	}

	public static float ConvertBetweenRanges(float sourceValue, float sourceMin, float sourceMax, float destMin, float destMax, bool invertDestValue)
	{
		float num = 0f;
		num = ((sourceValue <= sourceMin) ? 0f : ((!(sourceValue >= sourceMax)) ? ((sourceValue - sourceMin) / (sourceMax - sourceMin)) : 1f));
		if (invertDestValue)
		{
			num = 1f - num;
		}
		return Mathf.Lerp(destMin, destMax, num);
	}
}
