using UnityEngine;

namespace SA.Foundation.Patterns
{
	public abstract class SA_SingletonePrefab<T> : MonoBehaviour where T : MonoBehaviour
	{
		protected static T _Instance;

		public static T Instance
		{
			get
			{
				if ((Object)_Instance == (Object)null)
				{
					_Instance = (Object.FindObjectOfType(typeof(T)) as T);
					if ((Object)_Instance == (Object)null)
					{
						GameObject gameObject = Object.Instantiate(Resources.Load(typeof(T).Name)) as GameObject;
						_Instance = gameObject.GetComponent<T>();
						Object.DontDestroyOnLoad(gameObject);
					}
				}
				return _Instance;
			}
		}

		public static bool HasInstance => (Object)_Instance != (Object)null;
	}
}
