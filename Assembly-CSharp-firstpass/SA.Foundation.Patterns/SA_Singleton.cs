using UnityEngine;

namespace SA.Foundation.Patterns
{
	public abstract class SA_Singleton<T> : MonoBehaviour where T : MonoBehaviour
	{
		private static T s_instance;

		private static bool s_applicationIsQuitting;

		public static T Instance
		{
			get
			{
				if (s_applicationIsQuitting)
				{
					Debug.LogError(string.Concat(typeof(T), " [SA_Singleton] is already destroyed. Returning null. Please check HasInstance first before accessing instance in destructor."));
					return (T)null;
				}
				if ((Object)s_instance == (Object)null)
				{
					s_instance = (Object.FindObjectOfType(typeof(T)) as T);
					if ((Object)s_instance == (Object)null)
					{
						Instantiate();
					}
				}
				return s_instance;
			}
		}

		public static bool HasInstance => !IsDestroyed;

		public static bool IsDestroyed
		{
			get
			{
				if ((Object)s_instance == (Object)null)
				{
					return true;
				}
				return false;
			}
		}

		protected virtual void Awake()
		{
			Object.DontDestroyOnLoad(base.gameObject);
			base.gameObject.transform.SetParent(SA_SingletonService.Parent);
		}

		public static void Instantiate()
		{
			string fullName = typeof(T).FullName;
			s_instance = new GameObject(fullName).AddComponent<T>();
		}

		protected virtual void OnDestroy()
		{
			s_instance = (T)null;
			s_applicationIsQuitting = true;
		}

		protected virtual void OnApplicationQuit()
		{
			s_instance = (T)null;
			s_applicationIsQuitting = true;
		}
	}
}
