using UnityEngine;

namespace SA.Foundation.Patterns
{
	public abstract class SA_SceneSingleton<T> : MonoBehaviour where T : MonoBehaviour
	{
		private static T s_instance;

		public static T Instance
		{
			get
			{
				if ((Object)s_instance == (Object)null)
				{
					s_instance = (Object.FindObjectOfType(typeof(T)) as T);
					if ((Object)s_instance == (Object)null)
					{
						s_instance = new GameObject().AddComponent<T>();
						s_instance.gameObject.name = s_instance.GetType().FullName;
					}
				}
				return s_instance;
			}
		}

		public static bool HasInstance
		{
			get
			{
				if ((Object)s_instance == (Object)null)
				{
					return true;
				}
				return false;
			}
		}
	}
}
