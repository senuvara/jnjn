using UnityEngine;

namespace SA.Foundation.Patterns
{
	public static class SA_SingletonService
	{
		private static Transform s_services;

		public static Transform Parent
		{
			get
			{
				if (s_services == null)
				{
					s_services = new GameObject("SA_Singletons").transform;
					Object.DontDestroyOnLoad(s_services);
				}
				return s_services;
			}
		}
	}
}
