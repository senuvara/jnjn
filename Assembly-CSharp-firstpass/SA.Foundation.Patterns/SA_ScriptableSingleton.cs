using SA.Foundation.UtilitiesEditor;
using UnityEngine;

namespace SA.Foundation.Patterns
{
	public abstract class SA_ScriptableSingleton<T> : ScriptableObject where T : ScriptableObject
	{
		private static T s_instance;

		public static T Instance
		{
			get
			{
				if ((Object)s_instance == (Object)null)
				{
					s_instance = (Resources.Load(typeof(T).Name) as T);
					if ((Object)s_instance == (Object)null)
					{
						s_instance = ScriptableObject.CreateInstance<T>();
						SaveToAssetDatabase(s_instance);
					}
				}
				return s_instance;
			}
		}

		public static void Save()
		{
			SA_EditorUtility.SetDirty(Instance);
		}

		public static void Delete()
		{
			string assetFolderRelativePath = SA_AssetDatabase.GetAssetFolderRelativePath(Instance);
			SA_AssetDatabase.DeleteAsset(assetFolderRelativePath);
		}

		private static void SaveToAssetDatabase(T asset)
		{
			SA_AssetDatabase.CreateAsset(asset, "Plugins/StansAssets/Settings/Resources/" + asset.GetType().Name + ".asset");
		}
	}
}
