public interface IVisPrefabSpawnedTarget
{
	void OnSpawned(float current, float previous, float difference, float adjustedDifference);
}
