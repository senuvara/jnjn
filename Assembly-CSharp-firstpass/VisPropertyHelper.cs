using UnityEngine;

public static class VisPropertyHelper
{
	public static void SetGameObjectProperty(GameObject gameObject, GameObjectProperty targetProperty, float propertyValue)
	{
		if (gameObject == null)
		{
			return;
		}
		Rigidbody component = gameObject.GetComponent<Rigidbody>();
		switch (targetProperty)
		{
		case GameObjectProperty.XPosition:
			if (gameObject.transform != null)
			{
				Transform transform6 = gameObject.transform;
				Vector3 position3 = gameObject.transform.position;
				float y4 = position3.y;
				Vector3 position4 = gameObject.transform.position;
				transform6.position = new Vector3(propertyValue, y4, position4.z);
			}
			break;
		case GameObjectProperty.YPosition:
			if (gameObject.transform != null)
			{
				Transform transform8 = gameObject.transform;
				Vector3 position5 = gameObject.transform.position;
				float x8 = position5.x;
				Vector3 position6 = gameObject.transform.position;
				transform8.position = new Vector3(x8, propertyValue, position6.z);
			}
			break;
		case GameObjectProperty.ZPosition:
			if (gameObject.transform != null)
			{
				Transform transform3 = gameObject.transform;
				Vector3 position = gameObject.transform.position;
				float x3 = position.x;
				Vector3 position2 = gameObject.transform.position;
				transform3.position = new Vector3(x3, position2.y, propertyValue);
			}
			break;
		case GameObjectProperty.XRotation:
			if (gameObject.transform != null)
			{
				Transform transform7 = gameObject.transform;
				Vector3 eulerAngles3 = gameObject.transform.rotation.eulerAngles;
				float y5 = eulerAngles3.y;
				Vector3 eulerAngles4 = gameObject.transform.rotation.eulerAngles;
				transform7.rotation = Quaternion.Euler(new Vector3(propertyValue, y5, eulerAngles4.z));
			}
			break;
		case GameObjectProperty.YRotation:
			if (gameObject.transform != null)
			{
				Transform transform5 = gameObject.transform;
				Vector3 eulerAngles = gameObject.transform.rotation.eulerAngles;
				float x4 = eulerAngles.x;
				Vector3 eulerAngles2 = gameObject.transform.rotation.eulerAngles;
				transform5.rotation = Quaternion.Euler(new Vector3(x4, propertyValue, eulerAngles2.z));
			}
			break;
		case GameObjectProperty.ZRotation:
			if (gameObject.transform != null)
			{
				Transform transform9 = gameObject.transform;
				Vector3 eulerAngles5 = gameObject.transform.rotation.eulerAngles;
				float x10 = eulerAngles5.x;
				Vector3 eulerAngles6 = gameObject.transform.rotation.eulerAngles;
				transform9.rotation = Quaternion.Euler(new Vector3(x10, eulerAngles6.y, propertyValue));
			}
			break;
		case GameObjectProperty.XVelocity:
			if (component != null)
			{
				Vector3 velocity = component.velocity;
				float y3 = velocity.y;
				Vector3 velocity2 = component.velocity;
				component.velocity = new Vector3(propertyValue, y3, velocity2.z);
			}
			else if (gameObject.transform != null)
			{
				gameObject.transform.Translate(propertyValue * Time.deltaTime, 0f, 0f);
			}
			break;
		case GameObjectProperty.YVelocity:
			if (component != null)
			{
				Vector3 velocity5 = component.velocity;
				float x9 = velocity5.x;
				Vector3 velocity6 = component.velocity;
				component.velocity = new Vector3(x9, propertyValue, velocity6.z);
			}
			else if (gameObject.transform != null)
			{
				gameObject.transform.Translate(0f, propertyValue * Time.deltaTime, 0f);
			}
			break;
		case GameObjectProperty.ZVelocity:
			if (component != null)
			{
				Vector3 velocity3 = component.velocity;
				float x6 = velocity3.x;
				Vector3 velocity4 = component.velocity;
				component.velocity = new Vector3(x6, velocity4.y, propertyValue);
			}
			else if (gameObject.transform != null)
			{
				gameObject.transform.Translate(0f, 0f, propertyValue * Time.deltaTime);
			}
			break;
		case GameObjectProperty.XAngularVelocity:
			if (component != null)
			{
				Vector3 angularVelocity = component.angularVelocity;
				float y = angularVelocity.y;
				Vector3 angularVelocity2 = component.angularVelocity;
				component.angularVelocity = new Vector3(propertyValue, y, angularVelocity2.z);
			}
			else if (gameObject.transform != null)
			{
				gameObject.transform.Rotate(propertyValue * Time.deltaTime, 0f, 0f);
			}
			break;
		case GameObjectProperty.YAngularVelocity:
			if (component != null)
			{
				Vector3 angularVelocity5 = component.angularVelocity;
				float x7 = angularVelocity5.x;
				Vector3 angularVelocity6 = component.angularVelocity;
				component.angularVelocity = new Vector3(x7, propertyValue, angularVelocity6.z);
			}
			else if (gameObject.transform != null)
			{
				gameObject.transform.Rotate(0f, propertyValue * Time.deltaTime, 0f);
			}
			break;
		case GameObjectProperty.ZAngularVelocity:
			if (component != null)
			{
				Vector3 angularVelocity3 = component.angularVelocity;
				float x5 = angularVelocity3.x;
				Vector3 angularVelocity4 = component.angularVelocity;
				component.angularVelocity = new Vector3(x5, angularVelocity4.y, propertyValue);
			}
			else if (gameObject.transform != null)
			{
				gameObject.transform.Rotate(0f, 0f, propertyValue * Time.deltaTime);
			}
			break;
		case GameObjectProperty.UniformScale:
			if ((bool)gameObject.transform)
			{
				gameObject.transform.localScale = new Vector3(propertyValue, propertyValue, propertyValue);
			}
			break;
		case GameObjectProperty.XScale:
			if ((bool)gameObject.transform)
			{
				Transform transform4 = gameObject.transform;
				Vector3 localScale5 = gameObject.transform.localScale;
				float y2 = localScale5.y;
				Vector3 localScale6 = gameObject.transform.localScale;
				transform4.localScale = new Vector3(propertyValue, y2, localScale6.z);
			}
			break;
		case GameObjectProperty.YScale:
			if ((bool)gameObject.transform)
			{
				Transform transform2 = gameObject.transform;
				Vector3 localScale3 = gameObject.transform.localScale;
				float x2 = localScale3.x;
				Vector3 localScale4 = gameObject.transform.localScale;
				transform2.localScale = new Vector3(x2, propertyValue, localScale4.z);
			}
			break;
		case GameObjectProperty.ZScale:
			if ((bool)gameObject.transform)
			{
				Transform transform = gameObject.transform;
				Vector3 localScale = gameObject.transform.localScale;
				float x = localScale.x;
				Vector3 localScale2 = gameObject.transform.localScale;
				transform.localScale = new Vector3(x, localScale2.y, propertyValue);
			}
			break;
		}
	}

	public static void SetLightProperty(Light light, LightProperty targetProperty, float propertyValue)
	{
		if (!(light == null))
		{
			switch (targetProperty)
			{
			case LightProperty.ShadowSoftness_DEPRECATED:
			case LightProperty.ShadowSoftnessFade_DEPRECATED:
				break;
			case LightProperty.ColorRed:
			{
				Color color11 = light.color;
				float g3 = color11.g;
				Color color12 = light.color;
				float b2 = color12.b;
				Color color13 = light.color;
				light.color = new Color(propertyValue, g3, b2, color13.a);
				break;
			}
			case LightProperty.ColorGreen:
			{
				Color color8 = light.color;
				float r3 = color8.r;
				Color color9 = light.color;
				float b = color9.b;
				Color color10 = light.color;
				light.color = new Color(r3, propertyValue, b, color10.a);
				break;
			}
			case LightProperty.ColorBlue:
			{
				Color color5 = light.color;
				float r2 = color5.r;
				Color color6 = light.color;
				float g2 = color6.g;
				Color color7 = light.color;
				light.color = new Color(r2, g2, propertyValue, color7.a);
				break;
			}
			case LightProperty.ColorRGB:
			{
				Color color4 = light.color;
				light.color = new Color(propertyValue, propertyValue, propertyValue, color4.a);
				break;
			}
			case LightProperty.ColorAlpha:
			{
				Color color = light.color;
				float r = color.r;
				Color color2 = light.color;
				float g = color2.g;
				Color color3 = light.color;
				light.color = new Color(r, g, color3.b, propertyValue);
				break;
			}
			case LightProperty.Intensity:
				light.intensity = propertyValue;
				break;
			case LightProperty.Range:
				light.range = propertyValue;
				break;
			case LightProperty.SpotAngle:
				light.spotAngle = propertyValue;
				break;
			case LightProperty.ShadowBias:
				light.shadowBias = propertyValue;
				break;
			case LightProperty.ShadowStrength:
				light.shadowStrength = propertyValue;
				break;
			}
		}
	}

	public static void SetAnimationStateProperty(AnimationState animationState, AnimationStateProperty targetProperty, float propertyValue)
	{
		if (!(animationState == null))
		{
			switch (targetProperty)
			{
			case AnimationStateProperty.Speed:
				animationState.speed = propertyValue;
				break;
			case AnimationStateProperty.NormalizedTime:
				animationState.normalizedTime = propertyValue;
				break;
			case AnimationStateProperty.Weight:
				animationState.weight = propertyValue;
				break;
			}
		}
	}
}
