using PartaGames.Android;
using UnityEngine;
using UnityEngine.UI;

public class TestPermissions : MonoBehaviour
{
	public Text text;

	private static readonly string WRITE_EXTERNAL_STORAGE = "android.permission.WRITE_EXTERNAL_STORAGE";

	private static readonly string CAMERA = "android.permission.CAMERA";

	public void CheckShouldShowRequestPermissionRationale()
	{
		Text obj = text;
		obj.text = obj.text + "\n Should show request permission rationale: " + PermissionGranterUnity.ShouldShowRequestPermissionRationale(WRITE_EXTERNAL_STORAGE);
	}

	public void CheckStorage()
	{
		Text obj = text;
		obj.text = obj.text + "\nCheck permission: WRITE_EXTERNAL_STORAGE: " + ((!PermissionGranterUnity.IsPermissionGranted(WRITE_EXTERNAL_STORAGE)) ? "No" : "Yes");
	}

	public void CheckCamera()
	{
		Text obj = text;
		obj.text = obj.text + "\nCheck permission: CAMERA: " + ((!PermissionGranterUnity.IsPermissionGranted(CAMERA)) ? "No" : "Yes");
	}

	public void GrantStorage()
	{
		PermissionGranterUnity.GrantPermission(WRITE_EXTERNAL_STORAGE, PermissionGrantedCallback);
	}

	public void GrantCamera()
	{
		PermissionGranterUnity.GrantPermission(CAMERA, PermissionGrantedCallback);
	}

	public void GrantMultiplePerm()
	{
		PermissionGranterUnity.GrantPermissions(new string[2]
		{
			WRITE_EXTERNAL_STORAGE,
			CAMERA
		}, PermissionGrantedCallback);
	}

	public void PermissionGrantedCallback(string permission, bool isGranted)
	{
		Text obj = this.text;
		string text = obj.text;
		obj.text = text + "\nPermission granted: " + permission + ": " + ((!isGranted) ? "No" : "Yes");
	}
}
