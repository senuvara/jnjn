using UnityEngine;

[AddComponentMenu("Visualizer Studio/Triggers/Material Property Trigger")]
public class VisMaterialPropertyTrigger : VisBasePropertyTrigger
{
	public new static class Defaults
	{
		public const string targetProperty = "";

		public const bool setAsProceduralMaterial = false;

		public const bool rebuildProceduralTexturesImmediately = false;

		public const bool applyToMaterialIndex = false;

		public const int materialIndex = 0;
	}

	public string targetProperty = string.Empty;

	public bool setAsProceduralMaterial;

	public bool rebuildProceduralTexturesImmediately;

	public bool applyToMaterialIndex;

	public int materialIndex;

	private bool specialTargetProperty;

	public override void Reset()
	{
		base.Reset();
		targetProperty = string.Empty;
		setAsProceduralMaterial = false;
		rebuildProceduralTexturesImmediately = false;
		applyToMaterialIndex = false;
		materialIndex = 0;
	}

	public override void Start()
	{
		base.Start();
		switch (targetProperty.ToLower())
		{
		case "colorr":
		case "colorred":
		case "colorg":
		case "colorgreen":
		case "colorb":
		case "colorblue":
		case "colora":
		case "coloralpha":
		case "color":
			specialTargetProperty = true;
			break;
		default:
			specialTargetProperty = false;
			break;
		}
	}

	public override void SetProperty(float propertyValue)
	{
		Material material = null;
		Renderer component = GetComponent<Renderer>();
		if (setAsProceduralMaterial && applyToMaterialIndex && component != null && materialIndex >= 0 && materialIndex < component.sharedMaterials.Length && (bool)(component.sharedMaterials[materialIndex] as ProceduralMaterial))
		{
			material = component.sharedMaterials[materialIndex];
		}
		else if (setAsProceduralMaterial && component != null && component.sharedMaterial != null && (bool)(component.sharedMaterial as ProceduralMaterial))
		{
			material = component.sharedMaterial;
		}
		else if (applyToMaterialIndex && component != null && materialIndex >= 0 && materialIndex < component.materials.Length)
		{
			material = component.materials[materialIndex];
		}
		else if (component != null && component.material != null)
		{
			material = component.material;
		}
		if (!(material != null))
		{
			return;
		}
		if (setAsProceduralMaterial && (bool)(material as ProceduralMaterial))
		{
			ProceduralMaterial proceduralMaterial = material as ProceduralMaterial;
			float proceduralFloat = proceduralMaterial.GetProceduralFloat(targetProperty);
			if (proceduralFloat != propertyValue)
			{
				proceduralMaterial.SetProceduralFloat(targetProperty, propertyValue);
				if (rebuildProceduralTexturesImmediately)
				{
					proceduralMaterial.RebuildTexturesImmediately();
				}
				else
				{
					proceduralMaterial.RebuildTextures();
				}
			}
		}
		else if (specialTargetProperty)
		{
			Color color2;
			switch (targetProperty.ToLower())
			{
			case "colorr":
			case "colorred":
			{
				float num = Mathf.Clamp(propertyValue, 0f, 1f);
				float r5 = num;
				Color color12 = material.color;
				float g5 = color12.g;
				Color color13 = material.color;
				float b4 = color13.b;
				Color color14 = material.color;
				color2 = new Color(r5, g5, b4, color14.a);
				material.color = color2;
				break;
			}
			case "colorg":
			case "colorgreen":
			{
				float num = Mathf.Clamp(propertyValue, 0f, 1f);
				Color color9 = material.color;
				float r4 = color9.r;
				float g4 = num;
				Color color10 = material.color;
				float b3 = color10.b;
				Color color11 = material.color;
				color2 = new Color(r4, g4, b3, color11.a);
				material.color = color2;
				break;
			}
			case "colorb":
			case "colorblue":
			{
				float num = Mathf.Clamp(propertyValue, 0f, 1f);
				Color color6 = material.color;
				float r3 = color6.r;
				Color color7 = material.color;
				float g3 = color7.g;
				float b2 = num;
				Color color8 = material.color;
				color2 = new Color(r3, g3, b2, color8.a);
				material.color = color2;
				break;
			}
			case "colora":
			case "coloralpha":
			{
				float num = Mathf.Clamp(propertyValue, 0f, 1f);
				Color color3 = material.color;
				float r2 = color3.r;
				Color color4 = material.color;
				float g2 = color4.g;
				Color color5 = material.color;
				color2 = new Color(r2, g2, color5.b, num);
				material.color = color2;
				break;
			}
			case "color":
			{
				float num = Mathf.Clamp(propertyValue, 0f, 1f);
				float r = num;
				float g = num;
				float b = num;
				Color color = material.color;
				color2 = new Color(r, g, b, color.a);
				material.color = color2;
				break;
			}
			default:
				material.SetFloat(targetProperty, propertyValue);
				break;
			}
		}
		else
		{
			material.SetFloat(targetProperty, propertyValue);
		}
	}
}
