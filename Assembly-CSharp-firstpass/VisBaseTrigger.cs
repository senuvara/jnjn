using UnityEngine;

public abstract class VisBaseTrigger : MonoBehaviour, IVisManagerTarget, IVisBaseControllerTarget
{
	public static class Defaults
	{
		public const TriggerType triggerType = TriggerType.GreaterThanChangeThreshold;

		public const float triggerThreshold = 0.1f;

		public const float triggerReactivateDelay = 0.25f;

		public const float triggerRandomReactivateDelay = 0f;
	}

	public enum TriggerType
	{
		None,
		LessThanValueThreshold,
		GreaterThanValueThreshold,
		LessThanChangeThreshold,
		GreaterThanChangeThreshold
	}

	[SerializeField]
	private VisManager m_oVisManager;

	[HideInInspector]
	[SerializeField]
	private string m_szLastVisManagerName;

	[SerializeField]
	private VisBaseController controller;

	[HideInInspector]
	[SerializeField]
	private string m_szLastControllerName;

	public TriggerType triggerType = TriggerType.GreaterThanChangeThreshold;

	public float triggerThreshold = 0.1f;

	public float triggerReactivateDelay = 0.25f;

	public float triggerRandomReactivateDelay;

	protected float m_fTriggerDelayTimer;

	public VisManager Manager
	{
		get
		{
			return m_oVisManager;
		}
		set
		{
			m_oVisManager = value;
			if (m_oVisManager != null)
			{
				m_szLastVisManagerName = m_oVisManager.name;
			}
			else
			{
				m_szLastVisManagerName = null;
			}
		}
	}

	public string LastManagerName => m_szLastVisManagerName;

	public VisBaseController Controller
	{
		get
		{
			return controller;
		}
		set
		{
			controller = value;
			if (controller != null)
			{
				m_szLastControllerName = controller.controllerName;
			}
			else
			{
				m_szLastControllerName = null;
			}
		}
	}

	public string LastControllerName => m_szLastControllerName;

	public virtual void Reset()
	{
		triggerType = TriggerType.GreaterThanChangeThreshold;
		triggerThreshold = 0.1f;
		triggerReactivateDelay = 0.25f;
	}

	public virtual void Start()
	{
		VisManager.RestoreVisManagerTarget(this);
		VisBaseController.RestoreVisBaseControllerTarget(this);
		triggerThreshold = VisHelper.Validate(triggerThreshold, 0.0001f, 10000f, 0.1f, this, "triggerThreshold", error: false);
		triggerReactivateDelay = VisHelper.Validate(triggerReactivateDelay, 0f, 10000f, 0.25f, this, "triggerReactivateDelay", error: false);
	}

	public virtual void OnDestroy()
	{
	}

	public virtual void Update()
	{
		if (!(Controller != null) || !Controller.enabled)
		{
			return;
		}
		if (m_fTriggerDelayTimer > 0f)
		{
			m_fTriggerDelayTimer -= Time.deltaTime;
		}
		else
		{
			if (triggerType == TriggerType.None)
			{
				return;
			}
			bool flag = false;
			switch (triggerType)
			{
			case TriggerType.LessThanValueThreshold:
				if (Controller.GetCurrentValue() < triggerThreshold)
				{
					flag = true;
				}
				break;
			case TriggerType.GreaterThanValueThreshold:
				if (Controller.GetCurrentValue() > triggerThreshold)
				{
					flag = true;
				}
				break;
			case TriggerType.LessThanChangeThreshold:
				if (Controller.GetAdjustedValueDifference() < triggerThreshold)
				{
					flag = true;
				}
				break;
			case TriggerType.GreaterThanChangeThreshold:
				if (Controller.GetAdjustedValueDifference() > triggerThreshold)
				{
					flag = true;
				}
				break;
			}
			if (flag)
			{
				m_fTriggerDelayTimer = triggerReactivateDelay + Random.Range(0f - triggerRandomReactivateDelay, triggerRandomReactivateDelay);
				if (m_fTriggerDelayTimer < 0f)
				{
					m_fTriggerDelayTimer = 0f;
				}
				OnTriggered(Controller.GetCurrentValue(), Controller.GetPreviousValue(), Controller.GetValueDifference(), Controller.GetAdjustedValueDifference());
			}
		}
	}

	public abstract void OnTriggered(float current, float previous, float difference, float adjustedDifference);
}
