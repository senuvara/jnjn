using System;
using UnityEngine;

namespace SA.Foundation.Localization
{
	[Serializable]
	public class SA_ISOLanguage
	{
		[SerializeField]
		private string m_code;

		[SerializeField]
		private string m_name;

		[SerializeField]
		private string m_nativeName;

		public string Code => m_code;

		public string Name => m_name;

		public string NativeName => m_nativeName;
	}
}
