using System;
using System.Collections.Generic;
using UnityEngine;

namespace SA.Foundation.Localization
{
	[Serializable]
	public class SA_ISOLanguagesList
	{
		[SerializeField]
		private List<SA_ISOLanguage> m_lanuages = new List<SA_ISOLanguage>();

		private List<string> m_names;

		public List<SA_ISOLanguage> Languages => m_lanuages;

		public List<string> Names
		{
			get
			{
				if (m_names == null)
				{
					m_names = new List<string>();
					foreach (SA_ISOLanguage lanuage in m_lanuages)
					{
						m_names.Add(lanuage.Name);
					}
				}
				return m_names;
			}
		}
	}
}
