using UnityEngine;

namespace SA.Foundation.Localization
{
	public static class SA_LanguagesUtil
	{
		private static SA_ISOLanguagesList m_isoLanguages;

		public static SA_ISOLanguagesList ISOLanguagesList
		{
			get
			{
				if (m_isoLanguages == null)
				{
					TextAsset textAsset = Resources.Load("iso_languages") as TextAsset;
					m_isoLanguages = JsonUtility.FromJson<SA_ISOLanguagesList>(textAsset.text);
				}
				return m_isoLanguages;
			}
		}
	}
}
