using System;
using System.Collections.Generic;
using System.Collections.ObjectModel;
using UnityEngine;

[AddComponentMenu("Visualizer Studio/Manager")]
public sealed class VisManager : MonoBehaviour
{
	public static class Defaults
	{
		public const Channel channel = Channel.Left;

		public const WindowSize windowSize = WindowSize._1024;

		public const FFTWindow windowType = FFTWindow.Hamming;

		public const bool useAudioListener = false;

		public const bool displaySpectrumDebug = false;

		public const bool displayDataGroupDebug = false;

		public const bool displayControllerDebug = false;

		public const int debugSpectrumBarWidth = 1;

		public const int debugSpectrumBarHeight = 80;

		public const int debugRawAudioBarHeight = 40;

		public const int debugDataGroupBarWidth = 4;

		public const int debugDataGroupBarHeight = 40;

		public const int debugControllerBarWidth = 150;

		public const int debugControllerBarHeight = 6;

		public const int debugSeparation = 5;
	}

	public enum WindowSize
	{
		_64 = 0x40,
		_128 = 0x80,
		_256 = 0x100,
		_512 = 0x200,
		_1024 = 0x400,
		_2048 = 0x800,
		_4096 = 0x1000,
		_8192 = 0x2000
	}

	public enum Channel
	{
		Left,
		Right,
		Average,
		Min,
		Max
	}

	public Channel channel;

	public WindowSize windowSize = WindowSize._1024;

	public FFTWindow windowType = FFTWindow.Hamming;

	public AudioSource audioSource;

	public bool displaySpectrumDebug;

	public bool displayDataGroupDebug;

	public bool displayControllerDebug;

	public int debugSpectrumBarWidth = 1;

	public int debugSpectrumBarHeight = 80;

	public int debugRawAudioBarHeight = 40;

	public int debugDataGroupBarWidth = 4;

	public int debugDataGroupBarHeight = 40;

	public int debugControllerBarWidth = 150;

	public int debugControllerBarHeight = 6;

	public int debugSeparation = 5;

	public Texture debugTexture;

	private float[] m_afSpectrumData;

	private float[] m_afRawAudioData;

	private float[] m_afAuxiliaryData;

	private List<VisDataGroup> m_oDataGroups = new List<VisDataGroup>();

	private List<VisBaseController> m_oControllers = new List<VisBaseController>();

	private float m_fMaxDebugValue = 0.1f;

	private int m_nFrequencyRange;

	private float m_fFrequencyResolution;

	private bool m_bUseAudioListener;

	public ReadOnlyCollection<VisDataGroup> DataGroups
	{
		get
		{
			if (m_oDataGroups == null)
			{
				m_oDataGroups = new List<VisDataGroup>();
			}
			return m_oDataGroups.AsReadOnly();
		}
	}

	public ReadOnlyCollection<VisBaseController> Controllers
	{
		get
		{
			if (m_oControllers == null)
			{
				m_oControllers = new List<VisBaseController>();
			}
			return m_oControllers.AsReadOnly();
		}
	}

	public float FrequencyResolution => m_fFrequencyResolution;

	public void Reset()
	{
		channel = Channel.Left;
		windowSize = WindowSize._1024;
		windowType = FFTWindow.Hamming;
		m_bUseAudioListener = false;
		displaySpectrumDebug = false;
		displayDataGroupDebug = false;
		displayControllerDebug = false;
		debugSpectrumBarWidth = 1;
		debugSpectrumBarHeight = 80;
		debugRawAudioBarHeight = 40;
		debugDataGroupBarWidth = 4;
		debugDataGroupBarHeight = 40;
		debugControllerBarWidth = 150;
		debugControllerBarHeight = 6;
		debugSeparation = 5;
		debugTexture = null;
	}

	public void Start()
	{
		debugSpectrumBarWidth = VisHelper.Validate(debugSpectrumBarWidth, 1, 1000, 1, this, "debugSpectrumBarWidth", error: false);
		debugSpectrumBarHeight = VisHelper.Validate(debugSpectrumBarHeight, 1, 1000, 80, this, "debugSpectrumBarHeight", error: false);
		debugRawAudioBarHeight = VisHelper.Validate(debugRawAudioBarHeight, 1, 1000, 40, this, "debugRawAudioBarHeight", error: false);
		debugDataGroupBarWidth = VisHelper.Validate(debugDataGroupBarWidth, 1, 1000, 4, this, "debugDataGroupBarWidth", error: false);
		debugDataGroupBarHeight = VisHelper.Validate(debugDataGroupBarHeight, 1, 1000, 40, this, "debugDataGroupBarHeight", error: false);
		debugControllerBarWidth = VisHelper.Validate(debugControllerBarWidth, 1, 1000, 150, this, "debugControllerBarWidth", error: false);
		debugControllerBarHeight = VisHelper.Validate(debugControllerBarHeight, 1, 1000, 6, this, "debugControllerBarHeight", error: false);
		debugSeparation = VisHelper.Validate(debugSeparation, 0, 1000, 5, this, "debugSeparation", error: false);
		m_afSpectrumData = new float[(int)windowSize];
		m_afRawAudioData = new float[(int)windowSize];
		m_afAuxiliaryData = new float[(int)windowSize];
		for (int i = 0; i < (int)windowSize; i++)
		{
			m_afSpectrumData[i] = 0f;
			m_afRawAudioData[i] = 0f;
			m_afAuxiliaryData[i] = 0f;
		}
		if (debugTexture == null)
		{
			debugTexture = (Texture)Resources.Load("1x1White");
		}
		if (audioSource == null)
		{
			Debug.LogWarning("A VisManager should have an Audio Source assigned to it so it only listens to the specified music. This manager will now default to using the static Audio Listener, which contains ALL audio playing in the game.  This Audio Listener does not work near as well as listening directly to an Audio Source, so it is NOT recommended.");
			m_bUseAudioListener = true;
		}
		CalculateFrequencyResolution();
	}

	public void SetAudioSource(AudioSource _audioSource)
	{
		audioSource = _audioSource;
		Start();
	}

	public void CalculateFrequencyResolution()
	{
		if (!m_bUseAudioListener && audioSource != null && audioSource.clip != null)
		{
			m_nFrequencyRange = audioSource.clip.frequency;
			m_fFrequencyResolution = (float)m_nFrequencyRange / (float)windowSize;
		}
		else
		{
			m_fFrequencyResolution = 0f;
		}
	}

	private void Update()
	{
		if (m_bUseAudioListener)
		{
			if (channel == Channel.Left || channel == Channel.Right)
			{
				AudioListener.GetSpectrumData(m_afSpectrumData, (int)channel, windowType);
				AudioListener.GetOutputData(m_afRawAudioData, (int)channel);
			}
			else
			{
				AudioListener.GetSpectrumData(m_afSpectrumData, 0, windowType);
				AudioListener.GetSpectrumData(m_afAuxiliaryData, 1, windowType);
				for (int i = 0; i < (int)windowSize; i++)
				{
					if (channel == Channel.Average)
					{
						m_afSpectrumData[i] = (m_afSpectrumData[i] + m_afAuxiliaryData[i]) * 0.5f;
					}
					else if (channel == Channel.Min)
					{
						m_afSpectrumData[i] = Mathf.Min(m_afSpectrumData[i], m_afAuxiliaryData[i]);
					}
					else if (channel == Channel.Max)
					{
						m_afSpectrumData[i] = Mathf.Max(m_afSpectrumData[i], m_afAuxiliaryData[i]);
					}
				}
				AudioListener.GetOutputData(m_afRawAudioData, 0);
				AudioListener.GetOutputData(m_afAuxiliaryData, 1);
				for (int j = 0; j < (int)windowSize; j++)
				{
					if (channel == Channel.Average)
					{
						m_afRawAudioData[j] = (m_afRawAudioData[j] + m_afAuxiliaryData[j]) * 0.5f;
					}
					else if (channel == Channel.Min)
					{
						if (Mathf.Abs(m_afAuxiliaryData[j]) < Mathf.Abs(m_afRawAudioData[j]))
						{
							m_afRawAudioData[j] = m_afAuxiliaryData[j];
						}
					}
					else if (channel == Channel.Max && Mathf.Abs(m_afAuxiliaryData[j]) > Mathf.Abs(m_afRawAudioData[j]))
					{
						m_afRawAudioData[j] = m_afAuxiliaryData[j];
					}
				}
			}
		}
		else if (audioSource != null)
		{
			if (channel == Channel.Left || channel == Channel.Right)
			{
				audioSource.GetSpectrumData(m_afSpectrumData, (int)channel, windowType);
				audioSource.GetOutputData(m_afRawAudioData, (int)channel);
			}
			else
			{
				audioSource.GetSpectrumData(m_afSpectrumData, 0, windowType);
				audioSource.GetSpectrumData(m_afAuxiliaryData, 1, windowType);
				for (int k = 0; k < (int)windowSize; k++)
				{
					if (channel == Channel.Average)
					{
						m_afSpectrumData[k] = (m_afSpectrumData[k] + m_afAuxiliaryData[k]) * 0.5f;
					}
					else if (channel == Channel.Min)
					{
						m_afSpectrumData[k] = Mathf.Min(m_afSpectrumData[k], m_afAuxiliaryData[k]);
					}
					else if (channel == Channel.Max)
					{
						m_afSpectrumData[k] = Mathf.Max(m_afSpectrumData[k], m_afAuxiliaryData[k]);
					}
				}
				audioSource.GetOutputData(m_afRawAudioData, 0);
				audioSource.GetOutputData(m_afAuxiliaryData, 1);
				for (int l = 0; l < (int)windowSize; l++)
				{
					if (channel == Channel.Average)
					{
						m_afRawAudioData[l] = (m_afRawAudioData[l] + m_afAuxiliaryData[l]) * 0.5f;
					}
					else if (channel == Channel.Min)
					{
						if (Mathf.Abs(m_afAuxiliaryData[l]) < Mathf.Abs(m_afRawAudioData[l]))
						{
							m_afRawAudioData[l] = m_afAuxiliaryData[l];
						}
					}
					else if (channel == Channel.Max && Mathf.Abs(m_afAuxiliaryData[l]) > Mathf.Abs(m_afRawAudioData[l]))
					{
						m_afRawAudioData[l] = m_afAuxiliaryData[l];
					}
				}
			}
		}
		if (displaySpectrumDebug && debugTexture != null)
		{
			m_fMaxDebugValue -= 0.05f * Time.deltaTime;
			if (m_fMaxDebugValue < 0.1f)
			{
				m_fMaxDebugValue = 0.1f;
			}
		}
	}

	public void AddDataGroup(VisDataGroup dataGroup)
	{
		if (!(dataGroup != null) || m_oDataGroups == null || m_oDataGroups.Contains(dataGroup) || (!(dataGroup.Manager != this) && !(GetDataGroupByName(dataGroup.dataGroupName) == null)))
		{
			return;
		}
		for (int i = 0; i < m_oDataGroups.Count; i++)
		{
			if (m_oDataGroups[i].name == dataGroup.name && m_oDataGroups[i].dataGroupName == dataGroup.dataGroupName)
			{
				return;
			}
		}
		dataGroup.dataGroupName = EnsureUniqueDataGroupName(dataGroup.dataGroupName);
		m_oDataGroups.Add(dataGroup);
		for (int j = 0; j < m_oDataGroups.Count; j++)
		{
			if (m_oDataGroups[j] != dataGroup && m_oDataGroups[j] != null)
			{
				bool flag = false;
				if (dataGroup.frequencyRangeStartIndex >= m_oDataGroups[j].frequencyRangeStartIndex && dataGroup.frequencyRangeStartIndex <= m_oDataGroups[j].frequencyRangeEndIndex)
				{
					flag = true;
				}
				else if (dataGroup.frequencyRangeEndIndex >= m_oDataGroups[j].frequencyRangeStartIndex && dataGroup.frequencyRangeEndIndex <= m_oDataGroups[j].frequencyRangeEndIndex)
				{
					flag = true;
				}
				else if (m_oDataGroups[j].frequencyRangeStartIndex >= dataGroup.frequencyRangeStartIndex && m_oDataGroups[j].frequencyRangeStartIndex <= dataGroup.frequencyRangeEndIndex)
				{
					flag = true;
				}
				else if (m_oDataGroups[j].frequencyRangeEndIndex >= dataGroup.frequencyRangeStartIndex && m_oDataGroups[j].frequencyRangeEndIndex <= dataGroup.frequencyRangeEndIndex)
				{
					flag = true;
				}
				if (flag)
				{
					Debug.LogWarning("Data Group \"" + dataGroup.dataGroupName + "\" has its frequency range overlapping with Data Group \"" + m_oDataGroups[j].dataGroupName + "\".  This is not recommended due to performance considerations.");
				}
			}
		}
	}

	public void AddController(VisBaseController controller)
	{
		if (!(controller != null) || m_oControllers.Contains(controller) || (!(controller.Manager != this) && !(GetControllerByName(controller.controllerName) == null)))
		{
			return;
		}
		for (int i = 0; i < m_oControllers.Count; i++)
		{
			if (m_oControllers[i].name == controller.name && m_oControllers[i].controllerName == controller.controllerName)
			{
				return;
			}
		}
		controller.controllerName = EnsureUniqueControllerName(controller.controllerName);
		m_oControllers.Add(controller);
	}

	public void RemoveDataGroup(VisDataGroup dataGroup)
	{
		if (dataGroup != null && m_oDataGroups.Contains(dataGroup))
		{
			m_oDataGroups.Remove(dataGroup);
		}
	}

	public void RemoveController(VisBaseController controller)
	{
		if (controller != null && m_oControllers.Contains(controller))
		{
			m_oControllers.Remove(controller);
		}
	}

	public void ClearDataGroups()
	{
		m_oDataGroups.Clear();
	}

	public void ClearControllers()
	{
		m_oControllers.Clear();
	}

	public float[] GetSpectrumData()
	{
		return m_afSpectrumData;
	}

	public float[] GetRawAudioData()
	{
		return m_afRawAudioData;
	}

	public VisDataGroup GetDataGroupByName(string dataGroupName)
	{
		for (int i = 0; i < m_oDataGroups.Count; i++)
		{
			if (m_oDataGroups[i].dataGroupName == dataGroupName)
			{
				return m_oDataGroups[i];
			}
		}
		return null;
	}

	public VisBaseController GetControllerByName(string controllerName)
	{
		for (int i = 0; i < m_oControllers.Count; i++)
		{
			if (m_oControllers[i].controllerName == controllerName)
			{
				return m_oControllers[i];
			}
		}
		return null;
	}

	public string EnsureUniqueControllerName(string name)
	{
		int num = 1;
		string text = name;
		bool flag = true;
		do
		{
			flag = true;
			for (int i = 0; i < m_oControllers.Count; i++)
			{
				if (text.Equals(m_oControllers[i].controllerName, StringComparison.CurrentCultureIgnoreCase))
				{
					flag = false;
				}
			}
			if (!flag)
			{
				text = name + "_" + num++;
			}
		}
		while (!flag);
		return text;
	}

	public string EnsureUniqueDataGroupName(string name)
	{
		int num = 1;
		string text = name;
		bool flag = true;
		do
		{
			flag = true;
			for (int i = 0; i < m_oDataGroups.Count; i++)
			{
				if (text.Equals(m_oDataGroups[i].dataGroupName, StringComparison.CurrentCultureIgnoreCase))
				{
					flag = false;
				}
			}
			if (!flag)
			{
				text = name + "_" + num++;
			}
		}
		while (!flag);
		return text;
	}

	private void OnGUI()
	{
		if ((!displaySpectrumDebug && !displayDataGroupDebug && !displayControllerDebug) || !(debugTexture != null))
		{
			return;
		}
		int num = 10;
		int num2 = 20;
		int num3 = debugSeparation;
		int num4 = 20;
		int num5 = 5;
		int num6 = debugSpectrumBarWidth * (int)windowSize + num5 * 2;
		int num7 = debugSpectrumBarHeight + debugRawAudioBarHeight + num5 * 3 + num4 * 4;
		int num8 = 0;
		if (displaySpectrumDebug)
		{
			Rect position = new Rect(num - num5, num2 - num5, num6, num7);
			GUI.BeginGroup(position);
			GUI.color = new Color(0f, 0f, 0f, 0.5f);
			GUI.DrawTexture(new Rect(0f, 0f, position.width, position.height), debugTexture);
			GUI.color = Color.white;
			GUI.Label(new Rect(num5, num5, 200f, num4 + 3), "Range: " + ((m_nFrequencyRange <= 0) ? "N/A" : (m_nFrequencyRange + " Hz")));
			GUI.Label(new Rect(num6 - 80 - num5, num5, 80f, num4 + 3), "Max: " + m_fMaxDebugValue.ToString("F4"));
			GUI.Label(new Rect(num5, num5 + num4, 200f, num4 + 3), "Resolution: " + ((!(m_fFrequencyResolution > 0f)) ? "N/A" : (m_fFrequencyResolution.ToString("F1") + " Hz")));
			GUI.Label(new Rect(num5, num5 + num4 * 2, 200f, num4 + 3), "Spectrum");
			for (int i = 0; i < m_afSpectrumData.Length; i++)
			{
				float num9 = m_afSpectrumData[i];
				if (num9 > m_fMaxDebugValue)
				{
					m_fMaxDebugValue = num9;
				}
				float num10 = (!(m_fMaxDebugValue > 0f)) ? 0f : (num9 / m_fMaxDebugValue);
				int num11 = (int)(((float)debugSpectrumBarHeight - 1f) * num10) + 1;
				GUI.color = GetDebugColor(i, VisDataSource.Spectrum);
				GUI.DrawTexture(new Rect(num5 + debugSpectrumBarWidth * i, num4 * 3 + num5 + debugSpectrumBarHeight - num11, debugSpectrumBarWidth, num11), debugTexture);
			}
			GUI.Label(new Rect(num5, num5 + num4 * 3 + debugSpectrumBarHeight, 200f, num4 + 3), "Raw Audio");
			for (int j = 0; j < m_afRawAudioData.Length; j++)
			{
				float num12 = m_afRawAudioData[j];
				float num13 = Mathf.Abs(num12);
				int num14 = (int)(((float)(debugRawAudioBarHeight / 2) - 1f) * num13) + 1;
				GUI.color = GetDebugColor(j, VisDataSource.Raw);
				if (num12 < 0f)
				{
					GUI.DrawTexture(new Rect(num5 + debugSpectrumBarWidth * j, num4 * 4 + num5 * 2 + debugSpectrumBarHeight + debugRawAudioBarHeight / 2, debugSpectrumBarWidth, num14), debugTexture);
				}
				else
				{
					GUI.DrawTexture(new Rect(num5 + debugSpectrumBarWidth * j, num4 * 4 + num5 * 2 + debugSpectrumBarHeight + debugRawAudioBarHeight / 2 - num14 + 1, debugSpectrumBarWidth, num14), debugTexture);
				}
			}
			GUI.color = Color.white;
			GUI.DrawTexture(new Rect(0f, 0f, num6, 1f), debugTexture);
			GUI.DrawTexture(new Rect(0f, num7 - 1, num6, 1f), debugTexture);
			GUI.DrawTexture(new Rect(0f, 0f, 1f, num7), debugTexture);
			GUI.DrawTexture(new Rect(num6 - 1, 0f, 1f, num7), debugTexture);
			GUI.EndGroup();
			num2 += num7 + num3;
		}
		int num15 = (int)((float)Screen.width * 0.7f);
		if (displayDataGroupDebug)
		{
			for (int k = 0; k < m_oDataGroups.Count; k++)
			{
				Rect rect = m_oDataGroups[k].DisplayDebugGUI(num, num2, debugDataGroupBarWidth, debugDataGroupBarHeight, num3, debugTexture);
				if ((float)num8 < rect.height)
				{
					num8 = (int)rect.height;
				}
				num += (int)rect.width + num3;
				if (num > num15 || k == m_oDataGroups.Count - 1)
				{
					num = 10;
					num2 += num8 + num3;
				}
			}
		}
		if (!displayControllerDebug)
		{
			return;
		}
		num8 = 0;
		for (int l = 0; l < m_oControllers.Count; l++)
		{
			Rect rect2 = m_oControllers[l].DisplayDebugGUI(num, num2, debugControllerBarWidth, debugControllerBarHeight, num3, debugTexture);
			if ((float)num8 < rect2.height)
			{
				num8 = (int)rect2.height;
			}
			num += (int)rect2.width + num3;
			if (num > num15 || l == m_oControllers.Count - 1)
			{
				num = 10;
				num2 += num8 + num3;
			}
		}
	}

	private Color GetDebugColor(int freqIndex, VisDataSource dataSource)
	{
		for (int i = 0; i < m_oDataGroups.Count; i++)
		{
			if (m_oDataGroups[i] != null && m_oDataGroups[i].dataSource == dataSource && freqIndex >= m_oDataGroups[i].frequencyRangeStartIndex && freqIndex <= m_oDataGroups[i].frequencyRangeEndIndex)
			{
				return m_oDataGroups[i].debugColor;
			}
		}
		return Color.white;
	}

	public override string ToString()
	{
		return "VisManager \"" + base.name + "\"";
	}

	public static bool RestoreVisManagerTarget(IVisManagerTarget target)
	{
		if (target.LastManagerName != null && target.LastManagerName.Length > 0)
		{
			GameObject gameObject = GameObject.Find(target.LastManagerName);
			if (gameObject != null)
			{
				target.Manager = gameObject.GetComponent<VisManager>();
				if (target.Manager != null)
				{
					return true;
				}
			}
		}
		return false;
	}
}
