using UnityEngine;

public abstract class VisBaseController : MonoBehaviour, IVisManagerTarget
{
	public static class Defaults
	{
		public static int controllerNameCounter;

		public const string controllerName = "Default";

		public const bool limitIncreaseRate = false;

		public const float increaseRate = 1f;

		public const bool limitDecreaseRate = true;

		public const float decreaseRate = 1f;
	}

	public const float mc_fTargetAdjustedDifferenceTime = 0.0166666675f;

	[SerializeField]
	private VisManager m_oVisManager;

	[HideInInspector]
	[SerializeField]
	private string m_szLastVisManagerName;

	public string controllerName = string.Empty;

	public bool limitIncreaseRate;

	public float increaseRate = 1f;

	public bool limitDecreaseRate = true;

	public float decreaseRate = 1f;

	protected float m_fPreviousValue;

	protected float m_fValue;

	protected float m_fValueDifference;

	protected float m_fAdjustedValueDifference;

	private float m_fMinValue;

	private float m_fMaxValue = 1f;

	public VisManager Manager
	{
		get
		{
			return m_oVisManager;
		}
		set
		{
			if (m_oVisManager != null)
			{
				m_oVisManager.RemoveController(this);
			}
			if (value != null)
			{
				value.AddController(this);
			}
			m_oVisManager = value;
			if (m_oVisManager != null)
			{
				m_szLastVisManagerName = m_oVisManager.name;
			}
			else
			{
				m_szLastVisManagerName = null;
			}
		}
	}

	public string LastManagerName => m_szLastVisManagerName;

	public float MinValue => m_fMinValue;

	public float MaxValue => m_fMaxValue;

	public virtual void Reset()
	{
		controllerName = "Default" + ++Defaults.controllerNameCounter;
		limitIncreaseRate = false;
		increaseRate = 1f;
		limitDecreaseRate = true;
		decreaseRate = 1f;
	}

	public virtual void Awake()
	{
		VisManager.RestoreVisManagerTarget(this);
		if (m_oVisManager == null)
		{
			m_oVisManager = GetComponent<VisManager>();
			if (m_oVisManager == null)
			{
				Object[] array = Object.FindObjectsOfType(typeof(VisManager));
				for (int i = 0; i < array.Length; i++)
				{
					VisManager visManager = array[i] as VisManager;
					if (visManager.enabled)
					{
						m_oVisManager = visManager;
						break;
					}
				}
			}
		}
		ValidateManager(displayWarning: true);
		EnsureRegistered();
		if (m_oVisManager == null)
		{
			Debug.LogError("This Controller does not have a VisManager assigned to it, nor could it find an active VisManager. In order to function, this Controller needs a VisManager!");
		}
	}

	public virtual void Start()
	{
		increaseRate = VisHelper.Validate(increaseRate, 1E-05f, 10000f, 1f, this, "increaseRate", error: false);
		decreaseRate = VisHelper.Validate(decreaseRate, 1E-05f, 10000f, 1f, this, "decreaseRate", error: false);
	}

	public virtual void OnDestroy()
	{
		if (m_oVisManager != null)
		{
			m_oVisManager.RemoveController(this);
		}
	}

	public bool ValidateManager(bool displayWarning)
	{
		if (m_oVisManager != null && m_oVisManager.gameObject != base.gameObject)
		{
			if (displayWarning)
			{
				Debug.LogWarning("This Controller (" + controllerName + ") is in a different Game Object than it's Manager (" + m_oVisManager.name + ").  Please make sure it is attached to the same Game Object to prevent issues.", this);
			}
			return false;
		}
		return true;
	}

	public void EnsureRegistered()
	{
		if (m_oVisManager != null)
		{
			m_oVisManager.AddController(this);
		}
	}

	public void Update()
	{
		m_fPreviousValue = m_fValue;
		float customControllerValue = GetCustomControllerValue();
		if (customControllerValue < m_fValue && limitDecreaseRate)
		{
			m_fValue = Mathf.MoveTowards(m_fValue, customControllerValue, decreaseRate * Time.deltaTime);
		}
		else if (customControllerValue > m_fValue && limitIncreaseRate)
		{
			m_fValue = Mathf.MoveTowards(m_fValue, customControllerValue, increaseRate * Time.deltaTime);
		}
		else
		{
			m_fValue = customControllerValue;
		}
		m_fValueDifference = m_fValue - m_fPreviousValue;
		if (Mathf.Abs(m_fValueDifference) <= float.Epsilon)
		{
			m_fAdjustedValueDifference = 0f;
		}
		else
		{
			m_fAdjustedValueDifference = 1f / Mathf.Clamp(Time.deltaTime, 0.0001f, 0.5f) * m_fValueDifference / 59.9999962f;
		}
		if (m_fValue < m_fMinValue)
		{
			m_fMinValue = m_fValue;
		}
		if (m_fValue > m_fMaxValue)
		{
			m_fMaxValue = m_fValue;
		}
	}

	public virtual float GetCustomControllerValue()
	{
		return 0f;
	}

	public float GetCurrentValue()
	{
		return m_fValue;
	}

	public float GetPreviousValue()
	{
		return m_fPreviousValue;
	}

	public float GetValueDifference()
	{
		return m_fValueDifference;
	}

	public float GetAdjustedValueDifference()
	{
		return m_fAdjustedValueDifference;
	}

	public virtual Rect DisplayDebugGUI(int x, int y, int barWidth, int barHeight, int separation, Texture debugTexture)
	{
		if (debugTexture != null)
		{
			int num = 150;
			int num2 = 20;
			int num3 = 5;
			int num4 = Mathf.Max(barWidth, num) + num3 * 2;
			int num5 = num3 * 2 + num2 * 2 + barHeight;
			Rect rect = new Rect(x - num3, y - num3, num4, num5);
			GUI.BeginGroup(rect);
			GUI.color = new Color(0f, 0f, 0f, 0.5f);
			GUI.DrawTexture(new Rect(0f, 0f, rect.width, rect.height), debugTexture);
			GUI.color = Color.white;
			GUI.Label(new Rect(num3, num3, num, num2 + 3), "Controller: \"" + controllerName + "\"");
			GUI.Label(new Rect(num3, num3 + num2, num, num2 + 3), "VALUE: " + GetCurrentValue().ToString("F4"));
			float num6 = (m_fValue - m_fMinValue) / (m_fMaxValue - m_fMinValue) * 0.975f + 0.025f;
			GUI.DrawTexture(new Rect(num3, num3 + num2 * 2, (int)((float)barWidth * num6), barHeight), debugTexture);
			GUI.color = Color.white;
			GUI.DrawTexture(new Rect(0f, 0f, num4, 1f), debugTexture);
			GUI.DrawTexture(new Rect(0f, num5 - 1, num4, 1f), debugTexture);
			GUI.DrawTexture(new Rect(0f, 0f, 1f, num5), debugTexture);
			GUI.DrawTexture(new Rect(num4 - 1, 0f, 1f, num5), debugTexture);
			GUI.EndGroup();
			return rect;
		}
		return new Rect(0f, 0f, 0f, 0f);
	}

	public override string ToString()
	{
		return "VisBaseController \"" + controllerName + "\"";
	}

	public static bool RestoreVisBaseControllerTarget(IVisBaseControllerTarget target)
	{
		if (target.Controller == null && target.LastControllerName != null && target.LastControllerName.Length > 0)
		{
			VisManager visManager = null;
			if (target is IVisManagerTarget)
			{
				visManager = (target as IVisManagerTarget).Manager;
			}
			if (visManager != null)
			{
				for (int i = 0; i < visManager.Controllers.Count; i++)
				{
					if (visManager.Controllers[i].controllerName == target.LastControllerName)
					{
						target.Controller = visManager.Controllers[i];
						return true;
					}
				}
			}
		}
		return false;
	}
}
