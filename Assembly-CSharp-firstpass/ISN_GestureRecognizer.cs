using SA.Common.Pattern;
using System;

public class ISN_GestureRecognizer : Singleton<ISN_GestureRecognizer>
{
	public event Action<ISN_SwipeDirection> OnSwipe = delegate
	{
	};

	protected override void Awake()
	{
		base.Awake();
	}

	private void OnSwipeAction(string data)
	{
		int obj = Convert.ToInt32(data);
		this.OnSwipe((ISN_SwipeDirection)obj);
	}
}
