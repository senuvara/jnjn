using UnityEngine;

namespace SA.Common.Pattern
{
	internal static class SingletonParent
	{
		private static Transform s_transform;

		public static Transform Transform
		{
			get
			{
				if (s_transform == null)
				{
					GameObject gameObject = new GameObject("Singletons");
					Object.DontDestroyOnLoad(gameObject);
					s_transform = gameObject.transform;
				}
				return s_transform;
			}
		}
	}
}
