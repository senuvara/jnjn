using UnityEngine;

namespace SA.Common.Pattern
{
	public abstract class Singleton<T> : MonoBehaviour where T : MonoBehaviour
	{
		private static T s_instance;

		private static bool s_isApplicationIsQuitting;

		public static T Instance
		{
			get
			{
				if (s_isApplicationIsQuitting)
				{
					return (T)null;
				}
				Instantiate();
				return s_instance;
			}
		}

		public static bool HasInstance => !IsDestroyed;

		public static bool IsDestroyed
		{
			get
			{
				if ((Object)s_instance == (Object)null)
				{
					return true;
				}
				return false;
			}
		}

		protected virtual void Awake()
		{
			Object.DontDestroyOnLoad(base.gameObject);
			if (base.gameObject.transform.parent == null)
			{
				base.gameObject.transform.SetParent(SingletonParent.Transform);
			}
		}

		public static void Instantiate()
		{
			if ((Object)s_instance == (Object)null)
			{
				s_instance = (Object.FindObjectOfType(typeof(T)) as T);
				if ((Object)s_instance == (Object)null)
				{
					s_instance = new GameObject().AddComponent<T>();
					s_instance.gameObject.name = s_instance.GetType().FullName;
				}
			}
		}

		protected virtual void OnDestroy()
		{
			s_instance = (T)null;
			s_isApplicationIsQuitting = true;
		}

		protected virtual void OnApplicationQuit()
		{
			s_instance = (T)null;
			s_isApplicationIsQuitting = true;
		}
	}
}
