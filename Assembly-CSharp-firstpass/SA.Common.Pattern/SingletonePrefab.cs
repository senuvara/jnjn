using UnityEngine;

namespace SA.Common.Pattern
{
	public abstract class SingletonePrefab<T> : MonoBehaviour where T : MonoBehaviour
	{
		protected static T s_instance;

		public static T Instance
		{
			get
			{
				if ((Object)s_instance == (Object)null)
				{
					s_instance = (Object.FindObjectOfType(typeof(T)) as T);
					if ((Object)s_instance == (Object)null)
					{
						GameObject gameObject = Object.Instantiate(Resources.Load(typeof(T).Name)) as GameObject;
						s_instance = gameObject.GetComponent<T>();
						Object.DontDestroyOnLoad(gameObject);
					}
				}
				return s_instance;
			}
		}

		public static bool HasInstance => (Object)s_instance != (Object)null;
	}
}
