using UnityEngine;

public abstract class VisBaseModifier : MonoBehaviour, IVisManagerTarget, IVisBaseControllerTarget
{
	public static class Defaults
	{
	}

	[SerializeField]
	private VisManager m_oVisManager;

	[HideInInspector]
	[SerializeField]
	private string m_szLastVisManagerName;

	[SerializeField]
	private VisBaseController controller;

	[HideInInspector]
	[SerializeField]
	private string m_szLastControllerName;

	public VisManager Manager
	{
		get
		{
			return m_oVisManager;
		}
		set
		{
			m_oVisManager = value;
			if (m_oVisManager != null)
			{
				m_szLastVisManagerName = m_oVisManager.name;
			}
			else
			{
				m_szLastVisManagerName = null;
			}
		}
	}

	public string LastManagerName => m_szLastVisManagerName;

	public VisBaseController Controller
	{
		get
		{
			return controller;
		}
		set
		{
			controller = value;
			if (controller != null)
			{
				m_szLastControllerName = controller.controllerName;
			}
			else
			{
				m_szLastControllerName = null;
			}
		}
	}

	public string LastControllerName => m_szLastControllerName;

	public virtual void Reset()
	{
	}

	public virtual void Start()
	{
		VisManager.RestoreVisManagerTarget(this);
		VisBaseController.RestoreVisBaseControllerTarget(this);
	}

	public virtual void OnDestroy()
	{
	}

	public void Update()
	{
		if (Controller != null && Controller.enabled)
		{
			OnValueUpdated(Controller.GetCurrentValue(), Controller.GetPreviousValue(), Controller.GetValueDifference(), Controller.GetAdjustedValueDifference());
		}
	}

	public abstract void OnValueUpdated(float current, float previous, float difference, float adjustedDifference);
}
