using UnityEngine;

public class FrameRateLimiter : MonoBehaviour
{
	private void Start()
	{
		if (Application.targetFrameRate <= 0 || Application.targetFrameRate > 60)
		{
			Application.targetFrameRate = 60;
		}
	}
}
