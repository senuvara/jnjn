using UnityEngine;

[AddComponentMenu("Visualizer Studio/Triggers/Spawn Prefab Trigger")]
public class VisSpawnPrefabTrigger : VisBaseTrigger
{
	public new static class Defaults
	{
		public static readonly Vector3 randomOffset = Vector3.zero;
	}

	public GameObject prefab;

	public Vector3 randomOffset = Defaults.randomOffset;

	public override void Reset()
	{
		randomOffset = Defaults.randomOffset;
		base.Reset();
	}

	public override void Start()
	{
		base.Start();
	}

	public override void OnTriggered(float current, float previous, float difference, float adjustedDifference)
	{
		if (!(prefab != null) || !(base.transform != null))
		{
			return;
		}
		Vector3 b = new Vector3(Random.Range(0f - randomOffset.x, randomOffset.x), Random.Range(0f - randomOffset.y, randomOffset.y), Random.Range(0f - randomOffset.z, randomOffset.z));
		Object @object = Object.Instantiate(prefab, base.transform.position + b, base.transform.rotation);
		if (!(@object is GameObject))
		{
			return;
		}
		MonoBehaviour[] components = (@object as GameObject).GetComponents<MonoBehaviour>();
		for (int i = 0; i < components.Length; i++)
		{
			if (components[i].enabled && components[i] is IVisPrefabSpawnedTarget)
			{
				(components[i] as IVisPrefabSpawnedTarget).OnSpawned(current, previous, difference, adjustedDifference);
			}
		}
	}
}
