public interface IVisDataGroupTarget
{
	VisDataGroup DataGroup
	{
		get;
		set;
	}

	string LastDataGroupName
	{
		get;
	}
}
