using System;
using System.Collections.Generic;

namespace SA.Foundation.Events
{
	public class SA_Event<T> : SA_iEvent<T>, SA_iSafeEvent<T>
	{
		private class SafeActionInfo
		{
			public Action<T> Action;

			public object Target;
		}

		private List<SafeActionInfo> m_targetedActions = new List<SafeActionInfo>();

		public void AddListener(Action<T> listner)
		{
			if (listner != null)
			{
				SafeActionInfo safeActionInfo = new SafeActionInfo();
				safeActionInfo.Target = this;
				safeActionInfo.Action = listner;
				m_targetedActions.Add(safeActionInfo);
			}
		}

		public void AddSafeListener(object callbackTarget, Action<T> listner)
		{
			if (listner != null)
			{
				SafeActionInfo safeActionInfo = new SafeActionInfo();
				safeActionInfo.Target = callbackTarget;
				safeActionInfo.Action = listner;
				m_targetedActions.Add(safeActionInfo);
			}
		}

		public void Invoke(T obj)
		{
			List<SafeActionInfo> list = new List<SafeActionInfo>(m_targetedActions);
			foreach (SafeActionInfo item in list)
			{
				if (item.Target != null && !item.Target.Equals(null))
				{
					item.Action(obj);
				}
			}
		}

		public void RemoveListener(Action<T> listner)
		{
			foreach (SafeActionInfo targetedAction in m_targetedActions)
			{
				if (listner.Equals(targetedAction.Action))
				{
					m_targetedActions.Remove(targetedAction);
					break;
				}
			}
		}

		public void RemoveAllListeners()
		{
			m_targetedActions.Clear();
		}
	}
	public class SA_Event : SA_iEvent, SA_iSafeEvent
	{
		private class SafeActionInfo
		{
			public Action Action;

			public object Target;
		}

		private List<SafeActionInfo> m_targetedActions = new List<SafeActionInfo>();

		public void AddListener(Action listner)
		{
			if (listner != null)
			{
				SafeActionInfo safeActionInfo = new SafeActionInfo();
				safeActionInfo.Target = this;
				safeActionInfo.Action = listner;
				m_targetedActions.Add(safeActionInfo);
			}
		}

		public void AddSafeListener(object callbackTarget, Action listner)
		{
			if (listner != null)
			{
				SafeActionInfo safeActionInfo = new SafeActionInfo();
				safeActionInfo.Target = callbackTarget;
				safeActionInfo.Action = listner;
				m_targetedActions.Add(safeActionInfo);
			}
		}

		public void Invoke()
		{
			List<SafeActionInfo> list = new List<SafeActionInfo>(m_targetedActions);
			foreach (SafeActionInfo item in list)
			{
				if (item.Target != null && !item.Target.Equals(null))
				{
					item.Action();
				}
			}
		}

		public void RemoveListener(Action listner)
		{
			foreach (SafeActionInfo targetedAction in m_targetedActions)
			{
				if (listner.Equals(targetedAction.Action))
				{
					m_targetedActions.Remove(targetedAction);
					break;
				}
			}
		}

		public void RemoveAllListeners()
		{
			m_targetedActions.Clear();
		}
	}
}
