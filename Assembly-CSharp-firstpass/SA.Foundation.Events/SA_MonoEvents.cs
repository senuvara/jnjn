using SA.Foundation.Patterns;
using System;

namespace SA.Foundation.Events
{
	public class SA_MonoEvents : SA_Singleton<SA_MonoEvents>
	{
		public event Action ApplicationQuit = delegate
		{
		};

		public event Action<bool> ApplicationFocus = delegate
		{
		};

		public event Action<bool> ApplicationPause = delegate
		{
		};

		public event Action OnUpdate = delegate
		{
		};

		protected override void OnApplicationQuit()
		{
			base.OnApplicationQuit();
			this.ApplicationQuit();
		}

		private void OnApplicationFocus(bool focus)
		{
			this.ApplicationFocus(focus);
		}

		private void OnApplicationPause(bool pause)
		{
			this.ApplicationPause(pause);
		}

		private void Update()
		{
			this.OnUpdate();
		}
	}
}
