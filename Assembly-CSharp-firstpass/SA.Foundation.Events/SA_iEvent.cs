using System;

namespace SA.Foundation.Events
{
	public interface SA_iEvent<T> : SA_iSafeEvent<T>
	{
		void AddListener(Action<T> listner);
	}
	public interface SA_iEvent : SA_iSafeEvent
	{
		void AddListener(Action listner);
	}
}
