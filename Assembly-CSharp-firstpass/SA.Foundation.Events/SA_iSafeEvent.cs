using System;

namespace SA.Foundation.Events
{
	public interface SA_iSafeEvent<T>
	{
		void AddSafeListener(object callbackTarget, Action<T> listner);

		void RemoveListener(Action<T> listner);
	}
	public interface SA_iSafeEvent
	{
		void AddSafeListener(object callbackTarget, Action listner);

		void RemoveListener(Action listner);
	}
}
