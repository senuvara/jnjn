using SA.Foundation.Events;
using UnityEngine;

namespace SA.Foundation.Animation
{
	public class SA_ValuesTween : MonoBehaviour
	{
		private SA_Event m_onComplete = new SA_Event();

		private SA_Event<float> m_onValueChanged = new SA_Event<float>();

		private SA_Event<Vector3> m_onVectorValueChanged = new SA_Event<Vector3>();

		public bool DestoryGameObjectOnComplete = true;

		private float FinalFloatValue;

		private Vector3 FinalVectorValue;

		public SA_iSafeEvent OnComplete => m_onComplete;

		public SA_iSafeEvent<float> OnValueChanged => m_onValueChanged;

		public SA_iSafeEvent<Vector3> OnVectorValueChanged => m_onVectorValueChanged;

		public static SA_ValuesTween Create()
		{
			return new GameObject(typeof(SA_ValuesTween).FullName).AddComponent<SA_ValuesTween>();
		}

		private void Update()
		{
			if (!(base.gameObject == null) && !base.gameObject.Equals(null))
			{
				SA_Event<float> onValueChanged = m_onValueChanged;
				Vector3 position = base.transform.position;
				onValueChanged.Invoke(position.x);
				m_onVectorValueChanged.Invoke(base.transform.position);
			}
		}

		public void ValueTo(float from, float to, float time, SA_EaseType easeType = SA_EaseType.linear)
		{
			Vector3 position = base.transform.position;
			position.x = from;
			base.transform.position = position;
			FinalFloatValue = to;
			SA_iTween.MoveTo(base.gameObject, SA_iTween.Hash("x", to, "time", time, "easeType", easeType.ToString(), "oncomplete", "onTweenComplete", "oncompletetarget", base.gameObject));
		}

		public void VectorTo(Vector3 from, Vector3 to, float time, SA_EaseType easeType = SA_EaseType.linear)
		{
			base.transform.position = from;
			FinalVectorValue = to;
			SA_iTween.MoveTo(base.gameObject, SA_iTween.Hash("position", to, "time", time, "easeType", easeType.ToString(), "oncomplete", "onTweenComplete", "oncompletetarget", base.gameObject));
		}

		public void RotateTo(Vector3 from, Vector3 to, float time, SA_EaseType easeType = SA_EaseType.linear)
		{
			base.transform.rotation = Quaternion.Euler(from);
			FinalVectorValue = to;
			SA_iTween.RotateTo(base.gameObject, SA_iTween.Hash("rotation", to, "time", time, "easeType", easeType.ToString(), "oncomplete", "onTweenComplete", "oncompletetarget", base.gameObject));
		}

		public void ScaleTo(Vector3 from, Vector3 to, float time, SA_EaseType easeType = SA_EaseType.linear)
		{
			base.transform.localScale = from;
			FinalVectorValue = to;
			SA_iTween.ScaleTo(base.gameObject, SA_iTween.Hash("scale", to, "time", time, "easeType", easeType.ToString(), "oncomplete", "onTweenComplete", "oncompletetarget", base.gameObject));
		}

		public void VectorToS(Vector3 from, Vector3 to, float speed, SA_EaseType easeType = SA_EaseType.linear)
		{
			base.transform.position = from;
			FinalVectorValue = to;
			SA_iTween.MoveTo(base.gameObject, SA_iTween.Hash("position", to, "speed", speed, "easeType", easeType.ToString(), "oncomplete", "onTweenComplete", "oncompletetarget", base.gameObject));
		}

		public void Stop()
		{
			SA_iTween.Stop(base.gameObject);
			Object.Destroy(base.gameObject);
		}

		private void onTweenComplete()
		{
			if (!(base.gameObject == null) && !base.gameObject.Equals(null))
			{
				m_onValueChanged.Invoke(FinalFloatValue);
				m_onVectorValueChanged.Invoke(FinalVectorValue);
				m_onComplete.Invoke();
				if (DestoryGameObjectOnComplete)
				{
					Object.Destroy(base.gameObject);
				}
				else
				{
					Object.Destroy(this);
				}
			}
		}
	}
}
