using UnityEngine;
using UnityEngine.EventSystems;

public class YlyDelegateUtil
{
	public delegate void VoidDelegate(GameObject go, PointerEventData eventData = null);

	public delegate void StringDelegate(string arg);
}
