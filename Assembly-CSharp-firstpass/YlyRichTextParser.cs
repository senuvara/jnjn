using System;
using System.Collections;
using System.Collections.Generic;
using System.Diagnostics;
using System.Text;
using System.Text.RegularExpressions;

internal class YlyRichTextParser
{
	public const int RICHTEXT_MULL_BASE = 100;

	public const int RICHTEXT_EMOTE_BASE = 10000;

	public const int RICHTEXT_ANIM_EMOTE_BASE = 10100;

	public const int RICHTEXT_RES_PATH_BASE = 10200;

	public const int RICHTEXT_CICON_BASE = 10300;

	public const int RICHTEXT_COLOR_BASE = 20000;

	public const int RICHTEXT_LINK_BASE = 20100;

	public const int RICHTEXT_UNDER_LINE_BASE = 20200;

	public const int RICHTEXT_LINK_END = 20300;

	public const int RICHTEXT_COLOR_END = 20400;

	public const int RICHTEXT_ENTER = 20500;

	public const int RICHTEXT_UNDER_LINE_END = 20600;

	public const int RICHTEXT_BOLD_BASE = 20700;

	public const int RICHTEXT_BOLD_END = 20800;

	public const int RICHTEXT_ITALIC_BASE = 20900;

	public const int RICHTEXT_ITALIC_END = 21000;

	public const int RICHTEXT_SIZE_BASE = 21100;

	public const int RICHTEXT_SIZE_END = 21200;

	public const int RICHTEXT_DELETE_LINE_BASE = 21300;

	public const int RICHTEXT_DELETE_LINE_END = 21400;

	public const int RICHTEXT_UNKNOWN = -9999;

	public const int FIXED_LEN_COLOR_BLOCK = 16;

	public const int FIXED_LEN_COLOR_MORPH_BLOCK = 24;

	public const int EMOTE_IDX_BEGIN = 1;

	public const int EMOTE_IDX_END = 499;

	public const int ANIM_EMOTE_IDX_BEGIN = 500;

	public const int ANIM_EMOTE_IDX_END = 999;

	private static int[] _mask;

	private static Dictionary<int, string> _parms;

	private static Dictionary<string, int> _preLoadAsset;

	private static StringBuilder _logStr = new StringBuilder();

	public static int GetEmoteType(int idx)
	{
		if (idx >= 500 && idx <= 999)
		{
			return 10100;
		}
		if (idx >= 1 && idx <= 499)
		{
			return 10000;
		}
		return -9999;
	}

	public static string Parse(string str, out int[] mask, out Dictionary<int, string> param, out Dictionary<string, int> preLoadAsset, bool enableRichText)
	{
		Stopwatch stopwatch = new Stopwatch();
		stopwatch.Start();
		str += "\n";
		if (enableRichText)
		{
			str = str.Replace("\\n", "\n");
			_mask = new int[str.Length];
			_parms = new Dictionary<int, string>();
			_preLoadAsset = new Dictionary<string, int>();
			string rex = "<emote=(\\d{3})>";
			string rex2 = "<icon=(.+?)>";
			string rex3 = "<res=(.+?\\.png)>";
			string rex4 = "<color=([0-9a-fA-F]{8}|[0-9a-fA-F]{16})>";
			string rex5 = "<link=(.+?)>";
			string rex6 = "<u>";
			string rex7 = "</link>";
			string rex8 = "</color>";
			string rex9 = "</u>";
			string rex10 = "<b>";
			string rex11 = "</b>";
			string rex12 = "<i>";
			string rex13 = "</i>";
			string rex14 = "<size=(\\d+)>";
			string rex15 = "</size>";
			string rex16 = "<d>";
			string rex17 = "</d>";
			ParseSingle(str, rex, 10000);
			ParseSingle(str, rex2, 10300);
			ParseSingle(str, rex3, 10200);
			ParseSingle(str, rex4, 20000);
			ParseSingle(str, rex5, 20100);
			ParseSingle(str, rex6, 20200);
			ParseSingle(str, rex7, 20300);
			ParseSingle(str, rex8, 20400);
			ParseSingle(str, rex9, 20600);
			ParseSingle(str, rex10, 20700);
			ParseSingle(str, rex11, 20800);
			ParseSingle(str, rex12, 20900);
			ParseSingle(str, rex13, 21000);
			ParseSingle(str, rex14, 21100);
			ParseSingle(str, rex15, 21200);
			ParseSingle(str, rex16, 21300);
			ParseSingle(str, rex17, 21400);
		}
		else
		{
			_mask = new int[str.Length];
			_parms = new Dictionary<int, string>();
			_preLoadAsset = new Dictionary<string, int>();
		}
		stopwatch.Stop();
		mask = _mask;
		param = _parms;
		preLoadAsset = _preLoadAsset;
		_mask = null;
		_parms = null;
		_preLoadAsset = null;
		return str;
	}

	private static void ParseSingle(string str, string rex, int ibase)
	{
		IEnumerator enumerator = Regex.Matches(str, rex).GetEnumerator();
		try
		{
			while (enumerator.MoveNext())
			{
				Match match = (Match)enumerator.Current;
				string value = match.Groups[match.Groups.Count - 1].Value;
				_mask[match.Index] = ibase + match.Length;
				switch (ibase)
				{
				case 10000:
				{
					int result = -1;
					if (int.TryParse(value, out result))
					{
						if (GetEmoteType(result) == 10100)
						{
							string key2 = string.Format(YlyRichText.animEmotePrefabPathFormat, value);
							if (!_preLoadAsset.ContainsKey(key2))
							{
								_preLoadAsset.Add(key2, 10100);
							}
						}
						else
						{
							string key3 = string.Format(YlyRichText.emotePngPathFormat, value);
							if (!_preLoadAsset.ContainsKey(key3))
							{
								_preLoadAsset.Add(key3, 10000);
							}
						}
					}
					break;
				}
				case 10200:
				{
					string key4 = value;
					if (!_preLoadAsset.ContainsKey(key4))
					{
						_preLoadAsset.Add(key4, 10200);
					}
					break;
				}
				case 10300:
				{
					string key = string.Format(YlyRichText.cIconPngPathFormat, value);
					if (!_preLoadAsset.ContainsKey(key))
					{
						_preLoadAsset.Add(key, 10300);
					}
					break;
				}
				}
				_parms[match.Index] = value;
			}
		}
		finally
		{
			IDisposable disposable;
			if ((disposable = (enumerator as IDisposable)) != null)
			{
				disposable.Dispose();
			}
		}
	}

	public static bool IsPunctuation(char str)
	{
		return Regex.IsMatch(str.ToString(), "[\\[【】\\]~!！@#,，\\.。？\\?]");
	}

	public static string GetLog()
	{
		return "为了避免不必要的性能消耗，富文本的log被屏蔽了=、=";
	}
}
