using UnityEngine;

[AddComponentMenu("Visualizer Studio/Utility/Kill Timer")]
public class KillTimer : MonoBehaviour
{
	public float killDelay = 10f;

	private float killTimer;

	private void Start()
	{
		killTimer = killDelay;
	}

	private void Update()
	{
		killTimer -= Time.deltaTime;
		if (killTimer <= 0f)
		{
			Object.Destroy(base.gameObject);
		}
	}
}
