using UnityEngine;

[AddComponentMenu("Visualizer Studio/Controllers/Random Controller")]
public class VisRandomController : VisBaseController
{
	public new static class Defaults
	{
		public const float delayTime = 1f;
	}

	public float delayTime = 1f;

	private float m_fCurrentRandomValue;

	private float m_fNewValueTimer;

	public override void Reset()
	{
		base.Reset();
	}

	public override void Start()
	{
		base.Start();
	}

	public override float GetCustomControllerValue()
	{
		m_fNewValueTimer -= Time.deltaTime;
		if (m_fNewValueTimer <= 0f)
		{
			m_fNewValueTimer = delayTime;
			m_fCurrentRandomValue = Random.Range(0f, 1f);
		}
		return m_fCurrentRandomValue;
	}
}
