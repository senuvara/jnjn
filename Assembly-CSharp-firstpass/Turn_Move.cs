using UnityEngine;

public class Turn_Move : MonoBehaviour
{
	public float TurnX;

	public float TurnY;

	public float TurnZ;

	public float MoveX;

	public float MoveY;

	public float MoveZ;

	public bool World;

	private void Start()
	{
	}

	private void Update()
	{
		if (World)
		{
			base.transform.Rotate(TurnX * Time.deltaTime, TurnY * Time.deltaTime, TurnZ * Time.deltaTime, Space.World);
			base.transform.Translate(MoveX * Time.deltaTime, MoveY * Time.deltaTime, MoveZ * Time.deltaTime, Space.World);
		}
		else
		{
			base.transform.Rotate(TurnX * Time.deltaTime, TurnY * Time.deltaTime, TurnZ * Time.deltaTime, Space.Self);
			base.transform.Translate(MoveX * Time.deltaTime, MoveY * Time.deltaTime, MoveZ * Time.deltaTime, Space.Self);
		}
	}
}
