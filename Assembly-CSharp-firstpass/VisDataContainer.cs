using UnityEngine;

public class VisDataContainer
{
	public float average;

	public float previousAverage;

	public float averageDifference;

	public float median;

	public float previousMedian;

	public float medianDifference;

	public float sum;

	public float previousSum;

	public float sumDifference;

	public float minimum;

	public float previousMinimum;

	public float minimumDifference;

	public float maximum;

	public float previousMaximum;

	public float maximumDifference;

	public float GetValue(VisDataValueType valueType)
	{
		switch (valueType)
		{
		default:
			return average;
		case VisDataValueType.Median:
			return median;
		case VisDataValueType.Sum:
			return sum;
		case VisDataValueType.Minimum:
			return minimum;
		case VisDataValueType.Maximum:
			return maximum;
		}
	}

	public float GetPreviousValue(VisDataValueType valueType)
	{
		switch (valueType)
		{
		default:
			return previousAverage;
		case VisDataValueType.Median:
			return previousMedian;
		case VisDataValueType.Sum:
			return previousSum;
		case VisDataValueType.Minimum:
			return previousMinimum;
		case VisDataValueType.Maximum:
			return previousMaximum;
		}
	}

	public float GetValueDifference(VisDataValueType valueType)
	{
		switch (valueType)
		{
		default:
			return averageDifference;
		case VisDataValueType.Median:
			return medianDifference;
		case VisDataValueType.Sum:
			return sumDifference;
		case VisDataValueType.Minimum:
			return minimumDifference;
		case VisDataValueType.Maximum:
			return maximumDifference;
		}
	}

	public void UpdatePreviousValues()
	{
		previousAverage = average;
		previousMedian = median;
		previousSum = sum;
		previousMinimum = minimum;
		previousMaximum = maximum;
	}

	public void ResetCurrentValues()
	{
		average = 0f;
		median = 0f;
		sum = 0f;
		minimum = 10000f;
		maximum = -10000f;
	}

	public void ApplyBoostAndCutoff(float boost, float cutoff)
	{
		average = Mathf.Clamp(average * boost, (!(average < 0f)) ? 0f : (0f - cutoff), (!(average < 0f)) ? cutoff : 0f);
		median = Mathf.Clamp(median * boost, (!(median < 0f)) ? 0f : (0f - cutoff), (!(median < 0f)) ? cutoff : 0f);
		sum = Mathf.Clamp(sum * boost, (!(boost < 0f)) ? 0f : (0f - cutoff), (!(boost < 0f)) ? cutoff : 0f);
		minimum = Mathf.Clamp(minimum * boost, (!(minimum < 0f)) ? 0f : (0f - cutoff), (!(minimum < 0f)) ? cutoff : 0f);
		maximum = Mathf.Clamp(maximum * boost, (!(maximum < 0f)) ? 0f : (0f - cutoff), (!(maximum < 0f)) ? cutoff : 0f);
	}

	public void UpdateValueDifferences()
	{
		averageDifference = average - previousAverage;
		medianDifference = median - previousMedian;
		sumDifference = sum - previousSum;
		minimumDifference = minimum - previousMinimum;
		maximumDifference = maximum - previousMaximum;
	}
}
