using UnityEngine;

[AddComponentMenu("Visualizer Studio/Modifiers/Emitter Property Modifier")]
public class VisEmitterPropertyModifier : VisBasePropertyModifier
{
	public new static class Defaults
	{
		public const EmitterProperty targetProperty = EmitterProperty.EmitterVelocityScale;
	}

	public EmitterProperty targetProperty = EmitterProperty.EmitterVelocityScale;

	public override void Reset()
	{
		base.Reset();
		targetProperty = EmitterProperty.EmitterVelocityScale;
	}

	public override void Start()
	{
		base.Start();
	}

	public override void SetProperty(float propertyValue)
	{
		Debug.LogWarning("VisEmitterPropertyModifier is deprecated in Unity 2017 or newer and no longer works.", this);
	}
}
