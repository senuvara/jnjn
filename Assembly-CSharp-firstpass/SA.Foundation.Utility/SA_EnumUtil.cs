using System;
using UnityEngine;

namespace SA.Foundation.Utility
{
	public static class SA_EnumUtil
	{
		public static bool CanBeParsedToEnum<T>(string value)
		{
			try
			{
				Enum.Parse(typeof(T), value, ignoreCase: true);
				return true;
			}
			catch (Exception)
			{
				return false;
			}
		}

		public static bool TryParseEnum<T>(string value, out T result)
		{
			try
			{
				result = (T)Enum.Parse(typeof(T), value, ignoreCase: true);
				return true;
			}
			catch (Exception)
			{
				result = default(T);
				return false;
			}
		}

		public static T ParseEnum<T>(string value)
		{
			try
			{
				return (T)Enum.Parse(typeof(T), value, ignoreCase: true);
			}
			catch (Exception ex)
			{
				Debug.LogWarning("Enum Parsing failed: " + ex.Message);
				return default(T);
			}
		}
	}
}
