using System.Collections.Generic;
using System.IO;

namespace SA.Foundation.Utility
{
	public static class SA_FilesUtil
	{
		public static bool IsFileExists(string path)
		{
			if (string.IsNullOrEmpty(path))
			{
				return false;
			}
			path = FixRelativePath(path, createFolders: false);
			return File.Exists(SA_PathUtil.ConvertRelativeToAbsolutePath(path));
		}

		public static void CreateFile(string path)
		{
			path = FixRelativePath(path);
			File.Create(SA_PathUtil.ConvertRelativeToAbsolutePath(path));
		}

		public static void DeleteFile(string path)
		{
			if (IsFileExists(path))
			{
				path = SA_PathUtil.ConvertRelativeToAbsolutePath(path);
				File.Delete(path);
			}
		}

		public static List<string> GetFilesFromDirectory(string path, params string[] extentions)
		{
			path = SA_PathUtil.ConvertRelativeToAbsolutePath(path);
			string[] files = Directory.GetFiles(path);
			List<string> list = new List<string>();
			if (extentions.Length != 0)
			{
				string[] array = files;
				foreach (string path2 in array)
				{
					foreach (string text in extentions)
					{
						if (text.Equals(Path.GetExtension(path2)))
						{
							list.Add(Path.GetFileName(path2));
							break;
						}
					}
				}
			}
			else
			{
				list = new List<string>(files);
			}
			return list;
		}

		public static void Write(string path, string contents)
		{
			path = FixRelativePath(path);
			string path2 = SA_PathUtil.ConvertRelativeToAbsolutePath(path);
			TextWriter textWriter = new StreamWriter(path2, append: false);
			textWriter.Write(contents);
			textWriter.Close();
		}

		public static string Read(string path)
		{
			if (IsFileExists(path))
			{
				path = SA_PathUtil.ConvertRelativeToAbsolutePath(path);
				return File.ReadAllText(path);
			}
			return string.Empty;
		}

		public static void CopyFile(string srcPath, string destPath)
		{
			if (IsFileExists(srcPath) && !srcPath.Equals(destPath))
			{
				srcPath = FixRelativePath(srcPath, createFolders: false);
				destPath = FixRelativePath(destPath);
				string sourceFileName = SA_PathUtil.ConvertRelativeToAbsolutePath(srcPath);
				string destFileName = SA_PathUtil.ConvertRelativeToAbsolutePath(destPath);
				File.Copy(sourceFileName, destFileName, overwrite: true);
			}
		}

		public static void CopyDirectory(string srcPath, string destPath)
		{
			srcPath = FixRelativePath(srcPath);
			destPath = FixRelativePath(destPath);
			srcPath = SA_PathUtil.ConvertRelativeToAbsolutePath(srcPath);
			destPath = SA_PathUtil.ConvertRelativeToAbsolutePath(destPath);
			string[] directories = Directory.GetDirectories(srcPath, "*", SearchOption.AllDirectories);
			foreach (string text in directories)
			{
				Directory.CreateDirectory(text.Replace(srcPath, destPath));
			}
			string[] files = Directory.GetFiles(srcPath, "*.*", SearchOption.AllDirectories);
			foreach (string text2 in files)
			{
				File.Copy(text2, text2.Replace(srcPath, destPath), overwrite: true);
			}
		}

		public static void CreateDirectory(string path)
		{
			path = FixRelativePath(path);
			Directory.CreateDirectory(SA_PathUtil.ConvertRelativeToAbsolutePath(path));
		}

		public static void DeleteDirectory(string folderPath)
		{
			folderPath = SA_PathUtil.ConvertRelativeToAbsolutePath(folderPath);
			Directory.Delete(folderPath, recursive: true);
		}

		public static bool IsDirectoryExists(string path)
		{
			return SA_PathUtil.IsDirectoryExists(path);
		}

		private static string FixRelativePath(string path, bool createFolders = true)
		{
			path = SA_PathUtil.FixRelativePath(path);
			if (createFolders)
			{
				CreatePathFolders(path);
			}
			return path;
		}

		private static void CreatePathFolders(string path)
		{
			foreach (string item in SA_PathUtil.GetDirectoriesOutOfPath(path))
			{
				if (!IsDirectoryExists(item))
				{
					string path2 = SA_PathUtil.ConvertRelativeToAbsolutePath(item);
					Directory.CreateDirectory(path2);
				}
			}
		}
	}
}
