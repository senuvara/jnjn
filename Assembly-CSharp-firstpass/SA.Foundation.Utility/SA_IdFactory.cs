using UnityEngine;

namespace SA.Foundation.Utility
{
	public class SA_IdFactory
	{
		private const string PP_ID_KEY = "SA.Common.Utility.SA_IdFactory_Key";

		public static int NextId
		{
			get
			{
				int num = 1;
				if (PlayerPrefs.HasKey("SA.Common.Utility.SA_IdFactory_Key"))
				{
					num = PlayerPrefs.GetInt("SA.Common.Utility.SA_IdFactory_Key");
					num++;
				}
				PlayerPrefs.SetInt("SA.Common.Utility.SA_IdFactory_Key", num);
				return num;
			}
		}

		public static string RandomString
		{
			get
			{
				string text = "0123456789abcdefghijklmnopqrstuvwxABCDEFGHIJKLMNOPQRSTUVWXYZ";
				string text2 = string.Empty;
				for (int i = 0; i < 20; i++)
				{
					int index = Random.Range(0, text.Length);
					text2 += text[index];
				}
				return text2;
			}
		}
	}
}
