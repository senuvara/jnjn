using System;
using System.Collections.Generic;
using System.IO;
using UnityEngine;

namespace SA.Foundation.Utility
{
	public static class SA_PathUtil
	{
		public const string ASSETS = "Assets/";

		public const string FOLDER_SEPARATOR = "/";

		public static string FixRelativePath(string path)
		{
			if (path.StartsWith("/", StringComparison.CurrentCulture))
			{
				path = path.Remove(0, 1);
			}
			if (path.StartsWith("Assets/", StringComparison.CurrentCulture))
			{
				path = path.Remove(0, "Assets/".Length);
			}
			return path;
		}

		public static string ConvertRelativeToAbsolutePath(string relativePath)
		{
			relativePath = FixRelativePath(relativePath);
			string str = (!Application.isEditor) ? Application.persistentDataPath : Application.dataPath;
			return str + "/" + relativePath;
		}

		public static string ConvertAssetRelativeToProjectRelative(string relativePath)
		{
			relativePath = FixRelativePath(relativePath);
			return "Assets/" + relativePath;
		}

		public static bool IsDirectoryExists(string path)
		{
			return Directory.Exists(ConvertRelativeToAbsolutePath(path));
		}

		public static List<string> GetDirectoriesOutOfPath(string path)
		{
			List<string> list = new List<string>();
			string str = string.Empty;
			int num = path.IndexOf("/", StringComparison.CurrentCulture);
			while (num != -1)
			{
				int num2 = num + 1;
				string text = str + path.Substring(0, num2);
				list.Add(text);
				path = path.Substring(num2, path.Length - num2);
				num = path.IndexOf("/", StringComparison.CurrentCulture);
				str = text;
			}
			return list;
		}

		public static string GetPathDirectoryName(string folderPath)
		{
			if (folderPath.EndsWith("/", StringComparison.CurrentCulture))
			{
				folderPath = folderPath.Substring(0, folderPath.Length - 1);
			}
			int num = folderPath.LastIndexOf("/", StringComparison.CurrentCulture);
			if (num == -1)
			{
				return folderPath;
			}
			int num2 = num + 1;
			return folderPath.Substring(num2, folderPath.Length - num2);
		}
	}
}
