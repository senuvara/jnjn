using SA.Foundation.Async;
using System;
using System.Collections;
using UnityEngine;

namespace SA.Foundation.Utility
{
	public static class SA_ScreenUtil
	{
		public static void TakeScreenshot(Action<Texture2D> callback)
		{
			SA_Coroutine.Start(TakeScreenshotCoroutine(callback));
		}

		private static IEnumerator TakeScreenshotCoroutine(Action<Texture2D> callback)
		{
			yield return new WaitForEndOfFrame();
			int width = Screen.width;
			int height = Screen.width;
			Texture2D tex = new Texture2D(width, height, TextureFormat.RGB24, mipmap: false);
			tex.ReadPixels(new Rect(0f, 0f, width, height), 0, 0);
			tex.Apply();
			callback(tex);
		}
	}
}
