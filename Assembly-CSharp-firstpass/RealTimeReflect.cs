using UnityEngine;

public class RealTimeReflect : MonoBehaviour
{
	private ReflectionProbe probe;

	private void Awake()
	{
		probe = GetComponent<ReflectionProbe>();
	}

	private void Update()
	{
		Transform transform = probe.transform;
		Vector3 position = Camera.main.transform.position;
		float x = position.x;
		Vector3 position2 = Camera.main.transform.position;
		float y = position2.y * -1f;
		Vector3 position3 = Camera.main.transform.position;
		transform.position = new Vector3(x, y, position3.z);
		probe.RenderProbe();
	}
}
