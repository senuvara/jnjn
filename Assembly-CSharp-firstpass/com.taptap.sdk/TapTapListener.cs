using UnityEngine;

namespace com.taptap.sdk
{
	public class TapTapListener : MonoBehaviour
	{
		public static volatile bool inited;

		public static void Init()
		{
			if (!inited)
			{
				inited = true;
				GameObject gameObject = new GameObject();
				gameObject.name = "TapTapUnity";
				gameObject.AddComponent<TapTapListener>();
				Object.DontDestroyOnLoad(gameObject);
			}
		}

		private void OnQueryAppBoardStatusSuccess(string count)
		{
			if (TapTapSDK.Instance.GetCallback() != null && int.TryParse(count, out int result))
			{
				TapTapSDK.Instance.GetCallback().OnQueryAppBoardStatusSuccess(result);
			}
		}

		private void OnQueryAppBoardStatusFailed(string error)
		{
			if (TapTapSDK.Instance.GetCallback() != null)
			{
				TapTapSDK.Instance.GetCallback().OnQueryAppBoardStatusFailed(error);
			}
		}
	}
}
