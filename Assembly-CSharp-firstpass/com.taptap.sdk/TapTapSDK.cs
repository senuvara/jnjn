namespace com.taptap.sdk
{
	public sealed class TapTapSDK
	{
		private static volatile TapTapSDK instance;

		private static object syncRoot = new object();

		private TapTapSDKImpl taptapSDKimpl;

		public TapCallback tapCallback;

		public static TapTapSDK Instance
		{
			get
			{
				if (instance == null)
				{
					lock (syncRoot)
					{
						if (instance == null)
						{
							instance = new TapTapSDK();
						}
					}
				}
				return instance;
			}
		}

		private TapTapSDK()
		{
		}

		public void SetCallback(TapCallback callback)
		{
			tapCallback = callback;
		}

		public TapCallback GetCallback()
		{
			return tapCallback;
		}

		public void OpenTapTapForum(string appid)
		{
			if (taptapSDKimpl == null)
			{
			}
			if (taptapSDKimpl != null)
			{
				taptapSDKimpl.OpenTapTapForum(appid);
			}
		}

		public void InitAppBoard()
		{
			if (taptapSDKimpl == null)
			{
			}
			if (taptapSDKimpl != null)
			{
				taptapSDKimpl.InitAppBoard();
				TapTapListener.Init();
			}
		}

		public void QueryAppBoardStatus()
		{
			if (taptapSDKimpl == null)
			{
			}
			if (taptapSDKimpl != null)
			{
				taptapSDKimpl.QueryAppBoardStatus();
			}
		}

		public void OpenAppBoard(string appid)
		{
			if (taptapSDKimpl == null)
			{
			}
			if (taptapSDKimpl != null)
			{
				taptapSDKimpl.OpenAppBoard(appid);
			}
		}
	}
}
