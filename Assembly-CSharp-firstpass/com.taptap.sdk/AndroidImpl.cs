using UnityEngine;

namespace com.taptap.sdk
{
	public class AndroidImpl : TapTapSDKImpl
	{
		public override void OpenTapTapForum(string appid)
		{
			AndroidJavaClass androidJavaClass = new AndroidJavaClass("com.taptap.sdk.TapTapUnity");
			androidJavaClass.CallStatic("openTapTapForum", appid);
		}

		public override void InitAppBoard()
		{
			AndroidJavaClass androidJavaClass = new AndroidJavaClass("com.taptap.sdk.TapTapUnity");
			androidJavaClass.CallStatic("initAppBoard");
		}

		public override void QueryAppBoardStatus()
		{
			AndroidJavaClass androidJavaClass = new AndroidJavaClass("com.taptap.sdk.TapTapUnity");
			androidJavaClass.CallStatic("queryAppBoardStatus");
		}

		public override void OpenAppBoard(string appid)
		{
			AndroidJavaClass androidJavaClass = new AndroidJavaClass("com.taptap.sdk.TapTapUnity");
			androidJavaClass.CallStatic("openAppBoard", appid);
		}
	}
}
