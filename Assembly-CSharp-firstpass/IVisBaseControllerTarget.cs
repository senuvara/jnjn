public interface IVisBaseControllerTarget
{
	VisBaseController Controller
	{
		get;
		set;
	}

	string LastControllerName
	{
		get;
	}
}
