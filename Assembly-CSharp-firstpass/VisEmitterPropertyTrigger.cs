using UnityEngine;

[AddComponentMenu("Visualizer Studio/Triggers/Emitter Property Trigger")]
public class VisEmitterPropertyTrigger : VisBasePropertyTrigger
{
	public new static class Defaults
	{
		public const EmitterProperty targetProperty = EmitterProperty.EmitterVelocityScale;
	}

	public EmitterProperty targetProperty = EmitterProperty.EmitterVelocityScale;

	public override void SetProperty(float propertyValue)
	{
		Debug.LogWarning("VisEmitterPropertyTrigger is deprecated in Unity 2017 or newer and no longer works.", this);
	}
}
