public interface IVisTriggerTarget
{
	void OnTriggered(float current, float previous, float difference, float adjustedDifference);
}
