using UnityEngine;

internal class TestBallSpawnScalar : MonoBehaviour, IVisPrefabSpawnedTarget
{
	public void OnSpawned(float current, float previous, float difference, float adjustedDifference)
	{
		float num = VisHelper.ConvertBetweenRanges(adjustedDifference, 0.05f, 0.2f, 4f, 16f, invertDestValue: false);
		base.gameObject.transform.localScale = new Vector3(num, num, num);
	}
}
