using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;

namespace xdsdk.Unity
{
	public class Demo : MonoBehaviour
	{
		public Text result;

		public void Init()
		{
			string[] loginEntries = new string[4]
			{
				"XD_LOGIN",
				"QQ_LOGIN",
				"WX_LOGIN",
				"TAPTAP_LOGIN"
			};
			XDSDK.SetLoginEntries(loginEntries);
			XDSDK.SetCallback(delegate(ResultCode code, string data)
			{
				Debug.Log(string.Concat("Code : ", code, "  Data : ", data));
				result.text = string.Concat("Code : ", code, "  Data : ", data);
			});
			XDSDK.Init("a4d6xky5gt4c80s");
		}

		public void Login()
		{
			XDSDK.Login();
		}

		public void Logout()
		{
			XDSDK.Logout();
		}

		public void Pay()
		{
			Dictionary<string, string> dictionary = new Dictionary<string, string>();
			dictionary.Add("OrderId", "1234567890123456789012345678901234567890");
			dictionary.Add("Product_Price", "101");
			dictionary.Add("EXT", "{\"payCallbackCode\":999}");
			dictionary.Add("Sid", "2");
			dictionary.Add("Role_Id", "3");
			dictionary.Add("Product_Id", "4");
			dictionary.Add("Product_Name", "648大礼包");
			Dictionary<string, string> info = dictionary;
			XDSDK.Pay(info);
		}

		public void OpenRealName()
		{
			XDSDK.OpenRealName();
		}
	}
}
