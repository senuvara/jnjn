using UnityEngine;

public class SpectrumSize : MonoBehaviour
{
	public GameObject theObject;

	public int audioChannel = 4;

	public float audioSensibility = 0.15f;

	public float scaleFactorX = 4f;

	public float scaleFactorY = 4f;

	public float scaleFactorZ = 4f;

	public float lerpTime = 5f;

	private Vector3 oldLocalScale;

	private void Start()
	{
		oldLocalScale = theObject.transform.localScale;
	}

	private void Update()
	{
		if (SpectrumKernel.spects[audioChannel] * SpectrumKernel.threshold >= audioSensibility)
		{
			theObject.transform.localScale = new Vector3(scaleFactorX, scaleFactorY, scaleFactorZ);
		}
		theObject.transform.localScale = Vector3.Lerp(theObject.transform.localScale, oldLocalScale, lerpTime * Time.deltaTime);
	}
}
