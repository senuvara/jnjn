using SimpleJSON;
using System;
using UnityEngine;

namespace PartaGames.Android
{
	public class PermissionGranterUnity : MonoBehaviour
	{
		public static Action<string, bool> PermissionRequestCallback;

		private const string PERMISSION_GRANTER_GAMEOBJECT_NAME = "PermissionGranterUnity";

		private const string PERMISSION_GRANTED = "PERMISSION_GRANTED";

		private const string PERMISSION_DENIED = "PERMISSION_DENIED";

		private static PermissionGranterUnity instance;

		private static bool initialized;

		private static AndroidJavaObject currentActivity;

		private static AndroidJavaObject permissionGranter;

		public void Awake()
		{
			instance = this;
			UnityEngine.Object.DontDestroyOnLoad(base.gameObject);
			if (base.name != "PermissionGranterUnity")
			{
				base.name = "PermissionGranterUnity";
			}
		}

		private static void initialize()
		{
			if (instance == null)
			{
				GameObject gameObject = new GameObject();
				instance = gameObject.AddComponent<PermissionGranterUnity>();
				gameObject.name = "PermissionGranterUnity";
			}
			AndroidJavaClass androidJavaClass = new AndroidJavaClass("com.unity3d.player.UnityPlayer");
			currentActivity = androidJavaClass.GetStatic<AndroidJavaObject>("currentActivity");
			permissionGranter = new AndroidJavaObject("com.partagames.unity.permissiongranter.unity.PermissionGranterUnity");
			initialized = true;
		}

		public static bool ShouldShowRequestPermissionRationale(string permission)
		{
			Debug.LogWarning("PermissionGranter supports only Android devices.");
			return false;
		}

		public static bool IsPermissionGranted(string permission)
		{
			Debug.LogWarning("PermissionGranter supports only Android devices.");
			return false;
		}

		public static void GrantPermission(string permission, Action<string, bool> callback)
		{
			Debug.LogWarning("PermissionGranter supports only Android devices.");
		}

		public static void GrantPermissions(string[] permissions, Action<string, bool> callback)
		{
			Debug.LogWarning("PermissionGranter supports only Android devices.");
		}

		private void permissionRequestCallbackInternal(string message)
		{
			Debug.Log(message);
			JSONNode jSONNode = JSONNode.Parse(message);
			string arg = jSONNode["permission"];
			string a = jSONNode["grantResult"];
			bool arg2 = false;
			if (a == "PERMISSION_GRANTED")
			{
				arg2 = true;
			}
			if (PermissionRequestCallback != null)
			{
				PermissionRequestCallback(arg, arg2);
			}
		}
	}
}
