using UnityEngine;

public sealed class VisSubDataGroup : VisDataContainer
{
	public int frequencyRangeStartIndex;

	public int frequencyRangeEndIndex = 1;

	public VisSubDataGroup(int startIndex, int endIndex)
	{
		frequencyRangeStartIndex = startIndex;
		frequencyRangeEndIndex = endIndex;
	}

	public void Update(float[] spectrum)
	{
		if (spectrum == null)
		{
			return;
		}
		UpdatePreviousValues();
		ResetCurrentValues();
		int num = 0;
		for (int i = frequencyRangeStartIndex; i <= frequencyRangeEndIndex && i < spectrum.Length; i++)
		{
			num++;
			float num2 = spectrum[i];
			sum += num2;
			if (Mathf.Abs(num2) < minimum)
			{
				minimum = num2;
			}
			if (Mathf.Abs(num2) > maximum)
			{
				maximum = num2;
			}
		}
		if (num > 0)
		{
			average = sum / (float)num;
			median = (minimum + maximum) * 0.5f;
		}
		else
		{
			average = 0f;
			median = 0f;
			sum = 0f;
			maximum = 0f;
			minimum = 0f;
		}
		UpdateValueDifferences();
	}
}
