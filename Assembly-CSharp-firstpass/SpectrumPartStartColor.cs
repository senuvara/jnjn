using UnityEngine;

public class SpectrumPartStartColor : MonoBehaviour
{
	public ParticleSystem partEmitter;

	public int audioChannel = 3;

	public float audioSensibility = 0.1f;

	public Color beatColor = new Color(1f, 0f, 0f);

	public Color normalColor = new Color(0.5f, 0.5f, 0.5f);

	private void Update()
	{
		ParticleSystem.MainModule main = partEmitter.main;
		if (SpectrumKernel.spects[audioChannel] * SpectrumKernel.threshold >= audioSensibility)
		{
			main.startColor = beatColor;
			main.startSize = 2f;
		}
		else
		{
			main.startColor = normalColor;
			main.startSize = 1f;
		}
	}
}
