using System;

namespace SA.IOSDeploy
{
	[Serializable]
	public class ISD_PushNotificationsCapability
	{
		public bool Development = true;
	}
}
