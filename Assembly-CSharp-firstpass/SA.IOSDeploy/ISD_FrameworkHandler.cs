using SA.Foundation.Patterns;
using System;
using System.Collections;
using System.Collections.Generic;
using UnityEngine;

namespace SA.IOSDeploy
{
	public class ISD_FrameworkHandler : MonoBehaviour
	{
		private static List<ISD_Framework> _DefaultFrameworks;

		public static List<ISD_Framework> AvailableFrameworks
		{
			get
			{
				List<ISD_Framework> list = new List<ISD_Framework>();
				List<string> list2 = new List<string>(Enum.GetNames(typeof(ISD_iOSFramework)));
				foreach (ISD_Framework framework in SA_ScriptableSingleton<ISD_Settings>.Instance.Frameworks)
				{
					if (list2.Contains(framework.Type.ToString()))
					{
						list2.Remove(framework.Type.ToString());
					}
				}
				foreach (ISD_Framework defaultFramework in DefaultFrameworks)
				{
					if (list2.Contains(defaultFramework.Type.ToString()))
					{
						list2.Remove(defaultFramework.Type.ToString());
					}
				}
				IEnumerator enumerator3 = Enum.GetValues(typeof(ISD_iOSFramework)).GetEnumerator();
				try
				{
					while (enumerator3.MoveNext())
					{
						ISD_iOSFramework type = (ISD_iOSFramework)enumerator3.Current;
						if (list2.Contains(type.ToString()))
						{
							list.Add(new ISD_Framework(type));
						}
					}
					return list;
				}
				finally
				{
					IDisposable disposable;
					if ((disposable = (enumerator3 as IDisposable)) != null)
					{
						disposable.Dispose();
					}
				}
			}
		}

		public static List<ISD_Framework> DefaultFrameworks
		{
			get
			{
				if (_DefaultFrameworks == null)
				{
					_DefaultFrameworks = new List<ISD_Framework>();
					_DefaultFrameworks.Add(new ISD_Framework(ISD_iOSFramework.CoreText));
					_DefaultFrameworks.Add(new ISD_Framework(ISD_iOSFramework.AudioToolbox));
					_DefaultFrameworks.Add(new ISD_Framework(ISD_iOSFramework.AVFoundation));
					_DefaultFrameworks.Add(new ISD_Framework(ISD_iOSFramework.CFNetwork));
					_DefaultFrameworks.Add(new ISD_Framework(ISD_iOSFramework.CoreGraphics));
					_DefaultFrameworks.Add(new ISD_Framework(ISD_iOSFramework.CoreLocation));
					_DefaultFrameworks.Add(new ISD_Framework(ISD_iOSFramework.CoreMedia));
					_DefaultFrameworks.Add(new ISD_Framework(ISD_iOSFramework.CoreMotion));
					_DefaultFrameworks.Add(new ISD_Framework(ISD_iOSFramework.CoreVideo));
					_DefaultFrameworks.Add(new ISD_Framework(ISD_iOSFramework.Foundation));
					_DefaultFrameworks.Add(new ISD_Framework(ISD_iOSFramework.iAd));
					_DefaultFrameworks.Add(new ISD_Framework(ISD_iOSFramework.MediaPlayer));
					_DefaultFrameworks.Add(new ISD_Framework(ISD_iOSFramework.OpenAL));
					_DefaultFrameworks.Add(new ISD_Framework(ISD_iOSFramework.OpenGLES));
					_DefaultFrameworks.Add(new ISD_Framework(ISD_iOSFramework.QuartzCore));
					_DefaultFrameworks.Add(new ISD_Framework(ISD_iOSFramework.SystemConfiguration));
					_DefaultFrameworks.Add(new ISD_Framework(ISD_iOSFramework.UIKit));
				}
				return _DefaultFrameworks;
			}
		}

		public static string[] BaseFrameworksArray()
		{
			List<string> list = new List<string>(AvailableFrameworks.Capacity);
			foreach (ISD_Framework availableFramework in AvailableFrameworks)
			{
				list.Add(availableFramework.Type.ToString());
			}
			return list.ToArray();
		}
	}
}
