using System;
using UnityEngine;

namespace SA.IOSDeploy
{
	[Serializable]
	public class ISD_BuildProperty
	{
		[SerializeField]
		private string m_name;

		[SerializeField]
		private string m_value;

		[SerializeField]
		private string[] m_options = new string[2]
		{
			"YES",
			"NO"
		};

		public string Name => m_name;

		public string Value
		{
			get
			{
				if (string.IsNullOrEmpty(m_value))
				{
					m_value = m_options[0];
				}
				return m_value;
			}
			set
			{
				m_value = value;
			}
		}

		public string[] Options => m_options;

		public ISD_BuildProperty(string name, string value)
		{
			m_name = name;
			m_value = value;
		}
	}
}
