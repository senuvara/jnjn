using System;
using System.Collections.Generic;

namespace SA.IOSDeploy
{
	[Serializable]
	public class ISD_iCloudCapability
	{
		public bool KeyValueStorage;

		public bool iCloudDocument;

		public List<string> CustomContainers;
	}
}
