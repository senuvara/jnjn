using System;

namespace SA.IOSDeploy
{
	[Serializable]
	public class ISD_Flag
	{
		public string Name;

		public ISD_FlagType Type;
	}
}
