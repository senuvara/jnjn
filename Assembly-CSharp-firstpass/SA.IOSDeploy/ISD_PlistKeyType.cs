namespace SA.IOSDeploy
{
	public enum ISD_PlistKeyType
	{
		String,
		Integer,
		Boolean,
		Array,
		Dictionary
	}
}
