using SA.Foundation.UtilitiesEditor;
using System;
using System.IO;
using UnityEngine;

namespace SA.IOSDeploy
{
	[Serializable]
	public class ISD_AssetFile
	{
		public string XCodePath = string.Empty;

		public UnityEngine.Object Asset;

		public string FileName
		{
			get
			{
				if (Asset == null)
				{
					return "No File";
				}
				return Path.GetFileName(RelativeFilePath);
			}
		}

		public string RelativeFilePath
		{
			get
			{
				if (Asset == null)
				{
					return string.Empty;
				}
				return SA_AssetDatabase.GetProjectFolderRelativePath(Asset);
			}
		}

		public string AbsoluteFilePath
		{
			get
			{
				if (Asset == null)
				{
					return string.Empty;
				}
				return SA_AssetDatabase.GetAbsoluteAssetPath(Asset);
			}
		}

		public string XCodeRelativePath => XCodePath + FileName;

		public bool IsDirectory
		{
			get
			{
				FileAttributes attributes = File.GetAttributes(RelativeFilePath);
				if ((attributes & FileAttributes.Directory) == FileAttributes.Directory)
				{
					return true;
				}
				return false;
			}
		}
	}
}
