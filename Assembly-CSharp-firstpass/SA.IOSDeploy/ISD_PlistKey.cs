using SA.Foundation.Patterns;
using SA.Foundation.Utility;
using System;
using System.Collections.Generic;

namespace SA.IOSDeploy
{
	[Serializable]
	public class ISD_PlistKey
	{
		public bool IsOpen = true;

		public bool IsListOpen = true;

		public string Name = string.Empty;

		public ISD_PlistKeyType Type;

		public string StringValue = string.Empty;

		public int IntegerValue;

		public float FloatValue;

		public bool BooleanValue = true;

		public List<string> ChildrensIds = new List<string>();

		public List<ISD_PlistKey> Children
		{
			get
			{
				List<ISD_PlistKey> list = new List<ISD_PlistKey>();
				foreach (string childrensId in ChildrensIds)
				{
					ISD_PlistKey variableById = SA_ScriptableSingleton<ISD_Settings>.Instance.getVariableById(childrensId);
					list.Add(variableById);
				}
				return list;
			}
		}

		public void AddChild(ISD_PlistKey childKey)
		{
			if (Type.Equals(ISD_PlistKeyType.Dictionary))
			{
				foreach (string childrensId in ChildrensIds)
				{
					ISD_PlistKey variableById = SA_ScriptableSingleton<ISD_Settings>.Instance.getVariableById(childrensId);
					if (variableById.Name.Equals(childKey.Name))
					{
						SA_ScriptableSingleton<ISD_Settings>.Instance.RemoveVariable(variableById, ChildrensIds);
						break;
					}
				}
			}
			string randomString = SA_IdFactory.RandomString;
			SA_ScriptableSingleton<ISD_Settings>.Instance.AddVariableToDictionary(randomString, childKey);
			ChildrensIds.Add(randomString);
		}

		public void RemoveChild(ISD_PlistKey childKey)
		{
			SA_ScriptableSingleton<ISD_Settings>.Instance.RemoveVariable(childKey, ChildrensIds);
		}

		public ISD_PlistKey GetChildByStringValue(string val)
		{
			foreach (ISD_PlistKey child in Children)
			{
				if (child.StringValue.Equals(val))
				{
					return child;
				}
			}
			return null;
		}
	}
}
