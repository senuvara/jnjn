using System;

namespace SA.IOSDeploy
{
	[Serializable]
	public class ISD_Capability
	{
		public ISD_CapabilityType CapabilityType;

		public string EntitlementsFilePath = string.Empty;

		public bool AddOptionalFramework;
	}
}
