using SA.Foundation.Patterns;
using UnityEngine;

namespace SA.IOSDeploy
{
	public static class ISD_API
	{
		public static void SetBuildProperty(string name, string value)
		{
			SetBuildProperty(new ISD_BuildProperty(name, value));
		}

		public static void SetBuildProperty(ISD_BuildProperty property)
		{
			foreach (ISD_BuildProperty buildProperty in SA_ScriptableSingleton<ISD_Settings>.Instance.BuildProperties)
			{
				if (buildProperty.Name.Equals(property.Name))
				{
					SA_ScriptableSingleton<ISD_Settings>.Instance.BuildProperties.Remove(buildProperty);
					break;
				}
			}
			SA_ScriptableSingleton<ISD_Settings>.Instance.BuildProperties.Add(property);
		}

		public static void RemoveBuildProperty(ISD_BuildProperty property)
		{
			RemoveBuildProperty(property.Name);
		}

		public static void RemoveBuildProperty(string name)
		{
			foreach (ISD_BuildProperty buildProperty in SA_ScriptableSingleton<ISD_Settings>.Instance.BuildProperties)
			{
				if (buildProperty.Name.Equals(name))
				{
					SA_ScriptableSingleton<ISD_Settings>.Instance.BuildProperties.Remove(buildProperty);
					break;
				}
			}
		}

		public static void SetInfoPlistKey(ISD_PlistKey key)
		{
			ISD_PlistKey infoPlistKey = GetInfoPlistKey(key.Name);
			if (infoPlistKey != null)
			{
				SA_ScriptableSingleton<ISD_Settings>.Instance.PlistVariables.Remove(infoPlistKey);
			}
			SA_ScriptableSingleton<ISD_Settings>.Instance.PlistVariables.Add(key);
		}

		public static ISD_PlistKey GetInfoPlistKey(string name)
		{
			foreach (ISD_PlistKey plistVariable in SA_ScriptableSingleton<ISD_Settings>.Instance.PlistVariables)
			{
				if (plistVariable.Name.Equals(name))
				{
					return plistVariable;
				}
			}
			return null;
		}

		public static bool ContainsKey(string name)
		{
			return GetInfoPlistKey(name) != null;
		}

		public static void RemoveInfoPlistKey(ISD_PlistKey key)
		{
			RemoveInfoPlistKey(key.Name);
		}

		public static void RemoveInfoPlistKey(string name)
		{
			ISD_PlistKey infoPlistKey = GetInfoPlistKey(name);
			if (infoPlistKey != null)
			{
				SA_ScriptableSingleton<ISD_Settings>.Instance.RemoveVariable(infoPlistKey, SA_ScriptableSingleton<ISD_Settings>.Instance.PlistVariables);
			}
		}

		public static void AddFlag(string name, ISD_FlagType type)
		{
			foreach (ISD_Flag flag in SA_ScriptableSingleton<ISD_Settings>.Instance.Flags)
			{
				if (flag.Type == type && flag.Name.Equals(name))
				{
					return;
				}
			}
			ISD_Flag iSD_Flag = new ISD_Flag();
			iSD_Flag.Name = name;
			iSD_Flag.Type = ISD_FlagType.LinkerFlag;
			SA_ScriptableSingleton<ISD_Settings>.Instance.Flags.Add(iSD_Flag);
		}

		public static void RemoveFlag(string name)
		{
			foreach (ISD_Flag flag in SA_ScriptableSingleton<ISD_Settings>.Instance.Flags)
			{
				if (flag.Name.Equals(name))
				{
					SA_ScriptableSingleton<ISD_Settings>.Instance.Flags.Remove(flag);
					break;
				}
			}
		}

		public static void AddFramework(ISD_iOSFramework framework, bool weak = false)
		{
			AddFramework(new ISD_Framework(framework, weak));
		}

		public static void AddFramework(ISD_Framework framework)
		{
			foreach (ISD_Framework framework2 in SA_ScriptableSingleton<ISD_Settings>.Instance.Frameworks)
			{
				if (framework2.Type == framework.Type)
				{
					SA_ScriptableSingleton<ISD_Settings>.Instance.Frameworks.Remove(framework2);
					break;
				}
			}
			SA_ScriptableSingleton<ISD_Settings>.Instance.Frameworks.Add(framework);
		}

		public static void RemoveFramework(ISD_iOSFramework framework)
		{
			foreach (ISD_Framework framework2 in SA_ScriptableSingleton<ISD_Settings>.Instance.Frameworks)
			{
				if (framework2.Type == framework)
				{
					SA_ScriptableSingleton<ISD_Settings>.Instance.Frameworks.Remove(framework2);
					break;
				}
			}
		}

		public static void AddLibrary(ISD_iOSLibrary library, bool weak = false)
		{
			AddLibrary(new ISD_Library(library, weak));
		}

		public static void AddLibrary(ISD_Library library)
		{
			foreach (ISD_Library library2 in SA_ScriptableSingleton<ISD_Settings>.Instance.Libraries)
			{
				if (library2.Type == library.Type)
				{
					SA_ScriptableSingleton<ISD_Settings>.Instance.Libraries.Remove(library2);
					break;
				}
			}
			SA_ScriptableSingleton<ISD_Settings>.Instance.Libraries.Add(library);
		}

		public static void RemoveLibrary(ISD_iOSLibrary library)
		{
			foreach (ISD_Library library2 in SA_ScriptableSingleton<ISD_Settings>.Instance.Libraries)
			{
				if (library2.Type == library)
				{
					SA_ScriptableSingleton<ISD_Settings>.Instance.Libraries.Remove(library2);
					break;
				}
			}
		}

		public static void AddCapability(ISD_CapabilityType capability, string entitlementsFilePath = "", bool addOptionalFramework = false)
		{
			ISD_Capability iSD_Capability = new ISD_Capability();
			iSD_Capability.CapabilityType = capability;
			iSD_Capability.EntitlementsFilePath = entitlementsFilePath;
			iSD_Capability.AddOptionalFramework = addOptionalFramework;
			AddCapability(iSD_Capability);
		}

		public static void AddCapability(ISD_Capability capability)
		{
			foreach (ISD_Capability capability2 in SA_ScriptableSingleton<ISD_Settings>.Instance.Capabilities)
			{
				if (capability2.CapabilityType == capability.CapabilityType)
				{
					SA_ScriptableSingleton<ISD_Settings>.Instance.Capabilities.Remove(capability2);
					break;
				}
			}
			SA_ScriptableSingleton<ISD_Settings>.Instance.Capabilities.Add(capability);
		}

		public static void RemoveCapability(ISD_Capability capability)
		{
			RemoveCapability(capability.CapabilityType);
		}

		public static void RemoveCapability(ISD_CapabilityType capability)
		{
			foreach (ISD_Capability capability2 in SA_ScriptableSingleton<ISD_Settings>.Instance.Capabilities)
			{
				if (capability2.CapabilityType == capability)
				{
					SA_ScriptableSingleton<ISD_Settings>.Instance.Capabilities.Remove(capability2);
					break;
				}
			}
		}

		public static bool HasFile(Object asset)
		{
			foreach (ISD_AssetFile file in SA_ScriptableSingleton<ISD_Settings>.Instance.Files)
			{
				if (file.Asset.Equals(asset))
				{
					return true;
				}
			}
			return false;
		}

		public static void AddFile(Object asset, string xCodePath = "")
		{
			ISD_AssetFile iSD_AssetFile = new ISD_AssetFile();
			iSD_AssetFile.Asset = asset;
			iSD_AssetFile.XCodePath = xCodePath;
			AddFile(iSD_AssetFile);
		}

		public static void AddFile(ISD_AssetFile file)
		{
			foreach (ISD_AssetFile file2 in SA_ScriptableSingleton<ISD_Settings>.Instance.Files)
			{
				if (file2.Asset.Equals(file.Asset))
				{
					SA_ScriptableSingleton<ISD_Settings>.Instance.Files.Remove(file2);
					break;
				}
			}
			SA_ScriptableSingleton<ISD_Settings>.Instance.Files.Add(file);
		}
	}
}
