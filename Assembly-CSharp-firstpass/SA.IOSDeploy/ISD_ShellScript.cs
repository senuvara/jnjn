using System;

namespace SA.IOSDeploy
{
	[Serializable]
	public class ISD_ShellScript
	{
		public string Name = "New Run Script";

		public string Shell = string.Empty;

		public string Script = string.Empty;

		public string ShellScriptPath => "\\\"" + Script + "\\\"";
	}
}
