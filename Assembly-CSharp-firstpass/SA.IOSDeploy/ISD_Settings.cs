using SA.Foundation.Localization;
using SA.Foundation.Patterns;
using System.Collections;
using System.Collections.Generic;
using UnityEngine;

namespace SA.IOSDeploy
{
	public class ISD_Settings : SA_ScriptableSingleton<ISD_Settings>
	{
		public const string VERSION_NUMBER = "1.0.0";

		public const string ENTITLEMENTS_FILE_NAME = "ios_deploy.entitlements";

		public const string IOS_DEPLOY_FOLDER = "Plugins/StansAssets/NativePlugins/IOSDeploy/";

		public bool IsfwSettingOpen;

		public bool IsLibSettingOpen;

		public bool IslinkerSettingOpne;

		public bool IscompilerSettingsOpen;

		public bool IsPlistSettingsOpen;

		public bool IsLanguageSettingOpen = true;

		public bool IsDefFrameworksOpen;

		public bool IsDefLibrariesOpen;

		public bool IsBuildSettingsOpen;

		public int ToolbarIndex;

		public List<ISD_Framework> Frameworks = new List<ISD_Framework>();

		public List<ISD_EmbedFramework> EmbededFrameworks = new List<ISD_EmbedFramework>();

		public List<ISD_Library> Libraries = new List<ISD_Library>();

		public List<ISD_Flag> Flags = new List<ISD_Flag>();

		public List<ISD_PlistKey> PlistVariables = new List<ISD_PlistKey>();

		public List<ISD_PlistKeyId> VariableDictionary = new List<ISD_PlistKeyId>();

		public List<SA_ISOLanguage> Languages = new List<SA_ISOLanguage>();

		public List<ISD_ShellScript> ShellScripts = new List<ISD_ShellScript>();

		[SerializeField]
		private List<ISD_BuildProperty> m_buildProperties = new List<ISD_BuildProperty>();

		public List<ISD_Capability> Capabilities = new List<ISD_Capability>();

		public ISD_iCloudCapability iCloudCapabilitySettings = new ISD_iCloudCapability();

		public ISD_PushNotificationsCapability PushNotificationsCapabilitySettings = new ISD_PushNotificationsCapability();

		public List<ISD_AssetFile> Files = new List<ISD_AssetFile>();

		public List<ISD_BuildProperty> BuildProperties
		{
			get
			{
				if (m_buildProperties.Count == 0)
				{
					ISD_BuildProperty item = new ISD_BuildProperty("ENABLE_BITCODE", "NO");
					m_buildProperties.Add(item);
					item = new ISD_BuildProperty("ENABLE_TESTABILITY", "NO");
					m_buildProperties.Add(item);
					item = new ISD_BuildProperty("GENERATE_PROFILING_CODE", "NO");
					m_buildProperties.Add(item);
				}
				return m_buildProperties;
			}
		}

		public void AddVariableToDictionary(string uniqueIdKey, ISD_PlistKey var)
		{
			ISD_PlistKeyId iSD_PlistKeyId = new ISD_PlistKeyId();
			iSD_PlistKeyId.uniqueIdKey = uniqueIdKey;
			iSD_PlistKeyId.VariableValue = var;
			VariableDictionary.Add(iSD_PlistKeyId);
		}

		public void RemoveVariable(ISD_PlistKey v, IList ListWithThisVariable)
		{
			if (SA_ScriptableSingleton<ISD_Settings>.Instance.PlistVariables.Contains(v))
			{
				SA_ScriptableSingleton<ISD_Settings>.Instance.PlistVariables.Remove(v);
			}
			else
			{
				foreach (ISD_PlistKeyId item in VariableDictionary)
				{
					if (item.VariableValue.Equals(v))
					{
						VariableDictionary.Remove(item);
						string uniqueIdKey = item.uniqueIdKey;
						if (ListWithThisVariable.Contains(uniqueIdKey))
						{
							ListWithThisVariable.Remove(item.uniqueIdKey);
						}
						break;
					}
				}
			}
			List<ISD_PlistKeyId> list = new List<ISD_PlistKeyId>(VariableDictionary);
			foreach (ISD_PlistKeyId item2 in VariableDictionary)
			{
				if (!IsInUse(item2.uniqueIdKey, PlistVariables))
				{
					list.Remove(item2);
				}
			}
			VariableDictionary = list;
		}

		private bool IsInUse(string id, List<ISD_PlistKey> list)
		{
			foreach (ISD_PlistKey item in list)
			{
				if (item.ChildrensIds.Contains(id))
				{
					return true;
				}
				if (IsInUse(id, item.Children))
				{
					return true;
				}
			}
			return false;
		}

		public ISD_PlistKey getVariableById(string uniqueIdKey)
		{
			foreach (ISD_PlistKeyId item in VariableDictionary)
			{
				if (item.uniqueIdKey.Equals(uniqueIdKey))
				{
					return item.VariableValue;
				}
			}
			return null;
		}
	}
}
