using System;

namespace SA.IOSDeploy
{
	[Serializable]
	public class ISD_Library
	{
		public ISD_iOSLibrary Type;

		public bool IsOptional;

		public string Name => ISD_LibHandler.stringValueOf(Type);

		public ISD_Library(ISD_iOSLibrary lib, bool optional = false)
		{
			Type = lib;
			IsOptional = optional;
		}
	}
}
