namespace SA.IOSDeploy
{
	public enum ISD_CapabilityType
	{
		Cloud,
		PushNotifications,
		GameCenter,
		Wallet,
		Siri,
		ApplePay,
		InAppPurchase,
		Maps,
		PersonalVPN,
		BackgroundModes,
		InterAppAudio,
		KeychainSharing,
		AssociatedDomains,
		AppGroups,
		DataProtection,
		HomeKit,
		HealthKit,
		WirelessAccessoryConfiguration
	}
}
