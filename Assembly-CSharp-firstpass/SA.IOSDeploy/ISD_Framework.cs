using SA.Foundation.Utility;
using System;

namespace SA.IOSDeploy
{
	[Serializable]
	public class ISD_Framework
	{
		public bool IsOptional;

		public bool IsEmbeded;

		public ISD_iOSFramework Type;

		public string Name => Type.ToString() + ".framework";

		public ISD_Framework(ISD_iOSFramework type, bool optional = false)
		{
			Type = type;
			IsOptional = optional;
		}

		public ISD_Framework(string frameworkName, bool optional = false)
		{
			frameworkName = frameworkName.Replace(".framework", string.Empty);
			Type = SA_EnumUtil.ParseEnum<ISD_iOSFramework>(frameworkName);
			IsOptional = optional;
		}
	}
}
