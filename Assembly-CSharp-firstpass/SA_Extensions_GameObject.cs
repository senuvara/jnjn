using SA.Foundation.Animation;
using System;
using UnityEngine;

public static class SA_Extensions_GameObject
{
	public static void Reset(this GameObject go)
	{
		go.transform.Reset();
	}

	public static void RotateTo(this GameObject go, Vector3 eulerRotation, float time, SA_EaseType easeType = SA_EaseType.linear)
	{
		go.RotateGameObjectTo(go, eulerRotation, time, easeType, null);
	}

	public static void RotateTo(this GameObject go, object callbackTarget, Vector3 eulerRotation, float time, SA_EaseType easeType, Action OnCompleteAction)
	{
		go.RotateGameObjectTo(callbackTarget, eulerRotation, time, easeType, OnCompleteAction);
	}

	public static void RotateGameObjectTo(this GameObject go, object callbackTarget, Vector3 eulerRotation, float time, SA_EaseType easeType, Action OnCompleteAction)
	{
		SA_ValuesTween sA_ValuesTween = go.AddComponent<SA_ValuesTween>();
		sA_ValuesTween.DestoryGameObjectOnComplete = false;
		sA_ValuesTween.RotateTo(go.transform.rotation.eulerAngles, eulerRotation, time, easeType);
		sA_ValuesTween.OnComplete.AddSafeListener(callbackTarget, OnCompleteAction);
	}

	public static void MoveTo(this GameObject go, Vector3 position, float time, SA_EaseType easeType = SA_EaseType.linear)
	{
		MoveGameObjectTo(go, go, position, time, easeType, null);
	}

	public static void MoveTo(this GameObject go, object callbackTarget, Vector3 position, float time, SA_EaseType easeType, Action OnCompleteAction)
	{
		MoveGameObjectTo(go, callbackTarget, position, time, easeType, OnCompleteAction);
	}

	public static void MoveGameObjectTo(GameObject go, object callbackTarget, Vector3 position, float time, SA_EaseType easeType, Action OnCompleteAction)
	{
		SA_ValuesTween sA_ValuesTween = go.AddComponent<SA_ValuesTween>();
		sA_ValuesTween.DestoryGameObjectOnComplete = false;
		sA_ValuesTween.VectorTo(go.transform.position, position, time, easeType);
		sA_ValuesTween.OnComplete.AddSafeListener(callbackTarget, OnCompleteAction);
	}

	public static void ScaleTo(this GameObject go, Vector3 scale, float time, SA_EaseType easeType = SA_EaseType.linear)
	{
		ScaleGameObjectTo(go, go, scale, time, easeType, null);
	}

	public static void ScaleTo(this GameObject go, object callbackTarget, Vector3 scale, float time, SA_EaseType easeType, Action OnCompleteAction)
	{
		ScaleGameObjectTo(go, callbackTarget, scale, time, easeType, OnCompleteAction);
	}

	public static void ScaleGameObjectTo(GameObject go, object callbackTarget, Vector3 scale, float time, SA_EaseType easeType, Action OnCompleteAction)
	{
		SA_ValuesTween sA_ValuesTween = go.AddComponent<SA_ValuesTween>();
		sA_ValuesTween.DestoryGameObjectOnComplete = false;
		sA_ValuesTween.ScaleTo(go.transform.localScale, scale, time, easeType);
		sA_ValuesTween.OnComplete.AddSafeListener(callbackTarget, OnCompleteAction);
	}

	public static Bounds GetRendererBounds(this GameObject go)
	{
		return SA_Extensions_Bounds.CalculateBounds(go);
	}

	public static Vector3 GetVertex(this GameObject go, SA_VertexX x, SA_VertexY y, SA_VertexZ z)
	{
		Bounds rendererBounds = go.GetRendererBounds();
		return rendererBounds.GetVertex(x, y, z);
	}

	public static void SetLayerRecursively(this GameObject go, int layerNumber)
	{
		Transform[] componentsInChildren = go.GetComponentsInChildren<Transform>(includeInactive: true);
		foreach (Transform transform in componentsInChildren)
		{
			transform.gameObject.layer = layerNumber;
		}
	}
}
