using com.taptap.sdk;
using System.Collections.Generic;

namespace xdsdk
{
	public class XDSDK
	{
		public static void SetCallback(XDCallback callback)
		{
			XDSDKImp.GetInstance().SetCallback(callback);
		}

		public static void HideGuest()
		{
			XDSDKImp.GetInstance().HideGuest();
		}

		public static void HideWX()
		{
			XDSDKImp.GetInstance().HideWX();
		}

		public static void HideQQ()
		{
			XDSDKImp.GetInstance().HideQQ();
		}

		public static void ShowVC()
		{
			XDSDKImp.GetInstance().ShowVC();
		}

		public static void SetQQWeb()
		{
			XDSDKImp.GetInstance().SetQQWeb();
		}

		public static void SetWXWeb()
		{
			XDSDKImp.GetInstance().SetWXWeb();
		}

		public static void HideTapTap()
		{
			XDSDKImp.GetInstance().HideTapTap();
		}

		public static void SetLoginEntries(string[] entries)
		{
			XDSDKImp.GetInstance().SetLoginEntries(entries);
		}

		public static string GetSDKVersion()
		{
			return XDSDKImp.GetInstance().GetSDKVersion();
		}

		public static string GetAdChannelName()
		{
			return XDSDKImp.GetInstance().GetAdChannelName();
		}

		public static void InitSDK(string appid, int aOrientation, string channel, string version, bool enableTapdb)
		{
			XDSDKImp.GetInstance().InitSDK(appid, aOrientation, channel, version, enableTapdb);
			TapTapListener.Init();
		}

		public static void Login()
		{
			XDSDKImp.GetInstance().Login();
		}

		public static string GetAccessToken()
		{
			return XDSDKImp.GetInstance().GetAccessToken();
		}

		public static bool IsLoggedIn()
		{
			return XDSDKImp.GetInstance().IsLoggedIn();
		}

		public static bool OpenUserCenter()
		{
			return XDSDKImp.GetInstance().OpenUserCenter();
		}

		public static void OpenRealName()
		{
			XDSDKImp.GetInstance().OpenRealName();
		}

		public static void OpenUserBindView()
		{
			XDSDKImp.GetInstance().OpenUserBindView();
		}

		public static void UserFeedback()
		{
			XDSDKImp.GetInstance().UserFeedback();
		}

		public static bool Pay(Dictionary<string, string> info)
		{
			return XDSDKImp.GetInstance().Pay(info);
		}

		public static void RestorePay(Dictionary<string, string> info)
		{
			XDSDKImp.GetInstance().RestorePay(info);
		}

		public static void Logout()
		{
			XDSDKImp.GetInstance().Logout();
		}

		public static void Exit()
		{
			XDSDKImp.GetInstance().Exit();
		}

		public static void Share(Dictionary<string, string> content)
		{
			XDSDKImp.GetInstance().Share(content);
		}

		public static void SetLevel(int level)
		{
			XDSDKImp.GetInstance().SetLevel(level);
		}

		public static void SetServer(string server)
		{
			XDSDKImp.GetInstance().SetServer(server);
		}

		public static void OnResume()
		{
			XDSDKImp.GetInstance().OnResume();
		}

		public static void OnStop()
		{
			XDSDKImp.GetInstance().OnStop();
		}
	}
}
