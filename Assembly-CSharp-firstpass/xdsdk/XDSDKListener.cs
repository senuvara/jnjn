using UnityEngine;

namespace xdsdk
{
	public class XDSDKListener : MonoBehaviour
	{
		public delegate void UniversalCallbackDelegate(int code, string msg);

		private const int OnInitSucceedCode = 1;

		private const int OnInitFailedCode = 2;

		private const int OnLoginSucceedCode = 3;

		private const int OnLoginFailedCode = 4;

		private const int OnLoginCanceledCode = 5;

		private const int OnGuestBindSucceedCode = 6;

		private const int OnRealNameSucceedCode = 7;

		private const int OnRealNameFailedCode = 8;

		private const int OnLogoutSucceedCode = 9;

		private const int OnPayCompletedCode = 10;

		private const int OnPayFailedCode = 11;

		private const int OnPayCanceledCode = 12;

		private const int OnExitConfirmCode = 13;

		private const int OnExitCancelCode = 14;

		private const int OnWXShareSucceedCode = 15;

		private const int OnWXShareFailedCode = 16;

		private void Start()
		{
			base.name = "XDSDK";
		}

		public void OnInitSucceed()
		{
			XDSDKImp.GetInstance().GetXDCallback().OnInitSucceed();
		}

		public void OnInitFailed(string msg)
		{
			XDSDKImp.GetInstance().GetXDCallback().OnInitFailed(msg);
		}

		public void OnLoginSucceed(string token)
		{
			XDSDKImp.GetInstance().GetXDCallback().OnLoginSucceed(token);
		}

		public void OnLoginFailed(string msg)
		{
			XDSDKImp.GetInstance().GetXDCallback().OnLoginFailed(msg);
		}

		public void OnLoginCanceled()
		{
			XDSDKImp.GetInstance().GetXDCallback().OnLoginCanceled();
		}

		public void OnGuestBindSucceed(string token)
		{
			XDSDKImp.GetInstance().GetXDCallback().OnGuestBindSucceed(token);
		}

		public void OnGuestBindFailed(string msg)
		{
			XDSDKImp.GetInstance().GetXDCallback().OnGuestBindFailed(msg);
		}

		public void OnRealNameSucceed()
		{
			XDSDKImp.GetInstance().GetXDCallback().OnRealNameSucceed();
		}

		public void OnRealNameFailed()
		{
			XDSDKImp.GetInstance().GetXDCallback().OnRealNameFailed(string.Empty);
		}

		public void OnRealNameFailed(string msg)
		{
			XDSDKImp.GetInstance().GetXDCallback().OnRealNameFailed(msg);
		}

		public void OnLogoutSucceed()
		{
			XDSDKImp.GetInstance().GetXDCallback().OnLogoutSucceed();
		}

		public void OnPayCompleted()
		{
			XDSDKImp.GetInstance().GetXDCallback().OnPayCompleted();
		}

		public void OnPayFailed(string msg)
		{
			XDSDKImp.GetInstance().GetXDCallback().OnPayFailed(msg);
		}

		public void OnPayCanceled()
		{
			XDSDKImp.GetInstance().GetXDCallback().OnPayCanceled();
		}

		public void RestoredPayment(string msg)
		{
			Debug.Log("RestoredPayment ： " + msg);
		}

		public void OnExitConfirm()
		{
			XDSDKImp.GetInstance().GetXDCallback().OnExitConfirm();
		}

		public void OnExitCancel()
		{
			XDSDKImp.GetInstance().GetXDCallback().OnExitCancel();
		}

		public void OnWXShareSucceed()
		{
			XDSDKImp.GetInstance().GetXDCallback().OnWXShareSucceed();
		}

		public void OnWXShareFailed(string msg)
		{
			XDSDKImp.GetInstance().GetXDCallback().OnWXShareFailed(msg);
		}

		public static void UniversalCallback(int code, string msg)
		{
			switch (code)
			{
			case 1:
				XDSDKImp.GetInstance().GetXDCallback().OnInitSucceed();
				break;
			case 2:
				XDSDKImp.GetInstance().GetXDCallback().OnInitFailed(msg);
				break;
			case 3:
				XDSDKImp.GetInstance().GetXDCallback().OnLoginSucceed(msg);
				break;
			case 4:
				XDSDKImp.GetInstance().GetXDCallback().OnLoginFailed(msg);
				break;
			case 5:
				XDSDKImp.GetInstance().GetXDCallback().OnLoginCanceled();
				break;
			case 6:
				XDSDKImp.GetInstance().GetXDCallback().OnGuestBindSucceed(msg);
				break;
			case 7:
				XDSDKImp.GetInstance().GetXDCallback().OnRealNameSucceed();
				break;
			case 8:
				XDSDKImp.GetInstance().GetXDCallback().OnRealNameFailed(msg);
				break;
			case 9:
				XDSDKImp.GetInstance().GetXDCallback().OnLogoutSucceed();
				break;
			case 10:
				XDSDKImp.GetInstance().GetXDCallback().OnPayCompleted();
				break;
			case 11:
				XDSDKImp.GetInstance().GetXDCallback().OnPayFailed(msg);
				break;
			case 12:
				XDSDKImp.GetInstance().GetXDCallback().OnPayCanceled();
				break;
			case 13:
				XDSDKImp.GetInstance().GetXDCallback().OnExitConfirm();
				break;
			case 14:
				XDSDKImp.GetInstance().GetXDCallback().OnExitCancel();
				break;
			case 15:
				XDSDKImp.GetInstance().GetXDCallback().OnWXShareSucceed();
				break;
			case 16:
				XDSDKImp.GetInstance().GetXDCallback().OnWXShareFailed(msg);
				break;
			}
		}
	}
}
