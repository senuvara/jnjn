using System.Collections.Generic;
using System.Runtime.InteropServices;
using xdsdk.Unity;

namespace xdsdk
{
	public class XDSDKImp
	{
		private static XDSDKImp _instance;

		public XDCallback xdCallback;

		public static XDSDKImp GetInstance()
		{
			if (_instance == null)
			{
				_instance = new XDSDKImp();
			}
			return _instance;
		}

		public void SetCallback(XDCallback callback)
		{
			xdCallback = callback;
			xdsdk.Unity.XDSDK.SetCallback(delegate(ResultCode code, string data)
			{
				if (xdCallback != null)
				{
					switch (code)
					{
					case ResultCode.LogoutSucceed:
						break;
					case ResultCode.InitSucceed:
						xdCallback.OnInitSucceed();
						break;
					case ResultCode.InitFailed:
						xdCallback.OnInitFailed(data);
						break;
					case ResultCode.LoginSucceed:
						xdCallback.OnLoginSucceed(data);
						break;
					case ResultCode.LoginCanceled:
						xdCallback.OnLoginCanceled();
						break;
					case ResultCode.LoginFailed:
						xdCallback.OnLoginFailed(data);
						break;
					case ResultCode.PayCompleted:
						xdCallback.OnPayCompleted();
						break;
					case ResultCode.PayCanceled:
						xdCallback.OnPayCanceled();
						break;
					case ResultCode.PayFailed:
						xdCallback.OnPayFailed(data);
						break;
					case ResultCode.RealNameSucceed:
						xdCallback.OnRealNameSucceed();
						break;
					case ResultCode.RealNameFailed:
						xdCallback.OnRealNameFailed(data);
						break;
					}
				}
			});
		}

		public XDCallback GetXDCallback()
		{
			return xdCallback;
		}

		public void HideGuest()
		{
		}

		public void HideWX()
		{
		}

		public void HideQQ()
		{
		}

		public void ShowVC()
		{
		}

		public void SetQQWeb()
		{
		}

		public void SetWXWeb()
		{
		}

		public void HideTapTap()
		{
		}

		public void SetLoginEntries(string[] entries)
		{
			xdsdk.Unity.XDSDK.SetLoginEntries(entries);
		}

		public string GetSDKVersion()
		{
			return xdsdk.Unity.XDSDK.VERSION;
		}

		public string GetAdChannelName()
		{
			return string.Empty;
		}

		public void InitSDK(string appid, int aOrientation, string channel, string version, bool enableTapDB)
		{
			xdsdk.Unity.XDSDK.Init(appid);
		}

		public void Login()
		{
			xdsdk.Unity.XDSDK.Login();
		}

		public string GetAccessToken()
		{
			return xdsdk.Unity.XDSDK.GetAccessToken();
		}

		public bool IsLoggedIn()
		{
			return !string.IsNullOrEmpty(xdsdk.Unity.XDSDK.GetAccessToken());
		}

		public bool OpenUserCenter()
		{
			return false;
		}

		public void UserFeedback()
		{
		}

		public void OpenRealName()
		{
			xdsdk.Unity.XDSDK.OpenRealName();
		}

		public void OpenUserBindView()
		{
		}

		public bool Pay(Dictionary<string, string> info)
		{
			xdsdk.Unity.XDSDK.Pay(info);
			return false;
		}

		public void RestorePay(Dictionary<string, string> info)
		{
		}

		public void Logout()
		{
			xdsdk.Unity.XDSDK.Logout();
		}

		public void Exit()
		{
		}

		public void Share(Dictionary<string, string> content)
		{
		}

		public void SetLevel(int level)
		{
		}

		public void SetServer(string server)
		{
		}

		public void OnResume()
		{
		}

		public void OnStop()
		{
		}

		[DllImport("XDSDK")]
		private static extern void UnitySetCallback(XDSDKListener.UniversalCallbackDelegate universalCallback);

		[DllImport("XDSDK")]
		private static extern void UnitySetLoginEntries(string[] entries, int length);

		[DllImport("XDSDK")]
		private static extern void UnityInit(string appid);

		[DllImport("XDSDK")]
		private static extern void UnityLogin();

		[DllImport("XDSDK")]
		private static extern void UnityLogout();

		[DllImport("XDSDK")]
		private static extern string UnityGetAccessToken();

		[DllImport("XDSDK")]
		private static extern bool UnityIsLoggedIn();

		[DllImport("XDSDK")]
		private static extern void UnityOpenRealName();

		[DllImport("XDSDK")]
		private static extern string UnityGetSdkVersion();

		[DllImport("XDSDK")]
		private static extern void UnityPay(string proudct_name, string product_id, int product_price, string sid, string role_id, string order_id, string ext);
	}
}
