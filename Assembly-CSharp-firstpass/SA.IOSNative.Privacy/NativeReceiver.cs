using SA.Common.Pattern;

namespace SA.IOSNative.Privacy
{
	public class NativeReceiver : Singleton<NativeReceiver>
	{
		public void Init()
		{
		}

		private void PermissionRequestResponseReceived(string permissionData)
		{
			PermissionsManager.PermissionRequestResponse(permissionData);
		}
	}
}
