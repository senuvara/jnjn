using UnityEngine;

[AddComponentMenu("Visualizer Studio/Controllers/Sine Wave Controller")]
public class VisSineWaveController : VisBaseController
{
	public new static class Defaults
	{
		public const float speedScalar = 1f;
	}

	public float speedScalar = 1f;

	public override void Reset()
	{
		base.Reset();
		speedScalar = 1f;
	}

	public override void Start()
	{
		base.Start();
	}

	public override float GetCustomControllerValue()
	{
		return (Mathf.Sin(Time.time * speedScalar) + 1f) * 0.5f;
	}
}
