public delegate void ValueUpdatedDelegate(float current, float previous, float difference, float adjustedDifference);
