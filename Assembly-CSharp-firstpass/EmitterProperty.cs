public enum EmitterProperty
{
	AngularVelocity,
	RandomAngularVelocity,
	EmitterVelocityScale,
	MinSize,
	MaxSize,
	MinEnergy,
	MaxEnergy,
	MinEmission,
	MaxEmission
}
