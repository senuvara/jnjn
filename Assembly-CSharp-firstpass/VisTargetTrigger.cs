using System.Collections.Generic;
using UnityEngine;

[AddComponentMenu("Visualizer Studio/Triggers/Target Trigger")]
public class VisTargetTrigger : VisBaseTrigger
{
	public List<GameObject> targetGameObjects = new List<GameObject>();

	public event TriggeredDelegate Triggered;

	public override void Start()
	{
		base.Start();
		CreateAllDelegates();
	}

	public override void OnDestroy()
	{
		base.OnDestroy();
		DestroyAllDelegates();
	}

	public override void Update()
	{
		base.Update();
	}

	public void CreateAllDelegates()
	{
		for (int i = 0; i < targetGameObjects.Count; i++)
		{
			if (!(targetGameObjects[i] != null))
			{
				continue;
			}
			MonoBehaviour[] components = targetGameObjects[i].GetComponents<MonoBehaviour>();
			for (int j = 0; j < components.Length; j++)
			{
				if (components[j] != this && components[j] is IVisTriggerTarget)
				{
					IVisTriggerTarget visTriggerTarget = components[j] as IVisTriggerTarget;
					if (visTriggerTarget != null)
					{
						Triggered += visTriggerTarget.OnTriggered;
					}
				}
			}
		}
	}

	public void DestroyAllDelegates()
	{
		for (int i = 0; i < targetGameObjects.Count; i++)
		{
			if (!(targetGameObjects[i] != null))
			{
				continue;
			}
			MonoBehaviour[] components = targetGameObjects[i].GetComponents<MonoBehaviour>();
			for (int j = 0; j < components.Length; j++)
			{
				if (components[j] != this && components[j] is IVisTriggerTarget)
				{
					IVisTriggerTarget visTriggerTarget = components[j] as IVisTriggerTarget;
					if (visTriggerTarget != null)
					{
						Triggered -= visTriggerTarget.OnTriggered;
					}
				}
			}
		}
	}

	public void AddGameObject(GameObject gameObject)
	{
		if (!(gameObject != null) || targetGameObjects.Contains(gameObject))
		{
			return;
		}
		if (gameObject == this)
		{
			Debug.LogWarning("You are adding a game object to a modifier that is the parent of this modifier.  This may have unexpected results.");
		}
		targetGameObjects.Add(gameObject);
		MonoBehaviour[] components = gameObject.GetComponents<MonoBehaviour>();
		for (int i = 0; i < components.Length; i++)
		{
			if (components[i] != this && components[i] is IVisTriggerTarget)
			{
				IVisTriggerTarget visTriggerTarget = components[i] as IVisTriggerTarget;
				if (visTriggerTarget != null)
				{
					Triggered += visTriggerTarget.OnTriggered;
				}
			}
		}
	}

	public void RemoveGameObject(GameObject gameObject)
	{
		if (!(gameObject != null) || !targetGameObjects.Contains(gameObject))
		{
			return;
		}
		targetGameObjects.Remove(gameObject);
		MonoBehaviour[] components = gameObject.GetComponents<MonoBehaviour>();
		for (int i = 0; i < components.Length; i++)
		{
			if (components[i] != this && components[i] is IVisTriggerTarget)
			{
				IVisTriggerTarget visTriggerTarget = components[i] as IVisTriggerTarget;
				if (visTriggerTarget != null)
				{
					Triggered -= visTriggerTarget.OnTriggered;
				}
			}
		}
	}

	public override void OnTriggered(float current, float previous, float difference, float adjustedDifference)
	{
		if (this.Triggered != null)
		{
			this.Triggered(base.Controller.GetCurrentValue(), base.Controller.GetPreviousValue(), base.Controller.GetValueDifference(), base.Controller.GetAdjustedValueDifference());
		}
	}
}
