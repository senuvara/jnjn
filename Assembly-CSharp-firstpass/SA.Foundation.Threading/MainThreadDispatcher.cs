using SA.Foundation.Patterns;
using System;
using System.Collections.Generic;

namespace SA.Foundation.Threading
{
	public static class MainThreadDispatcher
	{
		private class MainThreadDispatcherPlaymode : SA_Singleton<MainThreadDispatcherPlaymode>
		{
			private static readonly Queue<Action> s_executionQueue = new Queue<Action>();

			public void Init()
			{
			}

			public void Update()
			{
				lock (s_executionQueue)
				{
					while (s_executionQueue.Count > 0)
					{
						s_executionQueue.Dequeue()?.Invoke();
					}
				}
			}

			public void Enqueue(Action action)
			{
				s_executionQueue.Enqueue(action);
			}
		}

		public static void Init()
		{
			SA_Singleton<MainThreadDispatcherPlaymode>.Instance.Init();
		}

		public static void Enqueue(Action action)
		{
			if (SA_Singleton<MainThreadDispatcherPlaymode>.HasInstance)
			{
				SA_Singleton<MainThreadDispatcherPlaymode>.Instance.Enqueue(action);
			}
		}
	}
}
