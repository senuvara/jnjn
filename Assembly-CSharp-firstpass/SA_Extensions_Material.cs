using UnityEngine;

public static class SA_Extensions_Material
{
	public static void SetAlpha(this Material material, float value)
	{
		if (material.HasProperty("_Color"))
		{
			Color color = material.color;
			color.a = value;
			material.color = color;
		}
	}
}
