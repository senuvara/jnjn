public interface IVisManagerTarget
{
	VisManager Manager
	{
		get;
		set;
	}

	string LastManagerName
	{
		get;
	}
}
