using UnityEngine;

public class ChannelDebug : MonoBehaviour
{
	public GameObject theObject;

	public int audioChannel = 4;

	public float audioSensibility = 0.15f;

	public float scalefactor = 2f;

	public float lerpTime = 5f;

	private int currentChannel = 4;

	private Vector3 oldLocalScale;

	private void Start()
	{
		oldLocalScale = theObject.transform.localScale;
		currentChannel = audioChannel;
	}

	private void Update()
	{
		if (SpectrumKernel.spects[audioChannel] * SpectrumKernel.threshold >= audioSensibility)
		{
			theObject.transform.localScale = new Vector3(scalefactor, scalefactor, scalefactor);
		}
		theObject.transform.localScale = Vector3.Lerp(theObject.transform.localScale, oldLocalScale, lerpTime * Time.deltaTime);
	}

	private void OnGUI()
	{
		GUI.Box(new Rect(10f, 10f, Screen.width - 20, 25f), "USE THIS TOOL TO SETUP THE COMPONENTS VARS : AUDIO CHANNEL / AUDIO SENSIBILITY (Usually, only channels 0 => 20 are useful)");
		for (int i = 0; i < 40; i++)
		{
			if (currentChannel == i)
			{
				GUI.backgroundColor = Color.red;
			}
			else
			{
				GUI.backgroundColor = Color.white;
			}
			if (GUI.Button(new Rect(10 + (i * (Screen.width / 40) + 2), Screen.height - 100, Screen.width / 40, 25f), string.Empty + i))
			{
				currentChannel = i;
				audioChannel = i;
			}
		}
		GUI.backgroundColor = Color.black;
		audioSensibility = GUI.HorizontalSlider(new Rect(15f, Screen.height - 60, Screen.width - 30, 40f), audioSensibility, 0f, 1f);
		GUI.backgroundColor = Color.white;
		GUI.Button(new Rect(10f, (float)(Screen.height + 6 - Screen.height / 4) - audioSensibility * ((float)Screen.height / 1.1f), Screen.width - 20, 0f), string.Empty);
		GUI.backgroundColor = Color.black;
		GUI.Box(new Rect(10f, Screen.height - 30, Screen.width - 20, 25f), "AUDIO CHANNEL = " + currentChannel + " /  AROUND AUDIO SENSIBILITY VALUE = " + audioSensibility);
	}
}
