namespace SA.Foundation.Config
{
	public class SA_Config
	{
		public const string STANS_ASSETS_SUPPORT_EMAIL = "support@stansassets.com";

		public const string STANS_ASSETS_CEO_EMAIL = "ceo@stansassets.com";

		public const string STANS_ASSETS_WEBSITE_ROOT_URL = "https://stansassets.com/";

		public const string STANS_ASSETS_PLUGINS_PATH = "Plugins/StansAssets/";

		public const string STANS_ASSETS_NATIVE_PLUGINS_PATH = "Plugins/StansAssets/NativePlugins/";

		public const string STANS_ASSETS_FOUNDATION_PATH = "Plugins/StansAssets/Foundation/";

		public const string STANS_ASSETS_SETTINGS_PATH = "Plugins/StansAssets/Settings/Resources/";

		public const string UNITY_IOS_PLUGINS_PATH = "Plugins/IOS/";

		public const string UNITY_ANDROID_PLUGINS_PATH = "Plugins/Android/";

		public const string EDITOR_MENU_ROOT = "Stan's Assets/";

		public const string EDITOR_SUPPORT_LIB_MENU_ROOT = "Stan's Assets/Support/";
	}
}
