using System;
using UnityEngine;

public static class SA_Extensions_String
{
	public static string GetLast(this string source, int count)
	{
		if (count >= source.Length)
		{
			return source;
		}
		return source.Substring(source.Length - count);
	}

	public static string GetFirst(this string source, int count)
	{
		if (count >= source.Length)
		{
			return source;
		}
		return source.Substring(0, count);
	}

	public static void CopyToClipboard(this string source)
	{
		TextEditor textEditor = new TextEditor();
		textEditor.text = source;
		textEditor.SelectAll();
		textEditor.Copy();
	}

	public static Uri CovertToURI(this string source)
	{
		return new Uri(source);
	}
}
