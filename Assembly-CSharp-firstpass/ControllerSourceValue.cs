public enum ControllerSourceValue
{
	Current,
	Previous,
	Difference
}
