using UnityEngine;

[AddComponentMenu("Visualizer Studio/Modifiers/Material Lerp Modifier")]
public class VisMaterialLerpModifier : VisBasePropertyModifier
{
	public new static class Defaults
	{
	}

	public Material lerpFromMaterial;

	public Material lerpToMaterial;

	public override void Reset()
	{
		base.Reset();
	}

	public override void Start()
	{
		base.Start();
	}

	public override void SetProperty(float propertyValue)
	{
		Renderer component = GetComponent<Renderer>();
		if (component != null && component.material != null && lerpFromMaterial != null && lerpToMaterial != null)
		{
			component.material.Lerp(lerpFromMaterial, lerpToMaterial, Mathf.Clamp(propertyValue, 0f, 1f));
		}
	}
}
