using SA.Foundation.Animation;
using System;
using UnityEngine;

public static class SA_Extensions_RectTransform
{
	public static void MoveTo(this RectTransform transform, object callbackTarget, Vector2 position, float time, SA_EaseType easeType = SA_EaseType.linear, Action OnCompleteAction = null)
	{
		SA_ValuesTween sA_ValuesTween = SA_ValuesTween.Create();
		sA_ValuesTween.VectorTo(transform.anchoredPosition, position, time, easeType);
		sA_ValuesTween.DestoryGameObjectOnComplete = true;
		sA_ValuesTween.OnVectorValueChanged.AddSafeListener(transform, delegate(Vector3 pos)
		{
			transform.anchoredPosition = pos;
		});
		sA_ValuesTween.OnComplete.AddSafeListener(callbackTarget, OnCompleteAction);
	}

	public static Rect GetScreenRect(this RectTransform transform)
	{
		Vector3[] array = new Vector3[4];
		transform.GetWorldCorners(array);
		Rect rect = new Rect(new Vector2(array[0].x, array[0].y), new Vector2(array[3].x - array[0].x, array[1].y - array[0].y));
		Canvas componentInParent = transform.GetComponentInParent<Canvas>();
		Vector3[] array2 = new Vector3[4];
		componentInParent.GetComponent<RectTransform>().GetWorldCorners(array2);
		Rect rect2 = new Rect(new Vector2(array2[0].x, array2[0].y), new Vector2(array2[3].x - array2[0].x, array2[1].y - array2[0].y));
		int width = Screen.width;
		int height = Screen.height;
		float num = width;
		Vector2 size = rect2.size;
		float num2 = num / size.x;
		Vector2 size2 = rect.size;
		float x = num2 * size2.x;
		float num3 = height;
		Vector2 size3 = rect2.size;
		float num4 = num3 / size3.y;
		Vector2 size4 = rect.size;
		Vector2 vector = new Vector2(x, num4 * size4.y);
		float num5 = width;
		float num6 = rect.x - rect2.x;
		Vector2 size5 = rect2.size;
		float x2 = num5 * (num6 / size5.x);
		float num7 = height;
		float num8 = 0f - rect2.y + rect.y;
		Vector2 size6 = rect2.size;
		Rect result = new Rect(x2, num7 * (num8 / size6.y), vector.x, vector.y);
		return result;
	}
}
