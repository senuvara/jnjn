using UnityEngine;

[AddComponentMenu("Visualizer Studio/Triggers/Animation State Property Trigger")]
public class VisAnimationStatePropertyTrigger : VisBasePropertyTrigger
{
	public new static class Defaults
	{
		public const AnimationStateProperty targetProperty = AnimationStateProperty.NormalizedTime;

		public const string targetAnimation = "Idle";
	}

	public AnimationStateProperty targetProperty = AnimationStateProperty.NormalizedTime;

	public string targetAnimation = "Idle";

	public override void Reset()
	{
		base.Reset();
		targetProperty = AnimationStateProperty.NormalizedTime;
		targetAnimation = "Idle";
	}

	public override void Start()
	{
		base.Start();
	}

	public override void SetProperty(float propertyValue)
	{
		Animation component = GetComponent<Animation>();
		if (component != null && GetComponent<Animation>().IsPlaying(targetAnimation))
		{
			VisPropertyHelper.SetAnimationStateProperty(component[targetAnimation], targetProperty, propertyValue);
		}
	}
}
