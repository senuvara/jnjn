public enum VisDataValueType
{
	Average,
	Median,
	Sum,
	Minimum,
	Maximum
}
