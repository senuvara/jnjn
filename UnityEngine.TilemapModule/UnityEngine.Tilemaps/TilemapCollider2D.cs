using UnityEngine.Bindings;

namespace UnityEngine.Tilemaps
{
	/// <summary>
	///   <para>Collider for 2D physics representing shapes defined by the corresponding Tilemap.</para>
	/// </summary>
	[NativeType(Header = "Modules/Tilemap/Public/TilemapCollider2D.h")]
	[RequireComponent(typeof(Tilemap))]
	public sealed class TilemapCollider2D : Collider2D
	{
	}
}
