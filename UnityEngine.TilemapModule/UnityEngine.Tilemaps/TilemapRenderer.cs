using System.Runtime.CompilerServices;
using UnityEngine.Bindings;

namespace UnityEngine.Tilemaps
{
	/// <summary>
	///   <para>The tile map renderer is used to render the tile map marked out by a component.</para>
	/// </summary>
	[NativeType(Header = "Modules/Tilemap/Public/TilemapRenderer.h")]
	[NativeHeader("Modules/Tilemap/TilemapRendererJobs.h")]
	[NativeHeader("Modules/Grid/Public/GridMarshalling.h")]
	[RequireComponent(typeof(Tilemap))]
	public sealed class TilemapRenderer : Renderer
	{
		/// <summary>
		///   <para>Sort order for all tiles rendered by the TilemapRenderer.</para>
		/// </summary>
		public enum SortOrder
		{
			/// <summary>
			///   <para>Sorts tiles for rendering starting from the tile with the lowest X and the lowest Y cell positions.</para>
			/// </summary>
			BottomLeft,
			/// <summary>
			///   <para>Sorts tiles for rendering starting from the tile with the highest X and the lowest Y cell positions.</para>
			/// </summary>
			BottomRight,
			/// <summary>
			///   <para>Sorts tiles for rendering starting from the tile with the lowest X and the highest Y cell positions.</para>
			/// </summary>
			TopLeft,
			/// <summary>
			///   <para>Sorts tiles for rendering starting from the tile with the highest X and the lowest Y cell positions.</para>
			/// </summary>
			TopRight
		}

		/// <summary>
		///   <para>Size in number of tiles of each chunk created by the TilemapRenderer.</para>
		/// </summary>
		public Vector3Int chunkSize
		{
			get
			{
				get_chunkSize_Injected(out Vector3Int ret);
				return ret;
			}
			set
			{
				set_chunkSize_Injected(ref value);
			}
		}

		/// <summary>
		///   <para>Maximum number of chunks the TilemapRenderer caches in memory.</para>
		/// </summary>
		public int maxChunkCount
		{
			[MethodImpl(MethodImplOptions.InternalCall)]
			get;
			[MethodImpl(MethodImplOptions.InternalCall)]
			set;
		}

		/// <summary>
		///   <para>Maximum number of frames the TilemapRenderer keeps unused chunks in memory.</para>
		/// </summary>
		public int maxFrameAge
		{
			[MethodImpl(MethodImplOptions.InternalCall)]
			get;
			[MethodImpl(MethodImplOptions.InternalCall)]
			set;
		}

		/// <summary>
		///   <para>Active sort order for the TilemapRenderer.</para>
		/// </summary>
		public SortOrder sortOrder
		{
			[MethodImpl(MethodImplOptions.InternalCall)]
			get;
			[MethodImpl(MethodImplOptions.InternalCall)]
			set;
		}

		/// <summary>
		///   <para>Specifies how the Tilemap interacts with the masks.</para>
		/// </summary>
		public SpriteMaskInteraction maskInteraction
		{
			[MethodImpl(MethodImplOptions.InternalCall)]
			get;
			[MethodImpl(MethodImplOptions.InternalCall)]
			set;
		}

		[MethodImpl(MethodImplOptions.InternalCall)]
		private extern void get_chunkSize_Injected(out Vector3Int ret);

		[MethodImpl(MethodImplOptions.InternalCall)]
		private extern void set_chunkSize_Injected(ref Vector3Int value);
	}
}
