using System;
using System.Collections.Generic;
using System.Linq;
using System.Reflection;

namespace AssemblyLister
{
	public static class Lister
	{
		public static IEnumerable<Assembly> AllAssemblies
		{
			get
			{
				HashSet<string> seen = new HashSet<string>();
				return AppDomain.CurrentDomain.GetAssemblies().SelectMany((Assembly asm) => asm.DeepWalkReferences(seen));
			}
		}

		private static IEnumerable<Assembly> DeepWalkReferences(this Assembly assembly, HashSet<string> seen = null)
		{
			seen = (seen ?? new HashSet<string>());
			if (!seen.Add(assembly.FullName))
			{
				return Enumerable.Empty<Assembly>();
			}
			List<Assembly> list = new List<Assembly>();
			list.Add(assembly);
			AssemblyName[] referencedAssemblies = assembly.GetReferencedAssemblies();
			foreach (AssemblyName assemblyName in referencedAssemblies)
			{
				if (!seen.Contains(assemblyName.FullName))
				{
					Assembly assembly2 = Assembly.Load(assemblyName);
					list.AddRange(assembly2.DeepWalkReferences(seen));
				}
			}
			return list;
		}
	}
}
