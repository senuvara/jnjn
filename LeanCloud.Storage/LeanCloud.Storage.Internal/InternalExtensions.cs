using System;
using System.Collections.Generic;
using System.Linq;
using System.Runtime.ExceptionServices;
using System.Threading.Tasks;

namespace LeanCloud.Storage.Internal
{
	public static class InternalExtensions
	{
		public delegate void PartialAccessor<T>(ref T arg);

		public static Task<T> Safe<T>(this Task<T> task)
		{
			return task ?? Task.FromResult(default(T));
		}

		public static Task Safe(this Task task)
		{
			return task ?? Task.FromResult<object>(null);
		}

		public static TValue GetOrDefault<TKey, TValue>(this IDictionary<TKey, TValue> self, TKey key, TValue defaultValue)
		{
			if (self.TryGetValue(key, out TValue value))
			{
				return value;
			}
			return defaultValue;
		}

		public static bool CollectionsEqual<T>(this IEnumerable<T> a, IEnumerable<T> b)
		{
			if (!object.Equals(a, b))
			{
				if (a != null && b != null)
				{
					return a.SequenceEqual(b);
				}
				return false;
			}
			return true;
		}

		public static Task<TResult> OnSuccess<TIn, TResult>(this Task<TIn> task, Func<Task<TIn>, TResult> continuation)
		{
			return task.OnSuccess((Task t) => continuation((Task<TIn>)t));
		}

		public static Task OnSuccess<TIn>(this Task<TIn> task, Action<Task<TIn>> continuation)
		{
			return task.OnSuccess((Func<Task<TIn>, object>)delegate(Task<TIn> t)
			{
				continuation(t);
				return null;
			});
		}

		public static Task<TResult> OnSuccess<TResult>(this Task task, Func<Task, TResult> continuation)
		{
			return task.ContinueWith(delegate(Task t)
			{
				if (t.IsFaulted)
				{
					AggregateException ex = t.Exception.Flatten();
					if (ex.InnerExceptions.Count == 1)
					{
						ExceptionDispatchInfo.Capture(ex.InnerExceptions[0]).Throw();
					}
					else
					{
						ExceptionDispatchInfo.Capture(ex).Throw();
					}
					return Task.FromResult(default(TResult));
				}
				if (t.IsCanceled)
				{
					TaskCompletionSource<TResult> taskCompletionSource = new TaskCompletionSource<TResult>();
					taskCompletionSource.SetCanceled();
					return taskCompletionSource.Task;
				}
				return Task.FromResult(continuation(t));
			}).Unwrap();
		}

		public static Task OnSuccess(this Task task, Action<Task> continuation)
		{
			return task.OnSuccess((Func<Task, object>)delegate(Task t)
			{
				continuation(t);
				return null;
			});
		}

		public static Task WhileAsync(Func<Task<bool>> predicate, Func<Task> body)
		{
			Func<Task> iterate = null;
			iterate = (() => predicate().OnSuccess((Task<bool> t) => (!t.Result) ? Task.FromResult(0) : body().OnSuccess((Task _) => iterate()).Unwrap()).Unwrap());
			return iterate();
		}
	}
}
