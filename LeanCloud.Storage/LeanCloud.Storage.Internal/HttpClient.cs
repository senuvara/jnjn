using System;
using System.Collections.Generic;
using System.IO;
using System.Net;
using System.Threading;
using System.Threading.Tasks;
using UnityEngine.Networking;

namespace LeanCloud.Storage.Internal
{
	public class HttpClient : IHttpClient
	{
		private static bool isCompiledByIL2CPP = AppDomain.CurrentDomain.FriendlyName.Equals("IL2CPP Root Domain");

		public Task<Tuple<HttpStatusCode, string>> ExecuteAsync(HttpRequest httpRequest, IProgress<AVUploadProgressEventArgs> uploadProgress, IProgress<AVDownloadProgressEventArgs> downloadProgress, CancellationToken cancellationToken)
		{
			TaskCompletionSource<Tuple<HttpStatusCode, string>> tcs = new TaskCompletionSource<Tuple<HttpStatusCode, string>>();
			cancellationToken.Register(delegate
			{
				tcs.TrySetCanceled();
			});
			uploadProgress = (uploadProgress ?? new Progress<AVUploadProgressEventArgs>());
			downloadProgress = (downloadProgress ?? new Progress<AVDownloadProgressEventArgs>());
			Task task = null;
			IDisposable toDisposeAfterReading = null;
			byte[] bytes = null;
			if (httpRequest.Data != null)
			{
				MemoryStream ms = new MemoryStream();
				toDisposeAfterReading = ms;
				task = httpRequest.Data.CopyToAsync(ms).OnSuccess(delegate
				{
					bytes = ms.ToArray();
				});
			}
			task.Safe().ContinueWith(delegate(Task t)
			{
				if (toDisposeAfterReading != null)
				{
					toDisposeAfterReading.Dispose();
				}
				return t;
			}).Unwrap()
				.OnSuccess(delegate
				{
					float oldDownloadProgress = 0f;
					float oldUploadProgress = 0f;
					Dispatcher.Instance.Post(delegate
					{
						WaitForWebRequest(GenerateRequest(httpRequest, bytes), delegate(UnityWebRequest request)
						{
							if (cancellationToken.IsCancellationRequested)
							{
								tcs.TrySetCanceled();
							}
							else if (request.isDone)
							{
								uploadProgress.Report(new AVUploadProgressEventArgs
								{
									Progress = 1.0
								});
								downloadProgress.Report(new AVDownloadProgressEventArgs
								{
									Progress = 1.0
								});
								HttpStatusCode responseStatusCode = GetResponseStatusCode(request);
								if (!string.IsNullOrEmpty(request.error) && string.IsNullOrEmpty(request.downloadHandler.text))
								{
									string item = $"{{\"error\":\"{request.error}\"}}";
									tcs.TrySetResult(new Tuple<HttpStatusCode, string>(responseStatusCode, item));
								}
								else
								{
									tcs.TrySetResult(new Tuple<HttpStatusCode, string>(responseStatusCode, request.downloadHandler.text));
								}
							}
							else
							{
								float uploadProgress2 = request.uploadProgress;
								if (oldUploadProgress < uploadProgress2)
								{
									uploadProgress.Report(new AVUploadProgressEventArgs
									{
										Progress = uploadProgress2
									});
								}
								oldUploadProgress = uploadProgress2;
								float downloadProgress2 = request.downloadProgress;
								if (oldDownloadProgress < downloadProgress2)
								{
									downloadProgress.Report(new AVDownloadProgressEventArgs
									{
										Progress = downloadProgress2
									});
								}
								oldDownloadProgress = downloadProgress2;
							}
						});
					});
				});
			return tcs.Task.ContinueWith(delegate
			{
				TaskCompletionSource<object> dispatchTcs = new TaskCompletionSource<object>();
				if (isCompiledByIL2CPP)
				{
					new Thread((ParameterizedThreadStart)delegate
					{
						dispatchTcs.TrySetResult(null);
					}).Start();
				}
				else
				{
					ThreadPool.QueueUserWorkItem(delegate
					{
						dispatchTcs.TrySetResult(null);
					});
				}
				return dispatchTcs.Task;
			}).Unwrap().ContinueWith((Task<object> _) => tcs.Task)
				.Unwrap();
		}

		private static HttpStatusCode GetResponseStatusCode(UnityWebRequest request)
		{
			if (Enum.IsDefined(typeof(HttpStatusCode), (int)request.responseCode))
			{
				return (HttpStatusCode)request.responseCode;
			}
			return HttpStatusCode.BadRequest;
		}

		private static UnityWebRequest GenerateRequest(HttpRequest request, byte[] bytes)
		{
			UnityWebRequest unityWebRequest = new UnityWebRequest();
			unityWebRequest.method = request.Method;
			unityWebRequest.url = request.Uri.AbsoluteUri;
			unityWebRequest.SetRequestHeader("Content-Type", "application/json");
			if (request.Headers != null)
			{
				foreach (KeyValuePair<string, string> header in request.Headers)
				{
					unityWebRequest.SetRequestHeader(header.Key, header.Value);
				}
			}
			if (bytes != null)
			{
				unityWebRequest.uploadHandler = new UploadHandlerRaw(bytes);
			}
			unityWebRequest.downloadHandler = new DownloadHandlerBuffer();
			unityWebRequest.Send();
			return unityWebRequest;
		}

		private static void WaitForWebRequest(UnityWebRequest request, Action<UnityWebRequest> action)
		{
			Dispatcher.Instance.Post(delegate
			{
				bool isDone = request.isDone;
				action(request);
				if (!isDone)
				{
					WaitForWebRequest(request, action);
				}
			});
		}
	}
}
