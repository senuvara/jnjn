using System;
using System.Collections;
using System.Collections.Generic;
using System.Threading;
using UnityEngine;

namespace LeanCloud.Storage.Internal
{
	public sealed class Dispatcher
	{
		private readonly ReaderWriterLockSlim dispatchQueueLock = new ReaderWriterLockSlim();

		private readonly Queue<Action> dispatchQueue = new Queue<Action>();

		public static Dispatcher Instance
		{
			get;
			private set;
		}

		public GameObject GameObject
		{
			get;
			set;
		}

		public IEnumerator DispatcherCoroutine
		{
			get;
			private set;
		}

		static Dispatcher()
		{
			Instance = new Dispatcher();
		}

		private Dispatcher()
		{
			DispatcherCoroutine = CreateDispatcherCoroutine();
		}

		public void Post(Action action)
		{
			if (dispatchQueueLock.IsWriteLockHeld)
			{
				dispatchQueue.Enqueue(action);
				return;
			}
			dispatchQueueLock.EnterWriteLock();
			try
			{
				dispatchQueue.Enqueue(action);
			}
			finally
			{
				dispatchQueueLock.ExitWriteLock();
			}
		}

		private IEnumerator CreateDispatcherCoroutine()
		{
			yield return null;
			while (true)
			{
				dispatchQueueLock.EnterUpgradeableReadLock();
				try
				{
					int num = dispatchQueue.Count;
					if (num > 0)
					{
						dispatchQueueLock.EnterWriteLock();
						try
						{
							while (num > 0)
							{
								try
								{
									dispatchQueue.Dequeue()();
								}
								catch (Exception exception)
								{
									Debug.LogException(exception);
								}
								num--;
							}
						}
						finally
						{
							dispatchQueueLock.ExitWriteLock();
						}
					}
				}
				finally
				{
					dispatchQueueLock.ExitUpgradeableReadLock();
				}
				yield return null;
			}
		}
	}
}
