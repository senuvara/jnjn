using System;
using System.Collections.Generic;
using System.Linq;
using System.Reflection;

namespace LeanCloud.Storage.Internal
{
	public static class ReflectionHelpers
	{
		public static IEnumerable<PropertyInfo> GetProperties(Type type)
		{
			return type.GetProperties();
		}

		public static MethodInfo GetMethod(Type type, string name, Type[] parameters)
		{
			return type.GetMethod(name, parameters);
		}

		public static bool IsPrimitive(Type type)
		{
			return type.IsPrimitive;
		}

		public static IEnumerable<Type> GetInterfaces(Type type)
		{
			return type.GetInterfaces();
		}

		public static bool IsConstructedGenericType(Type type)
		{
			if (type.IsGenericType)
			{
				return !type.IsGenericTypeDefinition;
			}
			return false;
		}

		public static IEnumerable<ConstructorInfo> GetConstructors(Type type)
		{
			BindingFlags bindingAttr = BindingFlags.Instance | BindingFlags.Public | BindingFlags.NonPublic;
			return type.GetConstructors(bindingAttr);
		}

		public static Type[] GetGenericTypeArguments(Type type)
		{
			return type.GetGenericArguments();
		}

		public static PropertyInfo GetProperty(Type type, string name)
		{
			return type.GetProperty(name);
		}

		public static ConstructorInfo FindConstructor(this Type self, params Type[] parameterTypes)
		{
			return (from constructor in GetConstructors(self)
				let parameters = constructor.GetParameters()
				let types = parameters.Select((ParameterInfo p) => p.ParameterType)
				where types.SequenceEqual(parameterTypes)
				select constructor).SingleOrDefault();
		}

		public static bool IsNullable(Type t)
		{
			if (t.IsGenericType && !t.IsGenericTypeDefinition)
			{
				return t.GetGenericTypeDefinition().Equals(typeof(Nullable<>));
			}
			return false;
		}

		public static IEnumerable<T> GetCustomAttributes<T>(this Assembly assembly) where T : Attribute
		{
			return from attr in assembly.GetCustomAttributes(typeof(T), inherit: false)
				select attr as T;
		}
	}
}
