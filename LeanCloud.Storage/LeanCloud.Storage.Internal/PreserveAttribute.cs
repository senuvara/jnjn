using System;

namespace LeanCloud.Storage.Internal
{
	[AttributeUsage(AttributeTargets.All)]
	internal class PreserveAttribute : Attribute
	{
		public bool AllMembers;

		public bool Conditional;
	}
}
