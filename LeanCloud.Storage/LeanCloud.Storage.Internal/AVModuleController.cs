using AssemblyLister;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Reflection;

namespace LeanCloud.Storage.Internal
{
	public class AVModuleController
	{
		private static readonly AVModuleController instance = new AVModuleController();

		private readonly object mutex = new object();

		private readonly List<IAVModule> modules = new List<IAVModule>();

		private bool isParseInitialized;

		public static AVModuleController Instance => instance;

		public void RegisterModule(IAVModule module)
		{
			if (module != null)
			{
				lock (mutex)
				{
					modules.Add(module);
					module.OnModuleRegistered();
					if (isParseInitialized)
					{
						module.OnParseInitialized();
					}
				}
			}
		}

		public void ScanForModules()
		{
			IEnumerable<Type> enumerable = from attr in Lister.AllAssemblies.SelectMany((Assembly asm) => asm.GetCustomAttributes<AVModuleAttribute>())
				select attr.ModuleType into type
				where type?.GetTypeInfo().ImplementedInterfaces.Contains(typeof(IAVModule)) ?? false
				select type;
			lock (mutex)
			{
				foreach (Type item in enumerable)
				{
					try
					{
						ConstructorInfo constructorInfo = item.FindConstructor();
						if (constructorInfo != null)
						{
							IAVModule module = constructorInfo.Invoke(new object[0]) as IAVModule;
							RegisterModule(module);
						}
					}
					catch (Exception)
					{
					}
				}
			}
		}

		public void Reset()
		{
			lock (mutex)
			{
				modules.Clear();
				isParseInitialized = false;
			}
		}

		public void LeanCloudDidInitialize()
		{
			lock (mutex)
			{
				foreach (IAVModule module in modules)
				{
					module.OnParseInitialized();
				}
				isParseInitialized = true;
			}
		}
	}
}
