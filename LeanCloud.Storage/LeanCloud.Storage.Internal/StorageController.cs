using System;
using System.Collections;
using System.Collections.Generic;
using System.IO;
using System.Linq;
using System.Threading;
using System.Threading.Tasks;
using UnityEngine;

namespace LeanCloud.Storage.Internal
{
	public class StorageController : IStorageController
	{
		private class StorageDictionary : IStorageDictionary<string, object>, IEnumerable<KeyValuePair<string, object>>, IEnumerable
		{
			private object mutex;

			private Dictionary<string, object> dictionary;

			private string settingsPath;

			private bool isWebPlayer;

			public IEnumerable<string> Keys
			{
				get
				{
					lock (mutex)
					{
						return dictionary.Keys;
					}
				}
			}

			public IEnumerable<object> Values
			{
				get
				{
					lock (mutex)
					{
						return dictionary.Values;
					}
				}
			}

			public object this[string key]
			{
				get
				{
					lock (mutex)
					{
						return dictionary[key];
					}
				}
			}

			public int Count
			{
				get
				{
					lock (mutex)
					{
						return dictionary.Count;
					}
				}
			}

			public StorageDictionary(string settingsPath, bool isWebPlayer)
			{
				this.settingsPath = settingsPath;
				this.isWebPlayer = isWebPlayer;
				mutex = new object();
				dictionary = new Dictionary<string, object>();
			}

			internal Task SaveAsync()
			{
				string value;
				lock (mutex)
				{
					value = Json.Encode(dictionary);
				}
				if (isWebPlayer)
				{
					PlayerPrefs.SetString("LeanCloud.settings", value);
					PlayerPrefs.Save();
				}
				else if (Application.platform == RuntimePlatform.tvOS)
				{
					Debug.Log("Running on TvOS, prefs cannot be saved.");
				}
				else
				{
					using (FileStream stream = new FileStream(settingsPath, FileMode.Create, FileAccess.Write))
					{
						using (StreamWriter streamWriter = new StreamWriter(stream))
						{
							streamWriter.Write(value);
						}
					}
				}
				return Task.FromResult<object>(null);
			}

			internal Task LoadAsync()
			{
				string text = null;
				try
				{
					if (isWebPlayer)
					{
						text = PlayerPrefs.GetString("LeanCloud.settings", null);
					}
					else if (Application.platform == RuntimePlatform.tvOS)
					{
						Debug.Log("Running on TvOS, prefs cannot be loaded.");
					}
					else
					{
						using (FileStream stream = new FileStream(settingsPath, FileMode.Open, FileAccess.Read))
						{
							text = new StreamReader(stream).ReadToEnd();
						}
					}
				}
				catch (Exception)
				{
				}
				if (text == null)
				{
					lock (mutex)
					{
						this.dictionary = new Dictionary<string, object>();
						return Task.FromResult<object>(null);
					}
				}
				Dictionary<string, object> dictionary = Json.Parse(text) as Dictionary<string, object>;
				lock (mutex)
				{
					this.dictionary = (dictionary ?? new Dictionary<string, object>());
					return Task.FromResult<object>(null);
				}
			}

			internal void Update(IDictionary<string, object> contents)
			{
				lock (mutex)
				{
					dictionary = contents.ToDictionary((KeyValuePair<string, object> p) => p.Key, (KeyValuePair<string, object> p) => p.Value);
				}
			}

			public Task AddAsync(string key, object value)
			{
				lock (mutex)
				{
					dictionary[key] = value;
				}
				return SaveAsync();
			}

			public Task RemoveAsync(string key)
			{
				lock (mutex)
				{
					dictionary.Remove(key);
				}
				return SaveAsync();
			}

			public bool ContainsKey(string key)
			{
				lock (mutex)
				{
					return dictionary.ContainsKey(key);
				}
			}

			public bool TryGetValue(string key, out object value)
			{
				lock (mutex)
				{
					return dictionary.TryGetValue(key, out value);
				}
			}

			public IEnumerator<KeyValuePair<string, object>> GetEnumerator()
			{
				lock (mutex)
				{
					return dictionary.GetEnumerator();
				}
			}

			IEnumerator IEnumerable.GetEnumerator()
			{
				lock (mutex)
				{
					return dictionary.GetEnumerator();
				}
			}
		}

		private const string LeanCloudStorageFileName = "LeanCloud.settings";

		private TaskQueue taskQueue = new TaskQueue();

		private string settingsPath;

		private StorageDictionary storageDictionary;

		private bool isWebPlayer;

		public StorageController()
			: this(Path.Combine(Application.persistentDataPath, "LeanCloud.settings"), isWebPlayer: false)
		{
		}

		public StorageController(string settingsPath)
			: this(settingsPath, isWebPlayer: false)
		{
		}

		public StorageController(bool isWebPlayer)
			: this(Path.Combine(Application.persistentDataPath, "LeanCloud.settings"), isWebPlayer)
		{
		}

		public StorageController(string settingsPath, bool isWebPlayer)
		{
			this.settingsPath = settingsPath;
			this.isWebPlayer = isWebPlayer;
		}

		public Task<IStorageDictionary<string, object>> LoadAsync()
		{
			return taskQueue.Enqueue((Task toAwait) => toAwait.ContinueWith(delegate
			{
				if (storageDictionary != null)
				{
					return Task.FromResult((IStorageDictionary<string, object>)storageDictionary);
				}
				storageDictionary = new StorageDictionary(settingsPath, isWebPlayer);
				return storageDictionary.LoadAsync().OnSuccess((Func<Task, IStorageDictionary<string, object>>)((Task __) => storageDictionary));
			}).Unwrap(), CancellationToken.None);
		}

		public Task<IStorageDictionary<string, object>> SaveAsync(IDictionary<string, object> contents)
		{
			return taskQueue.Enqueue((Task toAwait) => toAwait.ContinueWith(delegate
			{
				if (storageDictionary == null)
				{
					storageDictionary = new StorageDictionary(settingsPath, isWebPlayer);
				}
				storageDictionary.Update(contents);
				return storageDictionary.SaveAsync().OnSuccess((Func<Task, IStorageDictionary<string, object>>)((Task __) => storageDictionary));
			}).Unwrap(), CancellationToken.None);
		}
	}
}
