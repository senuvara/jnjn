using System.Collections.Generic;
using System.Runtime.CompilerServices;

namespace LeanCloud.Storage.Internal
{
	public class IdentityEqualityComparer<T> : IEqualityComparer<T>
	{
		public bool Equals(T x, T y)
		{
			return (object)x == (object)y;
		}

		public int GetHashCode(T obj)
		{
			return RuntimeHelpers.GetHashCode(obj);
		}
	}
}
