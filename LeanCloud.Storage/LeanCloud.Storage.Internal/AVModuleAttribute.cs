using System;

namespace LeanCloud.Storage.Internal
{
	[AttributeUsage(AttributeTargets.Assembly)]
	public class AVModuleAttribute : Attribute
	{
		public Type ModuleType
		{
			get;
			private set;
		}

		public AVModuleAttribute(Type ModuleType)
		{
			this.ModuleType = ModuleType;
		}
	}
}
