using System;
using System.Threading;
using System.Threading.Tasks;

namespace LeanCloud.Storage.Internal
{
	public class TaskQueue
	{
		private Task tail;

		private readonly object mutex = new object();

		public object Mutex => mutex;

		private Task GetTaskToAwait(CancellationToken cancellationToken)
		{
			lock (mutex)
			{
				return (tail ?? Task.FromResult(result: true)).ContinueWith(delegate
				{
				}, cancellationToken);
			}
		}

		public T Enqueue<T>(Func<Task, T> taskStart, CancellationToken cancellationToken) where T : Task
		{
			lock (mutex)
			{
				Task task = tail ?? Task.FromResult(result: true);
				T val = taskStart(GetTaskToAwait(cancellationToken));
				tail = Task.WhenAll(task, val);
				return val;
			}
		}
	}
}
