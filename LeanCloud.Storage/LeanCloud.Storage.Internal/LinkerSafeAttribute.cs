using System;

namespace LeanCloud.Storage.Internal
{
	[AttributeUsage(AttributeTargets.All)]
	internal class LinkerSafeAttribute : Attribute
	{
	}
}
