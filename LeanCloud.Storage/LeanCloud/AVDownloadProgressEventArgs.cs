using System;

namespace LeanCloud
{
	public class AVDownloadProgressEventArgs : EventArgs
	{
		public double Progress
		{
			get;
			set;
		}
	}
}
