using System;

namespace LeanCloud
{
	public class AVUploadProgressEventArgs : EventArgs
	{
		public double Progress
		{
			get;
			set;
		}
	}
}
