using System.Collections;
using System.Runtime.CompilerServices;
using System.Runtime.InteropServices;
using System.Threading;

namespace System.Globalization
{
	/// <summary>Provides information about a specific culture (called a "locale" for unmanaged code development). The information includes the names for the culture, the writing system, the calendar used, and formatting for dates and sort strings.</summary>
	[Serializable]
	[ComVisible(true)]
	public class CultureInfo : ICloneable, IFormatProvider
	{
		private const int NumOptionalCalendars = 5;

		private const int GregorianTypeMask = 16777215;

		private const int CalendarTypeBits = 24;

		private const int InvariantCultureId = 127;

		private static volatile CultureInfo invariant_culture_info;

		private static object shared_table_lock;

		internal static int BootstrapCultureID;

		private bool m_isReadOnly;

		private int cultureID;

		[NonSerialized]
		private int parent_lcid;

		[NonSerialized]
		private int specific_lcid;

		[NonSerialized]
		private int datetime_index;

		[NonSerialized]
		private int number_index;

		private bool m_useUserOverride;

		[NonSerialized]
		private volatile NumberFormatInfo numInfo;

		private volatile DateTimeFormatInfo dateTimeInfo;

		private volatile TextInfo textInfo;

		private string m_name;

		[NonSerialized]
		private string displayname;

		[NonSerialized]
		private string englishname;

		[NonSerialized]
		private string nativename;

		[NonSerialized]
		private string iso3lang;

		[NonSerialized]
		private string iso2lang;

		[NonSerialized]
		private string icu_name;

		[NonSerialized]
		private string win3lang;

		[NonSerialized]
		private string territory;

		private volatile CompareInfo compareInfo;

		[NonSerialized]
		private unsafe readonly int* calendar_data;

		[NonSerialized]
		private unsafe readonly void* textinfo_data;

		[NonSerialized]
		private Calendar[] optional_calendars;

		[NonSerialized]
		private CultureInfo parent_culture;

		private int m_dataItem;

		private Calendar calendar;

		[NonSerialized]
		private bool constructed;

		[NonSerialized]
		internal byte[] cached_serialized_form;

		private static readonly string MSG_READONLY;

		private static Hashtable shared_by_number;

		private static Hashtable shared_by_name;

		/// <summary>Gets the <see cref="T:System.Globalization.CultureInfo" /> that is culture-independent (invariant).</summary>
		/// <returns>The <see cref="T:System.Globalization.CultureInfo" /> that is culture-independent (invariant).</returns>
		public static CultureInfo InvariantCulture => invariant_culture_info;

		/// <summary>Gets the <see cref="T:System.Globalization.CultureInfo" /> that represents the culture used by the current thread.</summary>
		/// <returns>The <see cref="T:System.Globalization.CultureInfo" /> that represents the culture used by the current thread.</returns>
		public static CultureInfo CurrentCulture => Thread.CurrentThread.CurrentCulture;

		/// <summary>Gets the <see cref="T:System.Globalization.CultureInfo" /> that represents the current culture used by the Resource Manager to look up culture-specific resources at run time.</summary>
		/// <returns>The <see cref="T:System.Globalization.CultureInfo" /> that represents the current culture used by the Resource Manager to look up culture-specific resources at run time.</returns>
		/// <PermissionSet>
		///   <IPermission class="System.Security.Permissions.SecurityPermission, mscorlib, Version=2.0.3600.0, Culture=neutral, PublicKeyToken=b77a5c561934e089" version="1" Flags="UnmanagedCode" />
		/// </PermissionSet>
		public static CultureInfo CurrentUICulture => Thread.CurrentThread.CurrentUICulture;

		internal string Territory => territory;

		/// <summary>Gets the culture identifier for the current <see cref="T:System.Globalization.CultureInfo" />.</summary>
		/// <returns>The culture identifier for the current <see cref="T:System.Globalization.CultureInfo" />.</returns>
		public virtual int LCID => cultureID;

		/// <summary>Gets the culture name in the format "&lt;languagecode2&gt;-&lt;country/regioncode2&gt;".</summary>
		/// <returns>The culture name in the format "&lt;languagecode2&gt;-&lt;country/regioncode2&gt;", where &lt;languagecode2&gt; is a lowercase two-letter code derived from ISO 639-1 and &lt;country/regioncode2&gt; is an uppercase two-letter code derived from ISO 3166.</returns>
		public virtual string Name => m_name;

		/// <summary>Gets the culture name, consisting of the language, the country/region, and the optional script, that the culture is set to display.</summary>
		/// <returns>The culture name. consisting of the full name of the language, the full name of the country/region, and the optional script. The format is discussed in the description of the <see cref="T:System.Globalization.CultureInfo" /> class.</returns>
		public virtual string NativeName
		{
			get
			{
				if (!constructed)
				{
					Construct();
				}
				return nativename;
			}
		}

		/// <summary>Gets the default calendar used by the culture.</summary>
		/// <returns>A <see cref="T:System.Globalization.Calendar" /> that represents the default calendar used by the culture.</returns>
		public virtual Calendar Calendar => DateTimeFormat.Calendar;

		/// <summary>Gets the list of calendars that can be used by the culture.</summary>
		/// <returns>An array of type <see cref="T:System.Globalization.Calendar" /> that represents the calendars that can be used by the culture represented by the current <see cref="T:System.Globalization.CultureInfo" />.</returns>
		public virtual Calendar[] OptionalCalendars
		{
			get
			{
				if (optional_calendars == null)
				{
					lock (this)
					{
						if (optional_calendars == null)
						{
							ConstructCalendars();
						}
					}
				}
				return optional_calendars;
			}
		}

		/// <summary>Gets the <see cref="T:System.Globalization.CultureInfo" /> that represents the parent culture of the current <see cref="T:System.Globalization.CultureInfo" />.</summary>
		/// <returns>The <see cref="T:System.Globalization.CultureInfo" /> that represents the parent culture of the current <see cref="T:System.Globalization.CultureInfo" />.</returns>
		/// <PermissionSet>
		///   <IPermission class="System.Security.Permissions.SecurityPermission, mscorlib, Version=2.0.3600.0, Culture=neutral, PublicKeyToken=b77a5c561934e089" version="1" Flags="UnmanagedCode" />
		/// </PermissionSet>
		public virtual CultureInfo Parent
		{
			get
			{
				if (parent_culture == null)
				{
					if (!constructed)
					{
						Construct();
					}
					if (parent_lcid == cultureID)
					{
						return null;
					}
					if (parent_lcid == 127)
					{
						parent_culture = InvariantCulture;
					}
					else if (cultureID == 127)
					{
						parent_culture = this;
					}
					else
					{
						parent_culture = new CultureInfo(parent_lcid);
					}
				}
				return parent_culture;
			}
		}

		/// <summary>Gets the <see cref="T:System.Globalization.TextInfo" /> that defines the writing system associated with the culture.</summary>
		/// <returns>The <see cref="T:System.Globalization.TextInfo" /> that defines the writing system associated with the culture.</returns>
		/// <PermissionSet>
		///   <IPermission class="System.Security.Permissions.SecurityPermission, mscorlib, Version=2.0.3600.0, Culture=neutral, PublicKeyToken=b77a5c561934e089" version="1" Flags="UnmanagedCode" />
		/// </PermissionSet>
		public virtual TextInfo TextInfo
		{
			get
			{
				if (textInfo == null)
				{
					if (!constructed)
					{
						Construct();
					}
					lock (this)
					{
						if (textInfo == null)
						{
							textInfo = CreateTextInfo(m_isReadOnly);
						}
					}
				}
				return textInfo;
			}
		}

		/// <summary>Gets the ISO 639-2 three-letter code for the language of the current <see cref="T:System.Globalization.CultureInfo" />.</summary>
		/// <returns>The ISO 639-2 three-letter code for the language of the current <see cref="T:System.Globalization.CultureInfo" />.</returns>
		public virtual string ThreeLetterISOLanguageName
		{
			get
			{
				if (!constructed)
				{
					Construct();
				}
				return iso3lang;
			}
		}

		/// <summary>Gets the three-letter code for the language as defined in the Windows API.</summary>
		/// <returns>The three-letter code for the language as defined in the Windows API.</returns>
		public virtual string ThreeLetterWindowsLanguageName
		{
			get
			{
				if (!constructed)
				{
					Construct();
				}
				return win3lang;
			}
		}

		/// <summary>Gets the ISO 639-1 two-letter code for the language of the current <see cref="T:System.Globalization.CultureInfo" />.</summary>
		/// <returns>The ISO 639-1 two-letter code for the language of the current <see cref="T:System.Globalization.CultureInfo" />.</returns>
		public virtual string TwoLetterISOLanguageName
		{
			get
			{
				if (!constructed)
				{
					Construct();
				}
				return iso2lang;
			}
		}

		/// <summary>Gets a value indicating whether the current <see cref="T:System.Globalization.CultureInfo" /> uses the user-selected culture settings.</summary>
		/// <returns>true if the current <see cref="T:System.Globalization.CultureInfo" /> uses the user-selected culture settings; otherwise, false.</returns>
		public bool UseUserOverride => m_useUserOverride;

		internal string IcuName
		{
			get
			{
				if (!constructed)
				{
					Construct();
				}
				return icu_name;
			}
		}

		/// <summary>Gets the <see cref="T:System.Globalization.CompareInfo" /> that defines how to compare strings for the culture.</summary>
		/// <returns>The <see cref="T:System.Globalization.CompareInfo" /> that defines how to compare strings for the culture.</returns>
		/// <PermissionSet>
		///   <IPermission class="System.Security.Permissions.SecurityPermission, mscorlib, Version=2.0.3600.0, Culture=neutral, PublicKeyToken=b77a5c561934e089" version="1" Flags="UnmanagedCode" />
		/// </PermissionSet>
		public virtual CompareInfo CompareInfo
		{
			get
			{
				if (compareInfo == null)
				{
					if (!constructed)
					{
						Construct();
					}
					lock (this)
					{
						if (compareInfo == null)
						{
							compareInfo = new CompareInfo(this);
						}
					}
				}
				return compareInfo;
			}
		}

		/// <summary>Gets a value indicating whether the current <see cref="T:System.Globalization.CultureInfo" /> represents a neutral culture.</summary>
		/// <returns>true if the current <see cref="T:System.Globalization.CultureInfo" /> represents a neutral culture; otherwise, false.</returns>
		public virtual bool IsNeutralCulture
		{
			get
			{
				if (!constructed)
				{
					Construct();
				}
				if (cultureID == 127)
				{
					return false;
				}
				return (cultureID & 0xFF00) == 0 || specific_lcid == 0;
			}
		}

		/// <summary>Gets or sets a <see cref="T:System.Globalization.NumberFormatInfo" /> that defines the culturally appropriate format of displaying numbers, currency, and percentage.</summary>
		/// <returns>A <see cref="T:System.Globalization.NumberFormatInfo" /> that defines the culturally appropriate format of displaying numbers, currency, and percentage.</returns>
		/// <exception cref="T:System.ArgumentNullException">The property is set to null. </exception>
		/// <exception cref="T:System.NotSupportedException">The <see cref="T:System.Globalization.CultureInfo" /> is for a neutral culture. </exception>
		/// <exception cref="T:System.InvalidOperationException">The <see cref="P:System.Globalization.CultureInfo.NumberFormat" /> property or any of the <see cref="T:System.Globalization.NumberFormatInfo" /> properties is set, and the <see cref="T:System.Globalization.CultureInfo" /> is read-only. </exception>
		public virtual NumberFormatInfo NumberFormat
		{
			get
			{
				if (!constructed)
				{
					Construct();
				}
				CheckNeutral();
				if (numInfo == null)
				{
					lock (this)
					{
						if (numInfo == null)
						{
							numInfo = new NumberFormatInfo(m_isReadOnly);
							construct_number_format();
						}
					}
				}
				return numInfo;
			}
			set
			{
				if (!constructed)
				{
					Construct();
				}
				if (m_isReadOnly)
				{
					throw new InvalidOperationException(MSG_READONLY);
				}
				if (value == null)
				{
					throw new ArgumentNullException("NumberFormat");
				}
				numInfo = value;
			}
		}

		/// <summary>Gets or sets a <see cref="T:System.Globalization.DateTimeFormatInfo" /> that defines the culturally appropriate format of displaying dates and times.</summary>
		/// <returns>A <see cref="T:System.Globalization.DateTimeFormatInfo" /> that defines the culturally appropriate format of displaying dates and times.</returns>
		/// <exception cref="T:System.ArgumentNullException">The property is set to null. </exception>
		/// <exception cref="T:System.NotSupportedException">The <see cref="T:System.Globalization.CultureInfo" /> is for a neutral culture. </exception>
		/// <exception cref="T:System.InvalidOperationException">The <see cref="P:System.Globalization.CultureInfo.DateTimeFormat" /> property or any of the <see cref="T:System.Globalization.DateTimeFormatInfo" /> properties is set, and the <see cref="T:System.Globalization.CultureInfo" /> is read-only. </exception>
		public virtual DateTimeFormatInfo DateTimeFormat
		{
			get
			{
				if (!constructed)
				{
					Construct();
				}
				CheckNeutral();
				if (dateTimeInfo == null)
				{
					lock (this)
					{
						if (dateTimeInfo == null)
						{
							dateTimeInfo = new DateTimeFormatInfo(m_isReadOnly);
							construct_datetime_format();
							if (optional_calendars != null)
							{
								dateTimeInfo.Calendar = optional_calendars[0];
							}
						}
					}
				}
				return dateTimeInfo;
			}
			set
			{
				if (!constructed)
				{
					Construct();
				}
				if (m_isReadOnly)
				{
					throw new InvalidOperationException(MSG_READONLY);
				}
				if (value == null)
				{
					throw new ArgumentNullException("DateTimeFormat");
				}
				dateTimeInfo = value;
			}
		}

		/// <summary>Gets the culture name in the format "&lt;languagefull&gt; (&lt;country/regionfull&gt;)" in the language of the localized version of .NET Framework.</summary>
		/// <returns>The culture name in the format "&lt;languagefull&gt; (&lt;country/regionfull&gt;)" in the language of the localized version of .NET Framework, where &lt;languagefull&gt; is the full name of the language and &lt;country/regionfull&gt; is the full name of the country/region.</returns>
		public virtual string DisplayName
		{
			get
			{
				if (!constructed)
				{
					Construct();
				}
				return displayname;
			}
		}

		/// <summary>Gets the culture name in the format "&lt;languagefull&gt; (&lt;country/regionfull&gt;)" in English.</summary>
		/// <returns>The culture name in the format "&lt;languagefull&gt; (&lt;country/regionfull&gt;)" in English, where &lt;languagefull&gt; is the full name of the language and &lt;country/regionfull&gt; is the full name of the country/region.</returns>
		public virtual string EnglishName
		{
			get
			{
				if (!constructed)
				{
					Construct();
				}
				return englishname;
			}
		}

		/// <summary>Gets the <see cref="T:System.Globalization.CultureInfo" /> that represents the culture installed with the operating system.</summary>
		/// <returns>The <see cref="T:System.Globalization.CultureInfo" /> that represents the culture installed with the operating system.</returns>
		/// <PermissionSet>
		///   <IPermission class="System.Security.Permissions.SecurityPermission, mscorlib, Version=2.0.3600.0, Culture=neutral, PublicKeyToken=b77a5c561934e089" version="1" Flags="UnmanagedCode" />
		/// </PermissionSet>
		public static CultureInfo InstalledUICulture => GetCultureInfo(BootstrapCultureID);

		/// <summary>Gets a value indicating whether the current <see cref="T:System.Globalization.CultureInfo" /> is read-only.</summary>
		/// <returns>true if the current <see cref="T:System.Globalization.CultureInfo" /> is read-only; otherwise, false. The default is false.</returns>
		public bool IsReadOnly => m_isReadOnly;

		/// <summary>Initializes a new instance of the <see cref="T:System.Globalization.CultureInfo" /> class based on the culture specified by the culture identifier.</summary>
		/// <param name="culture">A predefined <see cref="T:System.Globalization.CultureInfo" /> identifier, <see cref="P:System.Globalization.CultureInfo.LCID" /> property of an existing <see cref="T:System.Globalization.CultureInfo" /> object, or Windows-only culture identifier. </param>
		/// <exception cref="T:System.ArgumentOutOfRangeException">
		///   <paramref name="culture" /> is less than zero. </exception>
		/// <exception cref="T:System.ArgumentException">
		///   <paramref name="culture" /> is not a valid culture identifier. -or-In .NET Compact Framework applications, <paramref name="culture" /> is not supported by the operating system of the device. </exception>
		public CultureInfo(int culture)
			: this(culture, useUserOverride: true)
		{
		}

		/// <summary>Initializes a new instance of the <see cref="T:System.Globalization.CultureInfo" /> class based on the culture specified by the culture identifier and on the Boolean that specifies whether to use the user-selected culture settings from the system.</summary>
		/// <param name="culture">A predefined <see cref="T:System.Globalization.CultureInfo" /> identifier, <see cref="P:System.Globalization.CultureInfo.LCID" /> property of an existing <see cref="T:System.Globalization.CultureInfo" /> object, or Windows-only culture identifier. </param>
		/// <param name="useUserOverride">A Boolean that denotes whether to use the user-selected culture settings (true) or the default culture settings (false). </param>
		/// <exception cref="T:System.ArgumentOutOfRangeException">
		///   <paramref name="culture" /> is less than zero. </exception>
		/// <exception cref="T:System.ArgumentException">
		///   <paramref name="culture" /> is not a valid culture identifier.-or-In .NET Compact Framework applications, <paramref name="culture" /> is not supported by the operating system of the device.  </exception>
		public CultureInfo(int culture, bool useUserOverride)
			: this(culture, useUserOverride, read_only: false)
		{
		}

		private CultureInfo(int culture, bool useUserOverride, bool read_only)
		{
			if (culture < 0)
			{
				throw new ArgumentOutOfRangeException("culture", "Positive number required.");
			}
			constructed = true;
			m_isReadOnly = read_only;
			m_useUserOverride = useUserOverride;
			if (culture == 127)
			{
				ConstructInvariant(read_only);
			}
			else if (!ConstructInternalLocaleFromLcid(culture))
			{
				throw new ArgumentException(string.Format("Culture ID {0} (0x{0:X4}) is not a supported culture.", culture), "culture");
			}
		}

		/// <summary>Initializes a new instance of the <see cref="T:System.Globalization.CultureInfo" /> class based on the culture specified by name.</summary>
		/// <param name="name">A predefined <see cref="T:System.Globalization.CultureInfo" /> name, <see cref="P:System.Globalization.CultureInfo.Name" /> of an existing <see cref="T:System.Globalization.CultureInfo" />, or Windows-only culture name. </param>
		/// <exception cref="T:System.ArgumentNullException">
		///   <paramref name="name" /> is null. </exception>
		/// <exception cref="T:System.ArgumentException">
		///   <paramref name="name" /> is not a valid culture name. -or-In .NET Compact Framework applications, <paramref name="culture" /> is not supported by the operating system of the device.</exception>
		public CultureInfo(string name)
			: this(name, useUserOverride: true)
		{
		}

		/// <summary>Initializes a new instance of the <see cref="T:System.Globalization.CultureInfo" /> class based on the culture specified by name and on the Boolean that specifies whether to use the user-selected culture settings from the system.</summary>
		/// <param name="name">A predefined <see cref="T:System.Globalization.CultureInfo" /> name, <see cref="P:System.Globalization.CultureInfo.Name" /> of an existing <see cref="T:System.Globalization.CultureInfo" />, or Windows-only culture name. </param>
		/// <param name="useUserOverride">A Boolean that denotes whether to use the user-selected culture settings (true) or the default culture settings (false). </param>
		/// <exception cref="T:System.ArgumentNullException">
		///   <paramref name="name" /> is null. </exception>
		/// <exception cref="T:System.ArgumentException">
		///   <paramref name="name" /> is not a valid culture name. -or-In .NET Compact Framework applications, <paramref name="culture" /> is not supported by the operating system of the device.</exception>
		public CultureInfo(string name, bool useUserOverride)
			: this(name, useUserOverride, read_only: false)
		{
		}

		private CultureInfo(string name, bool useUserOverride, bool read_only)
		{
			if (name == null)
			{
				throw new ArgumentNullException("name");
			}
			constructed = true;
			m_isReadOnly = read_only;
			m_useUserOverride = useUserOverride;
			if (name.Length == 0)
			{
				ConstructInvariant(read_only);
			}
			else if (!ConstructInternalLocaleFromName(name.ToLowerInvariant()))
			{
				throw new ArgumentException("Culture name " + name + " is not supported.", "name");
			}
		}

		private CultureInfo()
		{
			constructed = true;
		}

		static CultureInfo()
		{
			shared_table_lock = new object();
			MSG_READONLY = "This instance is read only";
			invariant_culture_info = new CultureInfo(127, useUserOverride: false, read_only: true);
		}

		/// <summary>Creates a <see cref="T:System.Globalization.CultureInfo" /> that represents the specific culture that is associated with the specified name.</summary>
		/// <returns>A <see cref="T:System.Globalization.CultureInfo" /> that represents:The invariant culture, if <paramref name="name" /> is an empty string ("").-or- The specific culture associated with <paramref name="name" />, if <paramref name="name" /> is a neutral culture.-or- The culture specified by <paramref name="name" />, if <paramref name="name" /> is already a specific culture.</returns>
		/// <param name="name">A predefined <see cref="T:System.Globalization.CultureInfo" /> name or the name of an existing <see cref="T:System.Globalization.CultureInfo" />. </param>
		/// <exception cref="T:System.ArgumentException">
		///   <paramref name="name" /> is not a valid culture name.-or- The culture specified by <paramref name="name" /> does not have a specific culture associated with it. For example: "zh-Hans" or "zh-Hant". </exception>
		/// <exception cref="T:System.NullReferenceException">
		///   <paramref name="name" /> is null. </exception>
		public static CultureInfo CreateSpecificCulture(string name)
		{
			if (name == null)
			{
				throw new ArgumentNullException("name");
			}
			if (name == string.Empty)
			{
				return InvariantCulture;
			}
			CultureInfo cultureInfo = new CultureInfo();
			if (!ConstructInternalLocaleFromSpecificName(cultureInfo, name.ToLowerInvariant()))
			{
				throw new ArgumentException("Culture name " + name + " is not supported.", name);
			}
			return cultureInfo;
		}

		internal static CultureInfo ConstructCurrentCulture()
		{
			CultureInfo cultureInfo = new CultureInfo();
			if (!ConstructInternalLocaleFromCurrentLocale(cultureInfo))
			{
				cultureInfo = InvariantCulture;
			}
			BootstrapCultureID = cultureInfo.cultureID;
			return cultureInfo;
		}

		internal static CultureInfo ConstructCurrentUICulture()
		{
			return ConstructCurrentCulture();
		}

		/// <summary>Refreshes cached culture-related information.</summary>
		/// <PermissionSet>
		///   <IPermission class="System.Security.Permissions.SecurityPermission, mscorlib, Version=2.0.3600.0, Culture=neutral, PublicKeyToken=b77a5c561934e089" version="1" Flags="UnmanagedCode" />
		/// </PermissionSet>
		public void ClearCachedData()
		{
			Thread.CurrentThread.CurrentCulture = null;
			Thread.CurrentThread.CurrentUICulture = null;
		}

		/// <summary>Creates a copy of the current <see cref="T:System.Globalization.CultureInfo" />.</summary>
		/// <returns>A copy of the current <see cref="T:System.Globalization.CultureInfo" />.</returns>
		public virtual object Clone()
		{
			if (!constructed)
			{
				Construct();
			}
			CultureInfo cultureInfo = (CultureInfo)MemberwiseClone();
			cultureInfo.m_isReadOnly = false;
			cultureInfo.cached_serialized_form = null;
			if (!IsNeutralCulture)
			{
				cultureInfo.NumberFormat = (NumberFormatInfo)NumberFormat.Clone();
				cultureInfo.DateTimeFormat = (DateTimeFormatInfo)DateTimeFormat.Clone();
			}
			return cultureInfo;
		}

		/// <summary>Determines whether the specified object is the same culture as the current <see cref="T:System.Globalization.CultureInfo" />.</summary>
		/// <returns>true if <paramref name="value" /> is the same culture as the current <see cref="T:System.Globalization.CultureInfo" />; otherwise, false.</returns>
		/// <param name="value">The object to compare with the current <see cref="T:System.Globalization.CultureInfo" />. </param>
		public override bool Equals(object value)
		{
			CultureInfo cultureInfo = value as CultureInfo;
			if (cultureInfo != null)
			{
				return cultureInfo.cultureID == cultureID;
			}
			return false;
		}

		/// <summary>Gets the list of supported cultures filtered by the specified <see cref="T:System.Globalization.CultureTypes" /> parameter.</summary>
		/// <returns>An array of type <see cref="T:System.Globalization.CultureInfo" /> that contains the cultures specified by the <paramref name="types" /> parameter. The array of cultures is unsorted.</returns>
		/// <param name="types">A bitwise combination of <see cref="T:System.Globalization.CultureTypes" /> values that filter the cultures to retrieve. </param>
		/// <exception cref="T:System.ArgumentOutOfRangeException">
		///   <paramref name="types" /> specifies an invalid combination of <see cref="T:System.Globalization.CultureTypes" /> values.</exception>
		/// <PermissionSet>
		///   <IPermission class="System.Security.Permissions.SecurityPermission, mscorlib, Version=2.0.3600.0, Culture=neutral, PublicKeyToken=b77a5c561934e089" version="1" Flags="UnmanagedCode" />
		/// </PermissionSet>
		public static CultureInfo[] GetCultures(CultureTypes types)
		{
			bool flag = (types & CultureTypes.NeutralCultures) != 0;
			bool specific = (types & CultureTypes.SpecificCultures) != 0;
			bool installed = (types & CultureTypes.InstalledWin32Cultures) != 0;
			CultureInfo[] array = internal_get_cultures(flag, specific, installed);
			if (flag && array.Length > 0 && array[0] == null)
			{
				array[0] = (CultureInfo)InvariantCulture.Clone();
			}
			return array;
		}

		/// <summary>Serves as a hash function for the current <see cref="T:System.Globalization.CultureInfo" />, suitable for hashing algorithms and data structures, such as a hash table.</summary>
		/// <returns>A hash code for the current <see cref="T:System.Globalization.CultureInfo" />.</returns>
		public override int GetHashCode()
		{
			return cultureID;
		}

		/// <summary>Returns a read-only wrapper around the specified <see cref="T:System.Globalization.CultureInfo" />.</summary>
		/// <returns>A read-only <see cref="T:System.Globalization.CultureInfo" /> wrapper around <paramref name="ci" />.</returns>
		/// <param name="ci">The <see cref="T:System.Globalization.CultureInfo" /> to wrap. </param>
		/// <exception cref="T:System.ArgumentNullException">
		///   <paramref name="ci" /> is null. </exception>
		public static CultureInfo ReadOnly(CultureInfo ci)
		{
			if (ci == null)
			{
				throw new ArgumentNullException("ci");
			}
			if (ci.m_isReadOnly)
			{
				return ci;
			}
			CultureInfo cultureInfo = (CultureInfo)ci.Clone();
			cultureInfo.m_isReadOnly = true;
			if (cultureInfo.numInfo != null)
			{
				cultureInfo.numInfo = NumberFormatInfo.ReadOnly(cultureInfo.numInfo);
			}
			if (cultureInfo.dateTimeInfo != null)
			{
				cultureInfo.dateTimeInfo = DateTimeFormatInfo.ReadOnly(cultureInfo.dateTimeInfo);
			}
			if (cultureInfo.textInfo != null)
			{
				cultureInfo.textInfo = TextInfo.ReadOnly(cultureInfo.textInfo);
			}
			return cultureInfo;
		}

		/// <summary>Returns a string containing the name of the current <see cref="T:System.Globalization.CultureInfo" /> in the format "&lt;languagecode2&gt;-&lt;country/regioncode2&gt;".</summary>
		/// <returns>A string containing the name of the current <see cref="T:System.Globalization.CultureInfo" />.</returns>
		public override string ToString()
		{
			return m_name;
		}

		internal static bool IsIDNeutralCulture(int lcid)
		{
			if (!internal_is_lcid_neutral(lcid, out bool is_neutral))
			{
				throw new ArgumentException(string.Format("Culture id 0x{:x4} is not supported.", lcid));
			}
			return is_neutral;
		}

		internal void CheckNeutral()
		{
			if (IsNeutralCulture)
			{
				throw new NotSupportedException("Culture \"" + m_name + "\" is a neutral culture. It can not be used in formatting and parsing and therefore cannot be set as the thread's current culture.");
			}
		}

		/// <summary>Gets an object that defines how to format the specified type.</summary>
		/// <returns>The value of the <see cref="P:System.Globalization.CultureInfo.NumberFormat" /> property, which is a <see cref="T:System.Globalization.NumberFormatInfo" /> containing the default number format information for the current <see cref="T:System.Globalization.CultureInfo" />, if <paramref name="formatType" /> is the <see cref="T:System.Type" /> object for the <see cref="T:System.Globalization.NumberFormatInfo" /> class.-or- The value of the <see cref="P:System.Globalization.CultureInfo.DateTimeFormat" /> property, which is a <see cref="T:System.Globalization.DateTimeFormatInfo" /> containing the default date and time format information for the current <see cref="T:System.Globalization.CultureInfo" />, if <paramref name="formatType" /> is the <see cref="T:System.Type" /> object for the <see cref="T:System.Globalization.DateTimeFormatInfo" /> class.-or- null, if <paramref name="formatType" /> is any other object.</returns>
		/// <param name="formatType">The <see cref="T:System.Type" /> for which to get a formatting object. This method only supports the <see cref="T:System.Globalization.NumberFormatInfo" /> and <see cref="T:System.Globalization.DateTimeFormatInfo" /> types. </param>
		public virtual object GetFormat(Type formatType)
		{
			object result = null;
			if (formatType == typeof(NumberFormatInfo))
			{
				result = NumberFormat;
			}
			else if (formatType == typeof(DateTimeFormatInfo))
			{
				result = DateTimeFormat;
			}
			return result;
		}

		private void Construct()
		{
			construct_internal_locale_from_lcid(cultureID);
			constructed = true;
		}

		private bool ConstructInternalLocaleFromName(string locale)
		{
			switch (locale)
			{
			case "zh-hans":
				locale = "zh-chs";
				break;
			case "zh-hant":
				locale = "zh-cht";
				break;
			}
			if (!construct_internal_locale_from_name(locale))
			{
				return false;
			}
			return true;
		}

		private bool ConstructInternalLocaleFromLcid(int lcid)
		{
			if (!construct_internal_locale_from_lcid(lcid))
			{
				return false;
			}
			return true;
		}

		private static bool ConstructInternalLocaleFromSpecificName(CultureInfo ci, string name)
		{
			if (!construct_internal_locale_from_specific_name(ci, name))
			{
				return false;
			}
			return true;
		}

		private static bool ConstructInternalLocaleFromCurrentLocale(CultureInfo ci)
		{
			if (!construct_internal_locale_from_current_locale(ci))
			{
				return false;
			}
			return true;
		}

		[MethodImpl(MethodImplOptions.InternalCall)]
		private extern bool construct_internal_locale_from_lcid(int lcid);

		[MethodImpl(MethodImplOptions.InternalCall)]
		private extern bool construct_internal_locale_from_name(string name);

		[MethodImpl(MethodImplOptions.InternalCall)]
		private static extern bool construct_internal_locale_from_specific_name(CultureInfo ci, string name);

		[MethodImpl(MethodImplOptions.InternalCall)]
		private static extern bool construct_internal_locale_from_current_locale(CultureInfo ci);

		[MethodImpl(MethodImplOptions.InternalCall)]
		private static extern CultureInfo[] internal_get_cultures(bool neutral, bool specific, bool installed);

		[MethodImpl(MethodImplOptions.InternalCall)]
		private extern void construct_datetime_format();

		[MethodImpl(MethodImplOptions.InternalCall)]
		private extern void construct_number_format();

		[MethodImpl(MethodImplOptions.InternalCall)]
		private static extern bool internal_is_lcid_neutral(int lcid, out bool is_neutral);

		private void ConstructInvariant(bool read_only)
		{
			cultureID = 127;
			numInfo = NumberFormatInfo.InvariantInfo;
			dateTimeInfo = DateTimeFormatInfo.InvariantInfo;
			if (!read_only)
			{
				numInfo = (NumberFormatInfo)numInfo.Clone();
				dateTimeInfo = (DateTimeFormatInfo)dateTimeInfo.Clone();
			}
			textInfo = CreateTextInfo(read_only);
			m_name = string.Empty;
			displayname = (englishname = (nativename = "Invariant Language (Invariant Country)"));
			iso3lang = "IVL";
			iso2lang = "iv";
			icu_name = "en_US_POSIX";
			win3lang = "IVL";
		}

		private unsafe TextInfo CreateTextInfo(bool readOnly)
		{
			return new TextInfo(this, cultureID, textinfo_data, readOnly);
		}

		private static void insert_into_shared_tables(CultureInfo c)
		{
			if (shared_by_number == null)
			{
				shared_by_number = new Hashtable();
				shared_by_name = new Hashtable();
			}
			shared_by_number[c.cultureID] = c;
			shared_by_name[c.m_name] = c;
		}

		/// <summary>Retrieves a cached, read-only instance of a culture using the specified culture identifier.</summary>
		/// <returns>A read-only <see cref="T:System.Globalization.CultureInfo" /> object.</returns>
		/// <param name="culture">A culture identifier.</param>
		/// <exception cref="T:System.ArgumentOutOfRangeException">
		///   <paramref name="culture" /> is less than zero.</exception>
		/// <exception cref="T:System.ArgumentException">
		///   <paramref name="culture" /> specifies a culture that is not supported.</exception>
		public static CultureInfo GetCultureInfo(int culture)
		{
			//Discarded unreachable code: IL_004f
			lock (shared_table_lock)
			{
				CultureInfo cultureInfo;
				if (shared_by_number != null)
				{
					cultureInfo = (shared_by_number[culture] as CultureInfo);
					if (cultureInfo != null)
					{
						return cultureInfo;
					}
				}
				cultureInfo = new CultureInfo(culture, useUserOverride: false, read_only: true);
				insert_into_shared_tables(cultureInfo);
				return cultureInfo;
			}
		}

		/// <summary>Retrieves a cached, read-only instance of a culture using the specified culture name. </summary>
		/// <returns>A read-only <see cref="T:System.Globalization.CultureInfo" /> object.</returns>
		/// <param name="name">The name of a culture.</param>
		/// <exception cref="T:System.ArgumentNullException">
		///   <paramref name="name" /> is null.</exception>
		/// <exception cref="T:System.ArgumentException">
		///   <paramref name="name" /> specifies a culture that is not supported.</exception>
		public static CultureInfo GetCultureInfo(string name)
		{
			//Discarded unreachable code: IL_005b
			if (name == null)
			{
				throw new ArgumentNullException("name");
			}
			lock (shared_table_lock)
			{
				CultureInfo cultureInfo;
				if (shared_by_name != null)
				{
					cultureInfo = (shared_by_name[name] as CultureInfo);
					if (cultureInfo != null)
					{
						return cultureInfo;
					}
				}
				cultureInfo = new CultureInfo(name, useUserOverride: false, read_only: true);
				insert_into_shared_tables(cultureInfo);
				return cultureInfo;
			}
		}

		/// <summary>Retrieves a cached, read-only instance of a culture. Parameters specify a culture that is initialized with the <see cref="T:System.Globalization.TextInfo" /> and <see cref="T:System.Globalization.CompareInfo" /> objects specified by another culture.</summary>
		/// <returns>A read-only <see cref="T:System.Globalization.CultureInfo" /> object.</returns>
		/// <param name="name">The name of a culture.</param>
		/// <param name="altName">The name of a culture that supplies the <see cref="T:System.Globalization.TextInfo" /> and <see cref="T:System.Globalization.CompareInfo" /> objects used to initialize <paramref name="name" />.</param>
		/// <exception cref="T:System.ArgumentNullException">
		///   <paramref name="name" /> or <paramref name="altName" /> is null.</exception>
		/// <exception cref="T:System.ArgumentException">
		///   <paramref name="name" /> or <paramref name="altName" /> specifies a culture that is not supported.</exception>
		[MonoTODO("Currently it ignores the altName parameter")]
		public static CultureInfo GetCultureInfo(string name, string altName)
		{
			if (name == null)
			{
				throw new ArgumentNullException("null");
			}
			if (altName == null)
			{
				throw new ArgumentNullException("null");
			}
			return GetCultureInfo(name);
		}

		/// <summary>Deprecated. Retrieves a read-only <see cref="T:System.Globalization.CultureInfo" /> object having linguistic characteristics that are identified by the specified RFC 4646 language tag. </summary>
		/// <returns>A read-only <see cref="T:System.Globalization.CultureInfo" /> object.</returns>
		/// <param name="name">The name of a language as specified by the RFC 4646 standard.</param>
		/// <exception cref="T:System.ArgumentNullException">
		///   <paramref name="name" /> is null.</exception>
		/// <exception cref="T:System.ArgumentException">
		///   <paramref name="name" /> does not correspond to a supported culture.</exception>
		public static CultureInfo GetCultureInfoByIetfLanguageTag(string name)
		{
			switch (name)
			{
			case "zh-Hans":
				return GetCultureInfo("zh-CHS");
			case "zh-Hant":
				return GetCultureInfo("zh-CHT");
			default:
				return GetCultureInfo(name);
			}
		}

		internal static CultureInfo CreateCulture(string name, bool reference)
		{
			bool flag = name.Length == 0;
			bool useUserOverride;
			bool read_only;
			if (reference)
			{
				useUserOverride = ((!flag) ? true : false);
				read_only = false;
			}
			else
			{
				read_only = false;
				useUserOverride = ((!flag) ? true : false);
			}
			return new CultureInfo(name, useUserOverride, read_only);
		}

		internal unsafe void ConstructCalendars()
		{
			if (calendar_data == null)
			{
				optional_calendars = new Calendar[1]
				{
					new GregorianCalendar(GregorianCalendarTypes.Localized)
				};
				return;
			}
			optional_calendars = new Calendar[5];
			for (int i = 0; i < 5; i++)
			{
				Calendar calendar = null;
				int num = *(int*)((byte*)calendar_data + i * 4);
				switch (num >> 24)
				{
				case 0:
				{
					GregorianCalendarTypes type = (GregorianCalendarTypes)(num & 0xFFFFFF);
					calendar = new GregorianCalendar(type);
					break;
				}
				case 1:
					calendar = new HijriCalendar();
					break;
				case 2:
					calendar = new ThaiBuddhistCalendar();
					break;
				default:
					throw new Exception("invalid calendar type:  " + num);
				}
				optional_calendars[i] = calendar;
			}
		}
	}
}
