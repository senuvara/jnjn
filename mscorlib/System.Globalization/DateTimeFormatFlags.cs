namespace System.Globalization
{
	[Flags]
	internal enum DateTimeFormatFlags
	{
		Unused = 0x0,
		But = 0x1,
		Serialized = 0x2,
		By = 0x3,
		Microsoft = 0x4
	}
}
