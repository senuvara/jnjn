using System.Runtime.InteropServices;

namespace System.Globalization
{
	/// <summary>Determines the styles permitted in numeric string arguments that are passed to the Parse methods of the numeric base type classes.</summary>
	[Serializable]
	[ComVisible(true)]
	[Flags]
	public enum NumberStyles
	{
		/// <summary>Indicates that none of the bit styles are allowed.</summary>
		None = 0x0,
		/// <summary>Indicates that a leading white-space character must be ignored during parsing. Valid white-space characters have the Unicode values U+0009, U+000A, U+000B, U+000C, U+000D, and U+0020.</summary>
		AllowLeadingWhite = 0x1,
		/// <summary>Indicates that trailing white-space character must be ignored during parsing. Valid white-space characters have the Unicode values U+0009, U+000A, U+000B, U+000C, U+000D, and U+0020.</summary>
		AllowTrailingWhite = 0x2,
		/// <summary>Indicates that the numeric string can have a leading sign. Valid leading sign characters are determined by the <see cref="P:System.Globalization.NumberFormatInfo.PositiveSign" /> and <see cref="P:System.Globalization.NumberFormatInfo.NegativeSign" /> properties of <see cref="T:System.Globalization.NumberFormatInfo" />.</summary>
		AllowLeadingSign = 0x4,
		/// <summary>Indicates that the numeric string can have a trailing sign. Valid trailing sign characters are determined by the <see cref="P:System.Globalization.NumberFormatInfo.PositiveSign" /> and <see cref="P:System.Globalization.NumberFormatInfo.NegativeSign" /> properties of <see cref="T:System.Globalization.NumberFormatInfo" />.</summary>
		AllowTrailingSign = 0x8,
		/// <summary>Indicates that the numeric string can have one pair of parentheses enclosing the number.</summary>
		AllowParentheses = 0x10,
		/// <summary>Indicates that the numeric string can have a decimal point. Valid decimal point characters are determined by the <see cref="P:System.Globalization.NumberFormatInfo.NumberDecimalSeparator" /> and <see cref="P:System.Globalization.NumberFormatInfo.CurrencyDecimalSeparator" /> properties of <see cref="T:System.Globalization.NumberFormatInfo" />.</summary>
		AllowDecimalPoint = 0x20,
		/// <summary>Indicates that the numeric string can have group separators, for example, separating the hundreds from the thousands. Valid group separator characters are determined by the <see cref="P:System.Globalization.NumberFormatInfo.NumberGroupSeparator" /> and <see cref="P:System.Globalization.NumberFormatInfo.CurrencyGroupSeparator" /> properties of <see cref="T:System.Globalization.NumberFormatInfo" /> and the number of digits in each group is determined by the <see cref="P:System.Globalization.NumberFormatInfo.NumberGroupSizes" /> and <see cref="P:System.Globalization.NumberFormatInfo.CurrencyGroupSizes" /> properties of <see cref="T:System.Globalization.NumberFormatInfo" />.</summary>
		AllowThousands = 0x40,
		/// <summary>Indicates that the numeric string can be in exponential notation.</summary>
		AllowExponent = 0x80,
		/// <summary>Indicates that the numeric string is parsed as currency if it contains a currency symbol. Otherwise, it is parsed as a number. Valid currency symbols are determined by the <see cref="P:System.Globalization.NumberFormatInfo.CurrencySymbol" /> property of <see cref="T:System.Globalization.NumberFormatInfo" />.</summary>
		AllowCurrencySymbol = 0x100,
		/// <summary>Indicates that the numeric string represents a hexadecimal value. Valid hexadecimal values include the numeric digits 0-9 and the hexadecimal digits A-F and a-f. Hexadecimal values can be left-padded with zeros. Strings parsed using this style are not permitted to be prefixed with "0x".</summary>
		AllowHexSpecifier = 0x200,
		/// <summary>Indicates that the AllowLeadingWhite, AllowTrailingWhite, and AllowLeadingSign styles are used. This is a composite number style.</summary>
		Integer = 0x7,
		/// <summary>Indicates that the AllowLeadingWhite, AllowTrailingWhite, and AllowHexSpecifier styles are used. This is a composite number style.</summary>
		HexNumber = 0x203,
		/// <summary>Indicates that the AllowLeadingWhite, AllowTrailingWhite, AllowLeadingSign, AllowTrailingSign, AllowDecimalPoint, and AllowThousands styles are used. This is a composite number style.</summary>
		Number = 0x6F,
		/// <summary>Indicates that the AllowLeadingWhite, AllowTrailingWhite, AllowLeadingSign, AllowDecimalPoint, and AllowExponent styles are used. This is a composite number style.</summary>
		Float = 0xA7,
		/// <summary>Indicates that all styles, except AllowExponent and AllowHexSpecifier, are used. This is a composite number style.</summary>
		Currency = 0x17F,
		/// <summary>Indicates that all styles, except AllowHexSpecifier, are used. This is a composite number style.</summary>
		Any = 0x1FF
	}
}
