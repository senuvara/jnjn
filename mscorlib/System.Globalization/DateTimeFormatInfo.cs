using System.Collections;
using System.Runtime.InteropServices;
using System.Threading;

namespace System.Globalization
{
	/// <summary>Defines how <see cref="T:System.DateTime" /> values are formatted and displayed, depending on the culture.</summary>
	[Serializable]
	[ComVisible(true)]
	public sealed class DateTimeFormatInfo : ICloneable, IFormatProvider
	{
		private const string _RoundtripPattern = "yyyy'-'MM'-'dd'T'HH':'mm':'ss.fffffffK";

		private static readonly string MSG_READONLY = "This instance is read only";

		private static readonly string MSG_ARRAYSIZE_MONTH = "An array with exactly 13 elements is needed";

		private static readonly string MSG_ARRAYSIZE_DAY = "An array with exactly 7 elements is needed";

		private static readonly string[] INVARIANT_ABBREVIATED_DAY_NAMES = new string[7]
		{
			"Sun",
			"Mon",
			"Tue",
			"Wed",
			"Thu",
			"Fri",
			"Sat"
		};

		private static readonly string[] INVARIANT_DAY_NAMES = new string[7]
		{
			"Sunday",
			"Monday",
			"Tuesday",
			"Wednesday",
			"Thursday",
			"Friday",
			"Saturday"
		};

		private static readonly string[] INVARIANT_ABBREVIATED_MONTH_NAMES = new string[13]
		{
			"Jan",
			"Feb",
			"Mar",
			"Apr",
			"May",
			"Jun",
			"Jul",
			"Aug",
			"Sep",
			"Oct",
			"Nov",
			"Dec",
			string.Empty
		};

		private static readonly string[] INVARIANT_MONTH_NAMES = new string[13]
		{
			"January",
			"February",
			"March",
			"April",
			"May",
			"June",
			"July",
			"August",
			"September",
			"October",
			"November",
			"December",
			string.Empty
		};

		private static readonly string[] INVARIANT_SHORT_DAY_NAMES = new string[7]
		{
			"Su",
			"Mo",
			"Tu",
			"We",
			"Th",
			"Fr",
			"Sa"
		};

		private static DateTimeFormatInfo theInvariantDateTimeFormatInfo;

		private bool m_isReadOnly;

		private string amDesignator;

		private string pmDesignator;

		private string dateSeparator;

		private string timeSeparator;

		private string shortDatePattern;

		private string longDatePattern;

		private string shortTimePattern;

		private string longTimePattern;

		private string monthDayPattern;

		private string yearMonthPattern;

		private string fullDateTimePattern;

		private string _RFC1123Pattern;

		private string _SortableDateTimePattern;

		private string _UniversalSortableDateTimePattern;

		private int firstDayOfWeek;

		private Calendar calendar;

		private int calendarWeekRule;

		private string[] abbreviatedDayNames;

		private string[] dayNames;

		private string[] monthNames;

		private string[] abbreviatedMonthNames;

		private string[] allShortDatePatterns;

		private string[] allLongDatePatterns;

		private string[] allShortTimePatterns;

		private string[] allLongTimePatterns;

		private string[] monthDayPatterns;

		private string[] yearMonthPatterns;

		private string[] shortDayNames;

		private int nDataItem;

		private bool m_useUserOverride;

		private bool m_isDefaultCalendar;

		private int CultureID;

		private bool bUseCalendarInfo;

		private string generalShortTimePattern;

		private string generalLongTimePattern;

		private string[] m_eraNames;

		private string[] m_abbrevEraNames;

		private string[] m_abbrevEnglishEraNames;

		private string[] m_dateWords;

		private int[] optionalCalendars;

		private string[] m_superShortDayNames;

		private string[] genitiveMonthNames;

		private string[] m_genitiveAbbreviatedMonthNames;

		private string[] leapYearMonthNames;

		private DateTimeFormatFlags formatFlags;

		private string m_name;

		private volatile string[] all_date_time_patterns;

		/// <summary>Gets a value indicating whether the <see cref="T:System.Globalization.DateTimeFormatInfo" /> object is read-only.</summary>
		/// <returns>true if the <see cref="T:System.Globalization.DateTimeFormatInfo" /> object is read-only; otherwise, false.</returns>
		public bool IsReadOnly => m_isReadOnly;

		/// <summary>Gets or sets a one-dimensional array of type <see cref="T:System.String" /> containing the culture-specific abbreviated names of the days of the week.</summary>
		/// <returns>A one-dimensional array of type <see cref="T:System.String" /> containing the culture-specific abbreviated names of the days of the week. The array for <see cref="P:System.Globalization.DateTimeFormatInfo.InvariantInfo" /> contains "Sun", "Mon", "Tue", "Wed", "Thu", "Fri", and "Sat".</returns>
		/// <exception cref="T:System.ArgumentNullException">The property is being set to null. </exception>
		/// <exception cref="T:System.ArgumentException">The property is being set to an array that is multidimensional or that has a length that is not exactly 7. </exception>
		/// <exception cref="T:System.InvalidOperationException">The property is being set and the <see cref="T:System.Globalization.DateTimeFormatInfo" /> is read-only. </exception>
		public string[] AbbreviatedDayNames
		{
			get
			{
				return (string[])RawAbbreviatedDayNames.Clone();
			}
			set
			{
				RawAbbreviatedDayNames = value;
			}
		}

		internal string[] RawAbbreviatedDayNames
		{
			get
			{
				return abbreviatedDayNames;
			}
			set
			{
				if (IsReadOnly)
				{
					throw new InvalidOperationException(MSG_READONLY);
				}
				if (value == null)
				{
					throw new ArgumentNullException();
				}
				if (value.GetLength(0) != 7)
				{
					throw new ArgumentException(MSG_ARRAYSIZE_DAY);
				}
				abbreviatedDayNames = (string[])value.Clone();
			}
		}

		/// <summary>Gets or sets a one-dimensional array of type <see cref="T:System.String" /> containing the culture-specific abbreviated names of the months.</summary>
		/// <returns>A one-dimensional array of type <see cref="T:System.String" /> containing the culture-specific abbreviated names of the months. In a 12-month calendar, the 13th element of the array is an empty string. The array for <see cref="P:System.Globalization.DateTimeFormatInfo.InvariantInfo" /> contains "Jan", "Feb", "Mar", "Apr", "May", "Jun", "Jul", "Aug", "Sep", "Oct", "Nov", "Dec", and "".</returns>
		/// <exception cref="T:System.ArgumentNullException">The property is being set to null. </exception>
		/// <exception cref="T:System.ArgumentException">The property is being set to an array that is multidimensional or that has a length that is not exactly 13. </exception>
		/// <exception cref="T:System.InvalidOperationException">The property is being set and the <see cref="T:System.Globalization.DateTimeFormatInfo" /> is read-only. </exception>
		public string[] AbbreviatedMonthNames
		{
			get
			{
				return (string[])RawAbbreviatedMonthNames.Clone();
			}
			set
			{
				RawAbbreviatedMonthNames = value;
			}
		}

		internal string[] RawAbbreviatedMonthNames
		{
			get
			{
				return abbreviatedMonthNames;
			}
			set
			{
				if (IsReadOnly)
				{
					throw new InvalidOperationException(MSG_READONLY);
				}
				if (value == null)
				{
					throw new ArgumentNullException();
				}
				if (value.GetLength(0) != 13)
				{
					throw new ArgumentException(MSG_ARRAYSIZE_MONTH);
				}
				abbreviatedMonthNames = (string[])value.Clone();
			}
		}

		/// <summary>Gets or sets a one-dimensional array of type <see cref="T:System.String" /> containing the culture-specific full names of the days of the week.</summary>
		/// <returns>A one-dimensional array of type <see cref="T:System.String" /> containing the culture-specific full names of the days of the week. The array for <see cref="P:System.Globalization.DateTimeFormatInfo.InvariantInfo" /> contains "Sunday", "Monday", "Tuesday", "Wednesday", "Thursday", "Friday", and "Saturday".</returns>
		/// <exception cref="T:System.ArgumentNullException">The property is being set to null. </exception>
		/// <exception cref="T:System.ArgumentException">The property is being set to an array that is multidimensional or that has a length that is not exactly 7. </exception>
		/// <exception cref="T:System.InvalidOperationException">The property is being set and the <see cref="T:System.Globalization.DateTimeFormatInfo" /> is read-only. </exception>
		public string[] DayNames
		{
			get
			{
				return (string[])RawDayNames.Clone();
			}
			set
			{
				RawDayNames = value;
			}
		}

		internal string[] RawDayNames
		{
			get
			{
				return dayNames;
			}
			set
			{
				if (IsReadOnly)
				{
					throw new InvalidOperationException(MSG_READONLY);
				}
				if (value == null)
				{
					throw new ArgumentNullException();
				}
				if (value.GetLength(0) != 7)
				{
					throw new ArgumentException(MSG_ARRAYSIZE_DAY);
				}
				dayNames = (string[])value.Clone();
			}
		}

		/// <summary>Gets or sets a one-dimensional array of type <see cref="T:System.String" /> containing the culture-specific full names of the months.</summary>
		/// <returns>A one-dimensional array of type <see cref="T:System.String" /> containing the culture-specific full names of the months. In a 12-month calendar, the 13th element of the array is an empty string. The array for <see cref="P:System.Globalization.DateTimeFormatInfo.InvariantInfo" /> contains "January", "February", "March", "April", "May", "June", "July", "August", "September", "October", "November", "December", and "".</returns>
		/// <exception cref="T:System.ArgumentNullException">The property is being set to null. </exception>
		/// <exception cref="T:System.ArgumentException">The property is being set to an array that is multidimensional or that has a length that is not exactly 13. </exception>
		/// <exception cref="T:System.InvalidOperationException">The property is being set and the <see cref="T:System.Globalization.DateTimeFormatInfo" /> is read-only. </exception>
		public string[] MonthNames
		{
			get
			{
				return (string[])RawMonthNames.Clone();
			}
			set
			{
				RawMonthNames = value;
			}
		}

		internal string[] RawMonthNames
		{
			get
			{
				return monthNames;
			}
			set
			{
				if (IsReadOnly)
				{
					throw new InvalidOperationException(MSG_READONLY);
				}
				if (value == null)
				{
					throw new ArgumentNullException();
				}
				if (value.GetLength(0) != 13)
				{
					throw new ArgumentException(MSG_ARRAYSIZE_MONTH);
				}
				monthNames = (string[])value.Clone();
			}
		}

		/// <summary>Gets or sets the string designator for hours that are "ante meridiem" (before noon).</summary>
		/// <returns>The string designator for hours that are "ante meridiem" (before noon). The default for <see cref="P:System.Globalization.DateTimeFormatInfo.InvariantInfo" /> is "AM".</returns>
		/// <exception cref="T:System.ArgumentNullException">The property is being set to null. </exception>
		/// <exception cref="T:System.InvalidOperationException">The property is being set and the <see cref="T:System.Globalization.DateTimeFormatInfo" /> is read-only. </exception>
		public string AMDesignator
		{
			get
			{
				return amDesignator;
			}
			set
			{
				if (IsReadOnly)
				{
					throw new InvalidOperationException(MSG_READONLY);
				}
				if (value == null)
				{
					throw new ArgumentNullException();
				}
				amDesignator = value;
			}
		}

		/// <summary>Gets or sets the string designator for hours that are "post meridiem" (after noon).</summary>
		/// <returns>The string designator for hours that are "post meridiem" (after noon). The default for <see cref="P:System.Globalization.DateTimeFormatInfo.InvariantInfo" /> is "PM".</returns>
		/// <exception cref="T:System.ArgumentNullException">The property is being set to null. </exception>
		/// <exception cref="T:System.InvalidOperationException">The property is being set and the <see cref="T:System.Globalization.DateTimeFormatInfo" /> is read-only. </exception>
		public string PMDesignator
		{
			get
			{
				return pmDesignator;
			}
			set
			{
				if (IsReadOnly)
				{
					throw new InvalidOperationException(MSG_READONLY);
				}
				if (value == null)
				{
					throw new ArgumentNullException();
				}
				pmDesignator = value;
			}
		}

		/// <summary>Gets or sets the string that separates the components of a date, that is, the year, month, and day.</summary>
		/// <returns>The string that separates the components of a date, that is, the year, month, and day. The default for <see cref="P:System.Globalization.DateTimeFormatInfo.InvariantInfo" /> is "/".</returns>
		/// <exception cref="T:System.ArgumentNullException">The property is being set to null. </exception>
		/// <exception cref="T:System.InvalidOperationException">The property is being set and the <see cref="T:System.Globalization.DateTimeFormatInfo" /> is read-only. </exception>
		public string DateSeparator
		{
			get
			{
				return dateSeparator;
			}
			set
			{
				if (IsReadOnly)
				{
					throw new InvalidOperationException(MSG_READONLY);
				}
				if (value == null)
				{
					throw new ArgumentNullException();
				}
				dateSeparator = value;
			}
		}

		/// <summary>Gets or sets the string that separates the components of time, that is, the hour, minutes, and seconds.</summary>
		/// <returns>The string that separates the components of time. The default for <see cref="P:System.Globalization.DateTimeFormatInfo.InvariantInfo" /> is ":".</returns>
		/// <exception cref="T:System.ArgumentNullException">The property is being set to null. </exception>
		/// <exception cref="T:System.InvalidOperationException">The property is being set and the <see cref="T:System.Globalization.DateTimeFormatInfo" /> is read-only. </exception>
		public string TimeSeparator
		{
			get
			{
				return timeSeparator;
			}
			set
			{
				if (IsReadOnly)
				{
					throw new InvalidOperationException(MSG_READONLY);
				}
				if (value == null)
				{
					throw new ArgumentNullException();
				}
				timeSeparator = value;
			}
		}

		/// <summary>Gets or sets the format pattern for a long date value, which is associated with the "D" format pattern.</summary>
		/// <returns>The format pattern for a long date value, which is associated with the "D" format pattern.</returns>
		/// <exception cref="T:System.ArgumentNullException">The property is being set to null. </exception>
		/// <exception cref="T:System.InvalidOperationException">The property is being set and the <see cref="T:System.Globalization.DateTimeFormatInfo" /> is read-only. </exception>
		public string LongDatePattern
		{
			get
			{
				return longDatePattern;
			}
			set
			{
				if (IsReadOnly)
				{
					throw new InvalidOperationException(MSG_READONLY);
				}
				if (value == null)
				{
					throw new ArgumentNullException();
				}
				longDatePattern = value;
			}
		}

		/// <summary>Gets or sets the format pattern for a short date value, which is associated with the "d" format pattern.</summary>
		/// <returns>The format pattern for a short date value, which is associated with the "d" format pattern.</returns>
		/// <exception cref="T:System.ArgumentNullException">The property is being set to null. </exception>
		/// <exception cref="T:System.InvalidOperationException">The property is being set and the <see cref="T:System.Globalization.DateTimeFormatInfo" /> is read-only. </exception>
		public string ShortDatePattern
		{
			get
			{
				return shortDatePattern;
			}
			set
			{
				if (IsReadOnly)
				{
					throw new InvalidOperationException(MSG_READONLY);
				}
				if (value == null)
				{
					throw new ArgumentNullException();
				}
				shortDatePattern = value;
			}
		}

		/// <summary>Gets or sets the format pattern for a short time value, which is associated with the "t" format pattern.</summary>
		/// <returns>The format pattern for a short time value, which is associated with the "t" format pattern.</returns>
		/// <exception cref="T:System.ArgumentNullException">The property is being set to null. </exception>
		/// <exception cref="T:System.InvalidOperationException">The property is being set and the <see cref="T:System.Globalization.DateTimeFormatInfo" /> is read-only. </exception>
		public string ShortTimePattern
		{
			get
			{
				return shortTimePattern;
			}
			set
			{
				if (IsReadOnly)
				{
					throw new InvalidOperationException(MSG_READONLY);
				}
				if (value == null)
				{
					throw new ArgumentNullException();
				}
				shortTimePattern = value;
			}
		}

		/// <summary>Gets or sets the format pattern for a long time value, which is associated with the "T" format pattern.</summary>
		/// <returns>The format pattern for a long time value, which is associated with the "T" format pattern.</returns>
		/// <exception cref="T:System.ArgumentNullException">The property is being set to null. </exception>
		/// <exception cref="T:System.InvalidOperationException">The property is being set and the <see cref="T:System.Globalization.DateTimeFormatInfo" /> is read-only. </exception>
		public string LongTimePattern
		{
			get
			{
				return longTimePattern;
			}
			set
			{
				if (IsReadOnly)
				{
					throw new InvalidOperationException(MSG_READONLY);
				}
				if (value == null)
				{
					throw new ArgumentNullException();
				}
				longTimePattern = value;
			}
		}

		/// <summary>Gets or sets the format pattern for a month and day value, which is associated with the "m" and "M" format patterns.</summary>
		/// <returns>The format pattern for a month and day value, which is associated with the "m" and "M" format patterns.</returns>
		/// <exception cref="T:System.ArgumentNullException">The property is being set to null. </exception>
		/// <exception cref="T:System.InvalidOperationException">The property is being set and the <see cref="T:System.Globalization.DateTimeFormatInfo" /> is read-only. </exception>
		public string MonthDayPattern
		{
			get
			{
				return monthDayPattern;
			}
			set
			{
				if (IsReadOnly)
				{
					throw new InvalidOperationException(MSG_READONLY);
				}
				if (value == null)
				{
					throw new ArgumentNullException();
				}
				monthDayPattern = value;
			}
		}

		/// <summary>Gets or sets the format pattern for a year and month value, which is associated with the "y" and "Y" format patterns.</summary>
		/// <returns>The format pattern for a year and month value, which is associated with the "y" and "Y" format patterns.</returns>
		/// <exception cref="T:System.ArgumentNullException">The property is being set to null. </exception>
		/// <exception cref="T:System.InvalidOperationException">The property is being set and the <see cref="T:System.Globalization.DateTimeFormatInfo" /> is read-only. </exception>
		public string YearMonthPattern
		{
			get
			{
				return yearMonthPattern;
			}
			set
			{
				if (IsReadOnly)
				{
					throw new InvalidOperationException(MSG_READONLY);
				}
				if (value == null)
				{
					throw new ArgumentNullException();
				}
				yearMonthPattern = value;
			}
		}

		/// <summary>Gets or sets the format pattern for a long date and long time value, which is associated with the "F" format pattern.</summary>
		/// <returns>The format pattern for a long date and long time value, which is associated with the "F" format pattern.</returns>
		/// <exception cref="T:System.ArgumentNullException">The property is being set to null. </exception>
		/// <exception cref="T:System.InvalidOperationException">The property is being set and the <see cref="T:System.Globalization.DateTimeFormatInfo" /> is read-only. </exception>
		public string FullDateTimePattern
		{
			get
			{
				if (fullDateTimePattern != null)
				{
					return fullDateTimePattern;
				}
				return longDatePattern + " " + longTimePattern;
			}
			set
			{
				if (IsReadOnly)
				{
					throw new InvalidOperationException(MSG_READONLY);
				}
				if (value == null)
				{
					throw new ArgumentNullException();
				}
				fullDateTimePattern = value;
			}
		}

		/// <summary>Gets a read-only <see cref="T:System.Globalization.DateTimeFormatInfo" /> object that formats values based on the current culture.</summary>
		/// <returns>A read-only <see cref="T:System.Globalization.DateTimeFormatInfo" /> object based on the <see cref="T:System.Globalization.CultureInfo" /> object for the current thread.</returns>
		public static DateTimeFormatInfo CurrentInfo => Thread.CurrentThread.CurrentCulture.DateTimeFormat;

		/// <summary>Gets the default read-only <see cref="T:System.Globalization.DateTimeFormatInfo" /> that is culture-independent (invariant).</summary>
		/// <returns>The default read-only <see cref="T:System.Globalization.DateTimeFormatInfo" /> object that is culture-independent (invariant).</returns>
		public static DateTimeFormatInfo InvariantInfo
		{
			get
			{
				if (theInvariantDateTimeFormatInfo == null)
				{
					theInvariantDateTimeFormatInfo = ReadOnly(new DateTimeFormatInfo());
					theInvariantDateTimeFormatInfo.FillInvariantPatterns();
				}
				return theInvariantDateTimeFormatInfo;
			}
		}

		/// <summary>Gets or sets the first day of the week.</summary>
		/// <returns>A <see cref="T:System.DayOfWeek" /> value representing the first day of the week. The default for <see cref="P:System.Globalization.DateTimeFormatInfo.InvariantInfo" /> is <see cref="F:System.DayOfWeek.Sunday" />.</returns>
		/// <exception cref="T:System.ArgumentOutOfRangeException">The property is being set to a value that is not a valid <see cref="T:System.DayOfWeek" /> value. </exception>
		/// <exception cref="T:System.InvalidOperationException">The property is being set and the <see cref="T:System.Globalization.DateTimeFormatInfo" /> is read-only. </exception>
		public DayOfWeek FirstDayOfWeek
		{
			get
			{
				return (DayOfWeek)firstDayOfWeek;
			}
			set
			{
				if (IsReadOnly)
				{
					throw new InvalidOperationException(MSG_READONLY);
				}
				if (value < DayOfWeek.Sunday || value > DayOfWeek.Saturday)
				{
					throw new ArgumentOutOfRangeException();
				}
				firstDayOfWeek = (int)value;
			}
		}

		/// <summary>Gets or sets the calendar to use for the current culture.</summary>
		/// <returns>The <see cref="T:System.Globalization.Calendar" /> indicating the calendar to use for the current culture. The default for <see cref="P:System.Globalization.DateTimeFormatInfo.InvariantInfo" /> is the <see cref="T:System.Globalization.GregorianCalendar" />.</returns>
		/// <exception cref="T:System.ArgumentNullException">The property is being set to null. </exception>
		/// <exception cref="T:System.ArgumentException">The property is being set to a <see cref="T:System.Globalization.Calendar" /> that is not valid for the current culture. </exception>
		/// <exception cref="T:System.InvalidOperationException">The property is being set and the <see cref="T:System.Globalization.DateTimeFormatInfo" /> is read-only. </exception>
		public Calendar Calendar
		{
			get
			{
				return calendar;
			}
			set
			{
				if (IsReadOnly)
				{
					throw new InvalidOperationException(MSG_READONLY);
				}
				if (value == null)
				{
					throw new ArgumentNullException();
				}
				calendar = value;
			}
		}

		/// <summary>Gets or sets a value that specifies which rule is used to determine the first calendar week of the year.</summary>
		/// <returns>A <see cref="T:System.Globalization.CalendarWeekRule" /> value that determines the first calendar week of the year. The default for <see cref="P:System.Globalization.DateTimeFormatInfo.InvariantInfo" /> is <see cref="F:System.Globalization.CalendarWeekRule.FirstDay" />.</returns>
		/// <exception cref="T:System.ArgumentOutOfRangeException">The property is being set to a value that is not a valid <see cref="T:System.Globalization.CalendarWeekRule" /> value. </exception>
		public CalendarWeekRule CalendarWeekRule
		{
			get
			{
				return (CalendarWeekRule)calendarWeekRule;
			}
			set
			{
				if (IsReadOnly)
				{
					throw new InvalidOperationException(MSG_READONLY);
				}
				calendarWeekRule = (int)value;
			}
		}

		/// <summary>Gets the format pattern for a time value, which is based on the Internet Engineering Task Force (IETF) Request for Comments (RFC) 1123 specification and is associated with the "r" and "R" format patterns.</summary>
		/// <returns>The format pattern for a time value, which is based on the IETF RFC 1123 specification and is associated with the "r" and "R" format patterns.</returns>
		public string RFC1123Pattern => _RFC1123Pattern;

		internal string RoundtripPattern => "yyyy'-'MM'-'dd'T'HH':'mm':'ss.fffffffK";

		/// <summary>Gets the format pattern for a sortable date and time value, which is associated with the "s" format pattern.</summary>
		/// <returns>The format pattern for a sortable date and time value, which is associated with the "s" format pattern.</returns>
		public string SortableDateTimePattern => _SortableDateTimePattern;

		/// <summary>Gets the format pattern for a universal sortable date and time value, which is associated with the "u" format pattern.</summary>
		/// <returns>The format pattern for a universal sortable date and time value, which is associated with the "u" format pattern.</returns>
		public string UniversalSortableDateTimePattern => _UniversalSortableDateTimePattern;

		/// <summary>Gets or sets a string array of abbreviated month names associated with the current <see cref="T:System.Globalization.DateTimeFormatInfo" /> object.</summary>
		/// <returns>A string array of abbreviated month names.</returns>
		/// <exception cref="T:System.ArgumentNullException">In a set operation, the value array or one of the elements of the value array is null.</exception>
		[ComVisible(false)]
		[MonoTODO("Returns only the English month abbreviated names")]
		public string[] AbbreviatedMonthGenitiveNames
		{
			get
			{
				return m_genitiveAbbreviatedMonthNames;
			}
			set
			{
				m_genitiveAbbreviatedMonthNames = value;
			}
		}

		/// <summary>Gets or sets a string array of month names associated with the current <see cref="T:System.Globalization.DateTimeFormatInfo" /> object.</summary>
		/// <returns>A string array of month names.</returns>
		/// <exception cref="T:System.ArgumentNullException">In a set operation, the value array or one of the elements of the value array is null.</exception>
		[ComVisible(false)]
		[MonoTODO("Returns only the English moth names")]
		public string[] MonthGenitiveNames
		{
			get
			{
				return genitiveMonthNames;
			}
			set
			{
				genitiveMonthNames = value;
			}
		}

		/// <summary>Gets the native name of the calendar associated with the current <see cref="T:System.Globalization.DateTimeFormatInfo" /> object.</summary>
		/// <returns>The native name of the calendar used in the culture associated with the current <see cref="T:System.Globalization.DateTimeFormatInfo" /> object if that name is available, or the empty string ("") if the native calendar name is not available.</returns>
		[MonoTODO("Returns an empty string as if the calendar name wasn't available")]
		[ComVisible(false)]
		public string NativeCalendarName => string.Empty;

		/// <summary>Gets or sets a string array of the shortest unique abbreviated day names associated with the current <see cref="T:System.Globalization.DateTimeFormatInfo" /> object.</summary>
		/// <returns>A string array of day names.</returns>
		/// <exception cref="T:System.ArgumentNullException">In a set operation, the value array or one of the elements of the value array is null.</exception>
		[ComVisible(false)]
		public string[] ShortestDayNames
		{
			get
			{
				return shortDayNames;
			}
			set
			{
				if (value == null)
				{
					throw new ArgumentNullException();
				}
				if (value.Length != 7)
				{
					throw new ArgumentException("Array must have 7 entries");
				}
				for (int i = 0; i < 7; i++)
				{
					if (value[i] == null)
					{
						throw new ArgumentNullException($"Element {i} is null");
					}
				}
				shortDayNames = value;
			}
		}

		internal DateTimeFormatInfo(bool read_only)
		{
			m_isReadOnly = read_only;
			amDesignator = "AM";
			pmDesignator = "PM";
			dateSeparator = "/";
			timeSeparator = ":";
			shortDatePattern = "MM/dd/yyyy";
			longDatePattern = "dddd, dd MMMM yyyy";
			shortTimePattern = "HH:mm";
			longTimePattern = "HH:mm:ss";
			monthDayPattern = "MMMM dd";
			yearMonthPattern = "yyyy MMMM";
			fullDateTimePattern = "dddd, dd MMMM yyyy HH:mm:ss";
			_RFC1123Pattern = "ddd, dd MMM yyyy HH':'mm':'ss 'GMT'";
			_SortableDateTimePattern = "yyyy'-'MM'-'dd'T'HH':'mm':'ss";
			_UniversalSortableDateTimePattern = "yyyy'-'MM'-'dd HH':'mm':'ss'Z'";
			firstDayOfWeek = 0;
			calendar = new GregorianCalendar();
			calendarWeekRule = 0;
			abbreviatedDayNames = INVARIANT_ABBREVIATED_DAY_NAMES;
			dayNames = INVARIANT_DAY_NAMES;
			abbreviatedMonthNames = INVARIANT_ABBREVIATED_MONTH_NAMES;
			monthNames = INVARIANT_MONTH_NAMES;
			m_genitiveAbbreviatedMonthNames = INVARIANT_ABBREVIATED_MONTH_NAMES;
			genitiveMonthNames = INVARIANT_MONTH_NAMES;
			shortDayNames = INVARIANT_SHORT_DAY_NAMES;
		}

		/// <summary>Initializes a new writable instance of the <see cref="T:System.Globalization.DateTimeFormatInfo" /> class that is culture-independent (invariant).</summary>
		public DateTimeFormatInfo()
			: this(read_only: false)
		{
		}

		/// <summary>Returns the <see cref="T:System.Globalization.DateTimeFormatInfo" /> associated with the specified <see cref="T:System.IFormatProvider" />.</summary>
		/// <returns>A <see cref="T:System.Globalization.DateTimeFormatInfo" /> associated with the specified <see cref="T:System.IFormatProvider" />.</returns>
		/// <param name="provider">The <see cref="T:System.IFormatProvider" /> that gets the <see cref="T:System.Globalization.DateTimeFormatInfo" />.-or- null to get <see cref="P:System.Globalization.DateTimeFormatInfo.CurrentInfo" />. </param>
		public static DateTimeFormatInfo GetInstance(IFormatProvider provider)
		{
			if (provider != null)
			{
				DateTimeFormatInfo dateTimeFormatInfo = (DateTimeFormatInfo)provider.GetFormat(typeof(DateTimeFormatInfo));
				if (dateTimeFormatInfo != null)
				{
					return dateTimeFormatInfo;
				}
			}
			return CurrentInfo;
		}

		/// <summary>Returns a read-only <see cref="T:System.Globalization.DateTimeFormatInfo" /> wrapper.</summary>
		/// <returns>A read-only <see cref="T:System.Globalization.DateTimeFormatInfo" /> wrapper around <paramref name="dtfi" />.</returns>
		/// <param name="dtfi">The <see cref="T:System.Globalization.DateTimeFormatInfo" /> to wrap. </param>
		/// <exception cref="T:System.ArgumentNullException">
		///   <paramref name="dtfi" /> is null. </exception>
		public static DateTimeFormatInfo ReadOnly(DateTimeFormatInfo dtfi)
		{
			DateTimeFormatInfo dateTimeFormatInfo = (DateTimeFormatInfo)dtfi.Clone();
			dateTimeFormatInfo.m_isReadOnly = true;
			return dateTimeFormatInfo;
		}

		/// <summary>Creates a shallow copy of the <see cref="T:System.Globalization.DateTimeFormatInfo" />.</summary>
		/// <returns>A new <see cref="T:System.Globalization.DateTimeFormatInfo" /> copied from the original <see cref="T:System.Globalization.DateTimeFormatInfo" />.</returns>
		public object Clone()
		{
			DateTimeFormatInfo dateTimeFormatInfo = (DateTimeFormatInfo)MemberwiseClone();
			dateTimeFormatInfo.m_isReadOnly = false;
			return dateTimeFormatInfo;
		}

		/// <summary>Returns an object of the specified type that provides a <see cref="T:System.DateTime" /> formatting service.</summary>
		/// <returns>The current <see cref="T:System.Globalization.DateTimeFormatInfo" />, if <paramref name="formatType" /> is the same as the type of the current <see cref="T:System.Globalization.DateTimeFormatInfo" />; otherwise, null.</returns>
		/// <param name="formatType">The <see cref="T:System.Type" /> of the required formatting service. </param>
		public object GetFormat(Type formatType)
		{
			return (formatType != GetType()) ? null : this;
		}

		/// <summary>Returns the string containing the abbreviated name of the specified era, if an abbreviation exists.</summary>
		/// <returns>A string containing the abbreviated name of the specified era, if an abbreviation exists.-or- A string containing the full name of the era, if an abbreviation does not exist.</returns>
		/// <param name="era">The integer representing the era. </param>
		/// <exception cref="T:System.ArgumentOutOfRangeException">
		///   <paramref name="era" /> does not represent a valid era in the calendar specified in the <see cref="P:System.Globalization.DateTimeFormatInfo.Calendar" /> property. </exception>
		public string GetAbbreviatedEraName(int era)
		{
			if (era < 0 || era >= calendar.AbbreviatedEraNames.Length)
			{
				throw new ArgumentOutOfRangeException("era", era.ToString());
			}
			return calendar.AbbreviatedEraNames[era];
		}

		/// <summary>Returns the culture-specific abbreviated name of the specified month based on the culture associated with the current <see cref="T:System.Globalization.DateTimeFormatInfo" /> object.</summary>
		/// <returns>The culture-specific abbreviated name of the month represented by <paramref name="month" />.</returns>
		/// <param name="month">An integer from 1 through 13 representing the name of the month to retrieve. </param>
		/// <exception cref="T:System.ArgumentOutOfRangeException">
		///   <paramref name="month" /> is less than 1 or greater than 13. </exception>
		public string GetAbbreviatedMonthName(int month)
		{
			if (month < 1 || month > 13)
			{
				throw new ArgumentOutOfRangeException();
			}
			return abbreviatedMonthNames[month - 1];
		}

		/// <summary>Returns the integer representing the specified era.</summary>
		/// <returns>The integer representing the era, if <paramref name="eraName" /> is valid; otherwise, -1.</returns>
		/// <param name="eraName">The string containing the name of the era. </param>
		/// <exception cref="T:System.ArgumentNullException">
		///   <paramref name="eraName" /> is null. </exception>
		public int GetEra(string eraName)
		{
			if (eraName == null)
			{
				throw new ArgumentNullException();
			}
			string[] eraNames = calendar.EraNames;
			for (int i = 0; i < eraNames.Length; i++)
			{
				if (CultureInfo.InvariantCulture.CompareInfo.Compare(eraName, eraNames[i], CompareOptions.IgnoreCase) == 0)
				{
					return calendar.Eras[i];
				}
			}
			eraNames = calendar.AbbreviatedEraNames;
			for (int j = 0; j < eraNames.Length; j++)
			{
				if (CultureInfo.InvariantCulture.CompareInfo.Compare(eraName, eraNames[j], CompareOptions.IgnoreCase) == 0)
				{
					return calendar.Eras[j];
				}
			}
			return -1;
		}

		/// <summary>Returns the string containing the name of the specified era.</summary>
		/// <returns>A string containing the name of the era.</returns>
		/// <param name="era">The integer representing the era. </param>
		/// <exception cref="T:System.ArgumentOutOfRangeException">
		///   <paramref name="era" /> does not represent a valid era in the calendar specified in the <see cref="P:System.Globalization.DateTimeFormatInfo.Calendar" /> property. </exception>
		public string GetEraName(int era)
		{
			if (era < 0 || era > calendar.EraNames.Length)
			{
				throw new ArgumentOutOfRangeException("era", era.ToString());
			}
			return calendar.EraNames[era - 1];
		}

		/// <summary>Returns the culture-specific full name of the specified month based on the culture associated with the current <see cref="T:System.Globalization.DateTimeFormatInfo" /> object.</summary>
		/// <returns>The culture-specific full name of the month represented by <paramref name="month" />.</returns>
		/// <param name="month">An integer from 1 through 13 representing the name of the month to retrieve. </param>
		/// <exception cref="T:System.ArgumentOutOfRangeException">
		///   <paramref name="month" /> is less than 1 or greater than 13. </exception>
		public string GetMonthName(int month)
		{
			if (month < 1 || month > 13)
			{
				throw new ArgumentOutOfRangeException();
			}
			return monthNames[month - 1];
		}

		/// <summary>Returns all the standard patterns in which <see cref="T:System.DateTime" /> values can be formatted.</summary>
		/// <returns>An array containing the standard patterns in which <see cref="T:System.DateTime" /> values can be formatted.</returns>
		public string[] GetAllDateTimePatterns()
		{
			return (string[])GetAllDateTimePatternsInternal().Clone();
		}

		internal string[] GetAllDateTimePatternsInternal()
		{
			FillAllDateTimePatterns();
			return all_date_time_patterns;
		}

		private void FillAllDateTimePatterns()
		{
			if (all_date_time_patterns == null)
			{
				ArrayList arrayList = new ArrayList();
				arrayList.AddRange(GetAllRawDateTimePatterns('d'));
				arrayList.AddRange(GetAllRawDateTimePatterns('D'));
				arrayList.AddRange(GetAllRawDateTimePatterns('g'));
				arrayList.AddRange(GetAllRawDateTimePatterns('G'));
				arrayList.AddRange(GetAllRawDateTimePatterns('f'));
				arrayList.AddRange(GetAllRawDateTimePatterns('F'));
				arrayList.AddRange(GetAllRawDateTimePatterns('m'));
				arrayList.AddRange(GetAllRawDateTimePatterns('M'));
				arrayList.AddRange(GetAllRawDateTimePatterns('r'));
				arrayList.AddRange(GetAllRawDateTimePatterns('R'));
				arrayList.AddRange(GetAllRawDateTimePatterns('s'));
				arrayList.AddRange(GetAllRawDateTimePatterns('t'));
				arrayList.AddRange(GetAllRawDateTimePatterns('T'));
				arrayList.AddRange(GetAllRawDateTimePatterns('u'));
				arrayList.AddRange(GetAllRawDateTimePatterns('U'));
				arrayList.AddRange(GetAllRawDateTimePatterns('y'));
				arrayList.AddRange(GetAllRawDateTimePatterns('Y'));
				all_date_time_patterns = (string[])arrayList.ToArray(typeof(string));
			}
		}

		/// <summary>Returns all the standard patterns in which <see cref="T:System.DateTime" /> values can be formatted using the specified format pattern.</summary>
		/// <returns>An array containing the standard patterns in which <see cref="T:System.DateTime" /> values can be formatted using the specified format pattern.</returns>
		/// <param name="format">A standard format pattern. </param>
		/// <exception cref="T:System.ArgumentException">
		///   <paramref name="format" /> is not a valid standard format pattern. </exception>
		public string[] GetAllDateTimePatterns(char format)
		{
			return (string[])GetAllRawDateTimePatterns(format).Clone();
		}

		internal string[] GetAllRawDateTimePatterns(char format)
		{
			switch (format)
			{
			case 'D':
				if (allLongDatePatterns == null || allLongDatePatterns.Length <= 0)
				{
					return new string[1]
					{
						LongDatePattern
					};
				}
				return allLongDatePatterns;
			case 'd':
				if (allShortDatePatterns == null || allShortDatePatterns.Length <= 0)
				{
					return new string[1]
					{
						ShortDatePattern
					};
				}
				return allShortDatePatterns;
			case 'T':
				if (allLongTimePatterns == null || allLongTimePatterns.Length <= 0)
				{
					return new string[1]
					{
						LongTimePattern
					};
				}
				return allLongTimePatterns;
			case 't':
				if (allShortTimePatterns == null || allShortTimePatterns.Length <= 0)
				{
					return new string[1]
					{
						ShortTimePattern
					};
				}
				return allShortTimePatterns;
			case 'G':
			{
				string[] array = PopulateCombinedList(allShortDatePatterns, allLongTimePatterns);
				if (array == null || array.Length <= 0)
				{
					return new string[1]
					{
						ShortDatePattern + " " + LongTimePattern
					};
				}
				return array;
			}
			case 'g':
			{
				string[] array = PopulateCombinedList(allShortDatePatterns, allShortTimePatterns);
				if (array == null || array.Length <= 0)
				{
					return new string[1]
					{
						ShortDatePattern + " " + ShortTimePattern
					};
				}
				return array;
			}
			case 'F':
			case 'U':
			{
				string[] array = PopulateCombinedList(allLongDatePatterns, allLongTimePatterns);
				if (array == null || array.Length <= 0)
				{
					return new string[1]
					{
						LongDatePattern + " " + LongTimePattern
					};
				}
				return array;
			}
			case 'f':
			{
				string[] array = PopulateCombinedList(allLongDatePatterns, allShortTimePatterns);
				if (array == null || array.Length <= 0)
				{
					return new string[1]
					{
						LongDatePattern + " " + ShortTimePattern
					};
				}
				return array;
			}
			case 'M':
			case 'm':
				if (monthDayPatterns == null || monthDayPatterns.Length <= 0)
				{
					return new string[1]
					{
						MonthDayPattern
					};
				}
				return monthDayPatterns;
			case 'Y':
			case 'y':
				if (yearMonthPatterns == null || yearMonthPatterns.Length <= 0)
				{
					return new string[1]
					{
						YearMonthPattern
					};
				}
				return yearMonthPatterns;
			case 'R':
			case 'r':
				return new string[1]
				{
					RFC1123Pattern
				};
			case 's':
				return new string[1]
				{
					SortableDateTimePattern
				};
			case 'u':
				return new string[1]
				{
					UniversalSortableDateTimePattern
				};
			default:
				throw new ArgumentException("Format specifier was invalid.");
			}
		}

		/// <summary>Returns the culture-specific full name of the specified day of the week based on the culture associated with the current <see cref="T:System.Globalization.DateTimeFormatInfo" /> object.</summary>
		/// <returns>The culture-specific full name of the day of the week represented by <paramref name="dayofweek" />.</returns>
		/// <param name="dayofweek">A <see cref="T:System.DayOfWeek" /> value. </param>
		/// <exception cref="T:System.ArgumentOutOfRangeException">
		///   <paramref name="dayofweek" /> is not a valid <see cref="T:System.DayOfWeek" /> value. </exception>
		public string GetDayName(DayOfWeek dayofweek)
		{
			if (dayofweek < DayOfWeek.Sunday || dayofweek > DayOfWeek.Saturday)
			{
				throw new ArgumentOutOfRangeException();
			}
			return dayNames[(int)dayofweek];
		}

		/// <summary>Returns the culture-specific abbreviated name of the specified day of the week based on the culture associated with the current <see cref="T:System.Globalization.DateTimeFormatInfo" /> object.</summary>
		/// <returns>The culture-specific abbreviated name of the day of the week represented by <paramref name="dayofweek" />.</returns>
		/// <param name="dayofweek">A <see cref="T:System.DayOfWeek" /> value. </param>
		/// <exception cref="T:System.ArgumentOutOfRangeException">
		///   <paramref name="dayofweek" /> is not a valid <see cref="T:System.DayOfWeek" /> value. </exception>
		public string GetAbbreviatedDayName(DayOfWeek dayofweek)
		{
			if (dayofweek < DayOfWeek.Sunday || dayofweek > DayOfWeek.Saturday)
			{
				throw new ArgumentOutOfRangeException();
			}
			return abbreviatedDayNames[(int)dayofweek];
		}

		private void FillInvariantPatterns()
		{
			allShortDatePatterns = new string[1]
			{
				"MM/dd/yyyy"
			};
			allLongDatePatterns = new string[1]
			{
				"dddd, dd MMMM yyyy"
			};
			allLongTimePatterns = new string[1]
			{
				"HH:mm:ss"
			};
			allShortTimePatterns = new string[4]
			{
				"HH:mm",
				"hh:mm tt",
				"H:mm",
				"h:mm tt"
			};
			monthDayPatterns = new string[1]
			{
				"MMMM dd"
			};
			yearMonthPatterns = new string[1]
			{
				"yyyy MMMM"
			};
		}

		private string[] PopulateCombinedList(string[] dates, string[] times)
		{
			if (dates != null && times != null)
			{
				string[] array = new string[dates.Length * times.Length];
				int num = 0;
				foreach (string str in dates)
				{
					foreach (string str2 in times)
					{
						array[num++] = str + " " + str2;
					}
				}
				return array;
			}
			return null;
		}

		/// <summary>Obtains the shortest abbreviated day name for a specified day of the week associated with the current <see cref="T:System.Globalization.DateTimeFormatInfo" /> object.</summary>
		/// <returns>The abbreviated name of the week that corresponds to the <paramref name="dayOfWeek" /> parameter.</returns>
		/// <param name="dayOfWeek">One of the <see cref="T:System.DayOfWeek" /> values.</param>
		[ComVisible(false)]
		public string GetShortestDayName(DayOfWeek dayOfWeek)
		{
			if (dayOfWeek < DayOfWeek.Sunday || dayOfWeek > DayOfWeek.Saturday)
			{
				throw new ArgumentOutOfRangeException();
			}
			return shortDayNames[(int)dayOfWeek];
		}

		/// <summary>Sets all the standard patterns in which a <see cref="T:System.DateTime" /> value can be formatted.</summary>
		/// <param name="patterns">A string array of format patterns.</param>
		/// <param name="format">The standard format pattern associated with the patterns specified in the <paramref name="patterns" /> parameter. </param>
		[ComVisible(false)]
		public void SetAllDateTimePatterns(string[] patterns, char format)
		{
			if (patterns == null)
			{
				throw new ArgumentNullException("patterns");
			}
			if (patterns.Length == 0)
			{
				throw new ArgumentException("patterns", "The argument patterns must not be of zero-length");
			}
			switch (format)
			{
			case 'Y':
			case 'y':
				yearMonthPatterns = patterns;
				break;
			case 'M':
			case 'm':
				monthDayPatterns = patterns;
				break;
			case 'D':
				allLongDatePatterns = patterns;
				break;
			case 'd':
				allShortDatePatterns = patterns;
				break;
			case 'T':
				allLongTimePatterns = patterns;
				break;
			case 't':
				allShortTimePatterns = patterns;
				break;
			default:
				throw new ArgumentException("format", "Format specifier is invalid");
			}
		}
	}
}
