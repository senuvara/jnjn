using System.Runtime.InteropServices;

namespace System.Security.Permissions
{
	/// <summary>Specifies the allowed usage of a private virtual file system. This class cannot be inherited.</summary>
	[Serializable]
	[ComVisible(true)]
	public sealed class IsolatedStorageFilePermission : IsolatedStoragePermission, IBuiltInPermission
	{
		private const int version = 1;

		/// <summary>Initializes a new instance of the <see cref="T:System.Security.Permissions.IsolatedStorageFilePermission" /> class with either fully restricted or unrestricted permission as specified.</summary>
		/// <param name="state">One of the <see cref="T:System.Security.Permissions.PermissionState" /> values. </param>
		/// <exception cref="T:System.ArgumentException">The <paramref name="state" /> parameter is not a valid value of <see cref="T:System.Security.Permissions.PermissionState" />. </exception>
		public IsolatedStorageFilePermission(PermissionState state)
			: base(state)
		{
		}

		int IBuiltInPermission.GetTokenIndex()
		{
			return 3;
		}

		/// <summary>Creates and returns an identical copy of the current permission.</summary>
		/// <returns>A copy of the current permission.</returns>
		public override IPermission Copy()
		{
			IsolatedStorageFilePermission isolatedStorageFilePermission = new IsolatedStorageFilePermission(PermissionState.None);
			isolatedStorageFilePermission.m_userQuota = m_userQuota;
			isolatedStorageFilePermission.m_machineQuota = m_machineQuota;
			isolatedStorageFilePermission.m_expirationDays = m_expirationDays;
			isolatedStorageFilePermission.m_permanentData = m_permanentData;
			isolatedStorageFilePermission.m_allowed = m_allowed;
			return isolatedStorageFilePermission;
		}

		/// <summary>Creates and returns a permission that is the intersection of the current permission and the specified permission.</summary>
		/// <returns>A new permission that represents the intersection of the current permission and the specified permission. This new permission is null if the intersection is empty.</returns>
		/// <param name="target">A permission to intersect with the current permission object. It must be of the same type as the current permission. </param>
		/// <exception cref="T:System.ArgumentException">The <paramref name="target" /> parameter is not null and is not of the same type as the current permission. </exception>
		public override IPermission Intersect(IPermission target)
		{
			IsolatedStorageFilePermission isolatedStorageFilePermission = Cast(target);
			if (isolatedStorageFilePermission == null)
			{
				return null;
			}
			if (IsEmpty() && isolatedStorageFilePermission.IsEmpty())
			{
				return null;
			}
			IsolatedStorageFilePermission isolatedStorageFilePermission2 = new IsolatedStorageFilePermission(PermissionState.None);
			isolatedStorageFilePermission2.m_userQuota = ((m_userQuota >= isolatedStorageFilePermission.m_userQuota) ? isolatedStorageFilePermission.m_userQuota : m_userQuota);
			isolatedStorageFilePermission2.m_machineQuota = ((m_machineQuota >= isolatedStorageFilePermission.m_machineQuota) ? isolatedStorageFilePermission.m_machineQuota : m_machineQuota);
			isolatedStorageFilePermission2.m_expirationDays = ((m_expirationDays >= isolatedStorageFilePermission.m_expirationDays) ? isolatedStorageFilePermission.m_expirationDays : m_expirationDays);
			isolatedStorageFilePermission2.m_permanentData = (m_permanentData && isolatedStorageFilePermission.m_permanentData);
			isolatedStorageFilePermission2.UsageAllowed = ((m_allowed >= isolatedStorageFilePermission.m_allowed) ? isolatedStorageFilePermission.m_allowed : m_allowed);
			return isolatedStorageFilePermission2;
		}

		/// <summary>Determines whether the current permission is a subset of the specified permission.</summary>
		/// <returns>true if the current permission is a subset of the specified permission; otherwise, false.</returns>
		/// <param name="target">A permission that is to be tested for the subset relationship. This permission must be of the same type as the current permission. </param>
		/// <exception cref="T:System.ArgumentException">The <paramref name="target" /> parameter is not null and is not of the same type as the current permission. </exception>
		public override bool IsSubsetOf(IPermission target)
		{
			IsolatedStorageFilePermission isolatedStorageFilePermission = Cast(target);
			if (isolatedStorageFilePermission == null)
			{
				return IsEmpty();
			}
			if (isolatedStorageFilePermission.IsUnrestricted())
			{
				return true;
			}
			if (m_userQuota > isolatedStorageFilePermission.m_userQuota)
			{
				return false;
			}
			if (m_machineQuota > isolatedStorageFilePermission.m_machineQuota)
			{
				return false;
			}
			if (m_expirationDays > isolatedStorageFilePermission.m_expirationDays)
			{
				return false;
			}
			if (m_permanentData != isolatedStorageFilePermission.m_permanentData)
			{
				return false;
			}
			if (m_allowed > isolatedStorageFilePermission.m_allowed)
			{
				return false;
			}
			return true;
		}

		/// <summary>Creates a permission that is the union of the current permission and the specified permission.</summary>
		/// <returns>A new permission that represents the union of the current permission and the specified permission.</returns>
		/// <param name="target">A permission to combine with the current permission. It must be of the same type as the current permission. </param>
		/// <exception cref="T:System.ArgumentException">The <paramref name="target" /> parameter is not null and is not of the same type as the current permission. </exception>
		public override IPermission Union(IPermission target)
		{
			IsolatedStorageFilePermission isolatedStorageFilePermission = Cast(target);
			if (isolatedStorageFilePermission == null)
			{
				return Copy();
			}
			IsolatedStorageFilePermission isolatedStorageFilePermission2 = new IsolatedStorageFilePermission(PermissionState.None);
			isolatedStorageFilePermission2.m_userQuota = ((m_userQuota <= isolatedStorageFilePermission.m_userQuota) ? isolatedStorageFilePermission.m_userQuota : m_userQuota);
			isolatedStorageFilePermission2.m_machineQuota = ((m_machineQuota <= isolatedStorageFilePermission.m_machineQuota) ? isolatedStorageFilePermission.m_machineQuota : m_machineQuota);
			isolatedStorageFilePermission2.m_expirationDays = ((m_expirationDays <= isolatedStorageFilePermission.m_expirationDays) ? isolatedStorageFilePermission.m_expirationDays : m_expirationDays);
			isolatedStorageFilePermission2.m_permanentData = (m_permanentData || isolatedStorageFilePermission.m_permanentData);
			isolatedStorageFilePermission2.UsageAllowed = ((m_allowed <= isolatedStorageFilePermission.m_allowed) ? isolatedStorageFilePermission.m_allowed : m_allowed);
			return isolatedStorageFilePermission2;
		}

		/// <summary>Creates an XML encoding of the permission and its current state.</summary>
		/// <returns>An XML encoding of the permission, including any state information.</returns>
		[ComVisible(false)]
		[MonoTODO("(2.0) new override - something must have been added ???")]
		public override SecurityElement ToXml()
		{
			return base.ToXml();
		}

		private IsolatedStorageFilePermission Cast(IPermission target)
		{
			if (target == null)
			{
				return null;
			}
			IsolatedStorageFilePermission isolatedStorageFilePermission = target as IsolatedStorageFilePermission;
			if (isolatedStorageFilePermission == null)
			{
				CodeAccessPermission.ThrowInvalidPermission(target, typeof(IsolatedStorageFilePermission));
			}
			return isolatedStorageFilePermission;
		}
	}
}
