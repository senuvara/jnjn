using System.Runtime.InteropServices;

namespace System.Security.Permissions
{
	/// <summary>Specifies the type of access to files allowed through the File dialog boxes.</summary>
	[Serializable]
	[Flags]
	[ComVisible(true)]
	public enum FileDialogPermissionAccess
	{
		/// <summary>No access to files through the File dialog boxes.</summary>
		None = 0x0,
		/// <summary>Ability to open files through the File dialog boxes.</summary>
		Open = 0x1,
		/// <summary>Ability to save files through the File dialog boxes.</summary>
		Save = 0x2,
		/// <summary>Ability to open and save files through the File dialog boxes.</summary>
		OpenSave = 0x3
	}
}
