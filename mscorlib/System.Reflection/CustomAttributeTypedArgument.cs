using System.Collections.ObjectModel;
using System.Runtime.InteropServices;

namespace System.Reflection
{
	/// <summary>Represents a typed argument of a custom attribute in the inspection context.</summary>
	[Serializable]
	[ComVisible(true)]
	public struct CustomAttributeTypedArgument
	{
		private Type argumentType;

		private object value;

		/// <summary>Gets the type of the typed argument.</summary>
		/// <returns>A <see cref="T:System.Type" /> object representing the type of the typed argument.</returns>
		public Type ArgumentType => argumentType;

		/// <summary>Gets the value of the typed argument.</summary>
		/// <returns>An object that represents the value of the typed argument, or a generic <see cref="T:System.Collections.ObjectModel.ReadOnlyCollection`1" /> of type <see cref="T:System.Reflection.CustomAttributeTypedArgument" /> that represents the values of an array-type argument.</returns>
		public object Value => value;

		internal CustomAttributeTypedArgument(Type argumentType, object value)
		{
			this.argumentType = argumentType;
			this.value = value;
			if (value is Array)
			{
				Array array = (Array)value;
				Type elementType = array.GetType().GetElementType();
				CustomAttributeTypedArgument[] array2 = new CustomAttributeTypedArgument[array.GetLength(0)];
				for (int i = 0; i < array2.Length; i++)
				{
					array2[i] = new CustomAttributeTypedArgument(elementType, array.GetValue(i));
				}
				this.value = new ReadOnlyCollection<CustomAttributeTypedArgument>(array2);
			}
		}

		/// <summary>Returns a string consisting of the argument name, the equal sign, and a string representation of the argument value.</summary>
		/// <returns>A string consisting of the argument name, the equal sign, and a string representation of the argument value.</returns>
		public override string ToString()
		{
			string text = (value == null) ? string.Empty : value.ToString();
			if (argumentType == typeof(string))
			{
				return "\"" + text + "\"";
			}
			if (argumentType == typeof(Type))
			{
				return "typeof (" + text + ")";
			}
			if (argumentType.IsEnum)
			{
				return "(" + argumentType.Name + ")" + text;
			}
			return text;
		}

		/// <summary />
		/// <returns>true if <paramref name="obj" /> and this instance are the same type and represent the same value; otherwise, false.</returns>
		/// <param name="obj">Another object to compare to. </param>
		public override bool Equals(object obj)
		{
			if (!(obj is CustomAttributeTypedArgument))
			{
				return false;
			}
			CustomAttributeTypedArgument customAttributeTypedArgument = (CustomAttributeTypedArgument)obj;
			return (customAttributeTypedArgument.argumentType != argumentType || value == null) ? (customAttributeTypedArgument.value == null) : value.Equals(customAttributeTypedArgument.value);
		}

		/// <summary />
		/// <returns>A 32-bit signed integer that is the hash code for this instance.</returns>
		public override int GetHashCode()
		{
			return (argumentType.GetHashCode() << 16) + ((value != null) ? value.GetHashCode() : 0);
		}

		/// <summary>Tests whether two <see cref="T:System.Reflection.CustomAttributeTypedArgument" /> structures are equivalent.</summary>
		/// <returns>true if the two <see cref="T:System.Reflection.CustomAttributeTypedArgument" /> structures are equal; otherwise, false.</returns>
		/// <param name="left">The <see cref="T:System.Reflection.CustomAttributeTypedArgument" /> structure to the left of the equality operator.</param>
		/// <param name="right">The <see cref="T:System.Reflection.CustomAttributeTypedArgument" /> structure to the right of the equality operator.</param>
		public static bool operator ==(CustomAttributeTypedArgument left, CustomAttributeTypedArgument right)
		{
			return left.Equals(right);
		}

		/// <summary>Tests whether two <see cref="T:System.Reflection.CustomAttributeTypedArgument" /> structures are different.</summary>
		/// <returns>true if the two <see cref="T:System.Reflection.CustomAttributeTypedArgument" /> structures are different; otherwise, false.</returns>
		/// <param name="left">The <see cref="T:System.Reflection.CustomAttributeTypedArgument" /> structure to the left of the inequality operator.</param>
		/// <param name="right">The <see cref="T:System.Reflection.CustomAttributeTypedArgument" /> structure to the right of the inequality operator.</param>
		public static bool operator !=(CustomAttributeTypedArgument left, CustomAttributeTypedArgument right)
		{
			return !left.Equals(right);
		}
	}
}
