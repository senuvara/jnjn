using System.Runtime.InteropServices;

namespace System.Reflection
{
	/// <summary>Defines an assembly title custom attribute for an assembly manifest.</summary>
	[AttributeUsage(AttributeTargets.Assembly, Inherited = false)]
	[ComVisible(true)]
	public sealed class AssemblyTitleAttribute : Attribute
	{
		private string name;

		/// <summary>Gets assembly title information.</summary>
		/// <returns>A string containing the assembly title.</returns>
		public string Title => name;

		/// <summary>Initializes a new instance of the <see cref="T:System.Reflection.AssemblyTitleAttribute" /> class.</summary>
		/// <param name="title">The assembly title. </param>
		public AssemblyTitleAttribute(string title)
		{
			name = title;
		}
	}
}
