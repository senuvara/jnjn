using System.Runtime.InteropServices;

namespace System.Reflection
{
	/// <summary>Specifies the resource location.</summary>
	[Serializable]
	[ComVisible(true)]
	[Flags]
	public enum ResourceLocation
	{
		/// <summary>Specifies an embedded (that is, non-linked) resource.</summary>
		Embedded = 0x1,
		/// <summary>Specifies that the resource is contained in another assembly.</summary>
		ContainedInAnotherAssembly = 0x2,
		/// <summary>Specifies that the resource is contained in the manifest file.</summary>
		ContainedInManifestFile = 0x4
	}
}
