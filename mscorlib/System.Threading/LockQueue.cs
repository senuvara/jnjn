namespace System.Threading
{
	internal class LockQueue
	{
		private ReaderWriterLock rwlock;

		private int lockCount;

		public bool IsEmpty
		{
			get
			{
				//Discarded unreachable code: IL_0017
				lock (this)
				{
					return lockCount == 0;
				}
			}
		}

		public LockQueue(ReaderWriterLock rwlock)
		{
			this.rwlock = rwlock;
		}

		public bool Wait(int timeout)
		{
			//Discarded unreachable code: IL_0032, IL_003e
			bool flag = false;
			try
			{
				lock (this)
				{
					lockCount++;
					Monitor.Exit(rwlock);
					flag = true;
					return Monitor.Wait(this, timeout);
				}
			}
			finally
			{
				if (flag)
				{
					Monitor.Enter(rwlock);
					lockCount--;
				}
			}
		}

		public void Pulse()
		{
			lock (this)
			{
				Monitor.Pulse(this);
			}
		}

		public void PulseAll()
		{
			lock (this)
			{
				Monitor.PulseAll(this);
			}
		}
	}
}
