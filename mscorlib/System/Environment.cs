using Microsoft.Win32;
using System.Collections;
using System.Diagnostics;
using System.IO;
using System.Runtime.CompilerServices;
using System.Runtime.InteropServices;
using System.Security.Permissions;
using System.Text;

namespace System
{
	/// <summary>Provides information about, and means to manipulate, the current environment and platform. This class cannot be inherited.</summary>
	/// <filterpriority>1</filterpriority>
	[ComVisible(true)]
	public static class Environment
	{
		/// <summary>Specifies enumerated constants used to retrieve directory paths to system special folders.</summary>
		[ComVisible(true)]
		public enum SpecialFolder
		{
			/// <summary>The "My Documents" folder.</summary>
			MyDocuments = 5,
			/// <summary>The logical Desktop rather than the physical file system location.</summary>
			Desktop = 0,
			/// <summary>The "My Computer" folder. </summary>
			MyComputer = 17,
			/// <summary>The directory that contains the user's program groups.</summary>
			Programs = 2,
			/// <summary>The directory that serves as a common repository for documents.</summary>
			Personal = 5,
			/// <summary>The directory that serves as a common repository for the user's favorite items.</summary>
			Favorites = 6,
			/// <summary>The directory that corresponds to the user's Startup program group.</summary>
			Startup = 7,
			/// <summary>The directory that contains the user's most recently used documents.</summary>
			Recent = 8,
			/// <summary>The directory that contains the Send To menu items.</summary>
			SendTo = 9,
			/// <summary>The directory that contains the Start menu items.</summary>
			StartMenu = 11,
			/// <summary>The "My Music" folder.</summary>
			MyMusic = 13,
			/// <summary>The directory used to physically store file objects on the desktop.</summary>
			DesktopDirectory = 0x10,
			/// <summary>The directory that serves as a common repository for document templates.</summary>
			Templates = 21,
			/// <summary>The directory that serves as a common repository for application-specific data for the current roaming user.</summary>
			ApplicationData = 26,
			/// <summary>The directory that serves as a common repository for application-specific data that is used by the current, non-roaming user.</summary>
			LocalApplicationData = 28,
			/// <summary>The directory that serves as a common repository for temporary Internet files.</summary>
			InternetCache = 0x20,
			/// <summary>The directory that serves as a common repository for Internet cookies.</summary>
			Cookies = 33,
			/// <summary>The directory that serves as a common repository for Internet history items.</summary>
			History = 34,
			/// <summary>The directory that serves as a common repository for application-specific data that is used by all users.</summary>
			CommonApplicationData = 35,
			/// <summary>The System directory.</summary>
			System = 37,
			/// <summary>The program files directory.</summary>
			ProgramFiles = 38,
			/// <summary>The "My Pictures" folder.</summary>
			MyPictures = 39,
			/// <summary>The directory for components that are shared across applications.</summary>
			CommonProgramFiles = 43
		}

		private const int mono_corlib_version = 82;

		private static OperatingSystem os;

		/// <summary>Gets the command line for this process.</summary>
		/// <returns>A string containing command-line arguments.</returns>
		/// <filterpriority>1</filterpriority>
		/// <PermissionSet>
		///   <IPermission class="System.Security.Permissions.EnvironmentPermission, mscorlib, Version=2.0.3600.0, Culture=neutral, PublicKeyToken=b77a5c561934e089" version="1" Read="Path" />
		/// </PermissionSet>
		public static string CommandLine => string.Join(" ", GetCommandLineArgs());

		/// <summary>Gets or sets the fully qualified path of the current working directory.</summary>
		/// <returns>A string containing a directory path. </returns>
		/// <exception cref="T:System.ArgumentException">Attempted to set to an empty string (""). </exception>
		/// <exception cref="T:System.ArgumentNullException">Attempted to set to null. </exception>
		/// <exception cref="T:System.IO.IOException">An I/O error occurred. </exception>
		/// <exception cref="T:System.IO.DirectoryNotFoundException">Attempted to set a local path that cannot be found. </exception>
		/// <exception cref="T:System.Security.SecurityException">The caller does not have the appropriate permission. </exception>
		/// <filterpriority>1</filterpriority>
		/// <PermissionSet>
		///   <IPermission class="System.Security.Permissions.FileIOPermission, mscorlib, Version=2.0.3600.0, Culture=neutral, PublicKeyToken=b77a5c561934e089" version="1" Unrestricted="true" />
		///   <IPermission class="System.Security.Permissions.SecurityPermission, mscorlib, Version=2.0.3600.0, Culture=neutral, PublicKeyToken=b77a5c561934e089" version="1" Flags="UnmanagedCode" />
		/// </PermissionSet>
		public static string CurrentDirectory
		{
			get
			{
				return Directory.GetCurrentDirectory();
			}
			set
			{
				Directory.SetCurrentDirectory(value);
			}
		}

		/// <summary>Gets or sets the exit code of the process.</summary>
		/// <returns>A 32-bit signed integer containing the exit code. The default value is zero.</returns>
		/// <filterpriority>1</filterpriority>
		public static int ExitCode
		{
			[MethodImpl(MethodImplOptions.InternalCall)]
			get;
			[MethodImpl(MethodImplOptions.InternalCall)]
			set;
		}

		/// <summary>Gets a value indicating whether the common language runtime is shutting down or the current application domain is unloading.</summary>
		/// <returns>true if the common language runtime is shutting down or the current <see cref="T:System.AppDomain" /> is unloading; otherwise, false.The current application domain is the <see cref="T:System.AppDomain" /> that contains the object that is calling <see cref="P:System.Environment.HasShutdownStarted" />.</returns>
		/// <filterpriority>1</filterpriority>
		public static bool HasShutdownStarted
		{
			[MethodImpl(MethodImplOptions.InternalCall)]
			get;
		}

		public static string EmbeddingHostName
		{
			[MethodImpl(MethodImplOptions.InternalCall)]
			get;
		}

		public static bool SocketSecurityEnabled
		{
			[MethodImpl(MethodImplOptions.InternalCall)]
			get;
		}

		public static bool UnityWebSecurityEnabled => SocketSecurityEnabled;

		/// <summary>Gets the NetBIOS name of this local computer.</summary>
		/// <returns>A string containing the name of this computer.</returns>
		/// <exception cref="T:System.InvalidOperationException">The name of this computer cannot be obtained. </exception>
		/// <filterpriority>1</filterpriority>
		/// <PermissionSet>
		///   <IPermission class="System.Security.Permissions.EnvironmentPermission, mscorlib, Version=2.0.3600.0, Culture=neutral, PublicKeyToken=b77a5c561934e089" version="1" Read="COMPUTERNAME" />
		///   <IPermission class="System.Security.Permissions.SecurityPermission, mscorlib, Version=2.0.3600.0, Culture=neutral, PublicKeyToken=b77a5c561934e089" version="1" Flags="UnmanagedCode" />
		/// </PermissionSet>
		public static string MachineName
		{
			[MethodImpl(MethodImplOptions.InternalCall)]
			get;
		}

		/// <summary>Gets the newline string defined for this environment.</summary>
		/// <returns>A string containing "\r\n" for non-Unix platforms,  or a string containing "\n" for Unix platforms.</returns>
		/// <filterpriority>1</filterpriority>
		public static string NewLine
		{
			[MethodImpl(MethodImplOptions.InternalCall)]
			get;
		}

		internal static PlatformID Platform
		{
			[MethodImpl(MethodImplOptions.InternalCall)]
			get;
		}

		/// <summary>Gets an <see cref="T:System.OperatingSystem" /> object that contains the current platform identifier and version number.</summary>
		/// <returns>An <see cref="T:System.OperatingSystem" /> object.</returns>
		/// <exception cref="T:System.InvalidOperationException">This property was unable to obtain the system version.-or- The obtained platform identifier is not a member of <see cref="T:System.PlatformID" />. </exception>
		/// <filterpriority>1</filterpriority>
		public static OperatingSystem OSVersion
		{
			get
			{
				if (os == null)
				{
					Version version = Version.CreateFromString(GetOSVersionString());
					PlatformID platform = Platform;
					os = new OperatingSystem(platform, version);
				}
				return os;
			}
		}

		/// <summary>Gets current stack trace information.</summary>
		/// <returns>A string containing stack trace information. This value can be <see cref="F:System.String.Empty" />.</returns>
		/// <exception cref="T:System.ArgumentOutOfRangeException">The requested stack trace information is out of range. </exception>
		/// <filterpriority>1</filterpriority>
		/// <PermissionSet>
		///   <IPermission class="System.Security.Permissions.EnvironmentPermission, mscorlib, Version=2.0.3600.0, Culture=neutral, PublicKeyToken=b77a5c561934e089" version="1" Unrestricted="true" />
		///   <IPermission class="System.Security.Permissions.FileIOPermission, mscorlib, Version=2.0.3600.0, Culture=neutral, PublicKeyToken=b77a5c561934e089" version="1" PathDiscovery="*AllFiles*" />
		/// </PermissionSet>
		public static string StackTrace
		{
			get
			{
				StackTrace stackTrace = new StackTrace(0, fNeedFileInfo: true);
				return stackTrace.ToString();
			}
		}

		/// <summary>Gets the number of milliseconds elapsed since the system started.</summary>
		/// <returns>A 32-bit signed integer containing the amount of time in milliseconds that has passed since the last time the computer was started.</returns>
		/// <filterpriority>1</filterpriority>
		public static int TickCount
		{
			[MethodImpl(MethodImplOptions.InternalCall)]
			get;
		}

		/// <summary>Gets the network domain name associated with the current user.</summary>
		/// <returns>The network domain name associated with the current user.</returns>
		/// <exception cref="T:System.PlatformNotSupportedException">The operating system does not support retrieving the network domain name. </exception>
		/// <exception cref="T:System.InvalidOperationException">The network domain name cannot be retrieved. </exception>
		/// <filterpriority>1</filterpriority>
		/// <PermissionSet>
		///   <IPermission class="System.Security.Permissions.EnvironmentPermission, mscorlib, Version=2.0.3600.0, Culture=neutral, PublicKeyToken=b77a5c561934e089" version="1" Read="UserName;UserDomainName" />
		/// </PermissionSet>
		public static string UserDomainName => MachineName;

		/// <summary>Gets a value indicating whether the current process is running in user interactive mode.</summary>
		/// <returns>true if the current process is running in user interactive mode; otherwise, false.</returns>
		/// <filterpriority>1</filterpriority>
		[MonoTODO("Currently always returns false, regardless of interactive state")]
		public static bool UserInteractive => false;

		/// <summary>Gets the user name of the person who is currently logged on to the Windows operating system.</summary>
		/// <returns>The user name of the person who is logged on to Windows.</returns>
		/// <filterpriority>1</filterpriority>
		/// <PermissionSet>
		///   <IPermission class="System.Security.Permissions.EnvironmentPermission, mscorlib, Version=2.0.3600.0, Culture=neutral, PublicKeyToken=b77a5c561934e089" version="1" Read="UserName" />
		/// </PermissionSet>
		public static string UserName
		{
			[MethodImpl(MethodImplOptions.InternalCall)]
			get;
		}

		/// <summary>Gets a <see cref="T:System.Version" /> object that describes the major, minor, build, and revision numbers of the common language runtime.</summary>
		/// <returns>A <see cref="T:System.Version" /> object.</returns>
		/// <filterpriority>1</filterpriority>
		public static Version Version => new Version("2.0.50727.1433");

		/// <summary>Gets the amount of physical memory mapped to the process context.</summary>
		/// <returns>A 64-bit signed integer containing the number of bytes of physical memory mapped to the process context.</returns>
		/// <filterpriority>1</filterpriority>
		/// <PermissionSet>
		///   <IPermission class="System.Security.Permissions.EnvironmentPermission, mscorlib, Version=2.0.3600.0, Culture=neutral, PublicKeyToken=b77a5c561934e089" version="1" Unrestricted="true" />
		/// </PermissionSet>
		[MonoTODO("Currently always returns zero")]
		public static long WorkingSet => 0L;

		/// <summary>Gets the number of processors on the current machine.</summary>
		/// <returns>The 32-bit signed integer that specifies the number of processors on the current machine. There is no default.</returns>
		/// <filterpriority>1</filterpriority>
		/// <PermissionSet>
		///   <IPermission class="System.Security.Permissions.EnvironmentPermission, mscorlib, Version=2.0.3600.0, Culture=neutral, PublicKeyToken=b77a5c561934e089" version="1" Read="NUMBER_OF_PROCESSORS" />
		/// </PermissionSet>
		public static int ProcessorCount
		{
			[MethodImpl(MethodImplOptions.InternalCall)]
			get;
		}

		internal static bool IsRunningOnWindows => Platform < PlatformID.Unix;

		[MethodImpl(MethodImplOptions.InternalCall)]
		internal static extern string GetOSVersionString();

		/// <summary>Terminates this process and gives the underlying operating system the specified exit code.</summary>
		/// <param name="exitCode">Exit code to be given to the operating system. </param>
		/// <exception cref="T:System.Security.SecurityException">The caller does not have sufficient security permission to perform this function. </exception>
		/// <filterpriority>1</filterpriority>
		/// <PermissionSet>
		///   <IPermission class="System.Security.Permissions.SecurityPermission, mscorlib, Version=2.0.3600.0, Culture=neutral, PublicKeyToken=b77a5c561934e089" version="1" Flags="UnmanagedCode" />
		/// </PermissionSet>
		[MethodImpl(MethodImplOptions.InternalCall)]
		public static extern void Exit(int exitCode);

		/// <summary>Replaces the name of each environment variable embedded in the specified string with the string equivalent of the value of the variable, then returns the resulting string.</summary>
		/// <returns>A string with each environment variable replaced by its value.</returns>
		/// <param name="name">A string containing the names of zero or more environment variables. Each environment variable is quoted with the percent sign character (%). </param>
		/// <exception cref="T:System.ArgumentNullException">
		///   <paramref name="name" /> is null. </exception>
		/// <filterpriority>1</filterpriority>
		/// <PermissionSet>
		///   <IPermission class="System.Security.Permissions.EnvironmentPermission, mscorlib, Version=2.0.3600.0, Culture=neutral, PublicKeyToken=b77a5c561934e089" version="1" Unrestricted="true" />
		/// </PermissionSet>
		public static string ExpandEnvironmentVariables(string name)
		{
			if (name == null)
			{
				throw new ArgumentNullException("name");
			}
			int num = name.IndexOf('%');
			if (num == -1)
			{
				return name;
			}
			int length = name.Length;
			int num2 = 0;
			if (num == length - 1 || (num2 = name.IndexOf('%', num + 1)) == -1)
			{
				return name;
			}
			StringBuilder stringBuilder = new StringBuilder();
			stringBuilder.Append(name, 0, num);
			Hashtable hashtable = null;
			do
			{
				string text = name.Substring(num + 1, num2 - num - 1);
				string text2 = GetEnvironmentVariable(text);
				if (text2 == null && IsRunningOnWindows)
				{
					if (hashtable == null)
					{
						hashtable = GetEnvironmentVariablesNoCase();
					}
					text2 = (hashtable[text] as string);
				}
				if (text2 == null)
				{
					stringBuilder.Append('%');
					stringBuilder.Append(text);
					num2--;
				}
				else
				{
					stringBuilder.Append(text2);
				}
				int num3 = num2;
				num = name.IndexOf('%', num2 + 1);
				num2 = ((num != -1 && num2 <= length - 1) ? name.IndexOf('%', num + 1) : (-1));
				int count = (num == -1 || num2 == -1) ? (length - num3 - 1) : ((text2 == null) ? (num - num3) : (num - num3 - 1));
				if (num >= num3 || num == -1)
				{
					stringBuilder.Append(name, num3 + 1, count);
				}
			}
			while (num2 > -1 && num2 < length);
			return stringBuilder.ToString();
		}

		/// <summary>Returns a string array containing the command-line arguments for the current process.</summary>
		/// <returns>An array of string where each element contains a command-line argument. The first element is the executable file name, and the following zero or more elements contain the remaining command-line arguments.</returns>
		/// <exception cref="T:System.NotSupportedException">The system does not support command-line arguments. </exception>
		/// <filterpriority>1</filterpriority>
		/// <PermissionSet>
		///   <IPermission class="System.Security.Permissions.EnvironmentPermission, mscorlib, Version=2.0.3600.0, Culture=neutral, PublicKeyToken=b77a5c561934e089" version="1" Read="Path" />
		/// </PermissionSet>
		[MethodImpl(MethodImplOptions.InternalCall)]
		public static extern string[] GetCommandLineArgs();

		[MethodImpl(MethodImplOptions.InternalCall)]
		internal static extern string internalGetEnvironmentVariable(string variable);

		/// <summary>Retrieves the value of an environment variable from the current process.</summary>
		/// <returns>The value of the environment variable specified by <paramref name="variable" />, or null if the environment variable is not found.</returns>
		/// <param name="variable">The name of the environment variable. </param>
		/// <exception cref="T:System.ArgumentNullException">
		///   <paramref name="variable" /> is null. </exception>
		/// <exception cref="T:System.Security.SecurityException">The caller does not have the required permission to perform this operation.</exception>
		/// <filterpriority>1</filterpriority>
		/// <PermissionSet>
		///   <IPermission class="System.Security.Permissions.EnvironmentPermission, mscorlib, Version=2.0.3600.0, Culture=neutral, PublicKeyToken=b77a5c561934e089" version="1" Unrestricted="true" />
		/// </PermissionSet>
		public static string GetEnvironmentVariable(string variable)
		{
			return internalGetEnvironmentVariable(variable);
		}

		private static Hashtable GetEnvironmentVariablesNoCase()
		{
			Hashtable hashtable = new Hashtable(CaseInsensitiveHashCodeProvider.Default, CaseInsensitiveComparer.Default);
			string[] environmentVariableNames = GetEnvironmentVariableNames();
			foreach (string text in environmentVariableNames)
			{
				hashtable[text] = internalGetEnvironmentVariable(text);
			}
			return hashtable;
		}

		/// <summary>Retrieves all environment variable names and their values from the current process.</summary>
		/// <returns>An <see cref="T:System.Collections.IDictionary" /> that contains all environment variable names and their values; otherwise, an empty dictionary if no environment variables are found.</returns>
		/// <exception cref="T:System.Security.SecurityException">The caller does not have the required permission to perform this operation.</exception>
		/// <exception cref="T:System.OutOfMemoryException">The buffer is out of memory.</exception>
		/// <filterpriority>1</filterpriority>
		/// <PermissionSet>
		///   <IPermission class="System.Security.Permissions.EnvironmentPermission, mscorlib, Version=2.0.3600.0, Culture=neutral, PublicKeyToken=b77a5c561934e089" version="1" Unrestricted="true" />
		/// </PermissionSet>
		public static IDictionary GetEnvironmentVariables()
		{
			Hashtable hashtable = new Hashtable();
			string[] environmentVariableNames = GetEnvironmentVariableNames();
			foreach (string text in environmentVariableNames)
			{
				hashtable[text] = internalGetEnvironmentVariable(text);
			}
			return hashtable;
		}

		[MethodImpl(MethodImplOptions.InternalCall)]
		private static extern string GetWindowsFolderPath(int folder);

		/// <summary>Gets the path to the system special folder identified by the specified enumeration.</summary>
		/// <returns>The path to the specified system special folder, if that folder physically exists on your computer; otherwise, the empty string ("").A folder will not physically exist if the operating system did not create it, the existing folder was deleted, or the folder is a virtual directory, such as My Computer, which does not correspond to a physical path.</returns>
		/// <param name="folder">An enumerated constant that identifies a system special folder. </param>
		/// <exception cref="T:System.ArgumentException">
		///   <paramref name="folder" /> is not a member of <see cref="T:System.Environment.SpecialFolder" />. </exception>
		/// <filterpriority>1</filterpriority>
		/// <PermissionSet>
		///   <IPermission class="System.Security.Permissions.FileIOPermission, mscorlib, Version=2.0.3600.0, Culture=neutral, PublicKeyToken=b77a5c561934e089" version="1" Unrestricted="true" />
		/// </PermissionSet>
		public static string GetFolderPath(SpecialFolder folder)
		{
			string text = null;
			if (IsRunningOnWindows)
			{
				return GetWindowsFolderPath((int)folder);
			}
			return InternalGetFolderPath(folder);
		}

		private static string ReadXdgUserDir(string config_dir, string home_dir, string key, string fallback)
		{
			string text = internalGetEnvironmentVariable(key);
			if (text != null && text != string.Empty)
			{
				return text;
			}
			string path = Path.Combine(config_dir, "user-dirs.dirs");
			if (!File.Exists(path))
			{
				return Path.Combine(home_dir, fallback);
			}
			try
			{
				using (StreamReader streamReader = new StreamReader(path))
				{
					string text2;
					while ((text2 = streamReader.ReadLine()) != null)
					{
						text2 = text2.Trim();
						int num = text2.IndexOf('=');
						if (num > 8 && text2.Substring(0, num) == key)
						{
							string text3 = text2.Substring(num + 1).Trim('"');
							bool flag = false;
							if (text3.StartsWith("$HOME/"))
							{
								flag = true;
								text3 = text3.Substring(6);
							}
							else if (!text3.StartsWith("/"))
							{
								flag = true;
							}
							return (!flag) ? text3 : Path.Combine(home_dir, text3);
						}
					}
				}
			}
			catch (FileNotFoundException)
			{
			}
			return Path.Combine(home_dir, fallback);
		}

		internal static string InternalGetFolderPath(SpecialFolder folder)
		{
			string text = internalGetHome();
			string text2 = internalGetEnvironmentVariable("XDG_DATA_HOME");
			if (text2 == null || text2 == string.Empty)
			{
				text2 = Path.Combine(text, ".local");
				text2 = Path.Combine(text2, "share");
			}
			string text3 = internalGetEnvironmentVariable("XDG_CONFIG_HOME");
			if (text3 == null || text3 == string.Empty)
			{
				text3 = Path.Combine(text, ".config");
			}
			switch (folder)
			{
			case SpecialFolder.MyComputer:
				return string.Empty;
			case SpecialFolder.MyDocuments:
				return text;
			case SpecialFolder.ApplicationData:
				return text3;
			case SpecialFolder.LocalApplicationData:
				return text2;
			case SpecialFolder.Desktop:
			case SpecialFolder.DesktopDirectory:
				return ReadXdgUserDir(text3, text, "XDG_DESKTOP_DIR", "Desktop");
			case SpecialFolder.MyMusic:
				return ReadXdgUserDir(text3, text, "XDG_MUSIC_DIR", "Music");
			case SpecialFolder.MyPictures:
				return ReadXdgUserDir(text3, text, "XDG_PICTURES_DIR", "Pictures");
			case SpecialFolder.Programs:
			case SpecialFolder.Favorites:
			case SpecialFolder.Startup:
			case SpecialFolder.Recent:
			case SpecialFolder.SendTo:
			case SpecialFolder.StartMenu:
			case SpecialFolder.Templates:
			case SpecialFolder.InternetCache:
			case SpecialFolder.Cookies:
			case SpecialFolder.History:
			case SpecialFolder.System:
			case SpecialFolder.ProgramFiles:
			case SpecialFolder.CommonProgramFiles:
				return string.Empty;
			case SpecialFolder.CommonApplicationData:
				return "/usr/share";
			default:
				throw new ArgumentException("Invalid SpecialFolder");
			}
		}

		/// <summary>Returns an array of string containing the names of the logical drives on the current computer.</summary>
		/// <returns>An array of string where each element contains the name of a logical drive. For example, if the computer's hard drive is the first logical drive, the first element returned is "C:\".</returns>
		/// <exception cref="T:System.IO.IOException">An I/O error occurs. </exception>
		/// <exception cref="T:System.Security.SecurityException">The caller does not have the required permissions. </exception>
		/// <filterpriority>1</filterpriority>
		/// <PermissionSet>
		///   <IPermission class="System.Security.Permissions.EnvironmentPermission, mscorlib, Version=2.0.3600.0, Culture=neutral, PublicKeyToken=b77a5c561934e089" version="1" Unrestricted="true" />
		/// </PermissionSet>
		public static string[] GetLogicalDrives()
		{
			return GetLogicalDrivesInternal();
		}

		[MethodImpl(MethodImplOptions.InternalCall)]
		private static extern void internalBroadcastSettingChange();

		/// <summary>Retrieves the value of an environment variable from the current process or from the Windows operating system registry key for the current user or local machine.</summary>
		/// <returns>The value of the environment variable specified by the <paramref name="variable" /> and <paramref name="target" /> parameters, or null if the environment variable is not found.</returns>
		/// <param name="variable">The name of an environment variable.</param>
		/// <param name="target">One of the <see cref="T:System.EnvironmentVariableTarget" /> values.</param>
		/// <exception cref="T:System.ArgumentNullException">
		///   <paramref name="variable" /> is null.</exception>
		/// <exception cref="T:System.NotSupportedException">
		///   <paramref name="target" /> is <see cref="F:System.EnvironmentVariableTarget.User" /> or <see cref="F:System.EnvironmentVariableTarget.Machine" /> and the current operating system is Windows 95, Windows 98, or Windows Me.</exception>
		/// <exception cref="T:System.ArgumentException">
		///   <paramref name="target" /> is not a valid <see cref="T:System.EnvironmentVariableTarget" /> value.</exception>
		/// <exception cref="T:System.Security.SecurityException">The caller does not have the required permission to perform this operation.</exception>
		/// <filterpriority>1</filterpriority>
		/// <PermissionSet>
		///   <IPermission class="System.Security.Permissions.EnvironmentPermission, mscorlib, Version=2.0.3600.0, Culture=neutral, PublicKeyToken=b77a5c561934e089" version="1" Unrestricted="true" />
		///   <IPermission class="System.Security.Permissions.RegistryPermission, mscorlib, Version=2.0.3600.0, Culture=neutral, PublicKeyToken=b77a5c561934e089" version="1" Unrestricted="true" />
		///   <IPermission class="System.Security.Permissions.SecurityPermission, mscorlib, Version=2.0.3600.0, Culture=neutral, PublicKeyToken=b77a5c561934e089" version="1" Flags="UnmanagedCode" />
		/// </PermissionSet>
		public static string GetEnvironmentVariable(string variable, EnvironmentVariableTarget target)
		{
			//Discarded unreachable code: IL_006a, IL_00c5
			switch (target)
			{
			case EnvironmentVariableTarget.Process:
				return GetEnvironmentVariable(variable);
			case EnvironmentVariableTarget.Machine:
			{
				new EnvironmentPermission(PermissionState.Unrestricted).Demand();
				if (!IsRunningOnWindows)
				{
					return null;
				}
				using (RegistryKey registryKey2 = Registry.LocalMachine.OpenSubKey("SYSTEM\\CurrentControlSet\\Control\\Session Manager\\Environment"))
				{
					return registryKey2.GetValue(variable)?.ToString();
				}
			}
			case EnvironmentVariableTarget.User:
			{
				new EnvironmentPermission(PermissionState.Unrestricted).Demand();
				if (!IsRunningOnWindows)
				{
					return null;
				}
				using (RegistryKey registryKey = Registry.CurrentUser.OpenSubKey("Environment", writable: false))
				{
					return registryKey.GetValue(variable)?.ToString();
				}
			}
			default:
				throw new ArgumentException("target");
			}
		}

		/// <summary>Retrieves all environment variable names and their values from the current process, or from the Windows operating system registry key for the current user or local machine.</summary>
		/// <returns>An <see cref="T:System.Collections.IDictionary" /> object that contains all environment variable names and their values from the source specified by the <paramref name="target" /> parameter; otherwise, an empty dictionary if no environment variables are found.</returns>
		/// <param name="target">One of the <see cref="T:System.EnvironmentVariableTarget" /> values.</param>
		/// <exception cref="T:System.Security.SecurityException">The caller does not have the required permission to perform this operation for the specified value of <paramref name="target" />.</exception>
		/// <exception cref="T:System.NotSupportedException">This method cannot be used on Windows 95 or Windows 98 platforms.</exception>
		/// <exception cref="T:System.ArgumentException">
		///   <paramref name="target" /> contains an illegal value.</exception>
		/// <filterpriority>1</filterpriority>
		/// <PermissionSet>
		///   <IPermission class="System.Security.Permissions.EnvironmentPermission, mscorlib, Version=2.0.3600.0, Culture=neutral, PublicKeyToken=b77a5c561934e089" version="1" Unrestricted="true" />
		///   <IPermission class="System.Security.Permissions.RegistryPermission, mscorlib, Version=2.0.3600.0, Culture=neutral, PublicKeyToken=b77a5c561934e089" version="1" Unrestricted="true" />
		///   <IPermission class="System.Security.Permissions.SecurityPermission, mscorlib, Version=2.0.3600.0, Culture=neutral, PublicKeyToken=b77a5c561934e089" version="1" Flags="UnmanagedCode" />
		/// </PermissionSet>
		public static IDictionary GetEnvironmentVariables(EnvironmentVariableTarget target)
		{
			IDictionary dictionary = new Hashtable();
			switch (target)
			{
			case EnvironmentVariableTarget.Process:
				dictionary = GetEnvironmentVariables();
				break;
			case EnvironmentVariableTarget.Machine:
				new EnvironmentPermission(PermissionState.Unrestricted).Demand();
				if (IsRunningOnWindows)
				{
					using (RegistryKey registryKey2 = Registry.LocalMachine.OpenSubKey("SYSTEM\\CurrentControlSet\\Control\\Session Manager\\Environment"))
					{
						string[] valueNames2 = registryKey2.GetValueNames();
						string[] array2 = valueNames2;
						foreach (string text2 in array2)
						{
							dictionary.Add(text2, registryKey2.GetValue(text2));
						}
						return dictionary;
					}
				}
				break;
			case EnvironmentVariableTarget.User:
				new EnvironmentPermission(PermissionState.Unrestricted).Demand();
				if (IsRunningOnWindows)
				{
					using (RegistryKey registryKey = Registry.CurrentUser.OpenSubKey("Environment"))
					{
						string[] valueNames = registryKey.GetValueNames();
						string[] array = valueNames;
						foreach (string text in array)
						{
							dictionary.Add(text, registryKey.GetValue(text));
						}
						return dictionary;
					}
				}
				break;
			default:
				throw new ArgumentException("target");
			}
			return dictionary;
		}

		/// <summary>Creates, modifies, or deletes an environment variable stored in the current process.</summary>
		/// <param name="variable">The name of an environment variable. </param>
		/// <param name="value">A value to assign to <paramref name="variable" />. </param>
		/// <exception cref="T:System.ArgumentNullException">
		///   <paramref name="variable" /> is null.</exception>
		/// <exception cref="T:System.ArgumentException">
		///   <paramref name="variable" /> contains a zero-length string, an initial hexadecimal zero character (0x00), or an equal sign ("="). -or-The length of <paramref name="variable" /> or <paramref name="value" /> is greater than or equal to 32,767 characters.-or-An error occurred during the execution of this operation.</exception>
		/// <exception cref="T:System.Security.SecurityException">The caller does not have the required permission to perform this operation. </exception>
		/// <filterpriority>1</filterpriority>
		/// <PermissionSet>
		///   <IPermission class="System.Security.Permissions.EnvironmentPermission, mscorlib, Version=2.0.3600.0, Culture=neutral, PublicKeyToken=b77a5c561934e089" version="1" Unrestricted="true" />
		/// </PermissionSet>
		public static void SetEnvironmentVariable(string variable, string value)
		{
			SetEnvironmentVariable(variable, value, EnvironmentVariableTarget.Process);
		}

		/// <summary>Creates, modifies, or deletes an environment variable stored in the current process or in the Windows operating system registry key reserved for the current user or local machine.</summary>
		/// <param name="variable">The name of an environment variable.</param>
		/// <param name="value">A value to assign to <paramref name="variable" />. </param>
		/// <param name="target">One of the <see cref="T:System.EnvironmentVariableTarget" /> values.</param>
		/// <exception cref="T:System.ArgumentNullException">
		///   <paramref name="variable" /> is null. </exception>
		/// <exception cref="T:System.ArgumentException">
		///   <paramref name="variable" /> contains a zero-length string, an initial hexadecimal zero character (0x00), or an equal sign ("="). -or-The length of <paramref name="variable" /> is greater than or equal to 32,767 characters.-or-<paramref name="target" /> is not a member of the <see cref="T:System.EnvironmentVariableTarget" /> enumeration. -or-<paramref name="target" /> is <see cref="F:System.EnvironmentVariableTarget.Machine" /> or <see cref="F:System.EnvironmentVariableTarget.User" /> and the length of <paramref name="variable" /> is greater than or equal to 255.-or-<paramref name="target" /> is <see cref="F:System.EnvironmentVariableTarget.Process" /> and the length of <paramref name="value" /> is greater than or equal to 32,767 characters. -or-An error occurred during the execution of this operation.</exception>
		/// <exception cref="T:System.NotSupportedException">
		///   <paramref name="target" /> is <see cref="F:System.EnvironmentVariableTarget.User" /> or <see cref="F:System.EnvironmentVariableTarget.Machine" /> and the current operating system is Windows 95, Windows 98, or Windows Me.</exception>
		/// <exception cref="T:System.Security.SecurityException">The caller does not have the required permission to perform this operation. </exception>
		/// <filterpriority>1</filterpriority>
		/// <PermissionSet>
		///   <IPermission class="System.Security.Permissions.EnvironmentPermission, mscorlib, Version=2.0.3600.0, Culture=neutral, PublicKeyToken=b77a5c561934e089" version="1" Unrestricted="true" />
		///   <IPermission class="System.Security.Permissions.RegistryPermission, mscorlib, Version=2.0.3600.0, Culture=neutral, PublicKeyToken=b77a5c561934e089" version="1" Unrestricted="true" />
		///   <IPermission class="System.Security.Permissions.SecurityPermission, mscorlib, Version=2.0.3600.0, Culture=neutral, PublicKeyToken=b77a5c561934e089" version="1" Flags="UnmanagedCode" />
		/// </PermissionSet>
		public static void SetEnvironmentVariable(string variable, string value, EnvironmentVariableTarget target)
		{
			if (variable == null)
			{
				throw new ArgumentNullException("variable");
			}
			if (variable == string.Empty)
			{
				throw new ArgumentException("String cannot be of zero length.", "variable");
			}
			if (variable.IndexOf('=') != -1)
			{
				throw new ArgumentException("Environment variable name cannot contain an equal character.", "variable");
			}
			if (variable[0] == '\0')
			{
				throw new ArgumentException("The first char in the string is the null character.", "variable");
			}
			switch (target)
			{
			case EnvironmentVariableTarget.Process:
				InternalSetEnvironmentVariable(variable, value);
				break;
			case EnvironmentVariableTarget.Machine:
				if (IsRunningOnWindows)
				{
					using (RegistryKey registryKey2 = Registry.LocalMachine.OpenSubKey("SYSTEM\\CurrentControlSet\\Control\\Session Manager\\Environment", writable: true))
					{
						if (string.IsNullOrEmpty(value))
						{
							registryKey2.DeleteValue(variable, throwOnMissingValue: false);
						}
						else
						{
							registryKey2.SetValue(variable, value);
						}
						internalBroadcastSettingChange();
					}
				}
				break;
			case EnvironmentVariableTarget.User:
				if (IsRunningOnWindows)
				{
					using (RegistryKey registryKey = Registry.CurrentUser.OpenSubKey("Environment", writable: true))
					{
						if (string.IsNullOrEmpty(value))
						{
							registryKey.DeleteValue(variable, throwOnMissingValue: false);
						}
						else
						{
							registryKey.SetValue(variable, value);
						}
						internalBroadcastSettingChange();
					}
				}
				break;
			default:
				throw new ArgumentException("target");
			}
		}

		[MethodImpl(MethodImplOptions.InternalCall)]
		internal static extern void InternalSetEnvironmentVariable(string variable, string value);

		/// <summary>Terminates a process but does not execute any active try-finally blocks or finalizers.</summary>
		/// <param name="message">A message that explains why the process was terminated, or null if no explanation is provided. </param>
		[MonoTODO("Not implemented")]
		public static void FailFast(string message)
		{
			throw new NotImplementedException();
		}

		[MethodImpl(MethodImplOptions.InternalCall)]
		private static extern string[] GetLogicalDrivesInternal();

		[MethodImpl(MethodImplOptions.InternalCall)]
		private static extern string[] GetEnvironmentVariableNames();

		[MethodImpl(MethodImplOptions.InternalCall)]
		internal static extern string GetMachineConfigPath();

		[MethodImpl(MethodImplOptions.InternalCall)]
		internal static extern string internalGetHome();
	}
}
