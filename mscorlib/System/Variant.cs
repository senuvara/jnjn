using System.Runtime.InteropServices;

namespace System
{
	[StructLayout(LayoutKind.Explicit)]
	internal struct Variant
	{
		[FieldOffset(0)]
		public short vt;

		[FieldOffset(2)]
		public ushort wReserved1;

		[FieldOffset(4)]
		public ushort wReserved2;

		[FieldOffset(6)]
		public ushort wReserved3;

		[FieldOffset(8)]
		public long llVal;

		[FieldOffset(8)]
		public int lVal;

		[FieldOffset(8)]
		public byte bVal;

		[FieldOffset(8)]
		public short iVal;

		[FieldOffset(8)]
		public float fltVal;

		[FieldOffset(8)]
		public double dblVal;

		[FieldOffset(8)]
		public short boolVal;

		[FieldOffset(8)]
		public IntPtr bstrVal;

		[FieldOffset(8)]
		public sbyte cVal;

		[FieldOffset(8)]
		public ushort uiVal;

		[FieldOffset(8)]
		public uint ulVal;

		[FieldOffset(8)]
		public ulong ullVal;

		[FieldOffset(8)]
		public int intVal;

		[FieldOffset(8)]
		public uint uintVal;

		[FieldOffset(8)]
		public IntPtr pdispVal;

		[FieldOffset(8)]
		public BRECORD bRecord;

		public void SetValue(object obj)
		{
			//Discarded unreachable code: IL_02d0, IL_030c
			vt = 0;
			if (obj != null)
			{
				Type type = obj.GetType();
				if (type.IsEnum)
				{
					type = Enum.GetUnderlyingType(type);
				}
				if (type == typeof(sbyte))
				{
					vt = 16;
					cVal = (sbyte)obj;
				}
				else if (type == typeof(byte))
				{
					vt = 17;
					bVal = (byte)obj;
				}
				else if (type == typeof(short))
				{
					vt = 2;
					iVal = (short)obj;
				}
				else if (type == typeof(ushort))
				{
					vt = 18;
					uiVal = (ushort)obj;
				}
				else if (type == typeof(int))
				{
					vt = 3;
					lVal = (int)obj;
				}
				else if (type == typeof(uint))
				{
					vt = 19;
					ulVal = (uint)obj;
				}
				else if (type == typeof(long))
				{
					vt = 20;
					llVal = (long)obj;
				}
				else if (type == typeof(ulong))
				{
					vt = 21;
					ullVal = (ulong)obj;
				}
				else if (type == typeof(float))
				{
					vt = 4;
					fltVal = (float)obj;
				}
				else if (type == typeof(double))
				{
					vt = 5;
					dblVal = (double)obj;
				}
				else if (type == typeof(string))
				{
					vt = 8;
					bstrVal = Marshal.StringToBSTR((string)obj);
				}
				else if (type == typeof(bool))
				{
					vt = 11;
					lVal = (((bool)obj) ? (-1) : 0);
				}
				else if (type == typeof(BStrWrapper))
				{
					vt = 8;
					bstrVal = Marshal.StringToBSTR(((BStrWrapper)obj).WrappedObject);
				}
				else if (type == typeof(UnknownWrapper))
				{
					vt = 13;
					pdispVal = Marshal.GetIUnknownForObject(((UnknownWrapper)obj).WrappedObject);
				}
				else if (type == typeof(DispatchWrapper))
				{
					vt = 9;
					pdispVal = Marshal.GetIDispatchForObject(((DispatchWrapper)obj).WrappedObject);
				}
				else
				{
					try
					{
						pdispVal = Marshal.GetIDispatchForObject(obj);
						vt = 9;
						return;
					}
					catch
					{
					}
					try
					{
						vt = 13;
						pdispVal = Marshal.GetIUnknownForObject(obj);
					}
					catch (Exception inner)
					{
						throw new NotImplementedException($"Variant couldn't handle object of type {obj.GetType()}", inner);
					}
				}
			}
		}

		public object GetValue()
		{
			object result = null;
			switch (vt)
			{
			case 16:
				result = cVal;
				break;
			case 17:
				result = bVal;
				break;
			case 2:
				result = iVal;
				break;
			case 18:
				result = uiVal;
				break;
			case 3:
				result = lVal;
				break;
			case 19:
				result = ulVal;
				break;
			case 20:
				result = llVal;
				break;
			case 21:
				result = ullVal;
				break;
			case 4:
				result = fltVal;
				break;
			case 5:
				result = dblVal;
				break;
			case 11:
				result = (boolVal != 0);
				break;
			case 8:
				result = Marshal.PtrToStringBSTR(bstrVal);
				break;
			case 9:
			case 13:
				if (pdispVal != IntPtr.Zero)
				{
					result = Marshal.GetObjectForIUnknown(pdispVal);
				}
				break;
			}
			return result;
		}

		public void Clear()
		{
			if (vt == 8)
			{
				Marshal.FreeBSTR(bstrVal);
			}
			else if ((vt == 9 || vt == 13) && pdispVal != IntPtr.Zero)
			{
				Marshal.Release(pdispVal);
			}
		}
	}
}
