using System.Globalization;
using System.Runtime.CompilerServices;
using System.Runtime.InteropServices;
using System.Security.Cryptography;
using System.Text;

namespace System
{
	/// <summary>Converts a base data type to another base data type.</summary>
	/// <filterpriority>1</filterpriority>
	public static class Convert
	{
		private const int MaxBytesPerLine = 57;

		/// <summary>A constant representing a database column absent of data; that is, database null.</summary>
		/// <filterpriority>1</filterpriority>
		public static readonly object DBNull = System.DBNull.Value;

		private static readonly Type[] conversionTable = new Type[19]
		{
			null,
			typeof(object),
			typeof(DBNull),
			typeof(bool),
			typeof(char),
			typeof(sbyte),
			typeof(byte),
			typeof(short),
			typeof(ushort),
			typeof(int),
			typeof(uint),
			typeof(long),
			typeof(ulong),
			typeof(float),
			typeof(double),
			typeof(decimal),
			typeof(DateTime),
			null,
			typeof(string)
		};

		[MethodImpl(MethodImplOptions.InternalCall)]
		private static extern byte[] InternalFromBase64String(string str, bool allowWhitespaceOnly);

		[MethodImpl(MethodImplOptions.InternalCall)]
		private static extern byte[] InternalFromBase64CharArray(char[] arr, int offset, int length);

		/// <summary>Converts a subset of a Unicode character array, which encodes binary data as base 64 digits, to an equivalent 8-bit unsigned integer array. Parameters specify the subset in the input array and the number of elements to convert.</summary>
		/// <returns>An array of 8-bit unsigned integers equivalent to <paramref name="length" /> elements at position <paramref name="offset" /> in <paramref name="inArray" />.</returns>
		/// <param name="inArray">A Unicode character array. </param>
		/// <param name="offset">A position within <paramref name="inArray" />. </param>
		/// <param name="length">The number of elements in <paramref name="inArray" /> to convert. </param>
		/// <exception cref="T:System.ArgumentNullException">
		///   <paramref name="inArray" /> is null. </exception>
		/// <exception cref="T:System.ArgumentOutOfRangeException">
		///   <paramref name="offset" /> or <paramref name="length" /> is less than 0.-or- <paramref name="offset" /> plus <paramref name="length" /> indicates a position not within <paramref name="inArray" />. </exception>
		/// <exception cref="T:System.FormatException">The length of <paramref name="inArray" />, ignoring white space characters, is not zero or a multiple of 4. -or-The format of <paramref name="inArray" /> is invalid. <paramref name="inArray" /> contains a non-base 64 character, more than two padding characters, or a non-white space character among the padding characters. </exception>
		/// <filterpriority>1</filterpriority>
		public static byte[] FromBase64CharArray(char[] inArray, int offset, int length)
		{
			if (inArray == null)
			{
				throw new ArgumentNullException("inArray");
			}
			if (offset < 0)
			{
				throw new ArgumentOutOfRangeException("offset < 0");
			}
			if (length < 0)
			{
				throw new ArgumentOutOfRangeException("length < 0");
			}
			if (offset > inArray.Length - length)
			{
				throw new ArgumentOutOfRangeException("offset + length > array.Length");
			}
			return InternalFromBase64CharArray(inArray, offset, length);
		}

		/// <summary>Converts the specified <see cref="T:System.String" />, which encodes binary data as base 64 digits, to an equivalent 8-bit unsigned integer array.</summary>
		/// <returns>An array of 8-bit unsigned integers equivalent to <paramref name="s" />.</returns>
		/// <param name="s">A <see cref="T:System.String" />. </param>
		/// <exception cref="T:System.ArgumentNullException">
		///   <paramref name="s" /> is null. </exception>
		/// <exception cref="T:System.FormatException">The length of <paramref name="s" />, ignoring white space characters, is not zero or a multiple of 4. -or-The format of <paramref name="s" /> is invalid. <paramref name="s" /> contains a non-base 64 character, more than two padding characters, or a non-white space character among the padding characters.</exception>
		/// <filterpriority>1</filterpriority>
		public static byte[] FromBase64String(string s)
		{
			if (s == null)
			{
				throw new ArgumentNullException("s");
			}
			if (s.Length == 0)
			{
				return new byte[0];
			}
			return InternalFromBase64String(s, allowWhitespaceOnly: true);
		}

		/// <summary>Returns the <see cref="T:System.TypeCode" /> for the specified object.</summary>
		/// <returns>The <see cref="T:System.TypeCode" /> for <paramref name="value" />, or <see cref="F:System.TypeCode.Empty" /> if <paramref name="value" /> is null.</returns>
		/// <param name="value">An <see cref="T:System.Object" /> that implements the <see cref="T:System.IConvertible" /> interface. </param>
		/// <filterpriority>1</filterpriority>
		public static TypeCode GetTypeCode(object value)
		{
			if (value == null)
			{
				return TypeCode.Empty;
			}
			return Type.GetTypeCode(value.GetType());
		}

		/// <summary>Returns an indication whether the specified object is of type <see cref="F:System.TypeCode.DBNull" />.</summary>
		/// <returns>true if <paramref name="value" /> is of type <see cref="F:System.TypeCode.DBNull" />; otherwise, false.</returns>
		/// <param name="value">An object. </param>
		/// <filterpriority>1</filterpriority>
		public static bool IsDBNull(object value)
		{
			if (value is DBNull)
			{
				return true;
			}
			return false;
		}

		/// <summary>Converts a subset of an 8-bit unsigned integer array to an equivalent subset of a Unicode character array encoded with base 64 digits. Parameters specify the subsets as offsets in the input and output arrays, and the number of elements in the input array to convert.</summary>
		/// <returns>A 32-bit signed integer containing the number of bytes in <paramref name="outArray" />.</returns>
		/// <param name="inArray">An input array of 8-bit unsigned integers. </param>
		/// <param name="offsetIn">A position within <paramref name="inArray" />. </param>
		/// <param name="length">The number of elements of <paramref name="inArray" /> to convert. </param>
		/// <param name="outArray">An output array of Unicode characters. </param>
		/// <param name="offsetOut">A position within <paramref name="outArray" />. </param>
		/// <exception cref="T:System.ArgumentNullException">
		///   <paramref name="inArray" /> or <paramref name="outArray" /> is null. </exception>
		/// <exception cref="T:System.ArgumentOutOfRangeException">
		///   <paramref name="offsetIn" />, <paramref name="offsetOut" />, or <paramref name="length" /> is negative.-or- <paramref name="offsetIn" /> plus <paramref name="length" /> is greater than the length of <paramref name="inArray" />.-or- <paramref name="offsetOut" /> plus the number of elements to return is greater than the length of <paramref name="outArray" />. </exception>
		/// <filterpriority>1</filterpriority>
		public static int ToBase64CharArray(byte[] inArray, int offsetIn, int length, char[] outArray, int offsetOut)
		{
			if (inArray == null)
			{
				throw new ArgumentNullException("inArray");
			}
			if (outArray == null)
			{
				throw new ArgumentNullException("outArray");
			}
			if (offsetIn < 0 || length < 0 || offsetOut < 0)
			{
				throw new ArgumentOutOfRangeException("offsetIn, length, offsetOut < 0");
			}
			if (offsetIn > inArray.Length - length)
			{
				throw new ArgumentOutOfRangeException("offsetIn + length > array.Length");
			}
			byte[] bytes = ToBase64Transform.InternalTransformFinalBlock(inArray, offsetIn, length);
			char[] chars = new ASCIIEncoding().GetChars(bytes);
			if (offsetOut > outArray.Length - chars.Length)
			{
				throw new ArgumentOutOfRangeException("offsetOut + cOutArr.Length > outArray.Length");
			}
			Array.Copy(chars, 0, outArray, offsetOut, chars.Length);
			return chars.Length;
		}

		/// <summary>Converts an array of 8-bit unsigned integers to its equivalent <see cref="T:System.String" /> representation encoded with base 64 digits.</summary>
		/// <returns>The <see cref="T:System.String" /> representation, in base 64, of the contents of <paramref name="inArray" />.</returns>
		/// <param name="inArray">An array of 8-bit unsigned integers. </param>
		/// <exception cref="T:System.ArgumentNullException">
		///   <paramref name="inArray" /> is null. </exception>
		/// <filterpriority>1</filterpriority>
		public static string ToBase64String(byte[] inArray)
		{
			if (inArray == null)
			{
				throw new ArgumentNullException("inArray");
			}
			return ToBase64String(inArray, 0, inArray.Length);
		}

		/// <summary>Converts a subset of an array of 8-bit unsigned integers to its equivalent <see cref="T:System.String" /> representation encoded with base 64 digits. Parameters specify the subset as an offset in the input array, and the number of elements in the array to convert.</summary>
		/// <returns>The <see cref="T:System.String" /> representation in base 64 of <paramref name="length" /> elements of <paramref name="inArray" /> starting at position <paramref name="offset" />.</returns>
		/// <param name="inArray">An array of 8-bit unsigned integers. </param>
		/// <param name="offset">An offset in <paramref name="inArray" />. </param>
		/// <param name="length">The number of elements of <paramref name="inArray" /> to convert. </param>
		/// <exception cref="T:System.ArgumentNullException">
		///   <paramref name="inArray" /> is null. </exception>
		/// <exception cref="T:System.ArgumentOutOfRangeException">
		///   <paramref name="offset" /> or <paramref name="length" /> is negative.-or- <paramref name="offset" /> plus <paramref name="length" /> is greater than the length of <paramref name="inArray" />. </exception>
		/// <filterpriority>1</filterpriority>
		public static string ToBase64String(byte[] inArray, int offset, int length)
		{
			if (inArray == null)
			{
				throw new ArgumentNullException("inArray");
			}
			if (offset < 0 || length < 0)
			{
				throw new ArgumentOutOfRangeException("offset < 0 || length < 0");
			}
			if (offset > inArray.Length - length)
			{
				throw new ArgumentOutOfRangeException("offset + length > array.Length");
			}
			byte[] bytes = ToBase64Transform.InternalTransformFinalBlock(inArray, offset, length);
			return new ASCIIEncoding().GetString(bytes);
		}

		/// <summary>Converts an array of 8-bit unsigned integers to its equivalent <see cref="T:System.String" /> representation encoded with base 64 digits. A parameter specifies whether to insert line breaks in the return value.</summary>
		/// <returns>The <see cref="T:System.String" /> representation in base 64 of the elements in <paramref name="inArray" />.</returns>
		/// <param name="inArray">An array of 8-bit unsigned integers. </param>
		/// <param name="options">
		///   <see cref="F:System.Base64FormattingOptions.InsertLineBreaks" /> to insert a line break every 76 characters, or <see cref="F:System.Base64FormattingOptions.None" /> to not insert line breaks.</param>
		/// <exception cref="T:System.ArgumentNullException">
		///   <paramref name="inArray" /> is null. </exception>
		/// <exception cref="T:System.ArgumentException">
		///   <paramref name="options" /> is not a valid <see cref="T:System.Base64FormattingOptions" /> value. </exception>
		/// <filterpriority>1</filterpriority>
		[ComVisible(false)]
		public static string ToBase64String(byte[] inArray, Base64FormattingOptions options)
		{
			if (inArray == null)
			{
				throw new ArgumentNullException("inArray");
			}
			return ToBase64String(inArray, 0, inArray.Length, options);
		}

		/// <summary>Converts a subset of an array of 8-bit unsigned integers to its equivalent <see cref="T:System.String" /> representation encoded with base 64 digits. Parameters specify the subset as an offset in the input array, the number of elements in the array to convert, and whether to insert line breaks in the return value.</summary>
		/// <returns>The <see cref="T:System.String" /> representation in base 64 of <paramref name="length" /> elements of <paramref name="inArray" /> starting at position <paramref name="offset" />.</returns>
		/// <param name="inArray">An array of 8-bit unsigned integers. </param>
		/// <param name="offset">An offset in <paramref name="inArray" />. </param>
		/// <param name="length">The number of elements of <paramref name="inArray" /> to convert. </param>
		/// <param name="options">
		///   <see cref="F:System.Base64FormattingOptions.InsertLineBreaks" /> to insert a line break every 76 characters, or <see cref="F:System.Base64FormattingOptions.None" /> to not insert line breaks.</param>
		/// <exception cref="T:System.ArgumentNullException">
		///   <paramref name="inArray" /> is null. </exception>
		/// <exception cref="T:System.ArgumentOutOfRangeException">
		///   <paramref name="offset" /> or <paramref name="length" /> is negative.-or- <paramref name="offset" /> plus <paramref name="length" /> is greater than the length of <paramref name="inArray" />. </exception>
		/// <exception cref="T:System.ArgumentException">
		///   <paramref name="options" /> is not a valid <see cref="T:System.Base64FormattingOptions" /> value. </exception>
		/// <filterpriority>1</filterpriority>
		[ComVisible(false)]
		public static string ToBase64String(byte[] inArray, int offset, int length, Base64FormattingOptions options)
		{
			if (inArray == null)
			{
				throw new ArgumentNullException("inArray");
			}
			if (offset < 0 || length < 0)
			{
				throw new ArgumentOutOfRangeException("offset < 0 || length < 0");
			}
			if (offset > inArray.Length - length)
			{
				throw new ArgumentOutOfRangeException("offset + length > array.Length");
			}
			if (length == 0)
			{
				return string.Empty;
			}
			if (options == Base64FormattingOptions.InsertLineBreaks)
			{
				return ToBase64StringBuilderWithLine(inArray, offset, length).ToString();
			}
			return Encoding.ASCII.GetString(ToBase64Transform.InternalTransformFinalBlock(inArray, offset, length));
		}

		/// <summary>Converts a subset of an 8-bit unsigned integer array to an equivalent subset of a Unicode character array encoded with base 64 digits. Parameters specify the subsets as offsets in the input and output arrays, the number of elements in the input array to convert, and whether line breaks are inserted in the output array.</summary>
		/// <returns>A 32-bit signed integer containing the number of bytes in <paramref name="outArray" />.</returns>
		/// <param name="inArray">An input array of 8-bit unsigned integers. </param>
		/// <param name="offsetIn">A position within <paramref name="inArray" />. </param>
		/// <param name="length">The number of elements of <paramref name="inArray" /> to convert. </param>
		/// <param name="outArray">An output array of Unicode characters. </param>
		/// <param name="offsetOut">A position within <paramref name="outArray" />. </param>
		/// <param name="options">
		///   <see cref="F:System.Base64FormattingOptions.InsertLineBreaks" /> to insert a line break every 76 characters, or <see cref="F:System.Base64FormattingOptions.None" /> to not insert line breaks.</param>
		/// <exception cref="T:System.ArgumentNullException">
		///   <paramref name="inArray" /> or <paramref name="outArray" /> is null. </exception>
		/// <exception cref="T:System.ArgumentOutOfRangeException">
		///   <paramref name="offsetIn" />, <paramref name="offsetOut" />, or <paramref name="length" /> is negative.-or- <paramref name="offsetIn" /> plus <paramref name="length" /> is greater than the length of <paramref name="inArray" />.-or- <paramref name="offsetOut" /> plus the number of elements to return is greater than the length of <paramref name="outArray" />. </exception>
		/// <exception cref="T:System.ArgumentException">
		///   <paramref name="options" /> is not a valid <see cref="T:System.Base64FormattingOptions" /> value. </exception>
		/// <filterpriority>1</filterpriority>
		[ComVisible(false)]
		public static int ToBase64CharArray(byte[] inArray, int offsetIn, int length, char[] outArray, int offsetOut, Base64FormattingOptions options)
		{
			if (inArray == null)
			{
				throw new ArgumentNullException("inArray");
			}
			if (outArray == null)
			{
				throw new ArgumentNullException("outArray");
			}
			if (offsetIn < 0 || length < 0 || offsetOut < 0)
			{
				throw new ArgumentOutOfRangeException("offsetIn, length, offsetOut < 0");
			}
			if (offsetIn > inArray.Length - length)
			{
				throw new ArgumentOutOfRangeException("offsetIn + length > array.Length");
			}
			if (length == 0)
			{
				return 0;
			}
			if (options == Base64FormattingOptions.InsertLineBreaks)
			{
				StringBuilder stringBuilder = ToBase64StringBuilderWithLine(inArray, offsetIn, length);
				stringBuilder.CopyTo(0, outArray, offsetOut, stringBuilder.Length);
				return stringBuilder.Length;
			}
			byte[] bytes = ToBase64Transform.InternalTransformFinalBlock(inArray, offsetIn, length);
			char[] chars = Encoding.ASCII.GetChars(bytes);
			if (offsetOut > outArray.Length - chars.Length)
			{
				throw new ArgumentOutOfRangeException("offsetOut + cOutArr.Length > outArray.Length");
			}
			Array.Copy(chars, 0, outArray, offsetOut, chars.Length);
			return chars.Length;
		}

		private static StringBuilder ToBase64StringBuilderWithLine(byte[] inArray, int offset, int length)
		{
			StringBuilder stringBuilder = new StringBuilder();
			int result;
			int num = Math.DivRem(length, 57, out result);
			for (int i = 0; i < num; i++)
			{
				byte[] bytes = ToBase64Transform.InternalTransformFinalBlock(inArray, offset, 57);
				stringBuilder.AppendLine(Encoding.ASCII.GetString(bytes));
				offset += 57;
			}
			if (result == 0)
			{
				int length2 = Environment.NewLine.Length;
				stringBuilder.Remove(stringBuilder.Length - length2, length2);
			}
			else
			{
				byte[] bytes2 = ToBase64Transform.InternalTransformFinalBlock(inArray, offset, result);
				stringBuilder.Append(Encoding.ASCII.GetString(bytes2));
			}
			return stringBuilder;
		}

		/// <summary>Returns the specified Boolean value; no actual conversion is performed.</summary>
		/// <returns>Parameter <paramref name="value" /> is returned unchanged.</returns>
		/// <param name="value">A Boolean. </param>
		/// <filterpriority>1</filterpriority>
		public static bool ToBoolean(bool value)
		{
			return value;
		}

		/// <summary>Converts the value of the specified 8-bit unsigned integer to an equivalent Boolean value.</summary>
		/// <returns>true if <paramref name="value" /> is not zero; otherwise, false.</returns>
		/// <param name="value">An 8-bit unsigned integer. </param>
		/// <filterpriority>1</filterpriority>
		public static bool ToBoolean(byte value)
		{
			return value != 0;
		}

		/// <summary>Calling this method always throws <see cref="T:System.InvalidCastException" />.</summary>
		/// <returns>This conversion is not supported. No value is returned.</returns>
		/// <param name="value">A Unicode character. </param>
		/// <exception cref="T:System.InvalidCastException">This conversion is not supported. </exception>
		/// <filterpriority>1</filterpriority>
		public static bool ToBoolean(char value)
		{
			throw new InvalidCastException(Locale.GetText("Can't convert char to bool"));
		}

		/// <summary>Calling this method always throws <see cref="T:System.InvalidCastException" />.</summary>
		/// <returns>This conversion is not supported. No value is returned.</returns>
		/// <param name="value">A <see cref="T:System.DateTime" />. </param>
		/// <exception cref="T:System.InvalidCastException">This conversion is not supported. </exception>
		/// <filterpriority>1</filterpriority>
		public static bool ToBoolean(DateTime value)
		{
			throw new InvalidCastException(Locale.GetText("Can't convert date to bool"));
		}

		/// <summary>Converts the value of the specified <see cref="T:System.Decimal" /> number to an equivalent Boolean value.</summary>
		/// <returns>true if <paramref name="value" /> is not zero; otherwise, false.</returns>
		/// <param name="value">A <see cref="T:System.Decimal" /> number. </param>
		/// <filterpriority>1</filterpriority>
		public static bool ToBoolean(decimal value)
		{
			return value != 0m;
		}

		/// <summary>Converts the value of the specified double-precision floating point number to an equivalent Boolean value.</summary>
		/// <returns>true if <paramref name="value" /> is not zero; otherwise, false.</returns>
		/// <param name="value">A double-precision floating point number. </param>
		/// <filterpriority>1</filterpriority>
		public static bool ToBoolean(double value)
		{
			return value != 0.0;
		}

		/// <summary>Converts the value of the specified single-precision floating point number to an equivalent Boolean value.</summary>
		/// <returns>true if <paramref name="value" /> is not zero; otherwise, false.</returns>
		/// <param name="value">A single-precision floating point number. </param>
		/// <filterpriority>1</filterpriority>
		public static bool ToBoolean(float value)
		{
			return value != 0f;
		}

		/// <summary>Converts the value of the specified 32-bit signed integer to an equivalent Boolean value.</summary>
		/// <returns>true if <paramref name="value" /> is not zero; otherwise, false.</returns>
		/// <param name="value">A 32-bit signed integer. </param>
		/// <filterpriority>1</filterpriority>
		public static bool ToBoolean(int value)
		{
			return value != 0;
		}

		/// <summary>Converts the value of the specified 64-bit signed integer to an equivalent Boolean value.</summary>
		/// <returns>true if <paramref name="value" /> is not zero; otherwise, false.</returns>
		/// <param name="value">A 64-bit signed integer. </param>
		/// <filterpriority>1</filterpriority>
		public static bool ToBoolean(long value)
		{
			return value != 0;
		}

		/// <summary>Converts the value of the specified 8-bit signed integer to an equivalent Boolean value.</summary>
		/// <returns>true if <paramref name="value" /> is not zero; otherwise, false.</returns>
		/// <param name="value">An 8-bit signed integer. </param>
		/// <filterpriority>1</filterpriority>
		[CLSCompliant(false)]
		public static bool ToBoolean(sbyte value)
		{
			return value != 0;
		}

		/// <summary>Converts the value of the specified 16-bit signed integer to an equivalent Boolean value.</summary>
		/// <returns>true if <paramref name="value" /> is not zero; otherwise, false.</returns>
		/// <param name="value">A 16-bit signed integer. </param>
		/// <filterpriority>1</filterpriority>
		public static bool ToBoolean(short value)
		{
			return value != 0;
		}

		/// <summary>Converts the specified <see cref="T:System.String" /> representation of a logical value to its Boolean equivalent.</summary>
		/// <returns>true if <paramref name="value" /> equals <see cref="F:System.Boolean.TrueString" />, or false if <paramref name="value" /> equals <see cref="F:System.Boolean.FalseString" /> or null.</returns>
		/// <param name="value">A <see cref="T:System.String" /> that contains the value of either <see cref="F:System.Boolean.TrueString" /> or <see cref="F:System.Boolean.FalseString" />. </param>
		/// <exception cref="T:System.FormatException">
		///   <paramref name="value" /> is not equal to <see cref="F:System.Boolean.TrueString" /> or <see cref="F:System.Boolean.FalseString" />. </exception>
		/// <filterpriority>1</filterpriority>
		public static bool ToBoolean(string value)
		{
			if (value == null)
			{
				return false;
			}
			return bool.Parse(value);
		}

		/// <summary>Converts the specified <see cref="T:System.String" /> representation of a logical value to its Boolean equivalent using the specified culture-specific formatting information.</summary>
		/// <returns>true if <paramref name="value" /> equals <see cref="F:System.Boolean.TrueString" />, or false if <paramref name="value" /> equals <see cref="F:System.Boolean.FalseString" /> or null.</returns>
		/// <param name="value">A string that contains the value of either <see cref="F:System.Boolean.TrueString" /> or <see cref="F:System.Boolean.FalseString" />. </param>
		/// <param name="provider">(Reserved) An <see cref="T:System.IFormatProvider" /> interface implementation that supplies culture-specific formatting information. </param>
		/// <exception cref="T:System.FormatException">
		///   <paramref name="value" /> is not equal to <see cref="F:System.Boolean.TrueString" /> or <see cref="F:System.Boolean.FalseString" />. </exception>
		/// <filterpriority>1</filterpriority>
		public static bool ToBoolean(string value, IFormatProvider provider)
		{
			if (value == null)
			{
				return false;
			}
			return bool.Parse(value);
		}

		/// <summary>Converts the value of the specified 32-bit unsigned integer to an equivalent Boolean value.</summary>
		/// <returns>true if <paramref name="value" /> is not zero; otherwise, false.</returns>
		/// <param name="value">A 32-bit unsigned integer. </param>
		/// <filterpriority>1</filterpriority>
		[CLSCompliant(false)]
		public static bool ToBoolean(uint value)
		{
			return value != 0;
		}

		/// <summary>Converts the value of the specified 64-bit unsigned integer to an equivalent Boolean value.</summary>
		/// <returns>true if <paramref name="value" /> is not zero; otherwise, false.</returns>
		/// <param name="value">A 64-bit unsigned integer. </param>
		/// <filterpriority>1</filterpriority>
		[CLSCompliant(false)]
		public static bool ToBoolean(ulong value)
		{
			return value != 0;
		}

		/// <summary>Converts the value of the specified 16-bit unsigned integer to an equivalent Boolean value.</summary>
		/// <returns>true if <paramref name="value" /> is not zero; otherwise, false.</returns>
		/// <param name="value">A 16-bit unsigned integer. </param>
		/// <filterpriority>1</filterpriority>
		[CLSCompliant(false)]
		public static bool ToBoolean(ushort value)
		{
			return value != 0;
		}

		/// <summary>Converts the value of a specified <see cref="T:System.Object" /> to an equivalent Boolean value.</summary>
		/// <returns>false if <paramref name="value" /> equals null.-or- true or false; the result of invoking the IConvertible.ToBoolean method for the underlying type of <paramref name="value" />.</returns>
		/// <param name="value">An <see cref="T:System.Object" /> that implements the <see cref="T:System.IConvertible" /> interface or null. </param>
		/// <filterpriority>1</filterpriority>
		public static bool ToBoolean(object value)
		{
			if (value == null)
			{
				return false;
			}
			return ToBoolean(value, null);
		}

		/// <summary>Converts the value of the specified <see cref="T:System.Object" /> to an equivalent Boolean value using the specified culture-specific formatting information.</summary>
		/// <returns>false if <paramref name="value" /> equals null.-or- true or false; the result of invoking the ToBoolean method for the underlying type of <paramref name="value" />.</returns>
		/// <param name="value">An <see cref="T:System.Object" /> that implements the <see cref="T:System.IConvertible" /> interface or null. </param>
		/// <param name="provider">An <see cref="T:System.IFormatProvider" /> interface implementation that supplies culture-specific formatting information. </param>
		/// <exception cref="T:System.InvalidCastException">
		///   <paramref name="value" /> does not implement the <see cref="T:System.IConvertible" /> interface. </exception>
		/// <filterpriority>1</filterpriority>
		public static bool ToBoolean(object value, IFormatProvider provider)
		{
			if (value == null)
			{
				return false;
			}
			return ((IConvertible)value).ToBoolean(provider);
		}

		/// <summary>Converts the value of the specified Boolean value to the equivalent 8-bit unsigned integer.</summary>
		/// <returns>The number 1 if <paramref name="value" /> is true; otherwise, 0.</returns>
		/// <param name="value">A Boolean value. </param>
		/// <filterpriority>1</filterpriority>
		public static byte ToByte(bool value)
		{
			return (byte)(value ? 1 : 0);
		}

		/// <summary>Returns the specified 8-bit unsigned integer; no actual conversion is performed.</summary>
		/// <returns>Parameter <paramref name="value" /> is returned unchanged.</returns>
		/// <param name="value">An 8-bit unsigned integer. </param>
		/// <filterpriority>1</filterpriority>
		public static byte ToByte(byte value)
		{
			return value;
		}

		/// <summary>Converts the value of the specified Unicode character to the equivalent 8-bit unsigned integer.</summary>
		/// <returns>The 8-bit unsigned integer equivalent to <paramref name="value" />.</returns>
		/// <param name="value">A Unicode character. </param>
		/// <exception cref="T:System.OverflowException">
		///   <paramref name="value" /> is greater than <see cref="F:System.Byte.MaxValue" />. </exception>
		/// <filterpriority>1</filterpriority>
		public static byte ToByte(char value)
		{
			if (value > 'ÿ')
			{
				throw new OverflowException(Locale.GetText("Value is greater than Byte.MaxValue"));
			}
			return (byte)value;
		}

		/// <summary>Calling this method always throws <see cref="T:System.InvalidCastException" />.</summary>
		/// <returns>This conversion is not supported. No value is returned.</returns>
		/// <param name="value">A <see cref="T:System.DateTime" />. </param>
		/// <exception cref="T:System.InvalidCastException">This conversion is not supported. </exception>
		/// <filterpriority>1</filterpriority>
		public static byte ToByte(DateTime value)
		{
			throw new InvalidCastException("This conversion is not supported.");
		}

		/// <summary>Converts the value of the specified <see cref="T:System.Decimal" /> number to an equivalent 8-bit unsigned integer.</summary>
		/// <returns>
		///   <paramref name="value" /> rounded to the nearest 8-bit signed integer. If <paramref name="value" /> is halfway between two whole numbers, the even number is returned; that is, 4.5 is converted to 4, and 5.5 is converted to 6.</returns>
		/// <param name="value">A <see cref="T:System.Decimal" /> number. </param>
		/// <exception cref="T:System.OverflowException">
		///   <paramref name="value" /> is greater than <see cref="F:System.Byte.MaxValue" /> or less than <see cref="F:System.Byte.MinValue" />. </exception>
		/// <filterpriority>1</filterpriority>
		public static byte ToByte(decimal value)
		{
			if (value > 255m || value < 0m)
			{
				throw new OverflowException(Locale.GetText("Value is greater than Byte.MaxValue or less than Byte.MinValue"));
			}
			return (byte)Math.Round(value);
		}

		/// <summary>Converts the value of the specified double-precision floating point number to an equivalent 8-bit unsigned integer.</summary>
		/// <returns>
		///   <paramref name="value" /> rounded to the nearest 8-bit signed integer. If <paramref name="value" /> is halfway between two whole numbers, the even number is returned; that is, 4.5 is converted to 4, and 5.5 is converted to 6.</returns>
		/// <param name="value">A double-precision floating point number. </param>
		/// <exception cref="T:System.OverflowException">
		///   <paramref name="value" /> is greater than <see cref="F:System.Byte.MaxValue" /> or less than <see cref="F:System.Byte.MinValue" />. </exception>
		/// <filterpriority>1</filterpriority>
		public static byte ToByte(double value)
		{
			if (value > 255.0 || value < 0.0)
			{
				throw new OverflowException(Locale.GetText("Value is greater than Byte.MaxValue or less than Byte.MinValue"));
			}
			if (double.IsNaN(value) || double.IsInfinity(value))
			{
				throw new OverflowException(Locale.GetText("Value is equal to Double.NaN, Double.PositiveInfinity, or Double.NegativeInfinity"));
			}
			return (byte)Math.Round(value);
		}

		/// <summary>Converts the value of the specified single-precision floating point number to an equivalent 8-bit unsigned integer.</summary>
		/// <returns>
		///   <paramref name="value" /> rounded to the nearest 8-bit signed integer. If <paramref name="value" /> is halfway between two whole numbers, the even number is returned; that is, 4.5 is converted to 4, and 5.5 is converted to 6.</returns>
		/// <param name="value">A single-precision floating point number. </param>
		/// <exception cref="T:System.OverflowException">
		///   <paramref name="value" /> is greater than <see cref="F:System.Byte.MaxValue" /> or less than <see cref="F:System.Byte.MinValue" />. </exception>
		/// <filterpriority>1</filterpriority>
		public static byte ToByte(float value)
		{
			if (value > 255f || value < 0f)
			{
				throw new OverflowException(Locale.GetText("Value is greater than Byte.MaxValue or less than Byte.Minalue"));
			}
			if (float.IsNaN(value) || float.IsInfinity(value))
			{
				throw new OverflowException(Locale.GetText("Value is equal to Single.NaN, Single.PositiveInfinity, or Single.NegativeInfinity"));
			}
			return (byte)Math.Round(value);
		}

		/// <summary>Converts the value of the specified 32-bit signed integer to an equivalent 8-bit unsigned integer.</summary>
		/// <returns>An 8-bit unsigned integer equivalent to the value of <paramref name="value" />.</returns>
		/// <param name="value">A 32-bit signed integer. </param>
		/// <exception cref="T:System.OverflowException">
		///   <paramref name="value" /> is less than <see cref="F:System.Byte.MinValue" /> or greater than <see cref="F:System.Byte.MaxValue" />. </exception>
		/// <filterpriority>1</filterpriority>
		public static byte ToByte(int value)
		{
			if (value > 255 || value < 0)
			{
				throw new OverflowException(Locale.GetText("Value is greater than Byte.MaxValue or less than Byte.MinValue"));
			}
			return (byte)value;
		}

		/// <summary>Converts the value of the specified 64-bit signed integer to an equivalent 8-bit unsigned integer.</summary>
		/// <returns>An 8-bit unsigned integer equivalent to the value of <paramref name="value" />.</returns>
		/// <param name="value">A 64-bit signed integer. </param>
		/// <exception cref="T:System.OverflowException">
		///   <paramref name="value" /> is less than <see cref="F:System.Byte.MinValue" /> or greater than <see cref="F:System.Byte.MaxValue" />. </exception>
		/// <filterpriority>1</filterpriority>
		public static byte ToByte(long value)
		{
			if (value > 255 || value < 0)
			{
				throw new OverflowException(Locale.GetText("Value is greater than Byte.MaxValue or less than Byte.MinValue"));
			}
			return (byte)value;
		}

		/// <summary>Converts the value of the specified 8-bit signed integer to an equivalent 8-bit unsigned integer.</summary>
		/// <returns>An 8-bit unsigned integer equivalent to the value of <paramref name="value" />.</returns>
		/// <param name="value">An 8-bit signed integer. </param>
		/// <exception cref="T:System.OverflowException">
		///   <paramref name="value" /> is less than <see cref="F:System.Byte.MinValue" />. </exception>
		/// <filterpriority>1</filterpriority>
		[CLSCompliant(false)]
		public static byte ToByte(sbyte value)
		{
			if (value < 0)
			{
				throw new OverflowException(Locale.GetText("Value is less than Byte.MinValue"));
			}
			return (byte)value;
		}

		/// <summary>Converts the value of the specified 16-bit signed integer to an equivalent 8-bit unsigned integer.</summary>
		/// <returns>An 8-bit unsigned integer equivalent to the value of <paramref name="value" />.</returns>
		/// <param name="value">A 16-bit signed integer. </param>
		/// <exception cref="T:System.OverflowException">
		///   <paramref name="value" /> is less than <see cref="F:System.Byte.MinValue" /> or greater than <see cref="F:System.Byte.MaxValue" />. </exception>
		/// <filterpriority>1</filterpriority>
		public static byte ToByte(short value)
		{
			if (value > 255 || value < 0)
			{
				throw new OverflowException(Locale.GetText("Value is greater than Byte.MaxValue or less than Byte.MinValue"));
			}
			return (byte)value;
		}

		/// <summary>Converts the specified <see cref="T:System.String" /> representation of a number to an equivalent 8-bit unsigned integer.</summary>
		/// <returns>An 8-bit unsigned integer equivalent to the value of <paramref name="value" />.-or- Zero if <paramref name="value" /> is null.</returns>
		/// <param name="value">A <see cref="T:System.String" /> containing a number to convert. </param>
		/// <exception cref="T:System.FormatException">
		///   <paramref name="value" /> does not consist of an optional sign followed by a sequence of digits (zero through nine). </exception>
		/// <exception cref="T:System.OverflowException">
		///   <paramref name="value" /> represents a number less than <see cref="F:System.Byte.MinValue" /> or greater than <see cref="F:System.Byte.MaxValue" />. </exception>
		/// <filterpriority>1</filterpriority>
		public static byte ToByte(string value)
		{
			if (value == null)
			{
				return 0;
			}
			return byte.Parse(value);
		}

		/// <summary>Converts the specified <see cref="T:System.String" /> representation of a number to an equivalent 8-bit signed integer using specified culture-specific formatting information.</summary>
		/// <returns>An 8-bit signed integer equivalent to the value of <paramref name="value" />.-or- Zero if <paramref name="value" /> is null.</returns>
		/// <param name="value">A <see cref="T:System.String" /> containing a number to convert. </param>
		/// <param name="provider">An <see cref="T:System.IFormatProvider" /> interface implementation that supplies culture-specific formatting information. </param>
		/// <exception cref="T:System.FormatException">
		///   <paramref name="value" /> does not consist of an optional sign followed by a sequence of digits (zero through nine). </exception>
		/// <exception cref="T:System.OverflowException">
		///   <paramref name="value" /> represents a number less than <see cref="F:System.Byte.MinValue" /> or greater than <see cref="F:System.Byte.MaxValue" />. </exception>
		/// <filterpriority>1</filterpriority>
		public static byte ToByte(string value, IFormatProvider provider)
		{
			if (value == null)
			{
				return 0;
			}
			return byte.Parse(value, provider);
		}

		/// <summary>Converts the <see cref="T:System.String" /> representation of a number in a specified base to an equivalent 8-bit unsigned integer.</summary>
		/// <returns>An 8-bit unsigned integer equivalent to the number in <paramref name="value" />.-or- Zero if <paramref name="value" /> is null.</returns>
		/// <param name="value">A <see cref="T:System.String" /> containing a number. </param>
		/// <param name="fromBase">The base of the number in <paramref name="value" />, which must be 2, 8, 10, or 16. </param>
		/// <exception cref="T:System.ArgumentException">
		///   <paramref name="fromBase" /> is not 2, 8, 10, or 16. -or-<paramref name="value" />, which represents a non-base 10 unsigned number, is prefixed with a negative sign. </exception>
		/// <exception cref="T:System.FormatException">
		///   <paramref name="value" /> contains a character that is not a valid digit in the base specified by <paramref name="fromBase" />. The exception message indicates that there are no digits to convert if the first character in <paramref name="value" /> is invalid; otherwise, the message indicates that <paramref name="value" /> contains invalid trailing characters.</exception>
		/// <exception cref="T:System.OverflowException">
		///   <paramref name="value" />, which represents a base 10 unsigned number, is prefixed with a negative sign.-or-The return value is less than <see cref="F:System.Byte.MinValue" /> or larger than <see cref="F:System.Byte.MaxValue" />.</exception>
		/// <filterpriority>1</filterpriority>
		public static byte ToByte(string value, int fromBase)
		{
			int num = ConvertFromBase(value, fromBase, unsigned: true);
			if (num < 0 || num > 255)
			{
				throw new OverflowException();
			}
			return (byte)num;
		}

		/// <summary>Converts the value of the specified 32-bit unsigned integer to an equivalent 8-bit unsigned integer.</summary>
		/// <returns>An 8-bit unsigned integer equivalent to the value of <paramref name="value" />.</returns>
		/// <param name="value">A 32-bit unsigned integer. </param>
		/// <exception cref="T:System.OverflowException">
		///   <paramref name="value" /> is greater than <see cref="F:System.Byte.MaxValue" />. </exception>
		/// <filterpriority>1</filterpriority>
		[CLSCompliant(false)]
		public static byte ToByte(uint value)
		{
			if (value > 255)
			{
				throw new OverflowException(Locale.GetText("Value is greater than Byte.MaxValue"));
			}
			return (byte)value;
		}

		/// <summary>Converts the value of the specified 64-bit unsigned integer to an equivalent 8-bit unsigned integer.</summary>
		/// <returns>An 8-bit unsigned integer equivalent to the value of <paramref name="value" />.</returns>
		/// <param name="value">A 64-bit unsigned integer. </param>
		/// <exception cref="T:System.OverflowException">
		///   <paramref name="value" /> is greater than <see cref="F:System.Byte.MaxValue" />. </exception>
		/// <filterpriority>1</filterpriority>
		[CLSCompliant(false)]
		public static byte ToByte(ulong value)
		{
			if (value > 255)
			{
				throw new OverflowException(Locale.GetText("Value is greater than Byte.MaxValue"));
			}
			return (byte)value;
		}

		/// <summary>Converts the value of the specified 16-bit unsigned integer to an equivalent 8-bit unsigned integer.</summary>
		/// <returns>An 8-bit unsigned integer equivalent to the value of <paramref name="value" />.</returns>
		/// <param name="value">A 16-bit unsigned integer. </param>
		/// <exception cref="T:System.OverflowException">
		///   <paramref name="value" /> is greater than <see cref="F:System.Byte.MaxValue" />. </exception>
		/// <filterpriority>1</filterpriority>
		[CLSCompliant(false)]
		public static byte ToByte(ushort value)
		{
			if (value > 255)
			{
				throw new OverflowException(Locale.GetText("Value is greater than Byte.MaxValue"));
			}
			return (byte)value;
		}

		/// <summary>Converts the value of the specified <see cref="T:System.Object" /> to an 8-bit unsigned integer.</summary>
		/// <returns>An 8-bit unsigned integer equivalent to the value of <paramref name="value" />, or zero if <paramref name="value" /> is null.</returns>
		/// <param name="value">An <see cref="T:System.Object" /> that implements the <see cref="T:System.IConvertible" /> interface or null. </param>
		/// <exception cref="T:System.InvalidCastException">
		///   <paramref name="value" /> does not implement <see cref="T:System.IConvertible" />. </exception>
		/// <filterpriority>1</filterpriority>
		public static byte ToByte(object value)
		{
			if (value == null)
			{
				return 0;
			}
			return ToByte(value, null);
		}

		/// <summary>Converts the value of the specified <see cref="T:System.Object" /> to an 8-bit unsigned integer using the specified culture-specific formatting information.</summary>
		/// <returns>An 8-bit unsigned integer equivalent to the value of <paramref name="value" />, or zero if <paramref name="value" /> is null.</returns>
		/// <param name="value">An <see cref="T:System.Object" /> that implements the <see cref="T:System.IConvertible" /> interface. </param>
		/// <param name="provider">An <see cref="T:System.IFormatProvider" /> interface implementation that supplies culture-specific formatting information. </param>
		/// <filterpriority>1</filterpriority>
		public static byte ToByte(object value, IFormatProvider provider)
		{
			if (value == null)
			{
				return 0;
			}
			return ((IConvertible)value).ToByte(provider);
		}

		/// <summary>Calling this method always throws <see cref="T:System.InvalidCastException" />.</summary>
		/// <returns>This conversion is not supported. No value is returned.</returns>
		/// <param name="value">A <see cref="T:System.Boolean" /> value. </param>
		/// <exception cref="T:System.InvalidCastException">This conversion is not supported. </exception>
		/// <filterpriority>1</filterpriority>
		public static char ToChar(bool value)
		{
			throw new InvalidCastException("This conversion is not supported.");
		}

		/// <summary>Converts the value of the specified 8-bit unsigned integer to its equivalent Unicode character.</summary>
		/// <returns>The Unicode character equivalent to the value of <paramref name="value" />.</returns>
		/// <param name="value">An 8-bit unsigned integer. </param>
		/// <filterpriority>1</filterpriority>
		public static char ToChar(byte value)
		{
			return (char)value;
		}

		/// <summary>Returns the specified Unicode character value; no actual conversion is performed.</summary>
		/// <returns>Parameter <paramref name="value" /> is returned unchanged.</returns>
		/// <param name="value">A Unicode character. </param>
		/// <filterpriority>1</filterpriority>
		public static char ToChar(char value)
		{
			return value;
		}

		/// <summary>Calling this method always throws <see cref="T:System.InvalidCastException" />.</summary>
		/// <returns>This conversion is not supported. No value is returned.</returns>
		/// <param name="value">A <see cref="T:System.DateTime" />. </param>
		/// <exception cref="T:System.InvalidCastException">This conversion is not supported. </exception>
		/// <filterpriority>1</filterpriority>
		public static char ToChar(DateTime value)
		{
			throw new InvalidCastException("This conversion is not supported.");
		}

		/// <summary>Calling this method always throws <see cref="T:System.InvalidCastException" />.</summary>
		/// <returns>This conversion is not supported. No value is returned.</returns>
		/// <param name="value">A <see cref="T:System.Decimal" /> number. </param>
		/// <exception cref="T:System.InvalidCastException">This conversion is not supported. </exception>
		/// <filterpriority>1</filterpriority>
		public static char ToChar(decimal value)
		{
			throw new InvalidCastException("This conversion is not supported.");
		}

		/// <summary>Calling this method always throws <see cref="T:System.InvalidCastException" />.</summary>
		/// <returns>This conversion is not supported. No value is returned.</returns>
		/// <param name="value">A double-precision floating-point number. </param>
		/// <exception cref="T:System.InvalidCastException">This conversion is not supported. </exception>
		/// <filterpriority>1</filterpriority>
		public static char ToChar(double value)
		{
			throw new InvalidCastException("This conversion is not supported.");
		}

		/// <summary>Converts the value of the specified 32-bit signed integer to its equivalent Unicode character.</summary>
		/// <returns>The Unicode character equivalent to the value of <paramref name="value" />.</returns>
		/// <param name="value">A 32-bit signed integer. </param>
		/// <exception cref="T:System.OverflowException">
		///   <paramref name="value" /> is less than <see cref="F:System.Char.MinValue" /> or greater than <see cref="F:System.Char.MaxValue" />. </exception>
		/// <filterpriority>1</filterpriority>
		public static char ToChar(int value)
		{
			if (value > 65535 || value < 0)
			{
				throw new OverflowException(Locale.GetText("Value is greater than Char.MaxValue or less than Char.MinValue"));
			}
			return (char)value;
		}

		/// <summary>Converts the value of the specified 64-bit signed integer to its equivalent Unicode character.</summary>
		/// <returns>The Unicode character equivalent to the value of <paramref name="value" />.</returns>
		/// <param name="value">A 64-bit signed integer. </param>
		/// <exception cref="T:System.OverflowException">
		///   <paramref name="value" /> is less than <see cref="F:System.Char.MinValue" /> or greater than <see cref="F:System.Char.MaxValue" />. </exception>
		/// <filterpriority>1</filterpriority>
		public static char ToChar(long value)
		{
			if (value > 65535 || value < 0)
			{
				throw new OverflowException(Locale.GetText("Value is greater than Char.MaxValue or less than Char.MinValue"));
			}
			return (char)value;
		}

		/// <summary>Calling this method always throws <see cref="T:System.InvalidCastException" />.</summary>
		/// <returns>This conversion is not supported. No value is returned.</returns>
		/// <param name="value">A single-precision floating-point number. </param>
		/// <exception cref="T:System.InvalidCastException">This conversion is not supported. </exception>
		/// <filterpriority>1</filterpriority>
		public static char ToChar(float value)
		{
			throw new InvalidCastException("This conversion is not supported.");
		}

		/// <summary>Converts the value of the specified 8-bit signed integer to its equivalent Unicode character.</summary>
		/// <returns>The Unicode character equivalent to the value of <paramref name="value" />.</returns>
		/// <param name="value">An 8-bit signed integer. </param>
		/// <exception cref="T:System.OverflowException">
		///   <paramref name="value" /> is less than <see cref="F:System.Char.MinValue" />. </exception>
		/// <filterpriority>1</filterpriority>
		[CLSCompliant(false)]
		public static char ToChar(sbyte value)
		{
			if (value < 0)
			{
				throw new OverflowException(Locale.GetText("Value is less than Char.MinValue"));
			}
			return (char)value;
		}

		/// <summary>Converts the value of the specified 16-bit signed integer to its equivalent Unicode character.</summary>
		/// <returns>The Unicode character equivalent to the value of <paramref name="value" />.</returns>
		/// <param name="value">A 16-bit signed integer. </param>
		/// <exception cref="T:System.OverflowException">
		///   <paramref name="value" /> is less than <see cref="F:System.Char.MinValue" />. </exception>
		/// <filterpriority>1</filterpriority>
		public static char ToChar(short value)
		{
			if (value < 0)
			{
				throw new OverflowException(Locale.GetText("Value is less than Char.MinValue"));
			}
			return (char)value;
		}

		/// <summary>Converts the first character of a <see cref="T:System.String" /> to a Unicode character.</summary>
		/// <returns>The Unicode character equivalent to the first and only character in <paramref name="value" />.</returns>
		/// <param name="value">A <see cref="T:System.String" /> of length 1 or null. </param>
		/// <exception cref="T:System.ArgumentNullException">
		///   <paramref name="value" /> is null. </exception>
		/// <exception cref="T:System.FormatException">The length of <paramref name="value" /> is not 1. </exception>
		/// <filterpriority>1</filterpriority>
		public static char ToChar(string value)
		{
			return char.Parse(value);
		}

		/// <summary>Converts the first character of a <see cref="T:System.String" /> to a Unicode character using specified culture-specific formatting information.</summary>
		/// <returns>The Unicode character equivalent to the first and only character in <paramref name="value" />.</returns>
		/// <param name="value">A <see cref="T:System.String" /> of length 1 or null. </param>
		/// <param name="provider">(Reserved) An <see cref="T:System.IFormatProvider" /> interface implementation that supplies culture-specific formatting information. </param>
		/// <exception cref="T:System.ArgumentNullException">
		///   <paramref name="value" /> is null. </exception>
		/// <exception cref="T:System.FormatException">The length of <paramref name="value" /> is not 1. </exception>
		/// <filterpriority>1</filterpriority>
		public static char ToChar(string value, IFormatProvider provider)
		{
			return char.Parse(value);
		}

		/// <summary>Converts the value of the specified 32-bit unsigned integer to its equivalent Unicode character.</summary>
		/// <returns>The Unicode character equivalent to the value of <paramref name="value" />.</returns>
		/// <param name="value">A 32-bit unsigned integer. </param>
		/// <exception cref="T:System.OverflowException">
		///   <paramref name="value" /> is greater than <see cref="F:System.Char.MaxValue" />. </exception>
		/// <filterpriority>1</filterpriority>
		[CLSCompliant(false)]
		public static char ToChar(uint value)
		{
			if (value > 65535)
			{
				throw new OverflowException(Locale.GetText("Value is greater than Char.MaxValue"));
			}
			return (char)value;
		}

		/// <summary>Converts the value of the specified 64-bit unsigned integer to its equivalent Unicode character.</summary>
		/// <returns>The Unicode character equivalent to the value of <paramref name="value" />.</returns>
		/// <param name="value">A 64-bit unsigned integer. </param>
		/// <exception cref="T:System.OverflowException">
		///   <paramref name="value" /> is greater than <see cref="F:System.Char.MaxValue" />. </exception>
		/// <filterpriority>1</filterpriority>
		[CLSCompliant(false)]
		public static char ToChar(ulong value)
		{
			if (value > 65535)
			{
				throw new OverflowException(Locale.GetText("Value is greater than Char.MaxValue"));
			}
			return (char)value;
		}

		/// <summary>Converts the value of the specified 16-bit unsigned integer to its equivalent Unicode character.</summary>
		/// <returns>The Unicode character equivalent to the value of <paramref name="value" />.</returns>
		/// <param name="value">A 16-bit unsigned integer. </param>
		/// <filterpriority>1</filterpriority>
		[CLSCompliant(false)]
		public static char ToChar(ushort value)
		{
			return (char)value;
		}

		/// <summary>Converts the value of the specified <see cref="T:System.Object" /> to a Unicode character.</summary>
		/// <returns>The Unicode character equivalent to the value of <paramref name="value" />.-or- <see cref="F:System.Char.MinValue" /> if <paramref name="value" /> equals null.</returns>
		/// <param name="value">An <see cref="T:System.Object" /> that implements the <see cref="T:System.IConvertible" /> interface. </param>
		/// <exception cref="T:System.InvalidCastException">
		///   <paramref name="value" /> does not implement the <see cref="T:System.IConvertible" /> interface. </exception>
		/// <filterpriority>1</filterpriority>
		public static char ToChar(object value)
		{
			if (value == null)
			{
				return '\0';
			}
			return ToChar(value, null);
		}

		/// <summary>Converts the value of the specified <see cref="T:System.Object" /> to its equivalent Unicode character using the specified culture-specific formatting information.</summary>
		/// <returns>The Unicode character equivalent to the value of <paramref name="value" />.-or- <see cref="F:System.Char.MinValue" /> if <paramref name="value" /> equals null.</returns>
		/// <param name="value">An <see cref="T:System.Object" /> that implements the <see cref="T:System.IConvertible" /> interface. </param>
		/// <param name="provider">An <see cref="T:System.IFormatProvider" /> interface implementation that supplies culture-specific formatting information. </param>
		/// <exception cref="T:System.InvalidCastException">
		///   <paramref name="value" /> does not implement the <see cref="T:System.IConvertible" /> interface. </exception>
		/// <filterpriority>1</filterpriority>
		public static char ToChar(object value, IFormatProvider provider)
		{
			if (value == null)
			{
				return '\0';
			}
			return ((IConvertible)value).ToChar(provider);
		}

		/// <summary>Converts the specified <see cref="T:System.String" /> representation of a date and time to an equivalent <see cref="T:System.DateTime" />.</summary>
		/// <returns>A <see cref="T:System.DateTime" /> equivalent to the value of <paramref name="value" />.-or- A <see cref="T:System.DateTime" /> equivalent to <see cref="F:System.DateTime.MinValue" /> if <paramref name="value" /> is null.</returns>
		/// <param name="value">The string representation of a date and time.</param>
		/// <exception cref="T:System.FormatException">
		///   <paramref name="value" /> is not a properly formatted date and time string. </exception>
		/// <filterpriority>1</filterpriority>
		public static DateTime ToDateTime(string value)
		{
			if (value == null)
			{
				return DateTime.MinValue;
			}
			return DateTime.Parse(value);
		}

		/// <summary>Converts the specified <see cref="T:System.String" /> representation of a number to an equivalent <see cref="T:System.DateTime" /> using the specified culture-specific formatting information.</summary>
		/// <returns>A <see cref="T:System.DateTime" /> equivalent to the value of <paramref name="value" />.-or- A <see cref="T:System.DateTime" /> equivalent to <see cref="F:System.DateTime.MinValue" /> if <paramref name="value" /> is null.</returns>
		/// <param name="value">A <see cref="T:System.String" /> containing a date and time to convert. </param>
		/// <param name="provider">An <see cref="T:System.IFormatProvider" /> interface implementation that supplies culture-specific formatting information. </param>
		/// <exception cref="T:System.FormatException">
		///   <paramref name="value" /> is not a properly formatted date and time string. </exception>
		/// <filterpriority>1</filterpriority>
		public static DateTime ToDateTime(string value, IFormatProvider provider)
		{
			if (value == null)
			{
				return DateTime.MinValue;
			}
			return DateTime.Parse(value, provider);
		}

		/// <summary>Calling this method always throws <see cref="T:System.InvalidCastException" />.</summary>
		/// <returns>This conversion is not supported. No value is returned.</returns>
		/// <param name="value">A Boolean value. </param>
		/// <exception cref="T:System.InvalidCastException">This conversion is not supported. </exception>
		/// <filterpriority>1</filterpriority>
		public static DateTime ToDateTime(bool value)
		{
			throw new InvalidCastException("This conversion is not supported.");
		}

		/// <summary>Calling this method always throws <see cref="T:System.InvalidCastException" />.</summary>
		/// <returns>This conversion is not supported. No value is returned.</returns>
		/// <param name="value">An 8-bit unsigned integer. </param>
		/// <exception cref="T:System.InvalidCastException">This conversion is not supported. </exception>
		/// <filterpriority>1</filterpriority>
		public static DateTime ToDateTime(byte value)
		{
			throw new InvalidCastException("This conversion is not supported.");
		}

		/// <summary>Calling this method always throws <see cref="T:System.InvalidCastException" />.</summary>
		/// <returns>This conversion is not supported. No value is returned.</returns>
		/// <param name="value">A Unicode character. </param>
		/// <exception cref="T:System.InvalidCastException">This conversion is not supported. </exception>
		/// <filterpriority>1</filterpriority>
		public static DateTime ToDateTime(char value)
		{
			throw new InvalidCastException("This conversion is not supported.");
		}

		/// <summary>Returns the specified <see cref="T:System.DateTime" />; no actual conversion is performed.</summary>
		/// <returns>Parameter <paramref name="value" /> is returned unchanged.</returns>
		/// <param name="value">A <see cref="T:System.DateTime" />. </param>
		/// <filterpriority>1</filterpriority>
		public static DateTime ToDateTime(DateTime value)
		{
			return value;
		}

		/// <summary>Calling this method always throws <see cref="T:System.InvalidCastException" />.</summary>
		/// <returns>This conversion is not supported. No value is returned.</returns>
		/// <param name="value">A <see cref="T:System.Decimal" /> value. </param>
		/// <exception cref="T:System.InvalidCastException">This conversion is not supported. </exception>
		/// <filterpriority>1</filterpriority>
		public static DateTime ToDateTime(decimal value)
		{
			throw new InvalidCastException("This conversion is not supported.");
		}

		/// <summary>Calling this method always throws <see cref="T:System.InvalidCastException" />.</summary>
		/// <returns>This conversion is not supported. No value is returned.</returns>
		/// <param name="value">A double-precision floating point value. </param>
		/// <exception cref="T:System.InvalidCastException">This conversion is not supported. </exception>
		/// <filterpriority>1</filterpriority>
		public static DateTime ToDateTime(double value)
		{
			throw new InvalidCastException("This conversion is not supported.");
		}

		/// <summary>Calling this method always throws <see cref="T:System.InvalidCastException" />.</summary>
		/// <returns>This conversion is not supported. No value is returned.</returns>
		/// <param name="value">A 16-bit signed integer. </param>
		/// <exception cref="T:System.InvalidCastException">This conversion is not supported. </exception>
		/// <filterpriority>1</filterpriority>
		public static DateTime ToDateTime(short value)
		{
			throw new InvalidCastException("This conversion is not supported.");
		}

		/// <summary>Calling this method always throws <see cref="T:System.InvalidCastException" />.</summary>
		/// <returns>This conversion is not supported. No value is returned.</returns>
		/// <param name="value">A 32-bit signed integer. </param>
		/// <exception cref="T:System.InvalidCastException">This conversion is not supported. </exception>
		/// <filterpriority>1</filterpriority>
		public static DateTime ToDateTime(int value)
		{
			throw new InvalidCastException("This conversion is not supported.");
		}

		/// <summary>Calling this method always throws <see cref="T:System.InvalidCastException" />.</summary>
		/// <returns>This conversion is not supported. No value is returned.</returns>
		/// <param name="value">A 64-bit signed integer. </param>
		/// <exception cref="T:System.InvalidCastException">This conversion is not supported. </exception>
		/// <filterpriority>1</filterpriority>
		public static DateTime ToDateTime(long value)
		{
			throw new InvalidCastException("This conversion is not supported.");
		}

		/// <summary>Calling this method always throws <see cref="T:System.InvalidCastException" />.</summary>
		/// <returns>This conversion is not supported. No value is returned.</returns>
		/// <param name="value">A single-precision floating point value. </param>
		/// <exception cref="T:System.InvalidCastException">This conversion is not supported. </exception>
		/// <filterpriority>1</filterpriority>
		public static DateTime ToDateTime(float value)
		{
			throw new InvalidCastException("This conversion is not supported.");
		}

		/// <summary>Converts the value of the specified <see cref="T:System.Object" /> to a <see cref="T:System.DateTime" />.</summary>
		/// <returns>A <see cref="T:System.DateTime" /> equivalent to the value of <paramref name="value" />.-or- A <see cref="T:System.DateTime" /> equivalent to <see cref="F:System.DateTime.MinValue" /> if <paramref name="value" /> is null.</returns>
		/// <param name="value">An <see cref="T:System.Object" /> that implements the <see cref="T:System.IConvertible" /> interface or null. </param>
		/// <exception cref="T:System.InvalidCastException">
		///   <paramref name="value" /> does not implement <see cref="T:System.IConvertible" />. </exception>
		/// <filterpriority>1</filterpriority>
		public static DateTime ToDateTime(object value)
		{
			if (value == null)
			{
				return DateTime.MinValue;
			}
			return ToDateTime(value, null);
		}

		/// <summary>Converts the value of the specified <see cref="T:System.Object" /> to a <see cref="T:System.DateTime" /> using the specified culture-specific formatting information.</summary>
		/// <returns>A <see cref="T:System.DateTime" /> equivalent to the value of <paramref name="value" />.-or- A <see cref="T:System.DateTime" /> equivalent to <see cref="F:System.DateTime.MinValue" /> if <paramref name="value" /> is null.</returns>
		/// <param name="value">An <see cref="T:System.Object" /> that implements the <see cref="T:System.IConvertible" /> interface. </param>
		/// <param name="provider">An <see cref="T:System.IFormatProvider" /> interface implementation that supplies culture-specific formatting information. </param>
		/// <exception cref="T:System.InvalidCastException">
		///   <paramref name="value" /> does not implement <see cref="T:System.IConvertible" />. </exception>
		/// <filterpriority>1</filterpriority>
		public static DateTime ToDateTime(object value, IFormatProvider provider)
		{
			if (value == null)
			{
				return DateTime.MinValue;
			}
			return ((IConvertible)value).ToDateTime(provider);
		}

		/// <summary>Calling this method always throws <see cref="T:System.InvalidCastException" />.</summary>
		/// <returns>This conversion is not supported. No value is returned.</returns>
		/// <param name="value">An 8-bit signed integer. </param>
		/// <exception cref="T:System.InvalidCastException">This conversion is not supported. </exception>
		/// <filterpriority>1</filterpriority>
		[CLSCompliant(false)]
		public static DateTime ToDateTime(sbyte value)
		{
			throw new InvalidCastException("This conversion is not supported.");
		}

		/// <summary>Calling this method always throws <see cref="T:System.InvalidCastException" />.</summary>
		/// <returns>This conversion is not supported. No value is returned.</returns>
		/// <param name="value">A 16-bit unsigned integer. </param>
		/// <exception cref="T:System.InvalidCastException">This conversion is not supported. </exception>
		/// <filterpriority>1</filterpriority>
		[CLSCompliant(false)]
		public static DateTime ToDateTime(ushort value)
		{
			throw new InvalidCastException("This conversion is not supported.");
		}

		/// <summary>Calling this method always throws <see cref="T:System.InvalidCastException" />.</summary>
		/// <returns>This conversion is not supported. No value is returned.</returns>
		/// <param name="value">A 32-bit unsigned integer. </param>
		/// <exception cref="T:System.InvalidCastException">This conversion is not supported. </exception>
		/// <filterpriority>1</filterpriority>
		[CLSCompliant(false)]
		public static DateTime ToDateTime(uint value)
		{
			throw new InvalidCastException("This conversion is not supported.");
		}

		/// <summary>Calling this method always throws <see cref="T:System.InvalidCastException" />.</summary>
		/// <returns>This conversion is not supported. No value is returned.</returns>
		/// <param name="value">A 64-bit unsigned integer. </param>
		/// <exception cref="T:System.InvalidCastException">This conversion is not supported. </exception>
		/// <filterpriority>1</filterpriority>
		[CLSCompliant(false)]
		public static DateTime ToDateTime(ulong value)
		{
			throw new InvalidCastException("This conversion is not supported.");
		}

		/// <summary>Converts the value of the specified Boolean value to the equivalent <see cref="T:System.Decimal" /> number.</summary>
		/// <returns>The number 1 if <paramref name="value" /> is true; otherwise, 0.</returns>
		/// <param name="value">A Boolean value. </param>
		/// <filterpriority>1</filterpriority>
		public static decimal ToDecimal(bool value)
		{
			return value ? 1 : 0;
		}

		/// <summary>Converts the value of the specified 8-bit unsigned integer to the equivalent <see cref="T:System.Decimal" /> number.</summary>
		/// <returns>The <see cref="T:System.Decimal" /> number equivalent to the value of <paramref name="value" />.</returns>
		/// <param name="value">An 8-bit unsigned integer. </param>
		/// <filterpriority>1</filterpriority>
		public static decimal ToDecimal(byte value)
		{
			return value;
		}

		/// <summary>Calling this method always throws <see cref="T:System.InvalidCastException" />.</summary>
		/// <returns>This conversion is not supported. No value is returned.</returns>
		/// <param name="value">A Unicode character. </param>
		/// <exception cref="T:System.InvalidCastException">This conversion is not supported. </exception>
		/// <filterpriority>1</filterpriority>
		public static decimal ToDecimal(char value)
		{
			throw new InvalidCastException("This conversion is not supported.");
		}

		/// <summary>Calling this method always throws <see cref="T:System.InvalidCastException" />.</summary>
		/// <returns>This conversion is not supported. No value is returned.</returns>
		/// <param name="value">A <see cref="T:System.DateTime" />. </param>
		/// <exception cref="T:System.InvalidCastException">This conversion is not supported. </exception>
		/// <filterpriority>1</filterpriority>
		public static decimal ToDecimal(DateTime value)
		{
			throw new InvalidCastException("This conversion is not supported.");
		}

		/// <summary>Returns the specified <see cref="T:System.Decimal" /> number; no actual conversion is performed.</summary>
		/// <returns>Parameter <paramref name="value" /> is returned unchanged.</returns>
		/// <param name="value">A <see cref="T:System.Decimal" /> number. </param>
		/// <filterpriority>1</filterpriority>
		public static decimal ToDecimal(decimal value)
		{
			return value;
		}

		/// <summary>Converts the value of the specified double-precision floating point number to an equivalent <see cref="T:System.Decimal" /> number.</summary>
		/// <returns>A <see cref="T:System.Decimal" /> number equivalent to the value of <paramref name="value" />. The <see cref="T:System.Decimal" /> contains 15 significant digits and is rounded using rounding to nearest. For example, when rounded to two decimals, the value 2.345 becomes 2.34 and the value 2.355 becomes 2.36.</returns>
		/// <param name="value">A double-precision floating point number. </param>
		/// <exception cref="T:System.OverflowException">The numeric value of <paramref name="value" /> is greater than <see cref="F:System.Decimal.MaxValue" /> or less than <see cref="F:System.Decimal.MinValue" />. </exception>
		/// <filterpriority>1</filterpriority>
		public static decimal ToDecimal(double value)
		{
			return (decimal)value;
		}

		/// <summary>Converts the value of the specified single-precision floating point number to the equivalent <see cref="T:System.Decimal" /> number.</summary>
		/// <returns>A <see cref="T:System.Decimal" /> number equivalent to the value of <paramref name="value" />. The <see cref="T:System.Decimal" /> contains 7 significant digits and is rounded using rounding to nearest. For example, when rounded to two decimals, the value 2.345 becomes 2.34 and the value 2.355 becomes 2.36.</returns>
		/// <param name="value">A single-precision floating point number. </param>
		/// <filterpriority>1</filterpriority>
		public static decimal ToDecimal(float value)
		{
			return (decimal)value;
		}

		/// <summary>Converts the value of the specified 32-bit signed integer to an equivalent <see cref="T:System.Decimal" /> number.</summary>
		/// <returns>A <see cref="T:System.Decimal" /> number equivalent to the value of <paramref name="value" />.</returns>
		/// <param name="value">A 32-bit signed integer. </param>
		/// <filterpriority>1</filterpriority>
		public static decimal ToDecimal(int value)
		{
			return value;
		}

		/// <summary>Converts the value of the specified 64-bit signed integer to an equivalent <see cref="T:System.Decimal" /> number.</summary>
		/// <returns>A <see cref="T:System.Decimal" /> number equivalent to the value of <paramref name="value" />.</returns>
		/// <param name="value">A 64-bit signed integer. </param>
		/// <filterpriority>1</filterpriority>
		public static decimal ToDecimal(long value)
		{
			return value;
		}

		/// <summary>Converts the value of the specified 8-bit signed integer to the equivalent <see cref="T:System.Decimal" /> number.</summary>
		/// <returns>The 8-bit signed integer equivalent to the value of <paramref name="value" />.</returns>
		/// <param name="value">An 8-bit signed integer. </param>
		/// <filterpriority>1</filterpriority>
		[CLSCompliant(false)]
		public static decimal ToDecimal(sbyte value)
		{
			return value;
		}

		/// <summary>Converts the value of the specified 16-bit signed integer to an equivalent <see cref="T:System.Decimal" /> number.</summary>
		/// <returns>A <see cref="T:System.Decimal" /> number equivalent to the value of <paramref name="value" />.</returns>
		/// <param name="value">A 16-bit signed integer. </param>
		/// <filterpriority>1</filterpriority>
		public static decimal ToDecimal(short value)
		{
			return value;
		}

		/// <summary>Converts the specified <see cref="T:System.String" /> representation of a number to an equivalent <see cref="T:System.Decimal" /> number.</summary>
		/// <returns>A <see cref="T:System.Decimal" /> number equivalent to the value of <paramref name="value" />.-or- Zero if <paramref name="value" /> is null.</returns>
		/// <param name="value">A <see cref="T:System.String" /> containing a number to convert. </param>
		/// <exception cref="T:System.FormatException">
		///   <paramref name="value" /> is not a number in a valid format.</exception>
		/// <exception cref="T:System.OverflowException">
		///   <paramref name="value" /> represents a number less than <see cref="F:System.Decimal.MinValue" /> or greater than <see cref="F:System.Decimal.MaxValue" />. </exception>
		/// <filterpriority>1</filterpriority>
		public static decimal ToDecimal(string value)
		{
			if (value == null)
			{
				return 0m;
			}
			return decimal.Parse(value);
		}

		/// <summary>Converts the specified <see cref="T:System.String" /> representation of a number to an equivalent <see cref="T:System.Decimal" /> number using the specified culture-specific formatting information.</summary>
		/// <returns>A <see cref="T:System.Decimal" /> number equivalent to the value of <paramref name="value" />.-or- Zero if <paramref name="value" /> is null.</returns>
		/// <param name="value">A <see cref="T:System.String" /> containing a number to convert. </param>
		/// <param name="provider">An <see cref="T:System.IFormatProvider" /> interface implementation that supplies culture-specific formatting information. </param>
		/// <exception cref="T:System.FormatException">
		///   <paramref name="value" /> is not a number in a valid format.</exception>
		/// <exception cref="T:System.OverflowException">
		///   <paramref name="value" /> represents a number less than <see cref="F:System.Decimal.MinValue" /> or greater than <see cref="F:System.Decimal.MaxValue" />. </exception>
		/// <filterpriority>1</filterpriority>
		public static decimal ToDecimal(string value, IFormatProvider provider)
		{
			if (value == null)
			{
				return 0m;
			}
			return decimal.Parse(value, provider);
		}

		/// <summary>Converts the value of the specified 32-bit unsigned integer to an equivalent <see cref="T:System.Decimal" /> number.</summary>
		/// <returns>A <see cref="T:System.Decimal" /> number equivalent to the value of <paramref name="value" />.</returns>
		/// <param name="value">A 32-bit unsigned integer. </param>
		/// <filterpriority>1</filterpriority>
		[CLSCompliant(false)]
		public static decimal ToDecimal(uint value)
		{
			return value;
		}

		/// <summary>Converts the value of the specified 64-bit unsigned integer to an equivalent <see cref="T:System.Decimal" /> number.</summary>
		/// <returns>A <see cref="T:System.Decimal" /> number equivalent to the value of <paramref name="value" />.</returns>
		/// <param name="value">A 64-bit unsigned integer. </param>
		/// <filterpriority>1</filterpriority>
		[CLSCompliant(false)]
		public static decimal ToDecimal(ulong value)
		{
			return value;
		}

		/// <summary>Converts the value of the specified 16-bit unsigned integer to the equivalent <see cref="T:System.Decimal" /> number.</summary>
		/// <returns>The <see cref="T:System.Decimal" /> number equivalent to the value of <paramref name="value" />.</returns>
		/// <param name="value">A 16-bit unsigned integer. </param>
		/// <filterpriority>1</filterpriority>
		[CLSCompliant(false)]
		public static decimal ToDecimal(ushort value)
		{
			return value;
		}

		/// <summary>Converts the value of the specified <see cref="T:System.Object" /> to a <see cref="T:System.Decimal" /> number.</summary>
		/// <returns>A <see cref="T:System.Decimal" /> number equivalent to the value of <paramref name="value" />, or zero if <paramref name="value" /> is null.</returns>
		/// <param name="value">An <see cref="T:System.Object" /> that implements the <see cref="T:System.IConvertible" /> interface or null. </param>
		/// <exception cref="T:System.InvalidCastException">
		///   <paramref name="value" /> does not implement <see cref="T:System.IConvertible" />. </exception>
		/// <filterpriority>1</filterpriority>
		public static decimal ToDecimal(object value)
		{
			if (value == null)
			{
				return 0m;
			}
			return ToDecimal(value, null);
		}

		/// <summary>Converts the value of the specified <see cref="T:System.Object" /> to an <see cref="T:System.Decimal" /> number using the specified culture-specific formatting information.</summary>
		/// <returns>A <see cref="T:System.Decimal" /> number equivalent to the value of <paramref name="value" />, or zero if <paramref name="value" /> is null.</returns>
		/// <param name="value">An <see cref="T:System.Object" /> that implements the <see cref="T:System.IConvertible" /> interface. </param>
		/// <param name="provider">An <see cref="T:System.IFormatProvider" /> interface implementation that supplies culture-specific formatting information. </param>
		/// <exception cref="T:System.InvalidCastException">
		///   <paramref name="value" /> does not implement <see cref="T:System.IConvertible" />. </exception>
		/// <filterpriority>1</filterpriority>
		public static decimal ToDecimal(object value, IFormatProvider provider)
		{
			if (value == null)
			{
				return 0m;
			}
			return ((IConvertible)value).ToDecimal(provider);
		}

		/// <summary>Converts the value of the specified Boolean value to the equivalent double-precision floating point number.</summary>
		/// <returns>The number 1 if <paramref name="value" /> is true; otherwise, 0.</returns>
		/// <param name="value">A Boolean value. </param>
		/// <filterpriority>1</filterpriority>
		public static double ToDouble(bool value)
		{
			return value ? 1 : 0;
		}

		/// <summary>Converts the value of the specified 8-bit unsigned integer to the equivalent double-precision floating point number.</summary>
		/// <returns>The double-precision floating point number equivalent to the value of <paramref name="value" />.</returns>
		/// <param name="value">An 8-bit unsigned integer. </param>
		/// <filterpriority>1</filterpriority>
		public static double ToDouble(byte value)
		{
			return (int)value;
		}

		/// <summary>Calling this method always throws <see cref="T:System.InvalidCastException" />.</summary>
		/// <returns>This conversion is not supported. No value is returned.</returns>
		/// <param name="value">A Unicode character. </param>
		/// <exception cref="T:System.InvalidCastException">This conversion is not supported. </exception>
		/// <filterpriority>1</filterpriority>
		public static double ToDouble(char value)
		{
			throw new InvalidCastException("This conversion is not supported.");
		}

		/// <summary>Calling this method always throws <see cref="T:System.InvalidCastException" />.</summary>
		/// <returns>This conversion is not supported. No value is returned.</returns>
		/// <param name="value">A <see cref="T:System.DateTime" />. </param>
		/// <exception cref="T:System.InvalidCastException">This conversion is not supported. </exception>
		/// <filterpriority>1</filterpriority>
		public static double ToDouble(DateTime value)
		{
			throw new InvalidCastException("This conversion is not supported.");
		}

		/// <summary>Converts the value of the specified <see cref="T:System.Decimal" /> number to an equivalent double-precision floating point number.</summary>
		/// <returns>A double-precision floating point number equivalent to the value of <paramref name="value" />.</returns>
		/// <param name="value">A <see cref="T:System.Decimal" /> number. </param>
		/// <filterpriority>1</filterpriority>
		public static double ToDouble(decimal value)
		{
			return (double)value;
		}

		/// <summary>Returns the specified double-precision floating point number; no actual conversion is performed.</summary>
		/// <returns>Parameter <paramref name="value" /> is returned unchanged.</returns>
		/// <param name="value">A double-precision floating point number. </param>
		/// <filterpriority>1</filterpriority>
		public static double ToDouble(double value)
		{
			return value;
		}

		/// <summary>Converts the value of the specified single-precision floating point number to an equivalent double-precision floating point number.</summary>
		/// <returns>A double-precision floating point number equivalent to the value of <paramref name="value" />.</returns>
		/// <param name="value">A single-precision floating point number. </param>
		/// <filterpriority>1</filterpriority>
		public static double ToDouble(float value)
		{
			return value;
		}

		/// <summary>Converts the value of the specified 32-bit signed integer to an equivalent double-precision floating point number.</summary>
		/// <returns>A double-precision floating point number equivalent to the value of <paramref name="value" />.</returns>
		/// <param name="value">A 32-bit signed integer. </param>
		/// <filterpriority>1</filterpriority>
		public static double ToDouble(int value)
		{
			return value;
		}

		/// <summary>Converts the value of the specified 64-bit signed integer to an equivalent double-precision floating point number.</summary>
		/// <returns>A double-precision floating point number equivalent to the value of <paramref name="value" />.</returns>
		/// <param name="value">A 64-bit signed integer. </param>
		/// <filterpriority>1</filterpriority>
		public static double ToDouble(long value)
		{
			return value;
		}

		/// <summary>Converts the value of the specified 8-bit signed integer to the equivalent double-precision floating point number.</summary>
		/// <returns>The 8-bit signed integer equivalent to the value of <paramref name="value" />.</returns>
		/// <param name="value">An 8-bit signed integer. </param>
		/// <filterpriority>1</filterpriority>
		[CLSCompliant(false)]
		public static double ToDouble(sbyte value)
		{
			return value;
		}

		/// <summary>Converts the value of the specified 16-bit signed integer to an equivalent double-precision floating point number.</summary>
		/// <returns>A double-precision floating point number equivalent to the value of <paramref name="value" />.</returns>
		/// <param name="value">A 16-bit signed integer. </param>
		/// <filterpriority>1</filterpriority>
		public static double ToDouble(short value)
		{
			return value;
		}

		/// <summary>Converts the specified <see cref="T:System.String" /> representation of a number to an equivalent double-precision floating point number.</summary>
		/// <returns>A double-precision floating point number equivalent to the value of <paramref name="value" />.-or- Zero if <paramref name="value" /> is null.</returns>
		/// <param name="value">A <see cref="T:System.String" /> containing a number to convert. </param>
		/// <exception cref="T:System.FormatException">
		///   <paramref name="value" /> is not a number in a valid format.</exception>
		/// <exception cref="T:System.OverflowException">
		///   <paramref name="value" /> represents a number less than <see cref="F:System.Double.MinValue" /> or greater than <see cref="F:System.Double.MaxValue" />. </exception>
		/// <filterpriority>1</filterpriority>
		public static double ToDouble(string value)
		{
			if (value == null)
			{
				return 0.0;
			}
			return double.Parse(value);
		}

		/// <summary>Converts the specified <see cref="T:System.String" /> representation of a number to an equivalent double-precision floating point number using the specified culture-specific formatting information.</summary>
		/// <returns>A double-precision floating point number equivalent to the value of <paramref name="value" />.-or- Zero if <paramref name="value" /> is null.</returns>
		/// <param name="value">A <see cref="T:System.String" /> containing a number to convert. </param>
		/// <param name="provider">An <see cref="T:System.IFormatProvider" /> interface implementation that supplies culture-specific formatting information. </param>
		/// <exception cref="T:System.FormatException">
		///   <paramref name="value" /> is not a number in a valid format.</exception>
		/// <exception cref="T:System.OverflowException">
		///   <paramref name="value" /> represents a number less than <see cref="F:System.Double.MinValue" /> or greater than <see cref="F:System.Double.MaxValue" />. </exception>
		/// <filterpriority>1</filterpriority>
		public static double ToDouble(string value, IFormatProvider provider)
		{
			if (value == null)
			{
				return 0.0;
			}
			return double.Parse(value, provider);
		}

		/// <summary>Converts the value of the specified 32-bit unsigned integer to an equivalent double-precision floating point number.</summary>
		/// <returns>A double-precision floating point number equivalent to the value of <paramref name="value" />.</returns>
		/// <param name="value">A 32-bit unsigned integer. </param>
		/// <filterpriority>1</filterpriority>
		[CLSCompliant(false)]
		public static double ToDouble(uint value)
		{
			return value;
		}

		/// <summary>Converts the value of the specified 64-bit unsigned integer to an equivalent double-precision floating point number.</summary>
		/// <returns>A double-precision floating point number equivalent to the value of <paramref name="value" />.</returns>
		/// <param name="value">A 64-bit unsigned integer. </param>
		/// <filterpriority>1</filterpriority>
		[CLSCompliant(false)]
		public static double ToDouble(ulong value)
		{
			return value;
		}

		/// <summary>Converts the value of the specified 16-bit unsigned integer to the equivalent double-precision floating point number.</summary>
		/// <returns>The double-precision floating point number equivalent to the value of <paramref name="value" />.</returns>
		/// <param name="value">A 16-bit unsigned integer. </param>
		/// <filterpriority>1</filterpriority>
		[CLSCompliant(false)]
		public static double ToDouble(ushort value)
		{
			return (int)value;
		}

		/// <summary>Converts the value of the specified <see cref="T:System.Object" /> to a double-precision floating point number.</summary>
		/// <returns>A double-precision floating point number equivalent to the value of <paramref name="value" />, or zero if <paramref name="value" /> is null.</returns>
		/// <param name="value">An <see cref="T:System.Object" /> that implements the <see cref="T:System.IConvertible" /> interface or null. </param>
		/// <exception cref="T:System.InvalidCastException">
		///   <paramref name="value" /> does not implement <see cref="T:System.IConvertible" />. </exception>
		/// <filterpriority>1</filterpriority>
		public static double ToDouble(object value)
		{
			if (value == null)
			{
				return 0.0;
			}
			return ToDouble(value, null);
		}

		/// <summary>Converts the value of the specified <see cref="T:System.Object" /> to an double-precision floating point number using the specified culture-specific formatting information.</summary>
		/// <returns>A double-precision floating point number equivalent to the value of <paramref name="value" />, or zero if <paramref name="value" /> is null.</returns>
		/// <param name="value">An <see cref="T:System.Object" /> that implements the <see cref="T:System.IConvertible" /> interface. </param>
		/// <param name="provider">An <see cref="T:System.IFormatProvider" /> interface implementation that supplies culture-specific formatting information. </param>
		/// <exception cref="T:System.InvalidCastException">
		///   <paramref name="value" /> does not implement <see cref="T:System.IConvertible" />. </exception>
		/// <filterpriority>1</filterpriority>
		public static double ToDouble(object value, IFormatProvider provider)
		{
			if (value == null)
			{
				return 0.0;
			}
			return ((IConvertible)value).ToDouble(provider);
		}

		/// <summary>Converts the value of the specified Boolean value to the equivalent 16-bit signed integer.</summary>
		/// <returns>The number 1 if <paramref name="value" /> is true; otherwise, 0.</returns>
		/// <param name="value">A Boolean value. </param>
		/// <filterpriority>1</filterpriority>
		public static short ToInt16(bool value)
		{
			return (short)(value ? 1 : 0);
		}

		/// <summary>Converts the value of the specified 8-bit unsigned integer to the equivalent 16-bit signed integer.</summary>
		/// <returns>The 16-bit signed integer equivalent to the value of <paramref name="value" />.</returns>
		/// <param name="value">An 8-bit unsigned integer. </param>
		/// <filterpriority>1</filterpriority>
		public static short ToInt16(byte value)
		{
			return value;
		}

		/// <summary>Converts the value of the specified Unicode character to the equivalent 16-bit signed integer.</summary>
		/// <returns>The 16-bit signed integer equivalent to <paramref name="value" />.</returns>
		/// <param name="value">A Unicode character. </param>
		/// <exception cref="T:System.OverflowException">
		///   <paramref name="value" /> is greater than <see cref="F:System.Int16.MaxValue" />. </exception>
		/// <filterpriority>1</filterpriority>
		public static short ToInt16(char value)
		{
			if (value > '翿')
			{
				throw new OverflowException(Locale.GetText("Value is greater than Int16.MaxValue"));
			}
			return (short)value;
		}

		/// <summary>Calling this method always throws <see cref="T:System.InvalidCastException" />.</summary>
		/// <returns>This conversion is not supported. No value is returned.</returns>
		/// <param name="value">A <see cref="T:System.DateTime" />. </param>
		/// <exception cref="T:System.InvalidCastException">This conversion is not supported. </exception>
		/// <filterpriority>1</filterpriority>
		public static short ToInt16(DateTime value)
		{
			throw new InvalidCastException("This conversion is not supported.");
		}

		/// <summary>Converts the value of the specified <see cref="T:System.Decimal" /> number to an equivalent 16-bit signed integer.</summary>
		/// <returns>
		///   <paramref name="value" /> rounded to the nearest 16-bit signed integer. If <paramref name="value" /> is halfway between two whole numbers, the even number is returned; that is, 4.5 is converted to 4, and 5.5 is converted to 6.</returns>
		/// <param name="value">A <see cref="T:System.Decimal" /> number. </param>
		/// <exception cref="T:System.OverflowException">
		///   <paramref name="value" /> is greater than <see cref="F:System.Int16.MaxValue" /> or less than <see cref="F:System.Int16.MinValue" />. </exception>
		/// <filterpriority>1</filterpriority>
		public static short ToInt16(decimal value)
		{
			if (value > 32767m || value < -32768m)
			{
				throw new OverflowException(Locale.GetText("Value is greater than Int16.MaxValue or less than Int16.MinValue"));
			}
			return (short)Math.Round(value);
		}

		/// <summary>Converts the value of the specified double-precision floating point number to an equivalent 16-bit signed integer.</summary>
		/// <returns>
		///   <paramref name="value" /> rounded to the nearest 16-bit signed integer. If <paramref name="value" /> is halfway between two whole numbers, the even number is returned; that is, 4.5 is converted to 4, and 5.5 is converted to 6.</returns>
		/// <param name="value">A double-precision floating point number. </param>
		/// <exception cref="T:System.OverflowException">
		///   <paramref name="value" /> is greater than <see cref="F:System.Int16.MaxValue" /> or less than <see cref="F:System.Int16.MinValue" />. </exception>
		/// <filterpriority>1</filterpriority>
		public static short ToInt16(double value)
		{
			if (value > 32767.0 || value < -32768.0)
			{
				throw new OverflowException(Locale.GetText("Value is greater than Int16.MaxValue or less than Int16.MinValue"));
			}
			return (short)Math.Round(value);
		}

		/// <summary>Converts the value of the specified single-precision floating point number to an equivalent 16-bit signed integer.</summary>
		/// <returns>
		///   <paramref name="value" /> rounded to the nearest 16-bit signed integer. If <paramref name="value" /> is halfway between two whole numbers, the even number is returned; that is, 4.5 is converted to 4, and 5.5 is converted to 6.</returns>
		/// <param name="value">A single-precision floating point number. </param>
		/// <exception cref="T:System.OverflowException">
		///   <paramref name="value" /> is greater than <see cref="F:System.Int16.MaxValue" /> or less than <see cref="F:System.Int16.MinValue" />. </exception>
		/// <filterpriority>1</filterpriority>
		public static short ToInt16(float value)
		{
			if (value > 32767f || value < -32768f)
			{
				throw new OverflowException(Locale.GetText("Value is greater than Int16.MaxValue or less than Int16.MinValue"));
			}
			return (short)Math.Round(value);
		}

		/// <summary>Converts the value of the specified 32-bit signed integer to an equivalent 16-bit signed integer.</summary>
		/// <returns>The 16-bit signed integer equivalent of <paramref name="value" />.</returns>
		/// <param name="value">A 32-bit signed integer. </param>
		/// <exception cref="T:System.OverflowException">
		///   <paramref name="value" /> is greater than <see cref="F:System.Int16.MaxValue" /> or less than <see cref="F:System.Int16.MinValue" />. </exception>
		/// <filterpriority>1</filterpriority>
		public static short ToInt16(int value)
		{
			if (value > 32767 || value < -32768)
			{
				throw new OverflowException(Locale.GetText("Value is greater than Int16.MaxValue or less than Int16.MinValue"));
			}
			return (short)value;
		}

		/// <summary>Converts the value of the specified 64-bit signed integer to an equivalent 16-bit signed integer.</summary>
		/// <returns>A 16-bit signed integer equivalent to the value of <paramref name="value" />.</returns>
		/// <param name="value">A 64-bit signed integer. </param>
		/// <exception cref="T:System.OverflowException">
		///   <paramref name="value" /> is greater than <see cref="F:System.Int16.MaxValue" /> or less than <see cref="F:System.Int16.MinValue" />. </exception>
		/// <filterpriority>1</filterpriority>
		public static short ToInt16(long value)
		{
			if (value > 32767 || value < -32768)
			{
				throw new OverflowException(Locale.GetText("Value is greater than Int16.MaxValue or less than Int16.MinValue"));
			}
			return (short)value;
		}

		/// <summary>Converts the value of the specified 8-bit signed integer to the equivalent 16-bit signed integer.</summary>
		/// <returns>The 8-bit signed integer equivalent to the value of <paramref name="value" />.</returns>
		/// <param name="value">An 8-bit signed integer. </param>
		/// <filterpriority>1</filterpriority>
		[CLSCompliant(false)]
		public static short ToInt16(sbyte value)
		{
			return value;
		}

		/// <summary>Returns the specified 16-bit signed integer; no actual conversion is performed.</summary>
		/// <returns>Parameter <paramref name="value" /> is returned unchanged.</returns>
		/// <param name="value">A 16-bit signed integer. </param>
		/// <filterpriority>1</filterpriority>
		public static short ToInt16(short value)
		{
			return value;
		}

		/// <summary>Converts the specified <see cref="T:System.String" /> representation of a number to an equivalent 16-bit signed integer.</summary>
		/// <returns>A 16-bit signed integer equivalent to the value of <paramref name="value" />.-or- Zero if <paramref name="value" /> is null.</returns>
		/// <param name="value">A <see cref="T:System.String" /> containing a number to convert. </param>
		/// <exception cref="T:System.FormatException">
		///   <paramref name="value" /> does not consist of an optional sign followed by a sequence of digits (zero through nine). </exception>
		/// <exception cref="T:System.OverflowException">
		///   <paramref name="value" /> represents a number less than <see cref="F:System.Int16.MinValue" /> or greater than <see cref="F:System.Int16.MaxValue" />. </exception>
		/// <filterpriority>1</filterpriority>
		public static short ToInt16(string value)
		{
			if (value == null)
			{
				return 0;
			}
			return short.Parse(value);
		}

		/// <summary>Converts the specified <see cref="T:System.String" /> representation of a number to an equivalent 16-bit signed integer using specified culture-specific formatting information.</summary>
		/// <returns>A 16-bit signed integer equivalent to the value of <paramref name="value" />.-or- Zero if <paramref name="value" /> is null.</returns>
		/// <param name="value">A <see cref="T:System.String" /> containing a number to convert. </param>
		/// <param name="provider">An <see cref="T:System.IFormatProvider" /> interface implementation that supplies culture-specific formatting information. </param>
		/// <exception cref="T:System.FormatException">
		///   <paramref name="value" /> does not consist of an optional sign followed by a sequence of digits (zero through nine). </exception>
		/// <exception cref="T:System.OverflowException">
		///   <paramref name="value" /> represents a number less than <see cref="F:System.Int16.MinValue" /> or greater than <see cref="F:System.Int16.MaxValue" />. </exception>
		/// <filterpriority>1</filterpriority>
		public static short ToInt16(string value, IFormatProvider provider)
		{
			if (value == null)
			{
				return 0;
			}
			return short.Parse(value, provider);
		}

		/// <summary>Converts the <see cref="T:System.String" /> representation of a number in a specified base to an equivalent 16-bit signed integer.</summary>
		/// <returns>A 16-bit signed integer equivalent to the number in <paramref name="value" />.-or- Zero if <paramref name="value" /> is null.</returns>
		/// <param name="value">A <see cref="T:System.String" /> containing a number. </param>
		/// <param name="fromBase">The base of the number in <paramref name="value" />, which must be 2, 8, 10, or 16. </param>
		/// <exception cref="T:System.ArgumentException">
		///   <paramref name="fromBase" /> is not 2, 8, 10, or 16. -or-<paramref name="value" />, which represents a non-base 10 signed number, is prefixed with a negative sign. </exception>
		/// <exception cref="T:System.FormatException">
		///   <paramref name="value" /> contains a character that is not a valid digit in the base specified by <paramref name="fromBase" />. The exception message indicates that there are no digits to convert if the first character in <paramref name="value" /> is invalid; otherwise, the message indicates that <paramref name="value" /> contains invalid trailing characters.</exception>
		/// <exception cref="T:System.OverflowException">
		///   <paramref name="value" />, which represents a base 10 signed number, is prefixed with a negative sign.-or-The return value is less than <see cref="F:System.Int16.MinValue" /> or larger than <see cref="F:System.Int16.MaxValue" />.</exception>
		/// <filterpriority>1</filterpriority>
		public static short ToInt16(string value, int fromBase)
		{
			int num = ConvertFromBase(value, fromBase, unsigned: false);
			if (fromBase != 10)
			{
				if (num > 65535)
				{
					throw new OverflowException("Value was either too large or too small for an Int16.");
				}
				if (num > 32767)
				{
					return ToInt16(-(65536 - num));
				}
			}
			return ToInt16(num);
		}

		/// <summary>Converts the value of the specified 32-bit unsigned integer to an equivalent 16-bit signed integer.</summary>
		/// <returns>A 16-bit signed integer equivalent to the value of <paramref name="value" />.</returns>
		/// <param name="value">A 32-bit unsigned integer. </param>
		/// <exception cref="T:System.OverflowException">
		///   <paramref name="value" /> is greater than <see cref="F:System.Int16.MaxValue" />. </exception>
		/// <filterpriority>1</filterpriority>
		[CLSCompliant(false)]
		public static short ToInt16(uint value)
		{
			if ((long)value > 32767L)
			{
				throw new OverflowException(Locale.GetText("Value is greater than Int16.MaxValue"));
			}
			return (short)value;
		}

		/// <summary>Converts the value of the specified 64-bit unsigned integer to an equivalent 16-bit signed integer.</summary>
		/// <returns>A 16-bit signed integer equivalent to the value of <paramref name="value" />.</returns>
		/// <param name="value">A 64-bit unsigned integer. </param>
		/// <exception cref="T:System.OverflowException">
		///   <paramref name="value" /> is greater than <see cref="F:System.Int16.MaxValue" />. </exception>
		/// <filterpriority>1</filterpriority>
		[CLSCompliant(false)]
		public static short ToInt16(ulong value)
		{
			if (value > 32767)
			{
				throw new OverflowException(Locale.GetText("Value is greater than Int16.MaxValue"));
			}
			return (short)value;
		}

		/// <summary>Converts the value of the specified 16-bit unsigned integer to the equivalent 16-bit signed integer.</summary>
		/// <returns>The 16-bit signed integer equivalent to the value of <paramref name="value" />.</returns>
		/// <param name="value">A 16-bit unsigned integer. </param>
		/// <exception cref="T:System.OverflowException">
		///   <paramref name="value" /> is greater than <see cref="F:System.Int16.MaxValue" />. </exception>
		/// <filterpriority>1</filterpriority>
		[CLSCompliant(false)]
		public static short ToInt16(ushort value)
		{
			if (value > 32767)
			{
				throw new OverflowException(Locale.GetText("Value is greater than Int16.MaxValue"));
			}
			return (short)value;
		}

		/// <summary>Converts the value of the specified <see cref="T:System.Object" /> to a 16-bit signed integer.</summary>
		/// <returns>A 16-bit signed integer equivalent to the value of <paramref name="value" />, or zero if <paramref name="value" /> is null.</returns>
		/// <param name="value">An <see cref="T:System.Object" /> that implements the <see cref="T:System.IConvertible" /> interface or null. </param>
		/// <exception cref="T:System.InvalidCastException">
		///   <paramref name="value" /> does not implement <see cref="T:System.IConvertible" />. </exception>
		/// <filterpriority>1</filterpriority>
		public static short ToInt16(object value)
		{
			if (value == null)
			{
				return 0;
			}
			return ToInt16(value, null);
		}

		/// <summary>Converts the value of the specified <see cref="T:System.Object" /> to a 16-bit signed integer using the specified culture-specific formatting information.</summary>
		/// <returns>A 16-bit signed integer equivalent to the value of <paramref name="value" />, or zero if <paramref name="value" /> is null.</returns>
		/// <param name="value">An <see cref="T:System.Object" /> that implements the <see cref="T:System.IConvertible" /> interface. </param>
		/// <param name="provider">An <see cref="T:System.IFormatProvider" /> interface implementation that supplies culture-specific formatting information. </param>
		/// <exception cref="T:System.InvalidCastException">
		///   <paramref name="value" /> does not implement <see cref="T:System.IConvertible" />. </exception>
		/// <filterpriority>1</filterpriority>
		public static short ToInt16(object value, IFormatProvider provider)
		{
			if (value == null)
			{
				return 0;
			}
			return ((IConvertible)value).ToInt16(provider);
		}

		/// <summary>Converts the value of the specified Boolean value to the equivalent 32-bit signed integer.</summary>
		/// <returns>The number 1 if <paramref name="value" /> is true; otherwise, 0.</returns>
		/// <param name="value">A Boolean value. </param>
		/// <filterpriority>1</filterpriority>
		public static int ToInt32(bool value)
		{
			return value ? 1 : 0;
		}

		/// <summary>Converts the value of the specified 8-bit unsigned integer to the equivalent 32-bit signed integer.</summary>
		/// <returns>The 32-bit signed integer equivalent to the value of <paramref name="value" />.</returns>
		/// <param name="value">An 8-bit unsigned integer. </param>
		/// <filterpriority>1</filterpriority>
		public static int ToInt32(byte value)
		{
			return value;
		}

		/// <summary>Converts the value of the specified Unicode character to the equivalent 32-bit signed integer.</summary>
		/// <returns>The 32-bit signed integer equivalent to <paramref name="value" />.</returns>
		/// <param name="value">A Unicode character. </param>
		/// <filterpriority>1</filterpriority>
		public static int ToInt32(char value)
		{
			return value;
		}

		/// <summary>Calling this method always throws <see cref="T:System.InvalidCastException" />.</summary>
		/// <returns>This conversion is not supported. No value is returned.</returns>
		/// <param name="value">A <see cref="T:System.DateTime" />. </param>
		/// <exception cref="T:System.InvalidCastException">This conversion is not supported. </exception>
		/// <filterpriority>1</filterpriority>
		public static int ToInt32(DateTime value)
		{
			throw new InvalidCastException("This conversion is not supported.");
		}

		/// <summary>Converts the value of the specified <see cref="T:System.Decimal" /> number to an equivalent 32-bit signed integer.</summary>
		/// <returns>
		///   <paramref name="value" /> rounded to the nearest 32-bit signed integer. If <paramref name="value" /> is halfway between two whole numbers, the even number is returned; that is, 4.5 is converted to 4, and 5.5 is converted to 6.</returns>
		/// <param name="value">A <see cref="T:System.Decimal" /> number. </param>
		/// <exception cref="T:System.OverflowException">
		///   <paramref name="value" /> is greater than <see cref="F:System.Int32.MaxValue" /> or less than <see cref="F:System.Int32.MinValue" />. </exception>
		/// <filterpriority>1</filterpriority>
		public static int ToInt32(decimal value)
		{
			if (value > 2147483647m || value < -2147483648m)
			{
				throw new OverflowException(Locale.GetText("Value is greater than Int32.MaxValue or less than Int32.MinValue"));
			}
			return (int)Math.Round(value);
		}

		/// <summary>Converts the value of the specified double-precision floating point number to an equivalent 32-bit signed integer.</summary>
		/// <returns>
		///   <paramref name="value" /> rounded to the nearest 32-bit signed integer. If <paramref name="value" /> is halfway between two whole numbers, the even number is returned; that is, 4.5 is converted to 4, and 5.5 is converted to 6.</returns>
		/// <param name="value">A double-precision floating point number. </param>
		/// <exception cref="T:System.OverflowException">
		///   <paramref name="value" /> is greater than <see cref="F:System.Int32.MaxValue" /> or less than <see cref="F:System.Int32.MinValue" />. </exception>
		/// <filterpriority>1</filterpriority>
		public static int ToInt32(double value)
		{
			if (value > 2147483647.0 || value < -2147483648.0)
			{
				throw new OverflowException(Locale.GetText("Value is greater than Int32.MaxValue or less than Int32.MinValue"));
			}
			return checked((int)Math.Round(value));
		}

		/// <summary>Converts the value of the specified single-precision floating point number to an equivalent 32-bit signed integer.</summary>
		/// <returns>
		///   <paramref name="value" /> rounded to the nearest 32-bit signed integer. If <paramref name="value" /> is halfway between two whole numbers, the even number is returned; that is, 4.5 is converted to 4, and 5.5 is converted to 6.</returns>
		/// <param name="value">A single-precision floating point number. </param>
		/// <exception cref="T:System.OverflowException">
		///   <paramref name="value" /> is greater than <see cref="F:System.Int32.MaxValue" /> or less than <see cref="F:System.Int32.MinValue" />. </exception>
		/// <filterpriority>1</filterpriority>
		public static int ToInt32(float value)
		{
			if (value > 2.14748365E+09f || value < -2.14748365E+09f)
			{
				throw new OverflowException(Locale.GetText("Value is greater than Int32.MaxValue or less than Int32.MinValue"));
			}
			return checked((int)Math.Round(value));
		}

		/// <summary>Returns the specified 32-bit signed integer; no actual conversion is performed.</summary>
		/// <returns>Parameter <paramref name="value" /> is returned unchanged.</returns>
		/// <param name="value">A 32-bit signed integer. </param>
		/// <filterpriority>1</filterpriority>
		public static int ToInt32(int value)
		{
			return value;
		}

		/// <summary>Converts the value of the specified 64-bit signed integer to an equivalent 32-bit signed integer.</summary>
		/// <returns>A 32-bit signed integer equivalent to the value of <paramref name="value" />.</returns>
		/// <param name="value">A 64-bit signed integer. </param>
		/// <exception cref="T:System.OverflowException">
		///   <paramref name="value" /> is greater than <see cref="F:System.Int32.MaxValue" /> or less than <see cref="F:System.Int32.MinValue" />. </exception>
		/// <filterpriority>1</filterpriority>
		public static int ToInt32(long value)
		{
			if (value > int.MaxValue || value < int.MinValue)
			{
				throw new OverflowException(Locale.GetText("Value is greater than Int32.MaxValue or less than Int32.MinValue"));
			}
			return (int)value;
		}

		/// <summary>Converts the value of the specified 8-bit signed integer to the equivalent 32-bit signed integer.</summary>
		/// <returns>The 8-bit signed integer equivalent to the value of <paramref name="value" />.</returns>
		/// <param name="value">An 8-bit signed integer. </param>
		/// <filterpriority>1</filterpriority>
		[CLSCompliant(false)]
		public static int ToInt32(sbyte value)
		{
			return value;
		}

		/// <summary>Converts the value of the specified 16-bit signed integer to an equivalent 32-bit signed integer.</summary>
		/// <returns>A 32-bit signed integer equivalent to the value of <paramref name="value" />.</returns>
		/// <param name="value">A 16-bit signed integer. </param>
		/// <filterpriority>1</filterpriority>
		public static int ToInt32(short value)
		{
			return value;
		}

		/// <summary>Converts the specified <see cref="T:System.String" /> representation of a number to an equivalent 32-bit signed integer.</summary>
		/// <returns>A 32-bit signed integer equivalent to the value of <paramref name="value" />.-or- Zero if <paramref name="value" /> is null.</returns>
		/// <param name="value">A <see cref="T:System.String" /> containing a number to convert. </param>
		/// <exception cref="T:System.FormatException">
		///   <paramref name="value" /> does not consist of an optional sign followed by a sequence of digits (zero through nine). </exception>
		/// <exception cref="T:System.OverflowException">
		///   <paramref name="value" /> represents a number less than <see cref="F:System.Int32.MinValue" /> or greater than <see cref="F:System.Int32.MaxValue" />. </exception>
		/// <filterpriority>1</filterpriority>
		public static int ToInt32(string value)
		{
			if (value == null)
			{
				return 0;
			}
			return int.Parse(value);
		}

		/// <summary>Converts the specified <see cref="T:System.String" /> representation of a number to an equivalent 32-bit signed integer using specified culture-specific formatting information.</summary>
		/// <returns>A 32-bit signed integer equivalent to the value of <paramref name="value" />.-or- Zero if <paramref name="value" /> is null.</returns>
		/// <param name="value">A <see cref="T:System.String" /> containing a number to convert. </param>
		/// <param name="provider">An <see cref="T:System.IFormatProvider" /> interface implementation that supplies culture-specific formatting information. </param>
		/// <exception cref="T:System.FormatException">
		///   <paramref name="value" /> does not consist of an optional sign followed by a sequence of digits (zero through nine). </exception>
		/// <exception cref="T:System.OverflowException">
		///   <paramref name="value" /> represents a number less than <see cref="F:System.Int32.MinValue" /> or greater than <see cref="F:System.Int32.MaxValue" />. </exception>
		/// <filterpriority>1</filterpriority>
		public static int ToInt32(string value, IFormatProvider provider)
		{
			if (value == null)
			{
				return 0;
			}
			return int.Parse(value, provider);
		}

		/// <summary>Converts the <see cref="T:System.String" /> representation of a number in a specified base to an equivalent 32-bit signed integer.</summary>
		/// <returns>A 32-bit signed integer equivalent to the number in <paramref name="value" />.-or- Zero if <paramref name="value" /> is null.</returns>
		/// <param name="value">A <see cref="T:System.String" /> containing a number. </param>
		/// <param name="fromBase">The base of the number in <paramref name="value" />, which must be 2, 8, 10, or 16. </param>
		/// <exception cref="T:System.ArgumentException">
		///   <paramref name="fromBase" /> is not 2, 8, 10, or 16. -or-<paramref name="value" />, which represents a non-base 10 signed number, is prefixed with a negative sign. </exception>
		/// <exception cref="T:System.FormatException">
		///   <paramref name="value" /> contains a character that is not a valid digit in the base specified by <paramref name="fromBase" />. The exception message indicates that there are no digits to convert if the first character in <paramref name="value" /> is invalid; otherwise, the message indicates that <paramref name="value" /> contains invalid trailing characters.</exception>
		/// <exception cref="T:System.OverflowException">
		///   <paramref name="value" />, which represents a base 10 signed number, is prefixed with a negative sign.-or-The return value is less than <see cref="F:System.Int32.MinValue" /> or larger than <see cref="F:System.Int32.MaxValue" />.</exception>
		/// <filterpriority>1</filterpriority>
		public static int ToInt32(string value, int fromBase)
		{
			return ConvertFromBase(value, fromBase, unsigned: false);
		}

		/// <summary>Converts the value of the specified 32-bit unsigned integer to an equivalent 32-bit signed integer.</summary>
		/// <returns>A 32-bit signed integer equivalent to the value of <paramref name="value" />.</returns>
		/// <param name="value">A 32-bit unsigned integer. </param>
		/// <exception cref="T:System.OverflowException">
		///   <paramref name="value" /> is greater than <see cref="F:System.Int32.MaxValue" />. </exception>
		/// <filterpriority>1</filterpriority>
		[CLSCompliant(false)]
		public static int ToInt32(uint value)
		{
			if (value > int.MaxValue)
			{
				throw new OverflowException(Locale.GetText("Value is greater than Int32.MaxValue"));
			}
			return (int)value;
		}

		/// <summary>Converts the value of the specified 64-bit unsigned integer to an equivalent 32-bit signed integer.</summary>
		/// <returns>A 32-bit signed integer equivalent to the value of <paramref name="value" />.</returns>
		/// <param name="value">A 64-bit unsigned integer. </param>
		/// <exception cref="T:System.OverflowException">
		///   <paramref name="value" /> is greater than <see cref="F:System.Int32.MaxValue" />. </exception>
		/// <filterpriority>1</filterpriority>
		[CLSCompliant(false)]
		public static int ToInt32(ulong value)
		{
			if (value > int.MaxValue)
			{
				throw new OverflowException(Locale.GetText("Value is greater than Int32.MaxValue"));
			}
			return (int)value;
		}

		/// <summary>Converts the value of the specified 16-bit unsigned integer to the equivalent 32-bit signed integer.</summary>
		/// <returns>The 32-bit signed integer equivalent to the value of <paramref name="value" />.</returns>
		/// <param name="value">A 16-bit unsigned integer. </param>
		/// <filterpriority>1</filterpriority>
		[CLSCompliant(false)]
		public static int ToInt32(ushort value)
		{
			return value;
		}

		/// <summary>Converts the value of the specified <see cref="T:System.Object" /> to a 32-bit signed integer.</summary>
		/// <returns>A 32-bit signed integer equivalent to the value of <paramref name="value" />, or zero if <paramref name="value" /> is null.</returns>
		/// <param name="value">An <see cref="T:System.Object" /> that implements the <see cref="T:System.IConvertible" /> interface or null. </param>
		/// <exception cref="T:System.InvalidCastException">
		///   <paramref name="value" /> does not implement <see cref="T:System.IConvertible" />. </exception>
		/// <filterpriority>1</filterpriority>
		public static int ToInt32(object value)
		{
			if (value == null)
			{
				return 0;
			}
			return ToInt32(value, null);
		}

		/// <summary>Converts the value of the specified <see cref="T:System.Object" /> to a 32-bit signed integer using the specified culture-specific formatting information.</summary>
		/// <returns>A 32-bit signed integer equivalent to the value of <paramref name="value" />, or zero if <paramref name="value" /> is null.</returns>
		/// <param name="value">An <see cref="T:System.Object" /> that implements the <see cref="T:System.IConvertible" /> interface. </param>
		/// <param name="provider">An <see cref="T:System.IFormatProvider" /> interface implementation that supplies culture-specific formatting information. </param>
		/// <exception cref="T:System.InvalidCastException">
		///   <paramref name="value" /> does not implement <see cref="T:System.IConvertible" />. </exception>
		/// <filterpriority>1</filterpriority>
		public static int ToInt32(object value, IFormatProvider provider)
		{
			if (value == null)
			{
				return 0;
			}
			return ((IConvertible)value).ToInt32(provider);
		}

		/// <summary>Converts the value of the specified Boolean value to the equivalent 64-bit signed integer.</summary>
		/// <returns>The number 1 if <paramref name="value" /> is true; otherwise, 0.</returns>
		/// <param name="value">A Boolean value. </param>
		/// <filterpriority>1</filterpriority>
		public static long ToInt64(bool value)
		{
			return value ? 1 : 0;
		}

		/// <summary>Converts the value of the specified 8-bit unsigned integer to the equivalent 64-bit signed integer.</summary>
		/// <returns>The 64-bit signed integer equivalent to the value of <paramref name="value" />.</returns>
		/// <param name="value">An 8-bit unsigned integer. </param>
		/// <filterpriority>1</filterpriority>
		public static long ToInt64(byte value)
		{
			return value;
		}

		/// <summary>Converts the value of the specified Unicode character to the equivalent 64-bit signed integer.</summary>
		/// <returns>The 64-bit signed integer equivalent to <paramref name="value" />.</returns>
		/// <param name="value">A Unicode character. </param>
		/// <filterpriority>1</filterpriority>
		public static long ToInt64(char value)
		{
			return (int)value;
		}

		/// <summary>Calling this method always throws <see cref="T:System.InvalidCastException" />.</summary>
		/// <returns>This conversion is not supported. No value is returned.</returns>
		/// <param name="value">A <see cref="T:System.DateTime" />. </param>
		/// <exception cref="T:System.InvalidCastException">This conversion is not supported. </exception>
		/// <filterpriority>1</filterpriority>
		public static long ToInt64(DateTime value)
		{
			throw new InvalidCastException("This conversion is not supported.");
		}

		/// <summary>Converts the value of the specified <see cref="T:System.Decimal" /> number to an equivalent 64-bit signed integer.</summary>
		/// <returns>
		///   <paramref name="value" /> rounded to the nearest 64-bit signed integer. If <paramref name="value" /> is halfway between two whole numbers, the even number is returned; that is, 4.5 is converted to 4, and 5.5 is converted to 6.</returns>
		/// <param name="value">A <see cref="T:System.Decimal" /> number. </param>
		/// <exception cref="T:System.OverflowException">
		///   <paramref name="value" /> is greater than <see cref="F:System.Int64.MaxValue" /> or less than <see cref="F:System.Int64.MinValue" />. </exception>
		/// <filterpriority>1</filterpriority>
		public static long ToInt64(decimal value)
		{
			if (value > 9223372036854775807m || value < -9223372036854775808m)
			{
				throw new OverflowException(Locale.GetText("Value is greater than Int64.MaxValue or less than Int64.MinValue"));
			}
			return (long)Math.Round(value);
		}

		/// <summary>Converts the value of the specified double-precision floating point number to an equivalent 64-bit signed integer.</summary>
		/// <returns>
		///   <paramref name="value" /> rounded to the nearest 64-bit signed integer. If <paramref name="value" /> is halfway between two whole numbers, the even number is returned; that is, 4.5 is converted to 4, and 5.5 is converted to 6.</returns>
		/// <param name="value">A double-precision floating point number. </param>
		/// <exception cref="T:System.OverflowException">
		///   <paramref name="value" /> is greater than <see cref="F:System.Int64.MaxValue" /> or less than <see cref="F:System.Int64.MinValue" />. </exception>
		/// <filterpriority>1</filterpriority>
		public static long ToInt64(double value)
		{
			if (value > 9.2233720368547758E+18 || value < -9.2233720368547758E+18)
			{
				throw new OverflowException(Locale.GetText("Value is greater than Int64.MaxValue or less than Int64.MinValue"));
			}
			return (long)Math.Round(value);
		}

		/// <summary>Converts the value of the specified single-precision floating point number to an equivalent 64-bit signed integer.</summary>
		/// <returns>
		///   <paramref name="value" /> rounded to the nearest 64-bit signed integer. If <paramref name="value" /> is halfway between two whole numbers, the even number is returned; that is, 4.5 is converted to 4, and 5.5 is converted to 6.</returns>
		/// <param name="value">A single-precision floating point number. </param>
		/// <exception cref="T:System.OverflowException">
		///   <paramref name="value" /> is greater than <see cref="F:System.Int64.MaxValue" /> or less than <see cref="F:System.Int64.MinValue" />. </exception>
		/// <filterpriority>1</filterpriority>
		public static long ToInt64(float value)
		{
			if (value > 9.223372E+18f || value < -9.223372E+18f)
			{
				throw new OverflowException(Locale.GetText("Value is greater than Int64.MaxValue or less than Int64.MinValue"));
			}
			return (long)Math.Round(value);
		}

		/// <summary>Converts the value of the specified 32-bit signed integer to an equivalent 64-bit signed integer.</summary>
		/// <returns>The 64-bit signed integer equivalent to the value of <paramref name="value" />.</returns>
		/// <param name="value">A 32-signed integer. </param>
		/// <filterpriority>1</filterpriority>
		public static long ToInt64(int value)
		{
			return value;
		}

		/// <summary>Returns the specified 64-bit signed integer; no actual conversion is performed.</summary>
		/// <returns>Parameter <paramref name="value" /> is returned unchanged.</returns>
		/// <param name="value">A 64-bit signed integer. </param>
		/// <filterpriority>1</filterpriority>
		public static long ToInt64(long value)
		{
			return value;
		}

		/// <summary>Converts the value of the specified 8-bit signed integer to the equivalent 64-bit signed integer.</summary>
		/// <returns>The 64-bit signed integer equivalent to the value of <paramref name="value" />.</returns>
		/// <param name="value">An 8-bit signed integer. </param>
		/// <filterpriority>1</filterpriority>
		[CLSCompliant(false)]
		public static long ToInt64(sbyte value)
		{
			return value;
		}

		/// <summary>Converts the value of the specified 16-bit signed integer to an equivalent 64-bit signed integer.</summary>
		/// <returns>A 64-bit signed integer equivalent to the value of <paramref name="value" />.</returns>
		/// <param name="value">A 16-bit signed integer. </param>
		/// <filterpriority>1</filterpriority>
		public static long ToInt64(short value)
		{
			return value;
		}

		/// <summary>Converts the specified <see cref="T:System.String" /> representation of a number to an equivalent 64-bit signed integer.</summary>
		/// <returns>A 64-bit signed integer equivalent to the value of <paramref name="value" />.-or- Zero if <paramref name="value" /> is null.</returns>
		/// <param name="value">A <see cref="T:System.String" /> containing a number to convert. </param>
		/// <exception cref="T:System.FormatException">
		///   <paramref name="value" /> does not consist of an optional sign followed by a sequence of digits (zero through nine). </exception>
		/// <exception cref="T:System.OverflowException">
		///   <paramref name="value" /> represents a number less than <see cref="F:System.Int64.MinValue" /> or greater than <see cref="F:System.Int64.MaxValue" />. </exception>
		/// <filterpriority>1</filterpriority>
		public static long ToInt64(string value)
		{
			if (value == null)
			{
				return 0L;
			}
			return long.Parse(value);
		}

		/// <summary>Converts the specified <see cref="T:System.String" /> representation of a number to an equivalent 64-bit signed integer using the specified culture-specific formatting information.</summary>
		/// <returns>A 64-bit signed integer equivalent to the value of <paramref name="value" />.-or- Zero if <paramref name="value" /> is null.</returns>
		/// <param name="value">A <see cref="T:System.String" /> containing a number to convert. </param>
		/// <param name="provider">An <see cref="T:System.IFormatProvider" /> interface implementation that supplies culture-specific formatting information. </param>
		/// <exception cref="T:System.FormatException">
		///   <paramref name="value" /> does not consist of an optional sign followed by a sequence of digits (zero through nine). </exception>
		/// <exception cref="T:System.OverflowException">
		///   <paramref name="value" /> represents a number less than <see cref="F:System.Int64.MinValue" /> or greater than <see cref="F:System.Int64.MaxValue" />. </exception>
		/// <filterpriority>1</filterpriority>
		public static long ToInt64(string value, IFormatProvider provider)
		{
			if (value == null)
			{
				return 0L;
			}
			return long.Parse(value, provider);
		}

		/// <summary>Converts the <see cref="T:System.String" /> representation of a number in a specified base to an equivalent 64-bit signed integer.</summary>
		/// <returns>A 64-bit signed integer equivalent to the number in <paramref name="value" />.-or- Zero if <paramref name="value" /> is null.</returns>
		/// <param name="value">A <see cref="T:System.String" /> containing a number. </param>
		/// <param name="fromBase">The base of the number in <paramref name="value" />, which must be 2, 8, 10, or 16. </param>
		/// <exception cref="T:System.ArgumentException">
		///   <paramref name="fromBase" /> is not 2, 8, 10, or 16. -or-<paramref name="value" />, which represents a non-base 10 signed number, is prefixed with a negative sign. </exception>
		/// <exception cref="T:System.FormatException">
		///   <paramref name="value" /> contains a character that is not a valid digit in the base specified by <paramref name="fromBase" />. The exception message indicates that there are no digits to convert if the first character in <paramref name="value" /> is invalid; otherwise, the message indicates that <paramref name="value" /> contains invalid trailing characters.</exception>
		/// <exception cref="T:System.OverflowException">
		///   <paramref name="value" />, which represents a base 10 signed number, is prefixed with a negative sign.-or-The return value is less than <see cref="F:System.Int64.MinValue" /> or larger than <see cref="F:System.Int64.MaxValue" />.</exception>
		/// <filterpriority>1</filterpriority>
		public static long ToInt64(string value, int fromBase)
		{
			return ConvertFromBase64(value, fromBase, unsigned: false);
		}

		/// <summary>Converts the value of the specified 32-bit unsigned integer to an equivalent 64-bit signed integer.</summary>
		/// <returns>A 64-bit signed integer equivalent to the value of <paramref name="value" />.</returns>
		/// <param name="value">A 32-bit unsigned integer. </param>
		/// <filterpriority>1</filterpriority>
		[CLSCompliant(false)]
		public static long ToInt64(uint value)
		{
			return value;
		}

		/// <summary>Converts the value of the specified 64-bit unsigned integer to an equivalent 64-bit signed integer.</summary>
		/// <returns>A 64-bit signed integer equivalent to the value of <paramref name="value" />.</returns>
		/// <param name="value">A 64-bit unsigned integer. </param>
		/// <exception cref="T:System.OverflowException">
		///   <paramref name="value" /> is greater than <see cref="F:System.Int64.MaxValue" />. </exception>
		/// <filterpriority>1</filterpriority>
		[CLSCompliant(false)]
		public static long ToInt64(ulong value)
		{
			if (value > long.MaxValue)
			{
				throw new OverflowException(Locale.GetText("Value is greater than Int64.MaxValue"));
			}
			return (long)value;
		}

		/// <summary>Converts the value of the specified 16-bit unsigned integer to the equivalent 64-bit signed integer.</summary>
		/// <returns>The 64-bit signed integer equivalent to the value of <paramref name="value" />.</returns>
		/// <param name="value">A 16-bit unsigned integer. </param>
		/// <filterpriority>1</filterpriority>
		[CLSCompliant(false)]
		public static long ToInt64(ushort value)
		{
			return value;
		}

		/// <summary>Converts the value of the specified <see cref="T:System.Object" /> to a 64-bit signed integer.</summary>
		/// <returns>A 64-bit signed integer equivalent to the value of <paramref name="value" />, or zero if <paramref name="value" /> is null.</returns>
		/// <param name="value">An <see cref="T:System.Object" /> that implements the <see cref="T:System.IConvertible" /> interface or null. </param>
		/// <exception cref="T:System.InvalidCastException">
		///   <paramref name="value" /> does not implement <see cref="T:System.IConvertible" />. </exception>
		/// <filterpriority>1</filterpriority>
		public static long ToInt64(object value)
		{
			if (value == null)
			{
				return 0L;
			}
			return ToInt64(value, null);
		}

		/// <summary>Converts the value of the specified <see cref="T:System.Object" /> to a 64-bit signed integer using the specified culture-specific formatting information.</summary>
		/// <returns>A 64-bit signed integer equivalent to the value of <paramref name="value" />, or zero if <paramref name="value" /> is null.</returns>
		/// <param name="value">An <see cref="T:System.Object" /> that implements the <see cref="T:System.IConvertible" /> interface. </param>
		/// <param name="provider">An <see cref="T:System.IFormatProvider" /> interface implementation that supplies culture-specific formatting information. </param>
		/// <exception cref="T:System.InvalidCastException">
		///   <paramref name="value" /> does not implement <see cref="T:System.IConvertible" />. </exception>
		/// <filterpriority>1</filterpriority>
		public static long ToInt64(object value, IFormatProvider provider)
		{
			if (value == null)
			{
				return 0L;
			}
			return ((IConvertible)value).ToInt64(provider);
		}

		/// <summary>Converts the value of the specified Boolean value to the equivalent 8-bit signed integer.</summary>
		/// <returns>The number 1 if <paramref name="value" /> is true; otherwise, 0.</returns>
		/// <param name="value">A Boolean value. </param>
		/// <filterpriority>1</filterpriority>
		[CLSCompliant(false)]
		public static sbyte ToSByte(bool value)
		{
			return (sbyte)(value ? 1 : 0);
		}

		/// <summary>Converts the value of the specified 8-bit unsigned integer to the equivalent 8-bit signed integer.</summary>
		/// <returns>The 8-bit signed integer equivalent to the value of <paramref name="value" />.</returns>
		/// <param name="value">An 8-bit unsigned integer. </param>
		/// <exception cref="T:System.OverflowException">
		///   <paramref name="value" /> is greater than <see cref="F:System.SByte.MaxValue" />. </exception>
		/// <filterpriority>1</filterpriority>
		[CLSCompliant(false)]
		public static sbyte ToSByte(byte value)
		{
			if (value > 127)
			{
				throw new OverflowException(Locale.GetText("Value is greater than SByte.MaxValue"));
			}
			return (sbyte)value;
		}

		/// <summary>Converts the value of the specified Unicode character to the equivalent 8-bit signed integer.</summary>
		/// <returns>The 8-bit signed integer equivalent to <paramref name="value" />.</returns>
		/// <param name="value">A Unicode character. </param>
		/// <exception cref="T:System.OverflowException">
		///   <paramref name="value" /> is greater than <see cref="F:System.SByte.MaxValue" />. </exception>
		/// <filterpriority>1</filterpriority>
		[CLSCompliant(false)]
		public static sbyte ToSByte(char value)
		{
			if (value > '\u007f')
			{
				throw new OverflowException(Locale.GetText("Value is greater than SByte.MaxValue"));
			}
			return (sbyte)value;
		}

		/// <summary>Calling this method always throws <see cref="T:System.InvalidCastException" />.</summary>
		/// <returns>This conversion is not supported. No value is returned.</returns>
		/// <param name="value">A <see cref="T:System.DateTime" />. </param>
		/// <exception cref="T:System.InvalidCastException">This conversion is not supported. </exception>
		/// <filterpriority>1</filterpriority>
		[CLSCompliant(false)]
		public static sbyte ToSByte(DateTime value)
		{
			throw new InvalidCastException("This conversion is not supported.");
		}

		/// <summary>Converts the value of the specified <see cref="T:System.Decimal" /> number to an equivalent 8-bit signed integer.</summary>
		/// <returns>
		///   <paramref name="value" /> rounded to the nearest 8-bit signed integer. If <paramref name="value" /> is halfway between two whole numbers, the even number is returned; that is, 4.5 is converted to 4, and 5.5 is converted to 6.</returns>
		/// <param name="value">A <see cref="T:System.Decimal" /> number. </param>
		/// <exception cref="T:System.OverflowException">
		///   <paramref name="value" /> is greater than <see cref="F:System.SByte.MaxValue" /> or less than <see cref="F:System.SByte.MinValue" />. </exception>
		/// <filterpriority>1</filterpriority>
		[CLSCompliant(false)]
		public static sbyte ToSByte(decimal value)
		{
			if (value > 127m || value < -128m)
			{
				throw new OverflowException(Locale.GetText("Value is greater than SByte.MaxValue or less than SByte.MinValue"));
			}
			return (sbyte)Math.Round(value);
		}

		/// <summary>Converts the value of the specified double-precision floating point number to an equivalent 8-bit signed integer.</summary>
		/// <returns>
		///   <paramref name="value" /> rounded to the nearest 8-bit signed integer. If <paramref name="value" /> is halfway between two whole numbers, the even number is returned; that is, 4.5 is converted to 4, and 5.5 is converted to 6.</returns>
		/// <param name="value">A double-precision floating point number. </param>
		/// <exception cref="T:System.OverflowException">
		///   <paramref name="value" /> is greater than <see cref="F:System.SByte.MaxValue" /> or less than <see cref="F:System.SByte.MinValue" />. </exception>
		/// <filterpriority>1</filterpriority>
		[CLSCompliant(false)]
		public static sbyte ToSByte(double value)
		{
			if (value > 127.0 || value < -128.0)
			{
				throw new OverflowException(Locale.GetText("Value is greater than SByte.MaxValue or less than SByte.MinValue"));
			}
			return (sbyte)Math.Round(value);
		}

		/// <summary>Converts the value of the specified single-precision floating point number to an equivalent 8-bit signed integer.</summary>
		/// <returns>
		///   <paramref name="value" /> rounded to the nearest 8-bit signed integer. If <paramref name="value" /> is halfway between two whole numbers, the even number is returned; that is, 4.5 is converted to 4, and 5.5 is converted to 6.</returns>
		/// <param name="value">A single-precision floating point number. </param>
		/// <exception cref="T:System.OverflowException">
		///   <paramref name="value" /> is greater than <see cref="F:System.SByte.MaxValue" /> or less than <see cref="F:System.SByte.MinValue" />. </exception>
		/// <filterpriority>1</filterpriority>
		[CLSCompliant(false)]
		public static sbyte ToSByte(float value)
		{
			if (value > 127f || value < -128f)
			{
				throw new OverflowException(Locale.GetText("Value is greater than SByte.MaxValue or less than SByte.Minalue"));
			}
			return (sbyte)Math.Round(value);
		}

		/// <summary>Converts the value of the specified 32-bit signed integer to an equivalent 8-bit signed integer.</summary>
		/// <returns>The 8-bit signed integer equivalent of <paramref name="value" />.</returns>
		/// <param name="value">A 32-bit signed integer. </param>
		/// <exception cref="T:System.OverflowException">
		///   <paramref name="value" /> is greater than <see cref="F:System.SByte.MaxValue" /> or less than <see cref="F:System.SByte.MinValue" />. </exception>
		/// <filterpriority>1</filterpriority>
		[CLSCompliant(false)]
		public static sbyte ToSByte(int value)
		{
			if (value > 127 || value < -128)
			{
				throw new OverflowException(Locale.GetText("Value is greater than SByte.MaxValue or less than SByte.MinValue"));
			}
			return (sbyte)value;
		}

		/// <summary>Converts the value of the specified 64-bit signed integer to an equivalent 8-bit signed integer.</summary>
		/// <returns>An 8-bit signed integer equivalent to the value of <paramref name="value" />.</returns>
		/// <param name="value">A 64-bit signed integer. </param>
		/// <exception cref="T:System.OverflowException">
		///   <paramref name="value" /> is greater than <see cref="F:System.SByte.MaxValue" /> or less than <see cref="F:System.SByte.MinValue" />. </exception>
		/// <filterpriority>1</filterpriority>
		[CLSCompliant(false)]
		public static sbyte ToSByte(long value)
		{
			if (value > 127 || value < -128)
			{
				throw new OverflowException(Locale.GetText("Value is greater than SByte.MaxValue or less than SByte.MinValue"));
			}
			return (sbyte)value;
		}

		/// <summary>Returns the specified 8-bit signed integer; no actual conversion is performed.</summary>
		/// <returns>Parameter <paramref name="value" /> is returned unchanged.</returns>
		/// <param name="value">An 8-bit signed integer. </param>
		/// <filterpriority>1</filterpriority>
		[CLSCompliant(false)]
		public static sbyte ToSByte(sbyte value)
		{
			return value;
		}

		/// <summary>Converts the value of the specified 16-bit signed integer to the equivalent 8-bit signed integer.</summary>
		/// <returns>The 8-bit signed integer equivalent to the value of <paramref name="value" />.</returns>
		/// <param name="value">A 16-bit signed integer. </param>
		/// <exception cref="T:System.OverflowException">
		///   <paramref name="value" /> is greater than <see cref="F:System.SByte.MaxValue" /> or less than <see cref="F:System.SByte.MinValue" />. </exception>
		/// <filterpriority>1</filterpriority>
		[CLSCompliant(false)]
		public static sbyte ToSByte(short value)
		{
			if (value > 127 || value < -128)
			{
				throw new OverflowException(Locale.GetText("Value is greater than SByte.MaxValue or less than SByte.MinValue"));
			}
			return (sbyte)value;
		}

		/// <summary>Converts the specified <see cref="T:System.String" /> representation of a number to an equivalent 8-bit signed integer.</summary>
		/// <returns>An 8-bit signed integer equivalent to the value of <paramref name="value" />.-or- Zero if value is null.</returns>
		/// <param name="value">A <see cref="T:System.String" /> containing a number to convert. </param>
		/// <exception cref="T:System.FormatException">
		///   <paramref name="value" /> does not consist of an optional sign followed by a sequence of digits (zero through nine). </exception>
		/// <exception cref="T:System.OverflowException">
		///   <paramref name="value" /> represents a number less than <see cref="F:System.SByte.MinValue" /> or greater than <see cref="F:System.SByte.MaxValue" />. </exception>
		/// <filterpriority>1</filterpriority>
		[CLSCompliant(false)]
		public static sbyte ToSByte(string value)
		{
			if (value == null)
			{
				return 0;
			}
			return sbyte.Parse(value);
		}

		/// <summary>Converts the specified <see cref="T:System.String" /> representation of a number to an equivalent 8-bit signed integer using specified culture-specific formatting information.</summary>
		/// <returns>An 8-bit signed integer equivalent to the value of <paramref name="value" />.</returns>
		/// <param name="value">A <see cref="T:System.String" /> containing a number to convert. </param>
		/// <param name="provider">An <see cref="T:System.IFormatProvider" /> interface implementation that supplies culture-specific formatting information. </param>
		/// <exception cref="T:System.ArgumentNullException">
		///   <paramref name="value" /> is null. </exception>
		/// <exception cref="T:System.FormatException">
		///   <paramref name="value" /> does not consist of an optional sign followed by a sequence of digits (zero through nine). </exception>
		/// <exception cref="T:System.OverflowException">
		///   <paramref name="value" /> represents a number less than <see cref="F:System.SByte.MinValue" /> or greater than <see cref="F:System.SByte.MaxValue" />. </exception>
		/// <filterpriority>1</filterpriority>
		[CLSCompliant(false)]
		public static sbyte ToSByte(string value, IFormatProvider provider)
		{
			if (value == null)
			{
				throw new ArgumentNullException("value");
			}
			return sbyte.Parse(value, provider);
		}

		/// <summary>Converts the <see cref="T:System.String" /> representation of a number in a specified base to an equivalent 8-bit signed integer.</summary>
		/// <returns>An 8-bit signed integer equivalent to the number in <paramref name="value" />.-or- Zero if <paramref name="value" /> is null.</returns>
		/// <param name="value">A <see cref="T:System.String" /> containing a number. </param>
		/// <param name="fromBase">The base of the number in <paramref name="value" />, which must be 2, 8, 10, or 16. </param>
		/// <exception cref="T:System.ArgumentException">
		///   <paramref name="fromBase" /> is not 2, 8, 10, or 16. -or-<paramref name="value" />, which represents a non-base 10 signed number, is prefixed with a negative sign. </exception>
		/// <exception cref="T:System.FormatException">
		///   <paramref name="value" /> contains a character that is not a valid digit in the base specified by <paramref name="fromBase" />. The exception message indicates that there are no digits to convert if the first character in <paramref name="value" /> is invalid; otherwise, the message indicates that <paramref name="value" /> contains invalid trailing characters.</exception>
		/// <exception cref="T:System.OverflowException">
		///   <paramref name="value" />, which represents a base 10 signed number, is prefixed with a negative sign.-or-The return value is less than <see cref="F:System.SByte.MinValue" /> or larger than <see cref="F:System.SByte.MaxValue" />.</exception>
		/// <filterpriority>1</filterpriority>
		[CLSCompliant(false)]
		public static sbyte ToSByte(string value, int fromBase)
		{
			int num = ConvertFromBase(value, fromBase, unsigned: false);
			if (fromBase != 10 && num > 127)
			{
				return ToSByte(-(256 - num));
			}
			return ToSByte(num);
		}

		/// <summary>Converts the value of the specified 32-bit unsigned integer to an equivalent 8-bit signed integer.</summary>
		/// <returns>An 8-bit signed integer equivalent to the value of <paramref name="value" />.</returns>
		/// <param name="value">A 32-bit unsigned integer. </param>
		/// <exception cref="T:System.OverflowException">
		///   <paramref name="value" /> is greater than <see cref="F:System.SByte.MaxValue" /> or less than <see cref="F:System.SByte.MinValue" />. </exception>
		/// <filterpriority>1</filterpriority>
		[CLSCompliant(false)]
		public static sbyte ToSByte(uint value)
		{
			if ((long)value > 127L)
			{
				throw new OverflowException(Locale.GetText("Value is greater than SByte.MaxValue"));
			}
			return (sbyte)value;
		}

		/// <summary>Converts the value of the specified 64-bit unsigned integer to an equivalent 8-bit signed integer.</summary>
		/// <returns>An 8-bit signed integer equivalent to the value of <paramref name="value" />.</returns>
		/// <param name="value">A 64-bit unsigned integer. </param>
		/// <exception cref="T:System.OverflowException">
		///   <paramref name="value" /> is greater than <see cref="F:System.SByte.MaxValue" /> or less than <see cref="F:System.SByte.MinValue" />. </exception>
		/// <filterpriority>1</filterpriority>
		[CLSCompliant(false)]
		public static sbyte ToSByte(ulong value)
		{
			if (value > 127)
			{
				throw new OverflowException(Locale.GetText("Value is greater than SByte.MaxValue"));
			}
			return (sbyte)value;
		}

		/// <summary>Converts the value of the specified 16-bit unsigned integer to the equivalent 8-bit signed integer.</summary>
		/// <returns>The 8-bit signed integer equivalent to the value of <paramref name="value" />.</returns>
		/// <param name="value">A 16-bit unsigned integer. </param>
		/// <exception cref="T:System.OverflowException">
		///   <paramref name="value" /> is greater than <see cref="F:System.SByte.MaxValue" />. </exception>
		/// <filterpriority>1</filterpriority>
		[CLSCompliant(false)]
		public static sbyte ToSByte(ushort value)
		{
			if (value > 127)
			{
				throw new OverflowException(Locale.GetText("Value is greater than SByte.MaxValue"));
			}
			return (sbyte)value;
		}

		/// <summary>Converts the value of the specified <see cref="T:System.Object" /> to an 8-bit signed integer.</summary>
		/// <returns>An 8-bit signed integer equivalent to the value of <paramref name="value" />, or zero if <paramref name="value" /> is null.</returns>
		/// <param name="value">An <see cref="T:System.Object" /> that implements the <see cref="T:System.IConvertible" /> interface or null. </param>
		/// <exception cref="T:System.InvalidCastException">
		///   <paramref name="value" /> does not implement <see cref="T:System.IConvertible" />. </exception>
		/// <filterpriority>1</filterpriority>
		[CLSCompliant(false)]
		public static sbyte ToSByte(object value)
		{
			if (value == null)
			{
				return 0;
			}
			return ToSByte(value, null);
		}

		/// <summary>Converts the value of the specified <see cref="T:System.Object" /> to an 8-bit signed integer using the specified culture-specific formatting information.</summary>
		/// <returns>An 8-bit signed integer equivalent to the value of <paramref name="value" />, or zero if <paramref name="value" /> is null.</returns>
		/// <param name="value">An <see cref="T:System.Object" /> that implements the <see cref="T:System.IConvertible" /> interface. </param>
		/// <param name="provider">An <see cref="T:System.IFormatProvider" /> interface implementation that supplies culture-specific formatting information. </param>
		/// <filterpriority>1</filterpriority>
		[CLSCompliant(false)]
		public static sbyte ToSByte(object value, IFormatProvider provider)
		{
			if (value == null)
			{
				return 0;
			}
			return ((IConvertible)value).ToSByte(provider);
		}

		/// <summary>Converts the value of the specified Boolean value to the equivalent single-precision floating point number.</summary>
		/// <returns>The number 1 if <paramref name="value" /> is true; otherwise, 0.</returns>
		/// <param name="value">A Boolean value. </param>
		/// <filterpriority>1</filterpriority>
		public static float ToSingle(bool value)
		{
			return value ? 1 : 0;
		}

		/// <summary>Converts the value of the specified 8-bit unsigned integer to the equivalent single-precision floating point number.</summary>
		/// <returns>The single-precision floating point number equivalent to the value of <paramref name="value" />.</returns>
		/// <param name="value">An 8-bit unsigned integer. </param>
		/// <filterpriority>1</filterpriority>
		public static float ToSingle(byte value)
		{
			return (int)value;
		}

		/// <summary>Calling this method always throws <see cref="T:System.InvalidCastException" />.</summary>
		/// <returns>This conversion is not supported. No value is returned.</returns>
		/// <param name="value">A Unicode character. </param>
		/// <exception cref="T:System.InvalidCastException">This conversion is not supported. </exception>
		/// <filterpriority>1</filterpriority>
		public static float ToSingle(char value)
		{
			throw new InvalidCastException("This conversion is not supported.");
		}

		/// <summary>Calling this method always throws <see cref="T:System.InvalidCastException" />.</summary>
		/// <returns>This conversion is not supported. No value is returned.</returns>
		/// <param name="value">A <see cref="T:System.DateTime" />. </param>
		/// <exception cref="T:System.InvalidCastException">This conversion is not supported. </exception>
		/// <filterpriority>1</filterpriority>
		public static float ToSingle(DateTime value)
		{
			throw new InvalidCastException("This conversion is not supported.");
		}

		/// <summary>Converts the value of the specified <see cref="T:System.Decimal" /> number to an equivalent single-precision floating point number.</summary>
		/// <returns>A single-precision floating point number equivalent to the value of <paramref name="value" />.<paramref name="value" /> is rounded using rounding to nearest. For example, when rounded to two decimals, the value 2.345 becomes 2.34 and the value 2.355 becomes 2.36.</returns>
		/// <param name="value">A <see cref="T:System.Decimal" /> number. </param>
		/// <filterpriority>1</filterpriority>
		public static float ToSingle(decimal value)
		{
			return (float)value;
		}

		/// <summary>Converts the value of the specified double-precision floating point number to an equivalent single-precision floating point number.</summary>
		/// <returns>A single-precision floating point number equivalent to the value of <paramref name="value" />.<paramref name="value" /> is rounded using rounding to nearest. For example, when rounded to two decimals, the value 2.345 becomes 2.34 and the value 2.355 becomes 2.36.</returns>
		/// <param name="value">A double-precision floating point number. </param>
		/// <filterpriority>1</filterpriority>
		public static float ToSingle(double value)
		{
			return (float)value;
		}

		/// <summary>Returns the specified single-precision floating point number; no actual conversion is performed.</summary>
		/// <returns>Parameter <paramref name="value" /> is returned unchanged.</returns>
		/// <param name="value">A single-precision floating point number. </param>
		/// <filterpriority>1</filterpriority>
		public static float ToSingle(float value)
		{
			return value;
		}

		/// <summary>Converts the value of the specified 32-bit signed integer to an equivalent single-precision floating point number.</summary>
		/// <returns>A single-precision floating point number equivalent to the value of <paramref name="value" />.</returns>
		/// <param name="value">A 32-bit signed integer. </param>
		/// <filterpriority>1</filterpriority>
		public static float ToSingle(int value)
		{
			return value;
		}

		/// <summary>Converts the value of the specified 64-bit signed integer to an equivalent single-precision floating point number.</summary>
		/// <returns>A single-precision floating point number equivalent to the value of <paramref name="value" />.</returns>
		/// <param name="value">A 64-bit signed integer. </param>
		/// <filterpriority>1</filterpriority>
		public static float ToSingle(long value)
		{
			return value;
		}

		/// <summary>Converts the value of the specified 8-bit signed integer to the equivalent single-precision floating point number.</summary>
		/// <returns>The 8-bit signed integer equivalent to the value of <paramref name="value" />.</returns>
		/// <param name="value">An 8-bit signed integer. </param>
		/// <filterpriority>1</filterpriority>
		[CLSCompliant(false)]
		public static float ToSingle(sbyte value)
		{
			return value;
		}

		/// <summary>Converts the value of the specified 16-bit signed integer to an equivalent single-precision floating point number.</summary>
		/// <returns>A single-precision floating point number equivalent to the value of <paramref name="value" />.</returns>
		/// <param name="value">A 16-bit signed integer. </param>
		/// <filterpriority>1</filterpriority>
		public static float ToSingle(short value)
		{
			return value;
		}

		/// <summary>Converts the specified <see cref="T:System.String" /> representation of a number to an equivalent single-precision floating point number.</summary>
		/// <returns>A single-precision floating point number equivalent to the value of <paramref name="value" />.-or- Zero if <paramref name="value" /> is null.</returns>
		/// <param name="value">A <see cref="T:System.String" /> containing a number to convert. </param>
		/// <exception cref="T:System.FormatException">
		///   <paramref name="value" /> is not a number in a valid format.</exception>
		/// <exception cref="T:System.OverflowException">
		///   <paramref name="value" /> represents a number less than <see cref="F:System.Single.MinValue" /> or greater than <see cref="F:System.Single.MaxValue" />. </exception>
		/// <filterpriority>1</filterpriority>
		public static float ToSingle(string value)
		{
			if (value == null)
			{
				return 0f;
			}
			return float.Parse(value);
		}

		/// <summary>Converts the specified <see cref="T:System.String" /> representation of a number to an equivalent single-precision floating point number using the specified culture-specific formatting information.</summary>
		/// <returns>A single-precision floating point number equivalent to the value of <paramref name="value" />.-or- Zero if <paramref name="value" /> is null.</returns>
		/// <param name="value">A <see cref="T:System.String" /> containing a number to convert. </param>
		/// <param name="provider">An <see cref="T:System.IFormatProvider" /> interface implementation that supplies culture-specific formatting information. </param>
		/// <exception cref="T:System.FormatException">
		///   <paramref name="value" /> is not a number in a valid format.</exception>
		/// <exception cref="T:System.OverflowException">
		///   <paramref name="value" /> represents a number less than <see cref="F:System.Single.MinValue" /> or greater than <see cref="F:System.Single.MaxValue" />. </exception>
		/// <filterpriority>1</filterpriority>
		public static float ToSingle(string value, IFormatProvider provider)
		{
			if (value == null)
			{
				return 0f;
			}
			return float.Parse(value, provider);
		}

		/// <summary>Converts the value of the specified 32-bit unsigned integer to an equivalent single-precision floating point number.</summary>
		/// <returns>A single-precision floating point number equivalent to the value of <paramref name="value" />.</returns>
		/// <param name="value">A 32-bit unsigned integer. </param>
		/// <filterpriority>1</filterpriority>
		[CLSCompliant(false)]
		public static float ToSingle(uint value)
		{
			return value;
		}

		/// <summary>Converts the value of the specified 64-bit unsigned integer to an equivalent single-precision floating point number.</summary>
		/// <returns>A single-precision floating point number equivalent to the value of <paramref name="value" />.</returns>
		/// <param name="value">A 64-bit unsigned integer. </param>
		/// <filterpriority>1</filterpriority>
		[CLSCompliant(false)]
		public static float ToSingle(ulong value)
		{
			return value;
		}

		/// <summary>Converts the value of the specified 16-bit unsigned integer to the equivalent single-precision floating point number.</summary>
		/// <returns>The single-precision floating point number equivalent to the value of <paramref name="value" />.</returns>
		/// <param name="value">A 16-bit unsigned integer. </param>
		/// <filterpriority>1</filterpriority>
		[CLSCompliant(false)]
		public static float ToSingle(ushort value)
		{
			return (int)value;
		}

		/// <summary>Converts the value of the specified <see cref="T:System.Object" /> to a single-precision floating point number.</summary>
		/// <returns>A single-precision floating point number equivalent to the value of <paramref name="value" />, or zero if <paramref name="value" /> is null.</returns>
		/// <param name="value">An <see cref="T:System.Object" /> that implements the <see cref="T:System.IConvertible" /> interface or null. </param>
		/// <exception cref="T:System.InvalidCastException">
		///   <paramref name="value" /> does not implement <see cref="T:System.IConvertible" />. </exception>
		/// <filterpriority>1</filterpriority>
		public static float ToSingle(object value)
		{
			if (value == null)
			{
				return 0f;
			}
			return ToSingle(value, null);
		}

		/// <summary>Converts the value of the specified <see cref="T:System.Object" /> to an single-precision floating point number using the specified culture-specific formatting information.</summary>
		/// <returns>A single-precision floating point number equivalent to the value of <paramref name="value" />, or zero if <paramref name="value" /> is null.</returns>
		/// <param name="value">An <see cref="T:System.Object" /> that implements the <see cref="T:System.IConvertible" /> interface. </param>
		/// <param name="provider">An <see cref="T:System.IFormatProvider" /> interface implementation that supplies culture-specific formatting information. </param>
		/// <exception cref="T:System.InvalidCastException">
		///   <paramref name="value" /> does not implement <see cref="T:System.IConvertible" />. </exception>
		/// <filterpriority>1</filterpriority>
		public static float ToSingle(object value, IFormatProvider provider)
		{
			if (value == null)
			{
				return 0f;
			}
			return ((IConvertible)value).ToSingle(provider);
		}

		/// <summary>Converts the value of the specified Boolean to its equivalent <see cref="T:System.String" /> representation.</summary>
		/// <returns>The <see cref="T:System.String" /> equivalent of the value of <paramref name="value" />.</returns>
		/// <param name="value">A Boolean value. </param>
		/// <filterpriority>1</filterpriority>
		public static string ToString(bool value)
		{
			return value.ToString();
		}

		/// <summary>Converts the value of the specified Boolean to its equivalent <see cref="T:System.String" /> representation.</summary>
		/// <returns>The <see cref="T:System.String" /> equivalent of the value of <paramref name="value" />.</returns>
		/// <param name="value">A Boolean value. </param>
		/// <param name="provider">(Reserved) An instance of an <see cref="T:System.IFormatProvider" /> interface implementation. </param>
		/// <filterpriority>1</filterpriority>
		public static string ToString(bool value, IFormatProvider provider)
		{
			return value.ToString();
		}

		/// <summary>Converts the value of the specified 8-bit unsigned integer to its equivalent <see cref="T:System.String" /> representation.</summary>
		/// <returns>The <see cref="T:System.String" /> equivalent of the value of <paramref name="value" />.</returns>
		/// <param name="value">An 8-bit unsigned integer. </param>
		/// <filterpriority>1</filterpriority>
		public static string ToString(byte value)
		{
			return value.ToString();
		}

		/// <summary>Converts the value of the specified 8-bit unsigned integer to its equivalent <see cref="T:System.String" /> representation.</summary>
		/// <returns>The <see cref="T:System.String" /> equivalent of the value of <paramref name="value" />.</returns>
		/// <param name="value">An 8-bit unsigned integer. </param>
		/// <param name="provider">An <see cref="T:System.IFormatProvider" /> interface implementation that supplies culture-specific formatting information. </param>
		/// <filterpriority>1</filterpriority>
		public static string ToString(byte value, IFormatProvider provider)
		{
			return value.ToString(provider);
		}

		/// <summary>Converts the value of an 8-bit unsigned integer to its equivalent <see cref="T:System.String" /> representation in a specified base.</summary>
		/// <returns>The <see cref="T:System.String" /> representation of <paramref name="value" /> in base <paramref name="toBase" />.</returns>
		/// <param name="value">An 8-bit unsigned integer. </param>
		/// <param name="toBase">The base of the return value, which must be 2, 8, 10, or 16. </param>
		/// <exception cref="T:System.ArgumentException">
		///   <paramref name="toBase" /> is not 2, 8, 10, or 16. </exception>
		/// <filterpriority>1</filterpriority>
		public static string ToString(byte value, int toBase)
		{
			if (value == 0)
			{
				return "0";
			}
			if (toBase == 10)
			{
				return value.ToString();
			}
			byte[] bytes = BitConverter.GetBytes(value);
			switch (toBase)
			{
			case 2:
				return ConvertToBase2(bytes);
			case 8:
				return ConvertToBase8(bytes);
			case 16:
				return ConvertToBase16(bytes);
			default:
				throw new ArgumentException(Locale.GetText("toBase is not valid."));
			}
		}

		/// <summary>Converts the value of the specified Unicode character to its equivalent <see cref="T:System.String" /> representation.</summary>
		/// <returns>The <see cref="T:System.String" /> equivalent of the value of <paramref name="value" />.</returns>
		/// <param name="value">A Unicode character. </param>
		/// <filterpriority>1</filterpriority>
		public static string ToString(char value)
		{
			return value.ToString();
		}

		/// <summary>Converts the value of the specified Unicode character to its equivalent <see cref="T:System.String" /> representation.</summary>
		/// <returns>The <see cref="T:System.String" /> equivalent of the value of <paramref name="value" />.</returns>
		/// <param name="value">A Unicode character. </param>
		/// <param name="provider">An <see cref="T:System.IFormatProvider" /> interface implementation that supplies culture-specific formatting information. </param>
		/// <filterpriority>1</filterpriority>
		public static string ToString(char value, IFormatProvider provider)
		{
			return value.ToString();
		}

		/// <summary>Converts the value of the specified <see cref="T:System.DateTime" /> to its equivalent <see cref="T:System.String" /> representation.</summary>
		/// <returns>The <see cref="T:System.String" /> equivalent of the value of <paramref name="value" />.</returns>
		/// <param name="value">A <see cref="T:System.DateTime" />. </param>
		/// <filterpriority>1</filterpriority>
		public static string ToString(DateTime value)
		{
			return value.ToString();
		}

		/// <summary>Converts the value of the specified <see cref="T:System.DateTime" /> to its equivalent <see cref="T:System.String" /> representation.</summary>
		/// <returns>The <see cref="T:System.String" /> equivalent of the value of <paramref name="value" />.</returns>
		/// <param name="value">A <see cref="T:System.DateTime" />. </param>
		/// <param name="provider">An <see cref="T:System.IFormatProvider" /> interface implementation that supplies culture-specific formatting information. </param>
		/// <filterpriority>1</filterpriority>
		public static string ToString(DateTime value, IFormatProvider provider)
		{
			return value.ToString(provider);
		}

		/// <summary>Converts the value of the specified <see cref="T:System.Decimal" /> number to its equivalent <see cref="T:System.String" /> representation.</summary>
		/// <returns>The <see cref="T:System.String" /> equivalent of the value of <paramref name="value" />.</returns>
		/// <param name="value">A <see cref="T:System.Decimal" /> number. </param>
		/// <filterpriority>1</filterpriority>
		public static string ToString(decimal value)
		{
			return value.ToString();
		}

		/// <summary>Converts the value of the specified <see cref="T:System.Decimal" /> number to its equivalent <see cref="T:System.String" /> representation.</summary>
		/// <returns>The <see cref="T:System.String" /> equivalent of the value of <paramref name="value" />.</returns>
		/// <param name="value">A <see cref="T:System.Decimal" /> number. </param>
		/// <param name="provider">An <see cref="T:System.IFormatProvider" /> interface implementation that supplies culture-specific formatting information. </param>
		/// <filterpriority>1</filterpriority>
		public static string ToString(decimal value, IFormatProvider provider)
		{
			return value.ToString(provider);
		}

		/// <summary>Converts the value of the specified double-precision floating point number to its equivalent <see cref="T:System.String" /> representation.</summary>
		/// <returns>The <see cref="T:System.String" /> equivalent of the value of <paramref name="value" />.</returns>
		/// <param name="value">A double-precision floating point number. </param>
		/// <filterpriority>1</filterpriority>
		public static string ToString(double value)
		{
			return value.ToString();
		}

		/// <summary>Converts the value of the specified double-precision floating point number to its equivalent <see cref="T:System.String" /> representation.</summary>
		/// <returns>The <see cref="T:System.String" /> equivalent of the value of <paramref name="value" />.<paramref name="provider" /> is ignored; it does not participate in this operation.</returns>
		/// <param name="value">A double-precision floating point number. </param>
		/// <param name="provider">An <see cref="T:System.IFormatProvider" /> interface implementation that supplies culture-specific formatting information. </param>
		/// <filterpriority>1</filterpriority>
		public static string ToString(double value, IFormatProvider provider)
		{
			return value.ToString(provider);
		}

		/// <summary>Converts the value of the specified single-precision floating point number to its equivalent <see cref="T:System.String" /> representation.</summary>
		/// <returns>The <see cref="T:System.String" /> equivalent of the value of <paramref name="value" />.</returns>
		/// <param name="value">A single-precision floating point number. </param>
		/// <filterpriority>1</filterpriority>
		public static string ToString(float value)
		{
			return value.ToString();
		}

		/// <summary>Converts the value of the specified single-precision floating point number to its equivalent <see cref="T:System.String" /> representation.</summary>
		/// <returns>The <see cref="T:System.String" /> equivalent of the value of <paramref name="value" />.</returns>
		/// <param name="value">A single-precision floating point number. </param>
		/// <param name="provider">An <see cref="T:System.IFormatProvider" /> interface implementation that supplies culture-specific formatting information. </param>
		/// <filterpriority>1</filterpriority>
		public static string ToString(float value, IFormatProvider provider)
		{
			return value.ToString(provider);
		}

		/// <summary>Converts the value of the specified 32-bit signed integer to its equivalent <see cref="T:System.String" /> representation.</summary>
		/// <returns>The <see cref="T:System.String" /> equivalent of the value of <paramref name="value" />.</returns>
		/// <param name="value">A 32-bit signed integer. </param>
		/// <filterpriority>1</filterpriority>
		public static string ToString(int value)
		{
			return value.ToString();
		}

		/// <summary>Converts the value of a 32-bit signed integer to its equivalent <see cref="T:System.String" /> representation in a specified base.</summary>
		/// <returns>The <see cref="T:System.String" /> representation of <paramref name="value" /> in base <paramref name="toBase" />.</returns>
		/// <param name="value">A 32-bit signed integer. </param>
		/// <param name="toBase">The base of the return value, which must be 2, 8, 10, or 16. </param>
		/// <exception cref="T:System.ArgumentException">
		///   <paramref name="toBase" /> is not 2, 8, 10, or 16. </exception>
		/// <filterpriority>1</filterpriority>
		public static string ToString(int value, int toBase)
		{
			if (value == 0)
			{
				return "0";
			}
			if (toBase == 10)
			{
				return value.ToString();
			}
			byte[] bytes = BitConverter.GetBytes(value);
			switch (toBase)
			{
			case 2:
				return ConvertToBase2(bytes);
			case 8:
				return ConvertToBase8(bytes);
			case 16:
				return ConvertToBase16(bytes);
			default:
				throw new ArgumentException(Locale.GetText("toBase is not valid."));
			}
		}

		/// <summary>Converts the value of the specified 32-bit signed integer to its equivalent <see cref="T:System.String" /> representation.</summary>
		/// <returns>The <see cref="T:System.String" /> equivalent of the value of <paramref name="value" />.</returns>
		/// <param name="value">A 32-bit signed integer. </param>
		/// <param name="provider">An <see cref="T:System.IFormatProvider" /> interface implementation that supplies culture-specific formatting information. </param>
		/// <filterpriority>1</filterpriority>
		public static string ToString(int value, IFormatProvider provider)
		{
			return value.ToString(provider);
		}

		/// <summary>Converts the value of the specified 64-bit signed integer to its equivalent <see cref="T:System.String" /> representation.</summary>
		/// <returns>The <see cref="T:System.String" /> equivalent of the value of <paramref name="value" />.</returns>
		/// <param name="value">A 64-bit signed integer. </param>
		/// <filterpriority>1</filterpriority>
		public static string ToString(long value)
		{
			return value.ToString();
		}

		/// <summary>Converts the value of a 64-bit signed integer to its equivalent <see cref="T:System.String" /> representation in a specified base.</summary>
		/// <returns>The <see cref="T:System.String" /> representation of <paramref name="value" /> in base <paramref name="toBase" />.</returns>
		/// <param name="value">A 64-bit signed integer. </param>
		/// <param name="toBase">The base of the return value, which must be 2, 8, 10, or 16. </param>
		/// <exception cref="T:System.ArgumentException">
		///   <paramref name="toBase" /> is not 2, 8, 10, or 16. </exception>
		/// <filterpriority>1</filterpriority>
		public static string ToString(long value, int toBase)
		{
			if (value == 0L)
			{
				return "0";
			}
			if (toBase == 10)
			{
				return value.ToString();
			}
			byte[] bytes = BitConverter.GetBytes(value);
			switch (toBase)
			{
			case 2:
				return ConvertToBase2(bytes);
			case 8:
				return ConvertToBase8(bytes);
			case 16:
				return ConvertToBase16(bytes);
			default:
				throw new ArgumentException(Locale.GetText("toBase is not valid."));
			}
		}

		/// <summary>Converts the value of the specified 64-bit signed integer to its equivalent <see cref="T:System.String" /> representation.</summary>
		/// <returns>The <see cref="T:System.String" /> equivalent of the value of <paramref name="value" />.</returns>
		/// <param name="value">A 64-bit signed integer. </param>
		/// <param name="provider">An <see cref="T:System.IFormatProvider" /> interface implementation that supplies culture-specific formatting information. </param>
		/// <filterpriority>1</filterpriority>
		public static string ToString(long value, IFormatProvider provider)
		{
			return value.ToString(provider);
		}

		/// <summary>Converts the value of the specified <see cref="T:System.Object" /> to its <see cref="T:System.String" /> representation.</summary>
		/// <returns>The <see cref="T:System.String" /> representation of the value of <paramref name="value" />, or <see cref="F:System.String.Empty" /> if value is null.</returns>
		/// <param name="value">An <see cref="T:System.Object" /> or null. </param>
		/// <filterpriority>1</filterpriority>
		public static string ToString(object value)
		{
			return ToString(value, null);
		}

		/// <summary>Converts the value of the specified <see cref="T:System.Object" /> to its equivalent <see cref="T:System.String" /> representation using the specified culture-specific formatting information.</summary>
		/// <returns>The <see cref="T:System.String" /> representation of the value of <paramref name="value" />, or <see cref="F:System.String.Empty" /> if <paramref name="value" /> is null.</returns>
		/// <param name="value">An <see cref="T:System.Object" /> or null. </param>
		/// <param name="provider">An <see cref="T:System.IFormatProvider" /> interface implementation that supplies culture-specific formatting information. </param>
		/// <filterpriority>1</filterpriority>
		public static string ToString(object value, IFormatProvider provider)
		{
			if (value is IConvertible)
			{
				return ((IConvertible)value).ToString(provider);
			}
			if (value != null)
			{
				return value.ToString();
			}
			return string.Empty;
		}

		/// <summary>Converts the value of the specified 8-bit signed integer to its equivalent <see cref="T:System.String" /> representation.</summary>
		/// <returns>The <see cref="T:System.String" /> equivalent of the value of <paramref name="value" />.</returns>
		/// <param name="value">An 8-bit signed integer. </param>
		/// <filterpriority>1</filterpriority>
		[CLSCompliant(false)]
		public static string ToString(sbyte value)
		{
			return value.ToString();
		}

		/// <summary>Converts the value of the specified 8-bit signed integer to its equivalent <see cref="T:System.String" /> representation.</summary>
		/// <returns>The <see cref="T:System.String" /> equivalent of the value of <paramref name="value" />.</returns>
		/// <param name="value">An 8-bit signed integer. </param>
		/// <param name="provider">An <see cref="T:System.IFormatProvider" /> interface implementation that supplies culture-specific formatting information. </param>
		/// <filterpriority>1</filterpriority>
		[CLSCompliant(false)]
		public static string ToString(sbyte value, IFormatProvider provider)
		{
			return value.ToString(provider);
		}

		/// <summary>Converts the value of the specified 16-bit signed integer to its equivalent <see cref="T:System.String" /> representation.</summary>
		/// <returns>The <see cref="T:System.String" /> equivalent of the value of <paramref name="value" />.</returns>
		/// <param name="value">A 16-bit signed integer. </param>
		/// <filterpriority>1</filterpriority>
		public static string ToString(short value)
		{
			return value.ToString();
		}

		/// <summary>Converts the value of a 16-bit signed integer to its equivalent <see cref="T:System.String" /> representation in a specified base.</summary>
		/// <returns>The <see cref="T:System.String" /> representation of <paramref name="value" /> in base <paramref name="toBase" />.</returns>
		/// <param name="value">A 16-bit signed integer. </param>
		/// <param name="toBase">The base of the return value, which must be 2, 8, 10, or 16. </param>
		/// <exception cref="T:System.ArgumentException">
		///   <paramref name="toBase" /> is not 2, 8, 10, or 16. </exception>
		/// <filterpriority>1</filterpriority>
		public static string ToString(short value, int toBase)
		{
			if (value == 0)
			{
				return "0";
			}
			if (toBase == 10)
			{
				return value.ToString();
			}
			byte[] bytes = BitConverter.GetBytes(value);
			switch (toBase)
			{
			case 2:
				return ConvertToBase2(bytes);
			case 8:
				return ConvertToBase8(bytes);
			case 16:
				return ConvertToBase16(bytes);
			default:
				throw new ArgumentException(Locale.GetText("toBase is not valid."));
			}
		}

		/// <summary>Converts the value of the specified 16-bit signed integer to its equivalent <see cref="T:System.String" /> representation.</summary>
		/// <returns>The <see cref="T:System.String" /> equivalent of the value of <paramref name="value" />.</returns>
		/// <param name="value">A 16-bit signed integer. </param>
		/// <param name="provider">An <see cref="T:System.IFormatProvider" /> interface implementation that supplies culture-specific formatting information. </param>
		/// <filterpriority>1</filterpriority>
		public static string ToString(short value, IFormatProvider provider)
		{
			return value.ToString(provider);
		}

		/// <summary>Returns the specified instance of <see cref="T:System.String" />; no actual conversion is performed.</summary>
		/// <returns>Parameter <paramref name="value" /> is returned unchanged.</returns>
		/// <param name="value">A <see cref="T:System.String" />. </param>
		/// <filterpriority>1</filterpriority>
		public static string ToString(string value)
		{
			return value;
		}

		/// <summary>Returns the specified instance of <see cref="T:System.String" />; no actual conversion is performed.</summary>
		/// <returns>Parameter <paramref name="value" /> is returned unchanged.</returns>
		/// <param name="value">A <see cref="T:System.String" />. </param>
		/// <param name="provider">An <see cref="T:System.IFormatProvider" /> interface implementation that supplies culture-specific formatting information. </param>
		/// <filterpriority>1</filterpriority>
		public static string ToString(string value, IFormatProvider provider)
		{
			return value;
		}

		/// <summary>Converts the value of the specified 32-bit unsigned integer to its equivalent <see cref="T:System.String" /> representation.</summary>
		/// <returns>The <see cref="T:System.String" /> equivalent of the value of <paramref name="value" />.</returns>
		/// <param name="value">A 32-bit unsigned integer. </param>
		/// <filterpriority>1</filterpriority>
		[CLSCompliant(false)]
		public static string ToString(uint value)
		{
			return value.ToString();
		}

		/// <summary>Converts the value of the specified 32-bit unsigned integer to its equivalent <see cref="T:System.String" /> representation.</summary>
		/// <returns>The <see cref="T:System.String" /> equivalent of the value of <paramref name="value" />.</returns>
		/// <param name="value">A 32-bit unsigned integer. </param>
		/// <param name="provider">An <see cref="T:System.IFormatProvider" /> interface implementation that supplies culture-specific formatting information. </param>
		/// <filterpriority>1</filterpriority>
		[CLSCompliant(false)]
		public static string ToString(uint value, IFormatProvider provider)
		{
			return value.ToString(provider);
		}

		/// <summary>Converts the value of the specified 64-bit unsigned integer to its equivalent <see cref="T:System.String" /> representation.</summary>
		/// <returns>The <see cref="T:System.String" /> equivalent of the value of <paramref name="value" />.</returns>
		/// <param name="value">A 64-bit unsigned integer. </param>
		/// <filterpriority>1</filterpriority>
		[CLSCompliant(false)]
		public static string ToString(ulong value)
		{
			return value.ToString();
		}

		/// <summary>Converts the value of the specified 64-bit unsigned integer to its equivalent <see cref="T:System.String" /> representation.</summary>
		/// <returns>The <see cref="T:System.String" /> equivalent of the value of <paramref name="value" />.</returns>
		/// <param name="value">A 64-bit unsigned integer. </param>
		/// <param name="provider">An <see cref="T:System.IFormatProvider" /> interface implementation that supplies culture-specific formatting information. </param>
		/// <filterpriority>1</filterpriority>
		[CLSCompliant(false)]
		public static string ToString(ulong value, IFormatProvider provider)
		{
			return value.ToString(provider);
		}

		/// <summary>Converts the value of the specified 16-bit unsigned integer to its equivalent <see cref="T:System.String" /> representation.</summary>
		/// <returns>The <see cref="T:System.String" /> equivalent of the value of <paramref name="value" />.</returns>
		/// <param name="value">A 16-bit unsigned integer. </param>
		/// <filterpriority>1</filterpriority>
		[CLSCompliant(false)]
		public static string ToString(ushort value)
		{
			return value.ToString();
		}

		/// <summary>Converts the value of the specified 16-bit unsigned integer to its equivalent <see cref="T:System.String" /> representation.</summary>
		/// <returns>The <see cref="T:System.String" /> equivalent of the value of <paramref name="value" />.</returns>
		/// <param name="value">A 16-bit unsigned integer. </param>
		/// <param name="provider">An <see cref="T:System.IFormatProvider" /> interface implementation that supplies culture-specific formatting information. </param>
		/// <filterpriority>1</filterpriority>
		[CLSCompliant(false)]
		public static string ToString(ushort value, IFormatProvider provider)
		{
			return value.ToString(provider);
		}

		/// <summary>Converts the value of the specified Boolean value to the equivalent 16-bit unsigned integer.</summary>
		/// <returns>The number 1 if <paramref name="value" /> is true; otherwise, 0.</returns>
		/// <param name="value">A Boolean value. </param>
		/// <filterpriority>1</filterpriority>
		[CLSCompliant(false)]
		public static ushort ToUInt16(bool value)
		{
			return (ushort)(value ? 1 : 0);
		}

		/// <summary>Converts the value of the specified 8-bit unsigned integer to the equivalent 16-bit unsigned integer.</summary>
		/// <returns>The 16-bit unsigned integer equivalent to the value of <paramref name="value" />.</returns>
		/// <param name="value">An 8-bit unsigned integer. </param>
		/// <filterpriority>1</filterpriority>
		[CLSCompliant(false)]
		public static ushort ToUInt16(byte value)
		{
			return value;
		}

		/// <summary>Converts the value of the specified Unicode character to the equivalent 16-bit unsigned integer.</summary>
		/// <returns>The 16-bit unsigned integer equivalent to <paramref name="value" />.</returns>
		/// <param name="value">A Unicode character. </param>
		/// <filterpriority>1</filterpriority>
		[CLSCompliant(false)]
		public static ushort ToUInt16(char value)
		{
			return value;
		}

		/// <summary>Calling this method always throws <see cref="T:System.InvalidCastException" />.</summary>
		/// <returns>This conversion is not supported. No value is returned.</returns>
		/// <param name="value">A <see cref="T:System.DateTime" />. </param>
		/// <exception cref="T:System.InvalidCastException">This conversion is not supported. </exception>
		/// <filterpriority>1</filterpriority>
		[CLSCompliant(false)]
		public static ushort ToUInt16(DateTime value)
		{
			throw new InvalidCastException("This conversion is not supported.");
		}

		/// <summary>Converts the value of the specified <see cref="T:System.Decimal" /> number to an equivalent 16-bit unsigned integer.</summary>
		/// <returns>
		///   <paramref name="value" /> rounded to the nearest 16-bit unsigned integer. If <paramref name="value" /> is halfway between two whole numbers, the even number is returned; that is, 4.5 is converted to 4, and 5.5 is converted to 6.</returns>
		/// <param name="value">A <see cref="T:System.Decimal" /> number. </param>
		/// <exception cref="T:System.OverflowException">
		///   <paramref name="value" /> is less than zero or greater than <see cref="F:System.UInt16.MaxValue" />. </exception>
		/// <filterpriority>1</filterpriority>
		[CLSCompliant(false)]
		public static ushort ToUInt16(decimal value)
		{
			if (value > 65535m || value < 0m)
			{
				throw new OverflowException(Locale.GetText("Value is greater than UInt16.MaxValue or less than UInt16.MinValue"));
			}
			return (ushort)Math.Round(value);
		}

		/// <summary>Converts the value of the specified double-precision floating point number to an equivalent 16-bit unsigned integer.</summary>
		/// <returns>
		///   <paramref name="value" /> rounded to the nearest 16-bit unsigned integer. If <paramref name="value" /> is halfway between two whole numbers, the even number is returned; that is, 4.5 is converted to 4, and 5.5 is converted to 6.</returns>
		/// <param name="value">A double-precision floating point number. </param>
		/// <exception cref="T:System.OverflowException">
		///   <paramref name="value" /> is less than zero or greater than <see cref="F:System.UInt16.MaxValue" />. </exception>
		/// <filterpriority>1</filterpriority>
		[CLSCompliant(false)]
		public static ushort ToUInt16(double value)
		{
			if (value > 65535.0 || value < 0.0)
			{
				throw new OverflowException(Locale.GetText("Value is greater than UInt16.MaxValue or less than UInt16.MinValue"));
			}
			return (ushort)Math.Round(value);
		}

		/// <summary>Converts the value of the specified single-precision floating point number to an equivalent 16-bit unsigned integer.</summary>
		/// <returns>
		///   <paramref name="value" /> rounded to the nearest 16-bit unsigned integer. If <paramref name="value" /> is halfway between two whole numbers, the even number is returned; that is, 4.5 is converted to 4, and 5.5 is converted to 6.</returns>
		/// <param name="value">A single-precision floating point number. </param>
		/// <exception cref="T:System.OverflowException">
		///   <paramref name="value" /> is less than zero or greater than <see cref="F:System.UInt16.MaxValue" />. </exception>
		/// <filterpriority>1</filterpriority>
		[CLSCompliant(false)]
		public static ushort ToUInt16(float value)
		{
			if (value > 65535f || value < 0f)
			{
				throw new OverflowException(Locale.GetText("Value is greater than UInt16.MaxValue or less than UInt16.MinValue"));
			}
			return (ushort)Math.Round(value);
		}

		/// <summary>Converts the value of the specified 32-bit signed integer to an equivalent 16-bit unsigned integer.</summary>
		/// <returns>The 16-bit unsigned integer equivalent of <paramref name="value" />.</returns>
		/// <param name="value">A 32-bit signed integer. </param>
		/// <exception cref="T:System.OverflowException">
		///   <paramref name="value" /> is less than zero or greater than <see cref="F:System.UInt16.MaxValue" />. </exception>
		/// <filterpriority>1</filterpriority>
		[CLSCompliant(false)]
		public static ushort ToUInt16(int value)
		{
			if (value > 65535 || value < 0)
			{
				throw new OverflowException(Locale.GetText("Value is greater than UInt16.MaxValue or less than UInt16.MinValue"));
			}
			return (ushort)value;
		}

		/// <summary>Converts the value of the specified 64-bit signed integer to an equivalent 16-bit unsigned integer.</summary>
		/// <returns>A 16-bit unsigned integer equivalent to the value of <paramref name="value" />.</returns>
		/// <param name="value">A 64-bit signed integer. </param>
		/// <exception cref="T:System.OverflowException">
		///   <paramref name="value" /> is less than zero or greater than <see cref="F:System.UInt16.MaxValue" />. </exception>
		/// <filterpriority>1</filterpriority>
		[CLSCompliant(false)]
		public static ushort ToUInt16(long value)
		{
			if (value > 65535 || value < 0)
			{
				throw new OverflowException(Locale.GetText("Value is greater than UInt16.MaxValue or less than UInt16.MinValue"));
			}
			return (ushort)value;
		}

		/// <summary>Converts the value of the specified 8-bit signed integer to the equivalent 16-bit unsigned integer.</summary>
		/// <returns>The 16-bit unsigned integer equivalent to the value of <paramref name="value" />.</returns>
		/// <param name="value">An 8-bit signed integer. </param>
		/// <exception cref="T:System.OverflowException">
		///   <paramref name="value" /> is less than zero. </exception>
		/// <filterpriority>1</filterpriority>
		[CLSCompliant(false)]
		public static ushort ToUInt16(sbyte value)
		{
			if (value < 0)
			{
				throw new OverflowException(Locale.GetText("Value is less than UInt16.MinValue"));
			}
			return (ushort)value;
		}

		/// <summary>Converts the value of the specified 16-bit signed integer to the equivalent 16-bit unsigned integer.</summary>
		/// <returns>The 16-bit unsigned integer equivalent to the value of <paramref name="value" />.</returns>
		/// <param name="value">A 16-bit signed integer. </param>
		/// <exception cref="T:System.OverflowException">
		///   <paramref name="value" /> is less than zero. </exception>
		/// <filterpriority>1</filterpriority>
		[CLSCompliant(false)]
		public static ushort ToUInt16(short value)
		{
			if (value < 0)
			{
				throw new OverflowException(Locale.GetText("Value is less than UInt16.MinValue"));
			}
			return (ushort)value;
		}

		/// <summary>Converts the specified <see cref="T:System.String" /> representation of a number to an equivalent 16-bit unsigned integer.</summary>
		/// <returns>A 16-bit unsigned integer equivalent to the value of <paramref name="value" />.-or- Zero if <paramref name="value" /> is null.</returns>
		/// <param name="value">A <see cref="T:System.String" /> containing a number to convert. </param>
		/// <exception cref="T:System.FormatException">
		///   <paramref name="value" /> does not consist of an optional sign followed by a sequence of digits (zero through nine). </exception>
		/// <exception cref="T:System.OverflowException">
		///   <paramref name="value" /> represents a number less than <see cref="F:System.Int16.MinValue" /> or greater than <see cref="F:System.Int16.MaxValue" />. </exception>
		/// <filterpriority>1</filterpriority>
		[CLSCompliant(false)]
		public static ushort ToUInt16(string value)
		{
			if (value == null)
			{
				return 0;
			}
			return ushort.Parse(value);
		}

		/// <summary>Converts the specified <see cref="T:System.String" /> representation of a number to an equivalent 16-bit unsigned integer using specified culture-specific formatting information.</summary>
		/// <returns>A 16-bit unsigned integer equivalent to the value of <paramref name="value" />.-or- Zero if <paramref name="value" /> is null.</returns>
		/// <param name="value">A <see cref="T:System.String" /> containing a number to convert. </param>
		/// <param name="provider">An <see cref="T:System.IFormatProvider" /> interface implementation that supplies culture-specific formatting information. </param>
		/// <exception cref="T:System.FormatException">
		///   <paramref name="value" /> does not consist of an optional sign followed by a sequence of digits (zero through nine). </exception>
		/// <exception cref="T:System.OverflowException">
		///   <paramref name="value" /> represents a number less than <see cref="F:System.Int16.MinValue" /> or greater than <see cref="F:System.Int16.MaxValue" />. </exception>
		/// <filterpriority>1</filterpriority>
		[CLSCompliant(false)]
		public static ushort ToUInt16(string value, IFormatProvider provider)
		{
			if (value == null)
			{
				return 0;
			}
			return ushort.Parse(value, provider);
		}

		/// <summary>Converts the <see cref="T:System.String" /> representation of a number in a specified base to an equivalent 16-bit unsigned integer.</summary>
		/// <returns>A 16-bit unsigned integer equivalent to the number in <paramref name="value" />.-or- Zero if <paramref name="value" /> is null.</returns>
		/// <param name="value">A <see cref="T:System.String" /> containing a number. </param>
		/// <param name="fromBase">The base of the number in <paramref name="value" />, which must be 2, 8, 10, or 16. </param>
		/// <exception cref="T:System.ArgumentException">
		///   <paramref name="fromBase" /> is not 2, 8, 10, or 16. -or-<paramref name="value" />, which represents a non-base 10 unsigned number, is prefixed with a negative sign. </exception>
		/// <exception cref="T:System.FormatException">
		///   <paramref name="value" /> contains a character that is not a valid digit in the base specified by <paramref name="fromBase" />. The exception message indicates that there are no digits to convert if the first character in <paramref name="value" /> is invalid; otherwise, the message indicates that <paramref name="value" /> contains invalid trailing characters.</exception>
		/// <exception cref="T:System.OverflowException">
		///   <paramref name="value" />, which represents a base 10 unsigned number, is prefixed with a negative sign.-or-The return value is less than <see cref="F:System.UInt16.MinValue" /> or larger than <see cref="F:System.UInt16.MaxValue" />.</exception>
		/// <filterpriority>1</filterpriority>
		[CLSCompliant(false)]
		public static ushort ToUInt16(string value, int fromBase)
		{
			return ToUInt16(ConvertFromBase(value, fromBase, unsigned: true));
		}

		/// <summary>Converts the value of the specified 32-bit unsigned integer to an equivalent 16-bit unsigned integer.</summary>
		/// <returns>A 16-bit unsigned integer equivalent to the value of <paramref name="value" />.</returns>
		/// <param name="value">A 32-bit unsigned integer. </param>
		/// <exception cref="T:System.OverflowException">
		///   <paramref name="value" /> is greater than <see cref="F:System.UInt16.MaxValue" />. </exception>
		/// <filterpriority>1</filterpriority>
		[CLSCompliant(false)]
		public static ushort ToUInt16(uint value)
		{
			if (value > 65535)
			{
				throw new OverflowException(Locale.GetText("Value is greater than UInt16.MaxValue"));
			}
			return (ushort)value;
		}

		/// <summary>Converts the value of the specified 64-bit unsigned integer to an equivalent 16-bit unsigned integer.</summary>
		/// <returns>A 16-bit unsigned integer equivalent to the value of <paramref name="value" />.</returns>
		/// <param name="value">A 64-bit unsigned integer. </param>
		/// <exception cref="T:System.OverflowException">
		///   <paramref name="value" /> is greater than <see cref="F:System.UInt16.MaxValue" />. </exception>
		/// <filterpriority>1</filterpriority>
		[CLSCompliant(false)]
		public static ushort ToUInt16(ulong value)
		{
			if (value > 65535)
			{
				throw new OverflowException(Locale.GetText("Value is greater than UInt16.MaxValue"));
			}
			return (ushort)value;
		}

		/// <summary>Returns the specified 16-bit unsigned integer; no actual conversion is performed.</summary>
		/// <returns>Parameter <paramref name="value" /> is returned unchanged.</returns>
		/// <param name="value">A 16-bit unsigned integer. </param>
		/// <filterpriority>1</filterpriority>
		[CLSCompliant(false)]
		public static ushort ToUInt16(ushort value)
		{
			return value;
		}

		/// <summary>Converts the value of the specified <see cref="T:System.Object" /> to a 16-bit unsigned integer.</summary>
		/// <returns>A 16-bit unsigned integer equivalent to the value of <paramref name="value" />, or zero if <paramref name="value" /> is null.</returns>
		/// <param name="value">An <see cref="T:System.Object" /> that implements the <see cref="T:System.IConvertible" /> interface or null. </param>
		/// <exception cref="T:System.InvalidCastException">
		///   <paramref name="value" /> does not implement <see cref="T:System.IConvertible" />. </exception>
		/// <filterpriority>1</filterpriority>
		[CLSCompliant(false)]
		public static ushort ToUInt16(object value)
		{
			if (value == null)
			{
				return 0;
			}
			return ToUInt16(value, null);
		}

		/// <summary>Converts the value of the specified <see cref="T:System.Object" /> to a 16-bit unsigned integer using the specified culture-specific formatting information.</summary>
		/// <returns>A 16-bit unsigned integer equivalent to the value of <paramref name="value" />, or zero if <paramref name="value" /> is null.</returns>
		/// <param name="value">An <see cref="T:System.Object" /> that implements the <see cref="T:System.IConvertible" /> interface. </param>
		/// <param name="provider">An <see cref="T:System.IFormatProvider" /> interface implementation that supplies culture-specific formatting information. </param>
		/// <filterpriority>1</filterpriority>
		[CLSCompliant(false)]
		public static ushort ToUInt16(object value, IFormatProvider provider)
		{
			if (value == null)
			{
				return 0;
			}
			return ((IConvertible)value).ToUInt16(provider);
		}

		/// <summary>Converts the value of the specified Boolean value to the equivalent 32-bit unsigned integer.</summary>
		/// <returns>The number 1 if <paramref name="value" /> is true; otherwise, 0.</returns>
		/// <param name="value">A Boolean value. </param>
		/// <filterpriority>1</filterpriority>
		[CLSCompliant(false)]
		public static uint ToUInt32(bool value)
		{
			return value ? 1u : 0u;
		}

		/// <summary>Converts the value of the specified 8-bit unsigned integer to the equivalent 32-bit unsigned integer.</summary>
		/// <returns>The 32-bit unsigned integer equivalent to the value of <paramref name="value" />.</returns>
		/// <param name="value">An 8-bit unsigned integer. </param>
		/// <filterpriority>1</filterpriority>
		[CLSCompliant(false)]
		public static uint ToUInt32(byte value)
		{
			return value;
		}

		/// <summary>Converts the value of the specified Unicode character to the equivalent 32-bit unsigned integer.</summary>
		/// <returns>The 32-bit unsigned integer equivalent to <paramref name="value" />.</returns>
		/// <param name="value">A Unicode character. </param>
		/// <filterpriority>1</filterpriority>
		[CLSCompliant(false)]
		public static uint ToUInt32(char value)
		{
			return value;
		}

		/// <summary>Calling this method always throws <see cref="T:System.InvalidCastException" />.</summary>
		/// <returns>This conversion is not supported. No value is returned.</returns>
		/// <param name="value">A <see cref="T:System.DateTime" />. </param>
		/// <exception cref="T:System.InvalidCastException">This conversion is not supported. </exception>
		/// <filterpriority>1</filterpriority>
		[CLSCompliant(false)]
		public static uint ToUInt32(DateTime value)
		{
			throw new InvalidCastException("This conversion is not supported.");
		}

		/// <summary>Converts the value of the specified <see cref="T:System.Decimal" /> number to an equivalent 32-bit unsigned integer.</summary>
		/// <returns>
		///   <paramref name="value" /> rounded to the nearest 32-bit unsigned integer. If <paramref name="value" /> is halfway between two whole numbers, the even number is returned; that is, 4.5 is converted to 4, and 5.5 is converted to 6.</returns>
		/// <param name="value">A <see cref="T:System.Decimal" /> number. </param>
		/// <exception cref="T:System.OverflowException">
		///   <paramref name="value" /> is less than zero or greater than <see cref="F:System.UInt32.MaxValue" />. </exception>
		/// <filterpriority>1</filterpriority>
		[CLSCompliant(false)]
		public static uint ToUInt32(decimal value)
		{
			if (value > 4294967295m || value < 0m)
			{
				throw new OverflowException(Locale.GetText("Value is greater than UInt32.MaxValue or less than UInt32.MinValue"));
			}
			return (uint)Math.Round(value);
		}

		/// <summary>Converts the value of the specified double-precision floating point number to an equivalent 32-bit unsigned integer.</summary>
		/// <returns>
		///   <paramref name="value" /> rounded to the nearest 32-bit unsigned integer. If <paramref name="value" /> is halfway between two whole numbers, the even number is returned; that is, 4.5 is converted to 4, and 5.5 is converted to 6.</returns>
		/// <param name="value">A double-precision floating point number. </param>
		/// <exception cref="T:System.OverflowException">
		///   <paramref name="value" /> is less than zero or greater than <see cref="F:System.UInt32.MaxValue" />. </exception>
		/// <filterpriority>1</filterpriority>
		[CLSCompliant(false)]
		public static uint ToUInt32(double value)
		{
			if (value > 4294967295.0 || value < 0.0)
			{
				throw new OverflowException(Locale.GetText("Value is greater than UInt32.MaxValue or less than UInt32.MinValue"));
			}
			return (uint)Math.Round(value);
		}

		/// <summary>Converts the value of the specified single-precision floating point number to an equivalent 32-bit unsigned integer.</summary>
		/// <returns>
		///   <paramref name="value" /> rounded to the nearest 32-bit unsigned integer. If <paramref name="value" /> is halfway between two whole numbers, the even number is returned; that is, 4.5 is converted to 4, and 5.5 is converted to 6.</returns>
		/// <param name="value">A single-precision floating point number. </param>
		/// <exception cref="T:System.OverflowException">
		///   <paramref name="value" /> is less than zero or greater than <see cref="F:System.UInt32.MaxValue" />. </exception>
		/// <filterpriority>1</filterpriority>
		[CLSCompliant(false)]
		public static uint ToUInt32(float value)
		{
			if (value > 4.2949673E+09f || value < 0f)
			{
				throw new OverflowException(Locale.GetText("Value is greater than UInt32.MaxValue or less than UInt32.MinValue"));
			}
			return (uint)Math.Round(value);
		}

		/// <summary>Converts the value of the specified 32-bit signed integer to an equivalent 32-bit unsigned integer.</summary>
		/// <returns>The 32-bit unsigned integer equivalent of <paramref name="value" />.</returns>
		/// <param name="value">A 32-bit signed integer. </param>
		/// <exception cref="T:System.OverflowException">
		///   <paramref name="value" /> is less than zero. </exception>
		/// <filterpriority>1</filterpriority>
		[CLSCompliant(false)]
		public static uint ToUInt32(int value)
		{
			if ((long)value < 0L)
			{
				throw new OverflowException(Locale.GetText("Value is less than UInt32.MinValue"));
			}
			return (uint)value;
		}

		/// <summary>Converts the value of the specified 64-bit signed integer to an equivalent 32-bit unsigned integer.</summary>
		/// <returns>A 32-bit unsigned integer equivalent to the value of <paramref name="value" />.</returns>
		/// <param name="value">A 64-bit signed integer. </param>
		/// <exception cref="T:System.OverflowException">
		///   <paramref name="value" /> is less than zero or greater than <see cref="F:System.UInt32.MaxValue" />. </exception>
		/// <filterpriority>1</filterpriority>
		[CLSCompliant(false)]
		public static uint ToUInt32(long value)
		{
			if (value > uint.MaxValue || value < 0)
			{
				throw new OverflowException(Locale.GetText("Value is greater than UInt32.MaxValue or less than UInt32.MinValue"));
			}
			return (uint)value;
		}

		/// <summary>Converts the value of the specified 8-bit signed integer to the equivalent 32-bit unsigned integer.</summary>
		/// <returns>The 8-bit unsigned integer equivalent to the value of <paramref name="value" />.</returns>
		/// <param name="value">An 8-bit signed integer. </param>
		/// <exception cref="T:System.OverflowException">
		///   <paramref name="value" /> is less than zero. </exception>
		/// <filterpriority>1</filterpriority>
		[CLSCompliant(false)]
		public static uint ToUInt32(sbyte value)
		{
			if ((long)value < 0L)
			{
				throw new OverflowException(Locale.GetText("Value is less than UInt32.MinValue"));
			}
			return (uint)value;
		}

		/// <summary>Converts the value of the specified 16-bit signed integer to the equivalent 32-bit unsigned integer.</summary>
		/// <returns>The 32-bit unsigned integer equivalent to the value of <paramref name="value" />.</returns>
		/// <param name="value">A 16-bit signed integer. </param>
		/// <exception cref="T:System.OverflowException">
		///   <paramref name="value" /> is less than zero. </exception>
		/// <filterpriority>1</filterpriority>
		[CLSCompliant(false)]
		public static uint ToUInt32(short value)
		{
			if ((long)value < 0L)
			{
				throw new OverflowException(Locale.GetText("Value is less than UInt32.MinValue"));
			}
			return (uint)value;
		}

		/// <summary>Converts the specified <see cref="T:System.String" /> representation of a number to an equivalent 32-bit unsigned integer.</summary>
		/// <returns>A 32-bit unsigned integer equivalent to the value of <paramref name="value" />.-or- Zero if <paramref name="value" /> is null.</returns>
		/// <param name="value">A <see cref="T:System.String" /> containing a number to convert. </param>
		/// <exception cref="T:System.FormatException">
		///   <paramref name="value" /> does not consist of an optional sign followed by a sequence of digits (zero through nine). </exception>
		/// <exception cref="T:System.OverflowException">
		///   <paramref name="value" /> represents a number less than <see cref="F:System.Int32.MinValue" /> or greater than <see cref="F:System.Int32.MaxValue" />. </exception>
		/// <filterpriority>1</filterpriority>
		[CLSCompliant(false)]
		public static uint ToUInt32(string value)
		{
			if (value == null)
			{
				return 0u;
			}
			return uint.Parse(value);
		}

		/// <summary>Converts the specified <see cref="T:System.String" /> representation of a number to an equivalent 32-bit unsigned integer using the specified culture-specific formatting information.</summary>
		/// <returns>A 32-bit unsigned integer equivalent to the value of <paramref name="value" />.-or- Zero if <paramref name="value" /> is null.</returns>
		/// <param name="value">A <see cref="T:System.String" /> containing a number to convert. </param>
		/// <param name="provider">An <see cref="T:System.IFormatProvider" /> interface implementation that supplies culture-specific formatting information. </param>
		/// <exception cref="T:System.FormatException">
		///   <paramref name="value" /> does not consist of an optional sign followed by a sequence of digits (zero through nine). </exception>
		/// <exception cref="T:System.OverflowException">
		///   <paramref name="value" /> represents a number less than <see cref="F:System.Int32.MinValue" /> or greater than <see cref="F:System.Int32.MaxValue" />. </exception>
		/// <filterpriority>1</filterpriority>
		[CLSCompliant(false)]
		public static uint ToUInt32(string value, IFormatProvider provider)
		{
			if (value == null)
			{
				return 0u;
			}
			return uint.Parse(value, provider);
		}

		/// <summary>Converts the <see cref="T:System.String" /> representation of a number in a specified base to an equivalent 32-bit unsigned integer.</summary>
		/// <returns>A 32-bit unsigned integer equivalent to the number in <paramref name="value" />.-or- Zero if <paramref name="value" /> is null.</returns>
		/// <param name="value">A <see cref="T:System.String" /> containing a number. </param>
		/// <param name="fromBase">The base of the number in <paramref name="value" />, which must be 2, 8, 10, or 16. </param>
		/// <exception cref="T:System.ArgumentException">
		///   <paramref name="fromBase" /> is not 2, 8, 10, or 16. -or-<paramref name="value" />, which represents a non-base 10 unsigned number, is prefixed with a negative sign. </exception>
		/// <exception cref="T:System.FormatException">
		///   <paramref name="value" /> contains a character that is not a valid digit in the base specified by <paramref name="fromBase" />. The exception message indicates that there are no digits to convert if the first character in <paramref name="value" /> is invalid; otherwise, the message indicates that <paramref name="value" /> contains invalid trailing characters.</exception>
		/// <exception cref="T:System.OverflowException">
		///   <paramref name="value" />, which represents a base 10 unsigned number, is prefixed with a negative sign.-or-The return value is less than <see cref="F:System.UInt32.MinValue" /> or larger than <see cref="F:System.UInt32.MaxValue" />.</exception>
		/// <filterpriority>1</filterpriority>
		[CLSCompliant(false)]
		public static uint ToUInt32(string value, int fromBase)
		{
			return (uint)ConvertFromBase(value, fromBase, unsigned: true);
		}

		/// <summary>Returns the specified 32-bit unsigned integer; no actual conversion is performed.</summary>
		/// <returns>Parameter <paramref name="value" /> is returned unchanged.</returns>
		/// <param name="value">A 32-bit unsigned integer. </param>
		/// <filterpriority>1</filterpriority>
		[CLSCompliant(false)]
		public static uint ToUInt32(uint value)
		{
			return value;
		}

		/// <summary>Converts the value of the specified 64-bit unsigned integer to an equivalent 32-bit unsigned integer.</summary>
		/// <returns>A 32-bit unsigned integer equivalent to the value of <paramref name="value" />.</returns>
		/// <param name="value">A 64-bit unsigned integer. </param>
		/// <exception cref="T:System.OverflowException">
		///   <paramref name="value" /> is greater than <see cref="F:System.UInt32.MaxValue" />. </exception>
		/// <filterpriority>1</filterpriority>
		[CLSCompliant(false)]
		public static uint ToUInt32(ulong value)
		{
			if (value > uint.MaxValue)
			{
				throw new OverflowException(Locale.GetText("Value is greater than UInt32.MaxValue"));
			}
			return (uint)value;
		}

		/// <summary>Converts the value of the specified 16-bit unsigned integer to the equivalent 32-bit unsigned integer.</summary>
		/// <returns>The 32-bit unsigned integer equivalent to the value of <paramref name="value" />.</returns>
		/// <param name="value">A 16-bit unsigned integer. </param>
		/// <filterpriority>1</filterpriority>
		[CLSCompliant(false)]
		public static uint ToUInt32(ushort value)
		{
			return value;
		}

		/// <summary>Converts the value of the specified <see cref="T:System.Object" /> to a 32-bit unsigned integer.</summary>
		/// <returns>A 32-bit unsigned integer equivalent to the value of <paramref name="value" />, or zero if <paramref name="value" /> is null.</returns>
		/// <param name="value">An <see cref="T:System.Object" /> that implements the <see cref="T:System.IConvertible" /> interface or null. </param>
		/// <exception cref="T:System.InvalidCastException">
		///   <paramref name="value" /> does not implement <see cref="T:System.IConvertible" />. </exception>
		/// <filterpriority>1</filterpriority>
		[CLSCompliant(false)]
		public static uint ToUInt32(object value)
		{
			if (value == null)
			{
				return 0u;
			}
			return ToUInt32(value, null);
		}

		/// <summary>Converts the value of the specified <see cref="T:System.Object" /> to a 32-bit unsigned integer using the specified culture-specific formatting information.</summary>
		/// <returns>A 32-bit unsigned integer equivalent to the value of <paramref name="value" />, or zero if <paramref name="value" /> is null.</returns>
		/// <param name="value">An <see cref="T:System.Object" /> that implements the <see cref="T:System.IConvertible" /> interface. </param>
		/// <param name="provider">An <see cref="T:System.IFormatProvider" /> interface implementation that supplies culture-specific formatting information. </param>
		/// <filterpriority>1</filterpriority>
		[CLSCompliant(false)]
		public static uint ToUInt32(object value, IFormatProvider provider)
		{
			if (value == null)
			{
				return 0u;
			}
			return ((IConvertible)value).ToUInt32(provider);
		}

		/// <summary>Converts the value of the specified Boolean value to the equivalent 64-bit unsigned integer.</summary>
		/// <returns>The number 1 if <paramref name="value" /> is true; otherwise, 0.</returns>
		/// <param name="value">A Boolean value. </param>
		/// <filterpriority>1</filterpriority>
		[CLSCompliant(false)]
		public static ulong ToUInt64(bool value)
		{
			return (ulong)(value ? 1 : 0);
		}

		/// <summary>Converts the value of the specified 8-bit unsigned integer to the equivalent 64-bit signed integer.</summary>
		/// <returns>The 64-bit signed integer equivalent to the value of <paramref name="value" />.</returns>
		/// <param name="value">An 8-bit unsigned integer. </param>
		/// <filterpriority>1</filterpriority>
		[CLSCompliant(false)]
		public static ulong ToUInt64(byte value)
		{
			return value;
		}

		/// <summary>Converts the value of the specified Unicode character to the equivalent 64-bit unsigned integer.</summary>
		/// <returns>The 64-bit unsigned integer equivalent to <paramref name="value" />.</returns>
		/// <param name="value">A Unicode character. </param>
		/// <filterpriority>1</filterpriority>
		[CLSCompliant(false)]
		public static ulong ToUInt64(char value)
		{
			return value;
		}

		/// <summary>Calling this method always throws <see cref="T:System.InvalidCastException" />.</summary>
		/// <returns>This conversion is not supported. No value is returned.</returns>
		/// <param name="value">A <see cref="T:System.DateTime" />. </param>
		/// <exception cref="T:System.InvalidCastException">This conversion is not supported. </exception>
		/// <filterpriority>1</filterpriority>
		[CLSCompliant(false)]
		public static ulong ToUInt64(DateTime value)
		{
			throw new InvalidCastException("The conversion is not supported.");
		}

		/// <summary>Converts the value of the specified <see cref="T:System.Decimal" /> number to an equivalent 64-bit unsigned integer.</summary>
		/// <returns>
		///   <paramref name="value" /> rounded to the nearest 64-bit unsigned integer. If <paramref name="value" /> is halfway between two whole numbers, the even number is returned; that is, 4.5 is converted to 4, and 5.5 is converted to 6.</returns>
		/// <param name="value">A <see cref="T:System.Decimal" /> number. </param>
		/// <exception cref="T:System.OverflowException">
		///   <paramref name="value" /> is less than zero or greater than <see cref="F:System.UInt64.MaxValue" />. </exception>
		/// <filterpriority>1</filterpriority>
		[CLSCompliant(false)]
		public static ulong ToUInt64(decimal value)
		{
			if (value > 18446744073709551615m || value < 0m)
			{
				throw new OverflowException(Locale.GetText("Value is greater than UInt64.MaxValue or less than UInt64.MinValue"));
			}
			return (ulong)Math.Round(value);
		}

		/// <summary>Converts the value of the specified double-precision floating point number to an equivalent 64-bit unsigned integer.</summary>
		/// <returns>
		///   <paramref name="value" /> rounded to the nearest 64-bit unsigned integer. If <paramref name="value" /> is halfway between two whole numbers, the even number is returned; that is, 4.5 is converted to 4, and 5.5 is converted to 6.</returns>
		/// <param name="value">A double-precision floating point number. </param>
		/// <exception cref="T:System.OverflowException">
		///   <paramref name="value" /> is less than zero or greater than <see cref="F:System.UInt64.MaxValue" />. </exception>
		/// <filterpriority>1</filterpriority>
		[CLSCompliant(false)]
		public static ulong ToUInt64(double value)
		{
			if (value > 1.8446744073709552E+19 || value < 0.0)
			{
				throw new OverflowException(Locale.GetText("Value is greater than UInt64.MaxValue or less than UInt64.MinValue"));
			}
			return (ulong)Math.Round(value);
		}

		/// <summary>Converts the value of the specified single-precision floating point number to an equivalent 64-bit unsigned integer.</summary>
		/// <returns>
		///   <paramref name="value" /> rounded to the nearest 64-bit unsigned integer. If <paramref name="value" /> is halfway between two whole numbers, the even number is returned; that is, 4.5 is converted to 4, and 5.5 is converted to 6.</returns>
		/// <param name="value">A single-precision floating point number. </param>
		/// <exception cref="T:System.OverflowException">
		///   <paramref name="value" /> is less than zero or greater than <see cref="F:System.UInt64.MaxValue" />. </exception>
		/// <filterpriority>1</filterpriority>
		[CLSCompliant(false)]
		public static ulong ToUInt64(float value)
		{
			if (value > 1.84467441E+19f || value < 0f)
			{
				throw new OverflowException(Locale.GetText("Value is greater than UInt64.MaxValue or less than UInt64.MinValue"));
			}
			return (ulong)Math.Round(value);
		}

		/// <summary>Converts the value of the specified 32-bit signed integer to an equivalent 64-bit unsigned integer.</summary>
		/// <returns>The 64-bit unsigned integer equivalent of <paramref name="value" />.</returns>
		/// <param name="value">A 32-bit signed integer. </param>
		/// <exception cref="T:System.OverflowException">
		///   <paramref name="value" /> is less than zero. </exception>
		/// <filterpriority>1</filterpriority>
		[CLSCompliant(false)]
		public static ulong ToUInt64(int value)
		{
			if (value < 0)
			{
				throw new OverflowException(Locale.GetText("Value is less than UInt64.MinValue"));
			}
			return (ulong)value;
		}

		/// <summary>Converts the value of the specified 64-bit signed integer to an equivalent 64-bit unsigned integer.</summary>
		/// <returns>A 64-bit unsigned integer equivalent to the value of <paramref name="value" />.</returns>
		/// <param name="value">A 64-bit signed integer. </param>
		/// <exception cref="T:System.OverflowException">
		///   <paramref name="value" /> is less than zero. </exception>
		/// <filterpriority>1</filterpriority>
		[CLSCompliant(false)]
		public static ulong ToUInt64(long value)
		{
			if (value < 0)
			{
				throw new OverflowException(Locale.GetText("Value is less than UInt64.MinValue"));
			}
			return (ulong)value;
		}

		/// <summary>Converts the value of the specified 8-bit signed integer to the equivalent 64-bit unsigned integer.</summary>
		/// <returns>The 64-bit unsigned integer equivalent to the value of <paramref name="value" />.</returns>
		/// <param name="value">An 8-bit signed integer. </param>
		/// <exception cref="T:System.OverflowException">
		///   <paramref name="value" /> is less than zero. </exception>
		/// <filterpriority>1</filterpriority>
		[CLSCompliant(false)]
		public static ulong ToUInt64(sbyte value)
		{
			if (value < 0)
			{
				throw new OverflowException("Value is less than UInt64.MinValue");
			}
			return (ulong)value;
		}

		/// <summary>Converts the value of the specified 16-bit signed integer to the equivalent 64-bit unsigned integer.</summary>
		/// <returns>The 64-bit unsigned integer equivalent to the value of <paramref name="value" />.</returns>
		/// <param name="value">A 16-bit signed integer. </param>
		/// <exception cref="T:System.OverflowException">
		///   <paramref name="value" /> is less than zero. </exception>
		/// <filterpriority>1</filterpriority>
		[CLSCompliant(false)]
		public static ulong ToUInt64(short value)
		{
			if (value < 0)
			{
				throw new OverflowException(Locale.GetText("Value is less than UInt64.MinValue"));
			}
			return (ulong)value;
		}

		/// <summary>Converts the specified <see cref="T:System.String" /> representation of a number to an equivalent 64-bit signed integer.</summary>
		/// <returns>A 64-bit signed integer equivalent to the value of <paramref name="value" />.-or- Zero if <paramref name="value" /> is null.</returns>
		/// <param name="value">A <see cref="T:System.String" /> containing a number to convert. </param>
		/// <exception cref="T:System.FormatException">
		///   <paramref name="value" /> does not consist of an optional sign followed by a sequence of digits (zero through nine). </exception>
		/// <exception cref="T:System.OverflowException">
		///   <paramref name="value" /> represents a number less than <see cref="F:System.Int64.MinValue" /> or greater than <see cref="F:System.Int64.MaxValue" />. </exception>
		/// <filterpriority>1</filterpriority>
		[CLSCompliant(false)]
		public static ulong ToUInt64(string value)
		{
			if (value == null)
			{
				return 0uL;
			}
			return ulong.Parse(value);
		}

		/// <summary>Converts the specified <see cref="T:System.String" /> representation of a number to an equivalent 64-bit unsigned integer using the specified culture-specific formatting information.</summary>
		/// <returns>A 64-bit unsigned integer equivalent to the value of <paramref name="value" />.-or- Zero if <paramref name="value" /> is null.</returns>
		/// <param name="value">A <see cref="T:System.String" /> containing a number to convert. </param>
		/// <param name="provider">An <see cref="T:System.IFormatProvider" /> interface implementation that supplies culture-specific formatting information. </param>
		/// <exception cref="T:System.FormatException">
		///   <paramref name="value" /> does not consist of an optional sign followed by a sequence of digits (zero through nine). </exception>
		/// <exception cref="T:System.OverflowException">
		///   <paramref name="value" /> represents a number less than <see cref="F:System.Int64.MinValue" /> or greater than <see cref="F:System.Int64.MaxValue" />. </exception>
		/// <filterpriority>1</filterpriority>
		[CLSCompliant(false)]
		public static ulong ToUInt64(string value, IFormatProvider provider)
		{
			if (value == null)
			{
				return 0uL;
			}
			return ulong.Parse(value, provider);
		}

		/// <summary>Converts the <see cref="T:System.String" /> representation of a number in a specified base to an equivalent 64-bit unsigned integer.</summary>
		/// <returns>A 64-bit unsigned integer equivalent to the number in <paramref name="value" />.-or- Zero if <paramref name="value" /> is null.</returns>
		/// <param name="value">A <see cref="T:System.String" /> containing a number. </param>
		/// <param name="fromBase">The base of the number in <paramref name="value" />, which must be 2, 8, 10, or 16. </param>
		/// <exception cref="T:System.ArgumentException">
		///   <paramref name="fromBase" /> is not 2, 8, 10, or 16. -or-<paramref name="value" />, which represents a non-base 10 unsigned number, is prefixed with a negative sign. </exception>
		/// <exception cref="T:System.FormatException">
		///   <paramref name="value" /> contains a character that is not a valid digit in the base specified by <paramref name="fromBase" />. The exception message indicates that there are no digits to convert if the first character in <paramref name="value" /> is invalid; otherwise, the message indicates that <paramref name="value" /> contains invalid trailing characters.</exception>
		/// <exception cref="T:System.OverflowException">
		///   <paramref name="value" />, which represents a base 10 unsigned number, is prefixed with a negative sign.-or-The return value is less than <see cref="F:System.UInt64.MinValue" /> or larger than <see cref="F:System.UInt64.MaxValue" />.</exception>
		/// <filterpriority>1</filterpriority>
		[CLSCompliant(false)]
		public static ulong ToUInt64(string value, int fromBase)
		{
			return (ulong)ConvertFromBase64(value, fromBase, unsigned: true);
		}

		/// <summary>Converts the value of the specified 32-bit unsigned integer to an equivalent 64-bit unsigned integer.</summary>
		/// <returns>The 64-bit unsigned integer equivalent of <paramref name="value" />.</returns>
		/// <param name="value">A 32-bit unsigned integer. </param>
		/// <filterpriority>1</filterpriority>
		[CLSCompliant(false)]
		public static ulong ToUInt64(uint value)
		{
			return value;
		}

		/// <summary>Returns the specified 64-bit unsigned integer; no actual conversion is performed.</summary>
		/// <returns>Parameter <paramref name="value" /> is returned unchanged.</returns>
		/// <param name="value">A 64-bit unsigned integer. </param>
		/// <filterpriority>1</filterpriority>
		[CLSCompliant(false)]
		public static ulong ToUInt64(ulong value)
		{
			return value;
		}

		/// <summary>Converts the value of the specified 16-bit unsigned integer to the equivalent 64-bit unsigned integer.</summary>
		/// <returns>The 64-bit unsigned integer equivalent to the value of <paramref name="value" />.</returns>
		/// <param name="value">A 16-bit unsigned integer. </param>
		/// <filterpriority>1</filterpriority>
		[CLSCompliant(false)]
		public static ulong ToUInt64(ushort value)
		{
			return value;
		}

		/// <summary>Converts the value of the specified <see cref="T:System.Object" /> to a 64-bit unsigned integer.</summary>
		/// <returns>A 64-bit unsigned integer equivalent to the value of <paramref name="value" />, or zero if <paramref name="value" /> is null.</returns>
		/// <param name="value">An <see cref="T:System.Object" /> that implements the <see cref="T:System.IConvertible" /> interface or null. </param>
		/// <exception cref="T:System.InvalidCastException">
		///   <paramref name="value" /> does not implement <see cref="T:System.IConvertible" />. </exception>
		/// <filterpriority>1</filterpriority>
		[CLSCompliant(false)]
		public static ulong ToUInt64(object value)
		{
			if (value == null)
			{
				return 0uL;
			}
			return ToUInt64(value, null);
		}

		/// <summary>Converts the value of the specified <see cref="T:System.Object" /> to a 64-bit unsigned integer using the specified culture-specific formatting information.</summary>
		/// <returns>A 64-bit unsigned integer equivalent to the value of <paramref name="value" />, or zero if <paramref name="value" /> is null.</returns>
		/// <param name="value">An <see cref="T:System.Object" /> that implements the <see cref="T:System.IConvertible" /> interface. </param>
		/// <param name="provider">An <see cref="T:System.IFormatProvider" /> interface implementation that supplies culture-specific formatting information. </param>
		/// <filterpriority>1</filterpriority>
		[CLSCompliant(false)]
		public static ulong ToUInt64(object value, IFormatProvider provider)
		{
			if (value == null)
			{
				return 0uL;
			}
			return ((IConvertible)value).ToUInt64(provider);
		}

		/// <summary>Returns an <see cref="T:System.Object" /> with the specified <see cref="T:System.Type" /> and whose value is equivalent to the specified object.</summary>
		/// <returns>An object whose <see cref="T:System.Type" /> is <paramref name="conversionType" /> and whose value is equivalent to <paramref name="value" />.-or-null, if <paramref name="value" /> is null and <paramref name="conversionType" /> is not a value type.</returns>
		/// <param name="value">An <see cref="T:System.Object" /> that implements the <see cref="T:System.IConvertible" /> interface. </param>
		/// <param name="conversionType">A <see cref="T:System.Type" />. </param>
		/// <exception cref="T:System.InvalidCastException">This conversion is not supported.  -or-<paramref name="value" /> is null and <paramref name="conversionType" /> is a value type.</exception>
		/// <exception cref="T:System.ArgumentNullException">
		///   <paramref name="conversionType" /> is null.</exception>
		/// <filterpriority>1</filterpriority>
		public static object ChangeType(object value, Type conversionType)
		{
			if (value != null && conversionType == null)
			{
				throw new ArgumentNullException("conversionType");
			}
			CultureInfo currentCulture = CultureInfo.CurrentCulture;
			IFormatProvider provider = (IFormatProvider)((conversionType != typeof(DateTime)) ? ((object)currentCulture.NumberFormat) : ((object)currentCulture.DateTimeFormat));
			return ToType(value, conversionType, provider, try_target_to_type: true);
		}

		/// <summary>Returns an <see cref="T:System.Object" /> with the specified <see cref="T:System.TypeCode" /> and whose value is equivalent to the specified object.</summary>
		/// <returns>An object whose underlying <see cref="T:System.TypeCode" /> is <paramref name="typeCode" /> and whose value is equivalent to <paramref name="value" />.-or-null, if <paramref name="value" /> equals null and <paramref name="typeCode" /> equals <see cref="F:System.TypeCode.Empty" />, <see cref="F:System.TypeCode.String" />, or <see cref="F:System.TypeCode.Object" />.</returns>
		/// <param name="value">An <see cref="T:System.Object" /> that implements the <see cref="T:System.IConvertible" /> interface. </param>
		/// <param name="typeCode">A <see cref="T:System.TypeCode" /></param>
		/// <exception cref="T:System.InvalidCastException">This conversion is not supported.  -or-<paramref name="value" /> is null and <paramref name="typeCode" /> specifies a value type.</exception>
		/// <exception cref="T:System.ArgumentException">
		///   <paramref name="typeCode" /> is invalid. </exception>
		/// <filterpriority>1</filterpriority>
		public static object ChangeType(object value, TypeCode typeCode)
		{
			CultureInfo currentCulture = CultureInfo.CurrentCulture;
			Type type = conversionTable[(int)typeCode];
			IFormatProvider provider = (IFormatProvider)((type != typeof(DateTime)) ? ((object)currentCulture.NumberFormat) : ((object)currentCulture.DateTimeFormat));
			return ToType(value, type, provider, try_target_to_type: true);
		}

		/// <summary>Returns an <see cref="T:System.Object" /> with the specified <see cref="T:System.Type" /> and whose value is equivalent to the specified object. A parameter supplies culture-specific formatting information.</summary>
		/// <returns>An object whose <see cref="T:System.Type" /> is <paramref name="conversionType" /> and whose value is equivalent to <paramref name="value" />.-or- <paramref name="value" />, if the <see cref="T:System.Type" /> of <paramref name="value" /> and <paramref name="conversionType" /> are equal.-or- null, if <paramref name="value" /> is null and <paramref name="conversionType" /> is not a value type.</returns>
		/// <param name="value">An <see cref="T:System.Object" /> that implements the <see cref="T:System.IConvertible" /> interface. </param>
		/// <param name="conversionType">A <see cref="T:System.Type" />. </param>
		/// <param name="provider">An <see cref="T:System.IFormatProvider" /> interface implementation that supplies culture-specific formatting information. </param>
		/// <exception cref="T:System.InvalidCastException">This conversion is not supported. -or-<paramref name="value" /> is null and <paramref name="conversionType" /> is a value type.</exception>
		/// <exception cref="T:System.ArgumentNullException">
		///   <paramref name="conversionType" /> is null.</exception>
		/// <filterpriority>1</filterpriority>
		public static object ChangeType(object value, Type conversionType, IFormatProvider provider)
		{
			if (value != null && conversionType == null)
			{
				throw new ArgumentNullException("conversionType");
			}
			return ToType(value, conversionType, provider, try_target_to_type: true);
		}

		/// <summary>Returns an <see cref="T:System.Object" /> with the specified <see cref="T:System.TypeCode" /> and whose value is equivalent to the specified object. A parameter supplies culture-specific formatting information.</summary>
		/// <returns>An object whose underlying <see cref="T:System.TypeCode" /> is <paramref name="typeCode" /> and whose value is equivalent to <paramref name="value" />.-or- null, if <paramref name="value" /> equals null and <paramref name="typeCode" /> equals <see cref="F:System.TypeCode.Empty" />, <see cref="F:System.TypeCode.String" />, or <see cref="F:System.TypeCode.Object" />.</returns>
		/// <param name="value">An <see cref="T:System.Object" /> that implements the <see cref="T:System.IConvertible" /> interface. </param>
		/// <param name="typeCode">A <see cref="T:System.TypeCode" />. </param>
		/// <param name="provider">An <see cref="T:System.IFormatProvider" /> interface implementation that supplies culture-specific formatting information. </param>
		/// <exception cref="T:System.InvalidCastException">This conversion is not supported.  -or-<paramref name="value" /> is null and <paramref name="typeCode" /> specifies a value type.</exception>
		/// <exception cref="T:System.ArgumentException">
		///   <paramref name="typeCode" /> is invalid. </exception>
		/// <filterpriority>1</filterpriority>
		public static object ChangeType(object value, TypeCode typeCode, IFormatProvider provider)
		{
			Type conversionType = conversionTable[(int)typeCode];
			return ToType(value, conversionType, provider, try_target_to_type: true);
		}

		private static bool NotValidBase(int value)
		{
			if (value == 2 || value == 8 || value == 10 || value == 16)
			{
				return false;
			}
			return true;
		}

		private static int ConvertFromBase(string value, int fromBase, bool unsigned)
		{
			if (NotValidBase(fromBase))
			{
				throw new ArgumentException("fromBase is not valid.");
			}
			if (value == null)
			{
				return 0;
			}
			int num = 0;
			int num2 = 0;
			int num3 = 0;
			int length = value.Length;
			bool flag = false;
			switch (fromBase)
			{
			case 10:
				if (value.Substring(num3, 1) == "-")
				{
					if (unsigned)
					{
						throw new OverflowException(Locale.GetText("The string was being parsed as an unsigned number and could not have a negative sign."));
					}
					flag = true;
					num3++;
				}
				break;
			case 16:
				if (value.Substring(num3, 1) == "-")
				{
					throw new ArgumentException("String cannot contain a minus sign if the base is not 10.");
				}
				if (length >= num3 + 2 && value[num3] == '0' && (value[num3 + 1] == 'x' || value[num3 + 1] == 'X'))
				{
					num3 += 2;
				}
				break;
			default:
				if (value.Substring(num3, 1) == "-")
				{
					throw new ArgumentException("String cannot contain a minus sign if the base is not 10.");
				}
				break;
			}
			if (length == num3)
			{
				throw new FormatException("Could not find any parsable digits.");
			}
			if (value[num3] == '+')
			{
				num3++;
			}
			while (num3 < length)
			{
				char c = value[num3++];
				int num4;
				if (char.IsNumber(c))
				{
					num4 = c - 48;
				}
				else
				{
					if (!char.IsLetter(c))
					{
						if (num > 0)
						{
							throw new FormatException("Additional unparsable characters are at the end of the string.");
						}
						throw new FormatException("Could not find any parsable digits.");
					}
					num4 = char.ToLowerInvariant(c) - 97 + 10;
				}
				if (num4 >= fromBase)
				{
					if (num > 0)
					{
						throw new FormatException("Additional unparsable characters are at the end of the string.");
					}
					throw new FormatException("Could not find any parsable digits.");
				}
				num2 = fromBase * num2 + num4;
				num++;
			}
			if (num == 0)
			{
				throw new FormatException("Could not find any parsable digits.");
			}
			if (flag)
			{
				return -num2;
			}
			return num2;
		}

		private static long ConvertFromBase64(string value, int fromBase, bool unsigned)
		{
			if (NotValidBase(fromBase))
			{
				throw new ArgumentException("fromBase is not valid.");
			}
			if (value == null)
			{
				return 0L;
			}
			int num = 0;
			int num2 = -1;
			long num3 = 0L;
			bool flag = false;
			int num4 = 0;
			int length = value.Length;
			switch (fromBase)
			{
			case 10:
				if (value.Substring(num4, 1) == "-")
				{
					if (unsigned)
					{
						throw new OverflowException(Locale.GetText("The string was being parsed as an unsigned number and could not have a negative sign."));
					}
					flag = true;
					num4++;
				}
				break;
			case 16:
				if (value.Substring(num4, 1) == "-")
				{
					throw new ArgumentException("String cannot contain a minus sign if the base is not 10.");
				}
				if (length >= num4 + 2 && value[num4] == '0' && (value[num4 + 1] == 'x' || value[num4 + 1] == 'X'))
				{
					num4 += 2;
				}
				break;
			default:
				if (value.Substring(num4, 1) == "-")
				{
					throw new ArgumentException("String cannot contain a minus sign if the base is not 10.");
				}
				break;
			}
			if (length == num4)
			{
				throw new FormatException("Could not find any parsable digits.");
			}
			if (value[num4] == '+')
			{
				num4++;
			}
			while (num4 < length)
			{
				char c = value[num4++];
				if (char.IsNumber(c))
				{
					num2 = c - 48;
				}
				else
				{
					if (!char.IsLetter(c))
					{
						if (num > 0)
						{
							throw new FormatException("Additional unparsable characters are at the end of the string.");
						}
						throw new FormatException("Could not find any parsable digits.");
					}
					num2 = char.ToLowerInvariant(c) - 97 + 10;
				}
				if (num2 >= fromBase)
				{
					if (num > 0)
					{
						throw new FormatException("Additional unparsable characters are at the end of the string.");
					}
					throw new FormatException("Could not find any parsable digits.");
				}
				num3 = fromBase * num3 + num2;
				num++;
			}
			if (num == 0)
			{
				throw new FormatException("Could not find any parsable digits.");
			}
			if (flag)
			{
				return -1 * num3;
			}
			return num3;
		}

		private static void EndianSwap(ref byte[] value)
		{
			byte[] array = new byte[value.Length];
			for (int i = 0; i < value.Length; i++)
			{
				array[i] = value[value.Length - 1 - i];
			}
			value = array;
		}

		private static string ConvertToBase2(byte[] value)
		{
			if (!BitConverter.IsLittleEndian)
			{
				EndianSwap(ref value);
			}
			StringBuilder stringBuilder = new StringBuilder();
			for (int num = value.Length - 1; num >= 0; num--)
			{
				byte b = value[num];
				for (int i = 0; i < 8; i++)
				{
					if ((b & 0x80) == 128)
					{
						stringBuilder.Append('1');
					}
					else if (stringBuilder.Length > 0)
					{
						stringBuilder.Append('0');
					}
					b = (byte)(b << 1);
				}
			}
			return stringBuilder.ToString();
		}

		private static string ConvertToBase8(byte[] value)
		{
			ulong num = 0uL;
			switch (value.Length)
			{
			case 1:
				num = value[0];
				break;
			case 2:
				num = BitConverter.ToUInt16(value, 0);
				break;
			case 4:
				num = BitConverter.ToUInt32(value, 0);
				break;
			case 8:
				num = BitConverter.ToUInt64(value, 0);
				break;
			default:
				throw new ArgumentException("value");
			}
			StringBuilder stringBuilder = new StringBuilder();
			for (int num2 = 21; num2 >= 0; num2--)
			{
				char c = (char)((num >> num2 * 3) & 7);
				if (c != 0 || stringBuilder.Length > 0)
				{
					c = (char)(c + 48);
					stringBuilder.Append(c);
				}
			}
			return stringBuilder.ToString();
		}

		private static string ConvertToBase16(byte[] value)
		{
			if (!BitConverter.IsLittleEndian)
			{
				EndianSwap(ref value);
			}
			StringBuilder stringBuilder = new StringBuilder();
			for (int num = value.Length - 1; num >= 0; num--)
			{
				char c = (char)((value[num] >> 4) & 0xF);
				if (c != 0 || stringBuilder.Length > 0)
				{
					if (c < '\n')
					{
						c = (char)(c + 48);
					}
					else
					{
						c = (char)(c - 10);
						c = (char)(c + 97);
					}
					stringBuilder.Append(c);
				}
				char c2 = (char)(value[num] & 0xF);
				if (c2 != 0 || stringBuilder.Length > 0)
				{
					if (c2 < '\n')
					{
						c2 = (char)(c2 + 48);
					}
					else
					{
						c2 = (char)(c2 - 10);
						c2 = (char)(c2 + 97);
					}
					stringBuilder.Append(c2);
				}
			}
			return stringBuilder.ToString();
		}

		internal static object ToType(object value, Type conversionType, IFormatProvider provider, bool try_target_to_type)
		{
			if (value == null)
			{
				if (conversionType != null && conversionType.IsValueType)
				{
					throw new InvalidCastException("Null object can not be converted to a value type.");
				}
				return null;
			}
			if (conversionType == null)
			{
				throw new InvalidCastException("Cannot cast to destination type.");
			}
			if (value.GetType() == conversionType)
			{
				return value;
			}
			if (value is IConvertible)
			{
				IConvertible convertible = (IConvertible)value;
				if (conversionType == conversionTable[0])
				{
					throw new ArgumentNullException();
				}
				if (conversionType == conversionTable[1])
				{
					return value;
				}
				if (conversionType == conversionTable[2])
				{
					throw new InvalidCastException("Cannot cast to DBNull, it's not IConvertible");
				}
				if (conversionType == conversionTable[3])
				{
					return convertible.ToBoolean(provider);
				}
				if (conversionType == conversionTable[4])
				{
					return convertible.ToChar(provider);
				}
				if (conversionType == conversionTable[5])
				{
					return convertible.ToSByte(provider);
				}
				if (conversionType == conversionTable[6])
				{
					return convertible.ToByte(provider);
				}
				if (conversionType == conversionTable[7])
				{
					return convertible.ToInt16(provider);
				}
				if (conversionType == conversionTable[8])
				{
					return convertible.ToUInt16(provider);
				}
				if (conversionType == conversionTable[9])
				{
					return convertible.ToInt32(provider);
				}
				if (conversionType == conversionTable[10])
				{
					return convertible.ToUInt32(provider);
				}
				if (conversionType == conversionTable[11])
				{
					return convertible.ToInt64(provider);
				}
				if (conversionType == conversionTable[12])
				{
					return convertible.ToUInt64(provider);
				}
				if (conversionType == conversionTable[13])
				{
					return convertible.ToSingle(provider);
				}
				if (conversionType == conversionTable[14])
				{
					return convertible.ToDouble(provider);
				}
				if (conversionType == conversionTable[15])
				{
					return convertible.ToDecimal(provider);
				}
				if (conversionType == conversionTable[16])
				{
					return convertible.ToDateTime(provider);
				}
				if (conversionType == conversionTable[18])
				{
					return convertible.ToString(provider);
				}
				if (try_target_to_type)
				{
					return convertible.ToType(conversionType, provider);
				}
			}
			throw new InvalidCastException(Locale.GetText("Value is not a convertible object: " + value.GetType().ToString() + " to " + conversionType.FullName));
		}
	}
}
