using System.Collections.Generic;
using System.Runtime.InteropServices;
using System.Text;

namespace System
{
	/// <summary>Represents a time interval.</summary>
	/// <filterpriority>1</filterpriority>
	[Serializable]
	[ComVisible(true)]
	public struct TimeSpan : IComparable, IComparable<TimeSpan>, IEquatable<TimeSpan>
	{
		private class Parser
		{
			private string _src;

			private int _cur;

			private int _length;

			private bool formatError;

			public bool AtEnd => _cur >= _length;

			public Parser(string src)
			{
				_src = src;
				_length = _src.Length;
			}

			private void ParseWhiteSpace()
			{
				while (!AtEnd && char.IsWhiteSpace(_src, _cur))
				{
					_cur++;
				}
			}

			private bool ParseSign()
			{
				bool result = false;
				if (!AtEnd && _src[_cur] == '-')
				{
					result = true;
					_cur++;
				}
				return result;
			}

			private int ParseInt(bool optional)
			{
				if (optional && AtEnd)
				{
					return 0;
				}
				int num = 0;
				int num2 = 0;
				while (!AtEnd && char.IsDigit(_src, _cur))
				{
					checked
					{
						num = num * 10 + unchecked((int)_src[_cur]) - 48;
					}
					_cur++;
					num2++;
				}
				if (!optional && num2 == 0)
				{
					formatError = true;
				}
				return num;
			}

			private bool ParseOptDot()
			{
				if (AtEnd)
				{
					return false;
				}
				if (_src[_cur] == '.')
				{
					_cur++;
					return true;
				}
				return false;
			}

			private void ParseOptColon()
			{
				if (!AtEnd)
				{
					if (_src[_cur] == ':')
					{
						_cur++;
					}
					else
					{
						formatError = true;
					}
				}
			}

			private long ParseTicks()
			{
				long num = 1000000L;
				long num2 = 0L;
				bool flag = false;
				while (num > 0 && !AtEnd && char.IsDigit(_src, _cur))
				{
					num2 += (_src[_cur] - 48) * num;
					_cur++;
					num /= 10;
					flag = true;
				}
				if (!flag)
				{
					formatError = true;
				}
				return num2;
			}

			public TimeSpan Execute()
			{
				int num = 0;
				ParseWhiteSpace();
				bool flag = ParseSign();
				int num2 = ParseInt(optional: false);
				if (ParseOptDot())
				{
					num = ParseInt(optional: true);
				}
				else if (!AtEnd)
				{
					num = num2;
					num2 = 0;
				}
				ParseOptColon();
				int num3 = ParseInt(optional: true);
				ParseOptColon();
				int num4 = ParseInt(optional: true);
				long num5 = (!ParseOptDot()) ? 0 : ParseTicks();
				ParseWhiteSpace();
				if (!AtEnd)
				{
					formatError = true;
				}
				if (num > 23 || num3 > 59 || num4 > 59)
				{
					throw new OverflowException(Locale.GetText("Invalid time data."));
				}
				if (formatError)
				{
					throw new FormatException(Locale.GetText("Invalid format for TimeSpan.Parse."));
				}
				long num6 = CalculateTicks(num2, num, num3, num4, 0);
				num6 = checked((!flag) ? (num6 + num5) : (-num6 - num5));
				return new TimeSpan(num6);
			}
		}

		/// <summary>Represents the number of ticks in 1 day. This field is constant.</summary>
		/// <filterpriority>1</filterpriority>
		public const long TicksPerDay = 864000000000L;

		/// <summary>Represents the number of ticks in 1 hour. This field is constant.</summary>
		/// <filterpriority>1</filterpriority>
		public const long TicksPerHour = 36000000000L;

		/// <summary>Represents the number of ticks in 1 millisecond. This field is constant.</summary>
		/// <filterpriority>1</filterpriority>
		public const long TicksPerMillisecond = 10000L;

		/// <summary>Represents the number of ticks in 1 minute. This field is constant.</summary>
		/// <filterpriority>1</filterpriority>
		public const long TicksPerMinute = 600000000L;

		/// <summary>Represents the number of ticks in 1 second.</summary>
		/// <filterpriority>1</filterpriority>
		public const long TicksPerSecond = 10000000L;

		/// <summary>Represents the maximum <see cref="T:System.TimeSpan" /> value. This field is read-only.</summary>
		/// <filterpriority>1</filterpriority>
		public static readonly TimeSpan MaxValue;

		/// <summary>Represents the minimum <see cref="T:System.TimeSpan" /> value. This field is read-only.</summary>
		/// <filterpriority>1</filterpriority>
		public static readonly TimeSpan MinValue;

		/// <summary>Represents the zero <see cref="T:System.TimeSpan" /> value. This field is read-only.</summary>
		/// <filterpriority>1</filterpriority>
		public static readonly TimeSpan Zero;

		private long _ticks;

		/// <summary>Gets the number of whole days represented by the current <see cref="T:System.TimeSpan" /> structure.</summary>
		/// <returns>The day component of this instance. The return value can be positive or negative.</returns>
		/// <filterpriority>1</filterpriority>
		public int Days => (int)(_ticks / 864000000000L);

		/// <summary>Gets the number of whole hours represented by the current <see cref="T:System.TimeSpan" /> structure.</summary>
		/// <returns>The hour component of the current <see cref="T:System.TimeSpan" /> structure. The return value ranges from -23 through 23.</returns>
		/// <filterpriority>1</filterpriority>
		public int Hours => (int)(_ticks % 864000000000L / 36000000000L);

		/// <summary>Gets the number of whole milliseconds represented by the current <see cref="T:System.TimeSpan" /> structure.</summary>
		/// <returns>The millisecond component of the current <see cref="T:System.TimeSpan" /> structure. The return value ranges from -999 through 999.</returns>
		/// <filterpriority>1</filterpriority>
		public int Milliseconds => (int)(_ticks % 10000000 / 10000);

		/// <summary>Gets the number of whole minutes represented by the current <see cref="T:System.TimeSpan" /> structure.</summary>
		/// <returns>The minute component of the current <see cref="T:System.TimeSpan" /> structure. The return value ranges from -59 through 59.</returns>
		/// <filterpriority>1</filterpriority>
		public int Minutes => (int)(_ticks % 36000000000L / 600000000);

		/// <summary>Gets the number of whole seconds represented by the current <see cref="T:System.TimeSpan" /> structure.</summary>
		/// <returns>The second component of the current <see cref="T:System.TimeSpan" /> structure. The return value ranges from -59 through 59.</returns>
		/// <filterpriority>1</filterpriority>
		public int Seconds => (int)(_ticks % 600000000 / 10000000);

		/// <summary>Gets the number of ticks that represent the value of the current <see cref="T:System.TimeSpan" /> structure.</summary>
		/// <returns>The number of ticks contained in this instance.</returns>
		/// <filterpriority>1</filterpriority>
		public long Ticks => _ticks;

		/// <summary>Gets the value of the current <see cref="T:System.TimeSpan" /> structure expressed in whole and fractional days.</summary>
		/// <returns>The total number of days represented by this instance.</returns>
		/// <filterpriority>1</filterpriority>
		public double TotalDays => (double)_ticks / 864000000000.0;

		/// <summary>Gets the value of the current <see cref="T:System.TimeSpan" /> structure expressed in whole and fractional hours.</summary>
		/// <returns>The total number of hours represented by this instance.</returns>
		/// <filterpriority>1</filterpriority>
		public double TotalHours => (double)_ticks / 36000000000.0;

		/// <summary>Gets the value of the current <see cref="T:System.TimeSpan" /> structure expressed in whole and fractional milliseconds.</summary>
		/// <returns>The total number of milliseconds represented by this instance.</returns>
		/// <filterpriority>1</filterpriority>
		public double TotalMilliseconds => (double)_ticks / 10000.0;

		/// <summary>Gets the value of the current <see cref="T:System.TimeSpan" /> structure expressed in whole and fractional minutes.</summary>
		/// <returns>The total number of minutes represented by this instance.</returns>
		/// <filterpriority>1</filterpriority>
		public double TotalMinutes => (double)_ticks / 600000000.0;

		/// <summary>Gets the value of the current <see cref="T:System.TimeSpan" /> structure expressed in whole and fractional seconds.</summary>
		/// <returns>The total number of seconds represented by this instance.</returns>
		/// <filterpriority>1</filterpriority>
		public double TotalSeconds => (double)_ticks / 10000000.0;

		/// <summary>Initializes a new <see cref="T:System.TimeSpan" /> to the specified number of ticks.</summary>
		/// <param name="ticks">A time period expressed in 100-nanosecond units. </param>
		public TimeSpan(long ticks)
		{
			_ticks = ticks;
		}

		/// <summary>Initializes a new <see cref="T:System.TimeSpan" /> to a specified number of hours, minutes, and seconds.</summary>
		/// <param name="hours">Number of hours. </param>
		/// <param name="minutes">Number of minutes. </param>
		/// <param name="seconds">Number of seconds. </param>
		/// <exception cref="T:System.ArgumentOutOfRangeException">The parameters specify a <see cref="T:System.TimeSpan" /> value less than <see cref="F:System.TimeSpan.MinValue" /> or greater than <see cref="F:System.TimeSpan.MaxValue" />. </exception>
		public TimeSpan(int hours, int minutes, int seconds)
		{
			_ticks = CalculateTicks(0, hours, minutes, seconds, 0);
		}

		/// <summary>Initializes a new <see cref="T:System.TimeSpan" /> to a specified number of days, hours, minutes, and seconds.</summary>
		/// <param name="days">Number of days. </param>
		/// <param name="hours">Number of hours. </param>
		/// <param name="minutes">Number of minutes. </param>
		/// <param name="seconds">Number of seconds. </param>
		/// <exception cref="T:System.ArgumentOutOfRangeException">The parameters specify a <see cref="T:System.TimeSpan" /> value less than <see cref="F:System.TimeSpan.MinValue" /> or greater than <see cref="F:System.TimeSpan.MaxValue" />. </exception>
		public TimeSpan(int days, int hours, int minutes, int seconds)
		{
			_ticks = CalculateTicks(days, hours, minutes, seconds, 0);
		}

		/// <summary>Initializes a new <see cref="T:System.TimeSpan" /> to a specified number of days, hours, minutes, seconds, and milliseconds.</summary>
		/// <param name="days">Number of days. </param>
		/// <param name="hours">Number of hours. </param>
		/// <param name="minutes">Number of minutes. </param>
		/// <param name="seconds">Number of seconds. </param>
		/// <param name="milliseconds">Number of milliseconds. </param>
		/// <exception cref="T:System.ArgumentOutOfRangeException">The parameters specify a <see cref="T:System.TimeSpan" /> value less than <see cref="F:System.TimeSpan.MinValue" /> or greater than <see cref="F:System.TimeSpan.MaxValue" />. </exception>
		public TimeSpan(int days, int hours, int minutes, int seconds, int milliseconds)
		{
			_ticks = CalculateTicks(days, hours, minutes, seconds, milliseconds);
		}

		static TimeSpan()
		{
			MaxValue = new TimeSpan(long.MaxValue);
			MinValue = new TimeSpan(long.MinValue);
			Zero = new TimeSpan(0L);
			if (MonoTouchAOTHelper.FalseFlag)
			{
				GenericComparer<TimeSpan> genericComparer = new GenericComparer<TimeSpan>();
				GenericEqualityComparer<TimeSpan> genericEqualityComparer = new GenericEqualityComparer<TimeSpan>();
			}
		}

		internal static long CalculateTicks(int days, int hours, int minutes, int seconds, int milliseconds)
		{
			int num = hours * 3600;
			int num2 = minutes * 60;
			long num3 = (long)(num + num2 + seconds) * 1000L + milliseconds;
			num3 *= 10000;
			bool flag = false;
			if (days > 0)
			{
				long num4 = 864000000000L * days;
				if (num3 < 0)
				{
					long num5 = num3;
					num3 += num4;
					flag = (num5 > num3);
				}
				else
				{
					num3 += num4;
					flag = (num3 < 0);
				}
			}
			else if (days < 0)
			{
				long num6 = 864000000000L * days;
				if (num3 <= 0)
				{
					num3 += num6;
					flag = (num3 > 0);
				}
				else
				{
					long num7 = num3;
					num3 += num6;
					flag = (num3 > num7);
				}
			}
			if (flag)
			{
				throw new ArgumentOutOfRangeException(Locale.GetText("The timespan is too big or too small."));
			}
			return num3;
		}

		/// <summary>Adds the specified <see cref="T:System.TimeSpan" /> to this instance.</summary>
		/// <returns>A <see cref="T:System.TimeSpan" /> that represents the value of this instance plus the value of <paramref name="ts" />.</returns>
		/// <param name="ts">A <see cref="T:System.TimeSpan" />. </param>
		/// <exception cref="T:System.OverflowException">The resulting <see cref="T:System.TimeSpan" /> is less than <see cref="F:System.TimeSpan.MinValue" /> or greater than <see cref="F:System.TimeSpan.MaxValue" />. </exception>
		/// <filterpriority>1</filterpriority>
		public TimeSpan Add(TimeSpan ts)
		{
			//Discarded unreachable code: IL_0019, IL_002f
			try
			{
				return new TimeSpan(checked(_ticks + ts.Ticks));
			}
			catch (OverflowException)
			{
				throw new OverflowException(Locale.GetText("Resulting timespan is too big."));
			}
		}

		/// <summary>Compares two <see cref="T:System.TimeSpan" /> values and returns an integer that indicates their relationship.</summary>
		/// <returns>Value Condition -1 <paramref name="t1" /> is less than <paramref name="t2" />0 <paramref name="t1" /> is equal to <paramref name="t2" />1 <paramref name="t1" /> is greater than <paramref name="t2" /></returns>
		/// <param name="t1">A <see cref="T:System.TimeSpan" />. </param>
		/// <param name="t2">A <see cref="T:System.TimeSpan" />. </param>
		/// <filterpriority>1</filterpriority>
		public static int Compare(TimeSpan t1, TimeSpan t2)
		{
			if (t1._ticks < t2._ticks)
			{
				return -1;
			}
			if (t1._ticks > t2._ticks)
			{
				return 1;
			}
			return 0;
		}

		/// <summary>Compares this instance to a specified object and returns an indication of their relative values.</summary>
		/// <returns>Value Condition -1 The value of this instance is less than the value of <paramref name="value" />. 0 The value of this instance is equal to the value of <paramref name="value" />. 1 The value of this instance is greater than the value of <paramref name="value" />.-or- <paramref name="value" /> is null. </returns>
		/// <param name="value">An object to compare, or null. </param>
		/// <exception cref="T:System.ArgumentException">
		///   <paramref name="value" /> is not a <see cref="T:System.TimeSpan" />. </exception>
		/// <filterpriority>1</filterpriority>
		public int CompareTo(object value)
		{
			if (value == null)
			{
				return 1;
			}
			if (!(value is TimeSpan))
			{
				throw new ArgumentException(Locale.GetText("Argument has to be a TimeSpan."), "value");
			}
			return Compare(this, (TimeSpan)value);
		}

		/// <summary>Compares this instance to a specified <see cref="T:System.TimeSpan" /> object and returns an indication of their relative values.</summary>
		/// <returns>A signed number indicating the relative values of this instance and <paramref name="value" />.Value Description A negative integer This instance is less than <paramref name="value" />. Zero This instance is equal to <paramref name="value" />. A positive integer This instance is greater than <paramref name="value" />. </returns>
		/// <param name="value">A <see cref="T:System.TimeSpan" /> object to compare to this instance.</param>
		/// <filterpriority>1</filterpriority>
		public int CompareTo(TimeSpan value)
		{
			return Compare(this, value);
		}

		/// <summary>Returns a value indicating whether this instance is equal to a specified <see cref="T:System.TimeSpan" /> object.</summary>
		/// <returns>true if <paramref name="obj" /> represents the same time interval as this instance; otherwise, false.</returns>
		/// <param name="obj">An <see cref="T:System.TimeSpan" /> object to compare with this instance. </param>
		/// <filterpriority>1</filterpriority>
		public bool Equals(TimeSpan obj)
		{
			return obj._ticks == _ticks;
		}

		/// <summary>Returns a new <see cref="T:System.TimeSpan" /> object whose value is the absolute value of the current <see cref="T:System.TimeSpan" /> object.</summary>
		/// <returns>A new <see cref="T:System.TimeSpan" /> whose value is the absolute value of the current <see cref="T:System.TimeSpan" /> object.</returns>
		/// <exception cref="T:System.OverflowException">The value of this instance is <see cref="F:System.TimeSpan.MinValue" />. </exception>
		/// <filterpriority>1</filterpriority>
		public TimeSpan Duration()
		{
			//Discarded unreachable code: IL_0016, IL_002c
			try
			{
				return new TimeSpan(Math.Abs(_ticks));
			}
			catch (OverflowException)
			{
				throw new OverflowException(Locale.GetText("This TimeSpan value is MinValue so you cannot get the duration."));
			}
		}

		/// <summary>Returns a value indicating whether this instance is equal to a specified object.</summary>
		/// <returns>true if <paramref name="value" /> is a <see cref="T:System.TimeSpan" /> object that represents the same time interval as the current <see cref="T:System.TimeSpan" /> structure; otherwise, false.</returns>
		/// <param name="value">An object to compare with this instance. </param>
		/// <filterpriority>1</filterpriority>
		public override bool Equals(object value)
		{
			if (!(value is TimeSpan))
			{
				return false;
			}
			long ticks = _ticks;
			TimeSpan timeSpan = (TimeSpan)value;
			return ticks == timeSpan._ticks;
		}

		/// <summary>Returns a value indicating whether two specified instances of <see cref="T:System.TimeSpan" /> are equal.</summary>
		/// <returns>true if the values of <paramref name="t1" /> and <paramref name="t2" /> are equal; otherwise, false.</returns>
		/// <param name="t1">A <see cref="T:System.TimeSpan" />. </param>
		/// <param name="t2">A TimeSpan. </param>
		/// <filterpriority>1</filterpriority>
		public static bool Equals(TimeSpan t1, TimeSpan t2)
		{
			return t1._ticks == t2._ticks;
		}

		/// <summary>Returns a <see cref="T:System.TimeSpan" /> that represents a specified number of days, where the specification is accurate to the nearest millisecond.</summary>
		/// <returns>A <see cref="T:System.TimeSpan" /> that represents <paramref name="value" />.</returns>
		/// <param name="value">A number of days, accurate to the nearest millisecond. </param>
		/// <exception cref="T:System.OverflowException">
		///   <paramref name="value" /> is less than <see cref="F:System.TimeSpan.MinValue" /> or greater than <see cref="F:System.TimeSpan.MaxValue" />. -or-<paramref name="value" /> is <see cref="F:System.Double.PositiveInfinity" />.-or-<paramref name="value" /> is <see cref="F:System.Double.NegativeInfinity" />.</exception>
		/// <exception cref="T:System.ArgumentException">
		///   <paramref name="value" /> is equal to <see cref="F:System.Double.NaN" />. </exception>
		/// <filterpriority>1</filterpriority>
		public static TimeSpan FromDays(double value)
		{
			return From(value, 864000000000L);
		}

		/// <summary>Returns a <see cref="T:System.TimeSpan" /> that represents a specified number of hours, where the specification is accurate to the nearest millisecond.</summary>
		/// <returns>A <see cref="T:System.TimeSpan" /> that represents <paramref name="value" />.</returns>
		/// <param name="value">A number of hours accurate to the nearest millisecond. </param>
		/// <exception cref="T:System.OverflowException">
		///   <paramref name="value" /> is less than <see cref="F:System.TimeSpan.MinValue" /> or greater than <see cref="F:System.TimeSpan.MaxValue" />. -or-<paramref name="value" /> is <see cref="F:System.Double.PositiveInfinity" />.-or-<paramref name="value" /> is <see cref="F:System.Double.NegativeInfinity" />.</exception>
		/// <exception cref="T:System.ArgumentException">
		///   <paramref name="value" /> is equal to <see cref="F:System.Double.NaN" />. </exception>
		/// <filterpriority>1</filterpriority>
		public static TimeSpan FromHours(double value)
		{
			return From(value, 36000000000L);
		}

		/// <summary>Returns a <see cref="T:System.TimeSpan" /> that represents a specified number of minutes, where the specification is accurate to the nearest millisecond.</summary>
		/// <returns>A <see cref="T:System.TimeSpan" /> that represents <paramref name="value" />.</returns>
		/// <param name="value">A number of minutes, accurate to the nearest millisecond. </param>
		/// <exception cref="T:System.OverflowException">
		///   <paramref name="value" /> is less than <see cref="F:System.TimeSpan.MinValue" /> or greater than <see cref="F:System.TimeSpan.MaxValue" />.-or-<paramref name="value" /> is <see cref="F:System.Double.PositiveInfinity" />.-or-<paramref name="value" /> is <see cref="F:System.Double.NegativeInfinity" />. </exception>
		/// <exception cref="T:System.ArgumentException">
		///   <paramref name="value" /> is equal to <see cref="F:System.Double.NaN" />. </exception>
		/// <filterpriority>1</filterpriority>
		public static TimeSpan FromMinutes(double value)
		{
			return From(value, 600000000L);
		}

		/// <summary>Returns a <see cref="T:System.TimeSpan" /> that represents a specified number of seconds, where the specification is accurate to the nearest millisecond.</summary>
		/// <returns>A <see cref="T:System.TimeSpan" /> that represents <paramref name="value" />.</returns>
		/// <param name="value">A number of seconds, accurate to the nearest millisecond. </param>
		/// <exception cref="T:System.OverflowException">
		///   <paramref name="value" /> is less than <see cref="F:System.TimeSpan.MinValue" /> or greater than <see cref="F:System.TimeSpan.MaxValue" />.-or-<paramref name="value" /> is <see cref="F:System.Double.PositiveInfinity" />.-or-<paramref name="value" /> is <see cref="F:System.Double.NegativeInfinity" />. </exception>
		/// <exception cref="T:System.ArgumentException">
		///   <paramref name="value" /> is equal to <see cref="F:System.Double.NaN" />. </exception>
		/// <filterpriority>1</filterpriority>
		public static TimeSpan FromSeconds(double value)
		{
			return From(value, 10000000L);
		}

		/// <summary>Returns a <see cref="T:System.TimeSpan" /> that represents a specified number of milliseconds.</summary>
		/// <returns>A <see cref="T:System.TimeSpan" /> that represents <paramref name="value" />.</returns>
		/// <param name="value">A number of milliseconds. </param>
		/// <exception cref="T:System.OverflowException">
		///   <paramref name="value" /> is less than <see cref="F:System.TimeSpan.MinValue" /> or greater than <see cref="F:System.TimeSpan.MaxValue" />.-or-<paramref name="value" /> is <see cref="F:System.Double.PositiveInfinity" />.-or-<paramref name="value" /> is <see cref="F:System.Double.NegativeInfinity" />. </exception>
		/// <exception cref="T:System.ArgumentException">
		///   <paramref name="value" /> is equal to <see cref="F:System.Double.NaN" />. </exception>
		/// <filterpriority>1</filterpriority>
		public static TimeSpan FromMilliseconds(double value)
		{
			return From(value, 10000L);
		}

		private static TimeSpan From(double value, long tickMultiplicator)
		{
			//Discarded unreachable code: IL_0096, IL_00ac
			if (double.IsNaN(value))
			{
				throw new ArgumentException(Locale.GetText("Value cannot be NaN."), "value");
			}
			if (double.IsNegativeInfinity(value) || double.IsPositiveInfinity(value) || value < (double)MinValue.Ticks || value > (double)MaxValue.Ticks)
			{
				throw new OverflowException(Locale.GetText("Outside range [MinValue,MaxValue]"));
			}
			try
			{
				value *= (double)(tickMultiplicator / 10000);
				checked
				{
					long num = (long)Math.Round(value);
					return new TimeSpan(num * 10000);
				}
			}
			catch (OverflowException)
			{
				throw new OverflowException(Locale.GetText("Resulting timespan is too big."));
			}
		}

		/// <summary>Returns a <see cref="T:System.TimeSpan" /> that represents a specified time, where the specification is in units of ticks.</summary>
		/// <returns>A <see cref="T:System.TimeSpan" /> with a value of <paramref name="value" />.</returns>
		/// <param name="value">A number of ticks that represent a time. </param>
		/// <filterpriority>1</filterpriority>
		public static TimeSpan FromTicks(long value)
		{
			return new TimeSpan(value);
		}

		/// <summary>Returns a hash code for this instance.</summary>
		/// <returns>A 32-bit signed integer hash code.</returns>
		/// <filterpriority>2</filterpriority>
		public override int GetHashCode()
		{
			return _ticks.GetHashCode();
		}

		/// <summary>Returns a <see cref="T:System.TimeSpan" /> whose value is the negated value of this instance.</summary>
		/// <returns>The same numeric value as this instance, but with the opposite sign.</returns>
		/// <exception cref="T:System.OverflowException">The negated value of this instance cannot be represented by a <see cref="T:System.TimeSpan" />; that is, the value of this instance is <see cref="F:System.TimeSpan.MinValue" />. </exception>
		/// <filterpriority>1</filterpriority>
		public TimeSpan Negate()
		{
			long ticks = _ticks;
			TimeSpan minValue = MinValue;
			if (ticks == minValue._ticks)
			{
				throw new OverflowException(Locale.GetText("This TimeSpan value is MinValue and cannot be negated."));
			}
			return new TimeSpan(-_ticks);
		}

		/// <summary>Constructs a new <see cref="T:System.TimeSpan" /> object from a time interval specified in a string.</summary>
		/// <returns>A <see cref="T:System.TimeSpan" /> that corresponds to <paramref name="s" />.</returns>
		/// <param name="s">A string that specifies a time interval. </param>
		/// <exception cref="T:System.ArgumentNullException">
		///   <paramref name="s" /> is null. </exception>
		/// <exception cref="T:System.FormatException">
		///   <paramref name="s" /> has an invalid format. </exception>
		/// <exception cref="T:System.OverflowException">
		///   <paramref name="s" /> represents a number less than <see cref="F:System.TimeSpan.MinValue" /> or greater than <see cref="F:System.TimeSpan.MaxValue" />.-or- At least one of the days, hours, minutes, or seconds components is outside its valid range. </exception>
		/// <filterpriority>1</filterpriority>
		public static TimeSpan Parse(string s)
		{
			if (s == null)
			{
				throw new ArgumentNullException("s");
			}
			Parser parser = new Parser(s);
			return parser.Execute();
		}

		/// <summary>Constructs a new <see cref="T:System.TimeSpan" /> object from a time interval specified in a string. Parameters specify the time interval and the variable where the new <see cref="T:System.TimeSpan" /> object is returned. </summary>
		/// <returns>true if <paramref name="s" /> was converted successfully; otherwise, false. This operation returns false if the <paramref name="s" /> parameter is null, has an invalid format, represents a time interval less than <see cref="F:System.TimeSpan.MinValue" /> or greater than <see cref="F:System.TimeSpan.MaxValue" />, or has at least one days, hours, minutes, or seconds component outside its valid range.</returns>
		/// <param name="s">A string that specifies a time interval.</param>
		/// <param name="result">When this method returns, contains an object that represents the time interval specified by <paramref name="s" />, or <see cref="F:System.TimeSpan.Zero" /> if the conversion failed. This parameter is passed uninitialized.</param>
		/// <filterpriority>1</filterpriority>
		public static bool TryParse(string s, out TimeSpan result)
		{
			//Discarded unreachable code: IL_0026, IL_003e
			if (s == null)
			{
				result = Zero;
				return false;
			}
			try
			{
				result = Parse(s);
				return true;
			}
			catch
			{
				result = Zero;
				return false;
			}
		}

		/// <summary>Subtracts the specified <see cref="T:System.TimeSpan" /> from this instance.</summary>
		/// <returns>A <see cref="T:System.TimeSpan" /> whose value is the result of the value of this instance minus the value of <paramref name="ts" />.</returns>
		/// <param name="ts">A <see cref="T:System.TimeSpan" />. </param>
		/// <exception cref="T:System.OverflowException">The return value is less than <see cref="F:System.TimeSpan.MinValue" /> or greater than <see cref="F:System.TimeSpan.MaxValue" />. </exception>
		/// <filterpriority>1</filterpriority>
		public TimeSpan Subtract(TimeSpan ts)
		{
			//Discarded unreachable code: IL_0019, IL_002f
			try
			{
				return new TimeSpan(checked(_ticks - ts.Ticks));
			}
			catch (OverflowException)
			{
				throw new OverflowException(Locale.GetText("Resulting timespan is too big."));
			}
		}

		/// <summary>Returns the string representation of the value of this instance.</summary>
		/// <returns>A string that represents the value of this instance. The return value is of the form: [-][d.]hh:mm:ss[.ff] Items in square brackets ([ and ]) are optional, colons and periods (: and.) are literal characters; and the other items are as follows.Item Description "-" optional minus sign indicating a negative time "d" optional days "hh" hours, ranging from 0 to 23 "mm" minutes, ranging from 0 to 59 "ss" seconds, ranging from 0 to 59 "ff" optional fractional seconds, from 1 to 7 decimal digits For more information about comparing the string representation of <see cref="T:System.TimeSpan" /> and Oracle data types, see article Q324577, "System.TimeSpan Does Not Match Oracle 9i INTERVAL DAY TO SECOND Data Type," in the Microsoft Knowledge Base at http://support.microsoft.com.</returns>
		/// <filterpriority>1</filterpriority>
		public override string ToString()
		{
			StringBuilder stringBuilder = new StringBuilder(14);
			if (_ticks < 0)
			{
				stringBuilder.Append('-');
			}
			if (Days != 0)
			{
				stringBuilder.Append(Math.Abs(Days));
				stringBuilder.Append('.');
			}
			stringBuilder.Append(Math.Abs(Hours).ToString("D2"));
			stringBuilder.Append(':');
			stringBuilder.Append(Math.Abs(Minutes).ToString("D2"));
			stringBuilder.Append(':');
			stringBuilder.Append(Math.Abs(Seconds).ToString("D2"));
			int num = (int)Math.Abs(_ticks % 10000000);
			if (num != 0)
			{
				stringBuilder.Append('.');
				stringBuilder.Append(num.ToString("D7"));
			}
			return stringBuilder.ToString();
		}

		/// <summary>Adds two specified <see cref="T:System.TimeSpan" /> instances.</summary>
		/// <returns>A <see cref="T:System.TimeSpan" /> whose value is the sum of the values of <paramref name="t1" /> and <paramref name="t2" />.</returns>
		/// <param name="t1">A <see cref="T:System.TimeSpan" />. </param>
		/// <param name="t2">A TimeSpan. </param>
		/// <exception cref="T:System.OverflowException">The resulting <see cref="T:System.TimeSpan" /> is less than <see cref="F:System.TimeSpan.MinValue" /> or greater than <see cref="F:System.TimeSpan.MaxValue" />. </exception>
		/// <filterpriority>3</filterpriority>
		public static TimeSpan operator +(TimeSpan t1, TimeSpan t2)
		{
			return t1.Add(t2);
		}

		/// <summary>Indicates whether two <see cref="T:System.TimeSpan" /> instances are equal.</summary>
		/// <returns>true if the values of <paramref name="t1" /> and <paramref name="t2" /> are equal; otherwise, false.</returns>
		/// <param name="t1">A <see cref="T:System.TimeSpan" />. </param>
		/// <param name="t2">A TimeSpan. </param>
		/// <filterpriority>3</filterpriority>
		public static bool operator ==(TimeSpan t1, TimeSpan t2)
		{
			return t1._ticks == t2._ticks;
		}

		/// <summary>Indicates whether a specified <see cref="T:System.TimeSpan" /> is greater than another specified <see cref="T:System.TimeSpan" />.</summary>
		/// <returns>true if the value of <paramref name="t1" /> is greater than the value of <paramref name="t2" />; otherwise, false.</returns>
		/// <param name="t1">A <see cref="T:System.TimeSpan" />. </param>
		/// <param name="t2">A TimeSpan. </param>
		/// <filterpriority>3</filterpriority>
		public static bool operator >(TimeSpan t1, TimeSpan t2)
		{
			return t1._ticks > t2._ticks;
		}

		/// <summary>Indicates whether a specified <see cref="T:System.TimeSpan" /> is greater than or equal to another specified <see cref="T:System.TimeSpan" />.</summary>
		/// <returns>true if the value of <paramref name="t1" /> is greater than or equal to the value of <paramref name="t2" />; otherwise, false.</returns>
		/// <param name="t1">A <see cref="T:System.TimeSpan" />. </param>
		/// <param name="t2">A TimeSpan. </param>
		/// <filterpriority>3</filterpriority>
		public static bool operator >=(TimeSpan t1, TimeSpan t2)
		{
			return t1._ticks >= t2._ticks;
		}

		/// <summary>Indicates whether two <see cref="T:System.TimeSpan" /> instances are not equal.</summary>
		/// <returns>true if the values of <paramref name="t1" /> and <paramref name="t2" /> are not equal; otherwise, false.</returns>
		/// <param name="t1">A <see cref="T:System.TimeSpan" />. </param>
		/// <param name="t2">A TimeSpan. </param>
		/// <filterpriority>3</filterpriority>
		public static bool operator !=(TimeSpan t1, TimeSpan t2)
		{
			return t1._ticks != t2._ticks;
		}

		/// <summary>Indicates whether a specified <see cref="T:System.TimeSpan" /> is less than another specified <see cref="T:System.TimeSpan" />.</summary>
		/// <returns>true if the value of <paramref name="t1" /> is less than the value of <paramref name="t2" />; otherwise, false.</returns>
		/// <param name="t1">A <see cref="T:System.TimeSpan" />. </param>
		/// <param name="t2">A TimeSpan. </param>
		/// <filterpriority>3</filterpriority>
		public static bool operator <(TimeSpan t1, TimeSpan t2)
		{
			return t1._ticks < t2._ticks;
		}

		/// <summary>Indicates whether a specified <see cref="T:System.TimeSpan" /> is less than or equal to another specified <see cref="T:System.TimeSpan" />.</summary>
		/// <returns>true if the value of <paramref name="t1" /> is less than or equal to the value of <paramref name="t2" />; otherwise, false.</returns>
		/// <param name="t1">A <see cref="T:System.TimeSpan" />. </param>
		/// <param name="t2">A TimeSpan. </param>
		/// <filterpriority>3</filterpriority>
		public static bool operator <=(TimeSpan t1, TimeSpan t2)
		{
			return t1._ticks <= t2._ticks;
		}

		/// <summary>Subtracts a specified <see cref="T:System.TimeSpan" /> from another specified <see cref="T:System.TimeSpan" />.</summary>
		/// <returns>A TimeSpan whose value is the result of the value of <paramref name="t1" /> minus the value of <paramref name="t2" />.</returns>
		/// <param name="t1">A <see cref="T:System.TimeSpan" />. </param>
		/// <param name="t2">A TimeSpan. </param>
		/// <exception cref="T:System.OverflowException">The return value is less than <see cref="F:System.TimeSpan.MinValue" /> or greater than <see cref="F:System.TimeSpan.MaxValue" />. </exception>
		/// <filterpriority>3</filterpriority>
		public static TimeSpan operator -(TimeSpan t1, TimeSpan t2)
		{
			return t1.Subtract(t2);
		}

		/// <summary>Returns a <see cref="T:System.TimeSpan" /> whose value is the negated value of the specified instance.</summary>
		/// <returns>A <see cref="T:System.TimeSpan" /> with the same numeric value as this instance, but the opposite sign.</returns>
		/// <param name="t">A <see cref="T:System.TimeSpan" />. </param>
		/// <exception cref="T:System.OverflowException">The negated value of this instance cannot be represented by a <see cref="T:System.TimeSpan" />; that is, the value of this instance is <see cref="F:System.TimeSpan.MinValue" />. </exception>
		/// <filterpriority>3</filterpriority>
		public static TimeSpan operator -(TimeSpan t)
		{
			return t.Negate();
		}

		/// <summary>Returns the specified instance of <see cref="T:System.TimeSpan" />.</summary>
		/// <returns>Returns <paramref name="t" />.</returns>
		/// <param name="t">A <see cref="T:System.TimeSpan" />. </param>
		/// <filterpriority>3</filterpriority>
		public static TimeSpan operator +(TimeSpan t)
		{
			return t;
		}
	}
}
