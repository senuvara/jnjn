using System.Runtime.InteropServices;

namespace System
{
	/// <summary>Provides functionality to format the value of an object into a string representation.</summary>
	/// <filterpriority>2</filterpriority>
	[ComVisible(true)]
	public interface IFormattable
	{
		/// <summary>Formats the value of the current instance using the specified format.</summary>
		/// <returns>A <see cref="T:System.String" /> containing the value of the current instance in the specified format.</returns>
		/// <param name="format">The <see cref="T:System.String" /> specifying the format to use.-or- null to use the default format defined for the type of the <see cref="T:System.IFormattable" /> implementation. </param>
		/// <param name="formatProvider">The <see cref="T:System.IFormatProvider" /> to use to format the value.-or- null to obtain the numeric format information from the current locale setting of the operating system. </param>
		/// <filterpriority>2</filterpriority>
		string ToString(string format, IFormatProvider formatProvider);
	}
}
