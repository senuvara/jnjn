using Mono.Security;
using System.Collections.Generic;
using System.Runtime.InteropServices;
using System.Security.Cryptography;
using System.Text;

namespace System
{
	/// <summary>Represents a globally unique identifier (GUID).</summary>
	/// <filterpriority>1</filterpriority>
	[Serializable]
	[ComVisible(true)]
	public struct Guid : IFormattable, IComparable, IComparable<Guid>, IEquatable<Guid>
	{
		internal class GuidParser
		{
			private string _src;

			private int _length;

			private int _cur;

			public GuidParser(string src)
			{
				_src = src;
				Reset();
			}

			private void Reset()
			{
				_cur = 0;
				_length = _src.Length;
			}

			private bool AtEnd()
			{
				return _cur >= _length;
			}

			private void ThrowFormatException()
			{
				throw new FormatException(Locale.GetText("Invalid format for Guid.Guid(string)."));
			}

			private ulong ParseHex(int length, bool strictLength)
			{
				ulong num = 0uL;
				bool flag = false;
				int num2 = 0;
				while (!flag && num2 < length)
				{
					if (AtEnd())
					{
						if (strictLength || num2 == 0)
						{
							ThrowFormatException();
						}
						else
						{
							flag = true;
						}
					}
					else
					{
						char c = char.ToLowerInvariant(_src[_cur]);
						if (char.IsDigit(c))
						{
							num = num * 16 + c - 48;
							_cur++;
						}
						else if (c >= 'a' && c <= 'f')
						{
							num = num * 16 + c - 97 + 10;
							_cur++;
						}
						else if (strictLength || num2 == 0)
						{
							ThrowFormatException();
						}
						else
						{
							flag = true;
						}
					}
					num2++;
				}
				return num;
			}

			private bool ParseOptChar(char c)
			{
				if (!AtEnd() && _src[_cur] == c)
				{
					_cur++;
					return true;
				}
				return false;
			}

			private void ParseChar(char c)
			{
				if (!ParseOptChar(c))
				{
					ThrowFormatException();
				}
			}

			private Guid ParseGuid1()
			{
				bool flag = true;
				char c = '}';
				byte[] array = new byte[8];
				bool flag2 = ParseOptChar('{');
				if (!flag2)
				{
					flag2 = ParseOptChar('(');
					if (flag2)
					{
						c = ')';
					}
				}
				int a = (int)ParseHex(8, strictLength: true);
				if (flag2)
				{
					ParseChar('-');
				}
				else
				{
					flag = ParseOptChar('-');
				}
				short b = (short)ParseHex(4, strictLength: true);
				if (flag)
				{
					ParseChar('-');
				}
				short c2 = (short)ParseHex(4, strictLength: true);
				if (flag)
				{
					ParseChar('-');
				}
				for (int i = 0; i < 8; i++)
				{
					array[i] = (byte)ParseHex(2, strictLength: true);
					if (i == 1 && flag)
					{
						ParseChar('-');
					}
				}
				if (flag2 && !ParseOptChar(c))
				{
					ThrowFormatException();
				}
				return new Guid(a, b, c2, array);
			}

			private void ParseHexPrefix()
			{
				ParseChar('0');
				ParseChar('x');
			}

			private Guid ParseGuid2()
			{
				byte[] array = new byte[8];
				ParseChar('{');
				ParseHexPrefix();
				int a = (int)ParseHex(8, strictLength: false);
				ParseChar(',');
				ParseHexPrefix();
				short b = (short)ParseHex(4, strictLength: false);
				ParseChar(',');
				ParseHexPrefix();
				short c = (short)ParseHex(4, strictLength: false);
				ParseChar(',');
				ParseChar('{');
				for (int i = 0; i < 8; i++)
				{
					ParseHexPrefix();
					array[i] = (byte)ParseHex(2, strictLength: false);
					if (i != 7)
					{
						ParseChar(',');
					}
				}
				ParseChar('}');
				ParseChar('}');
				return new Guid(a, b, c, array);
			}

			public Guid Parse()
			{
				Guid result;
				try
				{
					result = ParseGuid1();
				}
				catch (FormatException)
				{
					Reset();
					result = ParseGuid2();
				}
				if (!AtEnd())
				{
					ThrowFormatException();
				}
				return result;
			}
		}

		private int _a;

		private short _b;

		private short _c;

		private byte _d;

		private byte _e;

		private byte _f;

		private byte _g;

		private byte _h;

		private byte _i;

		private byte _j;

		private byte _k;

		/// <summary>A read-only instance of the <see cref="T:System.Guid" /> class whose value is guaranteed to be all zeroes.</summary>
		/// <filterpriority>1</filterpriority>
		public static readonly Guid Empty;

		private static object _rngAccess;

		private static RandomNumberGenerator _rng;

		private static RandomNumberGenerator _fastRng;

		/// <summary>Initializes a new instance of the <see cref="T:System.Guid" /> class using the specified array of bytes.</summary>
		/// <param name="b">A 16 element byte array containing values with which to initialize the GUID. </param>
		/// <exception cref="T:System.ArgumentNullException">
		///   <paramref name="b" /> is null. </exception>
		/// <exception cref="T:System.ArgumentException">
		///   <paramref name="b" /> is not 16 bytes long. </exception>
		public Guid(byte[] b)
		{
			CheckArray(b, 16);
			_a = BitConverterLE.ToInt32(b, 0);
			_b = BitConverterLE.ToInt16(b, 4);
			_c = BitConverterLE.ToInt16(b, 6);
			_d = b[8];
			_e = b[9];
			_f = b[10];
			_g = b[11];
			_h = b[12];
			_i = b[13];
			_j = b[14];
			_k = b[15];
		}

		/// <summary>Initializes a new instance of the <see cref="T:System.Guid" /> class using the value represented by the specified string.</summary>
		/// <param name="g">A <see cref="T:System.String" /> that contains a GUID in one of the following formats ('d' represents a hexadecimal digit whose case is ignored): 32 contiguous digits: dddddddddddddddddddddddddddddddd -or- Groups of 8, 4, 4, 4, and 12 digits with hyphens between the groups. The entire GUID can optionally be enclosed in matching braces or parentheses: dddddddd-dddd-dddd-dddd-dddddddddddd -or- {dddddddd-dddd-dddd-dddd-dddddddddddd} -or- (dddddddd-dddd-dddd-dddd-dddddddddddd) -or- Groups of 8, 4, and 4 digits, and a subset of eight groups of 2 digits, with each group prefixed by "0x" or "0X", and separated by commas. The entire GUID, as well as the subset, is enclosed in matching braces: {0xdddddddd, 0xdddd, 0xdddd,{0xdd,0xdd,0xdd,0xdd,0xdd,0xdd,0xdd,0xdd}} All braces, commas, and "0x" prefixes are required. All embedded spaces are ignored. All leading zeroes in a group are ignored.The digits shown in a group are the maximum number of meaningful digits that can appear in that group. You can specify from 1 to the number of digits shown for a group. The specified digits are assumed to be the low order digits of the group. </param>
		/// <exception cref="T:System.ArgumentNullException">
		///   <paramref name="g" /> is null. </exception>
		/// <exception cref="T:System.FormatException">The format of <paramref name="g" /> is invalid. </exception>
		/// <exception cref="T:System.OverflowException">The format of <paramref name="g" /> is invalid. </exception>
		public Guid(string g)
		{
			CheckNull(g);
			g = g.Trim();
			GuidParser guidParser = new GuidParser(g);
			Guid guid = this = guidParser.Parse();
		}

		/// <summary>Initializes a new instance of the <see cref="T:System.Guid" /> class using the specified integers and byte array.</summary>
		/// <param name="a">The first 4 bytes of the GUID. </param>
		/// <param name="b">The next 2 bytes of the GUID. </param>
		/// <param name="c">The next 2 bytes of the GUID. </param>
		/// <param name="d">The remaining 8 bytes of the GUID. </param>
		/// <exception cref="T:System.ArgumentNullException">
		///   <paramref name="d" /> is null. </exception>
		/// <exception cref="T:System.ArgumentException">
		///   <paramref name="d" /> is not 8 bytes long. </exception>
		public Guid(int a, short b, short c, byte[] d)
		{
			CheckArray(d, 8);
			_a = a;
			_b = b;
			_c = c;
			_d = d[0];
			_e = d[1];
			_f = d[2];
			_g = d[3];
			_h = d[4];
			_i = d[5];
			_j = d[6];
			_k = d[7];
		}

		/// <summary>Initializes a new instance of the <see cref="T:System.Guid" /> class using the specified integers and bytes.</summary>
		/// <param name="a">The first 4 bytes of the GUID. </param>
		/// <param name="b">The next 2 bytes of the GUID. </param>
		/// <param name="c">The next 2 bytes of the GUID. </param>
		/// <param name="d">The next byte of the GUID. </param>
		/// <param name="e">The next byte of the GUID. </param>
		/// <param name="f">The next byte of the GUID. </param>
		/// <param name="g">The next byte of the GUID. </param>
		/// <param name="h">The next byte of the GUID. </param>
		/// <param name="i">The next byte of the GUID. </param>
		/// <param name="j">The next byte of the GUID. </param>
		/// <param name="k">The next byte of the GUID. </param>
		public Guid(int a, short b, short c, byte d, byte e, byte f, byte g, byte h, byte i, byte j, byte k)
		{
			_a = a;
			_b = b;
			_c = c;
			_d = d;
			_e = e;
			_f = f;
			_g = g;
			_h = h;
			_i = i;
			_j = j;
			_k = k;
		}

		/// <summary>Initializes a new instance of the <see cref="T:System.Guid" /> class using the specified unsigned integers and bytes.</summary>
		/// <param name="a">The first 4 bytes of the GUID. </param>
		/// <param name="b">The next 2 bytes of the GUID. </param>
		/// <param name="c">The next 2 bytes of the GUID. </param>
		/// <param name="d">The next byte of the GUID. </param>
		/// <param name="e">The next byte of the GUID. </param>
		/// <param name="f">The next byte of the GUID. </param>
		/// <param name="g">The next byte of the GUID. </param>
		/// <param name="h">The next byte of the GUID. </param>
		/// <param name="i">The next byte of the GUID. </param>
		/// <param name="j">The next byte of the GUID. </param>
		/// <param name="k">The next byte of the GUID. </param>
		[CLSCompliant(false)]
		public Guid(uint a, ushort b, ushort c, byte d, byte e, byte f, byte g, byte h, byte i, byte j, byte k)
			: this((int)a, (short)b, (short)c, d, e, f, g, h, i, j, k)
		{
		}

		static Guid()
		{
			Empty = new Guid(0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0);
			_rngAccess = new object();
			if (MonoTouchAOTHelper.FalseFlag)
			{
				GenericComparer<Guid> genericComparer = new GenericComparer<Guid>();
				GenericEqualityComparer<Guid> genericEqualityComparer = new GenericEqualityComparer<Guid>();
			}
		}

		private static void CheckNull(object o)
		{
			if (o == null)
			{
				throw new ArgumentNullException(Locale.GetText("Value cannot be null."));
			}
		}

		private static void CheckLength(byte[] o, int l)
		{
			if (o.Length != l)
			{
				throw new ArgumentException(string.Format(Locale.GetText("Array should be exactly {0} bytes long."), l));
			}
		}

		private static void CheckArray(byte[] o, int l)
		{
			CheckNull(o);
			CheckLength(o, l);
		}

		private static int Compare(int x, int y)
		{
			if (x < y)
			{
				return -1;
			}
			return 1;
		}

		/// <summary>Compares this instance to a specified object and returns an indication of their relative values.</summary>
		/// <returns>A signed number indicating the relative values of this instance and <paramref name="value" />.Value Description A negative integer This instance is less than <paramref name="value" />. Zero This instance is equal to <paramref name="value" />. A positive integer This instance is greater than <paramref name="value" />.-or- <paramref name="value" /> is null. </returns>
		/// <param name="value">An object to compare, or null. </param>
		/// <exception cref="T:System.ArgumentException">
		///   <paramref name="value" /> is not a <see cref="T:System.Guid" />. </exception>
		/// <filterpriority>2</filterpriority>
		public int CompareTo(object value)
		{
			if (value == null)
			{
				return 1;
			}
			if (!(value is Guid))
			{
				throw new ArgumentException("value", Locale.GetText("Argument of System.Guid.CompareTo should be a Guid."));
			}
			return CompareTo((Guid)value);
		}

		/// <summary>Returns a value indicating whether this instance is equal to a specified object.</summary>
		/// <returns>true if <paramref name="o" /> is a <see cref="T:System.Guid" /> that has the same value as this instance; otherwise, false.</returns>
		/// <param name="o">The object to compare with this instance. </param>
		/// <filterpriority>2</filterpriority>
		public override bool Equals(object o)
		{
			if (o is Guid)
			{
				return CompareTo((Guid)o) == 0;
			}
			return false;
		}

		/// <summary>Compares this instance to a specified <see cref="T:System.Guid" /> object and returns an indication of their relative values.</summary>
		/// <returns>A signed number indicating the relative values of this instance and <paramref name="value" />.Value Description A negative integer This instance is less than <paramref name="value" />. Zero This instance is equal to <paramref name="value" />. A positive integer This instance is greater than <paramref name="value" />. </returns>
		/// <param name="value">A <see cref="T:System.Guid" /> object to compare to this instance.</param>
		/// <filterpriority>2</filterpriority>
		public int CompareTo(Guid value)
		{
			if (_a != value._a)
			{
				return Compare(_a, value._a);
			}
			if (_b != value._b)
			{
				return Compare(_b, value._b);
			}
			if (_c != value._c)
			{
				return Compare(_c, value._c);
			}
			if (_d != value._d)
			{
				return Compare(_d, value._d);
			}
			if (_e != value._e)
			{
				return Compare(_e, value._e);
			}
			if (_f != value._f)
			{
				return Compare(_f, value._f);
			}
			if (_g != value._g)
			{
				return Compare(_g, value._g);
			}
			if (_h != value._h)
			{
				return Compare(_h, value._h);
			}
			if (_i != value._i)
			{
				return Compare(_i, value._i);
			}
			if (_j != value._j)
			{
				return Compare(_j, value._j);
			}
			if (_k != value._k)
			{
				return Compare(_k, value._k);
			}
			return 0;
		}

		/// <summary>Returns a value indicating whether this instance and a specified <see cref="T:System.Guid" /> object represent the same value.</summary>
		/// <returns>true if <paramref name="g" /> is equal to this instance; otherwise, false.</returns>
		/// <param name="g">A <see cref="T:System.Guid" /> object to compare to this instance.</param>
		/// <filterpriority>2</filterpriority>
		public bool Equals(Guid g)
		{
			return CompareTo(g) == 0;
		}

		/// <summary>Returns the hash code for this instance.</summary>
		/// <returns>The hash code for this instance.</returns>
		/// <filterpriority>2</filterpriority>
		public override int GetHashCode()
		{
			int a = _a;
			a ^= ((_b << 16) | _c);
			a ^= _d << 24;
			a ^= _e << 16;
			a ^= _f << 8;
			a ^= _g;
			a ^= _h << 24;
			a ^= _i << 16;
			a ^= _j << 8;
			return a ^ _k;
		}

		private static char ToHex(int b)
		{
			return (char)((b >= 10) ? (97 + b - 10) : (48 + b));
		}

		/// <summary>Initializes a new instance of the <see cref="T:System.Guid" /> class.</summary>
		/// <returns>A new <see cref="T:System.Guid" /> object.</returns>
		/// <filterpriority>1</filterpriority>
		public static Guid NewGuid()
		{
			byte[] array = new byte[16];
			lock (_rngAccess)
			{
				if (_rng == null)
				{
					_rng = RandomNumberGenerator.Create();
				}
				_rng.GetBytes(array);
			}
			Guid result = new Guid(array);
			result._d = (byte)((result._d & 0x3F) | 0x80);
			result._c = (short)(((long)result._c & 0xFFFL) | 0x4000);
			return result;
		}

		internal static byte[] FastNewGuidArray()
		{
			byte[] array = new byte[16];
			lock (_rngAccess)
			{
				if (_rng != null)
				{
					_fastRng = _rng;
				}
				if (_fastRng == null)
				{
					_fastRng = new RNGCryptoServiceProvider();
				}
				_fastRng.GetBytes(array);
			}
			array[8] = (byte)((array[8] & 0x3F) | 0x80);
			array[7] = (byte)((array[7] & 0xF) | 0x40);
			return array;
		}

		/// <summary>Returns a 16-element byte array that contains the value of this instance.</summary>
		/// <returns>A 16-element byte array.</returns>
		/// <filterpriority>2</filterpriority>
		public byte[] ToByteArray()
		{
			byte[] array = new byte[16];
			int num = 0;
			byte[] bytes = BitConverterLE.GetBytes(_a);
			for (int i = 0; i < 4; i++)
			{
				array[num++] = bytes[i];
			}
			bytes = BitConverterLE.GetBytes(_b);
			for (int i = 0; i < 2; i++)
			{
				array[num++] = bytes[i];
			}
			bytes = BitConverterLE.GetBytes(_c);
			for (int i = 0; i < 2; i++)
			{
				array[num++] = bytes[i];
			}
			array[8] = _d;
			array[9] = _e;
			array[10] = _f;
			array[11] = _g;
			array[12] = _h;
			array[13] = _i;
			array[14] = _j;
			array[15] = _k;
			return array;
		}

		private static void AppendInt(StringBuilder builder, int value)
		{
			builder.Append(ToHex((value >> 28) & 0xF));
			builder.Append(ToHex((value >> 24) & 0xF));
			builder.Append(ToHex((value >> 20) & 0xF));
			builder.Append(ToHex((value >> 16) & 0xF));
			builder.Append(ToHex((value >> 12) & 0xF));
			builder.Append(ToHex((value >> 8) & 0xF));
			builder.Append(ToHex((value >> 4) & 0xF));
			builder.Append(ToHex(value & 0xF));
		}

		private static void AppendShort(StringBuilder builder, short value)
		{
			builder.Append(ToHex((value >> 12) & 0xF));
			builder.Append(ToHex((value >> 8) & 0xF));
			builder.Append(ToHex((value >> 4) & 0xF));
			builder.Append(ToHex(value & 0xF));
		}

		private static void AppendByte(StringBuilder builder, byte value)
		{
			builder.Append(ToHex((value >> 4) & 0xF));
			builder.Append(ToHex(value & 0xF));
		}

		private string BaseToString(bool h, bool p, bool b)
		{
			StringBuilder stringBuilder = new StringBuilder(40);
			if (p)
			{
				stringBuilder.Append('(');
			}
			else if (b)
			{
				stringBuilder.Append('{');
			}
			AppendInt(stringBuilder, _a);
			if (h)
			{
				stringBuilder.Append('-');
			}
			AppendShort(stringBuilder, _b);
			if (h)
			{
				stringBuilder.Append('-');
			}
			AppendShort(stringBuilder, _c);
			if (h)
			{
				stringBuilder.Append('-');
			}
			AppendByte(stringBuilder, _d);
			AppendByte(stringBuilder, _e);
			if (h)
			{
				stringBuilder.Append('-');
			}
			AppendByte(stringBuilder, _f);
			AppendByte(stringBuilder, _g);
			AppendByte(stringBuilder, _h);
			AppendByte(stringBuilder, _i);
			AppendByte(stringBuilder, _j);
			AppendByte(stringBuilder, _k);
			if (p)
			{
				stringBuilder.Append(')');
			}
			else if (b)
			{
				stringBuilder.Append('}');
			}
			return stringBuilder.ToString();
		}

		/// <summary>Returns a <see cref="T:System.String" /> representation of the value of this instance in registry format.</summary>
		/// <returns>A String formatted in this pattern: xxxxxxxx-xxxx-xxxx-xxxx-xxxxxxxxxxxx where the value of the GUID is represented as a series of lower-case hexadecimal digits in groups of 8, 4, 4, 4, and 12 digits and separated by hyphens. An example of a return value is "382c74c3-721d-4f34-80e5-57657b6cbc27".</returns>
		/// <filterpriority>1</filterpriority>
		public override string ToString()
		{
			return BaseToString(h: true, p: false, b: false);
		}

		/// <summary>Returns a <see cref="T:System.String" /> representation of the value of this <see cref="T:System.Guid" /> instance, according to the provided format specifier.</summary>
		/// <returns>A <see cref="T:System.String" /> representation of the value of this <see cref="T:System.Guid" />.</returns>
		/// <param name="format">A single format specifier that indicates how to format the value of this <see cref="T:System.Guid" />. The <paramref name="format" /> parameter can be "N", "D", "B", or "P". If <paramref name="format" /> is null or the empty string (""), "D" is used. </param>
		/// <exception cref="T:System.FormatException">The value of <paramref name="format" /> is not null, the empty string (""), "N", "D", "B", or "P". </exception>
		/// <filterpriority>1</filterpriority>
		public string ToString(string format)
		{
			bool h = true;
			bool p = false;
			bool b = false;
			if (format != null)
			{
				string text = format.ToLowerInvariant();
				switch (text)
				{
				case "b":
					b = true;
					break;
				case "p":
					p = true;
					break;
				case "n":
					h = false;
					break;
				default:
					if (text != "d" && text != string.Empty)
					{
						throw new FormatException(Locale.GetText("Argument to Guid.ToString(string format) should be \"b\", \"B\", \"d\", \"D\", \"n\", \"N\", \"p\" or \"P\""));
					}
					break;
				}
			}
			return BaseToString(h, p, b);
		}

		/// <summary>Returns a <see cref="T:System.String" /> representation of the value of this instance of the <see cref="T:System.Guid" /> class, according to the provided format specifier and culture-specific format information.</summary>
		/// <returns>A <see cref="T:System.String" /> representation of the value of this <see cref="T:System.Guid" />.</returns>
		/// <param name="format">A single format specifier that indicates how to format the value of this <see cref="T:System.Guid" />. The <paramref name="format" /> parameter can be "N", "D", "B", or "P". If <paramref name="format" /> is null or the empty string (""), "D" is used. </param>
		/// <param name="provider">(Reserved) An IFormatProvider reference that supplies culture-specific formatting services. </param>
		/// <exception cref="T:System.FormatException">The value of <paramref name="format" /> is not null, the empty string (""), "N", "D", "B", or "P". </exception>
		/// <filterpriority>1</filterpriority>
		public string ToString(string format, IFormatProvider provider)
		{
			return ToString(format);
		}

		/// <summary>Returns an indication whether the values of two specified <see cref="T:System.Guid" /> objects are equal.</summary>
		/// <returns>true if <paramref name="a" /> and <paramref name="b" /> are equal; otherwise, false.</returns>
		/// <param name="a">A <see cref="T:System.Guid" /> object. </param>
		/// <param name="b">A <see cref="T:System.Guid" /> object. </param>
		/// <filterpriority>3</filterpriority>
		public static bool operator ==(Guid a, Guid b)
		{
			return a.Equals(b);
		}

		/// <summary>Returns an indication whether the values of two specified <see cref="T:System.Guid" /> objects are not equal.</summary>
		/// <returns>true if <paramref name="a" /> and <paramref name="b" /> are not equal; otherwise, false.</returns>
		/// <param name="a">A <see cref="T:System.Guid" /> object. </param>
		/// <param name="b">A <see cref="T:System.Guid" /> object. </param>
		/// <filterpriority>3</filterpriority>
		public static bool operator !=(Guid a, Guid b)
		{
			return !a.Equals(b);
		}
	}
}
