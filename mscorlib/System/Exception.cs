using System.Collections;
using System.Diagnostics;
using System.Reflection;
using System.Runtime.InteropServices;
using System.Runtime.Serialization;
using System.Text;

namespace System
{
	/// <summary>Represents errors that occur during application execution.</summary>
	/// <filterpriority>1</filterpriority>
	[Serializable]
	[ClassInterface(ClassInterfaceType.None)]
	[ComDefaultInterface(typeof(_Exception))]
	[ComVisible(true)]
	public class Exception : ISerializable, _Exception
	{
		private IntPtr[] trace_ips;

		private Exception inner_exception;

		internal string message;

		private string help_link;

		private string class_name;

		private string stack_trace;

		private string _remoteStackTraceString;

		private int remote_stack_index;

		internal int hresult = -2146233088;

		private string source;

		private IDictionary _data;

		/// <summary>Gets the <see cref="T:System.Exception" /> instance that caused the current exception.</summary>
		/// <returns>An instance of Exception that describes the error that caused the current exception. The InnerException property returns the same value as was passed into the constructor, or a null reference (Nothing in Visual Basic) if the inner exception value was not supplied to the constructor. This property is read-only.</returns>
		/// <filterpriority>1</filterpriority>
		public Exception InnerException => inner_exception;

		/// <summary>Gets or sets a link to the help file associated with this exception.</summary>
		/// <returns>The Uniform Resource Name (URN) or Uniform Resource Locator (URL).</returns>
		/// <filterpriority>2</filterpriority>
		public virtual string HelpLink
		{
			get
			{
				return help_link;
			}
			set
			{
				help_link = value;
			}
		}

		/// <summary>Gets or sets HRESULT, a coded numerical value that is assigned to a specific exception.</summary>
		/// <returns>The HRESULT value.</returns>
		protected int HResult
		{
			get
			{
				return hresult;
			}
			set
			{
				hresult = value;
			}
		}

		private string ClassName
		{
			get
			{
				if (class_name == null)
				{
					class_name = GetType().ToString();
				}
				return class_name;
			}
		}

		/// <summary>Gets a message that describes the current exception.</summary>
		/// <returns>The error message that explains the reason for the exception, or an empty string("").</returns>
		/// <filterpriority>1</filterpriority>
		public virtual string Message
		{
			get
			{
				if (message == null)
				{
					message = string.Format(Locale.GetText("Exception of type '{0}' was thrown."), ClassName);
				}
				return message;
			}
		}

		/// <summary>Gets or sets the name of the application or the object that causes the error.</summary>
		/// <returns>The name of the application or the object that causes the error.</returns>
		/// <filterpriority>2</filterpriority>
		public virtual string Source
		{
			get
			{
				if (source == null)
				{
					StackTrace stackTrace = new StackTrace(this, fNeedFileInfo: true);
					if (stackTrace.FrameCount > 0)
					{
						StackFrame frame = stackTrace.GetFrame(0);
						if (stackTrace != null)
						{
							MethodBase method = frame.GetMethod();
							if (method != null)
							{
								source = method.DeclaringType.Assembly.UnprotectedGetName().Name;
							}
						}
					}
				}
				return source;
			}
			set
			{
				source = value;
			}
		}

		/// <summary>Gets a string representation of the frames on the call stack at the time the current exception was thrown.</summary>
		/// <returns>A string that describes the contents of the call stack, with the most recent method call appearing first.</returns>
		/// <filterpriority>2</filterpriority>
		/// <PermissionSet>
		///   <IPermission class="System.Security.Permissions.FileIOPermission, mscorlib, Version=2.0.3600.0, Culture=neutral, PublicKeyToken=b77a5c561934e089" version="1" PathDiscovery="*AllFiles*" />
		/// </PermissionSet>
		public virtual string StackTrace
		{
			get
			{
				if (stack_trace == null)
				{
					if (trace_ips == null)
					{
						return null;
					}
					StackTrace stackTrace = new StackTrace(this, 0, fNeedFileInfo: true, returnNativeFrames: true);
					StringBuilder stringBuilder = new StringBuilder();
					string value = string.Format("{0}  {1} ", Environment.NewLine, Locale.GetText("at"));
					string text = Locale.GetText("<unknown method>");
					for (int i = 0; i < stackTrace.FrameCount; i++)
					{
						StackFrame frame = stackTrace.GetFrame(i);
						if (i == 0)
						{
							stringBuilder.AppendFormat("  {0} ", Locale.GetText("at"));
						}
						else
						{
							stringBuilder.Append(value);
						}
						if (frame.GetMethod() == null)
						{
							string internalMethodName = frame.GetInternalMethodName();
							if (internalMethodName != null)
							{
								stringBuilder.Append(internalMethodName);
							}
							else
							{
								stringBuilder.AppendFormat("<0x{0:x5}> {1}", frame.GetNativeOffset(), text);
							}
							continue;
						}
						GetFullNameForStackTrace(stringBuilder, frame.GetMethod());
						if (frame.GetILOffset() == -1)
						{
							stringBuilder.AppendFormat(" <0x{0:x5}> ", frame.GetNativeOffset());
						}
						else
						{
							stringBuilder.AppendFormat(" [0x{0:x5}] ", frame.GetILOffset());
						}
						stringBuilder.AppendFormat("in {0}:{1} ", frame.GetSecureFileName(), frame.GetFileLineNumber());
					}
					stack_trace = stringBuilder.ToString();
				}
				return stack_trace;
			}
		}

		/// <summary>Gets the method that throws the current exception.</summary>
		/// <returns>The <see cref="T:System.Reflection.MethodBase" /> that threw the current exception.</returns>
		/// <filterpriority>2</filterpriority>
		/// <PermissionSet>
		///   <IPermission class="System.Security.Permissions.FileIOPermission, mscorlib, Version=2.0.3600.0, Culture=neutral, PublicKeyToken=b77a5c561934e089" version="1" Unrestricted="true" />
		///   <IPermission class="System.Security.Permissions.SecurityPermission, mscorlib, Version=2.0.3600.0, Culture=neutral, PublicKeyToken=b77a5c561934e089" version="1" Flags="ControlEvidence" />
		/// </PermissionSet>
		public MethodBase TargetSite
		{
			get
			{
				StackTrace stackTrace = new StackTrace(this, fNeedFileInfo: true);
				if (stackTrace.FrameCount > 0)
				{
					return stackTrace.GetFrame(0).GetMethod();
				}
				return null;
			}
		}

		/// <summary>Gets a collection of key/value pairs that provide additional user-defined information about the exception.</summary>
		/// <returns>An object that implements the <see cref="T:System.Collections.IDictionary" /> interface and contains a collection of user-defined key/value pairs. The default is an empty collection.</returns>
		/// <filterpriority>2</filterpriority>
		public virtual IDictionary Data
		{
			get
			{
				if (_data == null)
				{
					_data = new Hashtable();
				}
				return _data;
			}
		}

		/// <summary>Initializes a new instance of the <see cref="T:System.Exception" /> class.</summary>
		public Exception()
		{
		}

		/// <summary>Initializes a new instance of the <see cref="T:System.Exception" /> class with a specified error message.</summary>
		/// <param name="message">The message that describes the error. </param>
		public Exception(string message)
		{
			this.message = message;
		}

		/// <summary>Initializes a new instance of the <see cref="T:System.Exception" /> class with serialized data.</summary>
		/// <param name="info">The <see cref="T:System.Runtime.Serialization.SerializationInfo" /> that holds the serialized object data about the exception being thrown. </param>
		/// <param name="context">The <see cref="T:System.Runtime.Serialization.StreamingContext" /> that contains contextual information about the source or destination. </param>
		/// <exception cref="T:System.ArgumentNullException">The <paramref name="info" /> parameter is null. </exception>
		/// <exception cref="T:System.Runtime.Serialization.SerializationException">The class name is null or <see cref="P:System.Exception.HResult" /> is zero (0). </exception>
		protected Exception(SerializationInfo info, StreamingContext context)
		{
			if (info == null)
			{
				throw new ArgumentNullException("info");
			}
			class_name = info.GetString("ClassName");
			message = info.GetString("Message");
			help_link = info.GetString("HelpURL");
			stack_trace = info.GetString("StackTraceString");
			_remoteStackTraceString = info.GetString("RemoteStackTraceString");
			remote_stack_index = info.GetInt32("RemoteStackIndex");
			hresult = info.GetInt32("HResult");
			source = info.GetString("Source");
			inner_exception = (Exception)info.GetValue("InnerException", typeof(Exception));
			try
			{
				_data = (IDictionary)info.GetValue("Data", typeof(IDictionary));
			}
			catch (SerializationException)
			{
			}
		}

		/// <summary>Initializes a new instance of the <see cref="T:System.Exception" /> class with a specified error message and a reference to the inner exception that is the cause of this exception.</summary>
		/// <param name="message">The error message that explains the reason for the exception. </param>
		/// <param name="innerException">The exception that is the cause of the current exception, or a null reference (Nothing in Visual Basic) if no inner exception is specified. </param>
		public Exception(string message, Exception innerException)
		{
			inner_exception = innerException;
			this.message = message;
		}

		internal void SetMessage(string s)
		{
			message = s;
		}

		internal void SetStackTrace(string s)
		{
			stack_trace = s;
		}

		/// <summary>When overridden in a derived class, returns the <see cref="T:System.Exception" /> that is the root cause of one or more subsequent exceptions.</summary>
		/// <returns>The first exception thrown in a chain of exceptions. If the <see cref="P:System.Exception.InnerException" /> property of the current exception is a null reference (Nothing in Visual Basic), this property returns the current exception.</returns>
		/// <filterpriority>2</filterpriority>
		public virtual Exception GetBaseException()
		{
			Exception innerException = inner_exception;
			while (innerException != null)
			{
				if (innerException.InnerException != null)
				{
					innerException = innerException.InnerException;
					continue;
				}
				return innerException;
			}
			return this;
		}

		/// <summary>When overridden in a derived class, sets the <see cref="T:System.Runtime.Serialization.SerializationInfo" /> with information about the exception.</summary>
		/// <param name="info">The <see cref="T:System.Runtime.Serialization.SerializationInfo" /> that holds the serialized object data about the exception being thrown. </param>
		/// <param name="context">The <see cref="T:System.Runtime.Serialization.StreamingContext" /> that contains contextual information about the source or destination. </param>
		/// <exception cref="T:System.ArgumentNullException">The <paramref name="info" /> parameter is a null reference (Nothing in Visual Basic). </exception>
		/// <filterpriority>2</filterpriority>
		/// <PermissionSet>
		///   <IPermission class="System.Security.Permissions.FileIOPermission, mscorlib, Version=2.0.3600.0, Culture=neutral, PublicKeyToken=b77a5c561934e089" version="1" Read="*AllFiles*" PathDiscovery="*AllFiles*" />
		///   <IPermission class="System.Security.Permissions.SecurityPermission, mscorlib, Version=2.0.3600.0, Culture=neutral, PublicKeyToken=b77a5c561934e089" version="1" Flags="SerializationFormatter" />
		/// </PermissionSet>
		public virtual void GetObjectData(SerializationInfo info, StreamingContext context)
		{
			if (info == null)
			{
				throw new ArgumentNullException("info");
			}
			info.AddValue("ClassName", ClassName);
			info.AddValue("Message", message);
			info.AddValue("InnerException", inner_exception);
			info.AddValue("HelpURL", help_link);
			info.AddValue("StackTraceString", StackTrace);
			info.AddValue("RemoteStackTraceString", _remoteStackTraceString);
			info.AddValue("RemoteStackIndex", remote_stack_index);
			info.AddValue("HResult", hresult);
			info.AddValue("Source", Source);
			info.AddValue("ExceptionMethod", null);
			info.AddValue("Data", _data, typeof(IDictionary));
		}

		/// <summary>Creates and returns a string representation of the current exception.</summary>
		/// <returns>A string representation of the current exception.</returns>
		/// <filterpriority>1</filterpriority>
		/// <PermissionSet>
		///   <IPermission class="System.Security.Permissions.FileIOPermission, mscorlib, Version=2.0.3600.0, Culture=neutral, PublicKeyToken=b77a5c561934e089" version="1" PathDiscovery="*AllFiles*" />
		/// </PermissionSet>
		public override string ToString()
		{
			StringBuilder stringBuilder = new StringBuilder(ClassName);
			stringBuilder.Append(": ").Append(Message);
			if (_remoteStackTraceString != null)
			{
				stringBuilder.Append(_remoteStackTraceString);
			}
			if (inner_exception != null)
			{
				stringBuilder.Append(" ---> ").Append(inner_exception.ToString());
				stringBuilder.Append(Environment.NewLine);
				stringBuilder.Append(Locale.GetText("  --- End of inner exception stack trace ---"));
			}
			if (StackTrace != null)
			{
				stringBuilder.Append(Environment.NewLine).Append(StackTrace);
			}
			return stringBuilder.ToString();
		}

		internal Exception FixRemotingException()
		{
			string format = (remote_stack_index != 0) ? Locale.GetText("{1}{0}{0}Exception rethrown at [{2}]: {0}") : Locale.GetText("{0}{0}Server stack trace: {0}{1}{0}{0}Exception rethrown at [{2}]: {0}");
			string text = _remoteStackTraceString = string.Format(format, Environment.NewLine, StackTrace, remote_stack_index);
			remote_stack_index++;
			stack_trace = null;
			return this;
		}

		internal void GetFullNameForStackTrace(StringBuilder sb, MethodBase mi)
		{
			ParameterInfo[] parameters = mi.GetParameters();
			sb.Append(mi.DeclaringType.ToString());
			sb.Append(".");
			sb.Append(mi.Name);
			if (mi.IsGenericMethod)
			{
				Type[] genericArguments = mi.GetGenericArguments();
				sb.Append("[");
				for (int i = 0; i < genericArguments.Length; i++)
				{
					if (i > 0)
					{
						sb.Append(",");
					}
					sb.Append(genericArguments[i].Name);
				}
				sb.Append("]");
			}
			sb.Append(" (");
			for (int j = 0; j < parameters.Length; j++)
			{
				if (j > 0)
				{
					sb.Append(", ");
				}
				Type parameterType = parameters[j].ParameterType;
				if (parameterType.IsClass && parameterType.Namespace != string.Empty)
				{
					sb.Append(parameterType.Namespace);
					sb.Append(".");
				}
				sb.Append(parameterType.Name);
				if (parameters[j].Name != null)
				{
					sb.Append(" ");
					sb.Append(parameters[j].Name);
				}
			}
			sb.Append(")");
		}

		/// <summary>Gets the runtime type of the current instance.</summary>
		/// <returns>A <see cref="T:System.Type" /> object that represents the exact runtime type of the current instance.</returns>
		/// <filterpriority>2</filterpriority>
		public new Type GetType()
		{
			return base.GetType();
		}
	}
}
