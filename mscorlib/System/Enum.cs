using System.Collections;
using System.Globalization;
using System.Runtime.CompilerServices;
using System.Runtime.InteropServices;

namespace System
{
	/// <summary>Provides the base class for enumerations.</summary>
	/// <filterpriority>2</filterpriority>
	[Serializable]
	[ComVisible(true)]
	public abstract class Enum : ValueType, IFormattable, IConvertible, IComparable
	{
		private static char[] split_char = new char[1]
		{
			','
		};

		private object Value => get_value();

		/// <summary>Converts the current value to a Boolean value based on the underlying type.</summary>
		/// <param name="provider">An object that supplies culture-specific formatting information.</param>
		/// <exception cref="T:System.InvalidCastException">Always. </exception>
		bool IConvertible.ToBoolean(IFormatProvider provider)
		{
			return Convert.ToBoolean(Value, provider);
		}

		/// <summary>Converts the current value to an 8-bit unsigned integer based on the underlying type.</summary>
		/// <returns>The converted value.</returns>
		/// <param name="provider">An object that supplies culture-specific formatting information.</param>
		byte IConvertible.ToByte(IFormatProvider provider)
		{
			return Convert.ToByte(Value, provider);
		}

		/// <summary>Converts the current value to a unicode character based on the underlying type.</summary>
		/// <param name="provider">An object that supplies culture-specific formatting information.</param>
		/// <exception cref="T:System.InvalidCastException">Always. </exception>
		char IConvertible.ToChar(IFormatProvider provider)
		{
			return Convert.ToChar(Value, provider);
		}

		/// <summary>Converts the current value to a <see cref="T:System.DateTime" /> based on the underlying type.</summary>
		/// <param name="provider">An object that supplies culture-specific formatting information.</param>
		/// <exception cref="T:System.InvalidCastException">Always. </exception>
		DateTime IConvertible.ToDateTime(IFormatProvider provider)
		{
			return Convert.ToDateTime(Value, provider);
		}

		/// <summary>Converts the current value to a <see cref="T:System.Decimal" /> based on the underlying type.</summary>
		/// <param name="provider">An object that supplies culture-specific formatting information.</param>
		/// <exception cref="T:System.InvalidCastException">Always. </exception>
		decimal IConvertible.ToDecimal(IFormatProvider provider)
		{
			return Convert.ToDecimal(Value, provider);
		}

		/// <summary>Converts the current value to a double-precision floating point number based on the underlying type.</summary>
		/// <param name="provider">An object that supplies culture-specific formatting information.</param>
		/// <exception cref="T:System.InvalidCastException">Always. </exception>
		double IConvertible.ToDouble(IFormatProvider provider)
		{
			return Convert.ToDouble(Value, provider);
		}

		/// <summary>Converts the current value to a 16-bit signed integer based on the underlying type.</summary>
		/// <returns>The converted value.</returns>
		/// <param name="provider">An object that supplies culture-specific formatting information.</param>
		short IConvertible.ToInt16(IFormatProvider provider)
		{
			return Convert.ToInt16(Value, provider);
		}

		/// <summary>Converts the current value to a 32-bit signed integer based on the underlying type.</summary>
		/// <returns>The converted value.</returns>
		/// <param name="provider">An object that supplies culture-specific formatting information.</param>
		int IConvertible.ToInt32(IFormatProvider provider)
		{
			return Convert.ToInt32(Value, provider);
		}

		/// <summary>Converts the current value to a 64-bit signed integer based on the underlying type.</summary>
		/// <returns>The converted value.</returns>
		/// <param name="provider">An object that supplies culture-specific formatting information.</param>
		long IConvertible.ToInt64(IFormatProvider provider)
		{
			return Convert.ToInt64(Value, provider);
		}

		/// <summary>Converts the current value to an 8-bit signed integer based on the underlying type.</summary>
		/// <returns>The converted value.</returns>
		/// <param name="provider">An object that supplies culture-specific formatting information.</param>
		sbyte IConvertible.ToSByte(IFormatProvider provider)
		{
			return Convert.ToSByte(Value, provider);
		}

		/// <summary>Converts the current value to a single-precision floating point number based on the underlying type.</summary>
		/// <param name="provider">An object that supplies culture-specific formatting information.</param>
		/// <exception cref="T:System.InvalidCastException">Always. </exception>
		float IConvertible.ToSingle(IFormatProvider provider)
		{
			return Convert.ToSingle(Value, provider);
		}

		/// <summary>Converts the current value to a specified type based on the underlying type.</summary>
		/// <returns>The converted value.</returns>
		/// <param name="type">The type to convert to. </param>
		/// <param name="provider">An object that supplies culture-specific formatting information.</param>
		object IConvertible.ToType(Type targetType, IFormatProvider provider)
		{
			if (targetType == null)
			{
				throw new ArgumentNullException("targetType");
			}
			if (targetType == typeof(string))
			{
				return ToString(provider);
			}
			return Convert.ToType(Value, targetType, provider, try_target_to_type: false);
		}

		/// <summary>Converts the current value to a 16-bit unsigned integer based on the underlying type.</summary>
		/// <returns>The converted value.</returns>
		/// <param name="provider">An object that supplies culture-specific formatting information.</param>
		ushort IConvertible.ToUInt16(IFormatProvider provider)
		{
			return Convert.ToUInt16(Value, provider);
		}

		/// <summary>Converts the current value to a 32-bit unsigned integer based on the underlying type.</summary>
		/// <returns>The converted value.</returns>
		/// <param name="provider">An object that supplies culture-specific formatting information.</param>
		uint IConvertible.ToUInt32(IFormatProvider provider)
		{
			return Convert.ToUInt32(Value, provider);
		}

		/// <summary>Converts the current value to a 64-bit unsigned integer based on the underlying type.</summary>
		/// <returns>The converted value.</returns>
		/// <param name="provider">An object that supplies culture-specific formatting information.</param>
		ulong IConvertible.ToUInt64(IFormatProvider provider)
		{
			return Convert.ToUInt64(Value, provider);
		}

		/// <summary>Returns the underlying <see cref="T:System.TypeCode" /> for this instance.</summary>
		/// <returns>The <see cref="T:System.TypeCode" /> for this instance.</returns>
		/// <exception cref="T:System.InvalidOperationException">The enumeration type is unknown.</exception>
		/// <filterpriority>2</filterpriority>
		public TypeCode GetTypeCode()
		{
			return Type.GetTypeCode(GetUnderlyingType(GetType()));
		}

		[MethodImpl(MethodImplOptions.InternalCall)]
		private extern object get_value();

		/// <summary>Retrieves an array of the values of the constants in a specified enumeration.</summary>
		/// <returns>An <see cref="T:System.Array" /> of the values of the constants in <paramref name="enumType" />. The elements of the array are sorted by the binary values of the enumeration constants.</returns>
		/// <param name="enumType">An enumeration type. </param>
		/// <exception cref="T:System.ArgumentNullException">
		///   <paramref name="enumType" /> is null. </exception>
		/// <exception cref="T:System.ArgumentException">
		///   <paramref name="enumType" /> is not an <see cref="T:System.Enum" />. </exception>
		/// <filterpriority>1</filterpriority>
		[ComVisible(true)]
		public static Array GetValues(Type enumType)
		{
			if (enumType == null)
			{
				throw new ArgumentNullException("enumType");
			}
			if (!enumType.IsEnum)
			{
				throw new ArgumentException("enumType is not an Enum type.", "enumType");
			}
			MonoEnumInfo.GetInfo(enumType, out MonoEnumInfo info);
			return (Array)info.values.Clone();
		}

		/// <summary>Retrieves an array of the names of the constants in a specified enumeration.</summary>
		/// <returns>A string array of the names of the constants in <paramref name="enumType" />. </returns>
		/// <param name="enumType">An enumeration type. </param>
		/// <exception cref="T:System.ArgumentNullException">
		///   <paramref name="enumType" /> is null. </exception>
		/// <exception cref="T:System.ArgumentException">
		///   <paramref name="enumType" /> parameter is not an <see cref="T:System.Enum" />. </exception>
		/// <filterpriority>1</filterpriority>
		[ComVisible(true)]
		public static string[] GetNames(Type enumType)
		{
			if (enumType == null)
			{
				throw new ArgumentNullException("enumType");
			}
			if (!enumType.IsEnum)
			{
				throw new ArgumentException("enumType is not an Enum type.");
			}
			MonoEnumInfo.GetInfo(enumType, out MonoEnumInfo info);
			return (string[])info.names.Clone();
		}

		private static int FindPosition(object value, Array values)
		{
			IComparer comparer = null;
			if (!(values is byte[]) && !(values is ushort[]) && !(values is uint[]) && !(values is ulong[]))
			{
				if (values is int[])
				{
					return Array.BinarySearch(values, value, MonoEnumInfo.int_comparer);
				}
				if (values is short[])
				{
					return Array.BinarySearch(values, value, MonoEnumInfo.short_comparer);
				}
				if (values is sbyte[])
				{
					return Array.BinarySearch(values, value, MonoEnumInfo.sbyte_comparer);
				}
				if (values is long[])
				{
					return Array.BinarySearch(values, value, MonoEnumInfo.long_comparer);
				}
			}
			return Array.BinarySearch(values, value);
		}

		/// <summary>Retrieves the name of the constant in the specified enumeration that has the specified value.</summary>
		/// <returns>A string containing the name of the enumerated constant in <paramref name="enumType" /> whose value is <paramref name="value" />, or null if no such constant is found.</returns>
		/// <param name="enumType">An enumeration type. </param>
		/// <param name="value">The value of a particular enumerated constant in terms of its underlying type. </param>
		/// <exception cref="T:System.ArgumentNullException">
		///   <paramref name="enumType" /> or <paramref name="value" /> is null. </exception>
		/// <exception cref="T:System.ArgumentException">
		///   <paramref name="enumType" /> is not an <see cref="T:System.Enum" />.-or- <paramref name="value" /> is neither of type <paramref name="enumType" /> nor does it have the same underlying type as <paramref name="enumType" />. </exception>
		/// <filterpriority>1</filterpriority>
		[ComVisible(true)]
		public static string GetName(Type enumType, object value)
		{
			if (enumType == null)
			{
				throw new ArgumentNullException("enumType");
			}
			if (value == null)
			{
				throw new ArgumentNullException("value");
			}
			if (!enumType.IsEnum)
			{
				throw new ArgumentException("enumType is not an Enum type.", "enumType");
			}
			value = ToObject(enumType, value);
			MonoEnumInfo.GetInfo(enumType, out MonoEnumInfo info);
			int num = FindPosition(value, info.values);
			return (num < 0) ? null : info.names[num];
		}

		/// <summary>Returns an indication whether a constant with a specified value exists in a specified enumeration.</summary>
		/// <returns>true if a constant in <paramref name="enumType" /> has a value equal to <paramref name="value" />; otherwise, false.</returns>
		/// <param name="enumType">An enumeration type. </param>
		/// <param name="value">The value or name of a constant in <paramref name="enumType" />. </param>
		/// <exception cref="T:System.ArgumentNullException">
		///   <paramref name="enumType" /> or <paramref name="value" /> is null. </exception>
		/// <exception cref="T:System.ArgumentException">
		///   <paramref name="enumType" /> is not an Enum.-or- The type of <paramref name="value" /> is not an <paramref name="enumType" />.-or- The type of <paramref name="value" /> is not an underlying type of <paramref name="enumType" />. </exception>
		/// <exception cref="T:System.InvalidOperationException">
		///   <paramref name="value" /> is not type <see cref="T:System.SByte" />, <see cref="T:System.Int16" />, <see cref="T:System.Int32" />, <see cref="T:System.Int64" />, <see cref="T:System.Byte" />, <see cref="T:System.UInt16" />, <see cref="T:System.UInt32" />, or <see cref="T:System.UInt64" />, or <see cref="T:System.String" />. </exception>
		/// <filterpriority>1</filterpriority>
		[ComVisible(true)]
		public static bool IsDefined(Type enumType, object value)
		{
			if (enumType == null)
			{
				throw new ArgumentNullException("enumType");
			}
			if (value == null)
			{
				throw new ArgumentNullException("value");
			}
			if (!enumType.IsEnum)
			{
				throw new ArgumentException("enumType is not an Enum type.", "enumType");
			}
			MonoEnumInfo.GetInfo(enumType, out MonoEnumInfo info);
			Type type = value.GetType();
			if (type == typeof(string))
			{
				return ((IList)info.names).Contains(value);
			}
			if (type == info.utype || type == enumType)
			{
				value = ToObject(enumType, value);
				MonoEnumInfo.GetInfo(enumType, out info);
				return FindPosition(value, info.values) >= 0;
			}
			throw new ArgumentException("The value parameter is not the correct type.It must be type String or the same type as the underlying typeof the Enum.");
		}

		[MethodImpl(MethodImplOptions.InternalCall)]
		private static extern Type get_underlying_type(Type enumType);

		/// <summary>Returns the underlying type of the specified enumeration.</summary>
		/// <returns>The underlying <see cref="T:System.Type" /> of <paramref name="enumType" />.</returns>
		/// <param name="enumType">An enumeration type. </param>
		/// <exception cref="T:System.ArgumentNullException">
		///   <paramref name="enumType" /> is null. </exception>
		/// <exception cref="T:System.ArgumentException">
		///   <paramref name="enumType" /> is not an <see cref="T:System.Enum" />. </exception>
		/// <filterpriority>1</filterpriority>
		[ComVisible(true)]
		public static Type GetUnderlyingType(Type enumType)
		{
			if (enumType == null)
			{
				throw new ArgumentNullException("enumType");
			}
			if (!enumType.IsEnum)
			{
				throw new ArgumentException("enumType is not an Enum type.", "enumType");
			}
			return get_underlying_type(enumType);
		}

		/// <summary>Converts the string representation of the name or numeric value of one or more enumerated constants to an equivalent enumerated object.</summary>
		/// <returns>An object of type <paramref name="enumType" /> whose value is represented by <paramref name="value" />.</returns>
		/// <param name="enumType">The <see cref="T:System.Type" /> of the enumeration. </param>
		/// <param name="value">A string containing the name or value to convert. </param>
		/// <exception cref="T:System.ArgumentNullException">
		///   <paramref name="enumType" /> or <paramref name="value" /> is null. </exception>
		/// <exception cref="T:System.ArgumentException">
		///   <paramref name="enumType" /> is not an <see cref="T:System.Enum" />.-or- <paramref name="value" /> is either an empty string or only contains white space.-or- <paramref name="value" /> is a name, but not one of the named constants defined for the enumeration. </exception>
		/// <filterpriority>1</filterpriority>
		[ComVisible(true)]
		public static object Parse(Type enumType, string value)
		{
			return Parse(enumType, value, ignoreCase: false);
		}

		private static int FindName(Hashtable name_hash, string[] names, string name, bool ignoreCase)
		{
			if (!ignoreCase)
			{
				if (name_hash != null)
				{
					object obj = name_hash[name];
					if (obj != null)
					{
						return (int)obj;
					}
				}
				else
				{
					for (int i = 0; i < names.Length; i++)
					{
						if (name == names[i])
						{
							return i;
						}
					}
				}
			}
			else
			{
				for (int j = 0; j < names.Length; j++)
				{
					if (string.Compare(name, names[j], ignoreCase, CultureInfo.InvariantCulture) == 0)
					{
						return j;
					}
				}
			}
			return -1;
		}

		private static ulong GetValue(object value, TypeCode typeCode)
		{
			switch (typeCode)
			{
			case TypeCode.Byte:
				return (byte)value;
			case TypeCode.SByte:
				return (byte)(sbyte)value;
			case TypeCode.Int16:
				return (ushort)(short)value;
			case TypeCode.Int32:
				return (uint)(int)value;
			case TypeCode.Int64:
				return (ulong)(long)value;
			case TypeCode.UInt16:
				return (ushort)value;
			case TypeCode.UInt32:
				return (uint)value;
			case TypeCode.UInt64:
				return (ulong)value;
			default:
				throw new ArgumentException("typeCode is not a valid type code for an Enum");
			}
		}

		/// <summary>Converts the string representation of the name or numeric value of one or more enumerated constants to an equivalent enumerated object. A parameter specifies whether the operation is case-sensitive.</summary>
		/// <returns>An object of type <paramref name="enumType" /> whose value is represented by <paramref name="value" />.</returns>
		/// <param name="enumType">The <see cref="T:System.Type" /> of the enumeration. </param>
		/// <param name="value">A string containing the name or value to convert. </param>
		/// <param name="ignoreCase">If true, ignore case; otherwise, regard case. </param>
		/// <exception cref="T:System.ArgumentNullException">
		///   <paramref name="enumType" /> or <paramref name="value" /> is null. </exception>
		/// <exception cref="T:System.ArgumentException">
		///   <paramref name="enumType" /> is not an <see cref="T:System.Enum" />.-or- <paramref name="value" /> is either an empty string ("") or only contains white space.-or- <paramref name="value" /> is a name, but not one of the named constants defined for the enumeration. </exception>
		/// <filterpriority>1</filterpriority>
		[ComVisible(true)]
		public static object Parse(Type enumType, string value, bool ignoreCase)
		{
			if (enumType == null)
			{
				throw new ArgumentNullException("enumType");
			}
			if (value == null)
			{
				throw new ArgumentNullException("value");
			}
			if (!enumType.IsEnum)
			{
				throw new ArgumentException("enumType is not an Enum type.", "enumType");
			}
			value = value.Trim();
			if (value.Length == 0)
			{
				throw new ArgumentException("An empty string is not considered a valid value.");
			}
			MonoEnumInfo.GetInfo(enumType, out MonoEnumInfo info);
			int num = FindName(info.name_hash, info.names, value, ignoreCase);
			if (num >= 0)
			{
				return info.values.GetValue(num);
			}
			TypeCode typeCode = ((Enum)info.values.GetValue(0)).GetTypeCode();
			if (value.IndexOf(',') != -1)
			{
				string[] array = value.Split(split_char);
				ulong num2 = 0uL;
				for (int i = 0; i < array.Length; i++)
				{
					num = FindName(info.name_hash, info.names, array[i].Trim(), ignoreCase);
					if (num < 0)
					{
						throw new ArgumentException("The requested value was not found.");
					}
					num2 |= GetValue(info.values.GetValue(num), typeCode);
				}
				return ToObject(enumType, num2);
			}
			switch (typeCode)
			{
			case TypeCode.SByte:
			{
				if (sbyte.TryParse(value, out sbyte result8))
				{
					return ToObject(enumType, result8);
				}
				break;
			}
			case TypeCode.Byte:
			{
				if (byte.TryParse(value, out byte result4))
				{
					return ToObject(enumType, result4);
				}
				break;
			}
			case TypeCode.Int16:
			{
				if (short.TryParse(value, out short result6))
				{
					return ToObject(enumType, result6);
				}
				break;
			}
			case TypeCode.UInt16:
			{
				if (ushort.TryParse(value, out ushort result2))
				{
					return ToObject(enumType, result2);
				}
				break;
			}
			case TypeCode.Int32:
			{
				if (int.TryParse(value, out int result7))
				{
					return ToObject(enumType, result7);
				}
				break;
			}
			case TypeCode.UInt32:
			{
				if (uint.TryParse(value, out uint result5))
				{
					return ToObject(enumType, result5);
				}
				break;
			}
			case TypeCode.Int64:
			{
				if (long.TryParse(value, out long result3))
				{
					return ToObject(enumType, result3);
				}
				break;
			}
			case TypeCode.UInt64:
			{
				if (ulong.TryParse(value, out ulong result))
				{
					return ToObject(enumType, result);
				}
				break;
			}
			}
			throw new ArgumentException($"The requested value '{value}' was not found.");
		}

		[MethodImpl(MethodImplOptions.InternalCall)]
		private extern int compare_value_to(object other);

		/// <summary>Compares this instance to a specified object and returns an indication of their relative values.</summary>
		/// <returns>A signed number indicating the relative values of this instance and <paramref name="target" />.Return Value Description Less than zero The value of this instance is less than the value of <paramref name="target" />. Zero The value of this instance is equal to the value of <paramref name="target" />. Greater than zero The value of this instance is greater than the value of <paramref name="target" />.-or- <paramref name="target" /> is null. </returns>
		/// <param name="target">An object to compare, or null. </param>
		/// <exception cref="T:System.ArgumentException">
		///   <paramref name="target" /> and this instance are not the same type. </exception>
		/// <exception cref="T:System.InvalidOperationException">This instance is not type <see cref="T:System.SByte" />, <see cref="T:System.Int16" />, <see cref="T:System.Int32" />, <see cref="T:System.Int64" />, <see cref="T:System.Byte" />, <see cref="T:System.UInt16" />, <see cref="T:System.UInt32" />, or <see cref="T:System.UInt64" />. </exception>
		/// <exception cref="T:System.NullReferenceException">This instance is null.</exception>
		/// <filterpriority>2</filterpriority>
		public int CompareTo(object target)
		{
			if (target == null)
			{
				return 1;
			}
			Type type = GetType();
			if (target.GetType() != type)
			{
				throw new ArgumentException($"Object must be the same type as the enum. The type passed in was {target.GetType()}; the enum type was {type}.");
			}
			return compare_value_to(target);
		}

		/// <summary>Converts the value of this instance to its equivalent string representation.</summary>
		/// <returns>The string representation of the value of this instance.</returns>
		/// <filterpriority>2</filterpriority>
		public override string ToString()
		{
			return ToString("G");
		}

		/// <summary>This method overload is obsolete, use <see cref="M:System.Enum.Parse(System.Type,System.String)" />.</summary>
		/// <returns />
		/// <param name="provider">(obsolete) </param>
		/// <filterpriority>2</filterpriority>
		[Obsolete("Provider is ignored, just use ToString")]
		public string ToString(IFormatProvider provider)
		{
			return ToString("G", provider);
		}

		/// <summary>Converts the value of this instance to its equivalent string representation using the specified format.</summary>
		/// <returns>The string representation of the value of this instance as specified by <paramref name="format" />.</returns>
		/// <param name="format">A format string. </param>
		/// <exception cref="T:System.FormatException">
		///   <paramref name="format" /> contains an invalid specification. </exception>
		/// <filterpriority>2</filterpriority>
		public string ToString(string format)
		{
			if (format == string.Empty || format == null)
			{
				format = "G";
			}
			return Format(GetType(), Value, format);
		}

		/// <summary>This method overload is obsolete, use <see cref="M:System.Enum.Parse(System.Type,System.String)" />.</summary>
		/// <returns />
		/// <param name="format">A format specification. </param>
		/// <param name="provider">(obsolete)</param>
		/// <exception cref="T:System.FormatException">
		///   <paramref name="format" /> does not contain a valid format specification. </exception>
		/// <filterpriority>2</filterpriority>
		[Obsolete("Provider is ignored, just use ToString")]
		public string ToString(string format, IFormatProvider provider)
		{
			if (format == string.Empty || format == null)
			{
				format = "G";
			}
			return Format(GetType(), Value, format);
		}

		/// <summary>Returns an instance of the specified enumeration type set to the specified 8-bit unsigned integer value.</summary>
		/// <returns>An instance of the enumeration set to <paramref name="value" />.</returns>
		/// <param name="enumType">The enumeration for which to create a value. </param>
		/// <param name="value">The value to set. </param>
		/// <exception cref="T:System.ArgumentNullException">
		///   <paramref name="enumType" /> is null. </exception>
		/// <exception cref="T:System.ArgumentException">
		///   <paramref name="enumType" /> is not an <see cref="T:System.Enum" />. </exception>
		/// <filterpriority>1</filterpriority>
		[ComVisible(true)]
		public static object ToObject(Type enumType, byte value)
		{
			return ToObject(enumType, (object)value);
		}

		/// <summary>Returns an instance of the specified enumeration type set to the specified 16-bit signed integer value.</summary>
		/// <returns>An instance of the enumeration set to <paramref name="value" />.</returns>
		/// <param name="enumType">The enumeration for which to create a value. </param>
		/// <param name="value">The value to set. </param>
		/// <exception cref="T:System.ArgumentNullException">
		///   <paramref name="enumType" /> is null. </exception>
		/// <exception cref="T:System.ArgumentException">
		///   <paramref name="enumType" /> is not an <see cref="T:System.Enum" />. </exception>
		/// <filterpriority>1</filterpriority>
		[ComVisible(true)]
		public static object ToObject(Type enumType, short value)
		{
			return ToObject(enumType, (object)value);
		}

		/// <summary>Returns an instance of the specified enumeration type set to the specified 32-bit signed integer value.</summary>
		/// <returns>An instance of the enumeration set to <paramref name="value" />.</returns>
		/// <param name="enumType">The enumeration for which to create a value. </param>
		/// <param name="value">The value to set. </param>
		/// <exception cref="T:System.ArgumentNullException">
		///   <paramref name="enumType" /> is null. </exception>
		/// <exception cref="T:System.ArgumentException">
		///   <paramref name="enumType" /> is not an <see cref="T:System.Enum" />. </exception>
		/// <filterpriority>1</filterpriority>
		[ComVisible(true)]
		public static object ToObject(Type enumType, int value)
		{
			return ToObject(enumType, (object)value);
		}

		/// <summary>Returns an instance of the specified enumeration type set to the specified 64-bit signed integer value.</summary>
		/// <returns>An instance of the enumeration set to <paramref name="value" />.</returns>
		/// <param name="enumType">The enumeration for which to create a value. </param>
		/// <param name="value">The value to set. </param>
		/// <exception cref="T:System.ArgumentNullException">
		///   <paramref name="enumType" /> is null. </exception>
		/// <exception cref="T:System.ArgumentException">
		///   <paramref name="enumType" /> is not an <see cref="T:System.Enum" />. </exception>
		/// <filterpriority>1</filterpriority>
		[ComVisible(true)]
		public static object ToObject(Type enumType, long value)
		{
			return ToObject(enumType, (object)value);
		}

		/// <summary>Returns an instance of the specified enumeration set to the specified value.</summary>
		/// <returns>An enumeration object whose value is <paramref name="value" />.</returns>
		/// <param name="enumType">An enumeration. </param>
		/// <param name="value">The value. </param>
		/// <exception cref="T:System.ArgumentNullException">
		///   <paramref name="enumType" /> is null. </exception>
		/// <exception cref="T:System.ArgumentException">
		///   <paramref name="enumType" /> is not an <see cref="T:System.Enum" />.-or- <paramref name="value" /> is not type <see cref="T:System.SByte" />, <see cref="T:System.Int16" />, <see cref="T:System.Int32" />, <see cref="T:System.Int64" />, <see cref="T:System.Byte" />, <see cref="T:System.UInt16" />, <see cref="T:System.UInt32" />, or <see cref="T:System.UInt64" />. </exception>
		/// <filterpriority>1</filterpriority>
		[MethodImpl(MethodImplOptions.InternalCall)]
		[ComVisible(true)]
		public static extern object ToObject(Type enumType, object value);

		/// <summary>Returns an instance of the specified enumeration type set to the specified 8-bit signed integer value.</summary>
		/// <returns>An instance of the enumeration set to <paramref name="value" />.</returns>
		/// <param name="enumType">The enumeration for which to create a value. </param>
		/// <param name="value">The value to set. </param>
		/// <exception cref="T:System.ArgumentNullException">
		///   <paramref name="enumType" /> is null. </exception>
		/// <exception cref="T:System.ArgumentException">
		///   <paramref name="enumType" /> is not an <see cref="T:System.Enum" />. </exception>
		/// <filterpriority>1</filterpriority>
		[ComVisible(true)]
		[CLSCompliant(false)]
		public static object ToObject(Type enumType, sbyte value)
		{
			return ToObject(enumType, (object)value);
		}

		/// <summary>Returns an instance of the specified enumeration type set to the specified 16-bit unsigned integer value.</summary>
		/// <returns>An instance of the enumeration set to <paramref name="value" />.</returns>
		/// <param name="enumType">The enumeration for which to create a value. </param>
		/// <param name="value">The value to set. </param>
		/// <exception cref="T:System.ArgumentNullException">
		///   <paramref name="enumType" /> is null. </exception>
		/// <exception cref="T:System.ArgumentException">
		///   <paramref name="enumType" /> is not an <see cref="T:System.Enum" />. </exception>
		/// <filterpriority>1</filterpriority>
		[ComVisible(true)]
		[CLSCompliant(false)]
		public static object ToObject(Type enumType, ushort value)
		{
			return ToObject(enumType, (object)value);
		}

		/// <summary>Returns an instance of the specified enumeration type set to the specified 32-bit unsigned integer value.</summary>
		/// <returns>An instance of the enumeration set to <paramref name="value" />.</returns>
		/// <param name="enumType">The enumeration for which to create a value. </param>
		/// <param name="value">The value to set. </param>
		/// <exception cref="T:System.ArgumentNullException">
		///   <paramref name="enumType" /> is null. </exception>
		/// <exception cref="T:System.ArgumentException">
		///   <paramref name="enumType" /> is not an <see cref="T:System.Enum" />. </exception>
		/// <filterpriority>1</filterpriority>
		[ComVisible(true)]
		[CLSCompliant(false)]
		public static object ToObject(Type enumType, uint value)
		{
			return ToObject(enumType, (object)value);
		}

		/// <summary>Returns an instance of the specified enumeration type set to the specified 64-bit unsigned integer value.</summary>
		/// <returns>An instance of the enumeration set to <paramref name="value" />.</returns>
		/// <param name="enumType">The enumeration for which to create a value. </param>
		/// <param name="value">The value to set. </param>
		/// <exception cref="T:System.ArgumentNullException">
		///   <paramref name="enumType" /> is null. </exception>
		/// <exception cref="T:System.ArgumentException">
		///   <paramref name="enumType" /> is not an <see cref="T:System.Enum" />. </exception>
		/// <filterpriority>1</filterpriority>
		[ComVisible(true)]
		[CLSCompliant(false)]
		public static object ToObject(Type enumType, ulong value)
		{
			return ToObject(enumType, (object)value);
		}

		/// <summary>Returns a value indicating whether this instance is equal to a specified object.</summary>
		/// <returns>true if <paramref name="obj" /> is an <see cref="T:System.Enum" /> with the same underlying type and value as this instance; otherwise, false.</returns>
		/// <param name="obj">An object to compare with this instance, or null. </param>
		/// <filterpriority>2</filterpriority>
		public override bool Equals(object obj)
		{
			return ValueType.DefaultEquals(this, obj);
		}

		[MethodImpl(MethodImplOptions.InternalCall)]
		private extern int get_hashcode();

		/// <summary>Returns the hash code for the value of this instance.</summary>
		/// <returns>A 32-bit signed integer hash code.</returns>
		/// <filterpriority>2</filterpriority>
		public override int GetHashCode()
		{
			return get_hashcode();
		}

		private static string FormatSpecifier_X(Type enumType, object value, bool upper)
		{
			switch (Type.GetTypeCode(enumType))
			{
			case TypeCode.SByte:
				return ((sbyte)value).ToString((!upper) ? "x2" : "X2");
			case TypeCode.Byte:
				return ((byte)value).ToString((!upper) ? "x2" : "X2");
			case TypeCode.Int16:
				return ((short)value).ToString((!upper) ? "x4" : "X4");
			case TypeCode.UInt16:
				return ((ushort)value).ToString((!upper) ? "x4" : "X4");
			case TypeCode.Int32:
				return ((int)value).ToString((!upper) ? "x8" : "X8");
			case TypeCode.UInt32:
				return ((uint)value).ToString((!upper) ? "x8" : "X8");
			case TypeCode.Int64:
				return ((long)value).ToString((!upper) ? "x16" : "X16");
			case TypeCode.UInt64:
				return ((ulong)value).ToString((!upper) ? "x16" : "X16");
			default:
				throw new Exception("Invalid type code for enumeration.");
			}
		}

		private static string FormatFlags(Type enumType, object value)
		{
			string text = string.Empty;
			MonoEnumInfo.GetInfo(enumType, out MonoEnumInfo info);
			string text2 = value.ToString();
			if (text2 == "0")
			{
				text = GetName(enumType, value);
				if (text == null)
				{
					text = text2;
				}
				return text;
			}
			switch (((Enum)info.values.GetValue(0)).GetTypeCode())
			{
			case TypeCode.SByte:
			{
				sbyte b = (sbyte)value;
				for (int num7 = info.values.Length - 1; num7 >= 0; num7--)
				{
					sbyte b2 = (sbyte)info.values.GetValue(num7);
					if (b2 != 0 && (b & b2) == b2)
					{
						text = info.names[num7] + ((!(text == string.Empty)) ? ", " : string.Empty) + text;
						b = (sbyte)(b - b2);
					}
				}
				if (b != 0)
				{
					return text2;
				}
				break;
			}
			case TypeCode.Byte:
			{
				byte b3 = (byte)value;
				for (int num8 = info.values.Length - 1; num8 >= 0; num8--)
				{
					byte b4 = (byte)info.values.GetValue(num8);
					if (b4 != 0 && (b3 & b4) == b4)
					{
						text = info.names[num8] + ((!(text == string.Empty)) ? ", " : string.Empty) + text;
						b3 = (byte)(b3 - b4);
					}
				}
				if (b3 != 0)
				{
					return text2;
				}
				break;
			}
			case TypeCode.Int16:
			{
				short num18 = (short)value;
				for (int num19 = info.values.Length - 1; num19 >= 0; num19--)
				{
					short num20 = (short)info.values.GetValue(num19);
					if (num20 != 0 && (num18 & num20) == num20)
					{
						text = info.names[num19] + ((!(text == string.Empty)) ? ", " : string.Empty) + text;
						num18 = (short)(num18 - num20);
					}
				}
				if (num18 != 0)
				{
					return text2;
				}
				break;
			}
			case TypeCode.Int32:
			{
				int num12 = (int)value;
				for (int num13 = info.values.Length - 1; num13 >= 0; num13--)
				{
					int num14 = (int)info.values.GetValue(num13);
					if (num14 != 0 && (num12 & num14) == num14)
					{
						text = info.names[num13] + ((!(text == string.Empty)) ? ", " : string.Empty) + text;
						num12 -= num14;
					}
				}
				if (num12 != 0)
				{
					return text2;
				}
				break;
			}
			case TypeCode.UInt16:
			{
				ushort num15 = (ushort)value;
				for (int num16 = info.values.Length - 1; num16 >= 0; num16--)
				{
					ushort num17 = (ushort)info.values.GetValue(num16);
					if (num17 != 0 && (num15 & num17) == num17)
					{
						text = info.names[num16] + ((!(text == string.Empty)) ? ", " : string.Empty) + text;
						num15 = (ushort)(num15 - num17);
					}
				}
				if (num15 != 0)
				{
					return text2;
				}
				break;
			}
			case TypeCode.UInt32:
			{
				uint num4 = (uint)value;
				for (int num5 = info.values.Length - 1; num5 >= 0; num5--)
				{
					uint num6 = (uint)info.values.GetValue(num5);
					if (num6 != 0 && (num4 & num6) == num6)
					{
						text = info.names[num5] + ((!(text == string.Empty)) ? ", " : string.Empty) + text;
						num4 -= num6;
					}
				}
				if (num4 != 0)
				{
					return text2;
				}
				break;
			}
			case TypeCode.Int64:
			{
				long num9 = (long)value;
				for (int num10 = info.values.Length - 1; num10 >= 0; num10--)
				{
					long num11 = (long)info.values.GetValue(num10);
					if (num11 != 0L && (num9 & num11) == num11)
					{
						text = info.names[num10] + ((!(text == string.Empty)) ? ", " : string.Empty) + text;
						num9 -= num11;
					}
				}
				if (num9 != 0L)
				{
					return text2;
				}
				break;
			}
			case TypeCode.UInt64:
			{
				ulong num = (ulong)value;
				for (int num2 = info.values.Length - 1; num2 >= 0; num2--)
				{
					ulong num3 = (ulong)info.values.GetValue(num2);
					if (num3 != 0L && (num & num3) == num3)
					{
						text = info.names[num2] + ((!(text == string.Empty)) ? ", " : string.Empty) + text;
						num -= num3;
					}
				}
				if (num != 0L)
				{
					return text2;
				}
				break;
			}
			}
			if (text == string.Empty)
			{
				return text2;
			}
			return text;
		}

		/// <summary>Converts the specified value of a specified enumerated type to its equivalent string representation according to the specified format.</summary>
		/// <returns>A string representation of <paramref name="value" />.</returns>
		/// <param name="enumType">The enumeration type of the value to convert. </param>
		/// <param name="value">The value to convert. </param>
		/// <param name="format">The output format to use. </param>
		/// <exception cref="T:System.ArgumentNullException">The <paramref name="enumType" />, <paramref name="value" />, or <paramref name="format" /> parameter is null. </exception>
		/// <exception cref="T:System.ArgumentException">The <paramref name="enumType" /> parameter is not an <see cref="T:System.Enum" /> type.-or- The <paramref name="value" /> is from an enumeration that differs in type from <paramref name="enumType" />.-or- The type of <paramref name="value" /> is not an underlying type of <paramref name="enumType" />. </exception>
		/// <exception cref="T:System.FormatException">The <paramref name="format" /> parameter contains an invalid value. </exception>
		/// <filterpriority>1</filterpriority>
		[ComVisible(true)]
		public static string Format(Type enumType, object value, string format)
		{
			if (enumType == null)
			{
				throw new ArgumentNullException("enumType");
			}
			if (value == null)
			{
				throw new ArgumentNullException("value");
			}
			if (format == null)
			{
				throw new ArgumentNullException("format");
			}
			if (!enumType.IsEnum)
			{
				throw new ArgumentException("enumType is not an Enum type.", "enumType");
			}
			Type type = value.GetType();
			Type underlyingType = GetUnderlyingType(enumType);
			if (type.IsEnum)
			{
				if (type != enumType)
				{
					throw new ArgumentException(string.Format(CultureInfo.InvariantCulture, "Object must be the same type as the enum. The type passed in was {0}; the enum type was {1}.", type.FullName, enumType.FullName));
				}
			}
			else if (type != underlyingType)
			{
				throw new ArgumentException(string.Format(CultureInfo.InvariantCulture, "Enum underlying type and the object must be the same type or object. Type passed in was {0}; the enum underlying type was {1}.", type.FullName, underlyingType.FullName));
			}
			if (format.Length != 1)
			{
				throw new FormatException("Format String can be only \"G\",\"g\",\"X\",\"x\",\"F\",\"f\",\"D\" or \"d\".");
			}
			char c = format[0];
			string text;
			if (c == 'G' || c == 'g')
			{
				if (!enumType.IsDefined(typeof(FlagsAttribute), inherit: false))
				{
					text = GetName(enumType, value);
					if (text == null)
					{
						text = value.ToString();
					}
					return text;
				}
				c = 'f';
			}
			if (c == 'f' || c == 'F')
			{
				return FormatFlags(enumType, value);
			}
			text = string.Empty;
			switch (c)
			{
			case 'X':
				return FormatSpecifier_X(enumType, value, upper: true);
			case 'x':
				return FormatSpecifier_X(enumType, value, upper: false);
			case 'D':
			case 'd':
				if (underlyingType == typeof(ulong))
				{
					return Convert.ToUInt64(value).ToString();
				}
				return Convert.ToInt64(value).ToString();
			default:
				throw new FormatException("Format String can be only \"G\",\"g\",\"X\",\"x\",\"F\",\"f\",\"D\" or \"d\".");
			}
		}
	}
}
