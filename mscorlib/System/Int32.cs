using System.Globalization;
using System.Runtime.InteropServices;
using System.Threading;

namespace System
{
	/// <summary>Represents a 32-bit signed integer.</summary>
	/// <filterpriority>1</filterpriority>
	[Serializable]
	[ComVisible(true)]
	public struct Int32 : IFormattable, IConvertible, IComparable, IComparable<int>, IEquatable<int>
	{
		/// <summary>Represents the largest possible value of an <see cref="T:System.Int32" />. This field is constant.</summary>
		/// <filterpriority>1</filterpriority>
		public const int MaxValue = 2147483647;

		/// <summary>Represents the smallest possible value of <see cref="T:System.Int32" />. This field is constant.</summary>
		/// <filterpriority>1</filterpriority>
		public const int MinValue = -2147483648;

		internal int m_value;

		/// <summary>For a description of this member, see <see cref="M:System.IConvertible.ToBoolean(System.IFormatProvider)" />. </summary>
		/// <returns>true if the value of the current instance is not zero; otherwise, false.</returns>
		/// <param name="provider">This parameter is ignored.</param>
		bool IConvertible.ToBoolean(IFormatProvider provider)
		{
			return Convert.ToBoolean(this);
		}

		/// <summary>For a description of this member, see <see cref="M:System.IConvertible.ToByte(System.IFormatProvider)" />. </summary>
		/// <returns>The value of the current instance, converted to a <see cref="T:System.Byte" />.</returns>
		/// <param name="provider">This parameter is ignored.</param>
		byte IConvertible.ToByte(IFormatProvider provider)
		{
			return Convert.ToByte(this);
		}

		/// <summary>For a description of this member, see <see cref="M:System.IConvertible.ToChar(System.IFormatProvider)" />. </summary>
		/// <returns>The value of the current instance, converted to a <see cref="T:System.Char" />.</returns>
		/// <param name="provider">This parameter is ignored.</param>
		char IConvertible.ToChar(IFormatProvider provider)
		{
			return Convert.ToChar(this);
		}

		/// <summary>This conversion is not supported. Attempting to use this method throws an <see cref="T:System.InvalidCastException" />.</summary>
		/// <returns>This conversion is not supported. No value is returned.</returns>
		/// <param name="provider">This parameter is ignored.</param>
		/// <exception cref="T:System.InvalidCastException">In all cases.</exception>
		DateTime IConvertible.ToDateTime(IFormatProvider provider)
		{
			return Convert.ToDateTime(this);
		}

		/// <summary>For a description of this member, see <see cref="M:System.IConvertible.ToDecimal(System.IFormatProvider)" />.</summary>
		/// <returns>The value of the current instance, converted to a <see cref="T:System.Decimal" />.</returns>
		/// <param name="provider">This parameter is ignored.</param>
		decimal IConvertible.ToDecimal(IFormatProvider provider)
		{
			return Convert.ToDecimal(this);
		}

		/// <summary>For a description of this member, see <see cref="M:System.IConvertible.ToDouble(System.IFormatProvider)" />. </summary>
		/// <returns>The value of the current instance, converted to a <see cref="T:System.Double" />.</returns>
		/// <param name="provider">This parameter is ignored.</param>
		double IConvertible.ToDouble(IFormatProvider provider)
		{
			return Convert.ToDouble(this);
		}

		/// <summary>For a description of this member, see <see cref="M:System.IConvertible.ToInt16(System.IFormatProvider)" />. </summary>
		/// <returns>The value of the current instance, converted to an <see cref="T:System.Int16" />.</returns>
		/// <param name="provider">This parameter is ignored.</param>
		short IConvertible.ToInt16(IFormatProvider provider)
		{
			return Convert.ToInt16(this);
		}

		/// <summary>For a description of this member, see <see cref="M:System.IConvertible.ToInt32(System.IFormatProvider)" />. </summary>
		/// <returns>The value of the current instance, unchanged.</returns>
		/// <param name="provider">This parameter is ignored.</param>
		int IConvertible.ToInt32(IFormatProvider provider)
		{
			return this;
		}

		/// <summary>For a description of this member, see <see cref="M:System.IConvertible.ToInt64(System.IFormatProvider)" />. </summary>
		/// <returns>The value of the current instance, converted to an <see cref="T:System.Int64" />.</returns>
		/// <param name="provider">This parameter is ignored.</param>
		long IConvertible.ToInt64(IFormatProvider provider)
		{
			return Convert.ToInt64(this);
		}

		/// <summary>For a description of this member, see <see cref="M:System.IConvertible.ToSByte(System.IFormatProvider)" />. </summary>
		/// <returns>The value of the current instance, converted to an <see cref="T:System.SByte" />.</returns>
		/// <param name="provider">This parameter is ignored.</param>
		sbyte IConvertible.ToSByte(IFormatProvider provider)
		{
			return Convert.ToSByte(this);
		}

		/// <summary>For a description of this member, see <see cref="M:System.IConvertible.ToSingle(System.IFormatProvider)" />. </summary>
		/// <returns>The value of the current instance, converted to a <see cref="T:System.Single" />.</returns>
		/// <param name="provider">This parameter is ignored.</param>
		float IConvertible.ToSingle(IFormatProvider provider)
		{
			return Convert.ToSingle(this);
		}

		/// <summary>For a description of this member, see <see cref="M:System.IConvertible.ToType(System.Type,System.IFormatProvider)" />. </summary>
		/// <returns>The value of the current instance, converted to <paramref name="type" />.</returns>
		/// <param name="type">The type to which to convert this <see cref="T:System.Int32" /> value.</param>
		/// <param name="provider">An <see cref="T:System.IFormatProvider" /> implementation that provides information about the format of the returned value.</param>
		object IConvertible.ToType(Type targetType, IFormatProvider provider)
		{
			if (targetType == null)
			{
				throw new ArgumentNullException("targetType");
			}
			return Convert.ToType(this, targetType, provider, try_target_to_type: false);
		}

		/// <summary>For a description of this member, see <see cref="M:System.IConvertible.ToUInt16(System.IFormatProvider)" />. </summary>
		/// <returns>The value of the current instance, converted to a <see cref="T:System.UInt16" />.</returns>
		/// <param name="provider">This parameter is ignored.</param>
		ushort IConvertible.ToUInt16(IFormatProvider provider)
		{
			return Convert.ToUInt16(this);
		}

		/// <summary>For a description of this member, see <see cref="M:System.IConvertible.ToUInt32(System.IFormatProvider)" />. </summary>
		/// <returns>The value of the current instance, converted to a <see cref="T:System.UInt32" />.</returns>
		/// <param name="provider">This parameter is ignored.</param>
		uint IConvertible.ToUInt32(IFormatProvider provider)
		{
			return Convert.ToUInt32(this);
		}

		/// <summary>For a description of this member, see <see cref="M:System.IConvertible.ToUInt64(System.IFormatProvider)" />. </summary>
		/// <returns>The value of the current instance, converted to a <see cref="T:System.UInt64" />.</returns>
		/// <param name="provider">This parameter is ignored.</param>
		ulong IConvertible.ToUInt64(IFormatProvider provider)
		{
			return Convert.ToUInt64(this);
		}

		/// <summary>Compares this instance to a specified object and returns an indication of their relative values.</summary>
		/// <returns>A signed number indicating the relative values of this instance and <paramref name="value" />.Return Value Description Less than zero This instance is less than <paramref name="value" />. Zero This instance is equal to <paramref name="value" />. Greater than zero This instance is greater than <paramref name="value" />.-or- <paramref name="value" /> is null. </returns>
		/// <param name="value">An object to compare, or null. </param>
		/// <exception cref="T:System.ArgumentException">
		///   <paramref name="value" /> is not an <see cref="T:System.Int32" />. </exception>
		/// <filterpriority>2</filterpriority>
		public int CompareTo(object value)
		{
			if (value == null)
			{
				return 1;
			}
			if (!(value is int))
			{
				throw new ArgumentException(Locale.GetText("Value is not a System.Int32"));
			}
			int num = (int)value;
			if (this == num)
			{
				return 0;
			}
			if (this > num)
			{
				return 1;
			}
			return -1;
		}

		/// <summary>Returns a value indicating whether this instance is equal to a specified object.</summary>
		/// <returns>true if <paramref name="obj" /> is an instance of <see cref="T:System.Int32" /> and equals the value of this instance; otherwise, false.</returns>
		/// <param name="obj">An object to compare with this instance. </param>
		/// <filterpriority>2</filterpriority>
		public override bool Equals(object obj)
		{
			if (!(obj is int))
			{
				return false;
			}
			return (int)obj == this;
		}

		/// <summary>Returns the hash code for this instance.</summary>
		/// <returns>A 32-bit signed integer hash code.</returns>
		/// <filterpriority>2</filterpriority>
		public override int GetHashCode()
		{
			return this;
		}

		/// <summary>Compares this instance to a specified 32-bit signed integer and returns an indication of their relative values.</summary>
		/// <returns>A signed number indicating the relative values of this instance and <paramref name="value" />.Return Value Description Less than zero This instance is less than <paramref name="value" />. Zero This instance is equal to <paramref name="value" />. Greater than zero This instance is greater than <paramref name="value" />. </returns>
		/// <param name="value">An integer to compare. </param>
		/// <filterpriority>2</filterpriority>
		public int CompareTo(int value)
		{
			if (this == value)
			{
				return 0;
			}
			if (this > value)
			{
				return 1;
			}
			return -1;
		}

		/// <summary>Returns a value indicating whether this instance is equal to a specified <see cref="T:System.Int32" /> value. </summary>
		/// <returns>true if <paramref name="obj" /> has the same value as this instance; otherwise, false.</returns>
		/// <param name="obj">An <see cref="T:System.Int32" /> value to compare to this instance.</param>
		/// <filterpriority>2</filterpriority>
		public bool Equals(int obj)
		{
			return obj == this;
		}

		internal static bool ProcessTrailingWhitespace(bool tryParse, string s, int position, ref Exception exc)
		{
			int length = s.Length;
			for (int i = position; i < length; i++)
			{
				char c = s[i];
				if (c != 0 && !char.IsWhiteSpace(c))
				{
					if (!tryParse)
					{
						exc = GetFormatException();
					}
					return false;
				}
			}
			return true;
		}

		internal static bool Parse(string s, bool tryParse, out int result, out Exception exc)
		{
			int num = 0;
			int num2 = 1;
			bool flag = false;
			result = 0;
			exc = null;
			if (s == null)
			{
				if (!tryParse)
				{
					exc = new ArgumentNullException("s");
				}
				return false;
			}
			int length = s.Length;
			int i;
			for (i = 0; i < length; i++)
			{
				char c = s[i];
				if (!char.IsWhiteSpace(c))
				{
					break;
				}
			}
			if (i == length)
			{
				if (!tryParse)
				{
					exc = GetFormatException();
				}
				return false;
			}
			switch (s[i])
			{
			case '+':
				i++;
				break;
			case '-':
				num2 = -1;
				i++;
				break;
			}
			for (; i < length; i++)
			{
				char c = s[i];
				switch (c)
				{
				case '\0':
					i = length;
					break;
				case '0':
				case '1':
				case '2':
				case '3':
				case '4':
				case '5':
				case '6':
				case '7':
				case '8':
				case '9':
				{
					byte b = (byte)(c - 48);
					if (num <= 214748364)
					{
						if (num != 214748364)
						{
							num = num * 10 + b;
							flag = true;
							break;
						}
						if (b <= 7 || (num2 != 1 && b <= 8))
						{
							num = ((num2 != -1) ? (num * 10 + b) : (num * num2 * 10 - b));
							if (ProcessTrailingWhitespace(tryParse, s, i + 1, ref exc))
							{
								result = num;
								return true;
							}
						}
					}
					if (!tryParse)
					{
						exc = new OverflowException("Value is too large");
					}
					return false;
				}
				default:
					if (!ProcessTrailingWhitespace(tryParse, s, i, ref exc))
					{
						return false;
					}
					break;
				}
			}
			if (!flag)
			{
				if (!tryParse)
				{
					exc = GetFormatException();
				}
				return false;
			}
			if (num2 == -1)
			{
				result = num * num2;
			}
			else
			{
				result = num;
			}
			return true;
		}

		/// <summary>Converts the string representation of a number in a specified culture-specific format to its 32-bit signed integer equivalent.</summary>
		/// <returns>A 32-bit signed integer equivalent to the number specified in <paramref name="s" />.</returns>
		/// <param name="s">A string containing a number to convert. </param>
		/// <param name="provider">An <see cref="T:System.IFormatProvider" /> that supplies culture-specific formatting information about <paramref name="s" />. </param>
		/// <exception cref="T:System.ArgumentNullException">
		///   <paramref name="s" /> is null. </exception>
		/// <exception cref="T:System.FormatException">
		///   <paramref name="s" /> is not of the correct format. </exception>
		/// <exception cref="T:System.OverflowException">
		///   <paramref name="s" /> represents a number less than <see cref="F:System.Int32.MinValue" /> or greater than <see cref="F:System.Int32.MaxValue" />. </exception>
		/// <filterpriority>1</filterpriority>
		public static int Parse(string s, IFormatProvider provider)
		{
			return Parse(s, NumberStyles.Integer, provider);
		}

		/// <summary>Converts the string representation of a number in a specified style to its 32-bit signed integer equivalent.</summary>
		/// <returns>A 32-bit signed integer equivalent to the number specified in <paramref name="s" />.</returns>
		/// <param name="s">A string containing a number to convert. </param>
		/// <param name="style">A bitwise combination of <see cref="T:System.Globalization.NumberStyles" /> values that indicates the permitted format of <paramref name="s" />. A typical value to specify is <see cref="F:System.Globalization.NumberStyles.Integer" />.</param>
		/// <exception cref="T:System.ArgumentNullException">
		///   <paramref name="s" /> is null. </exception>
		/// <exception cref="T:System.ArgumentException">
		///   <paramref name="style" /> is not a <see cref="T:System.Globalization.NumberStyles" /> value. -or-<paramref name="style" /> is not a combination of <see cref="F:System.Globalization.NumberStyles.AllowHexSpecifier" /> and <see cref="F:System.Globalization.NumberStyles.HexNumber" /> values.</exception>
		/// <exception cref="T:System.FormatException">
		///   <paramref name="s" /> is not in a format compliant with <paramref name="style" />. </exception>
		/// <exception cref="T:System.OverflowException">
		///   <paramref name="s" /> represents a number less than <see cref="F:System.Int32.MinValue" /> or greater than <see cref="F:System.Int32.MaxValue" />. </exception>
		/// <filterpriority>1</filterpriority>
		public static int Parse(string s, NumberStyles style)
		{
			return Parse(s, style, null);
		}

		internal static bool CheckStyle(NumberStyles style, bool tryParse, ref Exception exc)
		{
			if ((style & NumberStyles.AllowHexSpecifier) != 0)
			{
				NumberStyles numberStyles = style ^ NumberStyles.AllowHexSpecifier;
				if ((numberStyles & NumberStyles.AllowLeadingWhite) != 0)
				{
					numberStyles ^= NumberStyles.AllowLeadingWhite;
				}
				if ((numberStyles & NumberStyles.AllowTrailingWhite) != 0)
				{
					numberStyles ^= NumberStyles.AllowTrailingWhite;
				}
				if (numberStyles != 0)
				{
					if (!tryParse)
					{
						exc = new ArgumentException("With AllowHexSpecifier only AllowLeadingWhite and AllowTrailingWhite are permitted.");
					}
					return false;
				}
			}
			else if ((uint)style > 511u)
			{
				if (!tryParse)
				{
					exc = new ArgumentException("Not a valid number style");
				}
				return false;
			}
			return true;
		}

		internal static bool JumpOverWhite(ref int pos, string s, bool reportError, bool tryParse, ref Exception exc)
		{
			while (pos < s.Length && char.IsWhiteSpace(s[pos]))
			{
				pos++;
			}
			if (reportError && pos >= s.Length)
			{
				if (!tryParse)
				{
					exc = GetFormatException();
				}
				return false;
			}
			return true;
		}

		internal static void FindSign(ref int pos, string s, NumberFormatInfo nfi, ref bool foundSign, ref bool negative)
		{
			if (pos + nfi.NegativeSign.Length <= s.Length && s.IndexOf(nfi.NegativeSign, pos, nfi.NegativeSign.Length) == pos)
			{
				negative = true;
				foundSign = true;
				pos += nfi.NegativeSign.Length;
			}
			else if (pos + nfi.PositiveSign.Length < s.Length && s.IndexOf(nfi.PositiveSign, pos, nfi.PositiveSign.Length) == pos)
			{
				negative = false;
				pos += nfi.PositiveSign.Length;
				foundSign = true;
			}
		}

		internal static void FindCurrency(ref int pos, string s, NumberFormatInfo nfi, ref bool foundCurrency)
		{
			if (pos + nfi.CurrencySymbol.Length <= s.Length && s.Substring(pos, nfi.CurrencySymbol.Length) == nfi.CurrencySymbol)
			{
				foundCurrency = true;
				pos += nfi.CurrencySymbol.Length;
			}
		}

		internal static bool FindExponent(ref int pos, string s, ref int exponent, bool tryParse, ref Exception exc)
		{
			exponent = 0;
			long num = 0L;
			int num2 = s.IndexOfAny(new char[2]
			{
				'e',
				'E'
			}, pos);
			if (num2 < 0)
			{
				exc = null;
				return false;
			}
			if (++num2 == s.Length)
			{
				exc = ((!tryParse) ? GetFormatException() : null);
				return true;
			}
			if (s[num2] == '-')
			{
				exc = ((!tryParse) ? new OverflowException("Value too large or too small.") : null);
				return true;
			}
			if (s[num2] == '+' && ++num2 == s.Length)
			{
				exc = ((!tryParse) ? GetFormatException() : null);
				return true;
			}
			for (; num2 < s.Length; num2++)
			{
				if (!char.IsDigit(s[num2]))
				{
					exc = ((!tryParse) ? GetFormatException() : null);
					return true;
				}
				checked
				{
					num = num * 10 - (unchecked((int)s[num2]) - 48);
					if (num < int.MinValue || num > int.MaxValue)
					{
						exc = ((!tryParse) ? new OverflowException("Value too large or too small.") : null);
						return true;
					}
				}
			}
			num = -num;
			exc = null;
			exponent = (int)num;
			pos = num2;
			return true;
		}

		internal static bool FindOther(ref int pos, string s, string other)
		{
			if (pos + other.Length <= s.Length && s.Substring(pos, other.Length) == other)
			{
				pos += other.Length;
				return true;
			}
			return false;
		}

		internal static bool ValidDigit(char e, bool allowHex)
		{
			if (allowHex)
			{
				return char.IsDigit(e) || (e >= 'A' && e <= 'F') || (e >= 'a' && e <= 'f');
			}
			return char.IsDigit(e);
		}

		internal static Exception GetFormatException()
		{
			return new FormatException("Input string was not in the correct format");
		}

		internal static bool Parse(string s, NumberStyles style, IFormatProvider fp, bool tryParse, out int result, out Exception exc)
		{
			//Discarded unreachable code: IL_0428
			result = 0;
			exc = null;
			if (s == null)
			{
				if (!tryParse)
				{
					exc = new ArgumentNullException();
				}
				return false;
			}
			if (s.Length == 0)
			{
				if (!tryParse)
				{
					exc = GetFormatException();
				}
				return false;
			}
			NumberFormatInfo numberFormatInfo = null;
			if (fp != null)
			{
				Type typeFromHandle = typeof(NumberFormatInfo);
				numberFormatInfo = (NumberFormatInfo)fp.GetFormat(typeFromHandle);
			}
			if (numberFormatInfo == null)
			{
				numberFormatInfo = Thread.CurrentThread.CurrentCulture.NumberFormat;
			}
			if (!CheckStyle(style, tryParse, ref exc))
			{
				return false;
			}
			bool flag = (style & NumberStyles.AllowCurrencySymbol) != 0;
			bool flag2 = (style & NumberStyles.AllowHexSpecifier) != 0;
			bool flag3 = (style & NumberStyles.AllowThousands) != 0;
			bool flag4 = (style & NumberStyles.AllowDecimalPoint) != 0;
			bool flag5 = (style & NumberStyles.AllowParentheses) != 0;
			bool flag6 = (style & NumberStyles.AllowTrailingSign) != 0;
			bool flag7 = (style & NumberStyles.AllowLeadingSign) != 0;
			bool flag8 = (style & NumberStyles.AllowTrailingWhite) != 0;
			bool flag9 = (style & NumberStyles.AllowLeadingWhite) != 0;
			bool flag10 = (style & NumberStyles.AllowExponent) != 0;
			int pos = 0;
			if (flag9 && !JumpOverWhite(ref pos, s, reportError: true, tryParse, ref exc))
			{
				return false;
			}
			bool flag11 = false;
			bool negative = false;
			bool foundSign = false;
			bool foundCurrency = false;
			if (flag5 && s[pos] == '(')
			{
				flag11 = true;
				foundSign = true;
				negative = true;
				pos++;
				if (flag9 && JumpOverWhite(ref pos, s, reportError: true, tryParse, ref exc))
				{
					return false;
				}
				if (s.Substring(pos, numberFormatInfo.NegativeSign.Length) == numberFormatInfo.NegativeSign)
				{
					if (!tryParse)
					{
						exc = GetFormatException();
					}
					return false;
				}
				if (s.Substring(pos, numberFormatInfo.PositiveSign.Length) == numberFormatInfo.PositiveSign)
				{
					if (!tryParse)
					{
						exc = GetFormatException();
					}
					return false;
				}
			}
			if (flag7 && !foundSign)
			{
				FindSign(ref pos, s, numberFormatInfo, ref foundSign, ref negative);
				if (foundSign)
				{
					if (flag9 && !JumpOverWhite(ref pos, s, reportError: true, tryParse, ref exc))
					{
						return false;
					}
					if (flag)
					{
						FindCurrency(ref pos, s, numberFormatInfo, ref foundCurrency);
						if (foundCurrency && flag9 && !JumpOverWhite(ref pos, s, reportError: true, tryParse, ref exc))
						{
							return false;
						}
					}
				}
			}
			if (flag && !foundCurrency)
			{
				FindCurrency(ref pos, s, numberFormatInfo, ref foundCurrency);
				if (foundCurrency)
				{
					if (flag9 && !JumpOverWhite(ref pos, s, reportError: true, tryParse, ref exc))
					{
						return false;
					}
					if (foundCurrency && !foundSign && flag7)
					{
						FindSign(ref pos, s, numberFormatInfo, ref foundSign, ref negative);
						if (foundSign && flag9 && !JumpOverWhite(ref pos, s, reportError: true, tryParse, ref exc))
						{
							return false;
						}
					}
				}
			}
			int num = 0;
			int num2 = 0;
			bool flag12 = false;
			int exponent = 0;
			do
			{
				if (!ValidDigit(s[pos], flag2))
				{
					if (!flag3 || !FindOther(ref pos, s, numberFormatInfo.NumberGroupSeparator))
					{
						if (flag12 || !flag4 || !FindOther(ref pos, s, numberFormatInfo.NumberDecimalSeparator))
						{
							break;
						}
						flag12 = true;
					}
				}
				else if (flag2)
				{
					num2++;
					char c = s[pos++];
					int num3 = char.IsDigit(c) ? (c - 48) : ((!char.IsLower(c)) ? (c - 65 + 10) : (c - 97 + 10));
					uint num4 = (uint)num;
					if (tryParse)
					{
						if (((int)num4 & -268435456) != 0)
						{
							return false;
						}
						num = (int)(num4 * 16) + num3;
					}
					else
					{
						num = (int)checked(num4 * 16u + (uint)num3);
					}
				}
				else if (flag12)
				{
					num2++;
					if (s[pos++] != '0')
					{
						if (!tryParse)
						{
							exc = new OverflowException("Value too large or too small.");
						}
						return false;
					}
				}
				else
				{
					num2++;
					checked
					{
						try
						{
							num = num * 10 - (unchecked((int)s[checked(pos++)]) - 48);
						}
						catch (OverflowException)
						{
							if (!tryParse)
							{
								exc = new OverflowException("Value too large or too small.");
							}
							return false;
						}
					}
				}
			}
			while (pos < s.Length);
			if (num2 == 0)
			{
				if (!tryParse)
				{
					exc = GetFormatException();
				}
				return false;
			}
			if (flag10 && FindExponent(ref pos, s, ref exponent, tryParse, ref exc) && exc != null)
			{
				return false;
			}
			if (flag6 && !foundSign)
			{
				FindSign(ref pos, s, numberFormatInfo, ref foundSign, ref negative);
				if (foundSign)
				{
					if (flag8 && !JumpOverWhite(ref pos, s, reportError: true, tryParse, ref exc))
					{
						return false;
					}
					if (flag)
					{
						FindCurrency(ref pos, s, numberFormatInfo, ref foundCurrency);
					}
				}
			}
			if (flag && !foundCurrency)
			{
				FindCurrency(ref pos, s, numberFormatInfo, ref foundCurrency);
				if (foundCurrency)
				{
					if (flag8 && !JumpOverWhite(ref pos, s, reportError: true, tryParse, ref exc))
					{
						return false;
					}
					if (!foundSign && flag6)
					{
						FindSign(ref pos, s, numberFormatInfo, ref foundSign, ref negative);
					}
				}
			}
			if (flag8 && pos < s.Length && !JumpOverWhite(ref pos, s, reportError: false, tryParse, ref exc))
			{
				return false;
			}
			if (flag11)
			{
				if (pos >= s.Length || s[pos++] != ')')
				{
					if (!tryParse)
					{
						exc = GetFormatException();
					}
					return false;
				}
				if (flag8 && pos < s.Length && !JumpOverWhite(ref pos, s, reportError: false, tryParse, ref exc))
				{
					return false;
				}
			}
			if (pos < s.Length && s[pos] != 0)
			{
				if (!tryParse)
				{
					exc = GetFormatException();
				}
				return false;
			}
			if (!negative && !flag2)
			{
				if (tryParse)
				{
					long num5 = -num;
					if (num5 < int.MinValue || num5 > int.MaxValue)
					{
						return false;
					}
					num = (int)num5;
				}
				else
				{
					num = checked(-num);
				}
			}
			if (exponent > 0)
			{
				double num6 = Math.Pow(10.0, exponent) * (double)num;
				if (num6 < -2147483648.0 || num6 > 2147483647.0)
				{
					if (!tryParse)
					{
						exc = new OverflowException("Value too large or too small.");
					}
					return false;
				}
				num = (int)num6;
			}
			result = num;
			return true;
		}

		/// <summary>Converts the string representation of a number to its 32-bit signed integer equivalent.</summary>
		/// <returns>A 32-bit signed integer equivalent to the number contained in <paramref name="s" />.</returns>
		/// <param name="s">A string containing a number to convert. </param>
		/// <exception cref="T:System.ArgumentNullException">
		///   <paramref name="s" /> is null. </exception>
		/// <exception cref="T:System.FormatException">
		///   <paramref name="s" /> does not consist solely of an optional negative sign followed by a sequence of digits ranging from 0 to 9. </exception>
		/// <exception cref="T:System.OverflowException">
		///   <paramref name="s" /> represents a number less than <see cref="F:System.Int32.MinValue" /> or greater than <see cref="F:System.Int32.MaxValue" />. </exception>
		/// <filterpriority>1</filterpriority>
		public static int Parse(string s)
		{
			if (!Parse(s, tryParse: false, out int result, out Exception exc))
			{
				throw exc;
			}
			return result;
		}

		/// <summary>Converts the string representation of a number in a specified style and culture-specific format to its 32-bit signed integer equivalent.</summary>
		/// <returns>A 32-bit signed integer equivalent to the number specified in <paramref name="s" />.</returns>
		/// <param name="s">A string containing a number to convert. </param>
		/// <param name="style">A bitwise combination of <see cref="T:System.Globalization.NumberStyles" /> values that indicates the permitted format of <paramref name="s" />. A typical value to specify is <see cref="F:System.Globalization.NumberStyles.Integer" />.</param>
		/// <param name="provider">An <see cref="T:System.IFormatProvider" /> that supplies culture-specific formatting information about <paramref name="s" />. </param>
		/// <exception cref="T:System.ArgumentNullException">
		///   <paramref name="s" /> is null. </exception>
		/// <exception cref="T:System.ArgumentException">
		///   <paramref name="style" /> is not a <see cref="T:System.Globalization.NumberStyles" /> value. -or-<paramref name="style" /> is not a combination of <see cref="F:System.Globalization.NumberStyles.AllowHexSpecifier" /> and <see cref="F:System.Globalization.NumberStyles.HexNumber" /> values.</exception>
		/// <exception cref="T:System.FormatException">
		///   <paramref name="s" /> is not in a format compliant with <paramref name="style" />. </exception>
		/// <exception cref="T:System.OverflowException">
		///   <paramref name="s" /> represents a number less than <see cref="F:System.Int32.MinValue" /> or greater than <see cref="F:System.Int32.MaxValue" />. </exception>
		/// <filterpriority>1</filterpriority>
		public static int Parse(string s, NumberStyles style, IFormatProvider provider)
		{
			if (!Parse(s, style, provider, tryParse: false, out int result, out Exception exc))
			{
				throw exc;
			}
			return result;
		}

		/// <summary>Converts the string representation of a number to its 32-bit signed integer equivalent. A return value indicates whether the operation succeeded.</summary>
		/// <returns>true if <paramref name="s" /> was converted successfully; otherwise, false.</returns>
		/// <param name="s">A string containing a number to convert. </param>
		/// <param name="result">When this method returns, contains the 32-bit signed integer value equivalent to the number contained in <paramref name="s" />, if the conversion succeeded, or zero if the conversion failed. The conversion fails if the <paramref name="s" /> parameter is null, is not of the correct format, or represents a number less than <see cref="F:System.Int32.MinValue" /> or greater than <see cref="F:System.Int32.MaxValue" />. This parameter is passed uninitialized. </param>
		/// <filterpriority>1</filterpriority>
		public static bool TryParse(string s, out int result)
		{
			if (!Parse(s, tryParse: true, out result, out Exception _))
			{
				result = 0;
				return false;
			}
			return true;
		}

		/// <summary>Converts the string representation of a number in a specified style and culture-specific format to its 32-bit signed integer equivalent. A return value indicates whether the operation succeeded.</summary>
		/// <returns>true if <paramref name="s" /> was converted successfully; otherwise, false.</returns>
		/// <param name="s">A string containing a number to convert. </param>
		/// <param name="style">A bitwise combination of <see cref="T:System.Globalization.NumberStyles" /> values that indicates the permitted format of <paramref name="s" />. A typical value to specify is <see cref="F:System.Globalization.NumberStyles.Integer" />.</param>
		/// <param name="provider">An <see cref="T:System.IFormatProvider" /> object that supplies culture-specific formatting information about <paramref name="s" />. </param>
		/// <param name="result">When this method returns, contains the 32-bit signed integer value equivalent to the number contained in <paramref name="s" />, if the conversion succeeded, or zero if the conversion failed. The conversion fails if the <paramref name="s" /> parameter is null, is not in a format compliant with <paramref name="style" />, or represents a number less than <see cref="F:System.Int32.MinValue" /> or greater than <see cref="F:System.Int32.MaxValue" />. This parameter is passed uninitialized. </param>
		/// <exception cref="T:System.ArgumentException">
		///   <paramref name="style" /> is not a <see cref="T:System.Globalization.NumberStyles" /> value. -or-<paramref name="style" /> is not a combination of <see cref="F:System.Globalization.NumberStyles.AllowHexSpecifier" /> and <see cref="F:System.Globalization.NumberStyles.HexNumber" /> values.</exception>
		/// <filterpriority>1</filterpriority>
		public static bool TryParse(string s, NumberStyles style, IFormatProvider provider, out int result)
		{
			if (!Parse(s, style, provider, tryParse: true, out result, out Exception _))
			{
				result = 0;
				return false;
			}
			return true;
		}

		/// <summary>Converts the numeric value of this instance to its equivalent string representation.</summary>
		/// <returns>The string representation of the value of this instance, consisting of a negative sign if the value is negative, and a sequence of digits ranging from 0 to 9 with no leading zeroes.</returns>
		/// <filterpriority>1</filterpriority>
		public override string ToString()
		{
			return NumberFormatter.NumberToString(this, null);
		}

		/// <summary>Converts the numeric value of this instance to its equivalent string representation using the specified culture-specific format information.</summary>
		/// <returns>The string representation of the value of this instance as specified by <paramref name="provider" />.</returns>
		/// <param name="provider">An <see cref="T:System.IFormatProvider" /> that supplies culture-specific formatting information. </param>
		/// <filterpriority>1</filterpriority>
		public string ToString(IFormatProvider provider)
		{
			return NumberFormatter.NumberToString(this, provider);
		}

		/// <summary>Converts the numeric value of this instance to its equivalent string representation, using the specified format.</summary>
		/// <returns>The string representation of the value of this instance as specified by <paramref name="format" />.</returns>
		/// <param name="format">A numeric format string.</param>
		/// <filterpriority>1</filterpriority>
		public string ToString(string format)
		{
			return ToString(format, null);
		}

		/// <summary>Converts the numeric value of this instance to its equivalent string representation using the specified format and culture-specific format information.</summary>
		/// <returns>The string representation of the value of this instance as specified by <paramref name="format" /> and <paramref name="provider" />.</returns>
		/// <param name="format">A numeric format string.</param>
		/// <param name="provider">An <see cref="T:System.IFormatProvider" /> that supplies culture-specific formatting information. </param>
		/// <filterpriority>1</filterpriority>
		public string ToString(string format, IFormatProvider provider)
		{
			return NumberFormatter.NumberToString(format, this, provider);
		}

		/// <summary>Returns the <see cref="T:System.TypeCode" /> for value type <see cref="T:System.Int32" />.</summary>
		/// <returns>The enumerated constant, <see cref="F:System.TypeCode.Int32" />.</returns>
		/// <filterpriority>2</filterpriority>
		public TypeCode GetTypeCode()
		{
			return TypeCode.Int32;
		}
	}
}
