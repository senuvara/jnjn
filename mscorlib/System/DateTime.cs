using System.Collections;
using System.Collections.Generic;
using System.Globalization;
using System.Runtime.CompilerServices;
using System.Runtime.InteropServices;

namespace System
{
	/// <summary>Represents an instant in time, typically expressed as a date and time of day. </summary>
	/// <filterpriority>1</filterpriority>
	[Serializable]
	[StructLayout(LayoutKind.Auto)]
	public struct DateTime : IFormattable, IConvertible, IComparable, IComparable<DateTime>, IEquatable<DateTime>
	{
		private enum Which
		{
			Day,
			DayYear,
			Month,
			Year
		}

		private const int dp400 = 146097;

		private const int dp100 = 36524;

		private const int dp4 = 1461;

		private const long w32file_epoch = 504911232000000000L;

		private const long MAX_VALUE_TICKS = 3155378975999999999L;

		internal const long UnixEpoch = 621355968000000000L;

		private const long ticks18991230 = 599264352000000000L;

		private const double OAMinValue = -657435.0;

		private const double OAMaxValue = 2958466.0;

		private const string formatExceptionMessage = "String was not recognized as a valid DateTime.";

		private TimeSpan ticks;

		private DateTimeKind kind;

		/// <summary>Represents the largest possible value of <see cref="T:System.DateTime" />. This field is read-only.</summary>
		/// <filterpriority>1</filterpriority>
		public static readonly DateTime MaxValue;

		/// <summary>Represents the smallest possible value of <see cref="T:System.DateTime" />. This field is read-only.</summary>
		/// <filterpriority>1</filterpriority>
		public static readonly DateTime MinValue;

		private static readonly string[] ParseTimeFormats;

		private static readonly string[] ParseYearDayMonthFormats;

		private static readonly string[] ParseYearMonthDayFormats;

		private static readonly string[] ParseDayMonthYearFormats;

		private static readonly string[] ParseMonthDayYearFormats;

		private static readonly string[] MonthDayShortFormats;

		private static readonly string[] DayMonthShortFormats;

		private static readonly int[] daysmonth;

		private static readonly int[] daysmonthleap;

		private static object to_local_time_span_object;

		private static long last_now;

		/// <summary>Gets the date component of this instance.</summary>
		/// <returns>A new <see cref="T:System.DateTime" /> with the same date as this instance, and the time value set to 12:00:00 midnight (00:00:00).</returns>
		/// <filterpriority>1</filterpriority>
		public DateTime Date
		{
			get
			{
				DateTime result = new DateTime(Year, Month, Day);
				result.kind = kind;
				return result;
			}
		}

		/// <summary>Gets the month component of the date represented by this instance.</summary>
		/// <returns>The month component, expressed as a value between 1 and 12.</returns>
		/// <filterpriority>1</filterpriority>
		public int Month => FromTicks(Which.Month);

		/// <summary>Gets the day of the month represented by this instance.</summary>
		/// <returns>The day component, expressed as a value between 1 and 31.</returns>
		/// <filterpriority>1</filterpriority>
		public int Day => FromTicks(Which.Day);

		/// <summary>Gets the day of the week represented by this instance.</summary>
		/// <returns>A <see cref="T:System.DayOfWeek" /> enumerated constant that indicates the day of the week. This property value ranges from zero, indicating Sunday, to six, indicating Saturday.</returns>
		/// <filterpriority>1</filterpriority>
		public DayOfWeek DayOfWeek => (DayOfWeek)((ticks.Days + 1) % 7);

		/// <summary>Gets the day of the year represented by this instance.</summary>
		/// <returns>The day of the year, expressed as a value between 1 and 366.</returns>
		/// <filterpriority>1</filterpriority>
		public int DayOfYear => FromTicks(Which.DayYear);

		/// <summary>Gets the time of day for this instance.</summary>
		/// <returns>A <see cref="T:System.TimeSpan" /> that represents the fraction of the day that has elapsed since midnight.</returns>
		/// <filterpriority>1</filterpriority>
		public TimeSpan TimeOfDay => new TimeSpan(ticks.Ticks % 864000000000L);

		/// <summary>Gets the hour component of the date represented by this instance.</summary>
		/// <returns>The hour component, expressed as a value between 0 and 23.</returns>
		/// <filterpriority>1</filterpriority>
		public int Hour => ticks.Hours;

		/// <summary>Gets the minute component of the date represented by this instance.</summary>
		/// <returns>The minute component, expressed as a value between 0 and 59.</returns>
		/// <filterpriority>1</filterpriority>
		public int Minute => ticks.Minutes;

		/// <summary>Gets the seconds component of the date represented by this instance.</summary>
		/// <returns>The seconds, between 0 and 59.</returns>
		/// <filterpriority>1</filterpriority>
		public int Second => ticks.Seconds;

		/// <summary>Gets the milliseconds component of the date represented by this instance.</summary>
		/// <returns>The milliseconds component, expressed as a value between 0 and 999.</returns>
		/// <filterpriority>1</filterpriority>
		public int Millisecond => ticks.Milliseconds;

		/// <summary>Gets a <see cref="T:System.DateTime" /> object that is set to the current date and time on this computer, expressed as the local time.</summary>
		/// <returns>A <see cref="T:System.DateTime" /> whose value is the current local date and time.</returns>
		/// <filterpriority>1</filterpriority>
		public static DateTime Now
		{
			get
			{
				long now = GetNow();
				DateTime dateTime = new DateTime(now);
				if (now - last_now > 600000000)
				{
					to_local_time_span_object = TimeZone.CurrentTimeZone.GetLocalTimeDiff(dateTime);
					last_now = now;
				}
				DateTime result = dateTime + (TimeSpan)to_local_time_span_object;
				result.kind = DateTimeKind.Local;
				return result;
			}
		}

		/// <summary>Gets the number of ticks that represent the date and time of this instance.</summary>
		/// <returns>The number of ticks that represent the date and time of this instance. The value is between <see cref="F:System.DateTime.MinValue" /> and <see cref="F:System.DateTime.MaxValue" />.</returns>
		/// <filterpriority>1</filterpriority>
		public long Ticks => ticks.Ticks;

		/// <summary>Gets the current date.</summary>
		/// <returns>A <see cref="T:System.DateTime" /> set to today's date, with the time component set to 00:00:00.</returns>
		/// <filterpriority>1</filterpriority>
		public static DateTime Today
		{
			get
			{
				DateTime now = Now;
				DateTime result = new DateTime(now.Year, now.Month, now.Day);
				result.kind = now.kind;
				return result;
			}
		}

		/// <summary>Gets a <see cref="T:System.DateTime" /> object that is set to the current date and time on this computer, expressed as the Coordinated Universal Time (UTC).</summary>
		/// <returns>A <see cref="T:System.DateTime" /> whose value is the current UTC date and time.</returns>
		/// <filterpriority>1</filterpriority>
		public static DateTime UtcNow => new DateTime(GetNow(), DateTimeKind.Utc);

		/// <summary>Gets the year component of the date represented by this instance.</summary>
		/// <returns>The year, between 1 and 9999.</returns>
		/// <filterpriority>1</filterpriority>
		public int Year => FromTicks(Which.Year);

		/// <summary>Gets a value that indicates whether the time represented by this instance is based on local time, Coordinated Universal Time (UTC), or neither.</summary>
		/// <returns>One of the <see cref="T:System.DateTimeKind" /> values. The default is <see cref="F:System.DateTimeKind.Unspecified" />.</returns>
		/// <filterpriority>1</filterpriority>
		public DateTimeKind Kind => kind;

		/// <summary>Initializes a new instance of the <see cref="T:System.DateTime" /> structure to a specified number of ticks.</summary>
		/// <param name="ticks">A date and time expressed in 100-nanosecond units. </param>
		/// <exception cref="T:System.ArgumentOutOfRangeException">
		///   <paramref name="ticks" /> is less than <see cref="F:System.DateTime.MinValue" /> or greater than <see cref="F:System.DateTime.MaxValue" />. </exception>
		public DateTime(long ticks)
		{
			this.ticks = new TimeSpan(ticks);
			if (ticks < MinValue.Ticks || ticks > MaxValue.Ticks)
			{
				string text = Locale.GetText("Value {0} is outside the valid range [{1},{2}].", ticks, MinValue.Ticks, MaxValue.Ticks);
				throw new ArgumentOutOfRangeException("ticks", text);
			}
			kind = DateTimeKind.Unspecified;
		}

		/// <summary>Initializes a new instance of the <see cref="T:System.DateTime" /> structure to the specified year, month, and day.</summary>
		/// <param name="year">The year (1 through 9999). </param>
		/// <param name="month">The month (1 through 12). </param>
		/// <param name="day">The day (1 through the number of days in <paramref name="month" />). </param>
		/// <exception cref="T:System.ArgumentOutOfRangeException">
		///   <paramref name="year" /> is less than 1 or greater than 9999.-or- <paramref name="month" /> is less than 1 or greater than 12.-or- <paramref name="day" /> is less than 1 or greater than the number of days in <paramref name="month" />. </exception>
		/// <exception cref="T:System.ArgumentException">The specified parameters evaluate to less than <see cref="F:System.DateTime.MinValue" /> or more than <see cref="F:System.DateTime.MaxValue" />. </exception>
		public DateTime(int year, int month, int day)
			: this(year, month, day, 0, 0, 0, 0)
		{
		}

		/// <summary>Initializes a new instance of the <see cref="T:System.DateTime" /> structure to the specified year, month, day, hour, minute, and second.</summary>
		/// <param name="year">The year (1 through 9999). </param>
		/// <param name="month">The month (1 through 12). </param>
		/// <param name="day">The day (1 through the number of days in <paramref name="month" />). </param>
		/// <param name="hour">The hours (0 through 23). </param>
		/// <param name="minute">The minutes (0 through 59). </param>
		/// <param name="second">The seconds (0 through 59). </param>
		/// <exception cref="T:System.ArgumentOutOfRangeException">
		///   <paramref name="year" /> is less than 1 or greater than 9999. -or- <paramref name="month" /> is less than 1 or greater than 12. -or- <paramref name="day" /> is less than 1 or greater than the number of days in <paramref name="month" />.-or- <paramref name="hour" /> is less than 0 or greater than 23. -or- <paramref name="minute" /> is less than 0 or greater than 59. -or- <paramref name="second" /> is less than 0 or greater than 59. </exception>
		/// <exception cref="T:System.ArgumentException">Specified parameters evaluate to less than <see cref="F:System.DateTime.MinValue" /> or more than <see cref="F:System.DateTime.MaxValue" />. </exception>
		public DateTime(int year, int month, int day, int hour, int minute, int second)
			: this(year, month, day, hour, minute, second, 0)
		{
		}

		/// <summary>Initializes a new instance of the <see cref="T:System.DateTime" /> structure to the specified year, month, day, hour, minute, second, and millisecond.</summary>
		/// <param name="year">The year (1 through 9999). </param>
		/// <param name="month">The month (1 through 12). </param>
		/// <param name="day">The day (1 through the number of days in <paramref name="month" />). </param>
		/// <param name="hour">The hours (0 through 23). </param>
		/// <param name="minute">The minutes (0 through 59). </param>
		/// <param name="second">The seconds (0 through 59). </param>
		/// <param name="millisecond">The milliseconds (0 through 999). </param>
		/// <exception cref="T:System.ArgumentOutOfRangeException">
		///   <paramref name="year" /> is less than 1 or greater than 9999.-or- <paramref name="month" /> is less than 1 or greater than 12.-or- <paramref name="day" /> is less than 1 or greater than the number of days in <paramref name="month" />.-or- <paramref name="hour" /> is less than 0 or greater than 23.-or- <paramref name="minute" /> is less than 0 or greater than 59.-or- <paramref name="second" /> is less than 0 or greater than 59.-or- <paramref name="millisecond" /> is less than 0 or greater than 999. </exception>
		/// <exception cref="T:System.ArgumentException">Specified parameters evaluate to less than <see cref="F:System.DateTime.MinValue" /> or more than <see cref="F:System.DateTime.MaxValue" />. </exception>
		public DateTime(int year, int month, int day, int hour, int minute, int second, int millisecond)
		{
			if (year < 1 || year > 9999 || month < 1 || month > 12 || day < 1 || day > DaysInMonth(year, month) || hour < 0 || hour > 23 || minute < 0 || minute > 59 || second < 0 || second > 59 || millisecond < 0 || millisecond > 999)
			{
				throw new ArgumentOutOfRangeException("Parameters describe an unrepresentable DateTime.");
			}
			ticks = new TimeSpan(AbsoluteDays(year, month, day), hour, minute, second, millisecond);
			kind = DateTimeKind.Unspecified;
		}

		/// <summary>Initializes a new instance of the <see cref="T:System.DateTime" /> structure to the specified year, month, and day for the specified calendar.</summary>
		/// <param name="year">The year (1 through the number of years in <paramref name="calendar" />). </param>
		/// <param name="month">The month (1 through the number of months in <paramref name="calendar" />). </param>
		/// <param name="day">The day (1 through the number of days in <paramref name="month" />). </param>
		/// <param name="calendar">The <see cref="T:System.Globalization.Calendar" /> that applies to this <see cref="T:System.DateTime" />. </param>
		/// <exception cref="T:System.ArgumentNullException">
		///   <paramref name="calendar" /> is null.</exception>
		/// <exception cref="T:System.ArgumentOutOfRangeException">
		///   <paramref name="year" /> is not in the range supported by <paramref name="calendar" />.-or- <paramref name="month" /> is less than 1 or greater than the number of months in <paramref name="calendar" />.-or- <paramref name="day" /> is less than 1 or greater than the number of days in <paramref name="month" />. </exception>
		/// <exception cref="T:System.ArgumentException">Specified parameters evaluate to less than <see cref="F:System.DateTime.MinValue" /> or more than <see cref="F:System.DateTime.MaxValue" />. </exception>
		public DateTime(int year, int month, int day, Calendar calendar)
			: this(year, month, day, 0, 0, 0, 0, calendar)
		{
		}

		/// <summary>Initializes a new instance of the <see cref="T:System.DateTime" /> structure to the specified year, month, day, hour, minute, and second for the specified calendar.</summary>
		/// <param name="year">The year (1 through the number of years in <paramref name="calendar" />). </param>
		/// <param name="month">The month (1 through the number of months in <paramref name="calendar" />). </param>
		/// <param name="day">The day (1 through the number of days in <paramref name="month" />). </param>
		/// <param name="hour">The hours (0 through 23). </param>
		/// <param name="minute">The minutes (0 through 59). </param>
		/// <param name="second">The seconds (0 through 59). </param>
		/// <param name="calendar">The <see cref="T:System.Globalization.Calendar" /> that applies to this <see cref="T:System.DateTime" />. </param>
		/// <exception cref="T:System.ArgumentNullException">
		///   <paramref name="calendar" /> is null. </exception>
		/// <exception cref="T:System.ArgumentOutOfRangeException">
		///   <paramref name="year" /> is not in the range supported by <paramref name="calendar" />.-or- <paramref name="month" /> is less than 1 or greater than the number of months in <paramref name="calendar" />.-or- <paramref name="day" /> is less than 1 or greater than the number of days in <paramref name="month" />.-or- <paramref name="hour" /> is less than 0 or greater than 23 -or- <paramref name="minute" /> is less than 0 or greater than 59. -or- <paramref name="second" /> is less than 0 or greater than 59. </exception>
		/// <exception cref="T:System.ArgumentException">Specified parameters evaluate to less than <see cref="F:System.DateTime.MinValue" /> or more than <see cref="F:System.DateTime.MaxValue" />. </exception>
		public DateTime(int year, int month, int day, int hour, int minute, int second, Calendar calendar)
			: this(year, month, day, hour, minute, second, 0, calendar)
		{
		}

		/// <summary>Initializes a new instance of the <see cref="T:System.DateTime" /> structure to the specified year, month, day, hour, minute, second, and millisecond for the specified calendar.</summary>
		/// <param name="year">The year (1 through the number of years in <paramref name="calendar" />). </param>
		/// <param name="month">The month (1 through the number of months in <paramref name="calendar" />). </param>
		/// <param name="day">The day (1 through the number of days in <paramref name="month" />). </param>
		/// <param name="hour">The hours (0 through 23). </param>
		/// <param name="minute">The minutes (0 through 59). </param>
		/// <param name="second">The seconds (0 through 59). </param>
		/// <param name="millisecond">The milliseconds (0 through 999). </param>
		/// <param name="calendar">The <see cref="T:System.Globalization.Calendar" /> that applies to this <see cref="T:System.DateTime" />. </param>
		/// <exception cref="T:System.ArgumentNullException">
		///   <paramref name="calendar" /> is null. </exception>
		/// <exception cref="T:System.ArgumentOutOfRangeException">
		///   <paramref name="year" /> is not in the range supported by <paramref name="calendar" />.-or- <paramref name="month" /> is less than 1 or greater than the number of months in <paramref name="calendar" />.-or- <paramref name="day" /> is less than 1 or greater than the number of days in <paramref name="month" />.-or- <paramref name="hour" /> is less than 0 or greater than 23.-or- <paramref name="minute" /> is less than 0 or greater than 59.-or- <paramref name="second" /> is less than 0 or greater than 59.-or- <paramref name="millisecond" /> is less than 0 or greater than 999. </exception>
		/// <exception cref="T:System.ArgumentException">Specified parameters evaluate to less than <see cref="F:System.DateTime.MinValue" /> or more than <see cref="F:System.DateTime.MaxValue" />. </exception>
		public DateTime(int year, int month, int day, int hour, int minute, int second, int millisecond, Calendar calendar)
		{
			if (calendar == null)
			{
				throw new ArgumentNullException("calendar");
			}
			DateTime dateTime = calendar.ToDateTime(year, month, day, hour, minute, second, millisecond);
			ticks = dateTime.ticks;
			kind = DateTimeKind.Unspecified;
		}

		internal DateTime(bool check, TimeSpan value)
		{
			if (check && (value.Ticks < MinValue.Ticks || value.Ticks > MaxValue.Ticks))
			{
				throw new ArgumentOutOfRangeException();
			}
			ticks = value;
			kind = DateTimeKind.Unspecified;
		}

		/// <summary>Initializes a new instance of the <see cref="T:System.DateTime" /> structure to a specified number of ticks and to Coordinated Universal Time (UTC) or local time.</summary>
		/// <param name="ticks">A date and time expressed in 100-nanosecond units. </param>
		/// <param name="kind">One of the <see cref="T:System.DateTimeKind" /> values that indicates whether <paramref name="ticks" /> specifies a local time, Coordinated Universal Time (UTC), or neither.</param>
		/// <exception cref="T:System.ArgumentOutOfRangeException">
		///   <paramref name="ticks" /> is less than <see cref="F:System.DateTime.MinValue" /> or greater than <see cref="F:System.DateTime.MaxValue" />. </exception>
		/// <exception cref="T:System.ArgumentException">
		///   <paramref name="kind" /> is not one of the <see cref="T:System.DateTimeKind" /> values.</exception>
		public DateTime(long ticks, DateTimeKind kind)
			: this(ticks)
		{
			CheckDateTimeKind(kind);
			this.kind = kind;
		}

		/// <summary>Initializes a new instance of the <see cref="T:System.DateTime" /> structure to the specified year, month, day, hour, minute, second, and Coordinated Universal Time (UTC) or local time.</summary>
		/// <param name="year">The year (1 through 9999). </param>
		/// <param name="month">The month (1 through 12). </param>
		/// <param name="day">The day (1 through the number of days in <paramref name="month" />). </param>
		/// <param name="hour">The hours (0 through 23). </param>
		/// <param name="minute">The minutes (0 through 59). </param>
		/// <param name="second">The seconds (0 through 59). </param>
		/// <param name="kind">One of the <see cref="T:System.DateTimeKind" /> values that indicates whether <paramref name="year" />, <paramref name="month" />, <paramref name="day" />, <paramref name="hour" />, <paramref name="minute" /> and <paramref name="second" /> specify a local time, Coordinated Universal Time (UTC), or neither.</param>
		/// <exception cref="T:System.ArgumentOutOfRangeException">
		///   <paramref name="year" /> is less than 1 or greater than 9999. -or- <paramref name="month" /> is less than 1 or greater than 12. -or- <paramref name="day" /> is less than 1 or greater than the number of days in <paramref name="month" />.-or- <paramref name="hour" /> is less than 0 or greater than 23. -or- <paramref name="minute" /> is less than 0 or greater than 59. -or- <paramref name="second" /> is less than 0 or greater than 59. </exception>
		/// <exception cref="T:System.ArgumentException">The specified time parameters evaluate to less than <see cref="F:System.DateTime.MinValue" /> or more than <see cref="F:System.DateTime.MaxValue" />. -or-<paramref name="kind" /> is not one of the <see cref="T:System.DateTimeKind" /> values.</exception>
		public DateTime(int year, int month, int day, int hour, int minute, int second, DateTimeKind kind)
			: this(year, month, day, hour, minute, second)
		{
			CheckDateTimeKind(kind);
			this.kind = kind;
		}

		/// <summary>Initializes a new instance of the <see cref="T:System.DateTime" /> structure to the specified year, month, day, hour, minute, second, millisecond, and Coordinated Universal Time (UTC) or local time.</summary>
		/// <param name="year">The year (1 through 9999). </param>
		/// <param name="month">The month (1 through 12). </param>
		/// <param name="day">The day (1 through the number of days in <paramref name="month" />). </param>
		/// <param name="hour">The hours (0 through 23). </param>
		/// <param name="minute">The minutes (0 through 59). </param>
		/// <param name="second">The seconds (0 through 59). </param>
		/// <param name="millisecond">The milliseconds (0 through 999). </param>
		/// <param name="kind">One of the <see cref="T:System.DateTimeKind" /> values that indicates whether <paramref name="year" />, <paramref name="month" />, <paramref name="day" />, <paramref name="hour" />, <paramref name="minute" />, <paramref name="second" />, and <paramref name="millisecond" /> specify a local time, Coordinated Universal Time (UTC), or neither.</param>
		/// <exception cref="T:System.ArgumentOutOfRangeException">
		///   <paramref name="year" /> is less than 1 or greater than 9999.-or- <paramref name="month" /> is less than 1 or greater than 12.-or- <paramref name="day" /> is less than 1 or greater than the number of days in <paramref name="month" />.-or- <paramref name="hour" /> is less than 0 or greater than 23.-or- <paramref name="minute" /> is less than 0 or greater than 59.-or- <paramref name="second" /> is less than 0 or greater than 59.-or- <paramref name="millisecond" /> is less than 0 or greater than 999. </exception>
		/// <exception cref="T:System.ArgumentException">The specified time parameters evaluate to less than <see cref="F:System.DateTime.MinValue" /> or more than <see cref="F:System.DateTime.MaxValue" />. -or-<paramref name="kind" /> is not one of the <see cref="T:System.DateTimeKind" /> values.</exception>
		public DateTime(int year, int month, int day, int hour, int minute, int second, int millisecond, DateTimeKind kind)
			: this(year, month, day, hour, minute, second, millisecond)
		{
			CheckDateTimeKind(kind);
			this.kind = kind;
		}

		/// <summary>Initializes a new instance of the <see cref="T:System.DateTime" /> structure to the specified year, month, day, hour, minute, second, millisecond, and Coordinated Universal Time (UTC) or local time for the specified calendar.</summary>
		/// <param name="year">The year (1 through the number of years in <paramref name="calendar" />). </param>
		/// <param name="month">The month (1 through the number of months in <paramref name="calendar" />). </param>
		/// <param name="day">The day (1 through the number of days in <paramref name="month" />). </param>
		/// <param name="hour">The hours (0 through 23). </param>
		/// <param name="minute">The minutes (0 through 59). </param>
		/// <param name="second">The seconds (0 through 59). </param>
		/// <param name="millisecond">The milliseconds (0 through 999). </param>
		/// <param name="calendar">The <see cref="T:System.Globalization.Calendar" /> object that applies to this <see cref="T:System.DateTime" />. </param>
		/// <param name="kind">One of the <see cref="T:System.DateTimeKind" /> values that indicates whether <paramref name="year" />, <paramref name="month" />, <paramref name="day" />, <paramref name="hour" />, <paramref name="minute" />, <paramref name="second" />, and <paramref name="millisecond" /> specify a local time, Coordinated Universal Time (UTC), or neither.</param>
		/// <exception cref="T:System.ArgumentNullException">
		///   <paramref name="calendar" /> is null. </exception>
		/// <exception cref="T:System.ArgumentOutOfRangeException">
		///   <paramref name="year" /> is not in the range supported by <paramref name="calendar" />.-or- <paramref name="month" /> is less than 1 or greater than the number of months in <paramref name="calendar" />.-or- <paramref name="day" /> is less than 1 or greater than the number of days in <paramref name="month" />.-or- <paramref name="hour" /> is less than 0 or greater than 23.-or- <paramref name="minute" /> is less than 0 or greater than 59.-or- <paramref name="second" /> is less than 0 or greater than 59.-or- <paramref name="millisecond" /> is less than 0 or greater than 999. </exception>
		/// <exception cref="T:System.ArgumentException">The specified time parameters evaluate to less than <see cref="F:System.DateTime.MinValue" /> or more than <see cref="F:System.DateTime.MaxValue" />. -or-<paramref name="kind" /> is not one of the <see cref="T:System.DateTimeKind" /> values.</exception>
		public DateTime(int year, int month, int day, int hour, int minute, int second, int millisecond, Calendar calendar, DateTimeKind kind)
			: this(year, month, day, hour, minute, second, millisecond, calendar)
		{
			CheckDateTimeKind(kind);
			this.kind = kind;
		}

		static DateTime()
		{
			MaxValue = new DateTime(check: false, new TimeSpan(3155378975999999999L));
			MinValue = new DateTime(check: false, new TimeSpan(0L));
			ParseTimeFormats = new string[9]
			{
				"H:m:s.fffffffzzz",
				"H:m:s.fffffff",
				"H:m:s tt zzz",
				"H:m:szzz",
				"H:m:s",
				"H:mzzz",
				"H:m",
				"H tt",
				"H'時'm'分's'秒'"
			};
			ParseYearDayMonthFormats = new string[10]
			{
				"yyyy/M/dT",
				"M/yyyy/dT",
				"yyyy'年'M'月'd'日",
				"yyyy/d/MMMM",
				"yyyy/MMM/d",
				"d/MMMM/yyyy",
				"MMM/d/yyyy",
				"d/yyyy/MMMM",
				"MMM/yyyy/d",
				"yy/d/M"
			};
			ParseYearMonthDayFormats = new string[12]
			{
				"yyyy/M/dT",
				"M/yyyy/dT",
				"yyyy'年'M'月'd'日",
				"yyyy/MMMM/d",
				"yyyy/d/MMM",
				"MMMM/d/yyyy",
				"d/MMM/yyyy",
				"MMMM/yyyy/d",
				"d/yyyy/MMM",
				"yy/MMMM/d",
				"yy/d/MMM",
				"MMM/yy/d"
			};
			ParseDayMonthYearFormats = new string[15]
			{
				"yyyy/M/dT",
				"M/yyyy/dT",
				"yyyy'年'M'月'd'日",
				"yyyy/MMMM/d",
				"yyyy/d/MMM",
				"d/MMMM/yyyy",
				"MMM/d/yyyy",
				"MMMM/yyyy/d",
				"d/yyyy/MMM",
				"d/MMMM/yy",
				"yy/MMM/d",
				"d/yy/MMM",
				"yy/d/MMM",
				"MMM/d/yy",
				"MMM/yy/d"
			};
			ParseMonthDayYearFormats = new string[15]
			{
				"yyyy/M/dT",
				"M/yyyy/dT",
				"yyyy'年'M'月'd'日",
				"yyyy/MMMM/d",
				"yyyy/d/MMM",
				"MMMM/d/yyyy",
				"d/MMM/yyyy",
				"MMMM/yyyy/d",
				"d/yyyy/MMM",
				"MMMM/d/yy",
				"MMM/yy/d",
				"d/MMM/yy",
				"yy/MMM/d",
				"d/yy/MMM",
				"yy/d/MMM"
			};
			MonthDayShortFormats = new string[3]
			{
				"MMMM/d",
				"d/MMM",
				"yyyy/MMMM"
			};
			DayMonthShortFormats = new string[3]
			{
				"d/MMMM",
				"MMM/yy",
				"yyyy/MMMM"
			};
			daysmonth = new int[13]
			{
				0,
				31,
				28,
				31,
				30,
				31,
				30,
				31,
				31,
				30,
				31,
				30,
				31
			};
			daysmonthleap = new int[13]
			{
				0,
				31,
				29,
				31,
				30,
				31,
				30,
				31,
				31,
				30,
				31,
				30,
				31
			};
			if (MonoTouchAOTHelper.FalseFlag)
			{
				GenericComparer<DateTime> genericComparer = new GenericComparer<DateTime>();
				GenericEqualityComparer<DateTime> genericEqualityComparer = new GenericEqualityComparer<DateTime>();
			}
		}

		/// <summary>This conversion is not supported. Attempting to use this method throws an <see cref="T:System.InvalidCastException" />.</summary>
		/// <returns>The return value for this member is not used.</returns>
		/// <param name="provider">An object that implements the <see cref="T:System.IFormatProvider" /> interface. (This parameter is not used; specify null.)</param>
		/// <exception cref="T:System.InvalidCastException">In all cases.</exception>
		bool IConvertible.ToBoolean(IFormatProvider provider)
		{
			throw new InvalidCastException();
		}

		/// <summary>This conversion is not supported. Attempting to use this method throws an <see cref="T:System.InvalidCastException" />.</summary>
		/// <returns>The return value for this member is not used.</returns>
		/// <param name="provider">An object that implements the <see cref="T:System.IFormatProvider" /> interface. (This parameter is not used; specify null.)</param>
		/// <exception cref="T:System.InvalidCastException">In all cases.</exception>
		byte IConvertible.ToByte(IFormatProvider provider)
		{
			throw new InvalidCastException();
		}

		/// <summary>This conversion is not supported. Attempting to use this method throws an <see cref="T:System.InvalidCastException" />.</summary>
		/// <returns>The return value for this member is not used.</returns>
		/// <param name="provider">An object that implements the <see cref="T:System.IFormatProvider" /> interface. (This parameter is not used; specify null.)</param>
		/// <exception cref="T:System.InvalidCastException">In all cases.</exception>
		char IConvertible.ToChar(IFormatProvider provider)
		{
			throw new InvalidCastException();
		}

		/// <summary>Returns the current <see cref="T:System.DateTime" /> object.</summary>
		/// <returns>The current <see cref="T:System.DateTime" /> object.</returns>
		/// <param name="provider">An object that implements the <see cref="T:System.IFormatProvider" /> interface. (This parameter is not used; specify null.)</param>
		DateTime IConvertible.ToDateTime(IFormatProvider provider)
		{
			return this;
		}

		/// <summary>This conversion is not supported. Attempting to use this method throws an <see cref="T:System.InvalidCastException" />.</summary>
		/// <returns>The return value for this member is not used.</returns>
		/// <param name="provider">An object that implements the <see cref="T:System.IFormatProvider" /> interface or null.</param>
		/// <exception cref="T:System.InvalidCastException">In all cases.</exception>
		decimal IConvertible.ToDecimal(IFormatProvider provider)
		{
			throw new InvalidCastException();
		}

		/// <summary>This conversion is not supported. Attempting to use this method throws an <see cref="T:System.InvalidCastException" />.</summary>
		/// <returns>The return value for this member is not used.</returns>
		/// <param name="provider">An object that implements the <see cref="T:System.IFormatProvider" /> interface. (This parameter is not used; specify null.)</param>
		/// <exception cref="T:System.InvalidCastException">In all cases.</exception>
		double IConvertible.ToDouble(IFormatProvider provider)
		{
			throw new InvalidCastException();
		}

		/// <summary>This conversion is not supported. Attempting to use this method throws an <see cref="T:System.InvalidCastException" />.</summary>
		/// <returns>The return value for this member is not used.</returns>
		/// <param name="provider">An object that implements the <see cref="T:System.IFormatProvider" /> interface. (This parameter is not used; specify null.)</param>
		/// <exception cref="T:System.InvalidCastException">In all cases.</exception>
		short IConvertible.ToInt16(IFormatProvider provider)
		{
			throw new InvalidCastException();
		}

		/// <summary>This conversion is not supported. Attempting to use this method throws an <see cref="T:System.InvalidCastException" />.</summary>
		/// <returns>The return value for this member is not used.</returns>
		/// <param name="provider">An object that implements the <see cref="T:System.IFormatProvider" /> interface. (This parameter is not used; specify null.)</param>
		/// <exception cref="T:System.InvalidCastException">In all cases.</exception>
		int IConvertible.ToInt32(IFormatProvider provider)
		{
			throw new InvalidCastException();
		}

		/// <summary>This conversion is not supported. Attempting to use this method throws an <see cref="T:System.InvalidCastException" />.</summary>
		/// <returns>The return value for this member is not used.</returns>
		/// <param name="provider">An object that implements the <see cref="T:System.IFormatProvider" /> interface. (This parameter is not used; specify null.)</param>
		/// <exception cref="T:System.InvalidCastException">In all cases.</exception>
		long IConvertible.ToInt64(IFormatProvider provider)
		{
			throw new InvalidCastException();
		}

		/// <summary>This conversion is not supported. Attempting to use this method throws an <see cref="T:System.InvalidCastException" />.</summary>
		/// <returns>The return value for this member is not used.</returns>
		/// <param name="provider">An object that implements the <see cref="T:System.IFormatProvider" /> interface. (This parameter is not used; specify null.)</param>
		/// <exception cref="T:System.InvalidCastException">In all cases.</exception>
		sbyte IConvertible.ToSByte(IFormatProvider provider)
		{
			throw new InvalidCastException();
		}

		/// <summary>This conversion is not supported. Attempting to use this method throws an <see cref="T:System.InvalidCastException" />.</summary>
		/// <returns>The return value for this member is not used.</returns>
		/// <param name="provider">An object that implements the <see cref="T:System.IFormatProvider" /> interface. (This parameter is not used; specify null.)</param>
		/// <exception cref="T:System.InvalidCastException">In all cases. </exception>
		float IConvertible.ToSingle(IFormatProvider provider)
		{
			throw new InvalidCastException();
		}

		/// <summary>Converts the current <see cref="T:System.DateTime" /> object to an object of a specified type.</summary>
		/// <returns>An object of the type specified by the <paramref name="type" /> parameter, with a value equivalent to the current <see cref="T:System.DateTime" /> object.</returns>
		/// <param name="type">The desired type. </param>
		/// <param name="provider">An object that implements the <see cref="T:System.IFormatProvider" /> interface. (This parameter is not used; specify null.)</param>
		/// <exception cref="T:System.ArgumentNullException">
		///   <paramref name="type" /> is null. </exception>
		/// <exception cref="T:System.InvalidCastException">This conversion is not supported for the <see cref="T:System.DateTime" /> type.</exception>
		object IConvertible.ToType(Type targetType, IFormatProvider provider)
		{
			if (targetType == null)
			{
				throw new ArgumentNullException("targetType");
			}
			if (targetType == typeof(DateTime))
			{
				return this;
			}
			if (targetType == typeof(string))
			{
				return ToString(provider);
			}
			if (targetType == typeof(object))
			{
				return this;
			}
			throw new InvalidCastException();
		}

		/// <summary>This conversion is not supported. Attempting to use this method throws an <see cref="T:System.InvalidCastException" />.</summary>
		/// <returns>The return value for this member is not used.</returns>
		/// <param name="provider">An object that implements the <see cref="T:System.IFormatProvider" /> interface. (This parameter is not used; specify null.)</param>
		/// <exception cref="T:System.InvalidCastException">In all cases.</exception>
		ushort IConvertible.ToUInt16(IFormatProvider provider)
		{
			throw new InvalidCastException();
		}

		/// <summary>This conversion is not supported. Attempting to use this method throws an <see cref="T:System.InvalidCastException" />.</summary>
		/// <returns>The return value for this member is not used.</returns>
		/// <param name="provider">An object that implements the <see cref="T:System.IFormatProvider" /> interface. (This parameter is not used; specify null.)</param>
		/// <exception cref="T:System.InvalidCastException">In all cases.</exception>
		uint IConvertible.ToUInt32(IFormatProvider provider)
		{
			throw new InvalidCastException();
		}

		/// <summary>This conversion is not supported. Attempting to use this method throws an <see cref="T:System.InvalidCastException" />.</summary>
		/// <returns>The return value for this member is not used.</returns>
		/// <param name="provider">An object that implements the <see cref="T:System.IFormatProvider" /> interface. (This parameter is not used; specify null.)</param>
		/// <exception cref="T:System.InvalidCastException">In all cases.</exception>
		ulong IConvertible.ToUInt64(IFormatProvider provider)
		{
			throw new InvalidCastException();
		}

		private static int AbsoluteDays(int year, int month, int day)
		{
			int num = 0;
			int num2 = 1;
			int[] array = (!IsLeapYear(year)) ? daysmonth : daysmonthleap;
			while (num2 < month)
			{
				num += array[num2++];
			}
			return day - 1 + num + 365 * (year - 1) + (year - 1) / 4 - (year - 1) / 100 + (year - 1) / 400;
		}

		private int FromTicks(Which what)
		{
			int num = 1;
			int[] array = daysmonth;
			int days = ticks.Days;
			int num2 = days / 146097;
			days -= num2 * 146097;
			int num3 = days / 36524;
			if (num3 == 4)
			{
				num3 = 3;
			}
			days -= num3 * 36524;
			int num4 = days / 1461;
			days -= num4 * 1461;
			int num5 = days / 365;
			if (num5 == 4)
			{
				num5 = 3;
			}
			if (what == Which.Year)
			{
				return num2 * 400 + num3 * 100 + num4 * 4 + num5 + 1;
			}
			days -= num5 * 365;
			if (what == Which.DayYear)
			{
				return days + 1;
			}
			if (num5 == 3 && (num3 == 3 || num4 != 24))
			{
				array = daysmonthleap;
			}
			while (days >= array[num])
			{
				days -= array[num++];
			}
			if (what == Which.Month)
			{
				return num;
			}
			return days + 1;
		}

		[MethodImpl(MethodImplOptions.InternalCall)]
		internal static extern long GetTimeMonotonic();

		[MethodImpl(MethodImplOptions.InternalCall)]
		internal static extern long GetNow();

		/// <summary>Adds the value of the specified <see cref="T:System.TimeSpan" /> to the value of this instance.</summary>
		/// <returns>A <see cref="T:System.DateTime" /> whose value is the sum of the date and time represented by this instance and the time interval represented by <paramref name="value" />.</returns>
		/// <param name="value">A <see cref="T:System.TimeSpan" /> that contains the interval to add. </param>
		/// <exception cref="T:System.ArgumentOutOfRangeException">The resulting <see cref="T:System.DateTime" /> is less than <see cref="F:System.DateTime.MinValue" /> or greater than <see cref="F:System.DateTime.MaxValue" />. </exception>
		/// <filterpriority>2</filterpriority>
		public DateTime Add(TimeSpan value)
		{
			DateTime result = AddTicks(value.Ticks);
			result.kind = kind;
			return result;
		}

		/// <summary>Adds the specified number of days to the value of this instance.</summary>
		/// <returns>A <see cref="T:System.DateTime" /> whose value is the sum of the date and time represented by this instance and the number of days represented by <paramref name="value" />.</returns>
		/// <param name="value">A number of whole and fractional days. The <paramref name="value" /> parameter can be negative or positive. </param>
		/// <exception cref="T:System.ArgumentOutOfRangeException">The resulting <see cref="T:System.DateTime" /> is less than <see cref="F:System.DateTime.MinValue" /> or greater than <see cref="F:System.DateTime.MaxValue" />. </exception>
		/// <filterpriority>2</filterpriority>
		public DateTime AddDays(double value)
		{
			return AddMilliseconds(Math.Round(value * 86400000.0));
		}

		/// <summary>Adds the specified number of ticks to the value of this instance.</summary>
		/// <returns>A <see cref="T:System.DateTime" /> whose value is the sum of the date and time represented by this instance and the time represented by <paramref name="value" />.</returns>
		/// <param name="value">A number of 100-nanosecond ticks. The <paramref name="value" /> parameter can be positive or negative. </param>
		/// <exception cref="T:System.ArgumentOutOfRangeException">The resulting <see cref="T:System.DateTime" /> is less than <see cref="F:System.DateTime.MinValue" /> or greater than <see cref="F:System.DateTime.MaxValue" />. </exception>
		/// <filterpriority>2</filterpriority>
		public DateTime AddTicks(long value)
		{
			if (value + ticks.Ticks > 3155378975999999999L || value + ticks.Ticks < 0)
			{
				throw new ArgumentOutOfRangeException();
			}
			DateTime result = new DateTime(value + ticks.Ticks);
			result.kind = kind;
			return result;
		}

		/// <summary>Adds the specified number of hours to the value of this instance.</summary>
		/// <returns>A <see cref="T:System.DateTime" /> whose value is the sum of the date and time represented by this instance and the number of hours represented by <paramref name="value" />.</returns>
		/// <param name="value">A number of whole and fractional hours. The <paramref name="value" /> parameter can be negative or positive. </param>
		/// <exception cref="T:System.ArgumentOutOfRangeException">The resulting <see cref="T:System.DateTime" /> is less than <see cref="F:System.DateTime.MinValue" /> or greater than <see cref="F:System.DateTime.MaxValue" />. </exception>
		/// <filterpriority>2</filterpriority>
		public DateTime AddHours(double value)
		{
			return AddMilliseconds(value * 3600000.0);
		}

		/// <summary>Adds the specified number of milliseconds to the value of this instance.</summary>
		/// <returns>A <see cref="T:System.DateTime" /> whose value is the sum of the date and time represented by this instance and the number of milliseconds represented by <paramref name="value" />.</returns>
		/// <param name="value">A number of whole and fractional milliseconds. The <paramref name="value" /> parameter can be negative or positive. Note that this value is rounded to the nearest integer.</param>
		/// <exception cref="T:System.ArgumentOutOfRangeException">The resulting <see cref="T:System.DateTime" /> is less than <see cref="F:System.DateTime.MinValue" /> or greater than <see cref="F:System.DateTime.MaxValue" />. </exception>
		/// <filterpriority>2</filterpriority>
		public DateTime AddMilliseconds(double value)
		{
			if (value * 10000.0 > 9.2233720368547758E+18 || value * 10000.0 < -9.2233720368547758E+18)
			{
				throw new ArgumentOutOfRangeException();
			}
			long value2 = (long)Math.Round(value * 10000.0);
			return AddTicks(value2);
		}

		private DateTime AddRoundedMilliseconds(double ms)
		{
			if (ms * 10000.0 > 9.2233720368547758E+18 || ms * 10000.0 < -9.2233720368547758E+18)
			{
				throw new ArgumentOutOfRangeException();
			}
			long value = (long)(ms += ((!(ms > 0.0)) ? (-0.5) : 0.5)) * 10000;
			return AddTicks(value);
		}

		/// <summary>Adds the specified number of minutes to the value of this instance.</summary>
		/// <returns>A <see cref="T:System.DateTime" /> whose value is the sum of the date and time represented by this instance and the number of minutes represented by <paramref name="value" />.</returns>
		/// <param name="value">A number of whole and fractional minutes. The <paramref name="value" /> parameter can be negative or positive. </param>
		/// <exception cref="T:System.ArgumentOutOfRangeException">The resulting <see cref="T:System.DateTime" /> is less than <see cref="F:System.DateTime.MinValue" /> or greater than <see cref="F:System.DateTime.MaxValue" />. </exception>
		/// <filterpriority>2</filterpriority>
		public DateTime AddMinutes(double value)
		{
			return AddMilliseconds(value * 60000.0);
		}

		/// <summary>Adds the specified number of months to the value of this instance.</summary>
		/// <returns>A <see cref="T:System.DateTime" /> whose value is the sum of the date and time represented by this instance and <paramref name="months" />.</returns>
		/// <param name="months">A number of months. The <paramref name="months" /> parameter can be negative or positive. </param>
		/// <exception cref="T:System.ArgumentOutOfRangeException">The resulting <see cref="T:System.DateTime" /> is less than <see cref="F:System.DateTime.MinValue" /> or greater than <see cref="F:System.DateTime.MaxValue" />.-or- <paramref name="months" /> is less than -120,000 or greater than 120,000. </exception>
		/// <filterpriority>2</filterpriority>
		public DateTime AddMonths(int months)
		{
			int num = Day;
			int num2 = Month + months % 12;
			int num3 = Year + months / 12;
			if (num2 < 1)
			{
				num2 = 12 + num2;
				num3--;
			}
			else if (num2 > 12)
			{
				num2 -= 12;
				num3++;
			}
			int num4 = DaysInMonth(num3, num2);
			if (num > num4)
			{
				num = num4;
			}
			DateTime dateTime = new DateTime(num3, num2, num);
			dateTime.kind = kind;
			return dateTime.Add(TimeOfDay);
		}

		/// <summary>Adds the specified number of seconds to the value of this instance.</summary>
		/// <returns>A <see cref="T:System.DateTime" /> whose value is the sum of the date and time represented by this instance and the number of seconds represented by <paramref name="value" />.</returns>
		/// <param name="value">A number of whole and fractional seconds. The <paramref name="value" /> parameter can be negative or positive. </param>
		/// <exception cref="T:System.ArgumentOutOfRangeException">The resulting <see cref="T:System.DateTime" /> is less than <see cref="F:System.DateTime.MinValue" /> or greater than <see cref="F:System.DateTime.MaxValue" />. </exception>
		/// <filterpriority>2</filterpriority>
		public DateTime AddSeconds(double value)
		{
			return AddMilliseconds(value * 1000.0);
		}

		/// <summary>Adds the specified number of years to the value of this instance.</summary>
		/// <returns>A <see cref="T:System.DateTime" /> whose value is the sum of the date and time represented by this instance and the number of years represented by <paramref name="value" />.</returns>
		/// <param name="value">A number of years. The <paramref name="value" /> parameter can be negative or positive. </param>
		/// <exception cref="T:System.ArgumentOutOfRangeException">
		///   <paramref name="value" /> or the resulting <see cref="T:System.DateTime" /> is less than <see cref="F:System.DateTime.MinValue" /> or greater than <see cref="F:System.DateTime.MaxValue" />. </exception>
		/// <filterpriority>2</filterpriority>
		public DateTime AddYears(int value)
		{
			return AddMonths(value * 12);
		}

		/// <summary>Compares two instances of <see cref="T:System.DateTime" /> and returns an indication of their relative values.</summary>
		/// <returns>A signed number indicating the relative values of <paramref name="t1" /> and <paramref name="t2" />.Value Type Condition Less than zero <paramref name="t1" /> is less than <paramref name="t2" />. Zero <paramref name="t1" /> equals <paramref name="t2" />. Greater than zero <paramref name="t1" /> is greater than <paramref name="t2" />. </returns>
		/// <param name="t1">The first <see cref="T:System.DateTime" />. </param>
		/// <param name="t2">The second <see cref="T:System.DateTime" />. </param>
		/// <filterpriority>1</filterpriority>
		public static int Compare(DateTime t1, DateTime t2)
		{
			if (t1.ticks < t2.ticks)
			{
				return -1;
			}
			if (t1.ticks > t2.ticks)
			{
				return 1;
			}
			return 0;
		}

		/// <summary>Compares the value of this instance to a specified object that contains a specified <see cref="T:System.DateTime" /> value, and returns an integer that indicates whether this instance is earlier than, the same as, or later than the specified <see cref="T:System.DateTime" /> value.</summary>
		/// <returns>A signed number indicating the relative values of this instance and <paramref name="value" />.Value Description Less than zero This instance is earlier than <paramref name="value" />. Zero This instance is the same as <paramref name="value" />. Greater than zero This instance is later than <paramref name="value" />, or <paramref name="value" /> is null. </returns>
		/// <param name="value">A boxed <see cref="T:System.DateTime" /> object to compare, or null. </param>
		/// <exception cref="T:System.ArgumentException">
		///   <paramref name="value" /> is not a <see cref="T:System.DateTime" />. </exception>
		/// <filterpriority>2</filterpriority>
		public int CompareTo(object value)
		{
			if (value == null)
			{
				return 1;
			}
			if (!(value is DateTime))
			{
				throw new ArgumentException(Locale.GetText("Value is not a System.DateTime"));
			}
			return Compare(this, (DateTime)value);
		}

		/// <summary>Indicates whether this instance of <see cref="T:System.DateTime" /> is within the Daylight Saving Time range for the current time zone.</summary>
		/// <returns>true if <see cref="P:System.DateTime.Kind" /> is <see cref="F:System.DateTimeKind.Local" /> or <see cref="F:System.DateTimeKind.Unspecified" /> and the value of this instance of <see cref="T:System.DateTime" /> is within the Daylight Saving Time range for the current time zone. false if <see cref="P:System.DateTime.Kind" /> is <see cref="F:System.DateTimeKind.Utc" />.</returns>
		/// <filterpriority>2</filterpriority>
		public bool IsDaylightSavingTime()
		{
			if (kind == DateTimeKind.Utc)
			{
				return false;
			}
			return TimeZone.CurrentTimeZone.IsDaylightSavingTime(this);
		}

		/// <summary>Compares the value of this instance to a specified <see cref="T:System.DateTime" /> value and returns an integer that indicates whether this instance is earlier than, the same as, or later than the specified <see cref="T:System.DateTime" /> value.</summary>
		/// <returns>A signed number indicating the relative values of this instance and the <paramref name="value" /> parameter.Value Description Less than zero This instance is earlier than <paramref name="value" />. Zero This instance is the same as <paramref name="value" />. Greater than zero This instance is later than <paramref name="value" />. </returns>
		/// <param name="value">A <see cref="T:System.DateTime" /> object to compare. </param>
		/// <filterpriority>2</filterpriority>
		public int CompareTo(DateTime value)
		{
			return Compare(this, value);
		}

		/// <summary>Returns a value indicating whether this instance is equal to the specified <see cref="T:System.DateTime" /> instance.</summary>
		/// <returns>true if the <paramref name="value" /> parameter equals the value of this instance; otherwise, false.</returns>
		/// <param name="value">A <see cref="T:System.DateTime" /> instance to compare to this instance. </param>
		/// <filterpriority>2</filterpriority>
		public bool Equals(DateTime value)
		{
			return value.ticks == ticks;
		}

		/// <summary>Serializes the current <see cref="T:System.DateTime" /> object to a 64-bit binary value that subsequently can be used to recreate the <see cref="T:System.DateTime" /> object.</summary>
		/// <returns>A 64-bit signed integer that encodes the <see cref="P:System.DateTime.Kind" /> and <see cref="P:System.DateTime.Ticks" /> properties. </returns>
		/// <filterpriority>2</filterpriority>
		public long ToBinary()
		{
			switch (kind)
			{
			case DateTimeKind.Utc:
				return Ticks | 0x4000000000000000L;
			case DateTimeKind.Local:
				return ToUniversalTime().Ticks | long.MinValue;
			default:
				return Ticks;
			}
		}

		/// <summary>Deserializes a 64-bit binary value and recreates an original serialized <see cref="T:System.DateTime" /> object.</summary>
		/// <returns>A <see cref="T:System.DateTime" /> object that is equivalent to the <see cref="T:System.DateTime" /> object that was serialized by the <see cref="M:System.DateTime.ToBinary" /> method.</returns>
		/// <param name="dateData">A 64-bit signed integer that encodes the <see cref="P:System.DateTime.Kind" /> property in a 2-bit field and the <see cref="P:System.DateTime.Ticks" /> property in a 62-bit field. </param>
		/// <exception cref="T:System.ArgumentException">
		///   <paramref name="dateData" /> is less than <see cref="F:System.DateTime.MinValue" /> or greater than <see cref="F:System.DateTime.MaxValue" />. </exception>
		/// <filterpriority>1</filterpriority>
		public static DateTime FromBinary(long dateData)
		{
			switch ((ulong)dateData >> 62)
			{
			case 1uL:
				return new DateTime(dateData ^ 0x4000000000000000L, DateTimeKind.Utc);
			case 0uL:
				return new DateTime(dateData, DateTimeKind.Unspecified);
			default:
				return new DateTime(dateData & 0x3FFFFFFFFFFFFFFFL, DateTimeKind.Utc).ToLocalTime();
			}
		}

		/// <summary>Creates a new <see cref="T:System.DateTime" /> object that represents the same time as the specified <see cref="T:System.DateTime" />, but is designated in either local time, Coordinated Universal Time (UTC), or neither, as indicated by the specified <see cref="T:System.DateTimeKind" /> value.</summary>
		/// <returns>A new <see cref="T:System.DateTime" /> object consisting of the same time represented by the <paramref name="value" /> parameter and the <see cref="T:System.DateTimeKind" /> value specified by the <paramref name="kind" /> parameter.</returns>
		/// <param name="value">A <see cref="T:System.DateTime" /> object.</param>
		/// <param name="kind">One of the <see cref="T:System.DateTimeKind" /> values.</param>
		/// <filterpriority>2</filterpriority>
		public static DateTime SpecifyKind(DateTime value, DateTimeKind kind)
		{
			return new DateTime(value.Ticks, kind);
		}

		/// <summary>Returns the number of days in the specified month and year.</summary>
		/// <returns>The number of days in <paramref name="month" /> for the specified <paramref name="year" />.For example, if <paramref name="month" /> equals 2 for February, the return value is 28 or 29 depending upon whether <paramref name="year" /> is a leap year.</returns>
		/// <param name="year">The year. </param>
		/// <param name="month">The month (a number ranging from 1 to 12). </param>
		/// <exception cref="T:System.ArgumentOutOfRangeException">
		///   <paramref name="month" /> is less than 1 or greater than 12.-or-<paramref name="year" /> is less than 1 or greater than 9999.</exception>
		/// <filterpriority>1</filterpriority>
		public static int DaysInMonth(int year, int month)
		{
			if (month < 1 || month > 12)
			{
				throw new ArgumentOutOfRangeException();
			}
			if (year < 1 || year > 9999)
			{
				throw new ArgumentOutOfRangeException();
			}
			int[] array = (!IsLeapYear(year)) ? daysmonth : daysmonthleap;
			return array[month];
		}

		/// <summary>Returns a value indicating whether this instance is equal to a specified object.</summary>
		/// <returns>true if <paramref name="value" /> is an instance of <see cref="T:System.DateTime" /> and equals the value of this instance; otherwise, false.</returns>
		/// <param name="value">An object to compare to this instance. </param>
		/// <filterpriority>2</filterpriority>
		public override bool Equals(object value)
		{
			if (!(value is DateTime))
			{
				return false;
			}
			DateTime dateTime = (DateTime)value;
			return dateTime.ticks == ticks;
		}

		/// <summary>Returns a value indicating whether two instances of <see cref="T:System.DateTime" /> are equal.</summary>
		/// <returns>true if the two <see cref="T:System.DateTime" /> values are equal; otherwise, false.</returns>
		/// <param name="t1">The first <see cref="T:System.DateTime" /> instance. </param>
		/// <param name="t2">The second <see cref="T:System.DateTime" /> instance. </param>
		/// <filterpriority>1</filterpriority>
		public static bool Equals(DateTime t1, DateTime t2)
		{
			return t1.ticks == t2.ticks;
		}

		/// <summary>Converts the specified Windows file time to an equivalent local time.</summary>
		/// <returns>A <see cref="T:System.DateTime" /> object that represents a local time equivalent to the date and time represented by the <paramref name="fileTime" /> parameter.</returns>
		/// <param name="fileTime">A Windows file time expressed in ticks. </param>
		/// <exception cref="T:System.ArgumentOutOfRangeException">
		///   <paramref name="fileTime" /> is less than 0 or represents a time greater than <see cref="F:System.DateTime.MaxValue" />. </exception>
		/// <filterpriority>1</filterpriority>
		public static DateTime FromFileTime(long fileTime)
		{
			if (fileTime < 0)
			{
				throw new ArgumentOutOfRangeException("fileTime", "< 0");
			}
			return new DateTime(504911232000000000L + fileTime).ToLocalTime();
		}

		/// <summary>Converts the specified Windows file time to an equivalent UTC time.</summary>
		/// <returns>A <see cref="T:System.DateTime" /> object that represents a UTC time equivalent to the date and time represented by the <paramref name="fileTime" /> parameter.</returns>
		/// <param name="fileTime">A Windows file time expressed in ticks. </param>
		/// <exception cref="T:System.ArgumentOutOfRangeException">
		///   <paramref name="fileTime" /> is less than 0 or represents a time greater than <see cref="F:System.DateTime.MaxValue" />. </exception>
		/// <filterpriority>1</filterpriority>
		public static DateTime FromFileTimeUtc(long fileTime)
		{
			if (fileTime < 0)
			{
				throw new ArgumentOutOfRangeException("fileTime", "< 0");
			}
			return new DateTime(504911232000000000L + fileTime);
		}

		/// <summary>Returns a <see cref="T:System.DateTime" /> equivalent to the specified OLE Automation Date.</summary>
		/// <returns>A <see cref="T:System.DateTime" /> that represents the same date and time as <paramref name="d" />.</returns>
		/// <param name="d">An OLE Automation Date value. </param>
		/// <exception cref="T:System.ArgumentException">The date is not a valid OLE Automation Date value. </exception>
		/// <filterpriority>1</filterpriority>
		public static DateTime FromOADate(double d)
		{
			if (d <= -657435.0 || d >= 2958466.0)
			{
				throw new ArgumentException("d", "[-657435,2958466]");
			}
			DateTime dateTime = new DateTime(599264352000000000L);
			if (d < 0.0)
			{
				double num = Math.Ceiling(d);
				dateTime = dateTime.AddRoundedMilliseconds(num * 86400000.0);
				double num2 = num - d;
				return dateTime.AddRoundedMilliseconds(num2 * 86400000.0);
			}
			return dateTime.AddRoundedMilliseconds(d * 86400000.0);
		}

		/// <summary>Converts the value of this instance to all the string representations supported by the standard <see cref="T:System.DateTime" /> format specifiers.</summary>
		/// <returns>A string array where each element is the representation of the value of this instance formatted with one of the standard <see cref="T:System.DateTime" /> formatting specifiers.</returns>
		/// <filterpriority>2</filterpriority>
		public string[] GetDateTimeFormats()
		{
			return GetDateTimeFormats(CultureInfo.CurrentCulture);
		}

		/// <summary>Converts the value of this instance to all the string representations supported by the specified standard <see cref="T:System.DateTime" /> format specifier.</summary>
		/// <returns>A string array where each element is the representation of the value of this instance formatted with the <paramref name="format" /> standard <see cref="T:System.DateTime" /> formatting specifier.</returns>
		/// <param name="format">A DateTime format string. </param>
		/// <filterpriority>2</filterpriority>
		public string[] GetDateTimeFormats(char format)
		{
			if ("dDgGfFmMrRstTuUyY".IndexOf(format) < 0)
			{
				throw new FormatException("Invalid format character.");
			}
			return new string[1]
			{
				ToString(format.ToString())
			};
		}

		/// <summary>Converts the value of this instance to all the string representations supported by the standard <see cref="T:System.DateTime" /> format specifiers and the specified culture-specific formatting information.</summary>
		/// <returns>A string array where each element is the representation of the value of this instance formatted with one of the standard <see cref="T:System.DateTime" /> formatting specifiers.</returns>
		/// <param name="provider">An <see cref="T:System.IFormatProvider" /> that supplies culture-specific formatting information about this instance. </param>
		/// <filterpriority>2</filterpriority>
		public string[] GetDateTimeFormats(IFormatProvider provider)
		{
			DateTimeFormatInfo provider2 = (DateTimeFormatInfo)provider.GetFormat(typeof(DateTimeFormatInfo));
			ArrayList arrayList = new ArrayList();
			string text = "dDgGfFmMrRstTuUyY";
			foreach (char format in text)
			{
				arrayList.AddRange(GetDateTimeFormats(format, provider2));
			}
			return arrayList.ToArray(typeof(string)) as string[];
		}

		/// <summary>Converts the value of this instance to all the string representations supported by the specified standard <see cref="T:System.DateTime" /> format specifier and culture-specific formatting information.</summary>
		/// <returns>A string array where each element is the representation of the value of this instance formatted with one of the standard <see cref="T:System.DateTime" /> formatting specifiers.</returns>
		/// <param name="format">A DateTime format string. </param>
		/// <param name="provider">An <see cref="T:System.IFormatProvider" /> that supplies culture-specific formatting information about this instance. </param>
		/// <filterpriority>2</filterpriority>
		public string[] GetDateTimeFormats(char format, IFormatProvider provider)
		{
			if ("dDgGfFmMrRstTuUyY".IndexOf(format) < 0)
			{
				throw new FormatException("Invalid format character.");
			}
			bool adjustutc = false;
			char c = format;
			if (c == 'U')
			{
				adjustutc = true;
			}
			DateTimeFormatInfo dateTimeFormatInfo = (DateTimeFormatInfo)provider.GetFormat(typeof(DateTimeFormatInfo));
			return GetDateTimeFormats(adjustutc, dateTimeFormatInfo.GetAllRawDateTimePatterns(format), dateTimeFormatInfo);
		}

		private string[] GetDateTimeFormats(bool adjustutc, string[] patterns, DateTimeFormatInfo dfi)
		{
			string[] array = new string[patterns.Length];
			DateTime dt = (!adjustutc) ? this : ToUniversalTime();
			for (int i = 0; i < array.Length; i++)
			{
				array[i] = DateTimeUtils.ToString(dt, patterns[i], dfi);
			}
			return array;
		}

		private void CheckDateTimeKind(DateTimeKind kind)
		{
			if (kind != 0 && kind != DateTimeKind.Utc && kind != DateTimeKind.Local)
			{
				throw new ArgumentException("Invalid DateTimeKind value.", "kind");
			}
		}

		/// <summary>Returns the hash code for this instance.</summary>
		/// <returns>A 32-bit signed integer hash code.</returns>
		/// <filterpriority>2</filterpriority>
		public override int GetHashCode()
		{
			return (int)ticks.Ticks;
		}

		/// <summary>Returns the <see cref="T:System.TypeCode" /> for value type <see cref="T:System.DateTime" />.</summary>
		/// <returns>The enumerated constant, <see cref="F:System.TypeCode.DateTime" />.</returns>
		/// <filterpriority>2</filterpriority>
		public TypeCode GetTypeCode()
		{
			return TypeCode.DateTime;
		}

		/// <summary>Returns an indication whether the specified year is a leap year.</summary>
		/// <returns>true if <paramref name="year" /> is a leap year; otherwise, false.</returns>
		/// <param name="year">A 4-digit year. </param>
		/// <exception cref="T:System.ArgumentOutOfRangeException">
		///   <paramref name="year" /> is less than 1 or greater than 9999.</exception>
		/// <filterpriority>1</filterpriority>
		public static bool IsLeapYear(int year)
		{
			if (year < 1 || year > 9999)
			{
				throw new ArgumentOutOfRangeException();
			}
			return (year % 4 == 0 && year % 100 != 0) || year % 400 == 0;
		}

		/// <summary>Converts the specified string representation of a date and time to its <see cref="T:System.DateTime" /> equivalent.</summary>
		/// <returns>A <see cref="T:System.DateTime" /> equivalent to the date and time contained in <paramref name="s" />.</returns>
		/// <param name="s">A string containing a date and time to convert. </param>
		/// <exception cref="T:System.ArgumentNullException">
		///   <paramref name="s" /> is null. </exception>
		/// <exception cref="T:System.FormatException">
		///   <paramref name="s" /> does not contain a valid string representation of a date and time. </exception>
		/// <filterpriority>1</filterpriority>
		public static DateTime Parse(string s)
		{
			return Parse(s, null);
		}

		/// <summary>Converts the specified string representation of a date and time to its <see cref="T:System.DateTime" /> equivalent using the specified culture-specific format information.</summary>
		/// <returns>A <see cref="T:System.DateTime" /> equivalent to the date and time contained in <paramref name="s" /> as specified by <paramref name="provider" />.</returns>
		/// <param name="s">A string containing a date and time to convert. </param>
		/// <param name="provider">An <see cref="T:System.IFormatProvider" /> that supplies culture-specific format information about <paramref name="s" />. </param>
		/// <exception cref="T:System.ArgumentNullException">
		///   <paramref name="s" /> is null. </exception>
		/// <exception cref="T:System.FormatException">
		///   <paramref name="s" /> does not contain a valid string representation of a date and time. </exception>
		/// <filterpriority>1</filterpriority>
		public static DateTime Parse(string s, IFormatProvider provider)
		{
			return Parse(s, provider, DateTimeStyles.AllowWhiteSpaces);
		}

		/// <summary>Converts the specified string representation of a date and time to its <see cref="T:System.DateTime" /> equivalent using the specified culture-specific format information and formatting style.</summary>
		/// <returns>A <see cref="T:System.DateTime" /> equivalent to the date and time contained in <paramref name="s" /> as specified by <paramref name="provider" /> and <paramref name="styles" />.</returns>
		/// <param name="s">A string containing a date and time to convert. </param>
		/// <param name="provider">An <see cref="T:System.IFormatProvider" /> that supplies culture-specific formatting information about <paramref name="s" />. </param>
		/// <param name="styles">A bitwise combination of <see cref="T:System.Globalization.DateTimeStyles" /> values that indicates the permitted format of <paramref name="s" />. A typical value to specify is <see cref="F:System.Globalization.DateTimeStyles.None" />.</param>
		/// <exception cref="T:System.ArgumentNullException">
		///   <paramref name="s" /> is null. </exception>
		/// <exception cref="T:System.FormatException">
		///   <paramref name="s" /> does not contain a valid string representation of a date and time. </exception>
		/// <exception cref="T:System.ArgumentException">
		///   <paramref name="styles" /> contains an invalid combination of <see cref="T:System.Globalization.DateTimeStyles" /> values. For example, both <see cref="F:System.Globalization.DateTimeStyles.AssumeLocal" /> and <see cref="F:System.Globalization.DateTimeStyles.AssumeUniversal" />.</exception>
		/// <filterpriority>1</filterpriority>
		public static DateTime Parse(string s, IFormatProvider provider, DateTimeStyles styles)
		{
			if (s == null)
			{
				throw new ArgumentNullException("s");
			}
			Exception exception = null;
			if (!CoreParse(s, provider, styles, out DateTime result, out DateTimeOffset _, setExceptionOnError: true, ref exception))
			{
				throw exception;
			}
			return result;
		}

		internal static bool CoreParse(string s, IFormatProvider provider, DateTimeStyles styles, out DateTime result, out DateTimeOffset dto, bool setExceptionOnError, ref Exception exception)
		{
			dto = new DateTimeOffset(0L, TimeSpan.Zero);
			if (s == null || s.Length == 0)
			{
				if (setExceptionOnError)
				{
					exception = new FormatException("String was not recognized as a valid DateTime.");
				}
				result = MinValue;
				return false;
			}
			if (provider == null)
			{
				provider = CultureInfo.CurrentCulture;
			}
			DateTimeFormatInfo instance = DateTimeFormatInfo.GetInstance(provider);
			string[] array = YearMonthDayFormats(instance, setExceptionOnError, ref exception);
			if (array == null)
			{
				result = MinValue;
				return false;
			}
			bool longYear = false;
			foreach (string firstPart in array)
			{
				bool incompleteFormat = false;
				if (_DoParse(s, firstPart, string.Empty, exact: false, out result, out dto, instance, styles, firstPartIsDate: true, ref incompleteFormat, ref longYear))
				{
					return true;
				}
				if (!incompleteFormat)
				{
					continue;
				}
				for (int j = 0; j < ParseTimeFormats.Length; j++)
				{
					if (_DoParse(s, firstPart, ParseTimeFormats[j], exact: false, out result, out dto, instance, styles, firstPartIsDate: true, ref incompleteFormat, ref longYear))
					{
						return true;
					}
				}
			}
			int num = instance.MonthDayPattern.IndexOf('d');
			int num2 = instance.MonthDayPattern.IndexOf('M');
			if (num == -1 || num2 == -1)
			{
				result = MinValue;
				if (setExceptionOnError)
				{
					exception = new FormatException(Locale.GetText("Order of month and date is not defined by {0}", instance.MonthDayPattern));
				}
				return false;
			}
			string[] array2 = (num >= num2) ? MonthDayShortFormats : DayMonthShortFormats;
			for (int k = 0; k < array2.Length; k++)
			{
				bool incompleteFormat2 = false;
				if (_DoParse(s, array2[k], string.Empty, exact: false, out result, out dto, instance, styles, firstPartIsDate: true, ref incompleteFormat2, ref longYear))
				{
					return true;
				}
			}
			for (int l = 0; l < ParseTimeFormats.Length; l++)
			{
				string firstPart2 = ParseTimeFormats[l];
				bool incompleteFormat3 = false;
				if (_DoParse(s, firstPart2, string.Empty, exact: false, out result, out dto, instance, styles, firstPartIsDate: false, ref incompleteFormat3, ref longYear))
				{
					return true;
				}
				if (!incompleteFormat3)
				{
					continue;
				}
				for (int m = 0; m < array2.Length; m++)
				{
					if (_DoParse(s, firstPart2, array2[m], exact: false, out result, out dto, instance, styles, firstPartIsDate: false, ref incompleteFormat3, ref longYear))
					{
						return true;
					}
				}
				foreach (string text in array)
				{
					if (text[text.Length - 1] != 'T' && _DoParse(s, firstPart2, text, exact: false, out result, out dto, instance, styles, firstPartIsDate: false, ref incompleteFormat3, ref longYear))
					{
						return true;
					}
				}
			}
			if (ParseExact(s, instance.GetAllDateTimePatternsInternal(), instance, styles, out result, exact: false, ref longYear, setExceptionOnError, ref exception))
			{
				return true;
			}
			if (!setExceptionOnError)
			{
				return false;
			}
			exception = new FormatException("String was not recognized as a valid DateTime.");
			return false;
		}

		/// <summary>Converts the specified string representation of a date and time to its <see cref="T:System.DateTime" /> equivalent using the specified format and culture-specific format information. The format of the string representation must match the specified format exactly.</summary>
		/// <returns>A <see cref="T:System.DateTime" /> equivalent to the date and time contained in <paramref name="s" /> as specified by <paramref name="format" /> and <paramref name="provider" />.</returns>
		/// <param name="s">A string containing a date and time to convert. </param>
		/// <param name="format">The expected format of <paramref name="s" />. </param>
		/// <param name="provider">An <see cref="T:System.IFormatProvider" /> that supplies culture-specific format information about <paramref name="s" />. </param>
		/// <exception cref="T:System.ArgumentNullException">
		///   <paramref name="s" /> or <paramref name="format" /> is null. </exception>
		/// <exception cref="T:System.FormatException">
		///   <paramref name="s" /> or <paramref name="format" /> is an empty string. -or- <paramref name="s" /> does not contain a date and time that corresponds to the pattern specified in <paramref name="format" />. </exception>
		/// <filterpriority>2</filterpriority>
		public static DateTime ParseExact(string s, string format, IFormatProvider provider)
		{
			return ParseExact(s, format, provider, DateTimeStyles.None);
		}

		private static string[] YearMonthDayFormats(DateTimeFormatInfo dfi, bool setExceptionOnError, ref Exception exc)
		{
			int num = dfi.ShortDatePattern.IndexOf('d');
			int num2 = dfi.ShortDatePattern.IndexOf('M');
			int num3 = dfi.ShortDatePattern.IndexOf('y');
			if (num == -1 || num2 == -1 || num3 == -1)
			{
				if (setExceptionOnError)
				{
					exc = new FormatException(Locale.GetText("Order of year, month and date is not defined by {0}", dfi.ShortDatePattern));
				}
				return null;
			}
			if (num3 < num2)
			{
				if (num2 < num)
				{
					return ParseYearMonthDayFormats;
				}
				if (num3 < num)
				{
					return ParseYearDayMonthFormats;
				}
				if (setExceptionOnError)
				{
					exc = new FormatException(Locale.GetText("Order of date, year and month defined by {0} is not supported", dfi.ShortDatePattern));
				}
				return null;
			}
			if (num < num2)
			{
				return ParseDayMonthYearFormats;
			}
			if (num < num3)
			{
				return ParseMonthDayYearFormats;
			}
			if (setExceptionOnError)
			{
				exc = new FormatException(Locale.GetText("Order of month, year and date defined by {0} is not supported", dfi.ShortDatePattern));
			}
			return null;
		}

		private static int _ParseNumber(string s, int valuePos, int min_digits, int digits, bool leadingzero, bool sloppy_parsing, out int num_parsed)
		{
			int num = 0;
			if (sloppy_parsing)
			{
				leadingzero = false;
			}
			if (!leadingzero)
			{
				int num2 = 0;
				for (int i = valuePos; i < s.Length && i < digits + valuePos && char.IsDigit(s[i]); i++)
				{
					num2++;
				}
				digits = num2;
			}
			if (digits < min_digits)
			{
				num_parsed = -1;
				return 0;
			}
			if (s.Length - valuePos < digits)
			{
				num_parsed = -1;
				return 0;
			}
			for (int i = valuePos; i < digits + valuePos; i++)
			{
				char c = s[i];
				if (!char.IsDigit(c))
				{
					num_parsed = -1;
					return 0;
				}
				num = num * 10 + (byte)(c - 48);
			}
			num_parsed = digits;
			return num;
		}

		private static int _ParseEnum(string s, int sPos, string[] values, string[] invValues, bool exact, out int num_parsed)
		{
			for (int num = values.Length - 1; num >= 0; num--)
			{
				if (!exact && invValues[num].Length > values[num].Length)
				{
					if (invValues[num].Length > 0 && _ParseString(s, sPos, 0, invValues[num], out num_parsed))
					{
						return num;
					}
					if (values[num].Length > 0 && _ParseString(s, sPos, 0, values[num], out num_parsed))
					{
						return num;
					}
				}
				else
				{
					if (values[num].Length > 0 && _ParseString(s, sPos, 0, values[num], out num_parsed))
					{
						return num;
					}
					if (!exact && invValues[num].Length > 0 && _ParseString(s, sPos, 0, invValues[num], out num_parsed))
					{
						return num;
					}
				}
			}
			num_parsed = -1;
			return -1;
		}

		private static bool _ParseString(string s, int sPos, int maxlength, string value, out int num_parsed)
		{
			if (maxlength <= 0)
			{
				maxlength = value.Length;
			}
			if (sPos + maxlength <= s.Length && string.Compare(s, sPos, value, 0, maxlength, ignoreCase: true, CultureInfo.InvariantCulture) == 0)
			{
				num_parsed = maxlength;
				return true;
			}
			num_parsed = -1;
			return false;
		}

		private static bool _ParseAmPm(string s, int valuePos, int num, DateTimeFormatInfo dfi, bool exact, out int num_parsed, ref int ampm)
		{
			num_parsed = -1;
			if (ampm != -1)
			{
				return false;
			}
			if (!IsLetter(s, valuePos))
			{
				if (dfi.AMDesignator != string.Empty)
				{
					return false;
				}
				if (exact)
				{
					ampm = 0;
				}
				num_parsed = 0;
				return true;
			}
			DateTimeFormatInfo invariantInfo = DateTimeFormatInfo.InvariantInfo;
			if ((!exact && _ParseString(s, valuePos, num, invariantInfo.PMDesignator, out num_parsed)) || (dfi.PMDesignator != string.Empty && _ParseString(s, valuePos, num, dfi.PMDesignator, out num_parsed)))
			{
				ampm = 1;
			}
			else
			{
				if ((exact || !_ParseString(s, valuePos, num, invariantInfo.AMDesignator, out num_parsed)) && !_ParseString(s, valuePos, num, dfi.AMDesignator, out num_parsed))
				{
					return false;
				}
				if (exact || num_parsed != 0)
				{
					ampm = 0;
				}
			}
			return true;
		}

		private static bool _ParseTimeSeparator(string s, int sPos, DateTimeFormatInfo dfi, bool exact, out int num_parsed)
		{
			return _ParseString(s, sPos, 0, dfi.TimeSeparator, out num_parsed) || (!exact && _ParseString(s, sPos, 0, ":", out num_parsed));
		}

		private static bool _ParseDateSeparator(string s, int sPos, DateTimeFormatInfo dfi, bool exact, out int num_parsed)
		{
			num_parsed = -1;
			if (exact && s[sPos] != '/')
			{
				return false;
			}
			if (_ParseTimeSeparator(s, sPos, dfi, exact, out num_parsed) || char.IsDigit(s[sPos]) || char.IsLetter(s[sPos]))
			{
				return false;
			}
			num_parsed = 1;
			return true;
		}

		private static bool IsLetter(string s, int pos)
		{
			return pos < s.Length && char.IsLetter(s[pos]);
		}

		private static bool _DoParse(string s, string firstPart, string secondPart, bool exact, out DateTime result, out DateTimeOffset dto, DateTimeFormatInfo dfi, DateTimeStyles style, bool firstPartIsDate, ref bool incompleteFormat, ref bool longYear)
		{
			bool useutc = false;
			bool use_invariant = false;
			bool sloppy_parsing = false;
			dto = new DateTimeOffset(0L, TimeSpan.Zero);
			bool flag = !exact && secondPart != null;
			incompleteFormat = false;
			int num = 0;
			string text = firstPart;
			bool flag2 = false;
			DateTimeFormatInfo invariantInfo = DateTimeFormatInfo.InvariantInfo;
			if (text.Length == 1)
			{
				text = DateTimeUtils.GetStandardPattern(text[0], dfi, out useutc, out use_invariant);
			}
			result = new DateTime(0L);
			if (text == null)
			{
				return false;
			}
			if (s == null)
			{
				return false;
			}
			if ((style & DateTimeStyles.AllowLeadingWhite) != 0)
			{
				text = text.TrimStart(null);
				s = s.TrimStart(null);
			}
			if ((style & DateTimeStyles.AllowTrailingWhite) != 0)
			{
				text = text.TrimEnd(null);
				s = s.TrimEnd(null);
			}
			if (use_invariant)
			{
				dfi = invariantInfo;
			}
			if ((style & DateTimeStyles.AllowInnerWhite) != 0)
			{
				sloppy_parsing = true;
			}
			string text2 = text;
			int length = text.Length;
			int i = 0;
			int num2 = 0;
			if (length == 0)
			{
				return false;
			}
			int num3 = -1;
			int num4 = -1;
			int num5 = -1;
			int num6 = -1;
			int num7 = -1;
			int num8 = -1;
			int num9 = -1;
			double num10 = -1.0;
			int ampm = -1;
			int num11 = -1;
			int num12 = -1;
			int num13 = -1;
			bool flag3 = true;
			while (num != s.Length)
			{
				int num_parsed = 0;
				if (flag && i + num2 == 0)
				{
					bool flag4 = IsLetter(s, num);
					if (flag4)
					{
						if (s[num] == 'Z')
						{
							num_parsed = 1;
						}
						else
						{
							_ParseString(s, num, 0, "GMT", out num_parsed);
						}
						if (num_parsed > 0 && !IsLetter(s, num + num_parsed))
						{
							num += num_parsed;
							useutc = true;
							continue;
						}
					}
					if (!flag2 && _ParseAmPm(s, num, 0, dfi, exact, out num_parsed, ref ampm))
					{
						if (IsLetter(s, num + num_parsed))
						{
							ampm = -1;
						}
						else if (num_parsed > 0)
						{
							num += num_parsed;
							continue;
						}
					}
					if (!flag2 && num4 == -1 && flag4)
					{
						num4 = _ParseEnum(s, num, dfi.RawDayNames, invariantInfo.RawDayNames, exact, out num_parsed);
						if (num4 == -1)
						{
							num4 = _ParseEnum(s, num, dfi.RawAbbreviatedDayNames, invariantInfo.RawAbbreviatedDayNames, exact, out num_parsed);
						}
						if (num4 != -1 && !IsLetter(s, num + num_parsed))
						{
							num += num_parsed;
							continue;
						}
						num4 = -1;
					}
					if (char.IsWhiteSpace(s[num]) || s[num] == ',')
					{
						num++;
						continue;
					}
					num_parsed = 0;
				}
				if (i + num2 >= length)
				{
					if (flag && num2 == 0)
					{
						flag2 = (flag3 && firstPart[firstPart.Length - 1] == 'T');
						if (!flag3 && text == string.Empty)
						{
							break;
						}
						i = 0;
						text = ((!flag3) ? string.Empty : secondPart);
						text2 = text;
						length = text2.Length;
						flag3 = false;
						continue;
					}
					break;
				}
				bool leadingzero = true;
				if (text2[i] == '\'')
				{
					for (num2 = 1; i + num2 < length && text2[i + num2] != '\''; num2++)
					{
						if (num == s.Length || s[num] != text2[i + num2])
						{
							return false;
						}
						num++;
					}
					i += num2 + 1;
					num2 = 0;
					continue;
				}
				if (text2[i] == '"')
				{
					for (num2 = 1; i + num2 < length && text2[i + num2] != '"'; num2++)
					{
						if (num == s.Length || s[num] != text2[i + num2])
						{
							return false;
						}
						num++;
					}
					i += num2 + 1;
					num2 = 0;
					continue;
				}
				if (text2[i] == '\\')
				{
					i += num2 + 1;
					num2 = 0;
					if (i >= length)
					{
						return false;
					}
					if (s[num] != text2[i])
					{
						return false;
					}
					num++;
					i++;
					continue;
				}
				if (text2[i] == '%')
				{
					i++;
					continue;
				}
				if (char.IsWhiteSpace(s[num]) || (s[num] == ',' && ((!exact && text2[i] == '/') || char.IsWhiteSpace(text2[i]))))
				{
					num++;
					num2 = 0;
					if (exact && (style & DateTimeStyles.AllowInnerWhite) == 0)
					{
						if (!char.IsWhiteSpace(text2[i]))
						{
							return false;
						}
						i++;
						continue;
					}
					int j;
					for (j = num; j < s.Length && (char.IsWhiteSpace(s[j]) || s[j] == ','); j++)
					{
					}
					num = j;
					for (j = i; j < text2.Length && (char.IsWhiteSpace(text2[j]) || text2[j] == ','); j++)
					{
					}
					i = j;
					if (!exact && i < text2.Length && text2[i] == '/' && !_ParseDateSeparator(s, num, dfi, exact, out num_parsed))
					{
						i++;
					}
					continue;
				}
				if (i + num2 + 1 < length && text2[i + num2 + 1] == text2[i + num2])
				{
					num2++;
					continue;
				}
				switch (text2[i])
				{
				case 'd':
					if ((num2 < 2 && num3 != -1) || (num2 >= 2 && num4 != -1))
					{
						return false;
					}
					switch (num2)
					{
					case 0:
						num3 = _ParseNumber(s, num, 1, 2, leadingzero: false, sloppy_parsing, out num_parsed);
						break;
					case 1:
						num3 = _ParseNumber(s, num, 1, 2, leadingzero: true, sloppy_parsing, out num_parsed);
						break;
					case 2:
						num4 = _ParseEnum(s, num, dfi.RawAbbreviatedDayNames, invariantInfo.RawAbbreviatedDayNames, exact, out num_parsed);
						break;
					default:
						num4 = _ParseEnum(s, num, dfi.RawDayNames, invariantInfo.RawDayNames, exact, out num_parsed);
						break;
					}
					break;
				case 'M':
					if (num5 != -1)
					{
						return false;
					}
					if (flag)
					{
						num_parsed = -1;
						if (num2 == 0 || num2 == 3)
						{
							num5 = _ParseNumber(s, num, 1, 2, leadingzero: false, sloppy_parsing, out num_parsed);
						}
						if (num2 > 1 && num_parsed == -1)
						{
							num5 = _ParseEnum(s, num, dfi.RawMonthNames, invariantInfo.RawMonthNames, exact, out num_parsed) + 1;
						}
						if (num2 > 1 && num_parsed == -1)
						{
							num5 = _ParseEnum(s, num, dfi.RawAbbreviatedMonthNames, invariantInfo.RawAbbreviatedMonthNames, exact, out num_parsed) + 1;
						}
						break;
					}
					switch (num2)
					{
					case 0:
						num5 = _ParseNumber(s, num, 1, 2, leadingzero: false, sloppy_parsing, out num_parsed);
						break;
					case 1:
						num5 = _ParseNumber(s, num, 1, 2, leadingzero: true, sloppy_parsing, out num_parsed);
						break;
					case 2:
						num5 = _ParseEnum(s, num, dfi.RawAbbreviatedMonthNames, invariantInfo.RawAbbreviatedMonthNames, exact, out num_parsed) + 1;
						break;
					default:
						num5 = _ParseEnum(s, num, dfi.RawMonthNames, invariantInfo.RawMonthNames, exact, out num_parsed) + 1;
						break;
					}
					break;
				case 'y':
					if (num6 != -1)
					{
						return false;
					}
					if (num2 == 0)
					{
						num6 = _ParseNumber(s, num, 1, 2, leadingzero: false, sloppy_parsing, out num_parsed);
					}
					else if (num2 < 3)
					{
						num6 = _ParseNumber(s, num, 1, 2, leadingzero: true, sloppy_parsing, out num_parsed);
					}
					else
					{
						num6 = _ParseNumber(s, num, (!exact) ? 3 : 4, 4, leadingzero: false, sloppy_parsing, out num_parsed);
						if (num6 >= 1000 && num_parsed == 4 && !longYear && s.Length > 4 + num)
						{
							int num_parsed2 = 0;
							int num14 = _ParseNumber(s, num, 5, 5, leadingzero: false, sloppy_parsing, out num_parsed2);
							longYear = (num14 > 9999);
						}
						num2 = 3;
					}
					if (num_parsed <= 2)
					{
						num6 += ((num6 >= 30) ? 1900 : 2000);
					}
					break;
				case 'h':
					if (num7 != -1)
					{
						return false;
					}
					num7 = ((num2 != 0) ? _ParseNumber(s, num, 1, 2, leadingzero: true, sloppy_parsing, out num_parsed) : _ParseNumber(s, num, 1, 2, leadingzero: false, sloppy_parsing, out num_parsed));
					if (num7 > 12)
					{
						return false;
					}
					if (num7 == 12)
					{
						num7 = 0;
					}
					break;
				case 'H':
					if (num7 != -1 || (!flag && ampm >= 0))
					{
						return false;
					}
					num7 = ((num2 != 0) ? _ParseNumber(s, num, 1, 2, leadingzero: true, sloppy_parsing, out num_parsed) : _ParseNumber(s, num, 1, 2, leadingzero: false, sloppy_parsing, out num_parsed));
					if (num7 >= 24)
					{
						return false;
					}
					break;
				case 'm':
					if (num8 != -1)
					{
						return false;
					}
					num8 = ((num2 != 0) ? _ParseNumber(s, num, 1, 2, leadingzero: true, sloppy_parsing, out num_parsed) : _ParseNumber(s, num, 1, 2, leadingzero: false, sloppy_parsing, out num_parsed));
					if (num8 >= 60)
					{
						return false;
					}
					break;
				case 's':
					if (num9 != -1)
					{
						return false;
					}
					num9 = ((num2 != 0) ? _ParseNumber(s, num, 1, 2, leadingzero: true, sloppy_parsing, out num_parsed) : _ParseNumber(s, num, 1, 2, leadingzero: false, sloppy_parsing, out num_parsed));
					if (num9 >= 60)
					{
						return false;
					}
					break;
				case 'F':
					leadingzero = false;
					goto case 'f';
				case 'f':
				{
					if (num2 > 6 || num10 != -1.0)
					{
						return false;
					}
					double num15 = _ParseNumber(s, num, 0, num2 + 1, leadingzero, sloppy_parsing, out num_parsed);
					if (num_parsed == -1)
					{
						return false;
					}
					num10 = num15 / Math.Pow(10.0, num_parsed);
					break;
				}
				case 't':
					if (!_ParseAmPm(s, num, (num2 <= 0) ? 1 : 0, dfi, exact, out num_parsed, ref ampm))
					{
						return false;
					}
					break;
				case 'z':
					if (num11 != -1)
					{
						return false;
					}
					if (s[num] == '+')
					{
						num11 = 0;
					}
					else
					{
						if (s[num] != '-')
						{
							return false;
						}
						num11 = 1;
					}
					num++;
					switch (num2)
					{
					case 0:
						num12 = _ParseNumber(s, num, 1, 2, leadingzero: false, sloppy_parsing, out num_parsed);
						break;
					case 1:
						num12 = _ParseNumber(s, num, 1, 2, leadingzero: true, sloppy_parsing, out num_parsed);
						break;
					default:
						num12 = _ParseNumber(s, num, 1, 2, leadingzero: true, sloppy_parsing: true, out num_parsed);
						num += num_parsed;
						if (num_parsed < 0)
						{
							return false;
						}
						num_parsed = 0;
						if ((num < s.Length && char.IsDigit(s[num])) || _ParseTimeSeparator(s, num, dfi, exact, out num_parsed))
						{
							num += num_parsed;
							num13 = _ParseNumber(s, num, 1, 2, leadingzero: true, sloppy_parsing, out num_parsed);
							if (num_parsed < 0)
							{
								return false;
							}
						}
						else
						{
							if (!flag)
							{
								return false;
							}
							num_parsed = 0;
						}
						break;
					}
					break;
				case 'K':
					if (s[num] == 'Z')
					{
						num++;
						useutc = true;
					}
					else if (s[num] == '+' || s[num] == '-')
					{
						if (num11 != -1)
						{
							return false;
						}
						if (s[num] == '+')
						{
							num11 = 0;
						}
						else if (s[num] == '-')
						{
							num11 = 1;
						}
						num++;
						num12 = _ParseNumber(s, num, 0, 2, leadingzero: true, sloppy_parsing, out num_parsed);
						num += num_parsed;
						if (num_parsed < 0)
						{
							return false;
						}
						if (char.IsDigit(s[num]))
						{
							num_parsed = 0;
						}
						else if (!_ParseString(s, num, 0, dfi.TimeSeparator, out num_parsed))
						{
							return false;
						}
						num += num_parsed;
						num13 = _ParseNumber(s, num, 0, 2, leadingzero: true, sloppy_parsing, out num_parsed);
						num2 = 2;
						if (num_parsed < 0)
						{
							return false;
						}
					}
					break;
				case 'Z':
					if (s[num] != 'Z')
					{
						return false;
					}
					num2 = 0;
					num_parsed = 1;
					useutc = true;
					break;
				case 'G':
					if (s[num] != 'G')
					{
						return false;
					}
					if (i + 2 < length && num + 2 < s.Length && text2[i + 1] == 'M' && s[num + 1] == 'M' && text2[i + 2] == 'T' && s[num + 2] == 'T')
					{
						useutc = true;
						num2 = 2;
						num_parsed = 3;
					}
					else
					{
						num2 = 0;
						num_parsed = 1;
					}
					break;
				case ':':
					if (!_ParseTimeSeparator(s, num, dfi, exact, out num_parsed))
					{
						return false;
					}
					break;
				case '/':
					if (!_ParseDateSeparator(s, num, dfi, exact, out num_parsed))
					{
						return false;
					}
					num2 = 0;
					break;
				default:
					if (s[num] != text2[i])
					{
						return false;
					}
					num2 = 0;
					num_parsed = 1;
					break;
				}
				if (num_parsed < 0)
				{
					return false;
				}
				num += num_parsed;
				if (!exact && !flag)
				{
					char c = text2[i];
					if ((c == 'F' || c == 'f' || c == 'm' || c == 's' || c == 'z') && s.Length > num && s[num] == 'Z' && (i + 1 == text2.Length || text2[i + 1] != 'Z'))
					{
						useutc = true;
						num++;
					}
				}
				i = i + num2 + 1;
				num2 = 0;
			}
			if (i + 1 < length && text2[i] == '.' && text2[i + 1] == 'F')
			{
				for (i++; i < length && text2[i] == 'F'; i++)
				{
				}
			}
			for (; i < length && text2[i] == 'K'; i++)
			{
			}
			if (i < length)
			{
				return false;
			}
			if (s.Length > num)
			{
				if (num == 0)
				{
					return false;
				}
				if (char.IsDigit(s[num]) && char.IsDigit(s[num - 1]))
				{
					return false;
				}
				if (char.IsLetter(s[num]) && char.IsLetter(s[num - 1]))
				{
					return false;
				}
				incompleteFormat = true;
				return false;
			}
			if (num7 == -1)
			{
				num7 = 0;
			}
			if (num8 == -1)
			{
				num8 = 0;
			}
			if (num9 == -1)
			{
				num9 = 0;
			}
			if (num10 == -1.0)
			{
				num10 = 0.0;
			}
			if (num3 == -1 && num5 == -1 && num6 == -1)
			{
				if ((style & DateTimeStyles.NoCurrentDateDefault) != 0)
				{
					num3 = 1;
					num5 = 1;
					num6 = 1;
				}
				else
				{
					num3 = Today.Day;
					num5 = Today.Month;
					num6 = Today.Year;
				}
			}
			if (num3 == -1)
			{
				num3 = 1;
			}
			if (num5 == -1)
			{
				num5 = 1;
			}
			if (num6 == -1)
			{
				num6 = (((style & DateTimeStyles.NoCurrentDateDefault) != 0) ? 1 : Today.Year);
			}
			if (ampm == 0 && num7 == 12)
			{
				num7 = 0;
			}
			if (ampm == 1 && (!flag || num7 < 12))
			{
				num7 += 12;
			}
			if (num6 < 1 || num6 > 9999 || num5 < 1 || num5 > 12 || num3 < 1 || num3 > DaysInMonth(num6, num5) || num7 < 0 || num7 > 23 || num8 < 0 || num8 > 59 || num9 < 0 || num9 > 59)
			{
				return false;
			}
			result = new DateTime(num6, num5, num3, num7, num8, num9, 0);
			result = result.AddSeconds(num10);
			if (num4 != -1 && num4 != (int)result.DayOfWeek)
			{
				return false;
			}
			if (num11 == -1)
			{
				if (result != MinValue)
				{
					try
					{
						dto = new DateTimeOffset(result);
					}
					catch
					{
					}
				}
			}
			else
			{
				if (num13 == -1)
				{
					num13 = 0;
				}
				if (num12 == -1)
				{
					num12 = 0;
				}
				if (num11 == 1)
				{
					num12 = -num12;
					num13 = -num13;
				}
				try
				{
					dto = new DateTimeOffset(result, new TimeSpan(num12, num13, 0));
				}
				catch
				{
				}
			}
			bool flag5 = (style & DateTimeStyles.AdjustToUniversal) != 0;
			if (num11 != -1)
			{
				long num16 = (result.ticks - dto.Offset).Ticks;
				if (num16 < 0)
				{
					num16 += 864000000000L;
				}
				result = new DateTime(check: false, new TimeSpan(num16));
				result.kind = DateTimeKind.Utc;
				if ((style & DateTimeStyles.RoundtripKind) != 0)
				{
					result = result.ToLocalTime();
				}
			}
			else if (useutc || (style & DateTimeStyles.AssumeUniversal) != 0)
			{
				result.kind = DateTimeKind.Utc;
			}
			else if ((style & DateTimeStyles.AssumeLocal) != 0)
			{
				result.kind = DateTimeKind.Local;
			}
			bool flag6 = !flag5 && (style & DateTimeStyles.RoundtripKind) == 0;
			if (result.kind != 0)
			{
				if (flag5)
				{
					result = result.ToUniversalTime();
				}
				else if (flag6)
				{
					result = result.ToLocalTime();
				}
			}
			return true;
		}

		/// <summary>Converts the specified string representation of a date and time to its <see cref="T:System.DateTime" /> equivalent using the specified format, culture-specific format information, and style. The format of the string representation must match the specified format exactly.</summary>
		/// <returns>A <see cref="T:System.DateTime" /> equivalent to the date and time contained in <paramref name="s" /> as specified by <paramref name="format" />, <paramref name="provider" />, and <paramref name="style" />.</returns>
		/// <param name="s">A string containing a date and time to convert. </param>
		/// <param name="format">The expected format of <paramref name="s" />. </param>
		/// <param name="provider">An <see cref="T:System.IFormatProvider" /> that supplies culture-specific formatting information about <paramref name="s" />. </param>
		/// <param name="style">A bitwise combination of <see cref="T:System.Globalization.DateTimeStyles" /> values that indicates the permitted format of <paramref name="s" />. A typical value to specify is <see cref="F:System.Globalization.DateTimeStyles.None" />.</param>
		/// <exception cref="T:System.ArgumentNullException">
		///   <paramref name="s" /> or <paramref name="format" /> is null. </exception>
		/// <exception cref="T:System.FormatException">
		///   <paramref name="s" /> or <paramref name="format" /> is an empty string. -or- <paramref name="s" /> does not contain a date and time that corresponds to the pattern specified in <paramref name="format" />. </exception>
		/// <exception cref="T:System.ArgumentException">
		///   <paramref name="style" /> contains an invalid combination of <see cref="T:System.Globalization.DateTimeStyles" /> values. For example, both <see cref="F:System.Globalization.DateTimeStyles.AssumeLocal" /> and <see cref="F:System.Globalization.DateTimeStyles.AssumeUniversal" />.</exception>
		/// <filterpriority>2</filterpriority>
		public static DateTime ParseExact(string s, string format, IFormatProvider provider, DateTimeStyles style)
		{
			if (format == null)
			{
				throw new ArgumentNullException("format");
			}
			return ParseExact(s, new string[1]
			{
				format
			}, provider, style);
		}

		/// <summary>Converts the specified string representation of a date and time to its <see cref="T:System.DateTime" /> equivalent using the specified array of formats, culture-specific format information, and style. The format of the string representation must match at least one of the specified formats exactly.</summary>
		/// <returns>A <see cref="T:System.DateTime" /> equivalent to the date and time contained in <paramref name="s" /> as specified by <paramref name="formats" />, <paramref name="provider" />, and <paramref name="style" />.</returns>
		/// <param name="s">A string containing one or more dates and times to convert. </param>
		/// <param name="formats">An array of expected formats of <paramref name="s" />. </param>
		/// <param name="provider">An <see cref="T:System.IFormatProvider" /> that supplies culture-specific format information about <paramref name="s" />. </param>
		/// <param name="style">A bitwise combination of <see cref="T:System.Globalization.DateTimeStyles" /> values that indicates the permitted format of <paramref name="s" />. A typical value to specify is <see cref="F:System.Globalization.DateTimeStyles.None" />.</param>
		/// <exception cref="T:System.ArgumentNullException">
		///   <paramref name="s" /> or <paramref name="formats" /> is null. </exception>
		/// <exception cref="T:System.FormatException">
		///   <paramref name="s" /> is an empty string. -or- an element of <paramref name="formats" /> is an empty string. -or- <paramref name="s" /> does not contain a date and time that corresponds to any element of <paramref name="formats" />. </exception>
		/// <exception cref="T:System.ArgumentException">
		///   <paramref name="style" /> contains an invalid combination of <see cref="T:System.Globalization.DateTimeStyles" /> values. For example, both <see cref="F:System.Globalization.DateTimeStyles.AssumeLocal" /> and <see cref="F:System.Globalization.DateTimeStyles.AssumeUniversal" />.</exception>
		/// <filterpriority>2</filterpriority>
		public static DateTime ParseExact(string s, string[] formats, IFormatProvider provider, DateTimeStyles style)
		{
			DateTimeFormatInfo instance = DateTimeFormatInfo.GetInstance(provider);
			CheckStyle(style);
			if (s == null)
			{
				throw new ArgumentNullException("s");
			}
			if (formats == null)
			{
				throw new ArgumentNullException("formats");
			}
			if (formats.Length == 0)
			{
				throw new FormatException("Format specifier was invalid.");
			}
			bool longYear = false;
			Exception exception = null;
			if (!ParseExact(s, formats, instance, style, out DateTime ret, exact: true, ref longYear, setExceptionOnError: true, ref exception))
			{
				throw exception;
			}
			return ret;
		}

		private static void CheckStyle(DateTimeStyles style)
		{
			if ((style & DateTimeStyles.RoundtripKind) != 0 && ((style & DateTimeStyles.AdjustToUniversal) != 0 || (style & DateTimeStyles.AssumeLocal) != 0 || (style & DateTimeStyles.AssumeUniversal) != 0))
			{
				throw new ArgumentException("The DateTimeStyles value RoundtripKind cannot be used with the values AssumeLocal, Asersal or AdjustToUniversal.", "style");
			}
			if ((style & DateTimeStyles.AssumeUniversal) != 0 && (style & DateTimeStyles.AssumeLocal) != 0)
			{
				throw new ArgumentException("The DateTimeStyles values AssumeLocal and AssumeUniversal cannot be used together.", "style");
			}
		}

		/// <summary>Converts the specified string representation of a date and time to its <see cref="T:System.DateTime" /> equivalent.</summary>
		/// <returns>true if the <paramref name="s" /> parameter was converted successfully; otherwise, false.</returns>
		/// <param name="s">A string containing a date and time to convert. </param>
		/// <param name="result">When this method returns, contains the <see cref="T:System.DateTime" /> value equivalent to the date and time contained in <paramref name="s" />, if the conversion succeeded, or <see cref="F:System.DateTime.MinValue" /> if the conversion failed. The conversion fails if the <paramref name="s" /> parameter is null, or does not contain a valid string representation of a date and time. This parameter is passed uninitialized. </param>
		/// <filterpriority>1</filterpriority>
		public static bool TryParse(string s, out DateTime result)
		{
			//Discarded unreachable code: IL_001c
			if (s != null)
			{
				try
				{
					Exception exception = null;
					DateTimeOffset dto;
					return CoreParse(s, null, DateTimeStyles.AllowWhiteSpaces, out result, out dto, setExceptionOnError: false, ref exception);
				}
				catch
				{
				}
			}
			result = MinValue;
			return false;
		}

		/// <summary>Converts the specified string representation of a date and time to its <see cref="T:System.DateTime" /> equivalent using the specified culture-specific format information and formatting style.</summary>
		/// <returns>true if the <paramref name="s" /> parameter was converted successfully; otherwise, false.</returns>
		/// <param name="s">A string containing a date and time to convert. </param>
		/// <param name="provider">An <see cref="T:System.IFormatProvider" /> object that supplies culture-specific formatting information about <paramref name="s" />. </param>
		/// <param name="styles">A bitwise combination of <see cref="T:System.Globalization.DateTimeStyles" /> values that indicates the permitted format of <paramref name="s" />. A typical value to specify is <see cref="F:System.Globalization.DateTimeStyles.None" />.</param>
		/// <param name="result">When this method returns, contains the <see cref="T:System.DateTime" /> value equivalent to the date and time contained in <paramref name="s" />, if the conversion succeeded, or <see cref="F:System.DateTime.MinValue" /> if the conversion failed. The conversion fails if the <paramref name="s" /> parameter is null, or does not contain a valid string representation of a date and time. This parameter is passed uninitialized. </param>
		/// <exception cref="T:System.ArgumentException">
		///   <paramref name="styles" /> is not a valid <see cref="T:System.Globalization.DateTimeStyles" /> value.-or-<paramref name="styles" /> contains an invalid combination of <see cref="T:System.Globalization.DateTimeStyles" /> values (for example, both <see cref="F:System.Globalization.DateTimeStyles.AssumeLocal" /> and <see cref="F:System.Globalization.DateTimeStyles.AssumeUniversal" />).</exception>
		/// <filterpriority>1</filterpriority>
		public static bool TryParse(string s, IFormatProvider provider, DateTimeStyles styles, out DateTime result)
		{
			//Discarded unreachable code: IL_001c
			if (s != null)
			{
				try
				{
					Exception exception = null;
					DateTimeOffset dto;
					return CoreParse(s, provider, styles, out result, out dto, setExceptionOnError: false, ref exception);
				}
				catch
				{
				}
			}
			result = MinValue;
			return false;
		}

		/// <summary>Converts the specified string representation of a date and time to its <see cref="T:System.DateTime" /> equivalent using the specified format, culture-specific format information, and style. The format of the string representation must match the specified format exactly.</summary>
		/// <returns>true if <paramref name="s" /> was converted successfully; otherwise, false.</returns>
		/// <param name="s">A string containing a date and time to convert. </param>
		/// <param name="format">The expected format of <paramref name="s" />. </param>
		/// <param name="provider">An <see cref="T:System.IFormatProvider" /> object that supplies culture-specific formatting information about <paramref name="s" />. </param>
		/// <param name="style">A bitwise combination of one or more <see cref="T:System.Globalization.DateTimeStyles" /> values that indicate the permitted format of <paramref name="s" />. </param>
		/// <param name="result">When this method returns, contains the <see cref="T:System.DateTime" /> value equivalent to the date and time contained in <paramref name="s" />, if the conversion succeeded, or <see cref="F:System.DateTime.MinValue" /> if the conversion failed. The conversion fails if either the <paramref name="s" /> or <paramref name="format" /> parameter is null, is an empty string, or does not contain a date and time that correspond to the pattern specified in <paramref name="format" />. This parameter is passed uninitialized. </param>
		/// <exception cref="T:System.ArgumentException">
		///   <paramref name="styles" /> is not a valid <see cref="T:System.Globalization.DateTimeStyles" /> value.-or-<paramref name="styles" /> contains an invalid combination of <see cref="T:System.Globalization.DateTimeStyles" /> values (for example, both <see cref="F:System.Globalization.DateTimeStyles.AssumeLocal" /> and <see cref="F:System.Globalization.DateTimeStyles.AssumeUniversal" />).</exception>
		/// <filterpriority>1</filterpriority>
		public static bool TryParseExact(string s, string format, IFormatProvider provider, DateTimeStyles style, out DateTime result)
		{
			return TryParseExact(s, new string[1]
			{
				format
			}, provider, style, out result);
		}

		/// <summary>Converts the specified string representation of a date and time to its <see cref="T:System.DateTime" /> equivalent using the specified array of formats, culture-specific format information, and style. The format of the string representation must match at least one of the specified formats exactly.</summary>
		/// <returns>true if the <paramref name="s" /> parameter was converted successfully; otherwise, false.</returns>
		/// <param name="s">A string containing one or more dates and times to convert. </param>
		/// <param name="formats">An array of expected formats of <paramref name="s" />. </param>
		/// <param name="provider">An <see cref="T:System.IFormatProvider" /> object that supplies culture-specific format information about <paramref name="s" />. </param>
		/// <param name="style">A bitwise combination of <see cref="T:System.Globalization.DateTimeStyles" /> values that indicates the permitted format of <paramref name="s" />. A typical value to specify is <see cref="F:System.Globalization.DateTimeStyles.None" />.</param>
		/// <param name="result">When this method returns, contains the <see cref="T:System.DateTime" /> value equivalent to the date and time contained in <paramref name="s" />, if the conversion succeeded, or <see cref="F:System.DateTime.MinValue" /> if the conversion failed. The conversion fails if <paramref name="s" /> or <paramref name="formats" /> is null, <paramref name="s" /> or an element of <paramref name="formats" /> is an empty string, or the format of <paramref name="s" /> is not exactly as specified by at least one of the format patterns in <paramref name="formats" />. This parameter is passed uninitialized. </param>
		/// <exception cref="T:System.ArgumentException">
		///   <paramref name="styles" /> is not a valid <see cref="T:System.Globalization.DateTimeStyles" /> value.-or-<paramref name="styles" /> contains an invalid combination of <see cref="T:System.Globalization.DateTimeStyles" /> values (for example, both <see cref="F:System.Globalization.DateTimeStyles.AssumeLocal" /> and <see cref="F:System.Globalization.DateTimeStyles.AssumeUniversal" />).</exception>
		/// <filterpriority>1</filterpriority>
		public static bool TryParseExact(string s, string[] formats, IFormatProvider provider, DateTimeStyles style, out DateTime result)
		{
			//Discarded unreachable code: IL_0022, IL_003b
			try
			{
				DateTimeFormatInfo instance = DateTimeFormatInfo.GetInstance(provider);
				bool longYear = false;
				Exception exception = null;
				return ParseExact(s, formats, instance, style, out result, exact: true, ref longYear, setExceptionOnError: false, ref exception);
			}
			catch
			{
				result = MinValue;
				return false;
			}
		}

		private static bool ParseExact(string s, string[] formats, DateTimeFormatInfo dfi, DateTimeStyles style, out DateTime ret, bool exact, ref bool longYear, bool setExceptionOnError, ref Exception exception)
		{
			bool incompleteFormat = false;
			for (int i = 0; i < formats.Length; i++)
			{
				string text = formats[i];
				if (text == null || text == string.Empty)
				{
					break;
				}
				if (_DoParse(s, formats[i], null, exact, out DateTime result, out DateTimeOffset _, dfi, style, firstPartIsDate: false, ref incompleteFormat, ref longYear))
				{
					ret = result;
					return true;
				}
			}
			if (setExceptionOnError)
			{
				exception = new FormatException("Invalid format string");
			}
			ret = MinValue;
			return false;
		}

		/// <summary>Subtracts the specified date and time from this instance.</summary>
		/// <returns>A <see cref="T:System.TimeSpan" /> interval equal to the date and time represented by this instance minus the date and time represented by <paramref name="value" />.</returns>
		/// <param name="value">An instance of <see cref="T:System.DateTime" />. </param>
		/// <exception cref="T:System.ArgumentOutOfRangeException">The result is less than <see cref="F:System.DateTime.MinValue" /> or greater than <see cref="F:System.DateTime.MaxValue" />. </exception>
		/// <filterpriority>2</filterpriority>
		public TimeSpan Subtract(DateTime value)
		{
			return new TimeSpan(ticks.Ticks) - value.ticks;
		}

		/// <summary>Subtracts the specified duration from this instance.</summary>
		/// <returns>A <see cref="T:System.DateTime" /> equal to the date and time represented by this instance minus the time interval represented by <paramref name="value" />.</returns>
		/// <param name="value">An instance of <see cref="T:System.TimeSpan" />. </param>
		/// <exception cref="T:System.ArgumentOutOfRangeException">The result is less than <see cref="F:System.DateTime.MinValue" /> or greater than <see cref="F:System.DateTime.MaxValue" />. </exception>
		/// <filterpriority>2</filterpriority>
		public DateTime Subtract(TimeSpan value)
		{
			TimeSpan value2 = new TimeSpan(ticks.Ticks) - value;
			DateTime result = new DateTime(check: true, value2);
			result.kind = kind;
			return result;
		}

		/// <summary>Converts the value of the current <see cref="T:System.DateTime" /> object to a Windows file time.</summary>
		/// <returns>The value of the current <see cref="T:System.DateTime" /> object expressed as a Windows file time.</returns>
		/// <exception cref="T:System.ArgumentOutOfRangeException">The resulting file time would represent a date and time before 12:00 midnight January 1, 1601 C.E. UTC. </exception>
		/// <filterpriority>2</filterpriority>
		public long ToFileTime()
		{
			DateTime dateTime = ToUniversalTime();
			if (dateTime.Ticks < 504911232000000000L)
			{
				throw new ArgumentOutOfRangeException("file time is not valid");
			}
			return dateTime.Ticks - 504911232000000000L;
		}

		/// <summary>Converts the value of the current <see cref="T:System.DateTime" /> object to a Windows file time.</summary>
		/// <returns>The value of the current <see cref="T:System.DateTime" /> object expressed as a Windows file time.</returns>
		/// <exception cref="T:System.ArgumentOutOfRangeException">The resulting file time would represent a date and time before 12:00 midnight January 1, 1601 C.E. UTC. </exception>
		/// <filterpriority>2</filterpriority>
		public long ToFileTimeUtc()
		{
			if (Ticks < 504911232000000000L)
			{
				throw new ArgumentOutOfRangeException("file time is not valid");
			}
			return Ticks - 504911232000000000L;
		}

		/// <summary>Converts the value of the current <see cref="T:System.DateTime" /> object to its equivalent long date string representation.</summary>
		/// <returns>A string that contains the long date string representation of the current <see cref="T:System.DateTime" /> object.</returns>
		/// <filterpriority>2</filterpriority>
		public string ToLongDateString()
		{
			return ToString("D");
		}

		/// <summary>Converts the value of the current <see cref="T:System.DateTime" /> object to its equivalent long time string representation.</summary>
		/// <returns>A string that contains the long time string representation of the current <see cref="T:System.DateTime" /> object.</returns>
		/// <filterpriority>2</filterpriority>
		public string ToLongTimeString()
		{
			return ToString("T");
		}

		/// <summary>Converts the value of this instance to the equivalent OLE Automation date.</summary>
		/// <returns>A double-precision floating-point number that contains an OLE Automation date equivalent to the value of this instance.</returns>
		/// <exception cref="T:System.OverflowException">The value of this instance cannot be represented as an OLE Automation Date. </exception>
		/// <filterpriority>2</filterpriority>
		public double ToOADate()
		{
			long num = Ticks;
			if (num == 0L)
			{
				return 0.0;
			}
			if (num < 31242239136000000L)
			{
				return -657434.999;
			}
			double num2 = new TimeSpan(Ticks - 599264352000000000L).TotalDays;
			if (num < 599264352000000000L)
			{
				double num3 = Math.Ceiling(num2);
				num2 = num3 - 2.0 - (num2 - num3);
			}
			else if (num2 >= 2958466.0)
			{
				num2 = 2958465.99999999;
			}
			return num2;
		}

		/// <summary>Converts the value of the current <see cref="T:System.DateTime" /> object to its equivalent short date string representation.</summary>
		/// <returns>A string that contains the short date string representation of the current <see cref="T:System.DateTime" /> object.</returns>
		/// <filterpriority>2</filterpriority>
		public string ToShortDateString()
		{
			return ToString("d");
		}

		/// <summary>Converts the value of the current <see cref="T:System.DateTime" /> object to its equivalent short time string representation.</summary>
		/// <returns>A string that contains the short time string representation of the current <see cref="T:System.DateTime" /> object.</returns>
		/// <filterpriority>2</filterpriority>
		public string ToShortTimeString()
		{
			return ToString("t");
		}

		/// <summary>Converts the value of the current <see cref="T:System.DateTime" /> object to its equivalent string representation.</summary>
		/// <returns>A string representation of the value of the current <see cref="T:System.DateTime" /> object.</returns>
		/// <filterpriority>1</filterpriority>
		public override string ToString()
		{
			return ToString("G", null);
		}

		/// <summary>Converts the value of the current <see cref="T:System.DateTime" /> object to its equivalent string representation using the specified culture-specific format information.</summary>
		/// <returns>A string representation of value of the current <see cref="T:System.DateTime" /> object as specified by <paramref name="provider" />.</returns>
		/// <param name="provider">An <see cref="T:System.IFormatProvider" /> that supplies culture-specific formatting information. </param>
		/// <filterpriority>1</filterpriority>
		public string ToString(IFormatProvider provider)
		{
			return ToString(null, provider);
		}

		/// <summary>Converts the value of the current <see cref="T:System.DateTime" /> object to its equivalent string representation using the specified format.</summary>
		/// <returns>A string representation of value of the current <see cref="T:System.DateTime" /> object as specified by <paramref name="format" />.</returns>
		/// <param name="format">A DateTime format string. </param>
		/// <exception cref="T:System.FormatException">The length of <paramref name="format" /> is 1, and it is not one of the format specifier characters defined for <see cref="T:System.Globalization.DateTimeFormatInfo" />.-or- <paramref name="format" /> does not contain a valid custom format pattern. </exception>
		/// <filterpriority>1</filterpriority>
		public string ToString(string format)
		{
			return ToString(format, null);
		}

		/// <summary>Converts the value of the current <see cref="T:System.DateTime" /> object to its equivalent string representation using the specified format and culture-specific format information.</summary>
		/// <returns>A string representation of value of the current <see cref="T:System.DateTime" /> object as specified by <paramref name="format" /> and <paramref name="provider" />.</returns>
		/// <param name="format">A DateTime format string. </param>
		/// <param name="provider">An <see cref="T:System.IFormatProvider" /> that supplies culture-specific formatting information. </param>
		/// <exception cref="T:System.FormatException">The length of <paramref name="format" /> is 1, and it is not one of the format specifier characters defined for <see cref="T:System.Globalization.DateTimeFormatInfo" />.-or- <paramref name="format" /> does not contain a valid custom format pattern. </exception>
		/// <filterpriority>1</filterpriority>
		public string ToString(string format, IFormatProvider provider)
		{
			DateTimeFormatInfo instance = DateTimeFormatInfo.GetInstance(provider);
			if (format == null || format == string.Empty)
			{
				format = "G";
			}
			bool useutc = false;
			bool use_invariant = false;
			if (format.Length == 1)
			{
				char c = format[0];
				format = DateTimeUtils.GetStandardPattern(c, instance, out useutc, out use_invariant);
				if (c == 'U')
				{
					return DateTimeUtils.ToString(ToUniversalTime(), format, instance);
				}
				if (format == null)
				{
					throw new FormatException("format is not one of the format specifier characters defined for DateTimeFormatInfo");
				}
			}
			return DateTimeUtils.ToString(this, format, instance);
		}

		/// <summary>Converts the value of the current <see cref="T:System.DateTime" /> object to local time.</summary>
		/// <returns>A <see cref="T:System.DateTime" /> object whose <see cref="P:System.DateTime.Kind" /> property is <see cref="F:System.DateTimeKind.Local" />, and whose value is the local time equivalent to the value of the current <see cref="T:System.DateTime" /> object, or <see cref="F:System.DateTime.MaxValue" /> if the converted value is too large to be represented by a <see cref="T:System.DateTime" /> object, or <see cref="F:System.DateTime.MinValue" /> if the converted value is too small to be represented as a <see cref="T:System.DateTime" /> object.</returns>
		/// <filterpriority>2</filterpriority>
		public DateTime ToLocalTime()
		{
			return TimeZone.CurrentTimeZone.ToLocalTime(this);
		}

		/// <summary>Converts the value of the current <see cref="T:System.DateTime" /> object to Coordinated Universal Time (UTC).</summary>
		/// <returns>A <see cref="T:System.DateTime" /> object whose <see cref="P:System.DateTime.Kind" /> property is <see cref="F:System.DateTimeKind.Utc" />, and whose value is the UTC equivalent to the value of the current <see cref="T:System.DateTime" /> object, or <see cref="F:System.DateTime.MaxValue" /> if the converted value is too large to be represented by a <see cref="T:System.DateTime" /> object, or <see cref="F:System.DateTime.MinValue" /> if the converted value is too small to be represented by a <see cref="T:System.DateTime" /> object.</returns>
		/// <filterpriority>2</filterpriority>
		public DateTime ToUniversalTime()
		{
			return TimeZone.CurrentTimeZone.ToUniversalTime(this);
		}

		/// <summary>Adds a specified time interval to a specified date and time, yielding a new date and time.</summary>
		/// <returns>A <see cref="T:System.DateTime" /> that is the sum of the values of <paramref name="d" /> and <paramref name="t" />.</returns>
		/// <param name="d">A <see cref="T:System.DateTime" />. </param>
		/// <param name="t">A <see cref="T:System.TimeSpan" />. </param>
		/// <exception cref="T:System.ArgumentOutOfRangeException">The resulting <see cref="T:System.DateTime" /> is less than <see cref="F:System.DateTime.MinValue" /> or greater than <see cref="F:System.DateTime.MaxValue" />. </exception>
		/// <filterpriority>3</filterpriority>
		public static DateTime operator +(DateTime d, TimeSpan t)
		{
			DateTime result = new DateTime(check: true, d.ticks + t);
			result.kind = d.kind;
			return result;
		}

		/// <summary>Determines whether two specified instances of <see cref="T:System.DateTime" /> are equal.</summary>
		/// <returns>true if <paramref name="d1" /> and <paramref name="d2" /> represent the same date and time; otherwise, false.</returns>
		/// <param name="d1">A <see cref="T:System.DateTime" />. </param>
		/// <param name="d2">A <see cref="T:System.DateTime" />. </param>
		/// <filterpriority>3</filterpriority>
		public static bool operator ==(DateTime d1, DateTime d2)
		{
			return d1.ticks == d2.ticks;
		}

		/// <summary>Determines whether one specified <see cref="T:System.DateTime" /> is greater than another specified <see cref="T:System.DateTime" />.</summary>
		/// <returns>true if <paramref name="t1" /> is greater than <paramref name="t2" />; otherwise, false.</returns>
		/// <param name="t1">A <see cref="T:System.DateTime" />. </param>
		/// <param name="t2">A <see cref="T:System.DateTime" />. </param>
		/// <filterpriority>3</filterpriority>
		public static bool operator >(DateTime t1, DateTime t2)
		{
			return t1.ticks > t2.ticks;
		}

		/// <summary>Determines whether one specified <see cref="T:System.DateTime" /> is greater than or equal to another specified <see cref="T:System.DateTime" />.</summary>
		/// <returns>true if <paramref name="t1" /> is greater than or equal to <paramref name="t2" />; otherwise, false.</returns>
		/// <param name="t1">A <see cref="T:System.DateTime" />. </param>
		/// <param name="t2">A <see cref="T:System.DateTime" />. </param>
		/// <filterpriority>3</filterpriority>
		public static bool operator >=(DateTime t1, DateTime t2)
		{
			return t1.ticks >= t2.ticks;
		}

		/// <summary>Determines whether two specified instances of <see cref="T:System.DateTime" /> are not equal.</summary>
		/// <returns>true if <paramref name="d1" /> and <paramref name="d2" /> do not represent the same date and time; otherwise, false.</returns>
		/// <param name="d1">A <see cref="T:System.DateTime" />. </param>
		/// <param name="d2">A <see cref="T:System.DateTime" />. </param>
		/// <filterpriority>3</filterpriority>
		public static bool operator !=(DateTime d1, DateTime d2)
		{
			return d1.ticks != d2.ticks;
		}

		/// <summary>Determines whether one specified <see cref="T:System.DateTime" /> is less than another specified <see cref="T:System.DateTime" />.</summary>
		/// <returns>true if <paramref name="t1" /> is less than <paramref name="t2" />; otherwise, false.</returns>
		/// <param name="t1">A <see cref="T:System.DateTime" />. </param>
		/// <param name="t2">A <see cref="T:System.DateTime" />. </param>
		/// <filterpriority>3</filterpriority>
		public static bool operator <(DateTime t1, DateTime t2)
		{
			return t1.ticks < t2.ticks;
		}

		/// <summary>Determines whether one specified <see cref="T:System.DateTime" /> is less than or equal to another specified <see cref="T:System.DateTime" />.</summary>
		/// <returns>true if <paramref name="t1" /> is less than or equal to <paramref name="t2" />; otherwise, false.</returns>
		/// <param name="t1">A <see cref="T:System.DateTime" />. </param>
		/// <param name="t2">A <see cref="T:System.DateTime" />. </param>
		/// <filterpriority>3</filterpriority>
		public static bool operator <=(DateTime t1, DateTime t2)
		{
			return t1.ticks <= t2.ticks;
		}

		/// <summary>Subtracts a specified date and time from another specified date and time, yielding a time interval.</summary>
		/// <returns>A <see cref="T:System.TimeSpan" /> that is the time interval between <paramref name="d1" /> and <paramref name="d2" />; that is, <paramref name="d1" /> minus <paramref name="d2" />.</returns>
		/// <param name="d1">A <see cref="T:System.DateTime" /> (the minuend). </param>
		/// <param name="d2">A <see cref="T:System.DateTime" /> (the subtrahend). </param>
		/// <filterpriority>3</filterpriority>
		public static TimeSpan operator -(DateTime d1, DateTime d2)
		{
			return new TimeSpan((d1.ticks - d2.ticks).Ticks);
		}

		/// <summary>Subtracts a specified time interval from a specified date and time, yielding a new date and time.</summary>
		/// <returns>A <see cref="T:System.DateTime" /> whose value is the value of <paramref name="d" /> minus the value of <paramref name="t" />.</returns>
		/// <param name="d">A <see cref="T:System.DateTime" />. </param>
		/// <param name="t">A <see cref="T:System.TimeSpan" />. </param>
		/// <exception cref="T:System.ArgumentOutOfRangeException">The resulting <see cref="T:System.DateTime" /> is less than <see cref="F:System.DateTime.MinValue" /> or greater than <see cref="F:System.DateTime.MaxValue" />. </exception>
		/// <filterpriority>3</filterpriority>
		public static DateTime operator -(DateTime d, TimeSpan t)
		{
			DateTime result = new DateTime(check: true, d.ticks - t);
			result.kind = d.kind;
			return result;
		}
	}
}
