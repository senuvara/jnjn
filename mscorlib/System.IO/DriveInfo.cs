using System.Collections;
using System.Runtime.CompilerServices;
using System.Runtime.InteropServices;
using System.Runtime.Serialization;
using System.Text;

namespace System.IO
{
	/// <summary>Provides access to information on a drive.</summary>
	/// <filterpriority>1</filterpriority>
	[Serializable]
	[ComVisible(true)]
	public sealed class DriveInfo : ISerializable
	{
		private enum _DriveType
		{
			GenericUnix,
			Linux,
			Windows
		}

		private _DriveType _drive_type;

		private string drive_format;

		private string path;

		/// <summary>Indicates the amount of available free space on a drive.</summary>
		/// <returns>The amount of free space available on the drive, in bytes.</returns>
		/// <exception cref="T:System.IO.IOException">An I/O error occurred (for example, a disk error or a drive was not ready). </exception>
		/// <filterpriority>1</filterpriority>
		public long AvailableFreeSpace
		{
			get
			{
				GetDiskFreeSpace(path, out ulong availableFreeSpace, out ulong _, out ulong _);
				return (long)((availableFreeSpace <= long.MaxValue) ? availableFreeSpace : long.MaxValue);
			}
		}

		/// <summary>Gets the total amount of free space available on a drive.</summary>
		/// <returns>The total free space available on a drive, in bytes.</returns>
		/// <exception cref="T:System.IO.IOException">An I/O error occurred (for example, a disk error or a drive was not ready). </exception>
		/// <filterpriority>1</filterpriority>
		public long TotalFreeSpace
		{
			get
			{
				GetDiskFreeSpace(path, out ulong _, out ulong _, out ulong totalFreeSpace);
				return (long)((totalFreeSpace <= long.MaxValue) ? totalFreeSpace : long.MaxValue);
			}
		}

		/// <summary>Gets the total size of storage space on a drive.</summary>
		/// <returns>The total size of the drive, in bytes.</returns>
		/// <exception cref="T:System.IO.IOException">An I/O error occurred (for example, a disk error or a drive was not ready). </exception>
		/// <filterpriority>1</filterpriority>
		public long TotalSize
		{
			get
			{
				GetDiskFreeSpace(path, out ulong _, out ulong totalSize, out ulong _);
				return (long)((totalSize <= long.MaxValue) ? totalSize : long.MaxValue);
			}
		}

		/// <summary>Gets or sets the volume label of a drive.</summary>
		/// <returns>The volume label.</returns>
		/// <exception cref="T:System.IO.IOException">An I/O error occurred (for example, a disk error or a drive was not ready). </exception>
		/// <exception cref="T:System.Security.SecurityException">The caller does not have the required permission. </exception>
		/// <exception cref="T:System.UnauthorizedAccessException">The volume label is being set on a network or CD-ROM drive. </exception>
		/// <filterpriority>1</filterpriority>
		/// <PermissionSet>
		///   <IPermission class="System.Security.Permissions.FileIOPermission, mscorlib, Version=2.0.3600.0, Culture=neutral, PublicKeyToken=b77a5c561934e089" version="1" Unrestricted="true" />
		/// </PermissionSet>
		[MonoTODO("Currently get only works on Mono/Unix; set not implemented")]
		public string VolumeLabel
		{
			get
			{
				if (_drive_type != _DriveType.Windows)
				{
					return path;
				}
				return path;
			}
			set
			{
				throw new NotImplementedException();
			}
		}

		/// <summary>Gets the name of the file system, such as NTFS or FAT32.</summary>
		/// <returns>The name of the file system on the specified drive.</returns>
		/// <exception cref="T:System.IO.IOException">An I/O error occurred (for example, a disk error or a drive was not ready). </exception>
		/// <filterpriority>1</filterpriority>
		public string DriveFormat => drive_format;

		/// <summary>Gets the drive type.</summary>
		/// <returns>One of the <see cref="T:System.IO.DriveType" /> values. </returns>
		/// <exception cref="T:System.IO.IOException">An I/O error occurred. </exception>
		/// <filterpriority>1</filterpriority>
		public DriveType DriveType => (DriveType)GetDriveTypeInternal(path);

		/// <summary>Gets the name of a drive.</summary>
		/// <returns>The name of the drive.</returns>
		/// <filterpriority>1</filterpriority>
		public string Name => path;

		/// <summary>Gets the root directory of a drive.</summary>
		/// <returns>A <see cref="T:System.IO.DirectoryInfo" /> object that contains the root directory of the drive.</returns>
		/// <filterpriority>1</filterpriority>
		/// <PermissionSet>
		///   <IPermission class="System.Security.Permissions.FileIOPermission, mscorlib, Version=2.0.3600.0, Culture=neutral, PublicKeyToken=b77a5c561934e089" version="1" Unrestricted="true" />
		/// </PermissionSet>
		public DirectoryInfo RootDirectory => new DirectoryInfo(path);

		/// <summary>Gets a value indicating whether a drive is ready.</summary>
		/// <returns>true if the drive is ready; false if the drive is not ready.</returns>
		/// <filterpriority>1</filterpriority>
		[MonoTODO("It always returns true")]
		public bool IsReady
		{
			get
			{
				if (_drive_type != _DriveType.Windows)
				{
					return true;
				}
				return true;
			}
		}

		private DriveInfo(_DriveType _drive_type, string path, string fstype)
		{
			this._drive_type = _drive_type;
			drive_format = fstype;
			this.path = path;
		}

		/// <summary>Provides access to information on the specified drive.</summary>
		/// <param name="driveName">A valid drive path or drive letter. This can be either uppercase or lowercase, 'a' to 'z'. A null value is not valid. </param>
		/// <exception cref="T:System.ArgumentNullException">The drive letter cannot be null. </exception>
		/// <exception cref="T:System.ArgumentException">The first letter of <paramref name="driveName" /> is not an uppercase or lowercase letter from 'a' to 'z'.-or-<paramref name="driveName" /> does not refer to a valid drive.</exception>
		public DriveInfo(string driveName)
		{
			DriveInfo[] drives = GetDrives();
			DriveInfo[] array = drives;
			foreach (DriveInfo driveInfo in array)
			{
				if (driveInfo.path == driveName)
				{
					path = driveInfo.path;
					drive_format = driveInfo.drive_format;
					path = driveInfo.path;
					return;
				}
			}
			throw new ArgumentException("The drive name does not exist", "driveName");
		}

		/// <summary>Populates a <see cref="T:System.Runtime.Serialization.SerializationInfo" /> object with the data needed to serialize the target object.</summary>
		/// <param name="info">The <see cref="T:System.Runtime.Serialization.SerializationInfo" /> object to populate with data.</param>
		/// <param name="context">The destination (see <see cref="T:System.Runtime.Serialization.StreamingContext" />) for this serialization.</param>
		void ISerializable.GetObjectData(SerializationInfo info, StreamingContext context)
		{
			throw new NotImplementedException();
		}

		private static void GetDiskFreeSpace(string path, out ulong availableFreeSpace, out ulong totalSize, out ulong totalFreeSpace)
		{
			if (!GetDiskFreeSpaceInternal(path, out availableFreeSpace, out totalSize, out totalFreeSpace, out MonoIOError error))
			{
				throw MonoIO.GetException(path, error);
			}
		}

		private static StreamReader TryOpen(string name)
		{
			if (File.Exists(name))
			{
				return new StreamReader(name, Encoding.ASCII);
			}
			return null;
		}

		private static DriveInfo[] LinuxGetDrives()
		{
			//Discarded unreachable code: IL_00d3
			using (StreamReader streamReader = TryOpen("/proc/mounts"))
			{
				ArrayList arrayList = new ArrayList();
				string text;
				while ((text = streamReader.ReadLine()) != null)
				{
					if (!text.StartsWith("rootfs"))
					{
						int num = text.IndexOf(' ');
						if (num != -1)
						{
							string text2 = text.Substring(num + 1);
							num = text2.IndexOf(' ');
							if (num != -1)
							{
								string text3 = text2.Substring(0, num);
								text2 = text2.Substring(num + 1);
								num = text2.IndexOf(' ');
								if (num != -1)
								{
									string fstype = text2.Substring(0, num);
									arrayList.Add(new DriveInfo(_DriveType.Linux, text3, fstype));
								}
							}
						}
					}
				}
				return (DriveInfo[])arrayList.ToArray(typeof(DriveInfo));
			}
		}

		private static DriveInfo[] UnixGetDrives()
		{
			DriveInfo[] array = null;
			try
			{
				using (StreamReader streamReader = TryOpen("/proc/sys/kernel/ostype"))
				{
					if (streamReader != null)
					{
						string a = streamReader.ReadLine();
						if (a == "Linux")
						{
							array = LinuxGetDrives();
						}
					}
				}
				if (array != null)
				{
					return array;
				}
			}
			catch (Exception)
			{
			}
			return new DriveInfo[1]
			{
				new DriveInfo(_DriveType.GenericUnix, "/", "unixfs")
			};
		}

		private static DriveInfo[] WindowsGetDrives()
		{
			throw new NotImplementedException();
		}

		/// <summary>Retrieves the drive names of all logical drives on a computer.</summary>
		/// <returns>An array of type <see cref="T:System.IO.DriveInfo" /> that represents the logical drives on a computer.</returns>
		/// <exception cref="T:System.IO.IOException">An I/O error occurred (for example, a disk error or a drive was not ready). </exception>
		/// <exception cref="T:System.UnauthorizedAccessException">The caller does not have the required permission. </exception>
		/// <filterpriority>1</filterpriority>
		/// <PermissionSet>
		///   <IPermission class="System.Security.Permissions.FileIOPermission, mscorlib, Version=2.0.3600.0, Culture=neutral, PublicKeyToken=b77a5c561934e089" version="1" Unrestricted="true" />
		///   <IPermission class="System.Security.Permissions.SecurityPermission, mscorlib, Version=2.0.3600.0, Culture=neutral, PublicKeyToken=b77a5c561934e089" version="1" Flags="UnmanagedCode" />
		/// </PermissionSet>
		[MonoTODO("Currently only implemented on Mono/Linux")]
		public static DriveInfo[] GetDrives()
		{
			int platform = (int)Environment.Platform;
			if (platform == 4 || platform == 128 || platform == 6)
			{
				return UnixGetDrives();
			}
			return WindowsGetDrives();
		}

		/// <summary>Returns a drive name as a string.</summary>
		/// <returns>The name of the drive.</returns>
		/// <filterpriority>1</filterpriority>
		public override string ToString()
		{
			return Name;
		}

		[MethodImpl(MethodImplOptions.InternalCall)]
		private static extern bool GetDiskFreeSpaceInternal(string pathName, out ulong freeBytesAvail, out ulong totalNumberOfBytes, out ulong totalNumberOfFreeBytes, out MonoIOError error);

		[MethodImpl(MethodImplOptions.InternalCall)]
		private static extern uint GetDriveTypeInternal(string rootPathName);
	}
}
