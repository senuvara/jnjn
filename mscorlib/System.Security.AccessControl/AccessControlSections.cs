namespace System.Security.AccessControl
{
	/// <summary>Specifies which sections of a security descriptor to save or load.</summary>
	[Flags]
	public enum AccessControlSections
	{
		/// <summary>No sections.</summary>
		None = 0x0,
		/// <summary>The system access control list (SACL).</summary>
		Audit = 0x1,
		/// <summary>The discretionary access control list (DACL).</summary>
		Access = 0x2,
		/// <summary>The owner.</summary>
		Owner = 0x4,
		/// <summary>The primary group.</summary>
		Group = 0x8,
		/// <summary>The entire security descriptor.</summary>
		All = 0xF
	}
}
