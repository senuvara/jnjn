namespace System.Security.AccessControl
{
	/// <summary>Specifies the presence of object types for Access Control Entries (ACEs).</summary>
	[Flags]
	public enum ObjectAceFlags
	{
		/// <summary>No object types are present.</summary>
		None = 0x0,
		/// <summary>The type of object that is associated with the ACE is present.</summary>
		ObjectAceTypePresent = 0x1,
		/// <summary>The type of object that can inherit the ACE.</summary>
		InheritedObjectAceTypePresent = 0x2
	}
}
