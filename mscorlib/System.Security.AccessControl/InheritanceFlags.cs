namespace System.Security.AccessControl
{
	/// <summary>Inheritance flags specify the semantics of inheritance for access control entries (ACEs).</summary>
	[Flags]
	public enum InheritanceFlags
	{
		/// <summary>The ACE is not inherited by child objects.</summary>
		None = 0x0,
		/// <summary>The ACE is inherited by child container objects.</summary>
		ContainerInherit = 0x1,
		/// <summary>The ACE is inherited by child leaf objects.</summary>
		ObjectInherit = 0x2
	}
}
