namespace System.Security.AccessControl
{
	/// <summary>Specifies how Access Control Entries (ACEs) are propagated to child objects.  These flags are significant only if inheritance flags are present. </summary>
	[Flags]
	public enum PropagationFlags
	{
		None = 0x0,
		/// <summary>Specifies that the ACE is not propagated to child objects.</summary>
		NoPropagateInherit = 0x1,
		/// <summary>Specifies that the ACE is propagated only to child objects. This includes both container and leaf child objects. </summary>
		InheritOnly = 0x2
	}
}
