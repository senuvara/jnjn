using System.Diagnostics;
using System.Runtime.InteropServices;

namespace System.Collections
{
	/// <summary>Represents a simple last-in-first-out (LIFO) non-generic collection of objects.</summary>
	/// <filterpriority>1</filterpriority>
	[Serializable]
	[DebuggerDisplay("Count={Count}")]
	[DebuggerTypeProxy(typeof(CollectionDebuggerView))]
	[ComVisible(true)]
	public class Stack : IEnumerable, ICloneable, ICollection
	{
		[Serializable]
		private class SyncStack : Stack
		{
			private Stack stack;

			public override int Count
			{
				get
				{
					//Discarded unreachable code: IL_001e
					lock (stack)
					{
						return stack.Count;
					}
				}
			}

			public override bool IsSynchronized => true;

			public override object SyncRoot => stack.SyncRoot;

			internal SyncStack(Stack s)
			{
				stack = s;
			}

			public override void Clear()
			{
				lock (stack)
				{
					stack.Clear();
				}
			}

			public override object Clone()
			{
				//Discarded unreachable code: IL_0028
				lock (stack)
				{
					return Synchronized((Stack)stack.Clone());
				}
			}

			public override bool Contains(object obj)
			{
				//Discarded unreachable code: IL_001f
				lock (stack)
				{
					return stack.Contains(obj);
				}
			}

			public override void CopyTo(Array array, int index)
			{
				lock (stack)
				{
					stack.CopyTo(array, index);
				}
			}

			public override IEnumerator GetEnumerator()
			{
				//Discarded unreachable code: IL_001e
				lock (stack)
				{
					return new Enumerator(stack);
				}
			}

			public override object Peek()
			{
				//Discarded unreachable code: IL_001e
				lock (stack)
				{
					return stack.Peek();
				}
			}

			public override object Pop()
			{
				//Discarded unreachable code: IL_001e
				lock (stack)
				{
					return stack.Pop();
				}
			}

			public override void Push(object obj)
			{
				lock (stack)
				{
					stack.Push(obj);
				}
			}

			public override object[] ToArray()
			{
				//Discarded unreachable code: IL_001e
				lock (stack)
				{
					return stack.ToArray();
				}
			}
		}

		private class Enumerator : IEnumerator, ICloneable
		{
			private const int EOF = -1;

			private const int BOF = -2;

			private Stack stack;

			private int modCount;

			private int current;

			public virtual object Current
			{
				get
				{
					if (modCount != stack.modCount || current == -2 || current == -1 || current > stack.count)
					{
						throw new InvalidOperationException();
					}
					return stack.contents[current];
				}
			}

			internal Enumerator(Stack s)
			{
				stack = s;
				modCount = s.modCount;
				current = -2;
			}

			public object Clone()
			{
				return MemberwiseClone();
			}

			public virtual bool MoveNext()
			{
				if (modCount != stack.modCount)
				{
					throw new InvalidOperationException();
				}
				switch (current)
				{
				case -2:
					current = stack.current;
					return current != -1;
				case -1:
					return false;
				default:
					current--;
					return current != -1;
				}
			}

			public virtual void Reset()
			{
				if (modCount != stack.modCount)
				{
					throw new InvalidOperationException();
				}
				current = -2;
			}
		}

		private const int default_capacity = 16;

		private object[] contents;

		private int current = -1;

		private int count;

		private int capacity;

		private int modCount;

		/// <summary>Gets the number of elements contained in the <see cref="T:System.Collections.Stack" />.</summary>
		/// <returns>The number of elements contained in the <see cref="T:System.Collections.Stack" />.</returns>
		/// <filterpriority>2</filterpriority>
		public virtual int Count => count;

		/// <summary>Gets a value indicating whether access to the <see cref="T:System.Collections.Stack" /> is synchronized (thread safe).</summary>
		/// <returns>true, if access to the <see cref="T:System.Collections.Stack" /> is synchronized (thread safe); otherwise, false. The default is false.</returns>
		/// <filterpriority>2</filterpriority>
		public virtual bool IsSynchronized => false;

		/// <summary>Gets an object that can be used to synchronize access to the <see cref="T:System.Collections.Stack" />.</summary>
		/// <returns>An <see cref="T:System.Object" /> that can be used to synchronize access to the <see cref="T:System.Collections.Stack" />.</returns>
		/// <filterpriority>2</filterpriority>
		public virtual object SyncRoot => this;

		/// <summary>Initializes a new instance of the <see cref="T:System.Collections.Stack" /> class that is empty and has the default initial capacity.</summary>
		public Stack()
		{
			contents = new object[16];
			capacity = 16;
		}

		/// <summary>Initializes a new instance of the <see cref="T:System.Collections.Stack" /> class that contains elements copied from the specified collection and has the same initial capacity as the number of elements copied.</summary>
		/// <param name="col">The <see cref="T:System.Collections.ICollection" /> to copy elements from. </param>
		/// <exception cref="T:System.ArgumentNullException">
		///   <paramref name="col" /> is null. </exception>
		public Stack(ICollection col)
			: this(col?.Count ?? 16)
		{
			if (col == null)
			{
				throw new ArgumentNullException("col");
			}
			foreach (object item in col)
			{
				Push(item);
			}
		}

		/// <summary>Initializes a new instance of the <see cref="T:System.Collections.Stack" /> class that is empty and has the specified initial capacity or the default initial capacity, whichever is greater.</summary>
		/// <param name="initialCapacity">The initial number of elements that the <see cref="T:System.Collections.Stack" /> can contain. </param>
		/// <exception cref="T:System.ArgumentOutOfRangeException">
		///   <paramref name="initialCapacity" /> is less than zero. </exception>
		public Stack(int initialCapacity)
		{
			if (initialCapacity < 0)
			{
				throw new ArgumentOutOfRangeException("initialCapacity");
			}
			capacity = initialCapacity;
			contents = new object[capacity];
		}

		private void Resize(int ncapacity)
		{
			ncapacity = Math.Max(ncapacity, 16);
			object[] destinationArray = new object[ncapacity];
			Array.Copy(contents, destinationArray, count);
			capacity = ncapacity;
			contents = destinationArray;
		}

		/// <summary>Returns a synchronized (thread safe) wrapper for the <see cref="T:System.Collections.Stack" />.</summary>
		/// <returns>A synchronized wrapper around the <see cref="T:System.Collections.Stack" />.</returns>
		/// <param name="stack">The <see cref="T:System.Collections.Stack" /> to synchronize. </param>
		/// <exception cref="T:System.ArgumentNullException">
		///   <paramref name="stack" /> is null. </exception>
		/// <filterpriority>2</filterpriority>
		public static Stack Synchronized(Stack stack)
		{
			if (stack == null)
			{
				throw new ArgumentNullException("stack");
			}
			return new SyncStack(stack);
		}

		/// <summary>Removes all objects from the <see cref="T:System.Collections.Stack" />.</summary>
		/// <filterpriority>2</filterpriority>
		public virtual void Clear()
		{
			modCount++;
			for (int i = 0; i < count; i++)
			{
				contents[i] = null;
			}
			count = 0;
			current = -1;
		}

		/// <summary>Creates a shallow copy of the <see cref="T:System.Collections.Stack" />.</summary>
		/// <returns>A shallow copy of the <see cref="T:System.Collections.Stack" />.</returns>
		/// <filterpriority>2</filterpriority>
		public virtual object Clone()
		{
			Stack stack = new Stack(contents);
			stack.current = current;
			stack.count = count;
			return stack;
		}

		/// <summary>Determines whether an element is in the <see cref="T:System.Collections.Stack" />.</summary>
		/// <returns>true, if <paramref name="obj" /> is found in the <see cref="T:System.Collections.Stack" />; otherwise, false.</returns>
		/// <param name="obj">The <see cref="T:System.Object" /> to locate in the <see cref="T:System.Collections.Stack" />. The value can be null. </param>
		/// <filterpriority>2</filterpriority>
		public virtual bool Contains(object obj)
		{
			if (count == 0)
			{
				return false;
			}
			if (obj == null)
			{
				for (int i = 0; i < count; i++)
				{
					if (contents[i] == null)
					{
						return true;
					}
				}
			}
			else
			{
				for (int j = 0; j < count; j++)
				{
					if (obj.Equals(contents[j]))
					{
						return true;
					}
				}
			}
			return false;
		}

		/// <summary>Copies the <see cref="T:System.Collections.Stack" /> to an existing one-dimensional <see cref="T:System.Array" />, starting at the specified array index.</summary>
		/// <param name="array">The one-dimensional <see cref="T:System.Array" /> that is the destination of the elements copied from <see cref="T:System.Collections.Stack" />. The <see cref="T:System.Array" /> must have zero-based indexing. </param>
		/// <param name="index">The zero-based index in <paramref name="array" /> at which copying begins. </param>
		/// <exception cref="T:System.ArgumentNullException">
		///   <paramref name="array" /> is null. </exception>
		/// <exception cref="T:System.ArgumentOutOfRangeException">
		///   <paramref name="index" /> is less than zero. </exception>
		/// <exception cref="T:System.ArgumentException">
		///   <paramref name="array" /> is multidimensional.-or- <paramref name="index" /> is equal to or greater than the length of <paramref name="array" />.-or- The number of elements in the source <see cref="T:System.Collections.Stack" /> is greater than the available space from <paramref name="index" /> to the end of the destination <paramref name="array" />. </exception>
		/// <exception cref="T:System.InvalidCastException">The type of the source <see cref="T:System.Collections.Stack" /> cannot be cast automatically to the type of the destination <paramref name="array" />. </exception>
		/// <filterpriority>2</filterpriority>
		public virtual void CopyTo(Array array, int index)
		{
			if (array == null)
			{
				throw new ArgumentNullException("array");
			}
			if (index < 0)
			{
				throw new ArgumentOutOfRangeException("index");
			}
			if (array.Rank > 1 || (array.Length > 0 && index >= array.Length) || count > array.Length - index)
			{
				throw new ArgumentException();
			}
			for (int num = current; num != -1; num--)
			{
				array.SetValue(contents[num], count - (num + 1) + index);
			}
		}

		/// <summary>Returns an <see cref="T:System.Collections.IEnumerator" /> for the <see cref="T:System.Collections.Stack" />.</summary>
		/// <returns>An <see cref="T:System.Collections.IEnumerator" /> for the <see cref="T:System.Collections.Stack" />.</returns>
		/// <filterpriority>2</filterpriority>
		public virtual IEnumerator GetEnumerator()
		{
			return new Enumerator(this);
		}

		/// <summary>Returns the object at the top of the <see cref="T:System.Collections.Stack" /> without removing it.</summary>
		/// <returns>The <see cref="T:System.Object" /> at the top of the <see cref="T:System.Collections.Stack" />.</returns>
		/// <exception cref="T:System.InvalidOperationException">The <see cref="T:System.Collections.Stack" /> is empty. </exception>
		/// <filterpriority>2</filterpriority>
		public virtual object Peek()
		{
			if (current == -1)
			{
				throw new InvalidOperationException();
			}
			return contents[current];
		}

		/// <summary>Removes and returns the object at the top of the <see cref="T:System.Collections.Stack" />.</summary>
		/// <returns>The <see cref="T:System.Object" /> removed from the top of the <see cref="T:System.Collections.Stack" />.</returns>
		/// <exception cref="T:System.InvalidOperationException">The <see cref="T:System.Collections.Stack" /> is empty. </exception>
		/// <filterpriority>2</filterpriority>
		public virtual object Pop()
		{
			if (current == -1)
			{
				throw new InvalidOperationException();
			}
			modCount++;
			object result = contents[current];
			contents[current] = null;
			count--;
			current--;
			if (count <= capacity / 4 && count > 16)
			{
				Resize(capacity / 2);
			}
			return result;
		}

		/// <summary>Inserts an object at the top of the <see cref="T:System.Collections.Stack" />.</summary>
		/// <param name="obj">The <see cref="T:System.Object" /> to push onto the <see cref="T:System.Collections.Stack" />. The value can be null. </param>
		/// <filterpriority>2</filterpriority>
		public virtual void Push(object obj)
		{
			modCount++;
			if (capacity == count)
			{
				Resize(capacity * 2);
			}
			count++;
			current++;
			contents[current] = obj;
		}

		/// <summary>Copies the <see cref="T:System.Collections.Stack" /> to a new array.</summary>
		/// <returns>A new array containing copies of the elements of the <see cref="T:System.Collections.Stack" />.</returns>
		/// <filterpriority>2</filterpriority>
		public virtual object[] ToArray()
		{
			object[] array = new object[count];
			Array.Copy(contents, array, count);
			Array.Reverse(array);
			return array;
		}
	}
}
