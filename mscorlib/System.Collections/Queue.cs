using System.Diagnostics;
using System.Runtime.InteropServices;

namespace System.Collections
{
	/// <summary>Represents a first-in, first-out collection of objects.</summary>
	/// <filterpriority>1</filterpriority>
	[Serializable]
	[ComVisible(true)]
	[DebuggerDisplay("Count={Count}")]
	[DebuggerTypeProxy(typeof(CollectionDebuggerView))]
	public class Queue : IEnumerable, ICloneable, ICollection
	{
		private class SyncQueue : Queue
		{
			private Queue queue;

			public override int Count
			{
				get
				{
					//Discarded unreachable code: IL_001e
					lock (queue)
					{
						return queue.Count;
					}
				}
			}

			public override bool IsSynchronized => true;

			public override object SyncRoot => queue.SyncRoot;

			internal SyncQueue(Queue queue)
			{
				this.queue = queue;
			}

			public override void CopyTo(Array array, int index)
			{
				lock (queue)
				{
					queue.CopyTo(array, index);
				}
			}

			public override IEnumerator GetEnumerator()
			{
				//Discarded unreachable code: IL_001e
				lock (queue)
				{
					return queue.GetEnumerator();
				}
			}

			public override object Clone()
			{
				//Discarded unreachable code: IL_0028
				lock (queue)
				{
					return new SyncQueue((Queue)queue.Clone());
				}
			}

			public override void Clear()
			{
				lock (queue)
				{
					queue.Clear();
				}
			}

			public override void TrimToSize()
			{
				lock (queue)
				{
					queue.TrimToSize();
				}
			}

			public override bool Contains(object obj)
			{
				//Discarded unreachable code: IL_001f
				lock (queue)
				{
					return queue.Contains(obj);
				}
			}

			public override object Dequeue()
			{
				//Discarded unreachable code: IL_001e
				lock (queue)
				{
					return queue.Dequeue();
				}
			}

			public override void Enqueue(object obj)
			{
				lock (queue)
				{
					queue.Enqueue(obj);
				}
			}

			public override object Peek()
			{
				//Discarded unreachable code: IL_001e
				lock (queue)
				{
					return queue.Peek();
				}
			}

			public override object[] ToArray()
			{
				//Discarded unreachable code: IL_001e
				lock (queue)
				{
					return queue.ToArray();
				}
			}
		}

		[Serializable]
		private class QueueEnumerator : IEnumerator, ICloneable
		{
			private Queue queue;

			private int _version;

			private int current;

			public virtual object Current
			{
				get
				{
					if (_version != queue._version || current < 0 || current >= queue._size)
					{
						throw new InvalidOperationException();
					}
					return queue._array[(queue._head + current) % queue._array.Length];
				}
			}

			internal QueueEnumerator(Queue q)
			{
				queue = q;
				_version = q._version;
				current = -1;
			}

			public object Clone()
			{
				QueueEnumerator queueEnumerator = new QueueEnumerator(queue);
				queueEnumerator._version = _version;
				queueEnumerator.current = current;
				return queueEnumerator;
			}

			public virtual bool MoveNext()
			{
				if (_version != queue._version)
				{
					throw new InvalidOperationException();
				}
				if (current >= queue._size - 1)
				{
					current = int.MaxValue;
					return false;
				}
				current++;
				return true;
			}

			public virtual void Reset()
			{
				if (_version != queue._version)
				{
					throw new InvalidOperationException();
				}
				current = -1;
			}
		}

		private object[] _array;

		private int _head;

		private int _size;

		private int _tail;

		private int _growFactor;

		private int _version;

		/// <summary>Gets the number of elements contained in the <see cref="T:System.Collections.Queue" />.</summary>
		/// <returns>The number of elements contained in the <see cref="T:System.Collections.Queue" />.</returns>
		/// <filterpriority>2</filterpriority>
		public virtual int Count => _size;

		/// <summary>Gets a value indicating whether access to the <see cref="T:System.Collections.Queue" /> is synchronized (thread safe).</summary>
		/// <returns>true if access to the <see cref="T:System.Collections.Queue" /> is synchronized (thread safe); otherwise, false. The default is false.</returns>
		/// <filterpriority>2</filterpriority>
		public virtual bool IsSynchronized => false;

		/// <summary>Gets an object that can be used to synchronize access to the <see cref="T:System.Collections.Queue" />.</summary>
		/// <returns>An object that can be used to synchronize access to the <see cref="T:System.Collections.Queue" />.</returns>
		/// <filterpriority>2</filterpriority>
		public virtual object SyncRoot => this;

		/// <summary>Initializes a new instance of the <see cref="T:System.Collections.Queue" /> class that is empty, has the default initial capacity, and uses the default growth factor.</summary>
		public Queue()
			: this(32, 2f)
		{
		}

		/// <summary>Initializes a new instance of the <see cref="T:System.Collections.Queue" /> class that is empty, has the specified initial capacity, and uses the default growth factor.</summary>
		/// <param name="capacity">The initial number of elements that the <see cref="T:System.Collections.Queue" /> can contain. </param>
		/// <exception cref="T:System.ArgumentOutOfRangeException">
		///   <paramref name="capacity" /> is less than zero. </exception>
		public Queue(int capacity)
			: this(capacity, 2f)
		{
		}

		/// <summary>Initializes a new instance of the <see cref="T:System.Collections.Queue" /> class that contains elements copied from the specified collection, has the same initial capacity as the number of elements copied, and uses the default growth factor.</summary>
		/// <param name="col">The <see cref="T:System.Collections.ICollection" /> to copy elements from. </param>
		/// <exception cref="T:System.ArgumentNullException">
		///   <paramref name="col" /> is null. </exception>
		public Queue(ICollection col)
			: this(col?.Count ?? 32)
		{
			if (col == null)
			{
				throw new ArgumentNullException("col");
			}
			foreach (object item in col)
			{
				Enqueue(item);
			}
		}

		/// <summary>Initializes a new instance of the <see cref="T:System.Collections.Queue" /> class that is empty, has the specified initial capacity, and uses the specified growth factor.</summary>
		/// <param name="capacity">The initial number of elements that the <see cref="T:System.Collections.Queue" /> can contain. </param>
		/// <param name="growFactor">The factor by which the capacity of the <see cref="T:System.Collections.Queue" /> is expanded. </param>
		/// <exception cref="T:System.ArgumentOutOfRangeException">
		///   <paramref name="capacity" /> is less than zero.-or- <paramref name="growFactor" /> is less than 1.0 or greater than 10.0. </exception>
		public Queue(int capacity, float growFactor)
		{
			if (capacity < 0)
			{
				throw new ArgumentOutOfRangeException("capacity", "Needs a non-negative number");
			}
			if (!(growFactor >= 1f) || !(growFactor <= 10f))
			{
				throw new ArgumentOutOfRangeException("growFactor", "Queue growth factor must be between 1.0 and 10.0, inclusive");
			}
			_array = new object[capacity];
			_growFactor = (int)(growFactor * 100f);
		}

		/// <summary>Copies the <see cref="T:System.Collections.Queue" /> elements to an existing one-dimensional <see cref="T:System.Array" />, starting at the specified array index.</summary>
		/// <param name="array">The one-dimensional <see cref="T:System.Array" /> that is the destination of the elements copied from <see cref="T:System.Collections.Queue" />. The <see cref="T:System.Array" /> must have zero-based indexing. </param>
		/// <param name="index">The zero-based index in <paramref name="array" /> at which copying begins. </param>
		/// <exception cref="T:System.ArgumentNullException">
		///   <paramref name="array" /> is null. </exception>
		/// <exception cref="T:System.ArgumentOutOfRangeException">
		///   <paramref name="index" /> is less than zero. </exception>
		/// <exception cref="T:System.ArgumentException">
		///   <paramref name="array" /> is multidimensional.-or- <paramref name="index" /> is equal to or greater than the length of <paramref name="array" />.-or- The number of elements in the source <see cref="T:System.Collections.Queue" /> is greater than the available space from <paramref name="index" /> to the end of the destination <paramref name="array" />. </exception>
		/// <exception cref="T:System.ArrayTypeMismatchException">The type of the source <see cref="T:System.Collections.Queue" /> cannot be cast automatically to the type of the destination <paramref name="array" />. </exception>
		/// <filterpriority>2</filterpriority>
		public virtual void CopyTo(Array array, int index)
		{
			if (array == null)
			{
				throw new ArgumentNullException("array");
			}
			if (index < 0)
			{
				throw new ArgumentOutOfRangeException("index");
			}
			if (array.Rank > 1 || (index != 0 && index >= array.Length) || _size > array.Length - index)
			{
				throw new ArgumentException();
			}
			int num = _array.Length;
			int num2 = num - _head;
			Array.Copy(_array, _head, array, index, Math.Min(_size, num2));
			if (_size > num2)
			{
				Array.Copy(_array, 0, array, index + num2, _size - num2);
			}
		}

		/// <summary>Returns an enumerator that iterates through the <see cref="T:System.Collections.Queue" />.</summary>
		/// <returns>An <see cref="T:System.Collections.IEnumerator" /> for the <see cref="T:System.Collections.Queue" />.</returns>
		/// <filterpriority>2</filterpriority>
		public virtual IEnumerator GetEnumerator()
		{
			return new QueueEnumerator(this);
		}

		/// <summary>Creates a shallow copy of the <see cref="T:System.Collections.Queue" />.</summary>
		/// <returns>A shallow copy of the <see cref="T:System.Collections.Queue" />.</returns>
		/// <filterpriority>2</filterpriority>
		public virtual object Clone()
		{
			Queue queue = new Queue(_array.Length);
			queue._growFactor = _growFactor;
			Array.Copy(_array, 0, queue._array, 0, _array.Length);
			queue._head = _head;
			queue._size = _size;
			queue._tail = _tail;
			return queue;
		}

		/// <summary>Removes all objects from the <see cref="T:System.Collections.Queue" />.</summary>
		/// <filterpriority>2</filterpriority>
		public virtual void Clear()
		{
			_version++;
			_head = 0;
			_size = 0;
			_tail = 0;
			for (int num = _array.Length - 1; num >= 0; num--)
			{
				_array[num] = null;
			}
		}

		/// <summary>Determines whether an element is in the <see cref="T:System.Collections.Queue" />.</summary>
		/// <returns>true if <paramref name="obj" /> is found in the <see cref="T:System.Collections.Queue" />; otherwise, false.</returns>
		/// <param name="obj">The <see cref="T:System.Object" /> to locate in the <see cref="T:System.Collections.Queue" />. The value can be null. </param>
		/// <filterpriority>2</filterpriority>
		public virtual bool Contains(object obj)
		{
			int num = _head + _size;
			if (obj == null)
			{
				for (int i = _head; i < num; i++)
				{
					if (_array[i % _array.Length] == null)
					{
						return true;
					}
				}
			}
			else
			{
				for (int j = _head; j < num; j++)
				{
					if (obj.Equals(_array[j % _array.Length]))
					{
						return true;
					}
				}
			}
			return false;
		}

		/// <summary>Removes and returns the object at the beginning of the <see cref="T:System.Collections.Queue" />.</summary>
		/// <returns>The object that is removed from the beginning of the <see cref="T:System.Collections.Queue" />.</returns>
		/// <exception cref="T:System.InvalidOperationException">The <see cref="T:System.Collections.Queue" /> is empty. </exception>
		/// <filterpriority>2</filterpriority>
		public virtual object Dequeue()
		{
			_version++;
			if (_size < 1)
			{
				throw new InvalidOperationException();
			}
			object result = _array[_head];
			_array[_head] = null;
			_head = (_head + 1) % _array.Length;
			_size--;
			return result;
		}

		/// <summary>Adds an object to the end of the <see cref="T:System.Collections.Queue" />.</summary>
		/// <param name="obj">The object to add to the <see cref="T:System.Collections.Queue" />. The value can be null. </param>
		/// <filterpriority>2</filterpriority>
		public virtual void Enqueue(object obj)
		{
			_version++;
			if (_size == _array.Length)
			{
				grow();
			}
			_array[_tail] = obj;
			_tail = (_tail + 1) % _array.Length;
			_size++;
		}

		/// <summary>Returns the object at the beginning of the <see cref="T:System.Collections.Queue" /> without removing it.</summary>
		/// <returns>The object at the beginning of the <see cref="T:System.Collections.Queue" />.</returns>
		/// <exception cref="T:System.InvalidOperationException">The <see cref="T:System.Collections.Queue" /> is empty. </exception>
		/// <filterpriority>2</filterpriority>
		public virtual object Peek()
		{
			if (_size < 1)
			{
				throw new InvalidOperationException();
			}
			return _array[_head];
		}

		/// <summary>Returns a <see cref="T:System.Collections.Queue" /> wrapper that is synchronized (thread safe).</summary>
		/// <returns>A <see cref="T:System.Collections.Queue" /> wrapper that is synchronized (thread safe).</returns>
		/// <param name="queue">The <see cref="T:System.Collections.Queue" /> to synchronize. </param>
		/// <exception cref="T:System.ArgumentNullException">
		///   <paramref name="queue" /> is null. </exception>
		/// <filterpriority>2</filterpriority>
		public static Queue Synchronized(Queue queue)
		{
			if (queue == null)
			{
				throw new ArgumentNullException("queue");
			}
			return new SyncQueue(queue);
		}

		/// <summary>Copies the <see cref="T:System.Collections.Queue" /> elements to a new array.</summary>
		/// <returns>A new array containing elements copied from the <see cref="T:System.Collections.Queue" />.</returns>
		/// <filterpriority>2</filterpriority>
		public virtual object[] ToArray()
		{
			object[] array = new object[_size];
			CopyTo(array, 0);
			return array;
		}

		/// <summary>Sets the capacity to the actual number of elements in the <see cref="T:System.Collections.Queue" />.</summary>
		/// <exception cref="T:System.NotSupportedException">The <see cref="T:System.Collections.Queue" /> is read-only.</exception>
		/// <filterpriority>2</filterpriority>
		public virtual void TrimToSize()
		{
			_version++;
			object[] array = new object[_size];
			CopyTo(array, 0);
			_array = array;
			_head = 0;
			_tail = 0;
		}

		private void grow()
		{
			int num = _array.Length * _growFactor / 100;
			if (num < _array.Length + 1)
			{
				num = _array.Length + 1;
			}
			object[] array = new object[num];
			CopyTo(array, 0);
			_array = array;
			_head = 0;
			_tail = _head + _size;
		}
	}
}
