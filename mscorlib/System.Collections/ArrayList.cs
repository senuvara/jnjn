using System.Diagnostics;
using System.Runtime.InteropServices;

namespace System.Collections
{
	/// <summary>Implements the <see cref="T:System.Collections.IList" /> interface using an array whose size is dynamically increased as required.</summary>
	/// <filterpriority>1</filterpriority>
	[Serializable]
	[ComVisible(true)]
	[DebuggerTypeProxy(typeof(CollectionDebuggerView))]
	[DebuggerDisplay("Count={Count}")]
	public class ArrayList : IEnumerable, ICloneable, ICollection, IList
	{
		private sealed class ArrayListEnumerator : IEnumerator, ICloneable
		{
			private int m_Pos;

			private int m_Index;

			private int m_Count;

			private object m_Current;

			private ArrayList m_List;

			private int m_ExpectedStateChanges;

			public object Current
			{
				get
				{
					if (m_Pos == m_Index - 1)
					{
						throw new InvalidOperationException("Enumerator unusable (Reset pending, or past end of array.");
					}
					return m_Current;
				}
			}

			public ArrayListEnumerator(ArrayList list)
				: this(list, 0, list.Count)
			{
			}

			public ArrayListEnumerator(ArrayList list, int index, int count)
			{
				m_List = list;
				m_Index = index;
				m_Count = count;
				m_Pos = m_Index - 1;
				m_Current = null;
				m_ExpectedStateChanges = list._version;
			}

			public object Clone()
			{
				return MemberwiseClone();
			}

			public bool MoveNext()
			{
				if (m_List._version != m_ExpectedStateChanges)
				{
					throw new InvalidOperationException("List has changed.");
				}
				m_Pos++;
				if (m_Pos - m_Index < m_Count)
				{
					m_Current = m_List[m_Pos];
					return true;
				}
				return false;
			}

			public void Reset()
			{
				m_Current = null;
				m_Pos = m_Index - 1;
			}
		}

		private sealed class SimpleEnumerator : IEnumerator, ICloneable
		{
			private ArrayList list;

			private int index;

			private int version;

			private object currentElement;

			private static object endFlag = new object();

			public object Current
			{
				get
				{
					if (currentElement == endFlag)
					{
						if (index == -1)
						{
							throw new InvalidOperationException("Enumerator not started");
						}
						throw new InvalidOperationException("Enumerator ended");
					}
					return currentElement;
				}
			}

			public SimpleEnumerator(ArrayList list)
			{
				this.list = list;
				index = -1;
				version = list._version;
				currentElement = endFlag;
			}

			public object Clone()
			{
				return MemberwiseClone();
			}

			public bool MoveNext()
			{
				if (version != list._version)
				{
					throw new InvalidOperationException("List has changed.");
				}
				if (++index < list.Count)
				{
					currentElement = list[index];
					return true;
				}
				currentElement = endFlag;
				return false;
			}

			public void Reset()
			{
				if (version != list._version)
				{
					throw new InvalidOperationException("List has changed.");
				}
				currentElement = endFlag;
				index = -1;
			}
		}

		[Serializable]
		private sealed class ArrayListAdapter : ArrayList
		{
			private sealed class EnumeratorWithRange : IEnumerator, ICloneable
			{
				private int m_StartIndex;

				private int m_Count;

				private int m_MaxCount;

				private IEnumerator m_Enumerator;

				public object Current => m_Enumerator.Current;

				public EnumeratorWithRange(IEnumerator enumerator, int index, int count)
				{
					m_Count = 0;
					m_StartIndex = index;
					m_MaxCount = count;
					m_Enumerator = enumerator;
					Reset();
				}

				public object Clone()
				{
					return MemberwiseClone();
				}

				public bool MoveNext()
				{
					if (m_Count >= m_MaxCount)
					{
						return false;
					}
					m_Count++;
					return m_Enumerator.MoveNext();
				}

				public void Reset()
				{
					m_Count = 0;
					m_Enumerator.Reset();
					for (int i = 0; i < m_StartIndex; i++)
					{
						m_Enumerator.MoveNext();
					}
				}
			}

			private IList m_Adaptee;

			public override object this[int index]
			{
				get
				{
					return m_Adaptee[index];
				}
				set
				{
					m_Adaptee[index] = value;
				}
			}

			public override int Count => m_Adaptee.Count;

			public override int Capacity
			{
				get
				{
					return m_Adaptee.Count;
				}
				set
				{
					if (value < m_Adaptee.Count)
					{
						throw new ArgumentException("capacity");
					}
				}
			}

			public override bool IsFixedSize => m_Adaptee.IsFixedSize;

			public override bool IsReadOnly => m_Adaptee.IsReadOnly;

			public override object SyncRoot => m_Adaptee.SyncRoot;

			public override bool IsSynchronized => m_Adaptee.IsSynchronized;

			public ArrayListAdapter(IList adaptee)
				: base(0, forceZeroSize: true)
			{
				m_Adaptee = adaptee;
			}

			public override int Add(object value)
			{
				return m_Adaptee.Add(value);
			}

			public override void Clear()
			{
				m_Adaptee.Clear();
			}

			public override bool Contains(object value)
			{
				return m_Adaptee.Contains(value);
			}

			public override int IndexOf(object value)
			{
				return m_Adaptee.IndexOf(value);
			}

			public override int IndexOf(object value, int startIndex)
			{
				return IndexOf(value, startIndex, m_Adaptee.Count - startIndex);
			}

			public override int IndexOf(object value, int startIndex, int count)
			{
				if (startIndex < 0 || startIndex > m_Adaptee.Count)
				{
					ThrowNewArgumentOutOfRangeException("startIndex", startIndex, "Does not specify valid index.");
				}
				if (count < 0)
				{
					ThrowNewArgumentOutOfRangeException("count", count, "Can't be less than 0.");
				}
				if (startIndex > m_Adaptee.Count - count)
				{
					throw new ArgumentOutOfRangeException("count", "Start index and count do not specify a valid range.");
				}
				if (value == null)
				{
					for (int i = startIndex; i < startIndex + count; i++)
					{
						if (m_Adaptee[i] == null)
						{
							return i;
						}
					}
				}
				else
				{
					for (int j = startIndex; j < startIndex + count; j++)
					{
						if (value.Equals(m_Adaptee[j]))
						{
							return j;
						}
					}
				}
				return -1;
			}

			public override int LastIndexOf(object value)
			{
				return LastIndexOf(value, m_Adaptee.Count - 1);
			}

			public override int LastIndexOf(object value, int startIndex)
			{
				return LastIndexOf(value, startIndex, startIndex + 1);
			}

			public override int LastIndexOf(object value, int startIndex, int count)
			{
				if (startIndex < 0)
				{
					ThrowNewArgumentOutOfRangeException("startIndex", startIndex, "< 0");
				}
				if (count < 0)
				{
					ThrowNewArgumentOutOfRangeException("count", count, "count is negative.");
				}
				if (startIndex - count + 1 < 0)
				{
					ThrowNewArgumentOutOfRangeException("count", count, "count is too large.");
				}
				if (value == null)
				{
					for (int num = startIndex; num > startIndex - count; num--)
					{
						if (m_Adaptee[num] == null)
						{
							return num;
						}
					}
				}
				else
				{
					for (int num2 = startIndex; num2 > startIndex - count; num2--)
					{
						if (value.Equals(m_Adaptee[num2]))
						{
							return num2;
						}
					}
				}
				return -1;
			}

			public override void Insert(int index, object value)
			{
				m_Adaptee.Insert(index, value);
			}

			public override void InsertRange(int index, ICollection c)
			{
				if (c == null)
				{
					throw new ArgumentNullException("c");
				}
				if (index > m_Adaptee.Count)
				{
					ThrowNewArgumentOutOfRangeException("index", index, "Index must be >= 0 and <= Count.");
				}
				foreach (object item in c)
				{
					m_Adaptee.Insert(index++, item);
				}
			}

			public override void Remove(object value)
			{
				m_Adaptee.Remove(value);
			}

			public override void RemoveAt(int index)
			{
				m_Adaptee.RemoveAt(index);
			}

			public override void RemoveRange(int index, int count)
			{
				CheckRange(index, count, m_Adaptee.Count);
				for (int i = 0; i < count; i++)
				{
					m_Adaptee.RemoveAt(index);
				}
			}

			public override void Reverse()
			{
				Reverse(0, m_Adaptee.Count);
			}

			public override void Reverse(int index, int count)
			{
				CheckRange(index, count, m_Adaptee.Count);
				for (int i = 0; i < count / 2; i++)
				{
					object value = m_Adaptee[i + index];
					m_Adaptee[i + index] = m_Adaptee[index + count - i + index - 1];
					m_Adaptee[index + count - i + index - 1] = value;
				}
			}

			public override void SetRange(int index, ICollection c)
			{
				if (c == null)
				{
					throw new ArgumentNullException("c");
				}
				if (index < 0 || index + c.Count > m_Adaptee.Count)
				{
					throw new ArgumentOutOfRangeException("index");
				}
				int num = index;
				foreach (object item in c)
				{
					m_Adaptee[num++] = item;
				}
			}

			public override void CopyTo(Array array)
			{
				m_Adaptee.CopyTo(array, 0);
			}

			public override void CopyTo(Array array, int index)
			{
				m_Adaptee.CopyTo(array, index);
			}

			public override void CopyTo(int index, Array array, int arrayIndex, int count)
			{
				if (index < 0)
				{
					ThrowNewArgumentOutOfRangeException("index", index, "Can't be less than zero.");
				}
				if (arrayIndex < 0)
				{
					ThrowNewArgumentOutOfRangeException("arrayIndex", arrayIndex, "Can't be less than zero.");
				}
				if (count < 0)
				{
					ThrowNewArgumentOutOfRangeException("index", index, "Can't be less than zero.");
				}
				if (index >= m_Adaptee.Count)
				{
					throw new ArgumentException("Can't be more or equal to list count.", "index");
				}
				if (array.Rank > 1)
				{
					throw new ArgumentException("Can't copy into multi-dimensional array.");
				}
				if (arrayIndex >= array.Length)
				{
					throw new ArgumentException("arrayIndex can't be greater than array.Length - 1.");
				}
				if (array.Length - arrayIndex + 1 < count)
				{
					throw new ArgumentException("Destination array is too small.");
				}
				if (index > m_Adaptee.Count - count)
				{
					throw new ArgumentException("Index and count do not denote a valid range of elements.", "index");
				}
				for (int i = 0; i < count; i++)
				{
					array.SetValue(m_Adaptee[index + i], arrayIndex + i);
				}
			}

			public override IEnumerator GetEnumerator()
			{
				return m_Adaptee.GetEnumerator();
			}

			public override IEnumerator GetEnumerator(int index, int count)
			{
				CheckRange(index, count, m_Adaptee.Count);
				return new EnumeratorWithRange(m_Adaptee.GetEnumerator(), index, count);
			}

			public override void AddRange(ICollection c)
			{
				foreach (object item in c)
				{
					m_Adaptee.Add(item);
				}
			}

			public override int BinarySearch(object value)
			{
				return BinarySearch(value, null);
			}

			public override int BinarySearch(object value, IComparer comparer)
			{
				return BinarySearch(0, m_Adaptee.Count, value, comparer);
			}

			public override int BinarySearch(int index, int count, object value, IComparer comparer)
			{
				CheckRange(index, count, m_Adaptee.Count);
				if (comparer == null)
				{
					comparer = Comparer.Default;
				}
				int num = index;
				int num2 = index + count - 1;
				while (num <= num2)
				{
					int num3 = num + (num2 - num) / 2;
					int num4 = comparer.Compare(value, m_Adaptee[num3]);
					if (num4 < 0)
					{
						num2 = num3 - 1;
						continue;
					}
					if (num4 > 0)
					{
						num = num3 + 1;
						continue;
					}
					return num3;
				}
				return ~num;
			}

			public override object Clone()
			{
				return new ArrayListAdapter(m_Adaptee);
			}

			public override ArrayList GetRange(int index, int count)
			{
				CheckRange(index, count, m_Adaptee.Count);
				return new RangedArrayList(this, index, count);
			}

			public override void TrimToSize()
			{
			}

			public override void Sort()
			{
				Sort(Comparer.Default);
			}

			public override void Sort(IComparer comparer)
			{
				Sort(0, m_Adaptee.Count, comparer);
			}

			public override void Sort(int index, int count, IComparer comparer)
			{
				CheckRange(index, count, m_Adaptee.Count);
				if (comparer == null)
				{
					comparer = Comparer.Default;
				}
				QuickSort(m_Adaptee, index, index + count - 1, comparer);
			}

			private static void Swap(IList list, int x, int y)
			{
				object value = list[x];
				list[x] = list[y];
				list[y] = value;
			}

			internal static void QuickSort(IList list, int left, int right, IComparer comparer)
			{
				if (left >= right)
				{
					return;
				}
				int num = left + (right - left) / 2;
				if (comparer.Compare(list[num], list[left]) < 0)
				{
					Swap(list, num, left);
				}
				if (comparer.Compare(list[right], list[left]) < 0)
				{
					Swap(list, right, left);
				}
				if (comparer.Compare(list[right], list[num]) < 0)
				{
					Swap(list, right, num);
				}
				if (right - left + 1 <= 3)
				{
					return;
				}
				Swap(list, right - 1, num);
				object y = list[right - 1];
				int num2 = left;
				int num3 = right - 1;
				while (true)
				{
					if (comparer.Compare(list[++num2], y) >= 0)
					{
						while (comparer.Compare(list[--num3], y) > 0)
						{
						}
						if (num2 >= num3)
						{
							break;
						}
						Swap(list, num2, num3);
					}
				}
				Swap(list, right - 1, num2);
				QuickSort(list, left, num2 - 1, comparer);
				QuickSort(list, num2 + 1, right, comparer);
			}

			public override object[] ToArray()
			{
				object[] array = new object[m_Adaptee.Count];
				m_Adaptee.CopyTo(array, 0);
				return array;
			}

			public override Array ToArray(Type elementType)
			{
				Array array = Array.CreateInstance(elementType, m_Adaptee.Count);
				m_Adaptee.CopyTo(array, 0);
				return array;
			}
		}

		[Serializable]
		private class ArrayListWrapper : ArrayList
		{
			protected ArrayList m_InnerArrayList;

			public override object this[int index]
			{
				get
				{
					return m_InnerArrayList[index];
				}
				set
				{
					m_InnerArrayList[index] = value;
				}
			}

			public override int Count => m_InnerArrayList.Count;

			public override int Capacity
			{
				get
				{
					return m_InnerArrayList.Capacity;
				}
				set
				{
					m_InnerArrayList.Capacity = value;
				}
			}

			public override bool IsFixedSize => m_InnerArrayList.IsFixedSize;

			public override bool IsReadOnly => m_InnerArrayList.IsReadOnly;

			public override bool IsSynchronized => m_InnerArrayList.IsSynchronized;

			public override object SyncRoot => m_InnerArrayList.SyncRoot;

			public ArrayListWrapper(ArrayList innerArrayList)
			{
				m_InnerArrayList = innerArrayList;
			}

			public override int Add(object value)
			{
				return m_InnerArrayList.Add(value);
			}

			public override void Clear()
			{
				m_InnerArrayList.Clear();
			}

			public override bool Contains(object value)
			{
				return m_InnerArrayList.Contains(value);
			}

			public override int IndexOf(object value)
			{
				return m_InnerArrayList.IndexOf(value);
			}

			public override int IndexOf(object value, int startIndex)
			{
				return m_InnerArrayList.IndexOf(value, startIndex);
			}

			public override int IndexOf(object value, int startIndex, int count)
			{
				return m_InnerArrayList.IndexOf(value, startIndex, count);
			}

			public override int LastIndexOf(object value)
			{
				return m_InnerArrayList.LastIndexOf(value);
			}

			public override int LastIndexOf(object value, int startIndex)
			{
				return m_InnerArrayList.LastIndexOf(value, startIndex);
			}

			public override int LastIndexOf(object value, int startIndex, int count)
			{
				return m_InnerArrayList.LastIndexOf(value, startIndex, count);
			}

			public override void Insert(int index, object value)
			{
				m_InnerArrayList.Insert(index, value);
			}

			public override void InsertRange(int index, ICollection c)
			{
				m_InnerArrayList.InsertRange(index, c);
			}

			public override void Remove(object value)
			{
				m_InnerArrayList.Remove(value);
			}

			public override void RemoveAt(int index)
			{
				m_InnerArrayList.RemoveAt(index);
			}

			public override void RemoveRange(int index, int count)
			{
				m_InnerArrayList.RemoveRange(index, count);
			}

			public override void Reverse()
			{
				m_InnerArrayList.Reverse();
			}

			public override void Reverse(int index, int count)
			{
				m_InnerArrayList.Reverse(index, count);
			}

			public override void SetRange(int index, ICollection c)
			{
				m_InnerArrayList.SetRange(index, c);
			}

			public override void CopyTo(Array array)
			{
				m_InnerArrayList.CopyTo(array);
			}

			public override void CopyTo(Array array, int index)
			{
				m_InnerArrayList.CopyTo(array, index);
			}

			public override void CopyTo(int index, Array array, int arrayIndex, int count)
			{
				m_InnerArrayList.CopyTo(index, array, arrayIndex, count);
			}

			public override IEnumerator GetEnumerator()
			{
				return m_InnerArrayList.GetEnumerator();
			}

			public override IEnumerator GetEnumerator(int index, int count)
			{
				return m_InnerArrayList.GetEnumerator(index, count);
			}

			public override void AddRange(ICollection c)
			{
				m_InnerArrayList.AddRange(c);
			}

			public override int BinarySearch(object value)
			{
				return m_InnerArrayList.BinarySearch(value);
			}

			public override int BinarySearch(object value, IComparer comparer)
			{
				return m_InnerArrayList.BinarySearch(value, comparer);
			}

			public override int BinarySearch(int index, int count, object value, IComparer comparer)
			{
				return m_InnerArrayList.BinarySearch(index, count, value, comparer);
			}

			public override object Clone()
			{
				return m_InnerArrayList.Clone();
			}

			public override ArrayList GetRange(int index, int count)
			{
				return m_InnerArrayList.GetRange(index, count);
			}

			public override void TrimToSize()
			{
				m_InnerArrayList.TrimToSize();
			}

			public override void Sort()
			{
				m_InnerArrayList.Sort();
			}

			public override void Sort(IComparer comparer)
			{
				m_InnerArrayList.Sort(comparer);
			}

			public override void Sort(int index, int count, IComparer comparer)
			{
				m_InnerArrayList.Sort(index, count, comparer);
			}

			public override object[] ToArray()
			{
				return m_InnerArrayList.ToArray();
			}

			public override Array ToArray(Type elementType)
			{
				return m_InnerArrayList.ToArray(elementType);
			}
		}

		[Serializable]
		private sealed class SynchronizedArrayListWrapper : ArrayListWrapper
		{
			private object m_SyncRoot;

			public override object this[int index]
			{
				get
				{
					//Discarded unreachable code: IL_001f
					lock (m_SyncRoot)
					{
						return m_InnerArrayList[index];
					}
				}
				set
				{
					lock (m_SyncRoot)
					{
						m_InnerArrayList[index] = value;
					}
				}
			}

			public override int Count
			{
				get
				{
					//Discarded unreachable code: IL_001e
					lock (m_SyncRoot)
					{
						return m_InnerArrayList.Count;
					}
				}
			}

			public override int Capacity
			{
				get
				{
					//Discarded unreachable code: IL_001e
					lock (m_SyncRoot)
					{
						return m_InnerArrayList.Capacity;
					}
				}
				set
				{
					lock (m_SyncRoot)
					{
						m_InnerArrayList.Capacity = value;
					}
				}
			}

			public override bool IsFixedSize
			{
				get
				{
					//Discarded unreachable code: IL_001e
					lock (m_SyncRoot)
					{
						return m_InnerArrayList.IsFixedSize;
					}
				}
			}

			public override bool IsReadOnly
			{
				get
				{
					//Discarded unreachable code: IL_001e
					lock (m_SyncRoot)
					{
						return m_InnerArrayList.IsReadOnly;
					}
				}
			}

			public override bool IsSynchronized => true;

			public override object SyncRoot => m_SyncRoot;

			internal SynchronizedArrayListWrapper(ArrayList innerArrayList)
				: base(innerArrayList)
			{
				m_SyncRoot = innerArrayList.SyncRoot;
			}

			public override int Add(object value)
			{
				//Discarded unreachable code: IL_001f
				lock (m_SyncRoot)
				{
					return m_InnerArrayList.Add(value);
				}
			}

			public override void Clear()
			{
				lock (m_SyncRoot)
				{
					m_InnerArrayList.Clear();
				}
			}

			public override bool Contains(object value)
			{
				//Discarded unreachable code: IL_001f
				lock (m_SyncRoot)
				{
					return m_InnerArrayList.Contains(value);
				}
			}

			public override int IndexOf(object value)
			{
				//Discarded unreachable code: IL_001f
				lock (m_SyncRoot)
				{
					return m_InnerArrayList.IndexOf(value);
				}
			}

			public override int IndexOf(object value, int startIndex)
			{
				//Discarded unreachable code: IL_0020
				lock (m_SyncRoot)
				{
					return m_InnerArrayList.IndexOf(value, startIndex);
				}
			}

			public override int IndexOf(object value, int startIndex, int count)
			{
				//Discarded unreachable code: IL_0021
				lock (m_SyncRoot)
				{
					return m_InnerArrayList.IndexOf(value, startIndex, count);
				}
			}

			public override int LastIndexOf(object value)
			{
				//Discarded unreachable code: IL_001f
				lock (m_SyncRoot)
				{
					return m_InnerArrayList.LastIndexOf(value);
				}
			}

			public override int LastIndexOf(object value, int startIndex)
			{
				//Discarded unreachable code: IL_0020
				lock (m_SyncRoot)
				{
					return m_InnerArrayList.LastIndexOf(value, startIndex);
				}
			}

			public override int LastIndexOf(object value, int startIndex, int count)
			{
				//Discarded unreachable code: IL_0021
				lock (m_SyncRoot)
				{
					return m_InnerArrayList.LastIndexOf(value, startIndex, count);
				}
			}

			public override void Insert(int index, object value)
			{
				lock (m_SyncRoot)
				{
					m_InnerArrayList.Insert(index, value);
				}
			}

			public override void InsertRange(int index, ICollection c)
			{
				lock (m_SyncRoot)
				{
					m_InnerArrayList.InsertRange(index, c);
				}
			}

			public override void Remove(object value)
			{
				lock (m_SyncRoot)
				{
					m_InnerArrayList.Remove(value);
				}
			}

			public override void RemoveAt(int index)
			{
				lock (m_SyncRoot)
				{
					m_InnerArrayList.RemoveAt(index);
				}
			}

			public override void RemoveRange(int index, int count)
			{
				lock (m_SyncRoot)
				{
					m_InnerArrayList.RemoveRange(index, count);
				}
			}

			public override void Reverse()
			{
				lock (m_SyncRoot)
				{
					m_InnerArrayList.Reverse();
				}
			}

			public override void Reverse(int index, int count)
			{
				lock (m_SyncRoot)
				{
					m_InnerArrayList.Reverse(index, count);
				}
			}

			public override void CopyTo(Array array)
			{
				lock (m_SyncRoot)
				{
					m_InnerArrayList.CopyTo(array);
				}
			}

			public override void CopyTo(Array array, int index)
			{
				lock (m_SyncRoot)
				{
					m_InnerArrayList.CopyTo(array, index);
				}
			}

			public override void CopyTo(int index, Array array, int arrayIndex, int count)
			{
				lock (m_SyncRoot)
				{
					m_InnerArrayList.CopyTo(index, array, arrayIndex, count);
				}
			}

			public override IEnumerator GetEnumerator()
			{
				//Discarded unreachable code: IL_001e
				lock (m_SyncRoot)
				{
					return m_InnerArrayList.GetEnumerator();
				}
			}

			public override IEnumerator GetEnumerator(int index, int count)
			{
				//Discarded unreachable code: IL_0020
				lock (m_SyncRoot)
				{
					return m_InnerArrayList.GetEnumerator(index, count);
				}
			}

			public override void AddRange(ICollection c)
			{
				lock (m_SyncRoot)
				{
					m_InnerArrayList.AddRange(c);
				}
			}

			public override int BinarySearch(object value)
			{
				//Discarded unreachable code: IL_001f
				lock (m_SyncRoot)
				{
					return m_InnerArrayList.BinarySearch(value);
				}
			}

			public override int BinarySearch(object value, IComparer comparer)
			{
				//Discarded unreachable code: IL_0020
				lock (m_SyncRoot)
				{
					return m_InnerArrayList.BinarySearch(value, comparer);
				}
			}

			public override int BinarySearch(int index, int count, object value, IComparer comparer)
			{
				//Discarded unreachable code: IL_0023
				lock (m_SyncRoot)
				{
					return m_InnerArrayList.BinarySearch(index, count, value, comparer);
				}
			}

			public override object Clone()
			{
				//Discarded unreachable code: IL_001e
				lock (m_SyncRoot)
				{
					return m_InnerArrayList.Clone();
				}
			}

			public override ArrayList GetRange(int index, int count)
			{
				//Discarded unreachable code: IL_0020
				lock (m_SyncRoot)
				{
					return m_InnerArrayList.GetRange(index, count);
				}
			}

			public override void TrimToSize()
			{
				lock (m_SyncRoot)
				{
					m_InnerArrayList.TrimToSize();
				}
			}

			public override void Sort()
			{
				lock (m_SyncRoot)
				{
					m_InnerArrayList.Sort();
				}
			}

			public override void Sort(IComparer comparer)
			{
				lock (m_SyncRoot)
				{
					m_InnerArrayList.Sort(comparer);
				}
			}

			public override void Sort(int index, int count, IComparer comparer)
			{
				lock (m_SyncRoot)
				{
					m_InnerArrayList.Sort(index, count, comparer);
				}
			}

			public override object[] ToArray()
			{
				//Discarded unreachable code: IL_001e
				lock (m_SyncRoot)
				{
					return m_InnerArrayList.ToArray();
				}
			}

			public override Array ToArray(Type elementType)
			{
				//Discarded unreachable code: IL_001f
				lock (m_SyncRoot)
				{
					return m_InnerArrayList.ToArray(elementType);
				}
			}
		}

		[Serializable]
		private class FixedSizeArrayListWrapper : ArrayListWrapper
		{
			protected virtual string ErrorMessage => "Can't add or remove from a fixed-size list.";

			public override int Capacity
			{
				get
				{
					return base.Capacity;
				}
				set
				{
					throw new NotSupportedException(ErrorMessage);
				}
			}

			public override bool IsFixedSize => true;

			public FixedSizeArrayListWrapper(ArrayList innerList)
				: base(innerList)
			{
			}

			public override int Add(object value)
			{
				throw new NotSupportedException(ErrorMessage);
			}

			public override void AddRange(ICollection c)
			{
				throw new NotSupportedException(ErrorMessage);
			}

			public override void Clear()
			{
				throw new NotSupportedException(ErrorMessage);
			}

			public override void Insert(int index, object value)
			{
				throw new NotSupportedException(ErrorMessage);
			}

			public override void InsertRange(int index, ICollection c)
			{
				throw new NotSupportedException(ErrorMessage);
			}

			public override void Remove(object value)
			{
				throw new NotSupportedException(ErrorMessage);
			}

			public override void RemoveAt(int index)
			{
				throw new NotSupportedException(ErrorMessage);
			}

			public override void RemoveRange(int index, int count)
			{
				throw new NotSupportedException(ErrorMessage);
			}

			public override void TrimToSize()
			{
				throw new NotSupportedException(ErrorMessage);
			}
		}

		[Serializable]
		private sealed class ReadOnlyArrayListWrapper : FixedSizeArrayListWrapper
		{
			protected override string ErrorMessage => "Can't modify a readonly list.";

			public override bool IsReadOnly => true;

			public override object this[int index]
			{
				get
				{
					return m_InnerArrayList[index];
				}
				set
				{
					throw new NotSupportedException(ErrorMessage);
				}
			}

			public ReadOnlyArrayListWrapper(ArrayList innerArrayList)
				: base(innerArrayList)
			{
			}

			public override void Reverse()
			{
				throw new NotSupportedException(ErrorMessage);
			}

			public override void Reverse(int index, int count)
			{
				throw new NotSupportedException(ErrorMessage);
			}

			public override void SetRange(int index, ICollection c)
			{
				throw new NotSupportedException(ErrorMessage);
			}

			public override void Sort()
			{
				throw new NotSupportedException(ErrorMessage);
			}

			public override void Sort(IComparer comparer)
			{
				throw new NotSupportedException(ErrorMessage);
			}

			public override void Sort(int index, int count, IComparer comparer)
			{
				throw new NotSupportedException(ErrorMessage);
			}
		}

		[Serializable]
		private sealed class RangedArrayList : ArrayListWrapper
		{
			private int m_InnerIndex;

			private int m_InnerCount;

			private int m_InnerStateChanges;

			public override bool IsSynchronized => false;

			public override object this[int index]
			{
				get
				{
					if (index < 0 || index > m_InnerCount)
					{
						throw new ArgumentOutOfRangeException("index");
					}
					return m_InnerArrayList[m_InnerIndex + index];
				}
				set
				{
					if (index < 0 || index > m_InnerCount)
					{
						throw new ArgumentOutOfRangeException("index");
					}
					m_InnerArrayList[m_InnerIndex + index] = value;
				}
			}

			public override int Count
			{
				get
				{
					VerifyStateChanges();
					return m_InnerCount;
				}
			}

			public override int Capacity
			{
				get
				{
					return m_InnerArrayList.Capacity;
				}
				set
				{
					if (value < m_InnerCount)
					{
						throw new ArgumentOutOfRangeException();
					}
				}
			}

			public RangedArrayList(ArrayList innerList, int index, int count)
				: base(innerList)
			{
				m_InnerIndex = index;
				m_InnerCount = count;
				m_InnerStateChanges = innerList._version;
			}

			private void VerifyStateChanges()
			{
				if (m_InnerStateChanges != m_InnerArrayList._version)
				{
					throw new InvalidOperationException("ArrayList view is invalid because the underlying ArrayList was modified.");
				}
			}

			public override int Add(object value)
			{
				VerifyStateChanges();
				m_InnerArrayList.Insert(m_InnerIndex + m_InnerCount, value);
				m_InnerStateChanges = m_InnerArrayList._version;
				return ++m_InnerCount;
			}

			public override void Clear()
			{
				VerifyStateChanges();
				m_InnerArrayList.RemoveRange(m_InnerIndex, m_InnerCount);
				m_InnerCount = 0;
				m_InnerStateChanges = m_InnerArrayList._version;
			}

			public override bool Contains(object value)
			{
				return m_InnerArrayList.Contains(value, m_InnerIndex, m_InnerCount);
			}

			public override int IndexOf(object value)
			{
				return IndexOf(value, 0);
			}

			public override int IndexOf(object value, int startIndex)
			{
				return IndexOf(value, startIndex, m_InnerCount - startIndex);
			}

			public override int IndexOf(object value, int startIndex, int count)
			{
				if (startIndex < 0 || startIndex > m_InnerCount)
				{
					ThrowNewArgumentOutOfRangeException("startIndex", startIndex, "Does not specify valid index.");
				}
				if (count < 0)
				{
					ThrowNewArgumentOutOfRangeException("count", count, "Can't be less than 0.");
				}
				if (startIndex > m_InnerCount - count)
				{
					throw new ArgumentOutOfRangeException("count", "Start index and count do not specify a valid range.");
				}
				int num = m_InnerArrayList.IndexOf(value, m_InnerIndex + startIndex, count);
				if (num == -1)
				{
					return -1;
				}
				return num - m_InnerIndex;
			}

			public override int LastIndexOf(object value)
			{
				return LastIndexOf(value, m_InnerCount - 1);
			}

			public override int LastIndexOf(object value, int startIndex)
			{
				return LastIndexOf(value, startIndex, startIndex + 1);
			}

			public override int LastIndexOf(object value, int startIndex, int count)
			{
				if (startIndex < 0)
				{
					ThrowNewArgumentOutOfRangeException("startIndex", startIndex, "< 0");
				}
				if (count < 0)
				{
					ThrowNewArgumentOutOfRangeException("count", count, "count is negative.");
				}
				int num = m_InnerArrayList.LastIndexOf(value, m_InnerIndex + startIndex, count);
				if (num == -1)
				{
					return -1;
				}
				return num - m_InnerIndex;
			}

			public override void Insert(int index, object value)
			{
				VerifyStateChanges();
				if (index < 0 || index > m_InnerCount)
				{
					ThrowNewArgumentOutOfRangeException("index", index, "Index must be >= 0 and <= Count.");
				}
				m_InnerArrayList.Insert(m_InnerIndex + index, value);
				m_InnerCount++;
				m_InnerStateChanges = m_InnerArrayList._version;
			}

			public override void InsertRange(int index, ICollection c)
			{
				VerifyStateChanges();
				if (index < 0 || index > m_InnerCount)
				{
					ThrowNewArgumentOutOfRangeException("index", index, "Index must be >= 0 and <= Count.");
				}
				m_InnerArrayList.InsertRange(m_InnerIndex + index, c);
				m_InnerCount += c.Count;
				m_InnerStateChanges = m_InnerArrayList._version;
			}

			public override void Remove(object value)
			{
				VerifyStateChanges();
				int num = IndexOf(value);
				if (num > -1)
				{
					RemoveAt(num);
				}
				m_InnerStateChanges = m_InnerArrayList._version;
			}

			public override void RemoveAt(int index)
			{
				VerifyStateChanges();
				if (index < 0 || index > m_InnerCount)
				{
					ThrowNewArgumentOutOfRangeException("index", index, "Index must be >= 0 and <= Count.");
				}
				m_InnerArrayList.RemoveAt(m_InnerIndex + index);
				m_InnerCount--;
				m_InnerStateChanges = m_InnerArrayList._version;
			}

			public override void RemoveRange(int index, int count)
			{
				VerifyStateChanges();
				CheckRange(index, count, m_InnerCount);
				m_InnerArrayList.RemoveRange(m_InnerIndex + index, count);
				m_InnerCount -= count;
				m_InnerStateChanges = m_InnerArrayList._version;
			}

			public override void Reverse()
			{
				Reverse(0, m_InnerCount);
			}

			public override void Reverse(int index, int count)
			{
				VerifyStateChanges();
				CheckRange(index, count, m_InnerCount);
				m_InnerArrayList.Reverse(m_InnerIndex + index, count);
				m_InnerStateChanges = m_InnerArrayList._version;
			}

			public override void SetRange(int index, ICollection c)
			{
				VerifyStateChanges();
				if (index < 0 || index > m_InnerCount)
				{
					ThrowNewArgumentOutOfRangeException("index", index, "Index must be >= 0 and <= Count.");
				}
				m_InnerArrayList.SetRange(m_InnerIndex + index, c);
				m_InnerStateChanges = m_InnerArrayList._version;
			}

			public override void CopyTo(Array array)
			{
				CopyTo(array, 0);
			}

			public override void CopyTo(Array array, int index)
			{
				CopyTo(0, array, index, m_InnerCount);
			}

			public override void CopyTo(int index, Array array, int arrayIndex, int count)
			{
				CheckRange(index, count, m_InnerCount);
				m_InnerArrayList.CopyTo(m_InnerIndex + index, array, arrayIndex, count);
			}

			public override IEnumerator GetEnumerator()
			{
				return GetEnumerator(0, m_InnerCount);
			}

			public override IEnumerator GetEnumerator(int index, int count)
			{
				CheckRange(index, count, m_InnerCount);
				return m_InnerArrayList.GetEnumerator(m_InnerIndex + index, count);
			}

			public override void AddRange(ICollection c)
			{
				VerifyStateChanges();
				m_InnerArrayList.InsertRange(m_InnerCount, c);
				m_InnerCount += c.Count;
				m_InnerStateChanges = m_InnerArrayList._version;
			}

			public override int BinarySearch(object value)
			{
				return BinarySearch(0, m_InnerCount, value, Comparer.Default);
			}

			public override int BinarySearch(object value, IComparer comparer)
			{
				return BinarySearch(0, m_InnerCount, value, comparer);
			}

			public override int BinarySearch(int index, int count, object value, IComparer comparer)
			{
				CheckRange(index, count, m_InnerCount);
				return m_InnerArrayList.BinarySearch(m_InnerIndex + index, count, value, comparer);
			}

			public override object Clone()
			{
				return new RangedArrayList((ArrayList)m_InnerArrayList.Clone(), m_InnerIndex, m_InnerCount);
			}

			public override ArrayList GetRange(int index, int count)
			{
				CheckRange(index, count, m_InnerCount);
				return new RangedArrayList(this, index, count);
			}

			public override void TrimToSize()
			{
				throw new NotSupportedException();
			}

			public override void Sort()
			{
				Sort(Comparer.Default);
			}

			public override void Sort(IComparer comparer)
			{
				Sort(0, m_InnerCount, comparer);
			}

			public override void Sort(int index, int count, IComparer comparer)
			{
				VerifyStateChanges();
				CheckRange(index, count, m_InnerCount);
				m_InnerArrayList.Sort(m_InnerIndex + index, count, comparer);
				m_InnerStateChanges = m_InnerArrayList._version;
			}

			public override object[] ToArray()
			{
				object[] array = new object[m_InnerCount];
				m_InnerArrayList.CopyTo(m_InnerIndex, array, 0, m_InnerCount);
				return array;
			}

			public override Array ToArray(Type elementType)
			{
				Array array = Array.CreateInstance(elementType, m_InnerCount);
				m_InnerArrayList.CopyTo(m_InnerIndex, array, 0, m_InnerCount);
				return array;
			}
		}

		[Serializable]
		private sealed class SynchronizedListWrapper : ListWrapper
		{
			private object m_SyncRoot;

			public override int Count
			{
				get
				{
					//Discarded unreachable code: IL_001e
					lock (m_SyncRoot)
					{
						return m_InnerList.Count;
					}
				}
			}

			public override bool IsSynchronized => true;

			public override object SyncRoot
			{
				get
				{
					//Discarded unreachable code: IL_001e
					lock (m_SyncRoot)
					{
						return m_InnerList.SyncRoot;
					}
				}
			}

			public override bool IsFixedSize
			{
				get
				{
					//Discarded unreachable code: IL_001e
					lock (m_SyncRoot)
					{
						return m_InnerList.IsFixedSize;
					}
				}
			}

			public override bool IsReadOnly
			{
				get
				{
					//Discarded unreachable code: IL_001e
					lock (m_SyncRoot)
					{
						return m_InnerList.IsReadOnly;
					}
				}
			}

			public override object this[int index]
			{
				get
				{
					//Discarded unreachable code: IL_001f
					lock (m_SyncRoot)
					{
						return m_InnerList[index];
					}
				}
				set
				{
					lock (m_SyncRoot)
					{
						m_InnerList[index] = value;
					}
				}
			}

			public SynchronizedListWrapper(IList innerList)
				: base(innerList)
			{
				m_SyncRoot = innerList.SyncRoot;
			}

			public override int Add(object value)
			{
				//Discarded unreachable code: IL_001f
				lock (m_SyncRoot)
				{
					return m_InnerList.Add(value);
				}
			}

			public override void Clear()
			{
				lock (m_SyncRoot)
				{
					m_InnerList.Clear();
				}
			}

			public override bool Contains(object value)
			{
				//Discarded unreachable code: IL_001f
				lock (m_SyncRoot)
				{
					return m_InnerList.Contains(value);
				}
			}

			public override int IndexOf(object value)
			{
				//Discarded unreachable code: IL_001f
				lock (m_SyncRoot)
				{
					return m_InnerList.IndexOf(value);
				}
			}

			public override void Insert(int index, object value)
			{
				lock (m_SyncRoot)
				{
					m_InnerList.Insert(index, value);
				}
			}

			public override void Remove(object value)
			{
				lock (m_SyncRoot)
				{
					m_InnerList.Remove(value);
				}
			}

			public override void RemoveAt(int index)
			{
				lock (m_SyncRoot)
				{
					m_InnerList.RemoveAt(index);
				}
			}

			public override void CopyTo(Array array, int index)
			{
				lock (m_SyncRoot)
				{
					m_InnerList.CopyTo(array, index);
				}
			}

			public override IEnumerator GetEnumerator()
			{
				//Discarded unreachable code: IL_001e
				lock (m_SyncRoot)
				{
					return m_InnerList.GetEnumerator();
				}
			}
		}

		[Serializable]
		private class FixedSizeListWrapper : ListWrapper
		{
			protected virtual string ErrorMessage => "List is fixed-size.";

			public override bool IsFixedSize => true;

			public FixedSizeListWrapper(IList innerList)
				: base(innerList)
			{
			}

			public override int Add(object value)
			{
				throw new NotSupportedException(ErrorMessage);
			}

			public override void Clear()
			{
				throw new NotSupportedException(ErrorMessage);
			}

			public override void Insert(int index, object value)
			{
				throw new NotSupportedException(ErrorMessage);
			}

			public override void Remove(object value)
			{
				throw new NotSupportedException(ErrorMessage);
			}

			public override void RemoveAt(int index)
			{
				throw new NotSupportedException(ErrorMessage);
			}
		}

		[Serializable]
		private sealed class ReadOnlyListWrapper : FixedSizeListWrapper
		{
			protected override string ErrorMessage => "List is read-only.";

			public override bool IsReadOnly => true;

			public override object this[int index]
			{
				get
				{
					return m_InnerList[index];
				}
				set
				{
					throw new NotSupportedException(ErrorMessage);
				}
			}

			public ReadOnlyListWrapper(IList innerList)
				: base(innerList)
			{
			}
		}

		[Serializable]
		private class ListWrapper : IEnumerable, ICollection, IList
		{
			protected IList m_InnerList;

			public virtual object this[int index]
			{
				get
				{
					return m_InnerList[index];
				}
				set
				{
					m_InnerList[index] = value;
				}
			}

			public virtual int Count => m_InnerList.Count;

			public virtual bool IsSynchronized => m_InnerList.IsSynchronized;

			public virtual object SyncRoot => m_InnerList.SyncRoot;

			public virtual bool IsFixedSize => m_InnerList.IsFixedSize;

			public virtual bool IsReadOnly => m_InnerList.IsReadOnly;

			public ListWrapper(IList innerList)
			{
				m_InnerList = innerList;
			}

			public virtual int Add(object value)
			{
				return m_InnerList.Add(value);
			}

			public virtual void Clear()
			{
				m_InnerList.Clear();
			}

			public virtual bool Contains(object value)
			{
				return m_InnerList.Contains(value);
			}

			public virtual int IndexOf(object value)
			{
				return m_InnerList.IndexOf(value);
			}

			public virtual void Insert(int index, object value)
			{
				m_InnerList.Insert(index, value);
			}

			public virtual void Remove(object value)
			{
				m_InnerList.Remove(value);
			}

			public virtual void RemoveAt(int index)
			{
				m_InnerList.RemoveAt(index);
			}

			public virtual void CopyTo(Array array, int index)
			{
				m_InnerList.CopyTo(array, index);
			}

			public virtual IEnumerator GetEnumerator()
			{
				return m_InnerList.GetEnumerator();
			}
		}

		private const int DefaultInitialCapacity = 4;

		private int _size;

		private object[] _items;

		private int _version;

		private static readonly object[] EmptyArray = new object[0];

		/// <summary>Gets or sets the element at the specified index.</summary>
		/// <returns>The element at the specified index.</returns>
		/// <param name="index">The zero-based index of the element to get or set. </param>
		/// <exception cref="T:System.ArgumentOutOfRangeException">
		///   <paramref name="index" /> is less than zero.-or- <paramref name="index" /> is equal to or greater than <see cref="P:System.Collections.ArrayList.Count" />. </exception>
		/// <filterpriority>1</filterpriority>
		public virtual object this[int index]
		{
			get
			{
				if (index < 0 || index >= _size)
				{
					ThrowNewArgumentOutOfRangeException("index", index, "Index is less than 0 or more than or equal to the list count.");
				}
				return _items[index];
			}
			set
			{
				if (index < 0 || index >= _size)
				{
					ThrowNewArgumentOutOfRangeException("index", index, "Index is less than 0 or more than or equal to the list count.");
				}
				_items[index] = value;
				_version++;
			}
		}

		/// <summary>Gets the number of elements actually contained in the <see cref="T:System.Collections.ArrayList" />.</summary>
		/// <returns>The number of elements actually contained in the <see cref="T:System.Collections.ArrayList" />.</returns>
		/// <filterpriority>1</filterpriority>
		public virtual int Count => _size;

		/// <summary>Gets or sets the number of elements that the <see cref="T:System.Collections.ArrayList" /> can contain.</summary>
		/// <returns>The number of elements that the <see cref="T:System.Collections.ArrayList" /> can contain.</returns>
		/// <exception cref="T:System.ArgumentOutOfRangeException">
		///   <see cref="P:System.Collections.ArrayList.Capacity" /> is set to a value that is less than <see cref="P:System.Collections.ArrayList.Count" />.</exception>
		/// <exception cref="T:System.OutOfMemoryException">There is not enough memory available on the system.</exception>
		/// <filterpriority>1</filterpriority>
		public virtual int Capacity
		{
			get
			{
				return _items.Length;
			}
			set
			{
				if (value < _size)
				{
					ThrowNewArgumentOutOfRangeException("Capacity", value, "Must be more than count.");
				}
				object[] array = new object[value];
				Array.Copy(_items, 0, array, 0, _size);
				_items = array;
			}
		}

		/// <summary>Gets a value indicating whether the <see cref="T:System.Collections.ArrayList" /> has a fixed size.</summary>
		/// <returns>true if the <see cref="T:System.Collections.ArrayList" /> has a fixed size; otherwise, false. The default is false.</returns>
		/// <filterpriority>2</filterpriority>
		public virtual bool IsFixedSize => false;

		/// <summary>Gets a value indicating whether the <see cref="T:System.Collections.ArrayList" /> is read-only.</summary>
		/// <returns>true if the <see cref="T:System.Collections.ArrayList" /> is read-only; otherwise, false. The default is false.</returns>
		/// <filterpriority>2</filterpriority>
		public virtual bool IsReadOnly => false;

		/// <summary>Gets a value indicating whether access to the <see cref="T:System.Collections.ArrayList" /> is synchronized (thread safe).</summary>
		/// <returns>true if access to the <see cref="T:System.Collections.ArrayList" /> is synchronized (thread safe); otherwise, false. The default is false.</returns>
		/// <filterpriority>2</filterpriority>
		public virtual bool IsSynchronized => false;

		/// <summary>Gets an object that can be used to synchronize access to the <see cref="T:System.Collections.ArrayList" />.</summary>
		/// <returns>An object that can be used to synchronize access to the <see cref="T:System.Collections.ArrayList" />.</returns>
		/// <filterpriority>2</filterpriority>
		public virtual object SyncRoot => this;

		/// <summary>Initializes a new instance of the <see cref="T:System.Collections.ArrayList" /> class that is empty and has the default initial capacity.</summary>
		public ArrayList()
		{
			_items = EmptyArray;
		}

		/// <summary>Initializes a new instance of the <see cref="T:System.Collections.ArrayList" /> class that contains elements copied from the specified collection and that has the same initial capacity as the number of elements copied.</summary>
		/// <param name="c">The <see cref="T:System.Collections.ICollection" /> whose elements are copied to the new list. </param>
		/// <exception cref="T:System.ArgumentNullException">
		///   <paramref name="c" /> is null. </exception>
		public ArrayList(ICollection c)
		{
			if (c == null)
			{
				throw new ArgumentNullException("c");
			}
			Array array = c as Array;
			if (array != null && array.Rank != 1)
			{
				throw new RankException();
			}
			_items = new object[c.Count];
			AddRange(c);
		}

		/// <summary>Initializes a new instance of the <see cref="T:System.Collections.ArrayList" /> class that is empty and has the specified initial capacity.</summary>
		/// <param name="capacity">The number of elements that the new list can initially store. </param>
		/// <exception cref="T:System.ArgumentOutOfRangeException">
		///   <paramref name="capacity" /> is less than zero. </exception>
		public ArrayList(int capacity)
		{
			if (capacity < 0)
			{
				ThrowNewArgumentOutOfRangeException("capacity", capacity, "The initial capacity can't be smaller than zero.");
			}
			if (capacity == 0)
			{
				capacity = 4;
			}
			_items = new object[capacity];
		}

		private ArrayList(int initialCapacity, bool forceZeroSize)
		{
			if (forceZeroSize)
			{
				_items = null;
				return;
			}
			throw new InvalidOperationException("Use ArrayList(int)");
		}

		private ArrayList(object[] array, int index, int count)
		{
			if (count == 0)
			{
				_items = new object[4];
			}
			else
			{
				_items = new object[count];
			}
			Array.Copy(array, index, _items, 0, count);
			_size = count;
		}

		private void EnsureCapacity(int count)
		{
			if (count > _items.Length)
			{
				int num = _items.Length << 1;
				if (num == 0)
				{
					num = 4;
				}
				while (num < count)
				{
					num <<= 1;
				}
				object[] array = new object[num];
				Array.Copy(_items, 0, array, 0, _items.Length);
				_items = array;
			}
		}

		private void Shift(int index, int count)
		{
			if (count > 0)
			{
				if (_size + count > _items.Length)
				{
					int num;
					for (num = ((_items.Length <= 0) ? 1 : (_items.Length << 1)); num < _size + count; num <<= 1)
					{
					}
					object[] array = new object[num];
					Array.Copy(_items, 0, array, 0, index);
					Array.Copy(_items, index, array, index + count, _size - index);
					_items = array;
				}
				else
				{
					Array.Copy(_items, index, _items, index + count, _size - index);
				}
			}
			else if (count < 0)
			{
				int num2 = index - count;
				Array.Copy(_items, num2, _items, index, _size - num2);
				Array.Clear(_items, _size + count, -count);
			}
		}

		/// <summary>Adds an object to the end of the <see cref="T:System.Collections.ArrayList" />.</summary>
		/// <returns>The <see cref="T:System.Collections.ArrayList" /> index at which the <paramref name="value" /> has been added.</returns>
		/// <param name="value">The <see cref="T:System.Object" /> to be added to the end of the <see cref="T:System.Collections.ArrayList" />. The value can be null. </param>
		/// <exception cref="T:System.NotSupportedException">The <see cref="T:System.Collections.ArrayList" /> is read-only.-or- The <see cref="T:System.Collections.ArrayList" /> has a fixed size. </exception>
		/// <filterpriority>1</filterpriority>
		public virtual int Add(object value)
		{
			if (_items.Length <= _size)
			{
				EnsureCapacity(_size + 1);
			}
			_items[_size] = value;
			_version++;
			return _size++;
		}

		/// <summary>Removes all elements from the <see cref="T:System.Collections.ArrayList" />.</summary>
		/// <exception cref="T:System.NotSupportedException">The <see cref="T:System.Collections.ArrayList" /> is read-only.-or- The <see cref="T:System.Collections.ArrayList" /> has a fixed size. </exception>
		/// <filterpriority>1</filterpriority>
		public virtual void Clear()
		{
			Array.Clear(_items, 0, _size);
			_size = 0;
			_version++;
		}

		/// <summary>Determines whether an element is in the <see cref="T:System.Collections.ArrayList" />.</summary>
		/// <returns>true if <paramref name="item" /> is found in the <see cref="T:System.Collections.ArrayList" />; otherwise, false.</returns>
		/// <param name="item">The <see cref="T:System.Object" /> to locate in the <see cref="T:System.Collections.ArrayList" />. The value can be null. </param>
		/// <filterpriority>1</filterpriority>
		public virtual bool Contains(object item)
		{
			return IndexOf(item, 0, _size) > -1;
		}

		internal virtual bool Contains(object value, int startIndex, int count)
		{
			return IndexOf(value, startIndex, count) > -1;
		}

		/// <summary>Searches for the specified <see cref="T:System.Object" /> and returns the zero-based index of the first occurrence within the entire <see cref="T:System.Collections.ArrayList" />.</summary>
		/// <returns>The zero-based index of the first occurrence of <paramref name="value" /> within the entire <see cref="T:System.Collections.ArrayList" />, if found; otherwise, -1.</returns>
		/// <param name="value">The <see cref="T:System.Object" /> to locate in the <see cref="T:System.Collections.ArrayList" />. The value can be null. </param>
		/// <filterpriority>1</filterpriority>
		public virtual int IndexOf(object value)
		{
			return IndexOf(value, 0);
		}

		/// <summary>Searches for the specified <see cref="T:System.Object" /> and returns the zero-based index of the first occurrence within the range of elements in the <see cref="T:System.Collections.ArrayList" /> that extends from the specified index to the last element.</summary>
		/// <returns>The zero-based index of the first occurrence of <paramref name="value" /> within the range of elements in the <see cref="T:System.Collections.ArrayList" /> that extends from <paramref name="startIndex" /> to the last element, if found; otherwise, -1.</returns>
		/// <param name="value">The <see cref="T:System.Object" /> to locate in the <see cref="T:System.Collections.ArrayList" />. The value can be null. </param>
		/// <param name="startIndex">The zero-based starting index of the search. </param>
		/// <exception cref="T:System.ArgumentOutOfRangeException">
		///   <paramref name="startIndex" /> is outside the range of valid indexes for the <see cref="T:System.Collections.ArrayList" />. </exception>
		/// <filterpriority>1</filterpriority>
		public virtual int IndexOf(object value, int startIndex)
		{
			return IndexOf(value, startIndex, _size - startIndex);
		}

		/// <summary>Searches for the specified <see cref="T:System.Object" /> and returns the zero-based index of the first occurrence within the range of elements in the <see cref="T:System.Collections.ArrayList" /> that starts at the specified index and contains the specified number of elements.</summary>
		/// <returns>The zero-based index of the first occurrence of <paramref name="value" /> within the range of elements in the <see cref="T:System.Collections.ArrayList" /> that starts at <paramref name="startIndex" /> and contains <paramref name="count" /> number of elements, if found; otherwise, -1.</returns>
		/// <param name="value">The <see cref="T:System.Object" /> to locate in the <see cref="T:System.Collections.ArrayList" />. The value can be null. </param>
		/// <param name="startIndex">The zero-based starting index of the search. </param>
		/// <param name="count">The number of elements in the section to search. </param>
		/// <exception cref="T:System.ArgumentOutOfRangeException">
		///   <paramref name="startIndex" /> is outside the range of valid indexes for the <see cref="T:System.Collections.ArrayList" />.-or- <paramref name="count" /> is less than zero.-or- <paramref name="startIndex" /> and <paramref name="count" /> do not specify a valid section in the <see cref="T:System.Collections.ArrayList" />. </exception>
		/// <filterpriority>1</filterpriority>
		public virtual int IndexOf(object value, int startIndex, int count)
		{
			if (startIndex < 0 || startIndex > _size)
			{
				ThrowNewArgumentOutOfRangeException("startIndex", startIndex, "Does not specify valid index.");
			}
			if (count < 0)
			{
				ThrowNewArgumentOutOfRangeException("count", count, "Can't be less than 0.");
			}
			if (startIndex > _size - count)
			{
				throw new ArgumentOutOfRangeException("count", "Start index and count do not specify a valid range.");
			}
			return Array.IndexOf(_items, value, startIndex, count);
		}

		/// <summary>Searches for the specified <see cref="T:System.Object" /> and returns the zero-based index of the last occurrence within the entire <see cref="T:System.Collections.ArrayList" />.</summary>
		/// <returns>The zero-based index of the last occurrence of <paramref name="value" /> within the entire the <see cref="T:System.Collections.ArrayList" />, if found; otherwise, -1.</returns>
		/// <param name="value">The <see cref="T:System.Object" /> to locate in the <see cref="T:System.Collections.ArrayList" />. The value can be null. </param>
		/// <filterpriority>2</filterpriority>
		public virtual int LastIndexOf(object value)
		{
			return LastIndexOf(value, _size - 1);
		}

		/// <summary>Searches for the specified <see cref="T:System.Object" /> and returns the zero-based index of the last occurrence within the range of elements in the <see cref="T:System.Collections.ArrayList" /> that extends from the first element to the specified index.</summary>
		/// <returns>The zero-based index of the last occurrence of <paramref name="value" /> within the range of elements in the <see cref="T:System.Collections.ArrayList" /> that extends from the first element to <paramref name="startIndex" />, if found; otherwise, -1.</returns>
		/// <param name="value">The <see cref="T:System.Object" /> to locate in the <see cref="T:System.Collections.ArrayList" />. The value can be null. </param>
		/// <param name="startIndex">The zero-based starting index of the backward search. </param>
		/// <exception cref="T:System.ArgumentOutOfRangeException">
		///   <paramref name="startIndex" /> is outside the range of valid indexes for the <see cref="T:System.Collections.ArrayList" />. </exception>
		/// <filterpriority>2</filterpriority>
		public virtual int LastIndexOf(object value, int startIndex)
		{
			return LastIndexOf(value, startIndex, startIndex + 1);
		}

		/// <summary>Searches for the specified <see cref="T:System.Object" /> and returns the zero-based index of the last occurrence within the range of elements in the <see cref="T:System.Collections.ArrayList" /> that contains the specified number of elements and ends at the specified index.</summary>
		/// <returns>The zero-based index of the last occurrence of <paramref name="value" /> within the range of elements in the <see cref="T:System.Collections.ArrayList" /> that contains <paramref name="count" /> number of elements and ends at <paramref name="startIndex" />, if found; otherwise, -1.</returns>
		/// <param name="value">The <see cref="T:System.Object" /> to locate in the <see cref="T:System.Collections.ArrayList" />. The value can be null. </param>
		/// <param name="startIndex">The zero-based starting index of the backward search. </param>
		/// <param name="count">The number of elements in the section to search. </param>
		/// <exception cref="T:System.ArgumentOutOfRangeException">
		///   <paramref name="startIndex" /> is outside the range of valid indexes for the <see cref="T:System.Collections.ArrayList" />.-or- <paramref name="count" /> is less than zero.-or- <paramref name="startIndex" /> and <paramref name="count" /> do not specify a valid section in the <see cref="T:System.Collections.ArrayList" />. </exception>
		/// <filterpriority>2</filterpriority>
		public virtual int LastIndexOf(object value, int startIndex, int count)
		{
			return Array.LastIndexOf(_items, value, startIndex, count);
		}

		/// <summary>Inserts an element into the <see cref="T:System.Collections.ArrayList" /> at the specified index.</summary>
		/// <param name="index">The zero-based index at which <paramref name="value" /> should be inserted. </param>
		/// <param name="value">The <see cref="T:System.Object" /> to insert. The value can be null. </param>
		/// <exception cref="T:System.ArgumentOutOfRangeException">
		///   <paramref name="index" /> is less than zero.-or- <paramref name="index" /> is greater than <see cref="P:System.Collections.ArrayList.Count" />. </exception>
		/// <exception cref="T:System.NotSupportedException">The <see cref="T:System.Collections.ArrayList" /> is read-only.-or- The <see cref="T:System.Collections.ArrayList" /> has a fixed size. </exception>
		/// <filterpriority>1</filterpriority>
		public virtual void Insert(int index, object value)
		{
			if (index < 0 || index > _size)
			{
				ThrowNewArgumentOutOfRangeException("index", index, "Index must be >= 0 and <= Count.");
			}
			Shift(index, 1);
			_items[index] = value;
			_size++;
			_version++;
		}

		/// <summary>Inserts the elements of a collection into the <see cref="T:System.Collections.ArrayList" /> at the specified index.</summary>
		/// <param name="index">The zero-based index at which the new elements should be inserted. </param>
		/// <param name="c">The <see cref="T:System.Collections.ICollection" /> whose elements should be inserted into the <see cref="T:System.Collections.ArrayList" />. The collection itself cannot be null, but it can contain elements that are null. </param>
		/// <exception cref="T:System.ArgumentNullException">
		///   <paramref name="c" /> is null. </exception>
		/// <exception cref="T:System.ArgumentOutOfRangeException">
		///   <paramref name="index" /> is less than zero.-or- <paramref name="index" /> is greater than <see cref="P:System.Collections.ArrayList.Count" />. </exception>
		/// <exception cref="T:System.NotSupportedException">The <see cref="T:System.Collections.ArrayList" /> is read-only.-or- The <see cref="T:System.Collections.ArrayList" /> has a fixed size. </exception>
		/// <filterpriority>2</filterpriority>
		public virtual void InsertRange(int index, ICollection c)
		{
			if (c == null)
			{
				throw new ArgumentNullException("c");
			}
			if (index < 0 || index > _size)
			{
				ThrowNewArgumentOutOfRangeException("index", index, "Index must be >= 0 and <= Count.");
			}
			int count = c.Count;
			if (_items.Length < _size + count)
			{
				EnsureCapacity(_size + count);
			}
			if (index < _size)
			{
				Array.Copy(_items, index, _items, index + count, _size - index);
			}
			if (this == c.SyncRoot)
			{
				Array.Copy(_items, 0, _items, index, index);
				Array.Copy(_items, index + count, _items, index << 1, _size - index);
			}
			else
			{
				c.CopyTo(_items, index);
			}
			_size += c.Count;
			_version++;
		}

		/// <summary>Removes the first occurrence of a specific object from the <see cref="T:System.Collections.ArrayList" />.</summary>
		/// <param name="obj">The <see cref="T:System.Object" /> to remove from the <see cref="T:System.Collections.ArrayList" />. The value can be null. </param>
		/// <exception cref="T:System.NotSupportedException">The <see cref="T:System.Collections.ArrayList" /> is read-only.-or- The <see cref="T:System.Collections.ArrayList" /> has a fixed size. </exception>
		/// <filterpriority>1</filterpriority>
		public virtual void Remove(object obj)
		{
			int num = IndexOf(obj);
			if (num > -1)
			{
				RemoveAt(num);
			}
			_version++;
		}

		/// <summary>Removes the element at the specified index of the <see cref="T:System.Collections.ArrayList" />.</summary>
		/// <param name="index">The zero-based index of the element to remove. </param>
		/// <exception cref="T:System.ArgumentOutOfRangeException">
		///   <paramref name="index" /> is less than zero.-or- <paramref name="index" /> is equal to or greater than <see cref="P:System.Collections.ArrayList.Count" />. </exception>
		/// <exception cref="T:System.NotSupportedException">The <see cref="T:System.Collections.ArrayList" /> is read-only.-or- The <see cref="T:System.Collections.ArrayList" /> has a fixed size. </exception>
		/// <filterpriority>1</filterpriority>
		public virtual void RemoveAt(int index)
		{
			if (index < 0 || index >= _size)
			{
				ThrowNewArgumentOutOfRangeException("index", index, "Less than 0 or more than list count.");
			}
			Shift(index, -1);
			_size--;
			_version++;
		}

		/// <summary>Removes a range of elements from the <see cref="T:System.Collections.ArrayList" />.</summary>
		/// <param name="index">The zero-based starting index of the range of elements to remove. </param>
		/// <param name="count">The number of elements to remove. </param>
		/// <exception cref="T:System.ArgumentOutOfRangeException">
		///   <paramref name="index" /> is less than zero.-or- <paramref name="count" /> is less than zero. </exception>
		/// <exception cref="T:System.ArgumentException">
		///   <paramref name="index" /> and <paramref name="count" /> do not denote a valid range of elements in the <see cref="T:System.Collections.ArrayList" />. </exception>
		/// <exception cref="T:System.NotSupportedException">The <see cref="T:System.Collections.ArrayList" /> is read-only.-or- The <see cref="T:System.Collections.ArrayList" /> has a fixed size. </exception>
		/// <filterpriority>2</filterpriority>
		public virtual void RemoveRange(int index, int count)
		{
			CheckRange(index, count, _size);
			Shift(index, -count);
			_size -= count;
			_version++;
		}

		/// <summary>Reverses the order of the elements in the entire <see cref="T:System.Collections.ArrayList" />.</summary>
		/// <exception cref="T:System.NotSupportedException">The <see cref="T:System.Collections.ArrayList" /> is read-only. </exception>
		/// <filterpriority>2</filterpriority>
		public virtual void Reverse()
		{
			Array.Reverse(_items, 0, _size);
			_version++;
		}

		/// <summary>Reverses the order of the elements in the specified range.</summary>
		/// <param name="index">The zero-based starting index of the range to reverse. </param>
		/// <param name="count">The number of elements in the range to reverse. </param>
		/// <exception cref="T:System.ArgumentOutOfRangeException">
		///   <paramref name="index" /> is less than zero.-or- <paramref name="count" /> is less than zero. </exception>
		/// <exception cref="T:System.ArgumentException">
		///   <paramref name="index" /> and <paramref name="count" /> do not denote a valid range of elements in the <see cref="T:System.Collections.ArrayList" />. </exception>
		/// <exception cref="T:System.NotSupportedException">The <see cref="T:System.Collections.ArrayList" /> is read-only. </exception>
		/// <filterpriority>2</filterpriority>
		public virtual void Reverse(int index, int count)
		{
			CheckRange(index, count, _size);
			Array.Reverse(_items, index, count);
			_version++;
		}

		/// <summary>Copies the entire <see cref="T:System.Collections.ArrayList" /> to a compatible one-dimensional <see cref="T:System.Array" />, starting at the beginning of the target array.</summary>
		/// <param name="array">The one-dimensional <see cref="T:System.Array" /> that is the destination of the elements copied from <see cref="T:System.Collections.ArrayList" />. The <see cref="T:System.Array" /> must have zero-based indexing. </param>
		/// <exception cref="T:System.ArgumentNullException">
		///   <paramref name="array" /> is null. </exception>
		/// <exception cref="T:System.ArgumentException">
		///   <paramref name="array" /> is multidimensional.-or- The number of elements in the source <see cref="T:System.Collections.ArrayList" /> is greater than the number of elements that the destination <paramref name="array" /> can contain. </exception>
		/// <exception cref="T:System.InvalidCastException">The type of the source <see cref="T:System.Collections.ArrayList" /> cannot be cast automatically to the type of the destination <paramref name="array" />. </exception>
		/// <filterpriority>2</filterpriority>
		public virtual void CopyTo(Array array)
		{
			Array.Copy(_items, array, _size);
		}

		/// <summary>Copies the entire <see cref="T:System.Collections.ArrayList" /> to a compatible one-dimensional <see cref="T:System.Array" />, starting at the specified index of the target array.</summary>
		/// <param name="array">The one-dimensional <see cref="T:System.Array" /> that is the destination of the elements copied from <see cref="T:System.Collections.ArrayList" />. The <see cref="T:System.Array" /> must have zero-based indexing. </param>
		/// <param name="arrayIndex">The zero-based index in <paramref name="array" /> at which copying begins. </param>
		/// <exception cref="T:System.ArgumentNullException">
		///   <paramref name="array" /> is null. </exception>
		/// <exception cref="T:System.ArgumentOutOfRangeException">
		///   <paramref name="arrayIndex" /> is less than zero. </exception>
		/// <exception cref="T:System.ArgumentException">
		///   <paramref name="array" /> is multidimensional.-or- <paramref name="arrayIndex" /> is equal to or greater than the length of <paramref name="array" />.-or- The number of elements in the source <see cref="T:System.Collections.ArrayList" /> is greater than the available space from <paramref name="arrayIndex" /> to the end of the destination <paramref name="array" />. </exception>
		/// <exception cref="T:System.InvalidCastException">The type of the source <see cref="T:System.Collections.ArrayList" /> cannot be cast automatically to the type of the destination <paramref name="array" />. </exception>
		/// <filterpriority>2</filterpriority>
		public virtual void CopyTo(Array array, int arrayIndex)
		{
			CopyTo(0, array, arrayIndex, _size);
		}

		/// <summary>Copies a range of elements from the <see cref="T:System.Collections.ArrayList" /> to a compatible one-dimensional <see cref="T:System.Array" />, starting at the specified index of the target array.</summary>
		/// <param name="index">The zero-based index in the source <see cref="T:System.Collections.ArrayList" /> at which copying begins. </param>
		/// <param name="array">The one-dimensional <see cref="T:System.Array" /> that is the destination of the elements copied from <see cref="T:System.Collections.ArrayList" />. The <see cref="T:System.Array" /> must have zero-based indexing. </param>
		/// <param name="arrayIndex">The zero-based index in <paramref name="array" /> at which copying begins. </param>
		/// <param name="count">The number of elements to copy. </param>
		/// <exception cref="T:System.ArgumentNullException">
		///   <paramref name="array" /> is null. </exception>
		/// <exception cref="T:System.ArgumentOutOfRangeException">
		///   <paramref name="index" /> is less than zero.-or- <paramref name="arrayIndex" /> is less than zero.-or- <paramref name="count" /> is less than zero. </exception>
		/// <exception cref="T:System.ArgumentException">
		///   <paramref name="array" /> is multidimensional.-or- <paramref name="index" /> is equal to or greater than the <see cref="P:System.Collections.ArrayList.Count" /> of the source <see cref="T:System.Collections.ArrayList" />.-or- <paramref name="arrayIndex" /> is equal to or greater than the length of <paramref name="array" />.-or- The number of elements from <paramref name="index" /> to the end of the source <see cref="T:System.Collections.ArrayList" /> is greater than the available space from <paramref name="arrayIndex" /> to the end of the destination <paramref name="array" />. </exception>
		/// <exception cref="T:System.InvalidCastException">The type of the source <see cref="T:System.Collections.ArrayList" /> cannot be cast automatically to the type of the destination <paramref name="array" />. </exception>
		/// <filterpriority>2</filterpriority>
		public virtual void CopyTo(int index, Array array, int arrayIndex, int count)
		{
			if (array == null)
			{
				throw new ArgumentNullException("array");
			}
			if (array.Rank != 1)
			{
				throw new ArgumentException("Must have only 1 dimensions.", "array");
			}
			Array.Copy(_items, index, array, arrayIndex, count);
		}

		/// <summary>Returns an enumerator for the entire <see cref="T:System.Collections.ArrayList" />.</summary>
		/// <returns>An <see cref="T:System.Collections.IEnumerator" /> for the entire <see cref="T:System.Collections.ArrayList" />.</returns>
		/// <filterpriority>2</filterpriority>
		public virtual IEnumerator GetEnumerator()
		{
			return new SimpleEnumerator(this);
		}

		/// <summary>Returns an enumerator for a range of elements in the <see cref="T:System.Collections.ArrayList" />.</summary>
		/// <returns>An <see cref="T:System.Collections.IEnumerator" /> for the specified range of elements in the <see cref="T:System.Collections.ArrayList" />.</returns>
		/// <param name="index">The zero-based starting index of the <see cref="T:System.Collections.ArrayList" /> section that the enumerator should refer to. </param>
		/// <param name="count">The number of elements in the <see cref="T:System.Collections.ArrayList" /> section that the enumerator should refer to. </param>
		/// <exception cref="T:System.ArgumentOutOfRangeException">
		///   <paramref name="index" /> is less than zero.-or- <paramref name="count" /> is less than zero. </exception>
		/// <exception cref="T:System.ArgumentException">
		///   <paramref name="index" /> and <paramref name="count" /> do not specify a valid range in the <see cref="T:System.Collections.ArrayList" />. </exception>
		/// <filterpriority>2</filterpriority>
		public virtual IEnumerator GetEnumerator(int index, int count)
		{
			CheckRange(index, count, _size);
			return new ArrayListEnumerator(this, index, count);
		}

		/// <summary>Adds the elements of an <see cref="T:System.Collections.ICollection" /> to the end of the <see cref="T:System.Collections.ArrayList" />.</summary>
		/// <param name="c">The <see cref="T:System.Collections.ICollection" /> whose elements should be added to the end of the <see cref="T:System.Collections.ArrayList" />. The collection itself cannot be null, but it can contain elements that are null. </param>
		/// <exception cref="T:System.ArgumentNullException">
		///   <paramref name="c" /> is null. </exception>
		/// <exception cref="T:System.NotSupportedException">The <see cref="T:System.Collections.ArrayList" /> is read-only.-or- The <see cref="T:System.Collections.ArrayList" /> has a fixed size. </exception>
		/// <filterpriority>1</filterpriority>
		public virtual void AddRange(ICollection c)
		{
			InsertRange(_size, c);
		}

		/// <summary>Searches the entire sorted <see cref="T:System.Collections.ArrayList" /> for an element using the default comparer and returns the zero-based index of the element.</summary>
		/// <returns>The zero-based index of <paramref name="value" /> in the sorted <see cref="T:System.Collections.ArrayList" />, if <paramref name="value" /> is found; otherwise, a negative number, which is the bitwise complement of the index of the next element that is larger than <paramref name="value" /> or, if there is no larger element, the bitwise complement of <see cref="P:System.Collections.ArrayList.Count" />.</returns>
		/// <param name="value">The <see cref="T:System.Object" /> to locate. The value can be null. </param>
		/// <exception cref="T:System.ArgumentException">Neither <paramref name="value" /> nor the elements of <see cref="T:System.Collections.ArrayList" /> implement the <see cref="T:System.IComparable" /> interface. </exception>
		/// <exception cref="T:System.InvalidOperationException">
		///   <paramref name="value" /> is not of the same type as the elements of the <see cref="T:System.Collections.ArrayList" />. </exception>
		/// <filterpriority>1</filterpriority>
		public virtual int BinarySearch(object value)
		{
			//Discarded unreachable code: IL_0019, IL_002b
			try
			{
				return Array.BinarySearch(_items, 0, _size, value);
			}
			catch (InvalidOperationException ex)
			{
				throw new ArgumentException(ex.Message);
			}
		}

		/// <summary>Searches the entire sorted <see cref="T:System.Collections.ArrayList" /> for an element using the specified comparer and returns the zero-based index of the element.</summary>
		/// <returns>The zero-based index of <paramref name="value" /> in the sorted <see cref="T:System.Collections.ArrayList" />, if <paramref name="value" /> is found; otherwise, a negative number, which is the bitwise complement of the index of the next element that is larger than <paramref name="value" /> or, if there is no larger element, the bitwise complement of <see cref="P:System.Collections.ArrayList.Count" />.</returns>
		/// <param name="value">The <see cref="T:System.Object" /> to locate. The value can be null. </param>
		/// <param name="comparer">The <see cref="T:System.Collections.IComparer" /> implementation to use when comparing elements.-or- null to use the default comparer that is the <see cref="T:System.IComparable" /> implementation of each element. </param>
		/// <exception cref="T:System.ArgumentException">
		///   <paramref name="comparer" /> is null and neither <paramref name="value" /> nor the elements of <see cref="T:System.Collections.ArrayList" /> implement the <see cref="T:System.IComparable" /> interface. </exception>
		/// <exception cref="T:System.InvalidOperationException">
		///   <paramref name="comparer" /> is null and <paramref name="value" /> is not of the same type as the elements of the <see cref="T:System.Collections.ArrayList" />. </exception>
		/// <filterpriority>1</filterpriority>
		public virtual int BinarySearch(object value, IComparer comparer)
		{
			//Discarded unreachable code: IL_001a, IL_002c
			try
			{
				return Array.BinarySearch(_items, 0, _size, value, comparer);
			}
			catch (InvalidOperationException ex)
			{
				throw new ArgumentException(ex.Message);
			}
		}

		/// <summary>Searches a range of elements in the sorted <see cref="T:System.Collections.ArrayList" /> for an element using the specified comparer and returns the zero-based index of the element.</summary>
		/// <returns>The zero-based index of <paramref name="value" /> in the sorted <see cref="T:System.Collections.ArrayList" />, if <paramref name="value" /> is found; otherwise, a negative number, which is the bitwise complement of the index of the next element that is larger than <paramref name="value" /> or, if there is no larger element, the bitwise complement of <see cref="P:System.Collections.ArrayList.Count" />.</returns>
		/// <param name="index">The zero-based starting index of the range to search. </param>
		/// <param name="count">The length of the range to search. </param>
		/// <param name="value">The <see cref="T:System.Object" /> to locate. The value can be null. </param>
		/// <param name="comparer">The <see cref="T:System.Collections.IComparer" /> implementation to use when comparing elements.-or- null to use the default comparer that is the <see cref="T:System.IComparable" /> implementation of each element. </param>
		/// <exception cref="T:System.ArgumentException">
		///   <paramref name="index" /> and <paramref name="count" /> do not denote a valid range in the <see cref="T:System.Collections.ArrayList" />.-or- <paramref name="comparer" /> is null and neither <paramref name="value" /> nor the elements of <see cref="T:System.Collections.ArrayList" /> implement the <see cref="T:System.IComparable" /> interface. </exception>
		/// <exception cref="T:System.InvalidOperationException">
		///   <paramref name="comparer" /> is null and <paramref name="value" /> is not of the same type as the elements of the <see cref="T:System.Collections.ArrayList" />. </exception>
		/// <exception cref="T:System.ArgumentOutOfRangeException">
		///   <paramref name="index" /> is less than zero.-or- <paramref name="count" /> is less than zero. </exception>
		/// <filterpriority>1</filterpriority>
		public virtual int BinarySearch(int index, int count, object value, IComparer comparer)
		{
			//Discarded unreachable code: IL_0016, IL_0028
			try
			{
				return Array.BinarySearch(_items, index, count, value, comparer);
			}
			catch (InvalidOperationException ex)
			{
				throw new ArgumentException(ex.Message);
			}
		}

		/// <summary>Returns an <see cref="T:System.Collections.ArrayList" /> which represents a subset of the elements in the source <see cref="T:System.Collections.ArrayList" />.</summary>
		/// <returns>An <see cref="T:System.Collections.ArrayList" /> which represents a subset of the elements in the source <see cref="T:System.Collections.ArrayList" />.</returns>
		/// <param name="index">The zero-based <see cref="T:System.Collections.ArrayList" /> index at which the range starts. </param>
		/// <param name="count">The number of elements in the range. </param>
		/// <exception cref="T:System.ArgumentOutOfRangeException">
		///   <paramref name="index" /> is less than zero.-or- <paramref name="count" /> is less than zero. </exception>
		/// <exception cref="T:System.ArgumentException">
		///   <paramref name="index" /> and <paramref name="count" /> do not denote a valid range of elements in the <see cref="T:System.Collections.ArrayList" />. </exception>
		/// <filterpriority>2</filterpriority>
		public virtual ArrayList GetRange(int index, int count)
		{
			CheckRange(index, count, _size);
			if (IsSynchronized)
			{
				return Synchronized(new RangedArrayList(this, index, count));
			}
			return new RangedArrayList(this, index, count);
		}

		/// <summary>Copies the elements of a collection over a range of elements in the <see cref="T:System.Collections.ArrayList" />.</summary>
		/// <param name="index">The zero-based <see cref="T:System.Collections.ArrayList" /> index at which to start copying the elements of <paramref name="c" />. </param>
		/// <param name="c">The <see cref="T:System.Collections.ICollection" /> whose elements to copy to the <see cref="T:System.Collections.ArrayList" />. The collection itself cannot be null, but it can contain elements that are null. </param>
		/// <exception cref="T:System.ArgumentOutOfRangeException">
		///   <paramref name="index" /> is less than zero.-or- <paramref name="index" /> plus the number of elements in <paramref name="c" /> is greater than <see cref="P:System.Collections.ArrayList.Count" />. </exception>
		/// <exception cref="T:System.ArgumentNullException">
		///   <paramref name="c" /> is null. </exception>
		/// <exception cref="T:System.NotSupportedException">The <see cref="T:System.Collections.ArrayList" /> is read-only. </exception>
		/// <filterpriority>2</filterpriority>
		public virtual void SetRange(int index, ICollection c)
		{
			if (c == null)
			{
				throw new ArgumentNullException("c");
			}
			if (index < 0 || index + c.Count > _size)
			{
				throw new ArgumentOutOfRangeException("index");
			}
			c.CopyTo(_items, index);
			_version++;
		}

		/// <summary>Sets the capacity to the actual number of elements in the <see cref="T:System.Collections.ArrayList" />.</summary>
		/// <exception cref="T:System.NotSupportedException">The <see cref="T:System.Collections.ArrayList" /> is read-only.-or- The <see cref="T:System.Collections.ArrayList" /> has a fixed size. </exception>
		/// <filterpriority>2</filterpriority>
		public virtual void TrimToSize()
		{
			if (_items.Length > _size)
			{
				object[] array = (_size != 0) ? new object[_size] : new object[4];
				Array.Copy(_items, 0, array, 0, _size);
				_items = array;
			}
		}

		/// <summary>Sorts the elements in the entire <see cref="T:System.Collections.ArrayList" /> using the <see cref="T:System.IComparable" /> implementation of each element.</summary>
		/// <exception cref="T:System.NotSupportedException">The <see cref="T:System.Collections.ArrayList" /> is read-only. </exception>
		/// <filterpriority>1</filterpriority>
		public virtual void Sort()
		{
			Array.Sort(_items, 0, _size);
			_version++;
		}

		/// <summary>Sorts the elements in the entire <see cref="T:System.Collections.ArrayList" /> using the specified comparer.</summary>
		/// <param name="comparer">The <see cref="T:System.Collections.IComparer" /> implementation to use when comparing elements.-or- null to use the <see cref="T:System.IComparable" /> implementation of each element. </param>
		/// <exception cref="T:System.NotSupportedException">The <see cref="T:System.Collections.ArrayList" /> is read-only. </exception>
		/// <filterpriority>1</filterpriority>
		public virtual void Sort(IComparer comparer)
		{
			Array.Sort(_items, 0, _size, comparer);
		}

		/// <summary>Sorts the elements in a range of elements in <see cref="T:System.Collections.ArrayList" /> using the specified comparer.</summary>
		/// <param name="index">The zero-based starting index of the range to sort. </param>
		/// <param name="count">The length of the range to sort. </param>
		/// <param name="comparer">The <see cref="T:System.Collections.IComparer" /> implementation to use when comparing elements.-or- null to use the <see cref="T:System.IComparable" /> implementation of each element. </param>
		/// <exception cref="T:System.ArgumentOutOfRangeException">
		///   <paramref name="index" /> is less than zero.-or- <paramref name="count" /> is less than zero. </exception>
		/// <exception cref="T:System.ArgumentException">
		///   <paramref name="index" /> and <paramref name="count" /> do not specify a valid range in the <see cref="T:System.Collections.ArrayList" />. </exception>
		/// <exception cref="T:System.NotSupportedException">The <see cref="T:System.Collections.ArrayList" /> is read-only. </exception>
		/// <filterpriority>1</filterpriority>
		public virtual void Sort(int index, int count, IComparer comparer)
		{
			CheckRange(index, count, _size);
			Array.Sort(_items, index, count, comparer);
		}

		/// <summary>Copies the elements of the <see cref="T:System.Collections.ArrayList" /> to a new <see cref="T:System.Object" /> array.</summary>
		/// <returns>An <see cref="T:System.Object" /> array containing copies of the elements of the <see cref="T:System.Collections.ArrayList" />.</returns>
		/// <filterpriority>1</filterpriority>
		public virtual object[] ToArray()
		{
			object[] array = new object[_size];
			CopyTo(array);
			return array;
		}

		/// <summary>Copies the elements of the <see cref="T:System.Collections.ArrayList" /> to a new array of the specified element type.</summary>
		/// <returns>An array of the specified element type containing copies of the elements of the <see cref="T:System.Collections.ArrayList" />.</returns>
		/// <param name="type">The element <see cref="T:System.Type" /> of the destination array to create and copy elements to.</param>
		/// <exception cref="T:System.ArgumentNullException">
		///   <paramref name="type" /> is null. </exception>
		/// <exception cref="T:System.InvalidCastException">The type of the source <see cref="T:System.Collections.ArrayList" /> cannot be cast automatically to the specified type. </exception>
		/// <filterpriority>1</filterpriority>
		public virtual Array ToArray(Type type)
		{
			Array array = Array.CreateInstance(type, _size);
			CopyTo(array);
			return array;
		}

		/// <summary>Creates a shallow copy of the <see cref="T:System.Collections.ArrayList" />.</summary>
		/// <returns>A shallow copy of the <see cref="T:System.Collections.ArrayList" />.</returns>
		/// <filterpriority>2</filterpriority>
		public virtual object Clone()
		{
			return new ArrayList(_items, 0, _size);
		}

		internal static void CheckRange(int index, int count, int listCount)
		{
			if (index < 0)
			{
				ThrowNewArgumentOutOfRangeException("index", index, "Can't be less than 0.");
			}
			if (count < 0)
			{
				ThrowNewArgumentOutOfRangeException("count", count, "Can't be less than 0.");
			}
			if (index > listCount - count)
			{
				throw new ArgumentException("Index and count do not denote a valid range of elements.", "index");
			}
		}

		internal static void ThrowNewArgumentOutOfRangeException(string name, object actual, string message)
		{
			throw new ArgumentOutOfRangeException(name, actual, message);
		}

		/// <summary>Creates an <see cref="T:System.Collections.ArrayList" /> wrapper for a specific <see cref="T:System.Collections.IList" />.</summary>
		/// <returns>The <see cref="T:System.Collections.ArrayList" /> wrapper around the <see cref="T:System.Collections.IList" />.</returns>
		/// <param name="list">The <see cref="T:System.Collections.IList" /> to wrap.</param>
		/// <exception cref="T:System.ArgumentNullException">
		///   <paramref name="list" /> is null.</exception>
		/// <filterpriority>2</filterpriority>
		public static ArrayList Adapter(IList list)
		{
			if (list == null)
			{
				throw new ArgumentNullException("list");
			}
			ArrayList arrayList = list as ArrayList;
			if (arrayList != null)
			{
				return arrayList;
			}
			arrayList = new ArrayListAdapter(list);
			if (list.IsSynchronized)
			{
				return Synchronized(arrayList);
			}
			return arrayList;
		}

		/// <summary>Returns an <see cref="T:System.Collections.ArrayList" /> wrapper that is synchronized (thread safe).</summary>
		/// <returns>An <see cref="T:System.Collections.ArrayList" /> wrapper that is synchronized (thread safe).</returns>
		/// <param name="list">The <see cref="T:System.Collections.ArrayList" /> to synchronize. </param>
		/// <exception cref="T:System.ArgumentNullException">
		///   <paramref name="list" /> is null. </exception>
		/// <filterpriority>2</filterpriority>
		public static ArrayList Synchronized(ArrayList list)
		{
			if (list == null)
			{
				throw new ArgumentNullException("list");
			}
			if (list.IsSynchronized)
			{
				return list;
			}
			return new SynchronizedArrayListWrapper(list);
		}

		/// <summary>Returns an <see cref="T:System.Collections.IList" /> wrapper that is synchronized (thread safe).</summary>
		/// <returns>An <see cref="T:System.Collections.IList" /> wrapper that is synchronized (thread safe).</returns>
		/// <param name="list">The <see cref="T:System.Collections.IList" /> to synchronize. </param>
		/// <exception cref="T:System.ArgumentNullException">
		///   <paramref name="list" /> is null. </exception>
		/// <filterpriority>2</filterpriority>
		public static IList Synchronized(IList list)
		{
			if (list == null)
			{
				throw new ArgumentNullException("list");
			}
			if (list.IsSynchronized)
			{
				return list;
			}
			return new SynchronizedListWrapper(list);
		}

		/// <summary>Returns a read-only <see cref="T:System.Collections.ArrayList" /> wrapper.</summary>
		/// <returns>A read-only <see cref="T:System.Collections.ArrayList" /> wrapper around <paramref name="list" />.</returns>
		/// <param name="list">The <see cref="T:System.Collections.ArrayList" /> to wrap. </param>
		/// <exception cref="T:System.ArgumentNullException">
		///   <paramref name="list" /> is null. </exception>
		/// <filterpriority>2</filterpriority>
		public static ArrayList ReadOnly(ArrayList list)
		{
			if (list == null)
			{
				throw new ArgumentNullException("list");
			}
			if (list.IsReadOnly)
			{
				return list;
			}
			return new ReadOnlyArrayListWrapper(list);
		}

		/// <summary>Returns a read-only <see cref="T:System.Collections.IList" /> wrapper.</summary>
		/// <returns>A read-only <see cref="T:System.Collections.IList" /> wrapper around <paramref name="list" />.</returns>
		/// <param name="list">The <see cref="T:System.Collections.IList" /> to wrap. </param>
		/// <exception cref="T:System.ArgumentNullException">
		///   <paramref name="list" /> is null. </exception>
		/// <filterpriority>2</filterpriority>
		public static IList ReadOnly(IList list)
		{
			if (list == null)
			{
				throw new ArgumentNullException("list");
			}
			if (list.IsReadOnly)
			{
				return list;
			}
			return new ReadOnlyListWrapper(list);
		}

		/// <summary>Returns an <see cref="T:System.Collections.ArrayList" /> wrapper with a fixed size.</summary>
		/// <returns>An <see cref="T:System.Collections.ArrayList" /> wrapper with a fixed size.</returns>
		/// <param name="list">The <see cref="T:System.Collections.ArrayList" /> to wrap. </param>
		/// <exception cref="T:System.ArgumentNullException">
		///   <paramref name="list" /> is null. </exception>
		/// <filterpriority>2</filterpriority>
		public static ArrayList FixedSize(ArrayList list)
		{
			if (list == null)
			{
				throw new ArgumentNullException("list");
			}
			if (list.IsFixedSize)
			{
				return list;
			}
			return new FixedSizeArrayListWrapper(list);
		}

		/// <summary>Returns an <see cref="T:System.Collections.IList" /> wrapper with a fixed size.</summary>
		/// <returns>An <see cref="T:System.Collections.IList" /> wrapper with a fixed size.</returns>
		/// <param name="list">The <see cref="T:System.Collections.IList" /> to wrap. </param>
		/// <exception cref="T:System.ArgumentNullException">
		///   <paramref name="list" /> is null. </exception>
		/// <filterpriority>2</filterpriority>
		public static IList FixedSize(IList list)
		{
			if (list == null)
			{
				throw new ArgumentNullException("list");
			}
			if (list.IsFixedSize)
			{
				return list;
			}
			return new FixedSizeListWrapper(list);
		}

		/// <summary>Returns an <see cref="T:System.Collections.ArrayList" /> whose elements are copies of the specified value.</summary>
		/// <returns>An <see cref="T:System.Collections.ArrayList" /> with <paramref name="count" /> number of elements, all of which are copies of <paramref name="value" />.</returns>
		/// <param name="value">The <see cref="T:System.Object" /> to copy multiple times in the new <see cref="T:System.Collections.ArrayList" />. The value can be null. </param>
		/// <param name="count">The number of times <paramref name="value" /> should be copied. </param>
		/// <exception cref="T:System.ArgumentOutOfRangeException">
		///   <paramref name="count" /> is less than zero. </exception>
		/// <filterpriority>2</filterpriority>
		public static ArrayList Repeat(object value, int count)
		{
			ArrayList arrayList = new ArrayList(count);
			for (int i = 0; i < count; i++)
			{
				arrayList.Add(value);
			}
			return arrayList;
		}
	}
}
