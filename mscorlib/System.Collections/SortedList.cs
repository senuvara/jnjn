using System.Diagnostics;
using System.Runtime.InteropServices;

namespace System.Collections
{
	/// <summary>Represents a collection of key/value pairs that are sorted by the keys and are accessible by key and by index.</summary>
	/// <filterpriority>1</filterpriority>
	[Serializable]
	[ComVisible(true)]
	[DebuggerDisplay("Count={Count}")]
	public class SortedList : IEnumerable, ICloneable, ICollection, IDictionary
	{
		[Serializable]
		internal struct Slot
		{
			internal object key;

			internal object value;
		}

		private enum EnumeratorMode
		{
			KEY_MODE,
			VALUE_MODE,
			ENTRY_MODE
		}

		private sealed class Enumerator : IEnumerator, ICloneable, IDictionaryEnumerator
		{
			private SortedList host;

			private int stamp;

			private int pos;

			private int size;

			private EnumeratorMode mode;

			private object currentKey;

			private object currentValue;

			private bool invalid;

			private static readonly string xstr = "SortedList.Enumerator: snapshot out of sync.";

			public DictionaryEntry Entry
			{
				get
				{
					if (invalid || pos >= size || pos == -1)
					{
						throw new InvalidOperationException(xstr);
					}
					return new DictionaryEntry(currentKey, currentValue);
				}
			}

			public object Key
			{
				get
				{
					if (invalid || pos >= size || pos == -1)
					{
						throw new InvalidOperationException(xstr);
					}
					return currentKey;
				}
			}

			public object Value
			{
				get
				{
					if (invalid || pos >= size || pos == -1)
					{
						throw new InvalidOperationException(xstr);
					}
					return currentValue;
				}
			}

			public object Current
			{
				get
				{
					if (invalid || pos >= size || pos == -1)
					{
						throw new InvalidOperationException(xstr);
					}
					switch (mode)
					{
					case EnumeratorMode.KEY_MODE:
						return currentKey;
					case EnumeratorMode.VALUE_MODE:
						return currentValue;
					case EnumeratorMode.ENTRY_MODE:
						return Entry;
					default:
						throw new NotSupportedException(string.Concat(mode, " is not a supported mode."));
					}
				}
			}

			public Enumerator(SortedList host, EnumeratorMode mode)
			{
				this.host = host;
				stamp = host.modificationCount;
				size = host.Count;
				this.mode = mode;
				Reset();
			}

			public Enumerator(SortedList host)
				: this(host, EnumeratorMode.ENTRY_MODE)
			{
			}

			public void Reset()
			{
				if (host.modificationCount != stamp || invalid)
				{
					throw new InvalidOperationException(xstr);
				}
				pos = -1;
				currentKey = null;
				currentValue = null;
			}

			public bool MoveNext()
			{
				if (host.modificationCount != stamp || invalid)
				{
					throw new InvalidOperationException(xstr);
				}
				Slot[] table = host.table;
				if (++pos < size)
				{
					Slot slot = table[pos];
					currentKey = slot.key;
					currentValue = slot.value;
					return true;
				}
				currentKey = null;
				currentValue = null;
				return false;
			}

			public object Clone()
			{
				Enumerator enumerator = new Enumerator(host, mode);
				enumerator.stamp = stamp;
				enumerator.pos = pos;
				enumerator.size = size;
				enumerator.currentKey = currentKey;
				enumerator.currentValue = currentValue;
				enumerator.invalid = invalid;
				return enumerator;
			}
		}

		[Serializable]
		private class ListKeys : IEnumerable, ICollection, IList
		{
			private SortedList host;

			public virtual int Count => host.Count;

			public virtual bool IsSynchronized => host.IsSynchronized;

			public virtual object SyncRoot => host.SyncRoot;

			public virtual bool IsFixedSize => true;

			public virtual bool IsReadOnly => true;

			public virtual object this[int index]
			{
				get
				{
					return host.GetKey(index);
				}
				set
				{
					throw new NotSupportedException("attempt to modify a key");
				}
			}

			public ListKeys(SortedList host)
			{
				if (host == null)
				{
					throw new ArgumentNullException();
				}
				this.host = host;
			}

			public virtual void CopyTo(Array array, int arrayIndex)
			{
				host.CopyToArray(array, arrayIndex, EnumeratorMode.KEY_MODE);
			}

			public virtual int Add(object value)
			{
				throw new NotSupportedException("IList::Add not supported");
			}

			public virtual void Clear()
			{
				throw new NotSupportedException("IList::Clear not supported");
			}

			public virtual bool Contains(object key)
			{
				return host.Contains(key);
			}

			public virtual int IndexOf(object key)
			{
				return host.IndexOfKey(key);
			}

			public virtual void Insert(int index, object value)
			{
				throw new NotSupportedException("IList::Insert not supported");
			}

			public virtual void Remove(object value)
			{
				throw new NotSupportedException("IList::Remove not supported");
			}

			public virtual void RemoveAt(int index)
			{
				throw new NotSupportedException("IList::RemoveAt not supported");
			}

			public virtual IEnumerator GetEnumerator()
			{
				return new Enumerator(host, EnumeratorMode.KEY_MODE);
			}
		}

		[Serializable]
		private class ListValues : IEnumerable, ICollection, IList
		{
			private SortedList host;

			public virtual int Count => host.Count;

			public virtual bool IsSynchronized => host.IsSynchronized;

			public virtual object SyncRoot => host.SyncRoot;

			public virtual bool IsFixedSize => true;

			public virtual bool IsReadOnly => true;

			public virtual object this[int index]
			{
				get
				{
					return host.GetByIndex(index);
				}
				set
				{
					throw new NotSupportedException("This operation is not supported on GetValueList return");
				}
			}

			public ListValues(SortedList host)
			{
				if (host == null)
				{
					throw new ArgumentNullException();
				}
				this.host = host;
			}

			public virtual void CopyTo(Array array, int arrayIndex)
			{
				host.CopyToArray(array, arrayIndex, EnumeratorMode.VALUE_MODE);
			}

			public virtual int Add(object value)
			{
				throw new NotSupportedException("IList::Add not supported");
			}

			public virtual void Clear()
			{
				throw new NotSupportedException("IList::Clear not supported");
			}

			public virtual bool Contains(object value)
			{
				return host.ContainsValue(value);
			}

			public virtual int IndexOf(object value)
			{
				return host.IndexOfValue(value);
			}

			public virtual void Insert(int index, object value)
			{
				throw new NotSupportedException("IList::Insert not supported");
			}

			public virtual void Remove(object value)
			{
				throw new NotSupportedException("IList::Remove not supported");
			}

			public virtual void RemoveAt(int index)
			{
				throw new NotSupportedException("IList::RemoveAt not supported");
			}

			public virtual IEnumerator GetEnumerator()
			{
				return new Enumerator(host, EnumeratorMode.VALUE_MODE);
			}
		}

		private class SynchedSortedList : SortedList
		{
			private SortedList host;

			public override int Capacity
			{
				get
				{
					//Discarded unreachable code: IL_0023
					lock (host.SyncRoot)
					{
						return host.Capacity;
					}
				}
				set
				{
					lock (host.SyncRoot)
					{
						host.Capacity = value;
					}
				}
			}

			public override int Count => host.Count;

			public override bool IsSynchronized => true;

			public override object SyncRoot => host.SyncRoot;

			public override bool IsFixedSize => host.IsFixedSize;

			public override bool IsReadOnly => host.IsReadOnly;

			public override ICollection Keys
			{
				get
				{
					ICollection collection = null;
					lock (host.SyncRoot)
					{
						return host.Keys;
					}
				}
			}

			public override ICollection Values
			{
				get
				{
					ICollection collection = null;
					lock (host.SyncRoot)
					{
						return host.Values;
					}
				}
			}

			public override object this[object key]
			{
				get
				{
					//Discarded unreachable code: IL_0024
					lock (host.SyncRoot)
					{
						return host.GetImpl(key);
					}
				}
				set
				{
					lock (host.SyncRoot)
					{
						host.PutImpl(key, value, overwrite: true);
					}
				}
			}

			public SynchedSortedList(SortedList host)
			{
				if (host == null)
				{
					throw new ArgumentNullException();
				}
				this.host = host;
			}

			public override void CopyTo(Array array, int arrayIndex)
			{
				lock (host.SyncRoot)
				{
					host.CopyTo(array, arrayIndex);
				}
			}

			public override void Add(object key, object value)
			{
				lock (host.SyncRoot)
				{
					host.PutImpl(key, value, overwrite: false);
				}
			}

			public override void Clear()
			{
				lock (host.SyncRoot)
				{
					host.Clear();
				}
			}

			public override bool Contains(object key)
			{
				//Discarded unreachable code: IL_002a
				lock (host.SyncRoot)
				{
					return host.Find(key) >= 0;
				}
			}

			public override IDictionaryEnumerator GetEnumerator()
			{
				//Discarded unreachable code: IL_0023
				lock (host.SyncRoot)
				{
					return host.GetEnumerator();
				}
			}

			public override void Remove(object key)
			{
				lock (host.SyncRoot)
				{
					host.Remove(key);
				}
			}

			public override bool ContainsKey(object key)
			{
				//Discarded unreachable code: IL_0024
				lock (host.SyncRoot)
				{
					return host.Contains(key);
				}
			}

			public override bool ContainsValue(object value)
			{
				//Discarded unreachable code: IL_0024
				lock (host.SyncRoot)
				{
					return host.ContainsValue(value);
				}
			}

			public override object Clone()
			{
				//Discarded unreachable code: IL_0028
				lock (host.SyncRoot)
				{
					return host.Clone() as SortedList;
				}
			}

			public override object GetByIndex(int index)
			{
				//Discarded unreachable code: IL_0024
				lock (host.SyncRoot)
				{
					return host.GetByIndex(index);
				}
			}

			public override object GetKey(int index)
			{
				//Discarded unreachable code: IL_0024
				lock (host.SyncRoot)
				{
					return host.GetKey(index);
				}
			}

			public override IList GetKeyList()
			{
				//Discarded unreachable code: IL_0023
				lock (host.SyncRoot)
				{
					return new ListKeys(host);
				}
			}

			public override IList GetValueList()
			{
				//Discarded unreachable code: IL_0023
				lock (host.SyncRoot)
				{
					return new ListValues(host);
				}
			}

			public override void RemoveAt(int index)
			{
				lock (host.SyncRoot)
				{
					host.RemoveAt(index);
				}
			}

			public override int IndexOfKey(object key)
			{
				//Discarded unreachable code: IL_0024
				lock (host.SyncRoot)
				{
					return host.IndexOfKey(key);
				}
			}

			public override int IndexOfValue(object val)
			{
				//Discarded unreachable code: IL_0024
				lock (host.SyncRoot)
				{
					return host.IndexOfValue(val);
				}
			}

			public override void SetByIndex(int index, object value)
			{
				lock (host.SyncRoot)
				{
					host.SetByIndex(index, value);
				}
			}

			public override void TrimToSize()
			{
				lock (host.SyncRoot)
				{
					host.TrimToSize();
				}
			}
		}

		private static readonly int INITIAL_SIZE = 16;

		private int inUse;

		private int modificationCount;

		private Slot[] table;

		private IComparer comparer;

		private int defaultCapacity;

		/// <summary>Gets the number of elements contained in a <see cref="T:System.Collections.SortedList" /> object.</summary>
		/// <returns>The number of elements contained in the <see cref="T:System.Collections.SortedList" /> object.</returns>
		/// <filterpriority>1</filterpriority>
		public virtual int Count => inUse;

		/// <summary>Gets a value indicating whether access to a <see cref="T:System.Collections.SortedList" /> object is synchronized (thread safe).</summary>
		/// <returns>true if access to the <see cref="T:System.Collections.SortedList" /> object is synchronized (thread safe); otherwise, false. The default is false.</returns>
		/// <filterpriority>2</filterpriority>
		public virtual bool IsSynchronized => false;

		/// <summary>Gets an object that can be used to synchronize access to a <see cref="T:System.Collections.SortedList" /> object.</summary>
		/// <returns>An object that can be used to synchronize access to the <see cref="T:System.Collections.SortedList" /> object.</returns>
		/// <filterpriority>2</filterpriority>
		public virtual object SyncRoot => this;

		/// <summary>Gets a value indicating whether a <see cref="T:System.Collections.SortedList" /> object has a fixed size.</summary>
		/// <returns>true if the <see cref="T:System.Collections.SortedList" /> object has a fixed size; otherwise, false. The default is false.</returns>
		/// <filterpriority>2</filterpriority>
		public virtual bool IsFixedSize => false;

		/// <summary>Gets a value indicating whether a <see cref="T:System.Collections.SortedList" /> object is read-only.</summary>
		/// <returns>true if the <see cref="T:System.Collections.SortedList" /> object is read-only; otherwise, false. The default is false.</returns>
		/// <filterpriority>2</filterpriority>
		public virtual bool IsReadOnly => false;

		/// <summary>Gets the keys in a <see cref="T:System.Collections.SortedList" /> object.</summary>
		/// <returns>An <see cref="T:System.Collections.ICollection" /> object containing the keys in the <see cref="T:System.Collections.SortedList" /> object.</returns>
		/// <filterpriority>1</filterpriority>
		public virtual ICollection Keys => new ListKeys(this);

		/// <summary>Gets the values in a <see cref="T:System.Collections.SortedList" /> object.</summary>
		/// <returns>An <see cref="T:System.Collections.ICollection" /> object containing the values in the <see cref="T:System.Collections.SortedList" /> object.</returns>
		/// <filterpriority>1</filterpriority>
		public virtual ICollection Values => new ListValues(this);

		/// <summary>Gets and sets the value associated with a specific key in a <see cref="T:System.Collections.SortedList" /> object.</summary>
		/// <returns>The value associated with the <paramref name="key" /> parameter in the <see cref="T:System.Collections.SortedList" /> object, if <paramref name="key" /> is found; otherwise, null.</returns>
		/// <param name="key">The key associated with the value to get or set. </param>
		/// <exception cref="T:System.ArgumentNullException">
		///   <paramref name="key" /> is null. </exception>
		/// <exception cref="T:System.NotSupportedException">The property is set and the <see cref="T:System.Collections.SortedList" /> object is read-only.-or- The property is set, <paramref name="key" /> does not exist in the collection, and the <see cref="T:System.Collections.SortedList" /> has a fixed size. </exception>
		/// <exception cref="T:System.OutOfMemoryException">There is not enough available memory to add the element to the <see cref="T:System.Collections.SortedList" />.</exception>
		/// <exception cref="T:System.InvalidOperationException">The comparer throws an exception. </exception>
		/// <filterpriority>1</filterpriority>
		public virtual object this[object key]
		{
			get
			{
				if (key == null)
				{
					throw new ArgumentNullException();
				}
				return GetImpl(key);
			}
			set
			{
				if (key == null)
				{
					throw new ArgumentNullException();
				}
				if (IsReadOnly)
				{
					throw new NotSupportedException("SortedList is Read Only.");
				}
				if (Find(key) < 0 && IsFixedSize)
				{
					throw new NotSupportedException("Key not found and SortedList is fixed size.");
				}
				PutImpl(key, value, overwrite: true);
			}
		}

		/// <summary>Gets or sets the capacity of a <see cref="T:System.Collections.SortedList" /> object.</summary>
		/// <returns>The number of elements that the <see cref="T:System.Collections.SortedList" /> object can contain.</returns>
		/// <exception cref="T:System.ArgumentOutOfRangeException">The value assigned is less than the current number of elements in the <see cref="T:System.Collections.SortedList" /> object.</exception>
		/// <exception cref="T:System.OutOfMemoryException">There is not enough memory available on the system.</exception>
		/// <filterpriority>2</filterpriority>
		public virtual int Capacity
		{
			get
			{
				return table.Length;
			}
			set
			{
				int num = table.Length;
				if (inUse > value)
				{
					throw new ArgumentOutOfRangeException("capacity too small");
				}
				if (value == 0)
				{
					Slot[] destinationArray = new Slot[defaultCapacity];
					Array.Copy(table, destinationArray, inUse);
					table = destinationArray;
				}
				else if (value > inUse)
				{
					Slot[] destinationArray2 = new Slot[value];
					Array.Copy(table, destinationArray2, inUse);
					table = destinationArray2;
				}
				else if (value > num)
				{
					Slot[] destinationArray3 = new Slot[value];
					Array.Copy(table, destinationArray3, num);
					table = destinationArray3;
				}
			}
		}

		/// <summary>Initializes a new instance of the <see cref="T:System.Collections.SortedList" /> class that is empty, has the default initial capacity, and is sorted according to the <see cref="T:System.IComparable" /> interface implemented by each key added to the <see cref="T:System.Collections.SortedList" /> object.</summary>
		public SortedList()
			: this(null, INITIAL_SIZE)
		{
		}

		/// <summary>Initializes a new instance of the <see cref="T:System.Collections.SortedList" /> class that is empty, has the specified initial capacity, and is sorted according to the <see cref="T:System.IComparable" /> interface implemented by each key added to the <see cref="T:System.Collections.SortedList" /> object.</summary>
		/// <param name="initialCapacity">The initial number of elements that the <see cref="T:System.Collections.SortedList" /> object can contain. </param>
		/// <exception cref="T:System.ArgumentOutOfRangeException">
		///   <paramref name="initialCapacity" /> is less than zero. </exception>
		/// <exception cref="T:System.OutOfMemoryException">There is not enough available memory to create a <see cref="T:System.Collections.SortedList" /> object with the specified <paramref name="initialCapacity" />.</exception>
		public SortedList(int initialCapacity)
			: this(null, initialCapacity)
		{
		}

		/// <summary>Initializes a new instance of the <see cref="T:System.Collections.SortedList" /> class that is empty, has the specified initial capacity, and is sorted according to the specified <see cref="T:System.Collections.IComparer" /> interface.</summary>
		/// <param name="comparer">The <see cref="T:System.Collections.IComparer" /> implementation to use when comparing keys.-or- null to use the <see cref="T:System.IComparable" /> implementation of each key. </param>
		/// <param name="capacity">The initial number of elements that the <see cref="T:System.Collections.SortedList" /> object can contain. </param>
		/// <exception cref="T:System.ArgumentOutOfRangeException">
		///   <paramref name="capacity" /> is less than zero. </exception>
		/// <exception cref="T:System.OutOfMemoryException">There is not enough available memory to create a <see cref="T:System.Collections.SortedList" /> object with the specified <paramref name="capacity" />.</exception>
		public SortedList(IComparer comparer, int capacity)
		{
			if (capacity < 0)
			{
				throw new ArgumentOutOfRangeException("capacity");
			}
			if (capacity == 0)
			{
				defaultCapacity = 0;
			}
			else
			{
				defaultCapacity = INITIAL_SIZE;
			}
			this.comparer = comparer;
			InitTable(capacity, forceSize: true);
		}

		/// <summary>Initializes a new instance of the <see cref="T:System.Collections.SortedList" /> class that is empty, has the default initial capacity, and is sorted according to the specified <see cref="T:System.Collections.IComparer" /> interface.</summary>
		/// <param name="comparer">The <see cref="T:System.Collections.IComparer" /> implementation to use when comparing keys.-or- null to use the <see cref="T:System.IComparable" /> implementation of each key. </param>
		public SortedList(IComparer comparer)
		{
			this.comparer = comparer;
			InitTable(INITIAL_SIZE, forceSize: true);
		}

		/// <summary>Initializes a new instance of the <see cref="T:System.Collections.SortedList" /> class that contains elements copied from the specified dictionary, has the same initial capacity as the number of elements copied, and is sorted according to the <see cref="T:System.IComparable" /> interface implemented by each key.</summary>
		/// <param name="d">The <see cref="T:System.Collections.IDictionary" /> implementation to copy to a new <see cref="T:System.Collections.SortedList" /> object.</param>
		/// <exception cref="T:System.ArgumentNullException">
		///   <paramref name="d" /> is null. </exception>
		/// <exception cref="T:System.InvalidCastException">One or more elements in <paramref name="d" /> do not implement the <see cref="T:System.IComparable" /> interface. </exception>
		public SortedList(IDictionary d)
			: this(d, null)
		{
		}

		/// <summary>Initializes a new instance of the <see cref="T:System.Collections.SortedList" /> class that contains elements copied from the specified dictionary, has the same initial capacity as the number of elements copied, and is sorted according to the specified <see cref="T:System.Collections.IComparer" /> interface.</summary>
		/// <param name="d">The <see cref="T:System.Collections.IDictionary" /> implementation to copy to a new <see cref="T:System.Collections.SortedList" /> object.</param>
		/// <param name="comparer">The <see cref="T:System.Collections.IComparer" /> implementation to use when comparing keys.-or- null to use the <see cref="T:System.IComparable" /> implementation of each key. </param>
		/// <exception cref="T:System.ArgumentNullException">
		///   <paramref name="d" /> is null. </exception>
		/// <exception cref="T:System.InvalidCastException">
		///   <paramref name="comparer" /> is null, and one or more elements in <paramref name="d" /> do not implement the <see cref="T:System.IComparable" /> interface. </exception>
		public SortedList(IDictionary d, IComparer comparer)
		{
			if (d == null)
			{
				throw new ArgumentNullException("dictionary");
			}
			InitTable(d.Count, forceSize: true);
			this.comparer = comparer;
			IDictionaryEnumerator enumerator = d.GetEnumerator();
			while (enumerator.MoveNext())
			{
				Add(enumerator.Key, enumerator.Value);
			}
		}

		/// <summary>Returns an <see cref="T:System.Collections.IEnumerator" /> that iterates through the <see cref="T:System.Collections.SortedList" />.</summary>
		/// <returns>An <see cref="T:System.Collections.IEnumerator" /> for the <see cref="T:System.Collections.SortedList" />.</returns>
		IEnumerator IEnumerable.GetEnumerator()
		{
			return new Enumerator(this, EnumeratorMode.ENTRY_MODE);
		}

		/// <summary>Adds an element with the specified key and value to a <see cref="T:System.Collections.SortedList" /> object.</summary>
		/// <param name="key">The key of the element to add. </param>
		/// <param name="value">The value of the element to add. The value can be null. </param>
		/// <exception cref="T:System.ArgumentNullException">
		///   <paramref name="key" /> is null. </exception>
		/// <exception cref="T:System.ArgumentException">An element with the specified <paramref name="key" /> already exists in the <see cref="T:System.Collections.SortedList" /> object.-or- The <see cref="T:System.Collections.SortedList" /> is set to use the <see cref="T:System.IComparable" /> interface, and <paramref name="key" /> does not implement the <see cref="T:System.IComparable" /> interface. </exception>
		/// <exception cref="T:System.NotSupportedException">The <see cref="T:System.Collections.SortedList" /> is read-only.-or- The <see cref="T:System.Collections.SortedList" /> has a fixed size. </exception>
		/// <exception cref="T:System.OutOfMemoryException">There is not enough available memory to add the element to the <see cref="T:System.Collections.SortedList" />.</exception>
		/// <exception cref="T:System.InvalidOperationException">The comparer throws an exception. </exception>
		/// <filterpriority>2</filterpriority>
		public virtual void Add(object key, object value)
		{
			PutImpl(key, value, overwrite: false);
		}

		/// <summary>Removes all elements from a <see cref="T:System.Collections.SortedList" /> object.</summary>
		/// <exception cref="T:System.NotSupportedException">The <see cref="T:System.Collections.SortedList" /> object is read-only.-or- The <see cref="T:System.Collections.SortedList" /> has a fixed size. </exception>
		/// <filterpriority>1</filterpriority>
		public virtual void Clear()
		{
			defaultCapacity = INITIAL_SIZE;
			table = new Slot[defaultCapacity];
			inUse = 0;
			modificationCount++;
		}

		/// <summary>Determines whether a <see cref="T:System.Collections.SortedList" /> object contains a specific key.</summary>
		/// <returns>true if the <see cref="T:System.Collections.SortedList" /> object contains an element with the specified <paramref name="key" />; otherwise, false.</returns>
		/// <param name="key">The key to locate in the <see cref="T:System.Collections.SortedList" /> object. </param>
		/// <exception cref="T:System.ArgumentNullException">
		///   <paramref name="key" /> is null. </exception>
		/// <exception cref="T:System.InvalidOperationException">The comparer throws an exception. </exception>
		/// <filterpriority>1</filterpriority>
		public virtual bool Contains(object key)
		{
			//Discarded unreachable code: IL_001f, IL_002b
			if (key == null)
			{
				throw new ArgumentNullException();
			}
			try
			{
				return Find(key) >= 0;
			}
			catch (Exception)
			{
				throw new InvalidOperationException();
			}
		}

		/// <summary>Returns an <see cref="T:System.Collections.IDictionaryEnumerator" /> object that iterates through a <see cref="T:System.Collections.SortedList" /> object.</summary>
		/// <returns>An <see cref="T:System.Collections.IDictionaryEnumerator" /> object for the <see cref="T:System.Collections.SortedList" /> object.</returns>
		/// <filterpriority>2</filterpriority>
		public virtual IDictionaryEnumerator GetEnumerator()
		{
			return new Enumerator(this, EnumeratorMode.ENTRY_MODE);
		}

		/// <summary>Removes the element with the specified key from a <see cref="T:System.Collections.SortedList" /> object.</summary>
		/// <param name="key">The key of the element to remove. </param>
		/// <exception cref="T:System.ArgumentNullException">
		///   <paramref name="key" /> is null. </exception>
		/// <exception cref="T:System.NotSupportedException">The <see cref="T:System.Collections.SortedList" /> object is read-only.-or- The <see cref="T:System.Collections.SortedList" /> has a fixed size. </exception>
		/// <filterpriority>1</filterpriority>
		public virtual void Remove(object key)
		{
			int num = IndexOfKey(key);
			if (num >= 0)
			{
				RemoveAt(num);
			}
		}

		/// <summary>Copies <see cref="T:System.Collections.SortedList" /> elements to a one-dimensional <see cref="T:System.Array" /> object, starting at the specified index in the array.</summary>
		/// <param name="array">The one-dimensional <see cref="T:System.Array" /> object that is the destination of the <see cref="T:System.Collections.DictionaryEntry" /> objects copied from <see cref="T:System.Collections.SortedList" />. The <see cref="T:System.Array" /> must have zero-based indexing. </param>
		/// <param name="arrayIndex">The zero-based index in <paramref name="array" /> at which copying begins. </param>
		/// <exception cref="T:System.ArgumentNullException">
		///   <paramref name="array" /> is null. </exception>
		/// <exception cref="T:System.ArgumentOutOfRangeException">
		///   <paramref name="arrayIndex" /> is less than zero. </exception>
		/// <exception cref="T:System.ArgumentException">
		///   <paramref name="array" /> is multidimensional.-or- <paramref name="arrayIndex" /> is equal to or greater than the length of <paramref name="array" />.-or- The number of elements in the source <see cref="T:System.Collections.SortedList" /> object is greater than the available space from <paramref name="arrayIndex" /> to the end of the destination <paramref name="array" />. </exception>
		/// <exception cref="T:System.InvalidCastException">The type of the source <see cref="T:System.Collections.SortedList" /> cannot be cast automatically to the type of the destination <paramref name="array" />. </exception>
		/// <filterpriority>2</filterpriority>
		public virtual void CopyTo(Array array, int arrayIndex)
		{
			if (array == null)
			{
				throw new ArgumentNullException();
			}
			if (arrayIndex < 0)
			{
				throw new ArgumentOutOfRangeException();
			}
			if (array.Rank > 1)
			{
				throw new ArgumentException("array is multi-dimensional");
			}
			if (arrayIndex >= array.Length)
			{
				throw new ArgumentNullException("arrayIndex is greater than or equal to array.Length");
			}
			if (Count > array.Length - arrayIndex)
			{
				throw new ArgumentNullException("Not enough space in array from arrayIndex to end of array");
			}
			IDictionaryEnumerator enumerator = GetEnumerator();
			int num = arrayIndex;
			while (enumerator.MoveNext())
			{
				array.SetValue(enumerator.Entry, num++);
			}
		}

		/// <summary>Creates a shallow copy of a <see cref="T:System.Collections.SortedList" /> object.</summary>
		/// <returns>A shallow copy of the <see cref="T:System.Collections.SortedList" /> object.</returns>
		/// <filterpriority>2</filterpriority>
		/// <PermissionSet>
		///   <IPermission class="System.Security.Permissions.SecurityPermission, mscorlib, Version=2.0.3600.0, Culture=neutral, PublicKeyToken=b77a5c561934e089" version="1" Flags="UnmanagedCode" />
		/// </PermissionSet>
		public virtual object Clone()
		{
			SortedList sortedList = new SortedList(this, comparer);
			sortedList.modificationCount = modificationCount;
			return sortedList;
		}

		/// <summary>Gets the keys in a <see cref="T:System.Collections.SortedList" /> object.</summary>
		/// <returns>An <see cref="T:System.Collections.IList" /> object containing the keys in the <see cref="T:System.Collections.SortedList" /> object.</returns>
		/// <filterpriority>2</filterpriority>
		public virtual IList GetKeyList()
		{
			return new ListKeys(this);
		}

		/// <summary>Gets the values in a <see cref="T:System.Collections.SortedList" /> object.</summary>
		/// <returns>An <see cref="T:System.Collections.IList" /> object containing the values in the <see cref="T:System.Collections.SortedList" /> object.</returns>
		/// <filterpriority>2</filterpriority>
		public virtual IList GetValueList()
		{
			return new ListValues(this);
		}

		/// <summary>Removes the element at the specified index of a <see cref="T:System.Collections.SortedList" /> object.</summary>
		/// <param name="index">The zero-based index of the element to remove. </param>
		/// <exception cref="T:System.ArgumentOutOfRangeException">
		///   <paramref name="index" /> is outside the range of valid indexes for the <see cref="T:System.Collections.SortedList" /> object. </exception>
		/// <exception cref="T:System.NotSupportedException">The <see cref="T:System.Collections.SortedList" /> is read-only.-or- The <see cref="T:System.Collections.SortedList" /> has a fixed size. </exception>
		/// <filterpriority>2</filterpriority>
		public virtual void RemoveAt(int index)
		{
			Slot[] array = table;
			int count = Count;
			if (index >= 0 && index < count)
			{
				if (index != count - 1)
				{
					Array.Copy(array, index + 1, array, index, count - 1 - index);
				}
				else
				{
					array[index].key = null;
					array[index].value = null;
				}
				inUse--;
				modificationCount++;
				return;
			}
			throw new ArgumentOutOfRangeException("index out of range");
		}

		/// <summary>Returns the zero-based index of the specified key in a <see cref="T:System.Collections.SortedList" /> object.</summary>
		/// <returns>The zero-based index of the <paramref name="key" /> parameter, if <paramref name="key" /> is found in the <see cref="T:System.Collections.SortedList" /> object; otherwise, -1.</returns>
		/// <param name="key">The key to locate in the <see cref="T:System.Collections.SortedList" /> object. </param>
		/// <exception cref="T:System.ArgumentNullException">
		///   <paramref name="key" /> is null. </exception>
		/// <exception cref="T:System.InvalidOperationException">The comparer throws an exception. </exception>
		/// <filterpriority>1</filterpriority>
		public virtual int IndexOfKey(object key)
		{
			//Discarded unreachable code: IL_0022
			if (key == null)
			{
				throw new ArgumentNullException();
			}
			int num = 0;
			try
			{
				num = Find(key);
			}
			catch (Exception)
			{
				throw new InvalidOperationException();
			}
			return num | (num >> 31);
		}

		/// <summary>Returns the zero-based index of the first occurrence of the specified value in a <see cref="T:System.Collections.SortedList" /> object.</summary>
		/// <returns>The zero-based index of the first occurrence of the <paramref name="value" /> parameter, if <paramref name="value" /> is found in the <see cref="T:System.Collections.SortedList" /> object; otherwise, -1.</returns>
		/// <param name="value">The value to locate in the <see cref="T:System.Collections.SortedList" /> object. The value can be null. </param>
		/// <filterpriority>1</filterpriority>
		public virtual int IndexOfValue(object value)
		{
			if (inUse == 0)
			{
				return -1;
			}
			for (int i = 0; i < inUse; i++)
			{
				Slot slot = table[i];
				if (object.Equals(value, slot.value))
				{
					return i;
				}
			}
			return -1;
		}

		/// <summary>Determines whether a <see cref="T:System.Collections.SortedList" /> object contains a specific key.</summary>
		/// <returns>true if the <see cref="T:System.Collections.SortedList" /> object contains an element with the specified <paramref name="key" />; otherwise, false.</returns>
		/// <param name="key">The key to locate in the <see cref="T:System.Collections.SortedList" /> object.</param>
		/// <exception cref="T:System.ArgumentNullException">
		///   <paramref name="key" /> is null. </exception>
		/// <exception cref="T:System.InvalidOperationException">The comparer throws an exception. </exception>
		/// <filterpriority>1</filterpriority>
		public virtual bool ContainsKey(object key)
		{
			//Discarded unreachable code: IL_0019, IL_0025
			if (key == null)
			{
				throw new ArgumentNullException();
			}
			try
			{
				return Contains(key);
			}
			catch (Exception)
			{
				throw new InvalidOperationException();
			}
		}

		/// <summary>Determines whether a <see cref="T:System.Collections.SortedList" /> object contains a specific value.</summary>
		/// <returns>true if the <see cref="T:System.Collections.SortedList" /> object contains an element with the specified <paramref name="value" />; otherwise, false.</returns>
		/// <param name="value">The value to locate in the <see cref="T:System.Collections.SortedList" /> object. The value can be null. </param>
		/// <filterpriority>2</filterpriority>
		public virtual bool ContainsValue(object value)
		{
			return IndexOfValue(value) >= 0;
		}

		/// <summary>Gets the value at the specified index of a <see cref="T:System.Collections.SortedList" /> object.</summary>
		/// <returns>The value at the specified index of the <see cref="T:System.Collections.SortedList" /> object.</returns>
		/// <param name="index">The zero-based index of the value to get. </param>
		/// <exception cref="T:System.ArgumentOutOfRangeException">
		///   <paramref name="index" /> is outside the range of valid indexes for the <see cref="T:System.Collections.SortedList" /> object. </exception>
		/// <filterpriority>2</filterpriority>
		public virtual object GetByIndex(int index)
		{
			if (index >= 0 && index < Count)
			{
				return table[index].value;
			}
			throw new ArgumentOutOfRangeException("index out of range");
		}

		/// <summary>Replaces the value at a specific index in a <see cref="T:System.Collections.SortedList" /> object.</summary>
		/// <param name="index">The zero-based index at which to save <paramref name="value" />. </param>
		/// <param name="value">The <see cref="T:System.Object" /> to save into the <see cref="T:System.Collections.SortedList" /> object. The value can be null. </param>
		/// <exception cref="T:System.ArgumentOutOfRangeException">
		///   <paramref name="index" /> is outside the range of valid indexes for the <see cref="T:System.Collections.SortedList" /> object. </exception>
		/// <filterpriority>2</filterpriority>
		public virtual void SetByIndex(int index, object value)
		{
			if (index >= 0 && index < Count)
			{
				table[index].value = value;
				return;
			}
			throw new ArgumentOutOfRangeException("index out of range");
		}

		/// <summary>Gets the key at the specified index of a <see cref="T:System.Collections.SortedList" /> object.</summary>
		/// <returns>The key at the specified index of the <see cref="T:System.Collections.SortedList" /> object.</returns>
		/// <param name="index">The zero-based index of the key to get. </param>
		/// <exception cref="T:System.ArgumentOutOfRangeException">
		///   <paramref name="index" /> is outside the range of valid indexes for the <see cref="T:System.Collections.SortedList" /> object.</exception>
		/// <filterpriority>2</filterpriority>
		public virtual object GetKey(int index)
		{
			if (index >= 0 && index < Count)
			{
				return table[index].key;
			}
			throw new ArgumentOutOfRangeException("index out of range");
		}

		/// <summary>Returns a synchronized (thread-safe) wrapper for a <see cref="T:System.Collections.SortedList" /> object.</summary>
		/// <returns>A synchronized (thread-safe) wrapper for the <see cref="T:System.Collections.SortedList" /> object.</returns>
		/// <param name="list">The <see cref="T:System.Collections.SortedList" /> object to synchronize. </param>
		/// <exception cref="T:System.ArgumentNullException">
		///   <paramref name="list" /> is null. </exception>
		/// <filterpriority>1</filterpriority>
		/// <PermissionSet>
		///   <IPermission class="System.Security.Permissions.SecurityPermission, mscorlib, Version=2.0.3600.0, Culture=neutral, PublicKeyToken=b77a5c561934e089" version="1" Flags="UnmanagedCode" />
		/// </PermissionSet>
		public static SortedList Synchronized(SortedList list)
		{
			if (list == null)
			{
				throw new ArgumentNullException(Locale.GetText("Base list is null."));
			}
			return new SynchedSortedList(list);
		}

		/// <summary>Sets the capacity to the actual number of elements in a <see cref="T:System.Collections.SortedList" /> object.</summary>
		/// <exception cref="T:System.NotSupportedException">The <see cref="T:System.Collections.SortedList" /> object is read-only.-or- The <see cref="T:System.Collections.SortedList" /> has a fixed size. </exception>
		/// <filterpriority>2</filterpriority>
		public virtual void TrimToSize()
		{
			if (Count == 0)
			{
				Resize(defaultCapacity, copy: false);
			}
			else
			{
				Resize(Count, copy: true);
			}
		}

		private void Resize(int n, bool copy)
		{
			Slot[] sourceArray = table;
			Slot[] destinationArray = new Slot[n];
			if (copy)
			{
				Array.Copy(sourceArray, 0, destinationArray, 0, n);
			}
			table = destinationArray;
		}

		private void EnsureCapacity(int n, int free)
		{
			Slot[] array = table;
			Slot[] array2 = null;
			int capacity = Capacity;
			bool flag = free >= 0 && free < Count;
			if (n > capacity)
			{
				array2 = new Slot[n << 1];
			}
			if (array2 != null)
			{
				if (flag)
				{
					int num = free;
					if (num > 0)
					{
						Array.Copy(array, 0, array2, 0, num);
					}
					num = Count - free;
					if (num > 0)
					{
						Array.Copy(array, free, array2, free + 1, num);
					}
				}
				else
				{
					Array.Copy(array, array2, Count);
				}
				table = array2;
			}
			else if (flag)
			{
				Array.Copy(array, free, array, free + 1, Count - free);
			}
		}

		private void PutImpl(object key, object value, bool overwrite)
		{
			//Discarded unreachable code: IL_002e
			if (key == null)
			{
				throw new ArgumentNullException("null key");
			}
			Slot[] array = table;
			int num = -1;
			try
			{
				num = Find(key);
			}
			catch (Exception)
			{
				throw new InvalidOperationException();
			}
			if (num >= 0)
			{
				if (!overwrite)
				{
					string text = Locale.GetText("Key '{0}' already exists in list.", key);
					throw new ArgumentException(text);
				}
				array[num].value = value;
				modificationCount++;
				return;
			}
			num = ~num;
			if (num > Capacity + 1)
			{
				throw new Exception(string.Concat("SortedList::internal error (", key, ", ", value, ") at [", num, "]"));
			}
			EnsureCapacity(Count + 1, num);
			array = table;
			array[num].key = key;
			array[num].value = value;
			inUse++;
			modificationCount++;
		}

		private object GetImpl(object key)
		{
			int num = Find(key);
			if (num >= 0)
			{
				return table[num].value;
			}
			return null;
		}

		private void InitTable(int capacity, bool forceSize)
		{
			if (!forceSize && capacity < defaultCapacity)
			{
				capacity = defaultCapacity;
			}
			table = new Slot[capacity];
			inUse = 0;
			modificationCount = 0;
		}

		private void CopyToArray(Array arr, int i, EnumeratorMode mode)
		{
			if (arr == null)
			{
				throw new ArgumentNullException("arr");
			}
			if (i < 0 || i + Count > arr.Length)
			{
				throw new ArgumentOutOfRangeException("i");
			}
			IEnumerator enumerator = new Enumerator(this, mode);
			while (enumerator.MoveNext())
			{
				arr.SetValue(enumerator.Current, i++);
			}
		}

		private int Find(object key)
		{
			Slot[] array = table;
			int count = Count;
			if (count == 0)
			{
				return -1;
			}
			IComparer obj;
			if (this.comparer == null)
			{
				IComparer @default = Comparer.Default;
				obj = @default;
			}
			else
			{
				obj = this.comparer;
			}
			IComparer comparer = obj;
			int num = 0;
			int num2 = count - 1;
			while (num <= num2)
			{
				int num3 = num + num2 >> 1;
				int num4 = comparer.Compare(array[num3].key, key);
				if (num4 == 0)
				{
					return num3;
				}
				if (num4 < 0)
				{
					num = num3 + 1;
				}
				else
				{
					num2 = num3 - 1;
				}
			}
			return ~num;
		}
	}
}
