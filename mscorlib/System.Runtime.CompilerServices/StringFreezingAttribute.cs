namespace System.Runtime.CompilerServices
{
	/// <summary>Freezes a string literal when creating native images using the Native Image Generator (Ngen.exe). This class cannot be inherited.</summary>
	[Serializable]
	[AttributeUsage(AttributeTargets.Assembly, Inherited = false)]
	public sealed class StringFreezingAttribute : Attribute
	{
	}
}
