using System.Runtime.InteropServices;

namespace System.Runtime.CompilerServices
{
	/// <summary>Defines the details of how a method is implemented.</summary>
	[Serializable]
	[Flags]
	[ComVisible(true)]
	public enum MethodImplOptions
	{
		/// <summary>Specifies that the method is implemented in unmanaged code.</summary>
		Unmanaged = 0x4,
		/// <summary>Specifies that the method is declared, but its implementation is provided elsewhere.</summary>
		ForwardRef = 0x10,
		/// <summary>Specifies an internal call. An internal call is a call to a method implemented within the common language runtime itself.</summary>
		InternalCall = 0x1000,
		/// <summary>Specifies that the method can be executed by only one thread at a time.  Static methods lock on the type, while instance methods lock on the instance. Only one thread can execute in any of the instance functions and only one thread can execute in any of a class's static functions.</summary>
		Synchronized = 0x20,
		/// <summary>Specifies that the method can not be inlined.</summary>
		NoInlining = 0x8,
		/// <summary>Specifies that the method signature is exported exactly as declared.</summary>
		PreserveSig = 0x80,
		NoOptimization = 0x40
	}
}
