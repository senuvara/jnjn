using System.Runtime.InteropServices;

namespace System.Security.Cryptography.X509Certificates
{
	/// <summary>Defines where and how to export the private key of an X.509 certificate.</summary>
	[Serializable]
	[ComVisible(true)]
	[Flags]
	public enum X509KeyStorageFlags
	{
		/// <summary>The default key set is used.  The user key set is usually the default. </summary>
		DefaultKeySet = 0x0,
		/// <summary>Private keys are stored in the current user store rather than the local computer store. This occurs even if the certificate specifies that the keys should go in the local computer store. </summary>
		UserKeySet = 0x1,
		/// <summary>Private keys are stored in the local computer store rather than the current user store. </summary>
		MachineKeySet = 0x2,
		/// <summary>Imported keys are marked as exportable.  .</summary>
		Exportable = 0x4,
		/// <summary>Notify the user through a dialog box or other method that the key is accessed.  The Cryptographic Service Provider (CSP) in use defines the precise behavior.</summary>
		UserProtected = 0x8,
		/// <summary>The key associated with a PFX file is persisted when importing a certificate.</summary>
		PersistKeySet = 0x10
	}
}
