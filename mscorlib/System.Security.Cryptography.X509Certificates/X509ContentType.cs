using System.Runtime.InteropServices;

namespace System.Security.Cryptography.X509Certificates
{
	/// <summary>Specifies the format of an X.509 certificate.</summary>
	[ComVisible(true)]
	public enum X509ContentType
	{
		/// <summary>An unknown X.509 Certificate.  </summary>
		Unknown = 0,
		/// <summary>A single X.509 Certificate.</summary>
		Cert = 1,
		/// <summary>A single serialized X.509 Certificate. </summary>
		SerializedCert = 2,
		/// <summary>A PFX-formatted certificate.  The <see cref="F:System.Security.Cryptography.X509Certificates.X509ContentType.Pfx" /> value is identical to the <see cref="F:System.Security.Cryptography.X509Certificates.X509ContentType.Pkcs12" /> value.</summary>
		Pfx = 3,
		/// <summary>A serialized store.</summary>
		SerializedStore = 4,
		/// <summary>A PKCS #7–formatted certificate.</summary>
		Pkcs7 = 5,
		/// <summary>An Authenticode X.509 certificate. </summary>
		Authenticode = 6,
		/// <summary>A PKCS #12–formatted certificate.  The <see cref="F:System.Security.Cryptography.X509Certificates.X509ContentType.Pkcs12" /> value is identical to the <see cref="F:System.Security.Cryptography.X509Certificates.X509ContentType.Pfx" /> value.</summary>
		Pkcs12 = 3
	}
}
