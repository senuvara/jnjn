using Mono.Security.Cryptography;
using System.Collections;
using System.Reflection;
using System.Runtime.InteropServices;
using System.Runtime.Serialization.Formatters.Binary;
using System.Security;
using System.Security.Cryptography;
using System.Security.Permissions;
using System.Security.Policy;
using System.Text;
using System.Threading;

namespace System.IO.IsolatedStorage
{
	/// <summary>Represents an isolated storage area containing files and directories.</summary>
	[ComVisible(true)]
	public sealed class IsolatedStorageFile : IsolatedStorage, IDisposable
	{
		[Serializable]
		private struct Identities
		{
			public object Application;

			public object Assembly;

			public object Domain;

			public Identities(object application, object assembly, object domain)
			{
				Application = application;
				Assembly = assembly;
				Domain = domain;
			}
		}

		private bool _resolved;

		private ulong _maxSize;

		private Evidence _fullEvidences;

		private static Mutex mutex = new Mutex();

		private DirectoryInfo directory;

		/// <summary>Gets the current size of the isolated storage.</summary>
		/// <returns>The total number of bytes of storage currently in use within the isolated storage scope.</returns>
		/// <exception cref="T:System.InvalidOperationException">The property is unavailable. The current store has a roaming scope or is not open. </exception>
		/// <exception cref="T:System.ObjectDisposedException">The current object size is undefined.</exception>
		/// <PermissionSet>
		///   <IPermission class="System.Security.Permissions.SecurityPermission, mscorlib, Version=2.0.3600.0, Culture=neutral, PublicKeyToken=b77a5c561934e089" version="1" Flags="UnmanagedCode" />
		/// </PermissionSet>
		[CLSCompliant(false)]
		public override ulong CurrentSize => GetDirectorySize(directory);

		/// <summary>Gets a value representing the maximum amount of space available for isolated storage within the limits established by the quota.</summary>
		/// <returns>The limit of isolated storage space in bytes.</returns>
		/// <exception cref="T:System.InvalidOperationException">The property is unavailable. <see cref="P:System.IO.IsolatedStorage.IsolatedStorageFile.MaximumSize" /> cannot be determined without evidence from the assembly's creation. The evidence could not be determined when the object was created. </exception>
		[CLSCompliant(false)]
		public override ulong MaximumSize
		{
			get
			{
				if (!SecurityManager.SecurityEnabled)
				{
					return 9223372036854775807uL;
				}
				if (_resolved)
				{
					return _maxSize;
				}
				Evidence evidence = null;
				if (_fullEvidences != null)
				{
					evidence = _fullEvidences;
				}
				else
				{
					evidence = new Evidence();
					if (_assemblyIdentity != null)
					{
						evidence.AddHost(_assemblyIdentity);
					}
				}
				if (evidence.Count < 1)
				{
					throw new InvalidOperationException(Locale.GetText("Couldn't get the quota from the available evidences."));
				}
				PermissionSet denied = null;
				PermissionSet permissionSet = SecurityManager.ResolvePolicy(evidence, null, null, null, out denied);
				IsolatedStoragePermission permission = GetPermission(permissionSet);
				if (permission == null)
				{
					if (!permissionSet.IsUnrestricted())
					{
						throw new InvalidOperationException(Locale.GetText("No quota from the available evidences."));
					}
					_maxSize = 9223372036854775807uL;
				}
				else
				{
					_maxSize = (ulong)permission.UserQuota;
				}
				_resolved = true;
				return _maxSize;
			}
		}

		internal string Root => directory.FullName;

		private IsolatedStorageFile(IsolatedStorageScope scope)
		{
			storage_scope = scope;
		}

		internal IsolatedStorageFile(IsolatedStorageScope scope, string location)
		{
			storage_scope = scope;
			directory = new DirectoryInfo(location);
			if (!directory.Exists)
			{
				string text = Locale.GetText("Invalid storage.");
				throw new IsolatedStorageException(text);
			}
		}

		/// <summary>Gets the enumerator for the <see cref="T:System.IO.IsolatedStorage.IsolatedStorageFile" /> stores within an isolated storage scope.</summary>
		/// <returns>Enumerator for the <see cref="T:System.IO.IsolatedStorage.IsolatedStorageFile" /> stores within the specified isolated storage scope.</returns>
		/// <param name="scope">Represents the <see cref="T:System.IO.IsolatedStorage.IsolatedStorageScope" /> for which to return isolated stores. User and User|Roaming are the only IsolatedStorageScope combinations supported. </param>
		/// <PermissionSet>
		///   <IPermission class="System.Security.Permissions.FileIOPermission, mscorlib, Version=2.0.3600.0, Culture=neutral, PublicKeyToken=b77a5c561934e089" version="1" Unrestricted="true" />
		///   <IPermission class="System.Security.Permissions.IsolatedStorageFilePermission, mscorlib, Version=2.0.3600.0, Culture=neutral, PublicKeyToken=b77a5c561934e089" version="1" Unrestricted="true" />
		///   <IPermission class="System.Security.Permissions.SecurityPermission, mscorlib, Version=2.0.3600.0, Culture=neutral, PublicKeyToken=b77a5c561934e089" version="1" Flags="UnmanagedCode" />
		/// </PermissionSet>
		public static IEnumerator GetEnumerator(IsolatedStorageScope scope)
		{
			Demand(scope);
			if (scope != IsolatedStorageScope.User && scope != (IsolatedStorageScope.User | IsolatedStorageScope.Roaming) && scope != IsolatedStorageScope.Machine)
			{
				string text = Locale.GetText("Invalid scope, only User, User|Roaming and Machine are valid");
				throw new ArgumentException(text);
			}
			return new IsolatedStorageFileEnumerator(scope, GetIsolatedStorageRoot(scope));
		}

		/// <summary>Obtains isolated storage corresponding to the given application domain and the assembly evidence objects and types.</summary>
		/// <returns>An <see cref="T:System.IO.IsolatedStorage.IsolatedStorageFile" /> object representing the parameters.</returns>
		/// <param name="scope">A bitwise combination of the <see cref="T:System.IO.IsolatedStorage.IsolatedStorageScope" /> values. </param>
		/// <param name="domainEvidence">An <see cref="T:System.Security.Policy.Evidence" /> object containing the application domain identity. </param>
		/// <param name="domainEvidenceType">The identity <see cref="T:System.Type" /> to choose from the application domain evidence. </param>
		/// <param name="assemblyEvidence">An <see cref="T:System.Security.Policy.Evidence" /> object containing the code assembly identity. </param>
		/// <param name="assemblyEvidenceType">The identity <see cref="T:System.Type" /> to choose from the application code assembly evidence. </param>
		/// <exception cref="T:System.Security.SecurityException">Sufficient isolated storage permissions have not been granted. </exception>
		/// <exception cref="T:System.ArgumentNullException">The <paramref name="domainEvidence" /> or <paramref name="assemblyEvidence" /> identity has not been passed in. </exception>
		/// <exception cref="T:System.ArgumentException">The <paramref name="scope" /> is invalid. </exception>
		/// <PermissionSet>
		///   <IPermission class="System.Security.Permissions.IsolatedStorageFilePermission, mscorlib, Version=2.0.3600.0, Culture=neutral, PublicKeyToken=b77a5c561934e089" version="1" Unrestricted="true" />
		///   <IPermission class="System.Security.Permissions.ReflectionPermission, mscorlib, Version=2.0.3600.0, Culture=neutral, PublicKeyToken=b77a5c561934e089" version="1" Flags="MemberAccess" />
		///   <IPermission class="System.Security.Permissions.SecurityPermission, mscorlib, Version=2.0.3600.0, Culture=neutral, PublicKeyToken=b77a5c561934e089" version="1" Flags="UnmanagedCode, ControlEvidence" />
		/// </PermissionSet>
		public static IsolatedStorageFile GetStore(IsolatedStorageScope scope, Evidence domainEvidence, Type domainEvidenceType, Evidence assemblyEvidence, Type assemblyEvidenceType)
		{
			Demand(scope);
			bool flag = (scope & IsolatedStorageScope.Domain) != 0;
			if (flag && domainEvidence == null)
			{
				throw new ArgumentNullException("domainEvidence");
			}
			bool flag2 = (scope & IsolatedStorageScope.Assembly) != 0;
			if (flag2 && assemblyEvidence == null)
			{
				throw new ArgumentNullException("assemblyEvidence");
			}
			IsolatedStorageFile isolatedStorageFile = new IsolatedStorageFile(scope);
			if (flag)
			{
				if (domainEvidenceType == null)
				{
					isolatedStorageFile._domainIdentity = GetDomainIdentityFromEvidence(domainEvidence);
				}
				else
				{
					isolatedStorageFile._domainIdentity = GetTypeFromEvidence(domainEvidence, domainEvidenceType);
				}
				if (isolatedStorageFile._domainIdentity == null)
				{
					throw new IsolatedStorageException(Locale.GetText("Couldn't find domain identity."));
				}
			}
			if (flag2)
			{
				if (assemblyEvidenceType == null)
				{
					isolatedStorageFile._assemblyIdentity = GetAssemblyIdentityFromEvidence(assemblyEvidence);
				}
				else
				{
					isolatedStorageFile._assemblyIdentity = GetTypeFromEvidence(assemblyEvidence, assemblyEvidenceType);
				}
				if (isolatedStorageFile._assemblyIdentity == null)
				{
					throw new IsolatedStorageException(Locale.GetText("Couldn't find assembly identity."));
				}
			}
			isolatedStorageFile.PostInit();
			return isolatedStorageFile;
		}

		/// <summary>Obtains the isolated storage corresponding to the given application domain and assembly evidence objects.</summary>
		/// <returns>An <see cref="T:System.IO.IsolatedStorage.IsolatedStorageFile" /> representing the parameters.</returns>
		/// <param name="scope">A bitwise combination of the <see cref="T:System.IO.IsolatedStorage.IsolatedStorageScope" /> values. </param>
		/// <param name="domainIdentity">An <see cref="T:System.Object" /> that contains evidence for the application domain identity. </param>
		/// <param name="assemblyIdentity">An <see cref="T:System.Object" /> that contains evidence for the code assembly identity. </param>
		/// <exception cref="T:System.Security.SecurityException">Sufficient isolated storage permissions have not been granted. </exception>
		/// <exception cref="T:System.ArgumentNullException">Neither the <paramref name="domainIdentity" /> nor <paramref name="assemblyIdentity" /> have been passed in. This verifies that the correct constructor is being used.-or- Either <paramref name="domainIdentity" /> or <paramref name="assemblyIdentity" /> are null. </exception>
		/// <exception cref="T:System.ArgumentException">The <paramref name="scope" /> is invalid. </exception>
		/// <PermissionSet>
		///   <IPermission class="System.Security.Permissions.IsolatedStorageFilePermission, mscorlib, Version=2.0.3600.0, Culture=neutral, PublicKeyToken=b77a5c561934e089" version="1" Unrestricted="true" />
		///   <IPermission class="System.Security.Permissions.ReflectionPermission, mscorlib, Version=2.0.3600.0, Culture=neutral, PublicKeyToken=b77a5c561934e089" version="1" Flags="MemberAccess" />
		///   <IPermission class="System.Security.Permissions.SecurityPermission, mscorlib, Version=2.0.3600.0, Culture=neutral, PublicKeyToken=b77a5c561934e089" version="1" Flags="UnmanagedCode" />
		/// </PermissionSet>
		public static IsolatedStorageFile GetStore(IsolatedStorageScope scope, object domainIdentity, object assemblyIdentity)
		{
			Demand(scope);
			if ((scope & IsolatedStorageScope.Domain) != 0 && domainIdentity == null)
			{
				throw new ArgumentNullException("domainIdentity");
			}
			bool flag = (scope & IsolatedStorageScope.Assembly) != 0;
			if (flag && assemblyIdentity == null)
			{
				throw new ArgumentNullException("assemblyIdentity");
			}
			IsolatedStorageFile isolatedStorageFile = new IsolatedStorageFile(scope);
			if (flag)
			{
				isolatedStorageFile._fullEvidences = Assembly.GetCallingAssembly().UnprotectedGetEvidence();
			}
			isolatedStorageFile._domainIdentity = domainIdentity;
			isolatedStorageFile._assemblyIdentity = assemblyIdentity;
			isolatedStorageFile.PostInit();
			return isolatedStorageFile;
		}

		/// <summary>Obtains isolated storage corresponding to the isolated storage scope given the application domain and assembly evidence types.</summary>
		/// <returns>An <see cref="T:System.IO.IsolatedStorage.IsolatedStorageFile" /> object representing the parameters.</returns>
		/// <param name="scope">A bitwise combination of the <see cref="T:System.IO.IsolatedStorage.IsolatedStorageScope" /> values. </param>
		/// <param name="domainEvidenceType">The type of the <see cref="T:System.Security.Policy.Evidence" /> that you can chose from the list of <see cref="T:System.Security.Policy.Evidence" /> present in the domain of the calling application. null lets the <see cref="T:System.IO.IsolatedStorage.IsolatedStorage" /> object choose the evidence. </param>
		/// <param name="assemblyEvidenceType">The type of the <see cref="T:System.Security.Policy.Evidence" /> that you can chose from the list of <see cref="T:System.Security.Policy.Evidence" /> present in the domain of the calling application. null lets the <see cref="T:System.IO.IsolatedStorage.IsolatedStorage" /> object choose the evidence. </param>
		/// <exception cref="T:System.Security.SecurityException">Sufficient isolated storage permissions have not been granted. </exception>
		/// <exception cref="T:System.ArgumentException">The <paramref name="scope" /> is invalid. </exception>
		/// <exception cref="T:System.IO.IsolatedStorage.IsolatedStorageException">The evidence type provided is missing in the assembly Evidence list. </exception>
		/// <PermissionSet>
		///   <IPermission class="System.Security.Permissions.IsolatedStorageFilePermission, mscorlib, Version=2.0.3600.0, Culture=neutral, PublicKeyToken=b77a5c561934e089" version="1" Unrestricted="true" />
		///   <IPermission class="System.Security.Permissions.ReflectionPermission, mscorlib, Version=2.0.3600.0, Culture=neutral, PublicKeyToken=b77a5c561934e089" version="1" Flags="MemberAccess" />
		///   <IPermission class="System.Security.Permissions.SecurityPermission, mscorlib, Version=2.0.3600.0, Culture=neutral, PublicKeyToken=b77a5c561934e089" version="1" Flags="UnmanagedCode" />
		/// </PermissionSet>
		public static IsolatedStorageFile GetStore(IsolatedStorageScope scope, Type domainEvidenceType, Type assemblyEvidenceType)
		{
			Demand(scope);
			IsolatedStorageFile isolatedStorageFile = new IsolatedStorageFile(scope);
			if ((scope & IsolatedStorageScope.Domain) != 0)
			{
				if (domainEvidenceType == null)
				{
					domainEvidenceType = typeof(Url);
				}
				isolatedStorageFile._domainIdentity = GetTypeFromEvidence(AppDomain.CurrentDomain.Evidence, domainEvidenceType);
			}
			if ((scope & IsolatedStorageScope.Assembly) != 0)
			{
				Evidence e = isolatedStorageFile._fullEvidences = Assembly.GetCallingAssembly().UnprotectedGetEvidence();
				if ((scope & IsolatedStorageScope.Domain) != 0)
				{
					if (assemblyEvidenceType == null)
					{
						assemblyEvidenceType = typeof(Url);
					}
					isolatedStorageFile._assemblyIdentity = GetTypeFromEvidence(e, assemblyEvidenceType);
				}
				else
				{
					isolatedStorageFile._assemblyIdentity = GetAssemblyIdentityFromEvidence(e);
				}
			}
			isolatedStorageFile.PostInit();
			return isolatedStorageFile;
		}

		/// <summary>Obtains isolated storage corresponding to the given application identity.</summary>
		/// <returns>An <see cref="T:System.IO.IsolatedStorage.IsolatedStorageFile" /> object representing the parameters.</returns>
		/// <param name="scope">A bitwise combination of the <see cref="T:System.IO.IsolatedStorage.IsolatedStorageScope" /> values. </param>
		/// <param name="applicationIdentity">An <see cref="T:System.Object" /> that contains evidence for the application identity. </param>
		/// <exception cref="T:System.Security.SecurityException">Sufficient isolated storage permissions have not been granted. </exception>
		/// <exception cref="T:System.ArgumentNullException">The <paramref name="applicationEvidence" /> identity has not been passed in. </exception>
		/// <exception cref="T:System.ArgumentException">The <paramref name="scope" /> is invalid. </exception>
		/// <PermissionSet>
		///   <IPermission class="System.Security.Permissions.IsolatedStorageFilePermission, mscorlib, Version=2.0.3600.0, Culture=neutral, PublicKeyToken=b77a5c561934e089" version="1" Unrestricted="true" />
		///   <IPermission class="System.Security.Permissions.ReflectionPermission, mscorlib, Version=2.0.3600.0, Culture=neutral, PublicKeyToken=b77a5c561934e089" version="1" Flags="MemberAccess" />
		///   <IPermission class="System.Security.Permissions.SecurityPermission, mscorlib, Version=2.0.3600.0, Culture=neutral, PublicKeyToken=b77a5c561934e089" version="1" Flags="UnmanagedCode" />
		/// </PermissionSet>
		public static IsolatedStorageFile GetStore(IsolatedStorageScope scope, object applicationIdentity)
		{
			Demand(scope);
			if (applicationIdentity == null)
			{
				throw new ArgumentNullException("applicationIdentity");
			}
			IsolatedStorageFile isolatedStorageFile = new IsolatedStorageFile(scope);
			isolatedStorageFile._applicationIdentity = applicationIdentity;
			isolatedStorageFile._fullEvidences = Assembly.GetCallingAssembly().UnprotectedGetEvidence();
			isolatedStorageFile.PostInit();
			return isolatedStorageFile;
		}

		/// <summary>Obtains isolated storage corresponding to the isolation scope and the application identity object.</summary>
		/// <returns>An <see cref="T:System.IO.IsolatedStorage.IsolatedStorageFile" /> object representing the parameters.</returns>
		/// <param name="scope">A bitwise combination of the <see cref="T:System.IO.IsolatedStorage.IsolatedStorageScope" /> values. </param>
		/// <param name="applicationEvidenceType">An <see cref="T:System.Security.Policy.Evidence" /> object containing the application identity. </param>
		/// <exception cref="T:System.Security.SecurityException">Sufficient isolated storage permissions have not been granted. </exception>
		/// <exception cref="T:System.ArgumentNullException">The <paramref name="domainEvidence" /> or <paramref name="assemblyEvidence" /> identity has not been passed in. </exception>
		/// <exception cref="T:System.ArgumentException">The <paramref name="scope" /> is invalid. </exception>
		/// <PermissionSet>
		///   <IPermission class="System.Security.Permissions.IsolatedStorageFilePermission, mscorlib, Version=2.0.3600.0, Culture=neutral, PublicKeyToken=b77a5c561934e089" version="1" Unrestricted="true" />
		///   <IPermission class="System.Security.Permissions.ReflectionPermission, mscorlib, Version=2.0.3600.0, Culture=neutral, PublicKeyToken=b77a5c561934e089" version="1" Flags="MemberAccess" />
		///   <IPermission class="System.Security.Permissions.SecurityPermission, mscorlib, Version=2.0.3600.0, Culture=neutral, PublicKeyToken=b77a5c561934e089" version="1" Flags="UnmanagedCode" />
		/// </PermissionSet>
		public static IsolatedStorageFile GetStore(IsolatedStorageScope scope, Type applicationEvidenceType)
		{
			Demand(scope);
			IsolatedStorageFile isolatedStorageFile = new IsolatedStorageFile(scope);
			isolatedStorageFile.InitStore(scope, applicationEvidenceType);
			isolatedStorageFile._fullEvidences = Assembly.GetCallingAssembly().UnprotectedGetEvidence();
			isolatedStorageFile.PostInit();
			return isolatedStorageFile;
		}

		/// <summary>Obtains machine-scoped isolated storage corresponding to the calling code's application identity.</summary>
		/// <returns>An <see cref="T:System.IO.IsolatedStorage.IsolatedStorageFile" /> object corresponding to the isolated storage scope based on the calling code's application identity.</returns>
		/// <exception cref="T:System.Security.SecurityException">Sufficient isolated storage permissions have not been granted. </exception>
		/// <PermissionSet>
		///   <IPermission class="System.Security.Permissions.IsolatedStorageFilePermission, mscorlib, Version=2.0.3600.0, Culture=neutral, PublicKeyToken=b77a5c561934e089" version="1" Unrestricted="true" />
		///   <IPermission class="System.Security.Permissions.ReflectionPermission, mscorlib, Version=2.0.3600.0, Culture=neutral, PublicKeyToken=b77a5c561934e089" version="1" Flags="MemberAccess" />
		///   <IPermission class="System.Security.Permissions.SecurityPermission, mscorlib, Version=2.0.3600.0, Culture=neutral, PublicKeyToken=b77a5c561934e089" version="1" Flags="UnmanagedCode" />
		/// </PermissionSet>
		public static IsolatedStorageFile GetMachineStoreForApplication()
		{
			IsolatedStorageScope scope = IsolatedStorageScope.Machine | IsolatedStorageScope.Application;
			IsolatedStorageFile isolatedStorageFile = new IsolatedStorageFile(scope);
			isolatedStorageFile.InitStore(scope, null);
			isolatedStorageFile._fullEvidences = Assembly.GetCallingAssembly().UnprotectedGetEvidence();
			isolatedStorageFile.PostInit();
			return isolatedStorageFile;
		}

		/// <summary>Obtains machine-scoped isolated storage corresponding to the calling code's assembly identity.</summary>
		/// <returns>An <see cref="T:System.IO.IsolatedStorage.IsolatedStorageFile" /> object corresponding to the isolated storage scope based on the calling code's assembly identity.</returns>
		/// <exception cref="T:System.Security.SecurityException">Sufficient isolated storage permissions have not been granted. </exception>
		/// <PermissionSet>
		///   <IPermission class="System.Security.Permissions.IsolatedStorageFilePermission, mscorlib, Version=2.0.3600.0, Culture=neutral, PublicKeyToken=b77a5c561934e089" version="1" Unrestricted="true" />
		///   <IPermission class="System.Security.Permissions.ReflectionPermission, mscorlib, Version=2.0.3600.0, Culture=neutral, PublicKeyToken=b77a5c561934e089" version="1" Flags="MemberAccess" />
		///   <IPermission class="System.Security.Permissions.SecurityPermission, mscorlib, Version=2.0.3600.0, Culture=neutral, PublicKeyToken=b77a5c561934e089" version="1" Flags="UnmanagedCode" />
		/// </PermissionSet>
		public static IsolatedStorageFile GetMachineStoreForAssembly()
		{
			IsolatedStorageScope scope = IsolatedStorageScope.Assembly | IsolatedStorageScope.Machine;
			IsolatedStorageFile isolatedStorageFile = new IsolatedStorageFile(scope);
			isolatedStorageFile._assemblyIdentity = GetAssemblyIdentityFromEvidence(isolatedStorageFile._fullEvidences = Assembly.GetCallingAssembly().UnprotectedGetEvidence());
			isolatedStorageFile.PostInit();
			return isolatedStorageFile;
		}

		/// <summary>Obtains machine-scoped isolated storage corresponding to the application domain identity and the assembly identity.</summary>
		/// <returns>An <see cref="T:System.IO.IsolatedStorage.IsolatedStorageFile" /> object corresponding to the <see cref="T:System.IO.IsolatedStorage.IsolatedStorageScope" />, based on a combination of the application domain identity and the assembly identity.</returns>
		/// <exception cref="T:System.Security.SecurityException">Sufficient isolated storage permissions have not been granted. </exception>
		/// <exception cref="T:System.IO.IsolatedStorage.IsolatedStorageException">The store failed to open.-or- The assembly specified has insufficient permissions to create isolated stores. </exception>
		/// <PermissionSet>
		///   <IPermission class="System.Security.Permissions.IsolatedStorageFilePermission, mscorlib, Version=2.0.3600.0, Culture=neutral, PublicKeyToken=b77a5c561934e089" version="1" Unrestricted="true" />
		///   <IPermission class="System.Security.Permissions.ReflectionPermission, mscorlib, Version=2.0.3600.0, Culture=neutral, PublicKeyToken=b77a5c561934e089" version="1" Flags="MemberAccess" />
		///   <IPermission class="System.Security.Permissions.SecurityPermission, mscorlib, Version=2.0.3600.0, Culture=neutral, PublicKeyToken=b77a5c561934e089" version="1" Flags="UnmanagedCode" />
		/// </PermissionSet>
		public static IsolatedStorageFile GetMachineStoreForDomain()
		{
			IsolatedStorageScope scope = IsolatedStorageScope.Domain | IsolatedStorageScope.Assembly | IsolatedStorageScope.Machine;
			IsolatedStorageFile isolatedStorageFile = new IsolatedStorageFile(scope);
			isolatedStorageFile._domainIdentity = GetDomainIdentityFromEvidence(AppDomain.CurrentDomain.Evidence);
			isolatedStorageFile._assemblyIdentity = GetAssemblyIdentityFromEvidence(isolatedStorageFile._fullEvidences = Assembly.GetCallingAssembly().UnprotectedGetEvidence());
			isolatedStorageFile.PostInit();
			return isolatedStorageFile;
		}

		/// <summary>Obtains user-scoped isolated storage corresponding to the calling code's application identity.</summary>
		/// <returns>An <see cref="T:System.IO.IsolatedStorage.IsolatedStorageFile" /> object corresponding to the isolated storage scope based on the calling code's assembly identity.</returns>
		/// <exception cref="T:System.Security.SecurityException">Sufficient isolated storage permissions have not been granted. </exception>
		/// <PermissionSet>
		///   <IPermission class="System.Security.Permissions.IsolatedStorageFilePermission, mscorlib, Version=2.0.3600.0, Culture=neutral, PublicKeyToken=b77a5c561934e089" version="1" Unrestricted="true" />
		///   <IPermission class="System.Security.Permissions.ReflectionPermission, mscorlib, Version=2.0.3600.0, Culture=neutral, PublicKeyToken=b77a5c561934e089" version="1" Flags="MemberAccess" />
		///   <IPermission class="System.Security.Permissions.SecurityPermission, mscorlib, Version=2.0.3600.0, Culture=neutral, PublicKeyToken=b77a5c561934e089" version="1" Flags="UnmanagedCode" />
		/// </PermissionSet>
		public static IsolatedStorageFile GetUserStoreForApplication()
		{
			IsolatedStorageScope scope = IsolatedStorageScope.User | IsolatedStorageScope.Application;
			IsolatedStorageFile isolatedStorageFile = new IsolatedStorageFile(scope);
			isolatedStorageFile.InitStore(scope, null);
			isolatedStorageFile._fullEvidences = Assembly.GetCallingAssembly().UnprotectedGetEvidence();
			isolatedStorageFile.PostInit();
			return isolatedStorageFile;
		}

		/// <summary>Obtains user-scoped isolated storage corresponding to the calling code's assembly identity.</summary>
		/// <returns>An <see cref="T:System.IO.IsolatedStorage.IsolatedStorageFile" /> object corresponding to the isolated storage scope based on the calling code's assembly identity.</returns>
		/// <exception cref="T:System.Security.SecurityException">Sufficient isolated storage permissions have not been granted. </exception>
		/// <PermissionSet>
		///   <IPermission class="System.Security.Permissions.IsolatedStorageFilePermission, mscorlib, Version=2.0.3600.0, Culture=neutral, PublicKeyToken=b77a5c561934e089" version="1" Unrestricted="true" />
		///   <IPermission class="System.Security.Permissions.ReflectionPermission, mscorlib, Version=2.0.3600.0, Culture=neutral, PublicKeyToken=b77a5c561934e089" version="1" Flags="MemberAccess" />
		///   <IPermission class="System.Security.Permissions.SecurityPermission, mscorlib, Version=2.0.3600.0, Culture=neutral, PublicKeyToken=b77a5c561934e089" version="1" Flags="UnmanagedCode" />
		/// </PermissionSet>
		public static IsolatedStorageFile GetUserStoreForAssembly()
		{
			IsolatedStorageScope scope = IsolatedStorageScope.User | IsolatedStorageScope.Assembly;
			IsolatedStorageFile isolatedStorageFile = new IsolatedStorageFile(scope);
			isolatedStorageFile._assemblyIdentity = GetAssemblyIdentityFromEvidence(isolatedStorageFile._fullEvidences = Assembly.GetCallingAssembly().UnprotectedGetEvidence());
			isolatedStorageFile.PostInit();
			return isolatedStorageFile;
		}

		/// <summary>Obtains user-scoped isolated storage corresponding to the application domain identity and assembly identity.</summary>
		/// <returns>An <see cref="T:System.IO.IsolatedStorage.IsolatedStorageFile" /> object corresponding to the <see cref="T:System.IO.IsolatedStorage.IsolatedStorageScope" />, based on a combination of the application domain identity and the assembly identity.</returns>
		/// <exception cref="T:System.Security.SecurityException">Sufficient isolated storage permissions have not been granted. </exception>
		/// <exception cref="T:System.IO.IsolatedStorage.IsolatedStorageException">The store failed to open.-or- The assembly specified has insufficient permissions to create isolated stores. </exception>
		/// <PermissionSet>
		///   <IPermission class="System.Security.Permissions.IsolatedStorageFilePermission, mscorlib, Version=2.0.3600.0, Culture=neutral, PublicKeyToken=b77a5c561934e089" version="1" Unrestricted="true" />
		///   <IPermission class="System.Security.Permissions.ReflectionPermission, mscorlib, Version=2.0.3600.0, Culture=neutral, PublicKeyToken=b77a5c561934e089" version="1" Flags="MemberAccess" />
		///   <IPermission class="System.Security.Permissions.SecurityPermission, mscorlib, Version=2.0.3600.0, Culture=neutral, PublicKeyToken=b77a5c561934e089" version="1" Flags="UnmanagedCode" />
		/// </PermissionSet>
		public static IsolatedStorageFile GetUserStoreForDomain()
		{
			IsolatedStorageScope scope = IsolatedStorageScope.User | IsolatedStorageScope.Domain | IsolatedStorageScope.Assembly;
			IsolatedStorageFile isolatedStorageFile = new IsolatedStorageFile(scope);
			isolatedStorageFile._domainIdentity = GetDomainIdentityFromEvidence(AppDomain.CurrentDomain.Evidence);
			isolatedStorageFile._assemblyIdentity = GetAssemblyIdentityFromEvidence(isolatedStorageFile._fullEvidences = Assembly.GetCallingAssembly().UnprotectedGetEvidence());
			isolatedStorageFile.PostInit();
			return isolatedStorageFile;
		}

		/// <summary>Removes the specified isolated storage scope for all identities.</summary>
		/// <param name="scope">A bitwise combination of the <see cref="T:System.IO.IsolatedStorage.IsolatedStorageScope" /> values. </param>
		/// <exception cref="T:System.IO.IsolatedStorage.IsolatedStorageException">The isolated store cannot be removed. </exception>
		/// <PermissionSet>
		///   <IPermission class="System.Security.Permissions.IsolatedStorageFilePermission, mscorlib, Version=2.0.3600.0, Culture=neutral, PublicKeyToken=b77a5c561934e089" version="1" Unrestricted="true" />
		///   <IPermission class="System.Security.Permissions.SecurityPermission, mscorlib, Version=2.0.3600.0, Culture=neutral, PublicKeyToken=b77a5c561934e089" version="1" Flags="UnmanagedCode" />
		/// </PermissionSet>
		public static void Remove(IsolatedStorageScope scope)
		{
			string isolatedStorageRoot = GetIsolatedStorageRoot(scope);
			Directory.Delete(isolatedStorageRoot, recursive: true);
		}

		internal static string GetIsolatedStorageRoot(IsolatedStorageScope scope)
		{
			string text = null;
			if ((scope & IsolatedStorageScope.User) != 0)
			{
				text = (((scope & IsolatedStorageScope.Roaming) == 0) ? Environment.InternalGetFolderPath(Environment.SpecialFolder.ApplicationData) : Environment.InternalGetFolderPath(Environment.SpecialFolder.LocalApplicationData));
			}
			else if ((scope & IsolatedStorageScope.Machine) != 0)
			{
				text = Environment.InternalGetFolderPath(Environment.SpecialFolder.CommonApplicationData);
			}
			if (text == null)
			{
				string text2 = Locale.GetText("Couldn't access storage location for '{0}'.");
				throw new IsolatedStorageException(string.Format(text2, scope));
			}
			return Path.Combine(text, ".isolated-storage");
		}

		private static void Demand(IsolatedStorageScope scope)
		{
			if (SecurityManager.SecurityEnabled)
			{
				IsolatedStorageFilePermission isolatedStorageFilePermission = new IsolatedStorageFilePermission(PermissionState.None);
				isolatedStorageFilePermission.UsageAllowed = ScopeToContainment(scope);
				isolatedStorageFilePermission.Demand();
			}
		}

		private static IsolatedStorageContainment ScopeToContainment(IsolatedStorageScope scope)
		{
			switch (scope)
			{
			case IsolatedStorageScope.User | IsolatedStorageScope.Domain | IsolatedStorageScope.Assembly:
				return IsolatedStorageContainment.DomainIsolationByUser;
			case IsolatedStorageScope.User | IsolatedStorageScope.Assembly:
				return IsolatedStorageContainment.AssemblyIsolationByUser;
			case IsolatedStorageScope.User | IsolatedStorageScope.Domain | IsolatedStorageScope.Assembly | IsolatedStorageScope.Roaming:
				return IsolatedStorageContainment.DomainIsolationByRoamingUser;
			case IsolatedStorageScope.User | IsolatedStorageScope.Assembly | IsolatedStorageScope.Roaming:
				return IsolatedStorageContainment.AssemblyIsolationByRoamingUser;
			case IsolatedStorageScope.User | IsolatedStorageScope.Application:
				return IsolatedStorageContainment.ApplicationIsolationByUser;
			case IsolatedStorageScope.Domain | IsolatedStorageScope.Assembly | IsolatedStorageScope.Machine:
				return IsolatedStorageContainment.DomainIsolationByMachine;
			case IsolatedStorageScope.Assembly | IsolatedStorageScope.Machine:
				return IsolatedStorageContainment.AssemblyIsolationByMachine;
			case IsolatedStorageScope.Machine | IsolatedStorageScope.Application:
				return IsolatedStorageContainment.ApplicationIsolationByMachine;
			case IsolatedStorageScope.User | IsolatedStorageScope.Roaming | IsolatedStorageScope.Application:
				return IsolatedStorageContainment.ApplicationIsolationByRoamingUser;
			default:
				return IsolatedStorageContainment.UnrestrictedIsolatedStorage;
			}
		}

		internal static ulong GetDirectorySize(DirectoryInfo di)
		{
			ulong num = 0uL;
			FileInfo[] files = di.GetFiles();
			foreach (FileInfo fileInfo in files)
			{
				num = (ulong)((long)num + fileInfo.Length);
			}
			DirectoryInfo[] directories = di.GetDirectories();
			foreach (DirectoryInfo di2 in directories)
			{
				num += GetDirectorySize(di2);
			}
			return num;
		}

		~IsolatedStorageFile()
		{
		}

		private void PostInit()
		{
			string isolatedStorageRoot = GetIsolatedStorageRoot(base.Scope);
			string text = null;
			if (_applicationIdentity != null)
			{
				text = $"a{SeparatorInternal}{GetNameFromIdentity(_applicationIdentity)}";
			}
			else if (_domainIdentity != null)
			{
				text = string.Format("d{0}{1}{0}{2}", SeparatorInternal, GetNameFromIdentity(_domainIdentity), GetNameFromIdentity(_assemblyIdentity));
			}
			else
			{
				if (_assemblyIdentity == null)
				{
					throw new IsolatedStorageException(Locale.GetText("No code identity available."));
				}
				text = string.Format("d{0}none{0}{1}", SeparatorInternal, GetNameFromIdentity(_assemblyIdentity));
			}
			isolatedStorageRoot = Path.Combine(isolatedStorageRoot, text);
			directory = new DirectoryInfo(isolatedStorageRoot);
			if (!directory.Exists)
			{
				try
				{
					directory.Create();
					SaveIdentities(isolatedStorageRoot);
				}
				catch (IOException)
				{
				}
			}
		}

		/// <summary>Closes a store previously opened with <see cref="M:System.IO.IsolatedStorage.IsolatedStorageFile.GetStore(System.IO.IsolatedStorage.IsolatedStorageScope,System.Type,System.Type)" />, <see cref="M:System.IO.IsolatedStorage.IsolatedStorageFile.GetUserStoreForAssembly" />, or <see cref="M:System.IO.IsolatedStorage.IsolatedStorageFile.GetUserStoreForDomain" />.</summary>
		public void Close()
		{
		}

		/// <summary>Creates a directory in the isolated storage scope.</summary>
		/// <param name="dir">The relative path of the directory to create within the isolated storage scope. </param>
		/// <exception cref="T:System.IO.IsolatedStorage.IsolatedStorageException">The current code has insufficient permissions to create isolated storage directory. </exception>
		/// <exception cref="T:System.ArgumentNullException">The directory path is null. </exception>
		/// <PermissionSet>
		///   <IPermission class="System.Security.Permissions.SecurityPermission, mscorlib, Version=2.0.3600.0, Culture=neutral, PublicKeyToken=b77a5c561934e089" version="1" Flags="UnmanagedCode" />
		/// </PermissionSet>
		public void CreateDirectory(string dir)
		{
			if (dir == null)
			{
				throw new ArgumentNullException("dir");
			}
			if (dir.IndexOfAny(Path.PathSeparatorChars) < 0)
			{
				if (directory.GetFiles(dir).Length > 0)
				{
					throw new IOException(Locale.GetText("Directory name already exists as a file."));
				}
				directory.CreateSubdirectory(dir);
				return;
			}
			string[] array = dir.Split(Path.PathSeparatorChars);
			DirectoryInfo directoryInfo = directory;
			int num = 0;
			while (true)
			{
				if (num < array.Length)
				{
					if (directoryInfo.GetFiles(array[num]).Length > 0)
					{
						break;
					}
					directoryInfo = directoryInfo.CreateSubdirectory(array[num]);
					num++;
					continue;
				}
				return;
			}
			throw new IOException(Locale.GetText("Part of the directory name already exists as a file."));
		}

		/// <summary>Deletes a directory in the isolated storage scope.</summary>
		/// <param name="dir">The relative path of the directory to delete within the isolated storage scope. </param>
		/// <exception cref="T:System.IO.IsolatedStorage.IsolatedStorageException">The directory could not be deleted. </exception>
		/// <exception cref="T:System.ArgumentNullException">The directory path was null. </exception>
		/// <PermissionSet>
		///   <IPermission class="System.Security.Permissions.SecurityPermission, mscorlib, Version=2.0.3600.0, Culture=neutral, PublicKeyToken=b77a5c561934e089" version="1" Flags="UnmanagedCode" />
		/// </PermissionSet>
		public void DeleteDirectory(string dir)
		{
			//Discarded unreachable code: IL_0033
			try
			{
				DirectoryInfo directoryInfo = directory.CreateSubdirectory(dir);
				directoryInfo.Delete();
			}
			catch
			{
				throw new IsolatedStorageException(Locale.GetText("Could not delete directory '{0}'", dir));
			}
		}

		/// <summary>Deletes a file in the isolated storage scope.</summary>
		/// <param name="file">The relative path of the file to delete within the isolated storage scope. </param>
		/// <exception cref="T:System.IO.IsolatedStorage.IsolatedStorageException">The target file is open or the path is incorrect. </exception>
		/// <exception cref="T:System.ArgumentNullException">The file path is null. </exception>
		/// <PermissionSet>
		///   <IPermission class="System.Security.Permissions.SecurityPermission, mscorlib, Version=2.0.3600.0, Culture=neutral, PublicKeyToken=b77a5c561934e089" version="1" Flags="UnmanagedCode" />
		/// </PermissionSet>
		public void DeleteFile(string file)
		{
			File.Delete(Path.Combine(directory.FullName, file));
		}

		/// <summary>Releases all resources used by the <see cref="T:System.IO.IsolatedStorage.IsolatedStorageFile" />. </summary>
		public void Dispose()
		{
			GC.SuppressFinalize(this);
		}

		/// <summary>Enumerates directories in an isolated storage scope that match a given pattern.</summary>
		/// <returns>An <see cref="T:System.Array" /> of the relative paths of directories in the isolated storage scope that match <paramref name="searchPattern" />. A zero-length array specifies that there are no directories that match.</returns>
		/// <param name="searchPattern">A search pattern. Both single-character ("?") and multi-character ("*") wildcards are supported. </param>
		/// <exception cref="T:System.ArgumentNullException">The <paramref name="searchPattern" /> was null. </exception>
		/// <PermissionSet>
		///   <IPermission class="System.Security.Permissions.FileIOPermission, mscorlib, Version=2.0.3600.0, Culture=neutral, PublicKeyToken=b77a5c561934e089" version="1" Unrestricted="true" />
		/// </PermissionSet>
		public string[] GetDirectoryNames(string searchPattern)
		{
			if (searchPattern == null)
			{
				throw new ArgumentNullException("searchPattern");
			}
			string directoryName = Path.GetDirectoryName(searchPattern);
			string fileName = Path.GetFileName(searchPattern);
			DirectoryInfo[] array = null;
			if (directoryName == null || directoryName.Length == 0)
			{
				array = directory.GetDirectories(searchPattern);
			}
			else
			{
				DirectoryInfo[] directories = directory.GetDirectories(directoryName);
				if (directories.Length != 1 || !(directories[0].Name == directoryName) || directories[0].FullName.IndexOf(directory.FullName) < 0)
				{
					throw new SecurityException();
				}
				array = directories[0].GetDirectories(fileName);
			}
			return GetNames(array);
		}

		private string[] GetNames(FileSystemInfo[] afsi)
		{
			string[] array = new string[afsi.Length];
			for (int i = 0; i != afsi.Length; i++)
			{
				array[i] = afsi[i].Name;
			}
			return array;
		}

		/// <summary>Enumerates files in isolated storage scope that match a given pattern.</summary>
		/// <returns>An <see cref="T:System.Array" /> of relative paths of files in the isolated storage scope that match <paramref name="searchPattern" />. A zero-length array specifies that there are no files that match.</returns>
		/// <param name="searchPattern">A search pattern. Both single-character ("?") and multi-character ("*") wildcards are supported. </param>
		/// <exception cref="T:System.ArgumentNullException">The <paramref name="searchPattern" /> was null. </exception>
		/// <PermissionSet>
		///   <IPermission class="System.Security.Permissions.FileIOPermission, mscorlib, Version=2.0.3600.0, Culture=neutral, PublicKeyToken=b77a5c561934e089" version="1" Unrestricted="true" />
		/// </PermissionSet>
		public string[] GetFileNames(string searchPattern)
		{
			if (searchPattern == null)
			{
				throw new ArgumentNullException("searchPattern");
			}
			string directoryName = Path.GetDirectoryName(searchPattern);
			string fileName = Path.GetFileName(searchPattern);
			FileInfo[] array = null;
			if (directoryName == null || directoryName.Length == 0)
			{
				array = directory.GetFiles(searchPattern);
			}
			else
			{
				DirectoryInfo[] directories = directory.GetDirectories(directoryName);
				if (directories.Length != 1 || !(directories[0].Name == directoryName) || directories[0].FullName.IndexOf(directory.FullName) < 0)
				{
					throw new SecurityException();
				}
				array = directories[0].GetFiles(fileName);
			}
			return GetNames(array);
		}

		/// <summary>Removes the isolated storage scope and all its contents.</summary>
		/// <exception cref="T:System.IO.IsolatedStorage.IsolatedStorageException">The isolated store cannot be deleted. </exception>
		/// <PermissionSet>
		///   <IPermission class="System.Security.Permissions.SecurityPermission, mscorlib, Version=2.0.3600.0, Culture=neutral, PublicKeyToken=b77a5c561934e089" version="1" Flags="UnmanagedCode" />
		/// </PermissionSet>
		public override void Remove()
		{
			directory.Delete(recursive: true);
		}

		protected override IsolatedStoragePermission GetPermission(PermissionSet ps)
		{
			if (ps == null)
			{
				return null;
			}
			return (IsolatedStoragePermission)ps.GetPermission(typeof(IsolatedStorageFilePermission));
		}

		private string GetNameFromIdentity(object identity)
		{
			byte[] bytes = Encoding.UTF8.GetBytes(identity.ToString());
			SHA1 sHA = SHA1.Create();
			byte[] src = sHA.ComputeHash(bytes, 0, bytes.Length);
			byte[] array = new byte[10];
			Buffer.BlockCopy(src, 0, array, 0, array.Length);
			return CryptoConvert.ToHex(array);
		}

		private static object GetTypeFromEvidence(Evidence e, Type t)
		{
			foreach (object item in e)
			{
				if (item.GetType() == t)
				{
					return item;
				}
			}
			return null;
		}

		internal static object GetAssemblyIdentityFromEvidence(Evidence e)
		{
			object typeFromEvidence = GetTypeFromEvidence(e, typeof(Publisher));
			if (typeFromEvidence != null)
			{
				return typeFromEvidence;
			}
			typeFromEvidence = GetTypeFromEvidence(e, typeof(StrongName));
			if (typeFromEvidence != null)
			{
				return typeFromEvidence;
			}
			return GetTypeFromEvidence(e, typeof(Url));
		}

		internal static object GetDomainIdentityFromEvidence(Evidence e)
		{
			object typeFromEvidence = GetTypeFromEvidence(e, typeof(ApplicationDirectory));
			if (typeFromEvidence != null)
			{
				return typeFromEvidence;
			}
			return GetTypeFromEvidence(e, typeof(Url));
		}

		private void SaveIdentities(string root)
		{
			Identities identities = new Identities(_applicationIdentity, _assemblyIdentity, _domainIdentity);
			BinaryFormatter binaryFormatter = new BinaryFormatter();
			mutex.WaitOne();
			try
			{
				using (FileStream serializationStream = File.Create(root + ".storage"))
				{
					binaryFormatter.Serialize(serializationStream, identities);
				}
			}
			finally
			{
				mutex.ReleaseMutex();
			}
		}
	}
}
