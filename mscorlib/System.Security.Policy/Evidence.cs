using System.Collections;
using System.Reflection;
using System.Runtime.CompilerServices;
using System.Runtime.InteropServices;
using System.Security.Permissions;

namespace System.Security.Policy
{
	/// <summary>Defines the set of information that constitutes input to security policy decisions. This class cannot be inherited.</summary>
	[Serializable]
	[ComVisible(true)]
	[MonoTODO("Serialization format not compatible with .NET")]
	public sealed class Evidence : IEnumerable, ICollection
	{
		private class EvidenceEnumerator : IEnumerator
		{
			private IEnumerator currentEnum;

			private IEnumerator hostEnum;

			private IEnumerator assemblyEnum;

			public object Current => currentEnum.Current;

			public EvidenceEnumerator(IEnumerator hostenum, IEnumerator assemblyenum)
			{
				hostEnum = hostenum;
				assemblyEnum = assemblyenum;
				currentEnum = hostEnum;
			}

			public bool MoveNext()
			{
				if (currentEnum == null)
				{
					return false;
				}
				bool flag = currentEnum.MoveNext();
				if (!flag && hostEnum == currentEnum && assemblyEnum != null)
				{
					currentEnum = assemblyEnum;
					flag = assemblyEnum.MoveNext();
				}
				return flag;
			}

			public void Reset()
			{
				if (hostEnum != null)
				{
					hostEnum.Reset();
					currentEnum = hostEnum;
				}
				else
				{
					currentEnum = assemblyEnum;
				}
				if (assemblyEnum != null)
				{
					assemblyEnum.Reset();
				}
			}
		}

		private bool _locked;

		private ArrayList hostEvidenceList;

		private ArrayList assemblyEvidenceList;

		private int _hashCode;

		/// <summary>Gets the number of evidence objects in the evidence set.</summary>
		/// <returns>The number of evidence objects in the evidence set.</returns>
		public int Count
		{
			get
			{
				int num = 0;
				if (hostEvidenceList != null)
				{
					num += hostEvidenceList.Count;
				}
				if (assemblyEvidenceList != null)
				{
					num += assemblyEvidenceList.Count;
				}
				return num;
			}
		}

		/// <summary>Gets a value indicating whether the evidence set is read-only.</summary>
		/// <returns>Always false because read-only evidence sets are not supported.</returns>
		public bool IsReadOnly => false;

		/// <summary>Gets a value indicating whether the evidence set is thread-safe.</summary>
		/// <returns>Always false because thread-safe evidence sets are not supported.</returns>
		public bool IsSynchronized => false;

		/// <summary>Gets or sets a value indicating whether the evidence is locked.</summary>
		/// <returns>true if the evidence is locked; otherwise, false. The default is false.</returns>
		/// <PermissionSet>
		///   <IPermission class="System.Security.Permissions.SecurityPermission, mscorlib, Version=2.0.3600.0, Culture=neutral, PublicKeyToken=b77a5c561934e089" version="1" Flags="ControlEvidence" />
		/// </PermissionSet>
		public bool Locked
		{
			get
			{
				return _locked;
			}
			set
			{
				_locked = value;
			}
		}

		/// <summary>Gets the synchronization root.</summary>
		/// <returns>Always this (Me in Visual Basic) because synchronization of evidence sets is not supported.</returns>
		public object SyncRoot => this;

		internal ArrayList HostEvidenceList
		{
			get
			{
				if (hostEvidenceList == null)
				{
					hostEvidenceList = ArrayList.Synchronized(new ArrayList());
				}
				return hostEvidenceList;
			}
		}

		internal ArrayList AssemblyEvidenceList
		{
			get
			{
				if (assemblyEvidenceList == null)
				{
					assemblyEvidenceList = ArrayList.Synchronized(new ArrayList());
				}
				return assemblyEvidenceList;
			}
		}

		/// <summary>Initializes a new empty instance of the <see cref="T:System.Security.Policy.Evidence" /> class.</summary>
		public Evidence()
		{
		}

		/// <summary>Initializes a new instance of the <see cref="T:System.Security.Policy.Evidence" /> class from a shallow copy of an existing one.</summary>
		/// <param name="evidence">The <see cref="T:System.Security.Policy.Evidence" /> instance from which to create the new instance. This instance is not deep copied. </param>
		/// <exception cref="T:System.ArgumentException">The <paramref name="evidence" /> parameter is not a valid instance of <see cref="T:System.Security.Policy.Evidence" />. </exception>
		public Evidence(Evidence evidence)
		{
			if (evidence != null)
			{
				Merge(evidence);
			}
		}

		/// <summary>Initializes a new instance of the <see cref="T:System.Security.Policy.Evidence" /> class from multiple sets of host and assembly evidence.</summary>
		/// <param name="hostEvidence">The host evidence from which to create the new instance. </param>
		/// <param name="assemblyEvidence">The assembly evidence from which to create the new instance. </param>
		public Evidence(object[] hostEvidence, object[] assemblyEvidence)
		{
			if (hostEvidence != null)
			{
				HostEvidenceList.AddRange(hostEvidence);
			}
			if (assemblyEvidence != null)
			{
				AssemblyEvidenceList.AddRange(assemblyEvidence);
			}
		}

		/// <summary>Adds the specified assembly evidence to the evidence set.</summary>
		/// <param name="id">Any evidence object. </param>
		public void AddAssembly(object id)
		{
			AssemblyEvidenceList.Add(id);
			_hashCode = 0;
		}

		/// <summary>Adds the specified evidence supplied by the host to the evidence set.</summary>
		/// <param name="id">Any evidence object. </param>
		/// <exception cref="T:System.Security.SecurityException">
		///   <see cref="P:System.Security.Policy.Evidence.Locked" /> is true and the code that calls this method does not have <see cref="F:System.Security.Permissions.SecurityPermissionFlag.ControlEvidence" />. </exception>
		/// <PermissionSet>
		///   <IPermission class="System.Security.Permissions.SecurityPermission, mscorlib, Version=2.0.3600.0, Culture=neutral, PublicKeyToken=b77a5c561934e089" version="1" Flags="ControlEvidence" />
		/// </PermissionSet>
		public void AddHost(object id)
		{
			if (_locked && SecurityManager.SecurityEnabled)
			{
				new SecurityPermission(SecurityPermissionFlag.ControlEvidence).Demand();
			}
			HostEvidenceList.Add(id);
			_hashCode = 0;
		}

		/// <summary>Removes the host and assembly evidence from the evidence set.</summary>
		[ComVisible(false)]
		public void Clear()
		{
			if (hostEvidenceList != null)
			{
				hostEvidenceList.Clear();
			}
			if (assemblyEvidenceList != null)
			{
				assemblyEvidenceList.Clear();
			}
			_hashCode = 0;
		}

		/// <summary>Copies evidence objects to an <see cref="T:System.Array" />.</summary>
		/// <param name="array">The target array to which to copy evidence objects. </param>
		/// <param name="index">The zero-based position in the array to which to begin copying evidence objects. </param>
		public void CopyTo(Array array, int index)
		{
			int num = 0;
			if (hostEvidenceList != null)
			{
				num = hostEvidenceList.Count;
				if (num > 0)
				{
					hostEvidenceList.CopyTo(array, index);
				}
			}
			if (assemblyEvidenceList != null && assemblyEvidenceList.Count > 0)
			{
				assemblyEvidenceList.CopyTo(array, index + num);
			}
		}

		/// <summary>Determines whether the specified <see cref="T:System.Security.Policy.Evidence" /> object is equal to the current <see cref="T:System.Security.Policy.Evidence" />.</summary>
		/// <returns>true if the specified <see cref="T:System.Security.Policy.Evidence" /> object is equal to the current <see cref="T:System.Security.Policy.Evidence" />; otherwise, false.</returns>
		/// <param name="obj">The <see cref="T:System.Security.Policy.Evidence" /> object to compare with the current <see cref="T:System.Security.Policy.Evidence" />. </param>
		[ComVisible(false)]
		public override bool Equals(object obj)
		{
			if (obj == null)
			{
				return false;
			}
			Evidence evidence = obj as Evidence;
			if (evidence == null)
			{
				return false;
			}
			if (HostEvidenceList.Count != evidence.HostEvidenceList.Count)
			{
				return false;
			}
			if (AssemblyEvidenceList.Count != evidence.AssemblyEvidenceList.Count)
			{
				return false;
			}
			for (int i = 0; i < hostEvidenceList.Count; i++)
			{
				bool flag = false;
				int num = 0;
				while (num < evidence.hostEvidenceList.Count)
				{
					if (hostEvidenceList[i].Equals(evidence.hostEvidenceList[num]))
					{
						flag = true;
						break;
					}
					i++;
				}
				if (!flag)
				{
					return false;
				}
			}
			for (int j = 0; j < assemblyEvidenceList.Count; j++)
			{
				bool flag2 = false;
				int num2 = 0;
				while (num2 < evidence.assemblyEvidenceList.Count)
				{
					if (assemblyEvidenceList[j].Equals(evidence.assemblyEvidenceList[num2]))
					{
						flag2 = true;
						break;
					}
					j++;
				}
				if (!flag2)
				{
					return false;
				}
			}
			return true;
		}

		/// <summary>Enumerates all evidence in the set, both that provided by the host and that provided by the assembly.</summary>
		/// <returns>An enumerator for evidence added by both the <see cref="M:System.Security.Policy.Evidence.AddHost(System.Object)" /> method and the <see cref="M:System.Security.Policy.Evidence.AddAssembly(System.Object)" /> method.</returns>
		public IEnumerator GetEnumerator()
		{
			IEnumerator hostenum = null;
			if (hostEvidenceList != null)
			{
				hostenum = hostEvidenceList.GetEnumerator();
			}
			IEnumerator assemblyenum = null;
			if (assemblyEvidenceList != null)
			{
				assemblyenum = assemblyEvidenceList.GetEnumerator();
			}
			return new EvidenceEnumerator(hostenum, assemblyenum);
		}

		/// <summary>Enumerates evidence provided by the assembly.</summary>
		/// <returns>An enumerator for evidence added by the <see cref="M:System.Security.Policy.Evidence.AddAssembly(System.Object)" /> method.</returns>
		public IEnumerator GetAssemblyEnumerator()
		{
			return AssemblyEvidenceList.GetEnumerator();
		}

		/// <summary>Gets a hash code for the <see cref="T:System.Security.Policy.Evidence" /> object that is suitable for use in hashing algorithms and data structures such as a hash table.</summary>
		/// <returns>A hash code for the current <see cref="T:System.Security.Policy.Evidence" /> object.</returns>
		[ComVisible(false)]
		public override int GetHashCode()
		{
			if (_hashCode == 0)
			{
				if (hostEvidenceList != null)
				{
					for (int i = 0; i < hostEvidenceList.Count; i++)
					{
						_hashCode ^= hostEvidenceList[i].GetHashCode();
					}
				}
				if (assemblyEvidenceList != null)
				{
					for (int j = 0; j < assemblyEvidenceList.Count; j++)
					{
						_hashCode ^= assemblyEvidenceList[j].GetHashCode();
					}
				}
			}
			return _hashCode;
		}

		/// <summary>Enumerates evidence supplied by the host.</summary>
		/// <returns>An enumerator for evidence added by the <see cref="M:System.Security.Policy.Evidence.AddHost(System.Object)" /> method.</returns>
		public IEnumerator GetHostEnumerator()
		{
			return HostEvidenceList.GetEnumerator();
		}

		/// <summary>Merges the specified evidence set into the current evidence set.</summary>
		/// <param name="evidence">The evidence set to be merged into the current evidence set. </param>
		/// <exception cref="T:System.ArgumentException">The <paramref name="evidence" /> parameter is not a valid instance of <see cref="T:System.Security.Policy.Evidence" />. </exception>
		/// <exception cref="T:System.Security.SecurityException">
		///   <see cref="P:System.Security.Policy.Evidence.Locked" /> is true, the code that calls this method does not have <see cref="F:System.Security.Permissions.SecurityPermissionFlag.ControlEvidence" />, and the <paramref name="evidence" /> parameter has a host list that is not empty. </exception>
		/// <PermissionSet>
		///   <IPermission class="System.Security.Permissions.SecurityPermission, mscorlib, Version=2.0.3600.0, Culture=neutral, PublicKeyToken=b77a5c561934e089" version="1" Flags="ControlEvidence" />
		/// </PermissionSet>
		public void Merge(Evidence evidence)
		{
			if (evidence != null && evidence.Count > 0)
			{
				if (evidence.hostEvidenceList != null)
				{
					foreach (object hostEvidence in evidence.hostEvidenceList)
					{
						AddHost(hostEvidence);
					}
				}
				if (evidence.assemblyEvidenceList != null)
				{
					foreach (object assemblyEvidence in evidence.assemblyEvidenceList)
					{
						AddAssembly(assemblyEvidence);
					}
				}
				_hashCode = 0;
			}
		}

		/// <summary>Removes the evidence for a given type from the host and assembly enumerations.</summary>
		/// <param name="t">The <see cref="T:System.Type" /> of the evidence to be removed. </param>
		[ComVisible(false)]
		public void RemoveType(Type t)
		{
			for (int num = hostEvidenceList.Count; num >= 0; num--)
			{
				if (hostEvidenceList.GetType() == t)
				{
					hostEvidenceList.RemoveAt(num);
					_hashCode = 0;
				}
			}
			for (int num2 = assemblyEvidenceList.Count; num2 >= 0; num2--)
			{
				if (assemblyEvidenceList.GetType() == t)
				{
					assemblyEvidenceList.RemoveAt(num2);
					_hashCode = 0;
				}
			}
		}

		[MethodImpl(MethodImplOptions.InternalCall)]
		private static extern bool IsAuthenticodePresent(Assembly a);

		internal static Evidence GetDefaultHostEvidence(Assembly a)
		{
			return new Evidence();
		}
	}
}
