using System.Runtime.InteropServices;

namespace System.Reflection.Emit
{
	/// <summary>Defines the access modes for a dynamic assembly.</summary>
	[Serializable]
	[ComVisible(true)]
	[Flags]
	public enum AssemblyBuilderAccess
	{
		/// <summary>Represents that the dynamic assembly can be executed, but not saved.</summary>
		Run = 0x1,
		/// <summary>Represents that the dynamic assembly can be saved, but not executed.</summary>
		Save = 0x2,
		/// <summary>Represents that the dynamic assembly can be executed and saved.</summary>
		RunAndSave = 0x3,
		/// <summary>Represents that the dynamic assembly is loaded into the reflection-only context, and cannot be executed.</summary>
		ReflectionOnly = 0x6
	}
}
