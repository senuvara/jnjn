using Mono.Security;
using System.Collections;
using System.Globalization;
using System.IO;
using System.Resources;
using System.Runtime.CompilerServices;
using System.Runtime.InteropServices;
using System.Security;

namespace System.Reflection.Emit
{
	/// <summary>Defines and represents a dynamic assembly.</summary>
	[ClassInterface(ClassInterfaceType.None)]
	[ComVisible(true)]
	[ComDefaultInterface(typeof(_AssemblyBuilder))]
	public sealed class AssemblyBuilder : Assembly, _AssemblyBuilder
	{
		private const AssemblyBuilderAccess COMPILER_ACCESS = (AssemblyBuilderAccess)2048;

		private UIntPtr dynamic_assembly;

		private MethodInfo entry_point;

		private ModuleBuilder[] modules;

		private string name;

		private string dir;

		private CustomAttributeBuilder[] cattrs;

		private MonoResource[] resources;

		private byte[] public_key;

		private string version;

		private string culture;

		private uint algid;

		private uint flags;

		private PEFileKinds pekind = PEFileKinds.Dll;

		private bool delay_sign;

		private uint access;

		private Module[] loaded_modules;

		private MonoWin32Resource[] win32_resources;

		private RefEmitPermissionSet[] permissions_minimum;

		private RefEmitPermissionSet[] permissions_optional;

		private RefEmitPermissionSet[] permissions_refused;

		private PortableExecutableKinds peKind;

		private ImageFileMachine machine;

		private bool corlib_internal;

		private Type[] type_forwarders;

		private byte[] pktoken;

		internal Type corlib_object_type = typeof(object);

		internal Type corlib_value_type = typeof(ValueType);

		internal Type corlib_enum_type = typeof(Enum);

		internal Type corlib_void_type = typeof(void);

		private ArrayList resource_writers;

		private Win32VersionResource version_res;

		private bool created;

		private bool is_module_only;

		private StrongName sn;

		private NativeResourceType native_resource;

		private readonly bool is_compiler_context;

		private string versioninfo_culture;

		private ModuleBuilder manifest_module;

		/// <summary>Gets the location of the assembly, as specified originally (such as in an <see cref="T:System.Reflection.AssemblyName" /> object).</summary>
		/// <returns>The location of the assembly, as specified originally.</returns>
		/// <exception cref="T:System.NotSupportedException">This method is not currently supported. </exception>
		/// <exception cref="T:System.Security.SecurityException">The caller does not have the required permission. </exception>
		public override string CodeBase
		{
			get
			{
				throw not_supported();
			}
		}

		/// <summary>Returns the entry point of this assembly.</summary>
		/// <returns>The entry point of this assembly.</returns>
		/// <exception cref="T:System.Security.SecurityException">The caller does not have the required permission. </exception>
		public override MethodInfo EntryPoint => entry_point;

		/// <summary>Gets the location, in codebase format, of the loaded file that contains the manifest if it is not shadow-copied.</summary>
		/// <returns>The location of the loaded file that contains the manifest. If the loaded file has been shadow-copied, the Location is that of the file before being shadow-copied.</returns>
		/// <exception cref="T:System.NotSupportedException">This method is not currently supported. </exception>
		/// <exception cref="T:System.Security.SecurityException">The caller does not have the required permission. </exception>
		public override string Location
		{
			get
			{
				throw not_supported();
			}
		}

		/// <summary>Gets the version of the common language runtime that will be saved in the file containing the manifest.</summary>
		/// <returns>A string representing the common language runtime version.</returns>
		/// <exception cref="T:System.Security.SecurityException">The caller does not have the required permission. </exception>
		/// <PermissionSet>
		///   <IPermission class="System.Security.Permissions.SecurityPermission, mscorlib, Version=2.0.3600.0, Culture=neutral, PublicKeyToken=b77a5c561934e089" version="1" Flags="UnmanagedCode" />
		/// </PermissionSet>
		public override string ImageRuntimeVersion => base.ImageRuntimeVersion;

		/// <summary>Gets a value indicating whether the dynamic assembly is in the reflection-only context.</summary>
		/// <returns>true if the dynamic assembly is in the reflection-only context; otherwise, false.</returns>
		[MonoTODO]
		public override bool ReflectionOnly => base.ReflectionOnly;

		internal bool IsCompilerContext => is_compiler_context;

		internal bool IsSave => access != 1;

		internal bool IsRun => access == 1 || access == 3;

		internal string AssemblyDir => dir;

		internal bool IsModuleOnly
		{
			get
			{
				return is_module_only;
			}
			set
			{
				is_module_only = value;
			}
		}

		internal AssemblyBuilder(AssemblyName n, string directory, AssemblyBuilderAccess access, bool corlib_internal)
		{
			is_compiler_context = ((access & (AssemblyBuilderAccess)2048) != 0);
			access &= (AssemblyBuilderAccess)(-2049);
			if (!Enum.IsDefined(typeof(AssemblyBuilderAccess), access))
			{
				throw new ArgumentException(string.Format(CultureInfo.InvariantCulture, "Argument value {0} is not valid.", (int)access), "access");
			}
			name = n.Name;
			this.access = (uint)access;
			flags = (uint)n.Flags;
			if (IsSave && (directory == null || directory.Length == 0))
			{
				dir = Directory.GetCurrentDirectory();
			}
			else
			{
				dir = directory;
			}
			if (n.CultureInfo != null)
			{
				culture = n.CultureInfo.Name;
				versioninfo_culture = n.CultureInfo.Name;
			}
			Version version = n.Version;
			if (version != null)
			{
				this.version = version.ToString();
			}
			if (n.KeyPair != null)
			{
				sn = n.KeyPair.StrongName();
			}
			else
			{
				byte[] publicKey = n.GetPublicKey();
				if (publicKey != null && publicKey.Length > 0)
				{
					sn = new StrongName(publicKey);
				}
			}
			if (sn != null)
			{
				flags |= 1u;
			}
			this.corlib_internal = corlib_internal;
			if (sn != null)
			{
				pktoken = new byte[sn.PublicKeyToken.Length * 2];
				int num = 0;
				byte[] publicKeyToken = sn.PublicKeyToken;
				foreach (byte b in publicKeyToken)
				{
					string text = b.ToString("x2");
					pktoken[num++] = (byte)text[0];
					pktoken[num++] = (byte)text[1];
				}
			}
			basic_init(this);
		}

		/// <summary>Maps a set of names to a corresponding set of dispatch identifiers.</summary>
		/// <param name="riid">Reserved for future use. Must be IID_NULL.</param>
		/// <param name="rgszNames">Passed-in array of names to be mapped.</param>
		/// <param name="cNames">Count of the names to be mapped.</param>
		/// <param name="lcid">The locale context in which to interpret the names.</param>
		/// <param name="rgDispId">Caller-allocated array which receives the IDs corresponding to the names.</param>
		/// <exception cref="T:System.NotImplementedException">The method is called late-bound using the COM IDispatch interface.</exception>
		void _AssemblyBuilder.GetIDsOfNames([In] ref Guid riid, IntPtr rgszNames, uint cNames, uint lcid, IntPtr rgDispId)
		{
			throw new NotImplementedException();
		}

		/// <summary>Retrieves the type information for an object, which can then be used to get the type information for an interface.</summary>
		/// <param name="iTInfo">The type information to return.</param>
		/// <param name="lcid">The locale identifier for the type information.</param>
		/// <param name="ppTInfo">Receives a pointer to the requested type information object.</param>
		/// <exception cref="T:System.NotImplementedException">The method is called late-bound using the COM IDispatch interface.</exception>
		void _AssemblyBuilder.GetTypeInfo(uint iTInfo, uint lcid, IntPtr ppTInfo)
		{
			throw new NotImplementedException();
		}

		/// <summary>Retrieves the number of type information interfaces that an object provides (either 0 or 1).</summary>
		/// <param name="pcTInfo">Points to a location that receives the number of type information interfaces provided by the object.</param>
		/// <exception cref="T:System.NotImplementedException">The method is called late-bound using the COM IDispatch interface.</exception>
		void _AssemblyBuilder.GetTypeInfoCount(out uint pcTInfo)
		{
			throw new NotImplementedException();
		}

		/// <summary>Provides access to properties and methods exposed by an object.</summary>
		/// <param name="dispIdMember">Identifies the member.</param>
		/// <param name="riid">Reserved for future use. Must be IID_NULL.</param>
		/// <param name="lcid">The locale context in which to interpret arguments.</param>
		/// <param name="wFlags">Flags describing the context of the call.</param>
		/// <param name="pDispParams">Pointer to a structure containing an array of arguments, an array of argument DISPIDs for named arguments, and counts for the number of elements in the arrays.</param>
		/// <param name="pVarResult">Pointer to the location where the result is to be stored.</param>
		/// <param name="pExcepInfo">Pointer to a structure that contains exception information.</param>
		/// <param name="puArgErr">The index of the first argument that has an error.</param>
		/// <exception cref="T:System.NotImplementedException">The method is called late-bound using the COM IDispatch interface.</exception>
		void _AssemblyBuilder.Invoke(uint dispIdMember, [In] ref Guid riid, uint lcid, short wFlags, IntPtr pDispParams, IntPtr pVarResult, IntPtr pExcepInfo, IntPtr puArgErr)
		{
			throw new NotImplementedException();
		}

		[MethodImpl(MethodImplOptions.InternalCall)]
		private static extern void basic_init(AssemblyBuilder ab);

		/// <summary>Adds an existing resource file to this assembly.</summary>
		/// <param name="name">The logical name of the resource. </param>
		/// <param name="fileName">The physical file name (.resources file) to which the logical name is mapped. This should not include a path; the file must be in the same directory as the assembly to which it is added. </param>
		/// <exception cref="T:System.ArgumentException">
		///   <paramref name="name" /> has been previously defined.-or- There is another file in the assembly named <paramref name="fileName" />.-or- The length of <paramref name="name" /> is zero.-or- The length of <paramref name="fileName" /> is zero, or if <paramref name="fileName" /> includes a path. </exception>
		/// <exception cref="T:System.ArgumentNullException">
		///   <paramref name="name" /> or <paramref name="fileName" /> is null. </exception>
		/// <exception cref="T:System.IO.FileNotFoundException">The file <paramref name="fileName" /> is not found. </exception>
		/// <exception cref="T:System.Security.SecurityException">The caller does not have the required permission. </exception>
		/// <PermissionSet>
		///   <IPermission class="System.Security.Permissions.FileIOPermission, mscorlib, Version=2.0.3600.0, Culture=neutral, PublicKeyToken=b77a5c561934e089" version="1" Unrestricted="true" />
		/// </PermissionSet>
		public void AddResourceFile(string name, string fileName)
		{
			AddResourceFile(name, fileName, ResourceAttributes.Public);
		}

		/// <summary>Adds an existing resource file to this assembly.</summary>
		/// <param name="name">The logical name of the resource. </param>
		/// <param name="fileName">The physical file name (.resources file) to which the logical name is mapped. This should not include a path; the file must be in the same directory as the assembly to which it is added. </param>
		/// <param name="attribute">The resource attributes. </param>
		/// <exception cref="T:System.ArgumentException">
		///   <paramref name="name" /> has been previously defined.-or- There is another file in the assembly named <paramref name="fileName" />.-or- The length of <paramref name="name" /> is zero or if the length of <paramref name="fileName" /> is zero.-or- <paramref name="fileName" /> includes a path. </exception>
		/// <exception cref="T:System.ArgumentNullException">
		///   <paramref name="name" /> or <paramref name="fileName" /> is null. </exception>
		/// <exception cref="T:System.IO.FileNotFoundException">If the file <paramref name="fileName" /> is not found. </exception>
		/// <exception cref="T:System.Security.SecurityException">The caller does not have the required permission. </exception>
		/// <PermissionSet>
		///   <IPermission class="System.Security.Permissions.FileIOPermission, mscorlib, Version=2.0.3600.0, Culture=neutral, PublicKeyToken=b77a5c561934e089" version="1" Unrestricted="true" />
		/// </PermissionSet>
		public void AddResourceFile(string name, string fileName, ResourceAttributes attribute)
		{
			AddResourceFile(name, fileName, attribute, fileNeedsToExists: true);
		}

		private void AddResourceFile(string name, string fileName, ResourceAttributes attribute, bool fileNeedsToExists)
		{
			check_name_and_filename(name, fileName, fileNeedsToExists);
			if (dir != null)
			{
				fileName = Path.Combine(dir, fileName);
			}
			if (resources != null)
			{
				MonoResource[] destinationArray = new MonoResource[resources.Length + 1];
				Array.Copy(resources, destinationArray, resources.Length);
				resources = destinationArray;
			}
			else
			{
				resources = new MonoResource[1];
			}
			int num = resources.Length - 1;
			resources[num].name = name;
			resources[num].filename = fileName;
			resources[num].attrs = attribute;
		}

		internal void AddPermissionRequests(PermissionSet required, PermissionSet optional, PermissionSet refused)
		{
		}

		internal void EmbedResourceFile(string name, string fileName)
		{
			EmbedResourceFile(name, fileName, ResourceAttributes.Public);
		}

		internal void EmbedResourceFile(string name, string fileName, ResourceAttributes attribute)
		{
			if (resources != null)
			{
				MonoResource[] destinationArray = new MonoResource[resources.Length + 1];
				Array.Copy(resources, destinationArray, resources.Length);
				resources = destinationArray;
			}
			else
			{
				resources = new MonoResource[1];
			}
			int num = resources.Length - 1;
			resources[num].name = name;
			resources[num].attrs = attribute;
			try
			{
				FileStream fileStream = new FileStream(fileName, FileMode.Open, FileAccess.Read);
				long length = fileStream.Length;
				resources[num].data = new byte[length];
				fileStream.Read(resources[num].data, 0, (int)length);
				fileStream.Close();
			}
			catch
			{
			}
		}

		internal void EmbedResource(string name, byte[] blob, ResourceAttributes attribute)
		{
			if (resources != null)
			{
				MonoResource[] destinationArray = new MonoResource[resources.Length + 1];
				Array.Copy(resources, destinationArray, resources.Length);
				resources = destinationArray;
			}
			else
			{
				resources = new MonoResource[1];
			}
			int num = resources.Length - 1;
			resources[num].name = name;
			resources[num].attrs = attribute;
			resources[num].data = blob;
		}

		internal void AddTypeForwarder(Type t)
		{
			if (t == null)
			{
				throw new ArgumentNullException("t");
			}
			if (type_forwarders == null)
			{
				type_forwarders = new Type[1]
				{
					t
				};
				return;
			}
			Type[] array = new Type[type_forwarders.Length + 1];
			Array.Copy(type_forwarders, array, type_forwarders.Length);
			array[type_forwarders.Length] = t;
			type_forwarders = array;
		}

		/// <summary>Defines a named transient dynamic module in this assembly.</summary>
		/// <returns>A <see cref="T:System.Reflection.Emit.ModuleBuilder" /> representing the defined dynamic module.</returns>
		/// <param name="name">The name of the dynamic module. Must be less than 260 characters in length. </param>
		/// <exception cref="T:System.ArgumentException">
		///   <paramref name="name" /> begins with white space.-or- The length of <paramref name="name" /> is zero.-or- The length of <paramref name="name" /> is greater than or equal to 260. </exception>
		/// <exception cref="T:System.ArgumentNullException">
		///   <paramref name="name" /> is null. </exception>
		/// <exception cref="T:System.Security.SecurityException">The caller does not have the required permission. </exception>
		/// <exception cref="T:System.ExecutionEngineException">The assembly for default symbol writer cannot be loaded.-or- The type that implements the default symbol writer interface cannot be found. </exception>
		/// <PermissionSet>
		///   <IPermission class="System.Security.Permissions.FileIOPermission, mscorlib, Version=2.0.3600.0, Culture=neutral, PublicKeyToken=b77a5c561934e089" version="1" Read="*AllFiles*" PathDiscovery="*AllFiles*" />
		///   <IPermission class="System.Security.Permissions.ReflectionPermission, mscorlib, Version=2.0.3600.0, Culture=neutral, PublicKeyToken=b77a5c561934e089" version="1" Flags="MemberAccess" />
		/// </PermissionSet>
		public ModuleBuilder DefineDynamicModule(string name)
		{
			return DefineDynamicModule(name, name, emitSymbolInfo: false, transient: true);
		}

		/// <summary>Defines a named transient dynamic module in this assembly and specifies whether symbol information should be emitted.</summary>
		/// <returns>A <see cref="T:System.Reflection.Emit.ModuleBuilder" /> representing the defined dynamic module.</returns>
		/// <param name="name">The name of the dynamic module. Must be less than 260 characters in length. </param>
		/// <param name="emitSymbolInfo">true if symbol information is to be emitted; otherwise, false. </param>
		/// <exception cref="T:System.ArgumentException">
		///   <paramref name="name" /> begins with white space.-or- The length of <paramref name="name" /> is zero.-or- The length of <paramref name="name" /> is greater than or equal to 260. </exception>
		/// <exception cref="T:System.ArgumentNullException">
		///   <paramref name="name" /> is null. </exception>
		/// <exception cref="T:System.ExecutionEngineException">The assembly for default symbol writer cannot be loaded.-or- The type that implements the default symbol writer interface cannot be found. </exception>
		/// <exception cref="T:System.Security.SecurityException">The caller does not have the required permission. </exception>
		/// <PermissionSet>
		///   <IPermission class="System.Security.Permissions.FileIOPermission, mscorlib, Version=2.0.3600.0, Culture=neutral, PublicKeyToken=b77a5c561934e089" version="1" Read="*AllFiles*" PathDiscovery="*AllFiles*" />
		///   <IPermission class="System.Security.Permissions.ReflectionPermission, mscorlib, Version=2.0.3600.0, Culture=neutral, PublicKeyToken=b77a5c561934e089" version="1" Flags="MemberAccess" />
		/// </PermissionSet>
		public ModuleBuilder DefineDynamicModule(string name, bool emitSymbolInfo)
		{
			return DefineDynamicModule(name, name, emitSymbolInfo, transient: true);
		}

		/// <summary>Defines a persistable dynamic module with the given name that will be saved to the specified file. No symbol information is emitted.</summary>
		/// <returns>A <see cref="T:System.Reflection.Emit.ModuleBuilder" /> object representing the defined dynamic module.</returns>
		/// <param name="name">The name of the dynamic module. Must be less than 260 characters in length. </param>
		/// <param name="fileName">The name of the file to which the dynamic module should be saved. </param>
		/// <exception cref="T:System.ArgumentNullException">
		///   <paramref name="name" /> or <paramref name="fileName" /> is null. </exception>
		/// <exception cref="T:System.ArgumentException">The length of <paramref name="name" /> or <paramref name="fileName" /> is zero.-or- The length of <paramref name="name" /> is greater than or equal to 260.-or- <paramref name="fileName" /> contains a path specification (a directory component, for example).-or- There is a conflict with the name of another file that belongs to this assembly. </exception>
		/// <exception cref="T:System.InvalidOperationException">This assembly has been previously saved. </exception>
		/// <exception cref="T:System.NotSupportedException">This assembly was called on a dynamic assembly with <see cref="F:System.Reflection.Emit.AssemblyBuilderAccess.Run" /> attribute. </exception>
		/// <exception cref="T:System.Security.SecurityException">The caller does not have the required permission. </exception>
		/// <exception cref="T:System.ExecutionEngineException">The assembly for default symbol writer cannot be loaded.-or- The type that implements the default symbol writer interface cannot be found. </exception>
		/// <PermissionSet>
		///   <IPermission class="System.Security.Permissions.FileIOPermission, mscorlib, Version=2.0.3600.0, Culture=neutral, PublicKeyToken=b77a5c561934e089" version="1" Read="*AllFiles*" PathDiscovery="*AllFiles*" />
		///   <IPermission class="System.Security.Permissions.ReflectionPermission, mscorlib, Version=2.0.3600.0, Culture=neutral, PublicKeyToken=b77a5c561934e089" version="1" Flags="MemberAccess" />
		/// </PermissionSet>
		public ModuleBuilder DefineDynamicModule(string name, string fileName)
		{
			return DefineDynamicModule(name, fileName, emitSymbolInfo: false, transient: false);
		}

		/// <summary>Defines a persistable dynamic module, specifying the module name, the name of the file to which the module will be saved, and whether symbol information should be emitted using the default symbol writer.</summary>
		/// <returns>A <see cref="T:System.Reflection.Emit.ModuleBuilder" /> object representing the defined dynamic module.</returns>
		/// <param name="name">The name of the dynamic module. Must be less than 260 characters in length. </param>
		/// <param name="fileName">The name of the file to which the dynamic module should be saved. </param>
		/// <param name="emitSymbolInfo">If true, symbolic information is written using the default symbol writer. </param>
		/// <exception cref="T:System.ArgumentNullException">
		///   <paramref name="name" /> or <paramref name="fileName" /> is null. </exception>
		/// <exception cref="T:System.ArgumentException">The length of <paramref name="name" /> or <paramref name="fileName" /> is zero.-or- The length of <paramref name="name" /> is greater than or equal to 260.-or- <paramref name="fileName" /> contains a path specification (a directory component, for example).-or- There is a conflict with the name of another file that belongs to this assembly. </exception>
		/// <exception cref="T:System.InvalidOperationException">This assembly has been previously saved. </exception>
		/// <exception cref="T:System.NotSupportedException">This assembly was called on a dynamic assembly with the <see cref="F:System.Reflection.Emit.AssemblyBuilderAccess.Run" /> attribute. </exception>
		/// <exception cref="T:System.ExecutionEngineException">The assembly for default symbol writer cannot be loaded.-or- The type that implements the default symbol writer interface cannot be found. </exception>
		/// <exception cref="T:System.Security.SecurityException">The caller does not have the required permission. </exception>
		/// <PermissionSet>
		///   <IPermission class="System.Security.Permissions.FileIOPermission, mscorlib, Version=2.0.3600.0, Culture=neutral, PublicKeyToken=b77a5c561934e089" version="1" Read="*AllFiles*" PathDiscovery="*AllFiles*" />
		///   <IPermission class="System.Security.Permissions.ReflectionPermission, mscorlib, Version=2.0.3600.0, Culture=neutral, PublicKeyToken=b77a5c561934e089" version="1" Flags="MemberAccess" />
		/// </PermissionSet>
		public ModuleBuilder DefineDynamicModule(string name, string fileName, bool emitSymbolInfo)
		{
			return DefineDynamicModule(name, fileName, emitSymbolInfo, transient: false);
		}

		private ModuleBuilder DefineDynamicModule(string name, string fileName, bool emitSymbolInfo, bool transient)
		{
			check_name_and_filename(name, fileName, fileNeedsToExists: false);
			if (!transient)
			{
				if (Path.GetExtension(fileName) == string.Empty)
				{
					throw new ArgumentException("Module file name '" + fileName + "' must have file extension.");
				}
				if (!IsSave)
				{
					throw new NotSupportedException("Persistable modules are not supported in a dynamic assembly created with AssemblyBuilderAccess.Run");
				}
				if (created)
				{
					throw new InvalidOperationException("Assembly was already saved.");
				}
			}
			ModuleBuilder moduleBuilder = new ModuleBuilder(this, name, fileName, emitSymbolInfo, transient);
			if (modules != null && is_module_only)
			{
				throw new InvalidOperationException("A module-only assembly can only contain one module.");
			}
			if (modules != null)
			{
				ModuleBuilder[] destinationArray = new ModuleBuilder[modules.Length + 1];
				Array.Copy(modules, destinationArray, modules.Length);
				modules = destinationArray;
			}
			else
			{
				modules = new ModuleBuilder[1];
			}
			modules[modules.Length - 1] = moduleBuilder;
			return moduleBuilder;
		}

		[MethodImpl(MethodImplOptions.InternalCall)]
		private extern Module InternalAddModule(string fileName);

		internal Module AddModule(string fileName)
		{
			if (fileName == null)
			{
				throw new ArgumentNullException(fileName);
			}
			Module module = InternalAddModule(fileName);
			if (loaded_modules != null)
			{
				Module[] destinationArray = new Module[loaded_modules.Length + 1];
				Array.Copy(loaded_modules, destinationArray, loaded_modules.Length);
				loaded_modules = destinationArray;
			}
			else
			{
				loaded_modules = new Module[1];
			}
			loaded_modules[loaded_modules.Length - 1] = module;
			return module;
		}

		/// <summary>Defines a standalone managed resource for this assembly with the default public resource attribute.</summary>
		/// <returns>A <see cref="T:System.Resources.ResourceWriter" /> object for the specified resource.</returns>
		/// <param name="name">The logical name of the resource. </param>
		/// <param name="description">A textual description of the resource. </param>
		/// <param name="fileName">The physical file name (.resources file) to which the logical name is mapped. This should not include a path. </param>
		/// <exception cref="T:System.ArgumentException">
		///   <paramref name="name" /> has been previously defined.-or- There is another file in the assembly named <paramref name="fileName" />.-or- The length of <paramref name="name" /> is zero.-or- The length of <paramref name="fileName" /> is zero.-or- <paramref name="fileName" /> includes a path. </exception>
		/// <exception cref="T:System.ArgumentNullException">
		///   <paramref name="name" /> or <paramref name="fileName" /> is null. </exception>
		/// <exception cref="T:System.Security.SecurityException">The caller does not have the required permission. </exception>
		/// <PermissionSet>
		///   <IPermission class="System.Security.Permissions.FileIOPermission, mscorlib, Version=2.0.3600.0, Culture=neutral, PublicKeyToken=b77a5c561934e089" version="1" Unrestricted="true" />
		/// </PermissionSet>
		public IResourceWriter DefineResource(string name, string description, string fileName)
		{
			return DefineResource(name, description, fileName, ResourceAttributes.Public);
		}

		/// <summary>Defines a standalone managed resource for this assembly. Attributes can be specified for the managed resource.</summary>
		/// <returns>A <see cref="T:System.Resources.ResourceWriter" /> object for the specified resource.</returns>
		/// <param name="name">The logical name of the resource. </param>
		/// <param name="description">A textual description of the resource. </param>
		/// <param name="fileName">The physical file name (.resources file) to which the logical name is mapped. This should not include a path. </param>
		/// <param name="attribute">The resource attributes. </param>
		/// <exception cref="T:System.ArgumentException">
		///   <paramref name="name" /> has been previously defined or if there is another file in the assembly named <paramref name="fileName" />.-or- The length of <paramref name="name" /> is zero.-or- The length of <paramref name="fileName" /> is zero.-or- <paramref name="fileName" /> includes a path. </exception>
		/// <exception cref="T:System.ArgumentNullException">
		///   <paramref name="name" /> or <paramref name="fileName" /> is null. </exception>
		/// <exception cref="T:System.Security.SecurityException">The caller does not have the required permission. </exception>
		/// <PermissionSet>
		///   <IPermission class="System.Security.Permissions.FileIOPermission, mscorlib, Version=2.0.3600.0, Culture=neutral, PublicKeyToken=b77a5c561934e089" version="1" Unrestricted="true" />
		/// </PermissionSet>
		public IResourceWriter DefineResource(string name, string description, string fileName, ResourceAttributes attribute)
		{
			AddResourceFile(name, fileName, attribute, fileNeedsToExists: false);
			IResourceWriter resourceWriter = new ResourceWriter(fileName);
			if (resource_writers == null)
			{
				resource_writers = new ArrayList();
			}
			resource_writers.Add(resourceWriter);
			return resourceWriter;
		}

		private void AddUnmanagedResource(Win32Resource res)
		{
			MemoryStream memoryStream = new MemoryStream();
			res.WriteTo(memoryStream);
			if (win32_resources != null)
			{
				MonoWin32Resource[] destinationArray = new MonoWin32Resource[win32_resources.Length + 1];
				Array.Copy(win32_resources, destinationArray, win32_resources.Length);
				win32_resources = destinationArray;
			}
			else
			{
				win32_resources = new MonoWin32Resource[1];
			}
			win32_resources[win32_resources.Length - 1] = new MonoWin32Resource(res.Type.Id, res.Name.Id, res.Language, memoryStream.ToArray());
		}

		/// <summary>Defines an unmanaged resource for this assembly as an opaque blob of bytes.</summary>
		/// <param name="resource">The opaque blob of bytes representing the unmanaged resource. </param>
		/// <exception cref="T:System.ArgumentException">An unmanaged resource was previously defined. </exception>
		/// <exception cref="T:System.ArgumentNullException">
		///   <paramref name="resource" /> is null. </exception>
		/// <exception cref="T:System.Security.SecurityException">The caller does not have the required permission. </exception>
		/// <PermissionSet>
		///   <IPermission class="System.Security.Permissions.FileIOPermission, mscorlib, Version=2.0.3600.0, Culture=neutral, PublicKeyToken=b77a5c561934e089" version="1" Unrestricted="true" />
		/// </PermissionSet>
		[MonoTODO("Not currently implemenented")]
		public void DefineUnmanagedResource(byte[] resource)
		{
			if (resource == null)
			{
				throw new ArgumentNullException("resource");
			}
			if (native_resource != 0)
			{
				throw new ArgumentException("Native resource has already been defined.");
			}
			native_resource = NativeResourceType.Unmanaged;
			throw new NotImplementedException();
		}

		/// <summary>Defines an unmanaged resource file for this assembly given the name of the resource file.</summary>
		/// <param name="resourceFileName">The name of the resource file. </param>
		/// <exception cref="T:System.ArgumentException">An unmanaged resource was previously defined.-or- The file <paramref name="resourceFileName" /> is not readable.-or- <paramref name="resourceFileName" /> is the empty string (""). </exception>
		/// <exception cref="T:System.ArgumentNullException">
		///   <paramref name="resourceFileName" /> is null. </exception>
		/// <exception cref="T:System.IO.FileNotFoundException">
		///   <paramref name="resourceFileName" /> is not found.-or- <paramref name="resourceFileName" /> is a directory. </exception>
		/// <exception cref="T:System.Security.SecurityException">The caller does not have the required permission. </exception>
		/// <PermissionSet>
		///   <IPermission class="System.Security.Permissions.FileIOPermission, mscorlib, Version=2.0.3600.0, Culture=neutral, PublicKeyToken=b77a5c561934e089" version="1" Unrestricted="true" />
		/// </PermissionSet>
		public void DefineUnmanagedResource(string resourceFileName)
		{
			if (resourceFileName == null)
			{
				throw new ArgumentNullException("resourceFileName");
			}
			if (resourceFileName.Length == 0)
			{
				throw new ArgumentException("resourceFileName");
			}
			if (!File.Exists(resourceFileName) || Directory.Exists(resourceFileName))
			{
				throw new FileNotFoundException("File '" + resourceFileName + "' does not exists or is a directory.");
			}
			if (native_resource != 0)
			{
				throw new ArgumentException("Native resource has already been defined.");
			}
			native_resource = NativeResourceType.Unmanaged;
			using (FileStream s = new FileStream(resourceFileName, FileMode.Open, FileAccess.Read))
			{
				Win32ResFileReader win32ResFileReader = new Win32ResFileReader(s);
				foreach (Win32EncodedResource item in win32ResFileReader.ReadResources())
				{
					if (item.Name.IsName || item.Type.IsName)
					{
						throw new InvalidOperationException("resource files with named resources or non-default resource types are not supported.");
					}
					AddUnmanagedResource(item);
				}
			}
		}

		/// <summary>Defines an unmanaged version information resource using the information specified in the assembly's AssemblyName object and the assembly's custom attributes.</summary>
		/// <exception cref="T:System.ArgumentException">An unmanaged version information resource was previously defined.-or- The unmanaged version information is too large to persist. </exception>
		/// <exception cref="T:System.Security.SecurityException">The caller does not have the required permission. </exception>
		public void DefineVersionInfoResource()
		{
			if (native_resource != 0)
			{
				throw new ArgumentException("Native resource has already been defined.");
			}
			native_resource = NativeResourceType.Assembly;
			version_res = new Win32VersionResource(1, 0, IsCompilerContext);
		}

		/// <summary>Defines an unmanaged version information resource for this assembly with the given specifications.</summary>
		/// <param name="product">The name of the product with which this assembly is distributed. </param>
		/// <param name="productVersion">The version of the product with which this assembly is distributed. </param>
		/// <param name="company">The name of the company that produced this assembly. </param>
		/// <param name="copyright">Describes all copyright notices, trademarks, and registered trademarks that apply to this assembly. This should include the full text of all notices, legal symbols, copyright dates, trademark numbers, and so on. In English, this string should be in the format "Copyright Microsoft Corp. 1990-2001". </param>
		/// <param name="trademark">Describes all trademarks and registered trademarks that apply to this assembly. This should include the full text of all notices, legal symbols, trademark numbers, and so on. In English, this string should be in the format "Windows is a trademark of Microsoft Corporation". </param>
		/// <exception cref="T:System.ArgumentException">An unmanaged version information resource was previously defined.-or- The unmanaged version information is too large to persist. </exception>
		/// <exception cref="T:System.Security.SecurityException">The caller does not have the required permission. </exception>
		public void DefineVersionInfoResource(string product, string productVersion, string company, string copyright, string trademark)
		{
			if (native_resource != 0)
			{
				throw new ArgumentException("Native resource has already been defined.");
			}
			native_resource = NativeResourceType.Explicit;
			version_res = new Win32VersionResource(1, 0, compilercontext: false);
			version_res.ProductName = ((product == null) ? " " : product);
			version_res.ProductVersion = ((productVersion == null) ? " " : productVersion);
			version_res.CompanyName = ((company == null) ? " " : company);
			version_res.LegalCopyright = ((copyright == null) ? " " : copyright);
			version_res.LegalTrademarks = ((trademark == null) ? " " : trademark);
		}

		internal void DefineIconResource(string iconFileName)
		{
			if (iconFileName == null)
			{
				throw new ArgumentNullException("iconFileName");
			}
			if (iconFileName.Length == 0)
			{
				throw new ArgumentException("iconFileName");
			}
			if (!File.Exists(iconFileName) || Directory.Exists(iconFileName))
			{
				throw new FileNotFoundException("File '" + iconFileName + "' does not exists or is a directory.");
			}
			using (FileStream s = new FileStream(iconFileName, FileMode.Open, FileAccess.Read))
			{
				Win32IconFileReader win32IconFileReader = new Win32IconFileReader(s);
				ICONDIRENTRY[] array = win32IconFileReader.ReadIcons();
				Win32IconResource[] array2 = new Win32IconResource[array.Length];
				for (int i = 0; i < array.Length; i++)
				{
					array2[i] = new Win32IconResource(i + 1, 0, array[i]);
					AddUnmanagedResource(array2[i]);
				}
				Win32GroupIconResource res = new Win32GroupIconResource(1, 0, array2);
				AddUnmanagedResource(res);
			}
		}

		private void DefineVersionInfoResourceImpl(string fileName)
		{
			if (versioninfo_culture != null)
			{
				version_res.FileLanguage = new CultureInfo(versioninfo_culture).LCID;
			}
			version_res.Version = ((version != null) ? version : "0.0.0.0");
			if (cattrs != null)
			{
				switch (native_resource)
				{
				case NativeResourceType.Assembly:
				{
					CustomAttributeBuilder[] array2 = cattrs;
					foreach (CustomAttributeBuilder customAttributeBuilder2 in array2)
					{
						switch (customAttributeBuilder2.Ctor.ReflectedType.FullName)
						{
						case "System.Reflection.AssemblyProductAttribute":
							version_res.ProductName = customAttributeBuilder2.string_arg();
							break;
						case "System.Reflection.AssemblyCompanyAttribute":
							version_res.CompanyName = customAttributeBuilder2.string_arg();
							break;
						case "System.Reflection.AssemblyCopyrightAttribute":
							version_res.LegalCopyright = customAttributeBuilder2.string_arg();
							break;
						case "System.Reflection.AssemblyTrademarkAttribute":
							version_res.LegalTrademarks = customAttributeBuilder2.string_arg();
							break;
						case "System.Reflection.AssemblyCultureAttribute":
							if (!IsCompilerContext)
							{
								version_res.FileLanguage = new CultureInfo(customAttributeBuilder2.string_arg()).LCID;
							}
							break;
						case "System.Reflection.AssemblyFileVersionAttribute":
						{
							string text = customAttributeBuilder2.string_arg();
							if (!IsCompilerContext || (text != null && text.Length != 0))
							{
								version_res.FileVersion = text;
							}
							break;
						}
						case "System.Reflection.AssemblyInformationalVersionAttribute":
							version_res.ProductVersion = customAttributeBuilder2.string_arg();
							break;
						case "System.Reflection.AssemblyTitleAttribute":
							version_res.FileDescription = customAttributeBuilder2.string_arg();
							break;
						case "System.Reflection.AssemblyDescriptionAttribute":
							version_res.Comments = customAttributeBuilder2.string_arg();
							break;
						}
					}
					break;
				}
				case NativeResourceType.Explicit:
				{
					CustomAttributeBuilder[] array = cattrs;
					foreach (CustomAttributeBuilder customAttributeBuilder in array)
					{
						string fullName = customAttributeBuilder.Ctor.ReflectedType.FullName;
						if (fullName == "System.Reflection.AssemblyCultureAttribute")
						{
							if (!IsCompilerContext)
							{
								version_res.FileLanguage = new CultureInfo(customAttributeBuilder.string_arg()).LCID;
							}
						}
						else if (fullName == "System.Reflection.AssemblyDescriptionAttribute")
						{
							version_res.Comments = customAttributeBuilder.string_arg();
						}
					}
					break;
				}
				}
			}
			version_res.OriginalFilename = fileName;
			if (IsCompilerContext)
			{
				version_res.InternalName = fileName;
				if (version_res.ProductVersion.Trim().Length == 0)
				{
					version_res.ProductVersion = version_res.FileVersion;
				}
			}
			else
			{
				version_res.InternalName = Path.GetFileNameWithoutExtension(fileName);
			}
			AddUnmanagedResource(version_res);
		}

		/// <summary>Returns the dynamic module with the specified name.</summary>
		/// <returns>A ModuleBuilder object representing the requested dynamic module.</returns>
		/// <param name="name">The name of the requested dynamic module. </param>
		/// <exception cref="T:System.ArgumentNullException">
		///   <paramref name="name" /> is null. </exception>
		/// <exception cref="T:System.ArgumentException">The length of <paramref name="name" /> is zero. </exception>
		/// <exception cref="T:System.Security.SecurityException">The caller does not have the required permission. </exception>
		public ModuleBuilder GetDynamicModule(string name)
		{
			if (name == null)
			{
				throw new ArgumentNullException("name");
			}
			if (name.Length == 0)
			{
				throw new ArgumentException("Empty name is not legal.", "name");
			}
			if (modules != null)
			{
				for (int i = 0; i < modules.Length; i++)
				{
					if (modules[i].name == name)
					{
						return modules[i];
					}
				}
			}
			return null;
		}

		/// <summary>Gets the exported types defined in this assembly.</summary>
		/// <returns>An array of <see cref="T:System.Type" /> containing the exported types defined in this assembly.</returns>
		/// <exception cref="T:System.NotSupportedException">This method is not implemented. </exception>
		/// <exception cref="T:System.Security.SecurityException">The caller does not have the required permission. </exception>
		public override Type[] GetExportedTypes()
		{
			throw not_supported();
		}

		/// <summary>Gets a <see cref="T:System.IO.FileStream" /> for the specified file in the file table of the manifest of this assembly.</summary>
		/// <returns>A <see cref="T:System.IO.FileStream" /> for the specified file, or null, if the file is not found.</returns>
		/// <param name="name">The name of the specified file. </param>
		/// <exception cref="T:System.NotSupportedException">This method is not currently supported. </exception>
		/// <exception cref="T:System.Security.SecurityException">The caller does not have the required permission. </exception>
		public override FileStream GetFile(string name)
		{
			throw not_supported();
		}

		/// <summary>Gets the files in the file table of an assembly manifest, specifying whether to include resource modules.</summary>
		/// <returns>An array of <see cref="T:System.IO.FileStream" /> objects.</returns>
		/// <param name="getResourceModules">true to include resource modules; otherwise, false. </param>
		/// <exception cref="T:System.NotSupportedException">This method is not currently supported. </exception>
		/// <exception cref="T:System.Security.SecurityException">The caller does not have the required permission. </exception>
		public override FileStream[] GetFiles(bool getResourceModules)
		{
			throw not_supported();
		}

		internal override Module[] GetModulesInternal()
		{
			if (modules == null)
			{
				return new Module[0];
			}
			return (Module[])modules.Clone();
		}

		internal override Type[] GetTypes(bool exportedOnly)
		{
			Type[] array = null;
			if (modules != null)
			{
				for (int i = 0; i < modules.Length; i++)
				{
					Type[] types = modules[i].GetTypes();
					if (array == null)
					{
						array = types;
						continue;
					}
					Type[] destinationArray = new Type[array.Length + types.Length];
					Array.Copy(array, 0, destinationArray, 0, array.Length);
					Array.Copy(types, 0, destinationArray, array.Length, types.Length);
				}
			}
			if (loaded_modules != null)
			{
				for (int j = 0; j < loaded_modules.Length; j++)
				{
					Type[] types2 = loaded_modules[j].GetTypes();
					if (array == null)
					{
						array = types2;
						continue;
					}
					Type[] destinationArray2 = new Type[array.Length + types2.Length];
					Array.Copy(array, 0, destinationArray2, 0, array.Length);
					Array.Copy(types2, 0, destinationArray2, array.Length, types2.Length);
				}
			}
			return (array != null) ? array : Type.EmptyTypes;
		}

		/// <summary>Returns information about how the given resource has been persisted.</summary>
		/// <returns>
		///   <see cref="T:System.Reflection.ManifestResourceInfo" /> populated with information about the resource's topology, or null if the resource is not found.</returns>
		/// <param name="resourceName">The name of the resource. </param>
		/// <exception cref="T:System.NotSupportedException">This method is not currently supported. </exception>
		/// <exception cref="T:System.Security.SecurityException">The caller does not have the required permission. </exception>
		public override ManifestResourceInfo GetManifestResourceInfo(string resourceName)
		{
			throw not_supported();
		}

		/// <summary>Loads the specified manifest resource from this assembly.</summary>
		/// <returns>An array of type String containing the names of all the resources.</returns>
		/// <exception cref="T:System.NotSupportedException">This method is not supported on a dynamic assembly. To get the manifest resource names, use <see cref="M:System.Reflection.Assembly.GetManifestResourceNames" />. </exception>
		/// <exception cref="T:System.Security.SecurityException">The caller does not have the required permission. </exception>
		public override string[] GetManifestResourceNames()
		{
			throw not_supported();
		}

		/// <summary>Loads the specified manifest resource from this assembly.</summary>
		/// <returns>A <see cref="T:System.IO.Stream" /> representing this manifest resource.</returns>
		/// <param name="name">The name of the manifest resource being requested. </param>
		/// <exception cref="T:System.NotSupportedException">This method is not currently supported. </exception>
		/// <exception cref="T:System.Security.SecurityException">The caller does not have the required permission. </exception>
		public override Stream GetManifestResourceStream(string name)
		{
			throw not_supported();
		}

		/// <summary>Loads the specified manifest resource, scoped by the namespace of the specified type, from this assembly.</summary>
		/// <returns>A <see cref="T:System.IO.Stream" /> representing this manifest resource.</returns>
		/// <param name="type">The type whose namespace is used to scope the manifest resource name. </param>
		/// <param name="name">The name of the manifest resource being requested. </param>
		/// <exception cref="T:System.NotSupportedException">This method is not currently supported. </exception>
		/// <exception cref="T:System.Security.SecurityException">The caller does not have the required permission. </exception>
		public override Stream GetManifestResourceStream(Type type, string name)
		{
			throw not_supported();
		}

		internal override Module GetManifestModule()
		{
			if (manifest_module == null)
			{
				manifest_module = DefineDynamicModule("Default Dynamic Module");
			}
			return manifest_module;
		}

		/// <summary>Saves this dynamic assembly to disk, specifying the nature of code in the assembly's executables and the target platform.</summary>
		/// <param name="assemblyFileName">The file name of the assembly.</param>
		/// <param name="portableExecutableKind">A bitwise combination of the <see cref="T:System.Reflection.PortableExecutableKinds" /> values that specifies the nature of the code.</param>
		/// <param name="imageFileMachine">One of the <see cref="T:System.Reflection.ImageFileMachine" /> values that specifies the target platform.</param>
		/// <exception cref="T:System.ArgumentException">The length of <paramref name="assemblyFileName" /> is 0.-or- There are two or more modules resource files in the assembly with the same name.-or- The target directory of the assembly is invalid.-or- <paramref name="assemblyFileName" /> is not a simple file name (for example, has a directory or drive component), or more than one unmanaged resource, including a version information resources, was defined in this assembly.-or- The CultureInfo string in <see cref="T:System.Reflection.AssemblyCultureAttribute" /> is not a valid string and <see cref="M:System.Reflection.Emit.AssemblyBuilder.DefineVersionInfoResource(System.String,System.String,System.String,System.String,System.String)" /> was called prior to calling this method. </exception>
		/// <exception cref="T:System.ArgumentNullException">
		///   <paramref name="assemblyFileName" /> is null. </exception>
		/// <exception cref="T:System.InvalidOperationException">This assembly has been saved before.-or- This assembly has access Run<see cref="T:System.Reflection.Emit.AssemblyBuilderAccess" /></exception>
		/// <exception cref="T:System.IO.IOException">An output error occurs during the save. </exception>
		/// <exception cref="T:System.NotSupportedException">
		///   <see cref="M:System.Reflection.Emit.TypeBuilder.CreateType" /> has not been called for any of the types in the modules of the assembly to be written to disk. </exception>
		[MonoLimitation("No support for PE32+ assemblies for AMD64 and IA64")]
		public void Save(string assemblyFileName, PortableExecutableKinds portableExecutableKind, ImageFileMachine imageFileMachine)
		{
			peKind = portableExecutableKind;
			machine = imageFileMachine;
			if ((peKind & PortableExecutableKinds.PE32Plus) != 0 || (peKind & PortableExecutableKinds.Unmanaged32Bit) != 0)
			{
				throw new NotImplementedException(peKind.ToString());
			}
			if (machine == ImageFileMachine.IA64 || machine == ImageFileMachine.AMD64)
			{
				throw new NotImplementedException(machine.ToString());
			}
			if (resource_writers != null)
			{
				foreach (IResourceWriter resource_writer in resource_writers)
				{
					resource_writer.Generate();
					resource_writer.Close();
				}
			}
			ModuleBuilder moduleBuilder = null;
			if (modules != null)
			{
				ModuleBuilder[] array = modules;
				foreach (ModuleBuilder moduleBuilder2 in array)
				{
					if (moduleBuilder2.FullyQualifiedName == assemblyFileName)
					{
						moduleBuilder = moduleBuilder2;
					}
				}
			}
			if (moduleBuilder == null)
			{
				moduleBuilder = DefineDynamicModule("RefEmit_OnDiskManifestModule", assemblyFileName);
			}
			if (!is_module_only)
			{
				moduleBuilder.IsMain = true;
			}
			if (entry_point != null && entry_point.DeclaringType.Module != moduleBuilder)
			{
				Type[] array2 = (entry_point.GetParameters().Length != 1) ? Type.EmptyTypes : new Type[1]
				{
					typeof(string)
				};
				MethodBuilder methodBuilder = moduleBuilder.DefineGlobalMethod("__EntryPoint$", MethodAttributes.Static, entry_point.ReturnType, array2);
				ILGenerator iLGenerator = methodBuilder.GetILGenerator();
				if (array2.Length == 1)
				{
					iLGenerator.Emit(OpCodes.Ldarg_0);
				}
				iLGenerator.Emit(OpCodes.Tailcall);
				iLGenerator.Emit(OpCodes.Call, entry_point);
				iLGenerator.Emit(OpCodes.Ret);
				entry_point = methodBuilder;
			}
			if (version_res != null)
			{
				DefineVersionInfoResourceImpl(assemblyFileName);
			}
			if (sn != null)
			{
				public_key = sn.PublicKey;
			}
			ModuleBuilder[] array3 = modules;
			foreach (ModuleBuilder moduleBuilder3 in array3)
			{
				if (moduleBuilder3 != moduleBuilder)
				{
					moduleBuilder3.Save();
				}
			}
			moduleBuilder.Save();
			if (sn != null && sn.CanSign)
			{
				sn.Sign(Path.Combine(AssemblyDir, assemblyFileName));
			}
			created = true;
		}

		/// <summary>Saves this dynamic assembly to disk.</summary>
		/// <param name="assemblyFileName">The file name of the assembly. </param>
		/// <exception cref="T:System.ArgumentException">The length of <paramref name="assemblyFileName" /> is 0.-or- There are two or more modules resource files in the assembly with the same name.-or- The target directory of the assembly is invalid.-or- <paramref name="assemblyFileName" /> is not a simple file name (for example, has a directory or drive component), or more than one unmanaged resource, including a version information resource, was defined in this assembly.-or- The CultureInfo string in <see cref="T:System.Reflection.AssemblyCultureAttribute" /> is not a valid string and <see cref="M:System.Reflection.Emit.AssemblyBuilder.DefineVersionInfoResource(System.String,System.String,System.String,System.String,System.String)" /> was called prior to calling this method. </exception>
		/// <exception cref="T:System.ArgumentNullException">
		///   <paramref name="assemblyFileName" /> is null. </exception>
		/// <exception cref="T:System.InvalidOperationException">This assembly has been saved before.-or- This assembly has access Run<see cref="T:System.Reflection.Emit.AssemblyBuilderAccess" /></exception>
		/// <exception cref="T:System.IO.IOException">An output error occurs during the save. </exception>
		/// <exception cref="T:System.NotSupportedException">
		///   <see cref="M:System.Reflection.Emit.TypeBuilder.CreateType" /> has not been called for any of the types in the modules of the assembly to be written to disk. </exception>
		/// <PermissionSet>
		///   <IPermission class="System.Security.Permissions.FileIOPermission, mscorlib, Version=2.0.3600.0, Culture=neutral, PublicKeyToken=b77a5c561934e089" version="1" Unrestricted="true" />
		/// </PermissionSet>
		public void Save(string assemblyFileName)
		{
			Save(assemblyFileName, PortableExecutableKinds.ILOnly, ImageFileMachine.I386);
		}

		/// <summary>Sets the entry point for this dynamic assembly, assuming that a console application is being built.</summary>
		/// <param name="entryMethod">A reference to the method that represents the entry point for this dynamic assembly. </param>
		/// <exception cref="T:System.ArgumentNullException">
		///   <paramref name="entryMethod" /> is null. </exception>
		/// <exception cref="T:System.InvalidOperationException">
		///   <paramref name="entryMethod" /> is not contained within this assembly. </exception>
		/// <exception cref="T:System.Security.SecurityException">The caller does not have the required permission. </exception>
		public void SetEntryPoint(MethodInfo entryMethod)
		{
			SetEntryPoint(entryMethod, PEFileKinds.ConsoleApplication);
		}

		/// <summary>Sets the entry point for this assembly and defines the type of the portable executable (PE file) being built.</summary>
		/// <param name="entryMethod">A reference to the method that represents the entry point for this dynamic assembly. </param>
		/// <param name="fileKind">The type of the assembly executable being built. </param>
		/// <exception cref="T:System.ArgumentNullException">
		///   <paramref name="entryMethod" /> is null. </exception>
		/// <exception cref="T:System.InvalidOperationException">
		///   <paramref name="entryMethod" /> is not contained within this assembly. </exception>
		/// <exception cref="T:System.Security.SecurityException">The caller does not have the required permission. </exception>
		public void SetEntryPoint(MethodInfo entryMethod, PEFileKinds fileKind)
		{
			if (entryMethod == null)
			{
				throw new ArgumentNullException("entryMethod");
			}
			if (entryMethod.DeclaringType.Assembly != this)
			{
				throw new InvalidOperationException("Entry method is not defined in the same assembly.");
			}
			entry_point = entryMethod;
			pekind = fileKind;
		}

		/// <summary>Set a custom attribute on this assembly using a custom attribute builder.</summary>
		/// <param name="customBuilder">An instance of a helper class to define the custom attribute. </param>
		/// <exception cref="T:System.ArgumentNullException">
		///   <paramref name="con" /> is null. </exception>
		/// <exception cref="T:System.Security.SecurityException">The caller does not have the required permission. </exception>
		public void SetCustomAttribute(CustomAttributeBuilder customBuilder)
		{
			if (customBuilder == null)
			{
				throw new ArgumentNullException("customBuilder");
			}
			if (IsCompilerContext)
			{
				switch (customBuilder.Ctor.ReflectedType.FullName)
				{
				case "System.Reflection.AssemblyVersionAttribute":
					version = create_assembly_version(customBuilder.string_arg());
					return;
				case "System.Reflection.AssemblyCultureAttribute":
					culture = GetCultureString(customBuilder.string_arg());
					break;
				case "System.Reflection.AssemblyAlgorithmIdAttribute":
				{
					byte[] data = customBuilder.Data;
					int num = 2;
					algid = data[num];
					algid |= (uint)(data[num + 1] << 8);
					algid |= (uint)(data[num + 2] << 16);
					algid |= (uint)(data[num + 3] << 24);
					break;
				}
				case "System.Reflection.AssemblyFlagsAttribute":
				{
					byte[] data = customBuilder.Data;
					int num = 2;
					flags |= data[num];
					flags |= (uint)(data[num + 1] << 8);
					flags |= (uint)(data[num + 2] << 16);
					flags |= (uint)(data[num + 3] << 24);
					if (sn == null)
					{
						flags &= 4294967294u;
					}
					break;
				}
				}
			}
			if (cattrs != null)
			{
				CustomAttributeBuilder[] array = new CustomAttributeBuilder[cattrs.Length + 1];
				cattrs.CopyTo(array, 0);
				array[cattrs.Length] = customBuilder;
				cattrs = array;
			}
			else
			{
				cattrs = new CustomAttributeBuilder[1];
				cattrs[0] = customBuilder;
			}
		}

		/// <summary>Set a custom attribute on this assembly using a specified custom attribute blob.</summary>
		/// <param name="con">The constructor for the custom attribute. </param>
		/// <param name="binaryAttribute">A byte blob representing the attributes. </param>
		/// <exception cref="T:System.ArgumentNullException">
		///   <paramref name="con" /> or <paramref name="binaryAttribute" /> is null. </exception>
		/// <exception cref="T:System.Security.SecurityException">The caller does not have the required permission. </exception>
		/// <exception cref="T:System.ArgumentException">
		///   <paramref name="con" /> is not a RuntimeConstructorInfo.</exception>
		[ComVisible(true)]
		public void SetCustomAttribute(ConstructorInfo con, byte[] binaryAttribute)
		{
			if (con == null)
			{
				throw new ArgumentNullException("con");
			}
			if (binaryAttribute == null)
			{
				throw new ArgumentNullException("binaryAttribute");
			}
			SetCustomAttribute(new CustomAttributeBuilder(con, binaryAttribute));
		}

		internal void SetCorlibTypeBuilders(Type corlib_object_type, Type corlib_value_type, Type corlib_enum_type)
		{
			this.corlib_object_type = corlib_object_type;
			this.corlib_value_type = corlib_value_type;
			this.corlib_enum_type = corlib_enum_type;
		}

		internal void SetCorlibTypeBuilders(Type corlib_object_type, Type corlib_value_type, Type corlib_enum_type, Type corlib_void_type)
		{
			SetCorlibTypeBuilders(corlib_object_type, corlib_value_type, corlib_enum_type);
			this.corlib_void_type = corlib_void_type;
		}

		private Exception not_supported()
		{
			return new NotSupportedException("The invoked member is not supported in a dynamic module.");
		}

		private void check_name_and_filename(string name, string fileName, bool fileNeedsToExists)
		{
			if (name == null)
			{
				throw new ArgumentNullException("name");
			}
			if (fileName == null)
			{
				throw new ArgumentNullException("fileName");
			}
			if (name.Length == 0)
			{
				throw new ArgumentException("Empty name is not legal.", "name");
			}
			if (fileName.Length == 0)
			{
				throw new ArgumentException("Empty file name is not legal.", "fileName");
			}
			if (Path.GetFileName(fileName) != fileName)
			{
				throw new ArgumentException("fileName '" + fileName + "' must not include a path.", "fileName");
			}
			string text = fileName;
			if (dir != null)
			{
				text = Path.Combine(dir, fileName);
			}
			if (fileNeedsToExists && !File.Exists(text))
			{
				throw new FileNotFoundException("Could not find file '" + fileName + "'");
			}
			if (resources != null)
			{
				for (int i = 0; i < resources.Length; i++)
				{
					if (resources[i].filename == text)
					{
						throw new ArgumentException("Duplicate file name '" + fileName + "'");
					}
					if (resources[i].name == name)
					{
						throw new ArgumentException("Duplicate name '" + name + "'");
					}
				}
			}
			if (modules == null)
			{
				return;
			}
			int num = 0;
			while (true)
			{
				if (num < modules.Length)
				{
					if (!modules[num].IsTransient() && modules[num].FileName == fileName)
					{
						throw new ArgumentException("Duplicate file name '" + fileName + "'");
					}
					if (modules[num].Name == name)
					{
						break;
					}
					num++;
					continue;
				}
				return;
			}
			throw new ArgumentException("Duplicate name '" + name + "'");
		}

		private string create_assembly_version(string version)
		{
			//Discarded unreachable code: IL_012b
			string[] array = version.Split('.');
			int[] array2 = new int[4];
			if (array.Length < 0 || array.Length > 4)
			{
				throw new ArgumentException("The version specified '" + version + "' is invalid");
			}
			for (int i = 0; i < array.Length; i++)
			{
				if (array[i] == "*")
				{
					DateTime now = DateTime.Now;
					switch (i)
					{
					case 2:
						array2[2] = (now - new DateTime(2000, 1, 1)).Days;
						if (array.Length == 3)
						{
							array2[3] = (now.Second + now.Minute * 60 + now.Hour * 3600) / 2;
						}
						break;
					case 3:
						array2[3] = (now.Second + now.Minute * 60 + now.Hour * 3600) / 2;
						break;
					default:
						throw new ArgumentException("The version specified '" + version + "' is invalid");
					}
				}
				else
				{
					try
					{
						array2[i] = int.Parse(array[i]);
					}
					catch (FormatException)
					{
						throw new ArgumentException("The version specified '" + version + "' is invalid");
					}
				}
			}
			return array2[0] + "." + array2[1] + "." + array2[2] + "." + array2[3];
		}

		private string GetCultureString(string str)
		{
			return (!(str == "neutral")) ? str : string.Empty;
		}

		internal override AssemblyName UnprotectedGetName()
		{
			AssemblyName assemblyName = base.UnprotectedGetName();
			if (sn != null)
			{
				assemblyName.SetPublicKey(sn.PublicKey);
				assemblyName.SetPublicKeyToken(sn.PublicKeyToken);
			}
			return assemblyName;
		}
	}
}
