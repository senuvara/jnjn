using System.IO;
using System.Runtime.InteropServices;

namespace System.Security.Cryptography
{
	/// <summary>Defines a stream that links data streams to cryptographic transformations.</summary>
	[ComVisible(true)]
	public class CryptoStream : Stream
	{
		private Stream _stream;

		private ICryptoTransform _transform;

		private CryptoStreamMode _mode;

		private byte[] _currentBlock;

		private bool _disposed;

		private bool _flushedFinalBlock;

		private int _partialCount;

		private bool _endOfStream;

		private byte[] _waitingBlock;

		private int _waitingCount;

		private byte[] _transformedBlock;

		private int _transformedPos;

		private int _transformedCount;

		private byte[] _workingBlock;

		private int _workingCount;

		/// <summary>Gets a value indicating whether the current <see cref="T:System.Security.Cryptography.CryptoStream" /> is readable.</summary>
		/// <returns>true if the current stream is readable; otherwise, false.</returns>
		public override bool CanRead => _mode == CryptoStreamMode.Read;

		/// <summary>Gets a value indicating whether you can seek within the current <see cref="T:System.Security.Cryptography.CryptoStream" />.</summary>
		/// <returns>Always false.</returns>
		public override bool CanSeek => false;

		/// <summary>Gets a value indicating whether the current <see cref="T:System.Security.Cryptography.CryptoStream" /> is writable.</summary>
		/// <returns>true if the current stream is writable; otherwise, false.</returns>
		public override bool CanWrite => _mode == CryptoStreamMode.Write;

		/// <summary>Gets the length in bytes of the stream.</summary>
		/// <returns>This property is not supported.</returns>
		/// <exception cref="T:System.NotSupportedException">This property is not supported. </exception>
		public override long Length
		{
			get
			{
				throw new NotSupportedException("Length");
			}
		}

		/// <summary>Gets or sets the position within the current stream.</summary>
		/// <returns>This property is not supported.</returns>
		/// <exception cref="T:System.NotSupportedException">This property is not supported. </exception>
		public override long Position
		{
			get
			{
				throw new NotSupportedException("Position");
			}
			set
			{
				throw new NotSupportedException("Position");
			}
		}

		/// <summary>Initializes a new instance of the <see cref="T:System.Security.Cryptography.CryptoStream" /> class with a target data stream, the transformation to use, and the mode of the stream.</summary>
		/// <param name="stream">The stream on which to perform the cryptographic transformation. </param>
		/// <param name="transform">The cryptographic transformation that is to be performed on the stream. </param>
		/// <param name="mode">One of the <see cref="T:System.Security.Cryptography.CryptoStreamMode" /> values. </param>
		/// <exception cref="T:System.ArgumentException">
		///   <paramref name="stream" /> is not readable.</exception>
		/// <exception cref="T:System.ArgumentException">
		///   <paramref name="stream" /> is not writable.</exception>
		/// <exception cref="T:System.ArgumentException">
		///   <paramref name="stream" /> is invalid.</exception>
		public CryptoStream(Stream stream, ICryptoTransform transform, CryptoStreamMode mode)
		{
			if (mode == CryptoStreamMode.Read && !stream.CanRead)
			{
				throw new ArgumentException(Locale.GetText("Can't read on stream"));
			}
			if (mode == CryptoStreamMode.Write && !stream.CanWrite)
			{
				throw new ArgumentException(Locale.GetText("Can't write on stream"));
			}
			_stream = stream;
			_transform = transform;
			_mode = mode;
			_disposed = false;
			if (transform != null)
			{
				switch (mode)
				{
				case CryptoStreamMode.Read:
					_currentBlock = new byte[transform.InputBlockSize];
					_workingBlock = new byte[transform.InputBlockSize];
					break;
				case CryptoStreamMode.Write:
					_currentBlock = new byte[transform.OutputBlockSize];
					_workingBlock = new byte[transform.OutputBlockSize];
					break;
				}
			}
		}

		~CryptoStream()
		{
			Dispose(disposing: false);
		}

		/// <summary>Releases all resources used by the <see cref="T:System.Security.Cryptography.CryptoStream" />.</summary>
		public void Clear()
		{
			Dispose(disposing: true);
			GC.SuppressFinalize(this);
		}

		/// <summary>Closes the current stream and releases any resources (such as sockets and file handles) associated with the current stream.</summary>
		/// <exception cref="T:System.NotSupportedException">The current stream is not writable. </exception>
		public override void Close()
		{
			if (!_flushedFinalBlock && _mode == CryptoStreamMode.Write)
			{
				FlushFinalBlock();
			}
			if (_stream != null)
			{
				_stream.Close();
			}
		}

		/// <summary>Reads a sequence of bytes from the current <see cref="T:System.Security.Cryptography.CryptoStream" /> and advances the position within the stream by the number of bytes read.</summary>
		/// <returns>The total number of bytes read into the buffer. This can be less than the number of bytes requested if that many bytes are not currently available, or zero if the end of the stream has been reached.</returns>
		/// <param name="buffer">An array of bytes. A maximum of <paramref name="count" /> bytes are read from the current stream and stored in <paramref name="buffer" />. </param>
		/// <param name="offset">The byte offset in <paramref name="buffer" /> at which to begin storing the data read from the current stream. </param>
		/// <param name="count">The maximum number of bytes to be read from the current stream. </param>
		/// <exception cref="T:System.NotSupportedException">The <see cref="T:System.Security.Cryptography.CryptoStreamMode" /> associated with current <see cref="T:System.Security.Cryptography.CryptoStream" /> object does not match the underlying stream.  For example, this exception is thrown when using <see cref="F:System.Security.Cryptography.CryptoStreamMode.Read" /> with an underlying stream that is write only.  </exception>
		/// <exception cref="T:System.ArgumentOutOfRangeException">The <paramref name="offset" /> parameter is less than zero.-or- The <paramref name="count" /> parameter is less than zero. </exception>
		/// <exception cref="T:System.ArgumentException">Thesum of the <paramref name="count" /> and <paramref name="offset" /> parameters is longer than the length of the buffer. </exception>
		public override int Read([In] [Out] byte[] buffer, int offset, int count)
		{
			if (_mode != 0)
			{
				throw new NotSupportedException(Locale.GetText("not in Read mode"));
			}
			if (offset < 0)
			{
				throw new ArgumentOutOfRangeException("offset", Locale.GetText("negative"));
			}
			if (count < 0)
			{
				throw new ArgumentOutOfRangeException("count", Locale.GetText("negative"));
			}
			if (offset > buffer.Length - count)
			{
				throw new ArgumentException("(offset+count)", Locale.GetText("buffer overflow"));
			}
			if (_workingBlock == null)
			{
				return 0;
			}
			int num = 0;
			if (count == 0 || (_transformedPos == _transformedCount && _endOfStream))
			{
				return num;
			}
			if (_waitingBlock == null)
			{
				_transformedBlock = new byte[_transform.OutputBlockSize << 2];
				_transformedPos = 0;
				_transformedCount = 0;
				_waitingBlock = new byte[_transform.InputBlockSize];
				_waitingCount = _stream.Read(_waitingBlock, 0, _waitingBlock.Length);
			}
			while (count > 0)
			{
				int num2 = _transformedCount - _transformedPos;
				if (num2 < _transform.InputBlockSize)
				{
					int num3 = 0;
					_workingCount = _stream.Read(_workingBlock, 0, _transform.InputBlockSize);
					_endOfStream = (_workingCount < _transform.InputBlockSize);
					if (!_endOfStream)
					{
						num3 = _transform.TransformBlock(_waitingBlock, 0, _waitingBlock.Length, _transformedBlock, _transformedCount);
						Buffer.BlockCopy(_workingBlock, 0, _waitingBlock, 0, _workingCount);
						_waitingCount = _workingCount;
					}
					else
					{
						if (_workingCount > 0)
						{
							num3 = _transform.TransformBlock(_waitingBlock, 0, _waitingBlock.Length, _transformedBlock, _transformedCount);
							Buffer.BlockCopy(_workingBlock, 0, _waitingBlock, 0, _workingCount);
							_waitingCount = _workingCount;
							num2 += num3;
							_transformedCount += num3;
						}
						if (!_flushedFinalBlock)
						{
							byte[] array = _transform.TransformFinalBlock(_waitingBlock, 0, _waitingCount);
							num3 = array.Length;
							Buffer.BlockCopy(array, 0, _transformedBlock, _transformedCount, array.Length);
							Array.Clear(array, 0, array.Length);
							_flushedFinalBlock = true;
						}
					}
					num2 += num3;
					_transformedCount += num3;
				}
				if (_transformedPos > _transform.OutputBlockSize)
				{
					Buffer.BlockCopy(_transformedBlock, _transformedPos, _transformedBlock, 0, num2);
					_transformedCount -= _transformedPos;
					_transformedPos = 0;
				}
				num2 = ((count >= num2) ? num2 : count);
				if (num2 > 0)
				{
					Buffer.BlockCopy(_transformedBlock, _transformedPos, buffer, offset, num2);
					_transformedPos += num2;
					num += num2;
					offset += num2;
					count -= num2;
				}
				if ((num2 != _transform.InputBlockSize && _waitingCount != _transform.InputBlockSize) || _endOfStream)
				{
					count = 0;
				}
			}
			return num;
		}

		/// <summary>Writes a sequence of bytes to the current <see cref="T:System.Security.Cryptography.CryptoStream" /> and advances the current position within this stream by the number of bytes written.</summary>
		/// <param name="buffer">An array of bytes. This method copies <paramref name="count" /> bytes from <paramref name="buffer" /> to the current stream. </param>
		/// <param name="offset">The byte offset in <paramref name="buffer" /> at which to begin copying bytes to the current stream. </param>
		/// <param name="count">The number of bytes to be written to the current stream. </param>
		/// <exception cref="T:System.NotSupportedException">The <see cref="T:System.Security.Cryptography.CryptoStreamMode" /> associated with current <see cref="T:System.Security.Cryptography.CryptoStream" /> object does not match the underlying stream.  For example, this exception is thrown when using <see cref="F:System.Security.Cryptography.CryptoStreamMode.Write" />  with an underlying stream that is read only.  </exception>
		/// <exception cref="T:System.ArgumentOutOfRangeException">The <paramref name="offset" /> parameter is less than zero.-or- The <paramref name="count" /> parameter is less than zero. </exception>
		/// <exception cref="T:System.ArgumentException">The sum of the <paramref name="count" /> and <paramref name="offset" /> parameters is longer than the length of the buffer. </exception>
		public override void Write(byte[] buffer, int offset, int count)
		{
			if (_mode != CryptoStreamMode.Write)
			{
				throw new NotSupportedException(Locale.GetText("not in Write mode"));
			}
			if (offset < 0)
			{
				throw new ArgumentOutOfRangeException("offset", Locale.GetText("negative"));
			}
			if (count < 0)
			{
				throw new ArgumentOutOfRangeException("count", Locale.GetText("negative"));
			}
			if (offset > buffer.Length - count)
			{
				throw new ArgumentException("(offset+count)", Locale.GetText("buffer overflow"));
			}
			if (_stream == null)
			{
				throw new ArgumentNullException("inner stream was diposed");
			}
			int num = count;
			if (_partialCount > 0 && _partialCount != _transform.InputBlockSize)
			{
				int num2 = _transform.InputBlockSize - _partialCount;
				num2 = ((count >= num2) ? num2 : count);
				Buffer.BlockCopy(buffer, offset, _workingBlock, _partialCount, num2);
				_partialCount += num2;
				offset += num2;
				count -= num2;
			}
			int num3 = offset;
			while (count > 0)
			{
				if (_partialCount == _transform.InputBlockSize)
				{
					int count2 = _transform.TransformBlock(_workingBlock, 0, _partialCount, _currentBlock, 0);
					_stream.Write(_currentBlock, 0, count2);
					_partialCount = 0;
				}
				if (_transform.CanTransformMultipleBlocks)
				{
					int num4 = count & ~(_transform.InputBlockSize - 1);
					int num5 = count & (_transform.InputBlockSize - 1);
					int num6 = (1 + num4 / _transform.InputBlockSize) * _transform.OutputBlockSize;
					if (_workingBlock.Length < num6)
					{
						Array.Clear(_workingBlock, 0, _workingBlock.Length);
						_workingBlock = new byte[num6];
					}
					if (num4 > 0)
					{
						int count3 = _transform.TransformBlock(buffer, offset, num4, _workingBlock, 0);
						_stream.Write(_workingBlock, 0, count3);
					}
					if (num5 > 0)
					{
						Buffer.BlockCopy(buffer, num - num5, _workingBlock, 0, num5);
					}
					_partialCount = num5;
					count = 0;
				}
				else
				{
					int num7 = Math.Min(_transform.InputBlockSize - _partialCount, count);
					Buffer.BlockCopy(buffer, num3, _workingBlock, _partialCount, num7);
					num3 += num7;
					_partialCount += num7;
					count -= num7;
				}
			}
		}

		/// <summary>Clears all buffers for this stream and causes any buffered data to be written to the underlying device.</summary>
		public override void Flush()
		{
			if (_stream != null)
			{
				_stream.Flush();
			}
		}

		/// <summary>Updates the underlying data source or repository with the current state of the buffer, then clears the buffer.</summary>
		/// <exception cref="T:System.Security.Cryptography.CryptographicException">The key is corrupt which can cause invalid padding to the stream. </exception>
		/// <exception cref="T:System.NotSupportedException">The current stream is not writable.-or- The final block has already been transformed. </exception>
		public void FlushFinalBlock()
		{
			if (_flushedFinalBlock)
			{
				throw new NotSupportedException(Locale.GetText("This method cannot be called twice."));
			}
			if (_disposed)
			{
				throw new NotSupportedException(Locale.GetText("CryptoStream was disposed."));
			}
			if (_mode != CryptoStreamMode.Write)
			{
				return;
			}
			_flushedFinalBlock = true;
			byte[] array = _transform.TransformFinalBlock(_workingBlock, 0, _partialCount);
			if (_stream != null)
			{
				_stream.Write(array, 0, array.Length);
				if (_stream is CryptoStream)
				{
					(_stream as CryptoStream).FlushFinalBlock();
				}
				_stream.Flush();
			}
			Array.Clear(array, 0, array.Length);
		}

		/// <summary>Sets the position within the current stream.</summary>
		/// <returns>This method is not supported.</returns>
		/// <param name="offset">A byte offset relative to the <paramref name="origin" /> parameter. </param>
		/// <param name="origin">A <see cref="T:System.IO.SeekOrigin" /> object indicating the reference point used to obtain the new position. </param>
		/// <exception cref="T:System.NotSupportedException">This method is not supported. </exception>
		public override long Seek(long offset, SeekOrigin origin)
		{
			throw new NotSupportedException("Seek");
		}

		/// <summary>Sets the length of the current stream.</summary>
		/// <param name="value">The desired length of the current stream in bytes. </param>
		/// <exception cref="T:System.NotSupportedException">This property exists only to support inheritance from <see cref="T:System.IO.Stream" />, and cannot be used.</exception>
		public override void SetLength(long value)
		{
			throw new NotSupportedException("SetLength");
		}

		/// <summary>Releases the unmanaged resources used by the <see cref="T:System.Security.Cryptography.CryptoStream" /> and optionally releases the managed resources.</summary>
		/// <param name="disposing">true to release both managed and unmanaged resources; false to release only unmanaged resources. </param>
		protected override void Dispose(bool disposing)
		{
			if (!_disposed)
			{
				_disposed = true;
				if (_workingBlock != null)
				{
					Array.Clear(_workingBlock, 0, _workingBlock.Length);
				}
				if (_currentBlock != null)
				{
					Array.Clear(_currentBlock, 0, _currentBlock.Length);
				}
				if (disposing)
				{
					_stream = null;
					_workingBlock = null;
					_currentBlock = null;
				}
			}
		}
	}
}
