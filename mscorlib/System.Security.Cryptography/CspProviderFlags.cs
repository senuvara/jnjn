using System.Runtime.InteropServices;

namespace System.Security.Cryptography
{
	/// <summary>Specifies flags that modify the behavior of the cryptographic service providers (CSP).</summary>
	[Serializable]
	[Flags]
	[ComVisible(true)]
	public enum CspProviderFlags
	{
		/// <summary>Use key information from the computer's key store.</summary>
		UseMachineKeyStore = 0x1,
		/// <summary>Use key information from the default key container.</summary>
		UseDefaultKeyContainer = 0x2,
		/// <summary>Use key information from the current key.</summary>
		UseExistingKey = 0x8,
		/// <summary>Don't specify any settings.</summary>
		NoFlags = 0x0,
		/// <summary>Prevent the CSP from displaying any user interface (UI) for this context.</summary>
		NoPrompt = 0x40,
		/// <summary>Allow a key to be exported for archival or recovery.</summary>
		UseArchivableKey = 0x10,
		/// <summary>Use key information that can not be exported.</summary>
		UseNonExportableKey = 0x4,
		/// <summary>Notify the user through a dialog box or another method when certain actions are attempting to use a key.  This flag is not compatible with the <see cref="F:System.Security.Cryptography.CspProviderFlags.NoPrompt" /> flag.</summary>
		UseUserProtectedKey = 0x20
	}
}
