using System.Runtime.InteropServices;
using System.Text;

namespace System.Security.Cryptography
{
	/// <summary>Derives a key from a password using an extension of the PBKDF1 algorithm.</summary>
	[ComVisible(true)]
	public class PasswordDeriveBytes : DeriveBytes
	{
		private string HashNameValue;

		private byte[] SaltValue;

		private int IterationsValue;

		private HashAlgorithm hash;

		private int state;

		private byte[] password;

		private byte[] initial;

		private byte[] output;

		private int position;

		private int hashnumber;

		/// <summary>Gets or sets the name of the hash algorithm for the operation.</summary>
		/// <returns>The name of the hash algorithm for the operation.</returns>
		/// <exception cref="T:System.Security.Cryptography.CryptographicException">The name of the hash value is fixed and an attempt is made to change this value. </exception>
		public string HashName
		{
			get
			{
				return HashNameValue;
			}
			set
			{
				if (value == null)
				{
					throw new ArgumentNullException("HashName");
				}
				if (state != 0)
				{
					throw new CryptographicException(Locale.GetText("Can't change this property at this stage"));
				}
				HashNameValue = value;
			}
		}

		/// <summary>Gets or sets the number of iterations for the operation.</summary>
		/// <returns>The number of iterations for the operation.</returns>
		/// <exception cref="T:System.Security.Cryptography.CryptographicException">The number of iterations is fixed and an attempt is made to change this value. </exception>
		/// <exception cref="T:System.ArgumentOutOfRangeException">The property cannot be set because its value is out of range. This property requires a non-negative number.</exception>
		public int IterationCount
		{
			get
			{
				return IterationsValue;
			}
			set
			{
				if (value < 1)
				{
					throw new ArgumentOutOfRangeException("> 0", "IterationCount");
				}
				if (state != 0)
				{
					throw new CryptographicException(Locale.GetText("Can't change this property at this stage"));
				}
				IterationsValue = value;
			}
		}

		/// <summary>Gets or sets the key salt value for the operation.</summary>
		/// <returns>The key salt value for the operation.</returns>
		/// <exception cref="T:System.Security.Cryptography.CryptographicException">The key salt value is fixed and an attempt is made to change this value. </exception>
		public byte[] Salt
		{
			get
			{
				if (SaltValue == null)
				{
					return null;
				}
				return (byte[])SaltValue.Clone();
			}
			set
			{
				if (state != 0)
				{
					throw new CryptographicException(Locale.GetText("Can't change this property at this stage"));
				}
				if (value != null)
				{
					SaltValue = (byte[])value.Clone();
				}
				else
				{
					SaltValue = null;
				}
			}
		}

		/// <summary>Initializes a new instance of the <see cref="T:System.Security.Cryptography.PasswordDeriveBytes" /> class with the password and key salt to use to derive the key.</summary>
		/// <param name="strPassword">The password for which to derive the key. </param>
		/// <param name="rgbSalt">The key salt to use to derive the key. </param>
		public PasswordDeriveBytes(string strPassword, byte[] rgbSalt)
		{
			Prepare(strPassword, rgbSalt, "SHA1", 100);
		}

		/// <summary>Initializes a new instance of the <see cref="T:System.Security.Cryptography.PasswordDeriveBytes" /> class with the password, key salt, and cryptographic service provider (CSP) parameters to use to derive the key.</summary>
		/// <param name="strPassword">The password for which to derive the key. </param>
		/// <param name="rgbSalt">The key salt to use to derive the key. </param>
		/// <param name="cspParams">The CSP parameters for the operation. </param>
		public PasswordDeriveBytes(string strPassword, byte[] rgbSalt, CspParameters cspParams)
		{
			Prepare(strPassword, rgbSalt, "SHA1", 100);
			if (cspParams != null)
			{
				throw new NotSupportedException(Locale.GetText("CspParameters not supported by Mono for PasswordDeriveBytes."));
			}
		}

		/// <summary>Initializes a new instance of the <see cref="T:System.Security.Cryptography.PasswordDeriveBytes" /> class with the password, key salt, hash name, and number of iterations to use to derive the key.</summary>
		/// <param name="strPassword">The password for which to derive the key. </param>
		/// <param name="rgbSalt">The key salt to use to derive the key. </param>
		/// <param name="strHashName">The name of the hash algorithm for the operation. </param>
		/// <param name="iterations">The number of iterations for the operation. </param>
		public PasswordDeriveBytes(string strPassword, byte[] rgbSalt, string strHashName, int iterations)
		{
			Prepare(strPassword, rgbSalt, strHashName, iterations);
		}

		/// <summary>Initializes a new instance of the <see cref="T:System.Security.Cryptography.PasswordDeriveBytes" /> class with the password, key salt, hash name, number of iterations, and cryptographic service provider (CSP) parameters to use to derive the key.</summary>
		/// <param name="strPassword">The password for which to derive the key. </param>
		/// <param name="rgbSalt">The key salt to use to derive the key. </param>
		/// <param name="strHashName">The name of the hash algorithm for the operation. </param>
		/// <param name="iterations">The number of iterations for the operation. </param>
		/// <param name="cspParams">The CSP parameters for the operation. </param>
		public PasswordDeriveBytes(string strPassword, byte[] rgbSalt, string strHashName, int iterations, CspParameters cspParams)
		{
			Prepare(strPassword, rgbSalt, strHashName, iterations);
			if (cspParams != null)
			{
				throw new NotSupportedException(Locale.GetText("CspParameters not supported by Mono for PasswordDeriveBytes."));
			}
		}

		/// <summary>Initializes a new instance of the <see cref="T:System.Security.Cryptography.PasswordDeriveBytes" /> class specifying the password and key salt to use to derive the key.</summary>
		/// <param name="password">The password to derive the key for.</param>
		/// <param name="salt">The key salt to use to derive the key.</param>
		public PasswordDeriveBytes(byte[] password, byte[] salt)
		{
			Prepare(password, salt, "SHA1", 100);
		}

		/// <summary>Initializes a new instance of the <see cref="T:System.Security.Cryptography.PasswordDeriveBytes" /> class specifying the password, key salt, and cryptographic service provider (CSP) to use to derive the key.</summary>
		/// <param name="password">The password to derive the key for.</param>
		/// <param name="salt">The key salt to use to derive the key.</param>
		/// <param name="cspParams">The cryptographic service provider (CSP) parameters for the operation.</param>
		public PasswordDeriveBytes(byte[] password, byte[] salt, CspParameters cspParams)
		{
			Prepare(password, salt, "SHA1", 100);
			if (cspParams != null)
			{
				throw new NotSupportedException(Locale.GetText("CspParameters not supported by Mono for PasswordDeriveBytes."));
			}
		}

		/// <summary>Initializes a new instance of the <see cref="T:System.Security.Cryptography.PasswordDeriveBytes" /> class specifying the password, key salt, hash name, and iterations to use to derive the key.</summary>
		/// <param name="password">The password to derive the key for.</param>
		/// <param name="salt">The key salt to use to derive the key.</param>
		/// <param name="hashName">The hash algorithm to use to derive the key.</param>
		/// <param name="iterations">The iteration count to use to derive the key.</param>
		public PasswordDeriveBytes(byte[] password, byte[] salt, string hashName, int iterations)
		{
			Prepare(password, salt, hashName, iterations);
		}

		/// <summary>Initializes a new instance of the <see cref="T:System.Security.Cryptography.PasswordDeriveBytes" /> class specifying the password, key salt, hash name, iterations, and cryptographic service provider (CSP) to use to derive the key.</summary>
		/// <param name="password">The password to derive the key for.</param>
		/// <param name="salt">The key salt to use to derive the key.</param>
		/// <param name="hashName">The hash algorithm to use to derive the key.</param>
		/// <param name="iterations">The iteration count to use to derive the key.</param>
		/// <param name="cspParams">The cryptographic service provider (CSP) parameters for the operation.</param>
		public PasswordDeriveBytes(byte[] password, byte[] salt, string hashName, int iterations, CspParameters cspParams)
		{
			Prepare(password, salt, hashName, iterations);
			if (cspParams != null)
			{
				throw new NotSupportedException(Locale.GetText("CspParameters not supported by Mono for PasswordDeriveBytes."));
			}
		}

		~PasswordDeriveBytes()
		{
			if (initial != null)
			{
				Array.Clear(initial, 0, initial.Length);
				initial = null;
			}
			Array.Clear(password, 0, password.Length);
		}

		private void Prepare(string strPassword, byte[] rgbSalt, string strHashName, int iterations)
		{
			if (strPassword == null)
			{
				throw new ArgumentNullException("strPassword");
			}
			byte[] bytes = Encoding.UTF8.GetBytes(strPassword);
			Prepare(bytes, rgbSalt, strHashName, iterations);
			Array.Clear(bytes, 0, bytes.Length);
		}

		private void Prepare(byte[] password, byte[] rgbSalt, string strHashName, int iterations)
		{
			if (password == null)
			{
				throw new ArgumentNullException("password");
			}
			this.password = (byte[])password.Clone();
			Salt = rgbSalt;
			HashName = strHashName;
			IterationCount = iterations;
			state = 0;
		}

		/// <summary>Derives a cryptographic key from the <see cref="T:System.Security.Cryptography.PasswordDeriveBytes" /> object.</summary>
		/// <returns>The derived key.</returns>
		/// <param name="algname">The algorithm name for which to derive the key. </param>
		/// <param name="alghashname">The hash algorithm name to use to derive the key. </param>
		/// <param name="keySize">The size of the key, in bits, to derive. </param>
		/// <param name="rgbIV">The initialization vector (IV) to use to derive the key. </param>
		/// <exception cref="T:System.Security.Cryptography.CryptographicException">The <paramref name="keySize" /> parameter is greater than 128.-or- The cryptographic service provider (CSP) cannot be acquired.-or- The <paramref name="algname" /> parameter is not a valid algorithm name.-or- The <paramref name="alghashname" /> parameter is not a valid hash algorithm name. </exception>
		/// <PermissionSet>
		///   <IPermission class="System.Security.Permissions.KeyContainerPermission, mscorlib, Version=2.0.3600.0, Culture=neutral, PublicKeyToken=b77a5c561934e089" version="1" Unrestricted="true" />
		/// </PermissionSet>
		public byte[] CryptDeriveKey(string algname, string alghashname, int keySize, byte[] rgbIV)
		{
			if (keySize > 128)
			{
				throw new CryptographicException(Locale.GetText("Key Size can't be greater than 128 bits"));
			}
			throw new NotSupportedException(Locale.GetText("CspParameters not supported by Mono"));
		}

		/// <summary>Returns pseudo-random key bytes.</summary>
		/// <returns>A byte array filled with pseudo-random key bytes.</returns>
		/// <param name="cb">The number of pseudo-random key bytes to generate. </param>
		[Obsolete("see Rfc2898DeriveBytes for PKCS#5 v2 support")]
		public override byte[] GetBytes(int cb)
		{
			if (cb < 1)
			{
				throw new IndexOutOfRangeException("cb");
			}
			if (state == 0)
			{
				Reset();
				state = 1;
			}
			byte[] array = new byte[cb];
			int num = 0;
			int num2 = Math.Max(1, IterationsValue - 1);
			if (output == null)
			{
				output = initial;
				for (int i = 0; i < num2 - 1; i++)
				{
					output = hash.ComputeHash(output);
				}
			}
			while (num < cb)
			{
				byte[] array2 = null;
				if (hashnumber == 0)
				{
					array2 = hash.ComputeHash(output);
				}
				else
				{
					if (hashnumber >= 1000)
					{
						throw new CryptographicException(Locale.GetText("too long"));
					}
					string text = Convert.ToString(hashnumber);
					array2 = new byte[output.Length + text.Length];
					for (int j = 0; j < text.Length; j++)
					{
						array2[j] = (byte)text[j];
					}
					Buffer.BlockCopy(output, 0, array2, text.Length, output.Length);
					array2 = hash.ComputeHash(array2);
				}
				int val = array2.Length - position;
				int num3 = Math.Min(cb - num, val);
				Buffer.BlockCopy(array2, position, array, num, num3);
				num += num3;
				position += num3;
				while (position >= array2.Length)
				{
					position -= array2.Length;
					hashnumber++;
				}
			}
			return array;
		}

		/// <summary>Resets the state of the operation.</summary>
		public override void Reset()
		{
			state = 0;
			position = 0;
			hashnumber = 0;
			hash = HashAlgorithm.Create(HashNameValue);
			if (SaltValue != null)
			{
				hash.TransformBlock(password, 0, password.Length, password, 0);
				hash.TransformFinalBlock(SaltValue, 0, SaltValue.Length);
				initial = hash.Hash;
			}
			else
			{
				initial = hash.ComputeHash(password);
			}
		}
	}
}
