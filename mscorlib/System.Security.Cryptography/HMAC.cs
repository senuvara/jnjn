using Mono.Security.Cryptography;
using System.Runtime.InteropServices;

namespace System.Security.Cryptography
{
	/// <summary>Represents the abstract class from which all implementations of Hash-based Message Authentication Code (HMAC) must derive.</summary>
	[ComVisible(true)]
	public abstract class HMAC : KeyedHashAlgorithm
	{
		private bool _disposed;

		private string _hashName;

		private HashAlgorithm _algo;

		private BlockProcessor _block;

		private int _blockSizeValue;

		/// <summary>Gets or sets the block size to use in the hash value.</summary>
		/// <returns>The block size to use in the hash value.</returns>
		protected int BlockSizeValue
		{
			get
			{
				return _blockSizeValue;
			}
			set
			{
				_blockSizeValue = value;
			}
		}

		/// <summary>Gets or sets the name of the hash algorithm to use for hashing.</summary>
		/// <returns>The name of the hash algorithm.</returns>
		/// <exception cref="T:System.Security.Cryptography.CryptographicException">The current hash algorithm cannot be changed.</exception>
		public string HashName
		{
			get
			{
				return _hashName;
			}
			set
			{
				_hashName = value;
				_algo = HashAlgorithm.Create(_hashName);
			}
		}

		/// <summary>Gets or sets the key to use in the hash algorithm.</summary>
		/// <returns>The key to use in the hash algorithm.</returns>
		/// <exception cref="T:System.Security.Cryptography.CryptographicException">An attempt is made to change the <see cref="P:System.Security.Cryptography.HMAC.Key" /> property after hashing has begun. </exception>
		public override byte[] Key
		{
			get
			{
				return (byte[])base.Key.Clone();
			}
			set
			{
				if (value != null && value.Length > 64)
				{
					base.Key = _algo.ComputeHash(value);
				}
				else
				{
					base.Key = (byte[])value.Clone();
				}
			}
		}

		internal BlockProcessor Block
		{
			get
			{
				if (_block == null)
				{
					_block = new BlockProcessor(_algo, BlockSizeValue >> 3);
				}
				return _block;
			}
		}

		/// <summary>Initializes a new instance of the <see cref="T:System.Security.Cryptography.HMAC" /> class. </summary>
		protected HMAC()
		{
			_disposed = false;
			_blockSizeValue = 64;
		}

		private byte[] KeySetup(byte[] key, byte padding)
		{
			byte[] array = new byte[BlockSizeValue];
			for (int i = 0; i < key.Length; i++)
			{
				array[i] = (byte)(key[i] ^ padding);
			}
			for (int j = key.Length; j < BlockSizeValue; j++)
			{
				array[j] = padding;
			}
			return array;
		}

		/// <summary>Releases the unmanaged resources used by the <see cref="T:System.Security.Cryptography.HMAC" /> class when a key change is legitimate and optionally releases the managed resources.</summary>
		/// <param name="disposing">true to release both managed and unmanaged resources; false to release only unmanaged resources. </param>
		protected override void Dispose(bool disposing)
		{
			if (!_disposed)
			{
				base.Dispose(disposing);
			}
		}

		/// <summary>When overridden in a derived class, routes data written to the object into the default <see cref="T:System.Security.Cryptography.HMAC" /> hash algorithm for computing the hash value.</summary>
		/// <param name="rgb">The input data. </param>
		/// <param name="ib">The offset into the byte array from which to begin using data. </param>
		/// <param name="cb">The number of bytes in the array to use as data. </param>
		protected override void HashCore(byte[] rgb, int ib, int cb)
		{
			if (_disposed)
			{
				throw new ObjectDisposedException("HMACSHA1");
			}
			if (State == 0)
			{
				Initialize();
				State = 1;
			}
			Block.Core(rgb, ib, cb);
		}

		/// <summary>When overridden in a derived class, finalizes the hash computation after the last data is processed by the cryptographic stream object.</summary>
		/// <returns>The computed hash code in a byte array.</returns>
		protected override byte[] HashFinal()
		{
			if (_disposed)
			{
				throw new ObjectDisposedException("HMAC");
			}
			State = 0;
			Block.Final();
			byte[] hash = _algo.Hash;
			byte[] array = KeySetup(Key, 92);
			_algo.Initialize();
			_algo.TransformBlock(array, 0, array.Length, array, 0);
			_algo.TransformFinalBlock(hash, 0, hash.Length);
			byte[] hash2 = _algo.Hash;
			_algo.Initialize();
			Array.Clear(array, 0, array.Length);
			Array.Clear(hash, 0, hash.Length);
			return hash2;
		}

		/// <summary>Initializes an instance of the default implementation of <see cref="T:System.Security.Cryptography.HMAC" />.</summary>
		public override void Initialize()
		{
			if (_disposed)
			{
				throw new ObjectDisposedException("HMAC");
			}
			State = 0;
			Block.Initialize();
			byte[] array = KeySetup(Key, 54);
			_algo.Initialize();
			Block.Core(array);
			Array.Clear(array, 0, array.Length);
		}

		/// <summary>Creates an instance of the default implementation of a Hash-based Message Authentication Code (HMAC).</summary>
		/// <returns>A new instance of an HMAC.</returns>
		/// <PermissionSet>
		///   <IPermission class="System.Security.Permissions.SecurityPermission, mscorlib, Version=2.0.3600.0, Culture=neutral, PublicKeyToken=b77a5c561934e089" version="1" Flags="UnmanagedCode" />
		/// </PermissionSet>
		public new static HMAC Create()
		{
			return Create("System.Security.Cryptography.HMAC");
		}

		/// <summary>Creates an instance of the default implementation of a Hash-based Message Authentication Code (HMAC).</summary>
		/// <returns>A new instance of the specified HMAC implementation.</returns>
		/// <param name="algorithmName">The HMAC implementation to use. </param>
		public new static HMAC Create(string algorithmName)
		{
			return (HMAC)CryptoConfig.CreateFromName(algorithmName);
		}
	}
}
