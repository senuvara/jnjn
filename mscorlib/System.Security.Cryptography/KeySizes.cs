namespace System.Security.Cryptography
{
	/// <summary>Determines the set of valid key sizes for the symmetric cryptographic algorithms.</summary>
	public sealed class KeySizes
	{
		private int _maxSize;

		private int _minSize;

		private int _skipSize;

		/// <summary>Specifies the maximum key size in bits.</summary>
		/// <returns>The maximum key size in bits.</returns>
		public int MaxSize => _maxSize;

		/// <summary>Specifies the minimum key size in bits.</summary>
		/// <returns>The minimum key size in bits.</returns>
		public int MinSize => _minSize;

		/// <summary>Specifies the interval between valid key sizes in bits.</summary>
		/// <returns>The interval between valid key sizes in bits.</returns>
		public int SkipSize => _skipSize;

		/// <summary>Initializes a new instance of the <see cref="T:System.Security.Cryptography.KeySizes" /> class with the specified key values.</summary>
		/// <returns>The newly created instance of <see cref="T:System.Security.Cryptography.KeySizes" />.</returns>
		/// <param name="minSize">The minimum valid key size. </param>
		/// <param name="maxSize">The maximum valid key size. </param>
		/// <param name="skipSize">The interval between valid key sizes. </param>
		public KeySizes(int minSize, int maxSize, int skipSize)
		{
			_maxSize = maxSize;
			_minSize = minSize;
			_skipSize = skipSize;
		}

		internal bool IsLegal(int keySize)
		{
			int num = keySize - MinSize;
			bool flag = num >= 0 && keySize <= MaxSize;
			return (SkipSize == 0) ? flag : (flag && num % SkipSize == 0);
		}

		internal static bool IsLegalKeySize(KeySizes[] legalKeys, int size)
		{
			foreach (KeySizes keySizes in legalKeys)
			{
				if (keySizes.IsLegal(size))
				{
					return true;
				}
			}
			return false;
		}
	}
}
