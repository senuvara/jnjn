namespace System.Runtime.InteropServices
{
	/// <summary>Use <see cref="T:System.Runtime.InteropServices.ComTypes.IDLFLAG" /> instead.</summary>
	[Serializable]
	[Obsolete]
	[Flags]
	public enum IDLFLAG
	{
		/// <summary>Whether the parameter passes or receives information is unspecified.</summary>
		IDLFLAG_NONE = 0x0,
		/// <summary>The parameter passes information from the caller to the callee.</summary>
		IDLFLAG_FIN = 0x1,
		/// <summary>The parameter returns information from the callee to the caller.</summary>
		IDLFLAG_FOUT = 0x2,
		/// <summary>The parameter is the local identifier of a client application.</summary>
		IDLFLAG_FLCID = 0x4,
		/// <summary>The parameter is the return value of the member.</summary>
		IDLFLAG_FRETVAL = 0x8
	}
}
