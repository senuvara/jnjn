namespace System.Runtime.InteropServices
{
	/// <summary>Use <see cref="T:System.Runtime.InteropServices.ComTypes.PARAMFLAG" /> instead.</summary>
	[Serializable]
	[Obsolete]
	[Flags]
	public enum PARAMFLAG
	{
		/// <summary>Whether the parameter passes or receives information is unspecified.</summary>
		PARAMFLAG_NONE = 0x0,
		/// <summary>The parameter passes information from the caller to the callee.</summary>
		PARAMFLAG_FIN = 0x1,
		/// <summary>The parameter returns information from the callee to the caller.</summary>
		PARAMFLAG_FOUT = 0x2,
		/// <summary>The parameter is the local identifier of a client application.</summary>
		PARAMFLAG_FLCID = 0x4,
		/// <summary>The parameter is the return value of the member.</summary>
		PARAMFLAG_FRETVAL = 0x8,
		/// <summary>The parameter is optional.</summary>
		PARAMFLAG_FOPT = 0x10,
		/// <summary>The parameter has default behaviors defined.</summary>
		PARAMFLAG_FHASDEFAULT = 0x20,
		/// <summary>The parameter has custom data.</summary>
		PARAMFLAG_FHASCUSTDATA = 0x40
	}
}
