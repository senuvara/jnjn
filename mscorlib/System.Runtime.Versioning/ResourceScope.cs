namespace System.Runtime.Versioning
{
	/// <summary>Identifies the scope of a sharable resource.</summary>
	[Flags]
	public enum ResourceScope
	{
		/// <summary>There is no shared state.</summary>
		None = 0x0,
		/// <summary>The state is shared by objects within the machine.</summary>
		Machine = 0x1,
		/// <summary>The state is shared within a process.</summary>
		Process = 0x2,
		/// <summary>The state is shared by objects within an <see cref="T:System.AppDomain" />.</summary>
		AppDomain = 0x4,
		/// <summary>The state is shared by objects within a library.</summary>
		Library = 0x8,
		/// <summary>The resource is visible to only the type.</summary>
		Private = 0x10,
		/// <summary>The resource is visible at an assembly scope.</summary>
		Assembly = 0x20
	}
}
