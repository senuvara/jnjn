using System;

namespace UnityEngine.AI
{
	/// <summary>
	///   <para>Bitmask used for operating with debug data from the NavMesh build process.</para>
	/// </summary>
	[Flags]
	public enum NavMeshBuildDebugFlags
	{
		/// <summary>
		///   <para>No debug data from the NavMesh build process is taken into consideration.</para>
		/// </summary>
		None = 0x0,
		/// <summary>
		///   <para>The triangles of all the geometry that is used as a base for computing the new NavMesh.</para>
		/// </summary>
		InputGeometry = 0x1,
		/// <summary>
		///   <para>The voxels produced by rasterizing the source geometry into walkable and unwalkable areas.</para>
		/// </summary>
		Voxels = 0x2,
		/// <summary>
		///   <para>The segmentation of the traversable surfaces into smaller areas necessary for producing simple polygons.</para>
		/// </summary>
		Regions = 0x4,
		/// <summary>
		///   <para>The contours that follow precisely the edges of each surface region.</para>
		/// </summary>
		RawContours = 0x8,
		/// <summary>
		///   <para>Contours bounding each of the surface regions, described through fewer vertices and straighter edges compared to RawContours.</para>
		/// </summary>
		SimplifiedContours = 0x10,
		/// <summary>
		///   <para>Meshes of convex polygons constructed within the unified contours of adjacent regions.</para>
		/// </summary>
		PolygonMeshes = 0x20,
		/// <summary>
		///   <para>The triangulated meshes with height details that better approximate the source geometry.</para>
		/// </summary>
		PolygonMeshesDetail = 0x40,
		/// <summary>
		///   <para>All debug data from the NavMesh build process is taken into consideration.</para>
		/// </summary>
		All = 0x7F
	}
}
