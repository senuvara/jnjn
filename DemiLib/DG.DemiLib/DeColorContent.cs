using System;
using UnityEngine;

namespace DG.DemiLib
{
	[Serializable]
	public class DeColorContent
	{
		public DeSkinColor def = new DeSkinColor(Color.black, new Color(0.7f, 0.7f, 0.7f, 1f));

		public DeSkinColor critical = new DeSkinColor(new Color(1f, 0.9148073f, 19f / 34f, 1f), new Color(1f, 0.3881846f, 0.3014706f, 1f));

		public DeSkinColor toggleOn = new DeSkinColor(new Color(1f, 0.9686275f, 0.6980392f, 1f), new Color(69f / 85f, 1f, 0.5607843f, 1f));

		public DeSkinColor toggleOff = new DeSkinColor(0.36f, 0.5f);
	}
}
