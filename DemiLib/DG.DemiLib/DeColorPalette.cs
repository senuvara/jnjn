using System;

namespace DG.DemiLib
{
	[Serializable]
	public class DeColorPalette
	{
		public DeColorBG bg = new DeColorBG();

		public DeColorContent content = new DeColorContent();
	}
}
