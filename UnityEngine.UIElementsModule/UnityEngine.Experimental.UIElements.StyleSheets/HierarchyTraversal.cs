using System.Collections.Generic;
using UnityEngine.StyleSheets;

namespace UnityEngine.Experimental.UIElements.StyleSheets
{
	internal abstract class HierarchyTraversal
	{
		public abstract bool ShouldSkipElement(VisualElement element);

		public abstract bool OnRuleMatchedElement(RuleMatcher matcher, VisualElement element);

		public virtual void OnBeginElementTest(VisualElement element, List<RuleMatcher> ruleMatchers)
		{
		}

		public void BeginElementTest(VisualElement element, List<RuleMatcher> ruleMatchers)
		{
			OnBeginElementTest(element, ruleMatchers);
		}

		public virtual void ProcessMatchedRules(VisualElement element)
		{
		}

		internal void Traverse(VisualElement element, int depth, List<RuleMatcher> ruleMatchers)
		{
			if (ShouldSkipElement(element))
			{
				return;
			}
			int count = ruleMatchers.Count;
			BeginElementTest(element, ruleMatchers);
			int count2 = ruleMatchers.Count;
			for (int i = 0; i < count2; i++)
			{
				RuleMatcher matcher = ruleMatchers[i];
				if (matcher.depth >= depth && Match(element, ref matcher))
				{
					StyleSelector[] selectors = matcher.complexSelector.selectors;
					int num = matcher.simpleSelectorIndex + 1;
					int num2 = selectors.Length;
					if (num < num2)
					{
						RuleMatcher ruleMatcher = default(RuleMatcher);
						ruleMatcher.complexSelector = matcher.complexSelector;
						ruleMatcher.depth = ((selectors[num].previousRelationship != StyleSelectorRelationship.Child) ? int.MaxValue : (depth + 1));
						ruleMatcher.simpleSelectorIndex = num;
						ruleMatcher.sheet = matcher.sheet;
						RuleMatcher item = ruleMatcher;
						ruleMatchers.Add(item);
					}
					else if (OnRuleMatchedElement(matcher, element))
					{
						return;
					}
				}
			}
			ProcessMatchedRules(element);
			Recurse(element, depth, ruleMatchers);
			if (ruleMatchers.Count > count)
			{
				ruleMatchers.RemoveRange(count, ruleMatchers.Count - count);
			}
		}

		protected virtual void Recurse(VisualElement element, int depth, List<RuleMatcher> ruleMatchers)
		{
			for (int i = 0; i < element.shadow.childCount; i++)
			{
				VisualElement element2 = element.shadow[i];
				Traverse(element2, depth + 1, ruleMatchers);
			}
		}

		protected virtual bool MatchSelectorPart(VisualElement element, StyleSelector selector, StyleSelectorPart part)
		{
			bool result = true;
			switch (part.type)
			{
			case StyleSelectorType.Class:
				result = element.ClassListContains(part.value);
				break;
			case StyleSelectorType.ID:
				result = (element.name == part.value);
				break;
			case StyleSelectorType.Type:
				result = (element.typeName == part.value);
				break;
			case StyleSelectorType.PseudoClass:
			{
				int pseudoStates = (int)element.pseudoStates;
				result = ((selector.pseudoStateMask & pseudoStates) == selector.pseudoStateMask);
				result &= ((selector.negatedPseudoStateMask & ~pseudoStates) == selector.negatedPseudoStateMask);
				break;
			}
			default:
				result = false;
				break;
			case StyleSelectorType.Wildcard:
				break;
			}
			return result;
		}

		public virtual bool Match(VisualElement element, ref RuleMatcher matcher)
		{
			bool flag = true;
			StyleSelector styleSelector = matcher.complexSelector.selectors[matcher.simpleSelectorIndex];
			int num = styleSelector.parts.Length;
			for (int i = 0; i < num; i++)
			{
				if (!flag)
				{
					break;
				}
				flag = MatchSelectorPart(element, styleSelector, styleSelector.parts[i]);
			}
			return flag;
		}
	}
}
