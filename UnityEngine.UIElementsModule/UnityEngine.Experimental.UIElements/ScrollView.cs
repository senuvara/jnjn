using System;

namespace UnityEngine.Experimental.UIElements
{
	public class ScrollView : VisualElement
	{
		public static readonly Vector2 kDefaultScrollerValues = new Vector2(0f, 100f);

		private VisualElement m_ContentContainer;

		public Vector2 horizontalScrollerValues
		{
			get;
			set;
		}

		public Vector2 verticalScrollerValues
		{
			get;
			set;
		}

		public bool showHorizontal
		{
			get;
			set;
		}

		public bool showVertical
		{
			get;
			set;
		}

		public bool needsHorizontal => showHorizontal || contentContainer.layout.width - base.layout.width > 0f;

		public bool needsVertical => showVertical || contentContainer.layout.height - base.layout.height > 0f;

		public Vector2 scrollOffset
		{
			get
			{
				return new Vector2(horizontalScroller.value, verticalScroller.value);
			}
			set
			{
				if (value != scrollOffset)
				{
					horizontalScroller.value = value.x;
					verticalScroller.value = value.y;
					UpdateContentViewTransform();
				}
			}
		}

		private float scrollableWidth => contentContainer.layout.width - contentViewport.layout.width;

		private float scrollableHeight => contentContainer.layout.height - contentViewport.layout.height;

		public VisualElement contentViewport
		{
			get;
			private set;
		}

		[Obsolete("Please use contentContainer instead", false)]
		public VisualElement contentView => contentContainer;

		public Scroller horizontalScroller
		{
			get;
			private set;
		}

		public Scroller verticalScroller
		{
			get;
			private set;
		}

		public override VisualElement contentContainer => m_ContentContainer;

		public ScrollView()
			: this(kDefaultScrollerValues, kDefaultScrollerValues)
		{
		}

		public ScrollView(Vector2 horizontalScrollerValues, Vector2 verticalScrollerValues)
		{
			this.horizontalScrollerValues = horizontalScrollerValues;
			this.verticalScrollerValues = verticalScrollerValues;
			contentViewport = new VisualElement
			{
				name = "ContentViewport"
			};
			contentViewport.clippingOptions = ClippingOptions.ClipContents;
			base.shadow.Add(contentViewport);
			m_ContentContainer = new VisualElement
			{
				name = "ContentView"
			};
			contentViewport.Add(m_ContentContainer);
			horizontalScroller = new Scroller(horizontalScrollerValues.x, horizontalScrollerValues.y, delegate(float value)
			{
				Vector2 scrollOffset2 = scrollOffset;
				scrollOffset = new Vector2(value, scrollOffset2.y);
			}, Slider.Direction.Horizontal)
			{
				name = "HorizontalScroller",
				persistenceKey = "HorizontalScroller"
			};
			base.shadow.Add(horizontalScroller);
			verticalScroller = new Scroller(verticalScrollerValues.x, verticalScrollerValues.y, delegate(float value)
			{
				Vector2 scrollOffset = this.scrollOffset;
				this.scrollOffset = new Vector2(scrollOffset.x, value);
			})
			{
				name = "VerticalScroller",
				persistenceKey = "VerticalScroller"
			};
			base.shadow.Add(verticalScroller);
			RegisterCallback<WheelEvent>(OnScrollWheel);
		}

		private void UpdateContentViewTransform()
		{
			Vector3 position = contentContainer.transform.position;
			Vector2 scrollOffset = this.scrollOffset;
			position.x = 0f - scrollOffset.x;
			position.y = 0f - scrollOffset.y;
			contentContainer.transform.position = position;
			Dirty(ChangeType.Repaint);
		}

		protected internal override void ExecuteDefaultAction(EventBase evt)
		{
			base.ExecuteDefaultAction(evt);
			if (evt.GetEventTypeId() == EventBase<PostLayoutEvent>.TypeId())
			{
				PostLayoutEvent postLayoutEvent = (PostLayoutEvent)evt;
				OnPostLayout(postLayoutEvent.hasNewLayout);
			}
		}

		private void OnPostLayout(bool hasNewLayout)
		{
			if (!hasNewLayout)
			{
				return;
			}
			if (contentContainer.layout.width > Mathf.Epsilon)
			{
				horizontalScroller.Adjust(contentViewport.layout.width / contentContainer.layout.width);
			}
			if (contentContainer.layout.height > Mathf.Epsilon)
			{
				verticalScroller.Adjust(contentViewport.layout.height / contentContainer.layout.height);
			}
			horizontalScroller.SetEnabled(contentContainer.layout.width - base.layout.width > 0f);
			verticalScroller.SetEnabled(contentContainer.layout.height - base.layout.height > 0f);
			contentViewport.style.positionRight = ((!needsVertical) ? 0f : verticalScroller.layout.width);
			horizontalScroller.style.positionRight = ((!needsVertical) ? 0f : verticalScroller.layout.width);
			contentViewport.style.positionBottom = ((!needsHorizontal) ? 0f : horizontalScroller.layout.height);
			verticalScroller.style.positionBottom = ((!needsHorizontal) ? 0f : horizontalScroller.layout.height);
			if (needsHorizontal)
			{
				horizontalScroller.lowValue = 0f;
				horizontalScroller.highValue = scrollableWidth;
			}
			else
			{
				horizontalScroller.value = 0f;
			}
			if (needsVertical)
			{
				verticalScroller.lowValue = 0f;
				verticalScroller.highValue = scrollableHeight;
			}
			else
			{
				verticalScroller.value = 0f;
			}
			if (horizontalScroller.visible != needsHorizontal)
			{
				horizontalScroller.visible = needsHorizontal;
				if (needsHorizontal)
				{
					contentViewport.AddToClassList("HorizontalScroll");
				}
				else
				{
					contentViewport.RemoveFromClassList("HorizontalScroll");
				}
			}
			if (verticalScroller.visible != needsVertical)
			{
				verticalScroller.visible = needsVertical;
				if (needsVertical)
				{
					contentViewport.AddToClassList("VerticalScroll");
				}
				else
				{
					contentViewport.RemoveFromClassList("VerticalScroll");
				}
			}
			UpdateContentViewTransform();
		}

		private void OnScrollWheel(WheelEvent evt)
		{
			if (contentContainer.layout.height - base.layout.height > 0f)
			{
				Vector3 delta = evt.delta;
				if (delta.y < 0f)
				{
					verticalScroller.ScrollPageUp();
				}
				else
				{
					Vector3 delta2 = evt.delta;
					if (delta2.y > 0f)
					{
						verticalScroller.ScrollPageDown();
					}
				}
			}
			evt.StopPropagation();
		}
	}
}
