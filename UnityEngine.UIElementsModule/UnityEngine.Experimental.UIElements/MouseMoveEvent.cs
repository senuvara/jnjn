namespace UnityEngine.Experimental.UIElements
{
	/// <summary>
	///   <para>Mouse move event.</para>
	/// </summary>
	public class MouseMoveEvent : MouseEventBase<MouseMoveEvent>
	{
	}
}
