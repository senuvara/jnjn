using System;
using UnityEngine.Experimental.UIElements.StyleSheets;

namespace UnityEngine.Experimental.UIElements
{
	/// <summary>
	///   <para>A textfield is a rectangular area where the user can edit a string.</para>
	/// </summary>
	public class TextField : VisualContainer
	{
		/// <summary>
		///   <para>Action that is called whenever the text changes in the textfield.</para>
		/// </summary>
		public Action<string> OnTextChanged;

		/// <summary>
		///   <para>Action that is called when the user validates the text in the textfield.</para>
		/// </summary>
		public Action OnTextChangeValidated;

		private const string SelectionColorProperty = "selection-color";

		private const string CursorColorProperty = "cursor-color";

		private StyleValue<Color> m_SelectionColor;

		private StyleValue<Color> m_CursorColor;

		private bool m_Multiline;

		private bool m_IsPasswordField;

		internal const int kMaxLengthNone = -1;

		/// <summary>
		///   <para>The color of the text selection.</para>
		/// </summary>
		public Color selectionColor => m_SelectionColor.GetSpecifiedValueOrDefault(Color.clear);

		/// <summary>
		///   <para>The color of the cursor.</para>
		/// </summary>
		public Color cursorColor => m_CursorColor.GetSpecifiedValueOrDefault(Color.clear);

		/// <summary>
		///   <para>Set this to true to allow multiple lines in the textfield and false if otherwise.</para>
		/// </summary>
		public bool multiline
		{
			get
			{
				return m_Multiline;
			}
			set
			{
				m_Multiline = value;
				if (!value)
				{
					base.text = base.text.Replace("\n", "");
				}
			}
		}

		/// <summary>
		///   <para>Set this to true to mask the characters and false if otherwise.</para>
		/// </summary>
		public bool isPasswordField
		{
			get
			{
				return m_IsPasswordField;
			}
			set
			{
				m_IsPasswordField = value;
				if (value)
				{
					multiline = false;
				}
			}
		}

		/// <summary>
		///   <para>The character used for masking in a password field.</para>
		/// </summary>
		public char maskChar
		{
			get;
			set;
		}

		/// <summary>
		///   <para>Set this to true to allow double-clicks to select the word under the mouse and false if otherwise.</para>
		/// </summary>
		public bool doubleClickSelectsWord
		{
			get;
			set;
		}

		/// <summary>
		///   <para>Set this to true to allow triple-clicks to select the line under the mouse and false if otherwise.</para>
		/// </summary>
		public bool tripleClickSelectsLine
		{
			get;
			set;
		}

		/// <summary>
		///   <para>The maximum number of characters this textfield can hold. If 0, there is no limit.</para>
		/// </summary>
		public int maxLength
		{
			get;
			set;
		}

		private bool touchScreenTextField => TouchScreenKeyboard.isSupported;

		/// <summary>
		///   <para>Returns true if the textfield has the focus and false if otherwise.</para>
		/// </summary>
		public bool hasFocus => base.elementPanel != null && base.elementPanel.focusController.focusedElement == this;

		internal TextEditor editor
		{
			get;
			set;
		}

		/// <summary>
		///   <para>Creates a new textfield.</para>
		/// </summary>
		/// <param name="maxLength">The maximum number of characters this textfield can hold. If 0, there is no limit.</param>
		/// <param name="multiline">Set this to true to allow multiple lines in the textfield and false if otherwise.</param>
		/// <param name="isPasswordField">Set this to true to mask the characters and false if otherwise.</param>
		/// <param name="maskChar">The character used for masking in a password field.</param>
		public TextField()
			: this(-1, multiline: false, isPasswordField: false, '\0')
		{
		}

		/// <summary>
		///   <para>Creates a new textfield.</para>
		/// </summary>
		/// <param name="maxLength">The maximum number of characters this textfield can hold. If 0, there is no limit.</param>
		/// <param name="multiline">Set this to true to allow multiple lines in the textfield and false if otherwise.</param>
		/// <param name="isPasswordField">Set this to true to mask the characters and false if otherwise.</param>
		/// <param name="maskChar">The character used for masking in a password field.</param>
		public TextField(int maxLength, bool multiline, bool isPasswordField, char maskChar)
		{
			this.maxLength = maxLength;
			this.multiline = multiline;
			this.isPasswordField = isPasswordField;
			this.maskChar = maskChar;
			if (touchScreenTextField)
			{
				editor = new TouchScreenTextEditor(this);
			}
			else
			{
				doubleClickSelectsWord = true;
				tripleClickSelectsLine = true;
				editor = new KeyboardTextEditor(this);
			}
			editor.style = new GUIStyle(editor.style);
			base.focusIndex = 0;
			this.AddManipulator(editor);
		}

		internal void TextFieldChanged()
		{
			if (OnTextChanged != null)
			{
				OnTextChanged(base.text);
			}
		}

		internal void TextFieldChangeValidated()
		{
			if (OnTextChangeValidated != null)
			{
				OnTextChangeValidated();
			}
		}

		/// <summary>
		///   <para>Called when the persistent data is accessible and/or when the data or persistence key have changed (VisualElement is properly parented).</para>
		/// </summary>
		public override void OnPersistentDataReady()
		{
			base.OnPersistentDataReady();
			string fullHierarchicalPersistenceKey = GetFullHierarchicalPersistenceKey();
			OverwriteFromPersistedData(this, fullHierarchicalPersistenceKey);
		}

		public override void OnStyleResolved(ICustomStyle style)
		{
			base.OnStyleResolved(style);
			base.effectiveStyle.ApplyCustomProperty("selection-color", ref m_SelectionColor);
			base.effectiveStyle.ApplyCustomProperty("cursor-color", ref m_CursorColor);
			base.effectiveStyle.WriteToGUIStyle(editor.style);
		}

		internal override void DoRepaint(IStylePainter painter)
		{
			if (touchScreenTextField)
			{
				TouchScreenTextEditor touchScreenTextEditor = editor as TouchScreenTextEditor;
				if (touchScreenTextEditor != null && touchScreenTextEditor.keyboardOnScreen != null)
				{
					base.text = touchScreenTextEditor.keyboardOnScreen.text;
					if (editor.maxLength >= 0 && base.text != null && base.text.Length > editor.maxLength)
					{
						base.text = base.text.Substring(0, editor.maxLength);
					}
					if (touchScreenTextEditor.keyboardOnScreen.done)
					{
						touchScreenTextEditor.keyboardOnScreen = null;
						GUI.changed = true;
					}
				}
				string text = base.text;
				if (touchScreenTextEditor != null && !string.IsNullOrEmpty(touchScreenTextEditor.secureText))
				{
					text = "".PadRight(touchScreenTextEditor.secureText.Length, maskChar);
				}
				base.DoRepaint(painter);
				base.text = text;
			}
			else if (isPasswordField)
			{
				string text2 = base.text;
				base.text = "".PadRight(base.text.Length, maskChar);
				if (!hasFocus)
				{
					base.DoRepaint(painter);
				}
				else
				{
					DrawWithTextSelectionAndCursor(painter, base.text);
				}
				base.text = text2;
			}
			else if (!hasFocus)
			{
				base.DoRepaint(painter);
			}
			else
			{
				DrawWithTextSelectionAndCursor(painter, base.text);
			}
		}

		private void DrawWithTextSelectionAndCursor(IStylePainter painter, string newText)
		{
			KeyboardTextEditor keyboardTextEditor = editor as KeyboardTextEditor;
			if (keyboardTextEditor == null)
			{
				return;
			}
			keyboardTextEditor.PreDrawCursor(newText);
			int cursorIndex = keyboardTextEditor.cursorIndex;
			int selectIndex = keyboardTextEditor.selectIndex;
			Rect localPosition = keyboardTextEditor.localPosition;
			Vector2 scrollOffset = keyboardTextEditor.scrollOffset;
			IStyle style = base.style;
			TextStylePainterParameters defaultTextParameters = painter.GetDefaultTextParameters(this);
			defaultTextParameters.text = " ";
			defaultTextParameters.wordWrapWidth = 0f;
			defaultTextParameters.wordWrap = false;
			float num = painter.ComputeTextHeight(defaultTextParameters);
			float num2 = (!style.wordWrap) ? 0f : base.contentRect.width;
			Input.compositionCursorPos = keyboardTextEditor.graphicalCursorPos - scrollOffset + new Vector2(localPosition.x, localPosition.y + num);
			Color color = (!(cursorColor != Color.clear)) ? GUI.skin.settings.cursorColor : cursorColor;
			int num3 = (!string.IsNullOrEmpty(Input.compositionString)) ? (cursorIndex + Input.compositionString.Length) : selectIndex;
			painter.DrawBackground(this);
			CursorPositionStylePainterParameters defaultCursorPositionParameters;
			if (cursorIndex != num3)
			{
				RectStylePainterParameters defaultRectParameters = painter.GetDefaultRectParameters(this);
				defaultRectParameters.color = selectionColor;
				defaultRectParameters.borderLeftWidth = 0f;
				defaultRectParameters.borderTopWidth = 0f;
				defaultRectParameters.borderRightWidth = 0f;
				defaultRectParameters.borderBottomWidth = 0f;
				defaultRectParameters.borderTopLeftRadius = 0f;
				defaultRectParameters.borderTopRightRadius = 0f;
				defaultRectParameters.borderBottomRightRadius = 0f;
				defaultRectParameters.borderBottomLeftRadius = 0f;
				int cursorIndex2 = (cursorIndex >= num3) ? num3 : cursorIndex;
				int cursorIndex3 = (cursorIndex <= num3) ? num3 : cursorIndex;
				defaultCursorPositionParameters = painter.GetDefaultCursorPositionParameters(this);
				defaultCursorPositionParameters.text = keyboardTextEditor.text;
				defaultCursorPositionParameters.wordWrapWidth = num2;
				defaultCursorPositionParameters.cursorIndex = cursorIndex2;
				Vector2 cursorPosition = painter.GetCursorPosition(defaultCursorPositionParameters);
				defaultCursorPositionParameters.cursorIndex = cursorIndex3;
				Vector2 cursorPosition2 = painter.GetCursorPosition(defaultCursorPositionParameters);
				cursorPosition -= scrollOffset;
				cursorPosition2 -= scrollOffset;
				if (Mathf.Approximately(cursorPosition.y, cursorPosition2.y))
				{
					defaultRectParameters.layout = new Rect(cursorPosition.x, cursorPosition.y, cursorPosition2.x - cursorPosition.x, num);
					painter.DrawRect(defaultRectParameters);
				}
				else
				{
					defaultRectParameters.layout = new Rect(cursorPosition.x, cursorPosition.y, num2 - cursorPosition.x, num);
					painter.DrawRect(defaultRectParameters);
					float num4 = cursorPosition2.y - cursorPosition.y - num;
					if (num4 > 0f)
					{
						defaultRectParameters.layout = new Rect(0f, cursorPosition.y + num, num2, num4);
						painter.DrawRect(defaultRectParameters);
					}
					defaultRectParameters.layout = new Rect(0f, cursorPosition2.y, cursorPosition2.x, num);
					painter.DrawRect(defaultRectParameters);
				}
			}
			painter.DrawBorder(this);
			if (!string.IsNullOrEmpty(keyboardTextEditor.text) && base.contentRect.width > 0f && base.contentRect.height > 0f)
			{
				defaultTextParameters = painter.GetDefaultTextParameters(this);
				defaultTextParameters.layout = new Rect(base.contentRect.x - scrollOffset.x, base.contentRect.y - scrollOffset.y, base.contentRect.width, base.contentRect.height);
				defaultTextParameters.text = keyboardTextEditor.text;
				painter.DrawText(defaultTextParameters);
			}
			RectStylePainterParameters rectStylePainterParameters;
			if (cursorIndex == num3 && (Font)style.font != null)
			{
				defaultCursorPositionParameters = painter.GetDefaultCursorPositionParameters(this);
				defaultCursorPositionParameters.text = keyboardTextEditor.text;
				defaultCursorPositionParameters.wordWrapWidth = num2;
				defaultCursorPositionParameters.cursorIndex = cursorIndex;
				Vector2 cursorPosition3 = painter.GetCursorPosition(defaultCursorPositionParameters);
				cursorPosition3 -= scrollOffset;
				rectStylePainterParameters = default(RectStylePainterParameters);
				rectStylePainterParameters.layout = new Rect(cursorPosition3.x, cursorPosition3.y, 1f, num);
				rectStylePainterParameters.color = color;
				RectStylePainterParameters painterParams = rectStylePainterParameters;
				painter.DrawRect(painterParams);
			}
			if (keyboardTextEditor.altCursorPosition != -1)
			{
				defaultCursorPositionParameters = painter.GetDefaultCursorPositionParameters(this);
				defaultCursorPositionParameters.text = keyboardTextEditor.text.Substring(0, keyboardTextEditor.altCursorPosition);
				defaultCursorPositionParameters.wordWrapWidth = num2;
				defaultCursorPositionParameters.cursorIndex = keyboardTextEditor.altCursorPosition;
				Vector2 cursorPosition4 = painter.GetCursorPosition(defaultCursorPositionParameters);
				cursorPosition4 -= scrollOffset;
				rectStylePainterParameters = default(RectStylePainterParameters);
				rectStylePainterParameters.layout = new Rect(cursorPosition4.x, cursorPosition4.y, 1f, num);
				rectStylePainterParameters.color = color;
				RectStylePainterParameters painterParams2 = rectStylePainterParameters;
				painter.DrawRect(painterParams2);
			}
			keyboardTextEditor.PostDrawCursor();
		}
	}
}
