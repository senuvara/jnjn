#define UNITY_ASSERTIONS
using System.Collections.Generic;

namespace UnityEngine.Experimental.UIElements
{
	internal class EventDispatcher : IEventDispatcher
	{
		private struct PropagationPaths
		{
			public List<VisualElement> capturePath;

			public List<VisualElement> bubblePath;

			public PropagationPaths(int initialSize)
			{
				capturePath = new List<VisualElement>(initialSize);
				bubblePath = new List<VisualElement>(initialSize);
			}
		}

		private VisualElement m_TopElementUnderMouse;

		public IEventHandler capture
		{
			get;
			set;
		}

		public void ReleaseCapture(IEventHandler handler)
		{
			Debug.Assert(handler == capture, "Element releasing capture does not have capture");
			capture = null;
		}

		public void RemoveCapture()
		{
			if (capture != null)
			{
				capture.OnLostCapture();
			}
			capture = null;
		}

		public void TakeCapture(IEventHandler handler)
		{
			if (capture != handler)
			{
				if (GUIUtility.hotControl != 0)
				{
					Debug.Log("Should not be capturing when there is a hotcontrol");
					return;
				}
				RemoveCapture();
				capture = handler;
			}
		}

		private void DispatchMouseEnterMouseLeave(VisualElement previousTopElementUnderMouse, VisualElement currentTopElementUnderMouse, IMouseEvent triggerEvent)
		{
			if (previousTopElementUnderMouse != currentTopElementUnderMouse)
			{
				int num = 0;
				VisualElement visualElement;
				for (visualElement = previousTopElementUnderMouse; visualElement != null; visualElement = visualElement.shadow.parent)
				{
					num++;
				}
				int num2 = 0;
				VisualElement visualElement2;
				for (visualElement2 = currentTopElementUnderMouse; visualElement2 != null; visualElement2 = visualElement2.shadow.parent)
				{
					num2++;
				}
				visualElement = previousTopElementUnderMouse;
				visualElement2 = currentTopElementUnderMouse;
				while (num > num2)
				{
					MouseLeaveEvent pooled = MouseEventBase<MouseLeaveEvent>.GetPooled(triggerEvent);
					pooled.target = visualElement;
					DispatchEvent(pooled, visualElement.panel);
					EventBase<MouseLeaveEvent>.ReleasePooled(pooled);
					num--;
					visualElement = visualElement.shadow.parent;
				}
				List<VisualElement> list = new List<VisualElement>(num2);
				while (num2 > num)
				{
					list.Add(visualElement2);
					num2--;
					visualElement2 = visualElement2.shadow.parent;
				}
				while (visualElement != visualElement2)
				{
					MouseLeaveEvent pooled2 = MouseEventBase<MouseLeaveEvent>.GetPooled(triggerEvent);
					pooled2.target = visualElement;
					DispatchEvent(pooled2, visualElement.panel);
					EventBase<MouseLeaveEvent>.ReleasePooled(pooled2);
					list.Add(visualElement2);
					visualElement = visualElement.shadow.parent;
					visualElement2 = visualElement2.shadow.parent;
				}
				for (int num3 = list.Count - 1; num3 >= 0; num3--)
				{
					MouseEnterEvent pooled3 = MouseEventBase<MouseEnterEvent>.GetPooled(triggerEvent);
					pooled3.target = list[num3];
					DispatchEvent(pooled3, list[num3].panel);
					EventBase<MouseEnterEvent>.ReleasePooled(pooled3);
				}
			}
		}

		private void DispatchMouseOverMouseOut(VisualElement previousTopElementUnderMouse, VisualElement currentTopElementUnderMouse, IMouseEvent triggerEvent)
		{
			if (previousTopElementUnderMouse != currentTopElementUnderMouse)
			{
				if (previousTopElementUnderMouse != null)
				{
					MouseOutEvent pooled = MouseEventBase<MouseOutEvent>.GetPooled(triggerEvent);
					pooled.target = previousTopElementUnderMouse;
					DispatchEvent(pooled, previousTopElementUnderMouse.panel);
					EventBase<MouseOutEvent>.ReleasePooled(pooled);
				}
				if (currentTopElementUnderMouse != null)
				{
					MouseOverEvent pooled2 = MouseEventBase<MouseOverEvent>.GetPooled(triggerEvent);
					pooled2.target = currentTopElementUnderMouse;
					DispatchEvent(pooled2, currentTopElementUnderMouse.panel);
					EventBase<MouseOverEvent>.ReleasePooled(pooled2);
				}
			}
		}

		public void DispatchEvent(EventBase evt, IPanel panel)
		{
			Event imguiEvent = evt.imguiEvent;
			if (imguiEvent != null && imguiEvent.type == EventType.Repaint)
			{
				return;
			}
			bool flag = false;
			VisualElement visualElement = capture as VisualElement;
			if (visualElement != null && visualElement.panel == null)
			{
				Debug.Log(string.Format("Capture has no panel, forcing removal (capture={0} eventType={1})", capture, (imguiEvent == null) ? "null" : imguiEvent.type.ToString()));
				RemoveCapture();
				visualElement = null;
			}
			if ((evt is IMouseEvent || imguiEvent != null) && capture != null)
			{
				if (panel != null && visualElement != null && visualElement.panel.contextType != panel.contextType)
				{
					return;
				}
				flag = true;
				evt.dispatch = true;
				evt.target = capture;
				evt.currentTarget = capture;
				evt.propagationPhase = PropagationPhase.AtTarget;
				capture.HandleEvent(evt);
				evt.propagationPhase = PropagationPhase.None;
				evt.currentTarget = null;
				evt.dispatch = false;
			}
			if (!evt.isPropagationStopped)
			{
				if (evt is IKeyboardEvent)
				{
					if (panel.focusController.focusedElement != null)
					{
						IMGUIContainer iMGUIContainer = panel.focusController.focusedElement as IMGUIContainer;
						flag = true;
						if (iMGUIContainer != null)
						{
							if (iMGUIContainer.HandleIMGUIEvent(evt.imguiEvent))
							{
								evt.StopPropagation();
								evt.PreventDefault();
							}
						}
						else
						{
							evt.target = panel.focusController.focusedElement;
							PropagateEvent(evt);
						}
					}
					else
					{
						evt.target = panel.visualTree;
						PropagateEvent(evt);
						flag = false;
					}
				}
				else if (evt.GetEventTypeId() == EventBase<MouseEnterEvent>.TypeId() || evt.GetEventTypeId() == EventBase<MouseLeaveEvent>.TypeId())
				{
					Debug.Assert(evt.target != null);
					flag = true;
					PropagateEvent(evt);
				}
				else if (evt is IMouseEvent || (imguiEvent != null && (imguiEvent.type == EventType.ContextClick || imguiEvent.type == EventType.MouseEnterWindow || imguiEvent.type == EventType.MouseLeaveWindow || imguiEvent.type == EventType.DragUpdated || imguiEvent.type == EventType.DragPerform || imguiEvent.type == EventType.DragExited)))
				{
					VisualElement topElementUnderMouse = m_TopElementUnderMouse;
					if (imguiEvent != null && imguiEvent.type == EventType.MouseLeaveWindow)
					{
						m_TopElementUnderMouse = null;
						DispatchMouseEnterMouseLeave(topElementUnderMouse, m_TopElementUnderMouse, evt as IMouseEvent);
						DispatchMouseOverMouseOut(topElementUnderMouse, m_TopElementUnderMouse, evt as IMouseEvent);
					}
					else if (evt is IMouseEvent || imguiEvent != null)
					{
						if (evt.target == null)
						{
							if (evt is IMouseEvent)
							{
								m_TopElementUnderMouse = panel.Pick((evt as IMouseEvent).localMousePosition);
							}
							else if (imguiEvent != null)
							{
								m_TopElementUnderMouse = panel.Pick(imguiEvent.mousePosition);
							}
							evt.target = m_TopElementUnderMouse;
						}
						if (evt.target != null)
						{
							flag = true;
							PropagateEvent(evt);
						}
						if (evt.GetEventTypeId() == EventBase<MouseMoveEvent>.TypeId())
						{
							DispatchMouseEnterMouseLeave(topElementUnderMouse, m_TopElementUnderMouse, evt as IMouseEvent);
							DispatchMouseOverMouseOut(topElementUnderMouse, m_TopElementUnderMouse, evt as IMouseEvent);
						}
					}
				}
				else if (imguiEvent != null && (imguiEvent.type == EventType.ExecuteCommand || imguiEvent.type == EventType.ValidateCommand))
				{
					IMGUIContainer iMGUIContainer2 = panel.focusController.focusedElement as IMGUIContainer;
					if (iMGUIContainer2 != null)
					{
						flag = true;
						if (iMGUIContainer2.HandleIMGUIEvent(evt.imguiEvent))
						{
							evt.StopPropagation();
							evt.PreventDefault();
						}
					}
					else if (panel.focusController.focusedElement != null)
					{
						flag = true;
						evt.target = panel.focusController.focusedElement;
						PropagateEvent(evt);
					}
				}
				else if (evt is IPropagatableEvent)
				{
					Debug.Assert(evt.target != null);
					flag = true;
					PropagateEvent(evt);
				}
			}
			if (!evt.isPropagationStopped && imguiEvent != null && (!flag || (imguiEvent != null && (imguiEvent.type == EventType.MouseEnterWindow || imguiEvent.type == EventType.MouseLeaveWindow || imguiEvent.type == EventType.Used))))
			{
				PropagateToIMGUIContainer(panel.visualTree, evt, visualElement);
			}
			if (evt.target == null)
			{
				evt.target = panel.visualTree;
			}
			ExecuteDefaultAction(evt);
		}

		private static void PropagateToIMGUIContainer(VisualElement root, EventBase evt, VisualElement capture)
		{
			IMGUIContainer iMGUIContainer = root as IMGUIContainer;
			if (iMGUIContainer != null && (evt.imguiEvent.type == EventType.Used || root != capture))
			{
				if (iMGUIContainer.HandleIMGUIEvent(evt.imguiEvent))
				{
					evt.StopPropagation();
					evt.PreventDefault();
				}
			}
			else
			{
				if (root == null)
				{
					return;
				}
				for (int i = 0; i < root.shadow.childCount; i++)
				{
					PropagateToIMGUIContainer(root.shadow[i], evt, capture);
					if (evt.isPropagationStopped)
					{
						break;
					}
				}
			}
		}

		private static void PropagateEvent(EventBase evt)
		{
			if (evt.dispatch)
			{
				return;
			}
			PropagationPaths propagationPaths = BuildPropagationPath(evt.target as VisualElement);
			evt.dispatch = true;
			if (evt.capturable && propagationPaths.capturePath.Count > 0)
			{
				evt.propagationPhase = PropagationPhase.Capture;
				int num = propagationPaths.capturePath.Count - 1;
				while (num >= 0 && !evt.isPropagationStopped)
				{
					evt.currentTarget = propagationPaths.capturePath[num];
					evt.currentTarget.HandleEvent(evt);
					num--;
				}
			}
			if (!evt.isPropagationStopped)
			{
				evt.propagationPhase = PropagationPhase.AtTarget;
				evt.currentTarget = evt.target;
				evt.currentTarget.HandleEvent(evt);
			}
			if (evt.bubbles && propagationPaths.bubblePath.Count > 0)
			{
				evt.propagationPhase = PropagationPhase.BubbleUp;
				for (int i = 0; i < propagationPaths.bubblePath.Count; i++)
				{
					if (evt.isPropagationStopped)
					{
						break;
					}
					evt.currentTarget = propagationPaths.bubblePath[i];
					evt.currentTarget.HandleEvent(evt);
				}
			}
			evt.dispatch = false;
			evt.propagationPhase = PropagationPhase.None;
			evt.currentTarget = null;
		}

		private static void ExecuteDefaultAction(EventBase evt)
		{
			if (!evt.isDefaultPrevented && evt.target != null)
			{
				evt.dispatch = true;
				evt.currentTarget = evt.target;
				evt.propagationPhase = PropagationPhase.DefaultAction;
				evt.currentTarget.HandleEvent(evt);
				evt.propagationPhase = PropagationPhase.None;
				evt.currentTarget = null;
				evt.dispatch = false;
			}
		}

		private static PropagationPaths BuildPropagationPath(VisualElement elem)
		{
			PropagationPaths result = new PropagationPaths(16);
			if (elem == null)
			{
				return result;
			}
			while (elem.shadow.parent != null)
			{
				if (elem.shadow.parent.enabledInHierarchy)
				{
					if (elem.shadow.parent.HasCaptureHandlers())
					{
						result.capturePath.Add(elem.shadow.parent);
					}
					if (elem.shadow.parent.HasBubbleHandlers())
					{
						result.bubblePath.Add(elem.shadow.parent);
					}
				}
				elem = elem.shadow.parent;
			}
			return result;
		}
	}
}
