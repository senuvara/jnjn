namespace UnityEngine.Experimental.UIElements
{
	/// <summary>
	///   <para>Event sent when a key is released on the keyboard. Capturable, bubbles, cancellable.</para>
	/// </summary>
	public class KeyUpEvent : KeyboardEventBase<KeyUpEvent>
	{
	}
}
