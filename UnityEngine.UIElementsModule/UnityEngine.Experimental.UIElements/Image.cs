namespace UnityEngine.Experimental.UIElements
{
	public class Image : VisualElement
	{
		public Texture image
		{
			get;
			set;
		}

		public ScaleMode scaleMode
		{
			get;
			set;
		}

		public Image()
		{
			scaleMode = ScaleMode.ScaleAndCrop;
		}

		protected internal override Vector2 DoMeasure(float width, MeasureMode widthMode, float height, MeasureMode heightMode)
		{
			float x = float.NaN;
			float y = float.NaN;
			if (image == null)
			{
				return new Vector2(x, y);
			}
			x = image.width;
			y = image.height;
			if (widthMode == MeasureMode.AtMost)
			{
				x = Mathf.Min(x, width);
			}
			if (heightMode == MeasureMode.AtMost)
			{
				y = Mathf.Min(y, height);
			}
			return new Vector2(x, y);
		}

		internal override void DoRepaint(IStylePainter painter)
		{
			if (image == null)
			{
				Debug.LogWarning("null texture passed to GUI.DrawTexture");
				return;
			}
			TextureStylePainterParameters textureStylePainterParameters = default(TextureStylePainterParameters);
			textureStylePainterParameters.layout = base.contentRect;
			textureStylePainterParameters.texture = image;
			textureStylePainterParameters.color = GUI.color;
			textureStylePainterParameters.scaleMode = scaleMode;
			TextureStylePainterParameters painterParams = textureStylePainterParameters;
			painter.DrawTexture(painterParams);
		}
	}
}
