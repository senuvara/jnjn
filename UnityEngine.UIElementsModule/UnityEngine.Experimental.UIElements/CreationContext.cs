using System.Collections.Generic;
using System.Runtime.InteropServices;

namespace UnityEngine.Experimental.UIElements
{
	/// <summary>
	///   <para>This class is used during UXML template instantiation.</para>
	/// </summary>
	[StructLayout(LayoutKind.Sequential, Size = 1)]
	public struct CreationContext
	{
		public static readonly CreationContext Default = default(CreationContext);

		public VisualElement target
		{
			get;
		}

		public VisualTreeAsset visualTreeAsset
		{
			get;
		}

		public Dictionary<string, VisualElement> slotInsertionPoints
		{
			get;
		}

		internal CreationContext(Dictionary<string, VisualElement> slotInsertionPoints, VisualTreeAsset vta, VisualElement target)
		{
			this.target = target;
			this.slotInsertionPoints = slotInsertionPoints;
			visualTreeAsset = vta;
		}
	}
}
