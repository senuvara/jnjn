namespace UnityEngine.Experimental.UIElements
{
	internal class TouchScreenTextEditor : TextEditor
	{
		private string m_SecureText;

		public string secureText
		{
			get
			{
				return m_SecureText;
			}
			set
			{
				string text = value ?? string.Empty;
				if (text != m_SecureText)
				{
					m_SecureText = text;
				}
			}
		}

		public TouchScreenTextEditor(TextField textField)
			: base(textField)
		{
			secureText = string.Empty;
		}

		protected override void RegisterCallbacksOnTarget()
		{
			base.target.RegisterCallback<MouseDownEvent>(OnMouseUpDownEvent);
		}

		protected override void UnregisterCallbacksFromTarget()
		{
			base.target.UnregisterCallback<MouseDownEvent>(OnMouseUpDownEvent);
		}

		private void OnMouseUpDownEvent(MouseDownEvent evt)
		{
			SyncTextEditor();
			base.textField.TakeCapture();
			keyboardOnScreen = TouchScreenKeyboard.Open(string.IsNullOrEmpty(secureText) ? base.textField.text : secureText, TouchScreenKeyboardType.Default, autocorrection: true, multiline, !string.IsNullOrEmpty(secureText));
			UpdateScrollOffset();
			evt.StopPropagation();
		}
	}
}
