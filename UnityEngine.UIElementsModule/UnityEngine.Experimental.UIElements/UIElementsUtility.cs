#define UNITY_ASSERTIONS
using System;
using System.Collections.Generic;
using System.Runtime.CompilerServices;

namespace UnityEngine.Experimental.UIElements
{
	internal class UIElementsUtility
	{
		private static Stack<IMGUIContainer> s_ContainerStack;

		private static Dictionary<int, Panel> s_UIElementsCache;

		private static Event s_EventInstance;

		private static EventDispatcher s_EventDispatcher;

		[CompilerGenerated]
		private static Action _003C_003Ef__mg_0024cache0;

		[CompilerGenerated]
		private static Action _003C_003Ef__mg_0024cache1;

		[CompilerGenerated]
		private static Func<int, IntPtr, bool> _003C_003Ef__mg_0024cache2;

		[CompilerGenerated]
		private static Action _003C_003Ef__mg_0024cache3;

		[CompilerGenerated]
		private static Func<Exception, bool> _003C_003Ef__mg_0024cache4;

		internal static IEventDispatcher eventDispatcher
		{
			get
			{
				if (s_EventDispatcher == null)
				{
					s_EventDispatcher = new EventDispatcher();
				}
				return s_EventDispatcher;
			}
		}

		static UIElementsUtility()
		{
			s_ContainerStack = new Stack<IMGUIContainer>();
			s_UIElementsCache = new Dictionary<int, Panel>();
			s_EventInstance = new Event();
			GUIUtility.takeCapture = (Action)Delegate.Combine(GUIUtility.takeCapture, new Action(TakeCapture));
			GUIUtility.releaseCapture = (Action)Delegate.Combine(GUIUtility.releaseCapture, new Action(ReleaseCapture));
			GUIUtility.processEvent = (Func<int, IntPtr, bool>)Delegate.Combine(GUIUtility.processEvent, new Func<int, IntPtr, bool>(ProcessEvent));
			GUIUtility.cleanupRoots = (Action)Delegate.Combine(GUIUtility.cleanupRoots, new Action(CleanupRoots));
			GUIUtility.endContainerGUIFromException = (Func<Exception, bool>)Delegate.Combine(GUIUtility.endContainerGUIFromException, new Func<Exception, bool>(EndContainerGUIFromException));
		}

		internal static void ClearDispatcher()
		{
			s_EventDispatcher = null;
		}

		private static void TakeCapture()
		{
			if (s_ContainerStack.Count <= 0)
			{
				return;
			}
			IMGUIContainer iMGUIContainer = s_ContainerStack.Peek();
			if (iMGUIContainer.GUIDepth == GUIUtility.Internal_GetGUIDepth())
			{
				if (eventDispatcher.capture != null && eventDispatcher.capture != iMGUIContainer)
				{
					Debug.Log($"Should not grab hot control with an active capture (current={eventDispatcher.capture} new={iMGUIContainer}");
				}
				eventDispatcher.TakeCapture(iMGUIContainer);
			}
		}

		private static void ReleaseCapture()
		{
			eventDispatcher.RemoveCapture();
		}

		private static bool ProcessEvent(int instanceID, IntPtr nativeEventPtr)
		{
			if (nativeEventPtr != IntPtr.Zero && s_UIElementsCache.TryGetValue(instanceID, out Panel value))
			{
				s_EventInstance.CopyFromPtr(nativeEventPtr);
				return DoDispatch(value);
			}
			return false;
		}

		public static void RemoveCachedPanel(int instanceID)
		{
			s_UIElementsCache.Remove(instanceID);
		}

		private static void CleanupRoots()
		{
			s_EventInstance = null;
			s_EventDispatcher = null;
			s_UIElementsCache = null;
			s_ContainerStack = null;
		}

		private static bool EndContainerGUIFromException(Exception exception)
		{
			if (s_ContainerStack.Count > 0)
			{
				GUIUtility.EndContainer();
				s_ContainerStack.Pop();
			}
			return GUIUtility.ShouldRethrowException(exception);
		}

		internal static void BeginContainerGUI(GUILayoutUtility.LayoutCache cache, Event evt, IMGUIContainer container)
		{
			if (container.useOwnerObjectGUIState)
			{
				GUIUtility.BeginContainerFromOwner(container.elementPanel.ownerObject);
			}
			else
			{
				GUIUtility.BeginContainer(container.guiState);
			}
			s_ContainerStack.Push(container);
			GUIUtility.s_SkinMode = (int)container.contextType;
			GUIUtility.s_OriginalID = container.elementPanel.ownerObject.GetInstanceID();
			Event.current = evt;
			GUI.enabled = container.enabledInHierarchy;
			GUILayoutUtility.BeginContainer(cache);
			GUIUtility.ResetGlobalState();
			Rect clipRect = container.lastWorldClip;
			if (clipRect.width == 0f || clipRect.height == 0f)
			{
				clipRect = container.worldBound;
			}
			Matrix4x4 lhs = container.worldTransform;
			if (evt.type == EventType.Repaint && container.elementPanel != null && container.elementPanel.stylePainter != null)
			{
				lhs = container.elementPanel.stylePainter.currentTransform;
			}
			GUIClip.SetTransform(lhs * Matrix4x4.Translate(container.layout.position), clipRect);
		}

		internal static void EndContainerGUI()
		{
			if (Event.current.type == EventType.Layout && s_ContainerStack.Count > 0)
			{
				Rect layout = s_ContainerStack.Peek().layout;
				GUILayoutUtility.LayoutFromContainer(layout.width, layout.height);
			}
			GUILayoutUtility.SelectIDList(GUIUtility.s_OriginalID, isWindow: false);
			GUIContent.ClearStaticCache();
			if (s_ContainerStack.Count > 0)
			{
				GUIUtility.EndContainer();
				s_ContainerStack.Pop();
			}
		}

		internal static ContextType GetGUIContextType()
		{
			return (GUIUtility.s_SkinMode != 0) ? ContextType.Editor : ContextType.Player;
		}

		internal static EventBase CreateEvent(Event systemEvent)
		{
			switch (systemEvent.type)
			{
			case EventType.MouseMove:
				return MouseEventBase<MouseMoveEvent>.GetPooled(systemEvent);
			case EventType.MouseDrag:
				return MouseEventBase<MouseMoveEvent>.GetPooled(systemEvent);
			case EventType.MouseDown:
				return MouseEventBase<MouseDownEvent>.GetPooled(systemEvent);
			case EventType.MouseUp:
				return MouseEventBase<MouseUpEvent>.GetPooled(systemEvent);
			case EventType.ScrollWheel:
				return WheelEvent.GetPooled(systemEvent);
			case EventType.KeyDown:
				return KeyboardEventBase<KeyDownEvent>.GetPooled(systemEvent);
			case EventType.KeyUp:
				return KeyboardEventBase<KeyUpEvent>.GetPooled(systemEvent);
			default:
				return IMGUIEvent.GetPooled(systemEvent);
			}
		}

		internal static void ReleaseEvent(EventBase evt)
		{
			long eventTypeId = evt.GetEventTypeId();
			if (eventTypeId == EventBase<MouseMoveEvent>.TypeId())
			{
				EventBase<MouseMoveEvent>.ReleasePooled((MouseMoveEvent)evt);
			}
			else if (eventTypeId == EventBase<MouseDownEvent>.TypeId())
			{
				EventBase<MouseDownEvent>.ReleasePooled((MouseDownEvent)evt);
			}
			else if (eventTypeId == EventBase<MouseUpEvent>.TypeId())
			{
				EventBase<MouseUpEvent>.ReleasePooled((MouseUpEvent)evt);
			}
			else if (eventTypeId == EventBase<WheelEvent>.TypeId())
			{
				EventBase<WheelEvent>.ReleasePooled((WheelEvent)evt);
			}
			else if (eventTypeId == EventBase<KeyDownEvent>.TypeId())
			{
				EventBase<KeyDownEvent>.ReleasePooled((KeyDownEvent)evt);
			}
			else if (eventTypeId == EventBase<KeyUpEvent>.TypeId())
			{
				EventBase<KeyUpEvent>.ReleasePooled((KeyUpEvent)evt);
			}
			else if (eventTypeId == EventBase<IMGUIEvent>.TypeId())
			{
				EventBase<IMGUIEvent>.ReleasePooled((IMGUIEvent)evt);
			}
		}

		private static bool DoDispatch(BaseVisualElementPanel panel)
		{
			bool result;
			if (s_EventInstance.type == EventType.Repaint)
			{
				panel.Repaint(s_EventInstance);
				result = (panel.IMGUIContainersCount > 0);
			}
			else
			{
				panel.ValidateLayout();
				EventBase eventBase = CreateEvent(s_EventInstance);
				Vector2 mousePosition = s_EventInstance.mousePosition;
				s_EventDispatcher.DispatchEvent(eventBase, panel);
				s_EventInstance.mousePosition = mousePosition;
				if (eventBase.isPropagationStopped)
				{
					panel.visualTree.Dirty(ChangeType.Repaint);
				}
				result = eventBase.isPropagationStopped;
				ReleaseEvent(eventBase);
			}
			return result;
		}

		internal static Dictionary<int, Panel>.Enumerator GetPanelsIterator()
		{
			return s_UIElementsCache.GetEnumerator();
		}

		internal static Panel FindOrCreatePanel(ScriptableObject ownerObject, ContextType contextType, IDataWatchService dataWatch = null)
		{
			if (!s_UIElementsCache.TryGetValue(ownerObject.GetInstanceID(), out Panel value))
			{
				value = new Panel(ownerObject, contextType, dataWatch, eventDispatcher);
				s_UIElementsCache.Add(ownerObject.GetInstanceID(), value);
			}
			else
			{
				Debug.Assert(contextType == value.contextType, "Context type mismatch");
			}
			return value;
		}

		internal static Panel FindOrCreatePanel(ScriptableObject ownerObject)
		{
			return FindOrCreatePanel(ownerObject, GetGUIContextType());
		}
	}
}
