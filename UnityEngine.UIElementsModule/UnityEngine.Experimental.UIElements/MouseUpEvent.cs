namespace UnityEngine.Experimental.UIElements
{
	/// <summary>
	///   <para>Mouse up event.</para>
	/// </summary>
	public class MouseUpEvent : MouseEventBase<MouseUpEvent>
	{
	}
}
