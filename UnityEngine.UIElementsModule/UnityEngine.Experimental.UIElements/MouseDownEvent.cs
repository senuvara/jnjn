namespace UnityEngine.Experimental.UIElements
{
	/// <summary>
	///   <para>Mouse down event.</para>
	/// </summary>
	public class MouseDownEvent : MouseEventBase<MouseDownEvent>
	{
	}
}
