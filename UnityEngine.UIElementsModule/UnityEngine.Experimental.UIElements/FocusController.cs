namespace UnityEngine.Experimental.UIElements
{
	/// <summary>
	///   <para>Class in charge of managing the focus inside a Panel.</para>
	/// </summary>
	public class FocusController
	{
		private IFocusRing focusRing
		{
			get;
			set;
		}

		/// <summary>
		///   <para>The currently focused element.</para>
		/// </summary>
		public Focusable focusedElement
		{
			get;
			private set;
		}

		internal int imguiKeyboardControl
		{
			get;
			set;
		}

		/// <summary>
		///   <para>Constructor.</para>
		/// </summary>
		/// <param name="focusRing"></param>
		public FocusController(IFocusRing focusRing)
		{
			this.focusRing = focusRing;
			focusedElement = null;
			imguiKeyboardControl = 0;
		}

		private static void AboutToReleaseFocus(Focusable focusable, Focusable willGiveFocusTo, FocusChangeDirection direction)
		{
			FocusOutEvent pooled = FocusEventBase<FocusOutEvent>.GetPooled(focusable, willGiveFocusTo, direction);
			UIElementsUtility.eventDispatcher.DispatchEvent(pooled, null);
			EventBase<FocusOutEvent>.ReleasePooled(pooled);
		}

		private static void ReleaseFocus(Focusable focusable, Focusable willGiveFocusTo, FocusChangeDirection direction)
		{
			BlurEvent pooled = FocusEventBase<BlurEvent>.GetPooled(focusable, willGiveFocusTo, direction);
			UIElementsUtility.eventDispatcher.DispatchEvent(pooled, null);
			EventBase<BlurEvent>.ReleasePooled(pooled);
		}

		private static void AboutToGrabFocus(Focusable focusable, Focusable willTakeFocusFrom, FocusChangeDirection direction)
		{
			FocusInEvent pooled = FocusEventBase<FocusInEvent>.GetPooled(focusable, willTakeFocusFrom, direction);
			UIElementsUtility.eventDispatcher.DispatchEvent(pooled, null);
			EventBase<FocusInEvent>.ReleasePooled(pooled);
		}

		private static void GrabFocus(Focusable focusable, Focusable willTakeFocusFrom, FocusChangeDirection direction)
		{
			FocusEvent pooled = FocusEventBase<FocusEvent>.GetPooled(focusable, willTakeFocusFrom, direction);
			UIElementsUtility.eventDispatcher.DispatchEvent(pooled, null);
			EventBase<FocusEvent>.ReleasePooled(pooled);
		}

		internal void SwitchFocus(Focusable newFocusedElement)
		{
			SwitchFocus(newFocusedElement, FocusChangeDirection.unspecified);
		}

		private void SwitchFocus(Focusable newFocusedElement, FocusChangeDirection direction)
		{
			if (newFocusedElement == this.focusedElement)
			{
				return;
			}
			Focusable focusedElement = this.focusedElement;
			if (newFocusedElement == null || !newFocusedElement.canGrabFocus)
			{
				if (focusedElement != null)
				{
					AboutToReleaseFocus(focusedElement, newFocusedElement, direction);
					this.focusedElement = null;
					ReleaseFocus(focusedElement, newFocusedElement, direction);
				}
			}
			else if (newFocusedElement != focusedElement)
			{
				if (focusedElement != null)
				{
					AboutToReleaseFocus(focusedElement, newFocusedElement, direction);
				}
				AboutToGrabFocus(newFocusedElement, focusedElement, direction);
				this.focusedElement = newFocusedElement;
				if (focusedElement != null)
				{
					ReleaseFocus(focusedElement, newFocusedElement, direction);
				}
				GrabFocus(newFocusedElement, focusedElement, direction);
			}
		}

		/// <summary>
		///   <para>Ask the controller to change the focus according to the event. The focus controller will use its focus ring to choose the next element to be focused.</para>
		/// </summary>
		/// <param name="e"></param>
		public void SwitchFocusOnEvent(EventBase e)
		{
			FocusChangeDirection focusChangeDirection = focusRing.GetFocusChangeDirection(focusedElement, e);
			if (focusChangeDirection != FocusChangeDirection.none)
			{
				Focusable nextFocusable = focusRing.GetNextFocusable(focusedElement, focusChangeDirection);
				SwitchFocus(nextFocusable, focusChangeDirection);
			}
		}

		internal void SyncIMGUIFocus(IMGUIContainer imguiContainerHavingKeyboardControl)
		{
			if (GUIUtility.keyboardControl != imguiKeyboardControl)
			{
				imguiKeyboardControl = GUIUtility.keyboardControl;
				if (GUIUtility.keyboardControl != 0)
				{
					SwitchFocus(imguiContainerHavingKeyboardControl, FocusChangeDirection.unspecified);
				}
				else
				{
					SwitchFocus(null, FocusChangeDirection.unspecified);
				}
			}
		}
	}
}
