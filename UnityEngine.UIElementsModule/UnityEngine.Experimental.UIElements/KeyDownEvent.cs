namespace UnityEngine.Experimental.UIElements
{
	/// <summary>
	///   <para>Event sent when a key is pressed on the keyboard. Capturable, bubbles, cancellable.</para>
	/// </summary>
	public class KeyDownEvent : KeyboardEventBase<KeyDownEvent>
	{
	}
}
