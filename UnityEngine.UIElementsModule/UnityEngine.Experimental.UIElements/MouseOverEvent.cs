namespace UnityEngine.Experimental.UIElements
{
	/// <summary>
	///   <para>Event sent when the mouse pointer enters an element. Capturable, bubbles, cancellable.</para>
	/// </summary>
	public class MouseOverEvent : MouseEventBase<MouseOverEvent>
	{
	}
}
