namespace UnityEngine.Experimental.UIElements
{
	/// <summary>
	///   <para>Event sent when the mouse pointer exits an element. Capturable, bubbles, cancellable.</para>
	/// </summary>
	public class MouseOutEvent : MouseEventBase<MouseOutEvent>
	{
	}
}
