using System;

namespace UnityEngine.Experimental.UIElements
{
	internal class KeyboardTextEditor : TextEditor
	{
		internal bool m_Changed;

		private bool m_Dragged;

		private bool m_DragToPosition = true;

		private bool m_PostPoneMove;

		private bool m_SelectAllOnMouseUp = true;

		private string m_PreDrawCursorText;

		public KeyboardTextEditor(TextField textField)
			: base(textField)
		{
		}

		protected override void RegisterCallbacksOnTarget()
		{
			base.RegisterCallbacksOnTarget();
			base.target.RegisterCallback<MouseDownEvent>(OnMouseDown);
			base.target.RegisterCallback<MouseUpEvent>(OnMouseUp);
			base.target.RegisterCallback<MouseMoveEvent>(OnMouseMove);
			base.target.RegisterCallback<KeyDownEvent>(OnKeyDown);
			base.target.RegisterCallback<IMGUIEvent>(OnIMGUIEvent);
		}

		protected override void UnregisterCallbacksFromTarget()
		{
			base.UnregisterCallbacksFromTarget();
			base.target.UnregisterCallback<MouseDownEvent>(OnMouseDown);
			base.target.UnregisterCallback<MouseUpEvent>(OnMouseUp);
			base.target.UnregisterCallback<MouseMoveEvent>(OnMouseMove);
			base.target.UnregisterCallback<KeyDownEvent>(OnKeyDown);
			base.target.UnregisterCallback<IMGUIEvent>(OnIMGUIEvent);
		}

		private void OnMouseDown(MouseDownEvent evt)
		{
			SyncTextEditor();
			m_Changed = false;
			base.target.TakeCapture();
			if (!m_HasFocus)
			{
				m_HasFocus = true;
				MoveCursorToPosition_Internal(evt.localMousePosition, evt.shiftKey);
				evt.StopPropagation();
			}
			else
			{
				if (evt.clickCount == 2 && base.doubleClickSelectsWord)
				{
					SelectCurrentWord();
					DblClickSnap(DblClickSnapping.WORDS);
					MouseDragSelectsWholeWords(on: true);
					m_DragToPosition = false;
				}
				else if (evt.clickCount == 3 && base.tripleClickSelectsLine)
				{
					SelectCurrentParagraph();
					MouseDragSelectsWholeWords(on: true);
					DblClickSnap(DblClickSnapping.PARAGRAPHS);
					m_DragToPosition = false;
				}
				else
				{
					MoveCursorToPosition_Internal(evt.localMousePosition, evt.shiftKey);
					m_SelectAllOnMouseUp = false;
				}
				evt.StopPropagation();
			}
			if (m_Changed)
			{
				if (base.maxLength >= 0 && base.text != null && base.text.Length > base.maxLength)
				{
					base.text = base.text.Substring(0, base.maxLength);
				}
				base.textField.text = base.text;
				base.textField.TextFieldChanged();
				evt.StopPropagation();
			}
			UpdateScrollOffset();
		}

		private void OnMouseUp(MouseUpEvent evt)
		{
			if (!base.target.HasCapture())
			{
				return;
			}
			SyncTextEditor();
			m_Changed = false;
			if (m_Dragged && m_DragToPosition)
			{
				MoveSelectionToAltCursor();
			}
			else if (m_PostPoneMove)
			{
				MoveCursorToPosition_Internal(evt.localMousePosition, evt.shiftKey);
			}
			else if (m_SelectAllOnMouseUp)
			{
				m_SelectAllOnMouseUp = false;
			}
			MouseDragSelectsWholeWords(on: false);
			base.target.ReleaseCapture();
			m_DragToPosition = true;
			m_Dragged = false;
			m_PostPoneMove = false;
			evt.StopPropagation();
			if (m_Changed)
			{
				if (base.maxLength >= 0 && base.text != null && base.text.Length > base.maxLength)
				{
					base.text = base.text.Substring(0, base.maxLength);
				}
				base.textField.text = base.text;
				base.textField.TextFieldChanged();
				evt.StopPropagation();
			}
			UpdateScrollOffset();
		}

		private void OnMouseMove(MouseMoveEvent evt)
		{
			if (!base.target.HasCapture())
			{
				return;
			}
			SyncTextEditor();
			m_Changed = false;
			if (!evt.shiftKey && base.hasSelection && m_DragToPosition)
			{
				MoveAltCursorToPosition(evt.localMousePosition);
			}
			else
			{
				if (evt.shiftKey)
				{
					MoveCursorToPosition_Internal(evt.localMousePosition, evt.shiftKey);
				}
				else
				{
					SelectToPosition(evt.localMousePosition);
				}
				m_DragToPosition = false;
				m_SelectAllOnMouseUp = !base.hasSelection;
			}
			m_Dragged = true;
			evt.StopPropagation();
			if (m_Changed)
			{
				if (base.maxLength >= 0 && base.text != null && base.text.Length > base.maxLength)
				{
					base.text = base.text.Substring(0, base.maxLength);
				}
				base.textField.text = base.text;
				base.textField.TextFieldChanged();
				evt.StopPropagation();
			}
			UpdateScrollOffset();
		}

		private void OnKeyDown(KeyDownEvent evt)
		{
			if (!base.textField.hasFocus)
			{
				return;
			}
			SyncTextEditor();
			m_Changed = false;
			if (HandleKeyEvent(evt.imguiEvent))
			{
				m_Changed = true;
				base.textField.text = base.text;
				evt.StopPropagation();
			}
			else
			{
				if (evt.keyCode == KeyCode.Tab || evt.character == '\t')
				{
					return;
				}
				char character = evt.character;
				if (character == '\n' && !multiline && !evt.altKey)
				{
					base.textField.TextFieldChangeValidated();
					return;
				}
				Font font = base.textField.editor.style.font;
				if ((font != null && font.HasCharacter(character)) || character == '\n')
				{
					Insert(character);
					m_Changed = true;
				}
				else if (character == '\0')
				{
					if (!string.IsNullOrEmpty(Input.compositionString))
					{
						ReplaceSelection("");
						m_Changed = true;
					}
					evt.StopPropagation();
				}
			}
			if (m_Changed)
			{
				if (base.maxLength >= 0 && base.text != null && base.text.Length > base.maxLength)
				{
					base.text = base.text.Substring(0, base.maxLength);
				}
				base.textField.text = base.text;
				base.textField.TextFieldChanged();
				evt.StopPropagation();
			}
			UpdateScrollOffset();
		}

		private void OnIMGUIEvent(IMGUIEvent evt)
		{
			if (!base.textField.hasFocus)
			{
				return;
			}
			SyncTextEditor();
			m_Changed = false;
			switch (evt.imguiEvent.type)
			{
			case EventType.ValidateCommand:
				switch (evt.imguiEvent.commandName)
				{
				case "Cut":
				case "Copy":
					if (!base.hasSelection)
					{
						return;
					}
					break;
				case "Paste":
					if (!CanPaste())
					{
						return;
					}
					break;
				}
				evt.StopPropagation();
				break;
			case EventType.ExecuteCommand:
			{
				bool flag = false;
				string text = base.text;
				if (!base.textField.hasFocus)
				{
					return;
				}
				switch (evt.imguiEvent.commandName)
				{
				case "OnLostFocus":
					evt.StopPropagation();
					return;
				case "Cut":
					Cut();
					flag = true;
					break;
				case "Copy":
					Copy();
					evt.StopPropagation();
					return;
				case "Paste":
					Paste();
					flag = true;
					break;
				case "SelectAll":
					SelectAll();
					evt.StopPropagation();
					return;
				case "Delete":
					if (SystemInfo.operatingSystemFamily == OperatingSystemFamily.MacOSX)
					{
						Delete();
					}
					else
					{
						Cut();
					}
					flag = true;
					break;
				}
				if (flag)
				{
					if (text != base.text)
					{
						m_Changed = true;
					}
					evt.StopPropagation();
				}
				break;
			}
			}
			if (m_Changed)
			{
				if (base.maxLength >= 0 && base.text != null && base.text.Length > base.maxLength)
				{
					base.text = base.text.Substring(0, base.maxLength);
				}
				base.textField.text = base.text;
				base.textField.TextFieldChanged();
				evt.StopPropagation();
			}
			UpdateScrollOffset();
		}

		public void PreDrawCursor(string newText)
		{
			SyncTextEditor();
			m_PreDrawCursorText = base.text;
			int num = base.cursorIndex;
			if (!string.IsNullOrEmpty(Input.compositionString))
			{
				base.text = newText.Substring(0, base.cursorIndex) + Input.compositionString + newText.Substring(base.selectIndex);
				num += Input.compositionString.Length;
			}
			else
			{
				base.text = newText;
			}
			if (base.maxLength >= 0 && base.text != null && base.text.Length > base.maxLength)
			{
				base.text = base.text.Substring(0, base.maxLength);
				num = Math.Min(num, base.maxLength - 1);
			}
			graphicalCursorPos = style.GetCursorPixelPosition(localPosition, new GUIContent(base.text), num);
		}

		public void PostDrawCursor()
		{
			base.text = m_PreDrawCursorText;
		}
	}
}
