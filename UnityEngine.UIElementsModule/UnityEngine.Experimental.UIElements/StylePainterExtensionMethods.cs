using UnityEngine.Experimental.UIElements.StyleSheets;

namespace UnityEngine.Experimental.UIElements
{
	internal static class StylePainterExtensionMethods
	{
		internal static TextureStylePainterParameters GetDefaultTextureParameters(this IStylePainter painter, VisualElement ve)
		{
			IStyle style = ve.style;
			TextureStylePainterParameters result = default(TextureStylePainterParameters);
			result.layout = ve.layout;
			result.color = Color.white;
			result.texture = (Texture2D)style.backgroundImage;
			result.scaleMode = style.backgroundSize;
			result.borderLeftWidth = style.borderLeftWidth;
			result.borderTopWidth = style.borderTopWidth;
			result.borderRightWidth = style.borderRightWidth;
			result.borderBottomWidth = style.borderBottomWidth;
			result.borderTopLeftRadius = style.borderTopLeftRadius;
			result.borderTopRightRadius = style.borderTopRightRadius;
			result.borderBottomRightRadius = style.borderBottomRightRadius;
			result.borderBottomLeftRadius = style.borderBottomLeftRadius;
			result.sliceLeft = style.sliceLeft;
			result.sliceTop = style.sliceTop;
			result.sliceRight = style.sliceRight;
			result.sliceBottom = style.sliceBottom;
			return result;
		}

		internal static RectStylePainterParameters GetDefaultRectParameters(this IStylePainter painter, VisualElement ve)
		{
			IStyle style = ve.style;
			RectStylePainterParameters result = default(RectStylePainterParameters);
			result.layout = ve.layout;
			result.color = style.backgroundColor;
			result.borderLeftWidth = style.borderLeftWidth;
			result.borderTopWidth = style.borderTopWidth;
			result.borderRightWidth = style.borderRightWidth;
			result.borderBottomWidth = style.borderBottomWidth;
			result.borderTopLeftRadius = style.borderTopLeftRadius;
			result.borderTopRightRadius = style.borderTopRightRadius;
			result.borderBottomRightRadius = style.borderBottomRightRadius;
			result.borderBottomLeftRadius = style.borderBottomLeftRadius;
			return result;
		}

		internal static TextStylePainterParameters GetDefaultTextParameters(this IStylePainter painter, VisualElement ve)
		{
			IStyle style = ve.style;
			TextStylePainterParameters result = default(TextStylePainterParameters);
			result.layout = ve.contentRect;
			result.text = ve.text;
			result.font = style.font;
			result.fontSize = style.fontSize;
			result.fontStyle = style.fontStyle;
			result.fontColor = style.textColor.GetSpecifiedValueOrDefault(Color.black);
			result.anchor = style.textAlignment;
			result.wordWrap = style.wordWrap;
			result.wordWrapWidth = ((!style.wordWrap) ? 0f : ve.contentRect.width);
			result.richText = false;
			result.clipping = style.textClipping;
			return result;
		}

		internal static CursorPositionStylePainterParameters GetDefaultCursorPositionParameters(this IStylePainter painter, VisualElement ve)
		{
			IStyle style = ve.style;
			CursorPositionStylePainterParameters result = default(CursorPositionStylePainterParameters);
			result.layout = ve.contentRect;
			result.text = ve.text;
			result.font = style.font;
			result.fontSize = style.fontSize;
			result.fontStyle = style.fontStyle;
			result.anchor = style.textAlignment;
			result.wordWrapWidth = ((!style.wordWrap) ? 0f : ve.contentRect.width);
			result.richText = false;
			result.cursorIndex = 0;
			return result;
		}

		internal static void DrawBackground(this IStylePainter painter, VisualElement ve)
		{
			IStyle style = ve.style;
			if (style.backgroundColor != Color.clear)
			{
				RectStylePainterParameters defaultRectParameters = painter.GetDefaultRectParameters(ve);
				defaultRectParameters.borderLeftWidth = 0f;
				defaultRectParameters.borderTopWidth = 0f;
				defaultRectParameters.borderRightWidth = 0f;
				defaultRectParameters.borderBottomWidth = 0f;
				painter.DrawRect(defaultRectParameters);
			}
			StyleValue<Texture2D> backgroundImage = style.backgroundImage;
			if (backgroundImage.value != null)
			{
				TextureStylePainterParameters defaultTextureParameters = painter.GetDefaultTextureParameters(ve);
				defaultTextureParameters.borderLeftWidth = 0f;
				defaultTextureParameters.borderTopWidth = 0f;
				defaultTextureParameters.borderRightWidth = 0f;
				defaultTextureParameters.borderBottomWidth = 0f;
				painter.DrawTexture(defaultTextureParameters);
			}
		}

		internal static void DrawBorder(this IStylePainter painter, VisualElement ve)
		{
			IStyle style = ve.style;
			if (style.borderColor != Color.clear && ((float)style.borderLeftWidth > 0f || (float)style.borderTopWidth > 0f || (float)style.borderRightWidth > 0f || (float)style.borderBottomWidth > 0f))
			{
				RectStylePainterParameters defaultRectParameters = painter.GetDefaultRectParameters(ve);
				defaultRectParameters.color = style.borderColor;
				painter.DrawRect(defaultRectParameters);
			}
		}

		internal static void DrawText(this IStylePainter painter, VisualElement ve)
		{
			if (!string.IsNullOrEmpty(ve.text) && ve.contentRect.width > 0f && ve.contentRect.height > 0f)
			{
				painter.DrawText(painter.GetDefaultTextParameters(ve));
			}
		}
	}
}
