using System;

namespace UnityEngine.Experimental.UIElements
{
	[Flags]
	internal enum PseudoStates
	{
		Active = 0x1,
		Hover = 0x2,
		Checked = 0x8,
		Selected = 0x10,
		Disabled = 0x20,
		Focus = 0x40,
		Invisible = int.MinValue
	}
}
