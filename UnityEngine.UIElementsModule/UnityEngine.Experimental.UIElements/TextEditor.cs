namespace UnityEngine.Experimental.UIElements
{
	public class TextEditor : UnityEngine.TextEditor, IManipulator
	{
		private VisualElement m_Target;

		public int maxLength
		{
			get;
			set;
		}

		public char maskChar
		{
			get;
			set;
		}

		public bool doubleClickSelectsWord
		{
			get;
			set;
		}

		public bool tripleClickSelectsLine
		{
			get;
			set;
		}

		protected TextField textField
		{
			get;
			set;
		}

		internal override Rect localPosition => new Rect(0f, 0f, base.position.width, base.position.height);

		public VisualElement target
		{
			get
			{
				return m_Target;
			}
			set
			{
				if (target != null)
				{
					UnregisterCallbacksFromTarget();
				}
				m_Target = value;
				if (target != null)
				{
					RegisterCallbacksOnTarget();
				}
			}
		}

		protected TextEditor(TextField textField)
		{
			this.textField = textField;
			SyncTextEditor();
		}

		protected virtual void RegisterCallbacksOnTarget()
		{
			target.RegisterCallback<FocusEvent>(OnFocus);
			target.RegisterCallback<BlurEvent>(OnBlur);
		}

		protected virtual void UnregisterCallbacksFromTarget()
		{
			target.UnregisterCallback<FocusEvent>(OnFocus);
			target.UnregisterCallback<BlurEvent>(OnBlur);
		}

		private void OnFocus(FocusEvent evt)
		{
			OnFocus();
		}

		private void OnBlur(BlurEvent evt)
		{
			OnLostFocus();
		}

		protected void SyncTextEditor()
		{
			string text = textField.text;
			if (maxLength >= 0 && text != null && text.Length > maxLength)
			{
				text = text.Substring(0, maxLength);
			}
			base.text = text;
			SaveBackup();
			base.position = textField.layout;
			maxLength = textField.maxLength;
			multiline = textField.multiline;
			isPasswordField = textField.isPasswordField;
			maskChar = textField.maskChar;
			doubleClickSelectsWord = textField.doubleClickSelectsWord;
			tripleClickSelectsLine = textField.tripleClickSelectsLine;
			DetectFocusChange();
		}

		internal override void OnDetectFocusChange()
		{
			if (m_HasFocus && !textField.hasFocus)
			{
				OnFocus();
			}
			if (!m_HasFocus && textField.hasFocus)
			{
				OnLostFocus();
			}
		}
	}
}
