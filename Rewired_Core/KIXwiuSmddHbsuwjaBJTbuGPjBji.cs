using Rewired;
using Rewired.Utils;
using Rewired.Utils.Attributes;
using Rewired.Utils.Classes.Data;
using Rewired.Utils.Libraries.TinyJson;
using System;
using System.Collections.Generic;
using UnityEngine;

internal sealed class KIXwiuSmddHbsuwjaBJTbuGPjBji
{
	[Serializable]
	[Preserve]
	public class UPEDbksMSUIEmKjlartPAAlIIDj
	{
		[SerializeField]
		[Serialize(Name = "tag")]
		private string _tag;

		[Serialize(Name = "enabled")]
		[SerializeField]
		private bool _enabled;

		[Serialize(Name = "categoryIds")]
		[SerializeField]
		private int[] _categoryIds;

		[SerializeField]
		[Serialize(Name = "layoutIds")]
		private int[] _layoutIds;

		[SerializeField]
		[Serialize(Name = "controllerSelector")]
		private ZVOPkHmpobXEknQJZShcFQBpbsRa _controllerSelector;

		[NonSerialized]
		private string[] rDlGKUDuuAoTZXVrfIUkxFBnSrr;

		[NonSerialized]
		private string[] fRsRNvQEMxCvvaDUjbOOoblRemug;

		[DoNotSerialize]
		public bool bIuTQDpJdxKPOSnzrcuXlibqkL
		{
			get
			{
				if (_layoutIds != null)
				{
					return _layoutIds.Length == 0;
				}
				return true;
			}
		}

		[DoNotSerialize]
		public string OUZkLKnsEYmoPVSOlDrULLUnzFD
		{
			get
			{
				return _tag;
			}
			set
			{
				_tag = value;
			}
		}

		[DoNotSerialize]
		public bool YvthbnNVBRzdDIZfdQsVTptORZS
		{
			get
			{
				return _enabled;
			}
			set
			{
				_enabled = value;
			}
		}

		[DoNotSerialize]
		public ZVOPkHmpobXEknQJZShcFQBpbsRa pMuDOCNOtKqkQVaEUcwrbdcrjzMc
		{
			get
			{
				return _controllerSelector ?? (_controllerSelector = new ZVOPkHmpobXEknQJZShcFQBpbsRa(ZVOPkHmpobXEknQJZShcFQBpbsRa.Type.ControllerType));
			}
			set
			{
				if (value == null)
				{
					value = new ZVOPkHmpobXEknQJZShcFQBpbsRa(ZVOPkHmpobXEknQJZShcFQBpbsRa.Type.ControllerType);
				}
				_controllerSelector = value;
			}
		}

		[DoNotSerialize]
		public int[] XvVDbgynVuDFOeJmbaeoQKtdIkNH
		{
			get
			{
				FzawOUbErNWuzjmqBpAPaNyVOxl();
				return _categoryIds ?? (_categoryIds = EmptyObjects<int>.array);
			}
			set
			{
				if (value == null)
				{
					value = EmptyObjects<int>.array;
				}
				_categoryIds = value;
				rDlGKUDuuAoTZXVrfIUkxFBnSrr = null;
			}
		}

		[DoNotSerialize]
		public int[] otJFFyToqhEfmlxQxlCdmYgIJgF
		{
			get
			{
				FzawOUbErNWuzjmqBpAPaNyVOxl();
				return _layoutIds ?? (_layoutIds = EmptyObjects<int>.array);
			}
			set
			{
				if (value == null)
				{
					value = EmptyObjects<int>.array;
					goto IL_000a;
				}
				goto IL_0057;
				IL_000a:
				int num = -808830332;
				goto IL_000f;
				IL_0057:
				_layoutIds = value;
				fRsRNvQEMxCvvaDUjbOOoblRemug = null;
				num = -808830331;
				goto IL_000f;
				IL_000f:
				while (true)
				{
					switch (num ^ -808830331)
					{
					default:
						return;
					case 3:
						return;
					case 2:
						break;
					case 4:
						ZdhdzExHhHqjNJcxvAYEctMJnvf();
						num = -808830330;
						continue;
					case 0:
						if (value != null)
						{
							goto case 4;
						}
						goto IL_0040;
					case 1:
						goto IL_0057;
					}
					break;
					IL_0040:
					int num2;
					if (value.Length > 0)
					{
						num = -808830335;
						num2 = num;
					}
					else
					{
						num = -808830330;
						num2 = num;
					}
				}
				goto IL_000a;
			}
		}

		[DoNotSerialize]
		public int ktfjKKxKmOGSnHhHDjmYrGAHoHz
		{
			get
			{
				FzawOUbErNWuzjmqBpAPaNyVOxl();
				while (true)
				{
					int num = 1495532950;
					while (true)
					{
						switch (num ^ 0x59240597)
						{
						case 2:
							break;
						case 1:
							if (_categoryIds != null)
							{
								if (_categoryIds.Length == 0)
								{
									goto IL_0036;
								}
								return XvVDbgynVuDFOeJmbaeoQKtdIkNH[0];
							}
							goto default;
						default:
							return -1;
						}
						break;
						IL_0036:
						num = 1495532951;
					}
				}
			}
			set
			{
				if (value < 0)
				{
					_categoryIds = EmptyObjects<int>.array;
					goto IL_000f;
				}
				goto IL_0063;
				IL_000f:
				int num = 544037054;
				goto IL_0014;
				IL_0063:
				if (_categoryIds != null)
				{
					int num2;
					if (_categoryIds.Length == 0)
					{
						num = 544037048;
						num2 = num;
					}
					else
					{
						num = 544037049;
						num2 = num;
					}
					goto IL_0014;
				}
				goto IL_0050;
				IL_0050:
				_categoryIds = new int[1];
				num = 544037049;
				goto IL_0014;
				IL_0014:
				while (true)
				{
					switch (num ^ 0x206D58BA)
					{
					case 5:
						break;
					case 4:
						num = 544037050;
						continue;
					case 3:
						_categoryIds[0] = value;
						num = 544037050;
						continue;
					case 2:
						goto IL_0050;
					case 1:
						goto IL_0063;
					default:
						rDlGKUDuuAoTZXVrfIUkxFBnSrr = null;
						return;
					}
					break;
				}
				goto IL_000f;
			}
		}

		[DoNotSerialize]
		public int jSVECUANwcgzIdTjgjLAcuUrKbOY
		{
			get
			{
				FzawOUbErNWuzjmqBpAPaNyVOxl();
				if (_layoutIds != null)
				{
					while (true)
					{
						int num = 11288940;
						while (true)
						{
							switch (num ^ 0xAC416D)
							{
							case 2:
								break;
							case 1:
								goto IL_002c;
							default:
								goto IL_003d;
							}
							break;
							IL_002c:
							if (_layoutIds.Length != 0)
							{
								return otJFFyToqhEfmlxQxlCdmYgIJgF[0];
							}
							num = 11288941;
						}
					}
				}
				goto IL_003d;
				IL_003d:
				return -1;
			}
			set
			{
				if (value >= 0)
				{
					goto IL_008a;
				}
				_layoutIds = EmptyObjects<int>.array;
				goto IL_00b6;
				IL_00b6:
				int num;
				int num2;
				if (value >= 0)
				{
					num = -370974829;
					num2 = num;
				}
				else
				{
					num = -370974825;
					num2 = num;
				}
				goto IL_001c;
				IL_008a:
				int num3;
				if (_layoutIds != null)
				{
					num = -370974826;
					num3 = num;
				}
				else
				{
					num = -370974827;
					num3 = num;
				}
				goto IL_001c;
				IL_001c:
				while (true)
				{
					switch (num ^ -370974828)
					{
					case 0:
						num = -370974832;
						continue;
					case 5:
						_layoutIds[0] = value;
						num = -370974830;
						continue;
					case 2:
						break;
					case 1:
						_layoutIds = new int[1];
						num = -370974831;
						continue;
					case 4:
						goto IL_008a;
					case 7:
						ZdhdzExHhHqjNJcxvAYEctMJnvf();
						num = -370974825;
						continue;
					case 6:
						goto IL_00b6;
					default:
						fRsRNvQEMxCvvaDUjbOOoblRemug = null;
						return;
					}
					int num4;
					if (_layoutIds.Length != 0)
					{
						num = -370974831;
						num4 = num;
					}
					else
					{
						num = -370974827;
						num4 = num;
					}
				}
			}
		}

		[DoNotSerialize]
		public string[] RiNqwyBzqcsIgSuQNFfFDRNPOPV
		{
			get
			{
				if (!ReInput.isReady)
				{
					goto IL_0007;
				}
				FzawOUbErNWuzjmqBpAPaNyVOxl();
				int num = 408103013;
				goto IL_000c;
				IL_0007:
				num = 408103015;
				goto IL_000c;
				IL_000c:
				string[] array = default(string[]);
				int num2 = default(int);
				InputMapCategory mapCategory = default(InputMapCategory);
				while (true)
				{
					switch (num ^ 0x18532863)
					{
					case 5:
						break;
					case 4:
						if (rDlGKUDuuAoTZXVrfIUkxFBnSrr == null)
						{
							return EmptyObjects<string>.array;
						}
						return rDlGKUDuuAoTZXVrfIUkxFBnSrr;
					case 0:
						array[num2] = ((mapCategory != null) ? mapCategory.name : "INVALID");
						num2++;
						num = 408103009;
						continue;
					case 3:
						num = 408103009;
						continue;
					case 1:
						mapCategory = ReInput.mapping.GetMapCategory(_categoryIds[num2]);
						num = 408103011;
						continue;
					case 6:
						if (_categoryIds == null)
						{
							return EmptyObjects<string>.array;
						}
						array = new string[_categoryIds.Length];
						num2 = 0;
						num = 408103008;
						continue;
					default:
						if (num2 >= _categoryIds.Length)
						{
							return array;
						}
						goto case 1;
					}
					break;
				}
				goto IL_0007;
			}
			set
			{
				//Discarded unreachable code: IL_0072, IL_00cf
				if (!ReInput.isReady)
				{
					rDlGKUDuuAoTZXVrfIUkxFBnSrr = ((value != null && value.Length > 0) ? value : null);
					goto IL_001d;
				}
				goto IL_00d9;
				IL_00d9:
				int num;
				int num2;
				if (value != null)
				{
					num = 1750609309;
					num2 = num;
				}
				else
				{
					num = 1750609307;
					num2 = num;
				}
				goto IL_0022;
				IL_0022:
				List<int> list = default(List<int>);
				int mapCategoryId = default(int);
				int num3 = default(int);
				while (true)
				{
					switch (num ^ 0x68582D9C)
					{
					default:
						return;
					case 10:
						return;
					case 9:
						break;
					case 11:
						_categoryIds = EmptyObjects<int>.array;
						return;
					case 1:
						goto IL_0079;
					case 5:
						_categoryIds = list.ToArray();
						num = 1750609302;
						continue;
					case 3:
						goto IL_00a2;
					case 7:
						rDlGKUDuuAoTZXVrfIUkxFBnSrr = null;
						_categoryIds = EmptyObjects<int>.array;
						return;
					case 2:
						goto IL_00d9;
					case 4:
						if (mapCategoryId >= 0)
						{
							list.Add(mapCategoryId);
							num = 1750609308;
							continue;
						}
						goto case 8;
					case 12:
						if (!string.IsNullOrEmpty(value[num3]))
						{
							mapCategoryId = ReInput.mapping.GetMapCategoryId(value[num3]);
							num = 1750609304;
							continue;
						}
						goto case 0;
					case 0:
						num3++;
						num = 1750609311;
						continue;
					case 8:
						Rewired.Logger.LogWarning("Map Category \"" + value[num3] + "\" does not exist.");
						num = 1750609308;
						continue;
					case 6:
						list = new List<int>(value.Length);
						num3 = 0;
						num = 1750609311;
						continue;
					}
					break;
					IL_00a2:
					int num4;
					if (num3 < value.Length)
					{
						num = 1750609296;
						num4 = num;
					}
					else
					{
						num = 1750609305;
						num4 = num;
					}
					continue;
					IL_0079:
					int num5;
					if (value.Length != 0)
					{
						num = 1750609306;
						num5 = num;
					}
					else
					{
						num = 1750609307;
						num5 = num;
					}
				}
				goto IL_001d;
				IL_001d:
				num = 1750609303;
				goto IL_0022;
			}
		}

		[DoNotSerialize]
		public string[] ucPYRkjDMBTEsbPRQgVKBgPIZDt
		{
			get
			{
				if (!ReInput.isReady)
				{
					if (fRsRNvQEMxCvvaDUjbOOoblRemug == null)
					{
						return EmptyObjects<string>.array;
					}
					return fRsRNvQEMxCvvaDUjbOOoblRemug;
				}
				FzawOUbErNWuzjmqBpAPaNyVOxl();
				int num2 = default(int);
				string[] array = default(string[]);
				while (true)
				{
					int num = 234698217;
					while (true)
					{
						switch (num ^ 0xDFD35ED)
						{
						case 0:
							break;
						case 3:
							num2++;
							num = 234698216;
							continue;
						case 1:
							return EmptyObjects<string>.array;
						case 2:
						{
							InputLayout layout = ReInput.mapping.GetLayout(pMuDOCNOtKqkQVaEUcwrbdcrjzMc.LBvurysqjogiYiEmqeJKfpvGlYKC, _layoutIds[num2]);
							array[num2] = ((layout != null) ? layout.name : "INVALID");
							num = 234698222;
							continue;
						}
						case 4:
							if (_layoutIds != null)
							{
								array = new string[_layoutIds.Length];
								num2 = 0;
								num = 234698216;
							}
							else
							{
								num = 234698220;
							}
							continue;
						default:
							if (num2 >= _layoutIds.Length)
							{
								return array;
							}
							goto case 2;
						}
						break;
					}
				}
			}
			set
			{
				//Discarded unreachable code: IL_005c, IL_00f2
				if (!ReInput.isReady)
				{
					goto IL_000a;
				}
				goto IL_00a3;
				IL_00a3:
				int num;
				if (value != null)
				{
					int num2;
					if (value.Length == 0)
					{
						num = 82816301;
						num2 = num;
					}
					else
					{
						num = 82816288;
						num2 = num;
					}
					goto IL_000f;
				}
				goto IL_0063;
				IL_0063:
				fRsRNvQEMxCvvaDUjbOOoblRemug = null;
				_layoutIds = EmptyObjects<int>.array;
				num = 82816295;
				goto IL_000f;
				IL_000f:
				List<int> list = default(List<int>);
				int num3 = default(int);
				while (true)
				{
					switch (num ^ 0x4EFAD2D)
					{
					case 10:
						return;
					case 11:
						break;
					case 0:
						goto IL_0063;
					case 1:
						ZdhdzExHhHqjNJcxvAYEctMJnvf();
						num = 82816292;
						continue;
					case 3:
						goto IL_0089;
					case 6:
						goto IL_00a3;
					case 5:
						num = 82816303;
						continue;
					case 8:
						num = 82816291;
						continue;
					case 9:
						fRsRNvQEMxCvvaDUjbOOoblRemug = ((value != null && value.Length > 0) ? value : null);
						_layoutIds = EmptyObjects<int>.array;
						return;
					case 7:
						goto IL_00fc;
					case 13:
						ZdhdzExHhHqjNJcxvAYEctMJnvf();
						list = new List<int>(value.Length);
						num3 = 0;
						num = 82816293;
						continue;
					case 4:
						Rewired.Logger.LogWarning("Layout \"" + value[num3] + "\" does not exist.");
						num = 82816303;
						continue;
					case 12:
						if (!string.IsNullOrEmpty(value[num3]))
						{
							int layoutId = ReInput.mapping.GetLayoutId(pMuDOCNOtKqkQVaEUcwrbdcrjzMc.LBvurysqjogiYiEmqeJKfpvGlYKC, value[num3]);
							if (layoutId >= 0)
							{
								list.Add(layoutId);
								num = 82816296;
								continue;
							}
							goto case 4;
						}
						goto case 2;
					case 2:
						num3++;
						num = 82816291;
						continue;
					default:
						if (num3 >= value.Length)
						{
							_layoutIds = list.ToArray();
							return;
						}
						goto case 12;
					}
					break;
					IL_00fc:
					int num4;
					if (value == null)
					{
						num = 82816292;
						num4 = num;
					}
					else
					{
						num = 82816302;
						num4 = num;
					}
					continue;
					IL_0089:
					int num5;
					if (value.Length <= 0)
					{
						num = 82816292;
						num5 = num;
					}
					else
					{
						num = 82816300;
						num5 = num;
					}
				}
				goto IL_000a;
				IL_000a:
				num = 82816298;
				goto IL_000f;
			}
		}

		[DoNotSerialize]
		public string rXpKUytZMFByafEBkZMEDOlYhej
		{
			get
			{
				//Discarded unreachable code: IL_00a1
				if (!ReInput.isReady)
				{
					if (rDlGKUDuuAoTZXVrfIUkxFBnSrr != null)
					{
						if (rDlGKUDuuAoTZXVrfIUkxFBnSrr.Length > 0)
						{
							return rDlGKUDuuAoTZXVrfIUkxFBnSrr[0];
						}
						goto IL_0023;
					}
					goto IL_00a8;
				}
				FzawOUbErNWuzjmqBpAPaNyVOxl();
				int num = 708896510;
				goto IL_0028;
				IL_0028:
				InputMapCategory mapCategory = default(InputMapCategory);
				int num2 = default(int);
				while (true)
				{
					switch (num ^ 0x2A40E6F8)
					{
					case 5:
						break;
					case 8:
						return "INVALID";
					case 7:
						goto IL_0069;
					case 4:
						mapCategory = ReInput.mapping.GetMapCategory(_categoryIds[num2]);
						num = 708896505;
						continue;
					case 2:
						goto IL_00a8;
					case 1:
						goto IL_00c3;
					case 6:
						if (_categoryIds != null)
						{
							goto IL_00d8;
						}
						goto case 0;
					case 0:
						return null;
					default:
						return "INVALID";
					}
					break;
					IL_00d8:
					if (_categoryIds.Length == 0)
					{
						num = 708896504;
						continue;
					}
					num2 = 0;
					num = 708896511;
					continue;
					IL_00c3:
					if (mapCategory != null)
					{
						return mapCategory.name;
					}
					num = 708896496;
					continue;
					IL_0069:
					int num3;
					if (num2 >= _categoryIds.Length)
					{
						num = 708896507;
						num3 = num;
					}
					else
					{
						num = 708896508;
						num3 = num;
					}
				}
				goto IL_0023;
				IL_00a8:
				return null;
				IL_0023:
				num = 708896506;
				goto IL_0028;
			}
			set
			{
				//Discarded unreachable code: IL_00b6, IL_00c8
				if (!ReInput.isReady)
				{
					rDlGKUDuuAoTZXVrfIUkxFBnSrr = ((!string.IsNullOrEmpty(value)) ? new string[1]
					{
						value
					} : null);
					_categoryIds = EmptyObjects<int>.array;
					return;
				}
				int mapCategoryId = default(int);
				while (true)
				{
					int num;
					if (string.IsNullOrEmpty(value))
					{
						rDlGKUDuuAoTZXVrfIUkxFBnSrr = null;
						_categoryIds = EmptyObjects<int>.array;
						num = 622691348;
						goto IL_0035;
					}
					goto IL_0086;
					IL_0035:
					while (true)
					{
						switch (num ^ 0x251D8411)
						{
						default:
							return;
						case 0:
							return;
						case 5:
							return;
						case 7:
							num = 622691351;
							continue;
						case 6:
							break;
						case 4:
							goto IL_0086;
						case 1:
							Rewired.Logger.LogWarning("Map Category \"" + value + "\" does not exist.");
							num = 622691345;
							continue;
						case 3:
							ktfjKKxKmOGSnHhHDjmYrGAHoHz = mapCategoryId;
							return;
						case 2:
							goto IL_00d2;
						}
						break;
						IL_00d2:
						int num2;
						if (mapCategoryId < 0)
						{
							num = 622691344;
							num2 = num;
						}
						else
						{
							num = 622691346;
							num2 = num;
						}
					}
					continue;
					IL_0086:
					mapCategoryId = ReInput.mapping.GetMapCategoryId(value);
					num = 622691347;
					goto IL_0035;
				}
			}
		}

		[DoNotSerialize]
		public string fNZDYSlzmRQXPEdgoRarKPEffVkH
		{
			get
			{
				//Discarded unreachable code: IL_003b
				if (!ReInput.isReady)
				{
					if (fRsRNvQEMxCvvaDUjbOOoblRemug != null)
					{
						goto IL_000f;
					}
					goto IL_0042;
				}
				FzawOUbErNWuzjmqBpAPaNyVOxl();
				if (_layoutIds == null)
				{
					goto IL_0035;
				}
				int num = default(int);
				if (_layoutIds.Length != 0)
				{
					num = 0;
					if (num >= _layoutIds.Length)
					{
						return "INVALID";
					}
					goto IL_007e;
				}
				int num2 = -1654772062;
				goto IL_0014;
				IL_000f:
				num2 = -1654772064;
				goto IL_0014;
				IL_0035:
				return null;
				IL_007e:
				InputLayout layout = ReInput.mapping.GetLayout(pMuDOCNOtKqkQVaEUcwrbdcrjzMc.LBvurysqjogiYiEmqeJKfpvGlYKC, _layoutIds[num]);
				if (layout == null)
				{
					return "INVALID";
				}
				return layout.name;
				IL_0042:
				return null;
				IL_0014:
				while (true)
				{
					switch (num2 ^ -1654772062)
					{
					case 3:
						break;
					case 0:
						goto IL_0035;
					case 1:
						goto IL_0042;
					case 2:
						goto IL_006c;
					default:
						goto IL_007e;
					}
					break;
					IL_006c:
					if (fRsRNvQEMxCvvaDUjbOOoblRemug.Length > 0)
					{
						return fRsRNvQEMxCvvaDUjbOOoblRemug[0];
					}
					num2 = -1654772061;
				}
				goto IL_000f;
			}
			set
			{
				//Discarded unreachable code: IL_0097, IL_00e5, IL_010f
				if (ReInput.isReady)
				{
					goto IL_0059;
				}
				if (!string.IsNullOrEmpty(value))
				{
					ZdhdzExHhHqjNJcxvAYEctMJnvf();
					goto IL_0018;
				}
				goto IL_00a1;
				IL_0018:
				int num = 163481616;
				goto IL_001d;
				IL_001d:
				int layoutId = default(int);
				while (true)
				{
					switch (num ^ 0x9BE8817)
					{
					case 10:
						return;
					case 8:
						break;
					case 2:
						goto IL_0059;
					case 1:
						ZdhdzExHhHqjNJcxvAYEctMJnvf();
						layoutId = ReInput.mapping.GetLayoutId(pMuDOCNOtKqkQVaEUcwrbdcrjzMc.LBvurysqjogiYiEmqeJKfpvGlYKC, value);
						num = 163481623;
						continue;
					case 7:
						goto IL_00a1;
					case 4:
						fRsRNvQEMxCvvaDUjbOOoblRemug = null;
						num = 163481618;
						continue;
					case 5:
						_layoutIds = EmptyObjects<int>.array;
						return;
					case 0:
						goto IL_00ef;
					case 3:
						jSVECUANwcgzIdTjgjLAcuUrKbOY = layoutId;
						return;
					case 9:
						_layoutIds = EmptyObjects<int>.array;
						num = 163481629;
						continue;
					default:
						Rewired.Logger.LogWarning("Map Layout \"" + value + "\" does not exist.");
						return;
					}
					break;
					IL_00ef:
					int num2;
					if (layoutId < 0)
					{
						num = 163481617;
						num2 = num;
					}
					else
					{
						num = 163481620;
						num2 = num;
					}
				}
				goto IL_0018;
				IL_0059:
				int num3;
				if (!string.IsNullOrEmpty(value))
				{
					num = 163481622;
					num3 = num;
				}
				else
				{
					num = 163481619;
					num3 = num;
				}
				goto IL_001d;
				IL_00a1:
				fRsRNvQEMxCvvaDUjbOOoblRemug = ((!string.IsNullOrEmpty(value)) ? new string[1]
				{
					value
				} : null);
				num = 163481630;
				goto IL_001d;
			}
		}

		[DoNotSerialize]
		internal bool nhSgbDorlvTISzgyPTBlFNZQEsC
		{
			get
			{
				if (_controllerSelector == null)
				{
					goto IL_000b;
				}
				if (!ReInput.isReady)
				{
					return true;
				}
				FzawOUbErNWuzjmqBpAPaNyVOxl();
				int num;
				if (_categoryIds != null && _categoryIds.Length > 0)
				{
					num = 1010702167;
					goto IL_0010;
				}
				goto IL_00b5;
				IL_00b5:
				if (_layoutIds != null && _layoutIds.Length > 0)
				{
					num = 1010702165;
					goto IL_0010;
				}
				goto IL_013b;
				IL_0136:
				bool flag = default(bool);
				if (!flag)
				{
					return false;
				}
				goto IL_013b;
				IL_013b:
				return true;
				IL_0010:
				int num3 = default(int);
				bool flag2 = default(bool);
				int num2 = default(int);
				while (true)
				{
					switch (num ^ 0x3C3E1755)
					{
					case 10:
						break;
					case 1:
						if (num3 >= _categoryIds.Length)
						{
							goto IL_0057;
						}
						goto case 3;
					case 2:
						flag2 = false;
						num3 = 0;
						num = 1010702164;
						continue;
					case 0:
						flag = false;
						num2 = 0;
						num = 1010702173;
						continue;
					case 3:
						if (ReInput.mapping.GetMapCategory(_categoryIds[num3]) != null)
						{
							flag2 = true;
							num = 1010702163;
							continue;
						}
						goto case 6;
					case 7:
						num2++;
						num = 1010702173;
						continue;
					case 6:
						num3++;
						num = 1010702164;
						continue;
					case 9:
						return false;
					case 4:
						if (ReInput.mapping.GetLayout(_controllerSelector.LBvurysqjogiYiEmqeJKfpvGlYKC, _layoutIds[num2]) != null)
						{
							flag = true;
							num = 1010702162;
							continue;
						}
						goto case 7;
					case 5:
						return false;
					default:
						if (num2 < _layoutIds.Length)
						{
							goto case 4;
						}
						goto IL_0136;
					}
					break;
					IL_0057:
					if (!flag2)
					{
						num = 1010702172;
						continue;
					}
					goto IL_00b5;
				}
				goto IL_000b;
				IL_000b:
				num = 1010702160;
				goto IL_0010;
			}
		}

		public UPEDbksMSUIEmKjlartPAAlIIDj()
		{
			_categoryIds = EmptyObjects<int>.array;
			_layoutIds = EmptyObjects<int>.array;
			_controllerSelector = new ZVOPkHmpobXEknQJZShcFQBpbsRa(ZVOPkHmpobXEknQJZShcFQBpbsRa.Type.ControllerType);
		}

		internal bool FLhKTSEmJOeEzkXHoSGatGpGmpHM(ControllerMap P_0)
		{
			if (P_0 == null)
			{
				goto IL_0003;
			}
			if (!nhSgbDorlvTISzgyPTBlFNZQEsC)
			{
				return false;
			}
			int num;
			if (_categoryIds != null)
			{
				num = -662233058;
				goto IL_0008;
			}
			goto IL_006e;
			IL_0003:
			num = -662233064;
			goto IL_0008;
			IL_006e:
			if (_layoutIds != null && _layoutIds.Length > 0 && !ArrayTools.Contains(_layoutIds, P_0.layoutId))
			{
				return false;
			}
			if (!_controllerSelector.FLhKTSEmJOeEzkXHoSGatGpGmpHM(P_0.controller))
			{
				num = -662233057;
				goto IL_0008;
			}
			return true;
			IL_0008:
			while (true)
			{
				switch (num ^ -662233060)
				{
				case 0:
					break;
				case 4:
					return false;
				case 2:
					goto IL_0047;
				case 1:
					goto IL_0059;
				default:
					return false;
				}
				break;
				IL_0059:
				if (!ArrayTools.Contains(_categoryIds, P_0.categoryId))
				{
					return false;
				}
				goto IL_006e;
				IL_0047:
				if (_categoryIds.Length > 0)
				{
					num = -662233059;
					continue;
				}
				goto IL_006e;
			}
			goto IL_0003;
		}

		private void FzawOUbErNWuzjmqBpAPaNyVOxl()
		{
			//Discarded unreachable code: IL_00c0, IL_02b4
			if (!ReInput.isReady)
			{
				goto IL_000a;
			}
			goto IL_02a8;
			IL_02a8:
			if (_controllerSelector == null)
			{
				return;
			}
			goto IL_00d6;
			IL_00d6:
			int num;
			if (_categoryIds == null)
			{
				_categoryIds = EmptyObjects<int>.array;
				num = 317393234;
				goto IL_000f;
			}
			goto IL_016b;
			IL_000a:
			num = 317393232;
			goto IL_000f;
			IL_016b:
			if (rDlGKUDuuAoTZXVrfIUkxFBnSrr != null)
			{
				int num2;
				if (rDlGKUDuuAoTZXVrfIUkxFBnSrr.Length != 0)
				{
					num = 317393225;
					num2 = num;
				}
				else
				{
					num = 317393216;
					num2 = num;
				}
				goto IL_000f;
			}
			goto IL_007b;
			IL_007b:
			int num3;
			if (fRsRNvQEMxCvvaDUjbOOoblRemug == null)
			{
				num = 317393239;
				num3 = num;
			}
			else
			{
				num = 317393237;
				num3 = num;
			}
			goto IL_000f;
			IL_000f:
			int num5 = default(int);
			int num4 = default(int);
			List<int> list2 = default(List<int>);
			List<int> list = default(List<int>);
			while (true)
			{
				switch (num ^ 0x12EB0944)
				{
				default:
					return;
				case 19:
					return;
				case 20:
					return;
				case 6:
					break;
				case 4:
					goto IL_007b;
				case 0:
					ZdhdzExHhHqjNJcxvAYEctMJnvf();
					num = 317393223;
					continue;
				case 16:
					num = 317393229;
					continue;
				case 21:
					num5++;
					num = 317393230;
					continue;
				case 14:
					num5 = 0;
					num = 317393228;
					continue;
				case 18:
					goto IL_00d6;
				case 1:
					num4++;
					num = 317393229;
					continue;
				case 12:
					Rewired.Logger.LogWarning("Map Category \"" + rDlGKUDuuAoTZXVrfIUkxFBnSrr[num5] + "\" does not exist.");
					num = 317393233;
					continue;
				case 13:
					list2 = new List<int>(rDlGKUDuuAoTZXVrfIUkxFBnSrr.Length);
					num = 317393226;
					continue;
				case 5:
					Rewired.Logger.LogWarning("Map Layout \"" + fRsRNvQEMxCvvaDUjbOOoblRemug[num4] + "\" does not exist.");
					num = 317393221;
					continue;
				case 22:
					goto IL_016b;
				case 11:
				{
					if (string.IsNullOrEmpty(fRsRNvQEMxCvvaDUjbOOoblRemug[num4]))
					{
						goto case 1;
					}
					int layoutId = ReInput.mapping.GetLayoutId(_controllerSelector.LBvurysqjogiYiEmqeJKfpvGlYKC, fRsRNvQEMxCvvaDUjbOOoblRemug[num4]);
					if (layoutId >= 0)
					{
						list.Add(layoutId);
						num = 317393221;
						continue;
					}
					goto case 5;
				}
				case 10:
					if (num5 >= rDlGKUDuuAoTZXVrfIUkxFBnSrr.Length)
					{
						_categoryIds = list2.ToArray();
						rDlGKUDuuAoTZXVrfIUkxFBnSrr = null;
						num = 317393216;
						continue;
					}
					goto case 2;
				case 3:
					list = new List<int>(fRsRNvQEMxCvvaDUjbOOoblRemug.Length);
					num4 = 0;
					num = 317393236;
					continue;
				case 15:
					_layoutIds = list.ToArray();
					fRsRNvQEMxCvvaDUjbOOoblRemug = null;
					num = 317393239;
					continue;
				case 9:
					goto IL_0241;
				case 2:
				{
					if (string.IsNullOrEmpty(rDlGKUDuuAoTZXVrfIUkxFBnSrr[num5]))
					{
						goto case 21;
					}
					int mapCategoryId = ReInput.mapping.GetMapCategoryId(rDlGKUDuuAoTZXVrfIUkxFBnSrr[num5]);
					if (mapCategoryId >= 0)
					{
						list2.Add(mapCategoryId);
						num = 317393233;
						continue;
					}
					goto case 12;
				}
				case 8:
					num = 317393230;
					continue;
				case 7:
					goto IL_02a8;
				case 17:
					goto IL_02be;
				}
				break;
				IL_02be:
				int num6;
				if (fRsRNvQEMxCvvaDUjbOOoblRemug.Length != 0)
				{
					num = 317393220;
					num6 = num;
				}
				else
				{
					num = 317393239;
					num6 = num;
				}
				continue;
				IL_0241:
				int num7;
				if (num4 >= fRsRNvQEMxCvvaDUjbOOoblRemug.Length)
				{
					num = 317393227;
					num7 = num;
				}
				else
				{
					num = 317393231;
					num7 = num;
				}
			}
			goto IL_000a;
		}

		private void ZdhdzExHhHqjNJcxvAYEctMJnvf()
		{
			//Discarded unreachable code: IL_0098
			if (_controllerSelector == null)
			{
				goto IL_0008;
			}
			goto IL_0079;
			IL_0079:
			object[] array = default(object[]);
			int num;
			if (!_controllerSelector.yvLiueCESwSvNDaZmDXVgPpfypFP)
			{
				array = new object[5];
				num = -1573505386;
				goto IL_000d;
			}
			return;
			IL_0008:
			num = -1573505392;
			goto IL_000d;
			IL_000d:
			while (true)
			{
				switch (num ^ -1573505389)
				{
				default:
					return;
				case 3:
					return;
				case 4:
					return;
				case 2:
					break;
				case 5:
					array[0] = "A Layout should not be set when using ";
					array[1] = typeof(ZVOPkHmpobXEknQJZShcFQBpbsRa.Type).FullName;
					array[2] = ".";
					array[3] = _controllerSelector.SSkMtjLDbBWEffiHSpoQpCqRSKa;
					array[4] = " because each Controller type has its own unique Layouts.";
					num = -1573505390;
					continue;
				case 0:
					goto IL_0079;
				case 1:
					Rewired.Logger.LogWarning(string.Concat(array), requiredThreadSafety: true);
					num = -1573505385;
					continue;
				}
				break;
			}
			goto IL_0008;
		}
	}

	private bool KfbFsZTwJuhYvyDZkVBtGMGpvXu;

	private Player xbqctAhVtwEUjBwVhYwnxXyunii;

	private WoSISrKqsFTIovjAPQFjHwQKWUo vUZCkIcmupsiZkBSYrdsFNaAlsN;

	private readonly int aYVuMCKfoBJZlPKnMohBfcrinAp;

	private List<UPEDbksMSUIEmKjlartPAAlIIDj> EfnMCRrRuufGggdgDGncEfXaYrLC;

	public bool YvthbnNVBRzdDIZfdQsVTptORZS
	{
		get
		{
			return KfbFsZTwJuhYvyDZkVBtGMGpvXu;
		}
		set
		{
			KfbFsZTwJuhYvyDZkVBtGMGpvXu = value;
			if (!value)
			{
				return;
			}
			while (true)
			{
				int num = -2084218322;
				while (true)
				{
					switch (num ^ -2084218321)
					{
					default:
						return;
					case 0:
						return;
					case 2:
						break;
					case 1:
						goto IL_0028;
					}
					break;
					IL_0028:
					iohqbZWfuktDXUDMHwmiziCYETC();
					num = -2084218321;
				}
			}
		}
	}

	public List<UPEDbksMSUIEmKjlartPAAlIIDj> WBfQciSrovnPIpngxqhztwVhEqL
	{
		get
		{
			return EfnMCRrRuufGggdgDGncEfXaYrLC;
		}
		set
		{
			if (value == null)
			{
				value = new List<UPEDbksMSUIEmKjlartPAAlIIDj>();
				goto IL_000a;
			}
			goto IL_0028;
			IL_000a:
			int num = 712345871;
			goto IL_000f;
			IL_0028:
			EfnMCRrRuufGggdgDGncEfXaYrLC = value;
			num = 712345868;
			goto IL_000f;
			IL_000f:
			switch (num ^ 0x2A75890D)
			{
			default:
				return;
			case 1:
				return;
			case 0:
				break;
			case 2:
				goto IL_0028;
			}
			goto IL_000a;
		}
	}

	internal KIXwiuSmddHbsuwjaBJTbuGPjBji(Player player, WoSISrKqsFTIovjAPQFjHwQKWUo startingControllerMapInfo)
	{
		if (player == null)
		{
			throw new ArgumentNullException("player");
		}
		aYVuMCKfoBJZlPKnMohBfcrinAp = ReInput.id;
		xbqctAhVtwEUjBwVhYwnxXyunii = player;
		vUZCkIcmupsiZkBSYrdsFNaAlsN = startingControllerMapInfo;
		EfnMCRrRuufGggdgDGncEfXaYrLC = new List<UPEDbksMSUIEmKjlartPAAlIIDj>();
	}

	public void iohqbZWfuktDXUDMHwmiziCYETC()
	{
		//Discarded unreachable code: IL_0045
		if (ReInput._id != aYVuMCKfoBJZlPKnMohBfcrinAp)
		{
			ReInput.CheckInitialized(aYVuMCKfoBJZlPKnMohBfcrinAp);
			return;
		}
		int num3 = default(int);
		ControllerMap controllerMap = default(ControllerMap);
		while (KfbFsZTwJuhYvyDZkVBtGMGpvXu)
		{
			while (true)
			{
				IL_004c:
				int count = EfnMCRrRuufGggdgDGncEfXaYrLC.Count;
				int num = -1946319718;
				while (true)
				{
					switch (num ^ -1946319719)
					{
					case 0:
						num = -1946319720;
						continue;
					case 1:
						break;
					case 2:
						goto IL_004c;
					default:
					{
						TempListPool.TList<ControllerMap> tList = TempListPool.GetTList<ControllerMap>();
						try
						{
							List<ControllerMap> list = tList.list;
							xbqctAhVtwEUjBwVhYwnxXyunii.controllers.maps.GetAllMaps(list);
							int count2 = list.Count;
							int num2 = 0;
							while (num2 < count)
							{
								while (true)
								{
									IL_00fd:
									UPEDbksMSUIEmKjlartPAAlIIDj uPEDbksMSUIEmKjlartPAAlIIDj = EfnMCRrRuufGggdgDGncEfXaYrLC[num2];
									int num4;
									if (uPEDbksMSUIEmKjlartPAAlIIDj != null)
									{
										num3 = 0;
										num4 = -1946319715;
										goto IL_0097;
									}
									goto IL_011d;
									IL_0097:
									while (true)
									{
										switch (num4 ^ -1946319719)
										{
										case 2:
											num4 = -1946319714;
											continue;
										case 3:
											if (uPEDbksMSUIEmKjlartPAAlIIDj.FLhKTSEmJOeEzkXHoSGatGpGmpHM(controllerMap))
											{
												controllerMap.enabled = uPEDbksMSUIEmKjlartPAAlIIDj.YvthbnNVBRzdDIZfdQsVTptORZS;
												num4 = -1946319720;
												continue;
											}
											goto case 1;
										case 4:
											break;
										case 7:
											goto IL_00fd;
										case 6:
											goto IL_011d;
										case 1:
											num3++;
											num4 = -1946319715;
											continue;
										case 0:
											goto IL_013d;
										default:
											goto IL_016b;
										}
										int num5;
										if (num3 < count2)
										{
											num4 = -1946319719;
											num5 = num4;
										}
										else
										{
											num4 = -1946319713;
											num5 = num4;
										}
										continue;
										IL_013d:
										controllerMap = list[num3];
										int num6;
										if (controllerMap.enabled != uPEDbksMSUIEmKjlartPAAlIIDj.YvthbnNVBRzdDIZfdQsVTptORZS)
										{
											num4 = -1946319718;
											num6 = num4;
										}
										else
										{
											num4 = -1946319720;
											num6 = num4;
										}
									}
									IL_011d:
									num2++;
									num4 = -1946319716;
									goto IL_0097;
								}
								IL_016b:;
							}
						}
						finally
						{
							if (tList != null)
							{
								while (true)
								{
									int num7 = -1946319717;
									while (true)
									{
										switch (num7 ^ -1946319719)
										{
										default:
											goto end_IL_0172;
										case 1:
											goto end_IL_0172;
										case 0:
											break;
										case 2:
											goto IL_0193;
										}
										break;
										IL_0193:
										((IDisposable)tList).Dispose();
										num7 = -1946319720;
									}
								}
							}
							end_IL_0172:;
						}
						return;
					}
					}
					break;
				}
				break;
			}
		}
	}

	public void HWCbzNBLkkIlPJWTSEJLjkhGLbmR()
	{
		//Discarded unreachable code: IL_0040
		if (ReInput._id != aYVuMCKfoBJZlPKnMohBfcrinAp)
		{
			goto IL_000d;
		}
		goto IL_0065;
		IL_0065:
		EfnMCRrRuufGggdgDGncEfXaYrLC = new List<UPEDbksMSUIEmKjlartPAAlIIDj>();
		HWCbzNBLkkIlPJWTSEJLjkhGLbmR(ControllerType.Joystick, vUZCkIcmupsiZkBSYrdsFNaAlsN.shCgZSFkHfpoEimFwwpCmtRZrrES, EfnMCRrRuufGggdgDGncEfXaYrLC);
		int num = -688350872;
		goto IL_0012;
		IL_0012:
		while (true)
		{
			switch (num ^ -688350869)
			{
			case 4:
				break;
			case 2:
				ReInput.CheckInitialized(aYVuMCKfoBJZlPKnMohBfcrinAp);
				return;
			case 3:
				HWCbzNBLkkIlPJWTSEJLjkhGLbmR(ControllerType.Keyboard, vUZCkIcmupsiZkBSYrdsFNaAlsN.XDOZVALrmnDdYDiRFjJYbwcFbWNR, EfnMCRrRuufGggdgDGncEfXaYrLC);
				num = -688350869;
				continue;
			case 1:
				goto IL_0065;
			default:
				HWCbzNBLkkIlPJWTSEJLjkhGLbmR(ControllerType.Mouse, vUZCkIcmupsiZkBSYrdsFNaAlsN.JOKNoTSEMGjlwRhcUZaelzvMATe, EfnMCRrRuufGggdgDGncEfXaYrLC);
				HWCbzNBLkkIlPJWTSEJLjkhGLbmR(ControllerType.Custom, vUZCkIcmupsiZkBSYrdsFNaAlsN.FGHDrZiTCpAywbDTmQqefpmaQjmR, EfnMCRrRuufGggdgDGncEfXaYrLC);
				iohqbZWfuktDXUDMHwmiziCYETC();
				return;
			}
			break;
		}
		goto IL_000d;
		IL_000d:
		num = -688350871;
		goto IL_0012;
	}

	public string dehJqgmcsKcKhfKnYbYnGMnMuCg()
	{
		if (ReInput._id != aYVuMCKfoBJZlPKnMohBfcrinAp)
		{
			ReInput.CheckInitialized(aYVuMCKfoBJZlPKnMohBfcrinAp);
			return string.Empty;
		}
		try
		{
			return pqGfItiJIaoUVUmYeCVieLFyoqUT().ToXmlString(writeDocumentTag: true);
		}
		catch (Exception ex)
		{
			Rewired.Logger.LogWarning("Error writing " + GetType().Name + " to XML. " + ex.Message);
			return string.Empty;
		}
	}

	public string CllIFqzdGfhYOlXkGEpZlQeTLUT()
	{
		if (ReInput._id != aYVuMCKfoBJZlPKnMohBfcrinAp)
		{
			ReInput.CheckInitialized(aYVuMCKfoBJZlPKnMohBfcrinAp);
			return string.Empty;
		}
		try
		{
			return pqGfItiJIaoUVUmYeCVieLFyoqUT().ToJsonString();
		}
		catch (Exception ex)
		{
			Rewired.Logger.LogWarning("Error writing " + GetType().Name + " to JSON. " + ex.Message);
			return string.Empty;
		}
	}

	public bool bQzotAliCTMQAAehjQmBQVeRhPv(string P_0)
	{
		if (ReInput._id != aYVuMCKfoBJZlPKnMohBfcrinAp)
		{
			ReInput.CheckInitialized(aYVuMCKfoBJZlPKnMohBfcrinAp);
			return false;
		}
		bool result = default(bool);
		try
		{
			YetmeIrLZHuTPNZCsINxPcRaAWfF(SerializedObject.FromXml(GetType(), P_0));
			iohqbZWfuktDXUDMHwmiziCYETC();
			while (true)
			{
				int num = 1612801516;
				while (true)
				{
					switch (num ^ 0x602165ED)
					{
					case 2:
						break;
					case 1:
						goto IL_0052;
					default:
						return result;
					case 0:
						return result;
					}
					break;
					IL_0052:
					result = true;
					num = 1612801517;
				}
			}
		}
		catch (Exception ex)
		{
			while (true)
			{
				int num2 = 1612801519;
				while (true)
				{
					switch (num2 ^ 0x602165ED)
					{
					case 0:
						break;
					case 2:
						goto IL_007c;
					default:
						return result;
					case 1:
						return result;
					}
					break;
					IL_007c:
					Rewired.Logger.LogError("Error importing " + GetType().Name + " data from XML. " + ex.Message);
					result = false;
					num2 = 1612801516;
				}
			}
		}
	}

	public bool rVhKYJtAtMHwhMoekMviBzAfAWH(string P_0)
	{
		if (ReInput._id != aYVuMCKfoBJZlPKnMohBfcrinAp)
		{
			ReInput.CheckInitialized(aYVuMCKfoBJZlPKnMohBfcrinAp);
			return false;
		}
		try
		{
			YetmeIrLZHuTPNZCsINxPcRaAWfF(SerializedObject.FromJson(GetType(), P_0));
			iohqbZWfuktDXUDMHwmiziCYETC();
			return true;
		}
		catch (Exception ex)
		{
			Rewired.Logger.LogError("Error importing " + GetType().Name + " data from JSON. " + ex.Message);
			return false;
		}
	}

	private SerializedObject pqGfItiJIaoUVUmYeCVieLFyoqUT()
	{
		SerializedObject serializedObject = new SerializedObject(GetType(), SerializedObject.ObjectType.Object);
		KrTNUBwBcnztJXDGAYSgCZgyKFB(serializedObject);
		return serializedObject;
	}

	private void KrTNUBwBcnztJXDGAYSgCZgyKFB(SerializedObject P_0)
	{
		if (P_0.xmlInfo == null)
		{
			P_0.xmlInfo = new SerializedObject.XmlInfo();
			goto IL_0013;
		}
		goto IL_003c;
		IL_0013:
		int num = -1652625428;
		goto IL_0018;
		IL_003c:
		P_0.Add("dataVersion", 1, SerializedObject.FieldOptions.ExculdeFromXml);
		num = -1652625429;
		goto IL_0018;
		IL_0018:
		while (true)
		{
			switch (num ^ -1652625425)
			{
			case 0:
				break;
			case 3:
				goto IL_003c;
			case 4:
				P_0.xmlInfo.attributes.Add(new SerializedObject.XmlInfo.XmlStringAttribute
				{
					localName = "dataVersion",
					value = 1.ToString()
				});
				num = -1652625426;
				continue;
			case 1:
				P_0.xmlInfo.attributes.Add(new SerializedObject.XmlInfo.XmlStringAttribute
				{
					prefix = "xmlns",
					localName = "xsi",
					ns = null,
					value = "http://www.w3.org/2001/XMLSchema-instance"
				});
				P_0.xmlInfo.attributes.Add(new SerializedObject.XmlInfo.XmlStringAttribute
				{
					prefix = "xsi",
					localName = "schemaLocation",
					ns = null,
					value = string.Format("{0} {1}{2}{3}{4}{5}", "http://guavaman.com/rewired", "http://guavaman.com/schemas/rewired/", "1.0", "/", GetType().Name, ".xsd")
				});
				num = -1652625427;
				continue;
			default:
				P_0.Add("enabled", KfbFsZTwJuhYvyDZkVBtGMGpvXu);
				P_0.Add("entries", EfnMCRrRuufGggdgDGncEfXaYrLC);
				return;
			}
			break;
		}
		goto IL_0013;
	}

	private bool YetmeIrLZHuTPNZCsINxPcRaAWfF(SerializedObject P_0)
	{
		KfbFsZTwJuhYvyDZkVBtGMGpvXu = false;
		EfnMCRrRuufGggdgDGncEfXaYrLC = new List<UPEDbksMSUIEmKjlartPAAlIIDj>();
		List<UPEDbksMSUIEmKjlartPAAlIIDj> value = default(List<UPEDbksMSUIEmKjlartPAAlIIDj>);
		while (true)
		{
			int num = 359096128;
			while (true)
			{
				switch (num ^ 0x15675F42)
				{
				case 4:
					break;
				case 2:
					P_0.TryGetDeserializedValueByRef("enabled", ref KfbFsZTwJuhYvyDZkVBtGMGpvXu);
					num = 359096131;
					continue;
				case 3:
					P_0.TryGetDeserializedValueByRef("entries", ref value);
					num = 359096130;
					continue;
				case 1:
					value = new List<UPEDbksMSUIEmKjlartPAAlIIDj>();
					num = 359096129;
					continue;
				default:
					WBfQciSrovnPIpngxqhztwVhEqL = value;
					return true;
				}
				break;
			}
		}
	}

	private static void HWCbzNBLkkIlPJWTSEJLjkhGLbmR(ControllerType P_0, GGibmocDPlUsrFgyupaNdcDlXcXi[] P_1, List<UPEDbksMSUIEmKjlartPAAlIIDj> P_2)
	{
		if (P_1 == null)
		{
			return;
		}
		GGibmocDPlUsrFgyupaNdcDlXcXi gGibmocDPlUsrFgyupaNdcDlXcXi2 = default(GGibmocDPlUsrFgyupaNdcDlXcXi);
		int num3 = default(int);
		GGibmocDPlUsrFgyupaNdcDlXcXi gGibmocDPlUsrFgyupaNdcDlXcXi = default(GGibmocDPlUsrFgyupaNdcDlXcXi);
		bool flag = default(bool);
		while (true)
		{
			int num = 0;
			int num2 = -566281732;
			while (true)
			{
				switch (num2 ^ -566281729)
				{
				case 0:
					num2 = -566281735;
					continue;
				case 13:
				{
					gGibmocDPlUsrFgyupaNdcDlXcXi2 = P_1[num3];
					int num7;
					if (gGibmocDPlUsrFgyupaNdcDlXcXi.ktfjKKxKmOGSnHhHDjmYrGAHoHz != gGibmocDPlUsrFgyupaNdcDlXcXi2.ktfjKKxKmOGSnHhHDjmYrGAHoHz)
					{
						num2 = -566281738;
						num7 = num2;
					}
					else
					{
						num2 = -566281731;
						num7 = num2;
					}
					continue;
				}
				case 2:
				{
					int num5;
					if (gGibmocDPlUsrFgyupaNdcDlXcXi.jSVECUANwcgzIdTjgjLAcuUrKbOY == gGibmocDPlUsrFgyupaNdcDlXcXi2.jSVECUANwcgzIdTjgjLAcuUrKbOY)
					{
						num2 = -566281738;
						num5 = num2;
					}
					else
					{
						num2 = -566281739;
						num5 = num2;
					}
					continue;
				}
				case 8:
					gGibmocDPlUsrFgyupaNdcDlXcXi = P_1[num];
					flag = false;
					num3 = 0;
					num2 = -566281730;
					continue;
				case 10:
					flag = true;
					num2 = -566281734;
					continue;
				case 1:
				{
					int num6;
					if (num3 >= P_1.Length)
					{
						num2 = -566281734;
						num6 = num2;
					}
					else
					{
						num2 = -566281736;
						num6 = num2;
					}
					continue;
				}
				case 7:
				{
					int num4;
					if (num == num3)
					{
						num2 = -566281738;
						num4 = num2;
					}
					else
					{
						num2 = -566281742;
						num4 = num2;
					}
					continue;
				}
				case 12:
					P_2.Add(new UPEDbksMSUIEmKjlartPAAlIIDj
					{
						pMuDOCNOtKqkQVaEUcwrbdcrjzMc = ZVOPkHmpobXEknQJZShcFQBpbsRa.MSHfTDGPfNGJRomSPLgDvMZNgLIc(P_0),
						YvthbnNVBRzdDIZfdQsVTptORZS = gGibmocDPlUsrFgyupaNdcDlXcXi.JvAcngzNcREqSmATVKFTFPdSbDB,
						ktfjKKxKmOGSnHhHDjmYrGAHoHz = gGibmocDPlUsrFgyupaNdcDlXcXi.ktfjKKxKmOGSnHhHDjmYrGAHoHz
					});
					num2 = -566281740;
					continue;
				case 9:
					num3++;
					num2 = -566281730;
					continue;
				case 11:
					num++;
					num2 = -566281733;
					continue;
				case 5:
					if (flag)
					{
						P_2.Add(new UPEDbksMSUIEmKjlartPAAlIIDj
						{
							pMuDOCNOtKqkQVaEUcwrbdcrjzMc = ZVOPkHmpobXEknQJZShcFQBpbsRa.MSHfTDGPfNGJRomSPLgDvMZNgLIc(P_0),
							YvthbnNVBRzdDIZfdQsVTptORZS = gGibmocDPlUsrFgyupaNdcDlXcXi.JvAcngzNcREqSmATVKFTFPdSbDB,
							ktfjKKxKmOGSnHhHDjmYrGAHoHz = gGibmocDPlUsrFgyupaNdcDlXcXi.ktfjKKxKmOGSnHhHDjmYrGAHoHz,
							jSVECUANwcgzIdTjgjLAcuUrKbOY = gGibmocDPlUsrFgyupaNdcDlXcXi.jSVECUANwcgzIdTjgjLAcuUrKbOY
						});
						num2 = -566281740;
						continue;
					}
					goto case 12;
				case 3:
					num2 = -566281733;
					continue;
				case 6:
					break;
				default:
					if (num >= P_1.Length)
					{
						return;
					}
					goto case 8;
				}
				break;
			}
		}
	}
}
