namespace Rewired.Config
{
	[CustomObfuscation(rename = false)]
	internal enum LogLevel
	{
		[CustomObfuscation(rename = false)]
		Info,
		[CustomObfuscation(rename = false)]
		Warning,
		[CustomObfuscation(rename = false)]
		Error,
		[CustomObfuscation(rename = false)]
		Debug
	}
}
