using System;

namespace Rewired.Config
{
	[Flags]
	public enum UpdateLoopSetting
	{
		None = 0x0,
		Update = 0x1,
		FixedUpdate = 0x2,
		OnGUI = 0x4
	}
}
