using System;

namespace Rewired.Config
{
	[Flags]
	public enum LogLevelFlags
	{
		Off = 0x0,
		Info = 0x1,
		Warning = 0x2,
		Error = 0x4,
		Debug = 0x8
	}
}
