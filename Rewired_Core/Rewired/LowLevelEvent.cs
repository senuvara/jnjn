namespace Rewired
{
	[CustomClassObfuscation(renamePubIntMembers = false, renamePrivateMembers = true)]
	[CustomObfuscation(rename = false)]
	internal abstract class LowLevelEvent
	{
		public uint id;

		public float timestamp;

		private static uint IRznRBGoBLjOSEXxmHfzTOViLWhE;

		protected static uint GetNextId()
		{
			if (IRznRBGoBLjOSEXxmHfzTOViLWhE == uint.MaxValue)
			{
				IRznRBGoBLjOSEXxmHfzTOViLWhE = 0u;
				return uint.MaxValue;
			}
			return ++IRznRBGoBLjOSEXxmHfzTOViLWhE;
		}
	}
}
