using UnityEngine;

namespace Rewired
{
	public struct ElementAssignmentConflictInfo
	{
		private bool TvyDrFJuZXuENJviWtlChMCfgUht;

		private bool EMURwJrQYwCVFwQSclXkSlnEruu;

		private int jNwSqiUbJJSyWZCKNvJDjxeYjBF;

		private ControllerType eBGLAIjxzCbUtbnMABFkvwtVwkt;

		private int aNAUCDXmNrGbeiAKTkerzFPUzNsk;

		private int CHdAsOEWFCVaQbFQcbOBQWDLlDnA;

		private int pCavdtuByBGKGwqUMxXlElgAjpF;

		private ControllerElementType xIOHjTqprxflrglDOorVKUSRYqOR;

		private int hSULoVXORPauXdCqLuMLNWSvuDs;

		private KeyCode mFLiSaIMLKvvniCzCrvrIvLvGOj;

		private ModifierKeyFlags LgDgYREyjYRivcBMzEebmXsCKxro;

		private int daRJuBBqMmlroNQwzmbJfOfRARM;

		public bool isConflict
		{
			get
			{
				return TvyDrFJuZXuENJviWtlChMCfgUht;
			}
			internal set
			{
				TvyDrFJuZXuENJviWtlChMCfgUht = value;
			}
		}

		public bool isUserAssignable
		{
			get
			{
				return EMURwJrQYwCVFwQSclXkSlnEruu;
			}
			internal set
			{
				EMURwJrQYwCVFwQSclXkSlnEruu = value;
			}
		}

		public int playerId
		{
			get
			{
				return jNwSqiUbJJSyWZCKNvJDjxeYjBF;
			}
			internal set
			{
				jNwSqiUbJJSyWZCKNvJDjxeYjBF = value;
			}
		}

		public ControllerType controllerType
		{
			get
			{
				return eBGLAIjxzCbUtbnMABFkvwtVwkt;
			}
			internal set
			{
				eBGLAIjxzCbUtbnMABFkvwtVwkt = value;
			}
		}

		public int controllerId
		{
			get
			{
				return aNAUCDXmNrGbeiAKTkerzFPUzNsk;
			}
			internal set
			{
				aNAUCDXmNrGbeiAKTkerzFPUzNsk = value;
			}
		}

		public int controllerMapId
		{
			get
			{
				return CHdAsOEWFCVaQbFQcbOBQWDLlDnA;
			}
			internal set
			{
				CHdAsOEWFCVaQbFQcbOBQWDLlDnA = value;
			}
		}

		public int elementMapId
		{
			get
			{
				return pCavdtuByBGKGwqUMxXlElgAjpF;
			}
			internal set
			{
				pCavdtuByBGKGwqUMxXlElgAjpF = value;
			}
		}

		public ControllerElementType elementType
		{
			get
			{
				return xIOHjTqprxflrglDOorVKUSRYqOR;
			}
			internal set
			{
				xIOHjTqprxflrglDOorVKUSRYqOR = value;
			}
		}

		public int elementIdentifierId
		{
			get
			{
				return hSULoVXORPauXdCqLuMLNWSvuDs;
			}
			internal set
			{
				hSULoVXORPauXdCqLuMLNWSvuDs = value;
			}
		}

		public KeyCode keyCode
		{
			get
			{
				return mFLiSaIMLKvvniCzCrvrIvLvGOj;
			}
			internal set
			{
				mFLiSaIMLKvvniCzCrvrIvLvGOj = value;
			}
		}

		public ModifierKeyFlags modifierKeyFlags
		{
			get
			{
				return LgDgYREyjYRivcBMzEebmXsCKxro;
			}
			internal set
			{
				LgDgYREyjYRivcBMzEebmXsCKxro = value;
			}
		}

		public int actionId
		{
			get
			{
				return daRJuBBqMmlroNQwzmbJfOfRARM;
			}
			internal set
			{
				daRJuBBqMmlroNQwzmbJfOfRARM = value;
			}
		}

		public Player player
		{
			get
			{
				if (!ReInput.isReady)
				{
					return null;
				}
				return ReInput.players.GetPlayer(jNwSqiUbJJSyWZCKNvJDjxeYjBF);
			}
		}

		public InputAction action
		{
			get
			{
				if (!ReInput.isReady)
				{
					return null;
				}
				return ReInput.mapping.GetAction(daRJuBBqMmlroNQwzmbJfOfRARM);
			}
		}

		public Controller controller
		{
			get
			{
				if (!ReInput.isReady)
				{
					return null;
				}
				return ReInput.controllers.GetController(eBGLAIjxzCbUtbnMABFkvwtVwkt, aNAUCDXmNrGbeiAKTkerzFPUzNsk);
			}
		}

		public ControllerMap controllerMap
		{
			get
			{
				if (player == null)
				{
					return null;
				}
				return player.controllers.maps.GetMap(eBGLAIjxzCbUtbnMABFkvwtVwkt, aNAUCDXmNrGbeiAKTkerzFPUzNsk, CHdAsOEWFCVaQbFQcbOBQWDLlDnA);
			}
		}

		public ControllerElementIdentifier elementIdentifier
		{
			get
			{
				if (controller == null)
				{
					return null;
				}
				return controller.GetElementIdentifierById(hSULoVXORPauXdCqLuMLNWSvuDs);
			}
		}

		public ActionElementMap elementMap
		{
			get
			{
				if (controllerMap == null)
				{
					return null;
				}
				return controllerMap.GetElementMap(pCavdtuByBGKGwqUMxXlElgAjpF);
			}
		}

		public string elementDisplayName
		{
			get
			{
				if (eBGLAIjxzCbUtbnMABFkvwtVwkt == ControllerType.Keyboard)
				{
					return Keyboard.GetKeyName(mFLiSaIMLKvvniCzCrvrIvLvGOj, LgDgYREyjYRivcBMzEebmXsCKxro);
				}
				if (controller == null)
				{
					return string.Empty;
				}
				ControllerElementIdentifier elementIdentifierById = controller.GetElementIdentifierById(hSULoVXORPauXdCqLuMLNWSvuDs);
				if (elementIdentifierById == null)
				{
					return string.Empty;
				}
				return elementIdentifierById.name;
			}
		}

		public ElementAssignmentConflictInfo(bool isConflict, bool isUserAssignable, int playerId, ControllerType controllerType, int controllerId, int controllerMapId, int elementMapId, int actionId, ControllerElementType elementType, int elementIdentifierId, KeyCode keyCode, ModifierKeyFlags modifierKeyFlags)
		{
			TvyDrFJuZXuENJviWtlChMCfgUht = isConflict;
			EMURwJrQYwCVFwQSclXkSlnEruu = isUserAssignable;
			jNwSqiUbJJSyWZCKNvJDjxeYjBF = playerId;
			eBGLAIjxzCbUtbnMABFkvwtVwkt = controllerType;
			aNAUCDXmNrGbeiAKTkerzFPUzNsk = controllerId;
			CHdAsOEWFCVaQbFQcbOBQWDLlDnA = controllerMapId;
			pCavdtuByBGKGwqUMxXlElgAjpF = elementMapId;
			daRJuBBqMmlroNQwzmbJfOfRARM = actionId;
			xIOHjTqprxflrglDOorVKUSRYqOR = elementType;
			hSULoVXORPauXdCqLuMLNWSvuDs = elementIdentifierId;
			mFLiSaIMLKvvniCzCrvrIvLvGOj = keyCode;
			LgDgYREyjYRivcBMzEebmXsCKxro = modifierKeyFlags;
		}

		public ElementAssignmentConflictInfo(ElementAssignmentConflictInfo source)
		{
			TvyDrFJuZXuENJviWtlChMCfgUht = source.TvyDrFJuZXuENJviWtlChMCfgUht;
			EMURwJrQYwCVFwQSclXkSlnEruu = source.EMURwJrQYwCVFwQSclXkSlnEruu;
			jNwSqiUbJJSyWZCKNvJDjxeYjBF = source.jNwSqiUbJJSyWZCKNvJDjxeYjBF;
			eBGLAIjxzCbUtbnMABFkvwtVwkt = source.eBGLAIjxzCbUtbnMABFkvwtVwkt;
			aNAUCDXmNrGbeiAKTkerzFPUzNsk = source.aNAUCDXmNrGbeiAKTkerzFPUzNsk;
			CHdAsOEWFCVaQbFQcbOBQWDLlDnA = source.CHdAsOEWFCVaQbFQcbOBQWDLlDnA;
			pCavdtuByBGKGwqUMxXlElgAjpF = source.pCavdtuByBGKGwqUMxXlElgAjpF;
			daRJuBBqMmlroNQwzmbJfOfRARM = source.daRJuBBqMmlroNQwzmbJfOfRARM;
			xIOHjTqprxflrglDOorVKUSRYqOR = source.xIOHjTqprxflrglDOorVKUSRYqOR;
			hSULoVXORPauXdCqLuMLNWSvuDs = source.hSULoVXORPauXdCqLuMLNWSvuDs;
			mFLiSaIMLKvvniCzCrvrIvLvGOj = source.mFLiSaIMLKvvniCzCrvrIvLvGOj;
			LgDgYREyjYRivcBMzEebmXsCKxro = source.LgDgYREyjYRivcBMzEebmXsCKxro;
		}
	}
}
