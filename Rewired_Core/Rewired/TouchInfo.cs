using UnityEngine;

namespace Rewired
{
	[CustomClassObfuscation(renamePubIntMembers = false, renamePrivateMembers = true)]
	[CustomObfuscation(rename = false)]
	internal struct TouchInfo
	{
		private bool yyUElHIsQTbVTWVsfwuYQchzxHb;

		private int WyBqwXWkYSPjsUeALlTGGnbyUKH;

		private Vector2 stQyYHSLaZBzmGbYKiEowkswkvF;

		private Vector2 AfiEIsCdUKJuKwrLAWtTdwdvzEdN;

		private Vector2 KPjCWeqsCJbsMKfKRNlHAtJIRrpZ;

		private Vector2 YjOVqsGfDAwPNCAChHGjOiuatJq;

		private float CdKmPhiwKutZTzkXYTTdCCTvjLu;

		private int UyoPYfEboMGpkDYUuICKnytjZoa;

		public bool isValid
		{
			get
			{
				return yyUElHIsQTbVTWVsfwuYQchzxHb;
			}
			internal set
			{
				yyUElHIsQTbVTWVsfwuYQchzxHb = value;
			}
		}

		public int touchId
		{
			get
			{
				return WyBqwXWkYSPjsUeALlTGGnbyUKH;
			}
			internal set
			{
				WyBqwXWkYSPjsUeALlTGGnbyUKH = value;
			}
		}

		public Vector2 touchPos
		{
			get
			{
				return stQyYHSLaZBzmGbYKiEowkswkvF;
			}
			internal set
			{
				stQyYHSLaZBzmGbYKiEowkswkvF = value;
			}
		}

		public Vector2 touchPosRaw
		{
			get
			{
				return AfiEIsCdUKJuKwrLAWtTdwdvzEdN;
			}
			internal set
			{
				AfiEIsCdUKJuKwrLAWtTdwdvzEdN = value;
			}
		}

		public Vector2 deltaPos
		{
			get
			{
				return KPjCWeqsCJbsMKfKRNlHAtJIRrpZ;
			}
			internal set
			{
				KPjCWeqsCJbsMKfKRNlHAtJIRrpZ = value;
			}
		}

		public Vector2 deltaPosRaw
		{
			get
			{
				return YjOVqsGfDAwPNCAChHGjOiuatJq;
			}
			internal set
			{
				YjOVqsGfDAwPNCAChHGjOiuatJq = value;
			}
		}

		public float deltaTime
		{
			get
			{
				return CdKmPhiwKutZTzkXYTTdCCTvjLu;
			}
			internal set
			{
				CdKmPhiwKutZTzkXYTTdCCTvjLu = value;
			}
		}

		public int tapCount
		{
			get
			{
				return UyoPYfEboMGpkDYUuICKnytjZoa;
			}
			internal set
			{
				UyoPYfEboMGpkDYUuICKnytjZoa = value;
			}
		}

		internal static TouchInfo Invalid
		{
			get
			{
				TouchInfo result = default(TouchInfo);
				result.yyUElHIsQTbVTWVsfwuYQchzxHb = false;
				return result;
			}
		}

		internal TouchInfo(bool isValid, int touchId, Vector2 touchPos, Vector2 touchPosRaw, Vector2 deltaPos, Vector2 deltaPosRaw, float deltaTime, int tapCount)
		{
			yyUElHIsQTbVTWVsfwuYQchzxHb = isValid;
			WyBqwXWkYSPjsUeALlTGGnbyUKH = touchId;
			stQyYHSLaZBzmGbYKiEowkswkvF = touchPos;
			AfiEIsCdUKJuKwrLAWtTdwdvzEdN = touchPosRaw;
			KPjCWeqsCJbsMKfKRNlHAtJIRrpZ = deltaPos;
			YjOVqsGfDAwPNCAChHGjOiuatJq = deltaPosRaw;
			CdKmPhiwKutZTzkXYTTdCCTvjLu = deltaTime;
			UyoPYfEboMGpkDYUuICKnytjZoa = tapCount;
		}
	}
}
