using System;

namespace Rewired
{
	public sealed class ControllerStatusChangedEventArgs : EventArgs
	{
		private string BGvCsqKINSdTDMbBCcWESKquxsA;

		private int aNAUCDXmNrGbeiAKTkerzFPUzNsk;

		private ControllerType eBGLAIjxzCbUtbnMABFkvwtVwkt;

		public string name => BGvCsqKINSdTDMbBCcWESKquxsA;

		public int controllerId => aNAUCDXmNrGbeiAKTkerzFPUzNsk;

		public ControllerType controllerType => eBGLAIjxzCbUtbnMABFkvwtVwkt;

		public Controller controller
		{
			get
			{
				if (!ReInput.isReady)
				{
					return null;
				}
				return ReInput.controllers.GetController(eBGLAIjxzCbUtbnMABFkvwtVwkt, aNAUCDXmNrGbeiAKTkerzFPUzNsk);
			}
		}

		public ControllerStatusChangedEventArgs(string name, int uniqueId, ControllerType controllerType)
		{
			BGvCsqKINSdTDMbBCcWESKquxsA = name;
			aNAUCDXmNrGbeiAKTkerzFPUzNsk = uniqueId;
			eBGLAIjxzCbUtbnMABFkvwtVwkt = controllerType;
		}
	}
}
