using System.Collections.Generic;

namespace Rewired
{
	public struct InputActionEventData
	{
		private goErYwwbamXOTPouIvXlmGbXjkS BBYLatetcsKBeYXiIgaJFlcpTFo;

		private InputActionEventType HJxPEIZcGskCGRrYabSUWjFnjHj;

		public readonly int playerId;

		public readonly int actionId;

		public readonly UpdateLoopType updateLoop;

		public InputActionEventType eventType
		{
			get
			{
				return HJxPEIZcGskCGRrYabSUWjFnjHj;
			}
			internal set
			{
				HJxPEIZcGskCGRrYabSUWjFnjHj = value;
			}
		}

		public Player player
		{
			get
			{
				if (!ReInput.isReady)
				{
					return null;
				}
				return ReInput.players.GetPlayer(playerId);
			}
		}

		public string actionName
		{
			get
			{
				if (!ReInput.isReady)
				{
					return string.Empty;
				}
				return ReInput.yWMZCcDtXLrXWdIdoGEBZuygnez.monDmoyJEVWYCoyAjYYvOowlmXZ(actionId).name;
			}
		}

		public string actionDescriptiveName
		{
			get
			{
				if (!ReInput.isReady)
				{
					return string.Empty;
				}
				return ReInput.yWMZCcDtXLrXWdIdoGEBZuygnez.monDmoyJEVWYCoyAjYYvOowlmXZ(actionId).descriptiveName;
			}
		}

		public float GetAxis()
		{
			return BBYLatetcsKBeYXiIgaJFlcpTFo.jysSoXUyiSSkTFElfEJZcYFYLeS();
		}

		public float GetAxisPrev()
		{
			return BBYLatetcsKBeYXiIgaJFlcpTFo.ZIotnUuDGZLmMyClmRjKhSQILoG();
		}

		public float GetAxisDelta()
		{
			return BBYLatetcsKBeYXiIgaJFlcpTFo.ddgfbpefAKeCSechoEYkcoVlCPE();
		}

		public float GetAxisTimeActive()
		{
			return BBYLatetcsKBeYXiIgaJFlcpTFo.ekFVrlWRpHLFLgoXJHsCldzBYN();
		}

		public float GetAxisTimeInactive()
		{
			return BBYLatetcsKBeYXiIgaJFlcpTFo.mlKDvRkrlmaLbUPhdekbcOXOJNuD();
		}

		public float GetAxisRaw()
		{
			return BBYLatetcsKBeYXiIgaJFlcpTFo.iLEDpkJxtcaYxUKyOfGtUzUZAsE();
		}

		public float GetAxisRawDelta()
		{
			return BBYLatetcsKBeYXiIgaJFlcpTFo.MBbDLbBOKvWzdNjhmdtLikCYURFm();
		}

		public float GetAxisRawPrev()
		{
			return BBYLatetcsKBeYXiIgaJFlcpTFo.dnnQgFlnFxsntIRTKexZBusPLuo();
		}

		public float GetAxisRawTimeActive()
		{
			return BBYLatetcsKBeYXiIgaJFlcpTFo.dDKOdJUtDfIsQCaBdAvWcxUpfcd();
		}

		public float GetAxisRawTimeInactive()
		{
			return BBYLatetcsKBeYXiIgaJFlcpTFo.bXoETCMHCpBuECYplDKeGaMitMPd();
		}

		public AxisCoordinateMode GetAxisCoordinateMode()
		{
			return BBYLatetcsKBeYXiIgaJFlcpTFo.dSUBqfiCflQqOGFxfYqsqaJJqbcN();
		}

		public AxisCoordinateMode GetAxisCoordinateModePrev()
		{
			return BBYLatetcsKBeYXiIgaJFlcpTFo.TxIcohdiRvuZGHhMDkCzEToFRrpi();
		}

		public AxisCoordinateMode GetAxisRawCoordinateMode()
		{
			return BBYLatetcsKBeYXiIgaJFlcpTFo.MngSXbOeVFuxeWuSeUeWRbFEGaYj();
		}

		public AxisCoordinateMode GetAxisRawCoordinateModePrev()
		{
			return BBYLatetcsKBeYXiIgaJFlcpTFo.nCmgCeFQsMVxLQKDmRVCDbCgPENg();
		}

		public bool GetButton()
		{
			return BBYLatetcsKBeYXiIgaJFlcpTFo.qgoFgKHxZobvMpubhNPRlpRVdysC();
		}

		public bool GetButtonPrev()
		{
			return BBYLatetcsKBeYXiIgaJFlcpTFo.curAstyswSGGNDMEKdBogWWasCPL();
		}

		public bool GetButtonDown()
		{
			return BBYLatetcsKBeYXiIgaJFlcpTFo.xHHbTaVwKcAdlSkEwCJnKtlcFsPO();
		}

		public bool GetButtonUp()
		{
			return BBYLatetcsKBeYXiIgaJFlcpTFo.LNXLeJpucmMJihssEvoSQWzmmRt();
		}

		public bool GetButtonSinglePressHold()
		{
			return BBYLatetcsKBeYXiIgaJFlcpTFo.fzsEolEAsSfKKqTJnivdCzOglUHC();
		}

		public bool GetButtonSinglePressDown()
		{
			return BBYLatetcsKBeYXiIgaJFlcpTFo.gXIeJSBsbBQhflLcNjJAeSLKxYGW();
		}

		public bool GetButtonSinglePressUp()
		{
			return BBYLatetcsKBeYXiIgaJFlcpTFo.rmoXwpjcZtFgIIbHOSnBmQVpRAkQ();
		}

		public bool GetButtonDoublePressDown()
		{
			return BBYLatetcsKBeYXiIgaJFlcpTFo.RFOXFHtyyMgEYzQGTLtAfetrZTH();
		}

		public bool GetButtonDoublePressDown(float speed)
		{
			return BBYLatetcsKBeYXiIgaJFlcpTFo.RFOXFHtyyMgEYzQGTLtAfetrZTH(speed);
		}

		public bool GetButtonDoublePressHold()
		{
			return BBYLatetcsKBeYXiIgaJFlcpTFo.dBaFkOiZDmjYxelbRyuPVOWWoJR();
		}

		public bool GetButtonDoublePressHold(float speed)
		{
			return BBYLatetcsKBeYXiIgaJFlcpTFo.dBaFkOiZDmjYxelbRyuPVOWWoJR(speed);
		}

		public bool GetButtonDoublePressUp()
		{
			return BBYLatetcsKBeYXiIgaJFlcpTFo.AnCUzGtXOaLDeCMYHCUiWWSbxFl();
		}

		public bool GetButtonDoublePressUp(float speed)
		{
			return BBYLatetcsKBeYXiIgaJFlcpTFo.AnCUzGtXOaLDeCMYHCUiWWSbxFl(speed);
		}

		public bool GetButtonTimedPress(float time)
		{
			return BBYLatetcsKBeYXiIgaJFlcpTFo.ncuknqWomectVJhxEaoLNlmfhGxQ(time, 0f);
		}

		public bool GetButtonTimedPress(float time, float expireIn)
		{
			return BBYLatetcsKBeYXiIgaJFlcpTFo.ncuknqWomectVJhxEaoLNlmfhGxQ(time, expireIn);
		}

		public bool GetButtonTimedPressDown(float time)
		{
			return BBYLatetcsKBeYXiIgaJFlcpTFo.ptIQyDWEChGQCRQKHPdlwQNSVrQ(time);
		}

		public bool GetButtonTimedPressUp(float time)
		{
			return BBYLatetcsKBeYXiIgaJFlcpTFo.ggBkkVfZOwrOBwbWjUlgDugOnNy(time, 0f);
		}

		public bool GetButtonTimedPressUp(float time, float expireIn)
		{
			return BBYLatetcsKBeYXiIgaJFlcpTFo.ggBkkVfZOwrOBwbWjUlgDugOnNy(time, expireIn);
		}

		public bool GetButtonShortPress()
		{
			return BBYLatetcsKBeYXiIgaJFlcpTFo.oyoUBuZlenEMBQlTGiGXLozdTSp();
		}

		public bool GetButtonShortPressDown()
		{
			return BBYLatetcsKBeYXiIgaJFlcpTFo.hmuWyiechwYgtrrncGqSGIhSTmf();
		}

		public bool GetButtonShortPressUp()
		{
			return BBYLatetcsKBeYXiIgaJFlcpTFo.DEUAwAHvLsXvneoHBZuWNcsaIARd();
		}

		public bool GetButtonLongPress()
		{
			return BBYLatetcsKBeYXiIgaJFlcpTFo.mCpvHviHdRNBWMQTWMlhLIfMRcG();
		}

		public bool GetButtonLongPressDown()
		{
			return BBYLatetcsKBeYXiIgaJFlcpTFo.hDvqUYOpIqJDHXMGAfVsMVPgWjU();
		}

		public bool GetButtonLongPressUp()
		{
			return BBYLatetcsKBeYXiIgaJFlcpTFo.dcgcilemQgkilvvRieuyYfQFOklH();
		}

		public bool GetButtonRepeating()
		{
			return BBYLatetcsKBeYXiIgaJFlcpTFo.CxgACMZEegGOpaZTApJsgPaGfaXJ();
		}

		public float GetButtonTimePressed()
		{
			return BBYLatetcsKBeYXiIgaJFlcpTFo.NgtApkCeFuphFSveqbmmcYlEaOOr();
		}

		public float GetButtonTimeUnpressed()
		{
			return BBYLatetcsKBeYXiIgaJFlcpTFo.hlyKXbQZitJTaPLFFICrQZPCGCl();
		}

		public bool GetNegativeButton()
		{
			return BBYLatetcsKBeYXiIgaJFlcpTFo.JrKbrjTsakLJNkhLorxSvdVCaINE();
		}

		public bool GetNegativeButtonPrev()
		{
			return BBYLatetcsKBeYXiIgaJFlcpTFo.OfsayCzPIhHXPEFQkLvIkRiwlan();
		}

		public bool GetNegativeButtonDown()
		{
			return BBYLatetcsKBeYXiIgaJFlcpTFo.BimtWjHWvqgbBcvSXNMYRScZWHM();
		}

		public bool GetNegativeButtonUp()
		{
			return BBYLatetcsKBeYXiIgaJFlcpTFo.WjNeeSOcoAEaxjtqHiZnBREZWbFs();
		}

		public bool GetNegativeButtonSinglePressHold()
		{
			return BBYLatetcsKBeYXiIgaJFlcpTFo.ZnHrfXcenwJPcWqEkcCFZDFvsHD();
		}

		public bool GetNegativeButtonSinglePressDown()
		{
			return BBYLatetcsKBeYXiIgaJFlcpTFo.YeEsSSfiWJIoZBZlYcGFfVoyUFO();
		}

		public bool GetNegativeButtonSinglePressUp()
		{
			return BBYLatetcsKBeYXiIgaJFlcpTFo.oMdPDeBVnrvxVIcvjhaCciKFBWR();
		}

		public bool GetNegativeButtonDoublePressDown()
		{
			return BBYLatetcsKBeYXiIgaJFlcpTFo.RhAMwoxvYfKrJaovWNwoQPaiRJV();
		}

		public bool GetNegativeButtonDoublePressDown(float speed)
		{
			return BBYLatetcsKBeYXiIgaJFlcpTFo.RhAMwoxvYfKrJaovWNwoQPaiRJV(speed);
		}

		public bool GetNegativeButtonDoublePressHold()
		{
			return BBYLatetcsKBeYXiIgaJFlcpTFo.RJlhFLWULitlIhnNbCUjELMyOHq();
		}

		public bool GetNegativeButtonDoublePressHold(float speed)
		{
			return BBYLatetcsKBeYXiIgaJFlcpTFo.RJlhFLWULitlIhnNbCUjELMyOHq(speed);
		}

		public bool GetNegativeButtonDoublePressUp()
		{
			return BBYLatetcsKBeYXiIgaJFlcpTFo.zWDmLqVTSxbODqirLHLmLqgaWzd();
		}

		public bool GetNegativeButtonDoublePressUp(float speed)
		{
			return BBYLatetcsKBeYXiIgaJFlcpTFo.zWDmLqVTSxbODqirLHLmLqgaWzd(speed);
		}

		public bool GetNegativeButtonTimedPress(float time)
		{
			return BBYLatetcsKBeYXiIgaJFlcpTFo.iuuXHLdVfidbWpKsiapokcMkrUOu(time, 0f);
		}

		public bool GetNegativeButtonTimedPress(float time, float expireIn)
		{
			return BBYLatetcsKBeYXiIgaJFlcpTFo.iuuXHLdVfidbWpKsiapokcMkrUOu(time, expireIn);
		}

		public bool GetNegativeButtonTimedPressDown(float time)
		{
			return BBYLatetcsKBeYXiIgaJFlcpTFo.VMofRKDApejYvyihoZkCgMzCDIPL(time);
		}

		public bool GetNegativeButtonTimedPressUp(float time)
		{
			return BBYLatetcsKBeYXiIgaJFlcpTFo.IXqZZGaouPdyTgcoLVCEUiHLGdhH(time, 0f);
		}

		public bool GetNegativeButtonTimedPressUp(float time, float expireIn)
		{
			return BBYLatetcsKBeYXiIgaJFlcpTFo.IXqZZGaouPdyTgcoLVCEUiHLGdhH(time, expireIn);
		}

		public bool GetNegativeButtonShortPress()
		{
			return BBYLatetcsKBeYXiIgaJFlcpTFo.KOLNnBNlgIgHzcubZFlQafnbMbNl();
		}

		public bool GetNegativeButtonShortPressDown()
		{
			return BBYLatetcsKBeYXiIgaJFlcpTFo.leDrcTSjmmdlsIdAdTBRDCfYmKdc();
		}

		public bool GetNegativeButtonShortPressUp()
		{
			return BBYLatetcsKBeYXiIgaJFlcpTFo.qigNloDClSnoWvqFBgdmkuQZqgCu();
		}

		public bool GetNegativeButtonLongPress()
		{
			return BBYLatetcsKBeYXiIgaJFlcpTFo.vErEuSingUIuETZByTWOMGICard();
		}

		public bool GetNegativeButtonLongPressDown()
		{
			return BBYLatetcsKBeYXiIgaJFlcpTFo.qPXwkvezpoEMAjhaFVaayLBsfvl();
		}

		public bool GetNegativeButtonLongPressUp()
		{
			return BBYLatetcsKBeYXiIgaJFlcpTFo.ipAxtwjxJeLCkfseIqZSbUAbxRr();
		}

		public bool GetNegativeButtonRepeating()
		{
			return BBYLatetcsKBeYXiIgaJFlcpTFo.SmeHcrPnqLJkiBRSxKshOIlnTuB();
		}

		public float GetNegativeButtonTimePressed()
		{
			return BBYLatetcsKBeYXiIgaJFlcpTFo.evbyoTKrShjBcWnPdSUbXqXnlAx();
		}

		public float GetNegativeButtonTimeUnpressed()
		{
			return BBYLatetcsKBeYXiIgaJFlcpTFo.uWOCnMcSmAZUiIbCoeeAXYeUgLgi();
		}

		public IList<InputActionSourceData> GetCurrentInputSources()
		{
			return ReInput.pqMqUoRXWgpVtbsIgGXgzxyhjoH.gOxnyJkBBRoSJhuwEvsQOHAkztQ(playerId, actionId, true)?.XacMviRjlQZlQUoXcmmZappxydv();
		}

		public bool IsCurrentInputSource(ControllerType controllerType)
		{
			return ReInput.pqMqUoRXWgpVtbsIgGXgzxyhjoH.gOxnyJkBBRoSJhuwEvsQOHAkztQ(playerId, actionId, true)?.fXTrNPowWBpYZxqKbTJzRMIqbGf(controllerType) ?? false;
		}

		public bool IsCurrentInputSource(ControllerType controllerType, int controllerId)
		{
			return ReInput.pqMqUoRXWgpVtbsIgGXgzxyhjoH.gOxnyJkBBRoSJhuwEvsQOHAkztQ(playerId, actionId, true)?.fXTrNPowWBpYZxqKbTJzRMIqbGf(controllerType, controllerId) ?? false;
		}

		public bool IsCurrentInputSource(Controller controller)
		{
			return ReInput.pqMqUoRXWgpVtbsIgGXgzxyhjoH.gOxnyJkBBRoSJhuwEvsQOHAkztQ(playerId, actionId, true)?.fXTrNPowWBpYZxqKbTJzRMIqbGf(controller) ?? false;
		}

		internal InputActionEventData(goErYwwbamXOTPouIvXlmGbXjkS vc, int playerId, int actionId, UpdateLoopType updateLoop)
		{
			HJxPEIZcGskCGRrYabSUWjFnjHj = InputActionEventType.Update;
			BBYLatetcsKBeYXiIgaJFlcpTFo = vc;
			this.playerId = playerId;
			this.actionId = actionId;
			this.updateLoop = updateLoop;
		}
	}
}
