using UnityEngine;

namespace Rewired
{
	public struct ControllerPollingInfo
	{
		private bool lixENkIdValEbbDBbYLcCNnsLerg;

		private int jNwSqiUbJJSyWZCKNvJDjxeYjBF;

		private int aNAUCDXmNrGbeiAKTkerzFPUzNsk;

		private string JrOrCsDeYETHbsLhhrRuqDGDyyH;

		private ControllerType eBGLAIjxzCbUtbnMABFkvwtVwkt;

		private ControllerElementType xIOHjTqprxflrglDOorVKUSRYqOR;

		private int vdkIPqfMwShfhFIMiCDUOMcdyscN;

		private Pole EvPnXjIySYpVdARIAByiBFUZHImA;

		private string jvTFjOjQXNurnMOSrAmTaCaCMMDA;

		private int hSULoVXORPauXdCqLuMLNWSvuDs;

		private KeyCode mZEtXRwiXObTmddZqpianngbFXI;

		public bool success
		{
			get
			{
				return lixENkIdValEbbDBbYLcCNnsLerg;
			}
			internal set
			{
				lixENkIdValEbbDBbYLcCNnsLerg = value;
			}
		}

		public int playerId
		{
			get
			{
				return jNwSqiUbJJSyWZCKNvJDjxeYjBF;
			}
			internal set
			{
				jNwSqiUbJJSyWZCKNvJDjxeYjBF = value;
			}
		}

		public int controllerId
		{
			get
			{
				return aNAUCDXmNrGbeiAKTkerzFPUzNsk;
			}
			internal set
			{
				aNAUCDXmNrGbeiAKTkerzFPUzNsk = value;
			}
		}

		public string controllerName
		{
			get
			{
				return JrOrCsDeYETHbsLhhrRuqDGDyyH;
			}
			internal set
			{
				JrOrCsDeYETHbsLhhrRuqDGDyyH = value;
			}
		}

		public ControllerType controllerType
		{
			get
			{
				return eBGLAIjxzCbUtbnMABFkvwtVwkt;
			}
			internal set
			{
				eBGLAIjxzCbUtbnMABFkvwtVwkt = value;
			}
		}

		public ControllerElementType elementType
		{
			get
			{
				return xIOHjTqprxflrglDOorVKUSRYqOR;
			}
			internal set
			{
				xIOHjTqprxflrglDOorVKUSRYqOR = value;
			}
		}

		public int elementIndex
		{
			get
			{
				return vdkIPqfMwShfhFIMiCDUOMcdyscN;
			}
			internal set
			{
				vdkIPqfMwShfhFIMiCDUOMcdyscN = value;
			}
		}

		public Pole axisPole
		{
			get
			{
				return EvPnXjIySYpVdARIAByiBFUZHImA;
			}
			internal set
			{
				EvPnXjIySYpVdARIAByiBFUZHImA = value;
			}
		}

		public string elementIdentifierName
		{
			get
			{
				return jvTFjOjQXNurnMOSrAmTaCaCMMDA;
			}
			internal set
			{
				jvTFjOjQXNurnMOSrAmTaCaCMMDA = value;
			}
		}

		public int elementIdentifierId
		{
			get
			{
				return hSULoVXORPauXdCqLuMLNWSvuDs;
			}
			internal set
			{
				hSULoVXORPauXdCqLuMLNWSvuDs = value;
			}
		}

		public KeyCode keyboardKey
		{
			get
			{
				return mZEtXRwiXObTmddZqpianngbFXI;
			}
			internal set
			{
				mZEtXRwiXObTmddZqpianngbFXI = value;
			}
		}

		public Player player
		{
			get
			{
				if (!ReInput.isReady)
				{
					return null;
				}
				if (!ReInput.TMigHLTIZLTdqBmYTQQXsrtiuQZ.DTnlUJgsWckZrgSEPoFnwiBGgDog(jNwSqiUbJJSyWZCKNvJDjxeYjBF))
				{
					return null;
				}
				return ReInput.TMigHLTIZLTdqBmYTQQXsrtiuQZ.EAezzhDBhNrAECYYxmlgyEsGPGk(jNwSqiUbJJSyWZCKNvJDjxeYjBF);
			}
		}

		public Controller controller
		{
			get
			{
				if (!ReInput.isReady)
				{
					return null;
				}
				return ReInput.controllers.GetController(eBGLAIjxzCbUtbnMABFkvwtVwkt, aNAUCDXmNrGbeiAKTkerzFPUzNsk);
			}
		}

		public ControllerElementIdentifier elementIdentifier
		{
			get
			{
				if (!ReInput.isReady)
				{
					return null;
				}
				return controller?.GetElementIdentifierById(hSULoVXORPauXdCqLuMLNWSvuDs);
			}
		}

		internal ControllerPollingInfo(bool success, int playerId, int controllerId, string controllerName, ControllerType controllerType, ControllerElementType elementType, int elementIndex, Pole axisPole, string elementIdentifierName, int elementIdentifierId, KeyCode keyboardKey)
		{
			lixENkIdValEbbDBbYLcCNnsLerg = success;
			jNwSqiUbJJSyWZCKNvJDjxeYjBF = playerId;
			aNAUCDXmNrGbeiAKTkerzFPUzNsk = controllerId;
			JrOrCsDeYETHbsLhhrRuqDGDyyH = controllerName;
			eBGLAIjxzCbUtbnMABFkvwtVwkt = controllerType;
			xIOHjTqprxflrglDOorVKUSRYqOR = elementType;
			vdkIPqfMwShfhFIMiCDUOMcdyscN = elementIndex;
			EvPnXjIySYpVdARIAByiBFUZHImA = axisPole;
			jvTFjOjQXNurnMOSrAmTaCaCMMDA = elementIdentifierName;
			hSULoVXORPauXdCqLuMLNWSvuDs = elementIdentifierId;
			mZEtXRwiXObTmddZqpianngbFXI = keyboardKey;
		}

		internal ControllerPollingInfo(ControllerPollingInfo source)
		{
			lixENkIdValEbbDBbYLcCNnsLerg = source.lixENkIdValEbbDBbYLcCNnsLerg;
			jNwSqiUbJJSyWZCKNvJDjxeYjBF = source.jNwSqiUbJJSyWZCKNvJDjxeYjBF;
			aNAUCDXmNrGbeiAKTkerzFPUzNsk = source.aNAUCDXmNrGbeiAKTkerzFPUzNsk;
			JrOrCsDeYETHbsLhhrRuqDGDyyH = source.JrOrCsDeYETHbsLhhrRuqDGDyyH;
			eBGLAIjxzCbUtbnMABFkvwtVwkt = source.eBGLAIjxzCbUtbnMABFkvwtVwkt;
			xIOHjTqprxflrglDOorVKUSRYqOR = source.xIOHjTqprxflrglDOorVKUSRYqOR;
			vdkIPqfMwShfhFIMiCDUOMcdyscN = source.vdkIPqfMwShfhFIMiCDUOMcdyscN;
			EvPnXjIySYpVdARIAByiBFUZHImA = source.EvPnXjIySYpVdARIAByiBFUZHImA;
			jvTFjOjQXNurnMOSrAmTaCaCMMDA = source.jvTFjOjQXNurnMOSrAmTaCaCMMDA;
			hSULoVXORPauXdCqLuMLNWSvuDs = source.hSULoVXORPauXdCqLuMLNWSvuDs;
			mZEtXRwiXObTmddZqpianngbFXI = source.mZEtXRwiXObTmddZqpianngbFXI;
		}

		internal static ControllerPollingInfo dTcOQCxVcEnAwvttEXOEIJYkhMR()
		{
			return new ControllerPollingInfo(success: false, -1, -1, string.Empty, ControllerType.Keyboard, ControllerElementType.Axis, -1, Pole.Positive, string.Empty, -1, KeyCode.None);
		}
	}
}
