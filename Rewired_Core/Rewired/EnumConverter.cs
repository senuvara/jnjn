using Rewired.Config;
using Rewired.Data.Mapping;
using Rewired.Utils.Classes.Utility;
using System;
using System.Collections.Generic;

namespace Rewired
{
	[CustomObfuscation(rename = false)]
	[CustomClassObfuscation(renamePrivateMembers = true, renamePubIntMembers = false)]
	internal static class EnumConverter
	{
		public static int ToUpdateLoopTypes(UpdateLoopSetting updateLoopSetting, List<UpdateLoopType> results)
		{
			if (results == null)
			{
				throw new ArgumentNullException("results");
			}
			UpdateLoopSetting valueAt = default(UpdateLoopSetting);
			while (true)
			{
				results.Clear();
				EnumNameValueCache<UpdateLoopSetting> @default = EnumNameValueCache<UpdateLoopSetting>.Default;
				int count = @default.Count;
				int num = 0;
				int num2 = 1242024289;
				while (true)
				{
					switch (num2 ^ 0x4A07C965)
					{
					case 0:
						num2 = 1242024294;
						continue;
					case 6:
						if ((updateLoopSetting & valueAt) != 0)
						{
							results.Add(EnumNameValueCache<UpdateLoopType>.Default.GetValue(@default.GetName((long)valueAt)));
							num2 = 1242024288;
							continue;
						}
						goto case 5;
					case 1:
					{
						valueAt = @default.GetValueAt(num);
						int num3;
						if (valueAt == UpdateLoopSetting.None)
						{
							num2 = 1242024288;
							num3 = num2;
						}
						else
						{
							num2 = 1242024291;
							num3 = num2;
						}
						continue;
					}
					case 5:
						num++;
						num2 = 1242024295;
						continue;
					case 3:
						break;
					case 4:
						num2 = 1242024295;
						continue;
					default:
						if (num >= count)
						{
							return results.Count;
						}
						goto case 1;
					}
					break;
				}
			}
		}

		public static AlternateAxisCalibrationType ToAlternateAxisCalibrationType(ThrottleCalibrationMode throttleCalibrationMode)
		{
			//Discarded unreachable code: IL_0030
			while (true)
			{
				switch (-2052550459 ^ -2052550460)
				{
				case 0:
					continue;
				case 1:
					switch (throttleCalibrationMode)
					{
					case ThrottleCalibrationMode.ZeroToOne:
						break;
					case ThrottleCalibrationMode.NegativeOneToOne:
						return AlternateAxisCalibrationType.ThrottleZeroCenter;
					default:
						throw new NotImplementedException();
					}
					break;
				}
				break;
			}
			return AlternateAxisCalibrationType.Default;
		}
	}
}
