using System.Diagnostics;
using UnityEngine;

namespace Rewired
{
	[CustomClassObfuscation(renamePubIntMembers = false)]
	[CustomObfuscation(rename = false)]
	internal static class Profiler
	{
		private const string qtUUFAXFsdVeqspiskGpunIZETq = "USE_PROFILER must be set in Rewired Core to use the profiler.";

		public static bool enableBinaryLog
		{
			get
			{
				cgHJoaIjuqbaabSXcPwNnwFzFLo();
				return false;
			}
			set
			{
				cgHJoaIjuqbaabSXcPwNnwFzFLo();
			}
		}

		public static bool enabled
		{
			get
			{
				cgHJoaIjuqbaabSXcPwNnwFzFLo();
				return false;
			}
			set
			{
				cgHJoaIjuqbaabSXcPwNnwFzFLo();
			}
		}

		public static string logFile
		{
			get
			{
				cgHJoaIjuqbaabSXcPwNnwFzFLo();
				return string.Empty;
			}
			set
			{
				cgHJoaIjuqbaabSXcPwNnwFzFLo();
			}
		}

		public static bool supported
		{
			get
			{
				cgHJoaIjuqbaabSXcPwNnwFzFLo();
				return false;
			}
		}

		public static uint usedHeapSize
		{
			get
			{
				cgHJoaIjuqbaabSXcPwNnwFzFLo();
				return 0u;
			}
		}

		public static long usedHeapSizeLong
		{
			get
			{
				cgHJoaIjuqbaabSXcPwNnwFzFLo();
				return 0L;
			}
		}

		private static void cgHJoaIjuqbaabSXcPwNnwFzFLo()
		{
			Logger.Log("USE_PROFILER must be set in Rewired Core to use the profiler.");
		}

		[Conditional("ENABLE_PROFILER")]
		public static void AddFramesFromFile(string file)
		{
			cgHJoaIjuqbaabSXcPwNnwFzFLo();
		}

		[Conditional("ENABLE_PROFILER")]
		public static void BeginSample(string name)
		{
			cgHJoaIjuqbaabSXcPwNnwFzFLo();
		}

		[Conditional("ENABLE_PROFILER")]
		public static void BeginSample(string name, Object targetObject)
		{
			cgHJoaIjuqbaabSXcPwNnwFzFLo();
		}

		[Conditional("ENABLE_PROFILER")]
		public static void EndSample()
		{
			cgHJoaIjuqbaabSXcPwNnwFzFLo();
		}

		public static uint GetMonoHeapSize()
		{
			cgHJoaIjuqbaabSXcPwNnwFzFLo();
			return 0u;
		}

		public static long GetMonoHeapSizeLong()
		{
			return 0L;
		}

		public static uint GetMonoUsedSize()
		{
			cgHJoaIjuqbaabSXcPwNnwFzFLo();
			return 0u;
		}

		public static long GetMonoUsedSizeLong()
		{
			return 0L;
		}

		public static int GetRuntimeMemorySize(Object o)
		{
			cgHJoaIjuqbaabSXcPwNnwFzFLo();
			return 0;
		}

		public static long GetRuntimeMemorySizeLong(Object o)
		{
			return 0L;
		}

		public static uint GetTotalAllocatedMemory()
		{
			cgHJoaIjuqbaabSXcPwNnwFzFLo();
			return 0u;
		}

		public static long GetTotalAllocatedMemoryLong()
		{
			return 0L;
		}

		public static uint GetTotalReservedMemory()
		{
			return 0u;
		}

		public static long GetTotalReservedMemoryLong()
		{
			return 0L;
		}

		public static uint GetTotalUnusedReservedMemory()
		{
			return 0u;
		}

		public static long GetTotalUnusedReservedMemoryLong()
		{
			return 0L;
		}
	}
}
