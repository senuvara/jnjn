using System;
using UnityEngine;

namespace Rewired
{
	public sealed class ElementAssignmentInfo
	{
		private readonly ControllerMap WhohQcVMSnuQPnwWDkClJXFjItS;

		private readonly ControllerElementType xIOHjTqprxflrglDOorVKUSRYqOR;

		private readonly int RVbaCwADrjHhraBbIOOUXxcpVRTE;

		private readonly int hSULoVXORPauXdCqLuMLNWSvuDs;

		private readonly AxisRange FsMqmlKoQZjsjAkRkDzgibKOMcWo;

		private readonly KeyCode mFLiSaIMLKvvniCzCrvrIvLvGOj;

		private readonly ModifierKeyFlags LgDgYREyjYRivcBMzEebmXsCKxro;

		private readonly int daRJuBBqMmlroNQwzmbJfOfRARM;

		private readonly Pole OFEjBkhaVzrTvFXhhLIoeRxvFIc;

		private readonly bool uPDaUIHJFThLrGTbfyOtOVSkLQXq;

		public Player player
		{
			get
			{
				if (!ReInput.isReady)
				{
					return null;
				}
				if (WhohQcVMSnuQPnwWDkClJXFjItS == null)
				{
					return null;
				}
				return WhohQcVMSnuQPnwWDkClJXFjItS.player;
			}
		}

		public InputAction action
		{
			get
			{
				if (!ReInput.isReady)
				{
					return null;
				}
				return ReInput.mapping.GetAction(daRJuBBqMmlroNQwzmbJfOfRARM);
			}
		}

		public Controller controller
		{
			get
			{
				if (!ReInput.isReady)
				{
					return null;
				}
				if (WhohQcVMSnuQPnwWDkClJXFjItS == null)
				{
					return null;
				}
				return ReInput.controllers.GetController(WhohQcVMSnuQPnwWDkClJXFjItS.controllerType, WhohQcVMSnuQPnwWDkClJXFjItS.controllerId);
			}
		}

		public ControllerType controllerType
		{
			get
			{
				if (ReInput.isReady)
				{
					while (true)
					{
						int num = -2102724224;
						while (true)
						{
							switch (num ^ -2102724223)
							{
							case 2:
								break;
							case 1:
								goto IL_0025;
							default:
								goto IL_0034;
							}
							break;
							IL_0025:
							if (WhohQcVMSnuQPnwWDkClJXFjItS != null)
							{
								return WhohQcVMSnuQPnwWDkClJXFjItS.controllerType;
							}
							num = -2102724223;
						}
					}
				}
				goto IL_0034;
				IL_0034:
				return ControllerType.Keyboard;
			}
		}

		public int controllerId
		{
			get
			{
				if (!ReInput.isReady || WhohQcVMSnuQPnwWDkClJXFjItS == null)
				{
					return -1;
				}
				return WhohQcVMSnuQPnwWDkClJXFjItS.controllerId;
			}
		}

		public ControllerMap controllerMap => WhohQcVMSnuQPnwWDkClJXFjItS;

		public ControllerElementIdentifier elementIdentifier
		{
			get
			{
				if (controller == null)
				{
					return null;
				}
				return controller.GetElementIdentifierById(hSULoVXORPauXdCqLuMLNWSvuDs);
			}
		}

		public ActionElementMap elementMap
		{
			get
			{
				if (WhohQcVMSnuQPnwWDkClJXFjItS == null)
				{
					return null;
				}
				return WhohQcVMSnuQPnwWDkClJXFjItS.GetElementMap(RVbaCwADrjHhraBbIOOUXxcpVRTE);
			}
		}

		public ControllerElementType elementType => xIOHjTqprxflrglDOorVKUSRYqOR;

		public Pole axisContribution => OFEjBkhaVzrTvFXhhLIoeRxvFIc;

		public AxisRange axisRange => FsMqmlKoQZjsjAkRkDzgibKOMcWo;

		public bool invert => uPDaUIHJFThLrGTbfyOtOVSkLQXq;

		public KeyCode keyCode => mFLiSaIMLKvvniCzCrvrIvLvGOj;

		public ModifierKeyFlags modifierKeyFlags => LgDgYREyjYRivcBMzEebmXsCKxro;

		public string elementDisplayName
		{
			get
			{
				if (WhohQcVMSnuQPnwWDkClJXFjItS == null)
				{
					return string.Empty;
				}
				if (controllerType == ControllerType.Keyboard)
				{
					goto IL_0016;
				}
				Controller controller = this.controller;
				int num;
				if (controller == null)
				{
					num = -946658903;
					goto IL_001b;
				}
				ControllerElementIdentifier elementIdentifierById = controller.GetElementIdentifierById(hSULoVXORPauXdCqLuMLNWSvuDs);
				if (elementIdentifierById == null)
				{
					return string.Empty;
				}
				if (xIOHjTqprxflrglDOorVKUSRYqOR == ControllerElementType.Axis)
				{
					if (FsMqmlKoQZjsjAkRkDzgibKOMcWo == AxisRange.Full)
					{
						return elementIdentifierById.name;
					}
					if (FsMqmlKoQZjsjAkRkDzgibKOMcWo == AxisRange.Positive)
					{
						return elementIdentifierById.positiveName;
					}
					if (FsMqmlKoQZjsjAkRkDzgibKOMcWo == AxisRange.Negative)
					{
						return elementIdentifierById.negativeName;
					}
				}
				return elementIdentifierById.name;
				IL_001b:
				switch (num ^ -946658904)
				{
				case 0:
					break;
				case 2:
					return Keyboard.GetKeyName(keyCode, modifierKeyFlags);
				default:
					return string.Empty;
				}
				goto IL_0016;
				IL_0016:
				num = -946658902;
				goto IL_001b;
			}
		}

		internal ElementAssignmentInfo(ControllerMap controllerMap, ElementAssignment assignment)
		{
			//Discarded unreachable code: IL_00fd
			while (true)
			{
				int num = 1478662226;
				while (true)
				{
					switch (num ^ 0x58229853)
					{
					default:
						return;
					case 5:
						return;
					case 4:
						break;
					case 0:
					{
						xIOHjTqprxflrglDOorVKUSRYqOR = wCRTwNGWCOIyihVCeAXxqcfkDyc.BWXpzwPaIFHlTLMBaamsifLKIRU(assignment.type);
						OFEjBkhaVzrTvFXhhLIoeRxvFIc = assignment.axisContribution;
						FsMqmlKoQZjsjAkRkDzgibKOMcWo = assignment.axisRange;
						int num2;
						if (WhohQcVMSnuQPnwWDkClJXFjItS.controllerType == ControllerType.Keyboard)
						{
							num = 1478662228;
							num2 = num;
						}
						else
						{
							num = 1478662230;
							num2 = num;
						}
						continue;
					}
					case 3:
						RVbaCwADrjHhraBbIOOUXxcpVRTE = assignment.elementMapId;
						hSULoVXORPauXdCqLuMLNWSvuDs = assignment.elementIdentifierId;
						mFLiSaIMLKvvniCzCrvrIvLvGOj = assignment.keyboardKey;
						num = 1478662229;
						continue;
					case 2:
						daRJuBBqMmlroNQwzmbJfOfRARM = assignment.actionId;
						WhohQcVMSnuQPnwWDkClJXFjItS = controllerMap;
						num = 1478662224;
						continue;
					case 7:
						Keyboard.FixKeyboardAssignments(ref hSULoVXORPauXdCqLuMLNWSvuDs, ref mFLiSaIMLKvvniCzCrvrIvLvGOj);
						num = 1478662230;
						continue;
					case 1:
						if (controllerMap == null)
						{
							throw new ArgumentNullException("controllerMap");
						}
						goto case 2;
					case 6:
						LgDgYREyjYRivcBMzEebmXsCKxro = assignment.modifierKeyFlags;
						uPDaUIHJFThLrGTbfyOtOVSkLQXq = assignment.invert;
						num = 1478662227;
						continue;
					}
					break;
				}
			}
		}
	}
}
