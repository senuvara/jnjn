using System;
using System.Collections.Generic;

namespace Rewired
{
	[CustomObfuscation(rename = false)]
	[CustomClassObfuscation(renamePubIntMembers = false, renamePrivateMembers = true)]
	internal class SteamActionSet
	{
		public readonly string name;

		public readonly ulong handle;

		public readonly Dictionary<string, SteamAction> actions;

		public SteamActionSet(string name, ulong handle)
		{
			this.name = name;
			this.handle = handle;
			actions = new Dictionary<string, SteamAction>();
		}

		public void AddAction(SteamAction action)
		{
			if (action == null)
			{
				throw new ArgumentNullException();
			}
			while (true)
			{
				actions.Add(action.name, action);
				int num = -653406466;
				while (true)
				{
					switch (num ^ -653406468)
					{
					default:
						return;
					case 2:
						return;
					case 0:
						goto IL_0009;
					case 1:
						break;
					}
					break;
					IL_0009:
					num = -653406467;
				}
			}
		}
	}
}
