using UnityEngine;

namespace Rewired
{
	public struct ElementAssignmentConflictCheck
	{
		private ElementAssignmentType jnIxJPJDDasOXgYfGTVoRVTejFS;

		private int jNwSqiUbJJSyWZCKNvJDjxeYjBF;

		private ControllerType eBGLAIjxzCbUtbnMABFkvwtVwkt;

		private int aNAUCDXmNrGbeiAKTkerzFPUzNsk;

		private int CHdAsOEWFCVaQbFQcbOBQWDLlDnA;

		private int VAooMkzWjhkWuKATOgqnrIKnhOP;

		private int pCavdtuByBGKGwqUMxXlElgAjpF;

		private int hSULoVXORPauXdCqLuMLNWSvuDs;

		private AxisRange FsMqmlKoQZjsjAkRkDzgibKOMcWo;

		private KeyCode mZEtXRwiXObTmddZqpianngbFXI;

		private ModifierKeyFlags LgDgYREyjYRivcBMzEebmXsCKxro;

		private int daRJuBBqMmlroNQwzmbJfOfRARM;

		private Pole OFEjBkhaVzrTvFXhhLIoeRxvFIc;

		private bool uPDaUIHJFThLrGTbfyOtOVSkLQXq;

		public ElementAssignmentType elementAssignmentType
		{
			get
			{
				return jnIxJPJDDasOXgYfGTVoRVTejFS;
			}
			set
			{
				jnIxJPJDDasOXgYfGTVoRVTejFS = value;
			}
		}

		public int playerId
		{
			get
			{
				return jNwSqiUbJJSyWZCKNvJDjxeYjBF;
			}
			set
			{
				jNwSqiUbJJSyWZCKNvJDjxeYjBF = value;
			}
		}

		public ControllerType controllerType
		{
			get
			{
				return eBGLAIjxzCbUtbnMABFkvwtVwkt;
			}
			set
			{
				eBGLAIjxzCbUtbnMABFkvwtVwkt = value;
			}
		}

		public int controllerId
		{
			get
			{
				return aNAUCDXmNrGbeiAKTkerzFPUzNsk;
			}
			set
			{
				aNAUCDXmNrGbeiAKTkerzFPUzNsk = value;
			}
		}

		public int controllerMapId
		{
			get
			{
				return CHdAsOEWFCVaQbFQcbOBQWDLlDnA;
			}
			set
			{
				CHdAsOEWFCVaQbFQcbOBQWDLlDnA = value;
			}
		}

		public int controllerMapCategoryId
		{
			get
			{
				return VAooMkzWjhkWuKATOgqnrIKnhOP;
			}
			set
			{
				VAooMkzWjhkWuKATOgqnrIKnhOP = value;
			}
		}

		public int elementMapId
		{
			get
			{
				return pCavdtuByBGKGwqUMxXlElgAjpF;
			}
			set
			{
				pCavdtuByBGKGwqUMxXlElgAjpF = value;
			}
		}

		public int elementIdentifierId
		{
			get
			{
				return hSULoVXORPauXdCqLuMLNWSvuDs;
			}
			set
			{
				hSULoVXORPauXdCqLuMLNWSvuDs = value;
			}
		}

		public AxisRange axisRange
		{
			get
			{
				return FsMqmlKoQZjsjAkRkDzgibKOMcWo;
			}
			set
			{
				FsMqmlKoQZjsjAkRkDzgibKOMcWo = value;
			}
		}

		public KeyCode keyboardKey
		{
			get
			{
				return mZEtXRwiXObTmddZqpianngbFXI;
			}
			set
			{
				mZEtXRwiXObTmddZqpianngbFXI = value;
			}
		}

		public ModifierKeyFlags modifierKeyFlags
		{
			get
			{
				return LgDgYREyjYRivcBMzEebmXsCKxro;
			}
			set
			{
				LgDgYREyjYRivcBMzEebmXsCKxro = value;
			}
		}

		public int actionId
		{
			get
			{
				return daRJuBBqMmlroNQwzmbJfOfRARM;
			}
			set
			{
				daRJuBBqMmlroNQwzmbJfOfRARM = value;
			}
		}

		public Pole axisContribution
		{
			get
			{
				return OFEjBkhaVzrTvFXhhLIoeRxvFIc;
			}
			set
			{
				OFEjBkhaVzrTvFXhhLIoeRxvFIc = value;
			}
		}

		public bool invert
		{
			get
			{
				return uPDaUIHJFThLrGTbfyOtOVSkLQXq;
			}
			set
			{
				uPDaUIHJFThLrGTbfyOtOVSkLQXq = value;
			}
		}

		public ElementAssignmentConflictCheck(int playerId, ControllerType controllerType, int controllerId, int controllerMapId, ElementAssignmentType elementAssignmentType, int elementIdentifierId, AxisRange axisRange, KeyCode keyboardKey, ModifierKeyFlags modifierKeyFlags, int actionId, Pole axisContribution, bool invert)
		{
			jNwSqiUbJJSyWZCKNvJDjxeYjBF = playerId;
			eBGLAIjxzCbUtbnMABFkvwtVwkt = controllerType;
			aNAUCDXmNrGbeiAKTkerzFPUzNsk = controllerId;
			CHdAsOEWFCVaQbFQcbOBQWDLlDnA = controllerMapId;
			jnIxJPJDDasOXgYfGTVoRVTejFS = elementAssignmentType;
			hSULoVXORPauXdCqLuMLNWSvuDs = elementIdentifierId;
			FsMqmlKoQZjsjAkRkDzgibKOMcWo = axisRange;
			mZEtXRwiXObTmddZqpianngbFXI = keyboardKey;
			LgDgYREyjYRivcBMzEebmXsCKxro = modifierKeyFlags;
			daRJuBBqMmlroNQwzmbJfOfRARM = actionId;
			OFEjBkhaVzrTvFXhhLIoeRxvFIc = axisContribution;
			uPDaUIHJFThLrGTbfyOtOVSkLQXq = invert;
			pCavdtuByBGKGwqUMxXlElgAjpF = -1;
			VAooMkzWjhkWuKATOgqnrIKnhOP = -1;
			qLGgTjPkhgSLwMYCvmznAYEobSf();
		}

		public ElementAssignmentConflictCheck(int playerId, ControllerType controllerType, int controllerId, int controllerMapId, ElementAssignmentType elementAssignmentType, int elementIdentifierId, AxisRange axisRange, KeyCode keyboardKey, ModifierKeyFlags modifierKeyFlags, int actionId, Pole axisContribution, bool invert, int elementMapId)
		{
			jNwSqiUbJJSyWZCKNvJDjxeYjBF = playerId;
			eBGLAIjxzCbUtbnMABFkvwtVwkt = controllerType;
			aNAUCDXmNrGbeiAKTkerzFPUzNsk = controllerId;
			CHdAsOEWFCVaQbFQcbOBQWDLlDnA = controllerMapId;
			jnIxJPJDDasOXgYfGTVoRVTejFS = elementAssignmentType;
			hSULoVXORPauXdCqLuMLNWSvuDs = elementIdentifierId;
			FsMqmlKoQZjsjAkRkDzgibKOMcWo = axisRange;
			mZEtXRwiXObTmddZqpianngbFXI = keyboardKey;
			LgDgYREyjYRivcBMzEebmXsCKxro = modifierKeyFlags;
			daRJuBBqMmlroNQwzmbJfOfRARM = actionId;
			OFEjBkhaVzrTvFXhhLIoeRxvFIc = axisContribution;
			uPDaUIHJFThLrGTbfyOtOVSkLQXq = invert;
			pCavdtuByBGKGwqUMxXlElgAjpF = elementMapId;
			VAooMkzWjhkWuKATOgqnrIKnhOP = -1;
			qLGgTjPkhgSLwMYCvmznAYEobSf();
		}

		public ElementAssignmentConflictCheck(int playerId, ControllerType controllerType, int controllerId, int controllerMapId, ControllerElementType elementType, int elementIdentifierId, AxisRange axisRange, KeyCode keyboardKey, ModifierKeyFlags modifierKeyFlags, int actionId, Pole axisContribution, bool invert)
		{
			jNwSqiUbJJSyWZCKNvJDjxeYjBF = playerId;
			eBGLAIjxzCbUtbnMABFkvwtVwkt = controllerType;
			aNAUCDXmNrGbeiAKTkerzFPUzNsk = controllerId;
			CHdAsOEWFCVaQbFQcbOBQWDLlDnA = controllerMapId;
			jnIxJPJDDasOXgYfGTVoRVTejFS = wCRTwNGWCOIyihVCeAXxqcfkDyc.yMZDxBFNQdUZCxfHaKEcVkbkwZs(controllerType, elementType, axisRange);
			hSULoVXORPauXdCqLuMLNWSvuDs = elementIdentifierId;
			FsMqmlKoQZjsjAkRkDzgibKOMcWo = axisRange;
			mZEtXRwiXObTmddZqpianngbFXI = keyboardKey;
			LgDgYREyjYRivcBMzEebmXsCKxro = modifierKeyFlags;
			daRJuBBqMmlroNQwzmbJfOfRARM = actionId;
			OFEjBkhaVzrTvFXhhLIoeRxvFIc = axisContribution;
			uPDaUIHJFThLrGTbfyOtOVSkLQXq = invert;
			pCavdtuByBGKGwqUMxXlElgAjpF = -1;
			VAooMkzWjhkWuKATOgqnrIKnhOP = -1;
			qLGgTjPkhgSLwMYCvmznAYEobSf();
		}

		public ElementAssignmentConflictCheck(int playerId, ControllerType controllerType, int controllerId, int controllerMapId, ControllerElementType elementType, int elementIdentifierId, AxisRange axisRange, KeyCode keyboardKey, ModifierKeyFlags modifierKeyFlags, int actionId, Pole axisContribution, bool invert, int elementMapId)
		{
			jnIxJPJDDasOXgYfGTVoRVTejFS = wCRTwNGWCOIyihVCeAXxqcfkDyc.yMZDxBFNQdUZCxfHaKEcVkbkwZs(controllerType, elementType, axisRange);
			jNwSqiUbJJSyWZCKNvJDjxeYjBF = playerId;
			eBGLAIjxzCbUtbnMABFkvwtVwkt = controllerType;
			aNAUCDXmNrGbeiAKTkerzFPUzNsk = controllerId;
			CHdAsOEWFCVaQbFQcbOBQWDLlDnA = controllerMapId;
			hSULoVXORPauXdCqLuMLNWSvuDs = elementIdentifierId;
			FsMqmlKoQZjsjAkRkDzgibKOMcWo = axisRange;
			mZEtXRwiXObTmddZqpianngbFXI = keyboardKey;
			LgDgYREyjYRivcBMzEebmXsCKxro = modifierKeyFlags;
			daRJuBBqMmlroNQwzmbJfOfRARM = actionId;
			OFEjBkhaVzrTvFXhhLIoeRxvFIc = axisContribution;
			uPDaUIHJFThLrGTbfyOtOVSkLQXq = invert;
			pCavdtuByBGKGwqUMxXlElgAjpF = elementMapId;
			VAooMkzWjhkWuKATOgqnrIKnhOP = -1;
			qLGgTjPkhgSLwMYCvmznAYEobSf();
		}

		public ElementAssignmentConflictCheck(int playerId, ControllerType controllerType, int controllerId, int controllerMapId, int elementIdentifierId, int actionId, bool invert)
		{
			jnIxJPJDDasOXgYfGTVoRVTejFS = ElementAssignmentType.FullAxis;
			jNwSqiUbJJSyWZCKNvJDjxeYjBF = playerId;
			eBGLAIjxzCbUtbnMABFkvwtVwkt = controllerType;
			aNAUCDXmNrGbeiAKTkerzFPUzNsk = controllerId;
			CHdAsOEWFCVaQbFQcbOBQWDLlDnA = controllerMapId;
			hSULoVXORPauXdCqLuMLNWSvuDs = elementIdentifierId;
			FsMqmlKoQZjsjAkRkDzgibKOMcWo = AxisRange.Full;
			mZEtXRwiXObTmddZqpianngbFXI = KeyCode.None;
			LgDgYREyjYRivcBMzEebmXsCKxro = ModifierKeyFlags.None;
			daRJuBBqMmlroNQwzmbJfOfRARM = actionId;
			OFEjBkhaVzrTvFXhhLIoeRxvFIc = Pole.Positive;
			uPDaUIHJFThLrGTbfyOtOVSkLQXq = invert;
			pCavdtuByBGKGwqUMxXlElgAjpF = -1;
			VAooMkzWjhkWuKATOgqnrIKnhOP = -1;
			qLGgTjPkhgSLwMYCvmznAYEobSf();
		}

		public ElementAssignmentConflictCheck(int playerId, ControllerType controllerType, int controllerId, int controllerMapId, int elementIdentifierId, int actionId, bool invert, int elementMapId)
		{
			jnIxJPJDDasOXgYfGTVoRVTejFS = ElementAssignmentType.FullAxis;
			jNwSqiUbJJSyWZCKNvJDjxeYjBF = playerId;
			eBGLAIjxzCbUtbnMABFkvwtVwkt = controllerType;
			aNAUCDXmNrGbeiAKTkerzFPUzNsk = controllerId;
			CHdAsOEWFCVaQbFQcbOBQWDLlDnA = controllerMapId;
			hSULoVXORPauXdCqLuMLNWSvuDs = elementIdentifierId;
			FsMqmlKoQZjsjAkRkDzgibKOMcWo = AxisRange.Full;
			mZEtXRwiXObTmddZqpianngbFXI = KeyCode.None;
			LgDgYREyjYRivcBMzEebmXsCKxro = ModifierKeyFlags.None;
			daRJuBBqMmlroNQwzmbJfOfRARM = actionId;
			OFEjBkhaVzrTvFXhhLIoeRxvFIc = Pole.Positive;
			uPDaUIHJFThLrGTbfyOtOVSkLQXq = invert;
			pCavdtuByBGKGwqUMxXlElgAjpF = elementMapId;
			VAooMkzWjhkWuKATOgqnrIKnhOP = -1;
			qLGgTjPkhgSLwMYCvmznAYEobSf();
		}

		public ElementAssignmentConflictCheck(int playerId, ControllerType controllerType, int controllerId, int controllerMapId, int elementIdentifierId, AxisRange axisRange, int actionId, Pole axisContribution)
		{
			jnIxJPJDDasOXgYfGTVoRVTejFS = ElementAssignmentType.FullAxis;
			jNwSqiUbJJSyWZCKNvJDjxeYjBF = playerId;
			eBGLAIjxzCbUtbnMABFkvwtVwkt = controllerType;
			aNAUCDXmNrGbeiAKTkerzFPUzNsk = controllerId;
			CHdAsOEWFCVaQbFQcbOBQWDLlDnA = controllerMapId;
			hSULoVXORPauXdCqLuMLNWSvuDs = elementIdentifierId;
			FsMqmlKoQZjsjAkRkDzgibKOMcWo = axisRange;
			mZEtXRwiXObTmddZqpianngbFXI = KeyCode.None;
			LgDgYREyjYRivcBMzEebmXsCKxro = ModifierKeyFlags.None;
			daRJuBBqMmlroNQwzmbJfOfRARM = actionId;
			OFEjBkhaVzrTvFXhhLIoeRxvFIc = axisContribution;
			uPDaUIHJFThLrGTbfyOtOVSkLQXq = false;
			pCavdtuByBGKGwqUMxXlElgAjpF = -1;
			VAooMkzWjhkWuKATOgqnrIKnhOP = -1;
			qLGgTjPkhgSLwMYCvmznAYEobSf();
		}

		public ElementAssignmentConflictCheck(int playerId, ControllerType controllerType, int controllerId, int controllerMapId, int elementIdentifierId, AxisRange axisRange, int actionId, Pole axisContribution, int elementMapId)
		{
			jnIxJPJDDasOXgYfGTVoRVTejFS = ElementAssignmentType.FullAxis;
			jNwSqiUbJJSyWZCKNvJDjxeYjBF = playerId;
			eBGLAIjxzCbUtbnMABFkvwtVwkt = controllerType;
			aNAUCDXmNrGbeiAKTkerzFPUzNsk = controllerId;
			CHdAsOEWFCVaQbFQcbOBQWDLlDnA = controllerMapId;
			hSULoVXORPauXdCqLuMLNWSvuDs = elementIdentifierId;
			FsMqmlKoQZjsjAkRkDzgibKOMcWo = axisRange;
			mZEtXRwiXObTmddZqpianngbFXI = KeyCode.None;
			LgDgYREyjYRivcBMzEebmXsCKxro = ModifierKeyFlags.None;
			daRJuBBqMmlroNQwzmbJfOfRARM = actionId;
			OFEjBkhaVzrTvFXhhLIoeRxvFIc = axisContribution;
			uPDaUIHJFThLrGTbfyOtOVSkLQXq = false;
			pCavdtuByBGKGwqUMxXlElgAjpF = elementMapId;
			VAooMkzWjhkWuKATOgqnrIKnhOP = -1;
			qLGgTjPkhgSLwMYCvmznAYEobSf();
		}

		public ElementAssignmentConflictCheck(int playerId, ControllerType controllerType, int controllerId, int controllerMapId, int elementIdentifierId, int actionId, Pole axisContribution)
		{
			jnIxJPJDDasOXgYfGTVoRVTejFS = ElementAssignmentType.Button;
			jNwSqiUbJJSyWZCKNvJDjxeYjBF = playerId;
			eBGLAIjxzCbUtbnMABFkvwtVwkt = controllerType;
			aNAUCDXmNrGbeiAKTkerzFPUzNsk = controllerId;
			CHdAsOEWFCVaQbFQcbOBQWDLlDnA = controllerMapId;
			hSULoVXORPauXdCqLuMLNWSvuDs = elementIdentifierId;
			FsMqmlKoQZjsjAkRkDzgibKOMcWo = AxisRange.Full;
			mZEtXRwiXObTmddZqpianngbFXI = KeyCode.None;
			LgDgYREyjYRivcBMzEebmXsCKxro = ModifierKeyFlags.None;
			daRJuBBqMmlroNQwzmbJfOfRARM = actionId;
			OFEjBkhaVzrTvFXhhLIoeRxvFIc = axisContribution;
			uPDaUIHJFThLrGTbfyOtOVSkLQXq = false;
			pCavdtuByBGKGwqUMxXlElgAjpF = -1;
			VAooMkzWjhkWuKATOgqnrIKnhOP = -1;
			qLGgTjPkhgSLwMYCvmznAYEobSf();
		}

		public ElementAssignmentConflictCheck(int playerId, ControllerType controllerType, int controllerId, int controllerMapId, int elementIdentifierId, int actionId, Pole axisContribution, int elementMapId)
		{
			jnIxJPJDDasOXgYfGTVoRVTejFS = ElementAssignmentType.Button;
			jNwSqiUbJJSyWZCKNvJDjxeYjBF = playerId;
			eBGLAIjxzCbUtbnMABFkvwtVwkt = controllerType;
			aNAUCDXmNrGbeiAKTkerzFPUzNsk = controllerId;
			CHdAsOEWFCVaQbFQcbOBQWDLlDnA = controllerMapId;
			hSULoVXORPauXdCqLuMLNWSvuDs = elementIdentifierId;
			FsMqmlKoQZjsjAkRkDzgibKOMcWo = AxisRange.Full;
			mZEtXRwiXObTmddZqpianngbFXI = KeyCode.None;
			LgDgYREyjYRivcBMzEebmXsCKxro = ModifierKeyFlags.None;
			daRJuBBqMmlroNQwzmbJfOfRARM = actionId;
			OFEjBkhaVzrTvFXhhLIoeRxvFIc = axisContribution;
			uPDaUIHJFThLrGTbfyOtOVSkLQXq = false;
			pCavdtuByBGKGwqUMxXlElgAjpF = elementMapId;
			VAooMkzWjhkWuKATOgqnrIKnhOP = -1;
			qLGgTjPkhgSLwMYCvmznAYEobSf();
		}

		public ElementAssignmentConflictCheck(int playerId, ControllerType controllerType, int controllerId, int controllerMapId, KeyCode keyboardKey, ModifierKeyFlags modifierKeyFlags, int actionId, Pole axisContribution)
		{
			jnIxJPJDDasOXgYfGTVoRVTejFS = ElementAssignmentType.KeyboardKey;
			jNwSqiUbJJSyWZCKNvJDjxeYjBF = playerId;
			eBGLAIjxzCbUtbnMABFkvwtVwkt = controllerType;
			aNAUCDXmNrGbeiAKTkerzFPUzNsk = controllerId;
			CHdAsOEWFCVaQbFQcbOBQWDLlDnA = controllerMapId;
			hSULoVXORPauXdCqLuMLNWSvuDs = -1;
			FsMqmlKoQZjsjAkRkDzgibKOMcWo = AxisRange.Full;
			mZEtXRwiXObTmddZqpianngbFXI = keyboardKey;
			LgDgYREyjYRivcBMzEebmXsCKxro = modifierKeyFlags;
			daRJuBBqMmlroNQwzmbJfOfRARM = actionId;
			OFEjBkhaVzrTvFXhhLIoeRxvFIc = axisContribution;
			uPDaUIHJFThLrGTbfyOtOVSkLQXq = false;
			pCavdtuByBGKGwqUMxXlElgAjpF = -1;
			VAooMkzWjhkWuKATOgqnrIKnhOP = -1;
			qLGgTjPkhgSLwMYCvmznAYEobSf();
		}

		public ElementAssignmentConflictCheck(int playerId, ControllerType controllerType, int controllerId, int controllerMapId, KeyCode keyboardKey, ModifierKeyFlags modifierKeyFlags, int actionId, Pole axisContribution, int elementMapId)
		{
			jnIxJPJDDasOXgYfGTVoRVTejFS = ElementAssignmentType.KeyboardKey;
			jNwSqiUbJJSyWZCKNvJDjxeYjBF = playerId;
			eBGLAIjxzCbUtbnMABFkvwtVwkt = controllerType;
			aNAUCDXmNrGbeiAKTkerzFPUzNsk = controllerId;
			CHdAsOEWFCVaQbFQcbOBQWDLlDnA = controllerMapId;
			hSULoVXORPauXdCqLuMLNWSvuDs = -1;
			FsMqmlKoQZjsjAkRkDzgibKOMcWo = AxisRange.Full;
			mZEtXRwiXObTmddZqpianngbFXI = keyboardKey;
			LgDgYREyjYRivcBMzEebmXsCKxro = modifierKeyFlags;
			daRJuBBqMmlroNQwzmbJfOfRARM = actionId;
			OFEjBkhaVzrTvFXhhLIoeRxvFIc = axisContribution;
			uPDaUIHJFThLrGTbfyOtOVSkLQXq = false;
			pCavdtuByBGKGwqUMxXlElgAjpF = elementMapId;
			VAooMkzWjhkWuKATOgqnrIKnhOP = -1;
			qLGgTjPkhgSLwMYCvmznAYEobSf();
		}

		public ElementAssignmentConflictCheck(ElementAssignmentConflictCheck source)
		{
			jnIxJPJDDasOXgYfGTVoRVTejFS = source.elementAssignmentType;
			jNwSqiUbJJSyWZCKNvJDjxeYjBF = source.playerId;
			eBGLAIjxzCbUtbnMABFkvwtVwkt = source.controllerType;
			aNAUCDXmNrGbeiAKTkerzFPUzNsk = source.controllerId;
			CHdAsOEWFCVaQbFQcbOBQWDLlDnA = source.controllerMapId;
			pCavdtuByBGKGwqUMxXlElgAjpF = source.elementMapId;
			hSULoVXORPauXdCqLuMLNWSvuDs = source.elementIdentifierId;
			FsMqmlKoQZjsjAkRkDzgibKOMcWo = source.axisRange;
			mZEtXRwiXObTmddZqpianngbFXI = source.keyboardKey;
			LgDgYREyjYRivcBMzEebmXsCKxro = source.modifierKeyFlags;
			daRJuBBqMmlroNQwzmbJfOfRARM = source.actionId;
			OFEjBkhaVzrTvFXhhLIoeRxvFIc = source.axisContribution;
			uPDaUIHJFThLrGTbfyOtOVSkLQXq = source.invert;
			VAooMkzWjhkWuKATOgqnrIKnhOP = source.controllerMapCategoryId;
		}

		public ElementAssignment ToElementAssignment()
		{
			qLGgTjPkhgSLwMYCvmznAYEobSf();
			return new ElementAssignment(elementAssignmentType, elementIdentifierId, axisRange, keyboardKey, modifierKeyFlags, actionId, axisContribution, invert, elementMapId);
		}

		public ElementAssignment ToFullAxisAssignment()
		{
			return new ElementAssignment(elementAssignmentType, elementIdentifierId, AxisRange.Full, KeyCode.None, ModifierKeyFlags.None, actionId, Pole.Positive, invert, elementMapId);
		}

		public ElementAssignment ToSplitAxisAssignment()
		{
			return new ElementAssignment(elementAssignmentType, elementIdentifierId, axisRange, KeyCode.None, ModifierKeyFlags.None, actionId, axisContribution, invert: false, elementMapId);
		}

		public ElementAssignment ToButtonAssignment()
		{
			qLGgTjPkhgSLwMYCvmznAYEobSf();
			return new ElementAssignment(elementAssignmentType, elementIdentifierId, AxisRange.Full, keyboardKey, modifierKeyFlags, actionId, axisContribution, invert: false, elementMapId);
		}

		public ElementAssignment ToKeyboardKeyAssignment()
		{
			qLGgTjPkhgSLwMYCvmznAYEobSf();
			return new ElementAssignment(elementAssignmentType, -1, AxisRange.Full, keyboardKey, modifierKeyFlags, actionId, axisContribution, invert: false, elementMapId);
		}

		private void qLGgTjPkhgSLwMYCvmznAYEobSf()
		{
			if (eBGLAIjxzCbUtbnMABFkvwtVwkt == ControllerType.Keyboard)
			{
				Keyboard.FixKeyboardAssignments(ref hSULoVXORPauXdCqLuMLNWSvuDs, ref mZEtXRwiXObTmddZqpianngbFXI);
			}
		}
	}
}
