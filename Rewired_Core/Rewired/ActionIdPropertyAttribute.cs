using System;
using UnityEngine;

namespace Rewired
{
	public class ActionIdPropertyAttribute : PropertyAttribute
	{
		private Type xgVZxqzhkfLujyUcmEYcqMemNFx;

		public Type Type => xgVZxqzhkfLujyUcmEYcqMemNFx;

		public ActionIdPropertyAttribute(Type type)
		{
			xgVZxqzhkfLujyUcmEYcqMemNFx = type;
		}
	}
}
