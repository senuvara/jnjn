using Rewired.Utils.Classes.Utility;
using System;
using System.Runtime.CompilerServices;

namespace Rewired
{
	[CustomObfuscation(rename = false)]
	[CustomClassObfuscation(renamePubIntMembers = false, renamePrivateMembers = true)]
	internal sealed class LowLevelButtonEvent : LowLevelEvent
	{
		private const int hCDAPWkBBwkzVeOrabwJtFLKZZUu = 100;

		public float value;

		private static readonly ObjectPool<LowLevelButtonEvent> nKZxXKdtKGqJhgflthHHdpVqmPED;

		[CompilerGenerated]
		private static Func<LowLevelButtonEvent> YavMCuUcpHoxDOfoOGeMNuzlsSd;

		private LowLevelButtonEvent()
		{
		}

		static LowLevelButtonEvent()
		{
			nKZxXKdtKGqJhgflthHHdpVqmPED = new ObjectPool<LowLevelButtonEvent>(100, () => new LowLevelButtonEvent());
		}

		public static LowLevelButtonEvent GetPooled(float timestamp, float value)
		{
			LowLevelButtonEvent lowLevelButtonEvent = nKZxXKdtKGqJhgflthHHdpVqmPED.Get();
			lowLevelButtonEvent.id = LowLevelEvent.GetNextId();
			lowLevelButtonEvent.timestamp = timestamp;
			lowLevelButtonEvent.value = value;
			return lowLevelButtonEvent;
		}

		public static void ReturnPooled(LowLevelButtonEvent @event)
		{
			if (@event != null)
			{
				nKZxXKdtKGqJhgflthHHdpVqmPED.Return(@event);
			}
		}

		[CompilerGenerated]
		private static LowLevelButtonEvent TCquobJtgZHjmnPtYEGlFHUVdjxG()
		{
			return new LowLevelButtonEvent();
		}
	}
}
