using Rewired.Utils;

namespace Rewired
{
	[CustomObfuscation(rename = false)]
	[CustomClassObfuscation(renamePubIntMembers = false, renamePrivateMembers = true)]
	internal class UnknownControllerHat
	{
		[CustomClassObfuscation(renamePubIntMembers = false, renamePrivateMembers = true)]
		[CustomObfuscation(rename = false)]
		public class HatButtons
		{
			private int[] ZcKAVjduWmbrAwwnQCXrtPiTLzv;

			public int this[int index] => ZcKAVjduWmbrAwwnQCXrtPiTLzv[index];

			public HatButtons(int[] buttons)
			{
				ZcKAVjduWmbrAwwnQCXrtPiTLzv = buttons;
			}

			public void GetNeighbors(int button, out int neighbor1, out int neighbor2)
			{
				//Discarded unreachable code: IL_0048
				int num = IndexOf(button);
				if (num < 0)
				{
					neighbor1 = -1;
					neighbor2 = -1;
					return;
				}
				while (true)
				{
					IL_0087:
					int num2;
					if (num > 0)
					{
						neighbor1 = ZcKAVjduWmbrAwwnQCXrtPiTLzv[num - 1];
						num2 = 1408889647;
						goto IL_0018;
					}
					goto IL_006d;
					IL_0018:
					while (true)
					{
						switch (num2 ^ 0x53F9F32F)
						{
						case 3:
							num2 = 1408889643;
							continue;
						case 1:
							neighbor2 = ZcKAVjduWmbrAwwnQCXrtPiTLzv[0];
							return;
						case 0:
							break;
						case 5:
							goto IL_006d;
						case 4:
							goto IL_0087;
						default:
							neighbor2 = ZcKAVjduWmbrAwwnQCXrtPiTLzv[num + 1];
							return;
						}
						int num3;
						if (num >= ZcKAVjduWmbrAwwnQCXrtPiTLzv.Length - 1)
						{
							num2 = 1408889646;
							num3 = num2;
						}
						else
						{
							num2 = 1408889645;
							num3 = num2;
						}
					}
					IL_006d:
					neighbor1 = ZcKAVjduWmbrAwwnQCXrtPiTLzv[ZcKAVjduWmbrAwwnQCXrtPiTLzv.Length - 1];
					num2 = 1408889647;
					goto IL_0018;
				}
			}

			public bool IsCardinal(int button)
			{
				int num = IndexOf(button);
				if (num < 0)
				{
					return false;
				}
				return MathTools.IsEven(num);
			}

			public bool IsCorner(int button)
			{
				int num = IndexOf(button);
				if (num < 0)
				{
					return false;
				}
				return !MathTools.IsEven(num);
			}

			public int IndexOf(int button)
			{
				int num = 0;
				while (num < ZcKAVjduWmbrAwwnQCXrtPiTLzv.Length)
				{
					while (true)
					{
						if (ZcKAVjduWmbrAwwnQCXrtPiTLzv[num] == button)
						{
							return num;
						}
						num++;
						int num2 = -722670016;
						while (true)
						{
							switch (num2 ^ -722670014)
							{
							case 0:
								num2 = -722670013;
								continue;
							case 1:
								break;
							default:
								goto IL_003a;
							}
							break;
						}
					}
					IL_003a:;
				}
				return -1;
			}

			public bool Contains(int button)
			{
				return IndexOf(button) >= 0;
			}
		}

		private HatButtons ZcKAVjduWmbrAwwnQCXrtPiTLzv;

		public UnknownControllerHat(HatButtons buttons)
		{
			ZcKAVjduWmbrAwwnQCXrtPiTLzv = buttons;
		}

		public bool ContainsButtonIndex(int index)
		{
			int num = 0;
			while (true)
			{
				int num2 = 2112745111;
				while (true)
				{
					switch (num2 ^ 0x7DEDEE96)
					{
					case 3:
						break;
					case 1:
						num2 = 2112745108;
						continue;
					case 0:
						if (ZcKAVjduWmbrAwwnQCXrtPiTLzv.Contains(index))
						{
							return true;
						}
						num++;
						num2 = 2112745108;
						continue;
					default:
						if (num >= 8)
						{
							return false;
						}
						goto case 0;
					}
					break;
				}
			}
		}

		public bool IsButtonIndexCardinal(int index)
		{
			int num = 0;
			while (true)
			{
				int num2;
				int num3;
				if (num < 8)
				{
					num2 = -1810589401;
					num3 = num2;
				}
				else
				{
					num2 = -1810589402;
					num3 = num2;
				}
				while (true)
				{
					switch (num2 ^ -1810589403)
					{
					case 0:
						num2 = -1810589401;
						continue;
					case 2:
						if (ZcKAVjduWmbrAwwnQCXrtPiTLzv.IsCardinal(index))
						{
							return true;
						}
						num++;
						num2 = -1810589404;
						continue;
					case 1:
						break;
					default:
						return false;
					}
					break;
				}
			}
		}

		public HatButtons GetButtons()
		{
			return ZcKAVjduWmbrAwwnQCXrtPiTLzv;
		}
	}
}
