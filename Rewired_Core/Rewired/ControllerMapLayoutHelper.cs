using Rewired.Utils;
using Rewired.Utils.Attributes;
using Rewired.Utils.Classes.Data;
using Rewired.Utils.Libraries.TinyJson;
using System;
using System.Collections.Generic;
using UnityEngine;

namespace Rewired
{
	internal sealed class ControllerMapLayoutHelper
	{
		[Serializable]
		[Preserve]
		public class Entry
		{
			[SerializeField]
			[Serialize(Name = "tag")]
			private string _tag;

			[Serialize(Name = "categoryIds")]
			[SerializeField]
			private int[] _categoryIds;

			[Serialize(Name = "layoutId")]
			[SerializeField]
			private int _layoutId;

			[Serialize(Name = "controllerSelector")]
			[SerializeField]
			private ZVOPkHmpobXEknQJZShcFQBpbsRa _controllerSelector;

			[NonSerialized]
			private string[] _preInitCategoryNames;

			[NonSerialized]
			private string _preInitLayoutName;

			[DoNotSerialize]
			public string tag
			{
				get
				{
					return _tag;
				}
				set
				{
					_tag = value;
				}
			}

			[DoNotSerialize]
			public ZVOPkHmpobXEknQJZShcFQBpbsRa controllerSelector
			{
				get
				{
					return _controllerSelector ?? (_controllerSelector = new ZVOPkHmpobXEknQJZShcFQBpbsRa(ZVOPkHmpobXEknQJZShcFQBpbsRa.Type.ControllerType));
				}
				set
				{
					if (value == null)
					{
						value = new ZVOPkHmpobXEknQJZShcFQBpbsRa(ZVOPkHmpobXEknQJZShcFQBpbsRa.Type.ControllerType);
						goto IL_000b;
					}
					goto IL_0068;
					IL_000b:
					int num = 2000442636;
					goto IL_0010;
					IL_0068:
					int num2;
					if (value.yvLiueCESwSvNDaZmDXVgPpfypFP)
					{
						num = 2000442634;
						num2 = num;
					}
					else
					{
						num = 2000442639;
						num2 = num;
					}
					goto IL_0010;
					IL_0010:
					while (true)
					{
						switch (num ^ 0x773C550E)
						{
						default:
							return;
						case 3:
							return;
						case 0:
							break;
						case 4:
							_controllerSelector = value;
							num = 2000442637;
							continue;
						case 1:
							Logger.LogError(string.Concat(value.SSkMtjLDbBWEffiHSpoQpCqRSKa, " is not allowed. Each Controller Type has its own unique Layouts and a single Layout cannot be set for all Controller Types. ControllerSelector.type has been changed to ControllerSelector.Type.ControllerType."), requiredThreadSafety: true);
							value.SSkMtjLDbBWEffiHSpoQpCqRSKa = ZVOPkHmpobXEknQJZShcFQBpbsRa.Type.ControllerType;
							num = 2000442634;
							continue;
						case 2:
							goto IL_0068;
						}
						break;
					}
					goto IL_000b;
				}
			}

			[DoNotSerialize]
			public int categoryId
			{
				get
				{
					Initialize();
					if (_categoryIds != null)
					{
						while (true)
						{
							int num = 493017998;
							while (true)
							{
								switch (num ^ 0x1D62DB8F)
								{
								case 2:
									break;
								case 1:
									goto IL_002c;
								default:
									goto IL_003d;
								}
								break;
								IL_002c:
								if (_categoryIds.Length != 0)
								{
									return categoryIds[0];
								}
								num = 493017999;
							}
						}
					}
					goto IL_003d;
					IL_003d:
					return -1;
				}
				set
				{
					if (value < 0)
					{
						_categoryIds = EmptyObjects<int>.array;
						goto IL_007d;
					}
					while (true)
					{
						int num;
						if (_categoryIds != null)
						{
							int num2;
							if (_categoryIds.Length != 0)
							{
								num = 267016942;
								num2 = num;
							}
							else
							{
								num = 267016943;
								num2 = num;
							}
							goto IL_0016;
						}
						goto IL_005a;
						IL_0016:
						while (true)
						{
							switch (num ^ 0xFEA5AEF)
							{
							case 2:
								num = 267016939;
								continue;
							case 4:
								break;
							case 0:
								goto IL_005a;
							case 1:
								_categoryIds[0] = value;
								num = 267016940;
								continue;
							default:
								goto IL_007d;
							}
							break;
						}
						continue;
						IL_005a:
						_categoryIds = new int[1];
						num = 267016942;
						goto IL_0016;
					}
					IL_007d:
					_preInitCategoryNames = null;
				}
			}

			[DoNotSerialize]
			public int[] categoryIds
			{
				get
				{
					Initialize();
					return _categoryIds ?? (_categoryIds = EmptyObjects<int>.array);
				}
				set
				{
					if (value == null)
					{
						goto IL_0003;
					}
					goto IL_0037;
					IL_0037:
					_categoryIds = value;
					int num = -2047056727;
					goto IL_0008;
					IL_0008:
					while (true)
					{
						switch (num ^ -2047056728)
						{
						default:
							return;
						case 2:
							return;
						case 4:
							break;
						case 3:
							value = EmptyObjects<int>.array;
							num = -2047056728;
							continue;
						case 0:
							goto IL_0037;
						case 1:
							_preInitCategoryNames = null;
							num = -2047056726;
							continue;
						}
						break;
					}
					goto IL_0003;
					IL_0003:
					num = -2047056725;
					goto IL_0008;
				}
			}

			[DoNotSerialize]
			public int layoutId
			{
				get
				{
					Initialize();
					return _layoutId;
				}
				set
				{
					if (value < 0)
					{
						goto IL_0004;
					}
					goto IL_0042;
					IL_0042:
					_layoutId = value;
					int num = 1664667122;
					goto IL_0009;
					IL_0009:
					while (true)
					{
						switch (num ^ 0x6338CDF6)
						{
						default:
							return;
						case 1:
							return;
						case 2:
							break;
						case 3:
							value = -1;
							num = 1664667126;
							continue;
						case 4:
							_preInitLayoutName = null;
							num = 1664667127;
							continue;
						case 0:
							goto IL_0042;
						}
						break;
					}
					goto IL_0004;
					IL_0004:
					num = 1664667125;
					goto IL_0009;
				}
			}

			[DoNotSerialize]
			public string categoryName
			{
				get
				{
					//Discarded unreachable code: IL_00b5
					if (!ReInput.isReady)
					{
						if (_preInitCategoryNames != null)
						{
							goto IL_0015;
						}
						goto IL_00bf;
					}
					Initialize();
					int num;
					int num2 = default(int);
					if (_categoryIds != null)
					{
						if (_categoryIds.Length == 0)
						{
							num = -877873817;
						}
						else
						{
							num2 = 0;
							num = -877873813;
						}
						goto IL_001a;
					}
					goto IL_00ec;
					IL_00ec:
					return null;
					IL_001a:
					InputMapCategory mapCategory = default(InputMapCategory);
					while (true)
					{
						switch (num ^ -877873821)
						{
						case 3:
							break;
						case 6:
							return "INVALID";
						case 8:
							goto IL_005b;
						case 5:
							mapCategory = ReInput.mapping.GetMapCategory(_categoryIds[num2]);
							num = -877873821;
							continue;
						case 2:
							goto IL_0091;
						case 0:
							goto IL_00a6;
						case 1:
							goto IL_00bf;
						case 4:
							goto IL_00ec;
						default:
							return "INVALID";
						}
						break;
						IL_00a6:
						if (mapCategory != null)
						{
							return mapCategory.name;
						}
						num = -877873819;
						continue;
						IL_0091:
						if (_preInitCategoryNames.Length <= 0)
						{
							num = -877873822;
							continue;
						}
						return _preInitCategoryNames[0];
						IL_005b:
						int num3;
						if (num2 >= _categoryIds.Length)
						{
							num = -877873820;
							num3 = num;
						}
						else
						{
							num = -877873818;
							num3 = num;
						}
					}
					goto IL_0015;
					IL_0015:
					num = -877873823;
					goto IL_001a;
					IL_00bf:
					return null;
				}
				set
				{
					//Discarded unreachable code: IL_0054, IL_009d, IL_00bd
					if (!ReInput.isReady)
					{
						goto IL_0007;
					}
					goto IL_005b;
					IL_005b:
					int num;
					int num2;
					if (string.IsNullOrEmpty(value))
					{
						num = -439184061;
						num2 = num;
					}
					else
					{
						num = -439184064;
						num2 = num;
					}
					goto IL_000c;
					IL_000c:
					int mapCategoryId = default(int);
					while (true)
					{
						switch (num ^ -439184057)
						{
						case 1:
							return;
						case 2:
							return;
						case 0:
							break;
						case 7:
							mapCategoryId = ReInput.mapping.GetMapCategoryId(value);
							num = -439184063;
							continue;
						case 8:
							goto IL_005b;
						case 3:
							_preInitCategoryNames = ((!string.IsNullOrEmpty(value)) ? new string[1]
							{
								value
							} : null);
							_categoryIds = EmptyObjects<int>.array;
							return;
						case 6:
							if (mapCategoryId >= 0)
							{
								categoryId = mapCategoryId;
								num = -439184059;
								continue;
							}
							goto default;
						case 4:
							_preInitCategoryNames = null;
							_categoryIds = EmptyObjects<int>.array;
							num = -439184058;
							continue;
						default:
							Logger.LogWarning("Map Category \"" + value + "\" does not exist.");
							return;
						}
						break;
					}
					goto IL_0007;
					IL_0007:
					num = -439184060;
					goto IL_000c;
				}
			}

			[DoNotSerialize]
			public string[] categoryNames
			{
				get
				{
					if (!ReInput.isReady)
					{
						if (_preInitCategoryNames == null)
						{
							return EmptyObjects<string>.array;
						}
						return _preInitCategoryNames;
					}
					Initialize();
					if (_categoryIds == null)
					{
						goto IL_002a;
					}
					string[] array = new string[_categoryIds.Length];
					int num = 929829335;
					goto IL_002f;
					IL_002f:
					int num2 = default(int);
					while (true)
					{
						switch (num ^ 0x376C11D4)
						{
						case 2:
							break;
						case 4:
							return EmptyObjects<string>.array;
						case 0:
						{
							InputMapCategory mapCategory = ReInput.mapping.GetMapCategory(_categoryIds[num2]);
							array[num2] = ((mapCategory != null) ? mapCategory.name : "INVALID");
							num = 929829329;
							continue;
						}
						case 5:
							num2++;
							num = 929829333;
							continue;
						case 3:
							num2 = 0;
							num = 929829333;
							continue;
						default:
							if (num2 >= _categoryIds.Length)
							{
								return array;
							}
							goto case 0;
						}
						break;
					}
					goto IL_002a;
					IL_002a:
					num = 929829328;
					goto IL_002f;
				}
				set
				{
					//Discarded unreachable code: IL_0087, IL_00bf
					if (!ReInput.isReady)
					{
						_preInitCategoryNames = ((value != null && value.Length > 0) ? value : null);
						_categoryIds = EmptyObjects<int>.array;
						goto IL_0028;
					}
					goto IL_00de;
					IL_00de:
					int num;
					int num2;
					if (value == null)
					{
						num = -1976541736;
						num2 = num;
					}
					else
					{
						num = -1976541734;
						num2 = num;
					}
					goto IL_002d;
					IL_002d:
					int num3 = default(int);
					List<int> list = default(List<int>);
					while (true)
					{
						switch (num ^ -1976541736)
						{
						case 9:
							return;
						case 6:
							break;
						case 2:
							goto IL_0065;
						case 7:
							num3++;
							num = -1976541735;
							continue;
						case 3:
							Logger.LogWarning("Map Category \"" + value[num3] + "\" does not exist.");
							num = -1976541729;
							continue;
						case 0:
							_preInitCategoryNames = null;
							_categoryIds = EmptyObjects<int>.array;
							return;
						case 4:
							list = new List<int>(value.Length);
							num3 = 0;
							num = -1976541735;
							continue;
						case 5:
							goto IL_00de;
						case 8:
						{
							if (string.IsNullOrEmpty(value[num3]))
							{
								goto case 7;
							}
							int mapCategoryId = ReInput.mapping.GetMapCategoryId(value[num3]);
							if (mapCategoryId >= 0)
							{
								list.Add(mapCategoryId);
								num = -1976541729;
								continue;
							}
							goto case 3;
						}
						default:
							if (num3 >= value.Length)
							{
								_categoryIds = list.ToArray();
								return;
							}
							goto case 8;
						}
						break;
						IL_0065:
						int num4;
						if (value.Length == 0)
						{
							num = -1976541736;
							num4 = num;
						}
						else
						{
							num = -1976541732;
							num4 = num;
						}
					}
					goto IL_0028;
					IL_0028:
					num = -1976541743;
					goto IL_002d;
				}
			}

			[DoNotSerialize]
			public string layoutName
			{
				get
				{
					if (!ReInput.isReady)
					{
						return _preInitLayoutName;
					}
					Initialize();
					ReInput.mapping.GetLayout(controllerSelector.LBvurysqjogiYiEmqeJKfpvGlYKC, _layoutId);
					return "INVALID";
				}
				set
				{
					//Discarded unreachable code: IL_0085
					if (!ReInput.isReady)
					{
						_preInitLayoutName = value;
						_layoutId = -1;
						return;
					}
					object[] array = default(object[]);
					while (true)
					{
						int num;
						int num2;
						if (!string.IsNullOrEmpty(value))
						{
							num = -2072251340;
							num2 = num;
						}
						else
						{
							num = -2072251338;
							num2 = num;
						}
						while (true)
						{
							switch (num ^ -2072251338)
							{
							default:
								return;
							case 7:
								return;
							case 3:
								num = -2072251344;
								continue;
							case 6:
								break;
							case 4:
								Logger.LogWarning(string.Concat(array));
								num = -2072251343;
								continue;
							case 0:
								_preInitLayoutName = null;
								_layoutId = -1;
								return;
							case 2:
								layoutId = ReInput.mapping.GetLayoutId(controllerSelector.LBvurysqjogiYiEmqeJKfpvGlYKC, value);
								num = -2072251341;
								continue;
							case 5:
								if (_layoutId < 0)
								{
									array = new object[4];
									num = -2072251337;
									continue;
								}
								return;
							case 1:
								array[0] = controllerSelector.LBvurysqjogiYiEmqeJKfpvGlYKC;
								array[1] = " Layout \"";
								array[2] = value;
								array[3] = "\" does not exist.";
								num = -2072251342;
								continue;
							}
							break;
						}
					}
				}
			}

			[DoNotSerialize]
			internal bool isValid
			{
				get
				{
					if (_controllerSelector == null)
					{
						return false;
					}
					Initialize();
					bool flag = default(bool);
					int num = default(int);
					int num2;
					if (_categoryIds != null)
					{
						if (_categoryIds.Length == 0)
						{
							goto IL_0022;
						}
						if (!ReInput.isReady)
						{
							if (_categoryIds[0] >= 0)
							{
								return _layoutId >= 0;
							}
							return false;
						}
						flag = false;
						num = 0;
						num2 = -2131441074;
						goto IL_0027;
					}
					goto IL_004c;
					IL_0027:
					while (true)
					{
						switch (num2 ^ -2131441073)
						{
						case 3:
							break;
						case 2:
							goto IL_004c;
						case 4:
							flag = true;
							num2 = -2131441073;
							continue;
						case 0:
							num++;
							num2 = -2131441074;
							continue;
						case 5:
							goto IL_008e;
						default:
							goto IL_00b6;
						}
						break;
						IL_00b6:
						if (num < _categoryIds.Length)
						{
							goto IL_008e;
						}
						goto IL_00c1;
						IL_008e:
						int num3;
						if (ReInput.mapping.GetMapCategory(_categoryIds[num]) != null)
						{
							num2 = -2131441077;
							num3 = num2;
						}
						else
						{
							num2 = -2131441073;
							num3 = num2;
						}
					}
					goto IL_0022;
					IL_0022:
					num2 = -2131441075;
					goto IL_0027;
					IL_00c1:
					if (!flag)
					{
						return false;
					}
					return ReInput.mapping.GetLayout(_controllerSelector.LBvurysqjogiYiEmqeJKfpvGlYKC, _layoutId) != null;
					IL_004c:
					return false;
				}
			}

			public Entry()
			{
				_categoryIds = EmptyObjects<int>.array;
				_layoutId = -1;
				_controllerSelector = new ZVOPkHmpobXEknQJZShcFQBpbsRa(ZVOPkHmpobXEknQJZShcFQBpbsRa.Type.ControllerType);
			}

			private void Initialize()
			{
				//Discarded unreachable code: IL_016d
				if (!ReInput.isReady)
				{
					return;
				}
				int num3 = default(int);
				List<int> list = default(List<int>);
				while (true)
				{
					int num;
					int num2;
					if (_controllerSelector == null)
					{
						num = -2145126095;
						num2 = num;
					}
					else
					{
						num = -2145126086;
						num2 = num;
					}
					while (true)
					{
						switch (num ^ -2145126093)
						{
						default:
							return;
						case 2:
							return;
						case 6:
							return;
						case 7:
							num = -2145126094;
							continue;
						case 8:
							if (!string.IsNullOrEmpty(_preInitLayoutName))
							{
								layoutName = _preInitLayoutName;
								num = -2145126089;
								continue;
							}
							return;
						case 0:
							_preInitCategoryNames = null;
							num = -2145126085;
							continue;
						case 9:
							if (_categoryIds == null)
							{
								_categoryIds = EmptyObjects<int>.array;
								num = -2145126090;
								continue;
							}
							goto case 5;
						case 11:
							num3++;
							num = -2145126081;
							continue;
						case 1:
							break;
						case 13:
							Logger.LogWarning("Map Category \"" + _preInitCategoryNames[num3] + "\" does not exist.");
							num = -2145126088;
							continue;
						case 3:
							num = -2145126088;
							continue;
						case 10:
						{
							if (string.IsNullOrEmpty(_preInitCategoryNames[num3]))
							{
								goto case 11;
							}
							int mapCategoryId = ReInput.mapping.GetMapCategoryId(_preInitCategoryNames[num3]);
							if (mapCategoryId >= 0)
							{
								list.Add(mapCategoryId);
								num = -2145126096;
								continue;
							}
							goto case 13;
						}
						case 5:
							if (_preInitCategoryNames != null && _preInitCategoryNames.Length != 0)
							{
								list = new List<int>(_preInitCategoryNames.Length);
								num3 = 0;
								num = -2145126081;
								continue;
							}
							goto case 8;
						case 12:
							if (num3 >= _preInitCategoryNames.Length)
							{
								_categoryIds = list.ToArray();
								num = -2145126093;
								continue;
							}
							goto case 10;
						case 4:
							_preInitLayoutName = null;
							num = -2145126091;
							continue;
						}
						break;
					}
				}
			}
		}

		private bool _enabled;

		private bool _loadFromUserDataStore = true;

		private Player _player;

		private WoSISrKqsFTIovjAPQFjHwQKWUo _startingControllerMapInfo;

		private readonly int _reInputId;

		private List<Entry> _entries;

		private Action _ApplyCalledEvent;

		public bool enabled
		{
			get
			{
				return _enabled;
			}
			set
			{
				_enabled = value;
				while (true)
				{
					int num = 512166795;
					while (true)
					{
						switch (num ^ 0x1E870B8A)
						{
						default:
							return;
						case 0:
							return;
						case 3:
							break;
						case 1:
						{
							int num2;
							if (!value)
							{
								num = 512166794;
								num2 = num;
							}
							else
							{
								num = 512166792;
								num2 = num;
							}
							continue;
						}
						case 2:
							Apply();
							num = 512166794;
							continue;
						}
						break;
					}
				}
			}
		}

		public bool loadFromUserDataStore
		{
			get
			{
				return _loadFromUserDataStore;
			}
			set
			{
				_loadFromUserDataStore = value;
			}
		}

		public List<Entry> entries
		{
			get
			{
				return _entries;
			}
			set
			{
				if (value == null)
				{
					goto IL_0003;
				}
				goto IL_0033;
				IL_0033:
				_entries = value;
				int num = 1455903032;
				goto IL_0008;
				IL_0008:
				while (true)
				{
					switch (num ^ 0x56C75139)
					{
					default:
						return;
					case 1:
						return;
					case 0:
						break;
					case 2:
						value = new List<Entry>();
						num = 1455903034;
						continue;
					case 3:
						goto IL_0033;
					}
					break;
				}
				goto IL_0003;
				IL_0003:
				num = 1455903035;
				goto IL_0008;
			}
		}

		internal event Action ApplyCalledEvent
		{
			add
			{
				_ApplyCalledEvent = (Action)Delegate.Combine(_ApplyCalledEvent, value);
			}
			remove
			{
				_ApplyCalledEvent = (Action)Delegate.Remove(_ApplyCalledEvent, value);
			}
		}

		internal ControllerMapLayoutHelper(Player player, WoSISrKqsFTIovjAPQFjHwQKWUo startingControllerMapInfo)
		{
			if (player == null)
			{
				throw new ArgumentNullException("player");
			}
			_reInputId = ReInput.id;
			_player = player;
			_startingControllerMapInfo = startingControllerMapInfo;
			_entries = new List<Entry>();
		}

		public void Apply()
		{
			//Discarded unreachable code: IL_0048, IL_006b, IL_0281, IL_035c
			if (ReInput._id != _reInputId)
			{
				ReInput.CheckInitialized(_reInputId);
				goto IL_0019;
			}
			goto IL_0082;
			IL_0019:
			int num = -1071784538;
			goto IL_001e;
			IL_0082:
			Action applyCalledEvent = _ApplyCalledEvent;
			num = -1071784541;
			goto IL_001e;
			IL_001e:
			int count = default(int);
			Entry entry = default(Entry);
			int num3 = default(int);
			int num8 = default(int);
			ControllerMap controllerMap2 = default(ControllerMap);
			int num5 = default(int);
			ControllerMap controllerMap = default(ControllerMap);
			int[] categoryIds = default(int[]);
			KntVdDqvnlRGbUGpRExKOwLUzbk kntVdDqvnlRGbUGpRExKOwLUzbk = default(KntVdDqvnlRGbUGpRExKOwLUzbk);
			while (true)
			{
				switch (num ^ -1071784541)
				{
				case 5:
					return;
				case 3:
					break;
				case 1:
					count = _entries.Count;
					num = -1071784539;
					continue;
				case 4:
					if (!_enabled)
					{
						return;
					}
					goto case 1;
				case 0:
					if (applyCalledEvent != null)
					{
						applyCalledEvent();
						num = -1071784537;
						continue;
					}
					goto case 4;
				case 2:
					goto IL_0082;
				default:
				{
					using (TempListPool.TList<ControllerMap> tList = TempListPool.GetTList<ControllerMap>())
					{
						List<ControllerMap> list = tList.list;
						TempListPool.TList<Controller> tList2 = TempListPool.GetTList<Controller>();
						try
						{
							List<Controller> list2 = tList2.list;
							if (!list2.Contains(ReInput.controllers.Keyboard))
							{
								list2.Add(ReInput.controllers.Keyboard);
								goto IL_00d1;
							}
							goto IL_0112;
							IL_00d6:
							int num2;
							while (true)
							{
								switch (num2 ^ -1071784541)
								{
								case 0:
									break;
								case 5:
									goto IL_0112;
								case 3:
									if (entry.isValid)
									{
										int count2 = list.Count;
										num3 = count2 - 1;
										num2 = -1071784542;
										continue;
									}
									goto IL_04b6;
								case 8:
									num3--;
									num2 = -1071784542;
									continue;
								case 10:
									goto IL_0171;
								case 9:
									entry = _entries[num8];
									if (entry != null)
									{
										num2 = -1071784544;
										continue;
									}
									goto IL_04b6;
								case 2:
									if (controllerMap2.layoutId != entry.layoutId)
									{
										list.RemoveAt(num3);
										num2 = -1071784537;
										continue;
									}
									goto case 8;
								case 4:
									_player.controllers.maps.RemoveMap(controllerMap2.controllerType, controllerMap2.controllerId, controllerMap2.id);
									num2 = -1071784533;
									continue;
								case 7:
									controllerMap2 = list[num3];
									if (!entry.controllerSelector.FLhKTSEmJOeEzkXHoSGatGpGmpHM(controllerMap2.controller))
									{
										goto case 8;
									}
									goto IL_0250;
								case 6:
									num8 = 0;
									goto IL_04bc;
								default:
									{
										if (num3 >= 0)
										{
											goto case 7;
										}
										IEnumerator<Controller> enumerator = _player.controllers.Controllers.GetEnumerator();
										try
										{
											while (enumerator.MoveNext())
											{
												while (true)
												{
													IL_0331:
													Controller current = enumerator.Current;
													int num4 = -1071784542;
													while (true)
													{
														int num6;
														switch (num4 ^ -1071784541)
														{
														case 5:
															num4 = -1071784544;
															continue;
														case 2:
															num5 = 0;
															num4 = -1071784541;
															continue;
														case 1:
															break;
														case 6:
															controllerMap = _player.controllers.maps.GetMap(current, categoryIds[num5], entry.layoutId);
															num4 = -1071784540;
															continue;
														case 3:
															goto IL_0331;
														case 4:
															categoryIds = entry.categoryIds;
															num4 = -1071784543;
															continue;
														default:
															if (controllerMap != null)
															{
																goto IL_0417;
															}
															if (_loadFromUserDataStore && kntVdDqvnlRGbUGpRExKOwLUzbk != null)
															{
																try
																{
																	controllerMap = kntVdDqvnlRGbUGpRExKOwLUzbk.pGAOikFEvaCPsMOoHAnmbLuGxjHU(_player.id, current.type, current.hardwareTypeGuid, current.hardwareIdentifier, categoryIds[num5], entry.layoutId);
																}
																catch (Exception exception)
																{
																	ReInput.HandleExternalInterfaceException(typeof(ControllerMapLayoutHelper).Name, exception);
																}
																if (controllerMap != null)
																{
																	_player.controllers.maps.AddMap(current, controllerMap);
																	goto IL_03ed;
																}
															}
															goto IL_043d;
														case 0:
															goto IL_0424;
															IL_03ed:
															num6 = -1071784543;
															goto IL_03f2;
															IL_0417:
															num5++;
															num6 = -1071784542;
															goto IL_03f2;
															IL_03f2:
															while (true)
															{
																switch (num6 ^ -1071784541)
																{
																case 3:
																	break;
																case 0:
																	goto IL_0417;
																case 1:
																	goto IL_0424;
																case 2:
																	num6 = -1071784541;
																	continue;
																case 5:
																	goto IL_043d;
																default:
																	goto IL_0477;
																}
																break;
															}
															goto IL_03ed;
															IL_043d:
															_player.controllers.maps.LoadMap(current.type, current.id, categoryIds[num5], entry.layoutId, startEnabled: true);
															num6 = -1071784541;
															goto IL_03f2;
															IL_0424:
															if (num5 < categoryIds.Length)
															{
																goto case 6;
															}
															num6 = -1071784537;
															goto IL_03f2;
														}
														if (!entry.controllerSelector.FLhKTSEmJOeEzkXHoSGatGpGmpHM(current))
														{
															break;
														}
														num4 = -1071784537;
													}
													break;
												}
												IL_0477:;
											}
										}
										finally
										{
											if (enumerator != null)
											{
												while (true)
												{
													int num7 = -1071784542;
													while (true)
													{
														switch (num7 ^ -1071784541)
														{
														default:
															goto end_IL_0485;
														case 2:
															goto end_IL_0485;
														case 0:
															break;
														case 1:
															goto IL_04a7;
														}
														break;
														IL_04a7:
														enumerator.Dispose();
														num7 = -1071784543;
													}
												}
											}
											end_IL_0485:;
										}
										goto IL_04b6;
									}
									IL_04b6:
									num8++;
									goto IL_04bc;
									IL_04bc:
									if (num8 >= count)
									{
										return;
									}
									goto case 9;
								}
								break;
								IL_0250:
								int num9;
								if (ArrayTools.Contains(entry.categoryIds, controllerMap2.categoryId))
								{
									num2 = -1071784543;
									num9 = num2;
								}
								else
								{
									num2 = -1071784533;
									num9 = num2;
								}
							}
							goto IL_00d1;
							IL_0112:
							if (!list2.Contains(ReInput.controllers.Mouse))
							{
								list2.Add(ReInput.controllers.Mouse);
								num2 = -1071784535;
								goto IL_00d6;
							}
							goto IL_0171;
							IL_00d1:
							num2 = -1071784538;
							goto IL_00d6;
							IL_0171:
							_player.controllers.maps.GetAllMaps(list);
							list2.AddRange(_player.controllers.Controllers);
							kntVdDqvnlRGbUGpRExKOwLUzbk = (ReInput.userDataStore as KntVdDqvnlRGbUGpRExKOwLUzbk);
							num2 = -1071784539;
							goto IL_00d6;
						}
						finally
						{
							if (tList2 != null)
							{
								while (true)
								{
									int num10 = -1071784543;
									while (true)
									{
										switch (num10 ^ -1071784541)
										{
										default:
											goto end_IL_04c6;
										case 1:
											goto end_IL_04c6;
										case 0:
											break;
										case 2:
											goto IL_04e8;
										}
										break;
										IL_04e8:
										((IDisposable)tList2).Dispose();
										num10 = -1071784542;
									}
								}
							}
							end_IL_04c6:;
						}
					}
				}
				}
				break;
			}
			goto IL_0019;
		}

		public void LoadDefaults()
		{
			if (ReInput._id != _reInputId)
			{
				ReInput.CheckInitialized(_reInputId);
				return;
			}
			while (true)
			{
				_entries = new List<Entry>();
				LoadDefaults(ControllerType.Joystick, _startingControllerMapInfo.shCgZSFkHfpoEimFwwpCmtRZrrES, _entries);
				int num = 881694099;
				while (true)
				{
					switch (num ^ 0x348D9593)
					{
					default:
						return;
					case 1:
						return;
					case 4:
						num = 881694102;
						continue;
					case 3:
						Apply();
						num = 881694098;
						continue;
					case 0:
						LoadDefaults(ControllerType.Keyboard, _startingControllerMapInfo.XDOZVALrmnDdYDiRFjJYbwcFbWNR, _entries);
						LoadDefaults(ControllerType.Mouse, _startingControllerMapInfo.JOKNoTSEMGjlwRhcUZaelzvMATe, _entries);
						num = 881694097;
						continue;
					case 5:
						break;
					case 2:
						LoadDefaults(ControllerType.Custom, _startingControllerMapInfo.FGHDrZiTCpAywbDTmQqefpmaQjmR, _entries);
						num = 881694096;
						continue;
					}
					break;
				}
			}
		}

		public string ToXmlString()
		{
			if (ReInput._id != _reInputId)
			{
				ReInput.CheckInitialized(_reInputId);
				return string.Empty;
			}
			try
			{
				return Export().ToXmlString(writeDocumentTag: true);
			}
			catch (Exception ex)
			{
				Logger.LogWarning("Error writing " + GetType().Name + " to XML. " + ex.Message);
				return string.Empty;
			}
		}

		public string ToJsonString()
		{
			if (ReInput._id != _reInputId)
			{
				ReInput.CheckInitialized(_reInputId);
				return string.Empty;
			}
			try
			{
				return Export().ToJsonString();
			}
			catch (Exception ex)
			{
				Logger.LogWarning("Error writing " + GetType().Name + " to JSON. " + ex.Message);
				return string.Empty;
			}
		}

		public bool ImportXml(string xmlString)
		{
			if (ReInput._id != _reInputId)
			{
				ReInput.CheckInitialized(_reInputId);
				return false;
			}
			try
			{
				Import(SerializedObject.FromXml(GetType(), xmlString));
				bool result = default(bool);
				while (true)
				{
					int num = -276130286;
					while (true)
					{
						switch (num ^ -276130285)
						{
						case 0:
							break;
						case 1:
							goto IL_004c;
						default:
							return result;
						case 2:
							return result;
						}
						break;
						IL_004c:
						Apply();
						result = true;
						num = -276130287;
					}
				}
			}
			catch (Exception ex)
			{
				Logger.LogError("Error importing " + GetType().Name + " data from XML. " + ex.Message);
				return false;
			}
		}

		public bool ImportJson(string jsonString)
		{
			if (ReInput._id != _reInputId)
			{
				ReInput.CheckInitialized(_reInputId);
				return false;
			}
			try
			{
				Import(SerializedObject.FromJson(GetType(), jsonString));
				Apply();
				return true;
			}
			catch (Exception ex)
			{
				Logger.LogError("Error importing " + GetType().Name + " data from JSON. " + ex.Message);
				return false;
			}
		}

		private SerializedObject Export()
		{
			SerializedObject serializedObject = new SerializedObject(GetType(), SerializedObject.ObjectType.Object);
			ExportDataToSerializedObject(serializedObject);
			return serializedObject;
		}

		private void ExportDataToSerializedObject(SerializedObject serializedObject)
		{
			if (serializedObject.xmlInfo == null)
			{
				goto IL_0008;
			}
			goto IL_0079;
			IL_0079:
			serializedObject.Add("dataVersion", 1, SerializedObject.FieldOptions.ExculdeFromXml);
			int num = -591343747;
			goto IL_000d;
			IL_000d:
			while (true)
			{
				switch (num ^ -591343748)
				{
				default:
					return;
				case 7:
					return;
				case 2:
					break;
				case 1:
					serializedObject.xmlInfo.attributes.Add(new SerializedObject.XmlInfo.XmlStringAttribute
					{
						localName = "dataVersion",
						value = 1.ToString()
					});
					num = -591343745;
					continue;
				case 0:
					goto IL_0079;
				case 4:
					serializedObject.xmlInfo = new SerializedObject.XmlInfo();
					num = -591343748;
					continue;
				case 5:
					serializedObject.xmlInfo.attributes.Add(new SerializedObject.XmlInfo.XmlStringAttribute
					{
						prefix = "xsi",
						localName = "schemaLocation",
						ns = null,
						value = string.Format("{0} {1}{2}{3}{4}{5}", "http://guavaman.com/rewired", "http://guavaman.com/schemas/rewired/", "1.0", "/", GetType().Name, ".xsd")
					});
					num = -591343756;
					continue;
				case 6:
					serializedObject.Add("loadFromUserDataStore", _loadFromUserDataStore);
					serializedObject.Add("entries", _entries);
					num = -591343749;
					continue;
				case 8:
					serializedObject.Add("enabled", _enabled);
					num = -591343750;
					continue;
				case 3:
					serializedObject.xmlInfo.attributes.Add(new SerializedObject.XmlInfo.XmlStringAttribute
					{
						prefix = "xmlns",
						localName = "xsi",
						ns = null,
						value = "http://www.w3.org/2001/XMLSchema-instance"
					});
					num = -591343751;
					continue;
				}
				break;
			}
			goto IL_0008;
			IL_0008:
			num = -591343752;
			goto IL_000d;
		}

		private bool Import(SerializedObject serializedObject)
		{
			_enabled = false;
			List<Entry> value = default(List<Entry>);
			while (true)
			{
				int num = -49940394;
				while (true)
				{
					switch (num ^ -49940393)
					{
					case 0:
						break;
					case 1:
						goto IL_0025;
					default:
						entries = value;
						return true;
					}
					break;
					IL_0025:
					_entries = new List<Entry>();
					serializedObject.TryGetDeserializedValueByRef("enabled", ref _enabled);
					serializedObject.TryGetDeserializedValueByRef("loadFromUserDataStore", ref _loadFromUserDataStore);
					value = new List<Entry>();
					serializedObject.TryGetDeserializedValueByRef("entries", ref value);
					num = -49940395;
				}
			}
		}

		private static void LoadDefaults(ControllerType controllerType, GGibmocDPlUsrFgyupaNdcDlXcXi[] mapInfo, List<Entry> entries)
		{
			if (mapInfo == null)
			{
				return;
			}
			int num3 = default(int);
			bool flag = default(bool);
			GGibmocDPlUsrFgyupaNdcDlXcXi gGibmocDPlUsrFgyupaNdcDlXcXi = default(GGibmocDPlUsrFgyupaNdcDlXcXi);
			while (true)
			{
				int num = 0;
				int num2 = 1500760572;
				while (true)
				{
					switch (num2 ^ 0x5973C9FC)
					{
					case 10:
						num2 = 1500760568;
						continue;
					case 8:
						num3++;
						num2 = 1500760573;
						continue;
					case 3:
						num++;
						num2 = 1500760572;
						continue;
					case 5:
					{
						int num5;
						if (flag)
						{
							num2 = 1500760575;
							num5 = num2;
						}
						else
						{
							num2 = 1500760565;
							num5 = num2;
						}
						continue;
					}
					case 6:
						if (num != num3)
						{
							GGibmocDPlUsrFgyupaNdcDlXcXi gGibmocDPlUsrFgyupaNdcDlXcXi2 = mapInfo[num3];
							if (gGibmocDPlUsrFgyupaNdcDlXcXi.ktfjKKxKmOGSnHhHDjmYrGAHoHz == gGibmocDPlUsrFgyupaNdcDlXcXi2.ktfjKKxKmOGSnHhHDjmYrGAHoHz && gGibmocDPlUsrFgyupaNdcDlXcXi.jSVECUANwcgzIdTjgjLAcuUrKbOY != gGibmocDPlUsrFgyupaNdcDlXcXi2.jSVECUANwcgzIdTjgjLAcuUrKbOY)
							{
								flag = true;
								num2 = 1500760569;
								continue;
							}
						}
						goto case 8;
					case 2:
						num2 = 1500760573;
						continue;
					case 7:
						gGibmocDPlUsrFgyupaNdcDlXcXi = mapInfo[num];
						flag = false;
						num3 = 0;
						num2 = 1500760574;
						continue;
					case 4:
						break;
					case 1:
					{
						int num4;
						if (num3 < mapInfo.Length)
						{
							num2 = 1500760570;
							num4 = num2;
						}
						else
						{
							num2 = 1500760569;
							num4 = num2;
						}
						continue;
					}
					case 9:
						entries.Add(new Entry
						{
							controllerSelector = ZVOPkHmpobXEknQJZShcFQBpbsRa.MSHfTDGPfNGJRomSPLgDvMZNgLIc(controllerType),
							categoryIds = new int[1]
							{
								gGibmocDPlUsrFgyupaNdcDlXcXi.ktfjKKxKmOGSnHhHDjmYrGAHoHz
							},
							layoutId = gGibmocDPlUsrFgyupaNdcDlXcXi.jSVECUANwcgzIdTjgjLAcuUrKbOY
						});
						num2 = 1500760575;
						continue;
					default:
						if (num >= mapInfo.Length)
						{
							return;
						}
						goto case 7;
					}
					break;
				}
			}
		}
	}
}
