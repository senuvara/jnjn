using System;

namespace Rewired
{
	public sealed class ControllerAssignmentChangedEventArgs : EventArgs
	{
		private bool eJrGlLYDGkirsdDIseycUDNYZlX;

		private int jNwSqiUbJJSyWZCKNvJDjxeYjBF;

		private int aNAUCDXmNrGbeiAKTkerzFPUzNsk;

		private ControllerType eBGLAIjxzCbUtbnMABFkvwtVwkt;

		public bool state => eJrGlLYDGkirsdDIseycUDNYZlX;

		public Controller controller
		{
			get
			{
				if (!ReInput.isReady)
				{
					return null;
				}
				return ReInput.controllers.GetController(eBGLAIjxzCbUtbnMABFkvwtVwkt, aNAUCDXmNrGbeiAKTkerzFPUzNsk);
			}
		}

		public Player player
		{
			get
			{
				if (!ReInput.isReady)
				{
					return null;
				}
				return ReInput.players.GetPlayer(jNwSqiUbJJSyWZCKNvJDjxeYjBF);
			}
		}

		internal ControllerAssignmentChangedEventArgs(int playerId, int controllerId, ControllerType controllerType, bool state)
		{
			eJrGlLYDGkirsdDIseycUDNYZlX = state;
			jNwSqiUbJJSyWZCKNvJDjxeYjBF = playerId;
			aNAUCDXmNrGbeiAKTkerzFPUzNsk = controllerId;
			eBGLAIjxzCbUtbnMABFkvwtVwkt = controllerType;
		}
	}
}
