using Rewired.Config;
using Rewired.Utils;
using System;
using System.Collections.Generic;

namespace Rewired
{
	[CustomClassObfuscation(renamePubIntMembers = false, renamePrivateMembers = true)]
	[CustomObfuscation(rename = false)]
	internal class UpdateLoopDataSet<T> where T : class
	{
		private class RrakZzGtSepTKmJhUwrVrTGdDUJ
		{
			public readonly UpdateLoopType jpeUZwJmLgwzUifJmJkdXGhujfP;

			public T ZGiUrWkOAHjlBUOOwMnaqzOKqcl;

			public RrakZzGtSepTKmJhUwrVrTGdDUJ(UpdateLoopType updateLoop)
			{
				jpeUZwJmLgwzUifJmJkdXGhujfP = updateLoop;
			}
		}

		private const int FeQRrnmUfzPyUombsNeAqwPAnym = 0;

		private RrakZzGtSepTKmJhUwrVrTGdDUJ TvNkjIYDwuchwDxshHgkepOFTCU;

		private int vYdbXfPZDHYvvQxlEbdcxrzhQod;

		private readonly int[] LbqOLdrINjKcandCMjwnGDugWHeg;

		private readonly RrakZzGtSepTKmJhUwrVrTGdDUJ[] xEGmDLgcojZqxGMRUjsUrLXHySx;

		private UpdateLoopType qGufOZBkLbDYxccgLpfPhBubnQHw = (UpdateLoopType)(-1);

		public T Current => TvNkjIYDwuchwDxshHgkepOFTCU.ZGiUrWkOAHjlBUOOwMnaqzOKqcl;

		public int Count => vYdbXfPZDHYvvQxlEbdcxrzhQod;

		public T this[int index]
		{
			get
			{
				//Discarded unreachable code: IL_0031
				if (index < 0)
				{
					goto IL_002b;
				}
				if (index >= vYdbXfPZDHYvvQxlEbdcxrzhQod)
				{
					while (true)
					{
						switch (-1077974643 ^ -1077974644)
						{
						case 2:
							break;
						case 1:
							goto IL_002b;
						default:
							goto IL_0038;
						}
					}
				}
				goto IL_0038;
				IL_0038:
				return xEGmDLgcojZqxGMRUjsUrLXHySx[index].ZGiUrWkOAHjlBUOOwMnaqzOKqcl;
				IL_002b:
				throw new IndexOutOfRangeException();
			}
			set
			{
				//Discarded unreachable code: IL_0035
				if (index < 0)
				{
					goto IL_002f;
				}
				if (index >= vYdbXfPZDHYvvQxlEbdcxrzhQod)
				{
					goto IL_000d;
				}
				goto IL_003c;
				IL_002f:
				throw new IndexOutOfRangeException();
				IL_003c:
				xEGmDLgcojZqxGMRUjsUrLXHySx[index].ZGiUrWkOAHjlBUOOwMnaqzOKqcl = value;
				int num = -537060108;
				goto IL_0012;
				IL_0012:
				switch (num ^ -537060107)
				{
				default:
					return;
				case 1:
					return;
				case 0:
					break;
				case 3:
					goto IL_002f;
				case 2:
					goto IL_003c;
				}
				goto IL_000d;
				IL_000d:
				num = -537060106;
				goto IL_0012;
			}
		}

		public UpdateLoopDataSet(UpdateLoopSetting updateLoops)
			: this(updateLoops, (Func<T>)null)
		{
		}

		public UpdateLoopDataSet(UpdateLoopSetting updateLoops, Func<T> instantiatorDelegate)
		{
			LbqOLdrINjKcandCMjwnGDugWHeg = new int[3];
			List<RrakZzGtSepTKmJhUwrVrTGdDUJ> list = new List<RrakZzGtSepTKmJhUwrVrTGdDUJ>();
			int num = 0;
			using (TempListPool.TList<UpdateLoopType> tList = TempListPool.GetTList<UpdateLoopType>(3))
			{
				List<UpdateLoopType> list2 = tList.list;
				EnumConverter.ToUpdateLoopTypes(updateLoops, list2);
				for (int i = 0; i < list2.Count; i++)
				{
					RrakZzGtSepTKmJhUwrVrTGdDUJ rrakZzGtSepTKmJhUwrVrTGdDUJ = new RrakZzGtSepTKmJhUwrVrTGdDUJ(list2[i]);
					if (instantiatorDelegate != null)
					{
						T val = rrakZzGtSepTKmJhUwrVrTGdDUJ.ZGiUrWkOAHjlBUOOwMnaqzOKqcl = instantiatorDelegate();
					}
					list.Add(rrakZzGtSepTKmJhUwrVrTGdDUJ);
					LbqOLdrINjKcandCMjwnGDugWHeg[(int)list2[i]] = num;
					num++;
				}
			}
			xEGmDLgcojZqxGMRUjsUrLXHySx = list.ToArray();
			vYdbXfPZDHYvvQxlEbdcxrzhQod = xEGmDLgcojZqxGMRUjsUrLXHySx.Length;
			SetUpdateLoop(xEGmDLgcojZqxGMRUjsUrLXHySx[0].jpeUZwJmLgwzUifJmJkdXGhujfP);
		}

		public void SetUpdateLoop(UpdateLoopType updateLoop)
		{
			if (qGufOZBkLbDYxccgLpfPhBubnQHw != updateLoop)
			{
				qGufOZBkLbDYxccgLpfPhBubnQHw = updateLoop;
				TvNkjIYDwuchwDxshHgkepOFTCU = xEGmDLgcojZqxGMRUjsUrLXHySx[LbqOLdrINjKcandCMjwnGDugWHeg[(int)updateLoop]];
			}
		}

		public T Get(int index)
		{
			//Discarded unreachable code: IL_0031
			if (index < 0)
			{
				goto IL_002b;
			}
			if (index >= vYdbXfPZDHYvvQxlEbdcxrzhQod)
			{
				while (true)
				{
					switch (-494924272 ^ -494924270)
					{
					case 0:
						break;
					case 2:
						goto IL_002b;
					default:
						goto IL_0038;
					}
				}
			}
			goto IL_0038;
			IL_0038:
			return xEGmDLgcojZqxGMRUjsUrLXHySx[index].ZGiUrWkOAHjlBUOOwMnaqzOKqcl;
			IL_002b:
			throw new IndexOutOfRangeException();
		}

		public T Get(UpdateLoopType updateLoop)
		{
			return xEGmDLgcojZqxGMRUjsUrLXHySx[LbqOLdrINjKcandCMjwnGDugWHeg[(int)updateLoop]].ZGiUrWkOAHjlBUOOwMnaqzOKqcl;
		}

		public void Set(int index, T item)
		{
			//Discarded unreachable code: IL_005f
			if (index >= 0)
			{
				while (true)
				{
					int num = -75506690;
					while (true)
					{
						switch (num ^ -75506689)
						{
						default:
							return;
						case 3:
							return;
						case 0:
							break;
						case 1:
							goto IL_002a;
						case 4:
							xEGmDLgcojZqxGMRUjsUrLXHySx[index].ZGiUrWkOAHjlBUOOwMnaqzOKqcl = item;
							num = -75506692;
							continue;
						case 2:
							goto IL_0059;
						}
						break;
						IL_002a:
						int num2;
						if (index < vYdbXfPZDHYvvQxlEbdcxrzhQod)
						{
							num = -75506693;
							num2 = num;
						}
						else
						{
							num = -75506691;
							num2 = num;
						}
					}
				}
			}
			goto IL_0059;
			IL_0059:
			throw new IndexOutOfRangeException();
		}
	}
}
