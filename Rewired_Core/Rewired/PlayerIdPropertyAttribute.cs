using System;
using UnityEngine;

namespace Rewired
{
	public class PlayerIdPropertyAttribute : PropertyAttribute
	{
		private Type xgVZxqzhkfLujyUcmEYcqMemNFx;

		public Type Type => xgVZxqzhkfLujyUcmEYcqMemNFx;

		public PlayerIdPropertyAttribute(Type type)
		{
			xgVZxqzhkfLujyUcmEYcqMemNFx = type;
		}
	}
}
