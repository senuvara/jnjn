namespace Rewired
{
	public struct InputActionSourceData
	{
		private Controller SzlUIkTCuzzKtIpFottQRnFTfsRF;

		private ControllerMap WhohQcVMSnuQPnwWDkClJXFjItS;

		private ActionElementMap RwlFGpdlTsUwasWIqciDziSwPmFi;

		public Controller controller => SzlUIkTCuzzKtIpFottQRnFTfsRF;

		public ControllerType controllerType => SzlUIkTCuzzKtIpFottQRnFTfsRF.type;

		public ControllerMap controllerMap => WhohQcVMSnuQPnwWDkClJXFjItS;

		public ActionElementMap actionElementMap => RwlFGpdlTsUwasWIqciDziSwPmFi;

		public string elementIdentifierName => RwlFGpdlTsUwasWIqciDziSwPmFi.jvTFjOjQXNurnMOSrAmTaCaCMMDA;

		internal InputActionSourceData(Controller controller, ControllerMap controllerMap, ActionElementMap actionElementMap)
		{
			SzlUIkTCuzzKtIpFottQRnFTfsRF = controller;
			WhohQcVMSnuQPnwWDkClJXFjItS = controllerMap;
			RwlFGpdlTsUwasWIqciDziSwPmFi = actionElementMap;
		}

		internal InputActionSourceData(sjlwXhdRDoIvgRhgrnLJbmPgcWb working)
		{
			SzlUIkTCuzzKtIpFottQRnFTfsRF = working.sFMEmBxJKWwCKnvCFurkGpgfDZbi;
			WhohQcVMSnuQPnwWDkClJXFjItS = working.CyjcQbohzBVvFphlpeRXPWhKLsu;
			RwlFGpdlTsUwasWIqciDziSwPmFi = working.kFKDDbkEgWiNLMrvbhvFqdHpHND;
		}
	}
}
