namespace Rewired
{
	[CustomObfuscation(rename = false)]
	[CustomClassObfuscation(renamePubIntMembers = false, renamePrivateMembers = true)]
	internal struct RewiredVersion
	{
		public int version1;

		public int version2;

		public int version3;

		public int version4;

		public string unityVersion;

		public RewiredVersion(int version1, int version2, int version3, int version4, string unityVersion)
		{
			this.version1 = version1;
			this.version2 = version2;
			this.version3 = version3;
			this.version4 = version4;
			this.unityVersion = unityVersion;
		}

		public RewiredVersion(string versionString)
		{
			if (!string.IsNullOrEmpty(versionString))
			{
				string[] array = versionString.Split('.');
				if (array.Length >= 4 && int.TryParse(array[0], out version1) && int.TryParse(array[1], out version2) && int.TryParse(array[2], out version3) && int.TryParse(array[3], out version4))
				{
					if (array.Length > 4)
					{
						unityVersion = array[4];
					}
					else
					{
						unityVersion = string.Empty;
					}
					return;
				}
			}
			version1 = 0;
			version2 = 0;
			version3 = 0;
			version4 = 0;
			unityVersion = string.Empty;
		}

		public override bool Equals(object obj)
		{
			if (object.ReferenceEquals(obj, null))
			{
				return false;
			}
			if (!(obj is RewiredVersion))
			{
				goto IL_0013;
			}
			RewiredVersion b = (RewiredVersion)obj;
			int num = -1586221273;
			goto IL_0018;
			IL_0013:
			num = -1586221276;
			goto IL_0018;
			IL_0018:
			switch (num ^ -1586221275)
			{
			case 0:
				break;
			case 1:
				return false;
			default:
				return this == b;
			}
			goto IL_0013;
		}

		public override int GetHashCode()
		{
			int num = 17;
			while (true)
			{
				int num2 = -747417061;
				while (true)
				{
					switch (num2 ^ -747417062)
					{
					case 0:
						break;
					case 1:
						goto IL_0021;
					default:
						return num;
					}
					break;
					IL_0021:
					num = num * 29 + version1.GetHashCode();
					num = num * 29 + version2.GetHashCode();
					num = num * 29 + version3.GetHashCode();
					num = num * 29 + version4.GetHashCode();
					num = num * 29 + unityVersion.GetHashCode();
					num2 = -747417064;
				}
			}
		}

		public override string ToString()
		{
			object[] array = new object[7];
			string text = default(string);
			while (true)
			{
				int num = -555765061;
				while (true)
				{
					switch (num ^ -555765062)
					{
					case 4:
						break;
					case 2:
						array[3] = ".";
						array[4] = version3;
						num = -555765060;
						continue;
					case 5:
					{
						int num2;
						if (!string.IsNullOrEmpty(unityVersion))
						{
							num = -555765070;
							num2 = num;
						}
						else
						{
							num = -555765062;
							num2 = num;
						}
						continue;
					}
					case 8:
						text = text + "." + unityVersion;
						num = -555765062;
						continue;
					case 1:
						array[0] = version1;
						num = -555765063;
						continue;
					case 3:
						array[1] = ".";
						array[2] = version2;
						num = -555765064;
						continue;
					case 7:
						text = string.Concat(array);
						num = -555765057;
						continue;
					case 6:
						array[5] = ".";
						array[6] = version4;
						num = -555765059;
						continue;
					default:
						return text;
					}
					break;
				}
			}
		}

		public static bool operator ==(RewiredVersion a, RewiredVersion b)
		{
			if (object.ReferenceEquals(a, b))
			{
				return true;
			}
			if (a.version1 == b.version1 && a.version2 == b.version2 && a.version3 == b.version3 && a.version4 == b.version4)
			{
				return string.Equals(a.unityVersion, b.unityVersion);
			}
			return false;
		}

		public static bool operator !=(RewiredVersion a, RewiredVersion b)
		{
			return !(a == b);
		}

		public static bool operator >(RewiredVersion a, RewiredVersion b)
		{
			if (a == b)
			{
				return false;
			}
			if (a.version1 > b.version1)
			{
				goto IL_001b;
			}
			if (a.version1 < b.version1)
			{
				return false;
			}
			if (a.version2 > b.version2)
			{
				return true;
			}
			if (a.version2 < b.version2)
			{
				return false;
			}
			int num;
			if (a.version3 > b.version3)
			{
				num = -755805359;
			}
			else if (a.version3 >= b.version3)
			{
				if (a.version4 > b.version4)
				{
					return true;
				}
				if (a.version4 >= b.version4)
				{
					return false;
				}
				num = -755805354;
			}
			else
			{
				num = -755805355;
			}
			goto IL_0020;
			IL_0020:
			switch (num ^ -755805355)
			{
			case 2:
				break;
			case 1:
				return true;
			case 0:
				return false;
			case 4:
				return true;
			default:
				return false;
			}
			goto IL_001b;
			IL_001b:
			num = -755805356;
			goto IL_0020;
		}

		public static bool operator <(RewiredVersion a, RewiredVersion b)
		{
			if (a == b)
			{
				goto IL_0009;
			}
			if (a.version1 < b.version1)
			{
				return true;
			}
			if (a.version1 > b.version1)
			{
				return false;
			}
			if (a.version2 < b.version2)
			{
				return true;
			}
			if (a.version2 > b.version2)
			{
				return false;
			}
			int num;
			if (a.version3 < b.version3)
			{
				num = -1914617782;
			}
			else
			{
				if (a.version3 <= b.version3)
				{
					if (a.version4 < b.version4)
					{
						return true;
					}
					if (a.version4 > b.version4)
					{
						return false;
					}
					return false;
				}
				num = -1914617781;
			}
			goto IL_000e;
			IL_000e:
			switch (num ^ -1914617783)
			{
			case 0:
				break;
			case 1:
				return false;
			case 3:
				return true;
			default:
				return false;
			}
			goto IL_0009;
			IL_0009:
			num = -1914617784;
			goto IL_000e;
		}
	}
}
