using Rewired.Config;
using System;
using System.Collections.Generic;
using System.Collections.ObjectModel;
using System.ComponentModel;

namespace Rewired
{
	[CustomClassObfuscation(renamePrivateMembers = true, renamePubIntMembers = false)]
	[CustomObfuscation(rename = false)]
	[EditorBrowsable(EditorBrowsableState.Never)]
	public static class Consts
	{
		public const int systemPlayerId = 9999999;

		public const string menuRoot = "Window/Rewired";

		internal const int programVersion1 = 1;

		internal const int programVersion2 = 1;

		internal const int programVersion3 = 24;

		internal const int programVersion4 = 0;

		internal const int dataVersion = 1;

		internal const int unityMajorVersion = 2017;

		internal const string unityMajorVersionIdentifier = "U2017";

		internal const bool isTrial = false;

		internal const string copyrightYear = "2018";

		internal const string defaultNamespace = "Rewired";

		internal const LogLevelFlags defaultLogLevel = LogLevelFlags.Info | LogLevelFlags.Warning | LogLevelFlags.Error;

		internal const string hwDefinitionVariantTag_RawInputDirectInput_xboxOneController_splitTriggers = "[SplitTriggers]";

		internal const string hwDefinitionVariantTag_RawInputDirectInput_xboxOneController_combinedTriggers = "[CombinedTriggers]";

		internal const float editorGUIUpdateInterval = 0.5f;

		internal const float joystickRefreshPollCheckTimeout = 1f;

		internal const float controllerRefreshWaitTimeout = 0.5f;

		internal const int buttonsPerHat = 8;

		internal const int keyboardKeyCount = 132;

		internal const int keyboardModifierKeyCount = 8;

		internal const int unityMouseButtonCount = 7;

		internal const int unityMouseAxisCount = 3;

		internal const int unityMaxJoysticks = 11;

		internal const int unityJoystickButtonCount = 20;

		internal const int unityJoystickStartingButtonKeycodeValue = 350;

		internal const int unityJoystickAxisCount = 29;

		internal const int unityJoystickLastJoystickIdWithButtonKeyCodes = 8;

		internal const string unityJoystickPrefix = "Joy";

		internal const string unityJoystickAxisSuffix = "Axis";

		internal const string unityJoystickButtonSuffix = "Button";

		internal const int directInputMaxButtons = 128;

		internal const int directInputMaxAxes = 32;

		internal const int directInputMaxHats = 4;

		internal const int directInputMaxSliders = 2;

		internal const int directInputMaxAxisValue = 65535;

		internal const int directInputMinAxisValue = -65535;

		internal const int directInputMaxHatValue = 36000;

		internal const int directInputHatZeroValue = -1;

		internal const int directInputHatSpan = 4500;

		internal const int directInputHatSpan4Way = 9000;

		internal const int directInput_hatValue_up = 0;

		internal const int directInput_hatValue_right = 9000;

		internal const int directInput_hatValue_down = 18000;

		internal const int directInput_hatValue_left = 27000;

		internal const int directInputLastDirectionValue = 31500;

		internal const int directInputLastDirectionValue4Way = 27000;

		internal const int directInputUnknownJoystickHatCount = 2;

		internal const int directInputUnknownJoystickHatButtonStartIndex = 128;

		internal const int directInputJoystickStateByteSize = 264;

		internal const int rawInputMaxButtons = 256;

		internal const int rawInputMaxAxes = 56;

		internal const int rawInputMaxHats = 4;

		internal const int rawInputMaxSliders = 2;

		internal const int rawInputMaxAxisValue = 65535;

		internal const int rawInputMinAxisValue = -65535;

		internal const int rawInputMaxHatValue = 36000;

		internal const int rawInputHatZeroValue = -1;

		internal const int rawInputHatSpan = 4500;

		internal const int rawInputHatSpan4Way = 9000;

		internal const int rawInput_hatValue_up = 0;

		internal const int rawInput_hatValue_right = 9000;

		internal const int rawInput_hatValue_down = 18000;

		internal const int rawInput_hatValue_left = 27000;

		internal const int rawInputLastDirectionValue = 31500;

		internal const int rawInputLastDirectionValue4Way = 27000;

		internal const int rawInputUnknownJoystickHatCount = 2;

		internal const int rawInputUnknownJoystickHatButtonStartIndex = 128;

		internal const int rawInputUnifiedMouseButtonCount = 5;

		internal const int rawInputUnifiedMouseAxisCount = 3;

		internal const float rawInputUnifiedMouseAxisUnityEquivalencyMultiplier = 0.5f;

		internal const int rawInputUnifiedKeyboardButtonCount = 132;

		internal const int osxMaxSticks = 4;

		internal const int osxMaxButtons = 128;

		internal const int osxMaxAxesPerStick = 14;

		internal const int osxMaxHatsPerStick = 4;

		internal const int osxMaxAxisValue = 65536;

		internal const int osxMinAxisValue = -65536;

		internal const int osxMaxPressureSensitiveButtonValue = 65536;

		internal const int osxMinPressureSensitiveButtonValue = 0;

		internal const int osxMaxHatValue = 36000;

		internal const int osxInputHatZeroValue = -1;

		internal const int osxHatSpan = 4500;

		internal const int osxHatSpan4Way = 9000;

		internal const int osx_hatValue_up = 0;

		internal const int osx_hatValue_right = 9000;

		internal const int osx_hatValue_down = 18000;

		internal const int osx_hatValue_left = 27000;

		internal const int osxLastDirectionValue = 31500;

		internal const int osxLastDirectionValue4Way = 27000;

		internal const int osxUnknownJoystickHatCount = 16;

		internal const int osxUnknownJoystickHatButtonStartIndex = 128;

		internal const int linuxMaxButtons = 256;

		internal const int linuxMaxAxes = 56;

		internal const int linuxMaxHats = 4;

		internal const int linuxMaxSliders = 2;

		internal const int linuxMaxAxisValue = 32767;

		internal const int linuxMinAxisValue = -32768;

		internal const int linuxMaxHatValue = 36000;

		internal const int linuxHatZeroValue = -1;

		internal const int linuxHatSpan = 4500;

		internal const int linuxHatSpan4Way = 9000;

		internal const int linux_hatValue_up = 0;

		internal const int linux_hatValue_right = 9000;

		internal const int linux_hatValue_down = 18000;

		internal const int linux_hatValue_left = 27000;

		internal const int linuxLastDirectionValue = 31500;

		internal const int linuxLastDirectionValue4Way = 27000;

		internal const int linuxUnknownJoystickHatCount = 2;

		internal const int linuxUnknownJoystickHatButtonStartIndex = 128;

		internal const int linuxUnifiedMouseButtonCount = 5;

		internal const int linuxUnifiedMouseAxisCount = 3;

		internal const float linuxUnifiedMouseAxisUnityEquivalencyMultiplier = 0.5f;

		internal const int sdl2MaxButtons = 256;

		internal const int sdl2MaxAxes = 56;

		internal const int sdl2MaxHats = 4;

		internal const int sdl2MaxSliders = 2;

		internal const int sdl2MaxAxisValue = 32768;

		internal const int sdl2MinAxisValue = -32767;

		internal const int sdl2AxisZeroValue = 0;

		internal const int sdl2MaxHatValue = 36000;

		internal const int sdl2HatZeroValue = -1;

		internal const int sdl2HatSpan = 4500;

		internal const int sdl2HatSpan4Way = 9000;

		internal const int sdl2_hatValue_up = 0;

		internal const int sdl2_hatValue_right = 9000;

		internal const int sdl2_hatValue_down = 18000;

		internal const int sdl2_hatValue_left = 27000;

		internal const int sdl2LastDirectionValue = 31500;

		internal const int sdl2LastDirectionValue4Way = 27000;

		internal const int sdl2UnknownJoystickHatCount = 2;

		internal const int sdl2UnknownJoystickHatButtonStartIndex = 128;

		internal const int sdl2UnifiedMouseButtonCount = 5;

		internal const int sdl2UnifiedMouseAxisCount = 3;

		internal const float sdl2UnifiedMouseAxisUnityEquivalencyMultiplier = 0.5f;

		internal const int windowsUWPMaxButtons = 256;

		internal const int windowsUWPMaxAxes = 56;

		internal const int windowsUWPMaxHats = 4;

		internal const int windowsUWPMaxSliders = 2;

		internal const int windowsUWPMaxAxisValue = 32767;

		internal const int windowsUWPMinAxisValue = -32768;

		internal const int windowsUWPMaxHatValue = 36000;

		internal const int windowsUWPHatZeroValue = -1;

		internal const int windowsUWPDirectionsPerHat = 8;

		internal const int windowsUWPHatSpan = 4500;

		internal const int windowsUWPHatSpan4Way = 9000;

		internal const int windowsUWPLastDirectionValue = 31500;

		internal const int windowsUWPLastDirectionValue4Way = 27000;

		internal const int windowsUWPUnknownJoystickHatCount = 2;

		internal const int windowsUWPUnknownJoystickHatButtonStartIndex = 128;

		internal const int windowsUWPUnifiedMouseButtonCount = 5;

		internal const int windowsUWPUnifiedMouseAxisCount = 3;

		internal const float windowsUWPUnifiedMouseAxisUnityEquivalencyMultiplier = 0.5f;

		internal const int xInputMaxVibration = 65535;

		internal const int xInputMinVibration = 0;

		internal const float xInputAllowedVibrationInterval = 0.01f;

		internal const int customPlatformMaxButtons = 256;

		internal const int customPlatformMaxAxes = 128;

		internal const int internalDriverMaxButtons = 256;

		internal const int internalDriverMaxAxes = 56;

		internal const int internalDriverMaxHats = 4;

		internal const int internalDriverMaxSliders = 2;

		internal const int internalDriverMaxAxisValue = 65535;

		internal const int internalDriverMinAxisValue = -65535;

		internal const int internalDriverMaxHatValue = 36000;

		internal const int internalDriverHatZeroValue = -1;

		internal const int internalDriverHatSpan = 4500;

		internal const int internalDriverHatSpan4Way = 9000;

		internal const int internalDriver_hatValue_up = 0;

		internal const int internalDriver_hatValue_right = 9000;

		internal const int internalDriver_hatValue_down = 18000;

		internal const int internalDriver_hatValue_left = 27000;

		internal const int internalDriverLastDirectionValue = 31500;

		internal const int internalDriverLastDirectionValue4Way = 27000;

		internal const int internalDriverUnknownJoystickHatCount = 2;

		internal const int internalDriverUnknownJoystickHatButtonStartIndex = 128;

		internal const int internalDriverUnifiedMouseButtonCount = 5;

		internal const int internalDriverUnifiedMouseAxisCount = 3;

		internal const float internalDriverUnifiedMouseAxisUnityEquivalencyMultiplier = 0.5f;

		internal const int webGLMaxButtons = 256;

		internal const int webGLMaxAxes = 128;

		internal const float axisPollingDeadzone = 0.7f;

		internal const float mouseXYAxisPollingDeadzone = 100f;

		internal const float mouseOtherAxisPollingDeadzone = 2f;

		internal const float defaultButtonDeadZone = 0.5f;

		internal const float hardwareButtonDeadZone = 0.01f;

		internal const float axisDefaultSensitivity = 1f;

		internal const AxisSensitivityType axisDefaultSensitivityType = AxisSensitivityType.Multiplier;

		internal const float defaultButtonDoublePressSpeed = 0.3f;

		internal const float minDoubleButtonPressSpeed = 0.01f;

		internal const float maxDoubleButtonPressSpeed = 10f;

		internal const float defaultButtonShortPressTime = 0.25f;

		internal const float minButtonShortPressTime = 0f;

		internal const float maxButtonShortPressTime = float.MaxValue;

		internal const float defaultButtonShortPressExpiresIn = 0f;

		internal const float minButtonShortPressExpiresIn = 0f;

		internal const float maxButtonShortPressExpiresIn = float.MaxValue;

		internal const float defaultButtonLongPressTime = 1f;

		internal const float minButtonLongPressTime = 0f;

		internal const float maxButtonLongPressTime = float.MaxValue;

		internal const float defaultButtonLongPressExpiresIn = 0f;

		internal const float minButtonLongPressExpiresIn = 0f;

		internal const float maxButtonLongPressExpiresIn = float.MaxValue;

		internal const float defaultButtonRepeatDelay = 0f;

		internal const float defaultButtonRepeatRate = 30f;

		internal const float minButtonRepeatRate = 0.001f;

		internal const float mouseAxisPollingTimerLength = 1f;

		internal const float fallbackPollingTimeout = 1f;

		internal const string unknownJoystickName = "Unknown Controller";

		internal const float xInputControllerVibrationRenewalInterval = 1.5f;

		internal const int defaultInputThreadUpdateRateFPS = 240;

		internal const int maxInputThreadUpdateRateFPS = 2000;

		internal const int osxXInputOutputReportRefreshRateFPS = 60;

		internal const int defaultOutputRefreshRateFPS = 100;

		internal const int hidOutputReportRefreshRateFPS = 100;

		internal const int hidOutputReportThreadKillTimeout = 10000;

		internal const int joystickInputReportRingBufferCapacity = 25;

		internal const string resourecesDLLPath_windowsStandalone = "Libs/Rewired_Windows";

		internal const string resourecesDLLPath_osxStandalone = "Libs/Rewired_OSX";

		internal const string resourecesDLLPath_linux = "Libs/Rewired_Linux";

		internal const float defaultInputBehaviorAxisSensitivity = 1f;

		internal const float defaultInputBehaviorAxisSimulation_gravity = 3f;

		internal const float defaultInputBehaviorAxisSimulation_sensitivity = 3f;

		internal const bool defaultInputBehaviorAxisSmoothing_snap = true;

		internal const bool defaultInputBehaviorAxisSmoothing_instantReverse = false;

		internal const bool defaultInputBehaviorAxisSimulation_enabled = true;

		internal const int allFlagsIntEnum = -1;

		internal const int lowLevelEventBuffers_buttonEventBufferSize = 16;

		internal const string schemaNameSpace = "http://guavaman.com/rewired";

		internal const string schemaBaseLocation = "http://guavaman.com/schemas/rewired/";

		internal const string schemaVersionControllerMap = "1.1";

		internal const string schemaVersionCalibrationMap = "1.3";

		internal const string schemaVersionInputBehavior = "1.4";

		internal const string schemaVersionControllerTemplateMap = "1.0";

		internal const string schemaVersionPlayerEnabledMapsHelperData = "1.0";

		internal const string schemaVersionPlayerControllerMapLayoutManagerData = "1.0";

		internal const int controllerMapDataVersion = 2;

		internal const int calibrationMapDataVersion = 4;

		internal const int inputBehaviorDataVersion = 5;

		internal const int controllerTemplateMapDataVersion = 1;

		internal const int playerMapEnablerDataVersion = 1;

		internal const int playerControllerMapLayoutManagerDataVersion = 1;

		internal const int controllerElementType_trueElements_minValue = 0;

		internal const int controllerElementType_trueElements_maxValue = 99;

		internal const float pressureSensitiveButtonDeadZone = 0.001f;

		internal const string rewiredEditorAssembly = "Rewired_Editor";

		internal const string rewiredEditorInputEditorClassFullName = "Rewired.Editor.InputEditor";

		internal const string nintendoSwitchPluginEditorRuntimeAssembly = "Rewired_NintendoSwitch_EditorRuntime";

		internal const string nintendoSwitchPluginInputManagerFullClassPath = "Rewired.Platforms.Switch.NintendoSwitchInputManager";

		internal const string nintendoSwitchPluginHWJoystickMapGuid_JoyConDual = "521b808c-0248-4526-bc10-f1d16ee76bf1";

		internal const string nintendoSwitchPluginHWJoystickMapGuid_Handheld = "1fbdd13b-0795-4173-8a95-a2a75de9d204";

		internal const int updateLoopTypeCount = 3;

		internal static readonly PidVid[] questionablePidVids;

		internal static readonly int[] questionableVIDs;

		internal static Guid joystickGuid_appleMFiController;

		internal static Guid joystickGuid_standardizedGamepad;

		internal static Guid joystickGuid_SonyDualShock4;

		internal static Guid hardwareTypeGuid_universalKeyboard;

		internal static Guid hardwareTypeGuid_universalMouse;

		internal static readonly IList<string> unityMouseElementNames;

		private static readonly string[] YHVvnCpSUybCaGMIcQJxFSCgHWJz;

		internal static readonly IList<string> unityMouseAxisPositiveNames;

		private static readonly string[] ABhuFsXZhERzXLWQbnLfpYtiIGx;

		internal static readonly IList<string> unityMouseAxisNegativeNames;

		private static readonly string[] fSObyfEwBsACAjIvdHDXhmQmGsjK;

		internal static readonly IList<string> rawInputUnifiedMouseElementNames;

		private static readonly string[] pTFRqsneljWyrMdJpAwvGEcIYMI;

		internal static readonly IList<string> rawInputUnifiedMouseAxisPositiveNames;

		private static readonly string[] afmeajiYoyXXDLNiPYbCICQQlsWd;

		internal static readonly IList<string> rawInputUnifiedMouseAxisNegativeNames;

		private static readonly string[] kqYGWypljZQaENBjHqNAsjVjhMG;

		internal static readonly IList<int> unityMouseElementIdentifierIds;

		private static readonly int[] FkMgMsmhKFerFOAEkSVfYcxOWg;

		internal static readonly IList<int> rawInputUnifiedMouseElementIdentifierIds;

		private static readonly int[] FDDVpoJkDxwPVyxRQbFuFvrVTKp;

		internal static readonly IList<string> mouseAxisUnityNames;

		private static readonly string[] yVKNBQLsUvgtkOssmzQDJINWadt;

		internal static readonly IList<string> mouseButtonUnityNames;

		private static readonly string[] VOnZwwYsYWALJaMMiYwVtDtgtzQ;

		internal static readonly IList<string> keyboardKeyNames;

		private static readonly string[] SyNsCYreRKLJJCVlcHNdHneHnaU;

		internal static readonly IList<int> keyboardKeyValues;

		internal static readonly int[] _keyboardKeyValues;

		internal static readonly IList<string> modifierKeyShortNames;

		private static readonly string[] nIvvxFxQbGqYYArfvTfQByXfdKp;

		internal static int nintendoSwitchPlugin_minPluginVersion => 4;

		static Consts()
		{
			joystickGuid_appleMFiController = new Guid("3d919cfa-468e-49f4-bce9-f6c43f2e7e62");
			joystickGuid_standardizedGamepad = new Guid("04c23ab3-2b99-4404-a5c4-f0df7e62938f");
			joystickGuid_SonyDualShock4 = new Guid("cd9718bf-a87a-44bc-8716-60a0def28a9f");
			string[] array2 = default(string[]);
			string[] array8 = default(string[]);
			string[] array9 = default(string[]);
			string[] array = default(string[]);
			PidVid[] array5 = default(PidVid[]);
			string[] array4 = default(string[]);
			string[] array7 = default(string[]);
			string[] array6 = default(string[]);
			string[] array3 = default(string[]);
			while (true)
			{
				int num = 2132734651;
				while (true)
				{
					switch (num ^ 0x7F1EF2F1)
					{
					default:
						return;
					case 29:
						return;
					case 12:
						break;
					case 16:
						array2[119] = "Left Control";
						array2[120] = "Right Alt";
						array2[121] = "Left Alt";
						array2[122] = "Right Command";
						array2[123] = "Left Command";
						num = 2132734668;
						continue;
					case 22:
						array2[92] = "Left Arrow";
						num = 2132734669;
						continue;
					case 31:
						array2[126] = "AltGr";
						array2[127] = "Help";
						num = 2132734642;
						continue;
					case 35:
						array2[88] = "Delete";
						array2[89] = "Up Arrow";
						array2[90] = "Down Arrow";
						array2[91] = "Right Arrow";
						num = 2132734695;
						continue;
					case 2:
						ABhuFsXZhERzXLWQbnLfpYtiIGx = new string[3]
						{
							"Mouse Right",
							"Mouse Up",
							"Mouse Wheel Up"
						};
						array8 = new string[3];
						num = 2132734685;
						continue;
					case 42:
						array9[2] = "Alt";
						array9[3] = "Shift";
						array9[4] = "Cmd";
						nIvvxFxQbGqYYArfvTfQByXfdKp = array9;
						num = 2132734659;
						continue;
					case 4:
						array2[118] = "Right Control";
						num = 2132734689;
						continue;
					case 32:
						Logger.LogError("Consts.keyboardKeyCount does not match _keyboardKeyNames.Length!");
						num = 2132734710;
						continue;
					case 34:
						array9 = new string[5]
						{
							"",
							"Ctrl",
							null,
							null,
							null
						};
						num = 2132734683;
						continue;
					case 18:
						array[6] = "MouseButton6";
						VOnZwwYsYWALJaMMiYwVtDtgtzQ = array;
						array2 = new string[132];
						array2[0] = "None";
						array2[1] = "A";
						num = 2132734657;
						continue;
					case 28:
						array2[9] = "I";
						num = 2132734705;
						continue;
					case 40:
						array2[59] = "Pause";
						array2[60] = "ESC";
						array2[61] = "!";
						array2[62] = "\"";
						array2[63] = "#";
						num = 2132734698;
						continue;
					case 56:
						array2[41] = "Keypad 4";
						array2[42] = "Keypad 5";
						num = 2132734704;
						continue;
					case 37:
					{
						questionablePidVids = array5;
						questionableVIDs = new int[2]
						{
							0,
							65535
						};
						int num3;
						if (132 != SyNsCYreRKLJJCVlcHNdHneHnaU.Length)
						{
							num = 2132734673;
							num3 = num;
						}
						else
						{
							num = 2132734710;
							num3 = num;
						}
						continue;
					}
					case 53:
						array2[35] = "8";
						array2[36] = "9";
						num = 2132734714;
						continue;
					case 51:
						array2[101] = "F4";
						array2[102] = "F5";
						array2[103] = "F6";
						array2[104] = "F7";
						array2[105] = "F8";
						array2[106] = "F9";
						array2[107] = "F10";
						array2[108] = "F11";
						array2[109] = "F12";
						array2[110] = "F13";
						array2[111] = "F14";
						array2[112] = "F15";
						array2[113] = "Numlock";
						array2[114] = "Caps Lock";
						array2[115] = "Scroll Lock";
						num = 2132734644;
						continue;
					case 48:
						array2[2] = "B";
						array2[3] = "C";
						array2[4] = "D";
						array2[5] = "E";
						array2[6] = "F";
						array2[7] = "G";
						num = 2132734679;
						continue;
					case 10:
						array4[0] = "Mouse Right";
						num = 2132734687;
						continue;
					case 49:
						array2[22] = "V";
						array2[23] = "W";
						array2[24] = "X";
						array2[25] = "Y";
						array2[26] = "Z";
						array2[27] = "0";
						array2[28] = "1";
						array2[29] = "2";
						array2[30] = "3";
						array2[31] = "4";
						array2[32] = "5";
						num = 2132734677;
						continue;
					case 71:
						_keyboardKeyValues = new int[132]
						{
							0,
							97,
							98,
							99,
							100,
							101,
							102,
							103,
							104,
							105,
							106,
							107,
							108,
							109,
							110,
							111,
							112,
							113,
							114,
							115,
							116,
							117,
							118,
							119,
							120,
							121,
							122,
							48,
							49,
							50,
							51,
							52,
							53,
							54,
							55,
							56,
							57,
							256,
							257,
							258,
							259,
							260,
							261,
							262,
							263,
							264,
							265,
							266,
							267,
							268,
							269,
							270,
							271,
							272,
							32,
							8,
							9,
							12,
							13,
							19,
							27,
							33,
							34,
							35,
							36,
							38,
							39,
							40,
							41,
							42,
							43,
							44,
							45,
							46,
							47,
							58,
							59,
							60,
							61,
							62,
							63,
							64,
							91,
							92,
							93,
							94,
							95,
							96,
							127,
							273,
							274,
							275,
							276,
							277,
							278,
							279,
							280,
							281,
							282,
							283,
							284,
							285,
							286,
							287,
							288,
							289,
							290,
							291,
							292,
							293,
							294,
							295,
							296,
							300,
							301,
							302,
							303,
							304,
							305,
							306,
							307,
							308,
							309,
							310,
							311,
							312,
							313,
							315,
							316,
							317,
							318,
							319
						};
						num = 2132734675;
						continue;
					case 66:
						array4[2] = "Mouse Wheel Up";
						num = 2132734662;
						continue;
					case 65:
						array = new string[7]
						{
							"MouseButton0",
							null,
							null,
							null,
							null,
							null,
							null
						};
						num = 2132734712;
						continue;
					case 27:
						array2[64] = "$";
						array2[65] = "&";
						array2[66] = "'";
						array2[67] = "(";
						num = 2132734680;
						continue;
					case 21:
						array2[125] = "Right Windows";
						num = 2132734702;
						continue;
					case 25:
						array2[78] = "=";
						num = 2132734697;
						continue;
					case 20:
						array2[80] = "?";
						array2[81] = "@";
						array2[82] = "[";
						array2[83] = "\\";
						num = 2132734661;
						continue;
					case 19:
						hardwareTypeGuid_universalMouse = new Guid("ad60107cea394d9cb90656d39d07be95");
						array7 = new string[10]
						{
							"Mouse Horizontal",
							"Mouse Vertical",
							"Mouse Wheel",
							"Left Mouse Button",
							"Right Mouse Button",
							"Mouse Button 3",
							"Mouse Button 4",
							"Mouse Button 5",
							null,
							null
						};
						num = 2132734647;
						continue;
					case 14:
						array6 = new string[3]
						{
							"MouseAxis1",
							"MouseAxis2",
							null
						};
						num = 2132734682;
						continue;
					case 39:
						array2[51] = "Keypad +";
						array2[52] = "Keypad Enter";
						num = 2132734703;
						continue;
					case 46:
						array4[1] = "Mouse Up";
						num = 2132734643;
						continue;
					case 59:
						array3[9] = "Mouse Button 7";
						pTFRqsneljWyrMdJpAwvGEcIYMI = array3;
						array4 = new string[3];
						num = 2132734715;
						continue;
					case 45:
						rawInputUnifiedMouseAxisNegativeNames = new ReadOnlyCollection<string>(kqYGWypljZQaENBjHqNAsjVjhMG);
						num = 2132734664;
						continue;
					case 6:
						Logger.LogError("Consts.keyboardKeyCount does not match _keyboardKeyValues.Length!");
						num = 2132734700;
						continue;
					case 57:
						rawInputUnifiedMouseElementIdentifierIds = new ReadOnlyCollection<int>(FDDVpoJkDxwPVyxRQbFuFvrVTKp);
						mouseAxisUnityNames = new ReadOnlyCollection<string>(yVKNBQLsUvgtkOssmzQDJINWadt);
						num = 2132734672;
						continue;
					case 7:
					{
						int num2;
						if (132 == _keyboardKeyValues.Length)
						{
							num = 2132734700;
							num2 = num;
						}
						else
						{
							num = 2132734711;
							num2 = num;
						}
						continue;
					}
					case 73:
						array3[8] = "Mouse Button 6";
						num = 2132734666;
						continue;
					case 72:
						array2[11] = "K";
						array2[12] = "L";
						array2[13] = "M";
						num = 2132734718;
						continue;
					case 74:
						hardwareTypeGuid_universalKeyboard = new Guid("ae4830f963db4d4c90b31beb46ecaf49");
						num = 2132734690;
						continue;
					case 69:
						array2[116] = "Right Shift";
						num = 2132734670;
						continue;
					case 26:
						array2[46] = "Keypad 9";
						array2[47] = "Keypad .";
						array2[48] = "Keypad /";
						array2[49] = "Keypad *";
						num = 2132734688;
						continue;
					case 38:
						array2[8] = "H";
						num = 2132734701;
						continue;
					case 30:
						array2[53] = "Keypad =";
						array2[54] = "Space";
						array2[55] = "Backspace";
						array2[56] = "Tab";
						array2[57] = "Clear";
						array2[58] = "Return";
						num = 2132734681;
						continue;
					case 43:
						array6[2] = "MouseAxis3";
						num = 2132734667;
						continue;
					case 5:
						array3[6] = "Mouse Button 4";
						array3[7] = "Mouse Button 5";
						num = 2132734648;
						continue;
					case 15:
						array2[14] = "N";
						array2[15] = "O";
						array2[16] = "P";
						array2[17] = "Q";
						array2[18] = "R";
						array2[19] = "S";
						array2[20] = "T";
						array2[21] = "U";
						num = 2132734656;
						continue;
					case 68:
						array3[0] = "Mouse Horizontal";
						num = 2132734694;
						continue;
					case 60:
						array2[93] = "Insert";
						array2[94] = "Home";
						num = 2132734716;
						continue;
					case 17:
						array2[50] = "Keypad -";
						num = 2132734678;
						continue;
					case 36:
						array2[33] = "6";
						array2[34] = "7";
						num = 2132734660;
						continue;
					case 8:
						array2[97] = "Page Down";
						array2[98] = "F1";
						array2[99] = "F2";
						array2[100] = "F3";
						num = 2132734658;
						continue;
					case 0:
						array2[10] = "J";
						num = 2132734649;
						continue;
					case 52:
						array2[84] = "]";
						array2[85] = "^";
						num = 2132734671;
						continue;
					case 24:
						array2[79] = ">";
						num = 2132734693;
						continue;
					case 64:
						array2[45] = "Keypad 8";
						num = 2132734699;
						continue;
					case 41:
						array2[68] = ")";
						array2[69] = "*";
						array2[70] = "+";
						array2[71] = ",";
						array2[72] = "-";
						num = 2132734663;
						continue;
					case 50:
						unityMouseElementNames = new ReadOnlyCollection<string>(YHVvnCpSUybCaGMIcQJxFSCgHWJz);
						unityMouseAxisPositiveNames = new ReadOnlyCollection<string>(ABhuFsXZhERzXLWQbnLfpYtiIGx);
						unityMouseAxisNegativeNames = new ReadOnlyCollection<string>(fSObyfEwBsACAjIvdHDXhmQmGsjK);
						unityMouseElementIdentifierIds = new ReadOnlyCollection<int>(FkMgMsmhKFerFOAEkSVfYcxOWg);
						rawInputUnifiedMouseElementNames = new ReadOnlyCollection<string>(pTFRqsneljWyrMdJpAwvGEcIYMI);
						rawInputUnifiedMouseAxisPositiveNames = new ReadOnlyCollection<string>(afmeajiYoyXXDLNiPYbCICQQlsWd);
						num = 2132734684;
						continue;
					case 44:
						array8[0] = "Mouse Left";
						array8[1] = "Mouse Down";
						array8[2] = "Mouse Wheel Down";
						num = 2132734686;
						continue;
					case 63:
						array2[117] = "Left Shift";
						num = 2132734709;
						continue;
					case 61:
						array2[124] = "Left Windows";
						num = 2132734692;
						continue;
					case 67:
						array2[128] = "Print";
						array2[129] = "SysReq";
						array2[130] = "Break";
						array2[131] = "Menu";
						SyNsCYreRKLJJCVlcHNdHneHnaU = array2;
						num = 2132734646;
						continue;
					case 11:
						array2[37] = "Keypad 0";
						array2[38] = "Keypad 1";
						array2[39] = "Keypad 2";
						array2[40] = "Keypad 3";
						num = 2132734665;
						continue;
					case 47:
						fSObyfEwBsACAjIvdHDXhmQmGsjK = array8;
						array3 = new string[10];
						num = 2132734645;
						continue;
					case 70:
						array7[8] = "Mouse Button 6";
						array7[9] = "Mouse Button 7";
						YHVvnCpSUybCaGMIcQJxFSCgHWJz = array7;
						num = 2132734707;
						continue;
					case 58:
						yVKNBQLsUvgtkOssmzQDJINWadt = array6;
						num = 2132734640;
						continue;
					case 33:
						mouseButtonUnityNames = new ReadOnlyCollection<string>(VOnZwwYsYWALJaMMiYwVtDtgtzQ);
						keyboardKeyNames = new ReadOnlyCollection<string>(SyNsCYreRKLJJCVlcHNdHneHnaU);
						keyboardKeyValues = new ReadOnlyCollection<int>(_keyboardKeyValues);
						modifierKeyShortNames = new ReadOnlyCollection<string>(nIvvxFxQbGqYYArfvTfQByXfdKp);
						array5 = new PidVid[4]
						{
							new PidVid(0, 0),
							new PidVid(0, ushort.MaxValue),
							new PidVid(ushort.MaxValue, ushort.MaxValue),
							new PidVid(ushort.MaxValue, 0)
						};
						num = 2132734676;
						continue;
					case 13:
						array2[95] = "End";
						array2[96] = "Page Up";
						num = 2132734713;
						continue;
					case 55:
						afmeajiYoyXXDLNiPYbCICQQlsWd = array4;
						kqYGWypljZQaENBjHqNAsjVjhMG = new string[3]
						{
							"Mouse Left",
							"Mouse Down",
							"Mouse Wheel Down"
						};
						FkMgMsmhKFerFOAEkSVfYcxOWg = new int[10]
						{
							0,
							1,
							2,
							3,
							4,
							5,
							6,
							7,
							8,
							9
						};
						FDDVpoJkDxwPVyxRQbFuFvrVTKp = new int[10]
						{
							0,
							1,
							2,
							3,
							4,
							5,
							6,
							7,
							8,
							9
						};
						num = 2132734719;
						continue;
					case 23:
						array3[1] = "Mouse Vertical";
						array3[2] = "Mouse Wheel";
						array3[3] = "Left Mouse Button";
						array3[4] = "Right Mouse Button";
						array3[5] = "Mouse Button 3";
						num = 2132734708;
						continue;
					case 3:
						array2[74] = "/";
						array2[75] = ":";
						array2[76] = ";";
						array2[77] = "<";
						num = 2132734696;
						continue;
					case 54:
						array2[73] = ".";
						num = 2132734706;
						continue;
					case 62:
						array2[86] = "_";
						array2[87] = "Back Quote";
						num = 2132734674;
						continue;
					case 1:
						array2[43] = "Keypad 6";
						array2[44] = "Keypad 7";
						num = 2132734641;
						continue;
					case 9:
						array[1] = "MouseButton1";
						array[2] = "MouseButton2";
						array[3] = "MouseButton3";
						array[4] = "MouseButton4";
						array[5] = "MouseButton5";
						num = 2132734691;
						continue;
					}
					break;
				}
			}
		}
	}
}
