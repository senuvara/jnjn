using Rewired.Config;
using Rewired.Utils;
using System;

namespace Rewired
{
	[CustomObfuscation(rename = false)]
	[CustomClassObfuscation(renamePubIntMembers = false, renamePrivateMembers = true)]
	internal class ButtonLoopSet : UpdateLoopDataSet<ButtonLoopSet.ButtonData>
	{
		[CustomClassObfuscation(renamePubIntMembers = false, renamePrivateMembers = true)]
		[CustomObfuscation(rename = false)]
		public class ButtonData
		{
			public readonly bool[] values;

			public readonly bool[] wasTrueThisFrame;

			public readonly bool[] effectiveValue;

			public ButtonData(int count)
			{
				values = new bool[count];
				wasTrueThisFrame = new bool[count];
				effectiveValue = new bool[count];
			}

			public void SetValue(int index, bool value)
			{
				values[index] = value;
				if (value)
				{
					wasTrueThisFrame[index] = true;
					goto IL_0015;
				}
				goto IL_0033;
				IL_0015:
				int num = 1344788609;
				goto IL_001a;
				IL_0033:
				effectiveValue[index] = (value | wasTrueThisFrame[index]);
				num = 1344788608;
				goto IL_001a;
				IL_001a:
				switch (num ^ 0x5027D880)
				{
				default:
					return;
				case 0:
					return;
				case 2:
					break;
				case 1:
					goto IL_0033;
				}
				goto IL_0015;
			}

			public void ClearWasTrueThisFrame()
			{
				int num = 0;
				while (true)
				{
					int num2;
					int num3;
					if (num < values.Length)
					{
						num2 = 1429983907;
						num3 = num2;
					}
					else
					{
						num2 = 1429983905;
						num3 = num2;
					}
					while (true)
					{
						switch (num2 ^ 0x553BD2A0)
						{
						default:
							return;
						case 1:
							return;
						case 2:
							num2 = 1429983907;
							continue;
						case 3:
							wasTrueThisFrame[num] = false;
							num2 = 1429983904;
							continue;
						case 0:
							effectiveValue[num] = values[num];
							num++;
							num2 = 1429983908;
							continue;
						case 4:
							break;
						}
						break;
					}
				}
			}

			public void Clear()
			{
				Array.Clear(values, 0, values.Length);
				while (true)
				{
					int num = 478757823;
					while (true)
					{
						switch (num ^ 0x1C8943BE)
						{
						case 0:
							break;
						case 1:
							goto IL_0032;
						default:
							Array.Clear(effectiveValue, 0, effectiveValue.Length);
							return;
						}
						break;
						IL_0032:
						Array.Clear(wasTrueThisFrame, 0, wasTrueThisFrame.Length);
						num = 478757820;
					}
				}
			}

			public void Import(ButtonData source)
			{
				//Discarded unreachable code: IL_0067
				if (source == null)
				{
					goto IL_0003;
				}
				goto IL_006e;
				IL_006e:
				int num = MathTools.Min(values.Length, source.values.Length);
				int num2 = 0;
				int num3 = 868456557;
				goto IL_0008;
				IL_0008:
				while (true)
				{
					switch (num3 ^ 0x33C39869)
					{
					default:
						return;
					case 1:
						return;
					case 5:
						return;
					case 2:
						break;
					case 0:
						values[num2] = source.values[num2];
						num3 = 868456559;
						continue;
					case 6:
						wasTrueThisFrame[num2] = source.wasTrueThisFrame[num2];
						num3 = 868456558;
						continue;
					case 3:
						goto IL_006e;
					case 4:
						goto IL_0090;
					case 7:
						effectiveValue[num2] = source.effectiveValue[num2];
						num2++;
						num3 = 868456557;
						continue;
					}
					break;
					IL_0090:
					int num4;
					if (num2 >= num)
					{
						num3 = 868456552;
						num4 = num3;
					}
					else
					{
						num3 = 868456553;
						num4 = num3;
					}
				}
				goto IL_0003;
				IL_0003:
				num3 = 868456556;
				goto IL_0008;
			}
		}

		public readonly int buttonCount;

		public ButtonLoopSet(UpdateLoopSetting updateLoops, int buttonCount)
			: base(updateLoops)
		{
			int num2 = default(int);
			while (true)
			{
				int num = -750536907;
				while (true)
				{
					switch (num ^ -750536911)
					{
					case 3:
						break;
					case 4:
						this.buttonCount = buttonCount;
						num2 = 0;
						num = -750536912;
						continue;
					case 0:
						num2++;
						num = -750536912;
						continue;
					case 2:
						base[num2] = new ButtonData(buttonCount);
						num = -750536911;
						continue;
					default:
						if (num2 >= base.Count)
						{
							return;
						}
						goto case 2;
					}
					break;
				}
			}
		}

		public void SetValue(int index, bool value, float timestamp)
		{
			int count = base.Count;
			int num = 0;
			while (true)
			{
				int num2 = -1049039701;
				while (true)
				{
					switch (num2 ^ -1049039702)
					{
					default:
						return;
					case 0:
						return;
					case 4:
						break;
					case 1:
						num2 = -1049039703;
						continue;
					case 2:
						base[num].SetValue(index, value);
						num++;
						num2 = -1049039703;
						continue;
					case 3:
					{
						int num3;
						if (num >= count)
						{
							num2 = -1049039702;
							num3 = num2;
						}
						else
						{
							num2 = -1049039704;
							num3 = num2;
						}
						continue;
					}
					}
					break;
				}
			}
		}

		public void Clear()
		{
			int count = base.Count;
			int num = 0;
			while (true)
			{
				int num2 = 808880269;
				while (true)
				{
					switch (num2 ^ 0x3036888C)
					{
					case 3:
						break;
					case 1:
						num2 = 808880270;
						continue;
					case 0:
						base[num].Clear();
						num++;
						num2 = 808880270;
						continue;
					default:
						if (num >= count)
						{
							return;
						}
						goto case 0;
					}
					break;
				}
			}
		}

		public void Import(ButtonLoopSet set)
		{
			//Discarded unreachable code: IL_007a
			if (set == null)
			{
				throw new ArgumentNullException("set");
			}
			while (set.buttonCount == buttonCount)
			{
				while (true)
				{
					IL_0081:
					int num = 0;
					int num2 = 232725282;
					while (true)
					{
						switch (num2 ^ 0xDDF1B24)
						{
						default:
							return;
						case 4:
							return;
						case 7:
							num2 = 232725285;
							continue;
						case 6:
							num2 = 232725284;
							continue;
						case 2:
							base[num].Import(set[num]);
							num2 = 232725281;
							continue;
						case 1:
							break;
						case 3:
							goto IL_0081;
						case 0:
							goto IL_008a;
						case 5:
							num++;
							num2 = 232725284;
							continue;
						}
						break;
						IL_008a:
						int num3;
						if (num >= base.Count)
						{
							num2 = 232725280;
							num3 = num2;
						}
						else
						{
							num2 = 232725286;
							num3 = num2;
						}
					}
					break;
				}
			}
			throw new Exception("Cannot import from a set with a different button count.");
		}
	}
}
