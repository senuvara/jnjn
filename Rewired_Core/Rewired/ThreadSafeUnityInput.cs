using Rewired.Utils;
using System;
using UnityEngine;

namespace Rewired
{
	[CustomClassObfuscation(renamePubIntMembers = false, renamePrivateMembers = true)]
	[CustomObfuscation(rename = false)]
	internal static class ThreadSafeUnityInput
	{
		[CustomClassObfuscation(renamePubIntMembers = false, renamePrivateMembers = true)]
		[CustomObfuscation(rename = false)]
		public sealed class Keyboard
		{
			private const int qDeuxpdhXxifHYdpmQYRPeoZbuJ = 132;

			public static readonly int keyValueIndex_Escape;

			public static readonly int keyValueIndex_Menu;

			public static readonly int keyValueIndex_F2;

			public static readonly int keyValueIndex_UpArrow;

			public static readonly int keyValueIndex_RightArrow;

			public static readonly int keyValueIndex_DownArrow;

			public static readonly int keyValueIndex_LeftArrow;

			private static readonly int[] HRnapTkRiwnHVeTQvAKjDASfBesh;

			private readonly int AMjYhzejDtCaMJKOxvrVNDwhbgL;

			private readonly int[] SZKuJmBBYcOQgYaXEzlxswywsRb;

			private readonly bool[] GMferKjgJbXcGqZUIBigKcNWrkEL;

			private bool KfbFsZTwJuhYvyDZkVBtGMGpvXu;

			private int AnKNDEkGxvsZHCirJHaITFxUxOc;

			private readonly bool IEfiDZhXePJXbjurgDzvGGSKkcUE;

			public bool enabled
			{
				get
				{
					return KfbFsZTwJuhYvyDZkVBtGMGpvXu;
				}
				set
				{
					if (value == KfbFsZTwJuhYvyDZkVBtGMGpvXu)
					{
						return;
					}
					while (true)
					{
						KfbFsZTwJuhYvyDZkVBtGMGpvXu = value;
						if (!KfbFsZTwJuhYvyDZkVBtGMGpvXu)
						{
							Clear();
							int num = -1429053504;
							while (true)
							{
								switch (num ^ -1429053502)
								{
								default:
									return;
								case 2:
									return;
								case 0:
									goto IL_000a;
								case 1:
									break;
								}
								break;
								IL_000a:
								num = -1429053501;
							}
							continue;
						}
						break;
					}
				}
			}

			public bool monitoring => AnKNDEkGxvsZHCirJHaITFxUxOc > 0;

			public int keyCount => 132;

			static Keyboard()
			{
				if (!UnityTools.isAndroidPlatform)
				{
					return;
				}
				int[] keyboardKeyValues = Consts._keyboardKeyValues;
				int[] array = new int[7]
				{
					keyValueIndex_Escape = ArrayTools.IndexOf(keyboardKeyValues, 27),
					keyValueIndex_Menu = ArrayTools.IndexOf(keyboardKeyValues, 319),
					keyValueIndex_F2 = ArrayTools.IndexOf(keyboardKeyValues, 283),
					0,
					0,
					0,
					0
				};
				while (true)
				{
					int num = -839010091;
					while (true)
					{
						switch (num ^ -839010092)
						{
						default:
							return;
						case 2:
							return;
						case 0:
							break;
						case 1:
							array[3] = (keyValueIndex_UpArrow = ArrayTools.IndexOf(keyboardKeyValues, 273));
							num = -839010089;
							continue;
						case 3:
							array[4] = (keyValueIndex_RightArrow = ArrayTools.IndexOf(keyboardKeyValues, 275));
							array[5] = (keyValueIndex_DownArrow = ArrayTools.IndexOf(keyboardKeyValues, 274));
							array[6] = (keyValueIndex_LeftArrow = ArrayTools.IndexOf(keyboardKeyValues, 276));
							HRnapTkRiwnHVeTQvAKjDASfBesh = array;
							num = -839010090;
							continue;
						}
						break;
					}
				}
			}

			public Keyboard()
			{
				GMferKjgJbXcGqZUIBigKcNWrkEL = new bool[132];
				int[] keyboardKeyValues = Consts._keyboardKeyValues;
				int num = keyboardKeyValues.Length;
				for (int i = 0; i < num; i++)
				{
					if (keyboardKeyValues[i] > AMjYhzejDtCaMJKOxvrVNDwhbgL)
					{
						AMjYhzejDtCaMJKOxvrVNDwhbgL = keyboardKeyValues[i];
					}
				}
				SZKuJmBBYcOQgYaXEzlxswywsRb = new int[AMjYhzejDtCaMJKOxvrVNDwhbgL + 1];
				ArrayTools.Fill(SZKuJmBBYcOQgYaXEzlxswywsRb, -1);
				for (int j = 0; j < num; j++)
				{
					SZKuJmBBYcOQgYaXEzlxswywsRb[keyboardKeyValues[j]] = j;
				}
			}

			public void Initialize()
			{
				if (AnKNDEkGxvsZHCirJHaITFxUxOc != 0)
				{
					uPJJuaMpKlxgFoQOoRUILjeCiXsC();
				}
				JwTMnHuJLMyKSJQseSTfXpsooCr();
			}

			public void PostInitialize()
			{
				Update();
			}

			public void Update()
			{
				//Discarded unreachable code: IL_00b2
				if (AnKNDEkGxvsZHCirJHaITFxUxOc == 0)
				{
					return;
				}
				int[] keyboardKeyValues = default(int[]);
				int num2 = default(int);
				while (true)
				{
					int num;
					if (KfbFsZTwJuhYvyDZkVBtGMGpvXu)
					{
						keyboardKeyValues = Consts._keyboardKeyValues;
						num = -533711208;
						goto IL_000e;
					}
					goto IL_00f0;
					IL_000e:
					while (true)
					{
						switch (num ^ -533711207)
						{
						default:
							return;
						case 3:
							return;
						case 4:
							num = -533711201;
							continue;
						case 6:
							break;
						case 2:
							GMferKjgJbXcGqZUIBigKcNWrkEL[keyValueIndex_RightArrow] = GetKey(KeyCode.RightArrow);
							GMferKjgJbXcGqZUIBigKcNWrkEL[keyValueIndex_DownArrow] = GetKey(KeyCode.DownArrow);
							GMferKjgJbXcGqZUIBigKcNWrkEL[keyValueIndex_LeftArrow] = GetKey(KeyCode.LeftArrow);
							num = -533711206;
							continue;
						case 8:
							if (num2 >= 132)
							{
								return;
							}
							goto case 7;
						case 7:
							GMferKjgJbXcGqZUIBigKcNWrkEL[num2] = Input.GetKey((KeyCode)keyboardKeyValues[num2]);
							num = -533711207;
							continue;
						case 1:
							num2 = 0;
							num = -533711215;
							continue;
						case 0:
							num2++;
							num = -533711215;
							continue;
						case 5:
							goto IL_00f0;
						}
						break;
					}
					continue;
					IL_00f0:
					if (IEfiDZhXePJXbjurgDzvGGSKkcUE)
					{
						GMferKjgJbXcGqZUIBigKcNWrkEL[keyValueIndex_Escape] = GetKey(KeyCode.Escape);
						GMferKjgJbXcGqZUIBigKcNWrkEL[keyValueIndex_Menu] = GetKey(KeyCode.Menu);
						GMferKjgJbXcGqZUIBigKcNWrkEL[keyValueIndex_F2] = GetKey(KeyCode.F2);
						GMferKjgJbXcGqZUIBigKcNWrkEL[keyValueIndex_UpArrow] = GetKey(KeyCode.UpArrow);
						num = -533711205;
						goto IL_000e;
					}
					break;
				}
			}

			public void Monitor(bool state)
			{
				//Discarded unreachable code: IL_0047
				if (state)
				{
					AnKNDEkGxvsZHCirJHaITFxUxOc++;
					if (AnKNDEkGxvsZHCirJHaITFxUxOc == 1)
					{
						eYDDTRfNVMLqmgOujEKtfxFIqINo();
						goto IL_0020;
					}
					return;
				}
				goto IL_004e;
				IL_0079:
				int num;
				if (AnKNDEkGxvsZHCirJHaITFxUxOc == 0)
				{
					QIuiwaoYpSoSVyDipkzedNFLwnP();
					num = 1060527086;
					goto IL_0025;
				}
				return;
				IL_0020:
				num = 1060527083;
				goto IL_0025;
				IL_004e:
				AnKNDEkGxvsZHCirJHaITFxUxOc--;
				if (AnKNDEkGxvsZHCirJHaITFxUxOc < 0)
				{
					AnKNDEkGxvsZHCirJHaITFxUxOc = 0;
					tyVEFAeVXwFQEaBAAwqusMDtzIMG();
					num = 1060527080;
					goto IL_0025;
				}
				goto IL_0079;
				IL_0025:
				switch (num ^ 0x3F365BEA)
				{
				default:
					return;
				case 1:
					return;
				case 4:
					return;
				case 3:
					break;
				case 0:
					goto IL_004e;
				case 2:
					goto IL_0079;
				}
				goto IL_0020;
			}

			public bool GetKey(KeyCode keyCode)
			{
				if (AnKNDEkGxvsZHCirJHaITFxUxOc == 0)
				{
					JItPuWVnvKWOKvqypZipimhZQJl();
					return false;
				}
				if ((uint)keyCode > (uint)AMjYhzejDtCaMJKOxvrVNDwhbgL)
				{
					return false;
				}
				return GMferKjgJbXcGqZUIBigKcNWrkEL[SZKuJmBBYcOQgYaXEzlxswywsRb[(int)keyCode]];
			}

			public void GetKeyValues(bool[] values)
			{
				//Discarded unreachable code: IL_0068, IL_0070
				if (AnKNDEkGxvsZHCirJHaITFxUxOc == 0)
				{
					goto IL_0008;
				}
				goto IL_004d;
				IL_004d:
				int num;
				int num2;
				if (values == null)
				{
					num = -2139460527;
					num2 = num;
				}
				else
				{
					num = -2139460528;
					num2 = num;
				}
				goto IL_000d;
				IL_000d:
				while (true)
				{
					switch (num ^ -2139460525)
					{
					case 2:
						return;
					case 0:
						break;
					case 3:
						goto IL_0032;
					case 1:
						goto IL_004d;
					case 4:
						JItPuWVnvKWOKvqypZipimhZQJl();
						return;
					default:
						Array.Copy(GMferKjgJbXcGqZUIBigKcNWrkEL, values, 132);
						return;
					}
					break;
					IL_0032:
					int num3;
					if (values.Length >= 132)
					{
						num = -2139460522;
						num3 = num;
					}
					else
					{
						num = -2139460527;
						num3 = num;
					}
				}
				goto IL_0008;
				IL_0008:
				num = -2139460521;
				goto IL_000d;
			}

			public void Clear()
			{
				//Discarded unreachable code: IL_0044
				if (IEfiDZhXePJXbjurgDzvGGSKkcUE)
				{
					int num = 0;
					while (true)
					{
						int num2 = 1404861767;
						while (true)
						{
							switch (num2 ^ 0x53BC7D46)
							{
							case 5:
								break;
							case 1:
								num2 = 1404861764;
								continue;
							case 2:
								if (num >= 132)
								{
									return;
								}
								goto case 4;
							case 4:
								if (Array.IndexOf(HRnapTkRiwnHVeTQvAKjDASfBesh, num) < 0)
								{
									GMferKjgJbXcGqZUIBigKcNWrkEL[num] = false;
									num2 = 1404861765;
									continue;
								}
								goto case 3;
							case 3:
								num++;
								num2 = 1404861764;
								continue;
							default:
								goto IL_0074;
							}
							break;
						}
					}
				}
				goto IL_0074;
				IL_0074:
				Array.Clear(GMferKjgJbXcGqZUIBigKcNWrkEL, 0, 132);
			}

			private void uPJJuaMpKlxgFoQOoRUILjeCiXsC()
			{
				Array.Clear(GMferKjgJbXcGqZUIBigKcNWrkEL, 0, 132);
			}

			private void JwTMnHuJLMyKSJQseSTfXpsooCr()
			{
				AnKNDEkGxvsZHCirJHaITFxUxOc = 0;
				KfbFsZTwJuhYvyDZkVBtGMGpvXu = true;
			}

			private void eYDDTRfNVMLqmgOujEKtfxFIqINo()
			{
			}

			private void QIuiwaoYpSoSVyDipkzedNFLwnP()
			{
				uPJJuaMpKlxgFoQOoRUILjeCiXsC();
			}

			private void JItPuWVnvKWOKvqypZipimhZQJl()
			{
				Logger.LogWarning("You are trying to use Keyboard without incrementing the monitor count.", requiredThreadSafety: true);
			}

			private void tyVEFAeVXwFQEaBAAwqusMDtzIMG()
			{
				Logger.LogWarning("You are decrementing the Keyboard monitor count more than you are incrementing it.", requiredThreadSafety: true);
			}
		}

		[CustomClassObfuscation(renamePubIntMembers = false, renamePrivateMembers = true)]
		[CustomObfuscation(rename = false)]
		public sealed class Mouse
		{
			private const int xxmNXgjiOLZEYUBXMoJLswJnXdL = 7;

			private const int jwxdGfoBVqTysAwLacjitRGsDKf = 3;

			private readonly bool[] arCmrATnXCKXtigWnqYrXYhIBTW;

			private readonly float[] RZyARyISglJbZaMPZdtqVjAAhsH;

			private int AnKNDEkGxvsZHCirJHaITFxUxOc;

			private Vector3 OKAYppIxQefWzVUgINIQyCgxhhO;

			private bool TYynGpfLeCGyJcOFvwjnHcQSFUKo;

			public bool monitoring => AnKNDEkGxvsZHCirJHaITFxUxOc > 0;

			public Vector3 mousePosition => OKAYppIxQefWzVUgINIQyCgxhhO;

			public bool mousePresent => TYynGpfLeCGyJcOFvwjnHcQSFUKo;

			public Mouse()
			{
				while (true)
				{
					int num = -2021401567;
					while (true)
					{
						switch (num ^ -2021401568)
						{
						case 0:
							break;
						case 1:
							goto IL_0024;
						default:
							RZyARyISglJbZaMPZdtqVjAAhsH = new float[3];
							JwTMnHuJLMyKSJQseSTfXpsooCr();
							return;
						}
						break;
						IL_0024:
						arCmrATnXCKXtigWnqYrXYhIBTW = new bool[7];
						num = -2021401566;
					}
				}
			}

			public void PostInitialize()
			{
				Update();
			}

			public void Update()
			{
				if (AnKNDEkGxvsZHCirJHaITFxUxOc == 0)
				{
					return;
				}
				int num3 = default(int);
				while (true)
				{
					int num = 0;
					int num2 = -1443564838;
					while (true)
					{
						switch (num2 ^ -1443564834)
						{
						case 6:
							num2 = -1443564839;
							continue;
						case 5:
							num++;
							num2 = -1443564838;
							continue;
						case 3:
							num3 = 0;
							num2 = -1443564836;
							continue;
						case 7:
							break;
						case 0:
							RZyARyISglJbZaMPZdtqVjAAhsH[num3] = Input.GetAxisRaw(Consts.mouseAxisUnityNames[num3]);
							num3++;
							num2 = -1443564836;
							continue;
						case 1:
							arCmrATnXCKXtigWnqYrXYhIBTW[num] = Input.GetButton(Consts.mouseButtonUnityNames[num]);
							num2 = -1443564837;
							continue;
						case 4:
						{
							int num5;
							if (num < 7)
							{
								num2 = -1443564833;
								num5 = num2;
							}
							else
							{
								num2 = -1443564835;
								num5 = num2;
							}
							continue;
						}
						case 2:
						{
							int num4;
							if (num3 < 3)
							{
								num2 = -1443564834;
								num4 = num2;
							}
							else
							{
								num2 = -1443564842;
								num4 = num2;
							}
							continue;
						}
						default:
							OKAYppIxQefWzVUgINIQyCgxhhO = Input.mousePosition;
							TYynGpfLeCGyJcOFvwjnHcQSFUKo = Input.mousePresent;
							return;
						}
						break;
					}
				}
			}

			public void Monitor(bool state)
			{
				//Discarded unreachable code: IL_004b
				if (state)
				{
					AnKNDEkGxvsZHCirJHaITFxUxOc++;
					goto IL_0011;
				}
				goto IL_0052;
				IL_0011:
				int num = 725078760;
				goto IL_0016;
				IL_0052:
				AnKNDEkGxvsZHCirJHaITFxUxOc--;
				int num2;
				if (AnKNDEkGxvsZHCirJHaITFxUxOc < 0)
				{
					num = 725078763;
					num2 = num;
				}
				else
				{
					num = 725078764;
					num2 = num;
				}
				goto IL_0016;
				IL_0016:
				while (true)
				{
					switch (num ^ 0x2B37D2E9)
					{
					default:
						return;
					case 0:
						return;
					case 4:
						break;
					case 1:
						if (AnKNDEkGxvsZHCirJHaITFxUxOc == 1)
						{
							eYDDTRfNVMLqmgOujEKtfxFIqINo();
						}
						return;
					case 3:
						goto IL_0052;
					case 5:
						if (AnKNDEkGxvsZHCirJHaITFxUxOc == 0)
						{
							QIuiwaoYpSoSVyDipkzedNFLwnP();
							num = 725078761;
							continue;
						}
						return;
					case 2:
						AnKNDEkGxvsZHCirJHaITFxUxOc = 0;
						tyVEFAeVXwFQEaBAAwqusMDtzIMG();
						num = 725078764;
						continue;
					}
					break;
				}
				goto IL_0011;
			}

			public bool GetButton(int index)
			{
				if (AnKNDEkGxvsZHCirJHaITFxUxOc == 0)
				{
					VaVvXoBuiZfUyMoUdVnMFYUNezE();
					return false;
				}
				if ((uint)index >= 7u)
				{
					return false;
				}
				return arCmrATnXCKXtigWnqYrXYhIBTW[index];
			}

			public float GetAxisRaw(int index)
			{
				if (AnKNDEkGxvsZHCirJHaITFxUxOc == 0)
				{
					VaVvXoBuiZfUyMoUdVnMFYUNezE();
					return 0f;
				}
				if ((uint)index >= 3u)
				{
					return 0f;
				}
				return RZyARyISglJbZaMPZdtqVjAAhsH[index];
			}

			public void GetButtonValues(bool[] buttons)
			{
				//Discarded unreachable code: IL_004c
				if (AnKNDEkGxvsZHCirJHaITFxUxOc == 0)
				{
					VaVvXoBuiZfUyMoUdVnMFYUNezE();
					return;
				}
				while (buttons != null)
				{
					int num;
					int num2;
					if (buttons.Length >= 7)
					{
						num = 1480967689;
						num2 = num;
					}
					else
					{
						num = 1480967691;
						num2 = num;
					}
					while (true)
					{
						switch (num ^ 0x5845C60B)
						{
						case 0:
							return;
						case 3:
							goto IL_000f;
						case 1:
							break;
						default:
							Array.Copy(arCmrATnXCKXtigWnqYrXYhIBTW, buttons, 7);
							return;
						}
						break;
						IL_000f:
						num = 1480967690;
					}
				}
			}

			public void GetAxisRawValues(float[] axes)
			{
				//Discarded unreachable code: IL_004c
				if (AnKNDEkGxvsZHCirJHaITFxUxOc == 0)
				{
					VaVvXoBuiZfUyMoUdVnMFYUNezE();
					return;
				}
				while (axes != null)
				{
					int num;
					int num2;
					if (axes.Length < 3)
					{
						num = 66251198;
						num2 = num;
					}
					else
					{
						num = 66251196;
						num2 = num;
					}
					while (true)
					{
						switch (num ^ 0x3F2E9BE)
						{
						case 0:
							return;
						case 3:
							goto IL_000f;
						case 1:
							break;
						default:
							Array.Copy(RZyARyISglJbZaMPZdtqVjAAhsH, axes, 3);
							return;
						}
						break;
						IL_000f:
						num = 66251199;
					}
				}
			}

			private void uPJJuaMpKlxgFoQOoRUILjeCiXsC()
			{
				Array.Clear(arCmrATnXCKXtigWnqYrXYhIBTW, 0, 7);
				Array.Clear(RZyARyISglJbZaMPZdtqVjAAhsH, 0, 3);
			}

			private void JwTMnHuJLMyKSJQseSTfXpsooCr()
			{
				AnKNDEkGxvsZHCirJHaITFxUxOc = 0;
				OKAYppIxQefWzVUgINIQyCgxhhO = Vector3.zero;
				TYynGpfLeCGyJcOFvwjnHcQSFUKo = false;
			}

			private void eYDDTRfNVMLqmgOujEKtfxFIqINo()
			{
			}

			private void QIuiwaoYpSoSVyDipkzedNFLwnP()
			{
				uPJJuaMpKlxgFoQOoRUILjeCiXsC();
			}

			private void VaVvXoBuiZfUyMoUdVnMFYUNezE()
			{
				Logger.LogWarning("You are trying to use Mouse without incrementing the monitor count.", requiredThreadSafety: true);
			}

			private void tyVEFAeVXwFQEaBAAwqusMDtzIMG()
			{
				Logger.LogWarning("You are decrementing the Mouse monitor count more than you are incrementing it.", requiredThreadSafety: true);
			}
		}

		private static Mouse NbCcwqOyobCIwbqERKNrSbCXaLiA;

		private static Keyboard aUEMaoeVTiQiGVBfkMQduuJpfzh;

		public static Mouse mouse => NbCcwqOyobCIwbqERKNrSbCXaLiA ?? (NbCcwqOyobCIwbqERKNrSbCXaLiA = new Mouse());

		public static Keyboard keyboard => aUEMaoeVTiQiGVBfkMQduuJpfzh ?? (aUEMaoeVTiQiGVBfkMQduuJpfzh = new Keyboard());

		public static void Initialize()
		{
		}

		public static void PostInitialize()
		{
			if (aUEMaoeVTiQiGVBfkMQduuJpfzh != null)
			{
				aUEMaoeVTiQiGVBfkMQduuJpfzh.PostInitialize();
				goto IL_0011;
			}
			goto IL_002f;
			IL_0016:
			int num;
			switch (num ^ -5863790)
			{
			default:
				return;
			case 1:
				return;
			case 0:
				break;
			case 2:
				goto IL_002f;
			}
			goto IL_0011;
			IL_002f:
			if (NbCcwqOyobCIwbqERKNrSbCXaLiA != null)
			{
				NbCcwqOyobCIwbqERKNrSbCXaLiA.PostInitialize();
				num = -5863789;
				goto IL_0016;
			}
			return;
			IL_0011:
			num = -5863792;
			goto IL_0016;
		}

		public static void PostInitialize2()
		{
		}

		public static void Deinitialize()
		{
			if (aUEMaoeVTiQiGVBfkMQduuJpfzh != null)
			{
				aUEMaoeVTiQiGVBfkMQduuJpfzh = null;
				goto IL_000d;
			}
			goto IL_002b;
			IL_0012:
			int num;
			switch (num ^ 0x21975BC4)
			{
			default:
				return;
			case 2:
				return;
			case 0:
				break;
			case 1:
				goto IL_002b;
			}
			goto IL_000d;
			IL_002b:
			if (NbCcwqOyobCIwbqERKNrSbCXaLiA != null)
			{
				NbCcwqOyobCIwbqERKNrSbCXaLiA = null;
				num = 563567558;
				goto IL_0012;
			}
			return;
			IL_000d:
			num = 563567557;
			goto IL_0012;
		}

		public static void Update()
		{
			if (aUEMaoeVTiQiGVBfkMQduuJpfzh != null)
			{
				aUEMaoeVTiQiGVBfkMQduuJpfzh.enabled = ReInput.controllers.Keyboard.enabled;
				goto IL_0020;
			}
			goto IL_0053;
			IL_0025:
			int num;
			while (true)
			{
				switch (num ^ 0x528F23B5)
				{
				default:
					return;
				case 0:
					return;
				case 3:
					break;
				case 1:
					aUEMaoeVTiQiGVBfkMQduuJpfzh.Update();
					num = 1385112503;
					continue;
				case 2:
					goto IL_0053;
				}
				break;
			}
			goto IL_0020;
			IL_0053:
			if (NbCcwqOyobCIwbqERKNrSbCXaLiA != null)
			{
				NbCcwqOyobCIwbqERKNrSbCXaLiA.Update();
				num = 1385112501;
				goto IL_0025;
			}
			return;
			IL_0020:
			num = 1385112500;
			goto IL_0025;
		}
	}
}
