using Rewired.Config;
using Rewired.Data.Mapping;
using Rewired.Interfaces;
using Rewired.Utils;
using System;
using System.Collections.Generic;
using System.Collections.ObjectModel;
using System.Runtime.CompilerServices;
using UnityEngine;

namespace Rewired
{
	public abstract class Controller
	{
		public abstract class Element
		{
			internal abstract class jdwGsADNuHdRfvSkYHsqVaIlpFY
			{
				public abstract class MwOKTMHhOtnTTGQGPANMbEDimQs
				{
					public abstract void Reset();
				}

				protected readonly int ZzWxhEhpryBDZblqXtuhaKKIFQIg;

				protected readonly int[] LbqOLdrINjKcandCMjwnGDugWHeg;

				protected MwOKTMHhOtnTTGQGPANMbEDimQs[] hlGzeGDRKMBrvrOwBJiMzXuUCQs;

				public MwOKTMHhOtnTTGQGPANMbEDimQs efdaUUDCCKozygKZVzqVRtnDUPn;

				private int OTwcevfveslTXvjuGoUWgCMeibVS;

				public int QdJgtIVEJpgyGztFjxSuDSieQFY = -1;

				protected ReadOnlyCollection<MwOKTMHhOtnTTGQGPANMbEDimQs> ZBPeTmKbzoyZhDflwKnDzXTYoFxf;

				public IList<MwOKTMHhOtnTTGQGPANMbEDimQs> nYhhlACKNviVZKXqFuHHtFvfQUi => ZBPeTmKbzoyZhDflwKnDzXTYoFxf;

				public UpdateLoopType jpeUZwJmLgwzUifJmJkdXGhujfP
				{
					set
					{
						if (QdJgtIVEJpgyGztFjxSuDSieQFY == (int)value)
						{
							return;
						}
						while (true)
						{
							QdJgtIVEJpgyGztFjxSuDSieQFY = (int)value;
							int num = 1492509747;
							while (true)
							{
								switch (num ^ 0x58F5E431)
								{
								case 0:
									goto IL_000c;
								case 1:
									break;
								default:
									OTwcevfveslTXvjuGoUWgCMeibVS = LbqOLdrINjKcandCMjwnGDugWHeg[(int)value];
									efdaUUDCCKozygKZVzqVRtnDUPn = hlGzeGDRKMBrvrOwBJiMzXuUCQs[OTwcevfveslTXvjuGoUWgCMeibVS];
									return;
								}
								break;
								IL_000c:
								num = 1492509744;
							}
						}
					}
				}

				public jdwGsADNuHdRfvSkYHsqVaIlpFY(UpdateLoopSetting updateLoopSetting)
				{
					LbqOLdrINjKcandCMjwnGDugWHeg = new int[3];
					ZzWxhEhpryBDZblqXtuhaKKIFQIg = 0;
					using (TempListPool.TList<UpdateLoopType> tList = TempListPool.GetTList<UpdateLoopType>(3))
					{
						List<UpdateLoopType> list = tList.list;
						EnumConverter.ToUpdateLoopTypes(updateLoopSetting, list);
						for (int i = 0; i < list.Count; i++)
						{
							LbqOLdrINjKcandCMjwnGDugWHeg[(int)list[i]] = ZzWxhEhpryBDZblqXtuhaKKIFQIg;
							ZzWxhEhpryBDZblqXtuhaKKIFQIg++;
						}
					}
					hlGzeGDRKMBrvrOwBJiMzXuUCQs = new MwOKTMHhOtnTTGQGPANMbEDimQs[ZzWxhEhpryBDZblqXtuhaKKIFQIg];
					ZBPeTmKbzoyZhDflwKnDzXTYoFxf = new ReadOnlyCollection<MwOKTMHhOtnTTGQGPANMbEDimQs>(hlGzeGDRKMBrvrOwBJiMzXuUCQs);
				}

				public void PuKZxoEWwKokoZYGyDVvhHcowAd()
				{
					int num = 0;
					while (true)
					{
						int num2;
						int num3;
						if (num >= ZzWxhEhpryBDZblqXtuhaKKIFQIg)
						{
							num2 = 1429706793;
							num3 = num2;
						}
						else
						{
							num2 = 1429706794;
							num3 = num2;
						}
						while (true)
						{
							switch (num2 ^ 0x5537982B)
							{
							default:
								return;
							case 2:
								return;
							case 3:
								num2 = 1429706794;
								continue;
							case 1:
								hlGzeGDRKMBrvrOwBJiMzXuUCQs[num].Reset();
								num2 = 1429706795;
								continue;
							case 4:
								break;
							case 0:
								num++;
								num2 = 1429706799;
								continue;
							}
							break;
						}
					}
				}
			}

			public readonly int id;

			public readonly string name;

			public readonly ControllerElementType type;

			internal jdwGsADNuHdRfvSkYHsqVaIlpFY CFUBuHdIcOeWCJuZmjVIAcmGFYOW;

			internal int jQRWXHvcjvaNYASwRBgjdfkYLvBF;

			internal Controller SzlUIkTCuzzKtIpFottQRnFTfsRF;

			internal readonly int aYVuMCKfoBJZlPKnMohBfcrinAp;

			public ControllerElementIdentifier elementIdentifier
			{
				get
				{
					if (ReInput._id != aYVuMCKfoBJZlPKnMohBfcrinAp)
					{
						ReInput.CheckInitialized(aYVuMCKfoBJZlPKnMohBfcrinAp);
						return null;
					}
					ControllerElementIdentifier elementIdentifierById = SzlUIkTCuzzKtIpFottQRnFTfsRF.GetElementIdentifierById(id);
					if (elementIdentifierById == null)
					{
						return ControllerElementIdentifier.BlankReadOnly;
					}
					return elementIdentifierById;
				}
			}

			public bool isMemberElement
			{
				get
				{
					if (ReInput._id != aYVuMCKfoBJZlPKnMohBfcrinAp)
					{
						ReInput.CheckInitialized(aYVuMCKfoBJZlPKnMohBfcrinAp);
						return false;
					}
					return jQRWXHvcjvaNYASwRBgjdfkYLvBF > 0;
				}
			}

			internal Element(Controller controller, int elementIdentifierId, string name, ControllerElementType type)
			{
				SzlUIkTCuzzKtIpFottQRnFTfsRF = controller;
				id = elementIdentifierId;
				this.name = name;
				this.type = type;
				aYVuMCKfoBJZlPKnMohBfcrinAp = ReInput.id;
			}

			public void Reset()
			{
				if (ReInput._id != aYVuMCKfoBJZlPKnMohBfcrinAp)
				{
					ReInput.CheckInitialized(aYVuMCKfoBJZlPKnMohBfcrinAp);
					return;
				}
				while (CFUBuHdIcOeWCJuZmjVIAcmGFYOW != null)
				{
					CFUBuHdIcOeWCJuZmjVIAcmGFYOW.PuKZxoEWwKokoZYGyDVvhHcowAd();
					int num = 1874075087;
					while (true)
					{
						switch (num ^ 0x6FB41DCF)
						{
						default:
							return;
						case 0:
							return;
						case 2:
							goto IL_001a;
						case 1:
							break;
						}
						break;
						IL_001a:
						num = 1874075086;
					}
				}
			}

			internal void YVogTthihPASQiwheBqNvWwwsIH()
			{
				if (jQRWXHvcjvaNYASwRBgjdfkYLvBF > 0)
				{
					Logger.LogWarning("This element is already a member of a compound element! This is not supported. Resulting values may be unpredictable.");
				}
				jQRWXHvcjvaNYASwRBgjdfkYLvBF++;
			}

			internal void vsMaTurtKltlZvkWtzFIQirGoUG()
			{
				//Discarded unreachable code: IL_0043
				if (jQRWXHvcjvaNYASwRBgjdfkYLvBF == 0)
				{
					while (true)
					{
						int num = 469270434;
						while (true)
						{
							switch (num ^ 0x1BF87FA1)
							{
							case 2:
								return;
							case 0:
								break;
							case 3:
								Logger.LogWarning("This element is not a member of a compound element!");
								jQRWXHvcjvaNYASwRBgjdfkYLvBF = 0;
								num = 469270435;
								continue;
							default:
								goto IL_004a;
							}
							break;
						}
					}
				}
				goto IL_004a;
				IL_004a:
				jQRWXHvcjvaNYASwRBgjdfkYLvBF--;
			}
		}

		public sealed class Axis : Element
		{
			internal class qxmhrTJltRhfxvALqOERYJwFBzd : jdwGsADNuHdRfvSkYHsqVaIlpFY
			{
				public class SyXXhrsOZmuGGWBIXEDNNTYmtpZ : MwOKTMHhOtnTTGQGPANMbEDimQs
				{
					private const float PHEzzGjrjJfgOELOeVKAgcWEtNz = 0.001f;

					public float OQeeUtoXIWATcsLBMoJCoZMLWDy;

					public float URkQoUdBuudCATWkvNdYaKAScNP;

					public float OxALpouZZzaPMmchVuSKWQLihTw;

					public float dxVIzrkKVYfjZDrukVMdgOQRrcO;

					public float tYdnopgQDKUDpgjspNKXjDSptyC;

					public float EMScZBDUKHljeLVItbLFqGrsHNLz;

					public float zgqrSSdIfibnvVfxjYUuhMWyCWy;

					public float HTSWGSPNvtDzqCeqpBiOGkhPFRU;

					public float UUwSqZtBPwDQRkWprfrfiGmLUVh;

					public float WyxgDHFQXGvwgkyUIXYybMbFlDhC;

					public float hJbrxcJnesiOLDKNWaDPcNtCxcrc;

					public float zlzltygRRtYarsGOyfrRmnnwMeJ;

					public float bfgCFqKjTFYfCLEWQQSfrQGohBFs
					{
						get
						{
							if (OQeeUtoXIWATcsLBMoJCoZMLWDy == 0f)
							{
								return 0f;
							}
							return ReInput.unscaledTime - UUwSqZtBPwDQRkWprfrfiGmLUVh;
						}
					}

					public float utwpWcasPuknIBiRCISPrOZyLDM
					{
						get
						{
							if (OxALpouZZzaPMmchVuSKWQLihTw == 0f)
							{
								return 0f;
							}
							return ReInput.unscaledTime - WyxgDHFQXGvwgkyUIXYybMbFlDhC;
						}
					}

					public float zEYXysAKeqhICbehFejMbjyfyRa
					{
						get
						{
							if (OQeeUtoXIWATcsLBMoJCoZMLWDy != 0f)
							{
								return 0f;
							}
							return ReInput.unscaledTime - zgqrSSdIfibnvVfxjYUuhMWyCWy;
						}
					}

					public float XnthxHsBZgJodNFDAywLQtfsCcH
					{
						get
						{
							if (OxALpouZZzaPMmchVuSKWQLihTw != 0f)
							{
								return 0f;
							}
							return ReInput.unscaledTime - HTSWGSPNvtDzqCeqpBiOGkhPFRU;
						}
					}

					public void ZjQHKQbKvMFFsImUmzFPKNdkLNLO(bool P_0)
					{
						float unscaledTime = ReInput.unscaledTime;
						if (P_0)
						{
							if (!MathTools.Approximately(tYdnopgQDKUDpgjspNKXjDSptyC, 0f))
							{
								zgqrSSdIfibnvVfxjYUuhMWyCWy = unscaledTime;
								goto IL_0025;
							}
							goto IL_0088;
						}
						goto IL_012b;
						IL_0025:
						int num = -1893511884;
						goto IL_002a;
						IL_002a:
						while (true)
						{
							switch (num ^ -1893511880)
							{
							default:
								return;
							case 0:
								return;
							case 8:
								break;
							case 4:
								zlzltygRRtYarsGOyfrRmnnwMeJ = unscaledTime;
								num = -1893511880;
								continue;
							case 9:
								goto IL_0088;
							case 13:
								goto IL_0096;
							case 14:
								goto IL_00c2;
							case 10:
								goto IL_00ee;
							case 15:
								hJbrxcJnesiOLDKNWaDPcNtCxcrc = unscaledTime;
								num = -1893511875;
								continue;
							case 1:
								goto IL_012b;
							case 2:
								WyxgDHFQXGvwgkyUIXYybMbFlDhC = unscaledTime;
								num = -1893511882;
								continue;
							case 3:
								zgqrSSdIfibnvVfxjYUuhMWyCWy = unscaledTime;
								num = -1893511886;
								continue;
							case 7:
								UUwSqZtBPwDQRkWprfrfiGmLUVh = unscaledTime;
								num = -1893511886;
								continue;
							case 12:
								num = -1893511883;
								continue;
							case 6:
								hJbrxcJnesiOLDKNWaDPcNtCxcrc = unscaledTime;
								num = -1893511885;
								continue;
							case 5:
								num = -1893511885;
								continue;
							case 11:
								if (!MathTools.Approximately(OxALpouZZzaPMmchVuSKWQLihTw, 0f))
								{
									HTSWGSPNvtDzqCeqpBiOGkhPFRU = unscaledTime;
									num = -1893511882;
									continue;
								}
								goto case 2;
							}
							break;
							IL_00ee:
							int num2;
							if (!MathTools.IsNear(OQeeUtoXIWATcsLBMoJCoZMLWDy, URkQoUdBuudCATWkvNdYaKAScNP, 0.001f))
							{
								num = -1893511874;
								num2 = num;
							}
							else
							{
								num = -1893511885;
								num2 = num;
							}
							continue;
							IL_00c2:
							int num3;
							if (!MathTools.IsNear(OxALpouZZzaPMmchVuSKWQLihTw, dxVIzrkKVYfjZDrukVMdgOQRrcO, 0.001f))
							{
								num = -1893511876;
								num3 = num;
							}
							else
							{
								num = -1893511880;
								num3 = num;
							}
							continue;
							IL_0096:
							int num4;
							if (MathTools.IsNear(tYdnopgQDKUDpgjspNKXjDSptyC, EMScZBDUKHljeLVItbLFqGrsHNLz, 0.001f))
							{
								num = -1893511885;
								num4 = num;
							}
							else
							{
								num = -1893511881;
								num4 = num;
							}
						}
						goto IL_0025;
						IL_012b:
						int num5;
						if (MathTools.Approximately(OQeeUtoXIWATcsLBMoJCoZMLWDy, 0f))
						{
							num = -1893511873;
							num5 = num;
						}
						else
						{
							num = -1893511877;
							num5 = num;
						}
						goto IL_002a;
						IL_0088:
						UUwSqZtBPwDQRkWprfrfiGmLUVh = unscaledTime;
						num = -1893511883;
						goto IL_002a;
					}

					public void hYkNcqbiKTNIUBVwsQctfJNTaBZC(float P_0)
					{
						if (dxVIzrkKVYfjZDrukVMdgOQRrcO != OxALpouZZzaPMmchVuSKWQLihTw)
						{
							dxVIzrkKVYfjZDrukVMdgOQRrcO = OxALpouZZzaPMmchVuSKWQLihTw;
							goto IL_001a;
						}
						goto IL_003c;
						IL_001a:
						int num = 1220464517;
						goto IL_001f;
						IL_003c:
						int num2;
						if (OxALpouZZzaPMmchVuSKWQLihTw != P_0)
						{
							num = 1220464518;
							num2 = num;
						}
						else
						{
							num = 1220464519;
							num2 = num;
						}
						goto IL_001f;
						IL_001f:
						while (true)
						{
							switch (num ^ 0x48BECF84)
							{
							default:
								return;
							case 3:
								return;
							case 0:
								break;
							case 1:
								goto IL_003c;
							case 2:
								OxALpouZZzaPMmchVuSKWQLihTw = P_0;
								num = 1220464519;
								continue;
							}
							break;
						}
						goto IL_001a;
					}

					public override void Reset()
					{
						OQeeUtoXIWATcsLBMoJCoZMLWDy = 0f;
						while (true)
						{
							int num = 2086573784;
							while (true)
							{
								switch (num ^ 0x7C5E96DE)
								{
								case 5:
									break;
								case 4:
									hJbrxcJnesiOLDKNWaDPcNtCxcrc = 0f;
									num = 2086573789;
									continue;
								case 1:
									HTSWGSPNvtDzqCeqpBiOGkhPFRU = 0f;
									num = 2086573788;
									continue;
								case 0:
									dxVIzrkKVYfjZDrukVMdgOQRrcO = 0f;
									zgqrSSdIfibnvVfxjYUuhMWyCWy = 0f;
									num = 2086573791;
									continue;
								case 6:
									URkQoUdBuudCATWkvNdYaKAScNP = 0f;
									OxALpouZZzaPMmchVuSKWQLihTw = 0f;
									num = 2086573790;
									continue;
								case 2:
									UUwSqZtBPwDQRkWprfrfiGmLUVh = 0f;
									WyxgDHFQXGvwgkyUIXYybMbFlDhC = 0f;
									num = 2086573786;
									continue;
								default:
									zlzltygRRtYarsGOyfrRmnnwMeJ = 0f;
									return;
								}
								break;
							}
						}
					}
				}

				public qxmhrTJltRhfxvALqOERYJwFBzd(UpdateLoopSetting updateCycle)
					: base(updateCycle)
				{
					int num2 = default(int);
					while (true)
					{
						int num = -44498708;
						while (true)
						{
							switch (num ^ -44498705)
							{
							case 0:
								break;
							case 2:
								hlGzeGDRKMBrvrOwBJiMzXuUCQs[num2] = new SyXXhrsOZmuGGWBIXEDNNTYmtpZ();
								num2++;
								num = -44498709;
								continue;
							case 1:
								num = -44498709;
								continue;
							case 3:
								num2 = 0;
								num = -44498706;
								continue;
							default:
								if (num2 >= ZzWxhEhpryBDZblqXtuhaKKIFQIg)
								{
									efdaUUDCCKozygKZVzqVRtnDUPn = hlGzeGDRKMBrvrOwBJiMzXuUCQs[0];
									return;
								}
								goto case 2;
							}
							break;
						}
					}
				}
			}

			internal readonly AxisRange NdrbIiWbMXtMxQeYPgFoqUiSgxtG;

			internal readonly HardwareAxisInfo myhMHSXGPCmqVhJTIUxcKcGKVBd;

			public float value
			{
				get
				{
					if (ReInput._id != aYVuMCKfoBJZlPKnMohBfcrinAp)
					{
						ReInput.CheckInitialized(aYVuMCKfoBJZlPKnMohBfcrinAp);
						return 0f;
					}
					if (base.isMemberElement)
					{
						return ((qxmhrTJltRhfxvALqOERYJwFBzd.SyXXhrsOZmuGGWBIXEDNNTYmtpZ)CFUBuHdIcOeWCJuZmjVIAcmGFYOW.efdaUUDCCKozygKZVzqVRtnDUPn).tYdnopgQDKUDpgjspNKXjDSptyC;
					}
					return ((qxmhrTJltRhfxvALqOERYJwFBzd.SyXXhrsOZmuGGWBIXEDNNTYmtpZ)CFUBuHdIcOeWCJuZmjVIAcmGFYOW.efdaUUDCCKozygKZVzqVRtnDUPn).OQeeUtoXIWATcsLBMoJCoZMLWDy;
				}
			}

			public float valuePrev
			{
				get
				{
					if (ReInput._id != aYVuMCKfoBJZlPKnMohBfcrinAp)
					{
						while (true)
						{
							int num = -258059405;
							while (true)
							{
								switch (num ^ -258059407)
								{
								case 0:
									break;
								case 2:
									goto IL_002b;
								default:
									return 0f;
								}
								break;
								IL_002b:
								ReInput.CheckInitialized(aYVuMCKfoBJZlPKnMohBfcrinAp);
								num = -258059408;
							}
						}
					}
					if (base.isMemberElement)
					{
						return ((qxmhrTJltRhfxvALqOERYJwFBzd.SyXXhrsOZmuGGWBIXEDNNTYmtpZ)CFUBuHdIcOeWCJuZmjVIAcmGFYOW.efdaUUDCCKozygKZVzqVRtnDUPn).EMScZBDUKHljeLVItbLFqGrsHNLz;
					}
					return ((qxmhrTJltRhfxvALqOERYJwFBzd.SyXXhrsOZmuGGWBIXEDNNTYmtpZ)CFUBuHdIcOeWCJuZmjVIAcmGFYOW.efdaUUDCCKozygKZVzqVRtnDUPn).URkQoUdBuudCATWkvNdYaKAScNP;
				}
			}

			public float valueRaw
			{
				get
				{
					if (ReInput._id != aYVuMCKfoBJZlPKnMohBfcrinAp)
					{
						while (true)
						{
							int num = 1210100645;
							while (true)
							{
								switch (num ^ 0x4820ABA7)
								{
								case 0:
									break;
								case 2:
									goto IL_002b;
								default:
									return 0f;
								}
								break;
								IL_002b:
								ReInput.CheckInitialized(aYVuMCKfoBJZlPKnMohBfcrinAp);
								num = 1210100646;
							}
						}
					}
					return ((qxmhrTJltRhfxvALqOERYJwFBzd.SyXXhrsOZmuGGWBIXEDNNTYmtpZ)CFUBuHdIcOeWCJuZmjVIAcmGFYOW.efdaUUDCCKozygKZVzqVRtnDUPn).OxALpouZZzaPMmchVuSKWQLihTw;
				}
				internal set
				{
					((qxmhrTJltRhfxvALqOERYJwFBzd.SyXXhrsOZmuGGWBIXEDNNTYmtpZ)CFUBuHdIcOeWCJuZmjVIAcmGFYOW.efdaUUDCCKozygKZVzqVRtnDUPn).hYkNcqbiKTNIUBVwsQctfJNTaBZC(value);
				}
			}

			public float valueRawPrev
			{
				get
				{
					if (ReInput._id != aYVuMCKfoBJZlPKnMohBfcrinAp)
					{
						ReInput.CheckInitialized(aYVuMCKfoBJZlPKnMohBfcrinAp);
						return 0f;
					}
					return ((qxmhrTJltRhfxvALqOERYJwFBzd.SyXXhrsOZmuGGWBIXEDNNTYmtpZ)CFUBuHdIcOeWCJuZmjVIAcmGFYOW.efdaUUDCCKozygKZVzqVRtnDUPn).dxVIzrkKVYfjZDrukVMdgOQRrcO;
				}
			}

			public float valueDelta
			{
				get
				{
					if (ReInput._id != aYVuMCKfoBJZlPKnMohBfcrinAp)
					{
						ReInput.CheckInitialized(aYVuMCKfoBJZlPKnMohBfcrinAp);
						return 0f;
					}
					return value - valuePrev;
				}
			}

			public float valueDeltaRaw
			{
				get
				{
					if (ReInput._id != aYVuMCKfoBJZlPKnMohBfcrinAp)
					{
						ReInput.CheckInitialized(aYVuMCKfoBJZlPKnMohBfcrinAp);
						return 0f;
					}
					return ((qxmhrTJltRhfxvALqOERYJwFBzd.SyXXhrsOZmuGGWBIXEDNNTYmtpZ)CFUBuHdIcOeWCJuZmjVIAcmGFYOW.efdaUUDCCKozygKZVzqVRtnDUPn).OxALpouZZzaPMmchVuSKWQLihTw - ((qxmhrTJltRhfxvALqOERYJwFBzd.SyXXhrsOZmuGGWBIXEDNNTYmtpZ)CFUBuHdIcOeWCJuZmjVIAcmGFYOW.efdaUUDCCKozygKZVzqVRtnDUPn).dxVIzrkKVYfjZDrukVMdgOQRrcO;
				}
			}

			public float lastTimeActive
			{
				get
				{
					if (ReInput._id != aYVuMCKfoBJZlPKnMohBfcrinAp)
					{
						ReInput.CheckInitialized(aYVuMCKfoBJZlPKnMohBfcrinAp);
						return 0f;
					}
					return ((qxmhrTJltRhfxvALqOERYJwFBzd.SyXXhrsOZmuGGWBIXEDNNTYmtpZ)CFUBuHdIcOeWCJuZmjVIAcmGFYOW.efdaUUDCCKozygKZVzqVRtnDUPn).zgqrSSdIfibnvVfxjYUuhMWyCWy;
				}
			}

			public float lastTimeActiveRaw
			{
				get
				{
					if (ReInput._id != aYVuMCKfoBJZlPKnMohBfcrinAp)
					{
						ReInput.CheckInitialized(aYVuMCKfoBJZlPKnMohBfcrinAp);
						return 0f;
					}
					return ((qxmhrTJltRhfxvALqOERYJwFBzd.SyXXhrsOZmuGGWBIXEDNNTYmtpZ)CFUBuHdIcOeWCJuZmjVIAcmGFYOW.efdaUUDCCKozygKZVzqVRtnDUPn).HTSWGSPNvtDzqCeqpBiOGkhPFRU;
				}
			}

			public float lastTimeInactive
			{
				get
				{
					if (ReInput._id != aYVuMCKfoBJZlPKnMohBfcrinAp)
					{
						ReInput.CheckInitialized(aYVuMCKfoBJZlPKnMohBfcrinAp);
						return 0f;
					}
					return ((qxmhrTJltRhfxvALqOERYJwFBzd.SyXXhrsOZmuGGWBIXEDNNTYmtpZ)CFUBuHdIcOeWCJuZmjVIAcmGFYOW.efdaUUDCCKozygKZVzqVRtnDUPn).UUwSqZtBPwDQRkWprfrfiGmLUVh;
				}
			}

			public float lastTimeInactiveRaw
			{
				get
				{
					if (ReInput._id != aYVuMCKfoBJZlPKnMohBfcrinAp)
					{
						ReInput.CheckInitialized(aYVuMCKfoBJZlPKnMohBfcrinAp);
						return 0f;
					}
					return ((qxmhrTJltRhfxvALqOERYJwFBzd.SyXXhrsOZmuGGWBIXEDNNTYmtpZ)CFUBuHdIcOeWCJuZmjVIAcmGFYOW.efdaUUDCCKozygKZVzqVRtnDUPn).WyxgDHFQXGvwgkyUIXYybMbFlDhC;
				}
			}

			public float lastTimeValueChanged
			{
				get
				{
					if (ReInput._id != aYVuMCKfoBJZlPKnMohBfcrinAp)
					{
						ReInput.CheckInitialized(aYVuMCKfoBJZlPKnMohBfcrinAp);
						return 0f;
					}
					return ((qxmhrTJltRhfxvALqOERYJwFBzd.SyXXhrsOZmuGGWBIXEDNNTYmtpZ)CFUBuHdIcOeWCJuZmjVIAcmGFYOW.efdaUUDCCKozygKZVzqVRtnDUPn).hJbrxcJnesiOLDKNWaDPcNtCxcrc;
				}
			}

			public float lastTimeValueChangedRaw
			{
				get
				{
					if (ReInput._id != aYVuMCKfoBJZlPKnMohBfcrinAp)
					{
						ReInput.CheckInitialized(aYVuMCKfoBJZlPKnMohBfcrinAp);
						return 0f;
					}
					return ((qxmhrTJltRhfxvALqOERYJwFBzd.SyXXhrsOZmuGGWBIXEDNNTYmtpZ)CFUBuHdIcOeWCJuZmjVIAcmGFYOW.efdaUUDCCKozygKZVzqVRtnDUPn).zlzltygRRtYarsGOyfrRmnnwMeJ;
				}
			}

			public float timeActive
			{
				get
				{
					if (ReInput._id != aYVuMCKfoBJZlPKnMohBfcrinAp)
					{
						while (true)
						{
							int num = -1271271383;
							while (true)
							{
								switch (num ^ -1271271384)
								{
								case 0:
									break;
								case 1:
									goto IL_002b;
								default:
									return 0f;
								}
								break;
								IL_002b:
								ReInput.CheckInitialized(aYVuMCKfoBJZlPKnMohBfcrinAp);
								num = -1271271382;
							}
						}
					}
					return ((qxmhrTJltRhfxvALqOERYJwFBzd.SyXXhrsOZmuGGWBIXEDNNTYmtpZ)CFUBuHdIcOeWCJuZmjVIAcmGFYOW.efdaUUDCCKozygKZVzqVRtnDUPn).bfgCFqKjTFYfCLEWQQSfrQGohBFs;
				}
			}

			public float timeActiveRaw
			{
				get
				{
					if (ReInput._id != aYVuMCKfoBJZlPKnMohBfcrinAp)
					{
						ReInput.CheckInitialized(aYVuMCKfoBJZlPKnMohBfcrinAp);
						return 0f;
					}
					return ((qxmhrTJltRhfxvALqOERYJwFBzd.SyXXhrsOZmuGGWBIXEDNNTYmtpZ)CFUBuHdIcOeWCJuZmjVIAcmGFYOW.efdaUUDCCKozygKZVzqVRtnDUPn).bfgCFqKjTFYfCLEWQQSfrQGohBFs;
				}
			}

			public float timeInactive
			{
				get
				{
					if (ReInput._id != aYVuMCKfoBJZlPKnMohBfcrinAp)
					{
						ReInput.CheckInitialized(aYVuMCKfoBJZlPKnMohBfcrinAp);
						return 0f;
					}
					return ((qxmhrTJltRhfxvALqOERYJwFBzd.SyXXhrsOZmuGGWBIXEDNNTYmtpZ)CFUBuHdIcOeWCJuZmjVIAcmGFYOW.efdaUUDCCKozygKZVzqVRtnDUPn).zEYXysAKeqhICbehFejMbjyfyRa;
				}
			}

			public float timeInactiveRaw
			{
				get
				{
					if (ReInput._id != aYVuMCKfoBJZlPKnMohBfcrinAp)
					{
						ReInput.CheckInitialized(aYVuMCKfoBJZlPKnMohBfcrinAp);
						return 0f;
					}
					return ((qxmhrTJltRhfxvALqOERYJwFBzd.SyXXhrsOZmuGGWBIXEDNNTYmtpZ)CFUBuHdIcOeWCJuZmjVIAcmGFYOW.efdaUUDCCKozygKZVzqVRtnDUPn).XnthxHsBZgJodNFDAywLQtfsCcH;
				}
			}

			internal float selfValue => ((qxmhrTJltRhfxvALqOERYJwFBzd.SyXXhrsOZmuGGWBIXEDNNTYmtpZ)CFUBuHdIcOeWCJuZmjVIAcmGFYOW.efdaUUDCCKozygKZVzqVRtnDUPn).OQeeUtoXIWATcsLBMoJCoZMLWDy;

			internal float selfValuePrev => ((qxmhrTJltRhfxvALqOERYJwFBzd.SyXXhrsOZmuGGWBIXEDNNTYmtpZ)CFUBuHdIcOeWCJuZmjVIAcmGFYOW.efdaUUDCCKozygKZVzqVRtnDUPn).URkQoUdBuudCATWkvNdYaKAScNP;

			internal void BDQBkZIuzzRukZWrbHSCGnFIEDw(float P_0)
			{
				qxmhrTJltRhfxvALqOERYJwFBzd.SyXXhrsOZmuGGWBIXEDNNTYmtpZ syXXhrsOZmuGGWBIXEDNNTYmtpZ = (qxmhrTJltRhfxvALqOERYJwFBzd.SyXXhrsOZmuGGWBIXEDNNTYmtpZ)CFUBuHdIcOeWCJuZmjVIAcmGFYOW.efdaUUDCCKozygKZVzqVRtnDUPn;
				syXXhrsOZmuGGWBIXEDNNTYmtpZ.EMScZBDUKHljeLVItbLFqGrsHNLz = syXXhrsOZmuGGWBIXEDNNTYmtpZ.tYdnopgQDKUDpgjspNKXjDSptyC;
				syXXhrsOZmuGGWBIXEDNNTYmtpZ.tYdnopgQDKUDpgjspNKXjDSptyC = P_0;
			}

			internal Axis(Controller controller, int elementIdentifierId, string name, AxisRange axisRange, HardwareAxisInfo axisInfo)
				: base(controller, elementIdentifierId, name, ControllerElementType.Axis)
			{
				CFUBuHdIcOeWCJuZmjVIAcmGFYOW = new qxmhrTJltRhfxvALqOERYJwFBzd(ReInput.configVars.updateLoop);
				NdrbIiWbMXtMxQeYPgFoqUiSgxtG = axisRange;
				myhMHSXGPCmqVhJTIUxcKcGKVBd = axisInfo;
			}

			internal void iWJNvUyOmARoKFViasWWBCUAiFN(UpdateLoopType P_0)
			{
				if (CFUBuHdIcOeWCJuZmjVIAcmGFYOW == null || CFUBuHdIcOeWCJuZmjVIAcmGFYOW.QdJgtIVEJpgyGztFjxSuDSieQFY == (int)P_0)
				{
					return;
				}
				while (true)
				{
					int num = 959558748;
					while (true)
					{
						switch (num ^ 0x3931B45D)
						{
						default:
							return;
						case 2:
							return;
						case 0:
							break;
						case 1:
							goto IL_0034;
						}
						break;
						IL_0034:
						CFUBuHdIcOeWCJuZmjVIAcmGFYOW.jpeUZwJmLgwzUifJmJkdXGhujfP = P_0;
						num = 959558751;
					}
				}
			}

			internal void NEyRemiysvxsIxztObiJHAiRuKQ(AxisCalibration P_0)
			{
				qxmhrTJltRhfxvALqOERYJwFBzd.SyXXhrsOZmuGGWBIXEDNNTYmtpZ syXXhrsOZmuGGWBIXEDNNTYmtpZ = (qxmhrTJltRhfxvALqOERYJwFBzd.SyXXhrsOZmuGGWBIXEDNNTYmtpZ)CFUBuHdIcOeWCJuZmjVIAcmGFYOW.efdaUUDCCKozygKZVzqVRtnDUPn;
				syXXhrsOZmuGGWBIXEDNNTYmtpZ.URkQoUdBuudCATWkvNdYaKAScNP = syXXhrsOZmuGGWBIXEDNNTYmtpZ.OQeeUtoXIWATcsLBMoJCoZMLWDy;
				float num = P_0.GetCalibratedValue(syXXhrsOZmuGGWBIXEDNNTYmtpZ.OxALpouZZzaPMmchVuSKWQLihTw, NdrbIiWbMXtMxQeYPgFoqUiSgxtG);
				if (P_0.applyRangeCalibration)
				{
					num = MathTools.Clamp(num, -1f, 1f);
				}
				syXXhrsOZmuGGWBIXEDNNTYmtpZ.OQeeUtoXIWATcsLBMoJCoZMLWDy = num;
			}

			internal void NEyRemiysvxsIxztObiJHAiRuKQ()
			{
				qxmhrTJltRhfxvALqOERYJwFBzd.SyXXhrsOZmuGGWBIXEDNNTYmtpZ syXXhrsOZmuGGWBIXEDNNTYmtpZ = (qxmhrTJltRhfxvALqOERYJwFBzd.SyXXhrsOZmuGGWBIXEDNNTYmtpZ)CFUBuHdIcOeWCJuZmjVIAcmGFYOW.efdaUUDCCKozygKZVzqVRtnDUPn;
				syXXhrsOZmuGGWBIXEDNNTYmtpZ.URkQoUdBuudCATWkvNdYaKAScNP = syXXhrsOZmuGGWBIXEDNNTYmtpZ.OQeeUtoXIWATcsLBMoJCoZMLWDy;
				syXXhrsOZmuGGWBIXEDNNTYmtpZ.OQeeUtoXIWATcsLBMoJCoZMLWDy = syXXhrsOZmuGGWBIXEDNNTYmtpZ.OxALpouZZzaPMmchVuSKWQLihTw;
			}

			internal void awtxaNsWndUlszlmNROcIYsdEYv()
			{
				qxmhrTJltRhfxvALqOERYJwFBzd.SyXXhrsOZmuGGWBIXEDNNTYmtpZ syXXhrsOZmuGGWBIXEDNNTYmtpZ = (qxmhrTJltRhfxvALqOERYJwFBzd.SyXXhrsOZmuGGWBIXEDNNTYmtpZ)CFUBuHdIcOeWCJuZmjVIAcmGFYOW.efdaUUDCCKozygKZVzqVRtnDUPn;
				syXXhrsOZmuGGWBIXEDNNTYmtpZ.URkQoUdBuudCATWkvNdYaKAScNP = syXXhrsOZmuGGWBIXEDNNTYmtpZ.OQeeUtoXIWATcsLBMoJCoZMLWDy;
				syXXhrsOZmuGGWBIXEDNNTYmtpZ.OQeeUtoXIWATcsLBMoJCoZMLWDy = 0f;
			}

			internal void wrBCPcYcwFrUkzguMpAlVXkYyWy()
			{
				qxmhrTJltRhfxvALqOERYJwFBzd.SyXXhrsOZmuGGWBIXEDNNTYmtpZ syXXhrsOZmuGGWBIXEDNNTYmtpZ = (qxmhrTJltRhfxvALqOERYJwFBzd.SyXXhrsOZmuGGWBIXEDNNTYmtpZ)CFUBuHdIcOeWCJuZmjVIAcmGFYOW.efdaUUDCCKozygKZVzqVRtnDUPn;
				syXXhrsOZmuGGWBIXEDNNTYmtpZ.ZjQHKQbKvMFFsImUmzFPKNdkLNLO(base.isMemberElement);
			}

			internal void GQrgXjfZKMNIRjUtzYlbLUJRndi(float P_0)
			{
				int num = 0;
				while (num < CFUBuHdIcOeWCJuZmjVIAcmGFYOW.nYhhlACKNviVZKXqFuHHtFvfQUi.Count)
				{
					while (true)
					{
						qxmhrTJltRhfxvALqOERYJwFBzd.SyXXhrsOZmuGGWBIXEDNNTYmtpZ syXXhrsOZmuGGWBIXEDNNTYmtpZ = CFUBuHdIcOeWCJuZmjVIAcmGFYOW.nYhhlACKNviVZKXqFuHHtFvfQUi[num] as qxmhrTJltRhfxvALqOERYJwFBzd.SyXXhrsOZmuGGWBIXEDNNTYmtpZ;
						int num2;
						int num3;
						if (syXXhrsOZmuGGWBIXEDNNTYmtpZ != null)
						{
							num2 = 1560781768;
							num3 = num2;
						}
						else
						{
							num2 = 1560781774;
							num3 = num2;
						}
						while (true)
						{
							switch (num2 ^ 0x5D07A3CA)
							{
							case 0:
								num2 = 1560781769;
								continue;
							case 3:
								break;
							case 4:
								num++;
								num2 = 1560781771;
								continue;
							case 2:
								syXXhrsOZmuGGWBIXEDNNTYmtpZ.hYkNcqbiKTNIUBVwsQctfJNTaBZC(P_0);
								syXXhrsOZmuGGWBIXEDNNTYmtpZ.URkQoUdBuudCATWkvNdYaKAScNP = syXXhrsOZmuGGWBIXEDNNTYmtpZ.OQeeUtoXIWATcsLBMoJCoZMLWDy;
								syXXhrsOZmuGGWBIXEDNNTYmtpZ.OQeeUtoXIWATcsLBMoJCoZMLWDy = 0f;
								syXXhrsOZmuGGWBIXEDNNTYmtpZ.ZjQHKQbKvMFFsImUmzFPKNdkLNLO(base.isMemberElement);
								num2 = 1560781774;
								continue;
							default:
								goto IL_0097;
							}
							break;
						}
					}
					IL_0097:;
				}
			}
		}

		public sealed class Button : Element
		{
			internal class jtduYPhzqBtbUDGZSYbpSQhCZJl : jdwGsADNuHdRfvSkYHsqVaIlpFY
			{
				public class WSChknhmmSafqILkAgKIkTyxVCw : MwOKTMHhOtnTTGQGPANMbEDimQs
				{
					public bool OQeeUtoXIWATcsLBMoJCoZMLWDy;

					public bool URkQoUdBuudCATWkvNdYaKAScNP;

					public ButtonStateRecorder KhosbRezsowqdChzdHNgPKqxHjg;

					public sTLBjADxzgJPtiLvMuPMxpiFzxH FiTgLUodvKUYcfyjNElpdnUrCkj;

					public WSChknhmmSafqILkAgKIkTyxVCw()
					{
						while (true)
						{
							int num = -672668633;
							while (true)
							{
								switch (num ^ -672668635)
								{
								default:
									return;
								case 1:
									return;
								case 0:
									break;
								case 2:
									goto IL_0024;
								}
								break;
								IL_0024:
								KhosbRezsowqdChzdHNgPKqxHjg = new ButtonStateRecorder();
								FiTgLUodvKUYcfyjNElpdnUrCkj = new sTLBjADxzgJPtiLvMuPMxpiFzxH(0.3f);
								num = -672668636;
							}
						}
					}

					public void HDNHWHMIgYVNwexHqBXaTGgQOqc(bool P_0)
					{
						if (URkQoUdBuudCATWkvNdYaKAScNP != OQeeUtoXIWATcsLBMoJCoZMLWDy)
						{
							URkQoUdBuudCATWkvNdYaKAScNP = OQeeUtoXIWATcsLBMoJCoZMLWDy;
							goto IL_001a;
						}
						goto IL_003c;
						IL_001f:
						int num;
						switch (num ^ -1033670319)
						{
						default:
							return;
						case 3:
							return;
						case 0:
							break;
						case 1:
							goto IL_003c;
						case 2:
							goto IL_0053;
						}
						goto IL_001a;
						IL_003c:
						if (OQeeUtoXIWATcsLBMoJCoZMLWDy != P_0)
						{
							OQeeUtoXIWATcsLBMoJCoZMLWDy = P_0;
							num = -1033670317;
							goto IL_001f;
						}
						goto IL_0053;
						IL_001a:
						num = -1033670320;
						goto IL_001f;
						IL_0053:
						KhosbRezsowqdChzdHNgPKqxHjg.ZjQHKQbKvMFFsImUmzFPKNdkLNLO(P_0);
						num = -1033670318;
						goto IL_001f;
					}

					public override void Reset()
					{
						OQeeUtoXIWATcsLBMoJCoZMLWDy = false;
						URkQoUdBuudCATWkvNdYaKAScNP = false;
						while (true)
						{
							int num = -1902393912;
							while (true)
							{
								switch (num ^ -1902393911)
								{
								default:
									return;
								case 2:
									return;
								case 0:
									break;
								case 1:
									goto IL_002c;
								}
								break;
								IL_002c:
								KhosbRezsowqdChzdHNgPKqxHjg.PuKZxoEWwKokoZYGyDVvhHcowAd();
								FiTgLUodvKUYcfyjNElpdnUrCkj.PuKZxoEWwKokoZYGyDVvhHcowAd();
								num = -1902393909;
							}
						}
					}
				}

				public class uervCLYIpOUmfHmjMkObzfaQbXg : WSChknhmmSafqILkAgKIkTyxVCw
				{
					public float tnYCtSrWzglAcqzIsRUVClwoMMX;

					public float UIDAjtGYlGQYWORwrpfrjJcpJxjx;

					public void HDNHWHMIgYVNwexHqBXaTGgQOqc(float P_0)
					{
						if (UIDAjtGYlGQYWORwrpfrjJcpJxjx != tnYCtSrWzglAcqzIsRUVClwoMMX)
						{
							UIDAjtGYlGQYWORwrpfrjJcpJxjx = tnYCtSrWzglAcqzIsRUVClwoMMX;
							goto IL_001a;
						}
						goto IL_0038;
						IL_005e:
						HDNHWHMIgYVNwexHqBXaTGgQOqc((tnYCtSrWzglAcqzIsRUVClwoMMX > 0f) ? true : false);
						return;
						IL_0038:
						int num;
						if (tnYCtSrWzglAcqzIsRUVClwoMMX != P_0)
						{
							tnYCtSrWzglAcqzIsRUVClwoMMX = ((P_0 > 0.001f) ? P_0 : 0f);
							num = 1367029780;
							goto IL_001f;
						}
						goto IL_005e;
						IL_001f:
						switch (num ^ 0x517B3816)
						{
						case 0:
							break;
						case 1:
							goto IL_0038;
						default:
							goto IL_005e;
						}
						goto IL_001a;
						IL_001a:
						num = 1367029783;
						goto IL_001f;
					}

					public override void Reset()
					{
						base.Reset();
						tnYCtSrWzglAcqzIsRUVClwoMMX = 0f;
						UIDAjtGYlGQYWORwrpfrjJcpJxjx = 0f;
					}
				}

				public jtduYPhzqBtbUDGZSYbpSQhCZJl(UpdateLoopSetting updateCycle, bool isPressureSensitive)
					: base(updateCycle)
				{
					for (int i = 0; i < ZzWxhEhpryBDZblqXtuhaKKIFQIg; i++)
					{
						if (isPressureSensitive)
						{
							hlGzeGDRKMBrvrOwBJiMzXuUCQs[i] = new uervCLYIpOUmfHmjMkObzfaQbXg();
						}
						else
						{
							hlGzeGDRKMBrvrOwBJiMzXuUCQs[i] = new WSChknhmmSafqILkAgKIkTyxVCw();
						}
					}
					efdaUUDCCKozygKZVzqVRtnDUPn = hlGzeGDRKMBrvrOwBJiMzXuUCQs[0];
				}

				public void nuSEGDUoZDhHVHtaNWsButHNjEuK(float P_0)
				{
					int num = 0;
					while (true)
					{
						int num2 = -1530376691;
						while (true)
						{
							switch (num2 ^ -1530376690)
							{
							case 0:
								break;
							case 3:
								num2 = -1530376692;
								continue;
							case 1:
								((WSChknhmmSafqILkAgKIkTyxVCw)hlGzeGDRKMBrvrOwBJiMzXuUCQs[num]).FiTgLUodvKUYcfyjNElpdnUrCkj.weYgVIcDJczbYzHqrkpWrVAtaFE(P_0);
								num++;
								num2 = -1530376692;
								continue;
							default:
								if (num >= hlGzeGDRKMBrvrOwBJiMzXuUCQs.Length)
								{
									return;
								}
								goto case 1;
							}
							break;
						}
					}
				}

				public void VNTBNhyEpwcKDssIMNYXnAvHcvpE()
				{
					int num = 0;
					while (num < hlGzeGDRKMBrvrOwBJiMzXuUCQs.Length)
					{
						while (true)
						{
							((WSChknhmmSafqILkAgKIkTyxVCw)hlGzeGDRKMBrvrOwBJiMzXuUCQs[num]).FiTgLUodvKUYcfyjNElpdnUrCkj.weYgVIcDJczbYzHqrkpWrVAtaFE(0.3f);
							int num2 = 314439691;
							while (true)
							{
								switch (num2 ^ 0x12BDF80A)
								{
								case 0:
									num2 = 314439688;
									continue;
								case 2:
									break;
								case 1:
									num++;
									num2 = 314439689;
									continue;
								default:
									goto IL_0054;
								}
								break;
							}
						}
						IL_0054:;
					}
				}
			}

			internal readonly bool NzEtIbXhndCRmkmnMiPBfEClEtEi;

			internal readonly HardwareButtonInfo WGLowJGWkhfxfZAOoGBhavBWonj;

			public bool valuePrev
			{
				get
				{
					if (ReInput._id != aYVuMCKfoBJZlPKnMohBfcrinAp)
					{
						ReInput.CheckInitialized(aYVuMCKfoBJZlPKnMohBfcrinAp);
						return false;
					}
					return ((jtduYPhzqBtbUDGZSYbpSQhCZJl.WSChknhmmSafqILkAgKIkTyxVCw)CFUBuHdIcOeWCJuZmjVIAcmGFYOW.efdaUUDCCKozygKZVzqVRtnDUPn).URkQoUdBuudCATWkvNdYaKAScNP;
				}
			}

			public bool value
			{
				get
				{
					if (ReInput._id != aYVuMCKfoBJZlPKnMohBfcrinAp)
					{
						ReInput.CheckInitialized(aYVuMCKfoBJZlPKnMohBfcrinAp);
						return false;
					}
					return ((jtduYPhzqBtbUDGZSYbpSQhCZJl.WSChknhmmSafqILkAgKIkTyxVCw)CFUBuHdIcOeWCJuZmjVIAcmGFYOW.efdaUUDCCKozygKZVzqVRtnDUPn).OQeeUtoXIWATcsLBMoJCoZMLWDy;
				}
			}

			public float pressure
			{
				get
				{
					if (ReInput._id != aYVuMCKfoBJZlPKnMohBfcrinAp)
					{
						goto IL_000d;
					}
					int num;
					if (!NzEtIbXhndCRmkmnMiPBfEClEtEi)
					{
						num = 1148210530;
						goto IL_0012;
					}
					return ((jtduYPhzqBtbUDGZSYbpSQhCZJl.uervCLYIpOUmfHmjMkObzfaQbXg)CFUBuHdIcOeWCJuZmjVIAcmGFYOW.efdaUUDCCKozygKZVzqVRtnDUPn).tnYCtSrWzglAcqzIsRUVClwoMMX;
					IL_0012:
					switch (num ^ 0x44704D60)
					{
					case 0:
						break;
					case 1:
						ReInput.CheckInitialized(aYVuMCKfoBJZlPKnMohBfcrinAp);
						return 0f;
					default:
						if (!((jtduYPhzqBtbUDGZSYbpSQhCZJl.WSChknhmmSafqILkAgKIkTyxVCw)CFUBuHdIcOeWCJuZmjVIAcmGFYOW.efdaUUDCCKozygKZVzqVRtnDUPn).OQeeUtoXIWATcsLBMoJCoZMLWDy)
						{
							return 0f;
						}
						return 1f;
					}
					goto IL_000d;
					IL_000d:
					num = 1148210529;
					goto IL_0012;
				}
			}

			public float pressurePrev
			{
				get
				{
					if (ReInput._id != aYVuMCKfoBJZlPKnMohBfcrinAp)
					{
						ReInput.CheckInitialized(aYVuMCKfoBJZlPKnMohBfcrinAp);
						return 0f;
					}
					if (!NzEtIbXhndCRmkmnMiPBfEClEtEi)
					{
						if (!((jtduYPhzqBtbUDGZSYbpSQhCZJl.WSChknhmmSafqILkAgKIkTyxVCw)CFUBuHdIcOeWCJuZmjVIAcmGFYOW.efdaUUDCCKozygKZVzqVRtnDUPn).URkQoUdBuudCATWkvNdYaKAScNP)
						{
							return 0f;
						}
						return 1f;
					}
					return ((jtduYPhzqBtbUDGZSYbpSQhCZJl.uervCLYIpOUmfHmjMkObzfaQbXg)CFUBuHdIcOeWCJuZmjVIAcmGFYOW.efdaUUDCCKozygKZVzqVRtnDUPn).UIDAjtGYlGQYWORwrpfrjJcpJxjx;
				}
			}

			public bool isPressureSensitive
			{
				get
				{
					if (ReInput._id != aYVuMCKfoBJZlPKnMohBfcrinAp)
					{
						ReInput.CheckInitialized(aYVuMCKfoBJZlPKnMohBfcrinAp);
						return false;
					}
					return NzEtIbXhndCRmkmnMiPBfEClEtEi;
				}
			}

			public bool justPressed
			{
				get
				{
					if (ReInput._id != aYVuMCKfoBJZlPKnMohBfcrinAp)
					{
						ReInput.CheckInitialized(aYVuMCKfoBJZlPKnMohBfcrinAp);
						return false;
					}
					if (!((jtduYPhzqBtbUDGZSYbpSQhCZJl.WSChknhmmSafqILkAgKIkTyxVCw)CFUBuHdIcOeWCJuZmjVIAcmGFYOW.efdaUUDCCKozygKZVzqVRtnDUPn).URkQoUdBuudCATWkvNdYaKAScNP && ((jtduYPhzqBtbUDGZSYbpSQhCZJl.WSChknhmmSafqILkAgKIkTyxVCw)CFUBuHdIcOeWCJuZmjVIAcmGFYOW.efdaUUDCCKozygKZVzqVRtnDUPn).OQeeUtoXIWATcsLBMoJCoZMLWDy)
					{
						return true;
					}
					return false;
				}
			}

			public bool justReleased
			{
				get
				{
					if (ReInput._id != aYVuMCKfoBJZlPKnMohBfcrinAp)
					{
						ReInput.CheckInitialized(aYVuMCKfoBJZlPKnMohBfcrinAp);
						return false;
					}
					if (((jtduYPhzqBtbUDGZSYbpSQhCZJl.WSChknhmmSafqILkAgKIkTyxVCw)CFUBuHdIcOeWCJuZmjVIAcmGFYOW.efdaUUDCCKozygKZVzqVRtnDUPn).URkQoUdBuudCATWkvNdYaKAScNP && !((jtduYPhzqBtbUDGZSYbpSQhCZJl.WSChknhmmSafqILkAgKIkTyxVCw)CFUBuHdIcOeWCJuZmjVIAcmGFYOW.efdaUUDCCKozygKZVzqVRtnDUPn).OQeeUtoXIWATcsLBMoJCoZMLWDy)
					{
						return true;
					}
					return false;
				}
			}

			public bool justChangedState
			{
				get
				{
					if (ReInput._id != aYVuMCKfoBJZlPKnMohBfcrinAp)
					{
						ReInput.CheckInitialized(aYVuMCKfoBJZlPKnMohBfcrinAp);
						return false;
					}
					if (((jtduYPhzqBtbUDGZSYbpSQhCZJl.WSChknhmmSafqILkAgKIkTyxVCw)CFUBuHdIcOeWCJuZmjVIAcmGFYOW.efdaUUDCCKozygKZVzqVRtnDUPn).URkQoUdBuudCATWkvNdYaKAScNP != ((jtduYPhzqBtbUDGZSYbpSQhCZJl.WSChknhmmSafqILkAgKIkTyxVCw)CFUBuHdIcOeWCJuZmjVIAcmGFYOW.efdaUUDCCKozygKZVzqVRtnDUPn).OQeeUtoXIWATcsLBMoJCoZMLWDy)
					{
						return true;
					}
					return false;
				}
			}

			public bool doublePressedAndHeld
			{
				get
				{
					if (ReInput._id != aYVuMCKfoBJZlPKnMohBfcrinAp)
					{
						ReInput.CheckInitialized(aYVuMCKfoBJZlPKnMohBfcrinAp);
						return false;
					}
					return ((jtduYPhzqBtbUDGZSYbpSQhCZJl.WSChknhmmSafqILkAgKIkTyxVCw)CFUBuHdIcOeWCJuZmjVIAcmGFYOW.efdaUUDCCKozygKZVzqVRtnDUPn).FiTgLUodvKUYcfyjNElpdnUrCkj.ZCwaVQOIjqyCDxclpvbiKuVsMmr;
				}
			}

			public bool justDoublePressed
			{
				get
				{
					if (ReInput._id != aYVuMCKfoBJZlPKnMohBfcrinAp)
					{
						ReInput.CheckInitialized(aYVuMCKfoBJZlPKnMohBfcrinAp);
						return false;
					}
					if (!justPressed)
					{
						return false;
					}
					return ((jtduYPhzqBtbUDGZSYbpSQhCZJl.WSChknhmmSafqILkAgKIkTyxVCw)CFUBuHdIcOeWCJuZmjVIAcmGFYOW.efdaUUDCCKozygKZVzqVRtnDUPn).FiTgLUodvKUYcfyjNElpdnUrCkj.ZCwaVQOIjqyCDxclpvbiKuVsMmr;
				}
			}

			public float timePressed
			{
				get
				{
					if (ReInput._id != aYVuMCKfoBJZlPKnMohBfcrinAp)
					{
						ReInput.CheckInitialized(aYVuMCKfoBJZlPKnMohBfcrinAp);
						return 0f;
					}
					return ((jtduYPhzqBtbUDGZSYbpSQhCZJl.WSChknhmmSafqILkAgKIkTyxVCw)CFUBuHdIcOeWCJuZmjVIAcmGFYOW.efdaUUDCCKozygKZVzqVRtnDUPn).KhosbRezsowqdChzdHNgPKqxHjg.YZJPXmQPWVBFCqmfpnKzUmaDcuo;
				}
			}

			public float timeUnpressed
			{
				get
				{
					if (ReInput._id != aYVuMCKfoBJZlPKnMohBfcrinAp)
					{
						ReInput.CheckInitialized(aYVuMCKfoBJZlPKnMohBfcrinAp);
						return 0f;
					}
					return ((jtduYPhzqBtbUDGZSYbpSQhCZJl.WSChknhmmSafqILkAgKIkTyxVCw)CFUBuHdIcOeWCJuZmjVIAcmGFYOW.efdaUUDCCKozygKZVzqVRtnDUPn).KhosbRezsowqdChzdHNgPKqxHjg.FBjxMdqiIcxSeLgpnWhZLyCNyZZ;
				}
			}

			public float lastTimePressed
			{
				get
				{
					if (ReInput._id != aYVuMCKfoBJZlPKnMohBfcrinAp)
					{
						ReInput.CheckInitialized(aYVuMCKfoBJZlPKnMohBfcrinAp);
						return 0f;
					}
					return ((jtduYPhzqBtbUDGZSYbpSQhCZJl.WSChknhmmSafqILkAgKIkTyxVCw)CFUBuHdIcOeWCJuZmjVIAcmGFYOW.efdaUUDCCKozygKZVzqVRtnDUPn).KhosbRezsowqdChzdHNgPKqxHjg.vTDmWARsxeeUxohDrynyBxFGitD;
				}
			}

			public float lastTimeUnpressed
			{
				get
				{
					if (ReInput._id != aYVuMCKfoBJZlPKnMohBfcrinAp)
					{
						ReInput.CheckInitialized(aYVuMCKfoBJZlPKnMohBfcrinAp);
						return 0f;
					}
					return ((jtduYPhzqBtbUDGZSYbpSQhCZJl.WSChknhmmSafqILkAgKIkTyxVCw)CFUBuHdIcOeWCJuZmjVIAcmGFYOW.efdaUUDCCKozygKZVzqVRtnDUPn).KhosbRezsowqdChzdHNgPKqxHjg.oZbnNUjVjffcZCqYdlJFLDKHbOn;
				}
			}

			public float lastTimeStateChanged
			{
				get
				{
					if (ReInput._id != aYVuMCKfoBJZlPKnMohBfcrinAp)
					{
						ReInput.CheckInitialized(aYVuMCKfoBJZlPKnMohBfcrinAp);
						return 0f;
					}
					return ((jtduYPhzqBtbUDGZSYbpSQhCZJl.WSChknhmmSafqILkAgKIkTyxVCw)CFUBuHdIcOeWCJuZmjVIAcmGFYOW.efdaUUDCCKozygKZVzqVRtnDUPn).KhosbRezsowqdChzdHNgPKqxHjg.VOhdWtuIOxTZdByLxMBeFaqwGTp;
				}
			}

			internal ButtonStateFlags state
			{
				get
				{
					jtduYPhzqBtbUDGZSYbpSQhCZJl.WSChknhmmSafqILkAgKIkTyxVCw wSChknhmmSafqILkAgKIkTyxVCw = (jtduYPhzqBtbUDGZSYbpSQhCZJl.WSChknhmmSafqILkAgKIkTyxVCw)CFUBuHdIcOeWCJuZmjVIAcmGFYOW.efdaUUDCCKozygKZVzqVRtnDUPn;
					ButtonStateFlags buttonStateFlags = ButtonStateFlags.mDAIjbNLvUSVEOgNmcNByLKLdGwC;
					if (wSChknhmmSafqILkAgKIkTyxVCw.OQeeUtoXIWATcsLBMoJCoZMLWDy)
					{
						goto IL_001d;
					}
					goto IL_004e;
					IL_004e:
					int num;
					if (wSChknhmmSafqILkAgKIkTyxVCw.URkQoUdBuudCATWkvNdYaKAScNP)
					{
						buttonStateFlags |= ButtonStateFlags.xEtdCfZbFHEebJHvZwtAPFMepKc;
						num = 1967335471;
						goto IL_0022;
					}
					goto IL_0074;
					IL_001d:
					num = 1967335470;
					goto IL_0022;
					IL_0022:
					while (true)
					{
						switch (num ^ 0x7543282F)
						{
						case 3:
							break;
						case 1:
							buttonStateFlags |= ButtonStateFlags.dhSkqCVZMLvsBTUzpLskLBEmHkve;
							num = 1967335467;
							continue;
						case 2:
							goto IL_004e;
						case 4:
							if (!wSChknhmmSafqILkAgKIkTyxVCw.URkQoUdBuudCATWkvNdYaKAScNP)
							{
								buttonStateFlags |= ButtonStateFlags.IbdWyFrzEdFqnmiwdbDCAuhEdnPG;
								num = 1967335471;
								continue;
							}
							goto IL_0074;
						default:
							goto IL_0074;
						}
						break;
					}
					goto IL_001d;
					IL_0074:
					return buttonStateFlags;
				}
			}

			internal Button(Controller controller, int elementIdentifierId, string name, HardwareButtonInfo buttonInfo)
				: base(controller, elementIdentifierId, name, ControllerElementType.Button)
			{
				WGLowJGWkhfxfZAOoGBhavBWonj = buttonInfo;
				CFUBuHdIcOeWCJuZmjVIAcmGFYOW = new jtduYPhzqBtbUDGZSYbpSQhCZJl(ReInput.configVars.updateLoop, isPressureSensitive: false);
			}

			internal Button(Controller controller, int elementIdentifierId, string name, bool isPressureSensitive, HardwareButtonInfo buttonInfo)
				: base(controller, elementIdentifierId, name, ControllerElementType.Button)
			{
				WGLowJGWkhfxfZAOoGBhavBWonj = buttonInfo;
				NzEtIbXhndCRmkmnMiPBfEClEtEi = isPressureSensitive;
				CFUBuHdIcOeWCJuZmjVIAcmGFYOW = new jtduYPhzqBtbUDGZSYbpSQhCZJl(ReInput.configVars.updateLoop, isPressureSensitive);
			}

			public bool DoublePressedAndHeld(float speed)
			{
				if (ReInput._id != aYVuMCKfoBJZlPKnMohBfcrinAp)
				{
					ReInput.CheckInitialized(aYVuMCKfoBJZlPKnMohBfcrinAp);
					return false;
				}
				if (speed <= 0f)
				{
					return ((jtduYPhzqBtbUDGZSYbpSQhCZJl.WSChknhmmSafqILkAgKIkTyxVCw)CFUBuHdIcOeWCJuZmjVIAcmGFYOW.efdaUUDCCKozygKZVzqVRtnDUPn).FiTgLUodvKUYcfyjNElpdnUrCkj.ZCwaVQOIjqyCDxclpvbiKuVsMmr;
				}
				return ((jtduYPhzqBtbUDGZSYbpSQhCZJl.WSChknhmmSafqILkAgKIkTyxVCw)CFUBuHdIcOeWCJuZmjVIAcmGFYOW.efdaUUDCCKozygKZVzqVRtnDUPn).KhosbRezsowqdChzdHNgPKqxHjg.BZTfBXIqllFrnXEiXCTtGCGRrip(speed);
			}

			public bool JustDoublePressed(float speed)
			{
				if (ReInput._id != aYVuMCKfoBJZlPKnMohBfcrinAp)
				{
					ReInput.CheckInitialized(aYVuMCKfoBJZlPKnMohBfcrinAp);
					return false;
				}
				if (!justPressed)
				{
					return false;
				}
				if (speed <= 0f)
				{
					return ((jtduYPhzqBtbUDGZSYbpSQhCZJl.WSChknhmmSafqILkAgKIkTyxVCw)CFUBuHdIcOeWCJuZmjVIAcmGFYOW.efdaUUDCCKozygKZVzqVRtnDUPn).FiTgLUodvKUYcfyjNElpdnUrCkj.ZCwaVQOIjqyCDxclpvbiKuVsMmr;
				}
				return ((jtduYPhzqBtbUDGZSYbpSQhCZJl.WSChknhmmSafqILkAgKIkTyxVCw)CFUBuHdIcOeWCJuZmjVIAcmGFYOW.efdaUUDCCKozygKZVzqVRtnDUPn).KhosbRezsowqdChzdHNgPKqxHjg.BZTfBXIqllFrnXEiXCTtGCGRrip(speed);
			}

			internal void HDNHWHMIgYVNwexHqBXaTGgQOqc(UpdateLoopType P_0, int P_1, ControllerDataUpdater P_2)
			{
				//Discarded unreachable code: IL_0066
				if (CFUBuHdIcOeWCJuZmjVIAcmGFYOW != null && CFUBuHdIcOeWCJuZmjVIAcmGFYOW.QdJgtIVEJpgyGztFjxSuDSieQFY != (int)P_0)
				{
					CFUBuHdIcOeWCJuZmjVIAcmGFYOW.jpeUZwJmLgwzUifJmJkdXGhujfP = P_0;
					while (true)
					{
						switch (0x5FC0B788 ^ 0x5FC0B789)
						{
						case 2:
							break;
						case 1:
							goto IL_0040;
						default:
							goto IL_006d;
						}
					}
				}
				goto IL_0040;
				IL_006d:
				((jtduYPhzqBtbUDGZSYbpSQhCZJl.WSChknhmmSafqILkAgKIkTyxVCw)CFUBuHdIcOeWCJuZmjVIAcmGFYOW.efdaUUDCCKozygKZVzqVRtnDUPn).HDNHWHMIgYVNwexHqBXaTGgQOqc(P_2.buttonValues[P_1]);
				return;
				IL_0040:
				if (NzEtIbXhndCRmkmnMiPBfEClEtEi)
				{
					((jtduYPhzqBtbUDGZSYbpSQhCZJl.uervCLYIpOUmfHmjMkObzfaQbXg)CFUBuHdIcOeWCJuZmjVIAcmGFYOW.efdaUUDCCKozygKZVzqVRtnDUPn).HDNHWHMIgYVNwexHqBXaTGgQOqc(P_2.buttonPressureValues[P_1]);
					return;
				}
				goto IL_006d;
			}

			internal void MiztKnmopjblqKIFLLcRAoZHuXqk(UpdateLoopType P_0)
			{
				//Discarded unreachable code: IL_0078
				if (CFUBuHdIcOeWCJuZmjVIAcmGFYOW != null && CFUBuHdIcOeWCJuZmjVIAcmGFYOW.QdJgtIVEJpgyGztFjxSuDSieQFY != (int)P_0)
				{
					CFUBuHdIcOeWCJuZmjVIAcmGFYOW.jpeUZwJmLgwzUifJmJkdXGhujfP = P_0;
					goto IL_0022;
				}
				goto IL_0044;
				IL_0027:
				int num;
				switch (num ^ 0x286A8CF0)
				{
				case 3:
					break;
				case 1:
					goto IL_0044;
				case 0:
					((jtduYPhzqBtbUDGZSYbpSQhCZJl.uervCLYIpOUmfHmjMkObzfaQbXg)CFUBuHdIcOeWCJuZmjVIAcmGFYOW.efdaUUDCCKozygKZVzqVRtnDUPn).HDNHWHMIgYVNwexHqBXaTGgQOqc(0f);
					return;
				default:
					((jtduYPhzqBtbUDGZSYbpSQhCZJl.WSChknhmmSafqILkAgKIkTyxVCw)CFUBuHdIcOeWCJuZmjVIAcmGFYOW.efdaUUDCCKozygKZVzqVRtnDUPn).HDNHWHMIgYVNwexHqBXaTGgQOqc(false);
					return;
				}
				goto IL_0022;
				IL_0022:
				num = 678071537;
				goto IL_0027;
				IL_0044:
				int num2;
				if (!NzEtIbXhndCRmkmnMiPBfEClEtEi)
				{
					num = 678071538;
					num2 = num;
				}
				else
				{
					num = 678071536;
					num2 = num;
				}
				goto IL_0027;
			}

			internal void GQrgXjfZKMNIRjUtzYlbLUJRndi()
			{
				int num = 0;
				jdwGsADNuHdRfvSkYHsqVaIlpFY.MwOKTMHhOtnTTGQGPANMbEDimQs mwOKTMHhOtnTTGQGPANMbEDimQs = default(jdwGsADNuHdRfvSkYHsqVaIlpFY.MwOKTMHhOtnTTGQGPANMbEDimQs);
				while (true)
				{
					int num2 = 1680894917;
					while (true)
					{
						switch (num2 ^ 0x64306BC7)
						{
						default:
							return;
						case 1:
							return;
						case 4:
							break;
						case 2:
							num2 = 1680894916;
							continue;
						case 0:
							num++;
							num2 = 1680894916;
							continue;
						case 5:
							((jtduYPhzqBtbUDGZSYbpSQhCZJl.WSChknhmmSafqILkAgKIkTyxVCw)mwOKTMHhOtnTTGQGPANMbEDimQs).HDNHWHMIgYVNwexHqBXaTGgQOqc(false);
							num2 = 1680894919;
							continue;
						case 3:
						{
							int num3;
							if (num < CFUBuHdIcOeWCJuZmjVIAcmGFYOW.nYhhlACKNviVZKXqFuHHtFvfQUi.Count)
							{
								num2 = 1680894913;
								num3 = num2;
							}
							else
							{
								num2 = 1680894918;
								num3 = num2;
							}
							continue;
						}
						case 6:
							mwOKTMHhOtnTTGQGPANMbEDimQs = CFUBuHdIcOeWCJuZmjVIAcmGFYOW.nYhhlACKNviVZKXqFuHHtFvfQUi[num];
							if (mwOKTMHhOtnTTGQGPANMbEDimQs == null)
							{
								goto case 0;
							}
							if (NzEtIbXhndCRmkmnMiPBfEClEtEi)
							{
								((jtduYPhzqBtbUDGZSYbpSQhCZJl.uervCLYIpOUmfHmjMkObzfaQbXg)mwOKTMHhOtnTTGQGPANMbEDimQs).HDNHWHMIgYVNwexHqBXaTGgQOqc(0f);
								num2 = 1680894919;
								continue;
							}
							goto case 5;
						}
						break;
					}
				}
			}
		}

		public abstract class CompoundElement
		{
			private class iTSTRPyonoQBRgwCqIqrXswlKll
			{
				public readonly Element iYpLnefngVCzxiXIEIhepTIxawrZ;

				public readonly int LuzHcdXcijFAfdEUdmYByDvAEPI;

				public iTSTRPyonoQBRgwCqIqrXswlKll(Element element, int elementIndex)
				{
					iYpLnefngVCzxiXIEIhepTIxawrZ = element;
					LuzHcdXcijFAfdEUdmYByDvAEPI = elementIndex;
				}
			}

			private int hSULoVXORPauXdCqLuMLNWSvuDs;

			private string BGvCsqKINSdTDMbBCcWESKquxsA;

			private CompoundControllerElementType xgVZxqzhkfLujyUcmEYcqMemNFx;

			private int FNJoQxSwoSqtsfJGqaJebETmYFMt;

			private iTSTRPyonoQBRgwCqIqrXswlKll[] WBfQciSrovnPIpngxqhztwVhEqL;

			private Controller SzlUIkTCuzzKtIpFottQRnFTfsRF;

			internal readonly int aYVuMCKfoBJZlPKnMohBfcrinAp;

			public int id
			{
				get
				{
					if (ReInput._id != aYVuMCKfoBJZlPKnMohBfcrinAp)
					{
						ReInput.CheckInitialized(aYVuMCKfoBJZlPKnMohBfcrinAp);
						return -1;
					}
					return hSULoVXORPauXdCqLuMLNWSvuDs;
				}
			}

			public string name
			{
				get
				{
					if (ReInput._id != aYVuMCKfoBJZlPKnMohBfcrinAp)
					{
						ReInput.CheckInitialized(aYVuMCKfoBJZlPKnMohBfcrinAp);
						return string.Empty;
					}
					return BGvCsqKINSdTDMbBCcWESKquxsA;
				}
			}

			public CompoundControllerElementType type
			{
				get
				{
					if (ReInput._id != aYVuMCKfoBJZlPKnMohBfcrinAp)
					{
						ReInput.CheckInitialized(aYVuMCKfoBJZlPKnMohBfcrinAp);
						return CompoundControllerElementType.Axis2D;
					}
					return xgVZxqzhkfLujyUcmEYcqMemNFx;
				}
			}

			public bool hasElements
			{
				get
				{
					if (ReInput._id != aYVuMCKfoBJZlPKnMohBfcrinAp)
					{
						while (true)
						{
							int num = 365492018;
							while (true)
							{
								switch (num ^ 0x15C8F730)
								{
								case 0:
									break;
								case 2:
									goto IL_002b;
								default:
									return false;
								}
								break;
								IL_002b:
								ReInput.CheckInitialized(aYVuMCKfoBJZlPKnMohBfcrinAp);
								num = 365492017;
							}
						}
					}
					return FNJoQxSwoSqtsfJGqaJebETmYFMt > 0;
				}
			}

			public int elementCount
			{
				get
				{
					if (ReInput._id != aYVuMCKfoBJZlPKnMohBfcrinAp)
					{
						ReInput.CheckInitialized(aYVuMCKfoBJZlPKnMohBfcrinAp);
						return 0;
					}
					return FNJoQxSwoSqtsfJGqaJebETmYFMt;
				}
			}

			public abstract int elementCapacity
			{
				get;
			}

			public ControllerElementIdentifier elementIdentifier
			{
				get
				{
					if (ReInput._id != aYVuMCKfoBJZlPKnMohBfcrinAp)
					{
						ReInput.CheckInitialized(aYVuMCKfoBJZlPKnMohBfcrinAp);
						return null;
					}
					ControllerElementIdentifier elementIdentifierById = SzlUIkTCuzzKtIpFottQRnFTfsRF.GetElementIdentifierById(hSULoVXORPauXdCqLuMLNWSvuDs);
					if (elementIdentifierById == null)
					{
						return ControllerElementIdentifier.BlankReadOnly;
					}
					return elementIdentifierById;
				}
			}

			internal CompoundElement(Controller controller, int elementIdentifierId, string name, CompoundControllerElementType type)
			{
				while (true)
				{
					int num = 2076732973;
					while (true)
					{
						switch (num ^ 0x7BC86E2F)
						{
						case 0:
							break;
						case 3:
							BGvCsqKINSdTDMbBCcWESKquxsA = name;
							xgVZxqzhkfLujyUcmEYcqMemNFx = type;
							WBfQciSrovnPIpngxqhztwVhEqL = new iTSTRPyonoQBRgwCqIqrXswlKll[elementCapacity];
							num = 2076732974;
							continue;
						case 4:
							hSULoVXORPauXdCqLuMLNWSvuDs = elementIdentifierId;
							num = 2076732972;
							continue;
						case 2:
							SzlUIkTCuzzKtIpFottQRnFTfsRF = controller;
							num = 2076732971;
							continue;
						default:
							aYVuMCKfoBJZlPKnMohBfcrinAp = ReInput.id;
							return;
						}
						break;
					}
				}
			}

			internal Element LauHTFgotpvUQIfEqOPDmnXmqEP(int P_0)
			{
				if (P_0 >= 0)
				{
					while (true)
					{
						int num = -404761277;
						while (true)
						{
							switch (num ^ -404761279)
							{
							case 0:
								break;
							case 2:
								goto IL_0026;
							case 3:
								goto IL_0038;
							default:
								return null;
							}
							break;
							IL_0026:
							if (P_0 >= WBfQciSrovnPIpngxqhztwVhEqL.Length)
							{
								num = -404761278;
							}
							else
							{
								if (WBfQciSrovnPIpngxqhztwVhEqL[P_0] != null)
								{
									return WBfQciSrovnPIpngxqhztwVhEqL[P_0].iYpLnefngVCzxiXIEIhepTIxawrZ;
								}
								num = -404761280;
							}
						}
					}
				}
				goto IL_0038;
				IL_0038:
				return null;
			}

			internal T LauHTFgotpvUQIfEqOPDmnXmqEP<T>(int P_0) where T : Element
			{
				T result = default(T);
				if (P_0 >= 0)
				{
					while (true)
					{
						int num = 378938272;
						while (true)
						{
							switch (num ^ 0x169623A2)
							{
							case 0:
								break;
							case 2:
								goto IL_0026;
							case 3:
								goto IL_0038;
							default:
								return result;
							}
							break;
							IL_0026:
							if (P_0 >= WBfQciSrovnPIpngxqhztwVhEqL.Length)
							{
								num = 378938273;
							}
							else
							{
								if (WBfQciSrovnPIpngxqhztwVhEqL[P_0] != null)
								{
									return WBfQciSrovnPIpngxqhztwVhEqL[P_0].iYpLnefngVCzxiXIEIhepTIxawrZ as T;
								}
								result = null;
								num = 378938275;
							}
						}
					}
				}
				goto IL_0038;
				IL_0038:
				return null;
			}

			internal T RKgGpaBtZjHVYGNWbnHehFRrNLWR<T>(int P_0, out int P_1) where T : Element
			{
				P_1 = -1;
				T result = default(T);
				while (true)
				{
					int num = -585674989;
					while (true)
					{
						switch (num ^ -585674985)
						{
						case 0:
							break;
						case 4:
						{
							int num2;
							if (P_0 >= 0)
							{
								num = -585674988;
								num2 = num;
							}
							else
							{
								num = -585674986;
								num2 = num;
							}
							continue;
						}
						case 1:
							result = null;
							num = -585674987;
							continue;
						case 3:
							if (P_0 >= WBfQciSrovnPIpngxqhztwVhEqL.Length)
							{
								num = -585674986;
								continue;
							}
							if (WBfQciSrovnPIpngxqhztwVhEqL[P_0] == null)
							{
								return null;
							}
							P_1 = WBfQciSrovnPIpngxqhztwVhEqL[P_0].LuzHcdXcijFAfdEUdmYByDvAEPI;
							return WBfQciSrovnPIpngxqhztwVhEqL[P_0].iYpLnefngVCzxiXIEIhepTIxawrZ as T;
						default:
							return result;
						}
						break;
					}
				}
			}

			internal bool htWbfivVfubrLgJDGOktUnLLIDLq(Element P_0, int P_1)
			{
				if (P_0 == null)
				{
					goto IL_0003;
				}
				int num;
				if (FNJoQxSwoSqtsfJGqaJebETmYFMt >= elementCapacity)
				{
					num = 1554270841;
				}
				else if (!P_0.isMemberElement)
				{
					if (MGtjpWXZDDUzBwucswZrgEsISuQ(P_0) >= 0)
					{
						Logger.LogWarning("Cannot add element! This Compound Element already contains the element you are trying to add.");
						return false;
					}
					int num2 = arEnNzlQkmlDgTWrKZYXuGnZGIJ();
					if (num2 >= 0)
					{
						return AnzZckCFqGhBGNKceFxmQwXArW(P_0, P_1, num2);
					}
					Logger.LogWarning("Cannot add element! This Compound Element already contains the maximum number of elements.");
					num = 1554270846;
				}
				else
				{
					Logger.LogWarning("Cannot add element! The element you are trying to add is already a member of another compound element.");
					num = 1554270847;
				}
				goto IL_0008;
				IL_0008:
				while (true)
				{
					switch (num ^ 0x5CA44A7D)
					{
					case 0:
						break;
					case 1:
						return false;
					case 4:
						goto IL_0044;
					case 2:
						return false;
					case 5:
						return false;
					default:
						return false;
					}
					break;
					IL_0044:
					Logger.LogWarning("Cannot add element! This Compound Element already contains the maximum number of elements.");
					num = 1554270840;
				}
				goto IL_0003;
				IL_0003:
				num = 1554270844;
				goto IL_0008;
			}

			internal bool PoEeTGSRIdiRAZOzpAdkEhhFFTwB(Element P_0)
			{
				if (P_0 == null)
				{
					goto IL_0003;
				}
				int num;
				if (FNJoQxSwoSqtsfJGqaJebETmYFMt == 0)
				{
					Logger.LogWarning("Cannot remove element! This Compound Element has no elements.");
					num = 176848780;
					goto IL_0008;
				}
				int num2 = MGtjpWXZDDUzBwucswZrgEsISuQ(P_0);
				if (num2 < 0)
				{
					Logger.LogWarning("Cannot remove element! This Compound Element does not contain the element you are trying to remove.");
					return false;
				}
				return jdZDAvDarKFzkibOOMyJEiSaNyyG(num2);
				IL_0008:
				switch (num ^ 0xA8A7F8C)
				{
				case 2:
					break;
				case 1:
					return false;
				default:
					return false;
				}
				goto IL_0003;
				IL_0003:
				num = 176848781;
				goto IL_0008;
			}

			internal void ZoNYqLVooNBdffNOxbbWfLHanBMQ()
			{
				int num = 0;
				while (true)
				{
					int num2;
					int num3;
					if (num < WBfQciSrovnPIpngxqhztwVhEqL.Length)
					{
						num2 = -1645552352;
						num3 = num2;
					}
					else
					{
						num2 = -1645552346;
						num3 = num2;
					}
					while (true)
					{
						switch (num2 ^ -1645552348)
						{
						default:
							return;
						case 3:
							return;
						case 0:
							num2 = -1645552352;
							continue;
						case 2:
							FNJoQxSwoSqtsfJGqaJebETmYFMt = 0;
							num2 = -1645552345;
							continue;
						case 1:
							break;
						case 4:
							jdZDAvDarKFzkibOOMyJEiSaNyyG(num);
							num++;
							num2 = -1645552347;
							continue;
						}
						break;
					}
				}
			}

			private int MGtjpWXZDDUzBwucswZrgEsISuQ(Element P_0)
			{
				if (P_0 == null)
				{
					goto IL_0003;
				}
				int num = 0;
				int num2 = -2147416501;
				goto IL_0008;
				IL_0003:
				num2 = -2147416504;
				goto IL_0008;
				IL_0008:
				while (true)
				{
					switch (num2 ^ -2147416503)
					{
					case 0:
						break;
					case 1:
						return -1;
					case 4:
						if (WBfQciSrovnPIpngxqhztwVhEqL[num] != null && WBfQciSrovnPIpngxqhztwVhEqL[num].iYpLnefngVCzxiXIEIhepTIxawrZ == P_0)
						{
							return num;
						}
						num++;
						num2 = -2147416502;
						continue;
					case 2:
						num2 = -2147416502;
						continue;
					case 3:
					{
						int num3;
						if (num < WBfQciSrovnPIpngxqhztwVhEqL.Length)
						{
							num2 = -2147416499;
							num3 = num2;
						}
						else
						{
							num2 = -2147416500;
							num3 = num2;
						}
						continue;
					}
					default:
						return -1;
					}
					break;
				}
				goto IL_0003;
			}

			private bool AnzZckCFqGhBGNKceFxmQwXArW(Element P_0, int P_1, int P_2)
			{
				if (P_2 >= 0)
				{
					while (true)
					{
						int num = 582572811;
						while (true)
						{
							switch (num ^ 0x22B95B0A)
							{
							case 4:
								break;
							case 1:
								goto IL_002a;
							case 2:
								goto IL_003c;
							case 0:
								FNJoQxSwoSqtsfJGqaJebETmYFMt++;
								num = 582572809;
								continue;
							default:
								return true;
							}
							break;
							IL_002a:
							if (P_2 >= WBfQciSrovnPIpngxqhztwVhEqL.Length)
							{
								num = 582572808;
							}
							else
							{
								if (WBfQciSrovnPIpngxqhztwVhEqL[P_2] != null)
								{
									return false;
								}
								WBfQciSrovnPIpngxqhztwVhEqL[P_2] = new iTSTRPyonoQBRgwCqIqrXswlKll(P_0, P_1);
								P_0.YVogTthihPASQiwheBqNvWwwsIH();
								num = 582572810;
							}
						}
					}
				}
				goto IL_003c;
				IL_003c:
				return false;
			}

			private bool jdZDAvDarKFzkibOOMyJEiSaNyyG(int P_0)
			{
				if (P_0 < 0)
				{
					goto IL_0031;
				}
				if (P_0 >= WBfQciSrovnPIpngxqhztwVhEqL.Length)
				{
					goto IL_000f;
				}
				if (WBfQciSrovnPIpngxqhztwVhEqL[P_0] == null)
				{
					return false;
				}
				int num;
				if (WBfQciSrovnPIpngxqhztwVhEqL[P_0].iYpLnefngVCzxiXIEIhepTIxawrZ != null)
				{
					WBfQciSrovnPIpngxqhztwVhEqL[P_0].iYpLnefngVCzxiXIEIhepTIxawrZ.vsMaTurtKltlZvkWtzFIQirGoUG();
					num = -1430712335;
					goto IL_0014;
				}
				goto IL_0067;
				IL_000f:
				num = -1430712333;
				goto IL_0014;
				IL_0067:
				WBfQciSrovnPIpngxqhztwVhEqL[P_0] = null;
				num = -1430712336;
				goto IL_0014;
				IL_0014:
				switch (num ^ -1430712334)
				{
				case 0:
					break;
				case 1:
					goto IL_0031;
				case 3:
					goto IL_0067;
				default:
					FNJoQxSwoSqtsfJGqaJebETmYFMt--;
					return true;
				}
				goto IL_000f;
				IL_0031:
				return false;
			}

			private int arEnNzlQkmlDgTWrKZYXuGnZGIJ()
			{
				int num = 0;
				while (true)
				{
					int num2 = 1997048872;
					while (true)
					{
						switch (num2 ^ 0x77088C29)
						{
						case 0:
							break;
						case 1:
							num2 = 1997048874;
							continue;
						case 2:
							if (WBfQciSrovnPIpngxqhztwVhEqL[num] == null)
							{
								return num;
							}
							num++;
							num2 = 1997048874;
							continue;
						default:
							if (num >= WBfQciSrovnPIpngxqhztwVhEqL.Length)
							{
								return -1;
							}
							goto case 2;
						}
						break;
					}
				}
			}
		}

		public sealed class Axis2D : CompoundElement
		{
			private const int TsuwQoCkcvBjnAPJBCuPpZsGDjb = 2;

			private CalibrationMap wzCQSkyDDhdFLLISGEYcGgXzUSQ;

			public override int elementCapacity => 2;

			public Axis xAxis
			{
				get
				{
					if (ReInput._id != aYVuMCKfoBJZlPKnMohBfcrinAp)
					{
						while (true)
						{
							int num = -1179049842;
							while (true)
							{
								switch (num ^ -1179049841)
								{
								case 0:
									break;
								case 1:
									goto IL_002b;
								default:
									return null;
								}
								break;
								IL_002b:
								ReInput.CheckInitialized(aYVuMCKfoBJZlPKnMohBfcrinAp);
								num = -1179049843;
							}
						}
					}
					return LauHTFgotpvUQIfEqOPDmnXmqEP<Axis>(0);
				}
			}

			public Axis yAxis
			{
				get
				{
					if (ReInput._id != aYVuMCKfoBJZlPKnMohBfcrinAp)
					{
						ReInput.CheckInitialized(aYVuMCKfoBJZlPKnMohBfcrinAp);
						return null;
					}
					return LauHTFgotpvUQIfEqOPDmnXmqEP<Axis>(1);
				}
			}

			public Vector2 value
			{
				get
				{
					if (ReInput._id != aYVuMCKfoBJZlPKnMohBfcrinAp)
					{
						ReInput.CheckInitialized(aYVuMCKfoBJZlPKnMohBfcrinAp);
						return Vector2.zero;
					}
					return vjbAcCTDysaQEApuUyRxENbRdAG();
				}
			}

			public Vector2 valuePrev
			{
				get
				{
					if (ReInput._id != aYVuMCKfoBJZlPKnMohBfcrinAp)
					{
						ReInput.CheckInitialized(aYVuMCKfoBJZlPKnMohBfcrinAp);
						return Vector2.zero;
					}
					return dnUiapNKmWKnkyHqHmPBlFCRjxd();
				}
			}

			public Vector2 valueRaw
			{
				get
				{
					if (ReInput._id != aYVuMCKfoBJZlPKnMohBfcrinAp)
					{
						ReInput.CheckInitialized(aYVuMCKfoBJZlPKnMohBfcrinAp);
						return Vector2.zero;
					}
					return new Vector2((xAxis != null) ? xAxis.valueRaw : 0f, (yAxis != null) ? yAxis.valueRaw : 0f);
				}
			}

			public Vector2 valueRawPrev
			{
				get
				{
					if (ReInput._id != aYVuMCKfoBJZlPKnMohBfcrinAp)
					{
						ReInput.CheckInitialized(aYVuMCKfoBJZlPKnMohBfcrinAp);
						return Vector2.zero;
					}
					return new Vector2((xAxis != null) ? xAxis.valueRawPrev : 0f, (yAxis != null) ? yAxis.valueRawPrev : 0f);
				}
			}

			internal Axis2D(Controller controller, int elementIdentifierId, string name, Axis xAxis, Axis yAxis, int xAxisIndex, int yAxisIndex, CalibrationMap calibratonMap)
				: base(controller, elementIdentifierId, name, CompoundControllerElementType.Axis2D)
			{
				htWbfivVfubrLgJDGOktUnLLIDLq(xAxis, xAxisIndex);
				htWbfivVfubrLgJDGOktUnLLIDLq(yAxis, yAxisIndex);
				wzCQSkyDDhdFLLISGEYcGgXzUSQ = calibratonMap;
			}

			internal void mfhYkStljXvqJpddoSRqFZDBXTj()
			{
				Vector2 value = this.value;
				while (true)
				{
					int num = -1912920303;
					while (true)
					{
						switch (num ^ -1912920304)
						{
						default:
							return;
						case 4:
							return;
						case 5:
							break;
						case 1:
						{
							int num3;
							if (xAxis != null)
							{
								num = -1912920304;
								num3 = num;
							}
							else
							{
								num = -1912920301;
								num3 = num;
							}
							continue;
						}
						case 2:
							yAxis.BDQBkZIuzzRukZWrbHSCGnFIEDw(value.y);
							num = -1912920300;
							continue;
						case 0:
							xAxis.BDQBkZIuzzRukZWrbHSCGnFIEDw(value.x);
							num = -1912920301;
							continue;
						case 3:
						{
							int num2;
							if (yAxis != null)
							{
								num = -1912920302;
								num2 = num;
							}
							else
							{
								num = -1912920300;
								num2 = num;
							}
							continue;
						}
						}
						break;
					}
				}
			}

			private Vector2 vjbAcCTDysaQEApuUyRxENbRdAG()
			{
				if (wzCQSkyDDhdFLLISGEYcGgXzUSQ == null)
				{
					return default(Vector2);
				}
				int xAxisIndex;
				Axis axis = RKgGpaBtZjHVYGNWbnHehFRrNLWR<Axis>(0, out xAxisIndex);
				int yAxisIndex;
				Axis axis2 = RKgGpaBtZjHVYGNWbnHehFRrNLWR<Axis>(1, out yAxisIndex);
				DeadZone2DType defaultJoystickAxis2DDeadZoneType = ReInput.configVars.defaultJoystickAxis2DDeadZoneType;
				AxisSensitivity2DType defaultJoystickAxis2DSensitivityType = default(AxisSensitivity2DType);
				float valueRawX = default(float);
				while (true)
				{
					int num = 822475689;
					while (true)
					{
						float num2;
						float valueRawY;
						switch (num ^ 0x3105FBAB)
						{
						case 0:
							break;
						case 2:
							defaultJoystickAxis2DSensitivityType = ReInput.configVars.defaultJoystickAxis2DSensitivityType;
							valueRawX = (axis?.valueRaw ?? 0f);
							if (axis2 == null)
							{
								goto IL_0072;
							}
							num2 = axis2.valueRaw;
							goto IL_0086;
						default:
							{
								num2 = 0f;
								goto IL_0086;
							}
							IL_0086:
							valueRawY = num2;
							return wzCQSkyDDhdFLLISGEYcGgXzUSQ.GetCalibrated2DValue(xAxisIndex, yAxisIndex, valueRawX, valueRawY, defaultJoystickAxis2DDeadZoneType, defaultJoystickAxis2DSensitivityType);
						}
						break;
						IL_0072:
						num = 822475690;
					}
				}
			}

			private Vector2 dnUiapNKmWKnkyHqHmPBlFCRjxd()
			{
				if (wzCQSkyDDhdFLLISGEYcGgXzUSQ == null)
				{
					goto IL_0008;
				}
				int xAxisIndex = default(int);
				Axis axis = RKgGpaBtZjHVYGNWbnHehFRrNLWR<Axis>(0, out xAxisIndex);
				int num = -1267931782;
				goto IL_000d;
				IL_0008:
				num = -1267931781;
				goto IL_000d;
				IL_000d:
				switch (num ^ -1267931782)
				{
				case 2:
					break;
				case 1:
					return default(Vector2);
				default:
				{
					int yAxisIndex;
					Axis axis2 = RKgGpaBtZjHVYGNWbnHehFRrNLWR<Axis>(1, out yAxisIndex);
					DeadZone2DType defaultJoystickAxis2DDeadZoneType = ReInput.configVars.defaultJoystickAxis2DDeadZoneType;
					AxisSensitivity2DType defaultJoystickAxis2DSensitivityType = ReInput.configVars.defaultJoystickAxis2DSensitivityType;
					float valueRawX = axis?.valueRawPrev ?? 0f;
					float valueRawY = axis2?.valueRawPrev ?? 0f;
					return wzCQSkyDDhdFLLISGEYcGgXzUSQ.GetCalibrated2DValue(xAxisIndex, yAxisIndex, valueRawX, valueRawY, defaultJoystickAxis2DDeadZoneType, defaultJoystickAxis2DSensitivityType);
				}
				}
				goto IL_0008;
			}
		}

		public sealed class Hat : CompoundElement
		{
			private const int TsuwQoCkcvBjnAPJBCuPpZsGDjb = 8;

			private const int ircNONiPmieuOaTvKGvQTSoJFIMB = 0;

			private const int BjmhWiBJeAjURoPPundHaCzhjlh = 1;

			private const int cAhcXPHvFLiwYRAlGxSWoTfaOgvA = 2;

			private const int FlkxoXhOvScSDTCQoxsvhpYXdLT = 3;

			private const int waBRpwVCtILOzvpqSATmxdvTCNpF = 4;

			private const int gbFFTJzDqpeOPLAgjTLxXhRCYXj = 5;

			private const int eNpayihrqjyUptmzweqxABJgbkJ = 6;

			private const int ZELkIYwaCFXCtVVfXyJAPdSjHFl = 7;

			private readonly int sKJDLgRVEcKPUOtgYbrpHejjJzN;

			private readonly Button[] arCmrATnXCKXtigWnqYrXYhIBTW;

			private readonly ReadOnlyCollection<Button> RmUEkIBCzDzZmtrECMcaGmbVIooS;

			private readonly int[] IZvRKkVEnKKjcxjNzidomtzrkHO;

			private bool mHcwofXZgGTHVbMnlISpIwciENlC;

			public override int elementCapacity => 8;

			public bool force4Way
			{
				get
				{
					if (ReInput._id != aYVuMCKfoBJZlPKnMohBfcrinAp)
					{
						while (true)
						{
							int num = -1367795396;
							while (true)
							{
								switch (num ^ -1367795395)
								{
								case 0:
									break;
								case 1:
									goto IL_002b;
								default:
									return false;
								}
								break;
								IL_002b:
								ReInput.CheckInitialized(aYVuMCKfoBJZlPKnMohBfcrinAp);
								num = -1367795393;
							}
						}
					}
					return mHcwofXZgGTHVbMnlISpIwciENlC;
				}
				set
				{
					//Discarded unreachable code: IL_0038
					if (ReInput._id != aYVuMCKfoBJZlPKnMohBfcrinAp)
					{
						while (true)
						{
							switch (-2029262609 ^ -2029262610)
							{
							case 0:
								continue;
							case 1:
								ReInput.CheckInitialized(aYVuMCKfoBJZlPKnMohBfcrinAp);
								return;
							}
							break;
						}
					}
					mHcwofXZgGTHVbMnlISpIwciENlC = value;
				}
			}

			public int directionCount
			{
				get
				{
					if (ReInput._id != aYVuMCKfoBJZlPKnMohBfcrinAp)
					{
						ReInput.CheckInitialized(aYVuMCKfoBJZlPKnMohBfcrinAp);
						return 0;
					}
					return sKJDLgRVEcKPUOtgYbrpHejjJzN;
				}
			}

			public IList<Button> Buttons
			{
				get
				{
					if (ReInput._id != aYVuMCKfoBJZlPKnMohBfcrinAp)
					{
						ReInput.CheckInitialized(aYVuMCKfoBJZlPKnMohBfcrinAp);
						return EmptyObjects<Button>.EmptyReadOnlyIListT;
					}
					return RmUEkIBCzDzZmtrECMcaGmbVIooS;
				}
			}

			public Button buttonUp
			{
				get
				{
					if (ReInput._id != aYVuMCKfoBJZlPKnMohBfcrinAp)
					{
						ReInput.CheckInitialized(aYVuMCKfoBJZlPKnMohBfcrinAp);
						return null;
					}
					return LauHTFgotpvUQIfEqOPDmnXmqEP<Button>(0);
				}
			}

			public Button buttonRight
			{
				get
				{
					if (ReInput._id != aYVuMCKfoBJZlPKnMohBfcrinAp)
					{
						ReInput.CheckInitialized(aYVuMCKfoBJZlPKnMohBfcrinAp);
						return null;
					}
					return LauHTFgotpvUQIfEqOPDmnXmqEP<Button>(2);
				}
			}

			public Button buttonDown
			{
				get
				{
					if (ReInput._id != aYVuMCKfoBJZlPKnMohBfcrinAp)
					{
						ReInput.CheckInitialized(aYVuMCKfoBJZlPKnMohBfcrinAp);
						return null;
					}
					return LauHTFgotpvUQIfEqOPDmnXmqEP<Button>(4);
				}
			}

			public Button buttonLeft
			{
				get
				{
					if (ReInput._id != aYVuMCKfoBJZlPKnMohBfcrinAp)
					{
						ReInput.CheckInitialized(aYVuMCKfoBJZlPKnMohBfcrinAp);
						return null;
					}
					return LauHTFgotpvUQIfEqOPDmnXmqEP<Button>(6);
				}
			}

			public Button buttonUpRight
			{
				get
				{
					if (ReInput._id != aYVuMCKfoBJZlPKnMohBfcrinAp)
					{
						ReInput.CheckInitialized(aYVuMCKfoBJZlPKnMohBfcrinAp);
						return null;
					}
					return LauHTFgotpvUQIfEqOPDmnXmqEP<Button>(1);
				}
			}

			public Button buttonDownRight
			{
				get
				{
					if (ReInput._id != aYVuMCKfoBJZlPKnMohBfcrinAp)
					{
						ReInput.CheckInitialized(aYVuMCKfoBJZlPKnMohBfcrinAp);
						return null;
					}
					return LauHTFgotpvUQIfEqOPDmnXmqEP<Button>(3);
				}
			}

			public Button buttonDownLeft
			{
				get
				{
					if (ReInput._id != aYVuMCKfoBJZlPKnMohBfcrinAp)
					{
						ReInput.CheckInitialized(aYVuMCKfoBJZlPKnMohBfcrinAp);
						return null;
					}
					return LauHTFgotpvUQIfEqOPDmnXmqEP<Button>(5);
				}
			}

			public Button buttonUpLeft
			{
				get
				{
					if (ReInput._id != aYVuMCKfoBJZlPKnMohBfcrinAp)
					{
						while (true)
						{
							int num = 756293601;
							while (true)
							{
								switch (num ^ 0x2D141FE3)
								{
								case 0:
									break;
								case 2:
									goto IL_002b;
								default:
									return null;
								}
								break;
								IL_002b:
								ReInput.CheckInitialized(aYVuMCKfoBJZlPKnMohBfcrinAp);
								num = 756293602;
							}
						}
					}
					return LauHTFgotpvUQIfEqOPDmnXmqEP<Button>(7);
				}
			}

			internal Hat(Controller controller, int elementIdentifierId, string name, Button[] buttons, int[] buttonIndices)
				: base(controller, elementIdentifierId, name, CompoundControllerElementType.Hat)
			{
				int num = (buttons != null) ? buttons.Length : 0;
				int num2 = (buttonIndices != null) ? buttonIndices.Length : 0;
				if (num != num2)
				{
					throw new ArgumentException("button.Length must equal buttonIndices.Length!");
				}
				if (num != 0 && num != 4 && num != 8)
				{
					throw new ArgumentException("button.Length must be 0, 4, or 8! Length: " + num);
				}
				for (int i = 0; i < num; i++)
				{
					htWbfivVfubrLgJDGOktUnLLIDLq(buttons[i], buttonIndices[i]);
				}
				arCmrATnXCKXtigWnqYrXYhIBTW = buttons;
				IZvRKkVEnKKjcxjNzidomtzrkHO = buttonIndices;
				sKJDLgRVEcKPUOtgYbrpHejjJzN = num;
				RmUEkIBCzDzZmtrECMcaGmbVIooS = new ReadOnlyCollection<Button>(buttons);
			}

			internal void mfhYkStljXvqJpddoSRqFZDBXTj(UpdateLoopType P_0, ControllerDataUpdater P_1)
			{
				//Discarded unreachable code: IL_00b5
				if (sKJDLgRVEcKPUOtgYbrpHejjJzN == 0)
				{
					return;
				}
				int num3 = default(int);
				while (true)
				{
					IL_00fe:
					int num;
					if (sKJDLgRVEcKPUOtgYbrpHejjJzN == 8)
					{
						int num2;
						if (mHcwofXZgGTHVbMnlISpIwciENlC)
						{
							num = -602623175;
							num2 = num;
						}
						else
						{
							num = -602623182;
							num2 = num;
						}
						goto IL_0011;
					}
					goto IL_0151;
					IL_0011:
					while (true)
					{
						switch (num ^ -602623173)
						{
						default:
							return;
						case 1:
							return;
						case 8:
							num = -602623176;
							continue;
						case 5:
							num3++;
							num = -602623172;
							continue;
						case 0:
							GjoCKnHlOzikPnxEGiAmSbZenvmC(arCmrATnXCKXtigWnqYrXYhIBTW[1], IZvRKkVEnKKjcxjNzidomtzrkHO[1], P_0, P_1);
							GjoCKnHlOzikPnxEGiAmSbZenvmC(arCmrATnXCKXtigWnqYrXYhIBTW[3], IZvRKkVEnKKjcxjNzidomtzrkHO[3], P_0, P_1);
							GjoCKnHlOzikPnxEGiAmSbZenvmC(arCmrATnXCKXtigWnqYrXYhIBTW[5], IZvRKkVEnKKjcxjNzidomtzrkHO[5], P_0, P_1);
							GjoCKnHlOzikPnxEGiAmSbZenvmC(arCmrATnXCKXtigWnqYrXYhIBTW[7], IZvRKkVEnKKjcxjNzidomtzrkHO[7], P_0, P_1);
							return;
						case 9:
							break;
						case 7:
							goto IL_00df;
						case 3:
							goto IL_00fe;
						case 4:
							if (arCmrATnXCKXtigWnqYrXYhIBTW[num3] != null)
							{
								arCmrATnXCKXtigWnqYrXYhIBTW[num3].HDNHWHMIgYVNwexHqBXaTGgQOqc(P_0, IZvRKkVEnKKjcxjNzidomtzrkHO[num3], P_1);
								num = -602623170;
								continue;
							}
							goto case 5;
						case 6:
							goto IL_0151;
						case 2:
							VWgqHxpNUAyjYDHicPAEtgNAiLDA(arCmrATnXCKXtigWnqYrXYhIBTW[0], IZvRKkVEnKKjcxjNzidomtzrkHO[0], IZvRKkVEnKKjcxjNzidomtzrkHO[7], IZvRKkVEnKKjcxjNzidomtzrkHO[1], P_0, P_1);
							VWgqHxpNUAyjYDHicPAEtgNAiLDA(arCmrATnXCKXtigWnqYrXYhIBTW[2], IZvRKkVEnKKjcxjNzidomtzrkHO[2], IZvRKkVEnKKjcxjNzidomtzrkHO[1], IZvRKkVEnKKjcxjNzidomtzrkHO[3], P_0, P_1);
							VWgqHxpNUAyjYDHicPAEtgNAiLDA(arCmrATnXCKXtigWnqYrXYhIBTW[4], IZvRKkVEnKKjcxjNzidomtzrkHO[4], IZvRKkVEnKKjcxjNzidomtzrkHO[5], IZvRKkVEnKKjcxjNzidomtzrkHO[3], P_0, P_1);
							VWgqHxpNUAyjYDHicPAEtgNAiLDA(arCmrATnXCKXtigWnqYrXYhIBTW[6], IZvRKkVEnKKjcxjNzidomtzrkHO[6], IZvRKkVEnKKjcxjNzidomtzrkHO[5], IZvRKkVEnKKjcxjNzidomtzrkHO[7], P_0, P_1);
							num = -602623173;
							continue;
						}
						int num4;
						if (ReInput.configVars.force4WayHats)
						{
							num = -602623175;
							num4 = num;
						}
						else
						{
							num = -602623171;
							num4 = num;
						}
						continue;
						IL_00df:
						int num5;
						if (num3 < arCmrATnXCKXtigWnqYrXYhIBTW.Length)
						{
							num = -602623169;
							num5 = num;
						}
						else
						{
							num = -602623174;
							num5 = num;
						}
					}
					IL_0151:
					num3 = 0;
					num = -602623172;
					goto IL_0011;
				}
			}

			private void VWgqHxpNUAyjYDHicPAEtgNAiLDA(Button P_0, int P_1, int P_2, int P_3, UpdateLoopType P_4, ControllerDataUpdater P_5)
			{
				//Discarded unreachable code: IL_0155
				if (P_0 == null)
				{
					return;
				}
				while (true)
				{
					int num = -1809845754;
					while (true)
					{
						switch (num ^ -1809845760)
						{
						case 5:
							return;
						case 0:
							break;
						case 6:
							if (P_1 >= 0)
							{
								int num2;
								if (P_1 < P_5.buttonCount)
								{
									num = -1809845757;
									num2 = num;
								}
								else
								{
									num = -1809845755;
									num2 = num;
								}
								continue;
							}
							return;
						case 3:
							if (!P_0.isPressureSensitive)
							{
								if (P_2 >= 0 && P_2 < P_5.buttonCount)
								{
									ref bool reference = ref P_5.buttonValues[P_1];
									reference |= P_5.buttonValues[P_2];
									num = -1809845758;
									continue;
								}
								goto case 2;
							}
							goto case 4;
						case 7:
							num = -1809845759;
							continue;
						case 2:
							if (P_3 >= 0 && P_3 < P_5.buttonCount)
							{
								ref bool reference2 = ref P_5.buttonValues[P_1];
								reference2 |= P_5.buttonValues[P_3];
								num = -1809845753;
								continue;
							}
							goto default;
						case 4:
							P_5.buttonPressureValues[P_1] = MathTools.MaxMagnitude(P_5.buttonPressureValues[P_1], MathTools.MaxMagnitude((P_2 >= 0 && P_2 < P_5.buttonCount) ? P_5.buttonPressureValues[P_2] : 0f, (P_3 >= 0 && P_3 < P_5.buttonCount) ? P_5.buttonPressureValues[P_3] : 0f));
							num = -1809845759;
							continue;
						default:
							P_0.HDNHWHMIgYVNwexHqBXaTGgQOqc(P_4, P_1, P_5);
							return;
						}
						break;
					}
				}
			}

			private void GjoCKnHlOzikPnxEGiAmSbZenvmC(Button P_0, int P_1, UpdateLoopType P_2, ControllerDataUpdater P_3)
			{
				//Discarded unreachable code: IL_0047
				if (P_0 == null || P_1 < 0)
				{
					return;
				}
				while (true)
				{
					int num = -331314666;
					while (true)
					{
						switch (num ^ -331314672)
						{
						default:
							return;
						case 1:
							return;
						case 3:
							return;
						case 4:
							break;
						case 5:
							P_0.HDNHWHMIgYVNwexHqBXaTGgQOqc(P_2, P_1, P_3);
							num = -331314669;
							continue;
						case 2:
							if (!P_0.isPressureSensitive)
							{
								P_3.buttonValues[P_1] = false;
								num = -331314667;
								continue;
							}
							goto case 0;
						case 0:
							P_3.buttonPressureValues[P_1] = 0f;
							num = -331314667;
							continue;
						case 6:
						{
							int num2;
							if (P_1 >= P_3.buttonCount)
							{
								num = -331314671;
								num2 = num;
							}
							else
							{
								num = -331314670;
								num2 = num;
							}
							continue;
						}
						}
						break;
					}
				}
			}
		}

		[CustomClassObfuscation(renamePubIntMembers = false, renamePrivateMembers = true)]
		public abstract class Extension
		{
			private Controller SzlUIkTCuzzKtIpFottQRnFTfsRF;

			private IControllerExtensionSource ClGctWbuvINLouElGTGLTPPBaaq;

			internal readonly int _reInputId;

			internal bool isJoystickConnected
			{
				get
				{
					if (SzlUIkTCuzzKtIpFottQRnFTfsRF == null)
					{
						return false;
					}
					return SzlUIkTCuzzKtIpFottQRnFTfsRF._isConnected;
				}
			}

			internal bool enabled
			{
				get
				{
					if (SzlUIkTCuzzKtIpFottQRnFTfsRF == null)
					{
						return false;
					}
					return SzlUIkTCuzzKtIpFottQRnFTfsRF.enabled;
				}
			}

			internal Controller controller => SzlUIkTCuzzKtIpFottQRnFTfsRF;

			internal Extension(IControllerExtensionSource source)
			{
				_reInputId = ReInput.id;
				wHgcBEckBrrsBWAbtVkpHhkeRXJ(source);
			}

			internal Extension(Extension source)
				: this(source.ClGctWbuvINLouElGTGLTPPBaaq)
			{
				SzlUIkTCuzzKtIpFottQRnFTfsRF = source.SzlUIkTCuzzKtIpFottQRnFTfsRF;
			}

			internal T GetController<T>() where T : Controller
			{
				if (SzlUIkTCuzzKtIpFottQRnFTfsRF == null)
				{
					return null;
				}
				return SzlUIkTCuzzKtIpFottQRnFTfsRF as T;
			}

			internal void SetController(Controller controller)
			{
				SzlUIkTCuzzKtIpFottQRnFTfsRF = controller;
			}

			[CustomObfuscation(rename = false)]
			internal IControllerExtensionSource GetSource()
			{
				return ClGctWbuvINLouElGTGLTPPBaaq;
			}

			internal void SetSource(Extension extension)
			{
				if (extension == null)
				{
					wHgcBEckBrrsBWAbtVkpHhkeRXJ(null);
				}
				else
				{
					wHgcBEckBrrsBWAbtVkpHhkeRXJ(extension.ClGctWbuvINLouElGTGLTPPBaaq);
				}
			}

			private void wHgcBEckBrrsBWAbtVkpHhkeRXJ(IControllerExtensionSource P_0)
			{
				ClGctWbuvINLouElGTGLTPPBaaq = P_0;
				SourceUpdated(ClGctWbuvINLouElGTGLTPPBaaq);
			}

			internal virtual void Clear()
			{
			}

			internal abstract void SourceUpdated(IControllerExtensionSource source);

			internal abstract void UpdateData(UpdateLoopType updateLoop);

			internal abstract Extension Clone();
		}

		public readonly int id;

		protected string _tag;

		protected string _name;

		protected string _hardwareName;

		protected readonly ControllerType _type;

		internal readonly Guid JfJqoYpfveStRCmJcRnLNTqrRwm;

		protected string _hardwareIdentifier;

		protected bool _isConnected;

		private Extension URrJKvNlsbObHZfzAftSOFHVrKY;

		private bool KfbFsZTwJuhYvyDZkVBtGMGpvXu;

		internal int aYVuMCKfoBJZlPKnMohBfcrinAp;

		protected readonly int _buttonCount;

		protected readonly Button[] buttons;

		protected readonly ReadOnlyCollection<Button> buttons_readOnly;

		private readonly IList<Element> BARfPlkouALNzdMLqPxStuuAallA;

		private readonly ReadOnlyCollection<Element> vGCOPgDAmnhLDaHDxDThyMqWgUAi;

		internal readonly InputSource PYJEFZJkvCatEnFRTCgEXYpsFLn;

		internal readonly ControllerDataUpdater fHeaxilTulwRIDeBNoWGpOscOuo;

		internal readonly HardwareControllerMap_Game IhNcOIksTzwhmmDhNHMZlLUdqwH;

		internal uint YDBiUEDKGnzkbOhjhvjzpjdDtIDh;

		private uint QgBoEtUpygGDScUaKEAmYnvlCfT;

		private uint ZdLAOjmMOSVqdkltzYJKzGScwkN;

		private Action<bool> WlWcgTHLbZSJaqIGqDBsamLJHPZe;

		private IControllerTemplate[] lUppgxXKJNRAqTCwzcJnBBYsgtW;

		private ReadOnlyCollection<IControllerTemplate> rubrBaVaCxdIwvuRgyLzRjjQJSp;

		private static Func<Controller, Guid, bool> QBCfgkELViqmqsFIOOCzMksbrlGI;

		private static Func<Controller, Type, bool> JswNBtCMRBaKpfAfwbhNdQDlVnv;

		[CompilerGenerated]
		private static Func<Controller, Guid, bool> HOywQdbGwAOfnrlmOdCOjRktwEmO;

		[CompilerGenerated]
		private static Func<Controller, Type, bool> AEBNrXoHgwFXfvZFAKMVJjxEwml;

		internal bool wasPollingPrev => QgBoEtUpygGDScUaKEAmYnvlCfT == ReInput.previousFrame;

		public bool enabled
		{
			get
			{
				if (ReInput._id != aYVuMCKfoBJZlPKnMohBfcrinAp)
				{
					ReInput.CheckInitialized(aYVuMCKfoBJZlPKnMohBfcrinAp);
					return false;
				}
				return KfbFsZTwJuhYvyDZkVBtGMGpvXu;
			}
			set
			{
				SetEnabled(value);
			}
		}

		public string name
		{
			get
			{
				if (ReInput._id != aYVuMCKfoBJZlPKnMohBfcrinAp)
				{
					ReInput.CheckInitialized(aYVuMCKfoBJZlPKnMohBfcrinAp);
					return string.Empty;
				}
				return _name;
			}
			internal set
			{
				_name = value;
			}
		}

		public string tag
		{
			get
			{
				if (ReInput._id != aYVuMCKfoBJZlPKnMohBfcrinAp)
				{
					ReInput.CheckInitialized(aYVuMCKfoBJZlPKnMohBfcrinAp);
					return string.Empty;
				}
				return _tag;
			}
			set
			{
				if (ReInput._id != aYVuMCKfoBJZlPKnMohBfcrinAp)
				{
					ReInput.CheckInitialized(aYVuMCKfoBJZlPKnMohBfcrinAp);
				}
				else
				{
					_tag = value;
				}
			}
		}

		public string hardwareName
		{
			get
			{
				if (ReInput._id != aYVuMCKfoBJZlPKnMohBfcrinAp)
				{
					ReInput.CheckInitialized(aYVuMCKfoBJZlPKnMohBfcrinAp);
					return string.Empty;
				}
				return _hardwareName;
			}
		}

		public ControllerType type
		{
			get
			{
				if (ReInput._id != aYVuMCKfoBJZlPKnMohBfcrinAp)
				{
					ReInput.CheckInitialized(aYVuMCKfoBJZlPKnMohBfcrinAp);
					return ControllerType.Keyboard;
				}
				return _type;
			}
		}

		public Guid hardwareTypeGuid
		{
			get
			{
				if (ReInput._id != aYVuMCKfoBJZlPKnMohBfcrinAp)
				{
					ReInput.CheckInitialized(aYVuMCKfoBJZlPKnMohBfcrinAp);
					return Guid.Empty;
				}
				return JfJqoYpfveStRCmJcRnLNTqrRwm;
			}
		}

		public abstract Guid deviceInstanceGuid
		{
			get;
		}

		public bool isConnected
		{
			get
			{
				if (ReInput._id != aYVuMCKfoBJZlPKnMohBfcrinAp)
				{
					ReInput.CheckInitialized(aYVuMCKfoBJZlPKnMohBfcrinAp);
					return false;
				}
				return _isConnected;
			}
			internal set
			{
				//Discarded unreachable code: IL_004a, IL_0073
				if (ReInput._id != aYVuMCKfoBJZlPKnMohBfcrinAp)
				{
					ReInput.CheckInitialized(aYVuMCKfoBJZlPKnMohBfcrinAp);
					goto IL_0019;
				}
				goto IL_0051;
				IL_0019:
				int num = -1540970202;
				goto IL_001e;
				IL_0051:
				int num2;
				if (!value)
				{
					num = -1540970205;
					num2 = num;
				}
				else
				{
					num = -1540970208;
					num2 = num;
				}
				goto IL_001e;
				IL_001e:
				while (true)
				{
					switch (num ^ -1540970205)
					{
					default:
						return;
					case 1:
						return;
					case 5:
						return;
					case 2:
						break;
					case 0:
						Disconnected();
						return;
					case 4:
						goto IL_0051;
					case 3:
						Connected();
						num = -1540970206;
						continue;
					}
					break;
				}
				goto IL_0019;
			}
		}

		public string hardwareIdentifier
		{
			get
			{
				if (ReInput._id != aYVuMCKfoBJZlPKnMohBfcrinAp)
				{
					ReInput.CheckInitialized(aYVuMCKfoBJZlPKnMohBfcrinAp);
					return string.Empty;
				}
				return _hardwareIdentifier;
			}
		}

		public string mapTypeString => _type.ToString() + "Map";

		public int elementCount
		{
			get
			{
				if (ReInput._id != aYVuMCKfoBJZlPKnMohBfcrinAp)
				{
					ReInput.CheckInitialized(aYVuMCKfoBJZlPKnMohBfcrinAp);
					return 0;
				}
				return BARfPlkouALNzdMLqPxStuuAallA.Count;
			}
		}

		public int buttonCount
		{
			get
			{
				if (ReInput._id != aYVuMCKfoBJZlPKnMohBfcrinAp)
				{
					ReInput.CheckInitialized(aYVuMCKfoBJZlPKnMohBfcrinAp);
					return 0;
				}
				return _buttonCount;
			}
		}

		public IList<Element> Elements
		{
			get
			{
				if (ReInput._id != aYVuMCKfoBJZlPKnMohBfcrinAp)
				{
					ReInput.CheckInitialized(aYVuMCKfoBJZlPKnMohBfcrinAp);
					return EmptyObjects<Element>.EmptyReadOnlyIListT;
				}
				return vGCOPgDAmnhLDaHDxDThyMqWgUAi;
			}
		}

		public IList<Button> Buttons
		{
			get
			{
				if (ReInput._id != aYVuMCKfoBJZlPKnMohBfcrinAp)
				{
					ReInput.CheckInitialized(aYVuMCKfoBJZlPKnMohBfcrinAp);
					return EmptyObjects<Button>.EmptyReadOnlyIListT;
				}
				return buttons_readOnly;
			}
		}

		public Extension extension
		{
			get
			{
				if (ReInput._id != aYVuMCKfoBJZlPKnMohBfcrinAp)
				{
					ReInput.CheckInitialized(aYVuMCKfoBJZlPKnMohBfcrinAp);
					return null;
				}
				return URrJKvNlsbObHZfzAftSOFHVrKY;
			}
		}

		public IList<ControllerElementIdentifier> ElementIdentifiers
		{
			get
			{
				if (ReInput._id != aYVuMCKfoBJZlPKnMohBfcrinAp)
				{
					ReInput.CheckInitialized(aYVuMCKfoBJZlPKnMohBfcrinAp);
					return EmptyObjects<ControllerElementIdentifier>.EmptyReadOnlyIListT;
				}
				return IhNcOIksTzwhmmDhNHMZlLUdqwH.elementIdentifiers_readOnly;
			}
		}

		public IList<ControllerElementIdentifier> ButtonElementIdentifiers
		{
			get
			{
				if (ReInput._id != aYVuMCKfoBJZlPKnMohBfcrinAp)
				{
					ReInput.CheckInitialized(aYVuMCKfoBJZlPKnMohBfcrinAp);
					return EmptyObjects<ControllerElementIdentifier>.EmptyReadOnlyIListT;
				}
				return IhNcOIksTzwhmmDhNHMZlLUdqwH.buttonElementIdentifiers_readOnly;
			}
		}

		public IList<IControllerTemplate> Templates
		{
			get
			{
				if (ReInput._id != aYVuMCKfoBJZlPKnMohBfcrinAp)
				{
					ReInput.CheckInitialized(aYVuMCKfoBJZlPKnMohBfcrinAp);
					return EmptyObjects<IControllerTemplate>.EmptyReadOnlyIListT;
				}
				return rubrBaVaCxdIwvuRgyLzRjjQJSp;
			}
		}

		public int templateCount
		{
			get
			{
				if (ReInput._id != aYVuMCKfoBJZlPKnMohBfcrinAp)
				{
					ReInput.CheckInitialized(aYVuMCKfoBJZlPKnMohBfcrinAp);
					return 0;
				}
				return lUppgxXKJNRAqTCwzcJnBBYsgtW.Length;
			}
		}

		internal static Func<Controller, Guid, bool> implementsTemplateDelegate_Guid => (Controller P_0, Guid P_1) => P_0.ImplementsTemplate(P_1);

		internal static Func<Controller, Type, bool> implementsTemplateDelegate_Type
		{
			get
			{
				Func<Controller, Type, bool> func = JswNBtCMRBaKpfAfwbhNdQDlVnv;
				if (func == null)
				{
					while (true)
					{
						int num = 34331718;
						while (true)
						{
							switch (num ^ 0x20BDC47)
							{
							case 0:
								break;
							case 1:
								if (AEBNrXoHgwFXfvZFAKMVJjxEwml == null)
								{
									AEBNrXoHgwFXfvZFAKMVJjxEwml = ((Controller P_0, Type P_1) => P_0.ImplementsTemplate(P_1));
									num = 34331717;
									continue;
								}
								goto end_IL_0009;
							default:
								goto end_IL_0009;
							}
							break;
						}
						continue;
						end_IL_0009:
						break;
					}
					func = (JswNBtCMRBaKpfAfwbhNdQDlVnv = AEBNrXoHgwFXfvZFAKMVJjxEwml);
				}
				return func;
			}
		}

		internal event Action<bool> EnabledStateChangedEvent
		{
			add
			{
				WlWcgTHLbZSJaqIGqDBsamLJHPZe = (Action<bool>)Delegate.Combine(WlWcgTHLbZSJaqIGqDBsamLJHPZe, value);
			}
			remove
			{
				WlWcgTHLbZSJaqIGqDBsamLJHPZe = (Action<bool>)Delegate.Remove(WlWcgTHLbZSJaqIGqDBsamLJHPZe, value);
			}
		}

		internal Controller(int controllerId, InputSource inputSource, string name, string hardwareName, string hardwareIdentifier, ControllerType type, Guid hardwareTypeGuid, int buttonCount, bool[] isButtonPressureSensitive, HardwareButtonInfo[] hwButtonInfo, HardwareControllerMap_Game hardwareMap, Extension extension, ControllerDataUpdater dataUpdater)
		{
			id = controllerId;
			PYJEFZJkvCatEnFRTCgEXYpsFLn = inputSource;
			_type = type;
			JfJqoYpfveStRCmJcRnLNTqrRwm = hardwareTypeGuid;
			_buttonCount = buttonCount;
			_name = name;
			_hardwareName = hardwareName;
			_hardwareIdentifier = hardwareIdentifier;
			fHeaxilTulwRIDeBNoWGpOscOuo = dataUpdater;
			IhNcOIksTzwhmmDhNHMZlLUdqwH = hardwareMap;
			KfbFsZTwJuhYvyDZkVBtGMGpvXu = true;
			aYVuMCKfoBJZlPKnMohBfcrinAp = ReInput.id;
			bmQbAQActXhqwBPqEbTQZEOfcqHF(extension);
			BARfPlkouALNzdMLqPxStuuAallA = new List<Element>(buttonCount);
			vGCOPgDAmnhLDaHDxDThyMqWgUAi = new ReadOnlyCollection<Element>(BARfPlkouALNzdMLqPxStuuAallA);
			buttons = new Button[buttonCount];
			if (isButtonPressureSensitive == null || isButtonPressureSensitive.Length < buttonCount)
			{
				for (int i = 0; i < buttonCount; i++)
				{
					buttons[i] = new Button(this, hardwareMap.buttonElementIdentifierIds[i], "Button " + i, isPressureSensitive: false, (hwButtonInfo != null) ? hwButtonInfo[i] : new HardwareButtonInfo());
					htWbfivVfubrLgJDGOktUnLLIDLq(buttons[i]);
				}
			}
			else
			{
				for (int j = 0; j < buttonCount; j++)
				{
					buttons[j] = new Button(this, hardwareMap.buttonElementIdentifierIds[j], "Button " + j, isButtonPressureSensitive[j], (hwButtonInfo != null) ? hwButtonInfo[j] : new HardwareButtonInfo());
					htWbfivVfubrLgJDGOktUnLLIDLq(buttons[j]);
				}
			}
			buttons_readOnly = new ReadOnlyCollection<Button>(buttons);
			lUppgxXKJNRAqTCwzcJnBBYsgtW = EmptyObjects<IControllerTemplate>.array;
			rubrBaVaCxdIwvuRgyLzRjjQJSp = new ReadOnlyCollection<IControllerTemplate>(lUppgxXKJNRAqTCwzcJnBBYsgtW);
			Connected();
		}

		public virtual Element GetElementById(int elementIdentifierId)
		{
			if (ReInput._id != aYVuMCKfoBJZlPKnMohBfcrinAp)
			{
				ReInput.CheckInitialized(aYVuMCKfoBJZlPKnMohBfcrinAp);
				return null;
			}
			if (IhNcOIksTzwhmmDhNHMZlLUdqwH == null)
			{
				return null;
			}
			int buttonIndex = IhNcOIksTzwhmmDhNHMZlLUdqwH.GetButtonIndex(elementIdentifierId);
			if (buttonIndex < 0)
			{
				return null;
			}
			return buttons[buttonIndex];
		}

		public int GetButtonIndexById(int elementIdentifierId)
		{
			if (ReInput._id != aYVuMCKfoBJZlPKnMohBfcrinAp)
			{
				ReInput.CheckInitialized(aYVuMCKfoBJZlPKnMohBfcrinAp);
				return -1;
			}
			return IhNcOIksTzwhmmDhNHMZlLUdqwH.GetButtonIndex(elementIdentifierId);
		}

		public ControllerElementIdentifier GetElementIdentifierById(int elementIdentifierId)
		{
			if (ReInput._id != aYVuMCKfoBJZlPKnMohBfcrinAp)
			{
				ReInput.CheckInitialized(aYVuMCKfoBJZlPKnMohBfcrinAp);
				return null;
			}
			return IhNcOIksTzwhmmDhNHMZlLUdqwH.GetElementIdentifierById(elementIdentifierId);
		}

		public virtual bool GetButton(int index)
		{
			if (ReInput._id != aYVuMCKfoBJZlPKnMohBfcrinAp)
			{
				ReInput.CheckInitialized(aYVuMCKfoBJZlPKnMohBfcrinAp);
				goto IL_0019;
			}
			int num;
			if (index >= 0)
			{
				if (index >= _buttonCount)
				{
					num = 561179950;
					goto IL_001e;
				}
				return buttons[index].value;
			}
			goto IL_004d;
			IL_0019:
			num = 561179949;
			goto IL_001e;
			IL_001e:
			switch (num ^ 0x2172ED2C)
			{
			case 0:
				break;
			case 1:
				return false;
			default:
				goto IL_004d;
			}
			goto IL_0019;
			IL_004d:
			return false;
		}

		public virtual bool GetButtonDown(int index)
		{
			if (ReInput._id != aYVuMCKfoBJZlPKnMohBfcrinAp)
			{
				ReInput.CheckInitialized(aYVuMCKfoBJZlPKnMohBfcrinAp);
				return false;
			}
			if (index < 0 || index >= _buttonCount)
			{
				return false;
			}
			return buttons[index].justPressed;
		}

		public virtual bool GetButtonUp(int index)
		{
			if (ReInput._id != aYVuMCKfoBJZlPKnMohBfcrinAp)
			{
				goto IL_000d;
			}
			int num;
			if (index >= 0)
			{
				if (index >= _buttonCount)
				{
					num = -112208448;
					goto IL_0012;
				}
				return buttons[index].justReleased;
			}
			goto IL_004d;
			IL_0012:
			switch (num ^ -112208448)
			{
			case 2:
				break;
			case 1:
				ReInput.CheckInitialized(aYVuMCKfoBJZlPKnMohBfcrinAp);
				return false;
			default:
				goto IL_004d;
			}
			goto IL_000d;
			IL_000d:
			num = -112208447;
			goto IL_0012;
			IL_004d:
			return false;
		}

		public virtual bool GetButtonChanged(int index)
		{
			if (ReInput._id != aYVuMCKfoBJZlPKnMohBfcrinAp)
			{
				ReInput.CheckInitialized(aYVuMCKfoBJZlPKnMohBfcrinAp);
				return false;
			}
			if (index >= 0)
			{
				while (true)
				{
					int num = -122209872;
					while (true)
					{
						switch (num ^ -122209871)
						{
						case 2:
							break;
						case 1:
							goto IL_003d;
						default:
							goto IL_004d;
						}
						break;
						IL_003d:
						if (index < _buttonCount)
						{
							return buttons[index].value != buttons[index].valuePrev;
						}
						num = -122209871;
					}
				}
			}
			goto IL_004d;
			IL_004d:
			return false;
		}

		public virtual bool GetButtonPrev(int index)
		{
			if (ReInput._id != aYVuMCKfoBJZlPKnMohBfcrinAp)
			{
				ReInput.CheckInitialized(aYVuMCKfoBJZlPKnMohBfcrinAp);
				return false;
			}
			if (index < 0 || index >= _buttonCount)
			{
				return false;
			}
			return buttons[index].valuePrev;
		}

		public virtual bool GetButtonDoublePressHold(int index, float speed)
		{
			if (ReInput._id != aYVuMCKfoBJZlPKnMohBfcrinAp)
			{
				ReInput.CheckInitialized(aYVuMCKfoBJZlPKnMohBfcrinAp);
				return false;
			}
			if (index >= 0)
			{
				while (true)
				{
					int num = -337694784;
					while (true)
					{
						switch (num ^ -337694783)
						{
						case 0:
							break;
						case 1:
							goto IL_003d;
						default:
							goto IL_004d;
						}
						break;
						IL_003d:
						if (index < _buttonCount)
						{
							return buttons[index].DoublePressedAndHeld(speed);
						}
						num = -337694781;
					}
				}
			}
			goto IL_004d;
			IL_004d:
			return false;
		}

		public virtual bool GetButtonDoublePressDown(int index, float speed)
		{
			if (ReInput._id != aYVuMCKfoBJZlPKnMohBfcrinAp)
			{
				ReInput.CheckInitialized(aYVuMCKfoBJZlPKnMohBfcrinAp);
				return false;
			}
			if (index < 0 || index >= _buttonCount)
			{
				return false;
			}
			return buttons[index].JustDoublePressed(speed);
		}

		public virtual bool GetButtonDoublePressHold(int index)
		{
			if (ReInput._id != aYVuMCKfoBJZlPKnMohBfcrinAp)
			{
				ReInput.CheckInitialized(aYVuMCKfoBJZlPKnMohBfcrinAp);
				return false;
			}
			return GetButtonDoublePressHold(index, 0f);
		}

		public virtual bool GetButtonDoublePressDown(int index)
		{
			if (ReInput._id != aYVuMCKfoBJZlPKnMohBfcrinAp)
			{
				ReInput.CheckInitialized(aYVuMCKfoBJZlPKnMohBfcrinAp);
				return false;
			}
			return GetButtonDoublePressDown(index, 0f);
		}

		public virtual float GetButtonTimePressed(int index)
		{
			if (ReInput._id != aYVuMCKfoBJZlPKnMohBfcrinAp)
			{
				ReInput.CheckInitialized(aYVuMCKfoBJZlPKnMohBfcrinAp);
				return 0f;
			}
			if (index >= 0)
			{
				while (true)
				{
					int num = -1738039473;
					while (true)
					{
						switch (num ^ -1738039474)
						{
						case 0:
							break;
						case 1:
							goto IL_0041;
						default:
							goto IL_0051;
						}
						break;
						IL_0041:
						if (index < _buttonCount)
						{
							return buttons[index].timePressed;
						}
						num = -1738039476;
					}
				}
			}
			goto IL_0051;
			IL_0051:
			return 0f;
		}

		public virtual float GetButtonTimeUnpressed(int index)
		{
			if (ReInput._id != aYVuMCKfoBJZlPKnMohBfcrinAp)
			{
				ReInput.CheckInitialized(aYVuMCKfoBJZlPKnMohBfcrinAp);
				return 0f;
			}
			if (index < 0 || index >= _buttonCount)
			{
				return 0f;
			}
			return buttons[index].timeUnpressed;
		}

		public virtual float GetButtonLastTimePressed(int index)
		{
			if (ReInput._id != aYVuMCKfoBJZlPKnMohBfcrinAp)
			{
				goto IL_000d;
			}
			int num;
			if (index >= 0)
			{
				if (index >= _buttonCount)
				{
					num = 1898659031;
					goto IL_0012;
				}
				return buttons[index].lastTimePressed;
			}
			goto IL_0051;
			IL_0012:
			switch (num ^ 0x712B3CD6)
			{
			case 0:
				break;
			case 2:
				ReInput.CheckInitialized(aYVuMCKfoBJZlPKnMohBfcrinAp);
				return 0f;
			default:
				goto IL_0051;
			}
			goto IL_000d;
			IL_0051:
			return 0f;
			IL_000d:
			num = 1898659028;
			goto IL_0012;
		}

		public virtual float GetButtonLastTimeUnpressed(int index)
		{
			if (ReInput._id != aYVuMCKfoBJZlPKnMohBfcrinAp)
			{
				ReInput.CheckInitialized(aYVuMCKfoBJZlPKnMohBfcrinAp);
				goto IL_0019;
			}
			int num;
			if (index >= 0)
			{
				if (index >= _buttonCount)
				{
					num = 425600485;
					goto IL_001e;
				}
				return buttons[index].lastTimeUnpressed;
			}
			goto IL_0051;
			IL_0019:
			num = 425600486;
			goto IL_001e;
			IL_001e:
			switch (num ^ 0x195E25E4)
			{
			case 0:
				break;
			case 2:
				return 0f;
			default:
				goto IL_0051;
			}
			goto IL_0019;
			IL_0051:
			return 0f;
		}

		public virtual bool GetAnyButton()
		{
			if (ReInput._id != aYVuMCKfoBJZlPKnMohBfcrinAp)
			{
				ReInput.CheckInitialized(aYVuMCKfoBJZlPKnMohBfcrinAp);
				return false;
			}
			int num = 0;
			while (true)
			{
				int num2 = -1368398597;
				while (true)
				{
					switch (num2 ^ -1368398598)
					{
					case 4:
						break;
					case 0:
						return true;
					case 2:
						if (!buttons[num].value)
						{
							num++;
							num2 = -1368398593;
						}
						else
						{
							num2 = -1368398598;
						}
						continue;
					case 5:
					{
						int num3;
						if (num < _buttonCount)
						{
							num2 = -1368398600;
							num3 = num2;
						}
						else
						{
							num2 = -1368398599;
							num3 = num2;
						}
						continue;
					}
					case 1:
						num2 = -1368398593;
						continue;
					default:
						return false;
					}
					break;
				}
			}
		}

		public virtual bool GetAnyButtonDown()
		{
			if (ReInput._id != aYVuMCKfoBJZlPKnMohBfcrinAp)
			{
				goto IL_000d;
			}
			int num = 0;
			int num2 = 543996652;
			goto IL_0012;
			IL_000d:
			num2 = 543996654;
			goto IL_0012;
			IL_0012:
			while (true)
			{
				switch (num2 ^ 0x206CBAED)
				{
				case 2:
					break;
				case 4:
					if (buttons[num].justPressed)
					{
						return true;
					}
					num++;
					num2 = 543996652;
					continue;
				case 0:
					return false;
				case 3:
					ReInput.CheckInitialized(aYVuMCKfoBJZlPKnMohBfcrinAp);
					num2 = 543996653;
					continue;
				default:
					if (num >= _buttonCount)
					{
						return false;
					}
					goto case 4;
				}
				break;
			}
			goto IL_000d;
		}

		public virtual bool GetAnyButtonUp()
		{
			if (ReInput._id != aYVuMCKfoBJZlPKnMohBfcrinAp)
			{
				ReInput.CheckInitialized(aYVuMCKfoBJZlPKnMohBfcrinAp);
				return false;
			}
			int num = 0;
			while (num < _buttonCount)
			{
				while (true)
				{
					int num2;
					if (buttons[num].justReleased)
					{
						num2 = 198003835;
					}
					else
					{
						num++;
						num2 = 198003832;
					}
					while (true)
					{
						switch (num2 ^ 0xBCD4C7A)
						{
						case 0:
							num2 = 198003833;
							continue;
						case 3:
							break;
						case 1:
							return true;
						default:
							goto IL_0064;
						}
						break;
					}
				}
				IL_0064:;
			}
			return false;
		}

		public virtual bool GetAnyButtonPrev()
		{
			if (ReInput._id != aYVuMCKfoBJZlPKnMohBfcrinAp)
			{
				goto IL_000d;
			}
			int num = 0;
			int num2 = 595100258;
			goto IL_0012;
			IL_000d:
			num2 = 595100261;
			goto IL_0012;
			IL_0012:
			while (true)
			{
				switch (num2 ^ 0x23788266)
				{
				case 0:
					break;
				case 3:
					ReInput.CheckInitialized(aYVuMCKfoBJZlPKnMohBfcrinAp);
					return false;
				case 2:
					if (buttons[num].valuePrev)
					{
						num2 = 595100263;
						continue;
					}
					num++;
					num2 = 595100258;
					continue;
				case 1:
					return true;
				default:
					if (num >= _buttonCount)
					{
						return false;
					}
					goto case 2;
				}
				break;
			}
			goto IL_000d;
		}

		public virtual bool GetAnyButtonChanged()
		{
			if (ReInput._id != aYVuMCKfoBJZlPKnMohBfcrinAp)
			{
				goto IL_000d;
			}
			int num = 0;
			int num2 = -1198382678;
			goto IL_0012;
			IL_000d:
			num2 = -1198382676;
			goto IL_0012;
			IL_0012:
			while (true)
			{
				switch (num2 ^ -1198382674)
				{
				case 0:
					break;
				case 1:
					if (buttons[num].justChangedState)
					{
						return true;
					}
					num++;
					num2 = -1198382675;
					continue;
				case 4:
					num2 = -1198382675;
					continue;
				case 2:
					ReInput.CheckInitialized(aYVuMCKfoBJZlPKnMohBfcrinAp);
					return false;
				default:
					if (num >= _buttonCount)
					{
						return false;
					}
					goto case 1;
				}
				break;
			}
			goto IL_000d;
		}

		public virtual bool GetButtonById(int elementIdentifierId)
		{
			if (ReInput._id != aYVuMCKfoBJZlPKnMohBfcrinAp)
			{
				goto IL_000d;
			}
			int buttonIndex = IhNcOIksTzwhmmDhNHMZlLUdqwH.GetButtonIndex(elementIdentifierId);
			int num;
			int num2;
			if (buttonIndex >= 0)
			{
				num = -505124095;
				num2 = num;
			}
			else
			{
				num = -505124094;
				num2 = num;
			}
			goto IL_0012;
			IL_000d:
			num = -505124093;
			goto IL_0012;
			IL_0012:
			while (true)
			{
				switch (num ^ -505124094)
				{
				case 2:
					break;
				case 1:
					ReInput.CheckInitialized(aYVuMCKfoBJZlPKnMohBfcrinAp);
					return false;
				case 3:
					if (buttonIndex >= _buttonCount)
					{
						goto IL_0068;
					}
					return buttons[buttonIndex].value;
				default:
					return false;
				}
				break;
				IL_0068:
				num = -505124094;
			}
			goto IL_000d;
		}

		public virtual bool GetButtonDownById(int elementIdentifierId)
		{
			if (ReInput._id != aYVuMCKfoBJZlPKnMohBfcrinAp)
			{
				ReInput.CheckInitialized(aYVuMCKfoBJZlPKnMohBfcrinAp);
				return false;
			}
			int buttonIndex = IhNcOIksTzwhmmDhNHMZlLUdqwH.GetButtonIndex(elementIdentifierId);
			while (true)
			{
				int num = -801436260;
				while (true)
				{
					switch (num ^ -801436259)
					{
					case 0:
						break;
					case 1:
					{
						int num2;
						if (buttonIndex < 0)
						{
							num = -801436257;
							num2 = num;
						}
						else
						{
							num = -801436258;
							num2 = num;
						}
						continue;
					}
					case 3:
						if (buttonIndex >= _buttonCount)
						{
							num = -801436257;
							continue;
						}
						return buttons[buttonIndex].justPressed;
					default:
						return false;
					}
					break;
				}
			}
		}

		public virtual bool GetButtonUpById(int elementIdentifierId)
		{
			if (ReInput._id != aYVuMCKfoBJZlPKnMohBfcrinAp)
			{
				ReInput.CheckInitialized(aYVuMCKfoBJZlPKnMohBfcrinAp);
				return false;
			}
			int buttonIndex = IhNcOIksTzwhmmDhNHMZlLUdqwH.GetButtonIndex(elementIdentifierId);
			if (buttonIndex < 0 || buttonIndex >= _buttonCount)
			{
				return false;
			}
			return buttons[buttonIndex].justReleased;
		}

		public virtual bool GetButtonDoublePressHoldById(int elementIdentifierId, float speed)
		{
			if (ReInput._id != aYVuMCKfoBJZlPKnMohBfcrinAp)
			{
				ReInput.CheckInitialized(aYVuMCKfoBJZlPKnMohBfcrinAp);
				goto IL_0019;
			}
			int buttonIndex = IhNcOIksTzwhmmDhNHMZlLUdqwH.GetButtonIndex(elementIdentifierId);
			int num;
			int num2;
			if (buttonIndex >= 0)
			{
				num = -1541093097;
				num2 = num;
			}
			else
			{
				num = -1541093099;
				num2 = num;
			}
			goto IL_001e;
			IL_001e:
			while (true)
			{
				switch (num ^ -1541093097)
				{
				case 3:
					break;
				case 1:
					return false;
				case 0:
					if (buttonIndex >= _buttonCount)
					{
						goto IL_0068;
					}
					return buttons[buttonIndex].DoublePressedAndHeld(speed);
				default:
					return false;
				}
				break;
				IL_0068:
				num = -1541093099;
			}
			goto IL_0019;
			IL_0019:
			num = -1541093098;
			goto IL_001e;
		}

		public virtual bool GetButtonDoublePressDownById(int elementIdentifierId, float speed)
		{
			if (ReInput._id != aYVuMCKfoBJZlPKnMohBfcrinAp)
			{
				goto IL_000d;
			}
			int buttonIndex = IhNcOIksTzwhmmDhNHMZlLUdqwH.GetButtonIndex(elementIdentifierId);
			int num;
			if (buttonIndex >= 0)
			{
				if (buttonIndex >= _buttonCount)
				{
					num = -2068980977;
					goto IL_0012;
				}
				return buttons[buttonIndex].JustDoublePressed(speed);
			}
			goto IL_005a;
			IL_0012:
			switch (num ^ -2068980978)
			{
			case 0:
				break;
			case 2:
				ReInput.CheckInitialized(aYVuMCKfoBJZlPKnMohBfcrinAp);
				return false;
			default:
				goto IL_005a;
			}
			goto IL_000d;
			IL_005a:
			return false;
			IL_000d:
			num = -2068980980;
			goto IL_0012;
		}

		public virtual bool GetButtonDoublePressHoldById(int elementIdentifierId)
		{
			if (ReInput._id != aYVuMCKfoBJZlPKnMohBfcrinAp)
			{
				ReInput.CheckInitialized(aYVuMCKfoBJZlPKnMohBfcrinAp);
				return false;
			}
			int buttonIndex = IhNcOIksTzwhmmDhNHMZlLUdqwH.GetButtonIndex(elementIdentifierId);
			return GetButtonDoublePressHold(buttonIndex, 0f);
		}

		public virtual bool GetButtonDoublePressDownById(int elementIdentifierId)
		{
			if (ReInput._id != aYVuMCKfoBJZlPKnMohBfcrinAp)
			{
				while (true)
				{
					int num = 1332542143;
					while (true)
					{
						switch (num ^ 0x4F6CFABE)
						{
						case 0:
							break;
						case 1:
							goto IL_002b;
						default:
							return false;
						}
						break;
						IL_002b:
						ReInput.CheckInitialized(aYVuMCKfoBJZlPKnMohBfcrinAp);
						num = 1332542140;
					}
				}
			}
			int buttonIndex = IhNcOIksTzwhmmDhNHMZlLUdqwH.GetButtonIndex(elementIdentifierId);
			return GetButtonDoublePressDown(buttonIndex, 0f);
		}

		public virtual bool GetButtonPrevById(int elementIdentifierId)
		{
			if (ReInput._id != aYVuMCKfoBJZlPKnMohBfcrinAp)
			{
				goto IL_000d;
			}
			int buttonIndex = IhNcOIksTzwhmmDhNHMZlLUdqwH.GetButtonIndex(elementIdentifierId);
			int num;
			int num2;
			if (buttonIndex < 0)
			{
				num = -495705922;
				num2 = num;
			}
			else
			{
				num = -495705924;
				num2 = num;
			}
			goto IL_0012;
			IL_000d:
			num = -495705923;
			goto IL_0012;
			IL_0012:
			while (true)
			{
				switch (num ^ -495705924)
				{
				case 3:
					break;
				case 1:
					ReInput.CheckInitialized(aYVuMCKfoBJZlPKnMohBfcrinAp);
					return false;
				case 0:
					if (buttonIndex >= _buttonCount)
					{
						goto IL_0068;
					}
					return buttons[buttonIndex].valuePrev;
				default:
					return false;
				}
				break;
				IL_0068:
				num = -495705922;
			}
			goto IL_000d;
		}

		public virtual float GetButtonTimePressedById(int elementIdentifierId)
		{
			if (ReInput._id != aYVuMCKfoBJZlPKnMohBfcrinAp)
			{
				ReInput.CheckInitialized(aYVuMCKfoBJZlPKnMohBfcrinAp);
				return 0f;
			}
			int buttonIndex = IhNcOIksTzwhmmDhNHMZlLUdqwH.GetButtonIndex(elementIdentifierId);
			if (buttonIndex >= 0)
			{
				while (true)
				{
					int num = -1171290714;
					while (true)
					{
						switch (num ^ -1171290713)
						{
						case 0:
							break;
						case 1:
							goto IL_004e;
						default:
							goto IL_005e;
						}
						break;
						IL_004e:
						if (buttonIndex < _buttonCount)
						{
							return buttons[buttonIndex].timePressed;
						}
						num = -1171290715;
					}
				}
			}
			goto IL_005e;
			IL_005e:
			return 0f;
		}

		public virtual float GetButtonTimeUnpressedById(int elementIdentifierId)
		{
			if (ReInput._id != aYVuMCKfoBJZlPKnMohBfcrinAp)
			{
				ReInput.CheckInitialized(aYVuMCKfoBJZlPKnMohBfcrinAp);
				goto IL_0019;
			}
			int buttonIndex = IhNcOIksTzwhmmDhNHMZlLUdqwH.GetButtonIndex(elementIdentifierId);
			int num;
			if (buttonIndex >= 0)
			{
				if (buttonIndex >= _buttonCount)
				{
					num = -1524658634;
					goto IL_001e;
				}
				return buttons[buttonIndex].timeUnpressed;
			}
			goto IL_005e;
			IL_0019:
			num = -1524658635;
			goto IL_001e;
			IL_001e:
			switch (num ^ -1524658633)
			{
			case 0:
				break;
			case 2:
				return 0f;
			default:
				goto IL_005e;
			}
			goto IL_0019;
			IL_005e:
			return 0f;
		}

		public virtual float GetButtonLastTimePressedById(int elementIdentifierId)
		{
			if (ReInput._id != aYVuMCKfoBJZlPKnMohBfcrinAp)
			{
				goto IL_000d;
			}
			int buttonIndex = IhNcOIksTzwhmmDhNHMZlLUdqwH.GetButtonIndex(elementIdentifierId);
			int num;
			if (buttonIndex >= 0)
			{
				if (buttonIndex >= _buttonCount)
				{
					num = 1889274501;
					goto IL_0012;
				}
				return buttons[buttonIndex].lastTimePressed;
			}
			goto IL_0069;
			IL_0012:
			while (true)
			{
				switch (num ^ 0x709C0A86)
				{
				case 0:
					break;
				case 1:
					ReInput.CheckInitialized(aYVuMCKfoBJZlPKnMohBfcrinAp);
					num = 1889274500;
					continue;
				case 2:
					return 0f;
				default:
					goto IL_0069;
				}
				break;
			}
			goto IL_000d;
			IL_0069:
			return 0f;
			IL_000d:
			num = 1889274503;
			goto IL_0012;
		}

		public virtual float GetButtonLastTimeUnpressedById(int elementIdentifierId)
		{
			if (ReInput._id != aYVuMCKfoBJZlPKnMohBfcrinAp)
			{
				ReInput.CheckInitialized(aYVuMCKfoBJZlPKnMohBfcrinAp);
				return 0f;
			}
			int buttonIndex = IhNcOIksTzwhmmDhNHMZlLUdqwH.GetButtonIndex(elementIdentifierId);
			if (buttonIndex < 0 || buttonIndex >= _buttonCount)
			{
				return 0f;
			}
			return buttons[buttonIndex].lastTimeUnpressed;
		}

		public virtual ControllerPollingInfo PollForFirstElement()
		{
			return PollForFirstButton();
		}

		public virtual ControllerPollingInfo PollForFirstElementDown()
		{
			return PollForFirstButtonDown();
		}

		public virtual ControllerPollingInfo PollForFirstButton()
		{
			if (ReInput._id != aYVuMCKfoBJZlPKnMohBfcrinAp)
			{
				goto IL_000d;
			}
			UpdatePollingFrameTracking();
			int num = 920685365;
			goto IL_0012;
			IL_000d:
			num = 920685364;
			goto IL_0012;
			IL_0012:
			int num2 = default(int);
			while (true)
			{
				switch (num ^ 0x36E08B35)
				{
				case 4:
					break;
				case 1:
					ReInput.CheckInitialized(aYVuMCKfoBJZlPKnMohBfcrinAp);
					return ControllerPollingInfo.dTcOQCxVcEnAwvttEXOEIJYkhMR();
				case 0:
					num2 = 0;
					num = 920685367;
					continue;
				case 3:
				{
					if (CwJPnAvsEehqEeHREJbzgiOcCFbz(num2, out int elementIdentifierId))
					{
						return new ControllerPollingInfo(success: true, -1, id, _name, _type, ControllerElementType.Button, num2, Pole.Positive, IhNcOIksTzwhmmDhNHMZlLUdqwH.GetElementIdentifierName(elementIdentifierId), elementIdentifierId, KeyCode.None);
					}
					num2++;
					num = 920685367;
					continue;
				}
				default:
					if (num2 >= _buttonCount)
					{
						return ControllerPollingInfo.dTcOQCxVcEnAwvttEXOEIJYkhMR();
					}
					goto case 3;
				}
				break;
			}
			goto IL_000d;
		}

		public virtual ControllerPollingInfo PollForFirstButtonDown()
		{
			if (ReInput._id != aYVuMCKfoBJZlPKnMohBfcrinAp)
			{
				goto IL_000d;
			}
			UpdatePollingFrameTracking();
			int num = 0;
			int num2 = 839059306;
			goto IL_0012;
			IL_000d:
			num2 = 839059307;
			goto IL_0012;
			IL_0012:
			while (true)
			{
				switch (num2 ^ 0x3203076A)
				{
				case 2:
					break;
				case 1:
					ReInput.CheckInitialized(aYVuMCKfoBJZlPKnMohBfcrinAp);
					return ControllerPollingInfo.dTcOQCxVcEnAwvttEXOEIJYkhMR();
				case 3:
				{
					if (!AWSSreeEbpAwVkOQojpqphbXfYD(num, out int elementIdentifierId))
					{
						goto IL_0086;
					}
					return new ControllerPollingInfo(success: true, -1, id, _name, _type, ControllerElementType.Button, num, Pole.Positive, IhNcOIksTzwhmmDhNHMZlLUdqwH.GetElementIdentifierName(elementIdentifierId), elementIdentifierId, KeyCode.None);
				}
				default:
					if (num >= _buttonCount)
					{
						return ControllerPollingInfo.dTcOQCxVcEnAwvttEXOEIJYkhMR();
					}
					goto case 3;
				}
				break;
				IL_0086:
				num++;
				num2 = 839059306;
			}
			goto IL_000d;
		}

		public virtual IEnumerable<ControllerPollingInfo> PollForAllElements()
		{
			return PollForAllButtons();
		}

		public virtual IEnumerable<ControllerPollingInfo> PollForAllElementsDown()
		{
			return PollForAllButtonsDown();
		}

		public virtual IEnumerable<ControllerPollingInfo> PollForAllButtons()
		{
			int num2 = default(int);
			while (true)
			{
				IL_0084:
				int num;
				if (ReInput._id != aYVuMCKfoBJZlPKnMohBfcrinAp)
				{
					ReInput.CheckInitialized(aYVuMCKfoBJZlPKnMohBfcrinAp);
					num = 505890772;
					goto IL_001f;
				}
				goto IL_006b;
				IL_001f:
				while (true)
				{
					switch (num ^ 0x1E2747D7)
					{
					default:
						yield break;
					case 0:
						num = 505890769;
						continue;
					case 2:
						num2++;
						num = 505890771;
						continue;
					case 7:
						num = 505890771;
						continue;
					case 5:
						break;
					case 6:
						goto IL_0084;
					case 1:
					{
						if (CwJPnAvsEehqEeHREJbzgiOcCFbz(num2, out int elementIdentifierId))
						{
							yield return new ControllerPollingInfo(success: true, -1, id, _name, _type, ControllerElementType.Button, num2, Pole.Positive, IhNcOIksTzwhmmDhNHMZlLUdqwH.GetElementIdentifierName(elementIdentifierId), elementIdentifierId, KeyCode.None);
							num = 505890773;
							continue;
						}
						goto case 2;
					}
					case 4:
						goto IL_0141;
					}
					break;
					IL_0141:
					num = ((num2 < _buttonCount) ? 505890774 : 505890772);
				}
				goto IL_006b;
				IL_006b:
				UpdatePollingFrameTracking();
				num2 = 0;
				num = 505890768;
				goto IL_001f;
			}
		}

		public virtual IEnumerable<ControllerPollingInfo> PollForAllButtonsDown()
		{
			int num2 = default(int);
			while (true)
			{
				int num = 1629452438;
				while (true)
				{
					switch (num ^ 0x611F7892)
					{
					default:
						yield break;
					case 3:
						num = 1629452432;
						continue;
					case 0:
						UpdatePollingFrameTracking();
						num = 1629452437;
						continue;
					case 4:
						if (ReInput._id != aYVuMCKfoBJZlPKnMohBfcrinAp)
						{
							ReInput.CheckInitialized(aYVuMCKfoBJZlPKnMohBfcrinAp);
							num = 1629452439;
							continue;
						}
						goto case 0;
					case 7:
						num2 = 0;
						num = 1629452442;
						continue;
					case 8:
						num = ((num2 >= _buttonCount) ? 1629452439 : 1629452435);
						continue;
					case 1:
					{
						if (AWSSreeEbpAwVkOQojpqphbXfYD(num2, out int elementIdentifierId))
						{
							yield return new ControllerPollingInfo(success: true, -1, id, _name, _type, ControllerElementType.Button, num2, Pole.Positive, IhNcOIksTzwhmmDhNHMZlLUdqwH.GetElementIdentifierName(elementIdentifierId), elementIdentifierId, KeyCode.None);
							num = 1629452436;
							continue;
						}
						goto case 6;
					}
					case 6:
						num2++;
						num = 1629452442;
						continue;
					case 2:
						break;
					}
					break;
				}
			}
		}

		private bool CwJPnAvsEehqEeHREJbzgiOcCFbz(int P_0, out int P_1)
		{
			P_1 = -1;
			while (true)
			{
				int num = -838254675;
				while (true)
				{
					switch (num ^ -838254676)
					{
					case 2:
						break;
					case 1:
						if (!buttons[P_0].value || buttons[P_0].WGLowJGWkhfxfZAOoGBhavBWonj._excludeFromPolling)
						{
							num = -838254676;
							continue;
						}
						P_1 = IhNcOIksTzwhmmDhNHMZlLUdqwH.buttonElementIdentifierIds[P_0];
						num = -838254673;
						continue;
					case 0:
						return false;
					default:
						if (P_1 < 0)
						{
							return false;
						}
						return true;
					}
					break;
				}
			}
		}

		private bool AWSSreeEbpAwVkOQojpqphbXfYD(int P_0, out int P_1)
		{
			P_1 = -1;
			if (!buttons[P_0].justPressed || buttons[P_0].WGLowJGWkhfxfZAOoGBhavBWonj._excludeFromPolling)
			{
				return false;
			}
			P_1 = IhNcOIksTzwhmmDhNHMZlLUdqwH.buttonElementIdentifierIds[P_0];
			if (P_1 < 0)
			{
				return false;
			}
			return true;
		}

		protected void UpdatePollingFrameTracking()
		{
			//Discarded unreachable code: IL_003f, IL_005c
			if (ZdLAOjmMOSVqdkltzYJKzGScwkN == ReInput.currentFrame)
			{
				goto IL_000d;
			}
			goto IL_0063;
			IL_0063:
			QgBoEtUpygGDScUaKEAmYnvlCfT = ZdLAOjmMOSVqdkltzYJKzGScwkN;
			ZdLAOjmMOSVqdkltzYJKzGScwkN = ReInput.currentFrame;
			int num;
			if (!wasPollingPrev)
			{
				int num2;
				if (YDBiUEDKGnzkbOhjhvjzpjdDtIDh == uint.MaxValue)
				{
					num = -490844346;
					num2 = num;
				}
				else
				{
					num = -490844349;
					num2 = num;
				}
				goto IL_0012;
			}
			return;
			IL_000d:
			num = -490844345;
			goto IL_0012;
			IL_0012:
			while (true)
			{
				switch (num ^ -490844346)
				{
				default:
					return;
				case 1:
					return;
				case 3:
					return;
				case 4:
					break;
				case 0:
					YDBiUEDKGnzkbOhjhvjzpjdDtIDh = 0u;
					return;
				case 5:
					YDBiUEDKGnzkbOhjhvjzpjdDtIDh++;
					num = -490844347;
					continue;
				case 2:
					goto IL_0063;
				}
				break;
			}
			goto IL_000d;
		}

		public virtual float GetLastTimeActive()
		{
			if (ReInput._id != aYVuMCKfoBJZlPKnMohBfcrinAp)
			{
				ReInput.CheckInitialized(aYVuMCKfoBJZlPKnMohBfcrinAp);
				return 0f;
			}
			return GetLastTimeActive(useRawValues: false);
		}

		public virtual float GetLastTimeActive(bool useRawValues)
		{
			if (ReInput._id != aYVuMCKfoBJZlPKnMohBfcrinAp)
			{
				ReInput.CheckInitialized(aYVuMCKfoBJZlPKnMohBfcrinAp);
				return 0f;
			}
			return GetLastTimeAnyButtonPressed();
		}

		public virtual float GetLastTimeAnyElementChanged()
		{
			if (ReInput._id != aYVuMCKfoBJZlPKnMohBfcrinAp)
			{
				ReInput.CheckInitialized(aYVuMCKfoBJZlPKnMohBfcrinAp);
				return 0f;
			}
			return GetLastTimeAnyElementChanged(useRawValues: false);
		}

		public virtual float GetLastTimeAnyElementChanged(bool useRawValues)
		{
			if (ReInput._id != aYVuMCKfoBJZlPKnMohBfcrinAp)
			{
				ReInput.CheckInitialized(aYVuMCKfoBJZlPKnMohBfcrinAp);
				return 0f;
			}
			return GetLastTimeAnyButtonChanged();
		}

		public float GetLastTimeAnyButtonPressed()
		{
			if (ReInput._id != aYVuMCKfoBJZlPKnMohBfcrinAp)
			{
				ReInput.CheckInitialized(aYVuMCKfoBJZlPKnMohBfcrinAp);
				goto IL_001c;
			}
			float num = default(float);
			int num2 = default(int);
			int num3;
			if (buttons != null)
			{
				num = 0f;
				num2 = 0;
				num3 = 1407181875;
			}
			else
			{
				num3 = 1407181873;
			}
			goto IL_0021;
			IL_0021:
			float lastTimePressed = default(float);
			while (true)
			{
				switch (num3 ^ 0x53DFE430)
				{
				case 5:
					break;
				case 0:
					lastTimePressed = buttons[num2].lastTimePressed;
					num3 = 1407181878;
					continue;
				case 1:
					return 0f;
				case 4:
					num2++;
					num3 = 1407181875;
					continue;
				case 6:
					if (lastTimePressed > num)
					{
						num = lastTimePressed;
						num3 = 1407181876;
						continue;
					}
					goto case 4;
				case 2:
					return 0f;
				default:
					if (num2 >= buttons.Length)
					{
						return num;
					}
					goto case 0;
				}
				break;
			}
			goto IL_001c;
			IL_001c:
			num3 = 1407181874;
			goto IL_0021;
		}

		public float GetLastTimeAnyButtonChanged()
		{
			if (ReInput._id != aYVuMCKfoBJZlPKnMohBfcrinAp)
			{
				ReInput.CheckInitialized(aYVuMCKfoBJZlPKnMohBfcrinAp);
				return 0f;
			}
			if (buttons == null)
			{
				return 0f;
			}
			float num = 0f;
			int num3 = default(int);
			float lastTimeStateChanged = default(float);
			while (true)
			{
				int num2 = 584154486;
				while (true)
				{
					switch (num2 ^ 0x22D17D73)
					{
					case 3:
						break;
					case 1:
						num3++;
						num2 = 584154481;
						continue;
					case 4:
						lastTimeStateChanged = buttons[num3].lastTimeStateChanged;
						num2 = 584154483;
						continue;
					case 5:
						num3 = 0;
						num2 = 584154481;
						continue;
					case 0:
						if (lastTimeStateChanged > num)
						{
							num = lastTimeStateChanged;
							num2 = 584154482;
							continue;
						}
						goto case 1;
					default:
						if (num3 >= buttons.Length)
						{
							return num;
						}
						goto case 4;
					}
					break;
				}
			}
		}

		public T GetExtension<T>() where T : class
		{
			if (ReInput._id != aYVuMCKfoBJZlPKnMohBfcrinAp)
			{
				ReInput.CheckInitialized(aYVuMCKfoBJZlPKnMohBfcrinAp);
				return null;
			}
			return URrJKvNlsbObHZfzAftSOFHVrKY as T;
		}

		public IControllerTemplate GetTemplate(Guid typeGuid)
		{
			if (ReInput._id != aYVuMCKfoBJZlPKnMohBfcrinAp)
			{
				ReInput.CheckInitialized(aYVuMCKfoBJZlPKnMohBfcrinAp);
				return null;
			}
			int num = 0;
			while (num < lUppgxXKJNRAqTCwzcJnBBYsgtW.Length)
			{
				while (true)
				{
					int num2;
					if (lUppgxXKJNRAqTCwzcJnBBYsgtW[num].typeGuid == typeGuid)
					{
						num2 = -1397826070;
					}
					else
					{
						num++;
						num2 = -1397826072;
					}
					while (true)
					{
						switch (num2 ^ -1397826071)
						{
						case 0:
							num2 = -1397826069;
							continue;
						case 2:
							break;
						case 3:
							return lUppgxXKJNRAqTCwzcJnBBYsgtW[num];
						default:
							goto IL_0071;
						}
						break;
					}
				}
				IL_0071:;
			}
			return null;
		}

		public IControllerTemplate GetTemplate(Type type)
		{
			if (ReInput._id != aYVuMCKfoBJZlPKnMohBfcrinAp)
			{
				goto IL_000d;
			}
			int num = 0;
			int num2 = 54243598;
			goto IL_0012;
			IL_000d:
			num2 = 54243596;
			goto IL_0012;
			IL_0012:
			while (true)
			{
				switch (num2 ^ 0x33BB10E)
				{
				case 3:
					break;
				case 2:
					ReInput.CheckInitialized(aYVuMCKfoBJZlPKnMohBfcrinAp);
					return null;
				case 4:
					if (ReflectionTools.DoesTypeImplement(lUppgxXKJNRAqTCwzcJnBBYsgtW[num].GetType(), type))
					{
						return lUppgxXKJNRAqTCwzcJnBBYsgtW[num];
					}
					num++;
					num2 = 54243598;
					continue;
				case 0:
				{
					int num3;
					if (num < lUppgxXKJNRAqTCwzcJnBBYsgtW.Length)
					{
						num2 = 54243594;
						num3 = num2;
					}
					else
					{
						num2 = 54243599;
						num3 = num2;
					}
					continue;
				}
				default:
					return null;
				}
				break;
			}
			goto IL_000d;
		}

		public T GetTemplate<T>() where T : class
		{
			if (ReInput._id != aYVuMCKfoBJZlPKnMohBfcrinAp)
			{
				ReInput.CheckInitialized(aYVuMCKfoBJZlPKnMohBfcrinAp);
				goto IL_0019;
			}
			int num = 0;
			int num2 = -1315836479;
			goto IL_001e;
			IL_001e:
			while (true)
			{
				switch (num2 ^ -1315836480)
				{
				case 2:
					break;
				case 3:
					return null;
				case 1:
				{
					int num3;
					if (num < lUppgxXKJNRAqTCwzcJnBBYsgtW.Length)
					{
						num2 = -1315836476;
						num3 = num2;
					}
					else
					{
						num2 = -1315836480;
						num3 = num2;
					}
					continue;
				}
				case 4:
					if (lUppgxXKJNRAqTCwzcJnBBYsgtW[num] as T != null)
					{
						return lUppgxXKJNRAqTCwzcJnBBYsgtW[num] as T;
					}
					num++;
					num2 = -1315836479;
					continue;
				default:
					return null;
				}
				break;
			}
			goto IL_0019;
			IL_0019:
			num2 = -1315836477;
			goto IL_001e;
		}

		public bool ImplementsTemplate(Guid typeGuid)
		{
			if (ReInput._id != aYVuMCKfoBJZlPKnMohBfcrinAp)
			{
				ReInput.CheckInitialized(aYVuMCKfoBJZlPKnMohBfcrinAp);
				goto IL_0019;
			}
			int num = 0;
			int num2 = 199514720;
			goto IL_001e;
			IL_001e:
			while (true)
			{
				switch (num2 ^ 0xBE45A63)
				{
				case 4:
					break;
				case 1:
					return false;
				case 3:
					num2 = 199514723;
					continue;
				case 2:
					if (lUppgxXKJNRAqTCwzcJnBBYsgtW[num].typeGuid == typeGuid)
					{
						return true;
					}
					num++;
					num2 = 199514723;
					continue;
				default:
					if (num >= lUppgxXKJNRAqTCwzcJnBBYsgtW.Length)
					{
						return false;
					}
					goto case 2;
				}
				break;
			}
			goto IL_0019;
			IL_0019:
			num2 = 199514722;
			goto IL_001e;
		}

		public bool ImplementsTemplate(Type type)
		{
			//Discarded unreachable code: IL_0053
			if (ReInput._id != aYVuMCKfoBJZlPKnMohBfcrinAp)
			{
				ReInput.CheckInitialized(aYVuMCKfoBJZlPKnMohBfcrinAp);
				return false;
			}
			if (type == null)
			{
				goto IL_001e;
			}
			goto IL_005a;
			IL_0023:
			int num;
			int num2 = default(int);
			while (true)
			{
				switch (num ^ -1987221178)
				{
				case 4:
					break;
				case 1:
					throw new ArgumentNullException("type");
				case 0:
					goto IL_005a;
				case 2:
					goto IL_0063;
				case 3:
					goto IL_0085;
				default:
					return false;
				}
				break;
				IL_0085:
				int num3;
				if (num2 < lUppgxXKJNRAqTCwzcJnBBYsgtW.Length)
				{
					num = -1987221180;
					num3 = num;
				}
				else
				{
					num = -1987221181;
					num3 = num;
				}
				continue;
				IL_0063:
				if (ReflectionTools.DoesTypeImplement(lUppgxXKJNRAqTCwzcJnBBYsgtW[num2].GetType(), type))
				{
					return true;
				}
				num2++;
				num = -1987221179;
			}
			goto IL_001e;
			IL_001e:
			num = -1987221177;
			goto IL_0023;
			IL_005a:
			num2 = 0;
			num = -1987221179;
			goto IL_0023;
		}

		public bool ImplementsTemplate<T>() where T : class
		{
			return ImplementsTemplate(typeof(T));
		}

		internal void sMTnFJuUCVmnnOXNncwgXbHoGoo(IControllerTemplate[] P_0)
		{
			if (P_0 == null)
			{
				return;
			}
			while (true)
			{
				lUppgxXKJNRAqTCwzcJnBBYsgtW = P_0;
				rubrBaVaCxdIwvuRgyLzRjjQJSp = new ReadOnlyCollection<IControllerTemplate>(lUppgxXKJNRAqTCwzcJnBBYsgtW);
				int num = -423698279;
				while (true)
				{
					switch (num ^ -423698279)
					{
					default:
						return;
					case 0:
						return;
					case 2:
						goto IL_0004;
					case 1:
						break;
					}
					break;
					IL_0004:
					num = -423698280;
				}
			}
		}

		internal virtual void UpdateData(UpdateLoopType P_0)
		{
			bool flag = ReInput.IsInputAllowed(_type);
			int buttonCount = _buttonCount;
			if (!flag)
			{
				goto IL_00cf;
			}
			int num = 0;
			goto IL_0125;
			IL_0125:
			int num2;
			int num3;
			if (num >= buttonCount)
			{
				num2 = -2069578125;
				num3 = num2;
			}
			else
			{
				num2 = -2069578128;
				num3 = num2;
			}
			goto IL_0025;
			IL_00cf:
			int num4 = 0;
			num2 = -2069578123;
			goto IL_0025;
			IL_0025:
			while (true)
			{
				switch (num2 ^ -2069578124)
				{
				default:
					return;
				case 8:
					return;
				case 2:
					num2 = -2069578128;
					continue;
				case 7:
					num2 = -2069578121;
					continue;
				case 0:
					num++;
					num2 = -2069578126;
					continue;
				case 3:
					if (URrJKvNlsbObHZfzAftSOFHVrKY != null)
					{
						URrJKvNlsbObHZfzAftSOFHVrKY.UpdateData(P_0);
						num2 = -2069578116;
						continue;
					}
					return;
				case 5:
					num4++;
					num2 = -2069578113;
					continue;
				case 4:
					if (buttons[num].jQRWXHvcjvaNYASwRBgjdfkYLvBF <= 0)
					{
						buttons[num].HDNHWHMIgYVNwexHqBXaTGgQOqc(P_0, num, fHeaxilTulwRIDeBNoWGpOscOuo);
						num2 = -2069578124;
						continue;
					}
					goto case 0;
				case 10:
					break;
				case 11:
					goto IL_00db;
				case 1:
					num2 = -2069578113;
					continue;
				case 9:
					if (buttons[num4].jQRWXHvcjvaNYASwRBgjdfkYLvBF <= 0)
					{
						buttons[num4].MiztKnmopjblqKIFLLcRAoZHuXqk(P_0);
						num2 = -2069578127;
						continue;
					}
					goto case 5;
				case 6:
					goto IL_0125;
				}
				break;
				IL_00db:
				int num5;
				if (num4 >= buttonCount)
				{
					num2 = -2069578121;
					num5 = num2;
				}
				else
				{
					num2 = -2069578115;
					num5 = num2;
				}
			}
			goto IL_00cf;
		}

		internal virtual ButtonStateFlags dwMprVlvXALFXGcWbznsPkfzZRu(int P_0)
		{
			if (P_0 < 0 || P_0 >= _buttonCount)
			{
				return ButtonStateFlags.mDAIjbNLvUSVEOgNmcNByLKLdGwC;
			}
			return buttons[P_0].state;
		}

		internal void bmQbAQActXhqwBPqEbTQZEOfcqHF(Extension P_0)
		{
			//Discarded unreachable code: IL_0041
			if (P_0 == null)
			{
				URrJKvNlsbObHZfzAftSOFHVrKY = null;
				return;
			}
			while (URrJKvNlsbObHZfzAftSOFHVrKY == null)
			{
				while (true)
				{
					IL_0048:
					P_0.SetController(this);
					int num = 397002391;
					while (true)
					{
						switch (num ^ 0x17A9C697)
						{
						default:
							return;
						case 4:
							return;
						case 3:
							num = 397002389;
							continue;
						case 2:
							break;
						case 1:
							goto IL_0048;
						case 0:
							URrJKvNlsbObHZfzAftSOFHVrKY = P_0.Clone();
							num = 397002387;
							continue;
						}
						break;
					}
					break;
				}
			}
			tmNyMjQkMoBiaWSRZHMbqbzmqVR(P_0);
		}

		internal void tmNyMjQkMoBiaWSRZHMbqbzmqVR(Extension P_0)
		{
			//Discarded unreachable code: IL_003d
			if (URrJKvNlsbObHZfzAftSOFHVrKY != null)
			{
				goto IL_0008;
			}
			goto IL_002e;
			IL_002e:
			bmQbAQActXhqwBPqEbTQZEOfcqHF(P_0);
			int num = 2072185755;
			goto IL_000d;
			IL_000d:
			while (true)
			{
				switch (num ^ 0x7B830B99)
				{
				default:
					return;
				case 2:
					return;
				case 3:
					return;
				case 0:
					break;
				case 1:
					goto IL_002e;
				case 4:
					URrJKvNlsbObHZfzAftSOFHVrKY.SetSource(P_0);
					URrJKvNlsbObHZfzAftSOFHVrKY.SetController(this);
					if (P_0 != null)
					{
						P_0.SetController(this);
						num = 2072185754;
						continue;
					}
					return;
				}
				break;
			}
			goto IL_0008;
			IL_0008:
			num = 2072185757;
			goto IL_000d;
		}

		internal virtual void Clear()
		{
			int num = 0;
			while (true)
			{
				int num2 = 1570630609;
				while (true)
				{
					switch (num2 ^ 0x5D9DEBD5)
					{
					default:
						return;
					case 7:
						return;
					case 3:
						break;
					case 6:
						num++;
						num2 = 1570630612;
						continue;
					case 0:
					{
						int num4;
						if (buttons[num] != null)
						{
							num2 = 1570630615;
							num4 = num2;
						}
						else
						{
							num2 = 1570630611;
							num4 = num2;
						}
						continue;
					}
					case 4:
						num2 = 1570630612;
						continue;
					case 8:
						URrJKvNlsbObHZfzAftSOFHVrKY.Clear();
						num2 = 1570630610;
						continue;
					case 1:
						if (num < _buttonCount)
						{
							goto case 0;
						}
						if (fHeaxilTulwRIDeBNoWGpOscOuo != null)
						{
							fHeaxilTulwRIDeBNoWGpOscOuo.ClearData();
							num2 = 1570630608;
							continue;
						}
						goto case 5;
					case 2:
						buttons[num].Reset();
						num2 = 1570630611;
						continue;
					case 5:
					{
						int num3;
						if (URrJKvNlsbObHZfzAftSOFHVrKY != null)
						{
							num2 = 1570630621;
							num3 = num2;
						}
						else
						{
							num2 = 1570630610;
							num3 = num2;
						}
						continue;
					}
					}
					break;
				}
			}
		}

		internal virtual bool SetEnabled(bool P_0)
		{
			if (KfbFsZTwJuhYvyDZkVBtGMGpvXu == P_0)
			{
				return false;
			}
			if (!P_0)
			{
				Clear();
				goto IL_0014;
			}
			goto IL_0036;
			IL_0036:
			KfbFsZTwJuhYvyDZkVBtGMGpvXu = P_0;
			int num = -1902101046;
			goto IL_0019;
			IL_0019:
			while (true)
			{
				switch (num ^ -1902101046)
				{
				case 3:
					break;
				case 1:
					goto IL_0036;
				case 0:
					if (WlWcgTHLbZSJaqIGqDBsamLJHPZe != null)
					{
						WlWcgTHLbZSJaqIGqDBsamLJHPZe(P_0);
						num = -1902101048;
						continue;
					}
					goto default;
				default:
					return true;
				}
				break;
			}
			goto IL_0014;
			IL_0014:
			num = -1902101045;
			goto IL_0019;
		}

		internal virtual void BakeMap(ControllerMap P_0)
		{
			if (P_0 == null)
			{
				return;
			}
			while (true)
			{
				P_0.controllerId = id;
				P_0.controllerType = _type;
				IList<ActionElementMap> buttonMaps = P_0.ButtonMaps;
				int num = 0;
				int num2 = 2038485471;
				while (true)
				{
					switch (num2 ^ 0x7980D1D9)
					{
					default:
						return;
					case 3:
						return;
					case 4:
						num2 = 2038485464;
						continue;
					case 5:
						BakeActionElementMap(P_0, buttonMaps[num]);
						num2 = 2038485467;
						continue;
					case 2:
						num++;
						num2 = 2038485465;
						continue;
					case 0:
					{
						int num3;
						if (num >= buttonMaps.Count)
						{
							num2 = 2038485466;
							num3 = num2;
						}
						else
						{
							num2 = 2038485468;
							num3 = num2;
						}
						continue;
					}
					case 6:
						num2 = 2038485465;
						continue;
					case 1:
						break;
					}
					break;
				}
			}
		}

		internal virtual void BakeActionElementMap(ControllerMap P_0, ActionElementMap P_1)
		{
			//Discarded unreachable code: IL_0030
			if (P_1 == null)
			{
				return;
			}
			while (P_1._elementType == ControllerElementType.Button)
			{
				while (true)
				{
					IL_0037:
					P_1.JfiNbsqoPpqigTfhbjHgNsDksJX(P_0);
					int num = 456223086;
					while (true)
					{
						switch (num ^ 0x1B31696D)
						{
						default:
							return;
						case 3:
							return;
						case 2:
							num = 456223084;
							continue;
						case 1:
							break;
						case 0:
							goto IL_0037;
						}
						break;
					}
					break;
				}
			}
		}

		internal bool uMvVcSUMilQqjnDgIlYcydEeAIg(ActionElementMap P_0, int P_1, out float P_2, out bool P_3)
		{
			P_3 = false;
			P_2 = 0f;
			if (P_1 != P_0._actionId)
			{
				return false;
			}
			int vdkIPqfMwShfhFIMiCDUOMcdyscN = P_0.vdkIPqfMwShfhFIMiCDUOMcdyscN;
			int num;
			if (vdkIPqfMwShfhFIMiCDUOMcdyscN >= 0)
			{
				if (vdkIPqfMwShfhFIMiCDUOMcdyscN >= _buttonCount)
				{
					goto IL_002a;
				}
				P_3 = buttons[vdkIPqfMwShfhFIMiCDUOMcdyscN].NzEtIbXhndCRmkmnMiPBfEClEtEi;
				int num2;
				if (!P_3)
				{
					num = -1450788372;
					num2 = num;
				}
				else
				{
					num = -1450788382;
					num2 = num;
				}
				goto IL_002f;
			}
			goto IL_006b;
			IL_002f:
			float num3 = default(float);
			while (true)
			{
				switch (num ^ -1450788380)
				{
				case 7:
					break;
				case 10:
					goto IL_006b;
				case 4:
					P_2 = num3;
					num = -1450788383;
					continue;
				case 9:
					if (P_0._elementType != 0)
					{
						goto case 4;
					}
					goto IL_00a5;
				case 1:
					if (P_0._axisContribution == Pole.Negative)
					{
						num3 *= -1f;
						num = -1450788384;
						continue;
					}
					goto case 4;
				case 2:
					if (num3 > 0f)
					{
						if (P_0._elementType != ControllerElementType.Button)
						{
							goto case 9;
						}
						if (P_0._axisContribution == Pole.Negative)
						{
							num3 *= -1f;
							num = -1450788384;
							continue;
						}
					}
					goto case 4;
				case 0:
					if (P_0._invert)
					{
						num3 *= -1f;
						num = -1450788384;
						continue;
					}
					goto case 4;
				case 6:
					num3 = buttons[vdkIPqfMwShfhFIMiCDUOMcdyscN].pressure;
					num = -1450788377;
					continue;
				case 8:
					num3 = (buttons[vdkIPqfMwShfhFIMiCDUOMcdyscN].value ? 1f : 0f);
					num = -1450788378;
					continue;
				case 3:
					num = -1450788378;
					continue;
				default:
					return true;
				}
				break;
				IL_00a5:
				int num4;
				if (P_0._axisRange != 0)
				{
					num = -1450788379;
					num4 = num;
				}
				else
				{
					num = -1450788380;
					num4 = num;
				}
			}
			goto IL_002a;
			IL_002a:
			num = -1450788370;
			goto IL_002f;
			IL_006b:
			return false;
		}

		internal bool uMvVcSUMilQqjnDgIlYcydEeAIg(ActionElementMap P_0, int P_1, bool P_2, out float P_3)
		{
			P_3 = 0f;
			if (P_1 != P_0._actionId)
			{
				return false;
			}
			float num = P_2 ? 1f : 0f;
			if (num > 0f)
			{
				while (true)
				{
					int num2 = -1143979628;
					while (true)
					{
						switch (num2 ^ -1143979625)
						{
						case 0:
							break;
						case 4:
							num *= -1f;
							num2 = -1143979630;
							continue;
						case 6:
							goto IL_006e;
						case 3:
							if (P_0._elementType != ControllerElementType.Button)
							{
								goto IL_00a9;
							}
							if (P_0._axisContribution == Pole.Negative)
							{
								num *= -1f;
								num2 = -1143979630;
								continue;
							}
							goto IL_00e7;
						case 1:
							goto IL_00a9;
						case 2:
							num *= -1f;
							num2 = -1143979630;
							continue;
						default:
							goto IL_00e7;
						}
						break;
						IL_00a9:
						if (P_0._elementType == ControllerElementType.Axis)
						{
							if (P_0._axisRange == AxisRange.Full)
							{
								int num3;
								if (!P_0._invert)
								{
									num2 = -1143979630;
									num3 = num2;
								}
								else
								{
									num2 = -1143979627;
									num3 = num2;
								}
								continue;
							}
							goto IL_006e;
						}
						goto IL_00e7;
						IL_006e:
						int num4;
						if (P_0._axisContribution != Pole.Negative)
						{
							num2 = -1143979630;
							num4 = num2;
						}
						else
						{
							num2 = -1143979629;
							num4 = num2;
						}
					}
				}
			}
			goto IL_00e7;
			IL_00e7:
			P_3 = num;
			return true;
		}

		internal void htWbfivVfubrLgJDGOktUnLLIDLq(Element P_0)
		{
			if (P_0 != null)
			{
				ListTools.AddIfUnique(BARfPlkouALNzdMLqPxStuuAallA, P_0);
			}
		}

		internal virtual Guid FkhgRRrmhleoQfwJfNzXNSUgsEz()
		{
			return Guid.Empty;
		}

		protected virtual void Connected()
		{
			_isConnected = true;
		}

		protected virtual void Disconnected()
		{
			_isConnected = false;
			if (fHeaxilTulwRIDeBNoWGpOscOuo != null)
			{
				fHeaxilTulwRIDeBNoWGpOscOuo.ClearData();
			}
		}

		[CompilerGenerated]
		private static bool sIIZhDolZtcWvVYZqpUTIuNFdUg(Controller P_0, Guid P_1)
		{
			return P_0.ImplementsTemplate(P_1);
		}

		[CompilerGenerated]
		private static bool ChkgFyUxtNlbXIhEtNlPjJLFiGQ(Controller P_0, Type P_1)
		{
			return P_0.ImplementsTemplate(P_1);
		}
	}
}
