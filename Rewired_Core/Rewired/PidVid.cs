using Rewired.Utils;
using System;
using System.Globalization;
using System.Text.RegularExpressions;

namespace Rewired
{
	[CustomObfuscation(rename = false)]
	[CustomClassObfuscation(renamePubIntMembers = false, renamePrivateMembers = true)]
	internal struct PidVid : IEquatable<PidVid>
	{
		private const string fvSMEYgDVQUnlnpcNxSYBlidHrQ = "[^a-fA-F0-9]";

		public ushort productId;

		public ushort vendorId;

		public bool isZero
		{
			get
			{
				if (vendorId == 0)
				{
					return productId == 0;
				}
				return false;
			}
		}

		public PidVid(ushort productId, ushort vendorId)
		{
			this.productId = productId;
			this.vendorId = vendorId;
		}

		public PidVid(string pidVid)
		{
			if (string.IsNullOrEmpty(dypVEfEUMERETPHlmeApdnRTBQqn(pidVid)))
			{
				productId = 0;
				vendorId = 0;
			}
			else
			{
				try
				{
					productId = ushort.Parse(pidVid.Substring(0, 4), NumberStyles.AllowHexSpecifier);
					vendorId = ushort.Parse(pidVid.Substring(4, 4), NumberStyles.AllowHexSpecifier);
				}
				catch
				{
					productId = 0;
					vendorId = 0;
				}
			}
		}

		public PidVid(Guid productGuid)
			: this(productGuid.ToString().Substring(0, 8))
		{
		}

		public bool Equals(string pidVid)
		{
			return OnLfksLIGcjECKFQHZYANRxQKbC(dypVEfEUMERETPHlmeApdnRTBQqn(pidVid));
		}

		public Guid ToProductGuid()
		{
			return MiscTools.CreateHIDProductGuid(vendorId, productId);
		}

		private bool OnLfksLIGcjECKFQHZYANRxQKbC(string P_0)
		{
			//Discarded unreachable code: IL_004f
			if (string.IsNullOrEmpty(P_0) || P_0.Length < 8)
			{
				return false;
			}
			try
			{
				if (productId != ushort.Parse(P_0.Substring(0, 4), NumberStyles.AllowHexSpecifier))
				{
					bool result = false;
					while (true)
					{
						switch (-1782570316 ^ -1782570315)
						{
						case 0:
							continue;
						case 1:
							return result;
						}
						break;
					}
				}
				return vendorId == ushort.Parse(P_0.Substring(4, 4), NumberStyles.AllowHexSpecifier);
			}
			catch
			{
				return false;
			}
		}

		public override bool Equals(object obj)
		{
			if (!(obj is PidVid))
			{
				return false;
			}
			PidVid pidVid = (PidVid)obj;
			if (pidVid.vendorId == vendorId)
			{
				return pidVid.productId == productId;
			}
			return false;
		}

		public override int GetHashCode()
		{
			int num = 17;
			num = num * 29 + vendorId.GetHashCode();
			while (true)
			{
				int num2 = -1402912025;
				while (true)
				{
					switch (num2 ^ -1402912026)
					{
					case 2:
						break;
					case 1:
						goto IL_0032;
					default:
						return num;
					}
					break;
					IL_0032:
					num = num * 29 + productId.GetHashCode();
					num2 = -1402912026;
				}
			}
		}

		public bool Equals(PidVid other)
		{
			if (vendorId == other.vendorId)
			{
				return productId == other.productId;
			}
			return false;
		}

		public static bool operator ==(PidVid x, PidVid y)
		{
			if (x.vendorId == y.vendorId)
			{
				return x.productId == y.productId;
			}
			return false;
		}

		public static bool operator !=(PidVid x, PidVid y)
		{
			return !(x == y);
		}

		public override string ToString()
		{
			return productId.ToString("x4") + vendorId.ToString("x4");
		}

		public static bool ArrayContains(string[] pidVids, ref PidVid vidPid)
		{
			if (pidVids == null)
			{
				goto IL_0003;
			}
			int num = 0;
			int num2 = 364639800;
			goto IL_0008;
			IL_0003:
			num2 = 364639807;
			goto IL_0008;
			IL_0008:
			while (true)
			{
				switch (num2 ^ 0x15BBF63B)
				{
				case 0:
					break;
				case 4:
					return false;
				case 2:
				{
					int num3;
					if (num < pidVids.Length)
					{
						num2 = 364639802;
						num3 = num2;
					}
					else
					{
						num2 = 364639806;
						num3 = num2;
					}
					continue;
				}
				case 1:
					if (vidPid.Equals(pidVids[num]))
					{
						return true;
					}
					num++;
					num2 = 364639801;
					continue;
				case 3:
					num2 = 364639801;
					continue;
				default:
					return false;
				}
				break;
			}
			goto IL_0003;
		}

		private static string dypVEfEUMERETPHlmeApdnRTBQqn(string P_0)
		{
			if (string.IsNullOrEmpty(P_0))
			{
				return null;
			}
			if (Regex.IsMatch(P_0, "[^a-fA-F0-9]"))
			{
				P_0 = Regex.Replace(P_0, "[^a-fA-F0-9]", "");
				goto IL_0029;
			}
			goto IL_0047;
			IL_0047:
			int num;
			if (string.IsNullOrEmpty(P_0))
			{
				num = 1705822237;
				goto IL_002e;
			}
			if (P_0.Length < 8)
			{
				return null;
			}
			return P_0;
			IL_0029:
			num = 1705822236;
			goto IL_002e;
			IL_002e:
			switch (num ^ 0x65ACC81D)
			{
			case 2:
				break;
			case 1:
				goto IL_0047;
			default:
				return null;
			}
			goto IL_0029;
		}
	}
}
