using Rewired.UI;
using Rewired.Utils;
using Rewired.Utils.Interfaces;
using UnityEngine;
using UnityEngine.UI;

namespace Rewired.ComponentControls.Effects
{
	[ExecuteInEditMode]
	[RequireComponent(typeof(Image))]
	[DisallowMultipleComponent]
	[RequireComponent(typeof(RectTransform))]
	public sealed class TouchJoystickAngleIndicator : MonoBehaviour, IVisibilityChangedHandler, TouchJoystick.IStickPositionChangedHandler
	{
		[CustomObfuscation(rename = false)]
		[SerializeField]
		[Tooltip("Toggles visibility.")]
		private bool _visible = true;

		[CustomObfuscation(rename = false)]
		[Tooltip("If enabled, the target angle will be determined by the transform's Local Rotation Z. Otherwise, the activation angle must be manually set.")]
		[SerializeField]
		private bool _targetAngleFromRotation = true;

		[Range(0f, -360f)]
		[Tooltip("The joystick angle at which this object should be considered fully active.\n0 = up with negative values increase rotating clockwise. Example: -45 degrees = up-right.")]
		[CustomObfuscation(rename = false)]
		[SerializeField]
		private float _targetAngle;

		[Tooltip("If enabled, the color will fade in and out based on the current joystick value.")]
		[SerializeField]
		[CustomObfuscation(rename = false)]
		private bool _fadeWithValue = true;

		[SerializeField]
		[Tooltip("If enabled, the color will fade in and out based on the current joystick angle. As the angle approaches the Target Angle, the color will become more intense.")]
		[CustomObfuscation(rename = false)]
		private bool _fadeWithAngle = true;

		[CustomObfuscation(rename = false)]
		[Range(0f, 360f)]
		[SerializeField]
		[Tooltip("The angle of rotation away from the Target Angle where the color fully fades out. If Fade with Angle is enabled, this is used to determine when the color will fully fade out when the joystick angle rotates away from the the Target Angle. This should be set to 1/2 of the complete rotation arc. Example: A value of 45 degrees would make the color fully fade out when the joystick angle is 45 degrees away from the Target Angle on either side, giving a complete arc of 90 degrees.")]
		private float _fadeRange = 45f;

		[Tooltip("The color when fully active.")]
		[SerializeField]
		[CustomObfuscation(rename = false)]
		private Color _activeColor = new Color(1f, 1f, 1f, 1f);

		[SerializeField]
		[Tooltip("The color when not active.")]
		[CustomObfuscation(rename = false)]
		private Color _normalColor = new Color(1f, 1f, 1f, 0.3f);

		private Image LwqDwVAciFWHTCjWPQXjdMlyCwJ;

		private RectTransform mgsrwkGejIMmMMAQCHbhFpCCJTb;

		private Vector2 UqmweiXbTIZiawkFFOOdpSEXMZU;

		private bool uNnreHBfJqFHmehkmKUMyjnhwVt;

		private IRegistrar<TouchJoystickAngleIndicator> XvQzsQEXZpszWQBsxoTRoBRwQBH;

		public bool visible
		{
			get
			{
				return _visible;
			}
			set
			{
				//Discarded unreachable code: IL_0028
				if (visible == value)
				{
					while (true)
					{
						switch (0x122202A4 ^ 0x122202A6)
						{
						case 2:
							return;
						case 0:
							continue;
						}
						break;
					}
				}
				DslcJwXTzoxJrqDkiUIuuiJQYNZ(value, false);
				bFoWQuDzcOvecUqeSCdTCzgfmRIE();
			}
		}

		public bool targetAngleFromRotation
		{
			get
			{
				return _targetAngleFromRotation;
			}
			set
			{
				//Discarded unreachable code: IL_002c
				if (_targetAngleFromRotation == value)
				{
					goto IL_0009;
				}
				goto IL_0033;
				IL_0033:
				_targetAngleFromRotation = value;
				bFoWQuDzcOvecUqeSCdTCzgfmRIE();
				int num = 1726376253;
				goto IL_000e;
				IL_000e:
				switch (num ^ 0x66E6693D)
				{
				default:
					return;
				case 0:
					return;
				case 1:
					return;
				case 2:
					break;
				case 3:
					goto IL_0033;
				}
				goto IL_0009;
				IL_0009:
				num = 1726376252;
				goto IL_000e;
			}
		}

		public float targetAngle
		{
			get
			{
				if (!_targetAngleFromRotation)
				{
					return _targetAngle;
				}
				return base.transform.localEulerAngles.z;
			}
			set
			{
				if (_targetAngle == value)
				{
					return;
				}
				while (true)
				{
					_targetAngle = value;
					bFoWQuDzcOvecUqeSCdTCzgfmRIE();
					int num = -672345153;
					while (true)
					{
						switch (num ^ -672345153)
						{
						default:
							return;
						case 0:
							return;
						case 2:
							goto IL_000a;
						case 1:
							break;
						}
						break;
						IL_000a:
						num = -672345154;
					}
				}
			}
		}

		public bool fadeWithValue
		{
			get
			{
				return _fadeWithValue;
			}
			set
			{
				//Discarded unreachable code: IL_0030
				if (_fadeWithValue == value)
				{
					goto IL_0009;
				}
				goto IL_0037;
				IL_0037:
				_fadeWithValue = value;
				int num = -1449994851;
				goto IL_000e;
				IL_000e:
				while (true)
				{
					switch (num ^ -1449994855)
					{
					default:
						return;
					case 2:
						return;
					case 3:
						return;
					case 0:
						break;
					case 1:
						goto IL_0037;
					case 4:
						bFoWQuDzcOvecUqeSCdTCzgfmRIE();
						num = -1449994854;
						continue;
					}
					break;
				}
				goto IL_0009;
				IL_0009:
				num = -1449994853;
				goto IL_000e;
			}
		}

		public bool fadeWithAngle
		{
			get
			{
				return _fadeWithAngle;
			}
			set
			{
				if (_fadeWithAngle == value)
				{
					return;
				}
				while (true)
				{
					_fadeWithAngle = value;
					int num = 1145969577;
					while (true)
					{
						switch (num ^ 0x444E1BA9)
						{
						case 2:
							goto IL_000a;
						case 1:
							break;
						default:
							bFoWQuDzcOvecUqeSCdTCzgfmRIE();
							return;
						}
						break;
						IL_000a:
						num = 1145969576;
					}
				}
			}
		}

		public float fadeRange
		{
			get
			{
				return _fadeRange;
			}
			set
			{
				if (_fadeRange != value)
				{
					_fadeRange = value;
					bFoWQuDzcOvecUqeSCdTCzgfmRIE();
				}
			}
		}

		public Color activeColor
		{
			get
			{
				return _activeColor;
			}
			set
			{
				_activeColor = value;
				bFoWQuDzcOvecUqeSCdTCzgfmRIE();
			}
		}

		public Color normalColor
		{
			get
			{
				return _normalColor;
			}
			set
			{
				_normalColor = value;
				bFoWQuDzcOvecUqeSCdTCzgfmRIE();
			}
		}

		internal Image image => LwqDwVAciFWHTCjWPQXjdMlyCwJ ?? (LwqDwVAciFWHTCjWPQXjdMlyCwJ = GetComponent<Image>());

		internal Sprite currentSprite
		{
			get
			{
				if (image == null)
				{
					return null;
				}
				if (LwqDwVAciFWHTCjWPQXjdMlyCwJ.overrideSprite != null)
				{
					return LwqDwVAciFWHTCjWPQXjdMlyCwJ.overrideSprite;
				}
				return LwqDwVAciFWHTCjWPQXjdMlyCwJ.sprite;
			}
		}

		internal RectTransform rectTransform => mgsrwkGejIMmMMAQCHbhFpCCJTb ?? (mgsrwkGejIMmMMAQCHbhFpCCJTb = GetComponent<RectTransform>());

		[CustomObfuscation(rename = false)]
		private TouchJoystickAngleIndicator()
		{
		}

		internal bool EdmMylnAfIqOdzIptshYWAZuDsN(out Vector2 P_0)
		{
			P_0 = Vector2.zero;
			if (image == null)
			{
				goto IL_0019;
			}
			Sprite sprite = LwqDwVAciFWHTCjWPQXjdMlyCwJ.overrideSprite;
			int num;
			if ((object)sprite == null)
			{
				num = 1957133578;
				goto IL_001e;
			}
			goto IL_0076;
			IL_006b:
			sprite = LwqDwVAciFWHTCjWPQXjdMlyCwJ.sprite;
			goto IL_0076;
			IL_001e:
			Rect textureRect = default(Rect);
			while (true)
			{
				switch (num ^ 0x74A77D0A)
				{
				case 2:
					break;
				case 4:
					return false;
				case 3:
					P_0.x = textureRect.width;
					num = 1957133579;
					continue;
				case 0:
					goto IL_006b;
				default:
					P_0.y = textureRect.height;
					return true;
				}
				break;
			}
			goto IL_0019;
			IL_0076:
			Sprite sprite2 = sprite;
			if (sprite2 == null)
			{
				return false;
			}
			textureRect = sprite2.textureRect;
			num = 1957133577;
			goto IL_001e;
			IL_0019:
			num = 1957133582;
			goto IL_001e;
		}

		[CustomObfuscation(rename = false)]
		private void Awake()
		{
			OnTouchJoystickStickPositionChanged(Vector2.zero);
			tJvGxLTYBcmKEdCZNJVdFQoLtqu();
		}

		[CustomObfuscation(rename = false)]
		private void OnEnable()
		{
			if (!Application.isPlaying)
			{
				tJvGxLTYBcmKEdCZNJVdFQoLtqu();
				rQIpofiIVbLGRIWiyWqIHlumHXc();
			}
			DbDVesDPXlejyIwdmYYmGHNqkyZe(UqmweiXbTIZiawkFFOOdpSEXMZU);
		}

		[CustomObfuscation(rename = false)]
		private void OnDisable()
		{
			ScVdvtInZvRDiUAQOYDEkXhsoUg();
		}

		[CustomObfuscation(rename = false)]
		private void OnValidate()
		{
			pQqdNGRnbQgbkiSLDqjfubKstGA();
			DbDVesDPXlejyIwdmYYmGHNqkyZe(UqmweiXbTIZiawkFFOOdpSEXMZU);
		}

		[CustomObfuscation(rename = false)]
		private void OnTransformParentChanged()
		{
			rQIpofiIVbLGRIWiyWqIHlumHXc();
		}

		private void DslcJwXTzoxJrqDkiUIuuiJQYNZ(bool P_0, bool P_1)
		{
			//Discarded unreachable code: IL_0067
			if (_visible == P_0 && !P_1)
			{
				return;
			}
			while (true)
			{
				_visible = P_0;
				if (P_0)
				{
					break;
				}
				Color normalColor = _normalColor;
				int num = -37962075;
				while (true)
				{
					switch (num ^ -37962075)
					{
					case 3:
						num = -37962076;
						continue;
					case 1:
						break;
					case 0:
						normalColor.a = 0f;
						image.CrossFadeColor(normalColor, 0f, ignoreTimeScale: true, useAlpha: true);
						return;
					default:
						goto end_IL_002f;
					}
					break;
				}
				continue;
				end_IL_002f:
				break;
			}
			DbDVesDPXlejyIwdmYYmGHNqkyZe(UqmweiXbTIZiawkFFOOdpSEXMZU);
		}

		private void DbDVesDPXlejyIwdmYYmGHNqkyZe(Vector2 P_0)
		{
			//Discarded unreachable code: IL_0077, IL_0230
			Color normalColor = default(Color);
			if (!_visible)
			{
				normalColor = _normalColor;
				goto IL_0012;
			}
			goto IL_01cb;
			IL_0017:
			int num;
			Color targetColor = default(Color);
			float num2 = default(float);
			float magnitude = default(float);
			float num5 = default(float);
			float num6 = default(float);
			float num3 = default(float);
			float num4 = default(float);
			while (true)
			{
				switch (num ^ 0x480771AB)
				{
				case 8:
					break;
				case 2:
					image.CrossFadeColor(targetColor, 0f, ignoreTimeScale: true, useAlpha: true);
					return;
				case 11:
					targetColor = Color.Lerp(_normalColor, _activeColor, num2);
					num = 1208447401;
					continue;
				case 9:
					num2 *= magnitude;
					num = 1208447403;
					continue;
				case 0:
					if (_fadeWithAngle)
					{
						float num7 = Mathf.Abs(MathTools.DeltaAngle(num5, num6));
						num3 = ((_fadeRange != 0f) ? MathTools.Clamp01(1f - num7 / _fadeRange) : 1f);
						num = 1208447404;
						continue;
					}
					goto case 11;
				case 12:
					targetColor = (MathTools.AngleIsNear(num5, num6, _fadeRange) ? _activeColor : _normalColor);
					num = 1208447401;
					continue;
				case 10:
					goto IL_0122;
				case 6:
					num5 = ((P_0.x < 0f) ? (360f - num4) : num4);
					num = 1208447397;
					continue;
				case 1:
					goto IL_0169;
				case 13:
					goto IL_0185;
				case 14:
					goto IL_01af;
				case 3:
					goto IL_01cb;
				case 7:
					num2 *= num3;
					num = 1208447392;
					continue;
				case 4:
					normalColor.a = 0f;
					image.CrossFadeColor(normalColor, 0f, ignoreTimeScale: true, useAlpha: true);
					return;
				default:
					goto IL_023a;
				}
				break;
				IL_01af:
				int num8;
				if (!_fadeWithAngle)
				{
					num = 1208447402;
					num8 = num;
				}
				else
				{
					num = 1208447393;
					num8 = num;
				}
				continue;
				IL_0169:
				int num9;
				if (_fadeWithValue)
				{
					num = 1208447393;
					num9 = num;
				}
				else
				{
					num = 1208447399;
					num9 = num;
				}
				continue;
				IL_0122:
				num2 = 1f;
				int num10;
				if (_fadeWithValue)
				{
					num = 1208447394;
					num10 = num;
				}
				else
				{
					num = 1208447403;
					num10 = num;
				}
			}
			goto IL_0012;
			IL_01cb:
			float num11;
			if (!MathTools.ApproximatelyZero(P_0.sqrMagnitude))
			{
				magnitude = P_0.magnitude;
				num4 = Vector2.Angle(Vector2.up, P_0);
				if (_targetAngleFromRotation)
				{
					num11 = base.transform.localEulerAngles.z;
					goto IL_019d;
				}
				num = 1208447398;
				goto IL_0017;
			}
			goto IL_023a;
			IL_023a:
			image.CrossFadeColor(_normalColor, 0f, ignoreTimeScale: true, useAlpha: true);
			return;
			IL_0185:
			num11 = _targetAngle;
			goto IL_019d;
			IL_0012:
			num = 1208447407;
			goto IL_0017;
			IL_019d:
			num6 = num11 * -1f;
			num = 1208447405;
			goto IL_0017;
		}

		private void tJvGxLTYBcmKEdCZNJVdFQoLtqu()
		{
			uNnreHBfJqFHmehkmKUMyjnhwVt = _visible;
		}

		private void pQqdNGRnbQgbkiSLDqjfubKstGA()
		{
			if (uNnreHBfJqFHmehkmKUMyjnhwVt == _visible)
			{
				return;
			}
			uNnreHBfJqFHmehkmKUMyjnhwVt = _visible;
			while (true)
			{
				int num = -1846700719;
				while (true)
				{
					switch (num ^ -1846700720)
					{
					default:
						return;
					case 2:
						return;
					case 0:
						break;
					case 1:
						goto IL_0038;
					}
					break;
					IL_0038:
					DslcJwXTzoxJrqDkiUIuuiJQYNZ(_visible, true);
					num = -1846700718;
				}
			}
		}

		private void bFoWQuDzcOvecUqeSCdTCzgfmRIE()
		{
		}

		private void rQIpofiIVbLGRIWiyWqIHlumHXc()
		{
			//Discarded unreachable code: IL_0044
			ScVdvtInZvRDiUAQOYDEkXhsoUg();
			IRegistrar<TouchJoystickAngleIndicator> componentInSelfOrParents = default(IRegistrar<TouchJoystickAngleIndicator>);
			while (true)
			{
				int num = 1357832120;
				while (true)
				{
					switch (num ^ 0x50EEDFBB)
					{
					case 0:
						break;
					case 3:
						goto IL_0028;
					case 1:
						if (componentInSelfOrParents.IsNullOrDestroyed())
						{
							return;
						}
						goto default;
					default:
						componentInSelfOrParents.Register(this);
						XvQzsQEXZpszWQBsxoTRoBRwQBH = componentInSelfOrParents;
						return;
					}
					break;
					IL_0028:
					componentInSelfOrParents = UnityTools.GetComponentInSelfOrParents<IRegistrar<TouchJoystickAngleIndicator>>(base.transform);
					num = 1357832122;
				}
			}
		}

		private void ScVdvtInZvRDiUAQOYDEkXhsoUg()
		{
			//Discarded unreachable code: IL_0043
			if (XvQzsQEXZpszWQBsxoTRoBRwQBH.IsNullOrDestroyed())
			{
				if (XvQzsQEXZpszWQBsxoTRoBRwQBH != null)
				{
					XvQzsQEXZpszWQBsxoTRoBRwQBH = null;
					goto IL_001c;
				}
				return;
			}
			goto IL_0058;
			IL_0021:
			int num;
			while (true)
			{
				switch (num ^ 0x5F5A06FF)
				{
				default:
					return;
				case 2:
					return;
				case 3:
					return;
				case 0:
					break;
				case 1:
					XvQzsQEXZpszWQBsxoTRoBRwQBH = null;
					num = 1599735548;
					continue;
				case 4:
					goto IL_0058;
				}
				break;
			}
			goto IL_001c;
			IL_001c:
			num = 1599735549;
			goto IL_0021;
			IL_0058:
			XvQzsQEXZpszWQBsxoTRoBRwQBH.Deregister(this);
			num = 1599735550;
			goto IL_0021;
		}

		public void OnVisibilityChanged(bool state)
		{
			DslcJwXTzoxJrqDkiUIuuiJQYNZ(state, false);
		}

		public void OnTouchJoystickStickPositionChanged(Vector2 value)
		{
			//Discarded unreachable code: IL_000a, IL_000f, IL_002a, IL_003c, IL_004c
			if (!(this == null))
			{
				UqmweiXbTIZiawkFFOOdpSEXMZU = value;
				if (UnityTools.IsActiveAndEnabled(this) && _visible)
				{
					DbDVesDPXlejyIwdmYYmGHNqkyZe(value);
				}
			}
		}

		void TouchJoystick.IStickPositionChangedHandler.OnStickPositionChanged(Vector2 P_0)
		{
			OnTouchJoystickStickPositionChanged(P_0);
		}
	}
}
