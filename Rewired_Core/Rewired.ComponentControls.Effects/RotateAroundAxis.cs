using System;
using UnityEngine;

namespace Rewired.ComponentControls.Effects
{
	public class RotateAroundAxis : MonoBehaviour
	{
		public enum Speed
		{
			Stopped,
			Slow,
			Fast
		}

		public enum RotationAxis
		{
			X,
			Y,
			Z
		}

		[SerializeField]
		[CustomObfuscation(rename = false)]
		[Tooltip("The current speed of rotation.")]
		private Speed _speed;

		[SerializeField]
		[Tooltip("The speed of rotation when Speed is set to Slow. This measured in degrees per second.")]
		[CustomObfuscation(rename = false)]
		private float _slowRotationSpeed = 5f;

		[Tooltip("The speed of rotation when Speed is set to Fast. This measured in degrees per second.")]
		[SerializeField]
		[CustomObfuscation(rename = false)]
		private float _fastRotationSpeed = 20f;

		[CustomObfuscation(rename = false)]
		[Tooltip("The axis around which rotation will occur.")]
		[SerializeField]
		private RotationAxis _rotateAroundAxis = RotationAxis.Z;

		[SerializeField]
		[CustomObfuscation(rename = false)]
		[Tooltip("The space in which rotation will occur.")]
		private Space _relativeTo = Space.Self;

		[CustomObfuscation(rename = false)]
		[SerializeField]
		[Tooltip("Reverses the rotation direction.")]
		private bool _reverse;

		public Speed speed
		{
			get
			{
				return _speed;
			}
			set
			{
				_speed = value;
			}
		}

		public float slowRotationSpeed
		{
			get
			{
				return _slowRotationSpeed;
			}
			set
			{
				_slowRotationSpeed = value;
			}
		}

		public float fastRotationSpeed
		{
			get
			{
				return _fastRotationSpeed;
			}
			set
			{
				_fastRotationSpeed = value;
			}
		}

		public RotationAxis rotateAroundAxis
		{
			get
			{
				return _rotateAroundAxis;
			}
			set
			{
				_rotateAroundAxis = value;
			}
		}

		public Space relativeTo
		{
			get
			{
				return _relativeTo;
			}
			set
			{
				_relativeTo = value;
			}
		}

		public bool reverse
		{
			get
			{
				return _reverse;
			}
			set
			{
				_reverse = value;
			}
		}

		[CustomObfuscation(rename = false)]
		private void Update()
		{
			if (_speed == Speed.Stopped)
			{
				return;
			}
			float num;
			while (true)
			{
				num = ((_speed == Speed.Fast) ? _fastRotationSpeed : _slowRotationSpeed);
				if (!_reverse)
				{
					break;
				}
				num *= -1f;
				int num2 = -1748936915;
				while (true)
				{
					switch (num2 ^ -1748936916)
					{
					case 0:
						num2 = -1748936914;
						continue;
					case 2:
						break;
					default:
						goto end_IL_0027;
					}
					break;
				}
				continue;
				end_IL_0027:
				break;
			}
			base.transform.Rotate(gAzSzJkCtfMaLuFfgNcZzPmRTSX(_rotateAroundAxis), num * Time.deltaTime, _relativeTo);
		}

		private static Vector3 gAzSzJkCtfMaLuFfgNcZzPmRTSX(RotationAxis P_0)
		{
			//Discarded unreachable code: IL_0034
			while (true)
			{
				switch (0x509CCE4A ^ 0x509CCE4B)
				{
				case 2:
					continue;
				case 1:
					switch (P_0)
					{
					case RotationAxis.X:
						break;
					case RotationAxis.Y:
						return new Vector3(0f, 1f, 0f);
					case RotationAxis.Z:
						return new Vector3(0f, 0f, 1f);
					default:
						throw new NotImplementedException();
					}
					break;
				}
				break;
			}
			return new Vector3(1f, 0f, 0f);
		}

		public void SetSpeed(Speed speed)
		{
			_speed = speed;
		}

		public void SetSpeed(int speed)
		{
			if (Enum.IsDefined(typeof(Speed), speed))
			{
				_speed = (Speed)speed;
			}
		}
	}
}
