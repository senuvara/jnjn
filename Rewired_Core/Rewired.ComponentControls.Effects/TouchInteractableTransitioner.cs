using Rewired.UI;
using Rewired.Utils;
using Rewired.Utils.Attributes;
using UnityEngine;
using UnityEngine.UI;

namespace Rewired.ComponentControls.Effects
{
	[ExecuteInEditMode]
	[RequireComponent(typeof(RectTransform))]
	[DisallowMultipleComponent]
	public sealed class TouchInteractableTransitioner : MonoBehaviour, IVisibilityChangedHandler, TouchInteractable.IInteractionStateTransitionHandler
	{
		[SerializeField]
		[CustomObfuscation(rename = false)]
		[Tooltip("Toggles visibility. An invisible control can still be interacted with. This property only has any effect when used with an Image Component.")]
		private bool _visible = true;

		[CustomObfuscation(rename = false)]
		[Bitmask(typeof(TouchInteractable.TransitionTypeFlags))]
		[Tooltip("The transition type(s) to be used when transitioning to various states. Multiple transition types can be used simultaneously.")]
		[SerializeField]
		private TouchInteractable.TransitionTypeFlags _transitionType;

		[SerializeField]
		[Tooltip("Settings using for Color Tint transitions.")]
		[CustomObfuscation(rename = false)]
		private ColorBlock _transitionColorTint = new ColorBlock
		{
			colorMultiplier = 1f,
			disabledColor = new Color(25f / 32f, 25f / 32f, 25f / 32f, 0.5f),
			highlightedColor = Color.white,
			normalColor = Color.white,
			pressedColor = Color.white,
			fadeDuration = 0.1f
		};

		[SerializeField]
		[Tooltip("Settings using for Sprite State transitions.")]
		[CustomObfuscation(rename = false)]
		private SpriteState _transitionSpriteState;

		[CustomObfuscation(rename = false)]
		[SerializeField]
		[Tooltip("Settings using for Animation Trigger transitions.")]
		private AnimationTriggers _transitionAnimationTriggers = new AnimationTriggers();

		[CustomObfuscation(rename = false)]
		[Tooltip("The target Graphic component for interaction state transitions. This should normally be set to an Image component on this GameObject.")]
		[SerializeField]
		private Graphic _targetGraphic;

		[SerializeField]
		[Tooltip("Toggles whether the fade duration is set by incoming transition events. If enabled, the duration of fades for visibility and Color Tint transitions will be synchronized with the event sender.")]
		[CustomObfuscation(rename = false)]
		private bool _syncFadeDurationWithTransitionEvent = true;

		[CustomObfuscation(rename = false)]
		[SerializeField]
		[Tooltip("Toggles whether the color tint is set by incoming transition events. If enabled, the color tint transition of the event sender will override any color tint setting here. This setting overrides Sync Fade Duration With Transition Event.")]
		private bool _syncColorTintWithTransitionEvent;

		private TouchInteractable.InteractionState rVRfQdITyYZlOdhPVAqzuEZkDFP;

		public bool visible
		{
			get
			{
				return _visible;
			}
			set
			{
				if (visible == value)
				{
					return;
				}
				while (true)
				{
					DslcJwXTzoxJrqDkiUIuuiJQYNZ(value, false);
					bFoWQuDzcOvecUqeSCdTCzgfmRIE();
					int num = 320429314;
					while (true)
					{
						switch (num ^ 0x13195D02)
						{
						default:
							return;
						case 0:
							return;
						case 2:
							goto IL_000a;
						case 1:
							break;
						}
						break;
						IL_000a:
						num = 320429315;
					}
				}
			}
		}

		public TouchInteractable.TransitionTypeFlags transitionType
		{
			get
			{
				return _transitionType;
			}
			set
			{
				if (_transitionType != value)
				{
					_transitionType = value;
					bFoWQuDzcOvecUqeSCdTCzgfmRIE();
				}
			}
		}

		public ColorBlock transitionColorTint
		{
			get
			{
				return _transitionColorTint;
			}
			set
			{
				_transitionColorTint = value;
				bFoWQuDzcOvecUqeSCdTCzgfmRIE();
			}
		}

		public SpriteState transitionSpriteState
		{
			get
			{
				return _transitionSpriteState;
			}
			set
			{
				//Discarded unreachable code: IL_0031
				if (_transitionSpriteState.Equals(value))
				{
					goto IL_000e;
				}
				goto IL_0038;
				IL_0038:
				_transitionSpriteState = value;
				int num = -345143352;
				goto IL_0013;
				IL_0013:
				switch (num ^ -345143351)
				{
				case 2:
					return;
				case 0:
					break;
				case 3:
					goto IL_0038;
				default:
					bFoWQuDzcOvecUqeSCdTCzgfmRIE();
					return;
				}
				goto IL_000e;
				IL_000e:
				num = -345143349;
				goto IL_0013;
			}
		}

		public AnimationTriggers transitionAnimationTriggers
		{
			get
			{
				return _transitionAnimationTriggers;
			}
			set
			{
				if (_transitionAnimationTriggers == value)
				{
					return;
				}
				while (true)
				{
					_transitionAnimationTriggers = value;
					int num = -18969713;
					while (true)
					{
						switch (num ^ -18969713)
						{
						case 2:
							goto IL_000a;
						case 1:
							break;
						default:
							bFoWQuDzcOvecUqeSCdTCzgfmRIE();
							return;
						}
						break;
						IL_000a:
						num = -18969714;
					}
				}
			}
		}

		public Graphic targetGraphic
		{
			get
			{
				return _targetGraphic;
			}
			set
			{
				if (_targetGraphic == value)
				{
					return;
				}
				while (true)
				{
					_targetGraphic = value;
					bFoWQuDzcOvecUqeSCdTCzgfmRIE();
					int num = -2049629276;
					while (true)
					{
						switch (num ^ -2049629274)
						{
						default:
							return;
						case 2:
							return;
						case 0:
							goto IL_000f;
						case 1:
							break;
						}
						break;
						IL_000f:
						num = -2049629273;
					}
				}
			}
		}

		public bool syncFadeDurationWithTransitionEvent
		{
			get
			{
				return _syncFadeDurationWithTransitionEvent;
			}
			set
			{
				if (_syncFadeDurationWithTransitionEvent == value)
				{
					return;
				}
				while (true)
				{
					_syncFadeDurationWithTransitionEvent = value;
					bFoWQuDzcOvecUqeSCdTCzgfmRIE();
					int num = -992796202;
					while (true)
					{
						switch (num ^ -992796201)
						{
						default:
							return;
						case 1:
							return;
						case 0:
							goto IL_000a;
						case 2:
							break;
						}
						break;
						IL_000a:
						num = -992796203;
					}
				}
			}
		}

		public bool syncColorTintWithTransitionEvent
		{
			get
			{
				return _syncColorTintWithTransitionEvent;
			}
			set
			{
				//Discarded unreachable code: IL_0028
				if (_syncColorTintWithTransitionEvent == value)
				{
					while (true)
					{
						switch (0xC262473 ^ 0xC262471)
						{
						case 2:
							return;
						case 0:
							continue;
						}
						break;
					}
				}
				_syncColorTintWithTransitionEvent = value;
				bFoWQuDzcOvecUqeSCdTCzgfmRIE();
			}
		}

		public Image image
		{
			get
			{
				return _targetGraphic as Image;
			}
			set
			{
				if (!(_targetGraphic == value))
				{
					_targetGraphic = value;
					bFoWQuDzcOvecUqeSCdTCzgfmRIE();
				}
			}
		}

		public Animator animator => base.gameObject.GetComponent<Animator>();

		[CustomObfuscation(rename = false)]
		private TouchInteractableTransitioner()
		{
		}

		[CustomObfuscation(rename = false)]
		private void Awake()
		{
			if (!Application.isPlaying)
			{
				return;
			}
			while (_targetGraphic == null)
			{
				_targetGraphic = base.gameObject.GetComponent<Graphic>();
				int num = 1793698565;
				while (true)
				{
					switch (num ^ 0x6AE9AB05)
					{
					case 2:
						num = 1793698564;
						continue;
					case 1:
						break;
					default:
						goto end_IL_0026;
					}
					break;
				}
				continue;
				end_IL_0026:
				break;
			}
			DslcJwXTzoxJrqDkiUIuuiJQYNZ(_visible, true);
		}

		[CustomObfuscation(rename = false)]
		private void OnEnable()
		{
			if (!Application.isPlaying)
			{
				DslcJwXTzoxJrqDkiUIuuiJQYNZ(_visible, true);
			}
			qKRTkfOGLUHHYELogNnEEhiEJte(true);
		}

		[CustomObfuscation(rename = false)]
		private void OnDisable()
		{
			yrErPSHhGnOuznuHirRTpPkmJMI();
		}

		[CustomObfuscation(rename = false)]
		private void OnValidate()
		{
			_transitionColorTint.fadeDuration = Mathf.Max(_transitionColorTint.fadeDuration, 0f);
			if (UnityTools.IsActiveAndEnabled(this))
			{
				YYvfGdnVUAOPddcwvcrImIcuboG(null);
				LJUhxiZrQAfWgFGuXVojBKYVgGz(Color.white, true);
				while (true)
				{
					int num = 220357905;
					while (true)
					{
						switch (num ^ 0xD226513)
						{
						case 0:
							break;
						case 2:
							DXZIsWKrjVFvmONoAmLYyqbnSYC(_transitionAnimationTriggers.normalTrigger);
							qKRTkfOGLUHHYELogNnEEhiEJte(true);
							num = 220357906;
							continue;
						default:
							goto IL_0078;
						}
						break;
					}
				}
			}
			goto IL_0078;
			IL_0078:
			GBExVtHFPRLrKdqZIHqEWcjtFsS();
		}

		[CustomObfuscation(rename = false)]
		private void Reset()
		{
			_targetGraphic = base.gameObject.GetComponent<Graphic>();
		}

		[CustomObfuscation(rename = false)]
		private void OnCanvasGroupWasChanged()
		{
			GBExVtHFPRLrKdqZIHqEWcjtFsS();
		}

		[CustomObfuscation(rename = false)]
		private void OnAnimationPropertiesWereApplied()
		{
			GBExVtHFPRLrKdqZIHqEWcjtFsS();
		}

		private void bFoWQuDzcOvecUqeSCdTCzgfmRIE()
		{
			GBExVtHFPRLrKdqZIHqEWcjtFsS();
		}

		private void GBExVtHFPRLrKdqZIHqEWcjtFsS()
		{
			//Discarded unreachable code: IL_0038
			if (!Application.isPlaying)
			{
				while (true)
				{
					int num = 2074576064;
					while (true)
					{
						switch (num ^ 0x7BA784C1)
						{
						case 0:
							return;
						case 3:
							break;
						case 1:
							qKRTkfOGLUHHYELogNnEEhiEJte(true);
							num = 2074576065;
							continue;
						default:
							goto IL_003f;
						}
						break;
					}
				}
			}
			goto IL_003f;
			IL_003f:
			qKRTkfOGLUHHYELogNnEEhiEJte(false);
		}

		private void qKRTkfOGLUHHYELogNnEEhiEJte(bool P_0)
		{
			ZfPVPoqRQiwHjlckPfDuMoPdZcW(rVRfQdITyYZlOdhPVAqzuEZkDFP, P_0);
		}

		private void DslcJwXTzoxJrqDkiUIuuiJQYNZ(bool P_0, bool P_1)
		{
			//Discarded unreachable code: IL_002b
			if (_visible == P_0)
			{
				while (true)
				{
					switch (0x1C7D199D ^ 0x1C7D199C)
					{
					case 0:
						continue;
					case 1:
						if (!P_1)
						{
							return;
						}
						break;
					}
					break;
				}
			}
			_visible = P_0;
		}

		private bool oYmKlXCbdsrYRVmzKwaXeFOfiCi()
		{
			return UnityTools.IsActiveAndEnabled(this);
		}

		private void yrErPSHhGnOuznuHirRTpPkmJMI()
		{
			string normalTrigger = _transitionAnimationTriggers.normalTrigger;
			while (true)
			{
				int num = 1036219156;
				while (true)
				{
					switch (num ^ 0x3DC37317)
					{
					default:
						return;
					case 4:
						return;
					case 0:
						break;
					case 3:
						if ((_transitionType & TouchInteractable.TransitionTypeFlags.ColorTint) != 0)
						{
							LJUhxiZrQAfWgFGuXVojBKYVgGz(Color.white, true);
							num = 1036219158;
							continue;
						}
						goto case 1;
					case 1:
						if ((_transitionType & TouchInteractable.TransitionTypeFlags.SpriteSwap) != 0)
						{
							YYvfGdnVUAOPddcwvcrImIcuboG(null);
							num = 1036219157;
							continue;
						}
						goto case 2;
					case 2:
						if ((_transitionType & TouchInteractable.TransitionTypeFlags.Animation) != 0)
						{
							DXZIsWKrjVFvmONoAmLYyqbnSYC(normalTrigger);
							num = 1036219155;
							continue;
						}
						return;
					}
					break;
				}
			}
		}

		private void ZfPVPoqRQiwHjlckPfDuMoPdZcW(TouchInteractable.InteractionState P_0, bool P_1)
		{
			Color color = default(Color);
			Sprite sprite = default(Sprite);
			string text = default(string);
			bool flag = default(bool);
			while (true)
			{
				int num = -1025029950;
				while (true)
				{
					switch (num ^ -1025029933)
					{
					default:
						return;
					case 1:
						return;
					case 6:
						break;
					case 11:
						color = Color.black;
						num = -1025029932;
						continue;
					case 8:
						color.a = 0f;
						num = -1025029929;
						continue;
					case 19:
						color = _transitionColorTint.pressedColor;
						sprite = _transitionSpriteState.pressedSprite;
						num = -1025029921;
						continue;
					case 14:
						if ((_transitionType & TouchInteractable.TransitionTypeFlags.Animation) != 0)
						{
							DXZIsWKrjVFvmONoAmLYyqbnSYC(text);
							num = -1025029934;
							continue;
						}
						return;
					case 7:
						sprite = null;
						num = -1025029930;
						continue;
					case 20:
						if ((_transitionType & TouchInteractable.TransitionTypeFlags.SpriteSwap) != 0)
						{
							YYvfGdnVUAOPddcwvcrImIcuboG(sprite);
							num = -1025029923;
							continue;
						}
						goto case 14;
					case 0:
						text = _transitionAnimationTriggers.highlightedTrigger;
						num = -1025029936;
						continue;
					case 13:
					{
						int num2;
						if (_visible)
						{
							num = -1025029929;
							num2 = num;
						}
						else
						{
							num = -1025029925;
							num2 = num;
						}
						continue;
					}
					case 10:
						goto IL_0128;
					case 16:
						text = _transitionAnimationTriggers.normalTrigger;
						num = -1025029936;
						continue;
					case 12:
						text = _transitionAnimationTriggers.pressedTrigger;
						num = -1025029936;
						continue;
					case 2:
						sprite = _transitionSpriteState.highlightedSprite;
						num = -1025029933;
						continue;
					case 3:
						flag = ((_transitionType & TouchInteractable.TransitionTypeFlags.ColorTint) != 0);
						if (!flag)
						{
							color = Color.white;
							num = -1025029922;
							continue;
						}
						goto case 13;
					case 18:
						goto IL_01bd;
					case 9:
						goto IL_01d3;
					case 17:
						switch (P_0)
						{
						case TouchInteractable.InteractionState.Pressed:
							break;
						case TouchInteractable.InteractionState.Disabled:
							goto IL_0128;
						case TouchInteractable.InteractionState.Highlighted:
							goto IL_01bd;
						case TouchInteractable.InteractionState.Normal:
							goto IL_01d3;
						default:
							goto IL_0202;
						}
						goto case 19;
					case 4:
						if (base.gameObject.activeInHierarchy)
						{
							if (flag)
							{
								LJUhxiZrQAfWgFGuXVojBKYVgGz(color * _transitionColorTint.colorMultiplier, P_1);
								num = -1025029945;
								continue;
							}
							goto case 15;
						}
						return;
					case 15:
						LJUhxiZrQAfWgFGuXVojBKYVgGz(color, P_1);
						num = -1025029945;
						continue;
					case 5:
						{
							text = string.Empty;
							num = -1025029936;
							continue;
						}
						IL_0202:
						num = -1025029928;
						continue;
						IL_01d3:
						color = _transitionColorTint.normalColor;
						sprite = null;
						num = -1025029949;
						continue;
						IL_01bd:
						color = _transitionColorTint.highlightedColor;
						num = -1025029935;
						continue;
						IL_0128:
						color = _transitionColorTint.disabledColor;
						sprite = _transitionSpriteState.disabledSprite;
						text = _transitionAnimationTriggers.disabledTrigger;
						num = -1025029936;
						continue;
					}
					break;
				}
			}
		}

		private void LJUhxiZrQAfWgFGuXVojBKYVgGz(Color P_0, bool P_1)
		{
			if (!(_targetGraphic == null))
			{
				_targetGraphic.CrossFadeColor(P_0, P_1 ? 0f : _transitionColorTint.fadeDuration, ignoreTimeScale: true, useAlpha: true);
			}
		}

		private void YYvfGdnVUAOPddcwvcrImIcuboG(Sprite P_0)
		{
			if (image == null)
			{
				return;
			}
			while (true)
			{
				image.overrideSprite = P_0;
				int num = 1673947251;
				while (true)
				{
					switch (num ^ 0x63C66872)
					{
					default:
						return;
					case 1:
						return;
					case 0:
						goto IL_000f;
					case 2:
						break;
					}
					break;
					IL_000f:
					num = 1673947248;
				}
			}
		}

		private void DXZIsWKrjVFvmONoAmLYyqbnSYC(string P_0)
		{
			//Discarded unreachable code: IL_00a2
			if ((_transitionType & TouchInteractable.TransitionTypeFlags.Animation) == 0 || animator == null)
			{
				return;
			}
			while (true)
			{
				int num = -456210026;
				while (true)
				{
					switch (num ^ -456210027)
					{
					case 0:
						return;
					case 2:
						break;
					case 3:
						if (UnityTools.IsActiveAndEnabled(animator) && !(animator.runtimeAnimatorController == null))
						{
							int num2;
							if (string.IsNullOrEmpty(P_0))
							{
								num = -456210027;
								num2 = num;
							}
							else
							{
								num = -456210032;
								num2 = num;
							}
							continue;
						}
						return;
					case 5:
						animator.ResetTrigger(_transitionAnimationTriggers.normalTrigger);
						num = -456210031;
						continue;
					case 4:
						animator.ResetTrigger(_transitionAnimationTriggers.pressedTrigger);
						animator.ResetTrigger(_transitionAnimationTriggers.highlightedTrigger);
						animator.ResetTrigger(_transitionAnimationTriggers.disabledTrigger);
						num = -456210028;
						continue;
					default:
						animator.SetTrigger(P_0);
						return;
					}
					break;
				}
			}
		}

		public void OnInteractionStateTransition(TouchInteractable.InteractionStateTransitionArgs args)
		{
			//Discarded unreachable code: IL_005b
			rVRfQdITyYZlOdhPVAqzuEZkDFP = args.state;
			if (_syncFadeDurationWithTransitionEvent)
			{
				goto IL_0017;
			}
			goto IL_00cb;
			IL_00cb:
			int num;
			if (_syncColorTintWithTransitionEvent)
			{
				int num2;
				if ((_transitionType & TouchInteractable.TransitionTypeFlags.ColorTint) != 0)
				{
					num = -1060608742;
					num2 = num;
				}
				else
				{
					num = -1060608738;
					num2 = num;
				}
				goto IL_001c;
			}
			goto IL_004c;
			IL_0017:
			num = -1060608737;
			goto IL_001c;
			IL_00a3:
			OnValidate();
			num = -1060608743;
			goto IL_001c;
			IL_001c:
			while (true)
			{
				switch (num ^ -1060608739)
				{
				default:
					return;
				case 4:
					return;
				case 6:
					break;
				case 0:
					goto IL_004c;
				case 7:
					if (args.sender != null)
					{
						_transitionColorTint = args.sender.transitionColorTint;
						num = -1060608739;
						continue;
					}
					goto IL_004c;
				case 2:
					_transitionColorTint.fadeDuration = args.duration;
					num = -1060608740;
					continue;
				case 5:
					goto IL_00a3;
				case 3:
					_transitionType |= TouchInteractable.TransitionTypeFlags.ColorTint;
					num = -1060608742;
					continue;
				case 1:
					goto IL_00cb;
				}
				break;
			}
			goto IL_0017;
			IL_004c:
			if (Application.isPlaying)
			{
				qKRTkfOGLUHHYELogNnEEhiEJte(false);
				return;
			}
			goto IL_00a3;
		}

		public void OnVisibilityChanged(bool state)
		{
			visible = state;
		}
	}
}
