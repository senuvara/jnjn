using Rewired.Utils;
using Rewired.Utils.Interfaces;
using System.Collections.Generic;
using UnityEngine;

namespace Rewired.ComponentControls.Effects
{
	[DisallowMultipleComponent]
	[RequireComponent(typeof(RectTransform))]
	[ExecuteInEditMode]
	public sealed class TouchJoystickRadialIndicator : MonoBehaviour, IRegistrar<TouchJoystickAngleIndicator>
	{
		[Tooltip("If enabled, the indicators will be scaled based on the size of the RectTransform.")]
		public bool _scale = true;

		[Tooltip("If enabled, the aspect ratio will be determined from the Sprite's texture.")]
		public bool _preserveSpriteAspectRatio;

		[Range(0.01f, 1f)]
		[Tooltip("The scale ratio of the indicators to the current RectTransform's height. A ratio of 0.1 means the indicator will be 0.1 times the size of the RectTransform's height. This is useful if you need to be able to scale the transform and have the indicators also scale with it.")]
		public float _scaleRatio = 0.1f;

		[Range(0.01f, 10f)]
		[Tooltip("The horizontal component of the desired aspect ratio of the indicator.")]
		public float _aspectRatioX = 1f;

		[Range(0.01f, 10f)]
		[Tooltip("The vertical component of the desired aspect ratio of the indicator.")]
		public float _aspectRatioY = 1f;

		[Tooltip("Offsets the indicator position up by this proportion of its height. 1.0 = 1 unit high offset.")]
		public float _offset;

		private static readonly Vector2 qanXkdGdoSzGBmfmjMHsltxfCdY = new Vector2(0.5f, 0.5f);

		private RectTransform mgsrwkGejIMmMMAQCHbhFpCCJTb;

		private List<TouchJoystickAngleIndicator> cDtbKfGNAOMXrFsdIXhMvfBtual = new List<TouchJoystickAngleIndicator>(8);

		public bool scale
		{
			get
			{
				return _scale;
			}
			set
			{
				if (_scale == value)
				{
					return;
				}
				while (true)
				{
					_scale = value;
					bFoWQuDzcOvecUqeSCdTCzgfmRIE();
					int num = -1362057047;
					while (true)
					{
						switch (num ^ -1362057045)
						{
						default:
							return;
						case 2:
							return;
						case 0:
							goto IL_000a;
						case 1:
							break;
						}
						break;
						IL_000a:
						num = -1362057046;
					}
				}
			}
		}

		public bool preserveSpriteAspectRatio
		{
			get
			{
				return _preserveSpriteAspectRatio;
			}
			set
			{
				if (_preserveSpriteAspectRatio != value)
				{
					_preserveSpriteAspectRatio = value;
					bFoWQuDzcOvecUqeSCdTCzgfmRIE();
				}
			}
		}

		public float scaleRatio
		{
			get
			{
				return _scaleRatio;
			}
			set
			{
				//Discarded unreachable code: IL_003e
				value = MathTools.Clamp(value, 0.01f, 1f);
				if (_scaleRatio == value)
				{
					goto IL_001b;
				}
				goto IL_0045;
				IL_0045:
				_scaleRatio = value;
				int num = -366444814;
				goto IL_0020;
				IL_0020:
				switch (num ^ -366444816)
				{
				case 1:
					return;
				case 0:
					break;
				case 3:
					goto IL_0045;
				default:
					bFoWQuDzcOvecUqeSCdTCzgfmRIE();
					return;
				}
				goto IL_001b;
				IL_001b:
				num = -366444815;
				goto IL_0020;
			}
		}

		public float aspectRatioX
		{
			get
			{
				return _aspectRatioX;
			}
			set
			{
				value = MathTools.Clamp(value, 0.01f, 10f);
				if (_aspectRatioX != value)
				{
					_aspectRatioX = value;
					bFoWQuDzcOvecUqeSCdTCzgfmRIE();
				}
			}
		}

		public float aspectRatioY
		{
			get
			{
				return _aspectRatioY;
			}
			set
			{
				//Discarded unreachable code: IL_003a
				value = MathTools.Clamp(value, 0.01f, 10f);
				while (true)
				{
					switch (-323210209 ^ -323210211)
					{
					case 0:
						continue;
					case 2:
						if (_aspectRatioY == value)
						{
							return;
						}
						break;
					}
					break;
				}
				_aspectRatioY = value;
				bFoWQuDzcOvecUqeSCdTCzgfmRIE();
			}
		}

		public float offset
		{
			get
			{
				return _offset;
			}
			set
			{
				//Discarded unreachable code: IL_0028
				if (_offset == value)
				{
					while (true)
					{
						switch (-350502834 ^ -350502833)
						{
						case 1:
							return;
						case 0:
							continue;
						}
						break;
					}
				}
				_offset = value;
				bFoWQuDzcOvecUqeSCdTCzgfmRIE();
			}
		}

		private RectTransform rectTransform => mgsrwkGejIMmMMAQCHbhFpCCJTb ?? (mgsrwkGejIMmMMAQCHbhFpCCJTb = GetComponent<RectTransform>());

		void IRegistrar<TouchJoystickAngleIndicator>.Register(TouchJoystickAngleIndicator P_0)
		{
			//Discarded unreachable code: IL_0043
			if (P_0 == null)
			{
				return;
			}
			while (true)
			{
				int num;
				int num2;
				if (!ListTools.AddIfUnique(cDtbKfGNAOMXrFsdIXhMvfBtual, P_0))
				{
					num = 377390400;
					num2 = num;
				}
				else
				{
					num = 377390406;
					num2 = num;
				}
				while (true)
				{
					switch (num ^ 0x167E8544)
					{
					default:
						return;
					case 0:
						return;
					case 4:
						return;
					case 5:
						num = 377390405;
						continue;
					case 3:
						DbDVesDPXlejyIwdmYYmGHNqkyZe(P_0);
						num = 377390400;
						continue;
					case 2:
					{
						int num3;
						if (!base.enabled)
						{
							num = 377390404;
							num3 = num;
						}
						else
						{
							num = 377390407;
							num3 = num;
						}
						continue;
					}
					case 1:
						break;
					}
					break;
				}
			}
		}

		void IRegistrar<TouchJoystickAngleIndicator>.Deregister(TouchJoystickAngleIndicator P_0)
		{
			if (P_0 == null)
			{
				return;
			}
			while (true)
			{
				cDtbKfGNAOMXrFsdIXhMvfBtual.Remove(P_0);
				int num = 1026755272;
				while (true)
				{
					switch (num ^ 0x3D330AC9)
					{
					default:
						return;
					case 1:
						return;
					case 0:
						goto IL_000a;
					case 2:
						break;
					}
					break;
					IL_000a:
					num = 1026755275;
				}
			}
		}

		[CustomObfuscation(rename = false)]
		private void Update()
		{
			XIRSwWTnFJsUXHyqFRyfFkBtEBk();
		}

		[CustomObfuscation(rename = false)]
		private void OnValidate()
		{
			//Discarded unreachable code: IL_002b
			if (!base.enabled)
			{
				goto IL_0008;
			}
			goto IL_0032;
			IL_0032:
			wOXgSlwNtJASAdzsUeQbAdFJWkG();
			XIRSwWTnFJsUXHyqFRyfFkBtEBk();
			int num = -874772417;
			goto IL_000d;
			IL_000d:
			switch (num ^ -874772418)
			{
			default:
				return;
			case 1:
				return;
			case 2:
				return;
			case 0:
				break;
			case 3:
				goto IL_0032;
			}
			goto IL_0008;
			IL_0008:
			num = -874772420;
			goto IL_000d;
		}

		[CustomObfuscation(rename = false)]
		private void OnEnable()
		{
			XIRSwWTnFJsUXHyqFRyfFkBtEBk();
		}

		[CustomObfuscation(rename = false)]
		private void OnDestroy()
		{
			cDtbKfGNAOMXrFsdIXhMvfBtual.Clear();
		}

		private void XIRSwWTnFJsUXHyqFRyfFkBtEBk()
		{
			int num = cDtbKfGNAOMXrFsdIXhMvfBtual.Count - 1;
			while (num >= 0)
			{
				while (true)
				{
					TouchJoystickAngleIndicator touchJoystickAngleIndicator = cDtbKfGNAOMXrFsdIXhMvfBtual[num];
					int num2;
					if (touchJoystickAngleIndicator.image.IsNullOrDestroyed())
					{
						cDtbKfGNAOMXrFsdIXhMvfBtual.RemoveAt(num);
						num2 = 1270272305;
						goto IL_0015;
					}
					goto IL_0067;
					IL_0015:
					while (true)
					{
						switch (num2 ^ 0x4BB6D132)
						{
						case 2:
							num2 = 1270272307;
							continue;
						case 1:
							break;
						case 4:
							goto IL_0067;
						case 3:
							num2 = 1270272306;
							continue;
						case 0:
							num--;
							num2 = 1270272311;
							continue;
						default:
							goto IL_0087;
						}
						break;
					}
					continue;
					IL_0067:
					DbDVesDPXlejyIwdmYYmGHNqkyZe(touchJoystickAngleIndicator);
					num2 = 1270272306;
					goto IL_0015;
				}
				IL_0087:;
			}
		}

		private void DbDVesDPXlejyIwdmYYmGHNqkyZe(TouchJoystickAngleIndicator P_0)
		{
			//Discarded unreachable code: IL_018e, IL_01a5
			if (!UnityTools.IsActiveAndEnabled(P_0.image))
			{
				return;
			}
			float num = default(float);
			Vector2 pivot = default(Vector2);
			float num3 = default(float);
			while (true)
			{
				IL_0178:
				RectTransform rectTransform = P_0.rectTransform;
				if (rectTransform == this.rectTransform)
				{
					break;
				}
				while (!(rectTransform == null))
				{
					while (true)
					{
						Rect rect = this.rectTransform.rect;
						int num2;
						if (_scale)
						{
							num = (num = _aspectRatioX / _aspectRatioY);
							num2 = 160579942;
							goto IL_0016;
						}
						goto IL_011c;
						IL_0016:
						while (true)
						{
							switch (num2 ^ 0x9924167)
							{
							case 6:
								num2 = 160579939;
								continue;
							case 1:
							{
								if (_preserveSpriteAspectRatio && P_0.EdmMylnAfIqOdzIptshYWAZuDsN(out Vector2 vector3))
								{
									num = vector3.x / vector3.y;
									num2 = 160579951;
									continue;
								}
								goto case 8;
							}
							case 8:
							{
								Vector2 vector2 = rectTransform.sizeDelta = new Vector2(rect.height * _scaleRatio * num, rect.height * _scaleRatio);
								num2 = 160579949;
								continue;
							}
							case 3:
								if (rectTransform.anchorMin != qanXkdGdoSzGBmfmjMHsltxfCdY)
								{
									rectTransform.anchorMin = qanXkdGdoSzGBmfmjMHsltxfCdY;
									num2 = 160579943;
									continue;
								}
								goto case 0;
							case 7:
								break;
							case 9:
								pivot = rectTransform.pivot;
								num2 = 160579938;
								continue;
							case 10:
								goto IL_011c;
							case 0:
								if (rectTransform.anchorMax != qanXkdGdoSzGBmfmjMHsltxfCdY)
								{
									rectTransform.anchorMax = qanXkdGdoSzGBmfmjMHsltxfCdY;
									num2 = 160579950;
									continue;
								}
								goto case 9;
							case 4:
								goto IL_0178;
							case 2:
								goto IL_0198;
							default:
								pivot.x = 0.5f;
								pivot.y = num3 + _offset * -1f;
								rectTransform.pivot = pivot;
								return;
							}
							break;
						}
						continue;
						IL_011c:
						num3 = (rect.height / 2f / rectTransform.rect.height - 1f) * -1f;
						num2 = 160579940;
						goto IL_0016;
					}
					IL_0198:;
				}
				break;
			}
		}

		private void bFoWQuDzcOvecUqeSCdTCzgfmRIE()
		{
			XIRSwWTnFJsUXHyqFRyfFkBtEBk();
		}

		private void wOXgSlwNtJASAdzsUeQbAdFJWkG()
		{
			Transform transform = base.transform;
			cDtbKfGNAOMXrFsdIXhMvfBtual.Clear();
			int childCount = transform.childCount;
			int num2 = default(int);
			while (true)
			{
				int num = 255947010;
				while (true)
				{
					switch (num ^ 0xF417106)
					{
					case 0:
						break;
					case 2:
					{
						Transform child = transform.GetChild(num2);
						TouchJoystickAngleIndicator component = child.GetComponent<TouchJoystickAngleIndicator>();
						if (component != null)
						{
							cDtbKfGNAOMXrFsdIXhMvfBtual.Add(component);
							num = 255947015;
							continue;
						}
						goto case 1;
					}
					case 5:
						num = 255947013;
						continue;
					case 1:
						num2++;
						num = 255947013;
						continue;
					case 4:
						num2 = 0;
						num = 255947011;
						continue;
					default:
						if (num2 >= childCount)
						{
							return;
						}
						goto case 2;
					}
					break;
				}
			}
		}
	}
}
