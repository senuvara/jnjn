using Rewired;
using Rewired.Utils;
using Rewired.Utils.Attributes;
using Rewired.Utils.Libraries.TinyJson;
using System;
using UnityEngine;

[Serializable]
[Preserve]
internal sealed class ZVOPkHmpobXEknQJZShcFQBpbsRa : ISerializationCallbackReceiver
{
	public enum Type
	{
		All,
		ControllerType,
		HardwareType,
		ControllerTemplateType,
		PersistentControllerInstance,
		SessionControllerInstance
	}

	[Serialize(Name = "type")]
	[SerializeField]
	private Type _type;

	[Serialize(Name = "controllerType")]
	[SerializeField]
	private ControllerType _controllerType;

	[SerializeField]
	[Serialize(Name = "guid")]
	private string _guid;

	[Serialize(Name = "hardwareIdentifier")]
	[SerializeField]
	private string _hardwareIdentifier;

	[Serialize(Name = "controllerId")]
	[SerializeField]
	private int _controllerId;

	[NonSerialized]
	private Guid MqDxKrSeQfcPSQqxQBOKDTKKAAT;

	[DoNotSerialize]
	internal bool yvLiueCESwSvNDaZmDXVgPpfypFP => _type != Type.All;

	[DoNotSerialize]
	public Type SSkMtjLDbBWEffiHSpoQpCqRSKa
	{
		get
		{
			return _type;
		}
		set
		{
			if (value != _type)
			{
				RRbxcKdYFJXAjvCWucIgYnaUDIRH();
			}
			_type = value;
		}
	}

	[DoNotSerialize]
	public ControllerType LBvurysqjogiYiEmqeJKfpvGlYKC
	{
		get
		{
			return _controllerType;
		}
		set
		{
			_controllerType = value;
		}
	}

	[DoNotSerialize]
	public Guid nVkzVOEUAmnDHQSyFEShOqagvoy
	{
		get
		{
			if (_type != Type.HardwareType)
			{
				return Guid.Empty;
			}
			return MqDxKrSeQfcPSQqxQBOKDTKKAAT;
		}
		set
		{
			if (_type != Type.ControllerTemplateType)
			{
				Rewired.Logger.LogWarning(string.Concat("hardwareTypeGuid can only be set when type is ", Type.HardwareType, "."), requiredThreadSafety: true);
				return;
			}
			while (true)
			{
				MqDxKrSeQfcPSQqxQBOKDTKKAAT = value;
				int num = 2138212011;
				while (true)
				{
					switch (num ^ 0x7F7286A9)
					{
					case 0:
						goto IL_0025;
					case 1:
						break;
					default:
						_guid = value.ToString();
						return;
					}
					break;
					IL_0025:
					num = 2138212008;
				}
			}
		}
	}

	[DoNotSerialize]
	public string upQXpnQUbbJeOeyxIcrppHWUPGk
	{
		get
		{
			return _hardwareIdentifier;
		}
		set
		{
			_hardwareIdentifier = value;
		}
	}

	[DoNotSerialize]
	public Guid MchhNblcmrtPhWyWsPNINPMvWgg
	{
		get
		{
			if (_type != Type.ControllerTemplateType)
			{
				return Guid.Empty;
			}
			return MqDxKrSeQfcPSQqxQBOKDTKKAAT;
		}
		set
		{
			//Discarded unreachable code: IL_0047
			if (_type != Type.ControllerTemplateType)
			{
				Rewired.Logger.LogWarning(string.Concat("controllerTemplateTypeGuid can only be set when type is ", Type.ControllerTemplateType, "."), requiredThreadSafety: true);
				goto IL_0024;
			}
			goto IL_004e;
			IL_0024:
			int num = -1465520191;
			goto IL_0029;
			IL_004e:
			MqDxKrSeQfcPSQqxQBOKDTKKAAT = value;
			num = -1465520189;
			goto IL_0029;
			IL_0029:
			switch (num ^ -1465520192)
			{
			case 1:
				return;
			case 2:
				break;
			case 0:
				goto IL_004e;
			default:
				_guid = value.ToString();
				return;
			}
			goto IL_0024;
		}
	}

	[DoNotSerialize]
	public Guid ygniJdKbZfDselwVIUyxBnfbedO
	{
		get
		{
			if (_type != Type.PersistentControllerInstance)
			{
				return Guid.Empty;
			}
			return MqDxKrSeQfcPSQqxQBOKDTKKAAT;
		}
		set
		{
			//Discarded unreachable code: IL_0047
			if (_type != Type.PersistentControllerInstance)
			{
				Rewired.Logger.LogWarning(string.Concat("deviceInstanceGuid can only be set when type is ", Type.PersistentControllerInstance, "."), requiredThreadSafety: true);
				goto IL_0024;
			}
			goto IL_004e;
			IL_0024:
			int num = 1303477617;
			goto IL_0029;
			IL_004e:
			MqDxKrSeQfcPSQqxQBOKDTKKAAT = value;
			num = 1303477618;
			goto IL_0029;
			IL_0029:
			switch (num ^ 0x4DB17D73)
			{
			case 2:
				return;
			case 0:
				break;
			case 3:
				goto IL_004e;
			default:
				_guid = value.ToString();
				return;
			}
			goto IL_0024;
		}
	}

	[DoNotSerialize]
	public int FqvFVReNSXRzaPXCDzmBgILZcsS
	{
		get
		{
			return _controllerId;
		}
		set
		{
			_controllerId = value;
		}
	}

	internal ZVOPkHmpobXEknQJZShcFQBpbsRa(Type type)
		: this()
	{
		_type = type;
	}

	public ZVOPkHmpobXEknQJZShcFQBpbsRa()
	{
		_controllerId = -1;
	}

	public bool FLhKTSEmJOeEzkXHoSGatGpGmpHM(Controller P_0)
	{
		if (P_0 == null)
		{
			return false;
		}
		if (_type != 0 && _controllerType != P_0.type)
		{
			return false;
		}
		switch (_type)
		{
		case Type.All:
		case Type.ControllerType:
			return true;
		case Type.HardwareType:
			if (MqDxKrSeQfcPSQqxQBOKDTKKAAT != Guid.Empty)
			{
				int num = 1523459621;
				while (true)
				{
					switch (num ^ 0x5ACE2625)
					{
					case 2:
						goto IL_0047;
					case 1:
						break;
					default:
						return MqDxKrSeQfcPSQqxQBOKDTKKAAT == P_0.hardwareTypeGuid;
					}
					break;
					IL_0047:
					num = 1523459620;
				}
				goto case Type.All;
			}
			if (string.IsNullOrEmpty(_hardwareIdentifier))
			{
				return false;
			}
			return string.Equals(_hardwareIdentifier, P_0.hardwareIdentifier, StringComparison.Ordinal);
		case Type.ControllerTemplateType:
			return P_0.ImplementsTemplate(MqDxKrSeQfcPSQqxQBOKDTKKAAT);
		case Type.PersistentControllerInstance:
			return P_0.deviceInstanceGuid == MqDxKrSeQfcPSQqxQBOKDTKKAAT;
		case Type.SessionControllerInstance:
			return P_0.id == _controllerId;
		default:
			throw new NotImplementedException();
		}
	}

	private void RRbxcKdYFJXAjvCWucIgYnaUDIRH()
	{
		_guid = string.Empty;
		MqDxKrSeQfcPSQqxQBOKDTKKAAT = Guid.Empty;
	}

	void ISerializationCallbackReceiver.OnAfterDeserialize()
	{
		MqDxKrSeQfcPSQqxQBOKDTKKAAT = StringTools.ToGuid(_guid);
	}

	void ISerializationCallbackReceiver.OnBeforeSerialize()
	{
	}

	public static ZVOPkHmpobXEknQJZShcFQBpbsRa anhJRjMEHswMKSYpEuPQzdeCKxh()
	{
		return new ZVOPkHmpobXEknQJZShcFQBpbsRa(Type.All);
	}

	public static ZVOPkHmpobXEknQJZShcFQBpbsRa MSHfTDGPfNGJRomSPLgDvMZNgLIc(ControllerType P_0)
	{
		ZVOPkHmpobXEknQJZShcFQBpbsRa zVOPkHmpobXEknQJZShcFQBpbsRa = new ZVOPkHmpobXEknQJZShcFQBpbsRa(Type.ControllerType);
		zVOPkHmpobXEknQJZShcFQBpbsRa._controllerType = P_0;
		return zVOPkHmpobXEknQJZShcFQBpbsRa;
	}

	public static ZVOPkHmpobXEknQJZShcFQBpbsRa tWHdeuMFuZYnVNbJZohXBESENih(ControllerType P_0, Guid P_1, string P_2)
	{
		ZVOPkHmpobXEknQJZShcFQBpbsRa zVOPkHmpobXEknQJZShcFQBpbsRa = new ZVOPkHmpobXEknQJZShcFQBpbsRa(Type.HardwareType);
		while (true)
		{
			int num = 830578982;
			while (true)
			{
				switch (num ^ 0x3181A127)
				{
				case 3:
					break;
				case 1:
					zVOPkHmpobXEknQJZShcFQBpbsRa._controllerType = P_0;
					num = 830578983;
					continue;
				case 0:
					zVOPkHmpobXEknQJZShcFQBpbsRa.nVkzVOEUAmnDHQSyFEShOqagvoy = P_1;
					num = 830578981;
					continue;
				default:
					zVOPkHmpobXEknQJZShcFQBpbsRa._hardwareIdentifier = P_2;
					return zVOPkHmpobXEknQJZShcFQBpbsRa;
				}
				break;
			}
		}
	}

	public static ZVOPkHmpobXEknQJZShcFQBpbsRa tWHdeuMFuZYnVNbJZohXBESENih(Controller P_0)
	{
		//Discarded unreachable code: IL_002c
		if (P_0 == null)
		{
			while (true)
			{
				switch (-1362918052 ^ -1362918051)
				{
				case 2:
					continue;
				case 1:
					throw new ArgumentNullException("controller");
				}
				break;
			}
		}
		return tWHdeuMFuZYnVNbJZohXBESENih(P_0.type, P_0.hardwareTypeGuid, P_0.hardwareIdentifier);
	}

	public static ZVOPkHmpobXEknQJZShcFQBpbsRa rfMpSHSjuAthRggXMQAYntfbxL(ControllerType P_0, Guid P_1)
	{
		ZVOPkHmpobXEknQJZShcFQBpbsRa zVOPkHmpobXEknQJZShcFQBpbsRa = new ZVOPkHmpobXEknQJZShcFQBpbsRa(Type.ControllerTemplateType);
		zVOPkHmpobXEknQJZShcFQBpbsRa._controllerType = P_0;
		zVOPkHmpobXEknQJZShcFQBpbsRa.MchhNblcmrtPhWyWsPNINPMvWgg = P_1;
		return zVOPkHmpobXEknQJZShcFQBpbsRa;
	}

	public static ZVOPkHmpobXEknQJZShcFQBpbsRa rfMpSHSjuAthRggXMQAYntfbxL(IControllerTemplate P_0)
	{
		if (P_0 == null)
		{
			throw new ArgumentNullException("controllerTemplate");
		}
		return rfMpSHSjuAthRggXMQAYntfbxL(P_0.controller.type, P_0.typeGuid);
	}

	public static ZVOPkHmpobXEknQJZShcFQBpbsRa uuGymNAZmmHyvismlhPXvhzweOQF(ControllerType P_0, Guid P_1)
	{
		ZVOPkHmpobXEknQJZShcFQBpbsRa zVOPkHmpobXEknQJZShcFQBpbsRa = new ZVOPkHmpobXEknQJZShcFQBpbsRa(Type.PersistentControllerInstance);
		while (true)
		{
			int num = 483362605;
			while (true)
			{
				switch (num ^ 0x1CCF872C)
				{
				case 0:
					break;
				case 1:
					goto IL_0025;
				default:
					return zVOPkHmpobXEknQJZShcFQBpbsRa;
				}
				break;
				IL_0025:
				zVOPkHmpobXEknQJZShcFQBpbsRa._controllerType = P_0;
				zVOPkHmpobXEknQJZShcFQBpbsRa.ygniJdKbZfDselwVIUyxBnfbedO = P_1;
				num = 483362606;
			}
		}
	}

	public static ZVOPkHmpobXEknQJZShcFQBpbsRa uuGymNAZmmHyvismlhPXvhzweOQF(Controller P_0)
	{
		if (P_0 == null)
		{
			throw new ArgumentNullException("controller");
		}
		return uuGymNAZmmHyvismlhPXvhzweOQF(P_0.type, P_0.deviceInstanceGuid);
	}

	public static ZVOPkHmpobXEknQJZShcFQBpbsRa XnKcJEItvWEJEoxzamcNIJNKoft(ControllerType P_0, int P_1)
	{
		ZVOPkHmpobXEknQJZShcFQBpbsRa zVOPkHmpobXEknQJZShcFQBpbsRa = new ZVOPkHmpobXEknQJZShcFQBpbsRa(Type.SessionControllerInstance);
		zVOPkHmpobXEknQJZShcFQBpbsRa._controllerType = P_0;
		zVOPkHmpobXEknQJZShcFQBpbsRa._controllerId = P_1;
		return zVOPkHmpobXEknQJZShcFQBpbsRa;
	}

	public static ZVOPkHmpobXEknQJZShcFQBpbsRa XnKcJEItvWEJEoxzamcNIJNKoft(Controller P_0)
	{
		if (P_0 == null)
		{
			throw new ArgumentNullException("controller");
		}
		return XnKcJEItvWEJEoxzamcNIJNKoft(P_0.type, P_0.id);
	}
}
