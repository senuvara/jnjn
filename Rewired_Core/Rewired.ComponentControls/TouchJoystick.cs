using Rewired.ComponentControls.Data;
using Rewired.Internal;
using Rewired.Utils;
using Rewired.Utils.Attributes;
using Rewired.Utils.UI;
using System;
using System.Collections;
using System.Collections.Generic;
using System.Runtime.CompilerServices;
using UnityEngine;
using UnityEngine.Events;
using UnityEngine.EventSystems;

namespace Rewired.ComponentControls
{
	[Serializable]
	[DisallowMultipleComponent]
	public sealed class TouchJoystick : TouchInteractable
	{
		public enum AxisDirection
		{
			Both,
			Horizontal,
			Vertical
		}

		public enum JoystickMode
		{
			Analog,
			Digital
		}

		public enum SnapDirections
		{
			None = 0,
			Four = 4,
			Eight = 8,
			Sixteen = 0x10,
			ThirtyTwo = 0x20,
			SixtyFour = 0x40
		}

		private enum jIEDtUWQxJuHGrwAkWDYJSIGudw
		{
			UHYvAyHcQsOOrxHRGHpDrbrHmPt,
			vZJAaAAZjhfVfUmYHsWMcjdbLrND,
			IkWnGUwOZwdrEJiPezsIYBjiDyT
		}

		private enum kLwRfBUiLzNGfTUHHvPyLdQaSsJ
		{
			PClOLQRhgHgGSKopqNIgjbqoRgUa,
			dHfPQraqXaVZHuxcjEqIITHISaN
		}

		public enum StickBounds
		{
			Circle,
			Square
		}

		[Serializable]
		public class ValueChangedEventHandler : UnityEvent<Vector2>
		{
		}

		[Serializable]
		public class StickPositionChangedEventHandler : UnityEvent<Vector2>
		{
		}

		[Serializable]
		public class TapEventHandler : UnityEvent
		{
		}

		[Serializable]
		public class TouchStartedEventHandler : UnityEvent
		{
		}

		[Serializable]
		public class TouchEndedEventHandler : UnityEvent
		{
		}

		public interface IValueChangedHandler
		{
			void OnValueChanged(Vector2 value);
		}

		public interface IStickPositionChangedHandler
		{
			void OnStickPositionChanged(Vector2 value);
		}

		private const float MAX_MOVE_SPEED = 20f;

		[SerializeField]
		[CustomObfuscation(rename = false)]
		[Tooltip("The Custom Controller element(s) that will receive input values from the joystick's X axis.")]
		private CustomControllerElementTargetSetForFloat _horizontalAxisCustomControllerElement = new CustomControllerElementTargetSetForFloat();

		[SerializeField]
		[CustomObfuscation(rename = false)]
		[Tooltip("The Custom Controller element(s) that will receive input values from the joystick's Y axis.")]
		private CustomControllerElementTargetSetForFloat _verticalAxisCustomControllerElement = new CustomControllerElementTargetSetForFloat();

		[Tooltip("The Custom Controller element that will receive input values from taps.")]
		[CustomObfuscation(rename = false)]
		[SerializeField]
		private CustomControllerElementTargetSetForBoolean _tapCustomControllerElement = new CustomControllerElementTargetSetForBoolean();

		[SerializeField]
		[CustomObfuscation(rename = false)]
		[Tooltip("The Rect Transform of the stick disc. This is moved around by the user when manipulating the joystick.")]
		private RectTransform _stickTransform;

		[SerializeField]
		[CustomObfuscation(rename = false)]
		[Tooltip("The joystick's mode of operation. Set this to Digital to simulate a D-Pad which has only On/Off states. If you want mimic a real D-Pad, you should also set Snap Directions to 8.")]
		private JoystickMode _joystickMode;

		[SerializeField]
		[CustomObfuscation(rename = false)]
		[Range(0f, 1f)]
		[Tooltip("A dead zone which is applied when Stick Mode is set to Digital. This is used to filter out tiny stick movements near 0, 0.")]
		private float _digitalModeDeadZone = 0.3f;

		[Tooltip("The range of movement of the stick in Canvas pixels. The larger the number, the further the stick must be moved from center to register movement.")]
		[SerializeField]
		[CustomObfuscation(rename = false)]
		[Range(0.01f, 1000f)]
		private float _stickRange = 60f;

		[CustomObfuscation(rename = false)]
		[Tooltip("If enabled, the stick range will scale with parent controls. Otherwise, the stick range will remain constant.")]
		[SerializeField]
		private bool _scaleStickRange = true;

		[SerializeField]
		[Tooltip("The shape of the range of movement of the joystick.")]
		[CustomObfuscation(rename = false)]
		private StickBounds _stickBounds;

		[Tooltip("The axis directions in which movement is allowed. You can restrict movement to one or both axes.")]
		[CustomObfuscation(rename = false)]
		[SerializeField]
		private AxisDirection _axesToUse;

		[SerializeField]
		[CustomObfuscation(rename = false)]
		[Tooltip("Snaps joystick movement to a fixed number of directions. This can be used to create a D-Pad, for example, setting it to 4 or 8 directions. If you want a true D-Pad, Stick Mode should be set to digital.")]
		private SnapDirections _snapDirections;

		[Tooltip("If true, the stick disc will snap immediately to the touch position when initially touched. This results in the stick disc being centered to the touch position. This will cause the stick to generate input immediately when touched if not touched perfectly centered.If false, the stick disc will remain in its current position on touch, and when dragged will retain the same offset. The stick's center point will be set to the position of the touch. The initial touch will not cause the stick to pop in any direction.")]
		[SerializeField]
		[CustomObfuscation(rename = false)]
		private bool _snapStickToTouch;

		[CustomObfuscation(rename = false)]
		[SerializeField]
		[Tooltip("If true, the stick will return to the center after it is released. Otherwise, the stick will remain in the last position and continue to return input.")]
		private bool _centerStickOnRelease = true;

		[SerializeField]
		[Tooltip("The underlying Axis 2D.")]
		[CustomObfuscation(rename = false)]
		private StandaloneAxis2D _axis2D = new StandaloneAxis2D();

		[CustomObfuscation(rename = false)]
		[SerializeField]
		[Tooltip("If true, the joystick can be activated by a touch swipe that began in an area outside the joystick region. If false, the joystick can only be activated by a direct touch.")]
		private bool _activateOnSwipeIn;

		[Tooltip("If true, the joystick will stay engaged even if the touch that activated it moves outside the joystick region. If false, the joystick will be released once the touch that activated it moves outside the joystick region.")]
		[SerializeField]
		[CustomObfuscation(rename = false)]
		private bool _stayActiveOnSwipeOut = true;

		[SerializeField]
		[Tooltip("Should taps on the touch pad be processed?")]
		[CustomObfuscation(rename = false)]
		private bool _allowTap;

		[SerializeField]
		[CustomObfuscation(rename = false)]
		[FieldRange(0f, float.MaxValue)]
		[Tooltip("The maximum touch duration allowed for the touch to be considered a tap. A touch that lasts longer than this value will not trigger a tap when released.")]
		private float _tapTimeout = 0.25f;

		[Tooltip("The maximum movement distance allowed in pixels since the touch began for the touch to be considered a tap. [-1 = no limit]")]
		[FieldRange(-1, int.MaxValue)]
		[SerializeField]
		[CustomObfuscation(rename = false)]
		private int _tapDistanceLimit = 10;

		[SerializeField]
		[CustomObfuscation(rename = false)]
		[Tooltip("Optional external region to use for hover/click/touch detection. If set, this region will be used for touch detection instead of or in addition to the joystick's RectTransform. This can be useful if you want a larger area of the screen to act as a joystick.")]
		private TouchRegion _touchRegion;

		[CustomObfuscation(rename = false)]
		[SerializeField]
		[Tooltip("If True, hovers/clicks/touches on the local joystick will be ignored and only Touch Region touches will be used. Otherwise, both touches on the joystick and on the Touch Region will be used. This also applies to mouse hover. This setting has no effect if no Touch Region is set.")]
		private bool _useTouchRegionOnly = true;

		[SerializeField]
		[CustomObfuscation(rename = false)]
		[Tooltip("If True, the joystick will move to the location of the current touch in the Touch Region. This can be used to designate an area of the screen as a hot-spot for a joystick and have the joystick graphics follow the users touches. This only has an effect if a Touch Region is set.")]
		private bool _moveToTouchPosition;

		[Tooltip("If Move To Touch Position is enabled, this will make the joystick return to its original position after the press is released. This only has an effect if a Touch Region is set.")]
		[SerializeField]
		[CustomObfuscation(rename = false)]
		private bool _returnOnRelease = true;

		[SerializeField]
		[CustomObfuscation(rename = false)]
		[Tooltip("If True, the joystick will follow the touch around until released. This setting overrides Move To Touch Position.")]
		private bool _followTouchPosition;

		[SerializeField]
		[Tooltip("Should the joystick animate when moving to the touch point? This only has an effect if Move To Touch Position is True and a Touch Region is set. This setting is ignored if Follow Touch Position is True.")]
		[CustomObfuscation(rename = false)]
		private bool _animateOnMoveToTouch = true;

		[Range(0f, 20f)]
		[Tooltip("The speed at which the joystick will move toward the touch position measured in screens per second (based on the larger of width and height). [1.0 = Move 1 screen/sec]. This only has an effect if Move To Touch Position is True, Animate On Move To Touch is true, and a Touch Region is set. This setting is ignored if Follow Touch Position is True.")]
		[CustomObfuscation(rename = false)]
		[SerializeField]
		private float _moveToTouchSpeed = 2f;

		[CustomObfuscation(rename = false)]
		[Tooltip("Should the joystick animate when moving back to its original position? This only has an effect if Follow Touch Position is True, or if Move To Touch Position is True and a Touch Region is set, and Return on Release is True.")]
		[SerializeField]
		private bool _animateOnReturn = true;

		[SerializeField]
		[CustomObfuscation(rename = false)]
		[Tooltip("The speed at which the joystick will move back toward its original position measured in screens per second (based on the larger of width and height). [1.0 = Move 1 screen/sec]. This only has an effect if Follow Touch Position is True, or if Move To Touch Position is True and a Touch Region is set, and Return on Release and Animate on Return are both True.")]
		[Range(0f, 20f)]
		private float _returnSpeed = 2f;

		[CustomObfuscation(rename = false)]
		[Tooltip("If True, it will attempt to automatically manage Graphic component raycasting for best results based on your current settings.")]
		[SerializeField]
		private bool _manageRaycasting = true;

		private bool _useXAxis;

		private bool _useYAxis;

		private PawhcdAEqKalvcbRBbHZlIgMMIHA.HierarchyEventHelper<IValueChangedHandler, Vector2> _hierarchyValueChangedHandlers;

		private PawhcdAEqKalvcbRBbHZlIgMMIHA.HierarchyEventHelper<IStickPositionChangedHandler, Vector2> _hierarchyStickPositionChangedHandlers;

		private TouchRegion _workingTouchRegion;

		private Vector2 _origAnchoredPosition;

		private Vector2 _origStickAnchoredPosition;

		private Vector2 _lastPressAnchoredPosition;

		private bool _isMoving;

		private bool _isMovedFromDefaultPosition;

		private jIEDtUWQxJuHGrwAkWDYJSIGudw _moveDirection;

		private int _pointerId = int.MinValue;

		private int _realMousePointerId = int.MinValue;

		[NonSerialized]
		private bool yrfVZiGUTfOClpVIfIvNRqImIGGe;

		[NonSerialized]
		private bool BSbeTqiFrdRDtCmjwklbhCLtNiqg;

		private bool _pointerDownIsFake;

		private Vector2 _lastPressStartingValue;

		private kLwRfBUiLzNGfTUHHvPyLdQaSsJ _lastClaimSource;

		private float _touchStartTime;

		private Vector2 _touchStartPosition;

		private IEnumerator _coroutineMove;

		private AownhRbtzBiFgCcEIIjJdDFsEtza _imageRaycastHelper = new AownhRbtzBiFgCcEIIjJdDFsEtza();

		private int _calculatedStickRange_lastUpdatedFrame = -1;

		private int _lastTapFrame = -1;

		private bool _isEligibleForTap;

		private float __calculatedStickRange_cachedValue;

		private Action<jIEDtUWQxJuHGrwAkWDYJSIGudw> __moveStartedDelegate;

		private Action<jIEDtUWQxJuHGrwAkWDYJSIGudw> __moveEndedDelegate;

		[Tooltip("Event sent when the joystick value changes.")]
		[CustomObfuscation(rename = false)]
		[SerializeField]
		private ValueChangedEventHandler _onValueChanged = new ValueChangedEventHandler();

		[SerializeField]
		[CustomObfuscation(rename = false)]
		[Tooltip("Event sent when the joystick's stick position changes.")]
		private ValueChangedEventHandler _onStickPositionChanged = new ValueChangedEventHandler();

		[CustomObfuscation(rename = false)]
		[Tooltip("Event sent when the joystick is touched.")]
		[SerializeField]
		private TouchStartedEventHandler _onTouchStarted = new TouchStartedEventHandler();

		[CustomObfuscation(rename = false)]
		[SerializeField]
		private TouchEndedEventHandler _onTouchEnded = new TouchEndedEventHandler();

		[SerializeField]
		[Tooltip("Event sent when the touch pad is tapped. This event will only be sent if allowTap is True.")]
		[CustomObfuscation(rename = false)]
		private TapEventHandler _onTap = new TapEventHandler();

		private Dictionary<int, PointerEventData> __fakePointerEventData;

		private static PawhcdAEqKalvcbRBbHZlIgMMIHA.EventFunction<IValueChangedHandler, Vector2> __valueChangedHandlerDelegate;

		private static PawhcdAEqKalvcbRBbHZlIgMMIHA.EventFunction<IStickPositionChangedHandler, Vector2> __stickPositionChangedHandlerDelegate;

		public CustomControllerElementTargetSetForFloat horizontalAxisCustomControllerElement => _horizontalAxisCustomControllerElement;

		public CustomControllerElementTargetSetForFloat verticalAxisCustomControllerElement => _verticalAxisCustomControllerElement;

		public CustomControllerElementTargetSetForBoolean tapCustomControllerElement => _tapCustomControllerElement;

		public RectTransform stickTransform
		{
			get
			{
				return _stickTransform;
			}
			set
			{
				if (!(_stickTransform == value))
				{
					_stickTransform = value;
					OnSetProperty();
				}
			}
		}

		public JoystickMode joystickMode
		{
			get
			{
				return _joystickMode;
			}
			set
			{
				//Discarded unreachable code: IL_002c
				if (_joystickMode == value)
				{
					goto IL_0009;
				}
				goto IL_0033;
				IL_0033:
				_joystickMode = value;
				OnSetProperty();
				int num = -938018329;
				goto IL_000e;
				IL_000e:
				switch (num ^ -938018332)
				{
				default:
					return;
				case 1:
					return;
				case 3:
					return;
				case 2:
					break;
				case 0:
					goto IL_0033;
				}
				goto IL_0009;
				IL_0009:
				num = -938018331;
				goto IL_000e;
			}
		}

		public float digitalModeDeadZone
		{
			get
			{
				return _digitalModeDeadZone;
			}
			set
			{
				//Discarded unreachable code: IL_0045
				value = MathTools.Clamp01(value);
				while (true)
				{
					int num = 629123453;
					while (true)
					{
						switch (num ^ 0x257FA97F)
						{
						case 3:
							return;
						case 0:
							break;
						case 2:
						{
							int num2;
							if (_digitalModeDeadZone != value)
							{
								num = 629123454;
								num2 = num;
							}
							else
							{
								num = 629123452;
								num2 = num;
							}
							continue;
						}
						default:
							_digitalModeDeadZone = value;
							OnSetProperty();
							return;
						}
						break;
					}
				}
			}
		}

		public float stickRange
		{
			get
			{
				return _stickRange;
			}
			set
			{
				//Discarded unreachable code: IL_003e
				value = MathTools.Clamp(value, 1f, 1000f);
				if (_stickRange == value)
				{
					goto IL_001b;
				}
				goto IL_0045;
				IL_0045:
				_stickRange = value;
				OnSetProperty();
				int num = 580802717;
				goto IL_0020;
				IL_0020:
				switch (num ^ 0x229E589F)
				{
				default:
					return;
				case 1:
					return;
				case 2:
					return;
				case 3:
					break;
				case 0:
					goto IL_0045;
				}
				goto IL_001b;
				IL_001b:
				num = 580802718;
				goto IL_0020;
			}
		}

		public bool scaleStickRange
		{
			get
			{
				return _scaleStickRange;
			}
			set
			{
				if (_scaleStickRange == value)
				{
					return;
				}
				while (true)
				{
					_scaleStickRange = value;
					OnSetProperty();
					int num = -2005831357;
					while (true)
					{
						switch (num ^ -2005831359)
						{
						default:
							return;
						case 2:
							return;
						case 0:
							goto IL_000a;
						case 1:
							break;
						}
						break;
						IL_000a:
						num = -2005831360;
					}
				}
			}
		}

		private StickBounds stickBounds
		{
			get
			{
				return _stickBounds;
			}
			set
			{
				if (_stickBounds != value)
				{
					_stickBounds = value;
					OnSetProperty();
				}
			}
		}

		public AxisDirection axesToUse
		{
			get
			{
				return _axesToUse;
			}
			set
			{
				if (_axesToUse == value)
				{
					return;
				}
				while (true)
				{
					iSfJagPsCsVhUfDNyhNvbxLabufM(value);
					int num = -122703993;
					while (true)
					{
						switch (num ^ -122703993)
						{
						case 2:
							goto IL_000a;
						case 1:
							break;
						default:
							OnSetProperty();
							return;
						}
						break;
						IL_000a:
						num = -122703994;
					}
				}
			}
		}

		public SnapDirections snapDirections
		{
			get
			{
				return _snapDirections;
			}
			set
			{
				//Discarded unreachable code: IL_0028
				if (_snapDirections == value)
				{
					while (true)
					{
						switch (-1209299575 ^ -1209299576)
						{
						case 1:
							return;
						case 0:
							continue;
						}
						break;
					}
				}
				_snapDirections = value;
				OnSetProperty();
			}
		}

		public bool snapStickToTouch
		{
			get
			{
				return _snapStickToTouch;
			}
			set
			{
				if (_snapStickToTouch == value)
				{
					return;
				}
				while (true)
				{
					_snapStickToTouch = value;
					int num = 730287582;
					while (true)
					{
						switch (num ^ 0x2B874DDC)
						{
						default:
							return;
						case 3:
							return;
						case 0:
							num = 730287581;
							continue;
						case 1:
							break;
						case 2:
							OnSetProperty();
							num = 730287583;
							continue;
						}
						break;
					}
				}
			}
		}

		public bool centerStickOnRelease
		{
			get
			{
				return _centerStickOnRelease;
			}
			set
			{
				//Discarded unreachable code: IL_002c
				if (_centerStickOnRelease == value)
				{
					goto IL_0009;
				}
				goto IL_0033;
				IL_0033:
				_centerStickOnRelease = value;
				OnSetProperty();
				int num = 364136738;
				goto IL_000e;
				IL_000e:
				switch (num ^ 0x15B44920)
				{
				default:
					return;
				case 1:
					return;
				case 2:
					return;
				case 3:
					break;
				case 0:
					goto IL_0033;
				}
				goto IL_0009;
				IL_0009:
				num = 364136737;
				goto IL_000e;
			}
		}

		public bool activateOnSwipeIn
		{
			get
			{
				return _activateOnSwipeIn;
			}
			set
			{
				if (_activateOnSwipeIn == value)
				{
					return;
				}
				while (true)
				{
					_activateOnSwipeIn = value;
					int num = -1887489994;
					while (true)
					{
						switch (num ^ -1887489994)
						{
						case 2:
							goto IL_000a;
						case 1:
							break;
						default:
							OnSetProperty();
							return;
						}
						break;
						IL_000a:
						num = -1887489993;
					}
				}
			}
		}

		public bool stayActiveOnSwipeOut
		{
			get
			{
				if (dFVbcRaqFpJjthtmulsGoBxPsGN())
				{
					return true;
				}
				return _stayActiveOnSwipeOut;
			}
			set
			{
				if (_stayActiveOnSwipeOut != value)
				{
					_stayActiveOnSwipeOut = value;
					OnSetProperty();
				}
			}
		}

		public bool allowTap
		{
			get
			{
				return _allowTap;
			}
			set
			{
				//Discarded unreachable code: IL_002c
				if (_allowTap == value)
				{
					goto IL_0009;
				}
				goto IL_0033;
				IL_0033:
				_allowTap = value;
				OnSetProperty();
				int num = -1729084211;
				goto IL_000e;
				IL_000e:
				switch (num ^ -1729084212)
				{
				default:
					return;
				case 1:
					return;
				case 3:
					return;
				case 2:
					break;
				case 0:
					goto IL_0033;
				}
				goto IL_0009;
				IL_0009:
				num = -1729084209;
				goto IL_000e;
			}
		}

		public float tapTimeout
		{
			get
			{
				return _tapTimeout;
			}
			set
			{
				value = MathTools.Max(0f, value);
				if (_tapTimeout == value)
				{
					return;
				}
				while (true)
				{
					_tapTimeout = value;
					OnSetProperty();
					int num = 960772470;
					while (true)
					{
						switch (num ^ 0x39443974)
						{
						default:
							return;
						case 2:
							return;
						case 0:
							goto IL_0017;
						case 1:
							break;
						}
						break;
						IL_0017:
						num = 960772469;
					}
				}
			}
		}

		public int tapDistanceLimit
		{
			get
			{
				return _tapDistanceLimit;
			}
			set
			{
				//Discarded unreachable code: IL_0035
				value = MathTools.Max(-1, value);
				if (_tapDistanceLimit == value)
				{
					goto IL_0012;
				}
				goto IL_003c;
				IL_003c:
				_tapDistanceLimit = value;
				int num = -891516709;
				goto IL_0017;
				IL_0017:
				switch (num ^ -891516710)
				{
				case 3:
					return;
				case 2:
					break;
				case 0:
					goto IL_003c;
				default:
					OnSetProperty();
					return;
				}
				goto IL_0012;
				IL_0012:
				num = -891516711;
				goto IL_0017;
			}
		}

		public TouchRegion touchRegion
		{
			get
			{
				return _touchRegion;
			}
			set
			{
				if (_touchRegion == value)
				{
					return;
				}
				while (true)
				{
					_touchRegion = value;
					OnSetProperty();
					int num = 622787552;
					while (true)
					{
						switch (num ^ 0x251EFBE1)
						{
						default:
							return;
						case 1:
							return;
						case 0:
							goto IL_000f;
						case 2:
							break;
						}
						break;
						IL_000f:
						num = 622787555;
					}
				}
			}
		}

		public bool useTouchRegionOnly
		{
			get
			{
				return _useTouchRegionOnly;
			}
			set
			{
				if (_useTouchRegionOnly != value)
				{
					_useTouchRegionOnly = value;
					OnSetProperty();
				}
			}
		}

		public bool moveToTouchPosition
		{
			get
			{
				return _moveToTouchPosition;
			}
			set
			{
				if (_moveToTouchPosition != value)
				{
					_moveToTouchPosition = value;
					OnSetProperty();
				}
			}
		}

		public bool returnOnRelease
		{
			get
			{
				return _returnOnRelease;
			}
			set
			{
				if (_returnOnRelease != value)
				{
					_returnOnRelease = value;
					OnSetProperty();
				}
			}
		}

		public bool followTouchPosition
		{
			get
			{
				return _followTouchPosition;
			}
			set
			{
				if (_followTouchPosition != value)
				{
					_followTouchPosition = value;
					OnSetProperty();
				}
			}
		}

		public bool animateOnMoveToTouch
		{
			get
			{
				return _animateOnMoveToTouch;
			}
			set
			{
				if (_animateOnMoveToTouch != value)
				{
					_animateOnMoveToTouch = value;
					OnSetProperty();
				}
			}
		}

		public float moveToTouchSpeed
		{
			get
			{
				return _moveToTouchSpeed;
			}
			set
			{
				//Discarded unreachable code: IL_003a
				value = MathTools.Clamp(value, 0f, 20f);
				while (true)
				{
					switch (-1603912195 ^ -1603912196)
					{
					case 2:
						continue;
					case 1:
						if (_moveToTouchSpeed == value)
						{
							return;
						}
						break;
					}
					break;
				}
				_moveToTouchSpeed = value;
				OnSetProperty();
			}
		}

		public bool animateOnReturn
		{
			get
			{
				return _animateOnReturn;
			}
			set
			{
				if (_animateOnReturn == value)
				{
					return;
				}
				while (true)
				{
					_animateOnReturn = value;
					int num = -816948060;
					while (true)
					{
						switch (num ^ -816948059)
						{
						case 0:
							goto IL_000a;
						case 2:
							break;
						default:
							OnSetProperty();
							return;
						}
						break;
						IL_000a:
						num = -816948057;
					}
				}
			}
		}

		public float returnSpeed
		{
			get
			{
				return _returnSpeed;
			}
			set
			{
				//Discarded unreachable code: IL_003a
				value = MathTools.Clamp(value, 0f, 20f);
				while (true)
				{
					switch (0x5B703AE3 ^ 0x5B703AE1)
					{
					case 0:
						continue;
					case 2:
						if (_returnSpeed == value)
						{
							return;
						}
						break;
					}
					break;
				}
				_returnSpeed = value;
				OnSetProperty();
			}
		}

		public bool manageRaycasting
		{
			get
			{
				return _manageRaycasting;
			}
			set
			{
				//Discarded unreachable code: IL_0034
				if (_manageRaycasting == value)
				{
					goto IL_0009;
				}
				goto IL_003b;
				IL_003b:
				_manageRaycasting = value;
				int num;
				if (value)
				{
					GdUGsVKpTgHEvzMEBLIYtxCfIFr();
					num = -984767494;
					goto IL_000e;
				}
				goto IL_0052;
				IL_0009:
				num = -984767493;
				goto IL_000e;
				IL_0052:
				_imageRaycastHelper.eeKsAmADUDrgsTtlbyhGDfhxCkc();
				num = -984767496;
				goto IL_000e;
				IL_000e:
				while (true)
				{
					switch (num ^ -984767494)
					{
					case 1:
						return;
					case 4:
						break;
					case 5:
						goto IL_003b;
					case 3:
						goto IL_0052;
					case 0:
						num = -984767496;
						continue;
					default:
						OnSetProperty();
						return;
					}
					break;
				}
				goto IL_0009;
			}
		}

		public AxisCalibration horizontalAxisCalibration => _axis2D.xAxis.calibration;

		public AxisCalibration verticalAxisCalibration => _axis2D.yAxis.calibration;

		public Axis2DCalibration deadZoneType => _axis2D.calibration;

		public int pointerId
		{
			get
			{
				return _pointerId;
			}
			set
			{
				_pointerId = value;
			}
		}

		public bool hasPointer => _pointerId != int.MinValue;

		private bool tapValue => _lastTapFrame == Time.frameCount;

		internal StandaloneAxis2D axis2D => _axis2D;

		private Action<jIEDtUWQxJuHGrwAkWDYJSIGudw> moveStartedDelegate
		{
			get
			{
				Action<jIEDtUWQxJuHGrwAkWDYJSIGudw> result = default(Action<jIEDtUWQxJuHGrwAkWDYJSIGudw>);
				if (__moveStartedDelegate == null)
				{
					while (true)
					{
						int num = 1168373181;
						while (true)
						{
							switch (num ^ 0x45A3F5BC)
							{
							case 2:
								break;
							case 1:
								goto IL_0026;
							default:
								return result;
							}
							break;
							IL_0026:
							result = (__moveStartedDelegate = dpevHZjzTtBoDJuKfSBypVLmMZP);
							num = 1168373180;
						}
					}
				}
				return __moveStartedDelegate;
			}
		}

		private Action<jIEDtUWQxJuHGrwAkWDYJSIGudw> moveEndedDelegate
		{
			get
			{
				if (__moveEndedDelegate == null)
				{
					return __moveEndedDelegate = szPdrxBxecWzCAmftCGFBijDAUM;
				}
				return __moveEndedDelegate;
			}
		}

		private int effectivePointerId
		{
			get
			{
				if (_pointerId == int.MinValue)
				{
					return int.MinValue;
				}
				if (_realMousePointerId != int.MinValue)
				{
					return _realMousePointerId;
				}
				return _pointerId;
			}
		}

		private RectTransform touchReferenceTransform
		{
			get
			{
				if (_lastClaimSource != kLwRfBUiLzNGfTUHHvPyLdQaSsJ.dHfPQraqXaVZHuxcjEqIITHISaN)
				{
					return base.transform as RectTransform;
				}
				return base.transform.parent as RectTransform;
			}
		}

		private float calculatedStickRange
		{
			get
			{
				if (Time.frameCount == _calculatedStickRange_lastUpdatedFrame)
				{
					return __calculatedStickRange_cachedValue;
				}
				RectTransform canvasTransform = base.canvasTransform;
				Vector3 lossyScale2 = default(Vector3);
				Vector3 lossyScale = default(Vector3);
				float magnitude = default(float);
				Vector3 a2 = default(Vector3);
				RectTransform touchReferenceTransform = default(RectTransform);
				Vector3 a = default(Vector3);
				Vector3 vector = default(Vector3);
				while (true)
				{
					int num = -961548867;
					while (true)
					{
						switch (num ^ -961548868)
						{
						case 12:
							break;
						case 4:
							if (lossyScale2.y != 0f)
							{
								lossyScale.y /= lossyScale2.y;
								num = -961548869;
								continue;
							}
							goto case 7;
						case 11:
							magnitude = a2.magnitude;
							num = -961548866;
							continue;
						case 5:
							if (_lastClaimSource == kLwRfBUiLzNGfTUHHvPyLdQaSsJ.dHfPQraqXaVZHuxcjEqIITHISaN)
							{
								lossyScale.Scale(base.transform.localScale);
								num = -961548868;
								continue;
							}
							goto case 0;
						case 8:
						{
							int num2;
							if (!_scaleStickRange)
							{
								num = -961548873;
								num2 = num;
							}
							else
							{
								num = -961548875;
								num2 = num;
							}
							continue;
						}
						case 1:
						{
							touchReferenceTransform = this.touchReferenceTransform;
							Vector3 position = new Vector3(0f, _stickRange, 0f);
							a = canvasTransform.TransformPoint(position) - canvasTransform.position;
							num = -961548874;
							continue;
						}
						case 2:
							__calculatedStickRange_cachedValue = magnitude;
							_calculatedStickRange_lastUpdatedFrame = Time.frameCount;
							num = -961548865;
							continue;
						case 0:
							vector = Vector3.Scale(a2, lossyScale);
							num = -961548879;
							continue;
						case 9:
							lossyScale2 = canvasTransform.lossyScale;
							lossyScale = touchReferenceTransform.lossyScale;
							if (lossyScale2.x != 0f)
							{
								lossyScale.x /= lossyScale2.x;
								num = -961548872;
								continue;
							}
							goto case 4;
						case 6:
							lossyScale.z /= lossyScale2.z;
							num = -961548871;
							continue;
						case 7:
						{
							int num3;
							if (lossyScale2.z != 0f)
							{
								num = -961548870;
								num3 = num;
							}
							else
							{
								num = -961548871;
								num3 = num;
							}
							continue;
						}
						case 13:
							magnitude = vector.magnitude;
							num = -961548866;
							continue;
						case 10:
							a2 = touchReferenceTransform.InverseTransformPoint(a + touchReferenceTransform.position);
							num = -961548876;
							continue;
						default:
							return magnitude;
						}
						break;
					}
				}
			}
		}

		internal static PawhcdAEqKalvcbRBbHZlIgMMIHA.EventFunction<IValueChangedHandler, Vector2> valueChangedHandlerDelegate
		{
			get
			{
				if (__valueChangedHandlerDelegate == null)
				{
					while (true)
					{
						int num = 2040466339;
						while (true)
						{
							switch (num ^ 0x799F0BA1)
							{
							case 3:
								break;
							case 2:
								if (CS_0024_003C_003E9__CachedAnonymousMethodDelegate8 == null)
								{
									CS_0024_003C_003E9__CachedAnonymousMethodDelegate8 = delegate(IValueChangedHandler P_0, Vector2 P_1)
									{
										P_0.OnValueChanged(P_1);
									};
									num = 2040466336;
									continue;
								}
								goto case 1;
							case 1:
								__valueChangedHandlerDelegate = CS_0024_003C_003E9__CachedAnonymousMethodDelegate8;
								num = 2040466337;
								continue;
							default:
								goto IL_0059;
							}
							break;
						}
					}
				}
				goto IL_0059;
				IL_0059:
				return __valueChangedHandlerDelegate;
			}
		}

		internal static PawhcdAEqKalvcbRBbHZlIgMMIHA.EventFunction<IStickPositionChangedHandler, Vector2> stickPositionChangedHandlerDelegate
		{
			get
			{
				if (__stickPositionChangedHandlerDelegate == null)
				{
					if (CS_0024_003C_003E9__CachedAnonymousMethodDelegatea == null)
					{
						CS_0024_003C_003E9__CachedAnonymousMethodDelegatea = delegate(IStickPositionChangedHandler P_0, Vector2 P_1)
						{
							P_0.OnStickPositionChanged(P_1);
						};
						goto IL_001f;
					}
					goto IL_003d;
				}
				goto IL_004e;
				IL_001f:
				int num = -80029353;
				goto IL_0024;
				IL_004e:
				return __stickPositionChangedHandlerDelegate;
				IL_003d:
				__stickPositionChangedHandlerDelegate = CS_0024_003C_003E9__CachedAnonymousMethodDelegatea;
				num = -80029356;
				goto IL_0024;
				IL_0024:
				switch (num ^ -80029354)
				{
				case 0:
					break;
				case 1:
					goto IL_003d;
				default:
					goto IL_004e;
				}
				goto IL_001f;
			}
		}

		public event UnityAction<Vector2> ValueChangedEvent
		{
			add
			{
				_onValueChanged.AddListener(value);
			}
			remove
			{
				_onValueChanged.RemoveListener(value);
			}
		}

		public event UnityAction<Vector2> StickPositionChangedEvent
		{
			add
			{
				_onStickPositionChanged.AddListener(value);
			}
			remove
			{
				_onStickPositionChanged.RemoveListener(value);
			}
		}

		public event UnityAction TouchDownEvent
		{
			add
			{
				_onTouchStarted.AddListener(value);
			}
			remove
			{
				_onTouchStarted.RemoveListener(value);
			}
		}

		public event UnityAction TouchUpEvent
		{
			add
			{
				_onTouchEnded.AddListener(value);
			}
			remove
			{
				_onTouchEnded.RemoveListener(value);
			}
		}

		public event UnityAction TapEvent
		{
			add
			{
				_onTap.AddListener(value);
			}
			remove
			{
				_onTap.RemoveListener(value);
			}
		}

		[CustomObfuscation(rename = false)]
		private TouchJoystick()
		{
		}

		public Vector2 GetValue()
		{
			if (!base.initialized)
			{
				return _axis2D.rawZero;
			}
			return _axis2D.value;
		}

		public Vector2 GetRawValue()
		{
			if (!base.initialized)
			{
				return _axis2D.rawZero;
			}
			return _axis2D.rawValue;
		}

		public void SetRawValue(Vector2 value)
		{
			if (!base.initialized)
			{
				return;
			}
			while (true)
			{
				IL_00c3:
				int num;
				if (_joystickMode == JoystickMode.Digital)
				{
					if (value.sqrMagnitude <= _digitalModeDeadZone * _digitalModeDeadZone)
					{
						value.x = 0f;
						num = 161752049;
						goto IL_0011;
					}
					goto IL_0186;
				}
				goto IL_0252;
				IL_0207:
				_axis2D.SetRawValue(_useXAxis ? value.x : 0f, _useYAxis ? value.y : 0f);
				num = 161752032;
				goto IL_0011;
				IL_0011:
				while (true)
				{
					switch (num ^ 0x9A423F1)
					{
					default:
						return;
					case 17:
						return;
					case 3:
						num = 161752052;
						continue;
					case 7:
						break;
					case 13:
						goto IL_00a1;
					case 5:
						goto IL_00c3;
					case 10:
						if (value.x == 0f)
						{
							goto IL_00a1;
						}
						if (MathTools.IsNearZero(value.x, 0.0001f))
						{
							value.x = 0f;
							num = 161752034;
							continue;
						}
						goto case 1;
					case 1:
						if (MathTools.IsNear(value.x, 1f, 0.0001f))
						{
							value.x = 1f;
							num = 161752037;
							continue;
						}
						goto IL_02aa;
					case 9:
						num = 161752062;
						continue;
					case 12:
						value.x = -1f;
						num = 161752060;
						continue;
					case 6:
						goto IL_0186;
					case 0:
						value.y = 0f;
						num = 161752056;
						continue;
					case 20:
						num = 161752060;
						continue;
					case 4:
						if (MathTools.IsNearZero(value.y, 0.0001f))
						{
							value.y = 0f;
							num = 161752063;
							continue;
						}
						break;
					case 16:
						goto IL_01e3;
					case 11:
						goto IL_0207;
					case 14:
						num = 161752033;
						continue;
					case 15:
						goto IL_0252;
					case 2:
						if (MathTools.IsNear(value.y, -1f, 0.0001f))
						{
							value.y = -1f;
							num = 161752033;
							continue;
						}
						goto IL_01e3;
					case 8:
						goto IL_02aa;
					case 18:
						value.y = 1f;
						num = 161752033;
						continue;
					case 19:
						num = 161752060;
						continue;
					}
					int num2;
					if (MathTools.IsNear(value.y, 1f, 0.0001f))
					{
						num = 161752035;
						num2 = num;
					}
					else
					{
						num = 161752051;
						num2 = num;
					}
					continue;
					IL_02aa:
					int num3;
					if (!MathTools.IsNear(value.x, -1f, 0.0001f))
					{
						num = 161752060;
						num3 = num;
					}
					else
					{
						num = 161752061;
						num3 = num;
					}
					continue;
					IL_00a1:
					int num4;
					if (value.y != 0f)
					{
						num = 161752053;
						num4 = num;
					}
					else
					{
						num = 161752033;
						num4 = num;
					}
				}
				IL_0252:
				if (_snapDirections != 0)
				{
					value = MathTools.SnapVectorToNearestAngle(value, 360f / (float)_snapDirections);
					num = 161752059;
					goto IL_0011;
				}
				goto IL_01e3;
				IL_0186:
				value.Normalize();
				num = 161752062;
				goto IL_0011;
				IL_01e3:
				if (!_useXAxis)
				{
					int num5;
					if (_useYAxis)
					{
						num = 161752058;
						num5 = num;
					}
					else
					{
						num = 161752032;
						num5 = num;
					}
					goto IL_0011;
				}
				goto IL_0207;
			}
		}

		public void SetDefaultPosition()
		{
			gNCdlfoYwYhSuJSFwIBHNQFiQYf(base.rectTransform.anchoredPosition);
		}

		private void gNCdlfoYwYhSuJSFwIBHNQFiQYf(Vector2 P_0)
		{
			//Discarded unreachable code: IL_0027
			if (!base.initialized)
			{
				while (true)
				{
					switch (0x789ED7E2 ^ 0x789ED7E0)
					{
					case 2:
						return;
					case 0:
						continue;
					}
					break;
				}
			}
			_origAnchoredPosition = P_0;
		}

		public void ReturnToDefaultPosition(bool instant)
		{
			if (base.initialized)
			{
				DnUKFYFvXrbUpuVHbPwuUqgMEfK(_origAnchoredPosition, PositionType.VMBFNpbuJhnKPkImrxDsseJhTir, !instant && _animateOnReturn, _returnSpeed, jIEDtUWQxJuHGrwAkWDYJSIGudw.IkWnGUwOZwdrEJiPezsIYBjiDyT);
			}
		}

		public void ReturnToDefaultPosition()
		{
			if (base.initialized)
			{
				ReturnToDefaultPosition(instant: false);
			}
		}

		[CustomObfuscation(rename = false)]
		internal override void Awake()
		{
			base.Awake();
			if (!Application.isPlaying)
			{
				return;
			}
			while (true)
			{
				_origAnchoredPosition = base.rectTransform.anchoredPosition;
				int num;
				int num2;
				if (!(_stickTransform != null))
				{
					num = -1294319433;
					num2 = num;
				}
				else
				{
					num = -1294319434;
					num2 = num;
				}
				while (true)
				{
					switch (num ^ -1294319436)
					{
					case 0:
						num = -1294319435;
						continue;
					case 1:
						break;
					case 2:
						_origStickAnchoredPosition = _stickTransform.anchoredPosition;
						num = -1294319433;
						continue;
					default:
						SetRawValue(axis2D.rawZero);
						return;
					}
					break;
				}
			}
		}

		[CustomObfuscation(rename = false)]
		internal override void OnEnable()
		{
			//Discarded unreachable code: IL_002d
			base.OnEnable();
			if (!base.initialized)
			{
				while (true)
				{
					switch (0x13A7EB50 ^ 0x13A7EB52)
					{
					case 2:
						return;
					case 0:
						continue;
					}
					break;
				}
			}
			GBExVtHFPRLrKdqZIHqEWcjtFsS();
		}

		[CustomObfuscation(rename = false)]
		internal override void OnDisable()
		{
			base.OnDisable();
			if (!base.initialized)
			{
				return;
			}
			while (true)
			{
				_axis2D.Deinitialize();
				OnClear();
				int num = -1557045649;
				while (true)
				{
					switch (num ^ -1557045651)
					{
					default:
						return;
					case 2:
						return;
					case 0:
						goto IL_000f;
					case 1:
						break;
					}
					break;
					IL_000f:
					num = -1557045652;
				}
			}
		}

		[CustomObfuscation(rename = false)]
		internal override void OnValidate()
		{
			//Discarded unreachable code: IL_0031
			base.OnValidate();
			while (true)
			{
				int num = -1515621494;
				while (true)
				{
					switch (num ^ -1515621496)
					{
					case 0:
						break;
					case 2:
						if (base.initialized)
						{
							goto IL_0038;
						}
						return;
					case 3:
						goto IL_0038;
					default:
						GBExVtHFPRLrKdqZIHqEWcjtFsS();
						return;
					}
					break;
					IL_0038:
					cZNwcKOoWKuxnmVZWmbkUjmXOtE();
					num = -1515621495;
				}
			}
		}

		internal override void OnUpdate()
		{
			//Discarded unreachable code: IL_004a
			base.OnUpdate();
			while (true)
			{
				int num = 831536785;
				while (true)
				{
					switch (num ^ 0x31903E95)
					{
					case 2:
						return;
					case 0:
						break;
					case 4:
					{
						int num2;
						if (!base.initialized)
						{
							num = 831536791;
							num2 = num;
						}
						else
						{
							num = 831536790;
							num2 = num;
						}
						continue;
					}
					case 5:
						FBlOXIugJLwhCfGPQgASiZxYKxE();
						num = 831536788;
						continue;
					case 3:
						UCAcUzyWwTaJkrollZKJQVQBPsx();
						num = 831536784;
						continue;
					default:
						qHupoFSluhFMHDLsPLailrXWQzD();
						return;
					}
					break;
				}
			}
		}

		internal override bool OnInitialize()
		{
			if (!base.OnInitialize())
			{
				return false;
			}
			cZNwcKOoWKuxnmVZWmbkUjmXOtE();
			_axis2D.Initialize();
			return true;
		}

		internal override void OnCustomControllerUpdate()
		{
			//Discarded unreachable code: IL_0066, IL_00af
			if (!base.initialized)
			{
				goto IL_0008;
			}
			goto IL_005d;
			IL_005d:
			if (!hasController)
			{
				return;
			}
			goto IL_006d;
			IL_006d:
			Vector2 value = _axis2D.value;
			int num;
			if (_useXAxis)
			{
				yQjPzGJGxRgoFPaCGxcgBaQECRD(_horizontalAxisCustomControllerElement, value.x, _axis2D.xAxis.buttonActivationThreshold);
				num = 1694601046;
				goto IL_000d;
			}
			goto IL_00b9;
			IL_0008:
			num = 1694601043;
			goto IL_000d;
			IL_00b9:
			if (_useYAxis)
			{
				yQjPzGJGxRgoFPaCGxcgBaQECRD(_verticalAxisCustomControllerElement, value.y, _axis2D.yAxis.buttonActivationThreshold);
				num = 1694601042;
				goto IL_000d;
			}
			goto IL_0039;
			IL_000d:
			switch (num ^ 0x65018F50)
			{
			default:
				return;
			case 0:
				return;
			case 3:
				return;
			case 4:
				break;
			case 2:
				goto IL_0039;
			case 1:
				goto IL_005d;
			case 5:
				goto IL_006d;
			case 6:
				goto IL_00b9;
			}
			goto IL_0008;
			IL_0039:
			if (_allowTap)
			{
				yQjPzGJGxRgoFPaCGxcgBaQECRD(_tapCustomControllerElement, tapValue);
				num = 1694601040;
				goto IL_000d;
			}
		}

		internal override void OnSubscribeEvents()
		{
			base.OnSubscribeEvents();
			_axis2D.ValueChangedEvent += ErAspGeeDMmNWjmJeGHrjQHrKNo;
		}

		internal override void OnUnsubscribeEvents()
		{
			base.OnUnsubscribeEvents();
			_axis2D.ValueChangedEvent -= ErAspGeeDMmNWjmJeGHrjQHrKNo;
		}

		internal override void OnSetProperty()
		{
			base.OnSetProperty();
			if (!base.initialized)
			{
				return;
			}
			while (true)
			{
				cZNwcKOoWKuxnmVZWmbkUjmXOtE();
				int num = -19670983;
				while (true)
				{
					switch (num ^ -19670983)
					{
					case 2:
						goto IL_000f;
					case 1:
						break;
					default:
						GBExVtHFPRLrKdqZIHqEWcjtFsS();
						return;
					}
					break;
					IL_000f:
					num = -19670984;
				}
			}
		}

		internal override void OnClear()
		{
			//Discarded unreachable code: IL_00e5
			if (!base.initialized)
			{
				goto IL_000b;
			}
			goto IL_0100;
			IL_0100:
			_pointerId = int.MinValue;
			int num = 1381492428;
			goto IL_0010;
			IL_0010:
			while (true)
			{
				switch (num ^ 0x5257E6C9)
				{
				case 7:
					return;
				case 0:
					break;
				case 8:
					_pointerDownIsFake = false;
					_lastPressAnchoredPosition = Vector2.zero;
					_lastPressStartingValue = Vector2.zero;
					_calculatedStickRange_lastUpdatedFrame = -1;
					num = 1381492426;
					continue;
				case 3:
					_lastTapFrame = -1;
					_isEligibleForTap = false;
					if (_returnOnRelease && _isMovedFromDefaultPosition)
					{
						if (!_moveToTouchPosition)
						{
							goto IL_009b;
						}
						goto case 6;
					}
					goto default;
				case 6:
					ReturnToDefaultPosition(instant: true);
					num = 1381492424;
					continue;
				case 5:
					_realMousePointerId = int.MinValue;
					yrfVZiGUTfOClpVIfIvNRqImIGGe = false;
					num = 1381492429;
					continue;
				case 4:
					BSbeTqiFrdRDtCmjwklbhCLtNiqg = false;
					num = 1381492417;
					continue;
				case 2:
					goto IL_0100;
				default:
					_isMovedFromDefaultPosition = false;
					_isMoving = false;
					_moveDirection = jIEDtUWQxJuHGrwAkWDYJSIGudw.UHYvAyHcQsOOrxHRGHpDrbrHmPt;
					XZuHRmWoQuERWskxXRGbpMclLVe();
					_axis2D.Clear();
					GBExVtHFPRLrKdqZIHqEWcjtFsS();
					return;
				}
				break;
				IL_009b:
				int num2;
				if (!_followTouchPosition)
				{
					num = 1381492424;
					num2 = num;
				}
				else
				{
					num = 1381492431;
					num2 = num;
				}
			}
			goto IL_000b;
			IL_000b:
			num = 1381492430;
			goto IL_0010;
		}

		internal override void FindEventHandlers()
		{
			base.FindEventHandlers();
			if (_hierarchyValueChangedHandlers == null)
			{
				_hierarchyValueChangedHandlers = new PawhcdAEqKalvcbRBbHZlIgMMIHA.HierarchyEventHelper<IValueChangedHandler, Vector2>(valueChangedHandlerDelegate);
				goto IL_001e;
			}
			goto IL_0073;
			IL_001e:
			int num = -2089338464;
			goto IL_0023;
			IL_0073:
			_hierarchyValueChangedHandlers.GetHandlers(base.transform);
			int num2;
			if (_hierarchyStickPositionChangedHandlers != null)
			{
				num = -2089338462;
				num2 = num;
			}
			else
			{
				num = -2089338461;
				num2 = num;
			}
			goto IL_0023;
			IL_0023:
			while (true)
			{
				switch (num ^ -2089338461)
				{
				default:
					return;
				case 4:
					return;
				case 2:
					break;
				case 1:
					_hierarchyStickPositionChangedHandlers.GetHandlers(base.transform);
					num = -2089338457;
					continue;
				case 0:
					_hierarchyStickPositionChangedHandlers = new PawhcdAEqKalvcbRBbHZlIgMMIHA.HierarchyEventHelper<IStickPositionChangedHandler, Vector2>(stickPositionChangedHandlerDelegate);
					num = -2089338462;
					continue;
				case 3:
					goto IL_0073;
				}
				break;
			}
			goto IL_001e;
		}

		public override void ClearValue()
		{
			if (!base.initialized)
			{
				return;
			}
			while (true)
			{
				_axis2D.Clear();
				_lastTapFrame = -1;
				int num = -1165984695;
				while (true)
				{
					switch (num ^ -1165984692)
					{
					default:
						return;
					case 0:
						return;
					case 2:
						num = -1165984689;
						continue;
					case 3:
						break;
					case 4:
						base.controller.ClearElementValue(_verticalAxisCustomControllerElement);
						base.controller.ClearElementValue(_tapCustomControllerElement);
						num = -1165984692;
						continue;
					case 1:
						base.controller.ClearElementValue(_horizontalAxisCustomControllerElement);
						num = -1165984696;
						continue;
					case 5:
					{
						int num2;
						if (hasController)
						{
							num = -1165984691;
							num2 = num;
						}
						else
						{
							num = -1165984692;
							num2 = num;
						}
						continue;
					}
					}
					break;
				}
			}
		}

		internal override bool IsPressed()
		{
			if (!base.initialized)
			{
				return false;
			}
			if (!agGGiexlYIVgemjaHHzzcCmJTcc())
			{
				return false;
			}
			return yrfVZiGUTfOClpVIfIvNRqImIGGe;
		}

		internal override bool IsThisOrTouchRegionGameObject(GameObject P_0)
		{
			if (P_0 == null)
			{
				return false;
			}
			if (base.IsThisOrTouchRegionGameObject(P_0))
			{
				return true;
			}
			if (_workingTouchRegion != null)
			{
				return _workingTouchRegion.gameObject == P_0;
			}
			return false;
		}

		private void GBExVtHFPRLrKdqZIHqEWcjtFsS()
		{
			_horizontalAxisCustomControllerElement.ClearElementCaches();
			while (true)
			{
				int num = 2128138138;
				while (true)
				{
					switch (num ^ 0x7ED8CF98)
					{
					case 0:
						break;
					case 2:
						goto IL_0029;
					default:
						qHupoFSluhFMHDLsPLailrXWQzD();
						GdUGsVKpTgHEvzMEBLIYtxCfIFr();
						return;
					}
					break;
					IL_0029:
					_verticalAxisCustomControllerElement.ClearElementCaches();
					_tapCustomControllerElement.ClearElementCaches();
					num = 2128138137;
				}
			}
		}

		private void GdUGsVKpTgHEvzMEBLIYtxCfIFr()
		{
			if (!_manageRaycasting)
			{
				return;
			}
			while (true)
			{
				_imageRaycastHelper.KQhvMfEZUJqKpskqtDnGiMNpkZC(base.transform, qcMCRlGDGTOLyeXLWQhWDlVKNSlE());
				int num = 863726571;
				while (true)
				{
					switch (num ^ 0x337B6BEA)
					{
					default:
						return;
					case 1:
						return;
					case 0:
						goto IL_0009;
					case 2:
						break;
					}
					break;
					IL_0009:
					num = 863726568;
				}
			}
		}

		private bool qcMCRlGDGTOLyeXLWQhWDlVKNSlE()
		{
			if (_workingTouchRegion != null && _useTouchRegionOnly)
			{
				return false;
			}
			return true;
		}

		private void JqOvUhTfOscNAZPCFerffqqVNDb(TouchRegion P_0)
		{
			if (P_0 == null)
			{
				return;
			}
			while (true)
			{
				FyFAaVTwKitmoZRrKTRHswoTOIH(P_0);
				P_0.PointerDownEvent += NSaCJNLNcGocStUSwnnYWnwuvob;
				int num = -1171033577;
				while (true)
				{
					switch (num ^ -1171033581)
					{
					default:
						return;
					case 0:
						return;
					case 5:
						num = -1171033582;
						continue;
					case 2:
						P_0.PointerExitEvent += rklTaLGzgkBquoJTvHXoKHosDCt;
						P_0.BeginDragEvent += codeyYnzeQCsqSKZcApqeXmuUaw;
						num = -1171033584;
						continue;
					case 4:
						P_0.PointerUpEvent += slcdeSGvhTIGRUXtiFKCcBoVLShw;
						P_0.PointerEnterEvent += qrwJpuJXUnJctsmgikDbkDazTTD;
						num = -1171033583;
						continue;
					case 3:
						P_0.DragEvent += WELQxPbMdFHlOBxaSraGOZNyCjn;
						P_0.EndDragEvent += DmDSJSYtZOOUGaQtKcjQIFlVMnSm;
						num = -1171033581;
						continue;
					case 1:
						break;
					}
					break;
				}
			}
		}

		private void FyFAaVTwKitmoZRrKTRHswoTOIH(TouchRegion P_0)
		{
			if (P_0 == null)
			{
				return;
			}
			while (true)
			{
				P_0.PointerDownEvent -= NSaCJNLNcGocStUSwnnYWnwuvob;
				P_0.PointerUpEvent -= slcdeSGvhTIGRUXtiFKCcBoVLShw;
				int num = 1359797000;
				while (true)
				{
					switch (num ^ 0x510CDB08)
					{
					default:
						return;
					case 2:
						return;
					case 3:
						num = 1359797001;
						continue;
					case 1:
						break;
					case 0:
						P_0.PointerEnterEvent -= qrwJpuJXUnJctsmgikDbkDazTTD;
						P_0.PointerExitEvent -= rklTaLGzgkBquoJTvHXoKHosDCt;
						P_0.BeginDragEvent -= codeyYnzeQCsqSKZcApqeXmuUaw;
						P_0.DragEvent -= WELQxPbMdFHlOBxaSraGOZNyCjn;
						P_0.EndDragEvent -= DmDSJSYtZOOUGaQtKcjQIFlVMnSm;
						num = 1359797002;
						continue;
					}
					break;
				}
			}
		}

		private void qHupoFSluhFMHDLsPLailrXWQzD()
		{
			if (_workingTouchRegion == _touchRegion)
			{
				return;
			}
			while (true)
			{
				FyFAaVTwKitmoZRrKTRHswoTOIH(_workingTouchRegion);
				int num = -1656274829;
				while (true)
				{
					switch (num ^ -1656274829)
					{
					case 2:
						goto IL_0014;
					case 1:
						break;
					default:
						_workingTouchRegion = _touchRegion;
						JqOvUhTfOscNAZPCFerffqqVNDb(_workingTouchRegion);
						return;
					}
					break;
					IL_0014:
					num = -1656274830;
				}
			}
		}

		private void RBwZttPSZcSXhOFMwihqtJLzOqE(Vector2 P_0, bool P_1, float P_2, jIEDtUWQxJuHGrwAkWDYJSIGudw P_3)
		{
			RectTransform rectTransform = base.transform.parent as RectTransform;
			Vector2 vector = QUSKPeaMdsIjKePEopWfjLfNMLfw.rKmDpgLJHuEtVKtAyNSdyFMtyRrf(base.canvas, rectTransform, P_0);
			Vector2 pivot = base.rectTransform.pivot;
			while (true)
			{
				int num = -1964149300;
				while (true)
				{
					switch (num ^ -1964149298)
					{
					default:
						return;
					case 3:
						return;
					case 0:
						break;
					case 2:
					{
						Vector2 sizeDelta = base.rectTransform.sizeDelta;
						Vector3 localScale = base.rectTransform.localScale;
						vector += new Vector2((pivot.x - 0.5f) * sizeDelta.x * localScale.x, (pivot.y - 0.5f) * sizeDelta.y * localScale.y);
						num = -1964149297;
						continue;
					}
					case 1:
						DnUKFYFvXrbUpuVHbPwuUqgMEfK(vector, PositionType.PClOLQRhgHgGSKopqNIgjbqoRgUa, P_1, P_2, P_3);
						num = -1964149299;
						continue;
					}
					break;
				}
			}
		}

		private void DnUKFYFvXrbUpuVHbPwuUqgMEfK(Vector2 P_0, PositionType P_1, bool P_2, float P_3, jIEDtUWQxJuHGrwAkWDYJSIGudw P_4)
		{
			//Discarded unreachable code: IL_02b7
			if (_isMoving && P_2 && _moveDirection == P_4)
			{
				return;
			}
			Vector2 one = default(Vector2);
			Transform transform = default(Transform);
			RectTransform canvasTransform = default(RectTransform);
			float num5 = default(float);
			float num3 = default(float);
			while (true)
			{
				IL_01af:
				int num;
				if (_isMoving)
				{
					int num2;
					if (_coroutineMove != null)
					{
						num = 29514640;
						num2 = num;
					}
					else
					{
						num = 29514632;
						num2 = num;
					}
					goto IL_0024;
				}
				goto IL_0080;
				IL_0024:
				while (true)
				{
					float num4;
					switch (num ^ 0x1C25B9A)
					{
					default:
						return;
					case 15:
						return;
					case 0:
						num = 29514646;
						continue;
					case 18:
						break;
					case 5:
						_moveDirection = jIEDtUWQxJuHGrwAkWDYJSIGudw.UHYvAyHcQsOOrxHRGHpDrbrHmPt;
						num = 29514632;
						continue;
					case 9:
						Logger.LogWarning("Animation cannot be used without a Canvas.");
						num = 29514653;
						continue;
					case 10:
						XZuHRmWoQuERWskxXRGbpMclLVe();
						num = 29514644;
						continue;
					case 8:
						if (base.canvas.renderMode == RenderMode.WorldSpace)
						{
							Logger.LogWarning("Animation can only be used with a screen space Canvas.");
							P_2 = false;
							num = 29514649;
							continue;
						}
						goto case 3;
					case 1:
						num4 = one.x;
						goto IL_0109;
					case 14:
						_isMoving = false;
						num = 29514655;
						continue;
					case 3:
						if (P_2)
						{
							transform = base.transform;
							canvasTransform = base.canvasTransform;
							one = Vector2.one;
							num = 29514654;
							continue;
						}
						goto case 6;
					case 13:
						if (!(transform == null))
						{
							one.x *= transform.localScale.x;
							num = 29514635;
							continue;
						}
						goto case 11;
					case 17:
						one.y *= transform.localScale.y;
						num = 29514654;
						continue;
					case 12:
						goto IL_01af;
					case 2:
						P_3 = P_3 / num5 * num3;
						_coroutineMove = HcJsOVwrfAiwEwdIJagXehXDuiO(P_0, P_1, P_3, P_4);
						StartCoroutine(_coroutineMove);
						_moveDirection = P_4;
						num = 29514634;
						continue;
					case 7:
						P_2 = false;
						num = 29514649;
						continue;
					case 6:
						moveStartedDelegate(P_4);
						xWWmXnncgSDTMtlbInPtiKaaKpz(P_4, P_0, P_1);
						num = 29514645;
						continue;
					case 4:
						goto IL_023f;
					case 11:
					{
						Vector2 sizeDelta = canvasTransform.sizeDelta;
						bool flag = sizeDelta.x < sizeDelta.y;
						num3 = MathTools.Max(sizeDelta.x, sizeDelta.y);
						if (flag)
						{
							num4 = one.y;
							goto IL_0109;
						}
						num = 29514651;
						continue;
					}
					case 16:
						{
							_isMovedFromDefaultPosition = true;
							moveStartedDelegate(P_4);
							return;
						}
						IL_0109:
						num5 = num4;
						if (num5 == 0f)
						{
							num5 = 0.0001f;
							num = 29514648;
							continue;
						}
						goto case 2;
					}
					break;
					IL_023f:
					int num6;
					if ((transform = transform.parent) != canvasTransform)
					{
						num = 29514647;
						num6 = num;
					}
					else
					{
						num = 29514641;
						num6 = num;
					}
				}
				goto IL_0080;
				IL_0080:
				int num7;
				if (!(base.canvas == null))
				{
					num = 29514642;
					num7 = num;
				}
				else
				{
					num = 29514643;
					num7 = num;
				}
				goto IL_0024;
			}
		}

		private IEnumerator HcJsOVwrfAiwEwdIJagXehXDuiO(Vector2 P_0, PositionType P_1, float P_2, jIEDtUWQxJuHGrwAkWDYJSIGudw P_3)
		{
			XzyAbWdlDmUPCfDeMzTJOQAHbNO xzyAbWdlDmUPCfDeMzTJOQAHbNO = new XzyAbWdlDmUPCfDeMzTJOQAHbNO(0);
			xzyAbWdlDmUPCfDeMzTJOQAHbNO.hFQAwvywpKYhLMOJAVKlWFLojQb = this;
			while (true)
			{
				int num = 2004085239;
				while (true)
				{
					switch (num ^ 0x7773E9F6)
					{
					case 3:
						break;
					case 1:
						xzyAbWdlDmUPCfDeMzTJOQAHbNO.hJgwKkVIBWGPaETCEkbEBSZQPPsk = P_0;
						xzyAbWdlDmUPCfDeMzTJOQAHbNO.tyTFjCkNBmrbeDPaKBpICCKcOdc = P_1;
						xzyAbWdlDmUPCfDeMzTJOQAHbNO.spMAfzHDKqkSmQgrpxPGEHxzkxZ = P_2;
						num = 2004085236;
						continue;
					case 2:
						xzyAbWdlDmUPCfDeMzTJOQAHbNO.CfKbLqXdRfoleqXNaNuSsBaejEV = P_3;
						num = 2004085238;
						continue;
					default:
						return xzyAbWdlDmUPCfDeMzTJOQAHbNO;
					}
					break;
				}
			}
		}

		private void xWWmXnncgSDTMtlbInPtiKaaKpz(jIEDtUWQxJuHGrwAkWDYJSIGudw P_0, Vector2 P_1, PositionType P_2)
		{
			QUSKPeaMdsIjKePEopWfjLfNMLfw.FKjtSQquCwWMYDSUOaOTaImZSWp(base.rectTransform, P_1, P_2);
			_isMoving = false;
			_moveDirection = jIEDtUWQxJuHGrwAkWDYJSIGudw.UHYvAyHcQsOOrxHRGHpDrbrHmPt;
			if (P_0 == jIEDtUWQxJuHGrwAkWDYJSIGudw.IkWnGUwOZwdrEJiPezsIYBjiDyT)
			{
				goto IL_0024;
			}
			goto IL_0065;
			IL_0065:
			int num;
			if (P_0 == jIEDtUWQxJuHGrwAkWDYJSIGudw.vZJAaAAZjhfVfUmYHsWMcjdbLrND)
			{
				_isMovedFromDefaultPosition = true;
				num = 642917847;
				goto IL_0029;
			}
			goto IL_008c;
			IL_0024:
			num = 642917844;
			goto IL_0029;
			IL_008c:
			XZuHRmWoQuERWskxXRGbpMclLVe();
			num = 642917845;
			goto IL_0029;
			IL_0029:
			while (true)
			{
				switch (num ^ 0x265225D7)
				{
				default:
					return;
				case 1:
					return;
				case 4:
					break;
				case 2:
					moveEndedDelegate(P_0);
					num = 642917846;
					continue;
				case 6:
					goto IL_0065;
				case 5:
					num = 642917847;
					continue;
				case 3:
					_isMovedFromDefaultPosition = false;
					num = 642917842;
					continue;
				case 0:
					goto IL_008c;
				}
				break;
			}
			goto IL_0024;
		}

		private void dpevHZjzTtBoDJuKfSBypVLmMZP(jIEDtUWQxJuHGrwAkWDYJSIGudw P_0)
		{
			if (!_manageRaycasting)
			{
				return;
			}
			bool flag = false;
			bool flag2 = false;
			if (_followTouchPosition)
			{
				if (!stayActiveOnSwipeOut)
				{
					goto IL_001f;
				}
				goto IL_0077;
			}
			goto IL_0090;
			IL_0077:
			int num;
			int num2;
			if (_returnOnRelease)
			{
				num = -1871857516;
				num2 = num;
			}
			else
			{
				num = -1871857520;
				num2 = num;
			}
			goto IL_0024;
			IL_0024:
			while (true)
			{
				switch (num ^ -1871857517)
				{
				default:
					return;
				case 1:
					return;
				case 4:
					break;
				case 3:
					if (flag)
					{
						_imageRaycastHelper.KQhvMfEZUJqKpskqtDnGiMNpkZC(base.transform, flag2);
						num = -1871857518;
						continue;
					}
					return;
				case 0:
					goto IL_0077;
				case 6:
					goto IL_0090;
				case 2:
					flag2 = false;
					num = -1871857520;
					continue;
				case 5:
					if (!(_workingTouchRegion != null))
					{
						goto case 3;
					}
					goto IL_00c6;
				case 7:
					if (P_0 == jIEDtUWQxJuHGrwAkWDYJSIGudw.vZJAaAAZjhfVfUmYHsWMcjdbLrND)
					{
						flag = true;
						num = -1871857519;
						continue;
					}
					goto case 3;
				case 8:
					goto IL_00f5;
				}
				break;
				IL_00f5:
				int num3;
				if (!_moveToTouchPosition)
				{
					num = -1871857520;
					num3 = num;
				}
				else
				{
					num = -1871857517;
					num3 = num;
				}
				continue;
				IL_00c6:
				int num4;
				if (_useTouchRegionOnly)
				{
					num = -1871857520;
					num4 = num;
				}
				else
				{
					num = -1871857509;
					num4 = num;
				}
			}
			goto IL_001f;
			IL_0090:
			int num5;
			if (_followTouchPosition)
			{
				num = -1871857520;
				num5 = num;
			}
			else
			{
				num = -1871857514;
				num5 = num;
			}
			goto IL_0024;
			IL_001f:
			num = -1871857515;
			goto IL_0024;
		}

		private void szPdrxBxecWzCAmftCGFBijDAUM(jIEDtUWQxJuHGrwAkWDYJSIGudw P_0)
		{
			if (!_manageRaycasting)
			{
				return;
			}
			bool flag = false;
			bool flag2 = default(bool);
			while (true)
			{
				int num = -1558686031;
				while (true)
				{
					switch (num ^ -1558686032)
					{
					default:
						return;
					case 4:
						return;
					case 7:
						break;
					case 6:
					{
						int num3;
						if (!flag)
						{
							num = -1558686028;
							num3 = num;
						}
						else
						{
							num = -1558686029;
							num3 = num;
						}
						continue;
					}
					case 3:
						_imageRaycastHelper.KQhvMfEZUJqKpskqtDnGiMNpkZC(base.transform, flag2);
						num = -1558686028;
						continue;
					case 2:
						if (_returnOnRelease && P_0 == jIEDtUWQxJuHGrwAkWDYJSIGudw.IkWnGUwOZwdrEJiPezsIYBjiDyT)
						{
							flag = true;
							num = -1558686027;
							continue;
						}
						goto case 6;
					case 1:
						flag2 = false;
						if (_followTouchPosition)
						{
							int num2;
							if (stayActiveOnSwipeOut)
							{
								num = -1558686030;
								num2 = num;
							}
							else
							{
								num = -1558686032;
								num2 = num;
							}
							continue;
						}
						goto case 0;
					case 5:
						flag2 = qcMCRlGDGTOLyeXLWQhWDlVKNSlE();
						num = -1558686026;
						continue;
					case 0:
						if (!_followTouchPosition && _workingTouchRegion != null && !_useTouchRegionOnly)
						{
							int num4;
							if (!_moveToTouchPosition)
							{
								num = -1558686026;
								num4 = num;
							}
							else
							{
								num = -1558686030;
								num4 = num;
							}
							continue;
						}
						goto case 6;
					}
					break;
				}
			}
		}

		private void XZuHRmWoQuERWskxXRGbpMclLVe()
		{
			if (_coroutineMove != null)
			{
				try
				{
					StopCoroutine(_coroutineMove);
				}
				catch
				{
				}
				_coroutineMove = null;
			}
		}

		private void sQQUfUMgAhuobjmJmIobItMuoeE(int P_0, Vector2 P_1, PositionType P_2)
		{
			if (!TouchInteractable.MUAEmHanKCAekGtjjVciojDFRqNV(P_0))
			{
				return;
			}
			while (true)
			{
				DnUKFYFvXrbUpuVHbPwuUqgMEfK((Vector2)QUSKPeaMdsIjKePEopWfjLfNMLfw.OKAeVJvWGXtzrySIYPqhxBZrEuO(base.rectTransform, P_2) + P_1, P_2, false, 0f, jIEDtUWQxJuHGrwAkWDYJSIGudw.vZJAaAAZjhfVfUmYHsWMcjdbLrND);
				if (_lastClaimSource == kLwRfBUiLzNGfTUHHvPyLdQaSsJ.dHfPQraqXaVZHuxcjEqIITHISaN)
				{
					_lastPressAnchoredPosition += P_1;
					int num = 1935260948;
					while (true)
					{
						switch (num ^ 0x7359BD15)
						{
						default:
							return;
						case 1:
							return;
						case 0:
							goto IL_0009;
						case 2:
							break;
						}
						break;
						IL_0009:
						num = 1935260951;
					}
					continue;
				}
				break;
			}
		}

		private void FBlOXIugJLwhCfGPQgASiZxYKxE()
		{
			//Discarded unreachable code: IL_00f5, IL_0100
			if (!hasPointer)
			{
				return;
			}
			PointerEventData pointerEventData;
			while (true)
			{
				IL_00bf:
				if (TouchInteractable.MUAEmHanKCAekGtjjVciojDFRqNV(effectivePointerId))
				{
					goto IL_0039;
				}
				pointerEventData = lYycfThbLcNDrXoSoMNFZmblhiW(effectivePointerId);
				if (pointerEventData != null && pointerEventData.pointerPress != null)
				{
					break;
				}
				goto IL_00af;
				IL_00af:
				wAYSKMoYLXrHlKmKvvdfOiGBjye();
				int num = -1619561383;
				goto IL_0011;
				IL_0011:
				while (true)
				{
					switch (num ^ -1619561379)
					{
					default:
						return;
					case 3:
						return;
					case 4:
						return;
					case 2:
						num = -1619561380;
						continue;
					case 5:
						break;
					case 0:
						goto IL_00af;
					case 1:
						goto IL_00bf;
					}
					break;
				}
				goto IL_0039;
				IL_0039:
				if (_pointerDownIsFake)
				{
					PointerEventData pointerEventData2 = XXngLZbgZgohzdkkHlAueNnBMbsM(effectivePointerId, (_workingTouchRegion != null && _useTouchRegionOnly) ? _workingTouchRegion.gameObject : ((_stickTransform != null) ? _stickTransform.gameObject : base.gameObject));
					if (pointerEventData2 != null)
					{
						urdpaQdIrgekBxXNFvDblTvZtVk(pointerEventData2, _lastClaimSource);
						num = -1619561378;
						goto IL_0011;
					}
					return;
				}
				return;
			}
			rNlfOZEoSimQmBIeJxNKvxgjMQsF(pointerEventData);
		}

		private void UCAcUzyWwTaJkrollZKJQVQBPsx()
		{
			if (!hasPointer)
			{
				return;
			}
			while (true)
			{
				Vector2 vector = TouchInteractable.peFNVeWnRvpCYQMMXzPLXboQIi(effectivePointerId);
				int num = -1672654584;
				while (true)
				{
					switch (num ^ -1672654584)
					{
					default:
						return;
					case 3:
						return;
					case 2:
						num = -1672654583;
						continue;
					case 1:
						break;
					case 0:
						aefnyHMpGSqzRWAcJcrBYBatSpe(ref vector);
						num = -1672654581;
						continue;
					}
					break;
				}
			}
		}

		private void aefnyHMpGSqzRWAcJcrBYBatSpe(ref Vector2 P_0)
		{
			//Discarded unreachable code: IL_003b
			if (_allowTap)
			{
				if (!_isEligibleForTap)
				{
					goto IL_0010;
				}
				goto IL_0084;
			}
			return;
			IL_0015:
			int num;
			while (true)
			{
				switch (num ^ 0x5094D109)
				{
				default:
					return;
				case 1:
					return;
				case 5:
					return;
				case 0:
					break;
				case 4:
					_isEligibleForTap = false;
					num = 1351930124;
					continue;
				case 3:
					goto IL_0050;
				case 2:
					goto IL_0084;
				}
				break;
			}
			goto IL_0010;
			IL_0084:
			if (_tapTimeout > 0f)
			{
				int num2;
				if (!(Time.realtimeSinceStartup - _touchStartTime > _tapTimeout))
				{
					num = 1351930122;
					num2 = num;
				}
				else
				{
					num = 1351930125;
					num2 = num;
				}
				goto IL_0015;
			}
			goto IL_0050;
			IL_0050:
			if (_tapDistanceLimit >= 0)
			{
				int num3;
				if (Vector2.Distance(_touchStartPosition, P_0) <= (float)_tapDistanceLimit)
				{
					num = 1351930124;
					num3 = num;
				}
				else
				{
					num = 1351930125;
					num3 = num;
				}
				goto IL_0015;
			}
			return;
			IL_0010:
			num = 1351930120;
			goto IL_0015;
		}

		private bool dFVbcRaqFpJjthtmulsGoBxPsGN()
		{
			if (!_followTouchPosition)
			{
				return false;
			}
			if (_touchRegion != null && _useTouchRegionOnly)
			{
				return false;
			}
			return true;
		}

		private void BgAdqUvZFqBlqudhjyphLkDsMtm()
		{
			_pointerId = int.MinValue;
			_realMousePointerId = int.MinValue;
			while (true)
			{
				int num = -339209120;
				while (true)
				{
					switch (num ^ -339209119)
					{
					default:
						return;
					case 0:
						return;
					case 2:
						break;
					case 1:
						goto IL_0034;
					}
					break;
					IL_0034:
					_lastClaimSource = kLwRfBUiLzNGfTUHHvPyLdQaSsJ.PClOLQRhgHgGSKopqNIgjbqoRgUa;
					num = -339209119;
				}
			}
		}

		private bool wzNIDgakHttMmiiBhPwkMJrPBdB(int P_0)
		{
			if (P_0 == int.MinValue)
			{
				return false;
			}
			if (_pointerId == int.MinValue)
			{
				return false;
			}
			if (_pointerId == P_0)
			{
				return true;
			}
			if (TouchInteractable.OenwdeSUZHKJsTIxPjYHJUNnctC(P_0) && _realMousePointerId != int.MinValue && P_0 == _realMousePointerId)
			{
				return true;
			}
			return false;
		}

		private PointerEventData KJdskTUpjJuElEhrvEVGouQMzgV(int P_0, GameObject P_1)
		{
			//Discarded unreachable code: IL_01db, IL_0208
			PointerEventData pointerEventData = lYycfThbLcNDrXoSoMNFZmblhiW(P_0);
			float unscaledTime2 = default(float);
			float num2 = default(float);
			float unscaledTime = default(float);
			GameObject gameObject2 = default(GameObject);
			GameObject gameObject = default(GameObject);
			GameObject gameObject3 = default(GameObject);
			while (true)
			{
				int num = -1998557324;
				while (true)
				{
					switch (num ^ -1998557344)
					{
					case 6:
						break;
					case 2:
						pointerEventData.clickCount = 1;
						num = -1998557329;
						continue;
					case 19:
						pointerEventData.clickTime = unscaledTime2;
						num = -1998557331;
						continue;
					case 4:
						if (TouchInteractable.BjWUubnUnTBxMVzCyvrMCwQMuce(P_0))
						{
							pointerEventData.eligibleForClick = true;
							pointerEventData.delta = Vector2.zero;
							num = -1998557337;
							continue;
						}
						goto case 21;
					case 18:
						if (num2 < 0.3f)
						{
							pointerEventData.clickCount++;
							num = -1998557329;
							continue;
						}
						goto case 2;
					case 9:
						pointerEventData.delta = Vector2.zero;
						pointerEventData.dragging = false;
						pointerEventData.useDragThreshold = true;
						pointerEventData.pressPosition = pointerEventData.position;
						pointerEventData.pointerPressRaycast = pointerEventData.pointerCurrentRaycast;
						num = -1998557327;
						continue;
					case 15:
						pointerEventData.clickTime = unscaledTime;
						num = -1998557328;
						continue;
					case 0:
						pointerEventData.clickCount = 1;
						num = -1998557328;
						continue;
					case 22:
					{
						gameObject2 = P_1;
						unscaledTime = Time.unscaledTime;
						int num4;
						if (gameObject2 == pointerEventData.lastPress)
						{
							num = -1998557330;
							num4 = num;
						}
						else
						{
							num = -1998557344;
							num4 = num;
						}
						continue;
					}
					case 7:
						pointerEventData.dragging = false;
						num = -1998557332;
						continue;
					case 20:
						if (pointerEventData == null)
						{
							return null;
						}
						gameObject = P_1;
						pointerEventData.position = TouchInteractable.peFNVeWnRvpCYQMMXzPLXboQIi(P_0);
						num = -1998557340;
						continue;
					case 11:
						pointerEventData.clickCount = 1;
						num = -1998557325;
						continue;
					case 21:
					{
						int num5;
						if (TouchInteractable.OenwdeSUZHKJsTIxPjYHJUNnctC(P_0))
						{
							num = -1998557334;
							num5 = num;
						}
						else
						{
							num = -1998557339;
							num5 = num;
						}
						continue;
					}
					case 3:
						pointerEventData.clickTime = unscaledTime;
						pointerEventData.pointerDrag = gameObject;
						goto IL_030d;
					case 13:
						pointerEventData.pointerPress = gameObject3;
						pointerEventData.rawPointerPress = gameObject;
						pointerEventData.clickTime = unscaledTime2;
						pointerEventData.pointerDrag = gameObject;
						goto IL_030d;
					case 17:
						gameObject3 = P_1;
						unscaledTime2 = Time.unscaledTime;
						if (gameObject3 == pointerEventData.lastPress)
						{
							float num3 = unscaledTime2 - pointerEventData.clickTime;
							if (num3 < 0.3f)
							{
								pointerEventData.clickCount++;
								num = -1998557325;
								continue;
							}
							goto case 11;
						}
						goto case 1;
					case 12:
						pointerEventData.useDragThreshold = true;
						pointerEventData.pressPosition = pointerEventData.position;
						pointerEventData.pointerPressRaycast = pointerEventData.pointerCurrentRaycast;
						if (pointerEventData.pointerEnter != gameObject)
						{
							pointerEventData.pointerEnter = gameObject;
							num = -1998557322;
							continue;
						}
						goto case 22;
					case 14:
						num2 = unscaledTime - pointerEventData.clickTime;
						num = -1998557326;
						continue;
					case 1:
						pointerEventData.clickCount = 1;
						num = -1998557331;
						continue;
					case 16:
						pointerEventData.pointerPress = gameObject2;
						num = -1998557336;
						continue;
					case 8:
						pointerEventData.rawPointerPress = gameObject;
						num = -1998557341;
						continue;
					case 10:
						pointerEventData.eligibleForClick = true;
						num = -1998557335;
						continue;
					default:
						{
							Logger.LogWarning("Unsupported pointerId: " + P_0);
							return null;
						}
						IL_030d:
						return pointerEventData;
					}
					break;
				}
			}
		}

		private PointerEventData XXngLZbgZgohzdkkHlAueNnBMbsM(int P_0, GameObject P_1)
		{
			PointerEventData pointerEventData = lYycfThbLcNDrXoSoMNFZmblhiW(P_0);
			if (pointerEventData == null)
			{
				return null;
			}
			Vector2 vector = TouchInteractable.peFNVeWnRvpCYQMMXzPLXboQIi(P_0);
			pointerEventData.delta = vector - pointerEventData.position;
			pointerEventData.position = vector;
			pointerEventData.dragging = true;
			pointerEventData.pointerDrag = P_1;
			pointerEventData.useDragThreshold = true;
			pointerEventData.pointerPress = null;
			pointerEventData.rawPointerPress = null;
			return pointerEventData;
		}

		private PointerEventData VSPCPJdlAOfJpTUOqerfRqxddALm(int P_0)
		{
			//Discarded unreachable code: IL_008c, IL_00cc
			PointerEventData pointerEventData = lYycfThbLcNDrXoSoMNFZmblhiW(P_0);
			if (pointerEventData == null)
			{
				return null;
			}
			if (TouchInteractable.BjWUubnUnTBxMVzCyvrMCwQMuce(P_0))
			{
				goto IL_0015;
			}
			goto IL_005f;
			IL_001a:
			int num;
			while (true)
			{
				switch (num ^ -609846183)
				{
				case 2:
					break;
				case 0:
					pointerEventData.dragging = false;
					pointerEventData.pointerDrag = null;
					num = -609846179;
					continue;
				case 1:
					goto IL_005f;
				case 4:
					pointerEventData.pointerEnter = null;
					goto IL_00ed;
				case 7:
					pointerEventData.eligibleForClick = false;
					pointerEventData.pointerPress = null;
					num = -609846182;
					continue;
				case 3:
					pointerEventData.rawPointerPress = null;
					num = -609846183;
					continue;
				case 5:
					pointerEventData.dragging = false;
					pointerEventData.pointerDrag = null;
					goto IL_00ed;
				default:
					goto IL_00d6;
					IL_00ed:
					return pointerEventData;
				}
				break;
			}
			goto IL_0015;
			IL_0015:
			num = -609846178;
			goto IL_001a;
			IL_005f:
			if (TouchInteractable.OenwdeSUZHKJsTIxPjYHJUNnctC(P_0))
			{
				pointerEventData.eligibleForClick = false;
				pointerEventData.pointerPress = null;
				pointerEventData.rawPointerPress = null;
				num = -609846180;
				goto IL_001a;
			}
			goto IL_00d6;
			IL_00d6:
			Logger.LogWarning("Unsupported pointerId: " + P_0);
			return null;
		}

		private void rNlfOZEoSimQmBIeJxNKvxgjMQsF(PointerEventData P_0)
		{
			if (P_0 == null)
			{
				return;
			}
			while (true)
			{
				OnPointerUp(P_0);
				int num = 18440374;
				while (true)
				{
					switch (num ^ 0x11960B7)
					{
					case 0:
						goto IL_0004;
					case 2:
						break;
					default:
						VSPCPJdlAOfJpTUOqerfRqxddALm(effectivePointerId);
						return;
					}
					break;
					IL_0004:
					num = 18440373;
				}
			}
		}

		private void urdpaQdIrgekBxXNFvDblTvZtVk(PointerEventData P_0, kLwRfBUiLzNGfTUHHvPyLdQaSsJ P_1)
		{
			//Discarded unreachable code: IL_0046
			if (P_0 == null)
			{
				return;
			}
			while (true)
			{
				IL_0062:
				int num;
				if (P_1 == kLwRfBUiLzNGfTUHHvPyLdQaSsJ.PClOLQRhgHgGSKopqNIgjbqoRgUa)
				{
					OnDrag(P_0);
					num = 2054135982;
					goto IL_0009;
				}
				goto IL_004d;
				IL_0009:
				while (true)
				{
					switch (num ^ 0x7A6FA0AA)
					{
					case 0:
						num = 2054135980;
						continue;
					case 2:
						WELQxPbMdFHlOBxaSraGOZNyCjn(P_0);
						num = 2054135977;
						continue;
					case 5:
						throw new NotImplementedException();
					case 1:
						break;
					case 6:
						goto IL_0062;
					case 4:
						num = 2054135977;
						continue;
					default:
						VSPCPJdlAOfJpTUOqerfRqxddALm(effectivePointerId);
						return;
					}
					break;
				}
				goto IL_004d;
				IL_004d:
				int num2;
				if (P_1 != kLwRfBUiLzNGfTUHHvPyLdQaSsJ.dHfPQraqXaVZHuxcjEqIITHISaN)
				{
					num = 2054135983;
					num2 = num;
				}
				else
				{
					num = 2054135976;
					num2 = num;
				}
				goto IL_0009;
			}
		}

		private PointerEventData lYycfThbLcNDrXoSoMNFZmblhiW(int P_0)
		{
			//Discarded unreachable code: IL_00d9
			if (P_0 == int.MinValue)
			{
				goto IL_000b;
			}
			int num;
			int num2;
			if (__fakePointerEventData == null)
			{
				num = 1374267127;
				num2 = num;
			}
			else
			{
				num = 1374267126;
				num2 = num;
			}
			goto IL_0010;
			IL_000b:
			num = 1374267129;
			goto IL_0010;
			IL_0010:
			PointerEventData.InputButton button = default(PointerEventData.InputButton);
			PointerEventData value = default(PointerEventData);
			while (true)
			{
				switch (num ^ 0x51E9A6F0)
				{
				case 3:
					break;
				case 1:
					button = PointerEventData.InputButton.Middle;
					num = 1374267130;
					continue;
				case 7:
					__fakePointerEventData = new Dictionary<int, PointerEventData>();
					num = 1374267126;
					continue;
				case 8:
					switch (P_0)
					{
					case -3:
						break;
					default:
						goto IL_007e;
					case -2:
						goto IL_0101;
					case -1:
						goto IL_010d;
					}
					goto case 1;
				case 6:
					if (!__fakePointerEventData.TryGetValue(P_0, out value))
					{
						value = new PointerEventData(EventSystem.current);
						value.pointerId = P_0;
						__fakePointerEventData.Add(P_0, value);
						int num3;
						if (!TouchInteractable.OenwdeSUZHKJsTIxPjYHJUNnctC(P_0))
						{
							num = 1374267125;
							num3 = num;
						}
						else
						{
							num = 1374267128;
							num3 = num;
						}
						continue;
					}
					goto default;
				case 0:
					throw new NotImplementedException();
				case 9:
					return null;
				case 4:
					goto IL_0101;
				case 2:
					goto IL_010d;
				case 10:
					value.button = button;
					num = 1374267125;
					continue;
				default:
					{
						return value;
					}
					IL_007e:
					num = 1374267120;
					continue;
					IL_010d:
					button = PointerEventData.InputButton.Left;
					num = 1374267130;
					continue;
					IL_0101:
					button = PointerEventData.InputButton.Right;
					num = 1374267130;
					continue;
				}
				break;
			}
			goto IL_000b;
		}

		private void cZNwcKOoWKuxnmVZWmbkUjmXOtE()
		{
			//Discarded unreachable code: IL_0088, IL_0098
			iSfJagPsCsVhUfDNyhNvbxLabufM(_axesToUse);
			while (true)
			{
				int num = -1878035813;
				while (true)
				{
					switch (num ^ -1878035811)
					{
					default:
						return;
					case 3:
						return;
					case 8:
						return;
					case 0:
						break;
					case 7:
						if (_allowTap)
						{
							base.controller.ValidateElements(_tapCustomControllerElement);
							num = -1878035819;
							continue;
						}
						return;
					case 1:
					{
						int num3;
						if (base.touchController.useCustomController)
						{
							num = -1878035816;
							num3 = num;
						}
						else
						{
							num = -1878035810;
							num3 = num;
						}
						continue;
					}
					case 6:
						if (!hasController)
						{
							return;
						}
						goto case 1;
					case 2:
					{
						int num2;
						if (_useYAxis)
						{
							num = -1878035815;
							num2 = num;
						}
						else
						{
							num = -1878035814;
							num2 = num;
						}
						continue;
					}
					case 4:
						base.controller.ValidateElements(_verticalAxisCustomControllerElement);
						num = -1878035814;
						continue;
					case 5:
						if (_useXAxis)
						{
							base.controller.ValidateElements(_horizontalAxisCustomControllerElement);
							num = -1878035809;
							continue;
						}
						goto case 2;
					}
					break;
				}
			}
		}

		private void iSfJagPsCsVhUfDNyhNvbxLabufM(AxisDirection P_0)
		{
			bool flag = P_0 == AxisDirection.Both || P_0 == AxisDirection.Horizontal;
			if (_useXAxis != flag)
			{
				goto IL_0017;
			}
			goto IL_016e;
			IL_00b6:
			int num;
			bool flag2 = (byte)num != 0;
			int num2;
			int num3;
			if (_useYAxis == flag2)
			{
				num2 = 957726053;
				num3 = num2;
			}
			else
			{
				num2 = 957726057;
				num3 = num2;
			}
			goto IL_001c;
			IL_00af:
			num = ((P_0 == AxisDirection.Vertical) ? 1 : 0);
			goto IL_00b6;
			IL_001c:
			int num5 = default(int);
			int num4 = default(int);
			int targetCount2 = default(int);
			int targetCount = default(int);
			while (true)
			{
				switch (num2 ^ 0x3915BD6E)
				{
				case 12:
					break;
				case 0:
					base.controller.ClearElementValue(_verticalAxisCustomControllerElement[num5]);
					num5++;
					num2 = 957726052;
					continue;
				case 4:
					goto IL_0089;
				case 5:
					num4++;
					num2 = 957726058;
					continue;
				case 13:
					goto IL_00af;
				case 2:
					_useXAxis = flag;
					num2 = 957726054;
					continue;
				case 10:
					goto IL_00e5;
				case 3:
					targetCount2 = _horizontalAxisCustomControllerElement.targetCount;
					num2 = 957726056;
					continue;
				case 7:
					_useYAxis = flag2;
					if (!flag2 && hasController)
					{
						targetCount = _verticalAxisCustomControllerElement.targetCount;
						num5 = 0;
						num2 = 957726052;
						continue;
					}
					goto default;
				case 6:
					num4 = 0;
					num2 = 957726058;
					continue;
				case 9:
					base.controller.ClearElementValue(_horizontalAxisCustomControllerElement[num4]);
					num2 = 957726059;
					continue;
				case 1:
					goto IL_016e;
				case 8:
					goto IL_017e;
				default:
					_axesToUse = P_0;
					return;
				}
				break;
				IL_017e:
				if (!flag)
				{
					int num6;
					if (hasController)
					{
						num2 = 957726061;
						num6 = num2;
					}
					else
					{
						num2 = 957726063;
						num6 = num2;
					}
					continue;
				}
				goto IL_016e;
				IL_0089:
				int num7;
				if (num4 >= targetCount2)
				{
					num2 = 957726063;
					num7 = num2;
				}
				else
				{
					num2 = 957726055;
					num7 = num2;
				}
				continue;
				IL_00e5:
				int num8;
				if (num5 < targetCount)
				{
					num2 = 957726062;
					num8 = num2;
				}
				else
				{
					num2 = 957726053;
					num8 = num2;
				}
			}
			goto IL_0017;
			IL_016e:
			if (P_0 == AxisDirection.Both)
			{
				num = 1;
				goto IL_00b6;
			}
			num2 = 957726051;
			goto IL_001c;
			IL_0017:
			num2 = 957726060;
			goto IL_001c;
		}

		private void CLppEvNomWTxgFFClMmyWJkbdN(PointerEventData P_0, kLwRfBUiLzNGfTUHHvPyLdQaSsJ P_1)
		{
			//Discarded unreachable code: IL_0039
			if (hasPointer)
			{
				goto IL_0008;
			}
			goto IL_0040;
			IL_0040:
			int num;
			if (agGGiexlYIVgemjaHHzzcCmJTcc() && IsInteractable())
			{
				dkbPRumzMHESljvUzfPJVvaxLFt(P_0.pointerId, P_0.pressPosition, P_1);
				num = 1365748479;
				goto IL_000d;
			}
			goto IL_006a;
			IL_006a:
			base.OnPointerDown(P_0);
			return;
			IL_0008:
			num = 1365748478;
			goto IL_000d;
			IL_000d:
			switch (num ^ 0x5167AAFD)
			{
			case 0:
				break;
			case 3:
				if (!wzNIDgakHttMmiiBhPwkMJrPBdB(P_0.pointerId))
				{
					return;
				}
				goto IL_0040;
			case 1:
				goto IL_0040;
			default:
				goto IL_006a;
			}
			goto IL_0008;
		}

		private void bsJnZKaMLeQiOiwSuuiDcemAIrg(PointerEventData P_0, kLwRfBUiLzNGfTUHHvPyLdQaSsJ P_1)
		{
			//Discarded unreachable code: IL_0047
			if (hasPointer && !wzNIDgakHttMmiiBhPwkMJrPBdB(P_0.pointerId))
			{
				return;
			}
			while (!TouchInteractable.MUAEmHanKCAekGtjjVciojDFRqNV(effectivePointerId))
			{
				while (true)
				{
					IL_004e:
					wAYSKMoYLXrHlKmKvvdfOiGBjye();
					int num = 864127810;
					while (true)
					{
						switch (num ^ 0x33818B43)
						{
						case 2:
							num = 864127808;
							continue;
						case 3:
							break;
						case 0:
							goto IL_004e;
						default:
							base.OnPointerUp(P_0);
							return;
						}
						break;
					}
					break;
				}
			}
		}

		private void jgmhCVCLNpdugCcpOmRLAZQAAUv(PointerEventData P_0, kLwRfBUiLzNGfTUHHvPyLdQaSsJ P_1)
		{
			//Discarded unreachable code: IL_01b7, IL_023f
			if (hasPointer && !wzNIDgakHttMmiiBhPwkMJrPBdB(P_0.pointerId))
			{
				return;
			}
			MouseButtonFlags allowedMouseButtons = default(MouseButtonFlags);
			GameObject gameObject = default(GameObject);
			PointerEventData pointerEventData = default(PointerEventData);
			while (true)
			{
				IL_010d:
				bool flag = TouchInteractable.OenwdeSUZHKJsTIxPjYHJUNnctC(P_0.pointerId);
				bool flag2 = false;
				int num;
				switch (P_1)
				{
				case kLwRfBUiLzNGfTUHHvPyLdQaSsJ.PClOLQRhgHgGSKopqNIgjbqoRgUa:
					allowedMouseButtons = base.allowedMouseButtons;
					num = 806756069;
					goto IL_0022;
				case kLwRfBUiLzNGfTUHHvPyLdQaSsJ.dHfPQraqXaVZHuxcjEqIITHISaN:
					goto IL_00e1;
				default:
					{
						num = 806756091;
						goto IL_0022;
					}
					IL_0022:
					while (true)
					{
						switch (num ^ 0x30161EEA)
						{
						case 2:
							num = 806756075;
							continue;
						case 9:
							if (IsInteractable() && (!flag || TouchInteractable.jWwDMVskgRDHPeFKiMdRILlfbbMg(allowedMouseButtons)) && !yrfVZiGUTfOClpVIfIvNRqImIGGe)
							{
								if (flag)
								{
									if (TouchInteractable.rXoHlhXIkgBrHzajLhaabUWFwEkO(allowedMouseButtons, out int realMousePointerId))
									{
										_realMousePointerId = realMousePointerId;
										num = 806756064;
										continue;
									}
									goto case 4;
								}
								goto case 11;
							}
							goto case 8;
						case 6:
							break;
						case 10:
							num = 806756065;
							continue;
						case 14:
							goto IL_00e1;
						case 4:
							_realMousePointerId = P_0.pointerId;
							num = 806756065;
							continue;
						case 1:
							goto IL_010d;
						case 0:
							gameObject = _workingTouchRegion.gameObject;
							num = 806756070;
							continue;
						case 12:
							goto IL_014e;
						case 3:
							goto IL_018b;
						case 17:
							num = 806756090;
							continue;
						case 7:
							num = 806756069;
							continue;
						case 13:
							throw new NotImplementedException();
						case 11:
							flag2 = true;
							num = 806756066;
							continue;
						case 15:
							if (_activateOnSwipeIn)
							{
								goto IL_01d5;
							}
							goto case 8;
						case 8:
							base.OnPointerEnter(P_0);
							if (flag2)
							{
								switch (P_1)
								{
								case kLwRfBUiLzNGfTUHHvPyLdQaSsJ.dHfPQraqXaVZHuxcjEqIITHISaN:
									break;
								case kLwRfBUiLzNGfTUHHvPyLdQaSsJ.PClOLQRhgHgGSKopqNIgjbqoRgUa:
									goto IL_018b;
								default:
									goto IL_020d;
								}
								goto case 0;
							}
							goto default;
						case 5:
							CLppEvNomWTxgFFClMmyWJkbdN(pointerEventData, P_1);
							if (yrfVZiGUTfOClpVIfIvNRqImIGGe)
							{
								_pointerDownIsFake = true;
								num = 806756088;
								continue;
							}
							goto default;
						case 16:
							throw new NotImplementedException();
						default:
							{
								BSbeTqiFrdRDtCmjwklbhCLtNiqg = true;
								return;
							}
							IL_020d:
							num = 806756071;
							continue;
							IL_018b:
							gameObject = base.gameObject;
							num = 806756070;
							continue;
						}
						break;
						IL_01d5:
						int num2;
						if (agGGiexlYIVgemjaHHzzcCmJTcc())
						{
							num = 806756067;
							num2 = num;
						}
						else
						{
							num = 806756066;
							num2 = num;
						}
						continue;
						IL_014e:
						pointerEventData = KJdskTUpjJuElEhrvEVGouQMzgV((_realMousePointerId != int.MinValue) ? _realMousePointerId : P_0.pointerId, gameObject);
						int num3;
						if (pointerEventData == null)
						{
							num = 806756088;
							num3 = num;
						}
						else
						{
							num = 806756079;
							num3 = num;
						}
					}
					goto case kLwRfBUiLzNGfTUHHvPyLdQaSsJ.PClOLQRhgHgGSKopqNIgjbqoRgUa;
					IL_00e1:
					allowedMouseButtons = _touchRegion.allowedMouseButtons;
					num = 806756077;
					goto IL_0022;
				}
			}
		}

		private void LGSqRyvgmhSuGkKYGWZieVFROaZ(PointerEventData P_0, kLwRfBUiLzNGfTUHHvPyLdQaSsJ P_1)
		{
			//Discarded unreachable code: IL_0092
			if (hasPointer)
			{
				goto IL_0008;
			}
			goto IL_0036;
			IL_0036:
			int num;
			if (!stayActiveOnSwipeOut)
			{
				int num2;
				if (!yrfVZiGUTfOClpVIfIvNRqImIGGe)
				{
					num = -1270223675;
					num2 = num;
				}
				else
				{
					num = -1270223679;
					num2 = num;
				}
				goto IL_000d;
			}
			goto IL_0080;
			IL_0080:
			base.OnPointerExit(P_0);
			num = -1270223674;
			goto IL_000d;
			IL_000d:
			while (true)
			{
				switch (num ^ -1270223680)
				{
				case 0:
					return;
				case 2:
					break;
				case 4:
					goto IL_0036;
				case 1:
					wAYSKMoYLXrHlKmKvvdfOiGBjye();
					num = -1270223675;
					continue;
				case 3:
					if (!wzNIDgakHttMmiiBhPwkMJrPBdB(P_0.pointerId))
					{
						base.OnPointerExit(P_0);
						num = -1270223680;
						continue;
					}
					goto IL_0036;
				case 5:
					goto IL_0080;
				default:
					BSbeTqiFrdRDtCmjwklbhCLtNiqg = false;
					return;
				}
				break;
			}
			goto IL_0008;
			IL_0008:
			num = -1270223677;
			goto IL_000d;
		}

		private void AiaRrfleQtDGldRevcCJRUfhRkbn(PointerEventData P_0, kLwRfBUiLzNGfTUHHvPyLdQaSsJ P_1)
		{
			//Discarded unreachable code: IL_0009, IL_000e, IL_0025, IL_0036
			if (hasPointer && wzNIDgakHttMmiiBhPwkMJrPBdB(P_0.pointerId))
			{
				base.OnBeginDrag(P_0);
			}
		}

		private void fFZAuQaOjkujCJdNXLCVEyGvwKFI(PointerEventData P_0, kLwRfBUiLzNGfTUHHvPyLdQaSsJ P_1)
		{
			//Discarded unreachable code: IL_006b, IL_00e6, IL_0235
			if (!hasPointer)
			{
				return;
			}
			Vector2 a = default(Vector2);
			Vector2 vector2 = default(Vector2);
			Vector2 vector3 = default(Vector2);
			bool flag = default(bool);
			bool flag2 = default(bool);
			while (wzNIDgakHttMmiiBhPwkMJrPBdB(P_0.pointerId))
			{
				while (true)
				{
					RectTransform touchReferenceTransform = this.touchReferenceTransform;
					Vector2 vector;
					int num;
					if (_snapStickToTouch)
					{
						vector = QUSKPeaMdsIjKePEopWfjLfNMLfw.iArRMEJfPHpMSdEbSpFfNsbsehz(base.rectTransform, touchReferenceTransform, base.rectTransform.rect.center);
						num = -843700870;
						goto IL_0011;
					}
					goto IL_00cf;
					IL_0011:
					while (true)
					{
						switch (num ^ -843700869)
						{
						case 3:
							num = -843700879;
							continue;
						case 8:
							throw new NotImplementedException();
						case 6:
							break;
						case 16:
							if (_stickBounds == StickBounds.Circle)
							{
								a = Vector2.ClampMagnitude(vector2, calculatedStickRange);
								num = -843700866;
								continue;
							}
							goto case 14;
						case 12:
							goto IL_00cf;
						case 0:
							throw new NotImplementedException();
						case 5:
						{
							Vector2 rawValue = a / calculatedStickRange;
							SetRawValue(rawValue);
							if (_followTouchPosition)
							{
								if (_stickBounds != 0)
								{
									goto case 13;
								}
								if (vector2.sqrMagnitude > calculatedStickRange)
								{
									vector3 = new Vector2(_useXAxis ? (vector2.x - a.x) : 0f, _useXAxis ? (vector2.y - a.y) : 0f);
									num = -843700871;
									continue;
								}
							}
							goto default;
						}
						case 14:
							if (_stickBounds == StickBounds.Square)
							{
								a = MathTools.Clamp(vector2, 0f - calculatedStickRange, calculatedStickRange);
								num = -843700866;
								continue;
							}
							goto case 8;
						case 15:
						{
							Vector2 vector4 = QUSKPeaMdsIjKePEopWfjLfNMLfw.ZZzkROLIMvrpvMngKoitSIDwnGa(base.canvas, touchReferenceTransform, P_0.position);
							vector2 = new Vector2(_useXAxis ? (vector4.x - vector.x) : 0f, _useYAxis ? (vector4.y - vector.y) : 0f);
							num = -843700885;
							continue;
						}
						case 1:
							goto IL_0207;
						case 10:
							goto IL_0223;
						case 2:
							sQQUfUMgAhuobjmJmIobItMuoeE(effectivePointerId, vector3, PositionType.VMBFNpbuJhnKPkImrxDsseJhTir);
							num = -843700880;
							continue;
						case 13:
							if (_stickBounds != StickBounds.Square)
							{
								goto case 0;
							}
							flag = (Mathf.Abs(vector2.x) > calculatedStickRange);
							flag2 = (Mathf.Abs(vector2.y) > calculatedStickRange);
							if (!flag)
							{
								goto IL_0294;
							}
							goto case 4;
						case 4:
						{
							Vector2 vector5 = new Vector2((_useXAxis && flag) ? (vector2.x - a.x) : 0f, (_useXAxis && flag2) ? (vector2.y - a.y) : 0f);
							sQQUfUMgAhuobjmJmIobItMuoeE(effectivePointerId, vector5, PositionType.VMBFNpbuJhnKPkImrxDsseJhTir);
							num = -843700878;
							continue;
						}
						case 7:
							if (!_snapStickToTouch)
							{
								vector -= _lastPressStartingValue * calculatedStickRange;
								num = -843700876;
								continue;
							}
							goto case 15;
						case 11:
							num = -843700878;
							continue;
						default:
							base.OnDrag(P_0);
							return;
						}
						break;
						IL_0207:
						int num2;
						if (!_centerStickOnRelease)
						{
							num = -843700868;
							num2 = num;
						}
						else
						{
							num = -843700876;
							num2 = num;
						}
						continue;
						IL_0294:
						int num3;
						if (flag2)
						{
							num = -843700865;
							num3 = num;
						}
						else
						{
							num = -843700878;
							num3 = num;
						}
					}
					continue;
					IL_00cf:
					vector = _lastPressAnchoredPosition;
					num = -843700870;
					goto IL_0011;
				}
				IL_0223:;
			}
		}

		private void hTVFsMcGJsVPkKyyrMhcIbhDMotn(PointerEventData P_0, kLwRfBUiLzNGfTUHHvPyLdQaSsJ P_1)
		{
			//Discarded unreachable code: IL_004b
			if (!hasPointer)
			{
				return;
			}
			while (true)
			{
				int num;
				int num2;
				if (wzNIDgakHttMmiiBhPwkMJrPBdB(P_0.pointerId))
				{
					num = 1702208593;
					num2 = num;
				}
				else
				{
					num = 1702208594;
					num2 = num;
				}
				while (true)
				{
					switch (num ^ 0x6575A450)
					{
					case 2:
						return;
					case 0:
						goto IL_0009;
					case 3:
						break;
					default:
						base.OnEndDrag(P_0);
						return;
					}
					break;
					IL_0009:
					num = 1702208595;
				}
			}
		}

		private void dkbPRumzMHESljvUzfPJVvaxLFt(int P_0, Vector2 P_1, kLwRfBUiLzNGfTUHHvPyLdQaSsJ P_2)
		{
			_pointerId = P_0;
			PointerEventData pointerEventData = default(PointerEventData);
			while (true)
			{
				int num = -78042952;
				while (true)
				{
					switch (num ^ -78042950)
					{
					default:
						return;
					case 3:
						return;
					case 8:
						break;
					case 0:
					{
						pointerEventData = XXngLZbgZgohzdkkHlAueNnBMbsM(_pointerId, (P_2 == kLwRfBUiLzNGfTUHHvPyLdQaSsJ.dHfPQraqXaVZHuxcjEqIITHISaN) ? _workingTouchRegion.gameObject : ((_stickTransform != null) ? _stickTransform.gameObject : base.gameObject));
						int num4;
						if (pointerEventData == null)
						{
							num = -78042951;
							num4 = num;
						}
						else
						{
							num = -78042959;
							num4 = num;
						}
						continue;
					}
					case 11:
						urdpaQdIrgekBxXNFvDblTvZtVk(pointerEventData, P_2);
						num = -78042951;
						continue;
					case 10:
						_lastPressStartingValue.x = MathTools.Clamp(_axis2D.value.x, -1f, 1f);
						_lastPressStartingValue.y = MathTools.Clamp(_axis2D.value.y, -1f, 1f);
						num = -78042949;
						continue;
					case 12:
						if (_onTouchStarted != null)
						{
							_onTouchStarted.Invoke();
							num = -78042950;
							continue;
						}
						goto case 0;
					case 4:
						if (_followTouchPosition)
						{
							RBwZttPSZcSXhOFMwihqtJLzOqE(P_1, false, 0f, jIEDtUWQxJuHGrwAkWDYJSIGudw.vZJAaAAZjhfVfUmYHsWMcjdbLrND);
							num = -78042954;
							continue;
						}
						goto case 5;
					case 9:
					{
						int num3;
						if (P_2 == kLwRfBUiLzNGfTUHHvPyLdQaSsJ.dHfPQraqXaVZHuxcjEqIITHISaN)
						{
							num = -78042947;
							num3 = num;
						}
						else
						{
							num = -78042954;
							num3 = num;
						}
						continue;
					}
					case 6:
						yrfVZiGUTfOClpVIfIvNRqImIGGe = true;
						num = -78042960;
						continue;
					case 5:
						RBwZttPSZcSXhOFMwihqtJLzOqE(P_1, _animateOnMoveToTouch, _moveToTouchSpeed, jIEDtUWQxJuHGrwAkWDYJSIGudw.vZJAaAAZjhfVfUmYHsWMcjdbLrND);
						num = -78042954;
						continue;
					case 1:
						_touchStartTime = Time.realtimeSinceStartup;
						_touchStartPosition = P_1;
						num = -78042957;
						continue;
					case 2:
						_lastClaimSource = P_2;
						_isEligibleForTap = true;
						_lastPressAnchoredPosition = QUSKPeaMdsIjKePEopWfjLfNMLfw.ZZzkROLIMvrpvMngKoitSIDwnGa(base.canvas, touchReferenceTransform, P_1);
						num = -78042948;
						continue;
					case 7:
						if (!_moveToTouchPosition)
						{
							int num2;
							if (_followTouchPosition)
							{
								num = -78042946;
								num2 = num;
							}
							else
							{
								num = -78042954;
								num2 = num;
							}
							continue;
						}
						goto case 4;
					}
					break;
				}
			}
		}

		private void wAYSKMoYLXrHlKmKvvdfOiGBjye()
		{
			BgAdqUvZFqBlqudhjyphLkDsMtm();
			if (_allowTap)
			{
				goto IL_0011;
			}
			int num = 0;
			goto IL_012e;
			IL_0011:
			int num2 = -2122570949;
			goto IL_0016;
			IL_012e:
			bool flag = (byte)num != 0;
			yrfVZiGUTfOClpVIfIvNRqImIGGe = false;
			num2 = -2122570947;
			goto IL_0016;
			IL_0016:
			while (true)
			{
				switch (num2 ^ -2122570948)
				{
				default:
					return;
				case 4:
					return;
				case 8:
					break;
				case 2:
					SetRawValue(_axis2D.rawZero);
					num2 = -2122570955;
					continue;
				case 3:
					_isEligibleForTap = false;
					if (flag)
					{
						_lastTapFrame = Time.frameCount + 1;
						_onTap.Invoke();
						num2 = -2122570952;
						continue;
					}
					return;
				case 6:
					if (_returnOnRelease && _isMovedFromDefaultPosition)
					{
						ReturnToDefaultPosition();
						num2 = -2122570951;
						continue;
					}
					goto IL_00cf;
				case 9:
					if (_onTouchEnded != null)
					{
						_onTouchEnded.Invoke();
						num2 = -2122570945;
						continue;
					}
					goto case 3;
				case 5:
					goto IL_00cf;
				case 0:
					_lastPressAnchoredPosition = Vector2.zero;
					_lastPressStartingValue = Vector2.zero;
					if (_followTouchPosition)
					{
						goto case 6;
					}
					goto IL_0109;
				case 7:
					goto IL_0125;
				case 1:
					_pointerDownIsFake = false;
					num2 = -2122570948;
					continue;
				}
				break;
				IL_0109:
				int num3;
				if (_moveToTouchPosition)
				{
					num2 = -2122570950;
					num3 = num2;
				}
				else
				{
					num2 = -2122570951;
					num3 = num2;
				}
				continue;
				IL_00cf:
				int num4;
				if (_centerStickOnRelease)
				{
					num2 = -2122570946;
					num4 = num2;
				}
				else
				{
					num2 = -2122570955;
					num4 = num2;
				}
			}
			goto IL_0011;
			IL_0125:
			num = (_isEligibleForTap ? 1 : 0);
			goto IL_012e;
		}

		internal override void OnPointerUp(PointerEventData eventData)
		{
			//Discarded unreachable code: IL_006f, IL_0086
			if (!base.initialized)
			{
				return;
			}
			while (true)
			{
				int num;
				int num2;
				if (TouchInteractable.gtAdYPaYTSdxlqopRartpbSncsib(eventData.pointerId, base.allowedMouseButtons, EventTriggerType.PointerUp))
				{
					num = -1169100944;
					num2 = num;
				}
				else
				{
					num = -1169100942;
					num2 = num;
				}
				while (true)
				{
					switch (num ^ -1169100944)
					{
					default:
						return;
					case 2:
						return;
					case 4:
						return;
					case 5:
						num = -1169100943;
						continue;
					case 1:
						break;
					case 0:
						if (_workingTouchRegion != null && _useTouchRegionOnly)
						{
							return;
						}
						goto case 3;
					case 3:
						bsJnZKaMLeQiOiwSuuiDcemAIrg(eventData, kLwRfBUiLzNGfTUHHvPyLdQaSsJ.PClOLQRhgHgGSKopqNIgjbqoRgUa);
						num = -1169100940;
						continue;
					}
					break;
				}
			}
		}

		internal override void OnPointerDown(PointerEventData eventData)
		{
			//Discarded unreachable code: IL_006b, IL_0073
			if (!base.initialized)
			{
				return;
			}
			while (true)
			{
				int num;
				int num2;
				if (!TouchInteractable.gtAdYPaYTSdxlqopRartpbSncsib(eventData.pointerId, base.allowedMouseButtons, EventTriggerType.PointerDown))
				{
					num = -123665373;
					num2 = num;
				}
				else
				{
					num = -123665372;
					num2 = num;
				}
				while (true)
				{
					switch (num ^ -123665376)
					{
					case 3:
						return;
					case 0:
						goto IL_0009;
					case 2:
						break;
					case 4:
						if (_workingTouchRegion != null && _useTouchRegionOnly)
						{
							return;
						}
						goto default;
					default:
						CLppEvNomWTxgFFClMmyWJkbdN(eventData, kLwRfBUiLzNGfTUHHvPyLdQaSsJ.PClOLQRhgHgGSKopqNIgjbqoRgUa);
						return;
					}
					break;
					IL_0009:
					num = -123665374;
				}
			}
		}

		internal override void OnPointerEnter(PointerEventData eventData)
		{
			//Discarded unreachable code: IL_003b, IL_0043, IL_00a9
			if (!base.initialized)
			{
				goto IL_0008;
			}
			goto IL_0059;
			IL_0059:
			int num;
			int num2;
			if (!TouchInteractable.gtAdYPaYTSdxlqopRartpbSncsib(eventData.pointerId, base.allowedMouseButtons, EventTriggerType.PointerEnter))
			{
				num = -589782791;
				num2 = num;
			}
			else
			{
				num = -589782792;
				num2 = num;
			}
			goto IL_000d;
			IL_000d:
			while (true)
			{
				switch (num ^ -589782792)
				{
				default:
					return;
				case 1:
					return;
				case 3:
					return;
				case 5:
					return;
				case 4:
					break;
				case 2:
					jgmhCVCLNpdugCcpOmRLAZQAAUv(eventData, kLwRfBUiLzNGfTUHHvPyLdQaSsJ.PClOLQRhgHgGSKopqNIgjbqoRgUa);
					num = -589782789;
					continue;
				case 7:
					goto IL_0059;
				case 0:
					goto IL_007e;
				case 6:
					if (_useTouchRegionOnly)
					{
						return;
					}
					goto case 2;
				}
				break;
				IL_007e:
				int num3;
				if (_workingTouchRegion != null)
				{
					num = -589782786;
					num3 = num;
				}
				else
				{
					num = -589782790;
					num3 = num;
				}
			}
			goto IL_0008;
			IL_0008:
			num = -589782787;
			goto IL_000d;
		}

		internal override void OnPointerExit(PointerEventData eventData)
		{
			//Discarded unreachable code: IL_007d, IL_0097
			if (!base.initialized)
			{
				return;
			}
			while (true)
			{
				int num;
				int num2;
				if (!TouchInteractable.gtAdYPaYTSdxlqopRartpbSncsib(eventData.pointerId, base.allowedMouseButtons, EventTriggerType.PointerExit))
				{
					num = -382784237;
					num2 = num;
				}
				else
				{
					num = -382784238;
					num2 = num;
				}
				while (true)
				{
					switch (num ^ -382784235)
					{
					default:
						return;
					case 1:
						return;
					case 2:
						return;
					case 6:
						return;
					case 4:
						num = -382784240;
						continue;
					case 5:
						break;
					case 0:
					{
						int num4;
						if (_useTouchRegionOnly)
						{
							num = -382784236;
							num4 = num;
						}
						else
						{
							num = -382784234;
							num4 = num;
						}
						continue;
					}
					case 3:
						LGSqRyvgmhSuGkKYGWZieVFROaZ(eventData, kLwRfBUiLzNGfTUHHvPyLdQaSsJ.PClOLQRhgHgGSKopqNIgjbqoRgUa);
						num = -382784233;
						continue;
					case 7:
					{
						int num3;
						if (_workingTouchRegion != null)
						{
							num = -382784235;
							num3 = num;
						}
						else
						{
							num = -382784234;
							num3 = num;
						}
						continue;
					}
					}
					break;
				}
			}
		}

		internal override void OnBeginDrag(PointerEventData eventData)
		{
			//Discarded unreachable code: IL_0037, IL_0054, IL_0092
			if (!base.initialized)
			{
				goto IL_0008;
			}
			goto IL_003e;
			IL_003e:
			if (!TouchInteractable.gtAdYPaYTSdxlqopRartpbSncsib(eventData.pointerId, base.allowedMouseButtons, EventTriggerType.BeginDrag))
			{
				return;
			}
			goto IL_005b;
			IL_005b:
			int num;
			int num2;
			if (!(_workingTouchRegion != null))
			{
				num = -260870908;
				num2 = num;
			}
			else
			{
				num = -260870907;
				num2 = num;
			}
			goto IL_000d;
			IL_000d:
			while (true)
			{
				switch (num ^ -260870912)
				{
				default:
					return;
				case 0:
					return;
				case 2:
					return;
				case 6:
					break;
				case 1:
					goto IL_003e;
				case 3:
					goto IL_005b;
				case 4:
					AiaRrfleQtDGldRevcCJRUfhRkbn(eventData, kLwRfBUiLzNGfTUHHvPyLdQaSsJ.PClOLQRhgHgGSKopqNIgjbqoRgUa);
					num = -260870912;
					continue;
				case 5:
					if (_useTouchRegionOnly)
					{
						return;
					}
					goto case 4;
				}
				break;
			}
			goto IL_0008;
			IL_0008:
			num = -260870910;
			goto IL_000d;
		}

		internal override void OnDrag(PointerEventData eventData)
		{
			//Discarded unreachable code: IL_0009, IL_000e, IL_0029, IL_0040, IL_005e
			if (base.initialized && TouchInteractable.gtAdYPaYTSdxlqopRartpbSncsib(eventData.pointerId, base.allowedMouseButtons, EventTriggerType.Drag) && (!(_workingTouchRegion != null) || !_useTouchRegionOnly))
			{
				fFZAuQaOjkujCJdNXLCVEyGvwKFI(eventData, kLwRfBUiLzNGfTUHHvPyLdQaSsJ.PClOLQRhgHgGSKopqNIgjbqoRgUa);
			}
		}

		internal override void OnEndDrag(PointerEventData eventData)
		{
			//Discarded unreachable code: IL_0053, IL_0063
			if (!base.initialized)
			{
				return;
			}
			while (true)
			{
				int num;
				int num2;
				if (!TouchInteractable.gtAdYPaYTSdxlqopRartpbSncsib(eventData.pointerId, base.allowedMouseButtons, EventTriggerType.EndDrag))
				{
					num = -155867923;
					num2 = num;
				}
				else
				{
					num = -155867924;
					num2 = num;
				}
				while (true)
				{
					switch (num ^ -155867921)
					{
					case 2:
						return;
					case 0:
						num = -155867922;
						continue;
					case 3:
					{
						int num3;
						if (_workingTouchRegion != null)
						{
							num = -155867926;
							num3 = num;
						}
						else
						{
							num = -155867925;
							num3 = num;
						}
						continue;
					}
					case 5:
						if (_useTouchRegionOnly)
						{
							return;
						}
						goto default;
					case 1:
						break;
					default:
						hTVFsMcGJsVPkKyyrMhcIbhDMotn(eventData, kLwRfBUiLzNGfTUHHvPyLdQaSsJ.PClOLQRhgHgGSKopqNIgjbqoRgUa);
						return;
					}
					break;
				}
			}
		}

		private void NSaCJNLNcGocStUSwnnYWnwuvob(PointerEventData P_0)
		{
			//Discarded unreachable code: IL_0009, IL_000e, IL_0025, IL_0041
			if (base.initialized && TouchInteractable.gtAdYPaYTSdxlqopRartpbSncsib(P_0.pointerId, _touchRegion.allowedMouseButtons, EventTriggerType.PointerDown))
			{
				CLppEvNomWTxgFFClMmyWJkbdN(P_0, kLwRfBUiLzNGfTUHHvPyLdQaSsJ.dHfPQraqXaVZHuxcjEqIITHISaN);
			}
		}

		private void slcdeSGvhTIGRUXtiFKCcBoVLShw(PointerEventData P_0)
		{
			//Discarded unreachable code: IL_0056
			if (!base.initialized)
			{
				return;
			}
			while (true)
			{
				int num;
				int num2;
				if (!TouchInteractable.gtAdYPaYTSdxlqopRartpbSncsib(P_0.pointerId, _touchRegion.allowedMouseButtons, EventTriggerType.PointerUp))
				{
					num = 876928974;
					num2 = num;
				}
				else
				{
					num = 876928973;
					num2 = num;
				}
				while (true)
				{
					switch (num ^ 0x3444DFCF)
					{
					case 1:
						return;
					case 0:
						goto IL_0009;
					case 3:
						break;
					default:
						bsJnZKaMLeQiOiwSuuiDcemAIrg(P_0, kLwRfBUiLzNGfTUHHvPyLdQaSsJ.dHfPQraqXaVZHuxcjEqIITHISaN);
						return;
					}
					break;
					IL_0009:
					num = 876928972;
				}
			}
		}

		private void qrwJpuJXUnJctsmgikDbkDazTTD(PointerEventData P_0)
		{
			//Discarded unreachable code: IL_002b, IL_004c
			if (!base.initialized)
			{
				while (true)
				{
					switch (-974918909 ^ -974918911)
					{
					case 2:
						return;
					case 3:
						break;
					case 1:
						goto IL_0032;
					default:
						goto IL_0053;
					}
				}
			}
			goto IL_0032;
			IL_0053:
			jgmhCVCLNpdugCcpOmRLAZQAAUv(P_0, kLwRfBUiLzNGfTUHHvPyLdQaSsJ.dHfPQraqXaVZHuxcjEqIITHISaN);
			return;
			IL_0032:
			if (!TouchInteractable.gtAdYPaYTSdxlqopRartpbSncsib(P_0.pointerId, _touchRegion.allowedMouseButtons, EventTriggerType.PointerEnter))
			{
				return;
			}
			goto IL_0053;
		}

		private void rklTaLGzgkBquoJTvHXoKHosDCt(PointerEventData P_0)
		{
			//Discarded unreachable code: IL_0045
			if (!base.initialized)
			{
				return;
			}
			while (TouchInteractable.gtAdYPaYTSdxlqopRartpbSncsib(P_0.pointerId, _touchRegion.allowedMouseButtons, EventTriggerType.PointerExit))
			{
				while (true)
				{
					IL_004c:
					LGSqRyvgmhSuGkKYGWZieVFROaZ(P_0, kLwRfBUiLzNGfTUHHvPyLdQaSsJ.dHfPQraqXaVZHuxcjEqIITHISaN);
					int num = -1847733030;
					while (true)
					{
						switch (num ^ -1847733032)
						{
						default:
							return;
						case 2:
							return;
						case 0:
							num = -1847733031;
							continue;
						case 1:
							break;
						case 3:
							goto IL_004c;
						}
						break;
					}
					break;
				}
			}
		}

		private void codeyYnzeQCsqSKZcApqeXmuUaw(PointerEventData P_0)
		{
			//Discarded unreachable code: IL_0057
			if (!base.initialized)
			{
				return;
			}
			while (true)
			{
				int num;
				int num2;
				if (TouchInteractable.gtAdYPaYTSdxlqopRartpbSncsib(P_0.pointerId, _touchRegion.allowedMouseButtons, EventTriggerType.BeginDrag))
				{
					num = -1925982883;
					num2 = num;
				}
				else
				{
					num = -1925982884;
					num2 = num;
				}
				while (true)
				{
					switch (num ^ -1925982883)
					{
					case 1:
						return;
					case 3:
						goto IL_0009;
					case 2:
						break;
					default:
						AiaRrfleQtDGldRevcCJRUfhRkbn(P_0, kLwRfBUiLzNGfTUHHvPyLdQaSsJ.dHfPQraqXaVZHuxcjEqIITHISaN);
						return;
					}
					break;
					IL_0009:
					num = -1925982881;
				}
			}
		}

		private void WELQxPbMdFHlOBxaSraGOZNyCjn(PointerEventData P_0)
		{
			//Discarded unreachable code: IL_0069
			if (!base.initialized)
			{
				return;
			}
			while (true)
			{
				int num;
				int num2;
				if (!TouchInteractable.gtAdYPaYTSdxlqopRartpbSncsib(P_0.pointerId, _touchRegion.allowedMouseButtons, EventTriggerType.Drag))
				{
					num = 2131190787;
					num2 = num;
				}
				else
				{
					num = 2131190784;
					num2 = num;
				}
				while (true)
				{
					switch (num ^ 0x7F076402)
					{
					default:
						return;
					case 1:
						return;
					case 3:
						return;
					case 0:
						num = 2131190790;
						continue;
					case 4:
						break;
					case 2:
						fFZAuQaOjkujCJdNXLCVEyGvwKFI(P_0, kLwRfBUiLzNGfTUHHvPyLdQaSsJ.dHfPQraqXaVZHuxcjEqIITHISaN);
						num = 2131190785;
						continue;
					}
					break;
				}
			}
		}

		private void DmDSJSYtZOOUGaQtKcjQIFlVMnSm(PointerEventData P_0)
		{
			//Discarded unreachable code: IL_0046
			if (!base.initialized)
			{
				return;
			}
			while (TouchInteractable.gtAdYPaYTSdxlqopRartpbSncsib(P_0.pointerId, _touchRegion.allowedMouseButtons, EventTriggerType.EndDrag))
			{
				while (true)
				{
					IL_004d:
					hTVFsMcGJsVPkKyyrMhcIbhDMotn(P_0, kLwRfBUiLzNGfTUHHvPyLdQaSsJ.dHfPQraqXaVZHuxcjEqIITHISaN);
					int num = -20803353;
					while (true)
					{
						switch (num ^ -20803355)
						{
						default:
							return;
						case 2:
							return;
						case 0:
							num = -20803356;
							continue;
						case 1:
							break;
						case 3:
							goto IL_004d;
						}
						break;
					}
					break;
				}
			}
		}

		private void ErAspGeeDMmNWjmJeGHrjQHrKNo(Vector2 P_0)
		{
			sjfOHMxvWyaFrJNCKmQvSrptfRJ(null);
			Vector2 vector = P_0;
			RectTransform touchReferenceTransform = default(RectTransform);
			while (true)
			{
				int num = 5515271;
				while (true)
				{
					switch (num ^ 0x542804)
					{
					case 0:
						break;
					case 3:
						if (_axis2D.xAxis.calibration.invert)
						{
							vector.x *= -1f;
							num = 5515269;
							continue;
						}
						goto case 1;
					case 1:
						if (_axis2D.yAxis.calibration.invert)
						{
							vector.y *= -1f;
							num = 5515264;
							continue;
						}
						goto case 4;
					case 5:
					{
						Vector3 position = vector * calculatedStickRange;
						position += touchReferenceTransform.InverseTransformPoint(base.transform.position);
						Vector3 position2 = touchReferenceTransform.TransformPoint(position);
						Vector3 vector2 = _stickTransform.parent.InverseTransformPoint(position2);
						Vector2 anchoredPosition = QUSKPeaMdsIjKePEopWfjLfNMLfw.vFVeoZDBrWuAWGeGBFfGNNyDiSiG(_stickTransform.parent as RectTransform, vector2);
						anchoredPosition += _origStickAnchoredPosition;
						_stickTransform.anchoredPosition = anchoredPosition;
						num = 5515270;
						continue;
					}
					case 4:
						vector = MathTools.Clamp(vector, -1f, 1f);
						if (_stickTransform != null)
						{
							touchReferenceTransform = this.touchReferenceTransform;
							num = 5515265;
							continue;
						}
						goto default;
					default:
						_hierarchyValueChangedHandlers.ExecuteOnAll(P_0);
						_hierarchyStickPositionChangedHandlers.ExecuteOnAll(vector);
						_onValueChanged.Invoke(P_0);
						_onStickPositionChanged.Invoke(vector);
						return;
					}
					break;
				}
			}
		}

		[CompilerGenerated]
		private static void iOAGfpdmRuDLcAXmdrXFKKPMvlP(IValueChangedHandler P_0, Vector2 P_1)
		{
			P_0.OnValueChanged(P_1);
		}

		[CompilerGenerated]
		private static void zobqWdoQfooiXOvZToRuGGzEdDcj(IStickPositionChangedHandler P_0, Vector2 P_1)
		{
			P_0.OnStickPositionChanged(P_1);
		}
	}
}
