using Rewired.Utils;
using System;
using System.Collections;
using UnityEngine;

namespace Rewired.ComponentControls
{
	[Serializable]
	[DisallowMultipleComponent]
	public abstract class ComponentControl : MonoBehaviour, IComponentControl
	{
		private IComponentController _controller;

		[NonSerialized]
		private bool IoRFdMQoQlRiOSEMjmYGiNSOeMq;

		[NonSerialized]
		private bool xrnCmAVSQONYdzJrDeARfwrcuFg;

		private int _lastUpdateFrame = -1;

		internal abstract bool hasController
		{
			get;
		}

		internal bool initialized => IoRFdMQoQlRiOSEMjmYGiNSOeMq;

		[CustomObfuscation(rename = false)]
		internal ComponentControl()
		{
		}

		public abstract void ClearValue();

		void IComponentControl.Update()
		{
			//Discarded unreachable code: IL_002e
			int frameCount = Time.frameCount;
			if (_lastUpdateFrame == frameCount)
			{
				while (true)
				{
					switch (0x783EC09B ^ 0x783EC09A)
					{
					case 1:
						return;
					case 0:
						continue;
					}
					break;
				}
			}
			_lastUpdateFrame = frameCount;
			OnUpdate();
		}

		[CustomObfuscation(rename = false)]
		internal virtual void Awake()
		{
			xrnCmAVSQONYdzJrDeARfwrcuFg = true;
		}

		[CustomObfuscation(rename = false)]
		internal virtual void Start()
		{
		}

		[CustomObfuscation(rename = false)]
		internal virtual void OnEnable()
		{
			//Discarded unreachable code: IL_0051, IL_0060
			if (!xrnCmAVSQONYdzJrDeARfwrcuFg)
			{
				IoRFdMQoQlRiOSEMjmYGiNSOeMq = false;
				while (true)
				{
					int num = 1405424120;
					while (true)
					{
						switch (num ^ 0x53C511F9)
						{
						case 0:
							break;
						case 1:
							StartCoroutine(NmDRsHjksrJFBYDLlbVJcOMiFbHa());
							num = 1405424122;
							continue;
						case 3:
							xrnCmAVSQONYdzJrDeARfwrcuFg = true;
							return;
						case 4:
							goto IL_0058;
						default:
							goto IL_0067;
						}
						break;
					}
				}
			}
			goto IL_0058;
			IL_0058:
			if (!Application.isPlaying)
			{
				return;
			}
			goto IL_0067;
			IL_0067:
			GyMEixNRvJvwisdpRohOwzNsGfY();
		}

		[CustomObfuscation(rename = false)]
		internal virtual void OnDisable()
		{
			if (!Application.isPlaying)
			{
				return;
			}
			while (true)
			{
				VnHLicMRKJjUECjClwavfguFDdIv();
				int num = 942899699;
				while (true)
				{
					switch (num ^ 0x383381F3)
					{
					default:
						return;
					case 0:
						return;
					case 2:
						goto IL_0008;
					case 1:
						break;
					}
					break;
					IL_0008:
					num = 942899698;
				}
			}
		}

		[CustomObfuscation(rename = false)]
		internal virtual void OnDestroy()
		{
		}

		[CustomObfuscation(rename = false)]
		internal virtual void OnValidate()
		{
			if (IoRFdMQoQlRiOSEMjmYGiNSOeMq)
			{
				GBExVtHFPRLrKdqZIHqEWcjtFsS();
			}
		}

		[CustomObfuscation(rename = false)]
		internal virtual void OnCanvasGroupChanged()
		{
			//Discarded unreachable code: IL_0027
			if (!IoRFdMQoQlRiOSEMjmYGiNSOeMq)
			{
				while (true)
				{
					switch (0x318551F ^ 0x318551E)
					{
					case 1:
						return;
					case 0:
						continue;
					}
					break;
				}
			}
			PJzFLmWZwITkzMSfvetoBbOUCxHf(false, false);
		}

		[CustomObfuscation(rename = false)]
		internal virtual void OnTransformParentChanged()
		{
			if (IoRFdMQoQlRiOSEMjmYGiNSOeMq)
			{
				PJzFLmWZwITkzMSfvetoBbOUCxHf(false, false);
			}
		}

		[CustomObfuscation(rename = false)]
		internal virtual void OnDidApplyAnimationProperties()
		{
			_ = IoRFdMQoQlRiOSEMjmYGiNSOeMq;
		}

		[CustomObfuscation(rename = false)]
		internal virtual void Reset()
		{
			_ = IoRFdMQoQlRiOSEMjmYGiNSOeMq;
		}

		internal virtual void OnUpdate()
		{
		}

		internal virtual bool OnInitialize()
		{
			IoRFdMQoQlRiOSEMjmYGiNSOeMq = false;
			if (!PJzFLmWZwITkzMSfvetoBbOUCxHf(true, true))
			{
				return false;
			}
			_controller.Register(this);
			return true;
		}

		internal virtual void VnHLicMRKJjUECjClwavfguFDdIv()
		{
			ClearValue();
			if (!_controller.IsNullOrDestroyed())
			{
				_controller.Deregister(this);
				goto IL_001f;
			}
			goto IL_003d;
			IL_001f:
			int num = 12285394;
			goto IL_0024;
			IL_003d:
			OnUnsubscribeEvents();
			IoRFdMQoQlRiOSEMjmYGiNSOeMq = false;
			num = 12285393;
			goto IL_0024;
			IL_0024:
			switch (num ^ 0xBB75D3)
			{
			default:
				return;
			case 2:
				return;
			case 0:
				break;
			case 1:
				goto IL_003d;
			}
			goto IL_001f;
		}

		internal virtual void OnSubscribeEvents()
		{
			//Discarded unreachable code: IL_002c
			if (_controller.IsNullOrDestroyed())
			{
				while (true)
				{
					switch (0x5D1AC40E ^ 0x5D1AC40F)
					{
					case 1:
						return;
					case 2:
						continue;
					}
					break;
				}
			}
			OnUnsubscribeEvents();
		}

		internal virtual void OnUnsubscribeEvents()
		{
			_controller.IsNullOrDestroyed();
		}

		internal virtual void OnSetProperty()
		{
			if (!IoRFdMQoQlRiOSEMjmYGiNSOeMq)
			{
				return;
			}
			while (true)
			{
				GBExVtHFPRLrKdqZIHqEWcjtFsS();
				int num = -1708123204;
				while (true)
				{
					switch (num ^ -1708123204)
					{
					default:
						return;
					case 0:
						return;
					case 2:
						goto IL_0009;
					case 1:
						break;
					}
					break;
					IL_0009:
					num = -1708123203;
				}
			}
		}

		internal virtual void OnClear()
		{
			_ = IoRFdMQoQlRiOSEMjmYGiNSOeMq;
		}

		internal virtual void FindEventHandlers()
		{
		}

		internal bool agGGiexlYIVgemjaHHzzcCmJTcc()
		{
			return UnityTools.IsActiveAndEnabled(this);
		}

		internal bool fTmOHcynxkhtiIjyUGTEdDIicBqJ()
		{
			return this == null;
		}

		internal IComponentController CaXYfBIdheMUKBrjNhdpQLCTFAB()
		{
			return _controller;
		}

		[CustomObfuscation(rename = false)]
		internal abstract IComponentController FindController();

		[CustomObfuscation(rename = false)]
		internal abstract Type GetRequiredControllerType();

		private IEnumerator NmDRsHjksrJFBYDLlbVJcOMiFbHa()
		{
			mtRegEaMRIiODCdUwYZcAGPpIzx mtRegEaMRIiODCdUwYZcAGPpIzx = new mtRegEaMRIiODCdUwYZcAGPpIzx(0);
			while (true)
			{
				int num = -38994829;
				while (true)
				{
					switch (num ^ -38994830)
					{
					case 2:
						break;
					case 1:
						goto IL_0025;
					default:
						return mtRegEaMRIiODCdUwYZcAGPpIzx;
					}
					break;
					IL_0025:
					mtRegEaMRIiODCdUwYZcAGPpIzx.hFQAwvywpKYhLMOJAVKlWFLojQb = this;
					num = -38994830;
				}
			}
		}

		private void GyMEixNRvJvwisdpRohOwzNsGfY()
		{
			if (!OnInitialize())
			{
				return;
			}
			while (true)
			{
				FindEventHandlers();
				IoRFdMQoQlRiOSEMjmYGiNSOeMq = true;
				OnSubscribeEvents();
				int num = -1972477296;
				while (true)
				{
					switch (num ^ -1972477295)
					{
					default:
						return;
					case 1:
						return;
					case 0:
						goto IL_0009;
					case 2:
						break;
					}
					break;
					IL_0009:
					num = -1972477293;
				}
			}
		}

		private bool PJzFLmWZwITkzMSfvetoBbOUCxHf(bool P_0, bool P_1)
		{
			//Discarded unreachable code: IL_00d0
			bool flag = false;
			try
			{
				IComponentController componentController = FindController();
				if (!_controller.IsNullOrDestroyed() && _controller != componentController)
				{
					goto IL_0025;
				}
				goto IL_00a7;
				IL_0025:
				int num = -1014987360;
				goto IL_002a;
				IL_00a7:
				_controller = componentController;
				int num2;
				if (_controller == null)
				{
					num = -1014987359;
					num2 = num;
				}
				else
				{
					num = -1014987356;
					num2 = num;
				}
				goto IL_002a;
				IL_002a:
				Type type = default(Type);
				while (true)
				{
					switch (num ^ -1014987356)
					{
					case 6:
						break;
					case 4:
						flag = true;
						num = -1014987355;
						continue;
					case 0:
						if (!P_0 && flag)
						{
							GyMEixNRvJvwisdpRohOwzNsGfY();
							num = -1014987353;
							continue;
						}
						goto default;
					case 8:
						Logger.LogError(type.Name + " could not be found. You must have a component that extends from " + type.Name + " on this or a parent GameObject.");
						num = -1014987357;
						continue;
					case 1:
						goto IL_00a7;
					case 7:
						throw new Exception();
					case 2:
						goto IL_00da;
					case 5:
						type = GetRequiredControllerType();
						if (type == null)
						{
							type = typeof(IComponentController);
							num = -1014987354;
							continue;
						}
						goto IL_00da;
					default:
						return true;
					}
					break;
					IL_00da:
					int num3;
					if (!P_1)
					{
						num = -1014987357;
						num3 = num;
					}
					else
					{
						num = -1014987348;
						num3 = num;
					}
				}
				goto IL_0025;
			}
			catch
			{
				VnHLicMRKJjUECjClwavfguFDdIv();
				return false;
			}
		}

		private void GBExVtHFPRLrKdqZIHqEWcjtFsS()
		{
			PJzFLmWZwITkzMSfvetoBbOUCxHf(false, true);
		}

		private void AZKlekytQLJdyWzEbfEIoqtMPaN()
		{
			//Discarded unreachable code: IL_002f
			if (fTmOHcynxkhtiIjyUGTEdDIicBqJ())
			{
				return;
			}
			if (!agGGiexlYIVgemjaHHzzcCmJTcc())
			{
				while (true)
				{
					switch (-1008874279 ^ -1008874280)
					{
					case 1:
						return;
					case 0:
						continue;
					}
					break;
				}
			}
			OnUpdate();
		}
	}
}
