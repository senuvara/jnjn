using Rewired.ComponentControls.Data;
using Rewired.Utils;
using System;
using UnityEngine;

namespace Rewired.ComponentControls
{
	[Serializable]
	[DisallowMultipleComponent]
	public abstract class CustomControllerControl : ComponentControl
	{
		internal CustomController controller => CaXYfBIdheMUKBrjNhdpQLCTFAB() as CustomController;

		internal override bool hasController => CaXYfBIdheMUKBrjNhdpQLCTFAB() as CustomController != null;

		[CustomObfuscation(rename = false)]
		internal CustomControllerControl()
		{
		}

		internal override void OnSubscribeEvents()
		{
			base.OnSubscribeEvents();
			if (hasController)
			{
				OnUnsubscribeEvents();
				controller.InputSourceUpdateEvent += UxWOEVNVKxiEBhjmlXLnDrkeChl;
			}
		}

		internal override void OnUnsubscribeEvents()
		{
			base.OnUnsubscribeEvents();
			if (hasController)
			{
				controller.InputSourceUpdateEvent -= UxWOEVNVKxiEBhjmlXLnDrkeChl;
			}
		}

		[CustomObfuscation(rename = false)]
		internal override IComponentController FindController()
		{
			return UnityTools.GetComponentInSelfOrParents<CustomController>(base.transform);
		}

		[CustomObfuscation(rename = false)]
		internal override Type GetRequiredControllerType()
		{
			return typeof(CustomController);
		}

		internal void yQjPzGJGxRgoFPaCGxcgBaQECRD(CustomControllerElementTargetSet P_0, float P_1, float P_2)
		{
			//Discarded unreachable code: IL_0051, IL_00ac, IL_00e3, IL_0106
			if (!hasController)
			{
				goto IL_000b;
			}
			goto IL_00a8;
			IL_00a8:
			if (P_0 == null)
			{
				return;
			}
			goto IL_0058;
			IL_0058:
			CustomControllerElementTargetSetForFloat customControllerElementTargetSetForFloat = P_0 as CustomControllerElementTargetSetForFloat;
			int num;
			if (customControllerElementTargetSetForFloat != null)
			{
				int num2;
				if (!customControllerElementTargetSetForFloat.splitValue)
				{
					num = -1604263876;
					num2 = num;
				}
				else
				{
					num = -1604263883;
					num2 = num;
				}
				goto IL_0010;
			}
			goto IL_00b6;
			IL_00b6:
			CustomControllerElementTargetSetForBoolean customControllerElementTargetSetForBoolean = P_0 as CustomControllerElementTargetSetForBoolean;
			num = -1604263888;
			goto IL_0010;
			IL_0010:
			while (true)
			{
				switch (num ^ -1604263883)
				{
				default:
					return;
				case 3:
					return;
				case 4:
					return;
				case 10:
					return;
				case 11:
					return;
				case 8:
					break;
				case 6:
					goto IL_0058;
				case 2:
					yQjPzGJGxRgoFPaCGxcgBaQECRD(customControllerElementTargetSetForFloat.negativeTarget, P_1, P_2);
					num = -1604263873;
					continue;
				case 9:
					yQjPzGJGxRgoFPaCGxcgBaQECRD(customControllerElementTargetSetForFloat.target, P_1, P_2);
					num = -1604263882;
					continue;
				case 7:
					goto IL_00a8;
				case 1:
					goto IL_00b6;
				case 5:
					if (customControllerElementTargetSetForBoolean != null)
					{
						yQjPzGJGxRgoFPaCGxcgBaQECRD(customControllerElementTargetSetForBoolean.target, P_1, P_2);
						num = -1604263874;
						continue;
					}
					return;
				case 0:
					yQjPzGJGxRgoFPaCGxcgBaQECRD(customControllerElementTargetSetForFloat.positiveTarget, P_1, P_2);
					num = -1604263881;
					continue;
				}
				break;
			}
			goto IL_000b;
			IL_000b:
			num = -1604263887;
			goto IL_0010;
		}

		internal void yQjPzGJGxRgoFPaCGxcgBaQECRD(CustomControllerElementTargetSet P_0, bool P_1)
		{
			//Discarded unreachable code: IL_0043, IL_0087, IL_00b8
			if (!hasController)
			{
				return;
			}
			CustomControllerElementTargetSetForFloat customControllerElementTargetSetForFloat = default(CustomControllerElementTargetSetForFloat);
			while (P_0 != null)
			{
				while (true)
				{
					CustomControllerElementTargetSetForBoolean customControllerElementTargetSetForBoolean = P_0 as CustomControllerElementTargetSetForBoolean;
					int num;
					if (customControllerElementTargetSetForBoolean != null)
					{
						yQjPzGJGxRgoFPaCGxcgBaQECRD(customControllerElementTargetSetForBoolean.target, P_1);
						num = 651641082;
						goto IL_000e;
					}
					goto IL_0068;
					IL_000e:
					while (true)
					{
						switch (num ^ 0x26D740F2)
						{
						default:
							return;
						case 3:
							return;
						case 8:
							return;
						case 0:
							num = 651641075;
							continue;
						case 5:
							break;
						case 4:
							goto IL_0068;
						case 1:
							goto IL_0083;
						case 7:
							goto IL_008e;
						case 2:
							yQjPzGJGxRgoFPaCGxcgBaQECRD(customControllerElementTargetSetForFloat.target, P_1);
							return;
						case 6:
							yQjPzGJGxRgoFPaCGxcgBaQECRD(customControllerElementTargetSetForFloat.positiveTarget, P_1);
							yQjPzGJGxRgoFPaCGxcgBaQECRD(customControllerElementTargetSetForFloat.negativeTarget, P_1);
							num = 651641073;
							continue;
						}
						break;
						IL_008e:
						int num2;
						if (customControllerElementTargetSetForFloat.splitValue)
						{
							num = 651641076;
							num2 = num;
						}
						else
						{
							num = 651641072;
							num2 = num;
						}
					}
					continue;
					IL_0068:
					customControllerElementTargetSetForFloat = (P_0 as CustomControllerElementTargetSetForFloat);
					int num3;
					if (customControllerElementTargetSetForFloat == null)
					{
						num = 651641073;
						num3 = num;
					}
					else
					{
						num = 651641077;
						num3 = num;
					}
					goto IL_000e;
				}
				IL_0083:;
			}
		}

		internal abstract void OnCustomControllerUpdate();

		private void yQjPzGJGxRgoFPaCGxcgBaQECRD(CustomControllerElementTarget P_0, float P_1, float P_2)
		{
			//Discarded unreachable code: IL_010f, IL_0240
			if (P_0 == null)
			{
				return;
			}
			CustomControllerElementTarget.ValueRange valueRange = default(CustomControllerElementTarget.ValueRange);
			while (true)
			{
				CustomControllerElementSelector.ElementType elementType = P_0.element.elementType;
				CustomControllerElementSelector.ElementType elementType2 = elementType;
				int num = 473983316;
				while (true)
				{
					int num2;
					switch (num ^ 0x1C40695C)
					{
					case 6:
						return;
					case 0:
						num = 473983323;
						continue;
					case 15:
						if (P_1 > 0f)
						{
							P_1 = 0f;
							num = 473983312;
							continue;
						}
						goto case 12;
					case 4:
					{
						CustomControllerElementTarget.ValueRange valueRange2 = P_0.valueRange;
						valueRange = valueRange2;
						num = 473983318;
						continue;
					}
					case 10:
						switch (valueRange)
						{
						case CustomControllerElementTarget.ValueRange.Negative:
							break;
						default:
							goto IL_00b0;
						case CustomControllerElementTarget.ValueRange.Positive:
							goto IL_0123;
						case CustomControllerElementTarget.ValueRange.Full:
							goto IL_018c;
						}
						goto case 15;
					case 20:
						P_1 *= -1f;
						num = 473983327;
						continue;
					case 13:
						if (P_1 > 0f)
						{
							P_1 = 0f;
							num = 473983308;
							continue;
						}
						goto IL_01fb;
					case 14:
						P_1 *= -1f;
						num = 473983327;
						continue;
					case 3:
						controller.SetAxisValue(P_0.element, P_1);
						return;
					case 19:
						num = 473983325;
						continue;
					case 2:
						goto IL_0123;
					case 17:
						goto IL_013f;
					case 9:
						num = 473983308;
						continue;
					case 11:
						goto IL_0165;
					case 5:
						goto IL_018c;
					case 12:
					{
						int num3;
						if (P_0.valueContribution == Pole.Positive)
						{
							num = 473983314;
							num3 = num;
						}
						else
						{
							num = 473983327;
							num3 = num;
						}
						continue;
					}
					case 7:
						break;
					case 18:
						if (P_0.valueContribution == Pole.Negative)
						{
							P_1 *= -1f;
							num = 473983327;
							continue;
						}
						goto case 3;
					case 16:
						goto IL_01fb;
					case 8:
						switch (elementType2)
						{
						case CustomControllerElementSelector.ElementType.Axis:
							break;
						case CustomControllerElementSelector.ElementType.Button:
							goto IL_0165;
						default:
							goto IL_0235;
						}
						goto case 4;
					default:
						{
							throw new NotImplementedException();
						}
						IL_0235:
						num = 473983311;
						continue;
						IL_0165:
						switch (P_0.valueRange)
						{
						case CustomControllerElementTarget.ValueRange.Negative:
							break;
						case CustomControllerElementTarget.ValueRange.Positive:
							goto IL_013f;
						default:
							goto IL_0182;
						case CustomControllerElementTarget.ValueRange.Full:
							goto IL_01fb;
						}
						goto case 13;
						IL_0182:
						num = 473983308;
						continue;
						IL_013f:
						if (P_1 < 0f)
						{
							P_1 = 0f;
							num = 473983317;
							continue;
						}
						goto IL_01fb;
						IL_00b0:
						num = 473983327;
						continue;
						IL_01fb:
						controller.SetButtonValue(P_0.element, MathTools.Abs(P_1) >= MathTools.Abs(P_2));
						num = 473983322;
						continue;
						IL_018c:
						if (!P_0.invert)
						{
							num = 473983327;
							num2 = num;
						}
						else
						{
							num = 473983304;
							num2 = num;
						}
						continue;
						IL_0123:
						if (P_1 < 0f)
						{
							P_1 = 0f;
							num = 473983310;
							continue;
						}
						goto case 18;
					}
					break;
				}
			}
		}

		private void yQjPzGJGxRgoFPaCGxcgBaQECRD(CustomControllerElementTarget P_0, bool P_1)
		{
			//Discarded unreachable code: IL_006e, IL_0141
			if (P_0 == null)
			{
				return;
			}
			float num3 = default(float);
			CustomControllerElementSelector.ElementType elementType2 = default(CustomControllerElementSelector.ElementType);
			while (true)
			{
				CustomControllerElementSelector.ElementType elementType = P_0.element.elementType;
				int num = 1848614467;
				while (true)
				{
					switch (num ^ 0x6E2F9E42)
					{
					case 2:
						return;
					case 7:
						num = 1848614478;
						continue;
					case 10:
						num = 1848614475;
						continue;
					case 3:
						controller.SetAxisValue(P_0.element, num3);
						return;
					case 11:
						num3 *= -1f;
						num = 1848614470;
						continue;
					case 8:
						controller.SetButtonValue(P_0.element, P_1);
						num = 1848614464;
						continue;
					case 5:
						goto IL_00a0;
					case 1:
						elementType2 = elementType;
						num = 1848614479;
						continue;
					case 4:
						num = 1848614465;
						continue;
					case 13:
						switch (elementType2)
						{
						case CustomControllerElementSelector.ElementType.Button:
							break;
						case CustomControllerElementSelector.ElementType.Axis:
							goto IL_00a0;
						default:
							goto IL_00de;
						}
						goto case 8;
					case 12:
						break;
					case 0:
						if (P_0.valueRange == CustomControllerElementTarget.ValueRange.Full)
						{
							int num2;
							if (P_0.invert)
							{
								num = 1848614473;
								num2 = num;
							}
							else
							{
								num = 1848614465;
								num2 = num;
							}
							continue;
						}
						goto case 6;
					case 6:
						if (P_0.valueContribution == Pole.Negative)
						{
							num3 *= -1f;
							num = 1848614465;
							continue;
						}
						goto case 3;
					default:
						{
							throw new NotImplementedException();
						}
						IL_00de:
						num = 1848614472;
						continue;
						IL_00a0:
						num3 = (P_1 ? 1f : 0f);
						num = 1848614466;
						continue;
					}
					break;
				}
			}
		}

		private void UxWOEVNVKxiEBhjmlXLnDrkeChl()
		{
			//Discarded unreachable code: IL_0044
			if (fTmOHcynxkhtiIjyUGTEdDIicBqJ())
			{
				return;
			}
			while (true)
			{
				int num = -484101758;
				while (true)
				{
					switch (num ^ -484101757)
					{
					case 3:
						return;
					case 0:
						break;
					case 1:
					{
						int num2;
						if (!agGGiexlYIVgemjaHHzzcCmJTcc())
						{
							num = -484101760;
							num2 = num;
						}
						else
						{
							num = -484101759;
							num2 = num;
						}
						continue;
					}
					default:
						OnCustomControllerUpdate();
						return;
					}
					break;
				}
			}
		}
	}
}
