using Rewired.Utils;
using System;
using UnityEngine;

namespace Rewired.ComponentControls
{
	[Serializable]
	[DisallowMultipleComponent]
	[RequireComponent(typeof(RectTransform))]
	public abstract class TouchControl : CustomControllerControl
	{
		private Canvas _canvas;

		private RectTransform __rectTransform;

		internal TouchController touchController => CaXYfBIdheMUKBrjNhdpQLCTFAB() as TouchController;

		internal Canvas canvas => _canvas;

		internal RectTransform canvasTransform
		{
			get
			{
				Canvas canvas = this.canvas;
				if (canvas == null)
				{
					return null;
				}
				return canvas.transform as RectTransform;
			}
		}

		internal RectTransform rectTransform => __rectTransform ?? (__rectTransform = GetComponent<RectTransform>());

		internal override bool hasController => CaXYfBIdheMUKBrjNhdpQLCTFAB() as TouchController != null;

		[CustomObfuscation(rename = false)]
		internal TouchControl()
		{
		}

		[CustomObfuscation(rename = false)]
		internal override void OnValidate()
		{
			//Discarded unreachable code: IL_002d
			base.OnValidate();
			if (!base.initialized)
			{
				while (true)
				{
					switch (-1098774810 ^ -1098774809)
					{
					case 1:
						return;
					case 0:
						continue;
					}
					break;
				}
			}
			PJzFLmWZwITkzMSfvetoBbOUCxHf(true, false);
		}

		[CustomObfuscation(rename = false)]
		internal override void OnCanvasGroupChanged()
		{
			base.OnCanvasGroupChanged();
			if (base.initialized)
			{
				PJzFLmWZwITkzMSfvetoBbOUCxHf(false, true);
			}
		}

		[CustomObfuscation(rename = false)]
		internal override void OnTransformParentChanged()
		{
			//Discarded unreachable code: IL_002d
			base.OnTransformParentChanged();
			if (!base.initialized)
			{
				while (true)
				{
					switch (0x7A622BDA ^ 0x7A622BDB)
					{
					case 1:
						return;
					case 2:
						continue;
					}
					break;
				}
			}
			PJzFLmWZwITkzMSfvetoBbOUCxHf(false, true);
		}

		internal override bool OnInitialize()
		{
			if (!base.OnInitialize())
			{
				return false;
			}
			if (!PJzFLmWZwITkzMSfvetoBbOUCxHf(true, true))
			{
				return false;
			}
			return true;
		}

		internal override void OnSetProperty()
		{
			base.OnSetProperty();
			if (!base.initialized)
			{
				return;
			}
			while (true)
			{
				PJzFLmWZwITkzMSfvetoBbOUCxHf(true, true);
				int num = -1739849923;
				while (true)
				{
					switch (num ^ -1739849924)
					{
					default:
						return;
					case 1:
						return;
					case 0:
						goto IL_000f;
					case 2:
						break;
					}
					break;
					IL_000f:
					num = -1739849922;
				}
			}
		}

		[CustomObfuscation(rename = false)]
		internal override IComponentController FindController()
		{
			return UnityTools.GetComponentInSelfOrParents<CustomController>(base.transform);
		}

		[CustomObfuscation(rename = false)]
		internal override Type GetRequiredControllerType()
		{
			return typeof(TouchController);
		}

		private bool PJzFLmWZwITkzMSfvetoBbOUCxHf(bool P_0, bool P_1)
		{
			_canvas = UnityTools.GetComponentInSelfOrParents<Canvas>(base.gameObject);
			while (true)
			{
				int num = 717064686;
				while (true)
				{
					switch (num ^ 0x2ABD89EF)
					{
					case 4:
						break;
					case 1:
						if (_canvas == null)
						{
							num = 717064685;
							continue;
						}
						if (_canvas.renderMode == RenderMode.WorldSpace)
						{
							if (P_1)
							{
								Logger.LogError("Touch controls cannot be used with a world space Canvas. Change the canvas render mode to screen space.");
								num = 717064687;
								continue;
							}
							goto default;
						}
						return true;
					case 3:
						return false;
					case 2:
						if (P_0)
						{
							Logger.LogError("No Canvas was found. Touch controls must be a child of a Canvas.");
							num = 717064684;
							continue;
						}
						goto case 3;
					default:
						return false;
					}
					break;
				}
			}
		}
	}
}
