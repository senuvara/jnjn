using System;
using UnityEngine;
using UnityEngine.Events;
using UnityEngine.EventSystems;

namespace Rewired.ComponentControls
{
	[Serializable]
	[DisallowMultipleComponent]
	public sealed class TouchRegion : TouchInteractable
	{
		[Serializable]
		private class MptdvUfCYwBKDJwVQcKsBIGtXfWw : UnityEvent<PointerEventData>
		{
		}

		[Serializable]
		private class FXgoGuSOFURUgzBTIspIHlHGNic : UnityEvent<PointerEventData>
		{
		}

		[Serializable]
		private class yrbRSjLTdzBjbzdfhxtpEmbuZiC : UnityEvent<PointerEventData>
		{
		}

		[Serializable]
		private class AdPxUCcGqMYwlgcaagcyCbLafchW : UnityEvent<PointerEventData>
		{
		}

		[Serializable]
		private class NrPwfUZfWJuXtELYKnTPjYarOJF : UnityEvent<PointerEventData>
		{
		}

		[Serializable]
		private class pkBEWWnCNxmSzphNfugwHyUduPw : UnityEvent<PointerEventData>
		{
		}

		[Serializable]
		private class ggtqDwVdwhLEBVuqdqpxfUNFkgw : UnityEvent<PointerEventData>
		{
		}

		[Tooltip("If enabled, the Touch Region will be hidden when gameplay starts.")]
		[CustomObfuscation(rename = false)]
		[SerializeField]
		private bool _hideAtRuntime = true;

		private MptdvUfCYwBKDJwVQcKsBIGtXfWw _onPointerDown = new MptdvUfCYwBKDJwVQcKsBIGtXfWw();

		private FXgoGuSOFURUgzBTIspIHlHGNic _onPointerUp = new FXgoGuSOFURUgzBTIspIHlHGNic();

		private yrbRSjLTdzBjbzdfhxtpEmbuZiC _onPointerEnter = new yrbRSjLTdzBjbzdfhxtpEmbuZiC();

		private AdPxUCcGqMYwlgcaagcyCbLafchW _onPointerExit = new AdPxUCcGqMYwlgcaagcyCbLafchW();

		private NrPwfUZfWJuXtELYKnTPjYarOJF _onBeginDrag = new NrPwfUZfWJuXtELYKnTPjYarOJF();

		private pkBEWWnCNxmSzphNfugwHyUduPw _onDrag = new pkBEWWnCNxmSzphNfugwHyUduPw();

		private ggtqDwVdwhLEBVuqdqpxfUNFkgw _onEndDrag = new ggtqDwVdwhLEBVuqdqpxfUNFkgw();

		public bool hideAtRuntime
		{
			get
			{
				return _hideAtRuntime;
			}
			set
			{
				if (!(_hideAtRuntime = value))
				{
					_hideAtRuntime = true;
					OnSetProperty();
				}
			}
		}

		public event UnityAction<PointerEventData> PointerDownEvent
		{
			add
			{
				_onPointerDown.AddListener(value);
			}
			remove
			{
				_onPointerDown.RemoveListener(value);
			}
		}

		public event UnityAction<PointerEventData> PointerUpEvent
		{
			add
			{
				_onPointerUp.AddListener(value);
			}
			remove
			{
				_onPointerUp.RemoveListener(value);
			}
		}

		public event UnityAction<PointerEventData> PointerEnterEvent
		{
			add
			{
				_onPointerEnter.AddListener(value);
			}
			remove
			{
				_onPointerEnter.RemoveListener(value);
			}
		}

		public event UnityAction<PointerEventData> PointerExitEvent
		{
			add
			{
				_onPointerExit.AddListener(value);
			}
			remove
			{
				_onPointerExit.RemoveListener(value);
			}
		}

		public event UnityAction<PointerEventData> BeginDragEvent
		{
			add
			{
				_onBeginDrag.AddListener(value);
			}
			remove
			{
				_onBeginDrag.RemoveListener(value);
			}
		}

		public event UnityAction<PointerEventData> DragEvent
		{
			add
			{
				_onDrag.AddListener(value);
			}
			remove
			{
				_onDrag.RemoveListener(value);
			}
		}

		public event UnityAction<PointerEventData> EndDragEvent
		{
			add
			{
				_onEndDrag.AddListener(value);
			}
			remove
			{
				_onEndDrag.RemoveListener(value);
			}
		}

		[CustomObfuscation(rename = false)]
		private TouchRegion()
		{
		}

		[CustomObfuscation(rename = false)]
		internal override void Awake()
		{
			base.Awake();
			if (!Application.isPlaying)
			{
				return;
			}
			while (_hideAtRuntime)
			{
				base.visible = false;
				int num = 1540583445;
				while (true)
				{
					switch (num ^ 0x5BD37014)
					{
					default:
						return;
					case 1:
						return;
					case 0:
						goto IL_000e;
					case 2:
						break;
					}
					break;
					IL_000e:
					num = 1540583446;
				}
			}
		}

		public override void ClearValue()
		{
		}

		internal override void OnCustomControllerUpdate()
		{
		}

		internal override void OnPointerDown(PointerEventData eventData)
		{
			//Discarded unreachable code: IL_0051, IL_0059, IL_00a0
			base.OnPointerDown(eventData);
			while (true)
			{
				int num = -1643153865;
				while (true)
				{
					switch (num ^ -1643153867)
					{
					default:
						return;
					case 4:
						return;
					case 7:
						return;
					case 5:
						break;
					case 0:
						if (!TouchInteractable.gtAdYPaYTSdxlqopRartpbSncsib(eventData.pointerId, base.allowedMouseButtons, EventTriggerType.PointerDown))
						{
							return;
						}
						goto case 3;
					case 3:
						if (_onPointerDown != null)
						{
							_onPointerDown.Invoke(eventData);
							num = -1643153871;
							continue;
						}
						return;
					case 6:
					{
						int num3;
						if (agGGiexlYIVgemjaHHzzcCmJTcc())
						{
							num = -1643153868;
							num3 = num;
						}
						else
						{
							num = -1643153870;
							num3 = num;
						}
						continue;
					}
					case 2:
						if (!base.initialized)
						{
							return;
						}
						goto case 6;
					case 1:
					{
						int num2;
						if (!IsInteractable())
						{
							num = -1643153870;
							num2 = num;
						}
						else
						{
							num = -1643153867;
							num2 = num;
						}
						continue;
					}
					}
					break;
				}
			}
		}

		internal override void OnPointerUp(PointerEventData eventData)
		{
			//Discarded unreachable code: IL_0045, IL_0061, IL_00bc
			base.OnPointerUp(eventData);
			while (true)
			{
				int num = 986962798;
				while (true)
				{
					switch (num ^ 0x3AD3DB68)
					{
					default:
						return;
					case 0:
						return;
					case 2:
						return;
					case 5:
						break;
					case 6:
						if (!base.initialized)
						{
							return;
						}
						goto case 1;
					case 4:
						if (!TouchInteractable.gtAdYPaYTSdxlqopRartpbSncsib(eventData.pointerId, base.allowedMouseButtons, EventTriggerType.PointerUp))
						{
							return;
						}
						goto case 3;
					case 1:
					{
						int num3;
						if (agGGiexlYIVgemjaHHzzcCmJTcc())
						{
							num = 986962799;
							num3 = num;
						}
						else
						{
							num = 986962792;
							num3 = num;
						}
						continue;
					}
					case 3:
						if (_onPointerUp != null)
						{
							_onPointerUp.Invoke(eventData);
							num = 986962794;
							continue;
						}
						return;
					case 7:
					{
						int num2;
						if (IsInteractable())
						{
							num = 986962796;
							num2 = num;
						}
						else
						{
							num = 986962792;
							num2 = num;
						}
						continue;
					}
					}
					break;
				}
			}
		}

		internal override void OnPointerEnter(PointerEventData eventData)
		{
			//Discarded unreachable code: IL_0087, IL_008f, IL_00d4
			base.OnPointerEnter(eventData);
			if (!base.initialized)
			{
				goto IL_000f;
			}
			goto IL_006d;
			IL_006d:
			int num;
			int num2;
			if (!agGGiexlYIVgemjaHHzzcCmJTcc())
			{
				num = 1114276971;
				num2 = num;
			}
			else
			{
				num = 1114276974;
				num2 = num;
			}
			goto IL_0014;
			IL_0014:
			while (true)
			{
				switch (num ^ 0x426A846B)
				{
				default:
					return;
				case 0:
					return;
				case 1:
					return;
				case 3:
					return;
				case 7:
					return;
				case 2:
					break;
				case 6:
					goto IL_0048;
				case 4:
					goto IL_006d;
				case 5:
					goto IL_0099;
				case 8:
					if (_onPointerEnter != null)
					{
						_onPointerEnter.Invoke(eventData);
						num = 1114276970;
						continue;
					}
					return;
				}
				break;
				IL_0099:
				int num3;
				if (!IsInteractable())
				{
					num = 1114276971;
					num3 = num;
				}
				else
				{
					num = 1114276973;
					num3 = num;
				}
				continue;
				IL_0048:
				int num4;
				if (TouchInteractable.gtAdYPaYTSdxlqopRartpbSncsib(eventData.pointerId, base.allowedMouseButtons, EventTriggerType.PointerEnter))
				{
					num = 1114276963;
					num4 = num;
				}
				else
				{
					num = 1114276968;
					num4 = num;
				}
			}
			goto IL_000f;
			IL_000f:
			num = 1114276972;
			goto IL_0014;
		}

		internal override void OnPointerExit(PointerEventData eventData)
		{
			//Discarded unreachable code: IL_0060, IL_0095
			base.OnPointerExit(eventData);
			if (!base.initialized)
			{
				return;
			}
			while (agGGiexlYIVgemjaHHzzcCmJTcc())
			{
				int num;
				int num2;
				if (!IsInteractable())
				{
					num = 1085112401;
					num2 = num;
				}
				else
				{
					num = 1085112405;
					num2 = num;
				}
				while (true)
				{
					switch (num ^ 0x40AD8053)
					{
					default:
						return;
					case 1:
						return;
					case 2:
						return;
					case 4:
						num = 1085112406;
						continue;
					case 5:
						break;
					case 0:
					{
						int num3;
						if (_onPointerExit == null)
						{
							num = 1085112402;
							num3 = num;
						}
						else
						{
							num = 1085112400;
							num3 = num;
						}
						continue;
					}
					case 6:
						if (!TouchInteractable.gtAdYPaYTSdxlqopRartpbSncsib(eventData.pointerId, base.allowedMouseButtons, EventTriggerType.PointerExit))
						{
							return;
						}
						goto case 0;
					case 3:
						_onPointerExit.Invoke(eventData);
						num = 1085112402;
						continue;
					}
					break;
				}
			}
		}

		internal override void OnBeginDrag(PointerEventData eventData)
		{
			//Discarded unreachable code: IL_0045, IL_0079, IL_00ba
			base.OnBeginDrag(eventData);
			while (true)
			{
				int num = 1256859022;
				while (true)
				{
					switch (num ^ 0x4AEA2589)
					{
					default:
						return;
					case 1:
						return;
					case 2:
						return;
					case 4:
						break;
					case 7:
						if (!base.initialized)
						{
							return;
						}
						goto case 6;
					case 3:
					{
						int num2;
						if (_onBeginDrag == null)
						{
							num = 1256859016;
							num2 = num;
						}
						else
						{
							num = 1256859020;
							num2 = num;
						}
						continue;
					}
					case 5:
						_onBeginDrag.Invoke(eventData);
						num = 1256859016;
						continue;
					case 6:
					{
						if (!agGGiexlYIVgemjaHHzzcCmJTcc())
						{
							return;
						}
						int num3;
						if (!IsInteractable())
						{
							num = 1256859019;
							num3 = num;
						}
						else
						{
							num = 1256859017;
							num3 = num;
						}
						continue;
					}
					case 0:
						if (!TouchInteractable.gtAdYPaYTSdxlqopRartpbSncsib(eventData.pointerId, base.allowedMouseButtons, EventTriggerType.BeginDrag))
						{
							return;
						}
						goto case 3;
					}
					break;
				}
			}
		}

		internal override void OnDrag(PointerEventData eventData)
		{
			//Discarded unreachable code: IL_003e, IL_005a, IL_0062
			base.OnDrag(eventData);
			while (true)
			{
				int num = -2120180882;
				while (true)
				{
					switch (num ^ -2120180883)
					{
					default:
						return;
					case 1:
						return;
					case 4:
						return;
					case 0:
						break;
					case 3:
						if (!base.initialized)
						{
							return;
						}
						goto case 2;
					case 6:
						if (!TouchInteractable.gtAdYPaYTSdxlqopRartpbSncsib(eventData.pointerId, base.allowedMouseButtons, EventTriggerType.Drag))
						{
							return;
						}
						goto case 5;
					case 2:
					{
						if (!agGGiexlYIVgemjaHHzzcCmJTcc())
						{
							return;
						}
						int num2;
						if (IsInteractable())
						{
							num = -2120180885;
							num2 = num;
						}
						else
						{
							num = -2120180884;
							num2 = num;
						}
						continue;
					}
					case 5:
						if (_onDrag != null)
						{
							_onDrag.Invoke(eventData);
							num = -2120180887;
							continue;
						}
						return;
					}
					break;
				}
			}
		}

		internal override void OnEndDrag(PointerEventData eventData)
		{
			//Discarded unreachable code: IL_0071, IL_0094
			base.OnEndDrag(eventData);
			if (!base.initialized)
			{
				return;
			}
			while (agGGiexlYIVgemjaHHzzcCmJTcc())
			{
				int num;
				int num2;
				if (!IsInteractable())
				{
					num = 173464335;
					num2 = num;
				}
				else
				{
					num = 173464332;
					num2 = num;
				}
				while (true)
				{
					switch (num ^ 0xA56DB0C)
					{
					default:
						return;
					case 3:
						return;
					case 5:
						return;
					case 4:
						num = 173464333;
						continue;
					case 1:
						break;
					case 0:
						if (!TouchInteractable.gtAdYPaYTSdxlqopRartpbSncsib(eventData.pointerId, base.allowedMouseButtons, EventTriggerType.EndDrag))
						{
							return;
						}
						goto case 2;
					case 2:
						if (_onEndDrag != null)
						{
							_onEndDrag.Invoke(eventData);
							num = 173464329;
							continue;
						}
						return;
					}
					break;
				}
			}
		}
	}
}
