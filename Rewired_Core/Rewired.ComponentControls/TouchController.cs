using System;
using UnityEngine;

namespace Rewired.ComponentControls
{
	[Serializable]
	[RequireComponent(typeof(RectTransform))]
	[DisallowMultipleComponent]
	public sealed class TouchController : CustomController
	{
		[SerializeField]
		[CustomObfuscation(rename = false)]
		[Tooltip("If true, disables mouse input when the Touch Controller script is enabled or GameObject is activated and re-enables mouse input when the script is disabled or GameObject is deactivated. This is useful for disabling Mouse Look controls when using touch controls in an FPS for example.")]
		private bool _disableMouseInputWhenEnabled = true;

		[SerializeField]
		[CustomObfuscation(rename = false)]
		[Tooltip("If true, a Custom Controller will be populated with the data from this controller.")]
		private bool _useCustomController = true;

		public bool disableMouseInputWhenEnabled
		{
			get
			{
				return _disableMouseInputWhenEnabled;
			}
			set
			{
				//Discarded unreachable code: IL_0028
				if (_disableMouseInputWhenEnabled == value)
				{
					while (true)
					{
						switch (-1682990630 ^ -1682990629)
						{
						case 1:
							return;
						case 2:
							continue;
						}
						break;
					}
				}
				_disableMouseInputWhenEnabled = value;
				OnSetProperty();
			}
		}

		public bool useCustomController
		{
			get
			{
				return _useCustomController;
			}
			set
			{
				if (_useCustomController == value)
				{
					return;
				}
				while (true)
				{
					_useCustomController = value;
					OnSetProperty();
					if (value)
					{
						CheckIsRewiredReady();
						int num = 17561698;
						while (true)
						{
							switch (num ^ 0x10BF863)
							{
							default:
								return;
							case 1:
								return;
							case 0:
								goto IL_000a;
							case 2:
								break;
							}
							break;
							IL_000a:
							num = 17561697;
						}
						continue;
					}
					break;
				}
			}
		}

		[CustomObfuscation(rename = false)]
		private TouchController()
		{
		}

		[CustomObfuscation(rename = false)]
		internal override void OnDisable()
		{
			base.OnDisable();
			if (!base.initialized)
			{
				return;
			}
			while (ReInput.isReady)
			{
				SetMouseState(state: true);
				int num = -1018875725;
				while (true)
				{
					switch (num ^ -1018875725)
					{
					default:
						return;
					case 0:
						return;
					case 2:
						goto IL_000f;
					case 1:
						break;
					}
					break;
					IL_000f:
					num = -1018875726;
				}
			}
		}

		internal override bool OnInitialize()
		{
			if (!base.OnInitialize())
			{
				goto IL_0008;
			}
			int num;
			if (ReInput.isReady)
			{
				SetMouseState(state: false);
				num = -289151928;
				goto IL_000d;
			}
			goto IL_003d;
			IL_003d:
			return true;
			IL_0008:
			num = -289151925;
			goto IL_000d;
			IL_000d:
			switch (num ^ -289151926)
			{
			case 0:
				break;
			case 1:
				return false;
			default:
				goto IL_003d;
			}
			goto IL_0008;
		}

		[CustomObfuscation(rename = false)]
		internal override bool GetUseCustomController()
		{
			return _useCustomController;
		}

		[CustomObfuscation(rename = false)]
		internal override void SetUseCustomController(bool value)
		{
			_useCustomController = value;
		}

		private void SetMouseState(bool state)
		{
			//Discarded unreachable code: IL_003f
			if (!_disableMouseInputWhenEnabled)
			{
				return;
			}
			while (!state)
			{
				while (true)
				{
					IL_0046:
					ReInput.controllers.Mouse.enabled = false;
					int num = 2072938456;
					while (true)
					{
						switch (num ^ 0x7B8E87D8)
						{
						default:
							return;
						case 0:
							return;
						case 2:
							num = 2072938457;
							continue;
						case 1:
							break;
						case 3:
							goto IL_0046;
						}
						break;
					}
					break;
				}
			}
			ReInput.controllers.Mouse.enabled = true;
		}

		private void OnSetProperty()
		{
		}

		private bool CheckIsRewiredReady()
		{
			if (ReInput.isReady)
			{
				return true;
			}
			Logger.LogError("Rewired is not initialized. You must have an enabled Rewired Input Manager in the scene if using a Touch Controller. Custom Controller support will be disabled on this Touch Controller.");
			SetUseCustomController(value: false);
			return false;
		}
	}
}
