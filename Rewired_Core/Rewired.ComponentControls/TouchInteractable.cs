using Rewired.UI;
using Rewired.Utils;
using Rewired.Utils.Attributes;
using Rewired.Utils.Classes.Utility;
using System;
using System.Collections.Generic;
using System.Runtime.CompilerServices;
using UnityEngine;
using UnityEngine.Events;
using UnityEngine.EventSystems;
using UnityEngine.UI;

namespace Rewired.ComponentControls
{
	[Serializable]
	[DisallowMultipleComponent]
	[ExecuteInEditMode]
	public abstract class TouchInteractable : TouchControl, IPointerDownHandler, IPointerUpHandler, IPointerEnterHandler, IPointerExitHandler, IBeginDragHandler, IDragHandler, IEndDragHandler, IEventSystemHandler
	{
		public enum InteractionState
		{
			Normal,
			Highlighted,
			Pressed,
			Disabled
		}

		[Flags]
		public enum TransitionTypeFlags
		{
			None = 0x0,
			ColorTint = 0x1,
			SpriteSwap = 0x2,
			Animation = 0x4
		}

		[Flags]
		public enum MouseButtonFlags
		{
			None = 0x0,
			LeftButton = 0x1,
			RightButton = 0x2,
			MiddleButton = 0x4,
			AnyButton = -1
		}

		[Serializable]
		public class InteractionStateTransitionEventHandler : UnityEvent<InteractionStateTransitionArgs>
		{
		}

		[Serializable]
		public class VisibilityChangedEventHandler : UnityEvent<bool>
		{
		}

		public class InteractionStateTransitionArgs
		{
			private TouchInteractable EZttLHuQYEgvIEKrHEeQXfjuQOT;

			private InteractionState eJrGlLYDGkirsdDIseycUDNYZlX;

			private float BEpzZiQlkzODkKRdrxPKTAjZsFc;

			public TouchInteractable sender => EZttLHuQYEgvIEKrHEeQXfjuQOT;

			public InteractionState state => eJrGlLYDGkirsdDIseycUDNYZlX;

			public float duration => BEpzZiQlkzODkKRdrxPKTAjZsFc;

			internal InteractionStateTransitionArgs()
			{
			}

			internal void cTeEIGCEGQRXvgfcnYwmgeUMNbf(TouchInteractable P_0, InteractionState P_1, float P_2)
			{
				EZttLHuQYEgvIEKrHEeQXfjuQOT = P_0;
				eJrGlLYDGkirsdDIseycUDNYZlX = P_1;
				BEpzZiQlkzODkKRdrxPKTAjZsFc = P_2;
			}
		}

		public interface IInteractionStateTransitionHandler
		{
			void OnInteractionStateTransition(InteractionStateTransitionArgs data);
		}

		public const int POINTER_ID_NULL = int.MinValue;

		public const int POINTER_ID_MOUSE_LEFT_BUTTON = -1;

		public const int POINTER_ID_MOUSE_RIGHT_BUTTON = -2;

		public const int POINTER_ID_MOUSE_MIDDLE_BUTTON = -3;

		internal const int MAX_MOUSE_BUTTONS = 3;

		[CustomObfuscation(rename = false)]
		[SerializeField]
		[Tooltip("Toggles whether the control can be interacted with by the user.")]
		private bool _interactable = true;

		[CustomObfuscation(rename = false)]
		[SerializeField]
		[Tooltip("Toggles visibility. An invisible control can still be interacted with. This property only has any effect when used with an Image Component.")]
		private bool _visible = true;

		[Tooltip("Sets visibility to False when the control is idle. When the control is no longer idle, visibility will be set to True again.")]
		[SerializeField]
		[CustomObfuscation(rename = false)]
		private bool _hideWhenIdle;

		[SerializeField]
		[Bitmask(typeof(MouseButtonFlags))]
		[CustomObfuscation(rename = false)]
		[Tooltip("The mouse buttons that are allowed to interact with this control.")]
		private MouseButtonFlags _allowedMouseButtons = MouseButtonFlags.LeftButton;

		[CustomObfuscation(rename = false)]
		[Bitmask(typeof(TransitionTypeFlags))]
		[Tooltip("The transition type(s) to be used when transitioning to various states. Multiple transition types can be used simultaneously.")]
		[SerializeField]
		private TransitionTypeFlags _transitionType;

		[SerializeField]
		[Tooltip("Settings using for Color Tint transitions.")]
		[CustomObfuscation(rename = false)]
		private ColorBlock _transitionColorTint = new ColorBlock
		{
			colorMultiplier = 1f,
			disabledColor = new Color(25f / 32f, 25f / 32f, 25f / 32f, 0.5f),
			highlightedColor = Color.white,
			normalColor = Color.white,
			pressedColor = Color.white,
			fadeDuration = 0.1f
		};

		[Tooltip("Settings using for Sprite State transitions.")]
		[CustomObfuscation(rename = false)]
		[SerializeField]
		private SpriteState _transitionSpriteState;

		[Tooltip("Settings using for Animation Trigger transitions.")]
		[CustomObfuscation(rename = false)]
		[SerializeField]
		private AnimationTriggers _transitionAnimationTriggers = new AnimationTriggers();

		[CustomObfuscation(rename = false)]
		[Tooltip("The target Graphic component for interaction state transitions. This should normally be set to an Image component on this GameObject.")]
		[SerializeField]
		private Graphic _targetGraphic;

		[SerializeField]
		[Tooltip("Event sent when the Interaction State changes.")]
		[CustomObfuscation(rename = false)]
		private InteractionStateTransitionEventHandler _onInteractionStateTransition = new InteractionStateTransitionEventHandler();

		[Tooltip("Event sent when visibility changes.")]
		[SerializeField]
		[CustomObfuscation(rename = false)]
		private VisibilityChangedEventHandler _onVisibilityChanged = new VisibilityChangedEventHandler();

		[Tooltip("Event sent when interaction state changes to Normal.")]
		[CustomObfuscation(rename = false)]
		[SerializeField]
		private UnityEvent _onInteractionStateChangedToNormal = new UnityEvent();

		[SerializeField]
		[CustomObfuscation(rename = false)]
		[Tooltip("Event sent when interaction state changes to Highlighted.")]
		private UnityEvent _onInteractionStateChangedToHighlighted = new UnityEvent();

		[CustomObfuscation(rename = false)]
		[Tooltip("Event sent when interaction state changes to Pressed.")]
		[SerializeField]
		private UnityEvent _onInteractionStateChangedToPressed = new UnityEvent();

		[Tooltip("Event sent when interaction state changes to Disabled.")]
		[CustomObfuscation(rename = false)]
		[SerializeField]
		private UnityEvent _onInteractionStateChangedToDisabled = new UnityEvent();

		private readonly List<CanvasGroup> _canvasGroupCache = new List<CanvasGroup>();

		private bool _groupsAllowInteraction = true;

		private InteractionState _interactionState;

		[NonSerialized]
		private bool BSbeTqiFrdRDtCmjwklbhCLtNiqg;

		[NonSerialized]
		private bool yrfVZiGUTfOClpVIfIvNRqImIGGe;

		private bool _varWatch_visible;

		private bool _varWatch_interactable;

		private bool _allowSendingEvents = true;

		private static InteractionStateTransitionArgs _transitionArgs = new InteractionStateTransitionArgs();

		private PawhcdAEqKalvcbRBbHZlIgMMIHA.HierarchyEventHelper<IVisibilityChangedHandler, bool> __hierarchyVisibilityChangedHandlers;

		private PawhcdAEqKalvcbRBbHZlIgMMIHA.HierarchyEventHelper<IInteractionStateTransitionHandler, InteractionStateTransitionArgs> __hierarchyInteractionStateTransitionHandlers;

		private static PawhcdAEqKalvcbRBbHZlIgMMIHA.EventFunction<IInteractionStateTransitionHandler, InteractionStateTransitionArgs> __interactionStateTransitionHandlerDelegate;

		private PawhcdAEqKalvcbRBbHZlIgMMIHA.HierarchyEventHelper<IVisibilityChangedHandler, bool> hierarchyVisibilityChangedHandlers
		{
			get
			{
				if (__hierarchyVisibilityChangedHandlers == null)
				{
					while (true)
					{
						int num = 492826329;
						while (true)
						{
							switch (num ^ 0x1D5FEED8)
							{
							case 0:
								break;
							case 1:
								__hierarchyVisibilityChangedHandlers = new PawhcdAEqKalvcbRBbHZlIgMMIHA.HierarchyEventHelper<IVisibilityChangedHandler, bool>(loRZPhaBSNCDvZUbaVxfVQsnYHF.JliIvlAcilIibMmRTWobPEGWNIb);
								num = 492826331;
								continue;
							case 3:
								__hierarchyVisibilityChangedHandlers.GetHandlers(base.transform);
								num = 492826330;
								continue;
							default:
								goto IL_0059;
							}
							break;
						}
					}
				}
				goto IL_0059;
				IL_0059:
				return __hierarchyVisibilityChangedHandlers;
			}
		}

		private PawhcdAEqKalvcbRBbHZlIgMMIHA.HierarchyEventHelper<IInteractionStateTransitionHandler, InteractionStateTransitionArgs> hierarchyInteractionStateTransitionHandlers
		{
			get
			{
				if (__hierarchyInteractionStateTransitionHandlers == null)
				{
					while (true)
					{
						int num = 539773498;
						while (true)
						{
							switch (num ^ 0x202C4A39)
							{
							case 0:
								break;
							case 3:
								__hierarchyInteractionStateTransitionHandlers = new PawhcdAEqKalvcbRBbHZlIgMMIHA.HierarchyEventHelper<IInteractionStateTransitionHandler, InteractionStateTransitionArgs>(interactionStateTransitionHandlerDelegate);
								num = 539773496;
								continue;
							case 1:
								__hierarchyInteractionStateTransitionHandlers.GetHandlers(base.transform);
								num = 539773499;
								continue;
							default:
								goto IL_0059;
							}
							break;
						}
					}
				}
				goto IL_0059;
				IL_0059:
				return __hierarchyInteractionStateTransitionHandlers;
			}
		}

		public bool interactable
		{
			get
			{
				return _interactable;
			}
			set
			{
				if (_interactable == value)
				{
					return;
				}
				while (true)
				{
					_interactable = value;
					int num = -1242966448;
					while (true)
					{
						switch (num ^ -1242966448)
						{
						case 2:
							goto IL_000a;
						case 1:
							break;
						default:
							OnSetProperty();
							return;
						}
						break;
						IL_000a:
						num = -1242966447;
					}
				}
			}
		}

		public bool visible
		{
			get
			{
				return _visible;
			}
			set
			{
				if (visible == value)
				{
					return;
				}
				while (true)
				{
					DslcJwXTzoxJrqDkiUIuuiJQYNZ(value, false);
					int num = -1440697742;
					while (true)
					{
						switch (num ^ -1440697744)
						{
						case 0:
							goto IL_000a;
						case 1:
							break;
						default:
							OnSetProperty();
							return;
						}
						break;
						IL_000a:
						num = -1440697743;
					}
				}
			}
		}

		public bool hideWhenIdle
		{
			get
			{
				return _hideWhenIdle;
			}
			set
			{
				//Discarded unreachable code: IL_0028
				if (_hideWhenIdle == value)
				{
					while (true)
					{
						switch (-1280700660 ^ -1280700659)
						{
						case 1:
							return;
						case 0:
							continue;
						}
						break;
					}
				}
				_hideWhenIdle = value;
				OnSetProperty();
			}
		}

		public MouseButtonFlags allowedMouseButtons
		{
			get
			{
				return _allowedMouseButtons;
			}
			set
			{
				if (_allowedMouseButtons == value)
				{
					return;
				}
				while (true)
				{
					_allowedMouseButtons = value;
					int num = -697602359;
					while (true)
					{
						switch (num ^ -697602358)
						{
						default:
							return;
						case 2:
							return;
						case 0:
							num = -697602357;
							continue;
						case 1:
							break;
						case 3:
							OnSetProperty();
							num = -697602360;
							continue;
						}
						break;
					}
				}
			}
		}

		public TransitionTypeFlags transitionType
		{
			get
			{
				return _transitionType;
			}
			set
			{
				if (_transitionType == value)
				{
					return;
				}
				while (true)
				{
					_transitionType = value;
					int num = -891526351;
					while (true)
					{
						switch (num ^ -891526352)
						{
						case 0:
							goto IL_000a;
						case 2:
							break;
						default:
							OnSetProperty();
							return;
						}
						break;
						IL_000a:
						num = -891526350;
					}
				}
			}
		}

		public ColorBlock transitionColorTint
		{
			get
			{
				return _transitionColorTint;
			}
			set
			{
				_transitionColorTint = value;
				OnSetProperty();
			}
		}

		public SpriteState transitionSpriteState
		{
			get
			{
				return _transitionSpriteState;
			}
			set
			{
				if (!_transitionSpriteState.Equals(value))
				{
					_transitionSpriteState = value;
					OnSetProperty();
				}
			}
		}

		public AnimationTriggers transitionAnimationTriggers
		{
			get
			{
				return _transitionAnimationTriggers;
			}
			set
			{
				if (_transitionAnimationTriggers == value)
				{
					return;
				}
				while (true)
				{
					_transitionAnimationTriggers = value;
					int num = 909910501;
					while (true)
					{
						switch (num ^ 0x363C21E6)
						{
						default:
							return;
						case 2:
							return;
						case 0:
							num = 909910503;
							continue;
						case 1:
							break;
						case 3:
							OnSetProperty();
							num = 909910500;
							continue;
						}
						break;
					}
				}
			}
		}

		public Graphic targetGraphic
		{
			get
			{
				return _targetGraphic;
			}
			set
			{
				if (_targetGraphic == value)
				{
					return;
				}
				while (true)
				{
					_targetGraphic = value;
					OnSetProperty();
					int num = -738472079;
					while (true)
					{
						switch (num ^ -738472079)
						{
						default:
							return;
						case 0:
							return;
						case 2:
							goto IL_000f;
						case 1:
							break;
						}
						break;
						IL_000f:
						num = -738472080;
					}
				}
			}
		}

		public Image image
		{
			get
			{
				return _targetGraphic as Image;
			}
			set
			{
				if (!(_targetGraphic == value))
				{
					_targetGraphic = value;
					OnSetProperty();
				}
			}
		}

		public Animator animator => base.gameObject.GetComponent<Animator>();

		public InteractionState interactionState => _interactionState;

		internal static PawhcdAEqKalvcbRBbHZlIgMMIHA.EventFunction<IInteractionStateTransitionHandler, InteractionStateTransitionArgs> interactionStateTransitionHandlerDelegate
		{
			get
			{
				if (__interactionStateTransitionHandlerDelegate == null)
				{
					if (CS_0024_003C_003E9__CachedAnonymousMethodDelegate4 == null)
					{
						CS_0024_003C_003E9__CachedAnonymousMethodDelegate4 = delegate(IInteractionStateTransitionHandler P_0, InteractionStateTransitionArgs P_1)
						{
							P_0.OnInteractionStateTransition(P_1);
						};
						goto IL_001f;
					}
					goto IL_003d;
				}
				goto IL_004e;
				IL_001f:
				int num = 1633124153;
				goto IL_0024;
				IL_004e:
				return __interactionStateTransitionHandlerDelegate;
				IL_003d:
				__interactionStateTransitionHandlerDelegate = CS_0024_003C_003E9__CachedAnonymousMethodDelegate4;
				num = 1633124154;
				goto IL_0024;
				IL_0024:
				switch (num ^ 0x61577F3B)
				{
				case 0:
					break;
				case 2:
					goto IL_003d;
				default:
					goto IL_004e;
				}
				goto IL_001f;
			}
		}

		public event UnityAction<InteractionStateTransitionArgs> InteractionStateSetEvent
		{
			add
			{
				_onInteractionStateTransition.AddListener(value);
			}
			remove
			{
				_onInteractionStateTransition.RemoveListener(value);
			}
		}

		public event UnityAction<bool> VisibilityChangedEvent
		{
			add
			{
				_onVisibilityChanged.AddListener(value);
			}
			remove
			{
				_onVisibilityChanged.RemoveListener(value);
			}
		}

		public event UnityAction InteractionStateChangedToNormal
		{
			add
			{
				_onInteractionStateChangedToNormal.AddListener(value);
			}
			remove
			{
				_onInteractionStateChangedToNormal.RemoveListener(value);
			}
		}

		public event UnityAction InteractionStateChangedToHighlighted
		{
			add
			{
				_onInteractionStateChangedToHighlighted.AddListener(value);
			}
			remove
			{
				_onInteractionStateChangedToHighlighted.RemoveListener(value);
			}
		}

		public event UnityAction InteractionStateChangedToPressed
		{
			add
			{
				_onInteractionStateChangedToPressed.AddListener(value);
			}
			remove
			{
				_onInteractionStateChangedToPressed.RemoveListener(value);
			}
		}

		public event UnityAction InteractionStateChangedToDisabled
		{
			add
			{
				_onInteractionStateChangedToDisabled.AddListener(value);
			}
			remove
			{
				_onInteractionStateChangedToDisabled.RemoveListener(value);
			}
		}

		[CustomObfuscation(rename = false)]
		internal TouchInteractable()
		{
		}

		[CustomObfuscation(rename = false)]
		internal override void Awake()
		{
			base.Awake();
			if (!Application.isPlaying)
			{
				return;
			}
			while (true)
			{
				int num;
				if (_targetGraphic == null)
				{
					_targetGraphic = base.gameObject.GetComponent<Graphic>();
					num = -1372157510;
					goto IL_0013;
				}
				goto IL_0056;
				IL_0013:
				while (true)
				{
					switch (num ^ -1372157511)
					{
					default:
						return;
					case 1:
						return;
					case 0:
						num = -1372157509;
						continue;
					case 2:
						break;
					case 3:
						goto IL_0056;
					}
					break;
				}
				continue;
				IL_0056:
				tJvGxLTYBcmKEdCZNJVdFQoLtqu();
				num = -1372157512;
				goto IL_0013;
			}
		}

		[CustomObfuscation(rename = false)]
		internal override void OnCanvasGroupChanged()
		{
			base.OnCanvasGroupChanged();
			bool flag = true;
			Transform transform = base.transform;
			bool flag2 = default(bool);
			int num4 = default(int);
			while (true)
			{
				int num;
				int num2;
				if (transform != null)
				{
					num = -1784306322;
					num2 = num;
				}
				else
				{
					num = -1784306326;
					num2 = num;
				}
				while (true)
				{
					switch (num ^ -1784306324)
					{
					default:
						return;
					case 1:
						return;
					case 9:
						num = -1784306322;
						continue;
					case 4:
					{
						int num6;
						if (flag2)
						{
							num = -1784306326;
							num6 = num;
						}
						else
						{
							num = -1784306335;
							num6 = num;
						}
						continue;
					}
					case 12:
						num4++;
						num = -1784306324;
						continue;
					case 0:
					{
						int num5;
						if (num4 < _canvasGroupCache.Count)
						{
							num = -1784306332;
							num5 = num;
						}
						else
						{
							num = -1784306328;
							num5 = num;
						}
						continue;
					}
					case 10:
						flag2 = true;
						num = -1784306327;
						continue;
					case 3:
						_groupsAllowInteraction = flag;
						num = -1784306329;
						continue;
					case 13:
						transform = transform.parent;
						num = -1784306325;
						continue;
					case 11:
						GBExVtHFPRLrKdqZIHqEWcjtFsS();
						num = -1784306323;
						continue;
					case 5:
						if (_canvasGroupCache[num4].ignoreParentGroups)
						{
							flag2 = true;
							num = -1784306336;
							continue;
						}
						goto case 12;
					case 7:
						break;
					case 2:
						transform.GetComponents(_canvasGroupCache);
						flag2 = false;
						num4 = 0;
						num = -1784306324;
						continue;
					case 8:
						if (!_canvasGroupCache[num4].interactable)
						{
							flag = false;
							num = -1784306330;
							continue;
						}
						goto case 5;
					case 6:
					{
						int num3;
						if (flag != _groupsAllowInteraction)
						{
							num = -1784306321;
							num3 = num;
						}
						else
						{
							num = -1784306323;
							num3 = num;
						}
						continue;
					}
					}
					break;
				}
			}
		}

		[CustomObfuscation(rename = false)]
		internal override void OnDidApplyAnimationProperties()
		{
			base.OnDidApplyAnimationProperties();
			GBExVtHFPRLrKdqZIHqEWcjtFsS();
		}

		[CustomObfuscation(rename = false)]
		internal override void OnEnable()
		{
			base.OnEnable();
			if (!Application.isPlaying)
			{
				goto IL_000d;
			}
			goto IL_0040;
			IL_0040:
			LPkunBXNoVtukVfxHWtaKQBtvIt(InteractionState.Normal);
			int num = 560806833;
			goto IL_0012;
			IL_0012:
			while (true)
			{
				switch (num ^ 0x216D3BB2)
				{
				default:
					return;
				case 0:
					return;
				case 2:
					break;
				case 1:
					tJvGxLTYBcmKEdCZNJVdFQoLtqu();
					num = 560806838;
					continue;
				case 4:
					goto IL_0040;
				case 3:
					qKRTkfOGLUHHYELogNnEEhiEJte(true);
					num = 560806834;
					continue;
				}
				break;
			}
			goto IL_000d;
			IL_000d:
			num = 560806835;
			goto IL_0012;
		}

		[CustomObfuscation(rename = false)]
		internal override void OnDisable()
		{
			yrErPSHhGnOuznuHirRTpPkmJMI();
			base.OnDisable();
		}

		[CustomObfuscation(rename = false)]
		internal override void OnValidate()
		{
			base.OnValidate();
			_transitionColorTint.fadeDuration = Mathf.Max(_transitionColorTint.fadeDuration, 0f);
			if (agGGiexlYIVgemjaHHzzcCmJTcc())
			{
				if (!_interactable && EventSystem.current != null)
				{
					goto IL_0046;
				}
				goto IL_0099;
			}
			goto IL_00dc;
			IL_00dc:
			pqeSwAyWVnauZHlEShRCcdDuCYY();
			GBExVtHFPRLrKdqZIHqEWcjtFsS();
			return;
			IL_0046:
			int num = 1751609845;
			goto IL_004b;
			IL_0099:
			YYvfGdnVUAOPddcwvcrImIcuboG(null);
			LJUhxiZrQAfWgFGuXVojBKYVgGz(Color.white, true);
			num = 1751609842;
			goto IL_004b;
			IL_004b:
			while (true)
			{
				switch (num ^ 0x686771F0)
				{
				case 3:
					break;
				case 5:
					if (EventSystem.current.currentSelectedGameObject == base.gameObject)
					{
						EventSystem.current.SetSelectedGameObject(null);
						num = 1751609840;
						continue;
					}
					goto IL_0099;
				case 0:
					goto IL_0099;
				case 1:
					qKRTkfOGLUHHYELogNnEEhiEJte(true);
					num = 1751609844;
					continue;
				case 2:
					DXZIsWKrjVFvmONoAmLYyqbnSYC(_transitionAnimationTriggers.normalTrigger);
					num = 1751609841;
					continue;
				default:
					goto IL_00dc;
				}
				break;
			}
			goto IL_0046;
		}

		[CustomObfuscation(rename = false)]
		internal override void Reset()
		{
			_targetGraphic = base.gameObject.GetComponent<Graphic>();
			while (true)
			{
				int num = 1209390908;
				while (true)
				{
					switch (num ^ 0x4815D73D)
					{
					default:
						return;
					case 3:
						return;
					case 2:
						break;
					case 1:
						_allowedMouseButtons = MouseButtonFlags.LeftButton;
						num = 1209390909;
						continue;
					case 0:
						base.Reset();
						num = 1209390910;
						continue;
					}
					break;
				}
			}
		}

		internal override void OnSetProperty()
		{
			base.OnSetProperty();
			GBExVtHFPRLrKdqZIHqEWcjtFsS();
		}

		internal override void FindEventHandlers()
		{
			base.FindEventHandlers();
			pqeSwAyWVnauZHlEShRCcdDuCYY();
		}

		private void yrErPSHhGnOuznuHirRTpPkmJMI()
		{
			string normalTrigger = _transitionAnimationTriggers.normalTrigger;
			while (true)
			{
				int num = 173082807;
				while (true)
				{
					switch (num ^ 0xA5108B5)
					{
					default:
						return;
					case 5:
						return;
					case 6:
						break;
					case 4:
						DXZIsWKrjVFvmONoAmLYyqbnSYC(normalTrigger);
						num = 173082800;
						continue;
					case 1:
						yrfVZiGUTfOClpVIfIvNRqImIGGe = false;
						if ((_transitionType & TransitionTypeFlags.ColorTint) != 0)
						{
							LJUhxiZrQAfWgFGuXVojBKYVgGz(Color.white, true);
							num = 173082805;
							continue;
						}
						goto case 0;
					case 2:
						BSbeTqiFrdRDtCmjwklbhCLtNiqg = false;
						num = 173082804;
						continue;
					case 3:
					{
						int num2;
						if ((_transitionType & TransitionTypeFlags.Animation) != 0)
						{
							num = 173082801;
							num2 = num;
						}
						else
						{
							num = 173082800;
							num2 = num;
						}
						continue;
					}
					case 0:
						if ((_transitionType & TransitionTypeFlags.SpriteSwap) != 0)
						{
							YYvfGdnVUAOPddcwvcrImIcuboG(null);
							num = 173082806;
							continue;
						}
						goto case 3;
					}
					break;
				}
			}
		}

		private void ZfPVPoqRQiwHjlckPfDuMoPdZcW(InteractionState P_0, bool P_1)
		{
			Color color;
			Sprite sprite = default(Sprite);
			string text = default(string);
			UnityEvent unityEvent = default(UnityEvent);
			int num;
			bool flag = default(bool);
			switch (P_0)
			{
			case InteractionState.Normal:
				goto IL_021f;
			default:
				goto IL_025c;
			case InteractionState.Highlighted:
				goto IL_026c;
			case InteractionState.Pressed:
				goto IL_02aa;
			case InteractionState.Disabled:
				goto IL_0331;
				IL_021f:
				color = _transitionColorTint.normalColor;
				sprite = null;
				text = _transitionAnimationTriggers.normalTrigger;
				unityEvent = _onInteractionStateChangedToNormal;
				num = 1987692372;
				goto IL_0024;
				IL_0024:
				while (true)
				{
					switch (num ^ 0x7679C753)
					{
					default:
						return;
					case 5:
						return;
					case 3:
						num = 1987692379;
						continue;
					case 10:
						LJUhxiZrQAfWgFGuXVojBKYVgGz(color, P_1);
						num = 1987692363;
						continue;
					case 16:
						LJUhxiZrQAfWgFGuXVojBKYVgGz(color * _transitionColorTint.colorMultiplier, P_1);
						num = 1987692363;
						continue;
					case 1:
						unityEvent = _onInteractionStateChangedToPressed;
						num = 1987692378;
						continue;
					case 12:
						sprite = null;
						num = 1987692371;
						continue;
					case 4:
						if (_allowSendingEvents)
						{
							_transitionArgs.cTeEIGCEGQRXvgfcnYwmgeUMNbf(this, P_0, P_1 ? 0f : _transitionColorTint.fadeDuration);
							hierarchyInteractionStateTransitionHandlers.ExecuteOnAll(_transitionArgs);
							if (_onInteractionStateTransition != null)
							{
								_onInteractionStateTransition.Invoke(_transitionArgs);
								num = 1987692359;
								continue;
							}
							goto IL_0182;
						}
						return;
					case 23:
						break;
					case 25:
						YYvfGdnVUAOPddcwvcrImIcuboG(sprite);
						num = 1987692356;
						continue;
					case 20:
						goto IL_0182;
					case 21:
						unityEvent.Invoke();
						num = 1987692374;
						continue;
					case 24:
						goto IL_01a9;
					case 6:
						color.a = 0f;
						num = 1987692361;
						continue;
					case 18:
						if (!flag)
						{
							color = Color.white;
							num = 1987692360;
							continue;
						}
						goto IL_02f3;
					case 19:
						unityEvent = _onInteractionStateChangedToHighlighted;
						num = 1987692369;
						continue;
					case 9:
						flag = ((_transitionType & TransitionTypeFlags.ColorTint) != 0);
						num = 1987692353;
						continue;
					case 8:
						goto IL_021f;
					case 0:
						text = string.Empty;
						unityEvent = null;
						num = 1987692378;
						continue;
					case 22:
						goto IL_025c;
					case 11:
						goto IL_026c;
					case 26:
						if (!base.gameObject.activeInHierarchy)
						{
							goto case 4;
						}
						goto IL_0292;
					case 13:
						goto IL_02aa;
					case 17:
						DXZIsWKrjVFvmONoAmLYyqbnSYC(text);
						num = 1987692375;
						continue;
					case 2:
						num = 1987692378;
						continue;
					case 27:
						goto IL_02f3;
					case 14:
						sprite = _transitionSpriteState.highlightedSprite;
						text = _transitionAnimationTriggers.highlightedTrigger;
						num = 1987692352;
						continue;
					case 15:
						goto IL_0331;
					case 7:
						num = 1987692378;
						continue;
					}
					int num2;
					if ((_transitionType & TransitionTypeFlags.Animation) == 0)
					{
						num = 1987692375;
						num2 = num;
					}
					else
					{
						num = 1987692354;
						num2 = num;
					}
					continue;
					IL_0292:
					int num3;
					if (flag)
					{
						num = 1987692355;
						num3 = num;
					}
					else
					{
						num = 1987692377;
						num3 = num;
					}
					continue;
					IL_01a9:
					int num4;
					if ((_transitionType & TransitionTypeFlags.SpriteSwap) != 0)
					{
						num = 1987692362;
						num4 = num;
					}
					else
					{
						num = 1987692356;
						num4 = num;
					}
					continue;
					IL_0182:
					int num5;
					if (unityEvent == null)
					{
						num = 1987692374;
						num5 = num;
					}
					else
					{
						num = 1987692358;
						num5 = num;
					}
					continue;
					IL_02f3:
					int num6;
					if (!_visible)
					{
						num = 1987692373;
						num6 = num;
					}
					else
					{
						num = 1987692361;
						num6 = num;
					}
				}
				IL_0331:
				color = _transitionColorTint.disabledColor;
				sprite = _transitionSpriteState.disabledSprite;
				text = _transitionAnimationTriggers.disabledTrigger;
				unityEvent = _onInteractionStateChangedToDisabled;
				num = 1987692378;
				goto IL_0024;
				IL_02aa:
				color = _transitionColorTint.pressedColor;
				sprite = _transitionSpriteState.pressedSprite;
				text = _transitionAnimationTriggers.pressedTrigger;
				num = 1987692370;
				goto IL_0024;
				IL_026c:
				color = _transitionColorTint.highlightedColor;
				num = 1987692381;
				goto IL_0024;
				IL_025c:
				color = Color.black;
				num = 1987692383;
				goto IL_0024;
			}
		}

		private void LJUhxiZrQAfWgFGuXVojBKYVgGz(Color P_0, bool P_1)
		{
			if (!(_targetGraphic == null))
			{
				_targetGraphic.CrossFadeColor(P_0, P_1 ? 0f : _transitionColorTint.fadeDuration, ignoreTimeScale: true, useAlpha: true);
			}
		}

		private void YYvfGdnVUAOPddcwvcrImIcuboG(Sprite P_0)
		{
			if (!(image == null))
			{
				image.overrideSprite = P_0;
			}
		}

		private void DXZIsWKrjVFvmONoAmLYyqbnSYC(string P_0)
		{
			//Discarded unreachable code: IL_00ba
			if ((_transitionType & TransitionTypeFlags.Animation) == 0)
			{
				return;
			}
			while (true)
			{
				int num = 795210744;
				while (true)
				{
					switch (num ^ 0x2F65F3FE)
					{
					default:
						return;
					case 1:
						return;
					case 4:
						return;
					case 2:
						break;
					case 3:
						animator.ResetTrigger(_transitionAnimationTriggers.normalTrigger);
						num = 795210747;
						continue;
					case 0:
						if (UnityTools.IsActiveAndEnabled(animator) && !(animator.runtimeAnimatorController == null))
						{
							int num3;
							if (!string.IsNullOrEmpty(P_0))
							{
								num = 795210749;
								num3 = num;
							}
							else
							{
								num = 795210751;
								num3 = num;
							}
							continue;
						}
						return;
					case 6:
					{
						int num2;
						if (animator == null)
						{
							num = 795210751;
							num2 = num;
						}
						else
						{
							num = 795210750;
							num2 = num;
						}
						continue;
					}
					case 5:
						animator.ResetTrigger(_transitionAnimationTriggers.pressedTrigger);
						animator.ResetTrigger(_transitionAnimationTriggers.highlightedTrigger);
						animator.ResetTrigger(_transitionAnimationTriggers.disabledTrigger);
						animator.SetTrigger(P_0);
						num = 795210746;
						continue;
					}
					break;
				}
			}
		}

		private void qKRTkfOGLUHHYELogNnEEhiEJte(bool P_0)
		{
			InteractionState interactionState = _interactionState;
			if (agGGiexlYIVgemjaHHzzcCmJTcc())
			{
				while (true)
				{
					int num = 14229356;
					while (true)
					{
						switch (num ^ 0xD91F6E)
						{
						case 0:
							break;
						case 2:
							if (!IsInteractable())
							{
								interactionState = InteractionState.Disabled;
								num = 14229359;
								continue;
							}
							goto IL_003e;
						default:
							goto IL_003e;
						}
						break;
					}
				}
			}
			goto IL_003e;
			IL_003e:
			ZfPVPoqRQiwHjlckPfDuMoPdZcW(interactionState, P_0);
		}

		public bool IsInteractable()
		{
			if (_groupsAllowInteraction)
			{
				return _interactable;
			}
			return false;
		}

		internal virtual bool IsPressed()
		{
			if (!agGGiexlYIVgemjaHHzzcCmJTcc())
			{
				return false;
			}
			if (BSbeTqiFrdRDtCmjwklbhCLtNiqg)
			{
				return yrfVZiGUTfOClpVIfIvNRqImIGGe;
			}
			return false;
		}

		internal void sjfOHMxvWyaFrJNCKmQvSrptfRJ(BaseEventData P_0)
		{
			//Discarded unreachable code: IL_0040, IL_0057
			if (agGGiexlYIVgemjaHHzzcCmJTcc())
			{
				if (!IsInteractable())
				{
					goto IL_0010;
				}
				goto IL_0047;
			}
			return;
			IL_0010:
			int num = 404068812;
			goto IL_0015;
			IL_0047:
			InteractionState interactionState = hxgPveyFoTIVfFuKNYuNINuylLQ(P_0);
			num = 404068810;
			goto IL_0015;
			IL_0015:
			switch (num ^ 0x181599C8)
			{
			case 4:
				return;
			case 0:
				break;
			case 2:
				if (interactionState == _interactionState)
				{
					return;
				}
				goto default;
			case 3:
				goto IL_0047;
			default:
				LPkunBXNoVtukVfxHWtaKQBtvIt(interactionState);
				qKRTkfOGLUHHYELogNnEEhiEJte(false);
				return;
			}
			goto IL_0010;
		}

		internal virtual bool IsThisOrTouchRegionGameObject(GameObject P_0)
		{
			return base.gameObject == P_0;
		}

		private bool CDEfHWcXCTPvIDRvzGPVDYZHTtaK(BaseEventData P_0)
		{
			bool flag = P_0 is PointerEventData;
			return CDEfHWcXCTPvIDRvzGPVDYZHTtaK(flag, flag ? (P_0 as PointerEventData).pointerPress : null);
		}

		private bool CDEfHWcXCTPvIDRvzGPVDYZHTtaK(bool P_0, GameObject P_1)
		{
			if (!agGGiexlYIVgemjaHHzzcCmJTcc())
			{
				goto IL_0008;
			}
			if (IsPressed())
			{
				return false;
			}
			bool flag = false;
			int num = 44458867;
			goto IL_000d;
			IL_0008:
			num = 44458864;
			goto IL_000d;
			IL_000d:
			while (true)
			{
				switch (num ^ 0x2A66371)
				{
				case 3:
					break;
				case 1:
					return false;
				case 0:
					flag |= BSbeTqiFrdRDtCmjwklbhCLtNiqg;
					num = 44458869;
					continue;
				case 2:
					if (P_0)
					{
						flag |= ((yrfVZiGUTfOClpVIfIvNRqImIGGe && !BSbeTqiFrdRDtCmjwklbhCLtNiqg && IsThisOrTouchRegionGameObject(P_1)) || (!yrfVZiGUTfOClpVIfIvNRqImIGGe && BSbeTqiFrdRDtCmjwklbhCLtNiqg && IsThisOrTouchRegionGameObject(P_1)) || (!yrfVZiGUTfOClpVIfIvNRqImIGGe && BSbeTqiFrdRDtCmjwklbhCLtNiqg && P_1 == null));
						num = 44458869;
						continue;
					}
					goto case 0;
				default:
					return flag;
				}
				break;
			}
			goto IL_0008;
		}

		private InteractionState hxgPveyFoTIVfFuKNYuNINuylLQ(BaseEventData P_0)
		{
			if (IsPressed())
			{
				return InteractionState.Pressed;
			}
			if (CDEfHWcXCTPvIDRvzGPVDYZHTtaK(P_0))
			{
				return InteractionState.Highlighted;
			}
			return InteractionState.Normal;
		}

		private bool LPkunBXNoVtukVfxHWtaKQBtvIt(InteractionState P_0)
		{
			if (_interactionState == P_0)
			{
				return false;
			}
			_interactionState = P_0;
			AaDRXXGXIQsHGXeDAGPeLoBHyny();
			return true;
		}

		private void AaDRXXGXIQsHGXeDAGPeLoBHyny()
		{
			AjtGQvaygyDapEyGSAwyqZkOGRQ();
		}

		private void AjtGQvaygyDapEyGSAwyqZkOGRQ()
		{
			//Discarded unreachable code: IL_002a, IL_003a
			if (!Application.isPlaying)
			{
				while (true)
				{
					switch (-513784686 ^ -513784685)
					{
					case 1:
						return;
					case 0:
						break;
					case 3:
						goto IL_0031;
					default:
						goto IL_0041;
					}
				}
			}
			goto IL_0031;
			IL_0041:
			DslcJwXTzoxJrqDkiUIuuiJQYNZ(_interactionState == InteractionState.Pressed, false);
			return;
			IL_0031:
			if (!_hideWhenIdle)
			{
				return;
			}
			goto IL_0041;
		}

		private void DslcJwXTzoxJrqDkiUIuuiJQYNZ(bool P_0, bool P_1)
		{
			//Discarded unreachable code: IL_002f
			if (_visible == P_0)
			{
				goto IL_0009;
			}
			goto IL_0036;
			IL_0036:
			_visible = P_0;
			_varWatch_visible = P_0;
			int num;
			if (_allowSendingEvents)
			{
				hierarchyVisibilityChangedHandlers.ExecuteOnAll(P_0);
				if (_onVisibilityChanged != null)
				{
					_onVisibilityChanged.Invoke(P_0);
					num = -198871401;
					goto IL_000e;
				}
				return;
			}
			return;
			IL_000e:
			switch (num ^ -198871404)
			{
			default:
				return;
			case 3:
				return;
			case 0:
				break;
			case 1:
				if (!P_1)
				{
					return;
				}
				goto IL_0036;
			case 2:
				goto IL_0036;
			}
			goto IL_0009;
			IL_0009:
			num = -198871403;
			goto IL_000e;
		}

		private void tJvGxLTYBcmKEdCZNJVdFQoLtqu()
		{
			_varWatch_visible = _visible;
			while (true)
			{
				int num = 1671458614;
				while (true)
				{
					switch (num ^ 0x63A06F37)
					{
					case 0:
						break;
					case 1:
						goto IL_002c;
					default:
						using (new SetAndRestoreVar<bool>(_allowSendingEvents, newValue: false, delegate(bool P_0)
						{
							_allowSendingEvents = P_0;
						}))
						{
							DslcJwXTzoxJrqDkiUIuuiJQYNZ(_visible, true);
							AjtGQvaygyDapEyGSAwyqZkOGRQ();
						}
						pqeSwAyWVnauZHlEShRCcdDuCYY();
						while (true)
						{
							int num2 = 1671458612;
							while (true)
							{
								switch (num2 ^ 0x63A06F37)
								{
								default:
									return;
								case 2:
									return;
								case 0:
									break;
								case 3:
									if (_allowSendingEvents)
									{
										hierarchyVisibilityChangedHandlers.ExecuteOnAll(_visible);
										num2 = 1671458614;
										continue;
									}
									return;
								case 1:
									if (_onVisibilityChanged != null)
									{
										_onVisibilityChanged.Invoke(_visible);
										num2 = 1671458613;
										continue;
									}
									return;
								}
								break;
							}
						}
					}
					break;
					IL_002c:
					_varWatch_interactable = IsInteractable();
					num = 1671458613;
				}
			}
		}

		private void pQqdNGRnbQgbkiSLDqjfubKstGA()
		{
			if (_varWatch_visible == _visible)
			{
				return;
			}
			while (true)
			{
				int num = -117785557;
				while (true)
				{
					switch (num ^ -117785560)
					{
					default:
						return;
					case 1:
						return;
					case 0:
						break;
					case 3:
						_varWatch_visible = _visible;
						num = -117785558;
						continue;
					case 2:
						if (_allowSendingEvents && _onVisibilityChanged != null)
						{
							hierarchyVisibilityChangedHandlers.ExecuteOnAll(_visible);
							_onVisibilityChanged.Invoke(_visible);
							num = -117785559;
							continue;
						}
						return;
					}
					break;
				}
			}
		}

		private void GBExVtHFPRLrKdqZIHqEWcjtFsS()
		{
			//Discarded unreachable code: IL_003d
			pQqdNGRnbQgbkiSLDqjfubKstGA();
			while (true)
			{
				int num = -1663551395;
				while (true)
				{
					switch (num ^ -1663551396)
					{
					default:
						return;
					case 0:
						return;
					case 3:
						break;
					case 1:
						AjtGQvaygyDapEyGSAwyqZkOGRQ();
						if (Application.isPlaying)
						{
							goto IL_0044;
						}
						qKRTkfOGLUHHYELogNnEEhiEJte(true);
						return;
					case 2:
						goto IL_0044;
					}
					break;
					IL_0044:
					qKRTkfOGLUHHYELogNnEEhiEJte(false);
					num = -1663551396;
				}
			}
		}

		private void pqeSwAyWVnauZHlEShRCcdDuCYY()
		{
			hierarchyVisibilityChangedHandlers.GetHandlers(base.transform);
			hierarchyInteractionStateTransitionHandlers.GetHandlers(base.transform);
		}

		internal virtual void OnPointerDown(PointerEventData P_0)
		{
			if (!gtAdYPaYTSdxlqopRartpbSncsib(P_0.pointerId, _allowedMouseButtons, EventTriggerType.PointerDown))
			{
				return;
			}
			while (true)
			{
				yrfVZiGUTfOClpVIfIvNRqImIGGe = true;
				int num = 1821289999;
				while (true)
				{
					switch (num ^ 0x6C8EAE0D)
					{
					case 0:
						goto IL_0015;
					case 1:
						break;
					default:
						sjfOHMxvWyaFrJNCKmQvSrptfRJ(P_0);
						return;
					}
					break;
					IL_0015:
					num = 1821289996;
				}
			}
		}

		internal virtual void OnPointerUp(PointerEventData P_0)
		{
			if (!gtAdYPaYTSdxlqopRartpbSncsib(P_0.pointerId, _allowedMouseButtons, EventTriggerType.PointerUp))
			{
				return;
			}
			while (true)
			{
				yrfVZiGUTfOClpVIfIvNRqImIGGe = false;
				int num = -259525693;
				while (true)
				{
					switch (num ^ -259525693)
					{
					default:
						return;
					case 2:
						return;
					case 3:
						num = -259525694;
						continue;
					case 1:
						break;
					case 0:
						sjfOHMxvWyaFrJNCKmQvSrptfRJ(P_0);
						num = -259525695;
						continue;
					}
					break;
				}
			}
		}

		internal virtual void OnPointerEnter(PointerEventData P_0)
		{
			//Discarded unreachable code: IL_003b
			if (!gtAdYPaYTSdxlqopRartpbSncsib(P_0.pointerId, _allowedMouseButtons, EventTriggerType.PointerEnter))
			{
				goto IL_0014;
			}
			goto IL_0042;
			IL_0042:
			BSbeTqiFrdRDtCmjwklbhCLtNiqg = true;
			int num = 222079037;
			goto IL_0019;
			IL_0019:
			while (true)
			{
				switch (num ^ 0xD3CA83D)
				{
				default:
					return;
				case 2:
					return;
				case 3:
					return;
				case 4:
					break;
				case 1:
					goto IL_0042;
				case 0:
					sjfOHMxvWyaFrJNCKmQvSrptfRJ(P_0);
					num = 222079038;
					continue;
				}
				break;
			}
			goto IL_0014;
			IL_0014:
			num = 222079039;
			goto IL_0019;
		}

		internal virtual void OnPointerExit(PointerEventData P_0)
		{
			if (!gtAdYPaYTSdxlqopRartpbSncsib(P_0.pointerId, _allowedMouseButtons, EventTriggerType.PointerExit))
			{
				return;
			}
			while (true)
			{
				BSbeTqiFrdRDtCmjwklbhCLtNiqg = false;
				int num = -615278740;
				while (true)
				{
					switch (num ^ -615278740)
					{
					case 2:
						goto IL_0015;
					case 1:
						break;
					default:
						sjfOHMxvWyaFrJNCKmQvSrptfRJ(P_0);
						return;
					}
					break;
					IL_0015:
					num = -615278739;
				}
			}
		}

		internal virtual void OnBeginDrag(PointerEventData P_0)
		{
			gtAdYPaYTSdxlqopRartpbSncsib(P_0.pointerId, _allowedMouseButtons, EventTriggerType.BeginDrag);
		}

		internal virtual void OnDrag(PointerEventData P_0)
		{
			gtAdYPaYTSdxlqopRartpbSncsib(P_0.pointerId, _allowedMouseButtons, EventTriggerType.Drag);
		}

		internal virtual void OnEndDrag(PointerEventData P_0)
		{
			gtAdYPaYTSdxlqopRartpbSncsib(P_0.pointerId, _allowedMouseButtons, EventTriggerType.EndDrag);
		}

		void IPointerDownHandler.OnPointerDown(PointerEventData eventData)
		{
			OnPointerDown(eventData);
		}

		void IPointerUpHandler.OnPointerUp(PointerEventData eventData)
		{
			OnPointerUp(eventData);
		}

		void IPointerEnterHandler.OnPointerEnter(PointerEventData eventData)
		{
			OnPointerEnter(eventData);
		}

		void IPointerExitHandler.OnPointerExit(PointerEventData eventData)
		{
			OnPointerExit(eventData);
		}

		void IBeginDragHandler.OnBeginDrag(PointerEventData eventData)
		{
			OnBeginDrag(eventData);
		}

		void IDragHandler.OnDrag(PointerEventData eventData)
		{
			OnDrag(eventData);
		}

		void IEndDragHandler.OnEndDrag(PointerEventData eventData)
		{
			OnEndDrag(eventData);
		}

		internal static bool MUAEmHanKCAekGtjjVciojDFRqNV(int P_0)
		{
			if (P_0 == int.MinValue)
			{
				return false;
			}
			if (BjWUubnUnTBxMVzCyvrMCwQMuce(P_0))
			{
				goto IL_0012;
			}
			int num;
			if (OenwdeSUZHKJsTIxPjYHJUNnctC(P_0) && Input.mousePresent)
			{
				num = -1758775405;
				goto IL_0017;
			}
			goto IL_0091;
			IL_0017:
			Touch touch = default(Touch);
			while (true)
			{
				switch (num ^ -1758775407)
				{
				case 3:
					break;
				case 1:
					goto IL_0034;
				case 0:
					goto IL_004f;
				default:
					goto IL_007f;
				}
				break;
				IL_0034:
				int num2 = wBuGXsVlsURbtjUgYIPNmgZAfMT(P_0);
				if (num2 < 0)
				{
					return false;
				}
				touch = Input.GetTouch(num2);
				num = -1758775407;
			}
			goto IL_0012;
			IL_0012:
			num = -1758775408;
			goto IL_0017;
			IL_007f:
			int num3 = vcgeGYeyQPQEkWwJbecpKPinCTmx(P_0);
			if (num3 >= 0)
			{
				return Input.GetMouseButton(num3);
			}
			goto IL_0091;
			IL_004f:
			if (touch.phase != TouchPhase.Ended)
			{
				return touch.phase != TouchPhase.Canceled;
			}
			return false;
			IL_0091:
			return false;
		}

		internal static Vector3 peFNVeWnRvpCYQMMXzPLXboQIi(int P_0)
		{
			int num = default(int);
			int num2;
			if (BjWUubnUnTBxMVzCyvrMCwQMuce(P_0))
			{
				num = wBuGXsVlsURbtjUgYIPNmgZAfMT(P_0);
				if (num >= 0 && Input.touchCount > num)
				{
					goto IL_001b;
				}
			}
			else if (OenwdeSUZHKJsTIxPjYHJUNnctC(P_0))
			{
				num2 = -1464956492;
				goto IL_0020;
			}
			goto IL_006b;
			IL_0020:
			switch (num2 ^ -1464956491)
			{
			case 0:
				break;
			case 2:
				return Input.touches[num].position;
			default:
				goto IL_005e;
			}
			goto IL_001b;
			IL_005e:
			if (Input.mousePresent)
			{
				return Input.mousePosition;
			}
			goto IL_006b;
			IL_006b:
			return Vector3.zero;
			IL_001b:
			num2 = -1464956489;
			goto IL_0020;
		}

		internal static bool BjWUubnUnTBxMVzCyvrMCwQMuce(int P_0)
		{
			return P_0 >= 0;
		}

		internal static bool OenwdeSUZHKJsTIxPjYHJUNnctC(int P_0)
		{
			if (P_0 != -1)
			{
				while (true)
				{
					int num = 1112632523;
					while (true)
					{
						switch (num ^ 0x42516CCA)
						{
						case 0:
							break;
						case 1:
							goto IL_0022;
						default:
							return P_0 == -2;
						}
						break;
						IL_0022:
						if (P_0 != -3)
						{
							num = 1112632520;
							continue;
						}
						goto IL_0034;
					}
				}
			}
			goto IL_0034;
			IL_0034:
			return true;
		}

		private static int wBuGXsVlsURbtjUgYIPNmgZAfMT(int P_0)
		{
			if (!BjWUubnUnTBxMVzCyvrMCwQMuce(P_0))
			{
				return -1;
			}
			int touchCount = Input.touchCount;
			int num = 0;
			while (true)
			{
				int num2;
				int num3;
				if (num >= touchCount)
				{
					num2 = 536832597;
					num3 = num2;
				}
				else
				{
					num2 = 536832592;
					num3 = num2;
				}
				while (true)
				{
					switch (num2 ^ 0x1FFF6A51)
					{
					case 0:
						num2 = 536832592;
						continue;
					case 1:
						if (Input.GetTouch(num).fingerId == P_0)
						{
							num2 = 536832594;
							continue;
						}
						num++;
						num2 = 536832595;
						continue;
					case 3:
						return num;
					case 2:
						break;
					default:
						return -1;
					}
					break;
				}
			}
		}

		internal static bool gtAdYPaYTSdxlqopRartpbSncsib(MouseButtonFlags P_0, int P_1)
		{
			int num;
			if (OenwdeSUZHKJsTIxPjYHJUNnctC(P_1))
			{
				if (!Cursor.visible)
				{
					goto IL_000f;
				}
				if (!Input.mousePresent)
				{
					num = -742535514;
					goto IL_0014;
				}
			}
			if (BjWUubnUnTBxMVzCyvrMCwQMuce(P_1))
			{
				num = -742535515;
				goto IL_0014;
			}
			if (ARJAtFBrmWWAEjjaIPZDnUNqTRvK(P_0, P_1))
			{
				return true;
			}
			return false;
			IL_0014:
			switch (num ^ -742535515)
			{
			case 2:
				break;
			case 1:
				return false;
			case 3:
				return false;
			default:
				return true;
			}
			goto IL_000f;
			IL_000f:
			num = -742535516;
			goto IL_0014;
		}

		private static bool ARJAtFBrmWWAEjjaIPZDnUNqTRvK(MouseButtonFlags P_0, int P_1)
		{
			//Discarded unreachable code: IL_0037
			while (true)
			{
				switch (-534461532 ^ -534461530)
				{
				case 0:
					continue;
				case 2:
					switch (P_1)
					{
					case -1:
						break;
					case -2:
						return (P_0 & MouseButtonFlags.RightButton) != 0;
					case -3:
						return (P_0 & MouseButtonFlags.MiddleButton) != 0;
					default:
						return false;
					}
					break;
				}
				break;
			}
			return (P_0 & MouseButtonFlags.LeftButton) != 0;
		}

		private static int vcgeGYeyQPQEkWwJbecpKPinCTmx(int P_0)
		{
			if (!OenwdeSUZHKJsTIxPjYHJUNnctC(P_0))
			{
				return -1;
			}
			switch (P_0)
			{
			case -1:
				return 0;
			case -2:
				return 1;
			case -3:
				return 2;
			default:
				return -1;
			}
		}

		internal static bool rXoHlhXIkgBrHzajLhaabUWFwEkO(MouseButtonFlags P_0, out int P_1)
		{
			int num = 0;
			while (num < 3)
			{
				while (true)
				{
					if (((int)P_0 & (1 << num)) != 0 && Input.GetMouseButton(num))
					{
						P_1 = (num + 1) * -1;
						return true;
					}
					num++;
					int num2 = 1070411344;
					while (true)
					{
						switch (num2 ^ 0x3FCD2E52)
						{
						case 0:
							num2 = 1070411347;
							continue;
						case 1:
							break;
						default:
							goto IL_004a;
						}
						break;
					}
				}
				IL_004a:;
			}
			P_1 = int.MinValue;
			return false;
		}

		internal static bool gtAdYPaYTSdxlqopRartpbSncsib(int P_0, MouseButtonFlags P_1, EventTriggerType P_2)
		{
			if (OenwdeSUZHKJsTIxPjYHJUNnctC(P_0))
			{
				if (P_2 == EventTriggerType.PointerEnter)
				{
					goto IL_002d;
				}
				if (P_2 == EventTriggerType.PointerExit)
				{
					goto IL_000f;
				}
			}
			goto IL_003c;
			IL_0014:
			int num;
			switch (num ^ 0x580D80F7)
			{
			case 0:
				break;
			case 2:
				goto IL_002d;
			default:
				goto IL_003c;
			}
			goto IL_000f;
			IL_000f:
			num = 1477279989;
			goto IL_0014;
			IL_002d:
			if (P_1 != 0)
			{
				P_1 |= MouseButtonFlags.LeftButton;
				num = 1477279990;
				goto IL_0014;
			}
			goto IL_003c;
			IL_003c:
			return gtAdYPaYTSdxlqopRartpbSncsib(P_1, P_0);
		}

		internal static bool jWwDMVskgRDHPeFKiMdRILlfbbMg(MouseButtonFlags P_0)
		{
			int num;
			return rXoHlhXIkgBrHzajLhaabUWFwEkO(P_0, out num);
		}

		[CompilerGenerated]
		private void AOANKhoJNGLIQPFRIVbTPhKwFcq(bool P_0)
		{
			_allowSendingEvents = P_0;
		}

		[CompilerGenerated]
		private static void QtlkZHwtvJHpXCOlgZmpzyAsQBu(IInteractionStateTransitionHandler P_0, InteractionStateTransitionArgs P_1)
		{
			P_0.OnInteractionStateTransition(P_1);
		}
	}
}
