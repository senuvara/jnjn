using Rewired.ComponentControls.Data;
using Rewired.Internal;
using Rewired.Utils;
using Rewired.Utils.Attributes;
using System;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.Events;
using UnityEngine.EventSystems;
using UnityEngine.UI;

namespace Rewired.ComponentControls
{
	[Serializable]
	[RequireComponent(typeof(Image))]
	[DisallowMultipleComponent]
	public sealed class TouchPad : TouchInteractable, IPointerDownHandler, IPointerUpHandler, IEventSystemHandler
	{
		public enum AxisDirection
		{
			Both,
			Horizontal,
			Vertical
		}

		public enum TouchPadMode
		{
			Delta,
			ScreenPosition,
			VectorFromCenter,
			VectorFromInitialTouch
		}

		public enum ValueFormat
		{
			Pixels,
			Screen,
			Physical,
			Direction
		}

		private class sAFSHuKZuEiOBBRymCuyAwASsXcn
		{
			private class ARaarudnkaYEEBNxBAVHfnPVGiZm
			{
				public float ouzcOEYydrlQCzMStrEYBESGfvN;

				public float SNaAPWdeiPmmfHMCHULecRXymMJa;

				public uint vnoSRcpwOOYOMFWJqrlGaiTFbMj;
			}

			private int zNXquWELhFNycOqfGGtqWvubSNJ;

			private ARaarudnkaYEEBNxBAVHfnPVGiZm[] WBfQciSrovnPIpngxqhztwVhEqL;

			private int OTwcevfveslTXvjuGoUWgCMeibVS = -1;

			public sAFSHuKZuEiOBBRymCuyAwASsXcn(int maxSmoothFrames)
			{
				if (maxSmoothFrames < 2)
				{
					throw new ArgumentOutOfRangeException("maxSmoothFrames must be >= 2");
				}
				zNXquWELhFNycOqfGGtqWvubSNJ = maxSmoothFrames;
				WBfQciSrovnPIpngxqhztwVhEqL = new ARaarudnkaYEEBNxBAVHfnPVGiZm[maxSmoothFrames];
				ArrayTools.Populate(WBfQciSrovnPIpngxqhztwVhEqL);
			}

			public void HDNHWHMIgYVNwexHqBXaTGgQOqc(float P_0, float P_1)
			{
				uint currentFrame = ReInput.currentFrame;
				if (OTwcevfveslTXvjuGoUWgCMeibVS >= 0 && WBfQciSrovnPIpngxqhztwVhEqL[OTwcevfveslTXvjuGoUWgCMeibVS].vnoSRcpwOOYOMFWJqrlGaiTFbMj == currentFrame)
				{
					return;
				}
				while (true)
				{
					HzLfIFUEyXoJNsJHpcndwfeETma();
					ARaarudnkaYEEBNxBAVHfnPVGiZm aRaarudnkaYEEBNxBAVHfnPVGiZm = WBfQciSrovnPIpngxqhztwVhEqL[OTwcevfveslTXvjuGoUWgCMeibVS];
					int num = -211071318;
					while (true)
					{
						switch (num ^ -211071318)
						{
						default:
							return;
						case 1:
							return;
						case 2:
							num = -211071319;
							continue;
						case 3:
							break;
						case 0:
							aRaarudnkaYEEBNxBAVHfnPVGiZm.ouzcOEYydrlQCzMStrEYBESGfvN = P_0;
							aRaarudnkaYEEBNxBAVHfnPVGiZm.SNaAPWdeiPmmfHMCHULecRXymMJa = P_1;
							aRaarudnkaYEEBNxBAVHfnPVGiZm.vnoSRcpwOOYOMFWJqrlGaiTFbMj = currentFrame;
							num = -211071317;
							continue;
						}
						break;
					}
				}
			}

			public Vector2 HQcOXZEZNJYXxzDNEfseqcouOPL()
			{
				if (OTwcevfveslTXvjuGoUWgCMeibVS < 0)
				{
					return default(Vector2);
				}
				int oTwcevfveslTXvjuGoUWgCMeibVS = OTwcevfveslTXvjuGoUWgCMeibVS;
				ARaarudnkaYEEBNxBAVHfnPVGiZm aRaarudnkaYEEBNxBAVHfnPVGiZm = WBfQciSrovnPIpngxqhztwVhEqL[oTwcevfveslTXvjuGoUWgCMeibVS];
				Vector2 result = new Vector2(aRaarudnkaYEEBNxBAVHfnPVGiZm.ouzcOEYydrlQCzMStrEYBESGfvN, aRaarudnkaYEEBNxBAVHfnPVGiZm.SNaAPWdeiPmmfHMCHULecRXymMJa);
				uint vnoSRcpwOOYOMFWJqrlGaiTFbMj = aRaarudnkaYEEBNxBAVHfnPVGiZm.vnoSRcpwOOYOMFWJqrlGaiTFbMj;
				int num2 = default(int);
				ARaarudnkaYEEBNxBAVHfnPVGiZm aRaarudnkaYEEBNxBAVHfnPVGiZm2 = default(ARaarudnkaYEEBNxBAVHfnPVGiZm);
				int num3 = default(int);
				while (true)
				{
					int num = -1939292547;
					while (true)
					{
						switch (num ^ -1939292552)
						{
						case 7:
							break;
						case 2:
							result.y /= num2;
							num = -1939292549;
							continue;
						case 6:
							if (WliiaFzVAXBnqmnAyhkeeXraakJY(aRaarudnkaYEEBNxBAVHfnPVGiZm2.vnoSRcpwOOYOMFWJqrlGaiTFbMj, vnoSRcpwOOYOMFWJqrlGaiTFbMj))
							{
								result.x += aRaarudnkaYEEBNxBAVHfnPVGiZm2.ouzcOEYydrlQCzMStrEYBESGfvN;
								num = -1939292548;
								continue;
							}
							goto case 1;
						case 8:
						{
							int num4;
							if ((num3 = AbGjedYACfEpSCNLxPESdVsaHSWM(num3, zNXquWELhFNycOqfGGtqWvubSNJ)) == oTwcevfveslTXvjuGoUWgCMeibVS)
							{
								num = -1939292551;
								num4 = num;
							}
							else
							{
								num = -1939292552;
								num4 = num;
							}
							continue;
						}
						case 5:
							num3 = oTwcevfveslTXvjuGoUWgCMeibVS;
							num2 = 1;
							num = -1939292560;
							continue;
						case 1:
							if (num2 > 0)
							{
								result.x /= num2;
								num = -1939292550;
								continue;
							}
							goto default;
						case 0:
							aRaarudnkaYEEBNxBAVHfnPVGiZm2 = WBfQciSrovnPIpngxqhztwVhEqL[num3];
							num = -1939292546;
							continue;
						case 4:
							result.y += aRaarudnkaYEEBNxBAVHfnPVGiZm2.SNaAPWdeiPmmfHMCHULecRXymMJa;
							vnoSRcpwOOYOMFWJqrlGaiTFbMj = aRaarudnkaYEEBNxBAVHfnPVGiZm2.vnoSRcpwOOYOMFWJqrlGaiTFbMj;
							num2++;
							num = -1939292560;
							continue;
						default:
							return result;
						}
						break;
					}
				}
			}

			private void HzLfIFUEyXoJNsJHpcndwfeETma()
			{
				OTwcevfveslTXvjuGoUWgCMeibVS = pgXCgKmaxuYiAkoNErHkvRcQeas(OTwcevfveslTXvjuGoUWgCMeibVS, zNXquWELhFNycOqfGGtqWvubSNJ);
			}

			private static int pgXCgKmaxuYiAkoNErHkvRcQeas(int P_0, int P_1)
			{
				if (P_0 >= P_1 - 1)
				{
					return 0;
				}
				return ++P_0;
			}

			private int AbGjedYACfEpSCNLxPESdVsaHSWM(int P_0, int P_1)
			{
				if (P_0 > 0)
				{
					return --P_0;
				}
				return P_1 - 1;
			}

			private static bool WliiaFzVAXBnqmnAyhkeeXraakJY(uint P_0, uint P_1)
			{
				if (P_1 == 0)
				{
					return P_0 == uint.MaxValue;
				}
				return P_0 == P_1 - 1;
			}
		}

		[Serializable]
		public class ValueChangedEventHandler : UnityEvent<Vector2>
		{
		}

		[Serializable]
		public class TapEventHandler : UnityEvent
		{
		}

		[Serializable]
		public class PressDownEventHandler : UnityEvent
		{
		}

		[Serializable]
		public class PressUpEventHandler : UnityEvent
		{
		}

		private const int SMOOTH_DELTA_FRAME_COUNT = 3;

		[Tooltip("The Custom Controller element that will receive input values from the touch pad's X axis.")]
		[CustomObfuscation(rename = false)]
		[SerializeField]
		private CustomControllerElementTargetSetForFloat _horizontalAxisCustomControllerElement = new CustomControllerElementTargetSetForFloat();

		[SerializeField]
		[CustomObfuscation(rename = false)]
		[Tooltip("The Custom Controller element that will receive input values from the touch pad's Y axis.")]
		private CustomControllerElementTargetSetForFloat _verticalAxisCustomControllerElement = new CustomControllerElementTargetSetForFloat();

		[Tooltip("The Custom Controller element that will receive input values from touch pad taps.")]
		[CustomObfuscation(rename = false)]
		[SerializeField]
		private CustomControllerElementTargetSetForBoolean _tapCustomControllerElement = new CustomControllerElementTargetSetForBoolean();

		[SerializeField]
		[Tooltip("The Custom Controller element that will receive input values from touch pad presses.")]
		[CustomObfuscation(rename = false)]
		private CustomControllerElementTargetSetForBoolean _pressCustomControllerElement = new CustomControllerElementTargetSetForBoolean();

		[CustomObfuscation(rename = false)]
		[SerializeField]
		[Tooltip("The axis directions in which movement is allowed. You can restrict movement to one or both axes.")]
		private AxisDirection _axesToUse;

		[Tooltip("The mode of the touch pad.\n\nDelta - Returns the change in position of the touch from the previous to the current frame.\n\nScreen Position - Returns the absolute position of the touch  on the screen.\n\nVector From Center - Returns a vector from the center of the Touch Pad to the current touch position.\n\nVector From Initial Touch - Returns a vector from the intial touch position to the current touch position.")]
		[CustomObfuscation(rename = false)]
		[SerializeField]
		private TouchPadMode _touchPadMode;

		[Tooltip("The format of the resulting data generated by the touch pad.\n\nPixels - Screen pixels.\n\nScreen - The proportion of the value to screen size in the corresponding dimension. 1 unit = 1 screen length (width for X, height for Y).\n\nPhysical - 1 unit = 1/100th of an inch. The resulting value will be consistent across different screen resolutions and sizes. IMPORTANT: This relies on the value returned by UnityEngine.Screen.dpi. If the device does not return a value, a reference resolution of 96 dpi will be used.\n\nDirection - A normalized direction vector.")]
		[SerializeField]
		[CustomObfuscation(rename = false)]
		private ValueFormat _valueFormat;

		[CustomObfuscation(rename = false)]
		[SerializeField]
		[Tooltip("If enabled, when swiped and released, the value will slowly fall toward zero based on the Friction value. This only has an effect if Touch Pad Mode is set to Position Delta.")]
		private bool _useInertia;

		[FieldRange(0f, float.MaxValue)]
		[SerializeField]
		[CustomObfuscation(rename = false)]
		[Tooltip("Determines how quickly a swipe value will fall toward zero when Use Inertia is enabled.")]
		private float _inertiaFriction = 3f;

		[SerializeField]
		[CustomObfuscation(rename = false)]
		[Tooltip("If true, the touch pad can be activated by a touch swipe that began in an area outside the touch pad region. If false, the touch pad can only be activated by a direct touch.")]
		private bool _activateOnSwipeIn;

		[CustomObfuscation(rename = false)]
		[SerializeField]
		[Tooltip("If true, the touch pad will stay engaged even if the touch that activated it moves outside the touch pad region. If false, the touch pad will be released once the touch that activated it moves outside the touch pad region.")]
		private bool _stayActiveOnSwipeOut = true;

		[Tooltip("Should taps on the touch pad be processed?")]
		[CustomObfuscation(rename = false)]
		[SerializeField]
		private bool _allowTap;

		[FieldRange(0f, float.MaxValue)]
		[SerializeField]
		[CustomObfuscation(rename = false)]
		[Tooltip("The maximum touch duration allowed for the touch to be considered a tap. A touch that lasts longer than this value will not trigger a tap when released.")]
		private float _tapTimeout = 0.25f;

		[SerializeField]
		[FieldRange(-1, int.MaxValue)]
		[CustomObfuscation(rename = false)]
		[Tooltip("The maximum movement distance allowed in pixels since the touch began for the touch to be considered a tap. [-1 = no limit]")]
		private int _tapDistanceLimit = 10;

		[Tooltip("Should presses (continual press like a button) on the touch pad be processed?")]
		[CustomObfuscation(rename = false)]
		[SerializeField]
		private bool _allowPress;

		[SerializeField]
		[CustomObfuscation(rename = false)]
		[Tooltip("Time the touch pad must be touched before it will be considered a press.")]
		private float _pressStartDelay = 0.1f;

		[SerializeField]
		[CustomObfuscation(rename = false)]
		[FieldRange(-1, int.MaxValue)]
		[Tooltip("The maximum movement distance allowed in pixels since the touch began for the touch to be considered a press. Any movement beyond this value will cancel the press. [-1 = no limit]")]
		private int _pressDistanceLimit = 10;

		[Tooltip("If enabled, the control will be hidden when gameplay starts.")]
		[CustomObfuscation(rename = false)]
		[SerializeField]
		private bool _hideAtRuntime;

		[CustomObfuscation(rename = false)]
		[Tooltip("The underlying Axis 2D.")]
		[SerializeField]
		private StandaloneAxis2D _axis2D = StandaloneAxis2D.CreateRelative();

		[Tooltip("Event sent when the value changes.")]
		[SerializeField]
		[CustomObfuscation(rename = false)]
		private ValueChangedEventHandler _onValueChanged = new ValueChangedEventHandler();

		[CustomObfuscation(rename = false)]
		[Tooltip("Event sent when the touch pad is tapped. This event will only be sent if allowTap is True.")]
		[SerializeField]
		private TapEventHandler _onTap = new TapEventHandler();

		[Tooltip("Event sent when the touch pad is initally pressed. This event is for the Press button simulation which must be enabled by setting Press Allowed to True. This event will only be sent if allowPress is True.")]
		[SerializeField]
		[CustomObfuscation(rename = false)]
		private PressDownEventHandler _onPressDown = new PressDownEventHandler();

		[Tooltip("Event sent when the touch pad is released after a press. This event is for the Press button simulation which must be enabled by setting Press Allowed to True. This event will only be sent if allowPress is True.")]
		[SerializeField]
		[CustomObfuscation(rename = false)]
		private PressUpEventHandler _onPressUp = new PressUpEventHandler();

		private bool _useXAxis;

		private bool _useYAxis;

		private int _pointerId = int.MinValue;

		private int _realMousePointerId = int.MinValue;

		[NonSerialized]
		private bool yrfVZiGUTfOClpVIfIvNRqImIGGe;

		[NonSerialized]
		private bool BSbeTqiFrdRDtCmjwklbhCLtNiqg;

		private bool _pointerDownIsFake;

		private Vector2 _touchStartPosition;

		private float _touchStartTime;

		private Vector3 _currentCenter;

		private Vector2 _previousTouchPosition;

		private int _lastTapFrame = -1;

		private bool _isEligibleForTap;

		private bool _isEligibleForPress;

		private bool _pressValue;

		private sAFSHuKZuEiOBBRymCuyAwASsXcn _smoothDelta = new sAFSHuKZuEiOBBRymCuyAwASsXcn(3);

		private Dictionary<int, PointerEventData> __fakePointerEventData;

		public CustomControllerElementTargetSetForFloat horizontalAxisCustomControllerElement => _horizontalAxisCustomControllerElement;

		public CustomControllerElementTargetSetForFloat verticalAxisCustomControllerElement => _verticalAxisCustomControllerElement;

		public CustomControllerElementTargetSetForBoolean tapCustomControllerElement => _tapCustomControllerElement;

		public CustomControllerElementTargetSetForBoolean pressCustomControllerElement => _pressCustomControllerElement;

		public AxisDirection axesToUse
		{
			get
			{
				return _axesToUse;
			}
			set
			{
				if (_axesToUse == value)
				{
					return;
				}
				while (true)
				{
					iSfJagPsCsVhUfDNyhNvbxLabufM(value);
					int num = -1633265500;
					while (true)
					{
						switch (num ^ -1633265499)
						{
						case 0:
							goto IL_000a;
						case 2:
							break;
						default:
							OnSetProperty();
							return;
						}
						break;
						IL_000a:
						num = -1633265497;
					}
				}
			}
		}

		public TouchPadMode touchPadMode
		{
			get
			{
				return _touchPadMode;
			}
			set
			{
				//Discarded unreachable code: IL_0028
				if (_touchPadMode == value)
				{
					while (true)
					{
						switch (0x1CF9F65F ^ 0x1CF9F65E)
						{
						case 1:
							return;
						case 2:
							continue;
						}
						break;
					}
				}
				_touchPadMode = value;
				OnSetProperty();
			}
		}

		public ValueFormat valueFormat
		{
			get
			{
				return _valueFormat;
			}
			set
			{
				if (_valueFormat == value)
				{
					return;
				}
				while (true)
				{
					_valueFormat = value;
					OnSetProperty();
					int num = 1277176764;
					while (true)
					{
						switch (num ^ 0x4C202BBD)
						{
						default:
							return;
						case 1:
							return;
						case 0:
							goto IL_000a;
						case 2:
							break;
						}
						break;
						IL_000a:
						num = 1277176767;
					}
				}
			}
		}

		public bool useInertia
		{
			get
			{
				return _useInertia;
			}
			set
			{
				//Discarded unreachable code: IL_0028
				if (_useInertia == value)
				{
					while (true)
					{
						switch (0x45CA5B84 ^ 0x45CA5B85)
						{
						case 1:
							return;
						case 2:
							continue;
						}
						break;
					}
				}
				_useInertia = value;
				OnSetProperty();
			}
		}

		public float inertiaFriction
		{
			get
			{
				return _inertiaFriction;
			}
			set
			{
				//Discarded unreachable code: IL_004a
				value = MathTools.Max(0f, value);
				while (true)
				{
					int num = -358019158;
					while (true)
					{
						switch (num ^ -358019159)
						{
						case 2:
							return;
						case 0:
							break;
						case 3:
						{
							int num2;
							if (_inertiaFriction != value)
							{
								num = -358019160;
								num2 = num;
							}
							else
							{
								num = -358019157;
								num2 = num;
							}
							continue;
						}
						default:
							_inertiaFriction = value;
							OnSetProperty();
							return;
						}
						break;
					}
				}
			}
		}

		public bool activateOnSwipeIn
		{
			get
			{
				return _activateOnSwipeIn;
			}
			set
			{
				if (_activateOnSwipeIn != value)
				{
					_activateOnSwipeIn = value;
					OnSetProperty();
				}
			}
		}

		public bool stayActiveOnSwipeOut
		{
			get
			{
				return _stayActiveOnSwipeOut;
			}
			set
			{
				if (_stayActiveOnSwipeOut != value)
				{
					_stayActiveOnSwipeOut = value;
					OnSetProperty();
				}
			}
		}

		public bool allowTap
		{
			get
			{
				return _allowTap;
			}
			set
			{
				if (_allowTap != value)
				{
					_allowTap = value;
					OnSetProperty();
				}
			}
		}

		public float tapTimeout
		{
			get
			{
				return _tapTimeout;
			}
			set
			{
				value = MathTools.Max(0f, value);
				if (_tapTimeout == value)
				{
					return;
				}
				while (true)
				{
					_tapTimeout = value;
					OnSetProperty();
					int num = 188770912;
					while (true)
					{
						switch (num ^ 0xB406A60)
						{
						default:
							return;
						case 0:
							return;
						case 2:
							goto IL_0017;
						case 1:
							break;
						}
						break;
						IL_0017:
						num = 188770913;
					}
				}
			}
		}

		public int tapDistanceLimit
		{
			get
			{
				return _tapDistanceLimit;
			}
			set
			{
				//Discarded unreachable code: IL_0031
				value = MathTools.Max(-1, value);
				while (true)
				{
					switch (-291074206 ^ -291074205)
					{
					case 2:
						continue;
					case 1:
						if (_tapDistanceLimit == value)
						{
							return;
						}
						break;
					}
					break;
				}
				_tapDistanceLimit = value;
				OnSetProperty();
			}
		}

		public bool allowPress
		{
			get
			{
				return _allowPress;
			}
			set
			{
				if (_allowPress == value)
				{
					return;
				}
				while (true)
				{
					_allowPress = value;
					int num = -1257857811;
					while (true)
					{
						switch (num ^ -1257857811)
						{
						case 2:
							goto IL_000a;
						case 1:
							break;
						default:
							OnSetProperty();
							return;
						}
						break;
						IL_000a:
						num = -1257857812;
					}
				}
			}
		}

		public float pressStartDelay
		{
			get
			{
				return _pressStartDelay;
			}
			set
			{
				//Discarded unreachable code: IL_0039
				value = Mathf.Max(0f, float.MaxValue);
				while (true)
				{
					switch (-1285528093 ^ -1285528094)
					{
					case 0:
						continue;
					case 1:
						if (_pressStartDelay == value)
						{
							return;
						}
						break;
					}
					break;
				}
				_pressStartDelay = value;
				OnSetProperty();
			}
		}

		public int pressDistanceLimit
		{
			get
			{
				return _pressDistanceLimit;
			}
			set
			{
				//Discarded unreachable code: IL_0031
				value = MathTools.Max(-1, value);
				while (true)
				{
					switch (0x65619665 ^ 0x65619664)
					{
					case 2:
						continue;
					case 1:
						if (_pressDistanceLimit == value)
						{
							return;
						}
						break;
					}
					break;
				}
				_pressDistanceLimit = value;
				OnSetProperty();
			}
		}

		public bool hideAtRuntime
		{
			get
			{
				return _hideAtRuntime;
			}
			set
			{
				if (!(_hideAtRuntime = value))
				{
					_hideAtRuntime = true;
					OnSetProperty();
				}
			}
		}

		public int pointerId
		{
			get
			{
				return _pointerId;
			}
			set
			{
				_pointerId = value;
			}
		}

		public bool hasPointer => _pointerId != int.MinValue;

		public Vector2 touchStartPosition
		{
			get
			{
				if (!hasPointer)
				{
					return Vector2.zero;
				}
				return _touchStartPosition;
			}
		}

		public Vector2 touchPosition
		{
			get
			{
				if (!TouchInteractable.MUAEmHanKCAekGtjjVciojDFRqNV(effectivePointerId))
				{
					return Vector2.zero;
				}
				return TouchInteractable.peFNVeWnRvpCYQMMXzPLXboQIi(effectivePointerId);
			}
		}

		internal StandaloneAxis2D axis2D => _axis2D;

		private int effectivePointerId
		{
			get
			{
				if (_pointerId == int.MinValue)
				{
					return int.MinValue;
				}
				if (_realMousePointerId != int.MinValue)
				{
					return _realMousePointerId;
				}
				return _pointerId;
			}
		}

		private bool tapValue => _lastTapFrame == Time.frameCount;

		public event UnityAction<Vector2> ValueChangedEvent
		{
			add
			{
				_onValueChanged.AddListener(value);
			}
			remove
			{
				_onValueChanged.RemoveListener(value);
			}
		}

		public event UnityAction TapEvent
		{
			add
			{
				_onTap.AddListener(value);
			}
			remove
			{
				_onTap.RemoveListener(value);
			}
		}

		public event UnityAction PressDownEvent
		{
			add
			{
				_onPressDown.AddListener(value);
			}
			remove
			{
				_onPressDown.RemoveListener(value);
			}
		}

		public event UnityAction PressUpEvent
		{
			add
			{
				_onPressUp.AddListener(value);
			}
			remove
			{
				_onPressUp.RemoveListener(value);
			}
		}

		[CustomObfuscation(rename = false)]
		private TouchPad()
		{
		}

		[CustomObfuscation(rename = false)]
		internal override void Awake()
		{
			base.Awake();
			if (!Application.isPlaying)
			{
				return;
			}
			while (_hideAtRuntime)
			{
				base.visible = false;
				int num = -1026971657;
				while (true)
				{
					switch (num ^ -1026971657)
					{
					default:
						return;
					case 0:
						return;
					case 2:
						goto IL_000e;
					case 1:
						break;
					}
					break;
					IL_000e:
					num = -1026971658;
				}
			}
		}

		[CustomObfuscation(rename = false)]
		internal override void OnValidate()
		{
			base.OnValidate();
			if (base.initialized)
			{
				cZNwcKOoWKuxnmVZWmbkUjmXOtE();
				GBExVtHFPRLrKdqZIHqEWcjtFsS();
			}
		}

		internal override bool OnInitialize()
		{
			if (!base.OnInitialize())
			{
				return false;
			}
			cZNwcKOoWKuxnmVZWmbkUjmXOtE();
			return true;
		}

		internal override void OnUpdate()
		{
			//Discarded unreachable code: IL_0035
			base.OnUpdate();
			while (true)
			{
				int num = -1448167190;
				while (true)
				{
					switch (num ^ -1448167189)
					{
					case 0:
						break;
					case 1:
						if (!base.initialized)
						{
							return;
						}
						goto case 3;
					case 4:
						OkKgKKVlDZfBBjhCGQnuBwkwcWaI();
						num = -1448167191;
						continue;
					case 3:
						UCAcUzyWwTaJkrollZKJQVQBPsx();
						FBlOXIugJLwhCfGPQgASiZxYKxE();
						num = -1448167185;
						continue;
					default:
						ZoKkNpzYwYljsuAyefAuMwwVXPM();
						IaovDVQjHWZnSxbEBXkayFEYapV();
						return;
					}
					break;
				}
			}
		}

		internal override void OnCustomControllerUpdate()
		{
			//Discarded unreachable code: IL_0074
			if (!base.initialized)
			{
				return;
			}
			Vector2 vector = default(Vector2);
			while (true)
			{
				int num;
				int num2;
				if (!hasController)
				{
					num = -1083952986;
					num2 = num;
				}
				else
				{
					num = -1083952985;
					num2 = num;
				}
				while (true)
				{
					switch (num ^ -1083952989)
					{
					default:
						return;
					case 5:
						return;
					case 6:
						return;
					case 0:
						num = -1083952991;
						continue;
					case 3:
						if (_useYAxis)
						{
							yQjPzGJGxRgoFPaCGxcgBaQECRD(_verticalAxisCustomControllerElement, vector.y, _axis2D.xAxis.buttonActivationThreshold);
							num = -1083952990;
							continue;
						}
						goto case 1;
					case 7:
						if (_allowPress)
						{
							yQjPzGJGxRgoFPaCGxcgBaQECRD(_pressCustomControllerElement, _pressValue);
							num = -1083952987;
							continue;
						}
						return;
					case 2:
						break;
					case 1:
						if (_allowTap)
						{
							yQjPzGJGxRgoFPaCGxcgBaQECRD(_tapCustomControllerElement, tapValue);
							num = -1083952988;
							continue;
						}
						goto case 7;
					case 4:
						vector = ((_touchPadMode == TouchPadMode.ScreenPosition) ? _axis2D.rawValue : _axis2D.value);
						if (_useXAxis)
						{
							yQjPzGJGxRgoFPaCGxcgBaQECRD(_horizontalAxisCustomControllerElement, vector.x, _axis2D.xAxis.buttonActivationThreshold);
							num = -1083952992;
							continue;
						}
						goto case 3;
					}
					break;
				}
			}
		}

		internal override void OnSetProperty()
		{
			//Discarded unreachable code: IL_002d
			base.OnSetProperty();
			while (true)
			{
				switch (0x147CD82F ^ 0x147CD82D)
				{
				case 0:
					continue;
				case 2:
					if (!base.initialized)
					{
						return;
					}
					break;
				}
				break;
			}
			cZNwcKOoWKuxnmVZWmbkUjmXOtE();
			GBExVtHFPRLrKdqZIHqEWcjtFsS();
		}

		internal override void OnClear()
		{
			//Discarded unreachable code: IL_007f
			base.OnClear();
			while (true)
			{
				int num = -271064583;
				while (true)
				{
					switch (num ^ -271064584)
					{
					case 0:
						break;
					case 4:
						_currentCenter = Vector2.zero;
						_previousTouchPosition = Vector2.zero;
						_axis2D.Clear();
						num = -271064577;
						continue;
					case 6:
						_pressValue = false;
						num = -271064582;
						continue;
					case 1:
						if (!base.initialized)
						{
							return;
						}
						goto case 5;
					case 7:
						_lastTapFrame = -1;
						num = -271064578;
						continue;
					case 3:
						yrfVZiGUTfOClpVIfIvNRqImIGGe = false;
						BSbeTqiFrdRDtCmjwklbhCLtNiqg = false;
						_pointerDownIsFake = false;
						num = -271064580;
						continue;
					case 5:
						_pointerId = int.MinValue;
						_realMousePointerId = int.MinValue;
						num = -271064581;
						continue;
					default:
						_isEligibleForTap = false;
						_isEligibleForPress = false;
						return;
					}
					break;
				}
			}
		}

		public override void ClearValue()
		{
			if (!base.initialized)
			{
				return;
			}
			while (true)
			{
				_axis2D.Clear();
				_lastTapFrame = -1;
				_pressValue = false;
				int num = -1974053437;
				while (true)
				{
					switch (num ^ -1974053437)
					{
					default:
						return;
					case 1:
						return;
					case 2:
						num = -1974053440;
						continue;
					case 3:
						break;
					case 0:
						if (hasController)
						{
							base.controller.ClearElementValue(_horizontalAxisCustomControllerElement);
							base.controller.ClearElementValue(_verticalAxisCustomControllerElement);
							base.controller.ClearElementValue(_tapCustomControllerElement);
							num = -1974053438;
							continue;
						}
						return;
					}
					break;
				}
			}
		}

		private void GBExVtHFPRLrKdqZIHqEWcjtFsS()
		{
			_horizontalAxisCustomControllerElement.ClearElementCaches();
			while (true)
			{
				int num = 696867685;
				while (true)
				{
					switch (num ^ 0x29895B64)
					{
					default:
						return;
					case 3:
						return;
					case 0:
						break;
					case 1:
						_verticalAxisCustomControllerElement.ClearElementCaches();
						_tapCustomControllerElement.ClearElementCaches();
						num = 696867686;
						continue;
					case 2:
						_pressCustomControllerElement.ClearElementCaches();
						num = 696867687;
						continue;
					}
					break;
				}
			}
		}

		private void cZNwcKOoWKuxnmVZWmbkUjmXOtE()
		{
			//Discarded unreachable code: IL_010d
			iSfJagPsCsVhUfDNyhNvbxLabufM(_axesToUse);
			if (!hasController)
			{
				return;
			}
			while (base.touchController.useCustomController)
			{
				while (true)
				{
					IL_00db:
					int num;
					if (_useXAxis)
					{
						base.controller.ValidateElements(_horizontalAxisCustomControllerElement);
						num = 501425712;
						goto IL_001d;
					}
					goto IL_00bf;
					IL_001d:
					while (true)
					{
						switch (num ^ 0x1DE32632)
						{
						default:
							return;
						case 9:
							return;
						case 3:
							num = 501425722;
							continue;
						case 7:
							break;
						case 1:
							base.controller.ValidateElements(_tapCustomControllerElement);
							num = 501425717;
							continue;
						case 5:
							base.controller.ValidateElements(_pressCustomControllerElement);
							num = 501425723;
							continue;
						case 4:
							base.controller.ValidateElements(_verticalAxisCustomControllerElement);
							num = 501425716;
							continue;
						case 2:
							goto IL_00bf;
						case 0:
							goto IL_00db;
						case 8:
							goto IL_00ff;
						case 6:
							goto IL_0117;
						}
						int num2;
						if (_allowPress)
						{
							num = 501425719;
							num2 = num;
						}
						else
						{
							num = 501425723;
							num2 = num;
						}
						continue;
						IL_0117:
						int num3;
						if (!_allowTap)
						{
							num = 501425717;
							num3 = num;
						}
						else
						{
							num = 501425715;
							num3 = num;
						}
					}
					IL_00bf:
					int num4;
					if (!_useYAxis)
					{
						num = 501425716;
						num4 = num;
					}
					else
					{
						num = 501425718;
						num4 = num;
					}
					goto IL_001d;
				}
				IL_00ff:;
			}
		}

		private void iSfJagPsCsVhUfDNyhNvbxLabufM(AxisDirection P_0)
		{
			bool flag = P_0 == AxisDirection.Both || P_0 == AxisDirection.Horizontal;
			bool flag2 = default(bool);
			while (true)
			{
				int num = -958030801;
				while (true)
				{
					switch (num ^ -958030805)
					{
					case 3:
						break;
					case 4:
					{
						int num3;
						if (_useXAxis != flag)
						{
							num = -958030802;
							num3 = num;
						}
						else
						{
							num = -958030807;
							num3 = num;
						}
						continue;
					}
					case 6:
					{
						int num2;
						if (_useYAxis != flag2)
						{
							num = -958030805;
							num2 = num;
						}
						else
						{
							num = -958030806;
							num2 = num;
						}
						continue;
					}
					case 2:
						flag2 = (P_0 == AxisDirection.Both || P_0 == AxisDirection.Vertical);
						num = -958030803;
						continue;
					case 7:
						if (!flag)
						{
							int num4;
							if (hasController)
							{
								num = -958030813;
								num4 = num;
							}
							else
							{
								num = -958030807;
								num4 = num;
							}
							continue;
						}
						goto case 2;
					case 0:
						_useYAxis = flag2;
						if (!flag2 && hasController)
						{
							base.controller.ClearElementValue(_verticalAxisCustomControllerElement);
							num = -958030806;
							continue;
						}
						goto default;
					case 5:
						_useXAxis = flag;
						num = -958030804;
						continue;
					case 8:
						base.controller.ClearElementValue(_horizontalAxisCustomControllerElement);
						num = -958030807;
						continue;
					default:
						_axesToUse = P_0;
						return;
					}
					break;
				}
			}
		}

		private void FBlOXIugJLwhCfGPQgASiZxYKxE()
		{
			//Discarded unreachable code: IL_0060, IL_0068
			if (!hasPointer)
			{
				goto IL_0008;
			}
			goto IL_007c;
			IL_007c:
			PointerEventData pointerEventData = default(PointerEventData);
			int num;
			if (!TouchInteractable.MUAEmHanKCAekGtjjVciojDFRqNV(effectivePointerId))
			{
				pointerEventData = lYycfThbLcNDrXoSoMNFZmblhiW(effectivePointerId);
				num = 662828451;
				goto IL_000d;
			}
			return;
			IL_0008:
			num = 662828454;
			goto IL_000d;
			IL_000d:
			while (true)
			{
				switch (num ^ 0x2781F5A0)
				{
				default:
					return;
				case 4:
					return;
				case 6:
					return;
				case 0:
					break;
				case 3:
					goto IL_0036;
				case 2:
					if (pointerEventData.pointerPress != null)
					{
						rNlfOZEoSimQmBIeJxNKvxgjMQsF(pointerEventData);
						return;
					}
					goto case 5;
				case 5:
					wAYSKMoYLXrHlKmKvvdfOiGBjye();
					num = 662828452;
					continue;
				case 1:
					goto IL_007c;
				}
				break;
				IL_0036:
				int num2;
				if (pointerEventData == null)
				{
					num = 662828453;
					num2 = num;
				}
				else
				{
					num = 662828450;
					num2 = num;
				}
			}
			goto IL_0008;
		}

		private void OkKgKKVlDZfBBjhCGQnuBwkwcWaI()
		{
			//Discarded unreachable code: IL_0163
			if (_touchPadMode == TouchPadMode.VectorFromCenter)
			{
				Graphic targetGraphic = base.targetGraphic;
				_currentCenter = ((targetGraphic != null) ? targetGraphic.transform.position : base.transform.position);
				_currentCenter = RectTransformUtility.WorldToScreenPoint(base.canvas.worldCamera, _currentCenter);
				goto IL_005b;
			}
			goto IL_0090;
			IL_0090:
			int num;
			if (hasPointer)
			{
				int num2;
				if (!TouchInteractable.MUAEmHanKCAekGtjjVciojDFRqNV(effectivePointerId))
				{
					num = 1959616950;
					num2 = num;
				}
				else
				{
					num = 1959616948;
					num2 = num;
				}
				goto IL_0060;
			}
			return;
			IL_005b:
			num = 1959616947;
			goto IL_0060;
			IL_0060:
			Vector3 v = default(Vector3);
			Vector2 vector = default(Vector2);
			while (true)
			{
				switch (num ^ 0x74CD61B7)
				{
				case 1:
					return;
				case 6:
					break;
				case 4:
					goto IL_0090;
				case 3:
					v = TouchInteractable.peFNVeWnRvpCYQMMXzPLXboQIi(effectivePointerId);
					if (_touchPadMode == TouchPadMode.ScreenPosition)
					{
						vector = v;
						num = 1959616944;
						continue;
					}
					goto case 2;
				case 0:
					vector = new Vector2(v.x - _currentCenter.x, v.y - _currentCenter.y);
					num = 1959616944;
					continue;
				case 7:
					vector = AuGsRIrcEYwDcqvvwEKaFTSdRpk(vector);
					_axis2D.SetRawValue(vector.x, vector.y);
					if (_touchPadMode == TouchPadMode.Delta)
					{
						_smoothDelta.HDNHWHMIgYVNwexHqBXaTGgQOqc(vector.x, vector.y);
						num = 1959616946;
						continue;
					}
					goto default;
				case 2:
					if (_touchPadMode == TouchPadMode.Delta)
					{
						_currentCenter = _previousTouchPosition;
						num = 1959616951;
						continue;
					}
					goto case 0;
				default:
					_previousTouchPosition = v;
					return;
				}
				break;
			}
			goto IL_005b;
		}

		private void ZoKkNpzYwYljsuAyefAuMwwVXPM()
		{
			//Discarded unreachable code: IL_0068, IL_00d4
			if (_touchPadMode == TouchPadMode.Delta)
			{
				if (!_useInertia)
				{
					goto IL_0013;
				}
				goto IL_005f;
			}
			return;
			IL_0013:
			int num = 615910145;
			goto IL_0018;
			IL_005f:
			if (hasPointer)
			{
				return;
			}
			goto IL_004c;
			IL_004c:
			Vector2 rawValue = _axis2D.rawValue;
			num = 615910149;
			goto IL_0018;
			IL_0018:
			float num3 = default(float);
			float smoothDeltaTime = default(float);
			float num2 = default(float);
			while (true)
			{
				switch (num ^ 0x24B60B02)
				{
				case 3:
					return;
				case 6:
					break;
				case 4:
					goto IL_004c;
				case 2:
					goto IL_005f;
				case 1:
					if (MathTools.IsNearZero(num3, 0.0001f))
					{
						num3 = 0f;
						num = 615910154;
						continue;
					}
					goto default;
				case 7:
					smoothDeltaTime = Time.smoothDeltaTime;
					num = 615910151;
					continue;
				case 0:
					num3 = Mathf.Lerp(rawValue.y, 0f, _inertiaFriction * smoothDeltaTime);
					if (MathTools.IsNearZero(num2, 0.0001f))
					{
						num2 = 0f;
						num = 615910147;
						continue;
					}
					goto case 1;
				case 5:
					num2 = Mathf.Lerp(rawValue.x, 0f, _inertiaFriction * smoothDeltaTime);
					num = 615910146;
					continue;
				default:
					_axis2D.SetRawValue(num2, num3);
					return;
				}
				break;
			}
			goto IL_0013;
		}

		private void UCAcUzyWwTaJkrollZKJQVQBPsx()
		{
			if (!hasPointer)
			{
				return;
			}
			while (true)
			{
				Vector2 vector = TouchInteractable.peFNVeWnRvpCYQMMXzPLXboQIi(effectivePointerId);
				int num = -1912977680;
				while (true)
				{
					switch (num ^ -1912977679)
					{
					case 0:
						num = -1912977678;
						continue;
					case 3:
						break;
					case 1:
						aefnyHMpGSqzRWAcJcrBYBatSpe(ref vector);
						num = -1912977677;
						continue;
					default:
						RrjGdBXnFrOytiuADHhXSrFmakd(ref vector);
						return;
					}
					break;
				}
			}
		}

		private void aefnyHMpGSqzRWAcJcrBYBatSpe(ref Vector2 P_0)
		{
			//Discarded unreachable code: IL_00b8
			if (_allowTap)
			{
				if (!_isEligibleForTap)
				{
					goto IL_0013;
				}
				goto IL_004e;
			}
			return;
			IL_0018:
			int num;
			while (true)
			{
				switch (num ^ 0x7FBEC9FC)
				{
				default:
					return;
				case 2:
					return;
				case 3:
					return;
				case 0:
					break;
				case 5:
					_isEligibleForTap = false;
					num = 2143209983;
					continue;
				case 1:
					goto IL_004e;
				case 4:
					goto IL_0080;
				}
				break;
			}
			goto IL_0013;
			IL_004e:
			if (_tapTimeout > 0f)
			{
				int num2;
				if (!(Time.realtimeSinceStartup - _touchStartTime > _tapTimeout))
				{
					num = 2143209976;
					num2 = num;
				}
				else
				{
					num = 2143209977;
					num2 = num;
				}
				goto IL_0018;
			}
			goto IL_0080;
			IL_0080:
			if (_tapDistanceLimit >= 0)
			{
				int num3;
				if (Vector2.Distance(_touchStartPosition, P_0) <= (float)_tapDistanceLimit)
				{
					num = 2143209983;
					num3 = num;
				}
				else
				{
					num = 2143209977;
					num3 = num;
				}
				goto IL_0018;
			}
			return;
			IL_0013:
			num = 2143209982;
			goto IL_0018;
		}

		private void RrjGdBXnFrOytiuADHhXSrFmakd(ref Vector2 P_0)
		{
			//Discarded unreachable code: IL_005a, IL_0099, IL_00dd
			if (!_allowPress)
			{
				return;
			}
			while (true)
			{
				int num = 495654669;
				while (true)
				{
					switch (num ^ 0x1D8B1708)
					{
					case 0:
						return;
					case 1:
						return;
					case 7:
						break;
					case 5:
					{
						int num3;
						if (_isEligibleForPress)
						{
							num = 495654666;
							num3 = num;
						}
						else
						{
							num = 495654664;
							num3 = num;
						}
						continue;
					}
					case 2:
						if (_pressDistanceLimit >= 0)
						{
							int num2;
							if (Vector2.Distance(_touchStartPosition, P_0) <= (float)_pressDistanceLimit)
							{
								num = 495654668;
								num2 = num;
							}
							else
							{
								num = 495654670;
								num2 = num;
							}
							continue;
						}
						goto case 4;
					case 6:
						_isEligibleForPress = false;
						cpliCGnFhUFiyKHRnjcFZAvzQWh(false);
						num = 495654665;
						continue;
					case 4:
						if (_pressStartDelay > 0f && Time.realtimeSinceStartup - _touchStartTime < _pressStartDelay)
						{
							return;
						}
						goto default;
					default:
						cpliCGnFhUFiyKHRnjcFZAvzQWh(true);
						return;
					}
					break;
				}
			}
		}

		private void IaovDVQjHWZnSxbEBXkayFEYapV()
		{
			//Discarded unreachable code: IL_00ba
			Vector2 value = default(Vector2);
			Vector2 valuePrev = default(Vector2);
			if (_touchPadMode == TouchPadMode.Delta)
			{
				value = _axis2D.value;
				valuePrev = _axis2D.valuePrev;
				goto IL_0020;
			}
			goto IL_006a;
			IL_0020:
			int num = 1755908117;
			goto IL_0025;
			IL_006a:
			Vector2 valueDelta = _axis2D.valueDelta;
			if (valueDelta.x == 0f)
			{
				int num2;
				if (valueDelta.y != 0f)
				{
					num = 1755908112;
					num2 = num;
				}
				else
				{
					num = 1755908116;
					num2 = num;
				}
				goto IL_0025;
			}
			goto IL_004d;
			IL_004d:
			_onValueChanged.Invoke(_axis2D.value);
			num = 1755908116;
			goto IL_0025;
			IL_0025:
			while (true)
			{
				switch (num ^ 0x68A90811)
				{
				default:
					return;
				case 5:
					return;
				case 2:
					break;
				case 1:
					goto IL_004d;
				case 0:
					goto IL_006a;
				case 3:
					_onValueChanged.Invoke(_axis2D.value);
					return;
				case 4:
					if (value.x != 0f || value.y != 0f || valuePrev.x != 0f)
					{
						goto case 3;
					}
					goto IL_00ee;
				}
				break;
				IL_00ee:
				int num3;
				if (valuePrev.y == 0f)
				{
					num = 1755908116;
					num3 = num;
				}
				else
				{
					num = 1755908114;
					num3 = num;
				}
			}
			goto IL_0020;
		}

		private Vector2 AuGsRIrcEYwDcqvvwEKaFTSdRpk(Vector2 P_0)
		{
			//Discarded unreachable code: IL_0091
			ValueFormat valueFormat = _valueFormat;
			float num2 = default(float);
			while (true)
			{
				int num = -2057706946;
				while (true)
				{
					switch (num ^ -2057706956)
					{
					case 0:
						break;
					case 9:
						P_0.x /= Screen.width;
						num = -2057706958;
						continue;
					case 7:
						goto IL_0063;
					case 2:
						P_0 = P_0 / num2 * 100f;
						num = -2057706948;
						continue;
					case 4:
						throw new NotImplementedException();
					case 8:
						num = -2057706953;
						continue;
					case 10:
						switch (valueFormat)
						{
						case ValueFormat.Screen:
							break;
						case ValueFormat.Direction:
							goto IL_0063;
						default:
							goto IL_00bb;
						case ValueFormat.Physical:
							goto IL_00ed;
						case ValueFormat.Pixels:
							goto IL_010e;
						}
						goto case 9;
					case 6:
						P_0.y /= Screen.height;
						num = -2057706953;
						continue;
					case 1:
						num = -2057706960;
						continue;
					case 5:
						goto IL_00ed;
					default:
						goto IL_010e;
						IL_010e:
						return P_0;
						IL_00ed:
						num2 = Screen.dpi;
						if (num2 < 10f)
						{
							num2 = 96f;
							num = -2057706954;
							continue;
						}
						goto case 2;
						IL_00bb:
						num = -2057706955;
						continue;
						IL_0063:
						P_0.Normalize();
						num = -2057706953;
						continue;
					}
					break;
				}
			}
		}

		private void cpliCGnFhUFiyKHRnjcFZAvzQWh(bool P_0)
		{
			//Discarded unreachable code: IL_000a, IL_000f, IL_0026, IL_003e
			if (P_0 != _pressValue)
			{
				_pressValue = P_0;
				if (P_0)
				{
					_onPressDown.Invoke();
				}
				else
				{
					_onPressUp.Invoke();
				}
			}
		}

		private void CLppEvNomWTxgFFClMmyWJkbdN(PointerEventData P_0)
		{
			if (hasPointer && !wzNIDgakHttMmiiBhPwkMJrPBdB(P_0.pointerId))
			{
				return;
			}
			while (agGGiexlYIVgemjaHHzzcCmJTcc())
			{
				int num;
				int num2;
				if (IsInteractable())
				{
					num = -1924114236;
					num2 = num;
				}
				else
				{
					num = -1924114235;
					num2 = num;
				}
				while (true)
				{
					switch (num ^ -1924114236)
					{
					case 2:
						num = -1924114233;
						continue;
					case 3:
						break;
					case 0:
						dkbPRumzMHESljvUzfPJVvaxLFt(P_0.pointerId, P_0.pressPosition);
						num = -1924114235;
						continue;
					default:
						goto end_IL_0039;
					}
					break;
				}
				continue;
				end_IL_0039:
				break;
			}
			base.OnPointerDown(P_0);
		}

		private void bsJnZKaMLeQiOiwSuuiDcemAIrg(PointerEventData P_0)
		{
			//Discarded unreachable code: IL_0017, IL_001c, IL_0033, IL_0043
			if ((!hasPointer || wzNIDgakHttMmiiBhPwkMJrPBdB(P_0.pointerId)) && !TouchInteractable.MUAEmHanKCAekGtjjVciojDFRqNV(effectivePointerId))
			{
				wAYSKMoYLXrHlKmKvvdfOiGBjye();
				base.OnPointerUp(P_0);
			}
		}

		private void jgmhCVCLNpdugCcpOmRLAZQAAUv(PointerEventData P_0)
		{
			if (hasPointer && !wzNIDgakHttMmiiBhPwkMJrPBdB(P_0.pointerId))
			{
				return;
			}
			int realMousePointerId = default(int);
			bool flag2 = default(bool);
			while (true)
			{
				bool flag = TouchInteractable.OenwdeSUZHKJsTIxPjYHJUNnctC(P_0.pointerId);
				int num = 682602211;
				while (true)
				{
					switch (num ^ 0x28AFAEE9)
					{
					case 4:
						num = 682602209;
						continue;
					case 7:
						_realMousePointerId = realMousePointerId;
						num = 682602218;
						continue;
					case 10:
						flag2 = false;
						if (_activateOnSwipeIn && agGGiexlYIVgemjaHHzzcCmJTcc() && IsInteractable() && (!flag || TouchInteractable.jWwDMVskgRDHPeFKiMdRILlfbbMg(base.allowedMouseButtons)))
						{
							int num3;
							if (yrfVZiGUTfOClpVIfIvNRqImIGGe)
							{
								num = 682602219;
								num3 = num;
							}
							else
							{
								num = 682602216;
								num3 = num;
							}
							continue;
						}
						goto case 2;
					case 5:
						_realMousePointerId = P_0.pointerId;
						num = 682602217;
						continue;
					case 0:
						flag2 = true;
						num = 682602219;
						continue;
					case 8:
						break;
					case 2:
						base.OnPointerEnter(P_0);
						if (flag2)
						{
							GameObject gameObject = base.gameObject;
							PointerEventData pointerEventData = KJdskTUpjJuElEhrvEVGouQMzgV((_realMousePointerId != int.MinValue) ? _realMousePointerId : P_0.pointerId, gameObject);
							if (pointerEventData != null)
							{
								CLppEvNomWTxgFFClMmyWJkbdN(pointerEventData);
								int num4;
								if (yrfVZiGUTfOClpVIfIvNRqImIGGe)
								{
									num = 682602208;
									num4 = num;
								}
								else
								{
									num = 682602223;
									num4 = num;
								}
								continue;
							}
						}
						goto default;
					case 3:
						num = 682602217;
						continue;
					case 9:
						_pointerDownIsFake = true;
						num = 682602223;
						continue;
					case 1:
						if (flag)
						{
							int num2;
							if (TouchInteractable.rXoHlhXIkgBrHzajLhaabUWFwEkO(base.allowedMouseButtons, out realMousePointerId))
							{
								num = 682602222;
								num2 = num;
							}
							else
							{
								num = 682602220;
								num2 = num;
							}
							continue;
						}
						goto case 0;
					default:
						BSbeTqiFrdRDtCmjwklbhCLtNiqg = true;
						return;
					}
					break;
				}
			}
		}

		private void LGSqRyvgmhSuGkKYGWZieVFROaZ(PointerEventData P_0)
		{
			//Discarded unreachable code: IL_0040
			if (hasPointer && !wzNIDgakHttMmiiBhPwkMJrPBdB(P_0.pointerId))
			{
				base.OnPointerExit(P_0);
				goto IL_001d;
			}
			goto IL_0047;
			IL_0064:
			base.OnPointerExit(P_0);
			BSbeTqiFrdRDtCmjwklbhCLtNiqg = false;
			return;
			IL_0022:
			int num;
			switch (num ^ 0x3DE61113)
			{
			case 3:
				return;
			case 0:
				break;
			case 2:
				goto IL_0047;
			default:
				goto IL_0064;
			}
			goto IL_001d;
			IL_0047:
			if (!stayActiveOnSwipeOut && yrfVZiGUTfOClpVIfIvNRqImIGGe)
			{
				wAYSKMoYLXrHlKmKvvdfOiGBjye();
				num = 1038487826;
				goto IL_0022;
			}
			goto IL_0064;
			IL_001d:
			num = 1038487824;
			goto IL_0022;
		}

		private void dkbPRumzMHESljvUzfPJVvaxLFt(int P_0, Vector2 P_1)
		{
			_pointerId = P_0;
			yrfVZiGUTfOClpVIfIvNRqImIGGe = true;
			_isEligibleForTap = true;
			_isEligibleForPress = true;
			if (_touchPadMode != TouchPadMode.VectorFromCenter)
			{
				_currentCenter = P_1;
				goto IL_0031;
			}
			goto IL_0053;
			IL_0036:
			int num;
			switch (num ^ -861530189)
			{
			default:
				return;
			case 2:
				return;
			case 3:
				break;
			case 1:
				goto IL_0053;
			case 0:
				goto IL_0069;
			}
			goto IL_0031;
			IL_0053:
			if (_touchPadMode == TouchPadMode.Delta)
			{
				_previousTouchPosition = P_1;
				num = -861530189;
				goto IL_0036;
			}
			goto IL_0069;
			IL_0031:
			num = -861530190;
			goto IL_0036;
			IL_0069:
			_touchStartTime = Time.realtimeSinceStartup;
			_touchStartPosition = P_1;
			num = -861530191;
			goto IL_0036;
		}

		private void wAYSKMoYLXrHlKmKvvdfOiGBjye()
		{
			bool flag = _allowTap && _isEligibleForTap;
			BgAdqUvZFqBlqudhjyphLkDsMtm();
			yrfVZiGUTfOClpVIfIvNRqImIGGe = false;
			if (_useInertia && _touchPadMode == TouchPadMode.Delta)
			{
				goto IL_0035;
			}
			goto IL_00ca;
			IL_00ca:
			_axis2D.SetRawValue(0f, 0f);
			int num = 1869632015;
			goto IL_003a;
			IL_003a:
			while (true)
			{
				switch (num ^ 0x6F70520A)
				{
				default:
					return;
				case 2:
					return;
				case 0:
					break;
				case 5:
					goto IL_0062;
				case 1:
					_lastTapFrame = Time.frameCount + 1;
					_onTap.Invoke();
					num = 1869632008;
					continue;
				case 3:
					_axis2D.SetRawValue(_smoothDelta.HQcOXZEZNJYXxzDNEfseqcouOPL());
					num = 1869632015;
					continue;
				case 4:
					goto IL_00ca;
				}
				break;
				IL_0062:
				cpliCGnFhUFiyKHRnjcFZAvzQWh(false);
				_isEligibleForTap = false;
				_isEligibleForPress = false;
				int num2;
				if (!flag)
				{
					num = 1869632008;
					num2 = num;
				}
				else
				{
					num = 1869632011;
					num2 = num;
				}
			}
			goto IL_0035;
			IL_0035:
			num = 1869632009;
			goto IL_003a;
		}

		internal override void OnPointerUp(PointerEventData eventData)
		{
			//Discarded unreachable code: IL_0009, IL_000e, IL_0025, IL_003c
			if (base.initialized && TouchInteractable.gtAdYPaYTSdxlqopRartpbSncsib(eventData.pointerId, base.allowedMouseButtons, EventTriggerType.PointerUp))
			{
				bsJnZKaMLeQiOiwSuuiDcemAIrg(eventData);
			}
		}

		internal override void OnPointerDown(PointerEventData eventData)
		{
			//Discarded unreachable code: IL_002f, IL_005c
			if (!base.initialized)
			{
				goto IL_0008;
			}
			goto IL_0036;
			IL_0036:
			int num;
			int num2;
			if (TouchInteractable.gtAdYPaYTSdxlqopRartpbSncsib(eventData.pointerId, base.allowedMouseButtons, EventTriggerType.PointerDown))
			{
				num = -1897737363;
				num2 = num;
			}
			else
			{
				num = -1897737367;
				num2 = num;
			}
			goto IL_000d;
			IL_000d:
			switch (num ^ -1897737363)
			{
			case 1:
				return;
			case 4:
				return;
			case 3:
				break;
			case 2:
				goto IL_0036;
			default:
				CLppEvNomWTxgFFClMmyWJkbdN(eventData);
				return;
			}
			goto IL_0008;
			IL_0008:
			num = -1897737364;
			goto IL_000d;
		}

		internal override void OnPointerEnter(PointerEventData eventData)
		{
			//Discarded unreachable code: IL_0009, IL_000e, IL_0025, IL_003c
			if (base.initialized && TouchInteractable.gtAdYPaYTSdxlqopRartpbSncsib(eventData.pointerId, base.allowedMouseButtons, EventTriggerType.PointerEnter))
			{
				jgmhCVCLNpdugCcpOmRLAZQAAUv(eventData);
			}
		}

		internal override void OnPointerExit(PointerEventData eventData)
		{
			//Discarded unreachable code: IL_0040
			if (!base.initialized)
			{
				return;
			}
			while (TouchInteractable.gtAdYPaYTSdxlqopRartpbSncsib(eventData.pointerId, base.allowedMouseButtons, EventTriggerType.PointerExit))
			{
				while (true)
				{
					IL_0047:
					LGSqRyvgmhSuGkKYGWZieVFROaZ(eventData);
					int num = -1338136780;
					while (true)
					{
						switch (num ^ -1338136780)
						{
						default:
							return;
						case 0:
							return;
						case 3:
							num = -1338136779;
							continue;
						case 1:
							break;
						case 2:
							goto IL_0047;
						}
						break;
					}
					break;
				}
			}
		}

		private void BgAdqUvZFqBlqudhjyphLkDsMtm()
		{
			_pointerId = int.MinValue;
			_realMousePointerId = int.MinValue;
		}

		private bool wzNIDgakHttMmiiBhPwkMJrPBdB(int P_0)
		{
			if (P_0 == int.MinValue)
			{
				return false;
			}
			if (_pointerId == int.MinValue)
			{
				return false;
			}
			if (_pointerId == P_0)
			{
				return true;
			}
			if (TouchInteractable.OenwdeSUZHKJsTIxPjYHJUNnctC(P_0) && _realMousePointerId != int.MinValue && P_0 == _realMousePointerId)
			{
				return true;
			}
			return false;
		}

		private PointerEventData KJdskTUpjJuElEhrvEVGouQMzgV(int P_0, GameObject P_1)
		{
			//Discarded unreachable code: IL_00bd, IL_02b8
			PointerEventData pointerEventData = lYycfThbLcNDrXoSoMNFZmblhiW(P_0);
			if (pointerEventData == null)
			{
				return null;
			}
			pointerEventData.position = TouchInteractable.peFNVeWnRvpCYQMMXzPLXboQIi(P_0);
			if (TouchInteractable.BjWUubnUnTBxMVzCyvrMCwQMuce(P_0))
			{
				pointerEventData.eligibleForClick = true;
				pointerEventData.delta = Vector2.zero;
				pointerEventData.dragging = false;
				goto IL_0044;
			}
			goto IL_00c4;
			IL_0044:
			int num = 2142207828;
			goto IL_0049;
			IL_0049:
			float unscaledTime = default(float);
			float num2 = default(float);
			GameObject gameObject2 = default(GameObject);
			float unscaledTime2 = default(float);
			GameObject gameObject = default(GameObject);
			while (true)
			{
				switch (num ^ 0x7FAF7F47)
				{
				case 15:
					break;
				case 16:
					pointerEventData.pointerDrag = P_1;
					goto IL_02f2;
				case 13:
					goto IL_00c4;
				case 8:
					pointerEventData.clickCount = 1;
					num = 2142207808;
					continue;
				case 5:
					pointerEventData.pointerPressRaycast = pointerEventData.pointerCurrentRaycast;
					if (pointerEventData.pointerEnter != P_1)
					{
						pointerEventData.pointerEnter = P_1;
						num = 2142207815;
						continue;
					}
					goto case 0;
				case 4:
					pointerEventData.clickTime = unscaledTime;
					num = 2142207822;
					continue;
				case 3:
					if (num2 < 0.3f)
					{
						pointerEventData.clickCount++;
						num = 2142207811;
						continue;
					}
					goto case 2;
				case 2:
					pointerEventData.clickCount = 1;
					num = 2142207811;
					continue;
				case 21:
					pointerEventData.clickCount = 1;
					num = 2142207822;
					continue;
				case 10:
					goto IL_017f;
				case 7:
					pointerEventData.pointerPress = gameObject2;
					pointerEventData.rawPointerPress = P_1;
					num = 2142207819;
					continue;
				case 6:
				{
					float num3 = unscaledTime2 - pointerEventData.clickTime;
					if (num3 < 0.3f)
					{
						pointerEventData.clickCount++;
						num = 2142207814;
						continue;
					}
					goto case 20;
				}
				case 11:
					pointerEventData.dragging = false;
					pointerEventData.useDragThreshold = true;
					pointerEventData.pressPosition = pointerEventData.position;
					pointerEventData.pointerPressRaycast = pointerEventData.pointerCurrentRaycast;
					gameObject = P_1;
					unscaledTime = Time.unscaledTime;
					if (gameObject == pointerEventData.lastPress)
					{
						num2 = unscaledTime - pointerEventData.clickTime;
						num = 2142207812;
						continue;
					}
					goto case 21;
				case 0:
					gameObject2 = P_1;
					num = 2142207821;
					continue;
				case 20:
					pointerEventData.clickCount = 1;
					num = 2142207814;
					continue;
				case 14:
					pointerEventData.pressPosition = pointerEventData.position;
					num = 2142207810;
					continue;
				case 19:
					pointerEventData.useDragThreshold = true;
					num = 2142207817;
					continue;
				case 12:
					pointerEventData.clickTime = unscaledTime2;
					num = 2142207831;
					continue;
				case 1:
					pointerEventData.clickTime = unscaledTime2;
					num = 2142207808;
					continue;
				case 17:
					pointerEventData.clickTime = unscaledTime;
					pointerEventData.pointerDrag = P_1;
					goto IL_02f2;
				case 9:
					pointerEventData.pointerPress = gameObject;
					pointerEventData.rawPointerPress = P_1;
					num = 2142207830;
					continue;
				default:
					goto IL_02db;
					IL_02f2:
					return pointerEventData;
				}
				break;
				IL_017f:
				unscaledTime2 = Time.unscaledTime;
				int num4;
				if (!(gameObject2 == pointerEventData.lastPress))
				{
					num = 2142207823;
					num4 = num;
				}
				else
				{
					num = 2142207809;
					num4 = num;
				}
			}
			goto IL_0044;
			IL_02db:
			Logger.LogWarning("Unsupported pointerId: " + P_0);
			return null;
			IL_00c4:
			if (TouchInteractable.OenwdeSUZHKJsTIxPjYHJUNnctC(P_0))
			{
				pointerEventData.eligibleForClick = true;
				pointerEventData.delta = Vector2.zero;
				num = 2142207820;
				goto IL_0049;
			}
			goto IL_02db;
		}

		private PointerEventData XXngLZbgZgohzdkkHlAueNnBMbsM(int P_0, GameObject P_1)
		{
			PointerEventData pointerEventData = lYycfThbLcNDrXoSoMNFZmblhiW(P_0);
			if (pointerEventData == null)
			{
				return null;
			}
			Vector2 vector = TouchInteractable.peFNVeWnRvpCYQMMXzPLXboQIi(P_0);
			pointerEventData.delta = vector - pointerEventData.position;
			pointerEventData.position = vector;
			pointerEventData.dragging = true;
			pointerEventData.pointerDrag = P_1;
			pointerEventData.useDragThreshold = true;
			pointerEventData.pointerPress = null;
			pointerEventData.rawPointerPress = null;
			return pointerEventData;
		}

		private PointerEventData VSPCPJdlAOfJpTUOqerfRqxddALm(int P_0)
		{
			//Discarded unreachable code: IL_009d, IL_00ad
			PointerEventData pointerEventData = lYycfThbLcNDrXoSoMNFZmblhiW(P_0);
			if (pointerEventData == null)
			{
				return null;
			}
			if (TouchInteractable.BjWUubnUnTBxMVzCyvrMCwQMuce(P_0))
			{
				pointerEventData.eligibleForClick = false;
				pointerEventData.pointerPress = null;
				pointerEventData.rawPointerPress = null;
				pointerEventData.dragging = false;
				goto IL_0031;
			}
			goto IL_0069;
			IL_00b4:
			Logger.LogWarning("Unsupported pointerId: " + P_0);
			return null;
			IL_0036:
			int num;
			while (true)
			{
				switch (num ^ 0x6D01524)
				{
				case 5:
					break;
				case 1:
					pointerEventData.pointerDrag = null;
					num = 114300196;
					continue;
				case 4:
					goto IL_0069;
				case 3:
					pointerEventData.rawPointerPress = null;
					pointerEventData.dragging = false;
					pointerEventData.pointerDrag = null;
					goto IL_00cb;
				case 0:
					pointerEventData.pointerEnter = null;
					goto IL_00cb;
				default:
					goto IL_00b4;
					IL_00cb:
					return pointerEventData;
				}
				break;
			}
			goto IL_0031;
			IL_0031:
			num = 114300197;
			goto IL_0036;
			IL_0069:
			if (TouchInteractable.OenwdeSUZHKJsTIxPjYHJUNnctC(P_0))
			{
				pointerEventData.eligibleForClick = false;
				pointerEventData.pointerPress = null;
				num = 114300199;
				goto IL_0036;
			}
			goto IL_00b4;
		}

		private void rNlfOZEoSimQmBIeJxNKvxgjMQsF(PointerEventData P_0)
		{
			if (P_0 != null)
			{
				OnPointerUp(P_0);
				VSPCPJdlAOfJpTUOqerfRqxddALm(effectivePointerId);
			}
		}

		private PointerEventData lYycfThbLcNDrXoSoMNFZmblhiW(int P_0)
		{
			//Discarded unreachable code: IL_0092
			if (P_0 == int.MinValue)
			{
				goto IL_0008;
			}
			int num;
			if (__fakePointerEventData == null)
			{
				__fakePointerEventData = new Dictionary<int, PointerEventData>();
				num = 2074141414;
				goto IL_000d;
			}
			goto IL_00de;
			IL_000d:
			int num2 = default(int);
			PointerEventData value = default(PointerEventData);
			PointerEventData.InputButton button = default(PointerEventData.InputButton);
			while (true)
			{
				switch (num ^ 0x7BA0E2ED)
				{
				case 2:
					break;
				case 5:
					return null;
				case 10:
					switch (num2)
					{
					case -2:
						goto IL_00ad;
					case -3:
						goto IL_0103;
					case -1:
						goto IL_0126;
					}
					num = 2074141417;
					continue;
				case 4:
					throw new NotImplementedException();
				case 9:
					value.button = button;
					num = 2074141421;
					continue;
				case 6:
					goto IL_00ad;
				case 12:
					value.pointerId = P_0;
					num = 2074141413;
					continue;
				case 1:
					if (TouchInteractable.OenwdeSUZHKJsTIxPjYHJUNnctC(P_0))
					{
						num2 = P_0;
						num = 2074141415;
						continue;
					}
					goto IL_0132;
				case 11:
					goto IL_00de;
				case 7:
					goto IL_0103;
				case 8:
					__fakePointerEventData.Add(P_0, value);
					num = 2074141420;
					continue;
				case 3:
					goto IL_0126;
				default:
					goto IL_0132;
					IL_0126:
					button = PointerEventData.InputButton.Left;
					num = 2074141412;
					continue;
					IL_0103:
					button = PointerEventData.InputButton.Middle;
					num = 2074141412;
					continue;
					IL_00ad:
					button = PointerEventData.InputButton.Right;
					num = 2074141412;
					continue;
				}
				break;
			}
			goto IL_0008;
			IL_0008:
			num = 2074141416;
			goto IL_000d;
			IL_00de:
			if (!__fakePointerEventData.TryGetValue(P_0, out value))
			{
				value = new PointerEventData(EventSystem.current);
				num = 2074141409;
				goto IL_000d;
			}
			goto IL_0132;
			IL_0132:
			return value;
		}
	}
}
