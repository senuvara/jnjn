using Rewired.Utils;
using Rewired.Utils.Interfaces;
using System;
using System.Collections;
using System.Collections.Generic;
using UnityEngine;

namespace Rewired.ComponentControls
{
	[Serializable]
	[DisallowMultipleComponent]
	public abstract class ComponentController : MonoBehaviour, IRegistrar<IComponentControl>, IComponentController
	{
		[NonSerialized]
		private bool IoRFdMQoQlRiOSEMjmYGiNSOeMq;

		[NonSerialized]
		private bool xrnCmAVSQONYdzJrDeARfwrcuFg;

		private List<IComponentControl> _controls = new List<IComponentControl>(10);

		internal bool initialized => IoRFdMQoQlRiOSEMjmYGiNSOeMq;

		[CustomObfuscation(rename = false)]
		internal ComponentController()
		{
		}

		[CustomObfuscation(rename = false)]
		internal virtual void Awake()
		{
			xrnCmAVSQONYdzJrDeARfwrcuFg = true;
		}

		[CustomObfuscation(rename = false)]
		internal virtual void Update()
		{
			if (!IoRFdMQoQlRiOSEMjmYGiNSOeMq)
			{
				return;
			}
			IComponentControl componentControl = default(IComponentControl);
			while (true)
			{
				int num = _controls.Count - 1;
				int num2 = -1817090432;
				while (true)
				{
					switch (num2 ^ -1817090427)
					{
					case 4:
						num2 = -1817090425;
						continue;
					case 0:
						num--;
						num2 = -1817090432;
						continue;
					case 3:
						componentControl = _controls[num];
						num2 = -1817090428;
						continue;
					case 1:
						if (componentControl.IsNullOrDestroyed())
						{
							_controls.RemoveAt(num);
							num2 = -1817090427;
							continue;
						}
						goto case 6;
					case 6:
						componentControl.Update();
						num2 = -1817090427;
						continue;
					case 2:
						break;
					default:
						if (num < 0)
						{
							return;
						}
						goto case 3;
					}
					break;
				}
			}
		}

		[CustomObfuscation(rename = false)]
		internal virtual void OnEnable()
		{
			//Discarded unreachable code: IL_003f
			if (!xrnCmAVSQONYdzJrDeARfwrcuFg)
			{
				goto IL_0008;
			}
			goto IL_0046;
			IL_0046:
			GyMEixNRvJvwisdpRohOwzNsGfY();
			int num = 2142679412;
			goto IL_000d;
			IL_000d:
			switch (num ^ 0x7FB6B176)
			{
			default:
				return;
			case 2:
				return;
			case 0:
				break;
			case 1:
				StartCoroutine(NnUmFTQyKzkFELnlFfZffwckdHf());
				xrnCmAVSQONYdzJrDeARfwrcuFg = true;
				return;
			case 3:
				goto IL_0046;
			}
			goto IL_0008;
			IL_0008:
			num = 2142679415;
			goto IL_000d;
		}

		[CustomObfuscation(rename = false)]
		internal virtual void OnDisable()
		{
			if (IoRFdMQoQlRiOSEMjmYGiNSOeMq)
			{
				OnUnsubscribeEvents();
			}
		}

		[CustomObfuscation(rename = false)]
		internal virtual void OnValidate()
		{
			if (IoRFdMQoQlRiOSEMjmYGiNSOeMq)
			{
				bFoWQuDzcOvecUqeSCdTCzgfmRIE();
			}
		}

		[CustomObfuscation(rename = false)]
		internal virtual void OnDestroy()
		{
			_controls.Clear();
		}

		internal virtual bool OnInitialize()
		{
			return true;
		}

		internal virtual void OnSubscribeEvents()
		{
			OnUnsubscribeEvents();
		}

		internal virtual void OnUnsubscribeEvents()
		{
		}

		void IRegistrar<IComponentControl>.Register(IComponentControl P_0)
		{
			if (!P_0.IsNullOrDestroyed())
			{
				ListTools.AddIfUnique(_controls, P_0);
			}
		}

		void IRegistrar<IComponentControl>.Deregister(IComponentControl P_0)
		{
			//Discarded unreachable code: IL_0027
			if (P_0.IsNullOrDestroyed())
			{
				while (true)
				{
					switch (0x2D6B115F ^ 0x2D6B115D)
					{
					case 2:
						return;
					case 0:
						continue;
					}
					break;
				}
			}
			_controls.Remove(P_0);
		}

		public virtual void ClearControlValues()
		{
			if (!IoRFdMQoQlRiOSEMjmYGiNSOeMq)
			{
				return;
			}
			while (true)
			{
				int num = _controls.Count - 1;
				int num2 = 964650398;
				while (true)
				{
					switch (num2 ^ 0x397F659A)
					{
					case 2:
						num2 = 964650395;
						continue;
					case 1:
						break;
					case 3:
						_controls[num].ClearValue();
						num2 = 964650399;
						continue;
					case 5:
						num--;
						num2 = 964650398;
						continue;
					case 0:
						if (_controls[num].IsNullOrDestroyed())
						{
							_controls.RemoveAt(num);
							num2 = 964650399;
							continue;
						}
						goto case 3;
					default:
						if (num < 0)
						{
							return;
						}
						goto case 0;
					}
					break;
				}
			}
		}

		private void GyMEixNRvJvwisdpRohOwzNsGfY()
		{
			//Discarded unreachable code: IL_0027
			if (!OnInitialize())
			{
				while (true)
				{
					switch (-535682942 ^ -535682941)
					{
					case 1:
						return;
					case 2:
						continue;
					}
					break;
				}
			}
			IoRFdMQoQlRiOSEMjmYGiNSOeMq = true;
			OnSubscribeEvents();
		}

		private void bFoWQuDzcOvecUqeSCdTCzgfmRIE()
		{
			_ = initialized;
		}

		private IEnumerator NnUmFTQyKzkFELnlFfZffwckdHf()
		{
			while (true)
			{
				int num = 113567740;
				while (true)
				{
					switch (num ^ 0x6C4E7FD)
					{
					default:
						yield break;
					case 3:
						break;
					case 1:
					{
						int num2;
						switch (num2)
						{
						default:
							num = 113567741;
							continue;
						case 0:
							break;
						case 1:
							GyMEixNRvJvwisdpRohOwzNsGfY();
							num = 113567741;
							continue;
						}
						goto case 2;
					}
					case 2:
						yield return null;
						break;
					}
					break;
				}
			}
		}
	}
}
