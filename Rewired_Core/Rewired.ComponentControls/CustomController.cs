using Rewired.ComponentControls.Data;
using Rewired.Data;
using Rewired.Utils;
using System;
using System.Collections.Generic;
using UnityEngine;

namespace Rewired.ComponentControls
{
	[Serializable]
	[CustomClassObfuscation(renamePrivateMembers = true, renamePubIntMembers = false)]
	[DisallowMultipleComponent]
	public class CustomController : ComponentController
	{
		[Serializable]
		public class CreateCustomControllerSettings
		{
			[CustomObfuscation(rename = false)]
			[Tooltip("If true, a new Custom Controller will be created. Otherwise, an existing Custom Controller will be found using the selector properties.")]
			[SerializeField]
			private bool _createCustomController = true;

			[CustomObfuscation(rename = false)]
			[SerializeField]
			[Tooltip("The source id of the Custom Controller to create. Get this from the Rewired Input Manager.")]
			private int _customControllerSourceId = -1;

			[Tooltip("The Player that will be assigned this Custom Controller when it is created.")]
			[SerializeField]
			[CustomObfuscation(rename = false)]
			private int _assignToPlayerId;

			[Tooltip("If true, the Custom Controller created by this component will be destroyed when this component is destroyed.")]
			[SerializeField]
			[CustomObfuscation(rename = false)]
			private bool _destroyCustomController = true;

			public bool createCustomController
			{
				get
				{
					return _createCustomController;
				}
				set
				{
					if (_createCustomController != value)
					{
						_createCustomController = value;
					}
				}
			}

			public int customControllerSourceId
			{
				get
				{
					return _customControllerSourceId;
				}
				set
				{
					_customControllerSourceId = value;
				}
			}

			public int assignToPlayerId
			{
				get
				{
					return _assignToPlayerId;
				}
				set
				{
					_assignToPlayerId = value;
				}
			}

			public bool destroyCustomController
			{
				get
				{
					return _destroyCustomController;
				}
				set
				{
					_destroyCustomController = value;
				}
			}
		}

		private struct InputEvent
		{
			public CustomControllerElementSelector.ElementType elementType;

			public int elementIndex;

			public float value;

			public InputEvent(CustomControllerElementSelector.ElementType elementType, int elementIndex, float value)
			{
				this.elementType = elementType;
				this.elementIndex = elementIndex;
				this.value = value;
			}

			public InputEvent(CustomControllerElementSelector.ElementType elementType, int elementIndex, bool value)
			{
				this.elementType = elementType;
				this.elementIndex = elementIndex;
				this.value = (value ? 1f : 0f);
			}

			public bool TargetMatches(CustomControllerElementSelector.ElementType elementType, int elementIndex)
			{
				if (this.elementType == elementType)
				{
					return this.elementIndex == elementIndex;
				}
				return false;
			}

			public void Merge(float value)
			{
				this.value = MathTools.MaxMagnitude(this.value, value);
			}

			public void Merge(bool value)
			{
				if (!value)
				{
					return;
				}
				while (true)
				{
					int num = 1646020304;
					while (true)
					{
						switch (num ^ 0x621C46D1)
						{
						default:
							return;
						case 2:
							return;
						case 0:
							break;
						case 1:
							goto IL_0021;
						}
						break;
						IL_0021:
						this.value = 1f;
						num = 1646020307;
					}
				}
			}
		}

		[CustomObfuscation(rename = false)]
		[SerializeField]
		[Tooltip("(Optional) Link the Rewired Input Manager here for easier access to Custom Controller elements, etc.")]
		private InputManager_Base _rewiredInputManager;

		[SerializeField]
		[CustomObfuscation(rename = false)]
		[Tooltip("Contains search parameters to find a particular Custom Controller.")]
		private CustomControllerSelector _customControllerSelector = new CustomControllerSelector();

		[CustomObfuscation(rename = false)]
		[Tooltip("Settings for creating a Custom Controller on start.")]
		[SerializeField]
		private CreateCustomControllerSettings _createCustomControllerSettings = new CreateCustomControllerSettings();

		private List<InputEvent> _inputEvents = new List<InputEvent>(10);

		[NonSerialized]
		private int _createdCustomControllerId = -1;

		private Action _InputSourceUpdateEvent;

		public InputManager_Base rewiredInputManager
		{
			get
			{
				return _rewiredInputManager;
			}
			set
			{
				//Discarded unreachable code: IL_0031
				if (_rewiredInputManager == value)
				{
					goto IL_000e;
				}
				goto IL_0038;
				IL_0038:
				_rewiredInputManager = value;
				int num = 520377521;
				goto IL_0013;
				IL_0013:
				switch (num ^ 0x1F0454B1)
				{
				case 1:
					return;
				case 3:
					break;
				case 2:
					goto IL_0038;
				default:
					OnSetProperty();
					return;
				}
				goto IL_000e;
				IL_000e:
				num = 520377520;
				goto IL_0013;
			}
		}

		public CustomControllerSelector customControllerSelector => _customControllerSelector;

		public CreateCustomControllerSettings createCustomControllerSettings => _createCustomControllerSettings;

		internal event Action InputSourceUpdateEvent
		{
			add
			{
				_InputSourceUpdateEvent = (Action)Delegate.Combine(_InputSourceUpdateEvent, value);
			}
			remove
			{
				_InputSourceUpdateEvent = (Action)Delegate.Remove(_InputSourceUpdateEvent, value);
			}
		}

		[CustomObfuscation(rename = false)]
		internal CustomController()
		{
		}

		public Rewired.CustomController GetCustomController()
		{
			return GetCustomController(warn: false);
		}

		[CustomObfuscation(rename = false)]
		internal override void OnEnable()
		{
			base.OnEnable();
			_ = base.initialized;
		}

		[CustomObfuscation(rename = false)]
		internal override void OnDisable()
		{
			base.OnDisable();
			if (base.initialized)
			{
				_inputEvents.Clear();
			}
		}

		[CustomObfuscation(rename = false)]
		internal override void OnValidate()
		{
			//Discarded unreachable code: IL_002d
			base.OnValidate();
			if (!base.initialized)
			{
				while (true)
				{
					switch (0x1E833DD2 ^ 0x1E833DD3)
					{
					case 1:
						return;
					case 2:
						continue;
					}
					break;
				}
			}
			OnSetProperty();
		}

		[CustomObfuscation(rename = false)]
		internal override void OnDestroy()
		{
			base.OnDestroy();
			TryDestroyCustomController();
		}

		internal override bool OnInitialize()
		{
			if (!base.OnInitialize())
			{
				return false;
			}
			if (GetUseCustomController())
			{
				if (!CheckIsRewiredReady())
				{
					return false;
				}
				if (GetCustomController(warn: true) == null)
				{
					while (true)
					{
						int num = 1543007110;
						while (true)
						{
							switch (num ^ 0x5BF86B87)
							{
							case 0:
								break;
							case 1:
								SetUseCustomController(value: false);
								num = 1543007109;
								continue;
							default:
								goto IL_0051;
							}
							break;
						}
					}
				}
			}
			goto IL_0051;
			IL_0051:
			return true;
		}

		internal override void OnSubscribeEvents()
		{
			//Discarded unreachable code: IL_0032
			base.OnSubscribeEvents();
			OnUnsubscribeEvents();
			while (true)
			{
				switch (0x298B86C4 ^ 0x298B86C5)
				{
				case 0:
					continue;
				case 1:
					if (!ReInput.isReady)
					{
						return;
					}
					break;
				}
				break;
			}
			ReInput.InputSourceUpdateEvent += OnInputSourceUpdate;
		}

		internal override void OnUnsubscribeEvents()
		{
			base.OnUnsubscribeEvents();
			ReInput.InputSourceUpdateEvent -= OnInputSourceUpdate;
		}

		public override void ClearControlValues()
		{
			base.ClearControlValues();
			if (!base.initialized)
			{
				return;
			}
			while (true)
			{
				_inputEvents.Clear();
				int num = -871356116;
				while (true)
				{
					switch (num ^ -871356115)
					{
					default:
						return;
					case 1:
						return;
					case 0:
						goto IL_000f;
					case 2:
						break;
					}
					break;
					IL_000f:
					num = -871356113;
				}
			}
		}

		[CustomObfuscation(rename = false)]
		internal virtual bool GetUseCustomController()
		{
			return true;
		}

		[CustomObfuscation(rename = false)]
		internal virtual void SetUseCustomController(bool value)
		{
		}

		internal void SetAxisValue(CustomControllerElementSelector element, float value)
		{
			//Discarded unreachable code: IL_0052, IL_0061, IL_006f, IL_00a1, IL_00f0
			if (!base.initialized)
			{
				return;
			}
			int elementIndex = default(int);
			int num2 = default(int);
			while (element != null)
			{
				while (true)
				{
					IL_0098:
					if (!GetUseCustomController())
					{
						return;
					}
					while (true)
					{
						IL_00ab:
						Rewired.CustomController customController = GetCustomController(warn: false);
						int num = 547997482;
						while (true)
						{
							switch (num ^ 0x20A9C72D)
							{
							case 5:
								num = 547997487;
								continue;
							case 2:
								break;
							case 1:
								if (elementIndex < 0)
								{
									return;
								}
								goto case 10;
							case 7:
								if (customController == null)
								{
									return;
								}
								goto case 0;
							case 11:
								goto IL_0076;
							case 3:
								goto IL_0098;
							case 4:
								goto IL_00ab;
							case 9:
							{
								InputEvent value2 = _inputEvents[num2];
								if (value2.TargetMatches(element.elementType, elementIndex))
								{
									value2.Merge(value);
									_inputEvents[num2] = value2;
									return;
								}
								goto case 8;
							}
							case 10:
								_ = _inputEvents.Count;
								num2 = 0;
								num = 547997478;
								continue;
							case 0:
								elementIndex = element.GetElementIndex(customController);
								num = 547997484;
								continue;
							case 8:
								num2++;
								num = 547997478;
								continue;
							default:
								_inputEvents.Add(new InputEvent(element.elementType, elementIndex, value));
								return;
							}
							break;
							IL_0076:
							int num3;
							if (num2 >= _inputEvents.Count)
							{
								num = 547997483;
								num3 = num;
							}
							else
							{
								num = 547997476;
								num3 = num;
							}
						}
						break;
					}
					break;
				}
			}
		}

		internal void SetButtonValue(CustomControllerElementSelector element, bool value)
		{
			//Discarded unreachable code: IL_0081, IL_00a3, IL_00d3, IL_012a, IL_0169
			if (!base.initialized)
			{
				return;
			}
			int num3 = default(int);
			int elementIndex = default(int);
			Rewired.CustomController customController = default(Rewired.CustomController);
			InputEvent value2 = default(InputEvent);
			while (true)
			{
				int num;
				int num2;
				if (element != null)
				{
					num = 1583450718;
					num2 = num;
				}
				else
				{
					num = 1583450706;
					num2 = num;
				}
				while (true)
				{
					switch (num ^ 0x5E618A53)
					{
					case 1:
						return;
					case 12:
						return;
					case 5:
						num = 1583450712;
						continue;
					case 14:
					{
						int num5;
						if (num3 < _inputEvents.Count)
						{
							num = 1583450711;
							num5 = num;
						}
						else
						{
							num = 1583450707;
							num5 = num;
						}
						continue;
					}
					case 8:
						if (elementIndex < 0)
						{
							return;
						}
						goto case 2;
					case 7:
						elementIndex = element.GetElementIndex(customController);
						num = 1583450715;
						continue;
					case 13:
						if (!GetUseCustomController())
						{
							return;
						}
						goto case 6;
					case 11:
						break;
					case 3:
						num3++;
						num = 1583450717;
						continue;
					case 2:
						_ = _inputEvents.Count;
						num = 1583450714;
						continue;
					case 4:
						value2 = _inputEvents[num3];
						num = 1583450713;
						continue;
					case 6:
					{
						customController = GetCustomController(warn: false);
						int num4;
						if (customController != null)
						{
							num = 1583450708;
							num4 = num;
						}
						else
						{
							num = 1583450719;
							num4 = num;
						}
						continue;
					}
					case 9:
						num3 = 0;
						num = 1583450717;
						continue;
					case 10:
						if (value2.TargetMatches(element.elementType, elementIndex))
						{
							value2.Merge(value);
							_inputEvents[num3] = value2;
							return;
						}
						goto case 3;
					default:
						_inputEvents.Add(new InputEvent(element.elementType, elementIndex, value));
						return;
					}
					break;
				}
			}
		}

		internal void ClearElementValue(CustomControllerElementTargetSet targetSet)
		{
			if (targetSet == null)
			{
				return;
			}
			int num2 = default(int);
			while (true)
			{
				int targetCount = targetSet.targetCount;
				int num = -1737734907;
				while (true)
				{
					switch (num ^ -1737734906)
					{
					case 5:
						num = -1737734908;
						continue;
					case 2:
						break;
					case 1:
						num2++;
						num = -1737734906;
						continue;
					case 4:
						ClearElementValue(targetSet[num2]);
						num = -1737734905;
						continue;
					case 3:
						num2 = 0;
						num = -1737734906;
						continue;
					default:
						if (num2 >= targetCount)
						{
							return;
						}
						goto case 4;
					}
					break;
				}
			}
		}

		internal void ClearElementValue(CustomControllerElementTarget target)
		{
			if (target != null)
			{
				ClearElementValue(target.element);
			}
		}

		internal void ClearElementValue(CustomControllerElementSelector element)
		{
			//Discarded unreachable code: IL_0089, IL_00e5, IL_00f5, IL_0115, IL_013d, IL_017c
			if (!base.initialized)
			{
				goto IL_000b;
			}
			goto IL_011f;
			IL_011f:
			int num;
			int num2;
			if (element == null)
			{
				num = 1480299674;
				num2 = num;
			}
			else
			{
				num = 1480299661;
				num2 = num;
			}
			goto IL_0010;
			IL_0010:
			Rewired.CustomController customController = default(Rewired.CustomController);
			int elementIndex = default(int);
			InputEvent inputEvent = default(InputEvent);
			int num3 = default(int);
			while (true)
			{
				switch (num ^ 0x583B948A)
				{
				case 14:
					return;
				case 16:
					return;
				case 3:
					break;
				case 0:
					switch (element.elementType)
					{
					case CustomControllerElementSelector.ElementType.Axis:
						goto IL_0147;
					case CustomControllerElementSelector.ElementType.Button:
						goto IL_01aa;
					}
					num = 1480299651;
					continue;
				case 4:
					num = 1480299656;
					continue;
				case 5:
					customController = GetCustomController(warn: false);
					num = 1480299659;
					continue;
				case 12:
					elementIndex = element.GetElementIndex(customController);
					num = 1480299655;
					continue;
				case 11:
					if (inputEvent.TargetMatches(element.elementType, elementIndex))
					{
						_inputEvents.RemoveAt(num3);
						num = 1480299648;
						continue;
					}
					goto case 10;
				case 9:
					throw new NotImplementedException();
				case 10:
					num3--;
					num = 1480299660;
					continue;
				case 13:
					if (elementIndex < 0)
					{
						return;
					}
					goto case 0;
				case 17:
					goto IL_011f;
				case 1:
					if (customController == null)
					{
						return;
					}
					goto case 12;
				case 15:
					goto IL_0147;
				case 18:
					inputEvent = _inputEvents[num3];
					num = 1480299649;
					continue;
				case 7:
					if (!GetUseCustomController())
					{
						return;
					}
					goto case 5;
				case 2:
					_ = _inputEvents.Count;
					num3 = _inputEvents.Count - 1;
					num = 1480299660;
					continue;
				case 8:
					goto IL_01aa;
				default:
					{
						if (num3 < 0)
						{
							return;
						}
						goto case 18;
					}
					IL_0147:
					customController.ClearAxisValue(elementIndex);
					num = 1480299662;
					continue;
					IL_01aa:
					customController.ClearButtonValue(elementIndex);
					num = 1480299656;
					continue;
				}
				break;
			}
			goto IL_000b;
			IL_000b:
			num = 1480299652;
			goto IL_0010;
		}

		internal int ElementExists_Editor(CustomControllerElementSelector element)
		{
			//Discarded unreachable code: IL_006f, IL_0080, IL_00aa
			if (element == null)
			{
				goto IL_0006;
			}
			if (!element.isAssigned)
			{
				return -1;
			}
			if (_rewiredInputManager == null)
			{
				return -1;
			}
			if (!_customControllerSelector.findUsingSourceId)
			{
				return -1;
			}
			CustomController_Editor customControllerById = _rewiredInputManager.userData.GetCustomControllerById(_customControllerSelector.sourceId);
			int num = -1889310794;
			goto IL_000b;
			IL_0006:
			num = -1889310791;
			goto IL_000b;
			IL_000b:
			CustomControllerElementSelector.ElementType elementType = default(CustomControllerElementSelector.ElementType);
			while (true)
			{
				switch (num ^ -1889310799)
				{
				case 5:
					break;
				case 4:
					if (!customControllerById.ContainsElementIdentifier(element.elementId))
					{
						return 0;
					}
					return 1;
				case 0:
					switch (elementType)
					{
					default:
						throw new NotImplementedException();
					case CustomControllerElementSelector.ElementType.Button:
						break;
					case CustomControllerElementSelector.ElementType.Axis:
						goto IL_00d9;
					}
					if (element.elementIndex >= 0)
					{
						if (element.elementIndex < customControllerById.buttonCount)
						{
							return 1;
						}
						num = -1889310800;
						continue;
					}
					goto case 1;
				case 1:
					return 0;
				case 7:
					if (customControllerById == null)
					{
						return -1;
					}
					switch (element.selectorType)
					{
					case CustomControllerElementSelector.SelectorType.Id:
						break;
					case CustomControllerElementSelector.SelectorType.Index:
						goto IL_0051;
					case CustomControllerElementSelector.SelectorType.Name:
						goto IL_014b;
					default:
						throw new NotImplementedException();
					}
					goto case 4;
				case 6:
					return 0;
				case 3:
					goto IL_00d9;
				case 8:
					return -1;
				default:
					goto IL_014b;
					IL_00d9:
					if (element.elementIndex >= 0)
					{
						if (element.elementIndex < customControllerById.axisCount)
						{
							return 1;
						}
						num = -1889310793;
						continue;
					}
					goto case 6;
					IL_014b:
					if (!ArrayTools.Contains(customControllerById.GetElementIdentifierNames(), element.elementName))
					{
						return 0;
					}
					return 1;
					IL_0051:
					elementType = element.elementType;
					num = -1889310799;
					continue;
				}
				break;
			}
			goto IL_0006;
		}

		internal bool ElementExists(CustomControllerElementSelector element)
		{
			if (!base.initialized)
			{
				return false;
			}
			if (element == null)
			{
				return false;
			}
			Rewired.CustomController customController = GetCustomController(warn: false);
			if (customController == null)
			{
				return false;
			}
			return element.GetElementIndex(customController) >= 0;
		}

		internal bool ValidateElements(CustomControllerElementTargetSet targetSet)
		{
			if (targetSet == null)
			{
				return false;
			}
			bool flag = true;
			int targetCount = targetSet.targetCount;
			int num2 = default(int);
			while (true)
			{
				int num = 1177233337;
				while (true)
				{
					switch (num ^ 0x462B27BB)
					{
					case 3:
						break;
					case 2:
						num2 = 0;
						num = 1177233338;
						continue;
					case 4:
						flag &= ValidateElement(targetSet[num2]);
						num2++;
						num = 1177233339;
						continue;
					case 1:
						num = 1177233339;
						continue;
					default:
						if (num2 >= targetCount)
						{
							return flag;
						}
						goto case 4;
					}
					break;
				}
			}
		}

		internal bool ValidateElement(CustomControllerElementTarget target)
		{
			if (target == null)
			{
				return false;
			}
			return ValidateElement(target.element);
		}

		internal bool ValidateElement(CustomControllerElementSelector element)
		{
			if (!base.initialized)
			{
				goto IL_0008;
			}
			if (!GetUseCustomController())
			{
				return false;
			}
			if (element == null)
			{
				return false;
			}
			if (!element.isAssigned)
			{
				return false;
			}
			Rewired.CustomController customController = GetCustomController(warn: false);
			if (customController == null)
			{
				return false;
			}
			string[] array = default(string[]);
			int num;
			if (!ElementExists(element))
			{
				array = new string[5];
				num = -1490066537;
				goto IL_000d;
			}
			return true;
			IL_000d:
			while (true)
			{
				switch (num ^ -1490066538)
				{
				case 4:
					break;
				case 5:
					array[1] = element.GetSelectorFormattedString();
					array[2] = " in Custom Controller \"";
					array[3] = customController.name;
					num = -1490066538;
					continue;
				case 0:
					array[4] = "\"";
					num = -1490066539;
					continue;
				case 6:
					return false;
				case 3:
					Logger.LogWarning(string.Concat(array));
					num = -1490066540;
					continue;
				case 1:
					array[0] = "No element found for ";
					num = -1490066541;
					continue;
				default:
					return false;
				}
				break;
			}
			goto IL_0008;
			IL_0008:
			num = -1490066544;
			goto IL_000d;
		}

		private void OnSetProperty()
		{
			//Discarded unreachable code: IL_0027
			if (!base.initialized)
			{
				while (true)
				{
					switch (-33177481 ^ -33177483)
					{
					case 2:
						return;
					case 0:
						continue;
					}
					break;
				}
			}
			_inputEvents.Clear();
		}

		private bool CheckIsRewiredReady()
		{
			if (ReInput.isReady)
			{
				return true;
			}
			Logger.LogError("Rewired is not initialized. You must have an enabled Rewired Input Manager in the scene if using a Custom Controller. Custom Controller support will be disabled on this Custom Controller.");
			SetUseCustomController(value: false);
			return false;
		}

		private void ProcessInputEvents()
		{
			//Discarded unreachable code: IL_00c4, IL_0132
			if (_inputEvents.Count == 0)
			{
				return;
			}
			CustomControllerElementSelector.ElementType elementType = default(CustomControllerElementSelector.ElementType);
			InputEvent inputEvent = default(InputEvent);
			while (true)
			{
				IL_00ad:
				Rewired.CustomController customController = GetCustomController(warn: false);
				if (customController == null)
				{
					break;
				}
				while (true)
				{
					IL_0109:
					int num = 0;
					int num2 = -1702552851;
					while (true)
					{
						switch (num2 ^ -1702552858)
						{
						case 4:
							num2 = -1702552857;
							continue;
						case 7:
							switch (elementType)
							{
							case CustomControllerElementSelector.ElementType.Button:
								goto IL_00ce;
							case CustomControllerElementSelector.ElementType.Axis:
								goto IL_013c;
							}
							num2 = -1702552850;
							continue;
						case 8:
							num2 = -1702552858;
							continue;
						case 11:
							break;
						case 6:
							num++;
							num2 = -1702552851;
							continue;
						case 1:
							goto IL_00ad;
						case 5:
							goto IL_00ce;
						case 10:
							elementType = inputEvent.elementType;
							num2 = -1702552863;
							continue;
						case 9:
							goto IL_0109;
						case 12:
							inputEvent = _inputEvents[num];
							num2 = -1702552852;
							continue;
						case 0:
							throw new NotImplementedException();
						case 3:
							goto IL_013c;
						default:
							{
								_inputEvents.Clear();
								return;
							}
							IL_013c:
							customController.SetAxisValue(inputEvent.elementIndex, inputEvent.value);
							num2 = -1702552864;
							continue;
							IL_00ce:
							customController.SetButtonValue(inputEvent.elementIndex, inputEvent.value != 0f);
							num2 = -1702552864;
							continue;
						}
						int num3;
						if (num < _inputEvents.Count)
						{
							num2 = -1702552854;
							num3 = num2;
						}
						else
						{
							num2 = -1702552860;
							num3 = num2;
						}
					}
				}
			}
			_inputEvents.Clear();
		}

		private Rewired.CustomController GetCustomController(bool warn)
		{
			if (!GetUseCustomController())
			{
				return null;
			}
			if (!ReInput.isReady)
			{
				return null;
			}
			if (_createdCustomControllerId < 0)
			{
				goto IL_00a1;
			}
			Rewired.CustomController customController = ReInput.controllers.GetCustomController(_createdCustomControllerId);
			if (customController == null)
			{
				_createdCustomControllerId = -1;
				goto IL_003d;
			}
			goto IL_0100;
			IL_00a1:
			customController = null;
			int num = 424084963;
			goto IL_0042;
			IL_0042:
			while (true)
			{
				switch (num ^ 0x194705E7)
				{
				case 9:
					break;
				case 5:
					goto IL_007a;
				case 3:
					goto IL_008d;
				case 6:
					goto IL_00a1;
				case 8:
					if (customController == null && GetUseCustomController())
					{
						Logger.LogWarning("No Custom Controller was found matching the search parameters.");
						num = 424084965;
						continue;
					}
					goto default;
				case 1:
					num = 424084963;
					continue;
				case 7:
					num = 424084964;
					continue;
				case 0:
					if (customController != null)
					{
						_createdCustomControllerId = customController.id;
						TryAssignCustomControllerToPlayer(customController);
						num = 424084960;
						continue;
					}
					goto IL_008d;
				case 4:
					goto IL_0100;
				default:
					return customController;
				}
				break;
			}
			goto IL_003d;
			IL_003d:
			num = 424084966;
			goto IL_0042;
			IL_0100:
			if (customController == null)
			{
				if (_createCustomControllerSettings.createCustomController)
				{
					customController = ReInput.controllers.CreateCustomController(_createCustomControllerSettings.customControllerSourceId);
					num = 424084967;
					goto IL_0042;
				}
				goto IL_007a;
			}
			goto IL_008d;
			IL_008d:
			int num2;
			if (warn)
			{
				num = 424084975;
				num2 = num;
			}
			else
			{
				num = 424084965;
				num2 = num;
			}
			goto IL_0042;
			IL_007a:
			customController = _customControllerSelector.GetCustomController();
			num = 424084964;
			goto IL_0042;
		}

		private void TryAssignCustomControllerToPlayer(Rewired.CustomController customController)
		{
			//Discarded unreachable code: IL_0035, IL_0092
			if (customController == null)
			{
				return;
			}
			Player player = default(Player);
			while (true)
			{
				int num;
				int num2;
				if (_createCustomControllerSettings.assignToPlayerId == -1)
				{
					num = -823735701;
					num2 = num;
				}
				else
				{
					num = -823735702;
					num2 = num;
				}
				while (true)
				{
					switch (num ^ -823735702)
					{
					case 5:
						return;
					case 3:
						num = -823735704;
						continue;
					case 1:
						if (!Application.isEditor)
						{
							return;
						}
						Logger.LogWarning("The Custom Controller has not been assigned to any Player and will not be used for input until it is assigned. You should set the Player to assign it to in the inspector.");
						num = -823735697;
						continue;
					case 0:
						player = ReInput.players.GetPlayer(_createCustomControllerSettings.assignToPlayerId);
						if (player == null)
						{
							Logger.LogError("Invalid Player Id " + _createCustomControllerSettings.assignToPlayerId + ". Cannot assign Custom Controller to Player.");
							return;
						}
						goto default;
					case 2:
						break;
					default:
						player.controllers.AddController(customController, removeFromOtherPlayers: true);
						return;
					}
					break;
				}
			}
		}

		private void TryDestroyCustomController()
		{
			//Discarded unreachable code: IL_0062, IL_006a, IL_0072
			if (_createdCustomControllerId < 0)
			{
				return;
			}
			Rewired.CustomController customController = default(Rewired.CustomController);
			while (true)
			{
				int num;
				int num2;
				if (!_createCustomControllerSettings.destroyCustomController)
				{
					num = -473357229;
					num2 = num;
				}
				else
				{
					num = -473357232;
					num2 = num;
				}
				while (true)
				{
					switch (num ^ -473357228)
					{
					case 0:
						return;
					case 3:
						return;
					case 7:
						return;
					case 2:
						num = -473357227;
						continue;
					case 1:
						break;
					case 5:
						ReInput.controllers.DestroyCustomController(customController);
						num = -473357220;
						continue;
					case 4:
					{
						customController = GetCustomController(warn: false);
						int num4;
						if (customController == null)
						{
							num = -473357228;
							num4 = num;
						}
						else
						{
							num = -473357230;
							num4 = num;
						}
						continue;
					}
					case 6:
					{
						int num3;
						if (!ReInput.isReady)
						{
							num = -473357225;
							num3 = num;
						}
						else
						{
							num = -473357231;
							num3 = num;
						}
						continue;
					}
					default:
						_createdCustomControllerId = -1;
						return;
					}
					break;
				}
			}
		}

		private void OnInputSourceUpdate()
		{
			if (_InputSourceUpdateEvent != null)
			{
				_InputSourceUpdateEvent();
			}
			ProcessInputEvents();
		}
	}
}
