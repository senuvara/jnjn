using Rewired.ComponentControls.Data;
using Rewired.Internal;
using Rewired.Utils;
using Rewired.Utils.Attributes;
using Rewired.Utils.UI;
using System;
using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.Events;
using UnityEngine.EventSystems;

namespace Rewired.ComponentControls
{
	[Serializable]
	[DisallowMultipleComponent]
	[CustomClassObfuscation(renamePrivateMembers = true, renamePubIntMembers = false)]
	public sealed class TouchButton : TouchInteractable
	{
		public enum ButtonType
		{
			Standard,
			ToggleSwitch
		}

		private enum iYMiiqNhvRvuajtUmoNwBOObbph
		{
			UHYvAyHcQsOOrxHRGHpDrbrHmPt,
			vZJAaAAZjhfVfUmYHsWMcjdbLrND,
			IkWnGUwOZwdrEJiPezsIYBjiDyT
		}

		private enum wpUgJeGUsoSRewkpBKJyJpvJjLv
		{
			PClOLQRhgHgGSKopqNIgjbqoRgUa,
			dHfPQraqXaVZHuxcjEqIITHISaN
		}

		[Serializable]
		public class AxisValueChangedEventHandler : UnityEvent<float>
		{
		}

		[Serializable]
		public class ButtonValueChangedEventHandler : UnityEvent<bool>
		{
		}

		[Serializable]
		public class ButtonDownEventHandler : UnityEvent
		{
		}

		[Serializable]
		public class ButtonUpEventHandler : UnityEvent
		{
		}

		private const float cboHHwwhBPgccDnewNhTtIFeriUn = 20f;

		[Tooltip("The Custom Controller element that will receive input values from this control.")]
		[SerializeField]
		[CustomObfuscation(rename = false)]
		private CustomControllerElementTargetSetForFloat _targetCustomControllerElement = new CustomControllerElementTargetSetForFloat(new CustomControllerElementTarget(new CustomControllerElementSelector
		{
			elementType = CustomControllerElementSelector.ElementType.Button
		}));

		[Tooltip("The type of button.\nStandard: A momentary switch. Returns True while the button is pressed down.\nToggle Switch: Alternately turns on and off with each press.")]
		[CustomObfuscation(rename = false)]
		[SerializeField]
		private ButtonType _buttonType;

		[SerializeField]
		[CustomObfuscation(rename = false)]
		[Tooltip("If true, the button can be turned on by a touch swipe that began in an area outside the button region. If false, the button can only be turned on by a direct press.")]
		private bool _activateOnSwipeIn;

		[SerializeField]
		[Tooltip("If true, the button will stay on even if the touch that activated it moves outside the button region. If false, the button will turn off once the touch that activated it moves outside the button region.")]
		[CustomObfuscation(rename = false)]
		private bool _stayActiveOnSwipeOut = true;

		[CustomObfuscation(rename = false)]
		[SerializeField]
		[Tooltip("Makes the axis value gradually change over time based on gravity and sensitivity as the button is pressed.")]
		private bool _useDigitalAxisSimulation;

		[SerializeField]
		[CustomObfuscation(rename = false)]
		[Tooltip("Speed (units/sec) that the axis value falls toward 0 when not pressed. A value of 1.0 means an axis value of 1 will drain to 0 over 1 second. A value of 3 equates to 1/3 of a second, and so on.")]
		[FieldRange(0f, float.PositiveInfinity)]
		private float _digitalAxisGravity = 3f;

		[SerializeField]
		[FieldRange(0f, float.PositiveInfinity)]
		[CustomObfuscation(rename = false)]
		[Tooltip("Speed to move toward an axis value of 1.0 in units/sec when pressed. A value of 1.0 means an axis value of 0 will reach 1 over 1 second. A value of 3 equates to 1/3 of a second, and so on.")]
		private float _digitalAxisSensitivity = 3f;

		[CustomObfuscation(rename = false)]
		[SerializeField]
		[Tooltip("The internal axis of the button. The axis is used for all value calculations.")]
		private StandaloneAxis _axis = new StandaloneAxis();

		[SerializeField]
		[CustomObfuscation(rename = false)]
		[Tooltip("Optional external region to use for hover/click/touch detection. If set, this region will be used for touch detection instead of or in addition to the button's RectTransform. This can be useful if you want a larger area of the screen to act as a button.")]
		private TouchRegion _touchRegion;

		[SerializeField]
		[Tooltip("If True, hovers/clicks/touches on the local button will be ignored and only Touch Region touches will be used. Otherwise, both touches on the button and on the Touch Region will be used. This also applies to mouse hover. This setting has no effect if no Touch Region is set.")]
		[CustomObfuscation(rename = false)]
		private bool _useTouchRegionOnly = true;

		[SerializeField]
		[Tooltip("If True, the button will move to the location of the current touch in the Touch Region. This can be used to designate an area of the screen as a hot-spot for a button and have the button graphics follow the users touches. This only has an effect if a Touch Region is set.")]
		[CustomObfuscation(rename = false)]
		private bool _moveToTouchPosition;

		[CustomObfuscation(rename = false)]
		[Tooltip("If Move To Touch Position is enabled, this will make the button return to its original position after the press is released. This only has an effect if a Touch Region is set.")]
		[SerializeField]
		private bool _returnOnRelease = true;

		[CustomObfuscation(rename = false)]
		[SerializeField]
		[Tooltip("If True, the button will follow the touch around until released. This setting overrides Move To Touch Position.")]
		private bool _followTouchPosition;

		[Tooltip("Should the button animate when moving to the touch point? This only has an effect if Move To Touch Position is True and a Touch Region is set. This setting is ignored if Follow Touch Position is True.")]
		[CustomObfuscation(rename = false)]
		[SerializeField]
		private bool _animateOnMoveToTouch = true;

		[Tooltip("The speed at which the button will move toward the touch position measured in screens per second (based on the larger of width and height). [1.0 = Move 1 screen/sec]. This only has an effect if Move To Touch Position is True, Animate On Move To Touch is true, and a Touch Region is set. This setting is ignored if Follow Touch Position is True.")]
		[Range(0f, 20f)]
		[CustomObfuscation(rename = false)]
		[SerializeField]
		private float _moveToTouchSpeed = 2f;

		[Tooltip("Should the button animate when moving back to its original position? This only has an effect if Follow Touch Position is True, or if Move To Touch Position is True and a Touch Region is set, and Return on Release is True.")]
		[CustomObfuscation(rename = false)]
		[SerializeField]
		private bool _animateOnReturn = true;

		[Range(0f, 20f)]
		[SerializeField]
		[Tooltip("The speed at which the button will move back toward its original position measured in screens per second (based on the larger of width and height). [1.0 = Move 1 screen/sec]. This only has an effect if Follow Touch Position is True, or if Move To Touch Position is True and a Touch Region is set, and Return on Release and Animate on Return are both True.")]
		[CustomObfuscation(rename = false)]
		private float _returnSpeed = 2f;

		[SerializeField]
		[CustomObfuscation(rename = false)]
		[Tooltip("If True, it will attempt to automatically manage Graphic component raycasting for best results based on your current settings.")]
		private bool _manageRaycasting = true;

		private float myOTuuQrqJuxxnqFGhfZXCYyQde;

		private float jDsWSkjgusvsQaokRkmYteByAhmE;

		private TouchRegion pTsRyfjQnsgwofrHFcNPaYsdQuF;

		private Vector2 FoMrdulYVFGKttcAyBAMgTlapfW;

		private bool HWooeOwFrqDzeHCXswWcfutKhdS;

		private bool GlEEcbCNWbgxmTRepbQqNmvhAgtA;

		private iYMiiqNhvRvuajtUmoNwBOObbph WBSCXEYmRZLHaKyzLRPWbvYHIBb;

		private int YfuiRllWCyNAOiNefrgAuQetcTCD = int.MinValue;

		private int tfmoiLbwVkRzJNaMHoSyQrJPizV = int.MinValue;

		[NonSerialized]
		private bool yrfVZiGUTfOClpVIfIvNRqImIGGe;

		[NonSerialized]
		private bool BSbeTqiFrdRDtCmjwklbhCLtNiqg;

		private IEnumerator dpYLyFGliITEqozxdonqdBfwRfu;

		private AownhRbtzBiFgCcEIIjJdDFsEtza hkCUJYrPdJOsWoxdYAwAPBmdUfL = new AownhRbtzBiFgCcEIIjJdDFsEtza();

		private Action<iYMiiqNhvRvuajtUmoNwBOObbph> JAgROncsagnGqPbsqYpLTRiSSVw;

		private Action<iYMiiqNhvRvuajtUmoNwBOObbph> sapxcwnCezCMTfxYGlbshzPdovDg;

		[SerializeField]
		[Tooltip("Event sent when the axis value changes.")]
		[CustomObfuscation(rename = false)]
		private AxisValueChangedEventHandler _onAxisValueChanged = new AxisValueChangedEventHandler();

		[CustomObfuscation(rename = false)]
		[Tooltip("Event sent when the button value changes.")]
		[SerializeField]
		private ButtonValueChangedEventHandler _onButtonValueChanged = new ButtonValueChangedEventHandler();

		[Tooltip("Event sent when the button is pressed.")]
		[CustomObfuscation(rename = false)]
		[SerializeField]
		private ButtonDownEventHandler _onButtonDown = new ButtonDownEventHandler();

		[SerializeField]
		[CustomObfuscation(rename = false)]
		[Tooltip("Event sent when the button is released.")]
		private ButtonUpEventHandler _onButtonUp = new ButtonUpEventHandler();

		private Dictionary<int, PointerEventData> sfxtihDClNcjhPUlTfnZDnVMdzt;

		public CustomControllerElementTargetSetForFloat targetCustomControllerElement => _targetCustomControllerElement;

		public ButtonType buttonType
		{
			get
			{
				return _buttonType;
			}
			set
			{
				if (_buttonType == value)
				{
					return;
				}
				while (true)
				{
					_buttonType = value;
					int num = -250253489;
					while (true)
					{
						switch (num ^ -250253491)
						{
						case 0:
							goto IL_000a;
						case 1:
							break;
						default:
							OnSetProperty();
							return;
						}
						break;
						IL_000a:
						num = -250253492;
					}
				}
			}
		}

		public bool activateOnSwipeIn
		{
			get
			{
				return _activateOnSwipeIn;
			}
			set
			{
				//Discarded unreachable code: IL_002c
				if (_activateOnSwipeIn == value)
				{
					goto IL_0009;
				}
				goto IL_0033;
				IL_0033:
				_activateOnSwipeIn = value;
				int num = 373878329;
				goto IL_000e;
				IL_000e:
				switch (num ^ 0x1648EE3B)
				{
				case 3:
					return;
				case 0:
					break;
				case 1:
					goto IL_0033;
				default:
					OnSetProperty();
					return;
				}
				goto IL_0009;
				IL_0009:
				num = 373878328;
				goto IL_000e;
			}
		}

		public bool stayActiveOnSwipeOut
		{
			get
			{
				if (dFVbcRaqFpJjthtmulsGoBxPsGN())
				{
					return true;
				}
				return _stayActiveOnSwipeOut;
			}
			set
			{
				//Discarded unreachable code: IL_002c
				if (_stayActiveOnSwipeOut == value)
				{
					goto IL_0009;
				}
				goto IL_0033;
				IL_0033:
				_stayActiveOnSwipeOut = value;
				OnSetProperty();
				int num = 1110945912;
				goto IL_000e;
				IL_000e:
				switch (num ^ 0x4237B079)
				{
				default:
					return;
				case 1:
					return;
				case 3:
					return;
				case 2:
					break;
				case 0:
					goto IL_0033;
				}
				goto IL_0009;
				IL_0009:
				num = 1110945914;
				goto IL_000e;
			}
		}

		public bool useDigitalAxisSimulation
		{
			get
			{
				return _useDigitalAxisSimulation;
			}
			set
			{
				//Discarded unreachable code: IL_0028
				if (_useDigitalAxisSimulation == value)
				{
					while (true)
					{
						switch (-1360334779 ^ -1360334780)
						{
						case 1:
							return;
						case 0:
							continue;
						}
						break;
					}
				}
				_useDigitalAxisSimulation = value;
				OnSetProperty();
			}
		}

		public float digitalAxisGravity
		{
			get
			{
				return _digitalAxisGravity;
			}
			set
			{
				if (_digitalAxisGravity == value)
				{
					return;
				}
				while (true)
				{
					_digitalAxisGravity = value;
					int num = -663659718;
					while (true)
					{
						switch (num ^ -663659717)
						{
						case 0:
							goto IL_000a;
						case 2:
							break;
						default:
							OnSetProperty();
							return;
						}
						break;
						IL_000a:
						num = -663659719;
					}
				}
			}
		}

		public float digitalAxisSensitivity
		{
			get
			{
				return _digitalAxisSensitivity;
			}
			set
			{
				if (_digitalAxisSensitivity != value)
				{
					_digitalAxisSensitivity = value;
					OnSetProperty();
				}
			}
		}

		public TouchRegion touchRegion
		{
			get
			{
				return _touchRegion;
			}
			set
			{
				if (_touchRegion == value)
				{
					return;
				}
				while (true)
				{
					_touchRegion = value;
					int num = 325554761;
					while (true)
					{
						switch (num ^ 0x1367924B)
						{
						case 0:
							goto IL_000f;
						case 1:
							break;
						default:
							OnSetProperty();
							return;
						}
						break;
						IL_000f:
						num = 325554762;
					}
				}
			}
		}

		public bool useTouchRegionOnly
		{
			get
			{
				return _useTouchRegionOnly;
			}
			set
			{
				//Discarded unreachable code: IL_002c
				if (_useTouchRegionOnly == value)
				{
					goto IL_0009;
				}
				goto IL_0033;
				IL_0033:
				_useTouchRegionOnly = value;
				int num = 1341436339;
				goto IL_000e;
				IL_000e:
				switch (num ^ 0x4FF4B1B0)
				{
				case 1:
					return;
				case 2:
					break;
				case 0:
					goto IL_0033;
				default:
					OnSetProperty();
					return;
				}
				goto IL_0009;
				IL_0009:
				num = 1341436337;
				goto IL_000e;
			}
		}

		public bool moveToTouchPosition
		{
			get
			{
				return _moveToTouchPosition;
			}
			set
			{
				if (_moveToTouchPosition != value)
				{
					_moveToTouchPosition = value;
					OnSetProperty();
				}
			}
		}

		public bool returnOnRelease
		{
			get
			{
				return _returnOnRelease;
			}
			set
			{
				//Discarded unreachable code: IL_002c
				if (_returnOnRelease == value)
				{
					goto IL_0009;
				}
				goto IL_0033;
				IL_0033:
				_returnOnRelease = value;
				int num = -476084011;
				goto IL_000e;
				IL_000e:
				switch (num ^ -476084010)
				{
				case 2:
					return;
				case 0:
					break;
				case 1:
					goto IL_0033;
				default:
					OnSetProperty();
					return;
				}
				goto IL_0009;
				IL_0009:
				num = -476084012;
				goto IL_000e;
			}
		}

		public bool followTouchPosition
		{
			get
			{
				return _followTouchPosition;
			}
			set
			{
				//Discarded unreachable code: IL_002c
				if (_followTouchPosition == value)
				{
					goto IL_0009;
				}
				goto IL_0033;
				IL_0033:
				_followTouchPosition = value;
				int num = -552536034;
				goto IL_000e;
				IL_000e:
				switch (num ^ -552536035)
				{
				case 1:
					return;
				case 0:
					break;
				case 2:
					goto IL_0033;
				default:
					OnSetProperty();
					return;
				}
				goto IL_0009;
				IL_0009:
				num = -552536036;
				goto IL_000e;
			}
		}

		public bool animateOnMoveToTouch
		{
			get
			{
				return _animateOnMoveToTouch;
			}
			set
			{
				if (_animateOnMoveToTouch != value)
				{
					_animateOnMoveToTouch = value;
					OnSetProperty();
				}
			}
		}

		public float moveToTouchSpeed
		{
			get
			{
				return _moveToTouchSpeed;
			}
			set
			{
				//Discarded unreachable code: IL_003e
				value = MathTools.Clamp(value, 0f, 20f);
				if (_moveToTouchSpeed == value)
				{
					goto IL_001b;
				}
				goto IL_0045;
				IL_0045:
				_moveToTouchSpeed = value;
				OnSetProperty();
				int num = 2089098495;
				goto IL_0020;
				IL_0020:
				switch (num ^ 0x7C851CFC)
				{
				default:
					return;
				case 1:
					return;
				case 3:
					return;
				case 0:
					break;
				case 2:
					goto IL_0045;
				}
				goto IL_001b;
				IL_001b:
				num = 2089098493;
				goto IL_0020;
			}
		}

		public bool animateOnReturn
		{
			get
			{
				return _animateOnReturn;
			}
			set
			{
				//Discarded unreachable code: IL_0028
				if (_animateOnReturn == value)
				{
					while (true)
					{
						switch (-1701114342 ^ -1701114341)
						{
						case 1:
							return;
						case 2:
							continue;
						}
						break;
					}
				}
				_animateOnReturn = value;
				OnSetProperty();
			}
		}

		public float returnSpeed
		{
			get
			{
				return _returnSpeed;
			}
			set
			{
				//Discarded unreachable code: IL_0053
				value = MathTools.Clamp(value, 0f, 20f);
				while (true)
				{
					int num = 1295566417;
					while (true)
					{
						switch (num ^ 0x4D38C652)
						{
						default:
							return;
						case 0:
							return;
						case 4:
							return;
						case 2:
							break;
						case 3:
						{
							int num2;
							if (_returnSpeed == value)
							{
								num = 1295566418;
								num2 = num;
							}
							else
							{
								num = 1295566419;
								num2 = num;
							}
							continue;
						}
						case 1:
							_returnSpeed = value;
							OnSetProperty();
							num = 1295566422;
							continue;
						}
						break;
					}
				}
			}
		}

		public bool manageRaycasting
		{
			get
			{
				return _manageRaycasting;
			}
			set
			{
				if (_manageRaycasting == value)
				{
					return;
				}
				while (true)
				{
					_manageRaycasting = value;
					int num;
					int num2;
					if (!value)
					{
						num = 1598438965;
						num2 = num;
					}
					else
					{
						num = 1598438962;
						num2 = num;
					}
					while (true)
					{
						switch (num ^ 0x5F463E31)
						{
						case 0:
							num = 1598438960;
							continue;
						case 1:
							break;
						case 4:
							hkCUJYrPdJOsWoxdYAwAPBmdUfL.eeKsAmADUDrgsTtlbyhGDfhxCkc();
							num = 1598438963;
							continue;
						case 3:
							GdUGsVKpTgHEvzMEBLIYtxCfIFr();
							num = 1598438963;
							continue;
						default:
							OnSetProperty();
							return;
						}
						break;
					}
				}
			}
		}

		public int pointerId
		{
			get
			{
				return YfuiRllWCyNAOiNefrgAuQetcTCD;
			}
			set
			{
				YfuiRllWCyNAOiNefrgAuQetcTCD = value;
			}
		}

		public bool hasPointer => YfuiRllWCyNAOiNefrgAuQetcTCD != int.MinValue;

		internal StandaloneAxis axis => _axis;

		private Action<iYMiiqNhvRvuajtUmoNwBOObbph> moveStartedDelegate
		{
			get
			{
				if (JAgROncsagnGqPbsqYpLTRiSSVw == null)
				{
					return JAgROncsagnGqPbsqYpLTRiSSVw = dpevHZjzTtBoDJuKfSBypVLmMZP;
				}
				return JAgROncsagnGqPbsqYpLTRiSSVw;
			}
		}

		private Action<iYMiiqNhvRvuajtUmoNwBOObbph> moveEndedDelegate
		{
			get
			{
				if (sapxcwnCezCMTfxYGlbshzPdovDg == null)
				{
					return sapxcwnCezCMTfxYGlbshzPdovDg = szPdrxBxecWzCAmftCGFBijDAUM;
				}
				return sapxcwnCezCMTfxYGlbshzPdovDg;
			}
		}

		private float axisValue
		{
			get
			{
				if (!_useDigitalAxisSimulation)
				{
					return _axis.value;
				}
				return myOTuuQrqJuxxnqFGhfZXCYyQde;
			}
		}

		private float axisValuePrev
		{
			get
			{
				if (!_useDigitalAxisSimulation)
				{
					return _axis.valuePrev;
				}
				return jDsWSkjgusvsQaokRkmYteByAhmE;
			}
		}

		private bool buttonValue => _axis.buttonValue;

		private bool buttonValuePrev => _axis.buttonValuePrev;

		private int effectivePointerId
		{
			get
			{
				if (YfuiRllWCyNAOiNefrgAuQetcTCD == int.MinValue)
				{
					return int.MinValue;
				}
				if (tfmoiLbwVkRzJNaMHoSyQrJPizV != int.MinValue)
				{
					return tfmoiLbwVkRzJNaMHoSyQrJPizV;
				}
				return YfuiRllWCyNAOiNefrgAuQetcTCD;
			}
		}

		public event UnityAction<float> AxisValueChangedEvent
		{
			add
			{
				_onAxisValueChanged.AddListener(value);
			}
			remove
			{
				_onAxisValueChanged.RemoveListener(value);
			}
		}

		public event UnityAction<bool> ButtonValueChangedEvent
		{
			add
			{
				_onButtonValueChanged.AddListener(value);
			}
			remove
			{
				_onButtonValueChanged.RemoveListener(value);
			}
		}

		public event UnityAction ButtonDownEvent
		{
			add
			{
				_onButtonDown.AddListener(value);
			}
			remove
			{
				_onButtonDown.RemoveListener(value);
			}
		}

		public event UnityAction ButtonUpEvent
		{
			add
			{
				_onButtonUp.AddListener(value);
			}
			remove
			{
				_onButtonUp.RemoveListener(value);
			}
		}

		[CustomObfuscation(rename = false)]
		private TouchButton()
		{
		}

		public void SetRawValue(float value)
		{
			//Discarded unreachable code: IL_0027
			if (!base.initialized)
			{
				while (true)
				{
					switch (-663972291 ^ -663972292)
					{
					case 1:
						return;
					case 2:
						continue;
					}
					break;
				}
			}
			_axis.SetRawValue(value);
		}

		public void SetDefaultPosition()
		{
			gNCdlfoYwYhSuJSFwIBHNQFiQYf(base.rectTransform.anchoredPosition);
		}

		private void gNCdlfoYwYhSuJSFwIBHNQFiQYf(Vector2 P_0)
		{
			if (base.initialized)
			{
				FoMrdulYVFGKttcAyBAMgTlapfW = P_0;
			}
		}

		public void ReturnToDefaultPosition(bool instant)
		{
			if (base.initialized)
			{
				DnUKFYFvXrbUpuVHbPwuUqgMEfK(FoMrdulYVFGKttcAyBAMgTlapfW, PositionType.VMBFNpbuJhnKPkImrxDsseJhTir, !instant && _animateOnReturn, _returnSpeed, iYMiiqNhvRvuajtUmoNwBOObbph.IkWnGUwOZwdrEJiPezsIYBjiDyT);
			}
		}

		public void ReturnToDefaultPosition()
		{
			if (base.initialized)
			{
				ReturnToDefaultPosition(instant: false);
			}
		}

		[CustomObfuscation(rename = false)]
		internal override void Awake()
		{
			base.Awake();
			if (Application.isPlaying)
			{
				FoMrdulYVFGKttcAyBAMgTlapfW = base.rectTransform.anchoredPosition;
			}
		}

		[CustomObfuscation(rename = false)]
		internal override void OnEnable()
		{
			//Discarded unreachable code: IL_0031
			base.OnEnable();
			while (true)
			{
				int num = -1593701725;
				while (true)
				{
					switch (num ^ -1593701726)
					{
					default:
						return;
					case 3:
						return;
					case 2:
						break;
					case 1:
						if (base.initialized)
						{
							goto IL_0038;
						}
						return;
					case 0:
						goto IL_0038;
					}
					break;
					IL_0038:
					GBExVtHFPRLrKdqZIHqEWcjtFsS();
					num = -1593701727;
				}
			}
		}

		[CustomObfuscation(rename = false)]
		internal override void OnDisable()
		{
			base.OnDisable();
			if (base.initialized)
			{
				OnClear();
			}
		}

		[CustomObfuscation(rename = false)]
		internal override void OnValidate()
		{
			base.OnValidate();
			if (!base.initialized)
			{
				return;
			}
			while (true)
			{
				GBExVtHFPRLrKdqZIHqEWcjtFsS();
				int num = -235127510;
				while (true)
				{
					switch (num ^ -235127512)
					{
					default:
						return;
					case 2:
						return;
					case 0:
						goto IL_000f;
					case 1:
						break;
					}
					break;
					IL_000f:
					num = -235127511;
				}
			}
		}

		[CustomObfuscation(rename = false)]
		internal override void Reset()
		{
			base.Reset();
			base.transitionType = TransitionTypeFlags.ColorTint;
		}

		internal override void OnUpdate()
		{
			base.OnUpdate();
			if (!base.initialized)
			{
				return;
			}
			while (true)
			{
				FBlOXIugJLwhCfGPQgASiZxYKxE();
				int num = -1942288749;
				while (true)
				{
					switch (num ^ -1942288745)
					{
					default:
						return;
					case 0:
						return;
					case 2:
						num = -1942288748;
						continue;
					case 1:
						AqJpPNLXtvAoqkWsSomMQAjAGcZ();
						if (_followTouchPosition)
						{
							sQQUfUMgAhuobjmJmIobItMuoeE(effectivePointerId);
							num = -1942288745;
							continue;
						}
						return;
					case 4:
						qHupoFSluhFMHDLsPLailrXWQzD();
						num = -1942288746;
						continue;
					case 3:
						break;
					}
					break;
				}
			}
		}

		internal override bool OnInitialize()
		{
			if (!base.OnInitialize())
			{
				return false;
			}
			return true;
		}

		internal override void OnCustomControllerUpdate()
		{
			//Discarded unreachable code: IL_0033, IL_0054
			if (!base.initialized)
			{
				goto IL_0008;
			}
			goto IL_003a;
			IL_003a:
			int num;
			int num2;
			if (hasController)
			{
				num = 650048592;
				num2 = num;
			}
			else
			{
				num = 650048596;
				num2 = num;
			}
			goto IL_000d;
			IL_000d:
			while (true)
			{
				switch (num ^ 0x26BEF455)
				{
				default:
					return;
				case 0:
					return;
				case 1:
					return;
				case 3:
					return;
				case 2:
					break;
				case 4:
					goto IL_003a;
				case 5:
					yQjPzGJGxRgoFPaCGxcgBaQECRD(_targetCustomControllerElement, axisValue, _axis.buttonActivationThreshold);
					num = 650048597;
					continue;
				}
				break;
			}
			goto IL_0008;
			IL_0008:
			num = 650048598;
			goto IL_000d;
		}

		internal override void OnSubscribeEvents()
		{
			base.OnSubscribeEvents();
			_axis.AxisValueChangedEvent += ErAspGeeDMmNWjmJeGHrjQHrKNo;
			_axis.ButtonValueChangedEvent += UKyYyMcDdRHoCPbtntTMPiMFhNRD;
			_axis.ButtonDownEvent += DdWGleCossJYefolfWVwHWAFzGXJ;
			_axis.ButtonUpEvent += mZnDrZYlkFuqrIDLdYRIsjCyvtr;
		}

		internal override void OnUnsubscribeEvents()
		{
			base.OnUnsubscribeEvents();
			_axis.AxisValueChangedEvent -= ErAspGeeDMmNWjmJeGHrjQHrKNo;
			_axis.ButtonValueChangedEvent -= UKyYyMcDdRHoCPbtntTMPiMFhNRD;
			_axis.ButtonDownEvent -= DdWGleCossJYefolfWVwHWAFzGXJ;
			_axis.ButtonUpEvent -= mZnDrZYlkFuqrIDLdYRIsjCyvtr;
		}

		internal override void OnSetProperty()
		{
			base.OnSetProperty();
			if (!base.initialized)
			{
				return;
			}
			while (true)
			{
				GBExVtHFPRLrKdqZIHqEWcjtFsS();
				int num = -356923838;
				while (true)
				{
					switch (num ^ -356923838)
					{
					default:
						return;
					case 0:
						return;
					case 2:
						goto IL_000f;
					case 1:
						break;
					}
					break;
					IL_000f:
					num = -356923837;
				}
			}
		}

		internal override void OnClear()
		{
			if (!base.initialized)
			{
				return;
			}
			while (true)
			{
				YfuiRllWCyNAOiNefrgAuQetcTCD = int.MinValue;
				tfmoiLbwVkRzJNaMHoSyQrJPizV = int.MinValue;
				yrfVZiGUTfOClpVIfIvNRqImIGGe = false;
				BSbeTqiFrdRDtCmjwklbhCLtNiqg = false;
				int num;
				if (_returnOnRelease)
				{
					int num2;
					if (GlEEcbCNWbgxmTRepbQqNmvhAgtA)
					{
						num = 599195438;
						num2 = num;
					}
					else
					{
						num = 599195437;
						num2 = num;
					}
					goto IL_000e;
				}
				goto IL_00a3;
				IL_000e:
				while (true)
				{
					switch (num ^ 0x23B6FF2F)
					{
					case 0:
						num = 599195432;
						continue;
					case 7:
						break;
					case 1:
						goto IL_0087;
					case 2:
						goto IL_00a3;
					case 4:
						myOTuuQrqJuxxnqFGhfZXCYyQde = 0f;
						num = 599195431;
						continue;
					case 3:
						goto IL_00dd;
					case 6:
						ReturnToDefaultPosition(instant: true);
						num = 599195437;
						continue;
					case 5:
						_axis.Clear();
						num = 599195435;
						continue;
					default:
						jDsWSkjgusvsQaokRkmYteByAhmE = 0f;
						GBExVtHFPRLrKdqZIHqEWcjtFsS();
						return;
					}
					break;
					IL_00dd:
					int num3;
					if (_followTouchPosition)
					{
						num = 599195433;
						num3 = num;
					}
					else
					{
						num = 599195437;
						num3 = num;
					}
					continue;
					IL_0087:
					int num4;
					if (!_moveToTouchPosition)
					{
						num = 599195436;
						num4 = num;
					}
					else
					{
						num = 599195433;
						num4 = num;
					}
				}
				continue;
				IL_00a3:
				GlEEcbCNWbgxmTRepbQqNmvhAgtA = false;
				HWooeOwFrqDzeHCXswWcfutKhdS = false;
				WBSCXEYmRZLHaKyzLRPWbvYHIBb = iYMiiqNhvRvuajtUmoNwBOObbph.UHYvAyHcQsOOrxHRGHpDrbrHmPt;
				XZuHRmWoQuERWskxXRGbpMclLVe();
				num = 599195434;
				goto IL_000e;
			}
		}

		public override void ClearValue()
		{
			if (!base.initialized)
			{
				return;
			}
			while (true)
			{
				_axis.Clear();
				myOTuuQrqJuxxnqFGhfZXCYyQde = 0f;
				int num = -497863042;
				while (true)
				{
					switch (num ^ -497863046)
					{
					default:
						return;
					case 2:
						return;
					case 0:
						num = -497863045;
						continue;
					case 1:
						break;
					case 4:
					{
						int num2;
						if (!hasController)
						{
							num = -497863048;
							num2 = num;
						}
						else
						{
							num = -497863047;
							num2 = num;
						}
						continue;
					}
					case 3:
						base.controller.ClearElementValue(_targetCustomControllerElement);
						num = -497863048;
						continue;
					}
					break;
				}
			}
		}

		internal override bool IsPressed()
		{
			if (!base.initialized)
			{
				goto IL_0008;
			}
			int num;
			if (!agGGiexlYIVgemjaHHzzcCmJTcc())
			{
				num = -1372647576;
				goto IL_000d;
			}
			if (!_axis.buttonValue)
			{
				return _axis.value != 0f;
			}
			return true;
			IL_000d:
			switch (num ^ -1372647574)
			{
			case 0:
				break;
			case 1:
				return false;
			default:
				return false;
			}
			goto IL_0008;
			IL_0008:
			num = -1372647573;
			goto IL_000d;
		}

		internal override bool IsThisOrTouchRegionGameObject(GameObject gameObject)
		{
			if (gameObject == null)
			{
				return false;
			}
			if (base.IsThisOrTouchRegionGameObject(gameObject))
			{
				return true;
			}
			if (pTsRyfjQnsgwofrHFcNPaYsdQuF != null)
			{
				return pTsRyfjQnsgwofrHFcNPaYsdQuF.gameObject == gameObject;
			}
			return false;
		}

		private void AqJpPNLXtvAoqkWsSomMQAjAGcZ()
		{
			//Discarded unreachable code: IL_002b, IL_0048
			if (!_useDigitalAxisSimulation)
			{
				while (true)
				{
					switch (0x49F45FF5 ^ 0x49F45FF4)
					{
					case 1:
						return;
					case 0:
						break;
					case 3:
						goto IL_0032;
					default:
						goto IL_004f;
					}
				}
			}
			goto IL_0032;
			IL_004f:
			UuAAiIWwieWLMqNMlDcpONGUKkx();
			return;
			IL_0032:
			if (_axis.buttonValue)
			{
				mDENDTjFSmbobEgQOjsWenITYYX();
				return;
			}
			goto IL_004f;
		}

		private void mDENDTjFSmbobEgQOjsWenITYYX()
		{
			float num = (_axis.value >= 0f) ? 1f : (-1f);
			float num2 = MathTools.Abs(_digitalAxisSensitivity);
			num *= num2 * Time.unscaledDeltaTime;
			num += myOTuuQrqJuxxnqFGhfZXCYyQde;
			num = MathTools.Clamp(num, -1f, 1f);
			eIgjzoJyKPKpiNhKnzUSbIhiitL(num, true);
		}

		private void UuAAiIWwieWLMqNMlDcpONGUKkx()
		{
			//Discarded unreachable code: IL_005c, IL_007f
			float digitalAxisGravity = _digitalAxisGravity;
			if (digitalAxisGravity == 0f)
			{
				goto IL_000f;
			}
			goto IL_004c;
			IL_004c:
			float num = myOTuuQrqJuxxnqFGhfZXCYyQde;
			if (num == 0f)
			{
				return;
			}
			goto IL_003d;
			IL_003d:
			float num2 = digitalAxisGravity * Time.unscaledDeltaTime;
			int num3 = 950874465;
			goto IL_0014;
			IL_0014:
			float num4 = default(float);
			while (true)
			{
				switch (num3 ^ 0x38AD3167)
				{
				case 1:
					return;
				case 3:
					break;
				case 4:
					goto IL_003d;
				case 0:
					goto IL_004c;
				case 6:
					if (MathTools.Abs(num2) >= MathTools.Abs(num))
					{
						num4 = 0f;
						num3 = 950874469;
						continue;
					}
					goto case 5;
				case 5:
				{
					float num5 = (num > 0f) ? (-1f) : 1f;
					num4 = num + num5 * num2;
					num3 = 950874469;
					continue;
				}
				default:
					eIgjzoJyKPKpiNhKnzUSbIhiitL(num4, true);
					return;
				}
				break;
			}
			goto IL_000f;
			IL_000f:
			num3 = 950874470;
			goto IL_0014;
		}

		private void eIgjzoJyKPKpiNhKnzUSbIhiitL(float P_0, bool P_1)
		{
			jDsWSkjgusvsQaokRkmYteByAhmE = myOTuuQrqJuxxnqFGhfZXCYyQde;
			myOTuuQrqJuxxnqFGhfZXCYyQde = P_0;
			while (true)
			{
				int num = 529926890;
				while (true)
				{
					switch (num ^ 0x1F960AEB)
					{
					default:
						return;
					case 3:
						return;
					case 0:
						break;
					case 1:
						if (P_0 != jDsWSkjgusvsQaokRkmYteByAhmE)
						{
							sjfOHMxvWyaFrJNCKmQvSrptfRJ(null);
							num = 529926889;
							continue;
						}
						goto case 2;
					case 2:
						if (P_1 && P_0 != jDsWSkjgusvsQaokRkmYteByAhmE)
						{
							_onAxisValueChanged.Invoke(P_0);
							num = 529926888;
							continue;
						}
						return;
					}
					break;
				}
			}
		}

		private void TIVHNkuERQEgFirHrNNwifqaPItH()
		{
			//Discarded unreachable code: IL_004a, IL_0052
			if (_buttonType == ButtonType.ToggleSwitch)
			{
				goto IL_0009;
			}
			goto IL_0059;
			IL_0059:
			int num;
			if (_buttonType == ButtonType.Standard)
			{
				_axis.SetRawValue(_axis.rawMax);
				num = 393390116;
				goto IL_000e;
			}
			return;
			IL_0009:
			num = 393390119;
			goto IL_000e;
			IL_000e:
			while (true)
			{
				switch (num ^ 0x1772A825)
				{
				default:
					return;
				case 1:
					return;
				case 4:
					return;
				case 3:
					break;
				case 0:
					_axis.SetRawValue(_axis.rawMax);
					return;
				case 5:
					goto IL_0059;
				case 2:
					if (buttonValue)
					{
						_axis.SetRawValue(_axis.rawZero);
						num = 393390113;
						continue;
					}
					goto case 0;
				}
				break;
			}
			goto IL_0009;
		}

		private void JZAzIivJqeEMSaTjWEtxNWCcplcg()
		{
			if (_buttonType != 0)
			{
				return;
			}
			while (true)
			{
				int num = -1464940518;
				while (true)
				{
					switch (num ^ -1464940520)
					{
					default:
						return;
					case 1:
						return;
					case 0:
						break;
					case 2:
						goto IL_0026;
					}
					break;
					IL_0026:
					_axis.SetRawValue(_axis.rawZero);
					num = -1464940519;
				}
			}
		}

		private void GBExVtHFPRLrKdqZIHqEWcjtFsS()
		{
			_targetCustomControllerElement.ClearElementCaches();
			qHupoFSluhFMHDLsPLailrXWQzD();
			GdUGsVKpTgHEvzMEBLIYtxCfIFr();
		}

		private void GdUGsVKpTgHEvzMEBLIYtxCfIFr()
		{
			//Discarded unreachable code: IL_0027
			if (!_manageRaycasting)
			{
				while (true)
				{
					switch (-1439427669 ^ -1439427670)
					{
					case 1:
						return;
					case 2:
						continue;
					}
					break;
				}
			}
			hkCUJYrPdJOsWoxdYAwAPBmdUfL.KQhvMfEZUJqKpskqtDnGiMNpkZC(base.transform, qcMCRlGDGTOLyeXLWQhWDlVKNSlE());
		}

		private bool qcMCRlGDGTOLyeXLWQhWDlVKNSlE()
		{
			if (pTsRyfjQnsgwofrHFcNPaYsdQuF != null && _useTouchRegionOnly)
			{
				return false;
			}
			return true;
		}

		private void JqOvUhTfOscNAZPCFerffqqVNDb(TouchRegion P_0)
		{
			//Discarded unreachable code: IL_0030
			if (P_0 == null)
			{
				goto IL_0009;
			}
			goto IL_0037;
			IL_0037:
			FyFAaVTwKitmoZRrKTRHswoTOIH(P_0);
			int num = 419756931;
			goto IL_000e;
			IL_000e:
			while (true)
			{
				switch (num ^ 0x1904FB87)
				{
				case 2:
					return;
				case 0:
					break;
				case 1:
					goto IL_0037;
				case 4:
					P_0.PointerDownEvent += NSaCJNLNcGocStUSwnnYWnwuvob;
					num = 419756932;
					continue;
				default:
					P_0.PointerUpEvent += slcdeSGvhTIGRUXtiFKCcBoVLShw;
					P_0.PointerEnterEvent += qrwJpuJXUnJctsmgikDbkDazTTD;
					P_0.PointerExitEvent += rklTaLGzgkBquoJTvHXoKHosDCt;
					return;
				}
				break;
			}
			goto IL_0009;
			IL_0009:
			num = 419756933;
			goto IL_000e;
		}

		private void FyFAaVTwKitmoZRrKTRHswoTOIH(TouchRegion P_0)
		{
			if (!(P_0 == null))
			{
				P_0.PointerDownEvent -= NSaCJNLNcGocStUSwnnYWnwuvob;
				P_0.PointerUpEvent -= slcdeSGvhTIGRUXtiFKCcBoVLShw;
				P_0.PointerEnterEvent -= qrwJpuJXUnJctsmgikDbkDazTTD;
				P_0.PointerExitEvent -= rklTaLGzgkBquoJTvHXoKHosDCt;
			}
		}

		private void qHupoFSluhFMHDLsPLailrXWQzD()
		{
			if (!(pTsRyfjQnsgwofrHFcNPaYsdQuF == _touchRegion))
			{
				FyFAaVTwKitmoZRrKTRHswoTOIH(pTsRyfjQnsgwofrHFcNPaYsdQuF);
				pTsRyfjQnsgwofrHFcNPaYsdQuF = _touchRegion;
				JqOvUhTfOscNAZPCFerffqqVNDb(pTsRyfjQnsgwofrHFcNPaYsdQuF);
			}
		}

		private void RBwZttPSZcSXhOFMwihqtJLzOqE(Vector2 P_0, bool P_1, float P_2, iYMiiqNhvRvuajtUmoNwBOObbph P_3)
		{
			RectTransform rectTransform = base.transform.parent as RectTransform;
			Vector2 vector = QUSKPeaMdsIjKePEopWfjLfNMLfw.rKmDpgLJHuEtVKtAyNSdyFMtyRrf(base.canvas, rectTransform, P_0);
			Vector2 pivot = default(Vector2);
			Vector2 sizeDelta = default(Vector2);
			while (true)
			{
				int num = -646177679;
				while (true)
				{
					switch (num ^ -646177680)
					{
					case 0:
						break;
					case 1:
						pivot = base.rectTransform.pivot;
						sizeDelta = base.rectTransform.sizeDelta;
						num = -646177677;
						continue;
					case 3:
					{
						Vector3 localScale = base.rectTransform.localScale;
						vector += new Vector2((pivot.x - 0.5f) * sizeDelta.x * localScale.x, (pivot.y - 0.5f) * sizeDelta.y * localScale.y);
						num = -646177678;
						continue;
					}
					default:
						DnUKFYFvXrbUpuVHbPwuUqgMEfK(vector, PositionType.PClOLQRhgHgGSKopqNIgjbqoRgUa, P_1, P_2, P_3);
						return;
					}
					break;
				}
			}
		}

		private void DnUKFYFvXrbUpuVHbPwuUqgMEfK(Vector2 P_0, PositionType P_1, bool P_2, float P_3, iYMiiqNhvRvuajtUmoNwBOObbph P_4)
		{
			//Discarded unreachable code: IL_0077, IL_01c3
			if (HWooeOwFrqDzeHCXswWcfutKhdS && P_2)
			{
				goto IL_000b;
			}
			goto IL_007e;
			IL_015f:
			int num;
			int num2;
			if (!(base.canvas == null))
			{
				num = 533436266;
				num2 = num;
			}
			else
			{
				num = 533436273;
				num2 = num;
			}
			goto IL_0010;
			IL_007e:
			if (HWooeOwFrqDzeHCXswWcfutKhdS && dpYLyFGliITEqozxdonqdBfwRfu != null)
			{
				XZuHRmWoQuERWskxXRGbpMclLVe();
				HWooeOwFrqDzeHCXswWcfutKhdS = false;
				WBSCXEYmRZLHaKyzLRPWbvYHIBb = iYMiiqNhvRvuajtUmoNwBOObbph.UHYvAyHcQsOOrxHRGHpDrbrHmPt;
				num = 533436261;
				goto IL_0010;
			}
			goto IL_015f;
			IL_0010:
			bool flag = default(bool);
			Vector2 one = default(Vector2);
			Transform transform = default(Transform);
			float num5 = default(float);
			Vector2 sizeDelta = default(Vector2);
			RectTransform canvasTransform = default(RectTransform);
			float num4 = default(float);
			while (true)
			{
				float num3;
				switch (num ^ 0x1FCB9761)
				{
				case 6:
					return;
				case 0:
					break;
				case 9:
					if (!flag)
					{
						num = 533436269;
						continue;
					}
					num3 = one.y;
					goto IL_0274;
				case 14:
					goto IL_007e;
				case 1:
					goto IL_00b2;
				case 15:
					if (!(transform == null))
					{
						one.x *= transform.localScale.x;
						num = 533436260;
						continue;
					}
					goto case 3;
				case 16:
					Logger.LogWarning("Animation cannot be used without a Canvas.");
					P_2 = false;
					num = 533436267;
					continue;
				case 13:
					num5 = MathTools.Max(sizeDelta.x, sizeDelta.y);
					num = 533436264;
					continue;
				case 10:
					if (P_2)
					{
						transform = base.transform;
						canvasTransform = base.canvasTransform;
						one = Vector2.one;
						num = 533436256;
						continue;
					}
					goto default;
				case 4:
					goto IL_015f;
				case 11:
					if (base.canvas.renderMode == RenderMode.WorldSpace)
					{
						Logger.LogWarning("Animation can only be used with a screen space Canvas.");
						P_2 = false;
						num = 533436267;
						continue;
					}
					goto case 10;
				case 7:
					WBSCXEYmRZLHaKyzLRPWbvYHIBb = P_4;
					GlEEcbCNWbgxmTRepbQqNmvhAgtA = true;
					moveStartedDelegate(P_4);
					return;
				case 5:
					one.y *= transform.localScale.y;
					num = 533436256;
					continue;
				case 3:
					sizeDelta = canvasTransform.sizeDelta;
					flag = (sizeDelta.x < sizeDelta.y);
					num = 533436268;
					continue;
				case 8:
					goto IL_0213;
				case 2:
					P_3 = P_3 / num4 * num5;
					dpYLyFGliITEqozxdonqdBfwRfu = HcJsOVwrfAiwEwdIJagXehXDuiO(P_0, P_1, P_3, P_4);
					StartCoroutine(dpYLyFGliITEqozxdonqdBfwRfu);
					num = 533436262;
					continue;
				case 12:
					num3 = one.x;
					goto IL_0274;
				default:
					{
						moveStartedDelegate(P_4);
						xWWmXnncgSDTMtlbInPtiKaaKpz(P_4, P_0, P_1);
						return;
					}
					IL_0274:
					num4 = num3;
					if (num4 == 0f)
					{
						num4 = 0.0001f;
						num = 533436259;
						continue;
					}
					goto case 2;
				}
				break;
				IL_0213:
				int num6;
				if (WBSCXEYmRZLHaKyzLRPWbvYHIBb == P_4)
				{
					num = 533436263;
					num6 = num;
				}
				else
				{
					num = 533436271;
					num6 = num;
				}
				continue;
				IL_00b2:
				int num7;
				if ((transform = transform.parent) != canvasTransform)
				{
					num = 533436270;
					num7 = num;
				}
				else
				{
					num = 533436258;
					num7 = num;
				}
			}
			goto IL_000b;
			IL_000b:
			num = 533436265;
			goto IL_0010;
		}

		private IEnumerator HcJsOVwrfAiwEwdIJagXehXDuiO(Vector2 P_0, PositionType P_1, float P_2, iYMiiqNhvRvuajtUmoNwBOObbph P_3)
		{
			giynhkJELfeBUYhdDOpcHgTTrPo giynhkJELfeBUYhdDOpcHgTTrPo = new giynhkJELfeBUYhdDOpcHgTTrPo(0);
			giynhkJELfeBUYhdDOpcHgTTrPo.hFQAwvywpKYhLMOJAVKlWFLojQb = this;
			giynhkJELfeBUYhdDOpcHgTTrPo.hJgwKkVIBWGPaETCEkbEBSZQPPsk = P_0;
			giynhkJELfeBUYhdDOpcHgTTrPo.tyTFjCkNBmrbeDPaKBpICCKcOdc = P_1;
			giynhkJELfeBUYhdDOpcHgTTrPo.spMAfzHDKqkSmQgrpxPGEHxzkxZ = P_2;
			giynhkJELfeBUYhdDOpcHgTTrPo.CfKbLqXdRfoleqXNaNuSsBaejEV = P_3;
			return giynhkJELfeBUYhdDOpcHgTTrPo;
		}

		private void xWWmXnncgSDTMtlbInPtiKaaKpz(iYMiiqNhvRvuajtUmoNwBOObbph P_0, Vector2 P_1, PositionType P_2)
		{
			QUSKPeaMdsIjKePEopWfjLfNMLfw.FKjtSQquCwWMYDSUOaOTaImZSWp(base.rectTransform, P_1, P_2);
			while (true)
			{
				int num = -1002036885;
				while (true)
				{
					switch (num ^ -1002036887)
					{
					default:
						return;
					case 5:
						return;
					case 3:
						break;
					case 4:
						if (P_0 == iYMiiqNhvRvuajtUmoNwBOObbph.vZJAaAAZjhfVfUmYHsWMcjdbLrND)
						{
							GlEEcbCNWbgxmTRepbQqNmvhAgtA = true;
							num = -1002036888;
							continue;
						}
						goto case 1;
					case 0:
						GlEEcbCNWbgxmTRepbQqNmvhAgtA = false;
						num = -1002036888;
						continue;
					case 6:
						moveEndedDelegate(P_0);
						num = -1002036884;
						continue;
					case 1:
						XZuHRmWoQuERWskxXRGbpMclLVe();
						num = -1002036881;
						continue;
					case 2:
						HWooeOwFrqDzeHCXswWcfutKhdS = false;
						num = -1002036882;
						continue;
					case 7:
					{
						WBSCXEYmRZLHaKyzLRPWbvYHIBb = iYMiiqNhvRvuajtUmoNwBOObbph.UHYvAyHcQsOOrxHRGHpDrbrHmPt;
						int num2;
						if (P_0 != iYMiiqNhvRvuajtUmoNwBOObbph.IkWnGUwOZwdrEJiPezsIYBjiDyT)
						{
							num = -1002036883;
							num2 = num;
						}
						else
						{
							num = -1002036887;
							num2 = num;
						}
						continue;
					}
					}
					break;
				}
			}
		}

		private void dpevHZjzTtBoDJuKfSBypVLmMZP(iYMiiqNhvRvuajtUmoNwBOObbph P_0)
		{
			if (!_manageRaycasting)
			{
				return;
			}
			bool flag = false;
			bool flag2 = default(bool);
			while (true)
			{
				int num = 1873859849;
				while (true)
				{
					switch (num ^ 0x6FB0D50A)
					{
					default:
						return;
					case 7:
						return;
					case 6:
						break;
					case 1:
						if (!_followTouchPosition)
						{
							int num4;
							if (pTsRyfjQnsgwofrHFcNPaYsdQuF != null)
							{
								num = 1873859855;
								num4 = num;
							}
							else
							{
								num = 1873859850;
								num4 = num;
							}
							continue;
						}
						goto case 0;
					case 5:
						if (!_useTouchRegionOnly)
						{
							int num5;
							if (!_moveToTouchPosition)
							{
								num = 1873859850;
								num5 = num;
							}
							else
							{
								num = 1873859842;
								num5 = num;
							}
							continue;
						}
						goto case 0;
					case 4:
					{
						int num2;
						if (!stayActiveOnSwipeOut)
						{
							num = 1873859851;
							num2 = num;
						}
						else
						{
							num = 1873859842;
							num2 = num;
						}
						continue;
					}
					case 0:
						if (flag)
						{
							hkCUJYrPdJOsWoxdYAwAPBmdUfL.KQhvMfEZUJqKpskqtDnGiMNpkZC(base.transform, flag2);
							num = 1873859853;
							continue;
						}
						return;
					case 2:
					{
						int num3;
						if (!_followTouchPosition)
						{
							num = 1873859851;
							num3 = num;
						}
						else
						{
							num = 1873859854;
							num3 = num;
						}
						continue;
					}
					case 3:
						flag2 = false;
						num = 1873859848;
						continue;
					case 8:
						if (_returnOnRelease && P_0 == iYMiiqNhvRvuajtUmoNwBOObbph.vZJAaAAZjhfVfUmYHsWMcjdbLrND)
						{
							flag = true;
							flag2 = false;
							num = 1873859850;
							continue;
						}
						goto case 0;
					}
					break;
				}
			}
		}

		private void szPdrxBxecWzCAmftCGFBijDAUM(iYMiiqNhvRvuajtUmoNwBOObbph P_0)
		{
			if (!_manageRaycasting)
			{
				return;
			}
			bool flag = default(bool);
			bool flag2 = default(bool);
			while (true)
			{
				int num = 757455815;
				while (true)
				{
					switch (num ^ 0x2D25DBC3)
					{
					default:
						return;
					case 3:
						return;
					case 2:
						break;
					case 4:
						flag = false;
						flag2 = false;
						if (_followTouchPosition)
						{
							int num4;
							if (stayActiveOnSwipeOut)
							{
								num = 757455814;
								num4 = num;
							}
							else
							{
								num = 757455811;
								num4 = num;
							}
							continue;
						}
						goto case 0;
					case 6:
						hkCUJYrPdJOsWoxdYAwAPBmdUfL.KQhvMfEZUJqKpskqtDnGiMNpkZC(base.transform, flag2);
						num = 757455808;
						continue;
					case 5:
						if (_returnOnRelease && P_0 == iYMiiqNhvRvuajtUmoNwBOObbph.IkWnGUwOZwdrEJiPezsIYBjiDyT)
						{
							flag = true;
							flag2 = qcMCRlGDGTOLyeXLWQhWDlVKNSlE();
							num = 757455810;
							continue;
						}
						goto case 1;
					case 1:
					{
						int num2;
						if (!flag)
						{
							num = 757455808;
							num2 = num;
						}
						else
						{
							num = 757455813;
							num2 = num;
						}
						continue;
					}
					case 0:
						if (!_followTouchPosition && pTsRyfjQnsgwofrHFcNPaYsdQuF != null && !_useTouchRegionOnly)
						{
							int num3;
							if (_moveToTouchPosition)
							{
								num = 757455814;
								num3 = num;
							}
							else
							{
								num = 757455810;
								num3 = num;
							}
							continue;
						}
						goto case 1;
					}
					break;
				}
			}
		}

		private void sQQUfUMgAhuobjmJmIobItMuoeE(int P_0)
		{
			//Discarded unreachable code: IL_0027
			if (!TouchInteractable.MUAEmHanKCAekGtjjVciojDFRqNV(P_0))
			{
				while (true)
				{
					switch (-810441281 ^ -810441282)
					{
					case 1:
						return;
					case 0:
						continue;
					}
					break;
				}
			}
			RBwZttPSZcSXhOFMwihqtJLzOqE(TouchInteractable.peFNVeWnRvpCYQMMXzPLXboQIi(P_0), false, 0f, iYMiiqNhvRvuajtUmoNwBOObbph.vZJAaAAZjhfVfUmYHsWMcjdbLrND);
		}

		private void XZuHRmWoQuERWskxXRGbpMclLVe()
		{
			if (dpYLyFGliITEqozxdonqdBfwRfu != null)
			{
				try
				{
					StopCoroutine(dpYLyFGliITEqozxdonqdBfwRfu);
				}
				catch
				{
				}
				dpYLyFGliITEqozxdonqdBfwRfu = null;
			}
		}

		private void FBlOXIugJLwhCfGPQgASiZxYKxE()
		{
			//Discarded unreachable code: IL_0052
			if (!hasPointer)
			{
				return;
			}
			while (!TouchInteractable.MUAEmHanKCAekGtjjVciojDFRqNV(effectivePointerId))
			{
				PointerEventData pointerEventData = lYycfThbLcNDrXoSoMNFZmblhiW(effectivePointerId);
				int num;
				int num2;
				if (pointerEventData == null)
				{
					num = -225164368;
					num2 = num;
				}
				else
				{
					num = -225164366;
					num2 = num;
				}
				while (true)
				{
					switch (num ^ -225164367)
					{
					default:
						return;
					case 4:
						return;
					case 0:
						num = -225164365;
						continue;
					case 1:
						wAYSKMoYLXrHlKmKvvdfOiGBjye();
						num = -225164363;
						continue;
					case 3:
						if (pointerEventData.pointerPress != null)
						{
							rNlfOZEoSimQmBIeJxNKvxgjMQsF(pointerEventData);
							return;
						}
						goto case 1;
					case 2:
						break;
					}
					break;
				}
			}
		}

		private bool dFVbcRaqFpJjthtmulsGoBxPsGN()
		{
			if (!_followTouchPosition)
			{
				goto IL_0008;
			}
			int num;
			if (_touchRegion != null && _useTouchRegionOnly)
			{
				num = -145202879;
				goto IL_000d;
			}
			return true;
			IL_000d:
			switch (num ^ -145202880)
			{
			case 0:
				break;
			case 2:
				return false;
			default:
				return false;
			}
			goto IL_0008;
			IL_0008:
			num = -145202878;
			goto IL_000d;
		}

		private void BgAdqUvZFqBlqudhjyphLkDsMtm()
		{
			YfuiRllWCyNAOiNefrgAuQetcTCD = int.MinValue;
			tfmoiLbwVkRzJNaMHoSyQrJPizV = int.MinValue;
		}

		private bool wzNIDgakHttMmiiBhPwkMJrPBdB(int P_0)
		{
			if (P_0 == int.MinValue)
			{
				return false;
			}
			if (YfuiRllWCyNAOiNefrgAuQetcTCD == int.MinValue)
			{
				goto IL_0017;
			}
			if (YfuiRllWCyNAOiNefrgAuQetcTCD == P_0)
			{
				return true;
			}
			int num;
			if (TouchInteractable.OenwdeSUZHKJsTIxPjYHJUNnctC(P_0) && tfmoiLbwVkRzJNaMHoSyQrJPizV != int.MinValue)
			{
				num = -1948317198;
				goto IL_001c;
			}
			goto IL_0069;
			IL_005e:
			if (P_0 == tfmoiLbwVkRzJNaMHoSyQrJPizV)
			{
				return true;
			}
			goto IL_0069;
			IL_0017:
			num = -1948317199;
			goto IL_001c;
			IL_0069:
			return false;
			IL_001c:
			switch (num ^ -1948317200)
			{
			case 0:
				break;
			case 1:
				return false;
			default:
				goto IL_005e;
			}
			goto IL_0017;
		}

		private PointerEventData KJdskTUpjJuElEhrvEVGouQMzgV(int P_0, GameObject P_1)
		{
			//Discarded unreachable code: IL_00e6, IL_0179
			PointerEventData pointerEventData = lYycfThbLcNDrXoSoMNFZmblhiW(P_0);
			if (pointerEventData == null)
			{
				goto IL_000e;
			}
			GameObject gameObject = P_1;
			pointerEventData.position = TouchInteractable.peFNVeWnRvpCYQMMXzPLXboQIi(P_0);
			int num = -675990795;
			goto IL_0013;
			IL_000e:
			num = -675990796;
			goto IL_0013;
			IL_0013:
			float num3 = default(float);
			float unscaledTime = default(float);
			GameObject gameObject2 = default(GameObject);
			GameObject gameObject3 = default(GameObject);
			float unscaledTime2 = default(float);
			while (true)
			{
				switch (num ^ -675990797)
				{
				case 17:
					break;
				case 12:
					pointerEventData.pressPosition = pointerEventData.position;
					num = -675990792;
					continue;
				case 0:
					pointerEventData.clickCount = 1;
					num = -675990809;
					continue;
				case 10:
					num3 = unscaledTime - pointerEventData.clickTime;
					num = -675990798;
					continue;
				case 18:
					pointerEventData.clickCount++;
					num = -675990811;
					continue;
				case 15:
					pointerEventData.clickCount = 1;
					num = -675990811;
					continue;
				case 1:
					if (num3 < 0.3f)
					{
						pointerEventData.clickCount++;
						num = -675990809;
						continue;
					}
					goto case 0;
				case 21:
					pointerEventData.pointerPress = gameObject2;
					pointerEventData.rawPointerPress = gameObject;
					pointerEventData.clickTime = unscaledTime;
					pointerEventData.pointerDrag = gameObject;
					num = -675990799;
					continue;
				case 7:
					return null;
				case 14:
					pointerEventData.pointerPress = gameObject3;
					pointerEventData.rawPointerPress = gameObject;
					pointerEventData.clickTime = unscaledTime2;
					pointerEventData.pointerDrag = gameObject;
					goto case 2;
				case 11:
				{
					pointerEventData.pointerPressRaycast = pointerEventData.pointerCurrentRaycast;
					gameObject2 = P_1;
					unscaledTime = Time.unscaledTime;
					int num6;
					if (gameObject2 == pointerEventData.lastPress)
					{
						num = -675990791;
						num6 = num;
					}
					else
					{
						num = -675990793;
						num6 = num;
					}
					continue;
				}
				case 13:
				{
					float num4 = unscaledTime2 - pointerEventData.clickTime;
					int num5;
					if (num4 < 0.3f)
					{
						num = -675990815;
						num5 = num;
					}
					else
					{
						num = -675990788;
						num5 = num;
					}
					continue;
				}
				case 5:
					pointerEventData.clickCount = 1;
					num = -675990787;
					continue;
				case 4:
					pointerEventData.clickCount = 1;
					num = -675990810;
					continue;
				case 8:
				{
					gameObject3 = P_1;
					unscaledTime2 = Time.unscaledTime;
					int num2;
					if (gameObject3 == pointerEventData.lastPress)
					{
						num = -675990786;
						num2 = num;
					}
					else
					{
						num = -675990794;
						num2 = num;
					}
					continue;
				}
				case 20:
					pointerEventData.clickTime = unscaledTime;
					num = -675990810;
					continue;
				case 22:
					pointerEventData.clickTime = unscaledTime2;
					num = -675990787;
					continue;
				case 19:
					if (pointerEventData.pointerEnter != gameObject)
					{
						pointerEventData.pointerEnter = gameObject;
						num = -675990789;
						continue;
					}
					goto case 8;
				case 3:
					pointerEventData.dragging = false;
					pointerEventData.useDragThreshold = true;
					num = -675990785;
					continue;
				case 16:
					if (TouchInteractable.OenwdeSUZHKJsTIxPjYHJUNnctC(P_0))
					{
						pointerEventData.eligibleForClick = true;
						pointerEventData.delta = Vector2.zero;
						num = -675990800;
						continue;
					}
					goto default;
				case 6:
					if (TouchInteractable.BjWUubnUnTBxMVzCyvrMCwQMuce(P_0))
					{
						pointerEventData.eligibleForClick = true;
						pointerEventData.delta = Vector2.zero;
						pointerEventData.dragging = false;
						pointerEventData.useDragThreshold = true;
						pointerEventData.pressPosition = pointerEventData.position;
						pointerEventData.pointerPressRaycast = pointerEventData.pointerCurrentRaycast;
						num = -675990816;
						continue;
					}
					goto case 16;
				default:
					Logger.LogWarning("Unsupported pointerId: " + P_0);
					return null;
				case 2:
					return pointerEventData;
				}
				break;
			}
			goto IL_000e;
		}

		private PointerEventData VSPCPJdlAOfJpTUOqerfRqxddALm(int P_0)
		{
			//Discarded unreachable code: IL_00a4, IL_00b7
			PointerEventData pointerEventData = lYycfThbLcNDrXoSoMNFZmblhiW(P_0);
			if (pointerEventData == null)
			{
				return null;
			}
			if (TouchInteractable.BjWUubnUnTBxMVzCyvrMCwQMuce(P_0))
			{
				pointerEventData.eligibleForClick = false;
				pointerEventData.pointerPress = null;
				goto IL_0023;
			}
			goto IL_0062;
			IL_0023:
			int num = 24681883;
			goto IL_0028;
			IL_00c1:
			Logger.LogWarning("Unsupported pointerId: " + P_0);
			num = 24681881;
			goto IL_0028;
			IL_0028:
			while (true)
			{
				switch (num ^ 0x1789D9A)
				{
				case 0:
					break;
				case 4:
					pointerEventData.dragging = false;
					num = 24681880;
					continue;
				case 5:
					goto IL_0062;
				case 1:
					pointerEventData.rawPointerPress = null;
					pointerEventData.dragging = false;
					pointerEventData.pointerDrag = null;
					pointerEventData.pointerEnter = null;
					goto IL_00e2;
				case 2:
					pointerEventData.pointerDrag = null;
					goto IL_00e2;
				case 6:
					goto IL_00c1;
				default:
					{
						return null;
					}
					IL_00e2:
					return pointerEventData;
				}
				break;
			}
			goto IL_0023;
			IL_0062:
			if (TouchInteractable.OenwdeSUZHKJsTIxPjYHJUNnctC(P_0))
			{
				pointerEventData.eligibleForClick = false;
				pointerEventData.pointerPress = null;
				pointerEventData.rawPointerPress = null;
				num = 24681886;
				goto IL_0028;
			}
			goto IL_00c1;
		}

		private void rNlfOZEoSimQmBIeJxNKvxgjMQsF(PointerEventData P_0)
		{
			if (P_0 != null)
			{
				OnPointerUp(P_0);
				VSPCPJdlAOfJpTUOqerfRqxddALm(effectivePointerId);
			}
		}

		private PointerEventData lYycfThbLcNDrXoSoMNFZmblhiW(int P_0)
		{
			//Discarded unreachable code: IL_00b8
			if (P_0 == int.MinValue)
			{
				goto IL_000b;
			}
			int num;
			if (sfxtihDClNcjhPUlTfnZDnVMdzt == null)
			{
				sfxtihDClNcjhPUlTfnZDnVMdzt = new Dictionary<int, PointerEventData>();
				num = 1804325403;
				goto IL_0010;
			}
			goto IL_00c2;
			IL_0010:
			PointerEventData value = default(PointerEventData);
			PointerEventData.InputButton button = default(PointerEventData.InputButton);
			while (true)
			{
				switch (num ^ 0x6B8BD21A)
				{
				case 4:
					break;
				case 8:
					value.pointerId = P_0;
					num = 1804325400;
					continue;
				case 9:
					button = PointerEventData.InputButton.Left;
					num = 1804325407;
					continue;
				case 0:
					switch (P_0)
					{
					case -1:
						break;
					default:
						goto IL_0082;
					case -3:
						goto IL_00e6;
					case -2:
						goto IL_0137;
					}
					goto case 9;
				case 2:
					goto IL_0089;
				case 6:
					throw new NotImplementedException();
				case 1:
					goto IL_00c2;
				case 12:
					goto IL_00e6;
				case 11:
					value = new PointerEventData(EventSystem.current);
					num = 1804325394;
					continue;
				case 5:
					value.button = button;
					num = 1804325392;
					continue;
				case 3:
					return null;
				case 7:
					goto IL_0137;
				default:
					{
						return value;
					}
					IL_0137:
					button = PointerEventData.InputButton.Right;
					num = 1804325407;
					continue;
					IL_00e6:
					button = PointerEventData.InputButton.Middle;
					num = 1804325407;
					continue;
					IL_0082:
					num = 1804325404;
					continue;
				}
				break;
				IL_0089:
				sfxtihDClNcjhPUlTfnZDnVMdzt.Add(P_0, value);
				int num2;
				if (TouchInteractable.OenwdeSUZHKJsTIxPjYHJUNnctC(P_0))
				{
					num = 1804325402;
					num2 = num;
				}
				else
				{
					num = 1804325392;
					num2 = num;
				}
			}
			goto IL_000b;
			IL_000b:
			num = 1804325401;
			goto IL_0010;
			IL_00c2:
			int num3;
			if (sfxtihDClNcjhPUlTfnZDnVMdzt.TryGetValue(P_0, out value))
			{
				num = 1804325392;
				num3 = num;
			}
			else
			{
				num = 1804325393;
				num3 = num;
			}
			goto IL_0010;
		}

		private void CLppEvNomWTxgFFClMmyWJkbdN(PointerEventData P_0, wpUgJeGUsoSRewkpBKJyJpvJjLv P_1)
		{
			//Discarded unreachable code: IL_0039
			if (hasPointer)
			{
				goto IL_0008;
			}
			goto IL_0040;
			IL_0040:
			int num;
			if (agGGiexlYIVgemjaHHzzcCmJTcc() && IsInteractable())
			{
				dkbPRumzMHESljvUzfPJVvaxLFt(P_0.pointerId, P_0.pressPosition, P_1);
				num = -441738733;
				goto IL_000d;
			}
			goto IL_006a;
			IL_006a:
			base.OnPointerDown(P_0);
			return;
			IL_0008:
			num = -441738736;
			goto IL_000d;
			IL_000d:
			switch (num ^ -441738734)
			{
			case 0:
				break;
			case 2:
				if (!wzNIDgakHttMmiiBhPwkMJrPBdB(P_0.pointerId))
				{
					return;
				}
				goto IL_0040;
			case 3:
				goto IL_0040;
			default:
				goto IL_006a;
			}
			goto IL_0008;
		}

		private void bsJnZKaMLeQiOiwSuuiDcemAIrg(PointerEventData P_0, wpUgJeGUsoSRewkpBKJyJpvJjLv P_1)
		{
			//Discarded unreachable code: IL_003d, IL_0063
			if (hasPointer && !wzNIDgakHttMmiiBhPwkMJrPBdB(P_0.pointerId))
			{
				goto IL_0016;
			}
			goto IL_0044;
			IL_0016:
			int num = -1685312256;
			goto IL_001b;
			IL_0044:
			int num2;
			if (!TouchInteractable.MUAEmHanKCAekGtjjVciojDFRqNV(effectivePointerId))
			{
				num = -1685312249;
				num2 = num;
			}
			else
			{
				num = -1685312251;
				num2 = num;
			}
			goto IL_001b;
			IL_001b:
			switch (num ^ -1685312252)
			{
			case 1:
				return;
			case 4:
				return;
			case 2:
				break;
			case 0:
				goto IL_0044;
			default:
				wAYSKMoYLXrHlKmKvvdfOiGBjye();
				base.OnPointerUp(P_0);
				return;
			}
			goto IL_0016;
		}

		private void jgmhCVCLNpdugCcpOmRLAZQAAUv(PointerEventData P_0, wpUgJeGUsoSRewkpBKJyJpvJjLv P_1)
		{
			//Discarded unreachable code: IL_0082, IL_00e5, IL_00f5
			if (hasPointer)
			{
				goto IL_000b;
			}
			goto IL_01b7;
			IL_01b7:
			bool flag = TouchInteractable.OenwdeSUZHKJsTIxPjYHJUNnctC(P_0.pointerId);
			bool flag2 = false;
			switch (P_1)
			{
			case wpUgJeGUsoSRewkpBKJyJpvJjLv.PClOLQRhgHgGSKopqNIgjbqoRgUa:
				break;
			case wpUgJeGUsoSRewkpBKJyJpvJjLv.dHfPQraqXaVZHuxcjEqIITHISaN:
				goto IL_0127;
			default:
				goto IL_01d7;
			}
			goto IL_00ce;
			IL_01d7:
			int num = -544285326;
			goto IL_0010;
			IL_00ce:
			MouseButtonFlags allowedMouseButtons = base.allowedMouseButtons;
			num = -544285323;
			goto IL_0010;
			IL_0010:
			PointerEventData pointerEventData = default(PointerEventData);
			GameObject gameObject = default(GameObject);
			while (true)
			{
				switch (num ^ -544285325)
				{
				case 0:
					break;
				case 18:
					if (!wzNIDgakHttMmiiBhPwkMJrPBdB(P_0.pointerId))
					{
						return;
					}
					goto IL_01b7;
				case 2:
					CLppEvNomWTxgFFClMmyWJkbdN(pointerEventData, P_1);
					num = -544285344;
					continue;
				case 6:
					goto IL_009c;
				case 8:
					goto IL_00ce;
				case 14:
					throw new NotImplementedException();
				case 1:
					throw new NotImplementedException();
				case 7:
					flag2 = true;
					num = -544285341;
					continue;
				case 3:
					num = -544285315;
					continue;
				case 9:
					gameObject = base.gameObject;
					num = -544285322;
					continue;
				case 15:
					goto IL_0127;
				case 5:
					goto IL_013d;
				case 11:
					if ((!flag || TouchInteractable.jWwDMVskgRDHPeFKiMdRILlfbbMg(allowedMouseButtons)) && !yrfVZiGUTfOClpVIfIvNRqImIGGe)
					{
						if (!flag)
						{
							goto case 7;
						}
						if (TouchInteractable.rXoHlhXIkgBrHzajLhaabUWFwEkO(allowedMouseButtons, out int num2))
						{
							tfmoiLbwVkRzJNaMHoSyQrJPizV = num2;
							num = -544285321;
							continue;
						}
						goto case 13;
					}
					goto IL_0218;
				case 10:
					goto IL_01b7;
				case 12:
					goto IL_01e1;
				case 13:
					tfmoiLbwVkRzJNaMHoSyQrJPizV = P_0.pointerId;
					num = -544285324;
					continue;
				case 4:
					num = -544285324;
					continue;
				case 16:
					goto IL_0218;
				case 17:
					switch (P_1)
					{
					case wpUgJeGUsoSRewkpBKJyJpvJjLv.PClOLQRhgHgGSKopqNIgjbqoRgUa:
						break;
					case wpUgJeGUsoSRewkpBKJyJpvJjLv.dHfPQraqXaVZHuxcjEqIITHISaN:
						goto IL_01e1;
					default:
						goto IL_0248;
					}
					goto case 9;
				default:
					{
						BSbeTqiFrdRDtCmjwklbhCLtNiqg = true;
						return;
					}
					IL_0248:
					num = -544285328;
					continue;
					IL_01e1:
					gameObject = pTsRyfjQnsgwofrHFcNPaYsdQuF.gameObject;
					num = -544285322;
					continue;
				}
				break;
				IL_013d:
				pointerEventData = KJdskTUpjJuElEhrvEVGouQMzgV((tfmoiLbwVkRzJNaMHoSyQrJPizV != int.MinValue) ? tfmoiLbwVkRzJNaMHoSyQrJPizV : P_0.pointerId, gameObject);
				int num3;
				if (pointerEventData == null)
				{
					num = -544285344;
					num3 = num;
				}
				else
				{
					num = -544285327;
					num3 = num;
				}
				continue;
				IL_009c:
				if (_activateOnSwipeIn && agGGiexlYIVgemjaHHzzcCmJTcc())
				{
					int num4;
					if (IsInteractable())
					{
						num = -544285320;
						num4 = num;
					}
					else
					{
						num = -544285341;
						num4 = num;
					}
					continue;
				}
				goto IL_0218;
				IL_0218:
				base.OnPointerEnter(P_0);
				int num5;
				if (!flag2)
				{
					num = -544285344;
					num5 = num;
				}
				else
				{
					num = -544285342;
					num5 = num;
				}
			}
			goto IL_000b;
			IL_000b:
			num = -544285343;
			goto IL_0010;
			IL_0127:
			allowedMouseButtons = _touchRegion.allowedMouseButtons;
			num = -544285323;
			goto IL_0010;
		}

		private void LGSqRyvgmhSuGkKYGWZieVFROaZ(PointerEventData P_0, wpUgJeGUsoSRewkpBKJyJpvJjLv P_1)
		{
			//Discarded unreachable code: IL_004f
			if (hasPointer)
			{
				goto IL_000b;
			}
			goto IL_0094;
			IL_0094:
			int num;
			int num2;
			if (stayActiveOnSwipeOut)
			{
				num = -67761935;
				num2 = num;
			}
			else
			{
				num = -67761933;
				num2 = num;
			}
			goto IL_0010;
			IL_0010:
			while (true)
			{
				switch (num ^ -67761931)
				{
				default:
					return;
				case 5:
					return;
				case 0:
					break;
				case 2:
					if (!wzNIDgakHttMmiiBhPwkMJrPBdB(P_0.pointerId))
					{
						base.OnPointerExit(P_0);
						return;
					}
					goto IL_0094;
				case 3:
					wAYSKMoYLXrHlKmKvvdfOiGBjye();
					num = -67761935;
					continue;
				case 4:
					base.OnPointerExit(P_0);
					BSbeTqiFrdRDtCmjwklbhCLtNiqg = false;
					num = -67761936;
					continue;
				case 6:
					goto IL_0078;
				case 1:
					goto IL_0094;
				}
				break;
				IL_0078:
				int num3;
				if (!yrfVZiGUTfOClpVIfIvNRqImIGGe)
				{
					num = -67761935;
					num3 = num;
				}
				else
				{
					num = -67761930;
					num3 = num;
				}
			}
			goto IL_000b;
			IL_000b:
			num = -67761929;
			goto IL_0010;
		}

		private void dkbPRumzMHESljvUzfPJVvaxLFt(int P_0, Vector2 P_1, wpUgJeGUsoSRewkpBKJyJpvJjLv P_2)
		{
			YfuiRllWCyNAOiNefrgAuQetcTCD = P_0;
			while (true)
			{
				int num = -1222446821;
				while (true)
				{
					switch (num ^ -1222446824)
					{
					case 4:
						break;
					case 3:
						yrfVZiGUTfOClpVIfIvNRqImIGGe = true;
						if (_followTouchPosition)
						{
							sQQUfUMgAhuobjmJmIobItMuoeE(P_0);
							num = -1222446823;
							continue;
						}
						goto case 2;
					case 0:
						RBwZttPSZcSXhOFMwihqtJLzOqE(P_1, _animateOnMoveToTouch, _moveToTouchSpeed, iYMiiqNhvRvuajtUmoNwBOObbph.vZJAaAAZjhfVfUmYHsWMcjdbLrND);
						num = -1222446818;
						continue;
					case 5:
					{
						int num2;
						if (!_moveToTouchPosition)
						{
							num = -1222446818;
							num2 = num;
						}
						else
						{
							num = -1222446824;
							num2 = num;
						}
						continue;
					}
					case 1:
						num = -1222446818;
						continue;
					case 2:
					{
						int num3;
						if (P_2 == wpUgJeGUsoSRewkpBKJyJpvJjLv.dHfPQraqXaVZHuxcjEqIITHISaN)
						{
							num = -1222446819;
							num3 = num;
						}
						else
						{
							num = -1222446818;
							num3 = num;
						}
						continue;
					}
					default:
						TIVHNkuERQEgFirHrNNwifqaPItH();
						return;
					}
					break;
				}
			}
		}

		private void wAYSKMoYLXrHlKmKvvdfOiGBjye()
		{
			BgAdqUvZFqBlqudhjyphLkDsMtm();
			yrfVZiGUTfOClpVIfIvNRqImIGGe = false;
			if (!_followTouchPosition)
			{
				goto IL_0015;
			}
			goto IL_0050;
			IL_0050:
			int num;
			if (_returnOnRelease && GlEEcbCNWbgxmTRepbQqNmvhAgtA)
			{
				ReturnToDefaultPosition();
				num = 292675650;
				goto IL_001a;
			}
			goto IL_006d;
			IL_0015:
			num = 292675651;
			goto IL_001a;
			IL_006d:
			JZAzIivJqeEMSaTjWEtxNWCcplcg();
			return;
			IL_001a:
			while (true)
			{
				switch (num ^ 0x1171E042)
				{
				case 2:
					break;
				case 1:
					goto IL_0037;
				case 3:
					goto IL_0050;
				default:
					goto IL_006d;
				}
				break;
				IL_0037:
				int num2;
				if (_moveToTouchPosition)
				{
					num = 292675649;
					num2 = num;
				}
				else
				{
					num = 292675650;
					num2 = num;
				}
			}
			goto IL_0015;
		}

		internal override void OnPointerDown(PointerEventData eventData)
		{
			//Discarded unreachable code: IL_0033, IL_0060, IL_007e
			if (!base.initialized)
			{
				goto IL_0008;
			}
			goto IL_003a;
			IL_003a:
			int num;
			int num2;
			if (TouchInteractable.gtAdYPaYTSdxlqopRartpbSncsib(eventData.pointerId, base.allowedMouseButtons, EventTriggerType.PointerDown))
			{
				num = -592718910;
				num2 = num;
			}
			else
			{
				num = -592718911;
				num2 = num;
			}
			goto IL_000d;
			IL_000d:
			switch (num ^ -592718910)
			{
			case 3:
				return;
			case 5:
				return;
			case 2:
				break;
			case 4:
				goto IL_003a;
			case 0:
				if (pTsRyfjQnsgwofrHFcNPaYsdQuF != null && _useTouchRegionOnly)
				{
					return;
				}
				goto default;
			default:
				CLppEvNomWTxgFFClMmyWJkbdN(eventData, wpUgJeGUsoSRewkpBKJyJpvJjLv.PClOLQRhgHgGSKopqNIgjbqoRgUa);
				return;
			}
			goto IL_0008;
			IL_0008:
			num = -592718905;
			goto IL_000d;
		}

		internal override void OnPointerUp(PointerEventData eventData)
		{
			//Discarded unreachable code: IL_0093, IL_009e
			if (!base.initialized)
			{
				return;
			}
			while (TouchInteractable.gtAdYPaYTSdxlqopRartpbSncsib(eventData.pointerId, base.allowedMouseButtons, EventTriggerType.PointerUp))
			{
				while (true)
				{
					IL_0050:
					int num;
					int num2;
					if (!(pTsRyfjQnsgwofrHFcNPaYsdQuF != null))
					{
						num = 618809769;
						num2 = num;
					}
					else
					{
						num = 618809768;
						num2 = num;
					}
					while (true)
					{
						switch (num ^ 0x24E249A8)
						{
						default:
							return;
						case 3:
							return;
						case 5:
							return;
						case 2:
							num = 618809774;
							continue;
						case 0:
							break;
						case 4:
							goto IL_0050;
						case 1:
							bsJnZKaMLeQiOiwSuuiDcemAIrg(eventData, wpUgJeGUsoSRewkpBKJyJpvJjLv.PClOLQRhgHgGSKopqNIgjbqoRgUa);
							num = 618809773;
							continue;
						case 6:
							goto IL_007e;
						}
						int num3;
						if (!_useTouchRegionOnly)
						{
							num = 618809769;
							num3 = num;
						}
						else
						{
							num = 618809771;
							num3 = num;
						}
					}
				}
				IL_007e:;
			}
		}

		internal override void OnPointerEnter(PointerEventData eventData)
		{
			//Discarded unreachable code: IL_0009, IL_000e, IL_0029, IL_0040, IL_005e
			if (base.initialized && TouchInteractable.gtAdYPaYTSdxlqopRartpbSncsib(eventData.pointerId, base.allowedMouseButtons, EventTriggerType.PointerEnter) && (!(pTsRyfjQnsgwofrHFcNPaYsdQuF != null) || !_useTouchRegionOnly))
			{
				jgmhCVCLNpdugCcpOmRLAZQAAUv(eventData, wpUgJeGUsoSRewkpBKJyJpvJjLv.PClOLQRhgHgGSKopqNIgjbqoRgUa);
			}
		}

		internal override void OnPointerExit(PointerEventData eventData)
		{
			//Discarded unreachable code: IL_0009, IL_000e, IL_0029, IL_0040, IL_005e
			if (base.initialized && TouchInteractable.gtAdYPaYTSdxlqopRartpbSncsib(eventData.pointerId, base.allowedMouseButtons, EventTriggerType.PointerExit) && (!(pTsRyfjQnsgwofrHFcNPaYsdQuF != null) || !_useTouchRegionOnly))
			{
				LGSqRyvgmhSuGkKYGWZieVFROaZ(eventData, wpUgJeGUsoSRewkpBKJyJpvJjLv.PClOLQRhgHgGSKopqNIgjbqoRgUa);
			}
		}

		private void NSaCJNLNcGocStUSwnnYWnwuvob(PointerEventData P_0)
		{
			//Discarded unreachable code: IL_0009, IL_000e, IL_0025, IL_0041
			if (base.initialized && TouchInteractable.gtAdYPaYTSdxlqopRartpbSncsib(P_0.pointerId, _touchRegion.allowedMouseButtons, EventTriggerType.PointerDown))
			{
				CLppEvNomWTxgFFClMmyWJkbdN(P_0, wpUgJeGUsoSRewkpBKJyJpvJjLv.dHfPQraqXaVZHuxcjEqIITHISaN);
			}
		}

		private void slcdeSGvhTIGRUXtiFKCcBoVLShw(PointerEventData P_0)
		{
			//Discarded unreachable code: IL_0045
			if (!base.initialized)
			{
				return;
			}
			while (TouchInteractable.gtAdYPaYTSdxlqopRartpbSncsib(P_0.pointerId, _touchRegion.allowedMouseButtons, EventTriggerType.PointerUp))
			{
				while (true)
				{
					IL_004c:
					bsJnZKaMLeQiOiwSuuiDcemAIrg(P_0, wpUgJeGUsoSRewkpBKJyJpvJjLv.dHfPQraqXaVZHuxcjEqIITHISaN);
					int num = -267497842;
					while (true)
					{
						switch (num ^ -267497843)
						{
						default:
							return;
						case 3:
							return;
						case 0:
							num = -267497841;
							continue;
						case 2:
							break;
						case 1:
							goto IL_004c;
						}
						break;
					}
					break;
				}
			}
		}

		private void qrwJpuJXUnJctsmgikDbkDazTTD(PointerEventData P_0)
		{
			//Discarded unreachable code: IL_0056
			if (!base.initialized)
			{
				return;
			}
			while (true)
			{
				int num;
				int num2;
				if (TouchInteractable.gtAdYPaYTSdxlqopRartpbSncsib(P_0.pointerId, _touchRegion.allowedMouseButtons, EventTriggerType.PointerEnter))
				{
					num = -1332534679;
					num2 = num;
				}
				else
				{
					num = -1332534678;
					num2 = num;
				}
				while (true)
				{
					switch (num ^ -1332534678)
					{
					case 0:
						return;
					case 2:
						goto IL_0009;
					case 1:
						break;
					default:
						jgmhCVCLNpdugCcpOmRLAZQAAUv(P_0, wpUgJeGUsoSRewkpBKJyJpvJjLv.dHfPQraqXaVZHuxcjEqIITHISaN);
						return;
					}
					break;
					IL_0009:
					num = -1332534677;
				}
			}
		}

		private void rklTaLGzgkBquoJTvHXoKHosDCt(PointerEventData P_0)
		{
			//Discarded unreachable code: IL_002f, IL_005f
			if (!base.initialized)
			{
				goto IL_0008;
			}
			goto IL_0045;
			IL_0045:
			if (!TouchInteractable.gtAdYPaYTSdxlqopRartpbSncsib(P_0.pointerId, _touchRegion.allowedMouseButtons, EventTriggerType.PointerExit))
			{
				return;
			}
			goto IL_0036;
			IL_0036:
			LGSqRyvgmhSuGkKYGWZieVFROaZ(P_0, wpUgJeGUsoSRewkpBKJyJpvJjLv.dHfPQraqXaVZHuxcjEqIITHISaN);
			int num = 260087280;
			goto IL_000d;
			IL_000d:
			switch (num ^ 0xF809DF3)
			{
			default:
				return;
			case 1:
				return;
			case 3:
				return;
			case 0:
				break;
			case 2:
				goto IL_0036;
			case 4:
				goto IL_0045;
			}
			goto IL_0008;
			IL_0008:
			num = 260087282;
			goto IL_000d;
		}

		private void ErAspGeeDMmNWjmJeGHrjQHrKNo(float P_0)
		{
			//Discarded unreachable code: IL_002f, IL_003f
			if (!base.initialized)
			{
				goto IL_0008;
			}
			goto IL_0036;
			IL_0036:
			if (_useDigitalAxisSimulation)
			{
				return;
			}
			goto IL_0046;
			IL_0046:
			sjfOHMxvWyaFrJNCKmQvSrptfRJ(null);
			int num = -1825174371;
			goto IL_000d;
			IL_000d:
			switch (num ^ -1825174370)
			{
			case 4:
				return;
			case 0:
				break;
			case 1:
				goto IL_0036;
			case 2:
				goto IL_0046;
			default:
				_onAxisValueChanged.Invoke(P_0);
				return;
			}
			goto IL_0008;
			IL_0008:
			num = -1825174374;
			goto IL_000d;
		}

		private void UKyYyMcDdRHoCPbtntTMPiMFhNRD(bool P_0)
		{
			//Discarded unreachable code: IL_0027
			if (!base.initialized)
			{
				while (true)
				{
					switch (0x3230BFB1 ^ 0x3230BFB0)
					{
					case 1:
						return;
					case 0:
						continue;
					}
					break;
				}
			}
			sjfOHMxvWyaFrJNCKmQvSrptfRJ(null);
			_onButtonValueChanged.Invoke(P_0);
		}

		private void DdWGleCossJYefolfWVwHWAFzGXJ()
		{
			if (base.initialized)
			{
				sjfOHMxvWyaFrJNCKmQvSrptfRJ(null);
				_onButtonDown.Invoke();
			}
		}

		private void mZnDrZYlkFuqrIDLdYRIsjCyvtr()
		{
			//Discarded unreachable code: IL_0027
			if (!base.initialized)
			{
				while (true)
				{
					switch (-1474865091 ^ -1474865092)
					{
					case 1:
						return;
					case 2:
						continue;
					}
					break;
				}
			}
			sjfOHMxvWyaFrJNCKmQvSrptfRJ(null);
			_onButtonUp.Invoke();
		}
	}
}
