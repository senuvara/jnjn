using Rewired.ComponentControls.Data;
using Rewired.Internal;
using Rewired.Utils;
using System;
using UnityEngine;

namespace Rewired.ComponentControls
{
	[Serializable]
	[DisallowMultipleComponent]
	public sealed class TiltControl : CustomControllerControl
	{
		public enum TiltDirection
		{
			Both,
			Horizontal,
			Forward
		}

		private const float maxFullTiltAngle = 180f;

		private const float maxAngleOffset = 90f;

		[Tooltip("The tilt directions in which movement is allowed. You can restrict movement to one or both directions.")]
		[CustomObfuscation(rename = false)]
		[SerializeField]
		private TiltDirection _allowedTiltDirections;

		[CustomObfuscation(rename = false)]
		[SerializeField]
		[Tooltip("The Custom Controller element that will receive input values from the X axis.")]
		private CustomControllerElementTargetSetForFloat _horizontalTiltCustomControllerElement = new CustomControllerElementTargetSetForFloat();

		[Range(0f, 180f)]
		[CustomObfuscation(rename = false)]
		[Tooltip("The maximum horizontal tilt angle in degrees. When the device is tilted to this angle or further in either direction, the axis will return a value of 1/-1.")]
		[SerializeField]
		private float _horizontalTiltLimit = 25f;

		[CustomObfuscation(rename = false)]
		[SerializeField]
		[Range(-90f, 90f)]
		[Tooltip("The offset angle from horizontal which will be considered the resting angle. This represents the angle at which the user holds the device without generating tilt.")]
		private float _horizontalRestAngle;

		[CustomObfuscation(rename = false)]
		[SerializeField]
		[Tooltip("The Custom Controller element that will receive input values from the Y axis.")]
		private CustomControllerElementTargetSetForFloat _forwardTiltCustomControllerElement = new CustomControllerElementTargetSetForFloat();

		[Tooltip("The maximum forward tilt angle in degrees. When the device is tilted to this angle or further in either direction, the axis will return a value of 1/-1.")]
		[Range(0f, 180f)]
		[CustomObfuscation(rename = false)]
		[SerializeField]
		private float _forwardTiltLimit = 25f;

		[CustomObfuscation(rename = false)]
		[Tooltip("The offset angle from vertical which will be considered the resting angle. This represents the angle at which the user holds the device without generating tilt. A typical value would be around 40 degrees.")]
		[Range(-90f, 90f)]
		[SerializeField]
		private float _forwardRestAngle = 40f;

		[Tooltip("The underlying 2D axis.")]
		[CustomObfuscation(rename = false)]
		[SerializeField]
		private StandaloneAxis2D _axis2D = new StandaloneAxis2D();

		private bool _useHAxis;

		private bool _useFAxis;

		private Func<Vector3> _getAccelerationValue;

		public TiltDirection axesToUse
		{
			get
			{
				return _allowedTiltDirections;
			}
			set
			{
				if (_allowedTiltDirections != value)
				{
					iSfJagPsCsVhUfDNyhNvbxLabufM(value);
					OnSetProperty();
				}
			}
		}

		public CustomControllerElementTargetSetForFloat horizontalTiltCustomControllerElement => _horizontalTiltCustomControllerElement;

		public float horizontalTiltLimit
		{
			get
			{
				return _horizontalTiltLimit;
			}
			set
			{
				value = MathTools.Clamp(value, 0f, 180f);
				if (_horizontalTiltLimit != value)
				{
					_horizontalTiltLimit = value;
					OnSetProperty();
				}
			}
		}

		public float horizontalRestAngle
		{
			get
			{
				return _horizontalRestAngle;
			}
			set
			{
				//Discarded unreachable code: IL_003a
				value = MathTools.Clamp(value, -90f, 90f);
				while (true)
				{
					switch (-1109853847 ^ -1109853845)
					{
					case 0:
						continue;
					case 2:
						if (_horizontalRestAngle == value)
						{
							return;
						}
						break;
					}
					break;
				}
				_horizontalRestAngle = value;
				OnSetProperty();
			}
		}

		public CustomControllerElementTargetSetForFloat forwardTiltCustomControllerElement => _forwardTiltCustomControllerElement;

		public float forwardTiltLimit
		{
			get
			{
				return _forwardTiltLimit;
			}
			set
			{
				//Discarded unreachable code: IL_0042
				value = MathTools.Clamp(value, 0f, 180f);
				while (true)
				{
					int num = 1751949631;
					while (true)
					{
						switch (num ^ 0x686CA13D)
						{
						default:
							return;
						case 0:
							return;
						case 4:
							break;
						case 2:
							if (_forwardTiltLimit == value)
							{
								return;
							}
							goto case 3;
						case 1:
							OnSetProperty();
							num = 1751949629;
							continue;
						case 3:
							_forwardTiltLimit = value;
							num = 1751949628;
							continue;
						}
						break;
					}
				}
			}
		}

		public float forwardRestAngle
		{
			get
			{
				return _forwardRestAngle;
			}
			set
			{
				value = MathTools.Clamp(value, -90f, 90f);
				if (_forwardRestAngle != value)
				{
					_forwardRestAngle = value;
					OnSetProperty();
				}
			}
		}

		public AxisCalibration horizontalAxisCalibration => _axis2D.xAxis.calibration;

		public AxisCalibration verticalAxisCalibration => _axis2D.yAxis.calibration;

		public Axis2DCalibration deadZoneType => _axis2D.calibration;

		internal StandaloneAxis2D axis2D => _axis2D;

		private Vector3 acceleration
		{
			get
			{
				if (_getAccelerationValue == null)
				{
					return Input.acceleration;
				}
				return _getAccelerationValue();
			}
		}

		[CustomObfuscation(rename = false)]
		internal TiltControl()
		{
		}

		public void SetAccelerationSourceCallback(Func<Vector3> callback)
		{
			_getAccelerationValue = callback;
		}

		public void SetRestOrientation()
		{
			Vector3 acceleration = this.acceleration;
			horizontalRestAngle = Mathf.Atan2(acceleration.x, 0f - acceleration.y) * 57.29578f * -1f;
			forwardRestAngle = Mathf.Atan2(acceleration.z, 0f - acceleration.y) * 57.29578f * -1f;
		}

		[CustomObfuscation(rename = false)]
		internal override void OnValidate()
		{
			base.OnValidate();
			if (base.initialized)
			{
				RSVeklVKOyscZAgLkkmEvJazvbE();
			}
		}

		internal override bool OnInitialize()
		{
			if (!base.OnInitialize())
			{
				return false;
			}
			RSVeklVKOyscZAgLkkmEvJazvbE();
			return true;
		}

		internal override void OnUpdate()
		{
			//Discarded unreachable code: IL_002d
			base.OnUpdate();
			if (!base.initialized)
			{
				while (true)
				{
					switch (0x66FA4CB3 ^ 0x66FA4CB1)
					{
					case 2:
						return;
					case 0:
						continue;
					}
					break;
				}
			}
			vwVDpWhYKYIMjDYLwclFpOJccTNZ();
		}

		internal override void OnCustomControllerUpdate()
		{
			//Discarded unreachable code: IL_0039, IL_00c5
			if (!base.initialized)
			{
				goto IL_000b;
			}
			goto IL_00bc;
			IL_00bc:
			if (!hasController)
			{
				return;
			}
			goto IL_007e;
			IL_007e:
			int num;
			if (_useFAxis)
			{
				yQjPzGJGxRgoFPaCGxcgBaQECRD(_forwardTiltCustomControllerElement, _axis2D.yAxis.value, _axis2D.yAxis.buttonActivationThreshold);
				num = -1341273169;
				goto IL_0010;
			}
			goto IL_0040;
			IL_0010:
			switch (num ^ -1341273172)
			{
			default:
				return;
			case 1:
				return;
			case 2:
				return;
			case 0:
				break;
			case 3:
				goto IL_0040;
			case 4:
				goto IL_007e;
			case 5:
				goto IL_00bc;
			}
			goto IL_000b;
			IL_0040:
			if (_useHAxis)
			{
				yQjPzGJGxRgoFPaCGxcgBaQECRD(_horizontalTiltCustomControllerElement, _axis2D.xAxis.value, _axis2D.xAxis.buttonActivationThreshold);
				num = -1341273170;
				goto IL_0010;
			}
			return;
			IL_000b:
			num = -1341273171;
			goto IL_0010;
		}

		public override void ClearValue()
		{
			_axis2D.xAxis.Clear();
			while (true)
			{
				int num = 764656610;
				while (true)
				{
					switch (num ^ 0x2D93BBE0)
					{
					default:
						return;
					case 1:
						return;
					case 0:
						break;
					case 2:
						_axis2D.yAxis.Clear();
						if (hasController)
						{
							goto IL_0046;
						}
						return;
					}
					break;
					IL_0046:
					base.controller.ClearElementValue(_horizontalTiltCustomControllerElement);
					base.controller.ClearElementValue(_forwardTiltCustomControllerElement);
					num = 764656609;
				}
			}
		}

		private void vwVDpWhYKYIMjDYLwclFpOJccTNZ()
		{
			if (_useHAxis)
			{
				goto IL_0008;
			}
			goto IL_0041;
			IL_0041:
			float num = default(float);
			int num2;
			if (_useFAxis)
			{
				if (acceleration == Vector3.zero)
				{
					num = 0f;
					num2 = -1779065717;
					goto IL_000d;
				}
				goto IL_009f;
			}
			return;
			IL_000d:
			float value = default(float);
			float rawValue = default(float);
			while (true)
			{
				switch (num2 ^ -1779065716)
				{
				default:
					return;
				case 2:
					return;
				case 4:
					break;
				case 3:
					goto IL_0041;
				case 5:
					value = Mathf.Atan2(acceleration.x, 0f - acceleration.y) * 57.29578f + _horizontalRestAngle;
					num2 = -1779065716;
					continue;
				case 6:
					goto IL_009f;
				case 8:
					_axis2D.xAxis.SetRawValue(rawValue);
					num2 = -1779065713;
					continue;
				case 0:
					rawValue = Mathf.InverseLerp(0f - _horizontalTiltLimit, _horizontalTiltLimit, value) * 2f - 1f;
					num2 = -1779065724;
					continue;
				case 7:
					_axis2D.yAxis.SetRawValue(0f - num);
					num2 = -1779065714;
					continue;
				case 1:
					if (acceleration == Vector3.zero)
					{
						rawValue = 0f;
						num2 = -1779065724;
						continue;
					}
					goto case 5;
				}
				break;
			}
			goto IL_0008;
			IL_0008:
			num2 = -1779065715;
			goto IL_000d;
			IL_009f:
			float value2 = Mathf.Atan2(acceleration.z, 0f - acceleration.y) * 57.29578f + _forwardRestAngle;
			num = Mathf.InverseLerp(0f - _forwardTiltLimit, _forwardTiltLimit, value2) * 2f - 1f;
			num2 = -1779065717;
			goto IL_000d;
		}

		private void RSVeklVKOyscZAgLkkmEvJazvbE()
		{
			iSfJagPsCsVhUfDNyhNvbxLabufM(_allowedTiltDirections);
			if (!hasController)
			{
				return;
			}
			while (true)
			{
				int num;
				if (_useHAxis)
				{
					base.controller.ValidateElements(_horizontalTiltCustomControllerElement);
					num = -134138755;
					goto IL_001a;
				}
				goto IL_0075;
				IL_001a:
				while (true)
				{
					switch (num ^ -134138759)
					{
					default:
						return;
					case 1:
						return;
					case 0:
						num = -134138757;
						continue;
					case 2:
						break;
					case 3:
						base.controller.ValidateElements(_forwardTiltCustomControllerElement);
						num = -134138760;
						continue;
					case 4:
						goto IL_0075;
					}
					break;
				}
				continue;
				IL_0075:
				int num2;
				if (_useFAxis)
				{
					num = -134138758;
					num2 = num;
				}
				else
				{
					num = -134138760;
					num2 = num;
				}
				goto IL_001a;
			}
		}

		private void iSfJagPsCsVhUfDNyhNvbxLabufM(TiltDirection P_0)
		{
			bool flag = P_0 == TiltDirection.Both || P_0 == TiltDirection.Horizontal;
			if (_useHAxis != flag)
			{
				goto IL_0014;
			}
			goto IL_005e;
			IL_0014:
			int num = -782241985;
			goto IL_0019;
			IL_00d5:
			_allowedTiltDirections = P_0;
			num = -782241992;
			goto IL_0019;
			IL_0019:
			while (true)
			{
				switch (num ^ -782241987)
				{
				default:
					return;
				case 5:
					return;
				case 4:
					break;
				case 0:
					goto IL_0045;
				case 6:
					goto IL_005e;
				case 2:
					_useHAxis = flag;
					if (!flag && hasController)
					{
						base.controller.ClearElementValue(_horizontalTiltCustomControllerElement);
						num = -782241989;
						continue;
					}
					goto IL_005e;
				case 3:
					base.controller.ClearElementValue(_forwardTiltCustomControllerElement);
					num = -782241988;
					continue;
				case 1:
					goto IL_00d5;
				}
				break;
				IL_0045:
				int num2;
				if (!hasController)
				{
					num = -782241988;
					num2 = num;
				}
				else
				{
					num = -782241986;
					num2 = num;
				}
			}
			goto IL_0014;
			IL_005e:
			bool flag2 = P_0 == TiltDirection.Both || P_0 == TiltDirection.Forward;
			if (_useFAxis != flag2)
			{
				_useFAxis = flag2;
				int num3;
				if (!flag2)
				{
					num = -782241987;
					num3 = num;
				}
				else
				{
					num = -782241988;
					num3 = num;
				}
				goto IL_0019;
			}
			goto IL_00d5;
		}
	}
}
