using Rewired.Utils;
using System.ComponentModel;
using UnityEngine;
using UnityEngine.UI;

namespace Rewired.Internal
{
	[Browsable(false)]
	[AddComponentMenu("")]
	[EditorBrowsable(EditorBrowsableState.Never)]
	public class GUIText : MonoBehaviour
	{
		private string ZefElZSkcTGENjxcLdqRLAUFCyMZ;

		private GUIStyle qYLcZMencwQyUvlomAdAOLSrCFEx;

		private TextAnchor WrAGxxRrcjFusZQOvjZPuCVScwe;

		private TextAlignment RKPlJrQyEkdXTQeOBVHQpylJhuh;

		private float HjbPsXAtKwImhCeecFncavPrBEH;

		private Font TVBcmjNQPCEFSKFSgFshUQBITWnm;

		private int nxOeecyDCJTlRafSLBWwiuRSjdWz = -1;

		private FontStyle bcvcUINyGwTuLsQNJtwHoqTbXnG;

		private Color xRCdSbIXNtRZBqjWZGAzAYctqWk = Color.white;

		[SerializeField]
		[CustomObfuscation(rename = false)]
		private Vector2 _pixelOffset;

		[SerializeField]
		[CustomObfuscation(rename = false)]
		private bool _useUnityUI;

		private bool nUJiuTXkeFTIhKWyrsIvMqyFCRCC;

		private bool SwvzvIwTdlqNAwZtfGNrSKBWAvtE;

		private bool ICEotFMRLPTnxZgpKXMcpVyAOQr;

		private bool eIyxXXGFnmOKJAQdyhyzHzpXecT;

		private bool asGntdiANuaLWdCyBNLInVTVeQ;

		private bool waIaBglrMlisrrHuxOBidkGZCCk;

		private bool cakUdbVSlIBQZkdfFfmVkqWpLyIw;

		private Text OHyakoryEArGFZQgugDyUBBgjog;

		private bool OLljOYwXLatiHCpfWOaDeSfDsiD;

		private bool jvHzSRMLWjbpOdpxwXlZwHPpSuo;

		public string text
		{
			get
			{
				return ZefElZSkcTGENjxcLdqRLAUFCyMZ;
			}
			set
			{
				ZefElZSkcTGENjxcLdqRLAUFCyMZ = value;
			}
		}

		public TextAnchor anchor
		{
			get
			{
				return WrAGxxRrcjFusZQOvjZPuCVScwe;
			}
			set
			{
				WrAGxxRrcjFusZQOvjZPuCVScwe = value;
				nUJiuTXkeFTIhKWyrsIvMqyFCRCC = true;
				if (qYLcZMencwQyUvlomAdAOLSrCFEx == null)
				{
					return;
				}
				while (true)
				{
					qYLcZMencwQyUvlomAdAOLSrCFEx.alignment = value;
					int num = 316734454;
					while (true)
					{
						switch (num ^ 0x12E0FBF7)
						{
						default:
							return;
						case 1:
							return;
						case 0:
							goto IL_0017;
						case 2:
							break;
						}
						break;
						IL_0017:
						num = 316734453;
					}
				}
			}
		}

		public TextAlignment alignment
		{
			get
			{
				return RKPlJrQyEkdXTQeOBVHQpylJhuh;
			}
			set
			{
				RKPlJrQyEkdXTQeOBVHQpylJhuh = value;
				SwvzvIwTdlqNAwZtfGNrSKBWAvtE = true;
			}
		}

		public float lineSpacing
		{
			get
			{
				return HjbPsXAtKwImhCeecFncavPrBEH;
			}
			set
			{
				HjbPsXAtKwImhCeecFncavPrBEH = value;
				ICEotFMRLPTnxZgpKXMcpVyAOQr = true;
				while (true)
				{
					int num = 661027842;
					while (true)
					{
						switch (num ^ 0x27667C03)
						{
						default:
							return;
						case 0:
							return;
						case 2:
							break;
						case 1:
							_ = qYLcZMencwQyUvlomAdAOLSrCFEx;
							num = 661027843;
							continue;
						}
						break;
					}
				}
			}
		}

		public Font font
		{
			get
			{
				return TVBcmjNQPCEFSKFSgFshUQBITWnm;
			}
			set
			{
				//Discarded unreachable code: IL_0039
				eIyxXXGFnmOKJAQdyhyzHzpXecT = true;
				TVBcmjNQPCEFSKFSgFshUQBITWnm = value;
				if (qYLcZMencwQyUvlomAdAOLSrCFEx == null)
				{
					goto IL_0016;
				}
				goto IL_0040;
				IL_0040:
				qYLcZMencwQyUvlomAdAOLSrCFEx.font = value;
				int num = 397634343;
				goto IL_001b;
				IL_001b:
				switch (num ^ 0x17B36B27)
				{
				default:
					return;
				case 0:
					return;
				case 1:
					return;
				case 3:
					break;
				case 2:
					goto IL_0040;
				}
				goto IL_0016;
				IL_0016:
				num = 397634342;
				goto IL_001b;
			}
		}

		public int fontSize
		{
			get
			{
				return nxOeecyDCJTlRafSLBWwiuRSjdWz;
			}
			set
			{
				nxOeecyDCJTlRafSLBWwiuRSjdWz = value;
				asGntdiANuaLWdCyBNLInVTVeQ = true;
				if (qYLcZMencwQyUvlomAdAOLSrCFEx == null)
				{
					return;
				}
				while (true)
				{
					qYLcZMencwQyUvlomAdAOLSrCFEx.fontSize = value;
					int num = 881146746;
					while (true)
					{
						switch (num ^ 0x34853B7B)
						{
						default:
							return;
						case 1:
							return;
						case 0:
							goto IL_0017;
						case 2:
							break;
						}
						break;
						IL_0017:
						num = 881146745;
					}
				}
			}
		}

		public FontStyle fontStyle
		{
			get
			{
				return bcvcUINyGwTuLsQNJtwHoqTbXnG;
			}
			set
			{
				bcvcUINyGwTuLsQNJtwHoqTbXnG = value;
				waIaBglrMlisrrHuxOBidkGZCCk = true;
				if (qYLcZMencwQyUvlomAdAOLSrCFEx != null)
				{
					qYLcZMencwQyUvlomAdAOLSrCFEx.fontStyle = value;
				}
			}
		}

		public Color color
		{
			get
			{
				return xRCdSbIXNtRZBqjWZGAzAYctqWk;
			}
			set
			{
				xRCdSbIXNtRZBqjWZGAzAYctqWk = value;
				cakUdbVSlIBQZkdfFfmVkqWpLyIw = true;
				if (qYLcZMencwQyUvlomAdAOLSrCFEx == null)
				{
					return;
				}
				while (true)
				{
					qYLcZMencwQyUvlomAdAOLSrCFEx.normal.textColor = value;
					int num = 1871561092;
					while (true)
					{
						switch (num ^ 0x6F8DC185)
						{
						default:
							return;
						case 1:
							return;
						case 0:
							goto IL_0017;
						case 2:
							break;
						}
						break;
						IL_0017:
						num = 1871561095;
					}
				}
			}
		}

		public Vector2 pixelOffset
		{
			get
			{
				return _pixelOffset;
			}
			set
			{
				_pixelOffset = value;
			}
		}

		public bool useUnityUI
		{
			get
			{
				return _useUnityUI;
			}
			set
			{
				//Discarded unreachable code: IL_004b
				if (_useUnityUI == value)
				{
					return;
				}
				while (true)
				{
					_useUnityUI = value;
					int num = -2033409613;
					while (true)
					{
						switch (num ^ -2033409615)
						{
						case 0:
							goto IL_000a;
						case 3:
							break;
						case 2:
							OLljOYwXLatiHCpfWOaDeSfDsiD = value;
							if (value)
							{
								hEZuwmYGgWaRogwHfeBIJjfmuXp();
								return;
							}
							goto default;
						default:
							ojlfUiQzgIcFPZkOancdXitwNsE();
							return;
						}
						break;
						IL_000a:
						num = -2033409614;
					}
				}
			}
		}

		[CustomObfuscation(rename = false)]
		private void Awake()
		{
			jvHzSRMLWjbpOdpxwXlZwHPpSuo = true;
		}

		[CustomObfuscation(rename = false)]
		private void Start()
		{
			OLljOYwXLatiHCpfWOaDeSfDsiD = _useUnityUI;
			if (_useUnityUI)
			{
				hEZuwmYGgWaRogwHfeBIJjfmuXp();
			}
		}

		[CustomObfuscation(rename = false)]
		private void OnGUI()
		{
			if (_useUnityUI)
			{
				return;
			}
			Rect position = default(Rect);
			while (true)
			{
				int num;
				if (qYLcZMencwQyUvlomAdAOLSrCFEx == null)
				{
					GpgdRjBOEZphGQagFOnmOKZTBtta();
					num = -1158087375;
					goto IL_000e;
				}
				goto IL_0064;
				IL_000e:
				while (true)
				{
					switch (num ^ -1158087375)
					{
					default:
						return;
					case 5:
						return;
					case 2:
						num = -1158087376;
						continue;
					case 1:
						break;
					case 3:
						GUI.Label(position, ZefElZSkcTGENjxcLdqRLAUFCyMZ, qYLcZMencwQyUvlomAdAOLSrCFEx);
						num = -1158087372;
						continue;
					case 0:
						goto IL_0064;
					case 4:
					{
						Vector2 vector = base.transform.localPosition;
						position = new Rect(vector.x * (float)Screen.width + _pixelOffset.x, vector.y * (float)Screen.height + pixelOffset.y, MathTools.Clamp((float)Screen.width - vector.x * (float)Screen.width, 0f, float.MaxValue), MathTools.Clamp((float)Screen.height - vector.y * (float)Screen.height, 0f, float.MaxValue));
						num = -1158087374;
						continue;
					}
					}
					break;
				}
				continue;
				IL_0064:
				int num2;
				if (!string.IsNullOrEmpty(ZefElZSkcTGENjxcLdqRLAUFCyMZ))
				{
					num = -1158087371;
					num2 = num;
				}
				else
				{
					num = -1158087372;
					num2 = num;
				}
				goto IL_000e;
			}
		}

		[CustomObfuscation(rename = false)]
		private void Update()
		{
			//Discarded unreachable code: IL_004f
			if (!_useUnityUI)
			{
				return;
			}
			while (true)
			{
				int num;
				if (OHyakoryEArGFZQgugDyUBBgjog == null)
				{
					Logger.LogError("Text component has been deleted.");
					num = 1932813230;
					goto IL_000e;
				}
				goto IL_0056;
				IL_000e:
				while (true)
				{
					switch (num ^ 0x733463AA)
					{
					case 4:
						return;
					case 2:
						num = 1932813227;
						continue;
					case 1:
						break;
					case 0:
						goto IL_0056;
					default:
						goto end_IL_002f;
					}
					break;
				}
				continue;
				IL_0056:
				RectTransform component = OHyakoryEArGFZQgugDyUBBgjog.GetComponent<RectTransform>();
				if (!(component.anchoredPosition != _pixelOffset))
				{
					break;
				}
				component.anchoredPosition = _pixelOffset;
				num = 1932813225;
				goto IL_000e;
				continue;
				end_IL_002f:
				break;
			}
			OHyakoryEArGFZQgugDyUBBgjog.text = ZefElZSkcTGENjxcLdqRLAUFCyMZ;
		}

		[CustomObfuscation(rename = false)]
		private void OnValidate()
		{
			//Discarded unreachable code: IL_0037, IL_0045
			if (!jvHzSRMLWjbpOdpxwXlZwHPpSuo)
			{
				goto IL_0008;
			}
			goto IL_0059;
			IL_0059:
			int num;
			int num2;
			if (_useUnityUI == OLljOYwXLatiHCpfWOaDeSfDsiD)
			{
				num = -1580251634;
				num2 = num;
			}
			else
			{
				num = -1580251639;
				num2 = num;
			}
			goto IL_000d;
			IL_000d:
			while (true)
			{
				switch (num ^ -1580251635)
				{
				default:
					return;
				case 1:
					return;
				case 3:
					return;
				case 2:
					break;
				case 0:
					hEZuwmYGgWaRogwHfeBIJjfmuXp();
					return;
				case 5:
					ojlfUiQzgIcFPZkOancdXitwNsE();
					num = -1580251634;
					continue;
				case 6:
					goto IL_0059;
				case 4:
					goto IL_0078;
				}
				break;
				IL_0078:
				OLljOYwXLatiHCpfWOaDeSfDsiD = _useUnityUI;
				int num3;
				if (!_useUnityUI)
				{
					num = -1580251640;
					num3 = num;
				}
				else
				{
					num = -1580251635;
					num3 = num;
				}
			}
			goto IL_0008;
			IL_0008:
			num = -1580251636;
			goto IL_000d;
		}

		private void hEZuwmYGgWaRogwHfeBIJjfmuXp()
		{
			//Discarded unreachable code: IL_023e
			if (!Application.isPlaying)
			{
				return;
			}
			RectTransform rectTransform = default(RectTransform);
			GameObject gameObject = default(GameObject);
			while (true)
			{
				Canvas canvas = UnityTools.GetComponentInSelfOrParents<Canvas>(base.transform);
				int num = 451232376;
				while (true)
				{
					switch (num ^ 0x1AE5427D)
					{
					default:
						return;
					case 10:
						return;
					case 23:
						num = 451232372;
						continue;
					case 16:
					{
						int num5;
						if (!asGntdiANuaLWdCyBNLInVTVeQ)
						{
							num = 451232381;
							num5 = num;
						}
						else
						{
							num = 451232368;
							num5 = num;
						}
						continue;
					}
					case 13:
						OHyakoryEArGFZQgugDyUBBgjog.fontSize = nxOeecyDCJTlRafSLBWwiuRSjdWz;
						num = 451232360;
						continue;
					case 6:
						OHyakoryEArGFZQgugDyUBBgjog = GetComponent<Text>();
						if (OHyakoryEArGFZQgugDyUBBgjog == null)
						{
							rectTransform = base.gameObject.AddComponent<RectTransform>();
							rectTransform.anchorMax = new Vector2(1f, 1f);
							rectTransform.anchorMin = new Vector2(0f, 0f);
							rectTransform.localPosition = Vector2.zero;
							num = 451232371;
							continue;
						}
						return;
					case 25:
						TVBcmjNQPCEFSKFSgFshUQBITWnm = OHyakoryEArGFZQgugDyUBBgjog.font;
						num = 451232365;
						continue;
					case 19:
						OHyakoryEArGFZQgugDyUBBgjog.fontStyle = bcvcUINyGwTuLsQNJtwHoqTbXnG;
						num = 451232367;
						continue;
					case 22:
					{
						canvas = gameObject.AddComponent<Canvas>();
						canvas.renderMode = RenderMode.ScreenSpaceOverlay;
						int num3;
						if (gameObject.GetComponent<CanvasScaler>() != null)
						{
							num = 451232369;
							num3 = num;
						}
						else
						{
							num = 451232358;
							num3 = num;
						}
						continue;
					}
					case 27:
						gameObject.AddComponent<CanvasScaler>();
						num = 451232379;
						continue;
					case 2:
						WrAGxxRrcjFusZQOvjZPuCVScwe = OHyakoryEArGFZQgugDyUBBgjog.alignment;
						num = 451232373;
						continue;
					case 11:
						gameObject = base.transform.root.gameObject;
						num = 451232363;
						continue;
					case 8:
						if (eIyxXXGFnmOKJAQdyhyzHzpXecT)
						{
							OHyakoryEArGFZQgugDyUBBgjog.font = TVBcmjNQPCEFSKFSgFshUQBITWnm;
							num = 451232365;
							continue;
						}
						goto case 25;
					case 0:
						nxOeecyDCJTlRafSLBWwiuRSjdWz = OHyakoryEArGFZQgugDyUBBgjog.fontSize;
						num = 451232380;
						continue;
					case 21:
						num = 451232380;
						continue;
					case 3:
						OHyakoryEArGFZQgugDyUBBgjog.color = xRCdSbIXNtRZBqjWZGAzAYctqWk;
						return;
					case 9:
						break;
					case 5:
						if (!(canvas == null))
						{
							goto case 6;
						}
						if (base.transform.root == base.transform)
						{
							gameObject = new GameObject("Canvas");
							num = 451232364;
							continue;
						}
						goto case 11;
					case 20:
						bcvcUINyGwTuLsQNJtwHoqTbXnG = OHyakoryEArGFZQgugDyUBBgjog.fontStyle;
						num = 451232367;
						continue;
					case 17:
						base.transform.SetParent(gameObject.transform, worldPositionStays: true);
						num = 451232363;
						continue;
					case 24:
						num = 451232373;
						continue;
					case 4:
						xRCdSbIXNtRZBqjWZGAzAYctqWk = OHyakoryEArGFZQgugDyUBBgjog.color;
						num = 451232375;
						continue;
					case 18:
					{
						int num4;
						if (!cakUdbVSlIBQZkdfFfmVkqWpLyIw)
						{
							num = 451232377;
							num4 = num;
						}
						else
						{
							num = 451232382;
							num4 = num;
						}
						continue;
					}
					case 14:
						rectTransform.anchoredPosition = Vector2.zero;
						rectTransform.sizeDelta = Vector3.zero;
						num = 451232359;
						continue;
					case 7:
						OHyakoryEArGFZQgugDyUBBgjog.fontSize = 13;
						if (nUJiuTXkeFTIhKWyrsIvMqyFCRCC)
						{
							OHyakoryEArGFZQgugDyUBBgjog.alignment = WrAGxxRrcjFusZQOvjZPuCVScwe;
							num = 451232357;
							continue;
						}
						goto case 2;
					case 15:
						OHyakoryEArGFZQgugDyUBBgjog.color = Color.white;
						OHyakoryEArGFZQgugDyUBBgjog.font = Resources.GetBuiltinResource<Font>("Arial.ttf");
						num = 451232378;
						continue;
					case 12:
						gameObject.GetComponent<CanvasScaler>();
						num = 451232379;
						continue;
					case 1:
					{
						int num2;
						if (!waIaBglrMlisrrHuxOBidkGZCCk)
						{
							num = 451232361;
							num2 = num;
						}
						else
						{
							num = 451232366;
							num2 = num;
						}
						continue;
					}
					case 26:
						OHyakoryEArGFZQgugDyUBBgjog = base.gameObject.AddComponent<Text>();
						num = 451232370;
						continue;
					}
					break;
				}
			}
		}

		private void ojlfUiQzgIcFPZkOancdXitwNsE()
		{
			//Discarded unreachable code: IL_002e
			if (!Application.isPlaying)
			{
				goto IL_0007;
			}
			goto IL_0035;
			IL_0035:
			int num;
			int num2;
			if (!(OHyakoryEArGFZQgugDyUBBgjog != null))
			{
				num = -1579462864;
				num2 = num;
			}
			else
			{
				num = -1579462861;
				num2 = num;
			}
			goto IL_000c;
			IL_000c:
			while (true)
			{
				switch (num ^ -1579462863)
				{
				case 3:
					return;
				case 0:
					break;
				case 4:
					goto IL_0035;
				case 2:
					OHyakoryEArGFZQgugDyUBBgjog.text = string.Empty;
					num = -1579462864;
					continue;
				default:
					OHyakoryEArGFZQgugDyUBBgjog = null;
					return;
				}
				break;
			}
			goto IL_0007;
			IL_0007:
			num = -1579462862;
			goto IL_000c;
		}

		private void GpgdRjBOEZphGQagFOnmOKZTBtta()
		{
			//Discarded unreachable code: IL_0111
			qYLcZMencwQyUvlomAdAOLSrCFEx = new GUIStyle(GUI.skin.label);
			while (true)
			{
				int num = 1127521431;
				while (true)
				{
					switch (num ^ 0x43349C9A)
					{
					case 10:
						break;
					case 5:
						if (eIyxXXGFnmOKJAQdyhyzHzpXecT)
						{
							qYLcZMencwQyUvlomAdAOLSrCFEx.font = TVBcmjNQPCEFSKFSgFshUQBITWnm;
							num = 1127521434;
							continue;
						}
						goto case 9;
					case 6:
						if (waIaBglrMlisrrHuxOBidkGZCCk)
						{
							qYLcZMencwQyUvlomAdAOLSrCFEx.fontStyle = bcvcUINyGwTuLsQNJtwHoqTbXnG;
							num = 1127521425;
							continue;
						}
						goto case 4;
					case 4:
						bcvcUINyGwTuLsQNJtwHoqTbXnG = qYLcZMencwQyUvlomAdAOLSrCFEx.fontStyle;
						num = 1127521425;
						continue;
					case 12:
						WrAGxxRrcjFusZQOvjZPuCVScwe = qYLcZMencwQyUvlomAdAOLSrCFEx.alignment;
						num = 1127521439;
						continue;
					case 11:
					{
						int num3;
						if (cakUdbVSlIBQZkdfFfmVkqWpLyIw)
						{
							num = 1127521426;
							num3 = num;
						}
						else
						{
							num = 1127521435;
							num3 = num;
						}
						continue;
					}
					case 8:
						qYLcZMencwQyUvlomAdAOLSrCFEx.normal.textColor = xRCdSbIXNtRZBqjWZGAzAYctqWk;
						return;
					case 3:
						qYLcZMencwQyUvlomAdAOLSrCFEx.alignment = WrAGxxRrcjFusZQOvjZPuCVScwe;
						num = 1127521439;
						continue;
					case 0:
						if (asGntdiANuaLWdCyBNLInVTVeQ)
						{
							qYLcZMencwQyUvlomAdAOLSrCFEx.fontSize = nxOeecyDCJTlRafSLBWwiuRSjdWz;
							num = 1127521437;
							continue;
						}
						goto case 2;
					case 2:
						nxOeecyDCJTlRafSLBWwiuRSjdWz = qYLcZMencwQyUvlomAdAOLSrCFEx.fontSize;
						num = 1127521436;
						continue;
					case 13:
					{
						int num2;
						if (!nUJiuTXkeFTIhKWyrsIvMqyFCRCC)
						{
							num = 1127521430;
							num2 = num;
						}
						else
						{
							num = 1127521433;
							num2 = num;
						}
						continue;
					}
					case 9:
						TVBcmjNQPCEFSKFSgFshUQBITWnm = qYLcZMencwQyUvlomAdAOLSrCFEx.font;
						num = 1127521434;
						continue;
					case 7:
						num = 1127521436;
						continue;
					default:
						xRCdSbIXNtRZBqjWZGAzAYctqWk = qYLcZMencwQyUvlomAdAOLSrCFEx.normal.textColor;
						return;
					}
					break;
				}
			}
		}

		[CustomObfuscation(rename = false)]
		internal static GUIText GetOrAddComponent(GameObject gameObject)
		{
			if (gameObject == null)
			{
				return null;
			}
			GUIText gUIText = gameObject.GetComponent<GUIText>();
			if (gUIText == null)
			{
				gUIText = gameObject.AddComponent<GUIText>();
			}
			return gUIText;
		}

		[CustomObfuscation(rename = false)]
		internal static GUIText CreateLogger(GameObject gameObject)
		{
			if (gameObject == null)
			{
				goto IL_0009;
			}
			GUIText orAddComponent = GetOrAddComponent(gameObject);
			int num = -1814882190;
			goto IL_000e;
			IL_0009:
			num = -1814882191;
			goto IL_000e;
			IL_000e:
			switch (num ^ -1814882192)
			{
			case 0:
				break;
			case 1:
				return null;
			default:
				orAddComponent.anchor = TextAnchor.LowerLeft;
				return orAddComponent;
			}
			goto IL_0009;
		}
	}
}
