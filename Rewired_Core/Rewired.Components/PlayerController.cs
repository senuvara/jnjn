using Rewired.Utils;
using Rewired.Utils.Attributes;
using System;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.Events;

namespace Rewired.Components
{
	[Serializable]
	public class PlayerController : ComponentWrapper<Rewired.PlayerController>, IPlayerController
	{
		[Serializable]
		public class ButtonStateChangedHandler : UnityEvent<int, bool>
		{
		}

		[Serializable]
		public class AxisValueChangedHandler : UnityEvent<int, float>
		{
		}

		[Serializable]
		public class EnabledStateChangedHandler : UnityEvent<bool>
		{
		}

		[Serializable]
		[CustomClassObfuscation(renamePubIntMembers = false, renamePrivateMembers = false)]
		[CustomObfuscation(rename = false)]
		internal sealed class ElementWithSourceInfo
		{
			[SerializeField]
			[Tooltip("The name of the element.")]
			private string _name;

			[SerializeField]
			[Tooltip("The element type.")]
			private Rewired.PlayerController.Element.TypeWithSource _elementType;

			[SerializeField]
			[Tooltip("Is this element enabled? Disabled elements return no value.")]
			private bool _enabled = true;

			[SerializeField]
			[Tooltip("The Action id of the Action which will be used as the input source for the Element.")]
			private int _actionId = -1;

			[Tooltip("The output coordinate mode of the axis. An Absolute axis will only return value for input received from Absolute sources. A Relative axis will return value for input received from both Relative and Absolute sources. When converting from an Absolute input source to a Relative output, absoluteToRelativeSensitivity will be multiplied by the Absolute value to yield a simulated Relative value.")]
			[SerializeField]
			private AxisCoordinateMode _coordinateMode;

			[SerializeField]
			[FieldRange(0f, float.MaxValue)]
			[Tooltip("The absolute to relative sensitivity multiplier. This is only applied when the axis coordinate mode is set to Relative and the axis receives Absolute coordinate mode input (joystick axes, keyboard keys, etc.).")]
			private float _absoluteToRelativeSensitivity = 1f;

			[FieldRange(0f, float.MaxValue)]
			[Tooltip("The number of times per second the wheel ticks when the value source is an absolute axis value.")]
			[SerializeField]
			private float _repeatRate = 4f;

			public string name
			{
				get
				{
					return _name;
				}
				set
				{
					_name = value;
				}
			}

			public Rewired.PlayerController.Element.TypeWithSource elementType
			{
				get
				{
					return _elementType;
				}
				set
				{
					_elementType = value;
				}
			}

			public bool enabled
			{
				get
				{
					return _enabled;
				}
				set
				{
					_enabled = value;
				}
			}

			public int actionId
			{
				get
				{
					return _actionId;
				}
				set
				{
					_actionId = value;
				}
			}

			public AxisCoordinateMode coordinateMode
			{
				get
				{
					return _coordinateMode;
				}
				set
				{
					_coordinateMode = value;
				}
			}

			public float absoluteSourceSensitivity
			{
				get
				{
					return _absoluteToRelativeSensitivity;
				}
				set
				{
					_absoluteToRelativeSensitivity = value;
				}
			}

			public float repeatRate
			{
				get
				{
					return _repeatRate;
				}
				set
				{
					_repeatRate = value;
				}
			}

			public Rewired.PlayerController.Element.Definition ToDefinition()
			{
				Rewired.PlayerController.Element.Definition definition = Rewired.PlayerController.Element.CreateDefinition((Rewired.PlayerController.Element.Type)elementType);
				Rewired.PlayerController.ElementWithSource.Definition definition2 = default(Rewired.PlayerController.ElementWithSource.Definition);
				if (definition is Rewired.PlayerController.ElementWithSource.Definition)
				{
					definition2 = (Rewired.PlayerController.ElementWithSource.Definition)definition;
					goto IL_001b;
				}
				goto IL_005f;
				IL_0094:
				definition.enabled = enabled;
				int num = -741254013;
				goto IL_0020;
				IL_005f:
				Rewired.PlayerController.Axis.Definition definition3 = default(Rewired.PlayerController.Axis.Definition);
				if (definition is Rewired.PlayerController.Axis.Definition)
				{
					definition3 = (Rewired.PlayerController.Axis.Definition)definition;
					definition3.coordinateMode = coordinateMode;
					num = -741254015;
					goto IL_0020;
				}
				goto IL_00aa;
				IL_0020:
				while (true)
				{
					switch (num ^ -741254016)
					{
					case 0:
						break;
					case 4:
						definition2.actionId = actionId;
						num = -741254010;
						continue;
					case 6:
						goto IL_005f;
					case 1:
						definition3.absoluteToRelativeSensitivity = absoluteSourceSensitivity;
						num = -741254011;
						continue;
					case 2:
						goto IL_0094;
					case 5:
						goto IL_00aa;
					default:
						definition.name = name;
						return definition;
					}
					break;
				}
				goto IL_001b;
				IL_00aa:
				if (definition is Rewired.PlayerController.MouseWheelAxis.Definition)
				{
					Rewired.PlayerController.MouseWheelAxis.Definition definition4 = (Rewired.PlayerController.MouseWheelAxis.Definition)definition;
					definition4.repeatRate = repeatRate;
					num = -741254014;
					goto IL_0020;
				}
				goto IL_0094;
				IL_001b:
				num = -741254012;
				goto IL_0020;
			}
		}

		[Serializable]
		[CustomObfuscation(rename = false)]
		[CustomClassObfuscation(renamePubIntMembers = false, renamePrivateMembers = false)]
		internal sealed class ElementInfo
		{
			[SerializeField]
			[Tooltip("The name of the element.")]
			private string _name;

			[Tooltip("The element type.")]
			[SerializeField]
			private Rewired.PlayerController.Element.Type _elementType;

			[Tooltip("Is this element enabled? Disabled elements return no value.")]
			[SerializeField]
			private bool _enabled = true;

			[SerializeField]
			private ElementWithSourceInfo[] _elements = new ElementWithSourceInfo[0];

			public string name
			{
				get
				{
					return _name;
				}
				set
				{
					_name = value;
				}
			}

			public Rewired.PlayerController.Element.Type elementType
			{
				get
				{
					return _elementType;
				}
				set
				{
					_elementType = value;
				}
			}

			public bool enabled
			{
				get
				{
					return _enabled;
				}
				set
				{
					_enabled = value;
				}
			}

			public ElementWithSourceInfo[] elements
			{
				get
				{
					return _elements;
				}
				set
				{
					_elements = value;
				}
			}

			public Rewired.PlayerController.Element.Definition ToDefinition()
			{
				Rewired.PlayerController.Element.Definition definition = Rewired.PlayerController.Element.CreateDefinition(elementType);
				if (definition is Rewired.PlayerController.ElementWithSource.Definition)
				{
					goto IL_0017;
				}
				goto IL_010b;
				IL_010b:
				int num;
				if (definition is Rewired.PlayerController.Axis.Definition)
				{
					Rewired.PlayerController.Axis.Definition definition2 = (Rewired.PlayerController.Axis.Definition)definition;
					definition2.coordinateMode = _elements[0].coordinateMode;
					definition2.absoluteToRelativeSensitivity = _elements[0].absoluteSourceSensitivity;
					num = 1322004647;
					goto IL_001c;
				}
				goto IL_0167;
				IL_001c:
				Rewired.PlayerController.MouseWheelAxis.Definition definition3 = default(Rewired.PlayerController.MouseWheelAxis.Definition);
				while (true)
				{
					switch (num ^ 0x4ECC30A4)
					{
					case 0:
						break;
					case 8:
						if (_elements != null)
						{
							goto IL_005b;
						}
						goto default;
					case 5:
						goto IL_006f;
					case 7:
						Logger.LogError("No element source was found for element with source definition.");
						return null;
					case 2:
						if (_elements == null)
						{
							goto case 7;
						}
						goto IL_00f7;
					case 4:
						goto IL_010b;
					case 6:
						definition3.repeatRate = _elements[0].repeatRate;
						num = 1322004641;
						continue;
					case 3:
						goto IL_0167;
					default:
						Logger.LogError("No element source was found for element with source definition.");
						return null;
					}
					break;
					IL_00f7:
					if (_elements.Length != 0)
					{
						Rewired.PlayerController.ElementWithSource.Definition definition4 = (Rewired.PlayerController.ElementWithSource.Definition)definition;
						definition4.name = _elements[0].name;
						definition4.enabled = _elements[0].enabled;
						definition4.actionId = _elements[0].actionId;
						num = 1322004640;
					}
					else
					{
						num = 1322004643;
					}
					continue;
					IL_005b:
					if (_elements.Length == 0)
					{
						num = 1322004645;
						continue;
					}
					goto IL_018f;
				}
				goto IL_0017;
				IL_0167:
				if (definition is Rewired.PlayerController.MouseWheelAxis.Definition)
				{
					definition3 = (Rewired.PlayerController.MouseWheelAxis.Definition)definition;
					num = 1322004642;
					goto IL_001c;
				}
				goto IL_006f;
				IL_0017:
				num = 1322004646;
				goto IL_001c;
				IL_006f:
				if (definition is Rewired.PlayerController.CompoundElement.Definition)
				{
					definition.name = name;
					definition.enabled = enabled;
					num = 1322004652;
					goto IL_001c;
				}
				return definition;
				IL_018f:
				if (definition is Rewired.PlayerController.MouseWheel.Definition)
				{
					Rewired.PlayerController.MouseWheel.Definition definition5 = definition as Rewired.PlayerController.MouseWheel.Definition;
					try
					{
						if (_elements.Length >= 1)
						{
							goto IL_01ad;
						}
						goto IL_01ef;
						IL_01ef:
						if (_elements.Length < 2)
						{
							return definition;
						}
						definition5.yAxis = (Rewired.PlayerController.MouseWheelAxis.Definition)_elements[1].ToDefinition();
						int num2 = 1322004644;
						goto IL_01b2;
						IL_01ad:
						num2 = 1322004645;
						goto IL_01b2;
						IL_01b2:
						while (true)
						{
							switch (num2 ^ 0x4ECC30A4)
							{
							case 3:
								break;
							case 1:
								definition5.xAxis = (Rewired.PlayerController.MouseWheelAxis.Definition)_elements[0].ToDefinition();
								num2 = 1322004646;
								continue;
							case 2:
								goto IL_01ef;
							default:
								return definition;
							case 0:
								return definition;
							}
							break;
						}
						goto IL_01ad;
					}
					catch
					{
						Rewired.PlayerController.Element.Definition result = default(Rewired.PlayerController.Element.Definition);
						while (true)
						{
							int num3 = 1322004646;
							while (true)
							{
								switch (num3 ^ 0x4ECC30A4)
								{
								case 3:
									break;
								case 2:
									Logger.LogError("Incorrect element source type found. Expecting MouseWheelAxis.");
									num3 = 1322004645;
									continue;
								case 1:
									result = null;
									num3 = 1322004644;
									continue;
								default:
									return result;
								case 0:
									return result;
								}
								break;
							}
						}
					}
				}
				if (definition is Rewired.PlayerController.Axis2D.Definition)
				{
					Rewired.PlayerController.Axis2D.Definition definition6 = definition as Rewired.PlayerController.Axis2D.Definition;
					try
					{
						if (_elements.Length >= 1)
						{
							definition6.xAxis = (Rewired.PlayerController.Axis.Definition)_elements[0].ToDefinition();
							goto IL_0299;
						}
						goto IL_02bb;
						IL_0299:
						int num4 = 1322004645;
						goto IL_029e;
						IL_02bb:
						int num5;
						if (_elements.Length >= 2)
						{
							num4 = 1322004646;
							num5 = num4;
						}
						else
						{
							num4 = 1322004644;
							num5 = num4;
						}
						goto IL_029e;
						IL_029e:
						while (true)
						{
							switch (num4 ^ 0x4ECC30A4)
							{
							case 3:
								break;
							case 1:
								goto IL_02bb;
							case 2:
								definition6.yAxis = (Rewired.PlayerController.Axis.Definition)_elements[1].ToDefinition();
								num4 = 1322004644;
								continue;
							default:
								return definition;
							case 0:
								return definition;
							}
							break;
						}
						goto IL_0299;
					}
					catch
					{
						Logger.LogError("Incorrect element source type found. Expecting Axis.");
						return null;
					}
				}
				throw new NotImplementedException();
			}
		}

		[SerializeField]
		[Tooltip("(Optional) Link the Rewired Input Manager here for easier access to Action ids, Player ids, etc.")]
		[CustomObfuscation(rename = false)]
		private InputManager_Base _rewiredInputManager;

		[CustomObfuscation(rename = false)]
		[SerializeField]
		[Tooltip("The Player id of the Player used for the source of input.")]
		private int _playerId = -1;

		[CustomObfuscation(rename = false)]
		[SerializeField]
		[Tooltip("The elements that will be created in the controller.")]
		private List<ElementInfo> _elements = new List<ElementInfo>();

		[SerializeField]
		[CustomObfuscation(rename = false)]
		[Tooltip("Triggered the first frame the button is pressed or released.")]
		private ButtonStateChangedHandler _onButtonStateChanged = new ButtonStateChangedHandler();

		[Tooltip("Triggered when the axis value changes.")]
		[CustomObfuscation(rename = false)]
		[SerializeField]
		private AxisValueChangedHandler _onAxisValueChanged = new AxisValueChangedHandler();

		[SerializeField]
		[Tooltip("Triggered when the controller is enabled or disabled.")]
		[CustomObfuscation(rename = false)]
		private EnabledStateChangedHandler _onEnabledStateChanged = new EnabledStateChangedHandler();

		public int playerId
		{
			get
			{
				if (!base.initialized)
				{
					return _playerId;
				}
				return base.source.playerId;
			}
			set
			{
				if (ReInput.isReady && ReInput.players.GetPlayer(value) == null)
				{
					Logger.LogWarning("Player id " + value + " does not exist.");
					return;
				}
				while (true)
				{
					_playerId = value;
					if (base.initialized)
					{
						base.source.playerId = value;
						int num = 2102568221;
						while (true)
						{
							switch (num ^ 0x7D52A51F)
							{
							default:
								return;
							case 2:
								return;
							case 0:
								goto IL_002f;
							case 1:
								break;
							}
							break;
							IL_002f:
							num = 2102568222;
						}
						continue;
					}
					break;
				}
			}
		}

		public IList<Rewired.PlayerController.Button> buttons
		{
			get
			{
				if (!base.initialized)
				{
					return EmptyObjects<Rewired.PlayerController.Button>.EmptyReadOnlyIListT;
				}
				return base.source.buttons;
			}
		}

		public IList<Rewired.PlayerController.Axis> axes
		{
			get
			{
				if (!base.initialized)
				{
					return EmptyObjects<Rewired.PlayerController.Axis>.EmptyReadOnlyIListT;
				}
				return base.source.axes;
			}
		}

		public IList<Rewired.PlayerController.Element> elements
		{
			get
			{
				if (!base.initialized)
				{
					return EmptyObjects<Rewired.PlayerController.Element>.EmptyReadOnlyIListT;
				}
				return base.source.elements;
			}
		}

		public int buttonCount
		{
			get
			{
				if (!base.initialized)
				{
					return 0;
				}
				return base.source.buttonCount;
			}
		}

		public int axisCount
		{
			get
			{
				if (!base.initialized)
				{
					return 0;
				}
				return base.source.axisCount;
			}
		}

		public int elementCount
		{
			get
			{
				if (!base.initialized)
				{
					return 0;
				}
				return base.source.elementCount;
			}
		}

		public event Action<int, bool> ButtonStateChangedEvent
		{
			add
			{
				if (base.initialized)
				{
					base.source.ButtonStateChangedEvent += value;
				}
			}
			remove
			{
				if (base.initialized)
				{
					base.source.ButtonStateChangedEvent -= value;
				}
			}
		}

		public event Action<int, float> AxisValueChangedEvent
		{
			add
			{
				if (base.initialized)
				{
					base.source.AxisValueChangedEvent += value;
				}
			}
			remove
			{
				if (!base.initialized)
				{
					return;
				}
				while (true)
				{
					base.source.AxisValueChangedEvent -= value;
					int num = -1062615618;
					while (true)
					{
						switch (num ^ -1062615618)
						{
						default:
							return;
						case 0:
							return;
						case 2:
							goto IL_0009;
						case 1:
							break;
						}
						break;
						IL_0009:
						num = -1062615617;
					}
				}
			}
		}

		public event Action<bool> EnabledStateChangedEvent
		{
			add
			{
				//Discarded unreachable code: IL_0027
				if (!base.initialized)
				{
					while (true)
					{
						switch (-1352287795 ^ -1352287796)
						{
						case 1:
							return;
						case 2:
							continue;
						}
						break;
					}
				}
				base.source.EnabledStateChangedEvent += value;
			}
			remove
			{
				if (base.initialized)
				{
					base.source.EnabledStateChangedEvent -= value;
				}
			}
		}

		public bool GetButton(int index)
		{
			if (!base.initialized)
			{
				return false;
			}
			return base.source.GetButton(index);
		}

		public bool GetButtonDown(int index)
		{
			if (!base.initialized)
			{
				return false;
			}
			return base.source.GetButtonDown(index);
		}

		public bool GetButtonUp(int index)
		{
			if (!base.initialized)
			{
				return false;
			}
			return base.source.GetButtonUp(index);
		}

		public float GetAxis(int index)
		{
			if (!base.initialized)
			{
				return 0f;
			}
			return base.source.GetAxis(index);
		}

		public float GetAxisRaw(int index)
		{
			if (!base.initialized)
			{
				return 0f;
			}
			return base.source.GetAxisRaw(index);
		}

		public Rewired.PlayerController.Element GetElement(int index)
		{
			if (!base.initialized)
			{
				return null;
			}
			return base.source.GetElement(index);
		}

		public T GetElement<T>(int index) where T : Rewired.PlayerController.Element
		{
			if (!base.initialized)
			{
				return null;
			}
			return base.source.GetElement<T>(index);
		}

		protected override void OnAwake()
		{
			RBifIBYrvxCfQMgoPaYgElmgNiz();
			base.OnAwake();
		}

		protected override void OnAwakeFinished()
		{
			base.OnAwakeFinished();
			if (!base.initialized)
			{
				return;
			}
			while (true)
			{
				int num = 1509425539;
				while (true)
				{
					switch (num ^ 0x59F80182)
					{
					default:
						return;
					case 0:
						return;
					case 2:
						break;
					case 1:
						goto IL_002c;
					}
					break;
					IL_002c:
					veSdhGCGhwznPErbolETHtdvJLr(true);
					num = 1509425538;
				}
			}
		}

		protected override void OnEnabled()
		{
			base.OnEnabled();
			if (base.initialized && ReInput.isReady)
			{
				base.source.enabled = true;
			}
		}

		protected override void OnDisabled()
		{
			base.OnDisabled();
			if (base.initialized && ReInput.isReady)
			{
				base.source.enabled = false;
			}
		}

		protected override void OnValidated()
		{
			base.OnValidated();
			while (true)
			{
				int num = 1735037348;
				while (true)
				{
					switch (num ^ 0x676A91A6)
					{
					default:
						return;
					case 1:
						return;
					case 0:
						break;
					case 2:
						goto IL_0024;
					}
					break;
					IL_0024:
					playerId = _playerId;
					_playerId = playerId;
					num = 1735037351;
				}
			}
		}

		protected override void OnReset()
		{
			base.OnReset();
			_rewiredInputManager = null;
			while (true)
			{
				int num = -1453165969;
				while (true)
				{
					switch (num ^ -1453165970)
					{
					case 2:
						break;
					case 1:
						goto IL_002b;
					default:
						_onEnabledStateChanged = new EnabledStateChangedHandler();
						RBifIBYrvxCfQMgoPaYgElmgNiz();
						return;
					}
					break;
					IL_002b:
					_playerId = -1;
					_elements = new List<ElementInfo>();
					_onButtonStateChanged = new ButtonStateChangedHandler();
					_onAxisValueChanged = new AxisValueChangedHandler();
					num = -1453165970;
				}
			}
		}

		protected override void Subscribe()
		{
			base.Subscribe();
			while (true)
			{
				int num = -271521488;
				while (true)
				{
					switch (num ^ -271521485)
					{
					default:
						return;
					case 2:
						return;
					case 0:
						break;
					case 3:
					{
						int num2;
						if (base.source != null)
						{
							num = -271521486;
							num2 = num;
						}
						else
						{
							num = -271521487;
							num2 = num;
						}
						continue;
					}
					case 1:
						base.source.ButtonStateChangedEvent += rjkWbeUcOvGcQTuFaqhMtDLynEU;
						base.source.AxisValueChangedEvent += ErAspGeeDMmNWjmJeGHrjQHrKNo;
						base.source.EnabledStateChangedEvent += veSdhGCGhwznPErbolETHtdvJLr;
						num = -271521487;
						continue;
					}
					break;
				}
			}
		}

		protected override void Unsubscribe()
		{
			base.Unsubscribe();
			if (base.source == null)
			{
				return;
			}
			while (true)
			{
				int num = 2131677395;
				while (true)
				{
					switch (num ^ 0x7F0ED0D1)
					{
					default:
						return;
					case 0:
						return;
					case 3:
						break;
					case 2:
						base.source.ButtonStateChangedEvent -= rjkWbeUcOvGcQTuFaqhMtDLynEU;
						num = 2131677392;
						continue;
					case 1:
						base.source.AxisValueChangedEvent -= ErAspGeeDMmNWjmJeGHrjQHrKNo;
						base.source.EnabledStateChangedEvent -= veSdhGCGhwznPErbolETHtdvJLr;
						num = 2131677393;
						continue;
					}
					break;
				}
			}
		}

		protected override object GetCreateSourceArgs()
		{
			return _elements;
		}

		protected override Rewired.PlayerController CreateSource(object args)
		{
			IList<ElementInfo> list = args as IList<ElementInfo>;
			if (list == null)
			{
				goto IL_0030;
			}
			if (list.Count == 0)
			{
				goto IL_0012;
			}
			goto IL_0048;
			IL_0048:
			List<Rewired.PlayerController.Element.Definition> list2 = new List<Rewired.PlayerController.Element.Definition>(list.Count);
			using (IEnumerator<ElementInfo> enumerator = list.GetEnumerator())
			{
				while (true)
				{
					int num;
					int num2;
					if (enumerator.MoveNext())
					{
						num = 1004260847;
						num2 = num;
					}
					else
					{
						num = 1004260844;
						num2 = num;
					}
					while (true)
					{
						switch (num ^ 0x3BDBCDEE)
						{
						default:
							goto end_IL_005c;
						case 2:
							goto end_IL_005c;
						case 0:
							num = 1004260847;
							continue;
						case 1:
						{
							ElementInfo current = enumerator.Current;
							list2.Add(current.ToDefinition());
							num = 1004260845;
							continue;
						}
						case 3:
							break;
						}
						break;
					}
				}
				end_IL_005c:;
			}
			Rewired.PlayerController.Definition definition = new Rewired.PlayerController.Definition();
			definition.playerId = _playerId;
			definition.elements = list2;
			return Rewired.PlayerController.Factory.Create(definition);
			IL_0030:
			Logger.LogWarning("Invalid element information. Did you configure elements in the inspector? Using defaults.");
			list = CreateDefaultElementInfos();
			int num3 = 1004260846;
			goto IL_0017;
			IL_0017:
			switch (num3 ^ 0x3BDBCDEE)
			{
			case 2:
				break;
			case 1:
				goto IL_0030;
			default:
				goto IL_0048;
			}
			goto IL_0012;
			IL_0012:
			num3 = 1004260847;
			goto IL_0017;
		}

		internal virtual List<ElementInfo> CreateDefaultElementInfos()
		{
			List<ElementInfo> list = new List<ElementInfo>();
			list.Add(new ElementInfo
			{
				name = "Stick",
				elementType = Rewired.PlayerController.Element.Type.Axis2D,
				elements = new ElementWithSourceInfo[2]
				{
					new ElementWithSourceInfo
					{
						name = "Stick Horizontal",
						elementType = Rewired.PlayerController.Element.TypeWithSource.Axis,
						coordinateMode = AxisCoordinateMode.Absolute
					},
					new ElementWithSourceInfo
					{
						name = "Stick Vertical",
						elementType = Rewired.PlayerController.Element.TypeWithSource.Axis,
						coordinateMode = AxisCoordinateMode.Absolute
					}
				}
			});
			list.Add(new ElementInfo
			{
				elements = new ElementWithSourceInfo[1]
				{
					new ElementWithSourceInfo
					{
						name = "Button 1",
						elementType = Rewired.PlayerController.Element.TypeWithSource.Button
					}
				}
			});
			list.Add(new ElementInfo
			{
				elements = new ElementWithSourceInfo[1]
				{
					new ElementWithSourceInfo
					{
						name = "Button 2",
						elementType = Rewired.PlayerController.Element.TypeWithSource.Button
					}
				}
			});
			list.Add(new ElementInfo
			{
				elements = new ElementWithSourceInfo[1]
				{
					new ElementWithSourceInfo
					{
						name = "Button 3",
						elementType = Rewired.PlayerController.Element.TypeWithSource.Button
					}
				}
			});
			list.Add(new ElementInfo
			{
				elements = new ElementWithSourceInfo[1]
				{
					new ElementWithSourceInfo
					{
						name = "Button 4",
						elementType = Rewired.PlayerController.Element.TypeWithSource.Button
					}
				}
			});
			return list;
		}

		private void rjkWbeUcOvGcQTuFaqhMtDLynEU(int P_0, bool P_1)
		{
			if (base.isActiveAndEnabled)
			{
				try
				{
					if (_onButtonStateChanged != null)
					{
						_onButtonStateChanged.Invoke(P_0, P_1);
					}
				}
				catch (Exception arg)
				{
					Logger.LogError("An exception occurred in a listener of ButtonStateChangedEvent. This means an exception was thrown by your code.\n" + arg);
				}
			}
		}

		private void ErAspGeeDMmNWjmJeGHrjQHrKNo(int P_0, float P_1)
		{
			if (base.isActiveAndEnabled)
			{
				try
				{
					if (_onAxisValueChanged != null)
					{
						_onAxisValueChanged.Invoke(P_0, P_1);
					}
				}
				catch (Exception arg)
				{
					Logger.LogError("An exception occurred in a listener of AxisValueChangedEvent. This means an exception was thrown by your code.\n" + arg);
				}
			}
		}

		private void veSdhGCGhwznPErbolETHtdvJLr(bool P_0)
		{
			try
			{
				if (_onEnabledStateChanged != null)
				{
					while (true)
					{
						int num = 1201864750;
						while (true)
						{
							switch (num ^ 0x47A3002C)
							{
							default:
								return;
							case 1:
								return;
							case 0:
								break;
							case 2:
								goto IL_0026;
							}
							break;
							IL_0026:
							_onEnabledStateChanged.Invoke(P_0);
							num = 1201864749;
						}
					}
				}
			}
			catch (Exception arg)
			{
				while (true)
				{
					int num2 = 1201864749;
					while (true)
					{
						switch (num2 ^ 0x47A3002C)
						{
						default:
							return;
						case 0:
							return;
						case 2:
							break;
						case 1:
							goto IL_005a;
						}
						break;
						IL_005a:
						Logger.LogError("An exception occurred in a listener of EnabledStateChangedEvent. This means an exception was thrown by your code.\n" + arg);
						num2 = 1201864748;
					}
				}
			}
		}

		private void RBifIBYrvxCfQMgoPaYgElmgNiz()
		{
			//Discarded unreachable code: IL_0035
			if (_elements != null && _elements.Count > 0)
			{
				while (true)
				{
					switch (0x263F083C ^ 0x263F083E)
					{
					case 2:
						return;
					case 0:
						continue;
					}
					break;
				}
			}
			_elements = CreateDefaultElementInfos();
		}

		bool IPlayerController.get_enabled()
		{
			return base.enabled;
		}

		void IPlayerController.set_enabled(bool P_0)
		{
			base.enabled = P_0;
		}
	}
}
