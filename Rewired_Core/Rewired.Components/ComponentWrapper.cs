using System;
using UnityEngine;

namespace Rewired.Components
{
	[Serializable]
	[AddComponentMenu("")]
	public abstract class ComponentWrapper<T> : MonoBehaviour where T : class
	{
		[NonSerialized]
		private T ClGctWbuvINLouElGTGLTPPBaaq;

		[NonSerialized]
		private bool IoRFdMQoQlRiOSEMjmYGiNSOeMq;

		protected T source => ClGctWbuvINLouElGTGLTPPBaaq;

		protected bool initialized => IoRFdMQoQlRiOSEMjmYGiNSOeMq;

		[CustomObfuscation(rename = false)]
		private void Awake()
		{
			OnAwake();
			OnAwakeFinished();
		}

		[CustomObfuscation(rename = false)]
		private void Start()
		{
			OnStart();
		}

		[CustomObfuscation(rename = false)]
		private void OnEnable()
		{
			OnEnabled();
		}

		[CustomObfuscation(rename = false)]
		private void OnDisable()
		{
			OnDisabled();
		}

		[CustomObfuscation(rename = false)]
		private void OnDestroy()
		{
			OnDestroyed();
		}

		[CustomObfuscation(rename = false)]
		private void Reset()
		{
			OnReset();
		}

		[CustomObfuscation(rename = false)]
		private void OnValidate()
		{
			OnValidated();
		}

		protected virtual void OnAwake()
		{
			ReInput.InitializedEvent += GBLhDrJCnYiiPqiFpxEfCGAisMe;
			Initialize();
		}

		protected virtual void OnAwakeFinished()
		{
		}

		protected virtual void OnStart()
		{
		}

		protected virtual void OnEnabled()
		{
		}

		protected virtual void OnDisabled()
		{
		}

		protected virtual void OnDestroyed()
		{
			Unsubscribe();
			ReInput.InitializedEvent -= GBLhDrJCnYiiPqiFpxEfCGAisMe;
		}

		protected virtual void OnReset()
		{
		}

		protected virtual void OnValidated()
		{
		}

		protected virtual void Initialize()
		{
			if (!TryInitialize())
			{
				return;
			}
			while (true)
			{
				IoRFdMQoQlRiOSEMjmYGiNSOeMq = true;
				int num = 35183507;
				while (true)
				{
					switch (num ^ 0x218DB93)
					{
					case 2:
						goto IL_0009;
					case 1:
						break;
					default:
						PostInitialize();
						return;
					}
					break;
					IL_0009:
					num = 35183506;
				}
			}
		}

		protected virtual bool TryInitialize()
		{
			if (IoRFdMQoQlRiOSEMjmYGiNSOeMq)
			{
				return false;
			}
			ClGctWbuvINLouElGTGLTPPBaaq = CreateSource(GetCreateSourceArgs());
			if (ClGctWbuvINLouElGTGLTPPBaaq == null)
			{
				Logger.LogError("Failed to create source object.");
				return false;
			}
			IoRFdMQoQlRiOSEMjmYGiNSOeMq = true;
			return true;
		}

		protected abstract T CreateSource(object args);

		protected abstract object GetCreateSourceArgs();

		protected virtual void PostInitialize()
		{
			Subscribe();
		}

		protected virtual void Deinitialize()
		{
			IoRFdMQoQlRiOSEMjmYGiNSOeMq = false;
			Unsubscribe();
			while (true)
			{
				int num = 338216966;
				while (true)
				{
					switch (num ^ 0x1428C804)
					{
					default:
						return;
					case 1:
						return;
					case 0:
						break;
					case 2:
						goto IL_002b;
					}
					break;
					IL_002b:
					ClGctWbuvINLouElGTGLTPPBaaq = null;
					num = 338216965;
				}
			}
		}

		protected virtual void Subscribe()
		{
			Unsubscribe();
			ReInput.ShutDownEvent += YhnXGSFIzyetOgNXYNQCKtkfzrN;
		}

		protected virtual void Unsubscribe()
		{
			ReInput.ShutDownEvent -= YhnXGSFIzyetOgNXYNQCKtkfzrN;
		}

		private void YhnXGSFIzyetOgNXYNQCKtkfzrN()
		{
			Deinitialize();
		}

		private void GBLhDrJCnYiiPqiFpxEfCGAisMe()
		{
			Initialize();
		}
	}
}
