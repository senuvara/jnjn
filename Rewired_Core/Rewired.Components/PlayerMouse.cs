using Rewired.UI;
using Rewired.Utils;
using Rewired.Utils.Classes.Data;
using System;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.Events;

namespace Rewired.Components
{
	[Serializable]
	public sealed class PlayerMouse : PlayerController, IPlayerController, IPlayerMouse, IMouseInputSource
	{
		[Serializable]
		public class ScreenPositionChangedHandler : UnityEvent<Vector2>
		{
		}

		[CustomObfuscation(rename = false)]
		[SerializeField]
		[Tooltip("If enabled, the screen position will default to the center of the allowed movement area. Otherwise, it will default to the lower-left corner of the allowed movement area.")]
		private bool _defaultToCenter = true;

		[CustomObfuscation(rename = false)]
		[SerializeField]
		[Tooltip("The pointer speed. This does not affect the speed of input from the mouse x/y axes if useHardwarePointerPosition is enabled. It only affects the speed from input sources other than mouse x/y or if mouse x/y are mapped to Actions assigned to Axes. ")]
		private float _pointerSpeed = 1f;

		[SerializeField]
		[CustomObfuscation(rename = false)]
		[Tooltip("If enabled, the hardware pointer position will be used for mouse input. Otherwise, the position of the pointer will be calculated only from the Axis Action values. The Player that owns this Player Mouse must have the physical mouse assigned to it in order for the hardware position to be used, ex: player.controllers.hasMouse == true.")]
		private bool _useHardwarePointerPosition = true;

		[SerializeField]
		[Tooltip("The allowed movement area for the mouse pointer. Set Movement Area Unit to determine the data format of this value. This rect is a screen-space rect with 0, 0 at the lower-left corner.")]
		[CustomObfuscation(rename = false)]
		private Rect _movementArea = new Rect(0f, 0f, 1f, 1f);

		[CustomObfuscation(rename = false)]
		[SerializeField]
		[Tooltip("The unit format of the movement area. This is used to determine the data format of Movement Area.")]
		private Rewired.PlayerMouse.MovementAreaUnit _movementAreaUnit;

		[Tooltip("Triggered when the screen position changes. Link this to your pointer to drive its position.")]
		[SerializeField]
		[CustomObfuscation(rename = false)]
		private ScreenPositionChangedHandler _onScreenPositionChanged = new ScreenPositionChangedHandler();

		private new Rewired.PlayerMouse source => base.source as Rewired.PlayerMouse;

		public bool defaultToCenter
		{
			get
			{
				if (!base.initialized)
				{
					return _defaultToCenter;
				}
				return source.defaultToCenter;
			}
			set
			{
				if (source == null)
				{
					return;
				}
				while (true)
				{
					source.defaultToCenter = value;
					int num = -82197638;
					while (true)
					{
						switch (num ^ -82197637)
						{
						default:
							return;
						case 1:
							return;
						case 0:
							goto IL_0009;
						case 2:
							break;
						}
						break;
						IL_0009:
						num = -82197639;
					}
				}
			}
		}

		public ScreenRect movementArea
		{
			get
			{
				if (!base.initialized)
				{
					return new ScreenRect(_movementArea.xMin, _movementArea.yMin, _movementArea.width, _movementArea.height);
				}
				return source.movementArea;
			}
			set
			{
				if (source != null)
				{
					source.movementArea = value;
				}
			}
		}

		public Rewired.PlayerMouse.MovementAreaUnit movementAreaUnit
		{
			get
			{
				if (!base.initialized)
				{
					return _movementAreaUnit;
				}
				return source.movementAreaUnit;
			}
			set
			{
				//Discarded unreachable code: IL_0027
				if (source == null)
				{
					while (true)
					{
						switch (0x7FEFEE2E ^ 0x7FEFEE2F)
						{
						case 1:
							return;
						case 0:
							continue;
						}
						break;
					}
				}
				source.movementAreaUnit = value;
			}
		}

		public Vector2 screenPosition
		{
			get
			{
				if (!base.initialized)
				{
					return Vector2.zero;
				}
				return source.screenPosition;
			}
			set
			{
				//Discarded unreachable code: IL_0027
				if (source == null)
				{
					while (true)
					{
						switch (-1060221220 ^ -1060221219)
						{
						case 1:
							return;
						case 2:
							continue;
						}
						break;
					}
				}
				source.screenPosition = value;
			}
		}

		public Vector2 screenPositionPrev
		{
			get
			{
				if (!base.initialized)
				{
					return Vector2.zero;
				}
				return source.screenPositionPrev;
			}
		}

		public Vector2 screenPositionDelta
		{
			get
			{
				if (!base.initialized)
				{
					return Vector2.zero;
				}
				return source.screenPositionDelta;
			}
		}

		public Rewired.PlayerController.MouseAxis xAxis
		{
			get
			{
				if (!base.initialized)
				{
					return null;
				}
				return source.xAxis;
			}
		}

		public Rewired.PlayerController.MouseAxis yAxis
		{
			get
			{
				if (!base.initialized)
				{
					return null;
				}
				return source.yAxis;
			}
		}

		public Rewired.PlayerController.MouseWheel wheel
		{
			get
			{
				if (!base.initialized)
				{
					return null;
				}
				return source.wheel;
			}
		}

		public Rewired.PlayerController.Button leftButton
		{
			get
			{
				if (!base.initialized)
				{
					return null;
				}
				return source.leftButton;
			}
		}

		public Rewired.PlayerController.Button rightButton
		{
			get
			{
				if (!base.initialized)
				{
					return null;
				}
				return source.rightButton;
			}
		}

		public Rewired.PlayerController.Button middleButton
		{
			get
			{
				if (!base.initialized)
				{
					return null;
				}
				return source.middleButton;
			}
		}

		public float pointerSpeed
		{
			get
			{
				if (!base.initialized)
				{
					return _pointerSpeed;
				}
				return source.pointerSpeed;
			}
			set
			{
				if (value < 0f)
				{
					value = 0f;
					goto IL_000f;
				}
				goto IL_0031;
				IL_000f:
				int num = -1839053443;
				goto IL_0014;
				IL_0031:
				_pointerSpeed = value;
				int num2;
				if (base.initialized)
				{
					num = -1839053442;
					num2 = num;
				}
				else
				{
					num = -1839053444;
					num2 = num;
				}
				goto IL_0014;
				IL_0014:
				while (true)
				{
					switch (num ^ -1839053441)
					{
					default:
						return;
					case 3:
						return;
					case 0:
						break;
					case 2:
						goto IL_0031;
					case 1:
						source.pointerSpeed = value;
						num = -1839053444;
						continue;
					}
					break;
				}
				goto IL_000f;
			}
		}

		public bool useHardwarePointerPosition
		{
			get
			{
				if (!base.initialized)
				{
					return _useHardwarePointerPosition;
				}
				return source.useHardwarePointerPosition;
			}
			set
			{
				_useHardwarePointerPosition = value;
				if (!base.initialized)
				{
					return;
				}
				while (true)
				{
					int num = 714037764;
					while (true)
					{
						switch (num ^ 0x2A8F5A05)
						{
						default:
							return;
						case 2:
							return;
						case 0:
							break;
						case 1:
							goto IL_002d;
						}
						break;
						IL_002d:
						source.useHardwarePointerPosition = value;
						num = 714037767;
					}
				}
			}
		}

		bool IMouseInputSource.enabled
		{
			get
			{
				if (!base.initialized)
				{
					return false;
				}
				return ((IMouseInputSource)source).enabled;
			}
		}

		Vector2 IMouseInputSource.screenPosition
		{
			get
			{
				if (!base.initialized)
				{
					return Vector2.zero;
				}
				return ((IMouseInputSource)source).screenPosition;
			}
		}

		Vector2 IMouseInputSource.screenPositionDelta
		{
			get
			{
				if (!base.initialized)
				{
					return Vector2.zero;
				}
				return ((IMouseInputSource)source).screenPositionDelta;
			}
		}

		Vector2 IMouseInputSource.wheelDelta
		{
			get
			{
				if (!base.initialized)
				{
					return Vector2.zero;
				}
				return ((IMouseInputSource)source).wheelDelta;
			}
		}

		bool IMouseInputSource.locked
		{
			get
			{
				if (!base.initialized)
				{
					return false;
				}
				return ((IMouseInputSource)source).locked;
			}
		}

		public event Action<Vector2> ScreenPositionChangedEvent
		{
			add
			{
				if (base.initialized)
				{
					source.ScreenPositionChangedEvent += value;
				}
			}
			remove
			{
				if (base.initialized)
				{
					source.ScreenPositionChangedEvent -= value;
				}
			}
		}

		protected override void OnValidated()
		{
			base.OnValidated();
			defaultToCenter = _defaultToCenter;
			while (true)
			{
				int num = -402458557;
				while (true)
				{
					switch (num ^ -402458553)
					{
					case 0:
						break;
					case 2:
						_movementAreaUnit = movementAreaUnit;
						pointerSpeed = _pointerSpeed;
						_pointerSpeed = pointerSpeed;
						num = -402458554;
						continue;
					case 3:
						movementAreaUnit = _movementAreaUnit;
						num = -402458555;
						continue;
					case 4:
					{
						_defaultToCenter = defaultToCenter;
						this.movementArea = new ScreenRect(_movementArea.xMin, _movementArea.yMin, _movementArea.width, _movementArea.height);
						ScreenRect movementArea = this.movementArea;
						_movementArea = new Rect(movementArea.xMin, movementArea.yMin, movementArea.width, movementArea.height);
						num = -402458556;
						continue;
					}
					default:
						useHardwarePointerPosition = _useHardwarePointerPosition;
						return;
					}
					break;
				}
			}
		}

		protected override void OnReset()
		{
			base.OnReset();
			_defaultToCenter = true;
			_pointerSpeed = 1f;
			_useHardwarePointerPosition = true;
			_movementArea = new Rect(0f, 0f, 1f, 1f);
			_movementAreaUnit = Rewired.PlayerMouse.MovementAreaUnit.Screen;
			_onScreenPositionChanged = new ScreenPositionChangedHandler();
		}

		protected override Rewired.PlayerController CreateSource(object args)
		{
			IList<ElementInfo> list = args as IList<ElementInfo>;
			ElementInfo current = default(ElementInfo);
			while (true)
			{
				int num = 2074756505;
				while (true)
				{
					switch (num ^ 0x7BAA4598)
					{
					case 2:
						break;
					case 1:
						if (list != null)
						{
							int num4;
							if (list.Count == 0)
							{
								num = 2074756508;
								num4 = num;
							}
							else
							{
								num = 2074756507;
								num4 = num;
							}
							continue;
						}
						goto case 4;
					case 0:
						list = CreateDefaultElementInfos();
						num = 2074756507;
						continue;
					case 4:
						Logger.LogWarning("Invalid element information. Did you configure elements in the inspector? Using defaults.");
						num = 2074756504;
						continue;
					default:
					{
						List<Rewired.PlayerController.Element.Definition> list2 = new List<Rewired.PlayerController.Element.Definition>(list.Count);
						using (IEnumerator<ElementInfo> enumerator = list.GetEnumerator())
						{
							while (true)
							{
								int num2;
								int num3;
								if (!enumerator.MoveNext())
								{
									num2 = 2074756505;
									num3 = num2;
								}
								else
								{
									num2 = 2074756506;
									num3 = num2;
								}
								while (true)
								{
									switch (num2 ^ 0x7BAA4598)
									{
									default:
										goto end_IL_007c;
									case 1:
										goto end_IL_007c;
									case 0:
										num2 = 2074756506;
										continue;
									case 4:
										break;
									case 3:
										list2.Add(current.ToDefinition());
										num2 = 2074756508;
										continue;
									case 2:
										current = enumerator.Current;
										num2 = 2074756507;
										continue;
									}
									break;
								}
							}
							end_IL_007c:;
						}
						Rewired.PlayerMouse.Definition definition = new Rewired.PlayerMouse.Definition();
						definition.playerId = base.playerId;
						definition.elements = list2;
						definition.defaultToCenter = _defaultToCenter;
						definition.movementArea = new ScreenRect(_movementArea.xMin, _movementArea.yMin, _movementArea.width, _movementArea.height);
						definition.movementAreaUnit = _movementAreaUnit;
						definition.pointerSpeed = _pointerSpeed;
						definition.useHardwarePointerPosition = _useHardwarePointerPosition;
						return Rewired.PlayerMouse.Factory.Create(definition);
					}
					}
					break;
				}
			}
		}

		protected override void Deinitialize()
		{
			base.Deinitialize();
		}

		protected override void Subscribe()
		{
			base.Subscribe();
			while (true)
			{
				int num = -811582934;
				while (true)
				{
					switch (num ^ -811582935)
					{
					default:
						return;
					case 1:
						return;
					case 0:
						break;
					case 3:
					{
						int num2;
						if (source != null)
						{
							num = -811582933;
							num2 = num;
						}
						else
						{
							num = -811582936;
							num2 = num;
						}
						continue;
					}
					case 2:
						source.ScreenPositionChangedEvent += yhEPFRLqjsHFQSCmEJFIzbvbMQA;
						num = -811582936;
						continue;
					}
					break;
				}
			}
		}

		protected override void Unsubscribe()
		{
			base.Unsubscribe();
			while (true)
			{
				int num = -986027968;
				while (true)
				{
					switch (num ^ -986027967)
					{
					default:
						return;
					case 0:
						return;
					case 2:
						break;
					case 1:
						if (source != null)
						{
							goto IL_002c;
						}
						return;
					}
					break;
					IL_002c:
					source.ScreenPositionChangedEvent -= yhEPFRLqjsHFQSCmEJFIzbvbMQA;
					num = -986027967;
				}
			}
		}

		internal override List<ElementInfo> CreateDefaultElementInfos()
		{
			List<ElementInfo> list = new List<ElementInfo>();
			list.Add(new ElementInfo
			{
				name = "Movement",
				elementType = Rewired.PlayerController.Element.Type.MouseAxis2D,
				elements = new ElementWithSourceInfo[2]
				{
					new ElementWithSourceInfo
					{
						name = "Horizontal",
						elementType = Rewired.PlayerController.Element.TypeWithSource.MouseAxis,
						coordinateMode = AxisCoordinateMode.Relative,
						absoluteSourceSensitivity = 600f
					},
					new ElementWithSourceInfo
					{
						name = "Vertical",
						elementType = Rewired.PlayerController.Element.TypeWithSource.MouseAxis,
						coordinateMode = AxisCoordinateMode.Relative,
						absoluteSourceSensitivity = 600f
					}
				}
			});
			list.Add(new ElementInfo
			{
				name = "Wheel",
				elementType = Rewired.PlayerController.Element.Type.MouseWheel,
				elements = new ElementWithSourceInfo[2]
				{
					new ElementWithSourceInfo
					{
						name = "Wheel Horizontal",
						elementType = Rewired.PlayerController.Element.TypeWithSource.MouseWheelAxis,
						coordinateMode = AxisCoordinateMode.Relative
					},
					new ElementWithSourceInfo
					{
						name = "Wheel Vertical",
						elementType = Rewired.PlayerController.Element.TypeWithSource.MouseWheelAxis,
						coordinateMode = AxisCoordinateMode.Relative
					}
				}
			});
			list.Add(new ElementInfo
			{
				elements = new ElementWithSourceInfo[1]
				{
					new ElementWithSourceInfo
					{
						name = "Left Button",
						elementType = Rewired.PlayerController.Element.TypeWithSource.Button
					}
				}
			});
			list.Add(new ElementInfo
			{
				elements = new ElementWithSourceInfo[1]
				{
					new ElementWithSourceInfo
					{
						name = "Right Button",
						elementType = Rewired.PlayerController.Element.TypeWithSource.Button
					}
				}
			});
			list.Add(new ElementInfo
			{
				elements = new ElementWithSourceInfo[1]
				{
					new ElementWithSourceInfo
					{
						name = "Middle Button",
						elementType = Rewired.PlayerController.Element.TypeWithSource.Button
					}
				}
			});
			return list;
		}

		private void yhEPFRLqjsHFQSCmEJFIzbvbMQA(Vector2 P_0)
		{
			if (UnityTools.IsActiveAndEnabled(this))
			{
				try
				{
					if (_onScreenPositionChanged != null)
					{
						while (true)
						{
							int num = -1030836698;
							while (true)
							{
								switch (num ^ -1030836697)
								{
								default:
									return;
								case 2:
									return;
								case 0:
									break;
								case 1:
									goto IL_002f;
								}
								break;
								IL_002f:
								_onScreenPositionChanged.Invoke(P_0);
								num = -1030836699;
							}
						}
					}
				}
				catch (Exception arg)
				{
					while (true)
					{
						int num2 = -1030836699;
						while (true)
						{
							switch (num2 ^ -1030836697)
							{
							default:
								return;
							case 1:
								return;
							case 0:
								break;
							case 2:
								goto IL_0063;
							}
							break;
							IL_0063:
							Logger.LogError("An exception occurred in a listener of ScreenPositionChangedEvent. This means an exception was thrown by your code.\n" + arg);
							num2 = -1030836698;
						}
					}
				}
			}
		}

		bool IMouseInputSource.GetButtonDown(int P_0)
		{
			if (!base.initialized)
			{
				return false;
			}
			return ((IMouseInputSource)source).GetButtonDown(P_0);
		}

		bool IMouseInputSource.GetButtonUp(int P_0)
		{
			if (!base.initialized)
			{
				return false;
			}
			return ((IMouseInputSource)source).GetButtonUp(P_0);
		}

		bool IMouseInputSource.GetButton(int P_0)
		{
			if (!base.initialized)
			{
				return false;
			}
			return ((IMouseInputSource)source).GetButton(P_0);
		}

		bool IPlayerController.get_enabled()
		{
			return base.enabled;
		}

		void IPlayerController.set_enabled(bool P_0)
		{
			base.enabled = P_0;
		}
	}
}
