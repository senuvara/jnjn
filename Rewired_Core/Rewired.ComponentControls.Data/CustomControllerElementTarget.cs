using System;
using UnityEngine;

namespace Rewired.ComponentControls.Data
{
	[Serializable]
	[CustomClassObfuscation(renamePrivateMembers = true, renamePubIntMembers = false)]
	public class CustomControllerElementTarget
	{
		[CustomObfuscation(rename = false)]
		internal enum ValueRange
		{
			[CustomObfuscation(rename = false)]
			Full,
			[CustomObfuscation(rename = false)]
			Positive,
			[CustomObfuscation(rename = false)]
			Negative
		}

		[CustomObfuscation(rename = false)]
		[SerializeField]
		[Tooltip("The Custom Controller element.")]
		private CustomControllerElementSelector _element = new CustomControllerElementSelector
		{
			elementType = CustomControllerElementSelector.ElementType.Axis
		};

		[SerializeField]
		[CustomObfuscation(rename = false)]
		private ValueRange _valueRange;

		[Tooltip("Should the final value be positive or negative?")]
		[CustomObfuscation(rename = false)]
		[SerializeField]
		private Pole _valueContribution;

		[CustomObfuscation(rename = false)]
		[SerializeField]
		[Tooltip("Should the final value be inverted?")]
		private bool _invert;

		public CustomControllerElementSelector element => _element;

		public Pole valueContribution
		{
			get
			{
				return _valueContribution;
			}
			set
			{
				_valueContribution = value;
			}
		}

		internal ValueRange valueRange
		{
			get
			{
				return _valueRange;
			}
			set
			{
				_valueRange = value;
			}
		}

		public bool invert
		{
			get
			{
				return _invert;
			}
			set
			{
				_invert = value;
			}
		}

		internal CustomControllerElementTarget()
		{
		}

		internal CustomControllerElementTarget(CustomControllerElementSelector selector)
		{
			_element = selector;
		}

		internal void ClearElementCaches()
		{
			//Discarded unreachable code: IL_0027
			if (_element == null)
			{
				while (true)
				{
					switch (0x623F8336 ^ 0x623F8337)
					{
					case 1:
						return;
					case 0:
						continue;
					}
					break;
				}
			}
			_element.ClearCache();
		}
	}
}
