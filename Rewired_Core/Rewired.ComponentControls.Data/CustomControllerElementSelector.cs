using Rewired.Utils.Attributes;
using System;
using System.Collections.Generic;
using UnityEngine;

namespace Rewired.ComponentControls.Data
{
	[Serializable]
	[CustomClassObfuscation(renamePrivateMembers = true, renamePubIntMembers = false)]
	public sealed class CustomControllerElementSelector
	{
		[CustomObfuscation(rename = false)]
		public enum ElementType
		{
			Axis,
			Button
		}

		[CustomObfuscation(rename = false)]
		public enum SelectorType
		{
			Name,
			Index,
			Id
		}

		[CustomObfuscation(rename = false)]
		[Tooltip("The target Custom Controller element type.")]
		[SerializeField]
		private ElementType _elementType;

		[SerializeField]
		[CustomObfuscation(rename = false)]
		[Tooltip("The method to use to look up the target Custom Controller element.")]
		private SelectorType _selectorType = SelectorType.Id;

		[Tooltip("The target Custom Controller element name.")]
		[SerializeField]
		[CustomObfuscation(rename = false)]
		private string _elementName;

		[FieldRange(-1, int.MaxValue)]
		[Tooltip("The target Custom Controller element index.")]
		[SerializeField]
		[CustomObfuscation(rename = false)]
		private int _elementIndex;

		[CustomObfuscation(rename = false)]
		[FieldRange(-1, int.MaxValue)]
		[SerializeField]
		[Tooltip("The target Custom Controller element id.")]
		private int _elementId = -1;

		[HideInInspector]
		private int JdhUKpSJKndOKyNTGxSKTbJbgdm = -1;

		[HideInInspector]
		private int NeeirDblEOfRcIDfQdhXkQtpyPdh = -1;

		public ElementType elementType
		{
			get
			{
				return _elementType;
			}
			set
			{
				if (_elementType != value)
				{
					_elementType = value;
					ClearCache();
				}
			}
		}

		public SelectorType selectorType
		{
			get
			{
				return _selectorType;
			}
			set
			{
				if (_selectorType == value)
				{
					return;
				}
				while (true)
				{
					_selectorType = value;
					ClearCache();
					int num = 727651143;
					while (true)
					{
						switch (num ^ 0x2B5F1347)
						{
						default:
							return;
						case 0:
							return;
						case 2:
							goto IL_000a;
						case 1:
							break;
						}
						break;
						IL_000a:
						num = 727651142;
					}
				}
			}
		}

		public string elementName
		{
			get
			{
				return _elementName;
			}
			set
			{
				if (!(_elementName == value))
				{
					_elementName = value;
					ClearCache();
				}
			}
		}

		public int elementIndex
		{
			get
			{
				return _elementIndex;
			}
			set
			{
				//Discarded unreachable code: IL_004b
				if (_elementIndex == value)
				{
					goto IL_0009;
				}
				goto IL_003c;
				IL_003c:
				_elementIndex = value;
				int num = -1327155436;
				goto IL_000e;
				IL_000e:
				while (true)
				{
					switch (num ^ -1327155435)
					{
					default:
						return;
					case 0:
						return;
					case 3:
						return;
					case 2:
						break;
					case 1:
						ClearCache();
						num = -1327155435;
						continue;
					case 4:
						goto IL_003c;
					}
					break;
				}
				goto IL_0009;
				IL_0009:
				num = -1327155434;
				goto IL_000e;
			}
		}

		public int elementId
		{
			get
			{
				return _elementId;
			}
			set
			{
				if (_elementId != value)
				{
					_elementId = value;
					ClearCache();
				}
			}
		}

		public bool isAssigned
		{
			get
			{
				switch (selectorType)
				{
				case SelectorType.Id:
					return _elementId >= 0;
				case SelectorType.Index:
					return _elementIndex >= 0;
				case SelectorType.Name:
					return !string.IsNullOrEmpty(_elementName);
				default:
					throw new NotImplementedException();
				}
			}
		}

		public int GetElementIndex(Rewired.CustomController customController)
		{
			//Discarded unreachable code: IL_0190
			if (customController == null)
			{
				return -1;
			}
			if (JdhUKpSJKndOKyNTGxSKTbJbgdm >= 0)
			{
				goto IL_0011;
			}
			goto IL_00a7;
			IL_0016:
			int num;
			IList<ControllerElementIdentifier> list = default(IList<ControllerElementIdentifier>);
			int num2 = default(int);
			int num3 = default(int);
			while (true)
			{
				switch (num ^ -734631935)
				{
				case 17:
					break;
				case 19:
					num = -734631931;
					continue;
				case 11:
					list = GsxEWFxtFvRdJBgVoqGGswEpwwL(customController, _elementType);
					num2 = 0;
					num = -734631922;
					continue;
				case 7:
					goto IL_00a7;
				case 14:
					NeeirDblEOfRcIDfQdhXkQtpyPdh = num2;
					num = -734631919;
					continue;
				case 16:
					num = -734631931;
					continue;
				case 15:
					num = -734631933;
					continue;
				case 9:
					goto IL_00f2;
				case 12:
					goto IL_0118;
				case 3:
					num = -734631931;
					continue;
				case 18:
					num3++;
					num = -734631927;
					continue;
				case 21:
					NeeirDblEOfRcIDfQdhXkQtpyPdh = num3;
					num = -734631934;
					continue;
				case 6:
					return -1;
				case 10:
					throw new NotImplementedException();
				case 8:
					goto IL_019a;
				case 20:
					num2++;
					num = -734631933;
					continue;
				case 13:
					num = -734631927;
					continue;
				case 2:
					if (num2 >= list.Count)
					{
						num = -734631931;
						continue;
					}
					goto IL_0203;
				case 22:
					if (JdhUKpSJKndOKyNTGxSKTbJbgdm != customController.id)
					{
						ClearCache();
						num = -734631930;
						continue;
					}
					goto IL_00a7;
				case 1:
					goto IL_0203;
				case 5:
					goto IL_022b;
				case 0:
					goto IL_0240;
				case 23:
					switch (_selectorType)
					{
					case SelectorType.Id:
						break;
					case SelectorType.Name:
						goto IL_022b;
					case SelectorType.Index:
						goto IL_0240;
					default:
						goto IL_027c;
					}
					goto case 11;
				default:
					{
						return NeeirDblEOfRcIDfQdhXkQtpyPdh;
					}
					IL_027c:
					num = -734631925;
					continue;
				}
				break;
				IL_0240:
				if (_elementIndex < 0)
				{
					return -1;
				}
				list = GsxEWFxtFvRdJBgVoqGGswEpwwL(customController, _elementType);
				num = -734631928;
				continue;
				IL_0203:
				int num4;
				if (list[num2].id != _elementId)
				{
					num = -734631915;
					num4 = num;
				}
				else
				{
					num = -734631921;
					num4 = num;
				}
				continue;
				IL_0118:
				int num5;
				if (list[num3].name.Equals(_elementName))
				{
					num = -734631916;
					num5 = num;
				}
				else
				{
					num = -734631917;
					num5 = num;
				}
				continue;
				IL_022b:
				if (_elementName != null)
				{
					list = GsxEWFxtFvRdJBgVoqGGswEpwwL(customController, _elementType);
					num3 = 0;
					num = -734631924;
				}
				else
				{
					num = -734631929;
				}
				continue;
				IL_00f2:
				if (_elementIndex >= list.Count)
				{
					return -1;
				}
				NeeirDblEOfRcIDfQdhXkQtpyPdh = _elementIndex;
				num = -734631931;
				continue;
				IL_019a:
				int num6;
				if (num3 < list.Count)
				{
					num = -734631923;
					num6 = num;
				}
				else
				{
					num = -734631918;
					num6 = num;
				}
			}
			goto IL_0011;
			IL_0011:
			num = -734631913;
			goto IL_0016;
			IL_00a7:
			if (NeeirDblEOfRcIDfQdhXkQtpyPdh >= 0)
			{
				return NeeirDblEOfRcIDfQdhXkQtpyPdh;
			}
			JdhUKpSJKndOKyNTGxSKTbJbgdm = customController.id;
			num = -734631914;
			goto IL_0016;
		}

		public string GetSelectorFormattedString()
		{
			//Discarded unreachable code: IL_0039
			SelectorType selectorType = this.selectorType;
			while (true)
			{
				switch (-5846710 ^ -5846709)
				{
				case 0:
					continue;
				case 1:
					switch (selectorType)
					{
					case SelectorType.Id:
						break;
					case SelectorType.Index:
						return "Index: " + _elementIndex;
					case SelectorType.Name:
						return "Name: " + _elementName;
					default:
						throw new NotImplementedException();
					}
					break;
				}
				break;
			}
			return "Id: " + _elementId;
		}

		private IList<ControllerElementIdentifier> GsxEWFxtFvRdJBgVoqGGswEpwwL(Rewired.CustomController P_0, ElementType P_1)
		{
			//Discarded unreachable code: IL_003b
			while (true)
			{
				int num = 362933377;
				while (true)
				{
					switch (num ^ 0x15A1EC80)
					{
					case 3:
						break;
					case 1:
						switch (P_1)
						{
						default:
							goto IL_0032;
						case ElementType.Axis:
							break;
						case ElementType.Button:
							return P_0.ButtonElementIdentifiers;
						}
						goto default;
					default:
						return P_0.AxisElementIdentifiers;
					case 0:
						throw new NotImplementedException();
					}
					break;
					IL_0032:
					num = 362933376;
				}
			}
		}

		public void ClearCache()
		{
			JdhUKpSJKndOKyNTGxSKTbJbgdm = -1;
			NeeirDblEOfRcIDfQdhXkQtpyPdh = -1;
		}
	}
}
