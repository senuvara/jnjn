using System;
using UnityEngine;

namespace Rewired.ComponentControls.Data
{
	[Serializable]
	[CustomClassObfuscation(renamePrivateMembers = true, renamePubIntMembers = false)]
	public class CustomControllerElementTargetSetForFloat : CustomControllerElementTargetSet
	{
		[Tooltip("Splits the value into positive and negative sides which can be assigned to different Custom Controller elements.")]
		[SerializeField]
		[CustomObfuscation(rename = false)]
		private bool _splitValue;

		[SerializeField]
		[CustomObfuscation(rename = false)]
		[Tooltip("The target element. This is unused if Split Value is enabled.")]
		private CustomControllerElementTarget _target = new CustomControllerElementTarget(new CustomControllerElementSelector
		{
			elementType = CustomControllerElementSelector.ElementType.Axis
		})
		{
			valueRange = CustomControllerElementTarget.ValueRange.Full
		};

		[SerializeField]
		[Tooltip("The positive target element. This is unused if Split Value is not enabled.")]
		[CustomObfuscation(rename = false)]
		private CustomControllerElementTarget _positiveTarget = new CustomControllerElementTarget(new CustomControllerElementSelector
		{
			elementType = CustomControllerElementSelector.ElementType.Button
		})
		{
			valueRange = CustomControllerElementTarget.ValueRange.Positive,
			valueContribution = Pole.Positive
		};

		[Tooltip("The negative target element. This is unused if Split Value is not enabled.")]
		[SerializeField]
		[CustomObfuscation(rename = false)]
		private CustomControllerElementTarget _negativeTarget = new CustomControllerElementTarget(new CustomControllerElementSelector
		{
			elementType = CustomControllerElementSelector.ElementType.Button
		})
		{
			valueRange = CustomControllerElementTarget.ValueRange.Negative,
			valueContribution = Pole.Positive
		};

		public bool splitValue
		{
			get
			{
				return _splitValue;
			}
			set
			{
				_splitValue = value;
			}
		}

		public CustomControllerElementTarget target => _target;

		public CustomControllerElementTarget positiveTarget => _positiveTarget;

		public CustomControllerElementTarget negativeTarget => _negativeTarget;

		internal override int targetCount
		{
			get
			{
				if (!_splitValue)
				{
					return 1;
				}
				return 2;
			}
		}

		internal override CustomControllerElementTarget this[int index]
		{
			get
			{
				//Discarded unreachable code: IL_0068
				if (_splitValue)
				{
					switch (index)
					{
					case 0:
						break;
					case 1:
						return _negativeTarget;
					default:
						throw new IndexOutOfRangeException();
					}
					goto IL_0054;
				}
				while (true)
				{
					IL_004b:
					int num = 476377294;
					while (true)
					{
						switch (num ^ 0x1C64F0CD)
						{
						case 2:
							num = 476377289;
							continue;
						case 3:
							break;
						case 0:
							goto IL_004b;
						case 4:
							goto IL_0054;
						default:
							return _target;
						}
						if (index == 0)
						{
							num = 476377292;
							continue;
						}
						throw new IndexOutOfRangeException();
					}
				}
				IL_0054:
				return _positiveTarget;
			}
		}

		internal CustomControllerElementTargetSetForFloat()
		{
		}

		internal CustomControllerElementTargetSetForFloat(CustomControllerElementTarget target)
		{
			_splitValue = false;
			_target = target;
		}

		internal CustomControllerElementTargetSetForFloat(CustomControllerElementTarget positiveTarget, CustomControllerElementTarget negativeTarget)
		{
			_splitValue = true;
			_positiveTarget = positiveTarget;
			_negativeTarget = negativeTarget;
		}

		internal override void ClearElementCaches()
		{
			if (_target != null)
			{
				_target.ClearElementCaches();
			}
		}
	}
}
