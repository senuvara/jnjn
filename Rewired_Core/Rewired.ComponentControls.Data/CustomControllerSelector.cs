using Rewired.Utils;
using Rewired.Utils.Attributes;
using System;
using UnityEngine;

namespace Rewired.ComponentControls.Data
{
	[Serializable]
	[CustomClassObfuscation(renamePrivateMembers = true, renamePubIntMembers = false)]
	public sealed class CustomControllerSelector
	{
		[SerializeField]
		[CustomObfuscation(rename = false)]
		[Tooltip("If true, the Custom Controller will be searched for by its source controller id. This can be used with Find in Player and/or Find Using Tag to further refine the search parameters.")]
		private bool _findUsingSourceId = true;

		[CustomObfuscation(rename = false)]
		[FieldRange(0, int.MaxValue)]
		[SerializeField]
		[Tooltip("The source id of the Custom Controller. This is used to find the Custom Controller if Find Using Source Id is True.")]
		private int _sourceId;

		[CustomObfuscation(rename = false)]
		[Tooltip("If true, the Custom Controller will be found using the tag specified here. This can be used with Find in Player and/or Find Using Source Id to further refine the search parameters.")]
		[SerializeField]
		private bool _findUsingTag;

		[SerializeField]
		[CustomObfuscation(rename = false)]
		[Tooltip("The tag on the Custom Controller you wish to use. This is used to find the Custom Controller.")]
		private string _tag;

		[SerializeField]
		[Tooltip("If true, the Custom Controller will be searched for in the Player specified in the Player Id field. This can be used with Find Using Source Id and/or Find Using Tag to further refine the search parameters.")]
		[CustomObfuscation(rename = false)]
		private bool _findInPlayer;

		[SerializeField]
		[CustomObfuscation(rename = false)]
		[Tooltip("The Player Id of the Player that owns the Custom Controller.")]
		private int _playerId;

		public bool findUsingSourceId
		{
			get
			{
				return _findUsingSourceId;
			}
			set
			{
				if (_findUsingSourceId != value)
				{
					_findUsingSourceId = value;
					bFoWQuDzcOvecUqeSCdTCzgfmRIE();
				}
			}
		}

		public int sourceId
		{
			get
			{
				return _sourceId;
			}
			set
			{
				value = MathTools.Max(0, value);
				if (_sourceId != value)
				{
					_sourceId = value;
					bFoWQuDzcOvecUqeSCdTCzgfmRIE();
				}
			}
		}

		public bool findUsingTag
		{
			get
			{
				return _findUsingTag;
			}
			set
			{
				if (_findUsingTag != value)
				{
					_findUsingTag = value;
					bFoWQuDzcOvecUqeSCdTCzgfmRIE();
				}
			}
		}

		public string tag
		{
			get
			{
				return _tag;
			}
			set
			{
				//Discarded unreachable code: IL_002d
				if (_tag == value)
				{
					while (true)
					{
						switch (-1770907972 ^ -1770907970)
						{
						case 2:
							return;
						case 0:
							continue;
						}
						break;
					}
				}
				_tag = value;
				bFoWQuDzcOvecUqeSCdTCzgfmRIE();
			}
		}

		public bool findInPlayer
		{
			get
			{
				return _findInPlayer;
			}
			set
			{
				if (_findInPlayer == value)
				{
					return;
				}
				while (true)
				{
					_findInPlayer = value;
					bFoWQuDzcOvecUqeSCdTCzgfmRIE();
					int num = 1964509213;
					while (true)
					{
						switch (num ^ 0x7518081F)
						{
						default:
							return;
						case 2:
							return;
						case 0:
							goto IL_000a;
						case 1:
							break;
						}
						break;
						IL_000a:
						num = 1964509214;
					}
				}
			}
		}

		public int playerId
		{
			get
			{
				return _playerId;
			}
			set
			{
				if (_playerId != value)
				{
					_playerId = value;
					bFoWQuDzcOvecUqeSCdTCzgfmRIE();
				}
			}
		}

		internal Rewired.CustomController GetCustomController()
		{
			if (!ReInput.isReady)
			{
				return null;
			}
			if (findInPlayer)
			{
				goto IL_0014;
			}
			goto IL_0099;
			IL_0019:
			int num;
			Rewired.CustomController customController = default(Rewired.CustomController);
			int num3 = default(int);
			while (true)
			{
				switch (num ^ 0x5B164338)
				{
				case 6:
					break;
				case 1:
					goto IL_0055;
				case 7:
					Logger.LogError("Invalid playerId " + playerId);
					return null;
				case 3:
					goto IL_00a5;
				case 10:
					num = 1528185649;
					continue;
				case 8:
					goto IL_00cb;
				case 2:
					return customController;
				case 9:
					goto IL_010c;
				case 5:
					goto IL_012d;
				case 4:
					goto IL_014e;
				default:
					return null;
				}
				break;
				IL_012d:
				Player player = ReInput.players.GetPlayer(playerId);
				if (player == null)
				{
					num = 1528185663;
					continue;
				}
				goto IL_0099;
				IL_00a5:
				int num2;
				if (!findInPlayer)
				{
					num = 1528185658;
					num2 = num;
				}
				else
				{
					num = 1528185657;
					num2 = num;
				}
				continue;
				IL_010c:
				int num4;
				if (num3 < ReInput.controllers.customControllerCount)
				{
					num = 1528185648;
					num4 = num;
				}
				else
				{
					num = 1528185656;
					num4 = num;
				}
				continue;
				IL_014e:
				if (!findUsingTag)
				{
					goto IL_00a5;
				}
				if (!(customController.tag != tag))
				{
					num = 1528185659;
					continue;
				}
				goto IL_00fe;
				IL_0055:
				if (ReInput.controllers.IsControllerAssignedToPlayer(customController.type, customController.id, playerId))
				{
					num = 1528185658;
					continue;
				}
				goto IL_00fe;
				IL_00cb:
				customController = ReInput.controllers.CustomControllers[num3];
				if (findUsingSourceId)
				{
					if (customController.sourceControllerId == sourceId)
					{
						num = 1528185660;
						continue;
					}
					goto IL_00fe;
				}
				goto IL_014e;
				IL_00fe:
				num3++;
				num = 1528185649;
			}
			goto IL_0014;
			IL_0014:
			num = 1528185661;
			goto IL_0019;
			IL_0099:
			num3 = 0;
			num = 1528185650;
			goto IL_0019;
		}

		private void bFoWQuDzcOvecUqeSCdTCzgfmRIE()
		{
		}
	}
}
