using System;
using UnityEngine;

namespace Rewired.Utils.Attributes
{
	[AttributeUsage(AttributeTargets.Field, AllowMultiple = false, Inherited = true)]
	public class FieldRangeAttribute : PropertyAttribute
	{
		private float HMGbOEtOuQTvMFIjblTfZrbKEXn;

		private float AHvXDMCklgDPHCfiKTBZFGrgKPF;

		private int YeEkInlHAAhUqERsppBEhSFcdGZE;

		private int ZoOHcnIlEglbrXnqpBgvRpCHpnC;

		public float minFloat => HMGbOEtOuQTvMFIjblTfZrbKEXn;

		public float maxFloat => AHvXDMCklgDPHCfiKTBZFGrgKPF;

		public int minInt => YeEkInlHAAhUqERsppBEhSFcdGZE;

		public int maxInt => ZoOHcnIlEglbrXnqpBgvRpCHpnC;

		public FieldRangeAttribute(float min, float max)
		{
			HMGbOEtOuQTvMFIjblTfZrbKEXn = min;
			AHvXDMCklgDPHCfiKTBZFGrgKPF = max;
			YeEkInlHAAhUqERsppBEhSFcdGZE = (int)min;
			ZoOHcnIlEglbrXnqpBgvRpCHpnC = (int)max;
		}

		public FieldRangeAttribute(int min, int max)
		{
			YeEkInlHAAhUqERsppBEhSFcdGZE = min;
			ZoOHcnIlEglbrXnqpBgvRpCHpnC = max;
			HMGbOEtOuQTvMFIjblTfZrbKEXn = min;
			AHvXDMCklgDPHCfiKTBZFGrgKPF = max;
		}
	}
}
