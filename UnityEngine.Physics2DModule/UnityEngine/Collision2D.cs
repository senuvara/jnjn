using System.Runtime.CompilerServices;
using System.Runtime.InteropServices;
using UnityEngine.Scripting;

namespace UnityEngine
{
	/// <summary>
	///   <para>Collision details returned by 2D physics callback functions.</para>
	/// </summary>
	[StructLayout(LayoutKind.Sequential)]
	[RequiredByNativeCode]
	public class Collision2D
	{
		internal int m_Collider;

		internal int m_OtherCollider;

		internal int m_Rigidbody;

		internal int m_OtherRigidbody;

		internal ContactPoint2D[] m_Contacts;

		internal Vector2 m_RelativeVelocity;

		internal int m_Enabled;

		/// <summary>
		///   <para>The incoming Collider2D involved in the collision with the otherCollider.</para>
		/// </summary>
		public Collider2D collider => Physics2D.GetColliderFromInstanceID(m_Collider);

		/// <summary>
		///   <para>The other Collider2D involved in the collision with the collider.</para>
		/// </summary>
		public Collider2D otherCollider => Physics2D.GetColliderFromInstanceID(m_OtherCollider);

		/// <summary>
		///   <para>The incoming Rigidbody2D involved in the collision with the otherRigidbody.</para>
		/// </summary>
		public Rigidbody2D rigidbody => Physics2D.GetRigidbodyFromInstanceID(m_Rigidbody);

		/// <summary>
		///   <para>The other Rigidbody2D involved in the collision with the rigidbody.</para>
		/// </summary>
		public Rigidbody2D otherRigidbody => Physics2D.GetRigidbodyFromInstanceID(m_OtherRigidbody);

		/// <summary>
		///   <para>The Transform of the incoming object involved in the collision.</para>
		/// </summary>
		public Transform transform => (!(rigidbody != null)) ? collider.transform : rigidbody.transform;

		/// <summary>
		///   <para>The incoming GameObject involved in the collision.</para>
		/// </summary>
		public GameObject gameObject => (!(rigidbody != null)) ? collider.gameObject : rigidbody.gameObject;

		/// <summary>
		///   <para>The specific points of contact with the incoming Collider2D.  You should avoid using this as it produces memory garbage.  Use GetContacts instead.</para>
		/// </summary>
		public ContactPoint2D[] contacts
		{
			get
			{
				if (m_Contacts == null)
				{
					m_Contacts = CreateCollisionContacts(collider, otherCollider, rigidbody, otherRigidbody, enabled);
				}
				return m_Contacts;
			}
		}

		/// <summary>
		///   <para>The relative linear velocity of the two colliding objects (Read Only).</para>
		/// </summary>
		public Vector2 relativeVelocity => m_RelativeVelocity;

		/// <summary>
		///   <para>Indicates whether the collision response or reaction is enabled or disabled.</para>
		/// </summary>
		public bool enabled => m_Enabled == 1;

		[MethodImpl(MethodImplOptions.InternalCall)]
		[GeneratedByOldBindingsGenerator]
		private static extern ContactPoint2D[] CreateCollisionContacts(Collider2D collider, Collider2D otherCollider, Rigidbody2D rigidbody, Rigidbody2D otherRigidbody, bool enabled);

		/// <summary>
		///   <para>Retrieves all contact points in for contacts between collider and otherCollider.</para>
		/// </summary>
		/// <param name="contacts">An array of ContactPoint2D used to receive the results.</param>
		/// <returns>
		///   <para>Returns the number of contacts placed in the contacts array.</para>
		/// </returns>
		public int GetContacts(ContactPoint2D[] contacts)
		{
			return Physics2D.GetContacts(collider, otherCollider, default(ContactFilter2D).NoFilter(), contacts);
		}
	}
}
