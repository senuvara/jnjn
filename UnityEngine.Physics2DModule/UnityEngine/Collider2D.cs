using System.Runtime.CompilerServices;
using UnityEngine.Bindings;
using UnityEngine.Internal;
using UnityEngine.Scripting;

namespace UnityEngine
{
	/// <summary>
	///   <para>Parent class for collider types used with 2D gameplay.</para>
	/// </summary>
	[NativeHeader("Modules/Physics2D/Public/Collider2D.h")]
	[RequireComponent(typeof(Transform))]
	[RequireComponent(typeof(Transform))]
	public class Collider2D : Behaviour
	{
		/// <summary>
		///   <para>The density of the collider used to calculate its mass (when auto mass is enabled).</para>
		/// </summary>
		public float density
		{
			[MethodImpl(MethodImplOptions.InternalCall)]
			get;
			[MethodImpl(MethodImplOptions.InternalCall)]
			set;
		}

		/// <summary>
		///   <para>Is this collider configured as a trigger?</para>
		/// </summary>
		public bool isTrigger
		{
			[MethodImpl(MethodImplOptions.InternalCall)]
			get;
			[MethodImpl(MethodImplOptions.InternalCall)]
			set;
		}

		/// <summary>
		///   <para>Whether the collider is used by an attached effector or not.</para>
		/// </summary>
		public bool usedByEffector
		{
			[MethodImpl(MethodImplOptions.InternalCall)]
			get;
			[MethodImpl(MethodImplOptions.InternalCall)]
			set;
		}

		/// <summary>
		///   <para>Sets whether the Collider will be used or not used by a CompositeCollider2D.</para>
		/// </summary>
		public bool usedByComposite
		{
			[MethodImpl(MethodImplOptions.InternalCall)]
			get;
			[MethodImpl(MethodImplOptions.InternalCall)]
			set;
		}

		/// <summary>
		///   <para>Get the CompositeCollider2D that is available to be attached to the collider.</para>
		/// </summary>
		public CompositeCollider2D composite
		{
			[MethodImpl(MethodImplOptions.InternalCall)]
			get;
		}

		/// <summary>
		///   <para>The local offset of the collider geometry.</para>
		/// </summary>
		public Vector2 offset
		{
			get
			{
				get_offset_Injected(out Vector2 ret);
				return ret;
			}
			set
			{
				set_offset_Injected(ref value);
			}
		}

		/// <summary>
		///   <para>The Rigidbody2D attached to the Collider2D.</para>
		/// </summary>
		public Rigidbody2D attachedRigidbody
		{
			[MethodImpl(MethodImplOptions.InternalCall)]
			[NativeMethod("GetAttachedRigidbody_Binding")]
			get;
		}

		/// <summary>
		///   <para>The number of separate shaped regions in the collider.</para>
		/// </summary>
		public int shapeCount
		{
			[MethodImpl(MethodImplOptions.InternalCall)]
			get;
		}

		/// <summary>
		///   <para>The world space bounding area of the collider.</para>
		/// </summary>
		public Bounds bounds
		{
			get
			{
				get_bounds_Injected(out Bounds ret);
				return ret;
			}
		}

		internal ColliderErrorState2D errorState
		{
			[MethodImpl(MethodImplOptions.InternalCall)]
			get;
		}

		internal bool compositeCapable
		{
			[MethodImpl(MethodImplOptions.InternalCall)]
			[NativeMethod("GetCompositeCapable_Binding")]
			get;
		}

		/// <summary>
		///   <para>The PhysicsMaterial2D that is applied to this collider.</para>
		/// </summary>
		public PhysicsMaterial2D sharedMaterial
		{
			[MethodImpl(MethodImplOptions.InternalCall)]
			[NativeMethod("GetMaterial")]
			get;
			[MethodImpl(MethodImplOptions.InternalCall)]
			[NativeMethod("SetMaterial")]
			set;
		}

		/// <summary>
		///   <para>Get the friction used by the collider.</para>
		/// </summary>
		public float friction
		{
			[MethodImpl(MethodImplOptions.InternalCall)]
			get;
		}

		/// <summary>
		///   <para>Get the bounciness used by the collider.</para>
		/// </summary>
		public float bounciness
		{
			[MethodImpl(MethodImplOptions.InternalCall)]
			get;
		}

		/// <summary>
		///   <para>Get a list of all colliders that overlap this collider.</para>
		/// </summary>
		/// <param name="contactFilter">The contact filter used to filter the results differently, such as by layer mask, Z depth.  Note that normal angle is not used for overlap testing.</param>
		/// <param name="results">The array to receive results.  The size of the array determines the maximum number of results that can be returned.</param>
		/// <returns>
		///   <para>Returns the number of results placed in the results array.</para>
		/// </returns>
		public int OverlapCollider(ContactFilter2D contactFilter, Collider2D[] results)
		{
			return Physics2D.OverlapCollider(this, contactFilter, results);
		}

		[ExcludeFromDocs]
		public int Raycast(Vector2 direction, ContactFilter2D contactFilter, RaycastHit2D[] results)
		{
			float distance = float.PositiveInfinity;
			return Raycast(direction, contactFilter, results, distance);
		}

		/// <summary>
		///   <para>Casts a ray into the scene starting at the collider position ignoring the collider itself.</para>
		/// </summary>
		/// <param name="direction">Vector representing the direction of the ray.</param>
		/// <param name="results">Array to receive results.</param>
		/// <param name="distance">Maximum distance over which to cast the ray.</param>
		/// <param name="layerMask">Filter to check objects only on specific layers.</param>
		/// <param name="minDepth">Only include objects with a Z coordinate (depth) greater than this value.</param>
		/// <param name="maxDepth">Only include objects with a Z coordinate (depth) less than this value.</param>
		/// <param name="contactFilter">Filter results defined by the contact filter.</param>
		/// <returns>
		///   <para>The number of results returned.</para>
		/// </returns>
		public int Raycast(Vector2 direction, ContactFilter2D contactFilter, RaycastHit2D[] results, [DefaultValue("Mathf.Infinity")] float distance)
		{
			return Internal_Raycast(direction, distance, contactFilter, results);
		}

		[ExcludeFromDocs]
		public int Raycast(Vector2 direction, RaycastHit2D[] results, float distance, int layerMask, float minDepth)
		{
			float maxDepth = float.PositiveInfinity;
			return Raycast(direction, results, distance, layerMask, minDepth, maxDepth);
		}

		[ExcludeFromDocs]
		public int Raycast(Vector2 direction, RaycastHit2D[] results, float distance, int layerMask)
		{
			float maxDepth = float.PositiveInfinity;
			float minDepth = float.NegativeInfinity;
			return Raycast(direction, results, distance, layerMask, minDepth, maxDepth);
		}

		[ExcludeFromDocs]
		public int Raycast(Vector2 direction, RaycastHit2D[] results, float distance)
		{
			float maxDepth = float.PositiveInfinity;
			float minDepth = float.NegativeInfinity;
			int layerMask = -1;
			return Raycast(direction, results, distance, layerMask, minDepth, maxDepth);
		}

		[ExcludeFromDocs]
		public int Raycast(Vector2 direction, RaycastHit2D[] results)
		{
			float maxDepth = float.PositiveInfinity;
			float minDepth = float.NegativeInfinity;
			int layerMask = -1;
			float distance = float.PositiveInfinity;
			return Raycast(direction, results, distance, layerMask, minDepth, maxDepth);
		}

		/// <summary>
		///   <para>Casts a ray into the scene starting at the collider position ignoring the collider itself.</para>
		/// </summary>
		/// <param name="direction">Vector representing the direction of the ray.</param>
		/// <param name="results">Array to receive results.</param>
		/// <param name="distance">Maximum distance over which to cast the ray.</param>
		/// <param name="layerMask">Filter to check objects only on specific layers.</param>
		/// <param name="minDepth">Only include objects with a Z coordinate (depth) greater than this value.</param>
		/// <param name="maxDepth">Only include objects with a Z coordinate (depth) less than this value.</param>
		/// <param name="contactFilter">Filter results defined by the contact filter.</param>
		/// <returns>
		///   <para>The number of results returned.</para>
		/// </returns>
		public int Raycast(Vector2 direction, RaycastHit2D[] results, [DefaultValue("Mathf.Infinity")] float distance, [DefaultValue("Physics2D.AllLayers")] int layerMask, [DefaultValue("-Mathf.Infinity")] float minDepth, [DefaultValue("Mathf.Infinity")] float maxDepth)
		{
			ContactFilter2D contactFilter = ContactFilter2D.CreateLegacyFilter(layerMask, minDepth, maxDepth);
			return Internal_Raycast(direction, distance, contactFilter, results);
		}

		private int Internal_Raycast(Vector2 direction, float distance, ContactFilter2D contactFilter, RaycastHit2D[] results)
		{
			return INTERNAL_CALL_Internal_Raycast(this, ref direction, distance, ref contactFilter, results);
		}

		[MethodImpl(MethodImplOptions.InternalCall)]
		[GeneratedByOldBindingsGenerator]
		private static extern int INTERNAL_CALL_Internal_Raycast(Collider2D self, ref Vector2 direction, float distance, ref ContactFilter2D contactFilter, RaycastHit2D[] results);

		[ExcludeFromDocs]
		public int Cast(Vector2 direction, ContactFilter2D contactFilter, RaycastHit2D[] results, float distance)
		{
			bool ignoreSiblingColliders = true;
			return Cast(direction, contactFilter, results, distance, ignoreSiblingColliders);
		}

		[ExcludeFromDocs]
		public int Cast(Vector2 direction, ContactFilter2D contactFilter, RaycastHit2D[] results)
		{
			bool ignoreSiblingColliders = true;
			float distance = float.PositiveInfinity;
			return Cast(direction, contactFilter, results, distance, ignoreSiblingColliders);
		}

		/// <summary>
		///   <para>Casts the collider shape into the scene starting at the collider position ignoring the collider itself.</para>
		/// </summary>
		/// <param name="direction">Vector representing the direction to cast the shape.</param>
		/// <param name="contactFilter">Filter results defined by the contact filter.</param>
		/// <param name="results">Array to receive results.</param>
		/// <param name="distance">Maximum distance over which to cast the shape.</param>
		/// <param name="ignoreSiblingColliders">Should colliders attached to the same Rigidbody2D (known as sibling colliders) be ignored?</param>
		/// <returns>
		///   <para>The number of results returned.</para>
		/// </returns>
		public int Cast(Vector2 direction, ContactFilter2D contactFilter, RaycastHit2D[] results, [DefaultValue("Mathf.Infinity")] float distance, [DefaultValue("true")] bool ignoreSiblingColliders)
		{
			return Internal_Cast(direction, contactFilter, distance, ignoreSiblingColliders, results);
		}

		[ExcludeFromDocs]
		public int Cast(Vector2 direction, RaycastHit2D[] results, float distance)
		{
			bool ignoreSiblingColliders = true;
			return Cast(direction, results, distance, ignoreSiblingColliders);
		}

		[ExcludeFromDocs]
		public int Cast(Vector2 direction, RaycastHit2D[] results)
		{
			bool ignoreSiblingColliders = true;
			float distance = float.PositiveInfinity;
			return Cast(direction, results, distance, ignoreSiblingColliders);
		}

		/// <summary>
		///   <para>Casts the collider shape into the scene starting at the collider position ignoring the collider itself.</para>
		/// </summary>
		/// <param name="direction">Vector representing the direction to cast the shape.</param>
		/// <param name="results">Array to receive results.</param>
		/// <param name="distance">Maximum distance over which to cast the shape.</param>
		/// <param name="ignoreSiblingColliders">Should colliders attached to the same Rigidbody2D (known as sibling colliders) be ignored?</param>
		/// <returns>
		///   <para>The number of results returned.</para>
		/// </returns>
		public int Cast(Vector2 direction, RaycastHit2D[] results, [DefaultValue("Mathf.Infinity")] float distance, [DefaultValue("true")] bool ignoreSiblingColliders)
		{
			ContactFilter2D contactFilter = default(ContactFilter2D);
			contactFilter.useTriggers = Physics2D.queriesHitTriggers;
			contactFilter.SetLayerMask(Physics2D.GetLayerCollisionMask(base.gameObject.layer));
			return Internal_Cast(direction, contactFilter, distance, ignoreSiblingColliders, results);
		}

		private int Internal_Cast(Vector2 direction, ContactFilter2D contactFilter, float distance, bool ignoreSiblingColliders, RaycastHit2D[] results)
		{
			return INTERNAL_CALL_Internal_Cast(this, ref direction, ref contactFilter, distance, ignoreSiblingColliders, results);
		}

		[MethodImpl(MethodImplOptions.InternalCall)]
		[GeneratedByOldBindingsGenerator]
		private static extern int INTERNAL_CALL_Internal_Cast(Collider2D self, ref Vector2 direction, ref ContactFilter2D contactFilter, float distance, bool ignoreSiblingColliders, RaycastHit2D[] results);

		/// <summary>
		///   <para>Retrieves all contact points for this collider.</para>
		/// </summary>
		/// <param name="contacts">An array of ContactPoint2D used to receive the results.</param>
		/// <returns>
		///   <para>Returns the number of contacts placed in the contacts array.</para>
		/// </returns>
		public int GetContacts(ContactPoint2D[] contacts)
		{
			return Physics2D.GetContacts(this, default(ContactFilter2D).NoFilter(), contacts);
		}

		/// <summary>
		///   <para>Retrieves all contact points for this collider, with the results filtered by the ContactFilter2D.</para>
		/// </summary>
		/// <param name="contactFilter">The contact filter used to filter the results differently, such as by layer mask, Z depth, or normal angle.</param>
		/// <param name="contacts">An array of ContactPoint2D used to receive the results.</param>
		/// <returns>
		///   <para>Returns the number of contacts placed in the contacts array.</para>
		/// </returns>
		public int GetContacts(ContactFilter2D contactFilter, ContactPoint2D[] contacts)
		{
			return Physics2D.GetContacts(this, contactFilter, contacts);
		}

		/// <summary>
		///   <para>Retrieves all colliders in contact with this collider.</para>
		/// </summary>
		/// <param name="colliders">An array of Collider2D used to receive the results.</param>
		/// <returns>
		///   <para>Returns the number of contacts placed in the colliders array.</para>
		/// </returns>
		public int GetContacts(Collider2D[] colliders)
		{
			return Physics2D.GetContacts(this, default(ContactFilter2D).NoFilter(), colliders);
		}

		/// <summary>
		///   <para>Retrieves all colliders in contact with this collider, with the results filtered by the ContactFilter2D.</para>
		/// </summary>
		/// <param name="contactFilter">The contact filter used to filter the results differently, such as by layer mask, Z depth, or normal angle.</param>
		/// <param name="colliders">An array of Collider2D used to receive the results.</param>
		/// <returns>
		///   <para>Returns the number of collidersplaced in the colliders array.</para>
		/// </returns>
		public int GetContacts(ContactFilter2D contactFilter, Collider2D[] colliders)
		{
			return Physics2D.GetContacts(this, contactFilter, colliders);
		}

		/// <summary>
		///   <para>Check whether this collider is touching the collider or not.</para>
		/// </summary>
		/// <param name="collider">The collider to check if it is touching this collider.</param>
		/// <returns>
		///   <para>Whether this collider is touching the collider or not.</para>
		/// </returns>
		[MethodImpl(MethodImplOptions.InternalCall)]
		public extern bool IsTouching([NotNull] [Writable] Collider2D collider);

		/// <summary>
		///   <para>Check whether this collider is touching the collider or not with the results filtered by the ContactFilter2D.</para>
		/// </summary>
		/// <param name="collider">The collider to check if it is touching this collider.</param>
		/// <param name="contactFilter">The contact filter used to filter the results differently, such as by layer mask, Z depth, or normal angle.</param>
		/// <returns>
		///   <para>Whether this collider is touching the collider or not.</para>
		/// </returns>
		public bool IsTouching([Writable] Collider2D collider, ContactFilter2D contactFilter)
		{
			return IsTouching_OtherColliderWithFilter(collider, contactFilter);
		}

		[NativeMethod("IsTouching")]
		private bool IsTouching_OtherColliderWithFilter([NotNull] [Writable] Collider2D collider, ContactFilter2D contactFilter)
		{
			return IsTouching_OtherColliderWithFilter_Injected(collider, ref contactFilter);
		}

		/// <summary>
		///   <para>Check whether this collider is touching other colliders or not with the results filtered by the ContactFilter2D.</para>
		/// </summary>
		/// <param name="contactFilter">The contact filter used to filter the results differently, such as by layer mask, Z depth, or normal angle.</param>
		/// <returns>
		///   <para>Whether this collider is touching the collider or not.</para>
		/// </returns>
		public bool IsTouching(ContactFilter2D contactFilter)
		{
			return IsTouching_AnyColliderWithFilter(contactFilter);
		}

		[NativeMethod("IsTouching")]
		private bool IsTouching_AnyColliderWithFilter(ContactFilter2D contactFilter)
		{
			return IsTouching_AnyColliderWithFilter_Injected(ref contactFilter);
		}

		public bool IsTouchingLayers()
		{
			return IsTouchingLayers(-1);
		}

		/// <summary>
		///   <para>Checks whether this collider is touching any colliders on the specified layerMask or not.</para>
		/// </summary>
		/// <param name="layerMask">Any colliders on any of these layers count as touching.</param>
		/// <returns>
		///   <para>Whether this collider is touching any collider on the specified layerMask or not.</para>
		/// </returns>
		[MethodImpl(MethodImplOptions.InternalCall)]
		public extern bool IsTouchingLayers([DefaultValue("Physics2D.AllLayers")] int layerMask);

		/// <summary>
		///   <para>Check if a collider overlaps a point in space.</para>
		/// </summary>
		/// <param name="point">A point in world space.</param>
		/// <returns>
		///   <para>Does point overlap the collider?</para>
		/// </returns>
		public bool OverlapPoint(Vector2 point)
		{
			return OverlapPoint_Injected(ref point);
		}

		/// <summary>
		///   <para>Calculates the minimum separation of this collider against another collider.</para>
		/// </summary>
		/// <param name="collider">A collider used to calculate the minimum separation against this collider.</param>
		/// <returns>
		///   <para>The minimum separation of collider and this collider.</para>
		/// </returns>
		public ColliderDistance2D Distance([Writable] Collider2D collider)
		{
			return Physics2D.Distance(this, collider);
		}

		[MethodImpl(MethodImplOptions.InternalCall)]
		private extern void get_offset_Injected(out Vector2 ret);

		[MethodImpl(MethodImplOptions.InternalCall)]
		private extern void set_offset_Injected(ref Vector2 value);

		[MethodImpl(MethodImplOptions.InternalCall)]
		private extern void get_bounds_Injected(out Bounds ret);

		[MethodImpl(MethodImplOptions.InternalCall)]
		private extern bool IsTouching_OtherColliderWithFilter_Injected([Writable] Collider2D collider, ref ContactFilter2D contactFilter);

		[MethodImpl(MethodImplOptions.InternalCall)]
		private extern bool IsTouching_AnyColliderWithFilter_Injected(ref ContactFilter2D contactFilter);

		[MethodImpl(MethodImplOptions.InternalCall)]
		private extern bool OverlapPoint_Injected(ref Vector2 point);
	}
}
