using System.Runtime.CompilerServices;
using UnityEngine.Bindings;
using UnityEngine.Internal;
using UnityEngine.Scripting;

namespace UnityEngine
{
	/// <summary>
	///   <para>Collider for 2D physics representing an arbitrary polygon defined by its vertices.</para>
	/// </summary>
	[NativeHeader("Modules/Physics2D/Public/PolygonCollider2D.h")]
	public sealed class PolygonCollider2D : Collider2D
	{
		/// <summary>
		///   <para>Corner points that define the collider's shape in local space.</para>
		/// </summary>
		public Vector2[] points
		{
			[MethodImpl(MethodImplOptions.InternalCall)]
			[GeneratedByOldBindingsGenerator]
			get;
			[MethodImpl(MethodImplOptions.InternalCall)]
			[GeneratedByOldBindingsGenerator]
			set;
		}

		/// <summary>
		///   <para>The number of paths in the polygon.</para>
		/// </summary>
		public int pathCount
		{
			[MethodImpl(MethodImplOptions.InternalCall)]
			get;
			[MethodImpl(MethodImplOptions.InternalCall)]
			set;
		}

		/// <summary>
		///   <para>Determines whether the PolygonCollider2D's shape is automatically updated based on a SpriteRenderer's tiling properties.</para>
		/// </summary>
		public bool autoTiling
		{
			[MethodImpl(MethodImplOptions.InternalCall)]
			get;
			[MethodImpl(MethodImplOptions.InternalCall)]
			set;
		}

		/// <summary>
		///   <para>Gets a path from the Collider by its index.</para>
		/// </summary>
		/// <param name="index">The index of the path to retrieve.</param>
		/// <returns>
		///   <para>An ordered array of the vertices or points in the selected path.</para>
		/// </returns>
		[MethodImpl(MethodImplOptions.InternalCall)]
		[GeneratedByOldBindingsGenerator]
		public extern Vector2[] GetPath(int index);

		/// <summary>
		///   <para>Define a path by its constituent points.</para>
		/// </summary>
		/// <param name="index">Index of the path to set.</param>
		/// <param name="points">Points that define the path.</param>
		[MethodImpl(MethodImplOptions.InternalCall)]
		[GeneratedByOldBindingsGenerator]
		public extern void SetPath(int index, Vector2[] points);

		/// <summary>
		///   <para>Creates as regular primitive polygon with the specified number of sides.</para>
		/// </summary>
		/// <param name="sides">The number of sides in the polygon.  This must be greater than two.</param>
		/// <param name="scale">The X/Y scale of the polygon.  These must be greater than zero.</param>
		/// <param name="offset">The X/Y offset of the polygon.</param>
		public void CreatePrimitive(int sides, [DefaultValue("Vector2.one")] Vector2 scale, [DefaultValue("Vector2.zero")] Vector2 offset)
		{
			INTERNAL_CALL_CreatePrimitive(this, sides, ref scale, ref offset);
		}

		[ExcludeFromDocs]
		public void CreatePrimitive(int sides, Vector2 scale)
		{
			Vector2 offset = Vector2.zero;
			INTERNAL_CALL_CreatePrimitive(this, sides, ref scale, ref offset);
		}

		[ExcludeFromDocs]
		public void CreatePrimitive(int sides)
		{
			Vector2 offset = Vector2.zero;
			Vector2 scale = Vector2.one;
			INTERNAL_CALL_CreatePrimitive(this, sides, ref scale, ref offset);
		}

		[MethodImpl(MethodImplOptions.InternalCall)]
		[GeneratedByOldBindingsGenerator]
		private static extern void INTERNAL_CALL_CreatePrimitive(PolygonCollider2D self, int sides, ref Vector2 scale, ref Vector2 offset);

		/// <summary>
		///   <para>Return the total number of points in the polygon in all paths.</para>
		/// </summary>
		[MethodImpl(MethodImplOptions.InternalCall)]
		[NativeMethod("GetPointCount")]
		public extern int GetTotalPointCount();
	}
}
