using System;

namespace UnityEngine
{
	/// <summary>
	///   <para>Cooking options that are available with MeshCollider.</para>
	/// </summary>
	[Flags]
	public enum MeshColliderCookingOptions
	{
		/// <summary>
		///   <para>No optional cooking steps will be run.</para>
		/// </summary>
		None = 0x0,
		/// <summary>
		///   <para>Allow the physics engine to increase the volume of the input mesh in attempt to generate a valid convex mesh.</para>
		/// </summary>
		InflateConvexMesh = 0x1,
		/// <summary>
		///   <para>Toggle between cooking for faster simulation or faster cooking time.</para>
		/// </summary>
		CookForFasterSimulation = 0x2,
		/// <summary>
		///   <para>Toggle cleaning of the mesh.</para>
		/// </summary>
		EnableMeshCleaning = 0x4,
		/// <summary>
		///   <para>Toggle the removal of equal vertices.</para>
		/// </summary>
		WeldColocatedVertices = 0x8
	}
}
