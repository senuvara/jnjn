using System;

namespace ZenFulcrum.EmbeddedBrowser.VR
{
	[Flags]
	public enum JoyPadType
	{
		None = 0x0,
		Unknown = 0x2,
		Joystick = 0x4,
		TouchPad = 0x8
	}
}
