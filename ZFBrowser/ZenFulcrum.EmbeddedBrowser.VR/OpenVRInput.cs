using System;
using System.Runtime.InteropServices;
using System.Text;
using UnityEngine;
using UnityEngine.XR;
using ZenFulcrum.VR.OpenVRBinding;

namespace ZenFulcrum.EmbeddedBrowser.VR
{
	internal class OpenVRInput : VRInput
	{
		private enum InputMode
		{
			Direct,
			MappedActions
		}

		private VRControllerState_t direct_lastState;

		private ulong pointPose;

		private ulong leftClick;

		private ulong middleClick;

		private ulong rightClick;

		private ulong joystickScroll;

		private ulong touchPadScrollTouch;

		private ulong touchPadScrollPos;

		private ulong leftHand;

		private ulong rightHand;

		private JoyPadType jpType = JoyPadType.Unknown;

		private ulong GetActionHandle(string handleName, string name)
		{
			if (string.IsNullOrEmpty(handleName))
			{
				return 0uL;
			}
			ulong pHandle = 0uL;
			EVRInputError actionHandle = OpenVR.Input.GetActionHandle(handleName, ref pHandle);
			if (actionHandle != 0)
			{
				throw new ApplicationException("Failed to set up VR action " + actionHandle);
			}
			return pHandle;
		}

		public static string GetStringProperty(uint deviceId, ETrackedDeviceProperty prop)
		{
			StringBuilder stringBuilder = new StringBuilder(32768);
			ETrackedPropertyError pError = ETrackedPropertyError.TrackedProp_Success;
			OpenVR.System.GetStringTrackedDeviceProperty(deviceId, prop, stringBuilder, 32768u, ref pError);
			if (pError != 0)
			{
				throw new Exception(string.Concat("Failed to get property ", prop, " on device ", deviceId, ": ", pError));
			}
			return stringBuilder.ToString();
		}

		public override string GetNodeName(XRNodeState node)
		{
			uint deviceId = (uint)GetDeviceId(node);
			try
			{
				return GetStringProperty(deviceId, ETrackedDeviceProperty.Prop_ModelNumber_String);
			}
			catch (Exception ex)
			{
				Debug.LogError("Failed to get device name for device " + deviceId + ": " + ex.Message);
				return base.GetNodeName(node);
			}
		}

		protected void Direct_ReadState(XRNodeState node)
		{
			if (OpenVR.System == null)
			{
				Debug.LogWarning("OpenVR not active");
				direct_lastState = default(VRControllerState_t);
				return;
			}
			int deviceId = GetDeviceId(node);
			if (deviceId < 0)
			{
				direct_lastState = default(VRControllerState_t);
			}
			else if (!OpenVR.System.GetControllerState((uint)deviceId, ref direct_lastState, (uint)Marshal.SizeOf(typeof(VRControllerState_t))))
			{
				Debug.LogWarning("Failed to get controller state");
			}
		}

		private bool GetDigitalInput(ulong action, XRNodeState node)
		{
			InputDigitalActionData_t pActionData = default(InputDigitalActionData_t);
			EVRInputError digitalActionData = OpenVR.Input.GetDigitalActionData(action, ref pActionData, (uint)Marshal.SizeOf(typeof(InputDigitalActionData_t)), (node.nodeType != XRNode.LeftHand) ? rightHand : leftHand);
			if (digitalActionData != 0)
			{
				throw new ApplicationException(string.Concat("Failed to get digital input data ", node.nodeType, ": ", digitalActionData));
			}
			return pActionData.bState;
		}

		private Vector2 GetVector2Input(ulong action, XRNodeState node)
		{
			InputAnalogActionData_t pActionData = default(InputAnalogActionData_t);
			EVRInputError analogActionData = OpenVR.Input.GetAnalogActionData(action, ref pActionData, (uint)Marshal.SizeOf(typeof(InputAnalogActionData_t)), (node.nodeType != XRNode.LeftHand) ? rightHand : leftHand);
			if (analogActionData != 0)
			{
				throw new ApplicationException(string.Concat("Failed to get vector input data ", node.nodeType, ": ", analogActionData));
			}
			return new Vector2(pActionData.x, pActionData.y);
		}

		private Pose GetPoseInput(ulong action, XRNodeState node)
		{
			InputPoseActionData_t pActionData = default(InputPoseActionData_t);
			EVRInputError poseActionData = OpenVR.Input.GetPoseActionData(action, (XRDevice.GetTrackingSpaceType() == TrackingSpaceType.RoomScale) ? ETrackingUniverseOrigin.TrackingUniverseStanding : ETrackingUniverseOrigin.TrackingUniverseSeated, 0f, ref pActionData, (uint)Marshal.SizeOf(typeof(InputPoseActionData_t)), (node.nodeType != XRNode.LeftHand) ? rightHand : leftHand);
			if (poseActionData != 0)
			{
				throw new ApplicationException(string.Concat("Failed to get pose input data ", node.nodeType, ": ", poseActionData));
			}
			HmdMatrix34_t mDeviceToAbsoluteTracking = pActionData.pose.mDeviceToAbsoluteTracking;
			Quaternion rotation = new Matrix4x4(new Vector4(mDeviceToAbsoluteTracking.m0, mDeviceToAbsoluteTracking.m1, mDeviceToAbsoluteTracking.m2, 0f), new Vector4(mDeviceToAbsoluteTracking.m4, mDeviceToAbsoluteTracking.m5, mDeviceToAbsoluteTracking.m6, 0f), new Vector4(mDeviceToAbsoluteTracking.m8, mDeviceToAbsoluteTracking.m9, mDeviceToAbsoluteTracking.m10, 0f), new Vector4(0f, 0f, 0f, 1f)).rotation;
			rotation.z *= -1f;
			Pose result = default(Pose);
			result.pos = new Vector3(mDeviceToAbsoluteTracking.m3, mDeviceToAbsoluteTracking.m7, 0f - mDeviceToAbsoluteTracking.m11);
			result.rot = rotation;
			return result;
		}

		public override float GetAxis(XRNodeState node, InputAxis axis)
		{
			throw new ArgumentOutOfRangeException("axis", axis, null);
		}

		public override Pose GetPose(XRNodeState node)
		{
			return GetPoseInput(pointPose, node);
		}

		private int GetDeviceId(XRNodeState node)
		{
			ETrackedControllerRole eTrackedControllerRole = (node.nodeType == XRNode.LeftHand) ? ETrackedControllerRole.LeftHand : ETrackedControllerRole.RightHand;
			for (uint num = 0u; num < 64; num++)
			{
				ETrackedControllerRole controllerRoleForTrackedDeviceIndex = OpenVR.System.GetControllerRoleForTrackedDeviceIndex(num);
				if (controllerRoleForTrackedDeviceIndex == eTrackedControllerRole)
				{
					return (int)num;
				}
			}
			return -1;
		}

		public override JoyPadType GetJoypadTypes(XRNodeState node)
		{
			if (jpType != JoyPadType.Unknown)
			{
				return jpType;
			}
			string nodeName = GetNodeName(node);
			if (nodeName.Contains("Oculus Touch Controller") || nodeName.StartsWith("Oculus Rift CV1"))
			{
				return jpType = JoyPadType.Joystick;
			}
			if (nodeName.StartsWith("Vive Controller"))
			{
				return jpType = JoyPadType.TouchPad;
			}
			Debug.LogWarning("Unknown controller type: " + nodeName);
			return jpType = JoyPadType.None;
		}
	}
}
