using System;

namespace ZenFulcrum.EmbeddedBrowser
{
	[Flags]
	public enum MouseButton
	{
		Left = 0x1,
		Middle = 0x2,
		Right = 0x4
	}
}
