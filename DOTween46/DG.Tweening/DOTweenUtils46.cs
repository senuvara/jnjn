using UnityEngine;

namespace DG.Tweening
{
	public static class DOTweenUtils46
	{
		public static Vector2 SwitchToRectTransform(RectTransform from, RectTransform to)
		{
			Vector2 vector = new Vector2(from.rect.width * 0.5f + from.rect.xMin, from.rect.height * 0.5f + from.rect.yMin);
			Vector2 screenPoint = RectTransformUtility.WorldToScreenPoint(null, from.position);
			screenPoint += vector;
			RectTransformUtility.ScreenPointToLocalPointInRectangle(to, screenPoint, null, out Vector2 localPoint);
			Vector2 b = new Vector2(to.rect.width * 0.5f + to.rect.xMin, to.rect.height * 0.5f + to.rect.yMin);
			return to.anchoredPosition + localPoint - b;
		}
	}
}
