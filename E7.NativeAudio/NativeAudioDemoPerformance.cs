using E7.Native;
using System.Collections;
using System.Collections.Generic;
using System.Diagnostics;
using System.Linq;
using UnityEngine;
using UnityEngine.UI;

public class NativeAudioDemoPerformance : MonoBehaviour
{
	private class PerformanceResult
	{
		public long asLoad;

		public List<long> asTicks = new List<long>();

		public float silenceAnalysis;

		public long naInitialize;

		public long naLoad;

		public List<long> naTicks = new List<long>();

		public string AnalysisText
		{
			get
			{
				if (naTicks.Count == 0)
				{
					naTicks.Add(0L);
				}
				string[] value = new string[10]
				{
					"AudioSource Load : [Ticks] " + asLoad + " [Ms] " + TicksToMs(asLoad),
					"AudioSource Play : [Avg. Ticks] " + asTicks.Average() + " [Avg. Ms] " + TicksToMs(asTicks.Average()) + " [Avg. FPS] " + 1000f / TicksToMs(asTicks.Average()),
					"AudioSource Play : [SD Ticks] " + StdDev(asTicks) + " [SD Ms] " + TicksToMs(StdDev(asTicks)),
					string.Empty,
					"Silent analysis : [Avg. FPS] " + silenceAnalysis,
					string.Empty,
					"NativeAudio Initialize : [Ticks] " + naInitialize + " [Ms] " + TicksToMs(naInitialize),
					"NativeAudio Load : [Ticks] " + naLoad + " [Ms] " + TicksToMs(naLoad),
					"NativeAudio Play : [Avg. Ticks] " + naTicks.Average() + " [Avg. Ms] " + TicksToMs(naTicks.Average()) + " [Avg. FPS] " + 1000f / TicksToMs(naTicks.Average()),
					"NativeAudio Play : [SD Ticks] " + StdDev(naTicks) + " [SD Ms] " + TicksToMs(StdDev(naTicks))
				};
				return string.Join("\n", value);
			}
		}

		private float TicksToMs(long ticks)
		{
			return (float)ticks / 10000f;
		}

		private float TicksToMs(double ticks)
		{
			return (float)(ticks / 10000.0);
		}

		private static float StdDev(IEnumerable<long> values)
		{
			float result = 0f;
			int num = values.Count();
			if (num > 1)
			{
				float avg = (float)values.Average();
				float num2 = values.Sum((long d) => ((float)d - avg) * ((float)d - avg));
				result = Mathf.Sqrt(num2 / (float)num);
			}
			return result;
		}
	}

	[SerializeField]
	private Text fpsText;

	[SerializeField]
	private Text resultText;

	private NativeAudioPointer loaded;

	public AudioSource audioSource;

	public Image background;

	public Color color1;

	public Color color2;

	private Stopwatch sw;

	private PerformanceResult pr;

	private bool running;

	private const float secondsOfPlay = 1.5f;

	private const int framesOfPlay = 90;

	public IEnumerator Start()
	{
		StartCoroutine(FPSUpdate());
		yield break;
	}

	public void Update()
	{
		if (Input.GetKeyDown(KeyCode.A))
		{
			audioSource.Play();
		}
	}

	private IEnumerator FPSUpdate()
	{
		while (true)
		{
			fpsText.text = (1f / Time.deltaTime).ToString("0.00");
			yield return new WaitForSeconds(0.1f);
		}
	}

	public void StartPerformanceTest()
	{
		Application.targetFrameRate = 60;
		if (!running)
		{
			StartCoroutine(PerformanceTest());
		}
	}

	public IEnumerator PerformanceTest()
	{
		running = true;
		sw = new Stopwatch();
		pr = new PerformanceResult();
		audioSource.clip.UnloadAudioData();
		UnityEngine.Debug.Log("Performance test : AudioSource..");
		UnityEngine.Debug.Log("Performance test : AudioSource loading..");
		sw.Start();
		audioSource.clip.LoadAudioData();
		sw.Stop();
		pr.asLoad = sw.ElapsedTicks;
		sw.Reset();
		yield return new WaitForSeconds(0.5f);
		UnityEngine.Debug.Log("Performance test : AudioSource playing..");
		for (int j = 0; j < 90; j++)
		{
			UnityEngine.Debug.Log("Performance test : AudioSource frame " + j);
			sw.Start();
			audioSource.Play();
			yield return null;
			sw.Stop();
			pr.asTicks.Add(sw.ElapsedTicks);
			sw.Reset();
		}
		yield return new WaitForSeconds(0.5f);
		if (!Application.isEditor)
		{
			UnityEngine.Debug.Log("Performance test : NativeAudio initializing..");
			sw.Start();
			NativeAudio.InitializationOptions initializationOptions = new NativeAudio.InitializationOptions();
			initializationOptions.androidAudioTrackCount = 3;
			NativeAudio.Initialize(initializationOptions);
			sw.Stop();
			pr.naInitialize = sw.ElapsedTicks;
			sw.Reset();
			yield return new WaitForSeconds(0.5f);
			UnityEngine.Debug.Log("Performance test : NativeAudio silently analyzing..");
			NativeAudioAnalyzer analyzer = NativeAudio.SilentAnalyze();
			yield return new WaitUntil(() => analyzer.Analyzed);
			pr.silenceAnalysis = analyzer.AnalysisResult.averageFps;
			UnityEngine.Debug.Log("Performance test : NativeAudio loading..");
			sw.Start();
			loaded = NativeAudio.Load("NativeAudioDemo1.wav");
			yield return null;
			sw.Stop();
			pr.naLoad = sw.ElapsedTicks;
			sw.Reset();
			yield return new WaitForSeconds(0.5f);
			UnityEngine.Debug.Log("Performance test : NativeAudio playing..");
			for (int i = 0; i < 90; i++)
			{
				UnityEngine.Debug.Log("Performance test : NativeAudio frame " + i);
				sw.Start();
				loaded.Play();
				yield return null;
				sw.Stop();
				pr.naTicks.Add(sw.ElapsedTicks);
				sw.Reset();
			}
			yield return new WaitForSeconds(0.5f);
		}
		UnityEngine.Debug.Log("Performance test : Ending..");
		resultText.text = pr.AnalysisText;
		running = false;
	}
}
