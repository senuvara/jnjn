using E7.Native;
using System.Collections;
using UnityEngine;
using UnityEngine.UI;

public class NativeAudioDemo : MonoBehaviour
{
	[SerializeField]
	private AudioSource audioSource;

	[Header("Native Audio now works with AudioClip from Unity's importer!")]
	[SerializeField]
	private AudioClip nativeClip1;

	[SerializeField]
	private AudioClip nativeClip2;

	[Space]
	[SerializeField]
	private Text fpsText;

	[SerializeField]
	private Text playbackTimeText;

	[SerializeField]
	private Text descriptionText;

	[Tooltip("Repeatedly play native audio every frame after the scene start without any input. Useful for remote testing on Firebase Test Lab, etc.")]
	[SerializeField]
	private bool autoRepeat;

	[Space]
	[SerializeField]
	private Text bufferSizeText;

	[SerializeField]
	private Slider bufferSizeSlider;

	private NativeAudioPointer nativeAudioPointer;

	private NativeAudioController nativeAudioController;

	private float rememberedTime;

	public void Start()
	{
		if (!NotRealDevice())
		{
			Application.targetFrameRate = 60;
			string text = "Unity output sample rate : " + AudioSettings.outputSampleRate;
			NativeAudio.DeviceAudioInformation deviceAudioInformation = NativeAudio.GetDeviceAudioInformation();
			descriptionText.text = descriptionText.text + "\n" + text + "\n" + deviceAudioInformation;
			bufferSizeSlider.value = 0f;
			bufferSizeText.text = "---";
			if (autoRepeat)
			{
				Initialize();
				LoadAudio1();
				StartCoroutine(RepeatedPlayRoutine());
			}
		}
	}

	private IEnumerator RepeatedPlayRoutine()
	{
		while (true)
		{
			PlayAudio1();
			yield return null;
		}
	}

	public void Update()
	{
		fpsText.text = (1f / Time.deltaTime).ToString("0.00");
	}

	public void UpdateBufferSizeText()
	{
	}

	public void Initialize()
	{
		NativeAudio.InitializationOptions initializationOptions = new NativeAudio.InitializationOptions();
		initializationOptions.androidAudioTrackCount = 2;
		initializationOptions.androidMinimumBufferSize = Mathf.RoundToInt(bufferSizeSlider.value);
		NativeAudio.Initialize(initializationOptions);
	}

	public void DisposeNativeAudio()
	{
		NativeAudio.Dispose();
	}

	public void PlayUnityAudioSource()
	{
		audioSource.Play();
	}

	public void LoadAudio1()
	{
		LoadAudio(nativeClip1);
	}

	public void LoadAudio2()
	{
		LoadAudio(nativeClip2);
	}

	public void LoadAudioSA()
	{
		if (!NotRealDevice())
		{
			UnloadIfLoaded();
			nativeAudioPointer = NativeAudio.Load("NativeAudioDemoSA.wav");
			Debug.Log("Loaded audio of length " + nativeAudioPointer.Length + " from ScriptableAssets");
		}
	}

	private void UnloadIfLoaded()
	{
		if (nativeAudioPointer != null)
		{
			nativeAudioPointer.Unload();
			nativeAudioController = null;
		}
	}

	public bool NotRealDevice()
	{
		return false;
	}

	public void StopLatestPlay()
	{
		if (!NotRealDevice() && nativeAudioController != null)
		{
			nativeAudioController.Stop();
		}
	}

	private void LoadAudio(AudioClip ac)
	{
		if (!NotRealDevice())
		{
			UnloadIfLoaded();
			nativeAudioPointer = NativeAudio.Load(ac);
			Debug.Log("Loaded audio of length " + nativeAudioPointer.Length);
		}
	}

	public void Prepare()
	{
		if (!NotRealDevice())
		{
			nativeAudioPointer.Prepare();
		}
	}

	public void PlayAudio1()
	{
		if (!NotRealDevice())
		{
			nativeAudioController = nativeAudioPointer.Play();
		}
	}

	public void PlayAudio2()
	{
		if (!NotRealDevice())
		{
			NativeAudio.PlayOptions defaultOptions = NativeAudio.PlayOptions.defaultOptions;
			defaultOptions.volume = 0.3f;
			defaultOptions.pan = 1f;
			nativeAudioController = nativeAudioPointer.Play(defaultOptions);
		}
	}

	public void PlayAudio3()
	{
		if (!NotRealDevice())
		{
			NativeAudio.PlayOptions defaultOptions = NativeAudio.PlayOptions.defaultOptions;
			defaultOptions.volume = 0.5f;
			defaultOptions.trackLoop = true;
			nativeAudioController = nativeAudioPointer.Play(defaultOptions);
		}
	}

	public void TrackPause()
	{
		if (nativeAudioController != null)
		{
			nativeAudioController.TrackPause();
		}
	}

	public void TrackResume()
	{
		if (!NotRealDevice() && nativeAudioController != null)
		{
			nativeAudioController.TrackResume();
		}
	}

	public void RememberPause()
	{
		if (!NotRealDevice() && nativeAudioController != null)
		{
			rememberedTime = nativeAudioController.GetPlaybackTime();
			nativeAudioController.Stop();
			Debug.Log("Pause and remembered time " + rememberedTime);
		}
	}

	public void RememberResume()
	{
		if (!NotRealDevice() && nativeAudioPointer != null)
		{
			NativeAudio.PlayOptions defaultOptions = NativeAudio.PlayOptions.defaultOptions;
			defaultOptions.offsetSeconds = rememberedTime;
			nativeAudioPointer.Play(defaultOptions);
			Debug.Log("Resume from time " + rememberedTime);
		}
	}

	public void Unload()
	{
		if (!NotRealDevice())
		{
			nativeAudioPointer.Unload();
		}
	}

	public void GetPlaybackTime()
	{
		if (!NotRealDevice() && nativeAudioController != null)
		{
			playbackTimeText.text = nativeAudioController.GetPlaybackTime().ToString();
		}
	}
}
