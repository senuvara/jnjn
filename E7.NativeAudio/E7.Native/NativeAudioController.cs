using System;

namespace E7.Native
{
	public class NativeAudioController
	{
		public int InstanceIndex
		{
			get;
			private set;
		}

		public NativeAudioController(int InstanceIndex)
		{
			this.InstanceIndex = InstanceIndex;
		}

		private void AssertInitialized()
		{
			if (!NativeAudio.Initialized)
			{
				throw new InvalidOperationException("You cannot use NativeAudioController while Native Audio itself is not in initialized state.");
			}
		}

		public void Stop()
		{
			AssertInitialized();
		}

		public void SetVolume(float volume)
		{
			AssertInitialized();
		}

		public void SetPan(float pan)
		{
			AssertInitialized();
		}

		public float GetPlaybackTime()
		{
			AssertInitialized();
			return 0f;
		}

		public void SetPlaybackTime(float offsetSeconds)
		{
			AssertInitialized();
		}

		public void TrackPause()
		{
			AssertInitialized();
		}

		public void TrackResume()
		{
			AssertInitialized();
		}
	}
}
