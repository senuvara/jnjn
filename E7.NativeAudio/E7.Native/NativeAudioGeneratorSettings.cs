namespace E7.Native
{
	public static class NativeAudioGeneratorSettings
	{
		public const string resourcePath = "NativeAudioResources/";

		public const string libraryName = "NativeAudioLibraryData";

		public const string scriptName = "NativeAudioLibrary.cs";

		public const string libraryClassName = "NativeAudioLibrary";

		public const string libraryCreateMethod = "CreateAssetFile";

		public const string libraryUpdateMethod = "UpdateLibrary";

		public const string libraryGetProperty = "Get";

		public const string libraryGroupClassName = "NativeAudioLibraryGroup";
	}
}
