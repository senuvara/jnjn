using System;
using System.Collections.Generic;

namespace E7.Native
{
	[Serializable]
	public class NativeAudioLibraryGroup
	{
		public virtual IEnumerable<NativeAudioObject> NativeAudioObjects => null;

		public void LoadGroup()
		{
			foreach (NativeAudioObject nativeAudioObject in NativeAudioObjects)
			{
				nativeAudioObject.Load();
			}
		}

		public void UnloadGroup()
		{
			foreach (NativeAudioObject nativeAudioObject in NativeAudioObjects)
			{
				nativeAudioObject.Unload();
			}
		}
	}
}
