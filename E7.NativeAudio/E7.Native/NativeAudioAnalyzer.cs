using System.Collections;
using System.Collections.Generic;
using System.Diagnostics;
using System.Linq;
using UnityEngine;

namespace E7.Native
{
	public class NativeAudioAnalyzer : MonoBehaviour
	{
		private NativeAudioAnalyzerResult analysisResult;

		private const float secondsOfPlay = 1f;

		private const int framesOfPlay = 60;

		public List<long> allTicks = new List<long>();

		private static NativeAudioPointer silence;

		private IEnumerator analyzeRoutine;

		private Stopwatch sw;

		public bool Analyzed => analyzeRoutine == null;

		public NativeAudioAnalyzerResult AnalysisResult => analysisResult;

		private float TicksToMs(long ticks)
		{
			return (float)ticks / 10000f;
		}

		private float TicksToMs(double ticks)
		{
			return (float)(ticks / 10000.0);
		}

		private static float StdDev(IEnumerable<long> values)
		{
			float result = 0f;
			int num = values.Count();
			if (num > 1)
			{
				float avg = (float)values.Average();
				float num2 = values.Sum((long d) => ((float)d - avg) * ((float)d - avg));
				result = Mathf.Sqrt(num2 / (float)num);
			}
			return result;
		}

		public void Analyze()
		{
			if (analyzeRoutine != null)
			{
				StopCoroutine(analyzeRoutine);
			}
			analyzeRoutine = AnalyzeRoutine();
			StartCoroutine(analyzeRoutine);
		}

		public void Finish()
		{
			Object.Destroy(this);
		}

		private IEnumerator AnalyzeRoutine()
		{
			UnityEngine.Debug.Log("Built in analyze start");
			sw = new Stopwatch();
			allTicks = new List<long>();
			if (silence != null)
			{
				silence.Unload();
			}
			silence = NativeAudio.Load(string.Empty);
			for (int j = 0; j < 30; j++)
			{
				silence.Play();
				yield return null;
			}
			for (int i = 0; i < 30; i++)
			{
				sw.Start();
				silence.Play();
				yield return null;
				sw.Stop();
				allTicks.Add(sw.ElapsedTicks);
				sw.Reset();
			}
			analysisResult = new NativeAudioAnalyzerResult
			{
				averageFps = 1000f / TicksToMs(allTicks.Average())
			};
			analyzeRoutine = null;
			UnityEngine.Debug.Log("Built in analyze end");
		}
	}
}
