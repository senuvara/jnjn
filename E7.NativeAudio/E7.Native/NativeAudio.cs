using System;
using System.IO;
using UnityEngine;

namespace E7.Native
{
	public class NativeAudio
	{
		public struct DeviceAudioInformation
		{
		}

		public class InitializationOptions
		{
			public static readonly InitializationOptions defaultOptions = new InitializationOptions();

			public int androidAudioTrackCount = 3;

			public int androidMinimumBufferSize = -1;

			public bool preserveOnMinimize;
		}

		public class LoadOptions
		{
			public enum ResamplingQuality
			{
				SINC_FASTEST = 2,
				ZERO_ORDER_HOLD,
				LINEAR
			}

			public static readonly LoadOptions defaultOptions = new LoadOptions();

			public ResamplingQuality resamplingQuality = ResamplingQuality.SINC_FASTEST;
		}

		public struct PlayOptions
		{
			public static readonly PlayOptions defaultOptions = new PlayOptions
			{
				audioPlayerIndex = -1,
				volume = 1f,
				pan = 0f,
				offsetSeconds = 0f,
				trackLoop = false
			};

			public int audioPlayerIndex;

			public float volume;

			public float pan;

			public float offsetSeconds;

			public bool trackLoop;
		}

		public static bool Initialized
		{
			get;
			private set;
		}

		private static void AssertInitialized()
		{
			if (!Initialized)
			{
				throw new InvalidOperationException("You cannot use Native Audio while in uninitialized state.");
			}
		}

		public static bool OnSupportedPlatform()
		{
			return false;
		}

		public static void Initialize()
		{
			Initialize(InitializationOptions.defaultOptions);
		}

		public static void Dispose()
		{
		}

		public static void Initialize(InitializationOptions initializationOptions)
		{
		}

		public static NativeAudioPointer Load(AudioClip audioClip)
		{
			return Load(audioClip, LoadOptions.defaultOptions);
		}

		public static NativeAudioPointer Load(AudioClip audioClip, LoadOptions loadOptions)
		{
			AssertAudioClip(audioClip);
			AssertInitialized();
			return null;
		}

		private static void AssertAudioClip(AudioClip audioClip)
		{
			if (audioClip.loadType != 0)
			{
				throw new NotSupportedException($"Your audio clip {audioClip.name} load type is not Decompress On Load but {audioClip.loadType}. Native Audio needs to read the raw PCM data by that import mode.");
			}
			if (audioClip.channels != 1 && audioClip.channels != 2)
			{
				throw new NotSupportedException($"Native Audio only supports mono or stereo. Your audio {audioClip.name} has {audioClip.channels} channels");
			}
			if (audioClip.ambisonic)
			{
				throw new NotSupportedException("Native Audio does not support ambisonic audio!");
			}
			if (audioClip.loadState != AudioDataLoadState.Loaded && audioClip.loadInBackground)
			{
				throw new InvalidOperationException("Your audio is not loaded yet while having the import settings Load In Background. Native Audio cannot wait for loading asynchronously for you and it would results in an empty audio. To keep Load In Background import settings, call `audioClip.LoadAudioData()` beforehand and ensure that `audioClip.loadState` is `AudioDataLoadState.Loaded` before calling `NativeAudio.Load`, or remove Load In Background then Native Audio could load it for you.");
			}
		}

		private static short[] AudioClipToShortArray(AudioClip audioClip)
		{
			if (audioClip.loadState != AudioDataLoadState.Loaded && !audioClip.LoadAudioData())
			{
				throw new Exception($"Loading audio {audioClip.name} failed!");
			}
			float[] array = new float[audioClip.samples * audioClip.channels];
			audioClip.GetData(array, 0);
			short[] array2 = new short[audioClip.samples * audioClip.channels];
			for (int i = 0; i < array2.Length; i++)
			{
				array2[i] = (short)(array[i] * 32767f);
			}
			return array2;
		}

		public static NativeAudioPointer Load(string streamingAssetsRelativePath)
		{
			return Load(streamingAssetsRelativePath, LoadOptions.defaultOptions);
		}

		public static NativeAudioPointer Load(string streamingAssetsRelativePath, LoadOptions loadOptions)
		{
			AssertInitialized();
			if (Path.GetExtension(streamingAssetsRelativePath).ToLower() == ".ogg")
			{
				throw new NotSupportedException("Loading via StreamingAssets does not support OGG. Please use the AudioClip overload and set the import settings to Vorbis.");
			}
			return null;
		}

		public static NativeAudioAnalyzer SilentAnalyze()
		{
			AssertInitialized();
			return null;
		}

		public static DeviceAudioInformation GetDeviceAudioInformation()
		{
			return default(DeviceAudioInformation);
		}
	}
}
