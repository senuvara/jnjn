using System;

namespace E7.Native
{
	public class NativeAudioPointer
	{
		private string soundPath;

		private int startingIndex;

		private int amount;

		private bool isUnloaded;

		private int currentIndex;

		public float Length
		{
			get;
			private set;
		}

		public int NextIndex
		{
			get
			{
				int result = currentIndex;
				currentIndex++;
				if (currentIndex > startingIndex + amount - 1)
				{
					currentIndex = startingIndex;
				}
				return result;
			}
		}

		public NativeAudioPointer(string soundPath, int index, float length, int amount = 1)
		{
			this.soundPath = soundPath;
			startingIndex = index;
			this.amount = amount;
			Length = length;
			currentIndex = index;
		}

		private void AssertLoadedAndInitialized()
		{
			if (isUnloaded)
			{
				throw new InvalidOperationException("You cannot use an unloaded NativeAudioPointer.");
			}
			if (!NativeAudio.Initialized)
			{
				throw new InvalidOperationException("You cannot use NativeAudioPointer while Native Audio itself is not in initialized state.");
			}
		}

		public NativeAudioController Play()
		{
			return Play(NativeAudio.PlayOptions.defaultOptions);
		}

		public NativeAudioController Play(NativeAudio.PlayOptions playOptions)
		{
			AssertLoadedAndInitialized();
			int instanceIndex = -1;
			return new NativeAudioController(instanceIndex);
		}

		public void Prepare()
		{
			AssertLoadedAndInitialized();
		}

		public override string ToString()
		{
			return soundPath;
		}

		public void Unload()
		{
			if (!isUnloaded)
			{
				isUnloaded = true;
			}
		}
	}
}
