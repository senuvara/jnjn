using System;
using UnityEngine;

namespace E7.Native
{
	[Serializable]
	public class NativeAudioObject
	{
		private NativeAudioPointer loadedPointer;

		[SerializeField]
		private string streamingAssetsPath;

		[Range(0f, 1f)]
		[SerializeField]
		private float volume = 1f;

		public NativeAudioPointer LoadedPointer => loadedPointer;

		private bool Loaded => loadedPointer != null;

		public virtual NativeAudioObject Get => this;

		public virtual string StreamingAssetsPath
		{
			get
			{
				return streamingAssetsPath;
			}
			set
			{
				streamingAssetsPath = value;
			}
		}

		public virtual float Volume
		{
			get
			{
				return volume;
			}
			set
			{
				volume = value;
			}
		}

		internal void MakeSureItIsLoaded()
		{
			Load();
		}

		public void Load()
		{
			if (!Loaded)
			{
				loadedPointer = NativeAudio.Load(StreamingAssetsPath);
			}
		}

		public void Unload()
		{
			if (Loaded)
			{
				loadedPointer.Unload();
				loadedPointer = null;
			}
		}

		public NativeAudioController Play()
		{
			return Play(NativeAudio.PlayOptions.defaultOptions);
		}

		public NativeAudioController Play(NativeAudio.PlayOptions playOptions)
		{
			if (!Loaded)
			{
				Load();
			}
			playOptions.volume *= Volume;
			return loadedPointer.Play(playOptions);
		}
	}
}
