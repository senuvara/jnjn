using E7.Native;
using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class NativeAudioDemoTimePlot : MonoBehaviour
{
	public float timeLimit = 2f;

	[Space]
	public AudioSource unitySource;

	[Space]
	public LineRenderer actualTime;

	public LineRenderer unityTime;

	public LineRenderer unityDspTime;

	public LineRenderer nativeAudioTime;

	[Space]
	public LineRenderer guideGrid;

	private NativeAudioPointer pointer;

	private List<float> actualTimeList = new List<float>();

	public void Start()
	{
		NativeAudio.Initialize();
		pointer = NativeAudio.Load("NativeAudioDemo1.wav");
	}

	private void TimeCaptureUpdate()
	{
		Debug.Log("Time capture on initialization! " + Time.realtimeSinceStartup);
	}

	public void StartPlotting()
	{
		StartCoroutine(PlottingRoutine());
	}

	private IEnumerator PlottingRoutine()
	{
		float startTime2 = 0f;
		double startDspTime2 = 0.0;
		actualTimeList.Clear();
		unitySource.clip.LoadAudioData();
		actualTime.positionCount = 0;
		unityTime.positionCount = 0;
		unityDspTime.positionCount = 0;
		nativeAudioTime.positionCount = 0;
		guideGrid.positionCount = 0;
		startTime2 = Time.realtimeSinceStartup;
		startDspTime2 = AudioSettings.dspTime;
		unitySource.Play();
		NativeAudioController controller = pointer.Play();
		int vertexCount = 0;
		float timeProgression;
		do
		{
			float realTime = Time.realtimeSinceStartup;
			double dspTime = AudioSettings.dspTime;
			float unitySourceTime = unitySource.time;
			float nativeSourceTime = controller.GetPlaybackTime();
			vertexCount++;
			timeProgression = (realTime - startTime2) / timeLimit;
			PlotNextVertex(actualTime, vertexCount, timeProgression, timeProgression);
			PlotNextVertex(unityDspTime, vertexCount, timeProgression, (float)((dspTime - startDspTime2) / (double)timeLimit));
			PlotNextVertex(unityTime, vertexCount, timeProgression, unitySourceTime / timeLimit);
			PlotNextVertex(nativeAudioTime, vertexCount, timeProgression, nativeSourceTime / timeLimit);
			PlotNextVertex(guideGrid, 3 * (vertexCount - 1) + 1, timeProgression, 0f);
			PlotNextVertex(guideGrid, 3 * (vertexCount - 1) + 2, timeProgression, 1f);
			PlotNextVertex(guideGrid, 3 * (vertexCount - 1) + 3, timeProgression, 0f);
			yield return null;
		}
		while (timeProgression < 1f);
	}

	private Vector3 PlotNextVertex(LineRenderer lr, int vertexCount, float x, float y)
	{
		lr.positionCount = vertexCount;
		Vector3 vector = Camera.main.ViewportToWorldPoint(new Vector3(x, y, 0f));
		vector.z = 0f;
		lr.SetPosition(vertexCount - 1, vector);
		return vector;
	}
}
