using System.Collections.ObjectModel;
using System.Reflection;

namespace System.Runtime.Serialization
{
	public interface IDataContractSurrogate
	{
		object GetCustomDataToExport(MemberInfo memberInfo, Type dataContractType);

		object GetCustomDataToExport(Type clrType, Type dataContractType);

		Type GetDataContractType(Type type);

		object GetDeserializedObject(object obj, Type targetType);

		void GetKnownCustomDataTypes(Collection<Type> customDataTypes);

		object GetObjectToSerialize(object obj, Type targetType);
	}
}
