using Sirenix.Utilities;
using UnityEngine;

namespace Sirenix.Serialization
{
	public static class UnitySerializationInitializer
	{
		private static readonly object LOCK = new object();

		private static bool initialized = false;

		private static void Initialize()
		{
			if (!initialized)
			{
				lock (LOCK)
				{
					if (!initialized)
					{
						GlobalConfig<GlobalSerializationConfig>.LoadInstanceIfAssetExists();
						initialized = true;
					}
				}
			}
		}

		[RuntimeInitializeOnLoadMethod(RuntimeInitializeLoadType.BeforeSceneLoad)]
		private static void InitializeRuntime()
		{
			Initialize();
		}

		private static void InitializeEditor()
		{
			Initialize();
		}
	}
}
