using Sirenix.Serialization.Utilities;
using System;
using System.Collections.Generic;
using System.IO;
using UnityEngine;

namespace Sirenix.Serialization
{
	public static class SerializationUtility
	{
		private static readonly object CACHE_LOCK = new object();

		private static readonly Dictionary<object, object> CacheMappings = new Dictionary<object, object>(ReferenceEqualityComparer<object>.Default);

		public static IDataWriter CreateWriter(Stream stream, SerializationContext context, DataFormat format)
		{
			switch (format)
			{
			case DataFormat.Binary:
				return new BinaryDataWriter(stream, context);
			case DataFormat.JSON:
				return new JsonDataWriter(stream, context);
			case DataFormat.Nodes:
				Debug.LogError(string.Concat("Cannot automatically create a writer for the format '", DataFormat.Nodes, "', because it does not use a stream."));
				return null;
			default:
				throw new NotImplementedException(format.ToString());
			}
		}

		public static IDataReader CreateReader(Stream stream, DeserializationContext context, DataFormat format)
		{
			switch (format)
			{
			case DataFormat.Binary:
				return new BinaryDataReader(stream, context);
			case DataFormat.JSON:
				return new JsonDataReader(stream, context);
			case DataFormat.Nodes:
				Debug.LogError(string.Concat("Cannot automatically create a reader for the format '", DataFormat.Nodes, "', because it does not use a stream."));
				return null;
			default:
				throw new NotImplementedException(format.ToString());
			}
		}

		private static IDataWriter GetCachedWriter(Stream stream, SerializationContext context, DataFormat format)
		{
			lock (CACHE_LOCK)
			{
				switch (format)
				{
				case DataFormat.Binary:
				{
					Cache<BinaryDataWriter> cache2 = Cache<BinaryDataWriter>.Claim();
					BinaryDataWriter value2 = cache2.Value;
					CacheMappings[value2] = cache2;
					value2.Stream = stream;
					value2.Context = context;
					value2.PrepareNewSerializationSession();
					return value2;
				}
				case DataFormat.JSON:
				{
					Cache<JsonDataWriter> cache = Cache<JsonDataWriter>.Claim();
					JsonDataWriter value = cache.Value;
					CacheMappings[value] = cache;
					value.Stream = stream;
					value.Context = context;
					value.PrepareNewSerializationSession();
					return value;
				}
				case DataFormat.Nodes:
					Debug.LogError(string.Concat("Cannot automatically create a writer for the format '", DataFormat.Nodes, "', because it does not use a stream."));
					return null;
				default:
					throw new NotImplementedException(format.ToString());
				}
			}
		}

		private static IDataReader GetCachedReader(Stream stream, DeserializationContext context, DataFormat format)
		{
			lock (CACHE_LOCK)
			{
				switch (format)
				{
				case DataFormat.Binary:
				{
					Cache<BinaryDataReader> cache2 = Cache<BinaryDataReader>.Claim();
					BinaryDataReader value2 = cache2.Value;
					CacheMappings[value2] = cache2;
					value2.Stream = stream;
					value2.Context = context;
					value2.PrepareNewSerializationSession();
					return value2;
				}
				case DataFormat.JSON:
				{
					Cache<JsonDataReader> cache = Cache<JsonDataReader>.Claim();
					JsonDataReader value = cache.Value;
					CacheMappings[value] = cache;
					value.Stream = stream;
					value.Context = context;
					value.PrepareNewSerializationSession();
					return value;
				}
				case DataFormat.Nodes:
					Debug.LogError(string.Concat("Cannot automatically create a writer for the format '", DataFormat.Nodes, "', because it does not use a stream."));
					return null;
				default:
					throw new NotImplementedException(format.ToString());
				}
			}
		}

		private static void FreeCachedReader(IDataReader reader)
		{
			lock (CACHE_LOCK)
			{
				if (CacheMappings.TryGetValue(reader, out object value))
				{
					CacheMappings.Remove(reader);
					if (reader.GetType() == typeof(BinaryDataReader))
					{
						Cache<BinaryDataReader>.Release((Cache<BinaryDataReader>)value);
					}
					else
					{
						if (reader.GetType() != typeof(JsonDataReader))
						{
							throw new NotImplementedException(reader.GetType().FullName);
						}
						Cache<JsonDataReader>.Release((Cache<JsonDataReader>)value);
					}
				}
			}
		}

		private static void FreeCachedWriter(IDataWriter writer)
		{
			lock (CACHE_LOCK)
			{
				if (CacheMappings.TryGetValue(writer, out object value))
				{
					CacheMappings.Remove(writer);
					if (writer.GetType() == typeof(BinaryDataWriter))
					{
						Cache<BinaryDataWriter>.Release((Cache<BinaryDataWriter>)value);
					}
					else
					{
						if (writer.GetType() != typeof(JsonDataWriter))
						{
							throw new NotImplementedException(writer.GetType().FullName);
						}
						Cache<JsonDataWriter>.Release((Cache<JsonDataWriter>)value);
					}
				}
			}
		}

		public static void SerializeValueWeak(object value, IDataWriter writer)
		{
			Serializer.GetForValue(value).WriteValueWeak(value, writer);
			writer.FlushToStream();
		}

		public static void SerializeValueWeak(object value, IDataWriter writer, out List<UnityEngine.Object> unityObjects)
		{
			using (Cache<UnityReferenceResolver> cache = Cache<UnityReferenceResolver>.Claim())
			{
				writer.Context.IndexReferenceResolver = cache.Value;
				Serializer.GetForValue(value).WriteValueWeak(value, writer);
				writer.FlushToStream();
				unityObjects = cache.Value.GetReferencedUnityObjects();
			}
		}

		public static void SerializeValue<T>(T value, IDataWriter writer)
		{
			Serializer.Get<T>().WriteValue(value, writer);
			writer.FlushToStream();
		}

		public static void SerializeValue<T>(T value, IDataWriter writer, out List<UnityEngine.Object> unityObjects)
		{
			using (Cache<UnityReferenceResolver> cache = Cache<UnityReferenceResolver>.Claim())
			{
				writer.Context.IndexReferenceResolver = cache.Value;
				Serializer.Get<T>().WriteValue(value, writer);
				writer.FlushToStream();
				unityObjects = cache.Value.GetReferencedUnityObjects();
			}
		}

		public static void SerializeValueWeak(object value, Stream stream, DataFormat format, SerializationContext context = null)
		{
			IDataWriter cachedWriter = GetCachedWriter(stream, context, format);
			try
			{
				if (context != null)
				{
					SerializeValueWeak(value, cachedWriter);
				}
				else
				{
					using (Cache<SerializationContext> cache = Cache<SerializationContext>.Claim())
					{
						cachedWriter.Context = cache;
						SerializeValueWeak(value, cachedWriter);
					}
				}
			}
			finally
			{
				FreeCachedWriter(cachedWriter);
			}
		}

		public static void SerializeValueWeak(object value, Stream stream, DataFormat format, out List<UnityEngine.Object> unityObjects, SerializationContext context = null)
		{
			IDataWriter cachedWriter = GetCachedWriter(stream, context, format);
			try
			{
				if (context != null)
				{
					SerializeValueWeak(value, cachedWriter, out unityObjects);
				}
				else
				{
					using (Cache<SerializationContext> cache = Cache<SerializationContext>.Claim())
					{
						cachedWriter.Context = cache;
						SerializeValueWeak(value, cachedWriter, out unityObjects);
					}
				}
			}
			finally
			{
				FreeCachedWriter(cachedWriter);
			}
		}

		public static void SerializeValue<T>(T value, Stream stream, DataFormat format, SerializationContext context = null)
		{
			IDataWriter cachedWriter = GetCachedWriter(stream, context, format);
			try
			{
				if (context != null)
				{
					SerializeValue(value, cachedWriter);
				}
				else
				{
					using (Cache<SerializationContext> cache = Cache<SerializationContext>.Claim())
					{
						cachedWriter.Context = cache;
						SerializeValue(value, cachedWriter);
					}
				}
			}
			finally
			{
				FreeCachedWriter(cachedWriter);
			}
		}

		public static void SerializeValue<T>(T value, Stream stream, DataFormat format, out List<UnityEngine.Object> unityObjects, SerializationContext context = null)
		{
			IDataWriter cachedWriter = GetCachedWriter(stream, context, format);
			try
			{
				if (context != null)
				{
					SerializeValue(value, cachedWriter, out unityObjects);
				}
				else
				{
					using (Cache<SerializationContext> cache = Cache<SerializationContext>.Claim())
					{
						cachedWriter.Context = cache;
						SerializeValue(value, cachedWriter, out unityObjects);
					}
				}
			}
			finally
			{
				FreeCachedWriter(cachedWriter);
			}
		}

		public static byte[] SerializeValueWeak(object value, DataFormat format, SerializationContext context = null)
		{
			using (Cache<CachedMemoryStream> cache = CachedMemoryStream.Claim())
			{
				SerializeValueWeak(value, cache.Value.MemoryStream, format, context);
				return cache.Value.MemoryStream.ToArray();
			}
		}

		public static byte[] SerializeValueWeak(object value, DataFormat format, out List<UnityEngine.Object> unityObjects)
		{
			using (Cache<CachedMemoryStream> cache = CachedMemoryStream.Claim())
			{
				SerializeValueWeak(value, cache.Value.MemoryStream, format, out unityObjects);
				return cache.Value.MemoryStream.ToArray();
			}
		}

		public static byte[] SerializeValue<T>(T value, DataFormat format, SerializationContext context = null)
		{
			using (Cache<CachedMemoryStream> cache = CachedMemoryStream.Claim())
			{
				SerializeValue(value, cache.Value.MemoryStream, format, context);
				return cache.Value.MemoryStream.ToArray();
			}
		}

		public static byte[] SerializeValue<T>(T value, DataFormat format, out List<UnityEngine.Object> unityObjects, SerializationContext context = null)
		{
			using (Cache<CachedMemoryStream> cache = CachedMemoryStream.Claim())
			{
				SerializeValue(value, cache.Value.MemoryStream, format, out unityObjects, context);
				return cache.Value.MemoryStream.ToArray();
			}
		}

		public static object DeserializeValueWeak(IDataReader reader)
		{
			return Serializer.Get<object>().ReadValueWeak(reader);
		}

		public static object DeserializeValueWeak(IDataReader reader, List<UnityEngine.Object> referencedUnityObjects)
		{
			using (Cache<UnityReferenceResolver> cache = Cache<UnityReferenceResolver>.Claim())
			{
				cache.Value.SetReferencedUnityObjects(referencedUnityObjects);
				reader.Context.IndexReferenceResolver = cache.Value;
				return Serializer.Get<object>().ReadValueWeak(reader);
			}
		}

		public static T DeserializeValue<T>(IDataReader reader)
		{
			return Serializer.Get<T>().ReadValue(reader);
		}

		public static T DeserializeValue<T>(IDataReader reader, List<UnityEngine.Object> referencedUnityObjects)
		{
			using (Cache<UnityReferenceResolver> cache = Cache<UnityReferenceResolver>.Claim())
			{
				cache.Value.SetReferencedUnityObjects(referencedUnityObjects);
				reader.Context.IndexReferenceResolver = cache.Value;
				return Serializer.Get<T>().ReadValue(reader);
			}
		}

		public static object DeserializeValueWeak(Stream stream, DataFormat format, DeserializationContext context = null)
		{
			IDataReader cachedReader = GetCachedReader(stream, context, format);
			try
			{
				if (context == null)
				{
					using (Cache<DeserializationContext> cache = Cache<DeserializationContext>.Claim())
					{
						cachedReader.Context = cache;
						return DeserializeValueWeak(cachedReader);
					}
				}
				return DeserializeValueWeak(cachedReader);
			}
			finally
			{
				FreeCachedReader(cachedReader);
			}
		}

		public static object DeserializeValueWeak(Stream stream, DataFormat format, List<UnityEngine.Object> referencedUnityObjects, DeserializationContext context = null)
		{
			IDataReader cachedReader = GetCachedReader(stream, context, format);
			try
			{
				if (context == null)
				{
					using (Cache<DeserializationContext> cache = Cache<DeserializationContext>.Claim())
					{
						cachedReader.Context = cache;
						return DeserializeValueWeak(cachedReader, referencedUnityObjects);
					}
				}
				return DeserializeValueWeak(cachedReader, referencedUnityObjects);
			}
			finally
			{
				FreeCachedReader(cachedReader);
			}
		}

		public static T DeserializeValue<T>(Stream stream, DataFormat format, DeserializationContext context = null)
		{
			IDataReader cachedReader = GetCachedReader(stream, context, format);
			try
			{
				if (context == null)
				{
					using (Cache<DeserializationContext> cache = Cache<DeserializationContext>.Claim())
					{
						cachedReader.Context = cache;
						return DeserializeValue<T>(cachedReader);
					}
				}
				return DeserializeValue<T>(cachedReader);
			}
			finally
			{
				FreeCachedReader(cachedReader);
			}
		}

		public static T DeserializeValue<T>(Stream stream, DataFormat format, List<UnityEngine.Object> referencedUnityObjects, DeserializationContext context = null)
		{
			IDataReader cachedReader = GetCachedReader(stream, context, format);
			try
			{
				if (context == null)
				{
					using (Cache<DeserializationContext> cache = Cache<DeserializationContext>.Claim())
					{
						cachedReader.Context = cache;
						return DeserializeValue<T>(cachedReader, referencedUnityObjects);
					}
				}
				return DeserializeValue<T>(cachedReader, referencedUnityObjects);
			}
			finally
			{
				FreeCachedReader(cachedReader);
			}
		}

		public static object DeserializeValueWeak(byte[] bytes, DataFormat format, DeserializationContext context = null)
		{
			using (Cache<CachedMemoryStream> cache = CachedMemoryStream.Claim(bytes))
			{
				return DeserializeValueWeak(cache.Value.MemoryStream, format, context);
			}
		}

		public static object DeserializeValueWeak(byte[] bytes, DataFormat format, List<UnityEngine.Object> referencedUnityObjects)
		{
			using (Cache<CachedMemoryStream> cache = CachedMemoryStream.Claim(bytes))
			{
				return DeserializeValueWeak(cache.Value.MemoryStream, format, referencedUnityObjects);
			}
		}

		public static T DeserializeValue<T>(byte[] bytes, DataFormat format, DeserializationContext context = null)
		{
			using (Cache<CachedMemoryStream> cache = CachedMemoryStream.Claim(bytes))
			{
				return DeserializeValue<T>(cache.Value.MemoryStream, format, context);
			}
		}

		public static T DeserializeValue<T>(byte[] bytes, DataFormat format, List<UnityEngine.Object> referencedUnityObjects, DeserializationContext context = null)
		{
			using (Cache<CachedMemoryStream> cache = CachedMemoryStream.Claim(bytes))
			{
				return DeserializeValue<T>(cache.Value.MemoryStream, format, referencedUnityObjects, context);
			}
		}

		public static object CreateCopy(object obj)
		{
			if (obj == null)
			{
				return null;
			}
			if (obj is string)
			{
				return obj;
			}
			Type type = obj.GetType();
			if (type.IsValueType)
			{
				return obj;
			}
			if (type.InheritsFrom(typeof(UnityEngine.Object)))
			{
				return obj;
			}
			using (MemoryStream memoryStream = new MemoryStream())
			{
				using (Cache<SerializationContext> cache = Cache<SerializationContext>.Claim())
				{
					using (Cache<DeserializationContext> cache2 = Cache<DeserializationContext>.Claim())
					{
						cache.Value.Config.SerializationPolicy = SerializationPolicies.Everything;
						cache2.Value.Config.SerializationPolicy = SerializationPolicies.Everything;
						SerializeValue(obj, memoryStream, DataFormat.Binary, out List<UnityEngine.Object> unityObjects, cache);
						memoryStream.Position = 0L;
						return DeserializeValue<object>(memoryStream, DataFormat.Binary, unityObjects, cache2);
					}
				}
			}
		}
	}
}
