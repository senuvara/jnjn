using System;
using System.Collections.Generic;

namespace Sirenix.Serialization
{
	public abstract class BaseDataReaderWriter
	{
		private Stack<NodeInfo> nodes = new Stack<NodeInfo>(32);

		[Obsolete("Use the Binder member on the writer's SerializationContext/DeserializationContext instead.", false)]
		public TwoWaySerializationBinder Binder
		{
			get
			{
				if (this is IDataWriter)
				{
					return (this as IDataWriter).Context.Binder;
				}
				if (this is IDataReader)
				{
					return (this as IDataReader).Context.Binder;
				}
				return TwoWaySerializationBinder.Default;
			}
			set
			{
				if (this is IDataWriter)
				{
					(this as IDataWriter).Context.Binder = value;
				}
				else if (this is IDataReader)
				{
					(this as IDataReader).Context.Binder = value;
				}
			}
		}

		public bool IsInArrayNode => CurrentNode.IsArray;

		protected int NodeDepth => nodes.Count;

		protected NodeInfo CurrentNode
		{
			get
			{
				if (nodes.Count != 0)
				{
					return nodes.Peek();
				}
				return NodeInfo.Empty;
			}
		}

		protected void PushNode(NodeInfo node)
		{
			nodes.Push(node);
		}

		protected void PushNode(string name, int id, Type type)
		{
			nodes.Push(new NodeInfo(name, id, type, isArray: false));
		}

		protected void PushArray()
		{
			NodeInfo t;
			if (NodeDepth == 0 || CurrentNode.IsArray)
			{
				t = new NodeInfo(null, -1, null, isArray: true);
			}
			else
			{
				NodeInfo currentNode = CurrentNode;
				t = new NodeInfo(currentNode.Name, currentNode.Id, currentNode.Type, isArray: true);
			}
			nodes.Push(t);
		}

		protected void PopNode(string name)
		{
			if (nodes.Count == 0)
			{
				throw new InvalidOperationException("There are no nodes to pop.");
			}
			NodeInfo currentNode = CurrentNode;
			if (currentNode.Name != name)
			{
				throw new InvalidOperationException("Tried to pop node with name " + name + " but current node's name is " + currentNode.Name);
			}
			nodes.Pop();
		}

		protected void PopArray()
		{
			if (!CurrentNode.IsArray)
			{
				throw new InvalidOperationException("Was not in array when exiting array.");
			}
			nodes.Pop();
		}

		protected void ClearNodes()
		{
			nodes.Clear();
		}
	}
}
