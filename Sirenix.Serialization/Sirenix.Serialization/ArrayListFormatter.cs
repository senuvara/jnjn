using System;
using System.Collections;

namespace Sirenix.Serialization
{
	public class ArrayListFormatter : BaseFormatter<ArrayList>
	{
		private static readonly Serializer<object> ObjectSerializer = Serializer.Get<object>();

		protected override ArrayList GetUninitializedObject()
		{
			return null;
		}

		protected override void DeserializeImplementation(ref ArrayList value, IDataReader reader)
		{
			string name;
			EntryType entryType = reader.PeekEntry(out name);
			if (entryType == EntryType.StartOfArray)
			{
				try
				{
					reader.EnterArray(out long length);
					value = new ArrayList((int)length);
					RegisterReferenceID(value, reader);
					int num = 0;
					while (true)
					{
						if (num >= length)
						{
							return;
						}
						if (reader.PeekEntry(out name) == EntryType.EndOfArray)
						{
							reader.Context.Config.DebugContext.LogError("Reached end of array after " + num + " elements, when " + length + " elements were expected.");
							return;
						}
						value.Add(ObjectSerializer.ReadValue(reader));
						if (!reader.IsInArrayNode)
						{
							break;
						}
						num++;
					}
					reader.Context.Config.DebugContext.LogError("Reading array went wrong at position " + reader.Stream.Position + ".");
				}
				finally
				{
					reader.ExitArray();
				}
			}
			else
			{
				reader.SkipEntry();
			}
		}

		protected override void SerializeImplementation(ref ArrayList value, IDataWriter writer)
		{
			try
			{
				writer.BeginArrayNode(value.Count);
				for (int i = 0; i < value.Count; i++)
				{
					try
					{
						ObjectSerializer.WriteValue(value[i], writer);
					}
					catch (Exception exception)
					{
						writer.Context.Config.DebugContext.LogException(exception);
					}
				}
			}
			finally
			{
				writer.EndArrayNode();
			}
		}
	}
}
