using JetBrains.Annotations;
using System;

namespace Sirenix.Serialization
{
	[MeansImplicitUse]
	[AttributeUsage(AttributeTargets.Property | AttributeTargets.Field)]
	public class OdinSerializeAttribute : Attribute
	{
	}
}
