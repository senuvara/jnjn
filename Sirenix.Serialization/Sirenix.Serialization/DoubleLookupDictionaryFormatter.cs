using Sirenix.Serialization.Utilities;
using System;
using System.Collections.Generic;

namespace Sirenix.Serialization
{
	internal sealed class DoubleLookupDictionaryFormatter<TPrimary, TSecondary, TValue> : BaseFormatter<DoubleLookupDictionary<TPrimary, TSecondary, TValue>>
	{
		private static readonly Serializer<TPrimary> PrimaryReaderWriter;

		private static readonly Serializer<Dictionary<TSecondary, TValue>> InnerReaderWriter;

		static DoubleLookupDictionaryFormatter()
		{
			PrimaryReaderWriter = Serializer.Get<TPrimary>();
			InnerReaderWriter = Serializer.Get<Dictionary<TSecondary, TValue>>();
			new DoubleLookupDictionaryFormatter<int, int, string>();
		}

		protected override DoubleLookupDictionary<TPrimary, TSecondary, TValue> GetUninitializedObject()
		{
			return null;
		}

		protected override void SerializeImplementation(ref DoubleLookupDictionary<TPrimary, TSecondary, TValue> value, IDataWriter writer)
		{
			try
			{
				writer.BeginArrayNode(value.Count);
				bool flag = true;
				foreach (KeyValuePair<TPrimary, Dictionary<TSecondary, TValue>> item in value)
				{
					try
					{
						writer.BeginStructNode(null, null);
						PrimaryReaderWriter.WriteValue(item.Key, writer);
						InnerReaderWriter.WriteValue(item.Value, writer);
					}
					catch (SerializationAbortException ex)
					{
						flag = false;
						throw ex;
					}
					catch (Exception exception)
					{
						writer.Context.Config.DebugContext.LogException(exception);
					}
					finally
					{
						if (flag)
						{
							writer.EndNode(null);
						}
					}
				}
			}
			finally
			{
				writer.EndArrayNode();
			}
		}

		protected override void DeserializeImplementation(ref DoubleLookupDictionary<TPrimary, TSecondary, TValue> value, IDataReader reader)
		{
			string name;
			EntryType entryType = reader.PeekEntry(out name);
			if (entryType == EntryType.StartOfArray)
			{
				try
				{
					reader.EnterArray(out long length);
					value = new DoubleLookupDictionary<TPrimary, TSecondary, TValue>();
					RegisterReferenceID(value, reader);
					int num = 0;
					while (true)
					{
						if (num >= length)
						{
							return;
						}
						if (reader.PeekEntry(out name) == EntryType.EndOfArray)
						{
							reader.Context.Config.DebugContext.LogError("Reached end of array after " + num + " elements, when " + length + " elements were expected.");
							return;
						}
						bool flag = true;
						try
						{
							reader.EnterNode(out Type _);
							TPrimary key = PrimaryReaderWriter.ReadValue(reader);
							Dictionary<TSecondary, TValue> value2 = InnerReaderWriter.ReadValue(reader);
							value.Add(key, value2);
						}
						catch (SerializationAbortException ex)
						{
							flag = false;
							throw ex;
						}
						catch (Exception exception)
						{
							reader.Context.Config.DebugContext.LogException(exception);
						}
						finally
						{
							if (flag)
							{
								reader.ExitNode();
							}
						}
						if (!reader.IsInArrayNode)
						{
							break;
						}
						num++;
					}
					reader.Context.Config.DebugContext.LogError("Reading array went wrong at position " + reader.Stream.Position + ".");
				}
				finally
				{
					reader.ExitArray();
				}
			}
			else
			{
				reader.SkipEntry();
			}
		}
	}
}
