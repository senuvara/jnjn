using System;
using System.Collections.Generic;

namespace Sirenix.Serialization
{
	public class ListFormatter<T> : BaseFormatter<List<T>>
	{
		private static readonly Serializer<T> TSerializer;

		static ListFormatter()
		{
			TSerializer = Serializer.Get<T>();
			new ListFormatter<int>();
		}

		protected override List<T> GetUninitializedObject()
		{
			return null;
		}

		protected override void DeserializeImplementation(ref List<T> value, IDataReader reader)
		{
			string name;
			EntryType entryType = reader.PeekEntry(out name);
			if (entryType == EntryType.StartOfArray)
			{
				try
				{
					reader.EnterArray(out long length);
					value = new List<T>((int)length);
					RegisterReferenceID(value, reader);
					int num = 0;
					while (true)
					{
						if (num >= length)
						{
							return;
						}
						if (reader.PeekEntry(out name) == EntryType.EndOfArray)
						{
							reader.Context.Config.DebugContext.LogError("Reached end of array after " + num + " elements, when " + length + " elements were expected.");
							return;
						}
						value.Add(TSerializer.ReadValue(reader));
						if (!reader.IsInArrayNode)
						{
							break;
						}
						num++;
					}
					reader.Context.Config.DebugContext.LogError("Reading array went wrong at position " + reader.Stream.Position + ".");
				}
				finally
				{
					reader.ExitArray();
				}
			}
			else
			{
				reader.SkipEntry();
			}
		}

		protected override void SerializeImplementation(ref List<T> value, IDataWriter writer)
		{
			try
			{
				writer.BeginArrayNode(value.Count);
				for (int i = 0; i < value.Count; i++)
				{
					try
					{
						TSerializer.WriteValue(value[i], writer);
					}
					catch (Exception exception)
					{
						writer.Context.Config.DebugContext.LogException(exception);
					}
				}
			}
			finally
			{
				writer.EndArrayNode();
			}
		}
	}
}
