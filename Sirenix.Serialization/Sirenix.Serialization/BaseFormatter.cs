using Sirenix.Serialization.Utilities;
using System;
using System.Linq;
using System.Reflection;
using System.Runtime.Serialization;
using UnityEngine;

namespace Sirenix.Serialization
{
	public abstract class BaseFormatter<T> : IFormatter<T>, IFormatter
	{
		protected static readonly Action<T, StreamingContext>[] OnSerializingCallbacks;

		protected static readonly Action<T, StreamingContext>[] OnSerializedCallbacks;

		protected static readonly Action<T, StreamingContext>[] OnDeserializingCallbacks;

		protected static readonly Action<T, StreamingContext>[] OnDeserializedCallbacks;

		protected static readonly bool IsValueType;

		protected static readonly bool ImplementsISerializationCallbackReceiver;

		protected static readonly bool ImplementsIDeserializationCallback;

		protected static readonly bool ImplementsIObjectReference;

		public Type SerializedType => typeof(T);

		static BaseFormatter()
		{
			IsValueType = typeof(T).IsValueType;
			ImplementsISerializationCallbackReceiver = typeof(T).ImplementsOrInherits(typeof(ISerializationCallbackReceiver));
			ImplementsIDeserializationCallback = typeof(T).ImplementsOrInherits(typeof(IDeserializationCallback));
			ImplementsIObjectReference = typeof(T).ImplementsOrInherits(typeof(IObjectReference));
			if (typeof(T).ImplementsOrInherits(typeof(UnityEngine.Object)))
			{
				DefaultLoggers.DefaultLogger.LogWarning("A formatter has been created for the UnityEngine.Object type " + typeof(T).Name + " - this is *strongly* discouraged. Unity should be allowed to handle serialization and deserialization of its own weird objects. Remember to serialize with a UnityReferenceResolver as the external index reference resolver in the serialization context.");
			}
			MethodInfo[] methods = typeof(T).GetMethods(BindingFlags.Instance | BindingFlags.Public | BindingFlags.NonPublic);
			Func<MethodInfo, Action<T, StreamingContext>> selector = delegate(MethodInfo info)
			{
				ParameterInfo[] parameters = info.GetParameters();
				if (parameters.Length == 0)
				{
					Action<T> action = EmitUtilities.CreateInstanceMethodCaller<T>(info);
					return delegate(T value, StreamingContext context)
					{
						action(value);
					};
				}
				if (parameters.Length == 1 && parameters[0].ParameterType == typeof(StreamingContext) && !parameters[0].ParameterType.IsByRef)
				{
					return EmitUtilities.CreateInstanceMethodCaller<T, StreamingContext>(info);
				}
				DefaultLoggers.DefaultLogger.LogWarning("The method " + info.GetNiceName() + " has an invalid signature and will be ignored by the serialization system.");
				return null;
			};
			OnSerializingCallbacks = (from n in methods.Where((MethodInfo n) => n.IsDefined(typeof(OnSerializingAttribute), inherit: true)).Select(selector)
				where n != null
				select n).ToArray();
			OnSerializedCallbacks = (from n in methods.Where((MethodInfo n) => n.IsDefined(typeof(OnSerializedAttribute), inherit: true)).Select(selector)
				where n != null
				select n).ToArray();
			OnDeserializingCallbacks = (from n in methods.Where((MethodInfo n) => n.IsDefined(typeof(OnDeserializingAttribute), inherit: true)).Select(selector)
				where n != null
				select n).ToArray();
			OnDeserializedCallbacks = (from n in methods.Where((MethodInfo n) => n.IsDefined(typeof(OnDeserializedAttribute), inherit: true)).Select(selector)
				where n != null
				select n).ToArray();
		}

		void IFormatter.Serialize(object value, IDataWriter writer)
		{
			Serialize((T)value, writer);
		}

		object IFormatter.Deserialize(IDataReader reader)
		{
			return Deserialize(reader);
		}

		public T Deserialize(IDataReader reader)
		{
			DeserializationContext context = reader.Context;
			T value = GetUninitializedObject();
			if (IsValueType)
			{
				InvokeOnDeserializingCallbacks(value, context);
			}
			else if (value != null)
			{
				RegisterReferenceID(value, reader);
				InvokeOnDeserializingCallbacks(value, context);
				if (ImplementsIObjectReference)
				{
					try
					{
						value = (T)(value as IObjectReference).GetRealObject(context.StreamingContext);
						RegisterReferenceID(value, reader);
					}
					catch (Exception exception)
					{
						context.Config.DebugContext.LogException(exception);
					}
				}
			}
			try
			{
				DeserializeImplementation(ref value, reader);
			}
			catch (Exception exception2)
			{
				context.Config.DebugContext.LogException(exception2);
			}
			if (IsValueType || value != null)
			{
				for (int i = 0; i < OnDeserializedCallbacks.Length; i++)
				{
					try
					{
						OnDeserializedCallbacks[i](value, context.StreamingContext);
					}
					catch (Exception exception3)
					{
						context.Config.DebugContext.LogException(exception3);
					}
				}
				if (ImplementsIDeserializationCallback)
				{
					(value as IDeserializationCallback).OnDeserialization(this);
				}
				if (ImplementsISerializationCallbackReceiver)
				{
					try
					{
						(value as ISerializationCallbackReceiver).OnAfterDeserialize();
						return value;
					}
					catch (Exception exception4)
					{
						context.Config.DebugContext.LogException(exception4);
						return value;
					}
				}
			}
			return value;
		}

		public void Serialize(T value, IDataWriter writer)
		{
			SerializationContext context = writer.Context;
			for (int i = 0; i < OnSerializingCallbacks.Length; i++)
			{
				try
				{
					OnSerializingCallbacks[i](value, context.StreamingContext);
				}
				catch (Exception exception)
				{
					context.Config.DebugContext.LogException(exception);
				}
			}
			if (ImplementsISerializationCallbackReceiver)
			{
				try
				{
					(value as ISerializationCallbackReceiver).OnBeforeSerialize();
				}
				catch (Exception exception2)
				{
					context.Config.DebugContext.LogException(exception2);
				}
			}
			try
			{
				SerializeImplementation(ref value, writer);
			}
			catch (Exception exception3)
			{
				context.Config.DebugContext.LogException(exception3);
			}
			for (int j = 0; j < OnSerializedCallbacks.Length; j++)
			{
				try
				{
					OnSerializedCallbacks[j](value, context.StreamingContext);
				}
				catch (Exception exception4)
				{
					context.Config.DebugContext.LogException(exception4);
				}
			}
		}

		protected virtual T GetUninitializedObject()
		{
			if (IsValueType)
			{
				return default(T);
			}
			return (T)FormatterServices.GetUninitializedObject(typeof(T));
		}

		protected void RegisterReferenceID(T value, IDataReader reader)
		{
			if (!IsValueType)
			{
				int currentNodeId = reader.CurrentNodeId;
				if (currentNodeId < 0)
				{
					reader.Context.Config.DebugContext.LogWarning("Reference type node is missing id upon deserialization. Some references may be broken. This tends to happen if a value type has changed to a reference type (IE, struct to class) since serialization took place.");
				}
				else
				{
					reader.Context.RegisterInternalReference(currentNodeId, value);
				}
			}
		}

		protected void InvokeOnDeserializingCallbacks(T value, DeserializationContext context)
		{
			for (int i = 0; i < OnDeserializingCallbacks.Length; i++)
			{
				try
				{
					OnDeserializingCallbacks[i](value, context.StreamingContext);
				}
				catch (Exception exception)
				{
					context.Config.DebugContext.LogException(exception);
				}
			}
		}

		protected abstract void DeserializeImplementation(ref T value, IDataReader reader);

		protected abstract void SerializeImplementation(ref T value, IDataWriter writer);
	}
}
