using Sirenix.Serialization.Utilities;
using System;

namespace Sirenix.Serialization
{
	public sealed class ComplexTypeSerializer<T> : Serializer<T>
	{
		private static readonly bool ComplexTypeMayBeBoxedValueType = typeof(T).IsInterface || typeof(T) == typeof(object) || typeof(T) == typeof(ValueType) || typeof(T) == typeof(Enum);

		private static readonly bool ComplexTypeIsAbstract = typeof(T).IsAbstract || typeof(T).IsInterface;

		private static readonly bool ComplexTypeIsNullable = typeof(T).IsGenericType && typeof(T).GetGenericTypeDefinition() == typeof(Nullable<>);

		private static readonly bool ComplexTypeIsValueType = typeof(T).IsValueType;

		public override T ReadValue(IDataReader reader)
		{
			DeserializationContext context = reader.Context;
			if (!context.Config.SerializationPolicy.AllowNonSerializableTypes && !typeof(T).IsSerializable)
			{
				context.Config.DebugContext.LogError("The type " + typeof(T).Name + " is not marked as serializable.");
				return default(T);
			}
			bool flag = true;
			string name;
			EntryType entryType = reader.PeekEntry(out name);
			if (ComplexTypeIsValueType)
			{
				switch (entryType)
				{
				case EntryType.Null:
					context.Config.DebugContext.LogWarning("Expecting complex struct of type " + typeof(T).GetNiceFullName() + " but got null value.");
					reader.ReadNull();
					return default(T);
				default:
					context.Config.DebugContext.LogWarning(string.Concat("Unexpected entry '", name, "' of type ", entryType.ToString(), ", when ", EntryType.StartOfNode, " was expected. A value has likely been lost."));
					reader.SkipEntry();
					return default(T);
				case EntryType.StartOfNode:
					try
					{
						Type typeFromHandle = typeof(T);
						if (reader.EnterNode(out Type type))
						{
							if (type != typeFromHandle)
							{
								if (type != null)
								{
									context.Config.DebugContext.LogWarning("Expected complex struct value " + typeFromHandle.Name + " but the serialized value is of type " + type.Name + ".");
									if (type.IsCastableTo(typeFromHandle))
									{
										object obj = FormatterLocator.GetFormatter(type, context.Config.SerializationPolicy).Deserialize(reader);
										bool flag2 = type.IsGenericType && type.GetGenericTypeDefinition() == typeof(Nullable<>);
										Func<object, object> func = (!ComplexTypeIsNullable && !flag2) ? type.GetCastMethodDelegate(typeFromHandle) : null;
										if (func != null)
										{
											return (T)func(obj);
										}
										return (T)obj;
									}
									context.Config.DebugContext.LogWarning("Can't cast serialized type " + type.Name + " into expected type " + typeFromHandle.Name + ". Value lost for node '" + name + "'.");
									return default(T);
								}
								context.Config.DebugContext.LogWarning("Expected complex struct value " + typeFromHandle.Name + " but the serialized type could not be resolved.");
								return default(T);
							}
							return FormatterLocator.GetFormatter<T>(context.Config.SerializationPolicy).Deserialize(reader);
						}
						context.Config.DebugContext.LogError("Failed to enter node '" + name + "'.");
						return default(T);
					}
					catch (SerializationAbortException ex)
					{
						flag = false;
						throw ex;
					}
					catch (Exception exception)
					{
						context.Config.DebugContext.LogException(exception);
						return default(T);
					}
					finally
					{
						if (flag)
						{
							reader.ExitNode();
						}
					}
				}
			}
			switch (entryType)
			{
			case EntryType.Null:
				reader.ReadNull();
				return default(T);
			case EntryType.ExternalReferenceByIndex:
			{
				reader.ReadExternalReference(out int index);
				object externalObject3 = context.GetExternalObject(index);
				try
				{
					return (T)externalObject3;
				}
				catch (InvalidCastException)
				{
					context.Config.DebugContext.LogWarning("Can't cast external reference type " + externalObject3.GetType().Name + " into expected type " + typeof(T).Name + ". Value lost for node '" + name + "'.");
					return default(T);
				}
			}
			case EntryType.ExternalReferenceByGuid:
			{
				reader.ReadExternalReference(out Guid guid);
				object externalObject2 = context.GetExternalObject(guid);
				try
				{
					return (T)externalObject2;
				}
				catch (InvalidCastException)
				{
					context.Config.DebugContext.LogWarning("Can't cast external reference type " + externalObject2.GetType().Name + " into expected type " + typeof(T).Name + ". Value lost for node '" + name + "'.");
					return default(T);
				}
			}
			case EntryType.ExternalReferenceByString:
			{
				reader.ReadExternalReference(out string id2);
				object externalObject = context.GetExternalObject(id2);
				try
				{
					return (T)externalObject;
				}
				catch (InvalidCastException)
				{
					context.Config.DebugContext.LogWarning("Can't cast external reference type " + externalObject.GetType().Name + " into expected type " + typeof(T).Name + ". Value lost for node '" + name + "'.");
					return default(T);
				}
			}
			case EntryType.InternalReference:
			{
				reader.ReadInternalReference(out int id);
				object internalReference = context.GetInternalReference(id);
				try
				{
					return (T)internalReference;
				}
				catch (InvalidCastException)
				{
					context.Config.DebugContext.LogWarning("Can't cast internal reference type " + internalReference.GetType().Name + " into expected type " + typeof(T).Name + ". Value lost for node '" + name + "'.");
					return default(T);
				}
			}
			case EntryType.StartOfNode:
				try
				{
					Type typeFromHandle2 = typeof(T);
					if (reader.EnterNode(out Type type2))
					{
						int currentNodeId = reader.CurrentNodeId;
						T val;
						if (type2 == null || typeFromHandle2 == type2)
						{
							val = ((!ComplexTypeIsAbstract) ? FormatterLocator.GetFormatter<T>(context.Config.SerializationPolicy).Deserialize(reader) : default(T));
						}
						else
						{
							bool flag3 = false;
							bool flag4 = FormatterUtilities.IsPrimitiveType(type2);
							bool flag5;
							if (ComplexTypeMayBeBoxedValueType && flag4)
							{
								Serializer serializer = Serializer.Get(type2);
								val = (T)serializer.ReadValueWeak(reader);
								flag3 = true;
							}
							else if ((flag5 = typeFromHandle2.IsAssignableFrom(type2)) || type2.HasCastDefined(typeFromHandle2, requireImplicitCast: false))
							{
								try
								{
									object obj2;
									if (flag4)
									{
										Serializer serializer2 = Serializer.Get(type2);
										obj2 = serializer2.ReadValueWeak(reader);
									}
									else
									{
										IFormatter formatter = FormatterLocator.GetFormatter(type2, context.Config.SerializationPolicy);
										obj2 = formatter.Deserialize(reader);
									}
									if (flag5)
									{
										val = (T)obj2;
									}
									else
									{
										Func<object, object> castMethodDelegate = type2.GetCastMethodDelegate(typeFromHandle2);
										val = ((castMethodDelegate == null) ? ((T)obj2) : ((T)castMethodDelegate(obj2)));
									}
									flag3 = true;
								}
								catch (SerializationAbortException ex6)
								{
									flag = false;
									throw ex6;
								}
								catch (InvalidCastException)
								{
									flag3 = false;
									val = default(T);
								}
							}
							else
							{
								IFormatter formatter2 = FormatterLocator.GetFormatter(type2, context.Config.SerializationPolicy);
								object reference = formatter2.Deserialize(reader);
								if (currentNodeId >= 0)
								{
									context.RegisterInternalReference(currentNodeId, reference);
								}
								val = default(T);
							}
							if (!flag3)
							{
								context.Config.DebugContext.LogWarning("Can't cast serialized type " + type2.Name + " into expected type " + typeFromHandle2.Name + ". Value lost for node '" + name + "'.");
								val = default(T);
							}
						}
						if (currentNodeId >= 0)
						{
							context.RegisterInternalReference(currentNodeId, val);
						}
						return val;
					}
					context.Config.DebugContext.LogError("Failed to enter node '" + name + "'.");
					return default(T);
				}
				catch (SerializationAbortException ex8)
				{
					flag = false;
					throw ex8;
				}
				catch (Exception exception2)
				{
					context.Config.DebugContext.LogException(exception2);
					return default(T);
				}
				finally
				{
					if (flag)
					{
						reader.ExitNode();
					}
				}
			case EntryType.Boolean:
				if (ComplexTypeMayBeBoxedValueType)
				{
					reader.ReadBoolean(out bool value5);
					return (T)(object)value5;
				}
				break;
			case EntryType.FloatingPoint:
				if (ComplexTypeMayBeBoxedValueType)
				{
					reader.ReadDouble(out double value4);
					return (T)(object)value4;
				}
				break;
			case EntryType.Integer:
				if (ComplexTypeMayBeBoxedValueType)
				{
					reader.ReadInt64(out long value3);
					return (T)(object)value3;
				}
				break;
			case EntryType.String:
				if (ComplexTypeMayBeBoxedValueType)
				{
					reader.ReadString(out string value2);
					return (T)(object)value2;
				}
				break;
			case EntryType.Guid:
				if (ComplexTypeMayBeBoxedValueType)
				{
					reader.ReadGuid(out Guid value);
					return (T)(object)value;
				}
				break;
			}
			context.Config.DebugContext.LogWarning("Unexpected entry of type " + entryType.ToString() + ", when a reference or node start was expected. A value has been lost.");
			reader.SkipEntry();
			return default(T);
		}

		public override void WriteValue(string name, T value, IDataWriter writer)
		{
			SerializationContext context = writer.Context;
			if (!context.Config.SerializationPolicy.AllowNonSerializableTypes && !typeof(T).IsSerializable)
			{
				context.Config.DebugContext.LogError("The type " + typeof(T).Name + " is not marked as serializable.");
				return;
			}
			if (ComplexTypeIsValueType)
			{
				bool flag = true;
				try
				{
					writer.BeginStructNode(name, typeof(T));
					FormatterLocator.GetFormatter<T>(context.Config.SerializationPolicy).Serialize(value, writer);
				}
				catch (SerializationAbortException ex)
				{
					flag = false;
					throw ex;
				}
				finally
				{
					if (flag)
					{
						writer.EndNode(name);
					}
				}
				return;
			}
			bool flag2 = true;
			int index;
			Guid guid;
			string id;
			int id2;
			if (value == null)
			{
				writer.WriteNull(name);
			}
			else if (context.TryRegisterExternalReference(value, out index))
			{
				writer.WriteExternalReference(name, index);
			}
			else if (context.TryRegisterExternalReference(value, out guid))
			{
				writer.WriteExternalReference(name, guid);
			}
			else if (context.TryRegisterExternalReference(value, out id))
			{
				writer.WriteExternalReference(name, id);
			}
			else if (context.TryRegisterInternalReference(value, out id2))
			{
				Type type = value.GetType();
				if (ComplexTypeMayBeBoxedValueType && FormatterUtilities.IsPrimitiveType(type))
				{
					try
					{
						writer.BeginReferenceNode(name, type, id2);
						Serializer serializer = Serializer.Get(type);
						serializer.WriteValueWeak(value, writer);
					}
					catch (SerializationAbortException ex2)
					{
						flag2 = false;
						throw ex2;
					}
					finally
					{
						if (flag2)
						{
							writer.EndNode(name);
						}
					}
					return;
				}
				IFormatter formatter = FormatterLocator.GetFormatter(type, context.Config.SerializationPolicy);
				try
				{
					writer.BeginReferenceNode(name, type, id2);
					formatter.Serialize(value, writer);
				}
				catch (SerializationAbortException ex3)
				{
					flag2 = false;
					throw ex3;
				}
				finally
				{
					if (flag2)
					{
						writer.EndNode(name);
					}
				}
			}
			else
			{
				writer.WriteInternalReference(name, id2);
			}
		}
	}
}
