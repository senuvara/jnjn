using System;
using System.Collections.Generic;
using System.Globalization;
using System.IO;
using System.Linq;
using System.Reflection;
using System.Threading;
using UnityEngine;

namespace Sirenix.Serialization.Utilities
{
	internal static class AssemblyUtilities
	{
		private static string[] userAssemblyPrefixes;

		private static string[] pluginAssemblyPrefixes;

		private static readonly object IS_DYNAMIC_CACHE_LOCK;

		private static readonly Dictionary<Assembly, bool> IsDynamicCache;

		private static readonly object ASSEMBLY_LOAD_QUEUE_LOCK;

		private static readonly Queue<Assembly> AssemblyLoadQueue;

		private static readonly object MAIN_LOCK;

		private static Assembly unityEngineAssembly;

		private static DirectoryInfo projectFolderDirectory;

		private static DirectoryInfo scriptAssembliesDirectory;

		private static Dictionary<string, Type> stringTypeLookup;

		private static Dictionary<Assembly, AssemblyTypeFlags> assemblyTypeFlagLookup;

		private static List<Assembly> allAssemblies;

		private static ImmutableList<Assembly> allAssembliesImmutable;

		private static List<Assembly> userAssemblies;

		private static List<Assembly> userEditorAssemblies;

		private static List<Assembly> pluginAssemblies;

		private static List<Assembly> pluginEditorAssemblies;

		private static List<Assembly> unityAssemblies;

		private static List<Assembly> unityEditorAssemblies;

		private static List<Assembly> otherAssemblies;

		private static List<Type> userTypes;

		private static List<Type> userEditorTypes;

		private static List<Type> pluginTypes;

		private static List<Type> pluginEditorTypes;

		private static List<Type> unityTypes;

		private static List<Type> unityEditorTypes;

		private static List<Type> otherTypes;

		private static string dataPath;

		private static string scriptAssembliesPath;

		private static volatile bool initialized;

		private static volatile bool initializing;

		static AssemblyUtilities()
		{
			userAssemblyPrefixes = new string[6]
			{
				"Assembly-CSharp",
				"Assembly-UnityScript",
				"Assembly-Boo",
				"Assembly-CSharp-Editor",
				"Assembly-UnityScript-Editor",
				"Assembly-Boo-Editor"
			};
			pluginAssemblyPrefixes = new string[6]
			{
				"Assembly-CSharp-firstpass",
				"Assembly-CSharp-Editor-firstpass",
				"Assembly-UnityScript-firstpass",
				"Assembly-UnityScript-Editor-firstpass",
				"Assembly-Boo-firstpass",
				"Assembly-Boo-Editor-firstpass"
			};
			IS_DYNAMIC_CACHE_LOCK = new object();
			IsDynamicCache = new Dictionary<Assembly, bool>();
			ASSEMBLY_LOAD_QUEUE_LOCK = new object();
			AssemblyLoadQueue = new Queue<Assembly>();
			MAIN_LOCK = new object();
			AppDomain.CurrentDomain.AssemblyLoad += delegate(object sender, AssemblyLoadEventArgs args)
			{
				if (!args.LoadedAssembly.IsDynamic() && !args.LoadedAssembly.ReflectionOnly)
				{
					RegisterAssembly_THREADSAFE(args.LoadedAssembly, mightNotBeInitialized: true);
				}
			};
			try
			{
				new Thread(EnsureInitialized).Start();
			}
			catch (Exception)
			{
			}
		}

		[RuntimeInitializeOnLoadMethod(RuntimeInitializeLoadType.BeforeSceneLoad)]
		private static void DoNothing()
		{
		}

		private static void EnsureInitialized()
		{
			if (initializing)
			{
				return;
			}
			if (initialized)
			{
				Assembly[] queue;
				while (GetAndClearQueuedAssemblies(out queue))
				{
					for (int i = 0; i < queue.Length; i++)
					{
						RegisterAssembly_THREADSAFE(queue[i], mightNotBeInitialized: false);
					}
				}
			}
			else
			{
				lock (MAIN_LOCK)
				{
					if (!initialized && !initializing)
					{
						initializing = true;
						Reload();
						initialized = true;
						initializing = false;
					}
				}
			}
		}

		public static void Reload()
		{
			lock (MAIN_LOCK)
			{
				dataPath = Environment.CurrentDirectory.Replace("\\", "//").Replace("//", "/").TrimEnd('/') + "/Assets";
				scriptAssembliesPath = Environment.CurrentDirectory.Replace("\\", "//").Replace("//", "/").TrimEnd('/') + "/Library/ScriptAssemblies";
				userAssemblies = new List<Assembly>();
				userEditorAssemblies = new List<Assembly>();
				pluginAssemblies = new List<Assembly>();
				pluginEditorAssemblies = new List<Assembly>();
				unityAssemblies = new List<Assembly>();
				unityEditorAssemblies = new List<Assembly>();
				otherAssemblies = new List<Assembly>();
				userTypes = new List<Type>();
				userEditorTypes = new List<Type>();
				pluginTypes = new List<Type>();
				pluginEditorTypes = new List<Type>();
				unityTypes = new List<Type>();
				unityEditorTypes = new List<Type>();
				otherTypes = new List<Type>();
				stringTypeLookup = new Dictionary<string, Type>();
				assemblyTypeFlagLookup = new Dictionary<Assembly, AssemblyTypeFlags>();
				unityEngineAssembly = typeof(Vector3).Assembly;
				projectFolderDirectory = new DirectoryInfo(dataPath);
				scriptAssembliesDirectory = new DirectoryInfo(scriptAssembliesPath);
				allAssemblies = new List<Assembly>();
				allAssembliesImmutable = new ImmutableList<Assembly>(allAssemblies);
				Assembly[] assemblies = AppDomain.CurrentDomain.GetAssemblies();
				for (int i = 0; i < assemblies.Length; i++)
				{
					RegisterAssembly_THREADSAFE(assemblies[i], mightNotBeInitialized: false);
				}
			}
		}

		private static void RegisterAssembly_THREADSAFE(Assembly assembly, bool mightNotBeInitialized)
		{
			if (mightNotBeInitialized)
			{
				EnsureInitialized();
			}
			if (Monitor.TryEnter(MAIN_LOCK))
			{
				try
				{
					RegisterAssembly_NOT_THREADSAFE(assembly);
					Assembly[] queue;
					while (GetAndClearQueuedAssemblies(out queue))
					{
						for (int i = 0; i < queue.Length; i++)
						{
							RegisterAssembly_NOT_THREADSAFE(queue[i]);
						}
					}
				}
				catch (ReflectionTypeLoadException)
				{
				}
				finally
				{
					Monitor.Exit(MAIN_LOCK);
				}
			}
			else
			{
				lock (ASSEMBLY_LOAD_QUEUE_LOCK)
				{
					AssemblyLoadQueue.Enqueue(assembly);
				}
			}
		}

		private static void RegisterAssembly_NOT_THREADSAFE(Assembly assembly)
		{
			if (!allAssemblies.Contains(assembly))
			{
				allAssemblies.Add(assembly);
				AssemblyTypeFlags assemblyTypeFlag = assembly.GetAssemblyTypeFlag();
				Type[] array = assembly.SafeGetTypes();
				foreach (Type type in array)
				{
					stringTypeLookup[type.FullName] = type;
				}
				switch (assemblyTypeFlag)
				{
				case AssemblyTypeFlags.UserTypes:
					userAssemblies.Add(assembly);
					userTypes.AddRange(assembly.SafeGetTypes());
					break;
				case AssemblyTypeFlags.UserEditorTypes:
					userEditorAssemblies.Add(assembly);
					userEditorTypes.AddRange(assembly.SafeGetTypes());
					break;
				case AssemblyTypeFlags.PluginTypes:
					pluginAssemblies.Add(assembly);
					pluginTypes.AddRange(assembly.SafeGetTypes());
					break;
				case AssemblyTypeFlags.PluginEditorTypes:
					pluginEditorAssemblies.Add(assembly);
					pluginEditorTypes.AddRange(assembly.SafeGetTypes());
					break;
				case AssemblyTypeFlags.UnityTypes:
					unityAssemblies.Add(assembly);
					unityTypes.AddRange(assembly.SafeGetTypes());
					break;
				case AssemblyTypeFlags.UnityEditorTypes:
					unityEditorAssemblies.Add(assembly);
					unityEditorTypes.AddRange(assembly.SafeGetTypes());
					break;
				case AssemblyTypeFlags.OtherTypes:
					otherAssemblies.Add(assembly);
					otherTypes.AddRange(assembly.SafeGetTypes());
					break;
				default:
					throw new NotImplementedException();
				}
			}
		}

		private static bool GetAndClearQueuedAssemblies(out Assembly[] queue)
		{
			lock (ASSEMBLY_LOAD_QUEUE_LOCK)
			{
				if (AssemblyLoadQueue.Count == 0)
				{
					queue = null;
					return false;
				}
				queue = AssemblyLoadQueue.ToArray();
				AssemblyLoadQueue.Clear();
				return true;
			}
		}

		public static ImmutableList<Assembly> GetAllAssemblies()
		{
			EnsureInitialized();
			lock (MAIN_LOCK)
			{
				Assembly[] innerList = allAssembliesImmutable.ToArray();
				return new ImmutableList<Assembly>(innerList);
			}
		}

		public static AssemblyTypeFlags GetAssemblyTypeFlag(this Assembly assembly)
		{
			if (assembly == null)
			{
				throw new NullReferenceException("assembly");
			}
			EnsureInitialized();
			lock (MAIN_LOCK)
			{
				if (!assemblyTypeFlagLookup.TryGetValue(assembly, out AssemblyTypeFlags value))
				{
					value = GetAssemblyTypeFlagNoLookup(assembly);
					assemblyTypeFlagLookup[assembly] = value;
				}
				return value;
			}
		}

		private static AssemblyTypeFlags GetAssemblyTypeFlagNoLookup(Assembly assembly)
		{
			string assemblyDirectory = assembly.GetAssemblyDirectory();
			string str = assembly.FullName.ToLower(CultureInfo.InvariantCulture);
			bool flag = false;
			bool flag2 = false;
			if (assemblyDirectory != null && Directory.Exists(assemblyDirectory))
			{
				DirectoryInfo directoryInfo = new DirectoryInfo(assemblyDirectory);
				flag = (directoryInfo.FullName == scriptAssembliesDirectory.FullName);
				flag2 = projectFolderDirectory.HasSubDirectory(directoryInfo);
			}
			bool flag3 = str.StartsWithAnyOf(userAssemblyPrefixes, StringComparison.InvariantCultureIgnoreCase);
			bool flag4 = str.StartsWithAnyOf(pluginAssemblyPrefixes, StringComparison.InvariantCultureIgnoreCase);
			bool flag5 = assembly.IsDependentOn(unityEngineAssembly);
			bool flag6 = flag4 || flag2 || (!flag3 && flag);
			bool flag7 = !flag6 && flag3;
			bool flag8 = false;
			if (!flag5 && !flag8 && !flag6 && !flag7)
			{
				return AssemblyTypeFlags.OtherTypes;
			}
			if (flag8 && !flag6 && !flag7)
			{
				return AssemblyTypeFlags.UnityEditorTypes;
			}
			if (flag5 && !flag8 && !flag6 && !flag7)
			{
				return AssemblyTypeFlags.UnityTypes;
			}
			if (flag8 && flag6 && !flag7)
			{
				return AssemblyTypeFlags.PluginEditorTypes;
			}
			if (!flag8 && flag6 && !flag7)
			{
				return AssemblyTypeFlags.PluginTypes;
			}
			if (flag8 && flag7)
			{
				return AssemblyTypeFlags.UserEditorTypes;
			}
			if (!flag8 && flag7)
			{
				return AssemblyTypeFlags.UserTypes;
			}
			return AssemblyTypeFlags.OtherTypes;
		}

		public static Type GetTypeByCachedFullName(string fullName)
		{
			if (string.IsNullOrEmpty(fullName))
			{
				return null;
			}
			EnsureInitialized();
			lock (MAIN_LOCK)
			{
				if (stringTypeLookup.TryGetValue(fullName, out Type value))
				{
					return value;
				}
				return null;
			}
		}

		[Obsolete("This method was renamed. Use GetTypeByCachedFullName instead.")]
		public static Type GetType(string fullName)
		{
			if (string.IsNullOrEmpty(fullName))
			{
				return null;
			}
			EnsureInitialized();
			lock (MAIN_LOCK)
			{
				if (stringTypeLookup.TryGetValue(fullName, out Type value))
				{
					return value;
				}
				return null;
			}
		}

		public static bool IsDependentOn(this Assembly assembly, Assembly otherAssembly)
		{
			if (assembly == null)
			{
				throw new NullReferenceException("assembly");
			}
			if (otherAssembly == null)
			{
				throw new NullReferenceException("otherAssembly");
			}
			if (assembly == otherAssembly)
			{
				return true;
			}
			string a = otherAssembly.GetName().ToString();
			EnsureInitialized();
			lock (MAIN_LOCK)
			{
				AssemblyName[] referencedAssemblies = assembly.GetReferencedAssemblies();
				for (int i = 0; i < referencedAssemblies.Length; i++)
				{
					if (a == referencedAssemblies[i].ToString())
					{
						return true;
					}
				}
				return false;
			}
		}

		public static bool IsDynamic(this Assembly assembly)
		{
			if (assembly == null)
			{
				throw new ArgumentNullException("assembly");
			}
			lock (IS_DYNAMIC_CACHE_LOCK)
			{
				if (IsDynamicCache.TryGetValue(assembly, out bool value))
				{
					return value;
				}
				try
				{
					value = (assembly.GetType().FullName.EndsWith("AssemblyBuilder") || assembly.Location == null || assembly.Location == "");
				}
				catch
				{
					value = true;
				}
				IsDynamicCache.Add(assembly, value);
				return value;
			}
		}

		public static string GetAssemblyDirectory(this Assembly assembly)
		{
			if (assembly == null)
			{
				throw new ArgumentNullException("assembly");
			}
			string assemblyFilePath = assembly.GetAssemblyFilePath();
			if (assemblyFilePath == null)
			{
				return null;
			}
			try
			{
				return Path.GetDirectoryName(assemblyFilePath);
			}
			catch
			{
				return null;
			}
		}

		public static string GetAssemblyFilePath(this Assembly assembly)
		{
			if (assembly == null)
			{
				return null;
			}
			if (assembly.IsDynamic())
			{
				return null;
			}
			if (assembly.CodeBase == null)
			{
				return null;
			}
			string text = "file:///";
			string codeBase = assembly.CodeBase;
			if (codeBase.StartsWith(text, StringComparison.InvariantCultureIgnoreCase))
			{
				codeBase = codeBase.Substring(text.Length);
				codeBase = codeBase.Replace('\\', '/');
				if (File.Exists(codeBase))
				{
					return codeBase;
				}
				if (!Path.IsPathRooted(codeBase))
				{
					codeBase = ((!File.Exists("/" + codeBase)) ? Path.GetFullPath(codeBase) : ("/" + codeBase));
				}
				if (File.Exists(codeBase))
				{
					return codeBase;
				}
				try
				{
					codeBase = assembly.Location;
				}
				catch
				{
					return null;
				}
				if (File.Exists(codeBase))
				{
					return codeBase;
				}
			}
			if (File.Exists(assembly.Location))
			{
				return assembly.Location;
			}
			return null;
		}

		public static IEnumerable<Type> GetTypes(AssemblyTypeFlags assemblyTypeFlags)
		{
			EnsureInitialized();
			lock (MAIN_LOCK)
			{
				bool flag = (assemblyTypeFlags & AssemblyTypeFlags.UserTypes) == AssemblyTypeFlags.UserTypes;
				bool includeUserEditorTypes = (assemblyTypeFlags & AssemblyTypeFlags.UserEditorTypes) == AssemblyTypeFlags.UserEditorTypes;
				bool includePluginTypes = (assemblyTypeFlags & AssemblyTypeFlags.PluginTypes) == AssemblyTypeFlags.PluginTypes;
				bool includePluginEditorTypes = (assemblyTypeFlags & AssemblyTypeFlags.PluginEditorTypes) == AssemblyTypeFlags.PluginEditorTypes;
				bool includeUnityTypes = (assemblyTypeFlags & AssemblyTypeFlags.UnityTypes) == AssemblyTypeFlags.UnityTypes;
				bool includeUnityEditorTypes = (assemblyTypeFlags & AssemblyTypeFlags.UnityEditorTypes) == AssemblyTypeFlags.UnityEditorTypes;
				bool includeOtherTypes = (assemblyTypeFlags & AssemblyTypeFlags.OtherTypes) == AssemblyTypeFlags.OtherTypes;
				if (flag)
				{
					for (int i2 = 0; i2 < userTypes.Count; i2++)
					{
						yield return userTypes[i2];
					}
				}
				if (includeUserEditorTypes)
				{
					for (int n = 0; n < userEditorTypes.Count; n++)
					{
						yield return userEditorTypes[n];
					}
				}
				if (includePluginTypes)
				{
					for (int m = 0; m < pluginTypes.Count; m++)
					{
						yield return pluginTypes[m];
					}
				}
				if (includePluginEditorTypes)
				{
					for (int l = 0; l < pluginEditorTypes.Count; l++)
					{
						yield return pluginEditorTypes[l];
					}
				}
				if (includeUnityTypes)
				{
					for (int k = 0; k < unityTypes.Count; k++)
					{
						yield return unityTypes[k];
					}
				}
				if (includeUnityEditorTypes)
				{
					for (int j = 0; j < unityEditorTypes.Count; j++)
					{
						yield return unityEditorTypes[j];
					}
				}
				if (includeOtherTypes)
				{
					for (int i = 0; i < otherTypes.Count; i++)
					{
						yield return otherTypes[i];
					}
				}
			}
		}

		public static Type[] SafeGetTypes(this Assembly assembly)
		{
			try
			{
				return assembly.GetTypes();
			}
			catch
			{
				return Type.EmptyTypes;
			}
		}

		private static bool StartsWithAnyOf(this string str, IEnumerable<string> values, StringComparison comparisonType = StringComparison.CurrentCulture)
		{
			IList<string> list = values as IList<string>;
			if (list != null)
			{
				int count = list.Count;
				for (int i = 0; i < count; i++)
				{
					if (str.StartsWith(list[i], comparisonType))
					{
						return true;
					}
				}
			}
			else
			{
				foreach (string value in values)
				{
					if (str.StartsWith(value, comparisonType))
					{
						return true;
					}
				}
			}
			return false;
		}
	}
}
