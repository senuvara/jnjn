namespace Sirenix.Serialization.Utilities
{
	internal interface ICacheNotificationReceiver
	{
		void OnFreed();

		void OnClaimed();
	}
}
