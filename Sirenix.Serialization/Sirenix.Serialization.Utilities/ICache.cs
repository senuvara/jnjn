using System;

namespace Sirenix.Serialization.Utilities
{
	internal interface ICache<T> : IDisposable where T : class, new()
	{
		T Value
		{
			get;
		}

		bool IsFree
		{
			get;
		}

		void Release();
	}
}
