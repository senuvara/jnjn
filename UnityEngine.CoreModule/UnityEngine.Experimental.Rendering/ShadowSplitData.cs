using System;
using UnityEngine.Scripting;

namespace UnityEngine.Experimental.Rendering
{
	/// <summary>
	///   <para>Describes the culling information for a given shadow split (e.g. directional cascade).</para>
	/// </summary>
	[UsedByNativeCode]
	public struct ShadowSplitData
	{
		/// <summary>
		///   <para>The number of culling planes.</para>
		/// </summary>
		public int cullingPlaneCount;

		private _003C_cullingPlanes_003E__FixedBuffer6 _cullingPlanes;

		/// <summary>
		///   <para>The culling sphere.  The first three components of the vector describe the sphere center, and the last component specifies the radius.</para>
		/// </summary>
		public Vector4 cullingSphere;

		/// <summary>
		///   <para>Gets a culling plane.</para>
		/// </summary>
		/// <param name="index">The culling plane index.</param>
		/// <returns>
		///   <para>The culling plane.</para>
		/// </returns>
		public unsafe Plane GetCullingPlane(int index)
		{
			if (index < 0 || index >= cullingPlaneCount || index >= 10)
			{
				throw new IndexOutOfRangeException("Invalid plane index");
			}
			fixed (float* ptr = &_cullingPlanes.FixedElementField)
			{
				return new Plane(new Vector3(*(float*)((long)ptr + (long)(IntPtr)(void*)((long)(index * 4) * 4L)), *(float*)((long)ptr + (long)(IntPtr)(void*)((long)(index * 4 + 1) * 4L)), *(float*)((long)ptr + (long)(IntPtr)(void*)((long)(index * 4 + 2) * 4L))), *(float*)((long)ptr + (long)(IntPtr)(void*)((long)(index * 4 + 3) * 4L)));
			}
		}

		/// <summary>
		///   <para>Sets a culling plane.</para>
		/// </summary>
		/// <param name="index">The index of the culling plane to set.</param>
		/// <param name="plane">The culling plane.</param>
		public unsafe void SetCullingPlane(int index, Plane plane)
		{
			if (index < 0 || index >= cullingPlaneCount || index >= 10)
			{
				throw new IndexOutOfRangeException("Invalid plane index");
			}
			fixed (float* ptr = &_cullingPlanes.FixedElementField)
			{
				_003F val = (long)ptr + (long)(IntPtr)(void*)((long)(index * 4) * 4L);
				Vector3 normal = plane.normal;
				*(float*)val = normal.x;
				_003F val2 = (long)ptr + (long)(IntPtr)(void*)((long)(index * 4 + 1) * 4L);
				Vector3 normal2 = plane.normal;
				*(float*)val2 = normal2.y;
				_003F val3 = (long)ptr + (long)(IntPtr)(void*)((long)(index * 4 + 2) * 4L);
				Vector3 normal3 = plane.normal;
				*(float*)val3 = normal3.z;
				*(float*)((long)ptr + (long)(IntPtr)(void*)((long)(index * 4 + 3) * 4L)) = plane.distance;
			}
		}
	}
}
