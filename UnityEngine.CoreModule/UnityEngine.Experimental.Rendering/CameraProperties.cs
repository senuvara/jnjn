using System;
using UnityEngine.Scripting;

namespace UnityEngine.Experimental.Rendering
{
	/// <summary>
	///   <para>Camera related properties in CullingParameters.</para>
	/// </summary>
	[UsedByNativeCode]
	public struct CameraProperties
	{
		private const int kNumLayers = 32;

		private Rect screenRect;

		private Vector3 viewDir;

		private float projectionNear;

		private float projectionFar;

		private float cameraNear;

		private float cameraFar;

		private float cameraAspect;

		private Matrix4x4 cameraToWorld;

		private Matrix4x4 actualWorldToClip;

		private Matrix4x4 cameraClipToWorld;

		private Matrix4x4 cameraWorldToClip;

		private Matrix4x4 implicitProjection;

		private Matrix4x4 stereoWorldToClipLeft;

		private Matrix4x4 stereoWorldToClipRight;

		private Matrix4x4 worldToCamera;

		private Vector3 up;

		private Vector3 right;

		private Vector3 transformDirection;

		private Vector3 cameraEuler;

		private Vector3 velocity;

		private float farPlaneWorldSpaceLength;

		private uint rendererCount;

		private _003C_shadowCullPlanes_003E__FixedBuffer1 _shadowCullPlanes;

		private _003C_cameraCullPlanes_003E__FixedBuffer2 _cameraCullPlanes;

		private float baseFarDistance;

		private Vector3 shadowCullCenter;

		private _003ClayerCullDistances_003E__FixedBuffer3 layerCullDistances;

		private int layerCullSpherical;

		private CoreCameraValues coreCameraValues;

		private uint cameraType;

		/// <summary>
		///   <para>Get a shadow culling plane.</para>
		/// </summary>
		/// <param name="index">Plane index (up to 5).</param>
		/// <returns>
		///   <para>Shadow culling plane.</para>
		/// </returns>
		public unsafe Plane GetShadowCullingPlane(int index)
		{
			if (index < 0 || index >= 6)
			{
				throw new IndexOutOfRangeException("Invalid plane index");
			}
			fixed (float* ptr = &_shadowCullPlanes.FixedElementField)
			{
				return new Plane(new Vector3(*(float*)((long)ptr + (long)(IntPtr)(void*)((long)(index * 4) * 4L)), *(float*)((long)ptr + (long)(IntPtr)(void*)((long)(index * 4 + 1) * 4L)), *(float*)((long)ptr + (long)(IntPtr)(void*)((long)(index * 4 + 2) * 4L))), *(float*)((long)ptr + (long)(IntPtr)(void*)((long)(index * 4 + 3) * 4L)));
			}
		}

		/// <summary>
		///   <para>Set a shadow culling plane.</para>
		/// </summary>
		/// <param name="index">Plane index (up to 5).</param>
		/// <param name="plane">Shadow culling plane.</param>
		public unsafe void SetShadowCullingPlane(int index, Plane plane)
		{
			if (index < 0 || index >= 6)
			{
				throw new IndexOutOfRangeException("Invalid plane index");
			}
			fixed (float* ptr = &_shadowCullPlanes.FixedElementField)
			{
				_003F val = (long)ptr + (long)(IntPtr)(void*)((long)(index * 4) * 4L);
				Vector3 normal = plane.normal;
				*(float*)val = normal.x;
				_003F val2 = (long)ptr + (long)(IntPtr)(void*)((long)(index * 4 + 1) * 4L);
				Vector3 normal2 = plane.normal;
				*(float*)val2 = normal2.y;
				_003F val3 = (long)ptr + (long)(IntPtr)(void*)((long)(index * 4 + 2) * 4L);
				Vector3 normal3 = plane.normal;
				*(float*)val3 = normal3.z;
				*(float*)((long)ptr + (long)(IntPtr)(void*)((long)(index * 4 + 3) * 4L)) = plane.distance;
			}
		}

		/// <summary>
		///   <para>Get a camera culling plane.</para>
		/// </summary>
		/// <param name="index">Plane index (up to 5).</param>
		/// <returns>
		///   <para>Camera culling plane.</para>
		/// </returns>
		public unsafe Plane GetCameraCullingPlane(int index)
		{
			if (index < 0 || index >= 6)
			{
				throw new IndexOutOfRangeException("Invalid plane index");
			}
			fixed (float* ptr = &_cameraCullPlanes.FixedElementField)
			{
				return new Plane(new Vector3(*(float*)((long)ptr + (long)(IntPtr)(void*)((long)(index * 4) * 4L)), *(float*)((long)ptr + (long)(IntPtr)(void*)((long)(index * 4 + 1) * 4L)), *(float*)((long)ptr + (long)(IntPtr)(void*)((long)(index * 4 + 2) * 4L))), *(float*)((long)ptr + (long)(IntPtr)(void*)((long)(index * 4 + 3) * 4L)));
			}
		}

		/// <summary>
		///   <para>Set a camera culling plane.</para>
		/// </summary>
		/// <param name="index">Plane index (up to 5).</param>
		/// <param name="plane">Camera culling plane.</param>
		public unsafe void SetCameraCullingPlane(int index, Plane plane)
		{
			if (index < 0 || index >= 6)
			{
				throw new IndexOutOfRangeException("Invalid plane index");
			}
			fixed (float* ptr = &_cameraCullPlanes.FixedElementField)
			{
				_003F val = (long)ptr + (long)(IntPtr)(void*)((long)(index * 4) * 4L);
				Vector3 normal = plane.normal;
				*(float*)val = normal.x;
				_003F val2 = (long)ptr + (long)(IntPtr)(void*)((long)(index * 4 + 1) * 4L);
				Vector3 normal2 = plane.normal;
				*(float*)val2 = normal2.y;
				_003F val3 = (long)ptr + (long)(IntPtr)(void*)((long)(index * 4 + 2) * 4L);
				Vector3 normal3 = plane.normal;
				*(float*)val3 = normal3.z;
				*(float*)((long)ptr + (long)(IntPtr)(void*)((long)(index * 4 + 3) * 4L)) = plane.distance;
			}
		}
	}
}
