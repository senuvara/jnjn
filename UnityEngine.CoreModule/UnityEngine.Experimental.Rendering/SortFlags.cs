using System;

namespace UnityEngine.Experimental.Rendering
{
	/// <summary>
	///   <para>How to sort objects during rendering.</para>
	/// </summary>
	[Flags]
	public enum SortFlags
	{
		/// <summary>
		///   <para>Do not sort objects.</para>
		/// </summary>
		None = 0x0,
		/// <summary>
		///   <para>Sort by renderer sorting layer.</para>
		/// </summary>
		SortingLayer = 0x1,
		/// <summary>
		///   <para>Sort by material render queue.</para>
		/// </summary>
		RenderQueue = 0x2,
		/// <summary>
		///   <para>Sort objects back to front.</para>
		/// </summary>
		BackToFront = 0x4,
		/// <summary>
		///   <para>Sort objects in rough front-to-back buckets.</para>
		/// </summary>
		QuantizedFrontToBack = 0x8,
		/// <summary>
		///   <para>Sort objects to reduce draw state changes.</para>
		/// </summary>
		OptimizeStateChanges = 0x10,
		/// <summary>
		///   <para>Sort renderers taking canvas order into account.</para>
		/// </summary>
		CanvasOrder = 0x20,
		/// <summary>
		///   <para>Typical sorting for opaque objects.</para>
		/// </summary>
		CommonOpaque = 0x3B,
		/// <summary>
		///   <para>Typical sorting for transparencies.</para>
		/// </summary>
		CommonTransparent = 0x17
	}
}
