using System;

namespace UnityEngine.Experimental.Rendering
{
	/// <summary>
	///   <para>What kind of per-object data to setup during rendering.</para>
	/// </summary>
	[Flags]
	public enum RendererConfiguration
	{
		/// <summary>
		///   <para>Do not setup any particular per-object data besides the transformation matrix.</para>
		/// </summary>
		None = 0x0,
		/// <summary>
		///   <para>Setup per-object light probe SH data.</para>
		/// </summary>
		PerObjectLightProbe = 0x1,
		/// <summary>
		///   <para>Setup per-object reflection probe data.</para>
		/// </summary>
		PerObjectReflectionProbes = 0x2,
		/// <summary>
		///   <para>Setup per-object light probe proxy volume data.</para>
		/// </summary>
		PerObjectLightProbeProxyVolume = 0x4,
		/// <summary>
		///   <para>Setup per-object lightmaps.</para>
		/// </summary>
		PerObjectLightmaps = 0x8,
		/// <summary>
		///   <para>Setup per-object light indices.</para>
		/// </summary>
		ProvideLightIndices = 0x10,
		/// <summary>
		///   <para>Setup per-object motion vectors.</para>
		/// </summary>
		PerObjectMotionVectors = 0x20,
		PerObjectLightIndices8 = 0x40
	}
}
