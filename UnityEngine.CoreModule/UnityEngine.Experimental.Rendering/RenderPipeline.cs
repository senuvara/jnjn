using System;

namespace UnityEngine.Experimental.Rendering
{
	/// <summary>
	///   <para>Defines a series of commands and settings that describes how Unity renders a frame.</para>
	/// </summary>
	public abstract class RenderPipeline : IRenderPipeline, IDisposable
	{
		/// <summary>
		///   <para>When the IRenderPipeline is invalid or destroyed this returns true.</para>
		/// </summary>
		public bool disposed
		{
			get;
			private set;
		}

		/// <summary>
		///   <para>Defines custom rendering for this RenderPipeline.</para>
		/// </summary>
		/// <param name="renderContext"></param>
		/// <param name="cameras"></param>
		public virtual void Render(ScriptableRenderContext renderContext, Camera[] cameras)
		{
			if (disposed)
			{
				throw new ObjectDisposedException($"{this} has been disposed. Do not call Render on disposed RenderLoops.");
			}
		}

		/// <summary>
		///   <para>Dispose the Renderpipeline destroying all internal state.</para>
		/// </summary>
		public virtual void Dispose()
		{
			disposed = true;
		}
	}
}
