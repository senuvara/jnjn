using System;

namespace UnityEngine.Rendering
{
	/// <summary>
	///   <para>Specifies which color components will get written into the target framebuffer.</para>
	/// </summary>
	[Flags]
	public enum ColorWriteMask
	{
		/// <summary>
		///   <para>Write alpha component.</para>
		/// </summary>
		Alpha = 0x1,
		/// <summary>
		///   <para>Write blue component.</para>
		/// </summary>
		Blue = 0x2,
		/// <summary>
		///   <para>Write green component.</para>
		/// </summary>
		Green = 0x4,
		/// <summary>
		///   <para>Write red component.</para>
		/// </summary>
		Red = 0x8,
		/// <summary>
		///   <para>Write all components (R, G, B and Alpha).</para>
		/// </summary>
		All = 0xF
	}
}
