namespace UnityEngine.Rendering
{
	/// <summary>
	///   <para>Light probe interpolation type.</para>
	/// </summary>
	public enum LightProbeUsage
	{
		/// <summary>
		///   <para>Light Probes are not used.</para>
		/// </summary>
		Off,
		/// <summary>
		///   <para>Simple light probe interpolation is used.</para>
		/// </summary>
		BlendProbes,
		/// <summary>
		///   <para>Uses a 3D grid of interpolated light probes.</para>
		/// </summary>
		UseProxyVolume
	}
}
