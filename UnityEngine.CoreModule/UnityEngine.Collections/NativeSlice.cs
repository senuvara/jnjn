using System;
using System.Collections;
using System.Collections.Generic;
using System.Diagnostics;

namespace UnityEngine.Collections
{
	[DebuggerTypeProxy(typeof(NativeSliceDebugView<>))]
	[NativeContainerSupportsMinMaxWriteRestriction]
	[DebuggerDisplay("Length = {Length}")]
	[NativeContainer]
	public struct NativeSlice<T> : IEnumerable<T>, IEnumerable where T : struct
	{
		public struct Enumerator : IEnumerator<T>, IEnumerator, IDisposable
		{
			private NativeSlice<T> array;

			private int index;

			object IEnumerator.Current => Current;

			public T Current => array[index];

			public Enumerator(ref NativeSlice<T> array)
			{
				this.array = array;
				index = -1;
			}

			public void Dispose()
			{
			}

			public bool MoveNext()
			{
				index++;
				return index < array.Length;
			}

			public void Reset()
			{
				index = -1;
			}
		}

		private IntPtr m_Buffer;

		private int m_Stride;

		private int m_Length;

		public T this[int index]
		{
			get
			{
				if ((uint)index >= (uint)m_Length)
				{
					FailOutOfRangeError(index);
				}
				return UnsafeUtility.ReadArrayElementWithStride<T>(m_Buffer, index, m_Stride);
			}
			set
			{
				if ((uint)index >= (uint)m_Length)
				{
					FailOutOfRangeError(index);
				}
				UnsafeUtility.WriteArrayElementWithStride(m_Buffer, index, m_Stride, value);
			}
		}

		public int Stride => m_Stride;

		public int Length => m_Length;

		public IntPtr UnsafePtr => m_Buffer;

		public IntPtr UnsafeReadOnlyPtr => m_Buffer;

		public NativeSlice(NativeArray<T> array)
			: this(array, 0, array.Length)
		{
		}

		public NativeSlice(NativeArray<T> array, int start)
			: this(array, start, array.Length - start)
		{
		}

		public unsafe NativeSlice(NativeArray<T> array, int start, int length)
		{
			if (start < 0)
			{
				throw new ArgumentException("Slice start range must be >= 0.");
			}
			if (length < 0)
			{
				throw new ArgumentException("Slice length must be >= 0.");
			}
			if (start + length > array.Length)
			{
				throw new ArgumentException("Slice start + length range must be <= array.Length");
			}
			m_Stride = UnsafeUtility.SizeOf<T>();
			byte* value = (byte*)(void*)array.m_Buffer + m_Stride * start;
			m_Buffer = (IntPtr)(void*)value;
			m_Length = length;
		}

		public NativeSlice<U> SliceConvert<U>() where U : struct
		{
			if (m_Stride != UnsafeUtility.SizeOf<T>())
			{
				throw new ArgumentException("SliceConvert requires that stride matches the size of the source type");
			}
			NativeSlice<U> result = default(NativeSlice<U>);
			result.m_Buffer = m_Buffer;
			int num = result.m_Stride = UnsafeUtility.SizeOf<U>();
			if (m_Stride * m_Length % num != 0)
			{
				throw new ArgumentException("SliceConvert requires that Length * Stride is a multiple of sizeof(U).");
			}
			result.m_Length = m_Length * m_Stride / num;
			return result;
		}

		public unsafe NativeSlice<U> SliceWithStride<U>(int offset) where U : struct
		{
			if (offset < 0)
			{
				throw new ArgumentException("SliceWithStride offset must be >= 0");
			}
			if (offset + UnsafeUtility.SizeOf<U>() > UnsafeUtility.SizeOf<T>())
			{
				throw new ArgumentException("SliceWithStride sizeof(U) + offset must be <= sizeof(T)");
			}
			byte* value = (byte*)(void*)m_Buffer + offset;
			NativeSlice<U> result = default(NativeSlice<U>);
			result.m_Buffer = (IntPtr)(void*)value;
			result.m_Stride = m_Stride;
			result.m_Length = m_Length;
			return result;
		}

		public NativeSlice<U> SliceWithStride<U>() where U : struct
		{
			return SliceWithStride<U>(0);
		}

		public NativeSlice<U> SliceWithStride<U>(string offsetFieldName) where U : struct
		{
			int offset = UnsafeUtility.OffsetOf<T>(offsetFieldName);
			return SliceWithStride<U>(offset);
		}

		private void FailOutOfRangeError(int index)
		{
			throw new IndexOutOfRangeException($"Index {index} is out of range of '{Length}' Length.");
		}

		public void CopyFrom(NativeSlice<T> slice)
		{
			if (Length != slice.Length)
			{
				throw new ArgumentException($"array.Length ({slice.Length}) does not match NativeSlice.Length({Length}).");
			}
			for (int i = 0; i != m_Length; i++)
			{
				this[i] = slice[i];
			}
		}

		public void CopyFrom(T[] array)
		{
			if (Length != array.Length)
			{
				throw new ArgumentException($"array.Length ({array.Length}) does not match NativeSlice.Length({Length}).");
			}
			for (int i = 0; i != m_Length; i++)
			{
				this[i] = array[i];
			}
		}

		public void CopyTo(NativeArray<T> array)
		{
			if (Length != array.Length)
			{
				throw new ArgumentException($"array.Length ({array.Length}) does not match NativeSlice.Length({Length}).");
			}
			for (int i = 0; i != m_Length; i++)
			{
				array[i] = this[i];
			}
		}

		public void CopyTo(T[] array)
		{
			if (Length != array.Length)
			{
				throw new ArgumentException($"array.Length ({array.Length}) does not match NativeSlice.Length({Length}).");
			}
			for (int i = 0; i != m_Length; i++)
			{
				array[i] = this[i];
			}
		}

		public T[] ToArray()
		{
			T[] array = new T[Length];
			CopyTo(array);
			return array;
		}

		public IEnumerator<T> GetEnumerator()
		{
			return new Enumerator(ref this);
		}

		IEnumerator IEnumerable.GetEnumerator()
		{
			return GetEnumerator();
		}
	}
}
