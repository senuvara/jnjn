namespace UnityEngine.Collections
{
	internal sealed class NativeArrayDebugView<T> where T : struct
	{
		private NativeArray<T> array;

		public T[] Items => array.ToArray();

		public NativeArrayDebugView(NativeArray<T> array)
		{
			this.array = array;
		}
	}
}
