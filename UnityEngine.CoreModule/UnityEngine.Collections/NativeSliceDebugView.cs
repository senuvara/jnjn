namespace UnityEngine.Collections
{
	internal sealed class NativeSliceDebugView<T> where T : struct
	{
		private NativeSlice<T> array;

		public T[] Items => array.ToArray();

		public NativeSliceDebugView(NativeSlice<T> array)
		{
			this.array = array;
		}
	}
}
