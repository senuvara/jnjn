using System;
using System.Collections;
using System.Collections.Generic;
using System.ComponentModel;
using System.Diagnostics;

namespace UnityEngine.Collections
{
	[DebuggerTypeProxy(typeof(NativeArrayDebugView<>))]
	[NativeContainer]
	[NativeContainerSupportsDeallocateOnJobCompletion]
	[NativeContainerSupportsMinMaxWriteRestriction]
	[DebuggerDisplay("Length = {Length}")]
	public struct NativeArray<T> : IDisposable, IEnumerable<T>, IEnumerable where T : struct
	{
		public struct Enumerator : IEnumerator<T>, IEnumerator, IDisposable
		{
			private NativeArray<T> array;

			private int index;

			object IEnumerator.Current => Current;

			public T Current => array[index];

			public Enumerator(ref NativeArray<T> array)
			{
				this.array = array;
				index = -1;
			}

			public void Dispose()
			{
			}

			public bool MoveNext()
			{
				index++;
				return index < array.Length;
			}

			public void Reset()
			{
				index = -1;
			}
		}

		internal IntPtr m_Buffer;

		internal int m_Length;

		private Allocator m_AllocatorLabel;

		public int Length => m_Length;

		public T this[int index]
		{
			get
			{
				if ((uint)index >= (uint)m_Length)
				{
					FailOutOfRangeError(index);
				}
				return UnsafeUtility.ReadArrayElement<T>(m_Buffer, index);
			}
			set
			{
				if ((uint)index >= (uint)m_Length)
				{
					FailOutOfRangeError(index);
				}
				UnsafeUtility.WriteArrayElement(m_Buffer, index, value);
			}
		}

		public bool IsCreated => m_Buffer != IntPtr.Zero;

		public IntPtr UnsafePtr => m_Buffer;

		public IntPtr UnsafeReadOnlyPtr => m_Buffer;

		public NativeArray(int length, Allocator allocMode)
		{
			Allocate(length, allocMode, out this);
			UnsafeUtility.MemClear(m_Buffer, Length * UnsafeUtility.SizeOf<T>());
		}

		public NativeArray(T[] array, Allocator allocMode)
		{
			if (array == null)
			{
				throw new ArgumentNullException("array");
			}
			Allocate(array.Length, allocMode, out this);
			CopyFrom(array);
		}

		public NativeArray(NativeArray<T> array, Allocator allocMode)
		{
			Allocate(array.Length, allocMode, out this);
			CopyFrom(array);
		}

		[EditorBrowsable(EditorBrowsableState.Never)]
		internal static NativeArray<T> ConvertExistingDataToNativeArrayInternal(IntPtr dataPointer, int length, AtomicSafetyHandle safety, Allocator allocMode)
		{
			NativeArray<T> result = default(NativeArray<T>);
			result.m_Buffer = dataPointer;
			result.m_Length = length;
			result.m_AllocatorLabel = allocMode;
			return result;
		}

		private static void Allocate(int length, Allocator allocator, out NativeArray<T> array)
		{
			if (allocator <= Allocator.None)
			{
				throw new ArgumentOutOfRangeException("allocMode", "Allocator must be Temp, Job or Persistent");
			}
			if (length < 0)
			{
				throw new ArgumentOutOfRangeException("length", "Length must be >= 0");
			}
			long num = (long)UnsafeUtility.SizeOf<T>() * (long)length;
			if (num > int.MaxValue)
			{
				throw new ArgumentOutOfRangeException("length", "Length * sizeof(T) cannot exceed " + int.MaxValue + "bytes");
			}
			array.m_Buffer = UnsafeUtility.Malloc((int)num, UnsafeUtility.AlignOf<T>(), allocator);
			array.m_Length = length;
			array.m_AllocatorLabel = allocator;
		}

		public void Dispose()
		{
			UnsafeUtility.Free(m_Buffer, m_AllocatorLabel);
			m_Buffer = IntPtr.Zero;
			m_Length = 0;
		}

		internal void GetUnsafeBufferPointerWithoutChecksInternal(out AtomicSafetyHandle handle, out IntPtr ptr)
		{
			ptr = m_Buffer;
			handle = default(AtomicSafetyHandle);
		}

		public void CopyFrom(T[] array)
		{
			if (Length != array.Length)
			{
				throw new ArgumentException("Array length does not match the length of this instance");
			}
			for (int i = 0; i < Length; i++)
			{
				UnsafeUtility.WriteArrayElement(m_Buffer, i, array[i]);
			}
		}

		public void CopyFrom(NativeArray<T> array)
		{
			array.CopyTo(this);
		}

		public void CopyTo(T[] array)
		{
			if (Length != array.Length)
			{
				throw new ArgumentException("Array length does not match the length of this instance");
			}
			for (int i = 0; i < Length; i++)
			{
				array[i] = UnsafeUtility.ReadArrayElement<T>(m_Buffer, i);
			}
		}

		public void CopyTo(NativeArray<T> array)
		{
			if (Length != array.Length)
			{
				throw new ArgumentException("Array length does not match the length of this instance");
			}
			UnsafeUtility.MemCpy(array.m_Buffer, m_Buffer, Length * UnsafeUtility.SizeOf<T>());
		}

		public T[] ToArray()
		{
			T[] array = new T[Length];
			CopyTo(array);
			return array;
		}

		private void FailOutOfRangeError(int index)
		{
			throw new IndexOutOfRangeException($"Index {index} is out of range of '{Length}' Length.");
		}

		public IEnumerator<T> GetEnumerator()
		{
			return new Enumerator(ref this);
		}

		IEnumerator IEnumerable.GetEnumerator()
		{
			return GetEnumerator();
		}
	}
}
