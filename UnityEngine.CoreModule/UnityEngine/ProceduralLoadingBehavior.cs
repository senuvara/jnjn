using System;

namespace UnityEngine
{
	/// <summary>
	///   <para>ProceduralMaterial loading behavior.</para>
	/// </summary>
	[Obsolete("Built-in support for Substance Designer materials has been deprecated and will be removed in Unity 2018.1. To continue using Substance Designer materials in Unity 2018.1, you will need to install Allegorithmic's external importer from the Asset Store.", false)]
	public enum ProceduralLoadingBehavior
	{
		/// <summary>
		///   <para>Does not generate the textures automatically when the scene is loaded as it is set to "DoNothing". RebuildTextures() or RebuildTexturesImmediately() must be called to generate the textures.</para>
		/// </summary>
		DoNothing,
		/// <summary>
		///   <para>Generate the textures when loading to favor application's size (default on supported platform).</para>
		/// </summary>
		Generate,
		/// <summary>
		///   <para>Bake the textures to speed up loading and keep the ProceduralMaterial data so that it can still be tweaked and regenerated later on.</para>
		/// </summary>
		BakeAndKeep,
		/// <summary>
		///   <para>Bake the textures to speed up loading and discard the ProceduralMaterial data (default on unsupported platform).</para>
		/// </summary>
		BakeAndDiscard,
		/// <summary>
		///   <para>Generate the textures when loading and cache them to diskflash to speed up subsequent gameapplication startups.</para>
		/// </summary>
		Cache,
		/// <summary>
		///   <para>Does not generate the textures automatically when the scene is loaded as it is set to "DoNothing". RebuildTextures() or RebuildTexturesImmediately() must be called to generate the textures. After the textures have been generrated for the first time, they are cached to diskflash to speed up subsequent gameapplication startups. This setting will not load the cached textures automatically when the scene is loaded as it is still set to "DoNothing".  RebuildTextures() or RebuildTexturesImmediately() must be called again to load the previously cached textures.</para>
		/// </summary>
		DoNothingAndCache
	}
}
