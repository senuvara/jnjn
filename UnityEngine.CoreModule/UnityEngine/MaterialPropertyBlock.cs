using System;
using System.Collections.Generic;
using System.Runtime.CompilerServices;
using UnityEngine.Scripting;

namespace UnityEngine
{
	/// <summary>
	///   <para>A block of material values to apply.</para>
	/// </summary>
	public sealed class MaterialPropertyBlock
	{
		internal IntPtr m_Ptr;

		/// <summary>
		///   <para>Is the material property block empty? (Read Only)</para>
		/// </summary>
		public bool isEmpty
		{
			[MethodImpl(MethodImplOptions.InternalCall)]
			[GeneratedByOldBindingsGenerator]
			get;
		}

		public MaterialPropertyBlock()
		{
			InitBlock();
		}

		[MethodImpl(MethodImplOptions.InternalCall)]
		[GeneratedByOldBindingsGenerator]
		internal extern void InitBlock();

		[MethodImpl(MethodImplOptions.InternalCall)]
		[ThreadAndSerializationSafe]
		[GeneratedByOldBindingsGenerator]
		internal extern void DestroyBlock();

		~MaterialPropertyBlock()
		{
			DestroyBlock();
		}

		/// <summary>
		///   <para>Clear material property values.</para>
		/// </summary>
		[MethodImpl(MethodImplOptions.InternalCall)]
		[GeneratedByOldBindingsGenerator]
		public extern void Clear();

		[MethodImpl(MethodImplOptions.InternalCall)]
		[GeneratedByOldBindingsGenerator]
		private extern void SetFloatImpl(int nameID, float value);

		private void SetVectorImpl(int nameID, Vector4 value)
		{
			INTERNAL_CALL_SetVectorImpl(this, nameID, ref value);
		}

		[MethodImpl(MethodImplOptions.InternalCall)]
		[GeneratedByOldBindingsGenerator]
		private static extern void INTERNAL_CALL_SetVectorImpl(MaterialPropertyBlock self, int nameID, ref Vector4 value);

		private void SetMatrixImpl(int nameID, Matrix4x4 value)
		{
			INTERNAL_CALL_SetMatrixImpl(this, nameID, ref value);
		}

		[MethodImpl(MethodImplOptions.InternalCall)]
		[GeneratedByOldBindingsGenerator]
		private static extern void INTERNAL_CALL_SetMatrixImpl(MaterialPropertyBlock self, int nameID, ref Matrix4x4 value);

		[MethodImpl(MethodImplOptions.InternalCall)]
		[GeneratedByOldBindingsGenerator]
		private extern void SetTextureImpl(int nameID, Texture value);

		[MethodImpl(MethodImplOptions.InternalCall)]
		[GeneratedByOldBindingsGenerator]
		private extern void SetBufferImpl(int nameID, ComputeBuffer value);

		private void SetColorImpl(int nameID, Color value)
		{
			INTERNAL_CALL_SetColorImpl(this, nameID, ref value);
		}

		[MethodImpl(MethodImplOptions.InternalCall)]
		[GeneratedByOldBindingsGenerator]
		private static extern void INTERNAL_CALL_SetColorImpl(MaterialPropertyBlock self, int nameID, ref Color value);

		[MethodImpl(MethodImplOptions.InternalCall)]
		[GeneratedByOldBindingsGenerator]
		private extern void SetFloatArrayImpl(int nameID, float[] values, int count);

		[MethodImpl(MethodImplOptions.InternalCall)]
		[GeneratedByOldBindingsGenerator]
		private extern void SetVectorArrayImpl(int nameID, Vector4[] values, int count);

		[MethodImpl(MethodImplOptions.InternalCall)]
		[GeneratedByOldBindingsGenerator]
		private extern void SetMatrixArrayImpl(int nameID, Matrix4x4[] values, int count);

		[MethodImpl(MethodImplOptions.InternalCall)]
		[GeneratedByOldBindingsGenerator]
		private extern float GetFloatImpl(int nameID);

		private Vector4 GetVectorImpl(int nameID)
		{
			INTERNAL_CALL_GetVectorImpl(this, nameID, out Vector4 value);
			return value;
		}

		[MethodImpl(MethodImplOptions.InternalCall)]
		[GeneratedByOldBindingsGenerator]
		private static extern void INTERNAL_CALL_GetVectorImpl(MaterialPropertyBlock self, int nameID, out Vector4 value);

		private Color GetColorImpl(int nameID)
		{
			INTERNAL_CALL_GetColorImpl(this, nameID, out Color value);
			return value;
		}

		[MethodImpl(MethodImplOptions.InternalCall)]
		[GeneratedByOldBindingsGenerator]
		private static extern void INTERNAL_CALL_GetColorImpl(MaterialPropertyBlock self, int nameID, out Color value);

		private Matrix4x4 GetMatrixImpl(int nameID)
		{
			INTERNAL_CALL_GetMatrixImpl(this, nameID, out Matrix4x4 value);
			return value;
		}

		[MethodImpl(MethodImplOptions.InternalCall)]
		[GeneratedByOldBindingsGenerator]
		private static extern void INTERNAL_CALL_GetMatrixImpl(MaterialPropertyBlock self, int nameID, out Matrix4x4 value);

		[MethodImpl(MethodImplOptions.InternalCall)]
		[GeneratedByOldBindingsGenerator]
		private extern float[] GetFloatArrayImpl(int nameID);

		[MethodImpl(MethodImplOptions.InternalCall)]
		[GeneratedByOldBindingsGenerator]
		private extern Vector4[] GetVectorArrayImpl(int nameID);

		[MethodImpl(MethodImplOptions.InternalCall)]
		[GeneratedByOldBindingsGenerator]
		private extern Matrix4x4[] GetMatrixArrayImpl(int nameID);

		[MethodImpl(MethodImplOptions.InternalCall)]
		[GeneratedByOldBindingsGenerator]
		private extern void GetFloatArrayImplList(int nameID, object list);

		[MethodImpl(MethodImplOptions.InternalCall)]
		[GeneratedByOldBindingsGenerator]
		private extern void GetVectorArrayImplList(int nameID, object list);

		[MethodImpl(MethodImplOptions.InternalCall)]
		[GeneratedByOldBindingsGenerator]
		private extern void GetMatrixArrayImplList(int nameID, object list);

		[MethodImpl(MethodImplOptions.InternalCall)]
		[GeneratedByOldBindingsGenerator]
		private extern Texture GetTextureImpl(int nameID);

		/// <summary>
		///   <para>Set a float property.</para>
		/// </summary>
		/// <param name="name">The name of the property.</param>
		/// <param name="nameID">The name ID of the property retrieved by Shader.PropertyToID.</param>
		/// <param name="value">The float value to set.</param>
		public void SetFloat(string name, float value)
		{
			SetFloat(Shader.PropertyToID(name), value);
		}

		/// <summary>
		///   <para>Set a float property.</para>
		/// </summary>
		/// <param name="name">The name of the property.</param>
		/// <param name="nameID">The name ID of the property retrieved by Shader.PropertyToID.</param>
		/// <param name="value">The float value to set.</param>
		public void SetFloat(int nameID, float value)
		{
			SetFloatImpl(nameID, value);
		}

		/// <summary>
		///   <para>Set a vector property.</para>
		/// </summary>
		/// <param name="name">The name of the property.</param>
		/// <param name="nameID">The name ID of the property retrieved by Shader.PropertyToID.</param>
		/// <param name="value">The Vector4 value to set.</param>
		public void SetVector(string name, Vector4 value)
		{
			SetVector(Shader.PropertyToID(name), value);
		}

		/// <summary>
		///   <para>Set a vector property.</para>
		/// </summary>
		/// <param name="name">The name of the property.</param>
		/// <param name="nameID">The name ID of the property retrieved by Shader.PropertyToID.</param>
		/// <param name="value">The Vector4 value to set.</param>
		public void SetVector(int nameID, Vector4 value)
		{
			SetVectorImpl(nameID, value);
		}

		/// <summary>
		///   <para>Set a color property.</para>
		/// </summary>
		/// <param name="name">The name of the property.</param>
		/// <param name="nameID">The name ID of the property retrieved by Shader.PropertyToID.</param>
		/// <param name="value">The Color value to set.</param>
		public void SetColor(string name, Color value)
		{
			SetColor(Shader.PropertyToID(name), value);
		}

		/// <summary>
		///   <para>Set a color property.</para>
		/// </summary>
		/// <param name="name">The name of the property.</param>
		/// <param name="nameID">The name ID of the property retrieved by Shader.PropertyToID.</param>
		/// <param name="value">The Color value to set.</param>
		public void SetColor(int nameID, Color value)
		{
			SetColorImpl(nameID, value);
		}

		/// <summary>
		///   <para>Set a matrix property.</para>
		/// </summary>
		/// <param name="name">The name of the property.</param>
		/// <param name="nameID">The name ID of the property retrieved by Shader.PropertyToID.</param>
		/// <param name="value">The matrix value to set.</param>
		public void SetMatrix(string name, Matrix4x4 value)
		{
			SetMatrix(Shader.PropertyToID(name), value);
		}

		/// <summary>
		///   <para>Set a matrix property.</para>
		/// </summary>
		/// <param name="name">The name of the property.</param>
		/// <param name="nameID">The name ID of the property retrieved by Shader.PropertyToID.</param>
		/// <param name="value">The matrix value to set.</param>
		public void SetMatrix(int nameID, Matrix4x4 value)
		{
			SetMatrixImpl(nameID, value);
		}

		/// <summary>
		///   <para>Set a ComputeBuffer property.</para>
		/// </summary>
		/// <param name="name">The name of the property.</param>
		/// <param name="nameID">The name ID of the property retrieved by Shader.PropertyToID.</param>
		/// <param name="value">The ComputeBuffer to set.</param>
		public void SetBuffer(string name, ComputeBuffer value)
		{
			SetBuffer(Shader.PropertyToID(name), value);
		}

		/// <summary>
		///   <para>Set a ComputeBuffer property.</para>
		/// </summary>
		/// <param name="name">The name of the property.</param>
		/// <param name="nameID">The name ID of the property retrieved by Shader.PropertyToID.</param>
		/// <param name="value">The ComputeBuffer to set.</param>
		public void SetBuffer(int nameID, ComputeBuffer value)
		{
			SetBufferImpl(nameID, value);
		}

		/// <summary>
		///   <para>Set a texture property.</para>
		/// </summary>
		/// <param name="name">The name of the property.</param>
		/// <param name="nameID">The name ID of the property retrieved by Shader.PropertyToID.</param>
		/// <param name="value">The Texture to set.</param>
		public void SetTexture(string name, Texture value)
		{
			SetTexture(Shader.PropertyToID(name), value);
		}

		/// <summary>
		///   <para>Set a texture property.</para>
		/// </summary>
		/// <param name="name">The name of the property.</param>
		/// <param name="nameID">The name ID of the property retrieved by Shader.PropertyToID.</param>
		/// <param name="value">The Texture to set.</param>
		public void SetTexture(int nameID, Texture value)
		{
			if (value == null)
			{
				throw new ArgumentNullException("value");
			}
			SetTextureImpl(nameID, value);
		}

		public void SetFloatArray(string name, List<float> values)
		{
			SetFloatArray(Shader.PropertyToID(name), values);
		}

		public void SetFloatArray(int nameID, List<float> values)
		{
			SetFloatArray(nameID, (float[])NoAllocHelpers.ExtractArrayFromList(values), values.Count);
		}

		/// <summary>
		///   <para>Set a float array property.</para>
		/// </summary>
		/// <param name="name">The name of the property.</param>
		/// <param name="nameID">The name ID of the property retrieved by Shader.PropertyToID.</param>
		/// <param name="values">The array to set.</param>
		public void SetFloatArray(string name, float[] values)
		{
			SetFloatArray(Shader.PropertyToID(name), values);
		}

		/// <summary>
		///   <para>Set a float array property.</para>
		/// </summary>
		/// <param name="name">The name of the property.</param>
		/// <param name="nameID">The name ID of the property retrieved by Shader.PropertyToID.</param>
		/// <param name="values">The array to set.</param>
		public void SetFloatArray(int nameID, float[] values)
		{
			SetFloatArray(nameID, values, values.Length);
		}

		private void SetFloatArray(int nameID, float[] values, int count)
		{
			if (values == null)
			{
				throw new ArgumentNullException("values");
			}
			if (values.Length == 0)
			{
				throw new ArgumentException("Zero-sized array is not allowed.");
			}
			if (values.Length < count)
			{
				throw new ArgumentException("array has less elements than passed count.");
			}
			SetFloatArrayImpl(nameID, values, count);
		}

		public void SetVectorArray(string name, List<Vector4> values)
		{
			SetVectorArray(Shader.PropertyToID(name), values);
		}

		public void SetVectorArray(int nameID, List<Vector4> values)
		{
			SetVectorArray(nameID, (Vector4[])NoAllocHelpers.ExtractArrayFromList(values), values.Count);
		}

		/// <summary>
		///   <para>Set a vector array property.</para>
		/// </summary>
		/// <param name="nameID">The name of the property.</param>
		/// <param name="values">The name ID of the property retrieved by Shader.PropertyToID.</param>
		/// <param name="name">The array to set.</param>
		public void SetVectorArray(string name, Vector4[] values)
		{
			SetVectorArray(Shader.PropertyToID(name), values);
		}

		/// <summary>
		///   <para>Set a vector array property.</para>
		/// </summary>
		/// <param name="nameID">The name of the property.</param>
		/// <param name="values">The name ID of the property retrieved by Shader.PropertyToID.</param>
		/// <param name="name">The array to set.</param>
		public void SetVectorArray(int nameID, Vector4[] values)
		{
			SetVectorArray(nameID, values, values.Length);
		}

		private void SetVectorArray(int nameID, Vector4[] values, int count)
		{
			if (values == null)
			{
				throw new ArgumentNullException("values");
			}
			if (values.Length == 0)
			{
				throw new ArgumentException("Zero-sized array is not allowed.");
			}
			if (values.Length < count)
			{
				throw new ArgumentException("array has less elements than passed count.");
			}
			SetVectorArrayImpl(nameID, values, count);
		}

		public void SetMatrixArray(string name, List<Matrix4x4> values)
		{
			SetMatrixArray(Shader.PropertyToID(name), values);
		}

		public void SetMatrixArray(int nameID, List<Matrix4x4> values)
		{
			SetMatrixArray(nameID, (Matrix4x4[])NoAllocHelpers.ExtractArrayFromList(values), values.Count);
		}

		/// <summary>
		///   <para>Set a matrix array property.</para>
		/// </summary>
		/// <param name="name">The name of the property.</param>
		/// <param name="values">The name ID of the property retrieved by Shader.PropertyToID.</param>
		/// <param name="nameID">The array to set.</param>
		public void SetMatrixArray(string name, Matrix4x4[] values)
		{
			SetMatrixArray(Shader.PropertyToID(name), values);
		}

		/// <summary>
		///   <para>Set a matrix array property.</para>
		/// </summary>
		/// <param name="name">The name of the property.</param>
		/// <param name="values">The name ID of the property retrieved by Shader.PropertyToID.</param>
		/// <param name="nameID">The array to set.</param>
		public void SetMatrixArray(int nameID, Matrix4x4[] values)
		{
			SetMatrixArray(nameID, values, values.Length);
		}

		private void SetMatrixArray(int nameID, Matrix4x4[] values, int count)
		{
			if (values == null)
			{
				throw new ArgumentNullException("values");
			}
			if (values.Length == 0)
			{
				throw new ArgumentException("Zero-sized array is not allowed.");
			}
			if (values.Length < count)
			{
				throw new ArgumentException("array has less elements than passed count.");
			}
			SetMatrixArrayImpl(nameID, values, count);
		}

		/// <summary>
		///   <para>Get a float from the property block.</para>
		/// </summary>
		/// <param name="name"></param>
		/// <param name="nameID"></param>
		public float GetFloat(string name)
		{
			return GetFloat(Shader.PropertyToID(name));
		}

		/// <summary>
		///   <para>Get a float from the property block.</para>
		/// </summary>
		/// <param name="name"></param>
		/// <param name="nameID"></param>
		public float GetFloat(int nameID)
		{
			return GetFloatImpl(nameID);
		}

		/// <summary>
		///   <para>Get a vector from the property block.</para>
		/// </summary>
		/// <param name="name"></param>
		/// <param name="nameID"></param>
		public Vector4 GetVector(string name)
		{
			return GetVector(Shader.PropertyToID(name));
		}

		/// <summary>
		///   <para>Get a vector from the property block.</para>
		/// </summary>
		/// <param name="name"></param>
		/// <param name="nameID"></param>
		public Vector4 GetVector(int nameID)
		{
			return GetVectorImpl(nameID);
		}

		/// <summary>
		///   <para>Get a color from the property block.</para>
		/// </summary>
		/// <param name="name"></param>
		/// <param name="nameID"></param>
		public Color GetColor(string name)
		{
			return GetColor(Shader.PropertyToID(name));
		}

		/// <summary>
		///   <para>Get a color from the property block.</para>
		/// </summary>
		/// <param name="name"></param>
		/// <param name="nameID"></param>
		public Color GetColor(int nameID)
		{
			return GetColorImpl(nameID);
		}

		/// <summary>
		///   <para>Get a matrix from the property block.</para>
		/// </summary>
		/// <param name="name"></param>
		/// <param name="nameID"></param>
		public Matrix4x4 GetMatrix(string name)
		{
			return GetMatrix(Shader.PropertyToID(name));
		}

		/// <summary>
		///   <para>Get a matrix from the property block.</para>
		/// </summary>
		/// <param name="name"></param>
		/// <param name="nameID"></param>
		public Matrix4x4 GetMatrix(int nameID)
		{
			return GetMatrixImpl(nameID);
		}

		public void GetFloatArray(string name, List<float> values)
		{
			GetFloatArray(Shader.PropertyToID(name), values);
		}

		public void GetFloatArray(int nameID, List<float> values)
		{
			if (values == null)
			{
				throw new ArgumentNullException("values");
			}
			GetFloatArrayImplList(nameID, values);
		}

		/// <summary>
		///   <para>Get a float array from the property block.</para>
		/// </summary>
		/// <param name="name"></param>
		/// <param name="nameID"></param>
		public float[] GetFloatArray(string name)
		{
			return GetFloatArray(Shader.PropertyToID(name));
		}

		/// <summary>
		///   <para>Get a float array from the property block.</para>
		/// </summary>
		/// <param name="name"></param>
		/// <param name="nameID"></param>
		public float[] GetFloatArray(int nameID)
		{
			return GetFloatArrayImpl(nameID);
		}

		public void GetVectorArray(string name, List<Vector4> values)
		{
			GetVectorArray(Shader.PropertyToID(name), values);
		}

		public void GetVectorArray(int nameID, List<Vector4> values)
		{
			if (values == null)
			{
				throw new ArgumentNullException("values");
			}
			GetVectorArrayImplList(nameID, values);
		}

		/// <summary>
		///   <para>Get a vector array from the property block.</para>
		/// </summary>
		/// <param name="name"></param>
		/// <param name="nameID"></param>
		public Vector4[] GetVectorArray(string name)
		{
			return GetVectorArray(Shader.PropertyToID(name));
		}

		/// <summary>
		///   <para>Get a vector array from the property block.</para>
		/// </summary>
		/// <param name="name"></param>
		/// <param name="nameID"></param>
		public Vector4[] GetVectorArray(int nameID)
		{
			return GetVectorArrayImpl(nameID);
		}

		public void GetMatrixArray(string name, List<Matrix4x4> values)
		{
			GetMatrixArray(Shader.PropertyToID(name), values);
		}

		public void GetMatrixArray(int nameID, List<Matrix4x4> values)
		{
			if (values == null)
			{
				throw new ArgumentNullException("values");
			}
			GetMatrixArrayImplList(nameID, values);
		}

		/// <summary>
		///   <para>Get a matrix array from the property block.</para>
		/// </summary>
		/// <param name="name"></param>
		/// <param name="nameID"></param>
		public Matrix4x4[] GetMatrixArray(string name)
		{
			return GetMatrixArray(Shader.PropertyToID(name));
		}

		/// <summary>
		///   <para>Get a matrix array from the property block.</para>
		/// </summary>
		/// <param name="name"></param>
		/// <param name="nameID"></param>
		public Matrix4x4[] GetMatrixArray(int nameID)
		{
			return GetMatrixArrayImpl(nameID);
		}

		/// <summary>
		///   <para>Get a texture from the property block.</para>
		/// </summary>
		/// <param name="name"></param>
		/// <param name="nameID"></param>
		public Texture GetTexture(string name)
		{
			return GetTexture(Shader.PropertyToID(name));
		}

		/// <summary>
		///   <para>Get a texture from the property block.</para>
		/// </summary>
		/// <param name="name"></param>
		/// <param name="nameID"></param>
		public Texture GetTexture(int nameID)
		{
			return GetTextureImpl(nameID);
		}

		[Obsolete("Use SetFloat instead (UnityUpgradable) -> SetFloat(*)", false)]
		public void AddFloat(string name, float value)
		{
			SetFloat(Shader.PropertyToID(name), value);
		}

		[Obsolete("Use SetFloat instead (UnityUpgradable) -> SetFloat(*)", false)]
		public void AddFloat(int nameID, float value)
		{
			SetFloat(nameID, value);
		}

		[Obsolete("Use SetVector instead (UnityUpgradable) -> SetVector(*)", false)]
		public void AddVector(string name, Vector4 value)
		{
			SetVector(Shader.PropertyToID(name), value);
		}

		[Obsolete("Use SetVector instead (UnityUpgradable) -> SetVector(*)", false)]
		public void AddVector(int nameID, Vector4 value)
		{
			SetVector(nameID, value);
		}

		[Obsolete("Use SetColor instead (UnityUpgradable) -> SetColor(*)", false)]
		public void AddColor(string name, Color value)
		{
			SetColor(Shader.PropertyToID(name), value);
		}

		[Obsolete("Use SetColor instead (UnityUpgradable) -> SetColor(*)", false)]
		public void AddColor(int nameID, Color value)
		{
			SetColor(nameID, value);
		}

		[Obsolete("Use SetMatrix instead (UnityUpgradable) -> SetMatrix(*)", false)]
		public void AddMatrix(string name, Matrix4x4 value)
		{
			SetMatrix(Shader.PropertyToID(name), value);
		}

		[Obsolete("Use SetMatrix instead (UnityUpgradable) -> SetMatrix(*)", false)]
		public void AddMatrix(int nameID, Matrix4x4 value)
		{
			SetMatrix(nameID, value);
		}

		[Obsolete("Use SetTexture instead (UnityUpgradable) -> SetTexture(*)", false)]
		public void AddTexture(string name, Texture value)
		{
			SetTexture(Shader.PropertyToID(name), value);
		}

		[Obsolete("Use SetTexture instead (UnityUpgradable) -> SetTexture(*)", false)]
		public void AddTexture(int nameID, Texture value)
		{
			SetTexture(nameID, value);
		}
	}
}
