using System;

namespace UnityEngine
{
	/// <summary>
	///   <para>Describes different types of camera.</para>
	/// </summary>
	[Flags]
	public enum CameraType
	{
		/// <summary>
		///   <para>Used to indicate a regular in-game camera.</para>
		/// </summary>
		Game = 0x1,
		/// <summary>
		///   <para>Used to indicate that a camera is used for rendering the Scene View in the Editor.</para>
		/// </summary>
		SceneView = 0x2,
		/// <summary>
		///   <para>Used to indicate a camera that is used for rendering previews in the Editor.</para>
		/// </summary>
		Preview = 0x4,
		/// <summary>
		///   <para>Used to indicate that a camera is used for rendering VR (in edit mode) in the Editor.</para>
		/// </summary>
		VR = 0x8,
		/// <summary>
		///   <para>Used to indicate a camera that is used for rendering reflection probes.</para>
		/// </summary>
		Reflection = 0x10
	}
}
