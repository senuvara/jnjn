using System;
using System.ComponentModel;
using System.Runtime.CompilerServices;
using UnityEngine.Bindings;
using UnityEngine.Internal;
using UnityEngine.Scripting;

namespace UnityEngine
{
	/// <summary>
	///   <para>Access to display information.</para>
	/// </summary>
	[NativeHeader("Runtime/Graphics/ScreenManager.h")]
	[StaticAccessor("GetScreenManager()", StaticAccessorType.Dot)]
	public sealed class Screen
	{
		/// <summary>
		///   <para>All fullscreen resolutions supported by the monitor (Read Only).</para>
		/// </summary>
		public static Resolution[] resolutions
		{
			[MethodImpl(MethodImplOptions.InternalCall)]
			[GeneratedByOldBindingsGenerator]
			get;
		}

		/// <summary>
		///   <para>The current screen resolution (Read Only).</para>
		/// </summary>
		public static Resolution currentResolution
		{
			get
			{
				INTERNAL_get_currentResolution(out Resolution value);
				return value;
			}
		}

		/// <summary>
		///   <para>Is the game running fullscreen?</para>
		/// </summary>
		public static bool fullScreen
		{
			[MethodImpl(MethodImplOptions.InternalCall)]
			[GeneratedByOldBindingsGenerator]
			get;
			[MethodImpl(MethodImplOptions.InternalCall)]
			[GeneratedByOldBindingsGenerator]
			set;
		}

		/// <summary>
		///   <para>The current width of the screen window in pixels (Read Only).</para>
		/// </summary>
		public static int width
		{
			[MethodImpl(MethodImplOptions.InternalCall)]
			[NativeMethod(Name = "GetWidth", IsThreadSafe = true)]
			get;
		}

		/// <summary>
		///   <para>The current height of the screen window in pixels (Read Only).</para>
		/// </summary>
		public static int height
		{
			[MethodImpl(MethodImplOptions.InternalCall)]
			[NativeMethod(Name = "GetHeight", IsThreadSafe = true)]
			get;
		}

		/// <summary>
		///   <para>The current DPI of the screen / device (Read Only).</para>
		/// </summary>
		public static float dpi
		{
			[MethodImpl(MethodImplOptions.InternalCall)]
			[NativeMethod(Name = "GetDPI")]
			get;
		}

		/// <summary>
		///   <para>Specifies logical orientation of the screen.</para>
		/// </summary>
		public static ScreenOrientation orientation
		{
			[MethodImpl(MethodImplOptions.InternalCall)]
			[NativeMethod(Name = "GetScreenOrientation")]
			get;
			[MethodImpl(MethodImplOptions.InternalCall)]
			[NativeMethod(Name = "RequestOrientation")]
			set;
		}

		/// <summary>
		///   <para>A power saving setting, allowing the screen to dim some time after the last active user interaction.</para>
		/// </summary>
		[NativeProperty("ScreenTimeout")]
		public static int sleepTimeout
		{
			[MethodImpl(MethodImplOptions.InternalCall)]
			get;
			[MethodImpl(MethodImplOptions.InternalCall)]
			set;
		}

		/// <summary>
		///   <para>Allow auto-rotation to portrait?</para>
		/// </summary>
		public static bool autorotateToPortrait
		{
			get
			{
				return IsOrientationEnabled(EnabledOrientation.kAutorotateToPortrait);
			}
			set
			{
				SetOrientationEnabled(EnabledOrientation.kAutorotateToPortrait, value);
			}
		}

		/// <summary>
		///   <para>Allow auto-rotation to portrait, upside down?</para>
		/// </summary>
		public static bool autorotateToPortraitUpsideDown
		{
			get
			{
				return IsOrientationEnabled(EnabledOrientation.kAutorotateToPortraitUpsideDown);
			}
			set
			{
				SetOrientationEnabled(EnabledOrientation.kAutorotateToPortraitUpsideDown, value);
			}
		}

		/// <summary>
		///   <para>Allow auto-rotation to landscape left?</para>
		/// </summary>
		public static bool autorotateToLandscapeLeft
		{
			get
			{
				return IsOrientationEnabled(EnabledOrientation.kAutorotateToLandscapeLeft);
			}
			set
			{
				SetOrientationEnabled(EnabledOrientation.kAutorotateToLandscapeLeft, value);
			}
		}

		/// <summary>
		///   <para>Allow auto-rotation to landscape right?</para>
		/// </summary>
		public static bool autorotateToLandscapeRight
		{
			get
			{
				return IsOrientationEnabled(EnabledOrientation.kAutorotateToLandscapeRight);
			}
			set
			{
				SetOrientationEnabled(EnabledOrientation.kAutorotateToLandscapeRight, value);
			}
		}

		/// <summary>
		///   <para>Returns the safe area of the screen in pixels (Read Only).</para>
		/// </summary>
		public static Rect safeArea
		{
			get
			{
				get_safeArea_Injected(out Rect ret);
				return ret;
			}
		}

		/// <summary>
		///   <para>Should the cursor be locked?</para>
		/// </summary>
		[EditorBrowsable(EditorBrowsableState.Never)]
		[Obsolete("Use Cursor.lockState and Cursor.visible instead.", false)]
		public static bool lockCursor
		{
			get
			{
				return CursorLockMode.Locked == Cursor.lockState;
			}
			set
			{
				if (value)
				{
					Cursor.visible = false;
					Cursor.lockState = CursorLockMode.Locked;
				}
				else
				{
					Cursor.lockState = CursorLockMode.None;
					Cursor.visible = true;
				}
			}
		}

		[MethodImpl(MethodImplOptions.InternalCall)]
		[GeneratedByOldBindingsGenerator]
		private static extern void INTERNAL_get_currentResolution(out Resolution value);

		/// <summary>
		///   <para>Switches the screen resolution.</para>
		/// </summary>
		/// <param name="width"></param>
		/// <param name="height"></param>
		/// <param name="fullscreen"></param>
		/// <param name="preferredRefreshRate"></param>
		[MethodImpl(MethodImplOptions.InternalCall)]
		[GeneratedByOldBindingsGenerator]
		public static extern void SetResolution(int width, int height, bool fullscreen, [UnityEngine.Internal.DefaultValue("0")] int preferredRefreshRate);

		/// <summary>
		///   <para>Switches the screen resolution.</para>
		/// </summary>
		/// <param name="width"></param>
		/// <param name="height"></param>
		/// <param name="fullscreen"></param>
		/// <param name="preferredRefreshRate"></param>
		[ExcludeFromDocs]
		public static void SetResolution(int width, int height, bool fullscreen)
		{
			int preferredRefreshRate = 0;
			SetResolution(width, height, fullscreen, preferredRefreshRate);
		}

		[MethodImpl(MethodImplOptions.InternalCall)]
		[NativeName("GetIsOrientationEnabled")]
		private static extern bool IsOrientationEnabled(EnabledOrientation orient);

		[MethodImpl(MethodImplOptions.InternalCall)]
		[NativeName("SetIsOrientationEnabled")]
		private static extern void SetOrientationEnabled(EnabledOrientation orient, bool enabled);

		[MethodImpl(MethodImplOptions.InternalCall)]
		private static extern void get_safeArea_Injected(out Rect ret);
	}
}
