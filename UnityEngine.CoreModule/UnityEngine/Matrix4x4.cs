using System;
using System.Runtime.CompilerServices;
using UnityEngine.Scripting;

namespace UnityEngine
{
	/// <summary>
	///   <para>A standard 4x4 transformation matrix.</para>
	/// </summary>
	[UsedByNativeCode]
	public struct Matrix4x4
	{
		public float m00;

		public float m10;

		public float m20;

		public float m30;

		public float m01;

		public float m11;

		public float m21;

		public float m31;

		public float m02;

		public float m12;

		public float m22;

		public float m32;

		public float m03;

		public float m13;

		public float m23;

		public float m33;

		private static readonly Matrix4x4 zeroMatrix = new Matrix4x4(new Vector4(0f, 0f, 0f, 0f), new Vector4(0f, 0f, 0f, 0f), new Vector4(0f, 0f, 0f, 0f), new Vector4(0f, 0f, 0f, 0f));

		private static readonly Matrix4x4 identityMatrix = new Matrix4x4(new Vector4(1f, 0f, 0f, 0f), new Vector4(0f, 1f, 0f, 0f), new Vector4(0f, 0f, 1f, 0f), new Vector4(0f, 0f, 0f, 1f));

		/// <summary>
		///   <para>The inverse of this matrix (Read Only).</para>
		/// </summary>
		public Matrix4x4 inverse => Inverse(this);

		/// <summary>
		///   <para>Returns the transpose of this matrix (Read Only).</para>
		/// </summary>
		public Matrix4x4 transpose => Transpose(this);

		/// <summary>
		///   <para>Attempts to get a rotation quaternion from this matrix.</para>
		/// </summary>
		public Quaternion rotation
		{
			get
			{
				INTERNAL_get_rotation(out Quaternion value);
				return value;
			}
		}

		/// <summary>
		///   <para>Attempts to get a scale value from the matrix.</para>
		/// </summary>
		public Vector3 lossyScale
		{
			get
			{
				INTERNAL_get_lossyScale(out Vector3 value);
				return value;
			}
		}

		/// <summary>
		///   <para>Is this the identity matrix?</para>
		/// </summary>
		public bool isIdentity
		{
			[MethodImpl(MethodImplOptions.InternalCall)]
			[GeneratedByOldBindingsGenerator]
			get;
		}

		/// <summary>
		///   <para>The determinant of the matrix.</para>
		/// </summary>
		public float determinant => Determinant(this);

		/// <summary>
		///   <para>This property takes a projection matrix and returns the six plane coordinates that define a projection frustum.</para>
		/// </summary>
		public FrustumPlanes decomposeProjection
		{
			get
			{
				INTERNAL_get_decomposeProjection(out FrustumPlanes value);
				return value;
			}
		}

		public float this[int row, int column]
		{
			get
			{
				return this[row + column * 4];
			}
			set
			{
				this[row + column * 4] = value;
			}
		}

		public float this[int index]
		{
			get
			{
				switch (index)
				{
				case 0:
					return m00;
				case 1:
					return m10;
				case 2:
					return m20;
				case 3:
					return m30;
				case 4:
					return m01;
				case 5:
					return m11;
				case 6:
					return m21;
				case 7:
					return m31;
				case 8:
					return m02;
				case 9:
					return m12;
				case 10:
					return m22;
				case 11:
					return m32;
				case 12:
					return m03;
				case 13:
					return m13;
				case 14:
					return m23;
				case 15:
					return m33;
				default:
					throw new IndexOutOfRangeException("Invalid matrix index!");
				}
			}
			set
			{
				switch (index)
				{
				case 0:
					m00 = value;
					break;
				case 1:
					m10 = value;
					break;
				case 2:
					m20 = value;
					break;
				case 3:
					m30 = value;
					break;
				case 4:
					m01 = value;
					break;
				case 5:
					m11 = value;
					break;
				case 6:
					m21 = value;
					break;
				case 7:
					m31 = value;
					break;
				case 8:
					m02 = value;
					break;
				case 9:
					m12 = value;
					break;
				case 10:
					m22 = value;
					break;
				case 11:
					m32 = value;
					break;
				case 12:
					m03 = value;
					break;
				case 13:
					m13 = value;
					break;
				case 14:
					m23 = value;
					break;
				case 15:
					m33 = value;
					break;
				default:
					throw new IndexOutOfRangeException("Invalid matrix index!");
				}
			}
		}

		/// <summary>
		///   <para>Returns a matrix with all elements set to zero (Read Only).</para>
		/// </summary>
		public static Matrix4x4 zero => zeroMatrix;

		/// <summary>
		///   <para>Returns the identity matrix (Read Only).</para>
		/// </summary>
		public static Matrix4x4 identity => identityMatrix;

		public Matrix4x4(Vector4 column0, Vector4 column1, Vector4 column2, Vector4 column3)
		{
			m00 = column0.x;
			m01 = column1.x;
			m02 = column2.x;
			m03 = column3.x;
			m10 = column0.y;
			m11 = column1.y;
			m12 = column2.y;
			m13 = column3.y;
			m20 = column0.z;
			m21 = column1.z;
			m22 = column2.z;
			m23 = column3.z;
			m30 = column0.w;
			m31 = column1.w;
			m32 = column2.w;
			m33 = column3.w;
		}

		[ThreadAndSerializationSafe]
		public static Matrix4x4 Inverse(Matrix4x4 m)
		{
			INTERNAL_CALL_Inverse(ref m, out Matrix4x4 value);
			return value;
		}

		[MethodImpl(MethodImplOptions.InternalCall)]
		[GeneratedByOldBindingsGenerator]
		private static extern void INTERNAL_CALL_Inverse(ref Matrix4x4 m, out Matrix4x4 value);

		public static Matrix4x4 Transpose(Matrix4x4 m)
		{
			INTERNAL_CALL_Transpose(ref m, out Matrix4x4 value);
			return value;
		}

		[MethodImpl(MethodImplOptions.InternalCall)]
		[GeneratedByOldBindingsGenerator]
		private static extern void INTERNAL_CALL_Transpose(ref Matrix4x4 m, out Matrix4x4 value);

		internal static bool Invert(Matrix4x4 inMatrix, out Matrix4x4 dest)
		{
			return INTERNAL_CALL_Invert(ref inMatrix, out dest);
		}

		[MethodImpl(MethodImplOptions.InternalCall)]
		[GeneratedByOldBindingsGenerator]
		private static extern bool INTERNAL_CALL_Invert(ref Matrix4x4 inMatrix, out Matrix4x4 dest);

		[MethodImpl(MethodImplOptions.InternalCall)]
		[GeneratedByOldBindingsGenerator]
		private extern void INTERNAL_get_rotation(out Quaternion value);

		[MethodImpl(MethodImplOptions.InternalCall)]
		[GeneratedByOldBindingsGenerator]
		private extern void INTERNAL_get_lossyScale(out Vector3 value);

		/// <summary>
		///   <para>Checks if this matrix is a valid transform matrix.</para>
		/// </summary>
		[MethodImpl(MethodImplOptions.InternalCall)]
		[GeneratedByOldBindingsGenerator]
		public extern bool ValidTRS();

		public static float Determinant(Matrix4x4 m)
		{
			return INTERNAL_CALL_Determinant(ref m);
		}

		[MethodImpl(MethodImplOptions.InternalCall)]
		[GeneratedByOldBindingsGenerator]
		private static extern float INTERNAL_CALL_Determinant(ref Matrix4x4 m);

		/// <summary>
		///   <para>Sets this matrix to a translation, rotation and scaling matrix.</para>
		/// </summary>
		/// <param name="pos"></param>
		/// <param name="q"></param>
		/// <param name="s"></param>
		public void SetTRS(Vector3 pos, Quaternion q, Vector3 s)
		{
			this = TRS(pos, q, s);
		}

		/// <summary>
		///   <para>Creates a translation, rotation and scaling matrix.</para>
		/// </summary>
		/// <param name="pos"></param>
		/// <param name="q"></param>
		/// <param name="s"></param>
		public static Matrix4x4 TRS(Vector3 pos, Quaternion q, Vector3 s)
		{
			INTERNAL_CALL_TRS(ref pos, ref q, ref s, out Matrix4x4 value);
			return value;
		}

		[MethodImpl(MethodImplOptions.InternalCall)]
		[GeneratedByOldBindingsGenerator]
		private static extern void INTERNAL_CALL_TRS(ref Vector3 pos, ref Quaternion q, ref Vector3 s, out Matrix4x4 value);

		/// <summary>
		///   <para>Creates an orthogonal projection matrix.</para>
		/// </summary>
		/// <param name="left"></param>
		/// <param name="right"></param>
		/// <param name="bottom"></param>
		/// <param name="top"></param>
		/// <param name="zNear"></param>
		/// <param name="zFar"></param>
		public static Matrix4x4 Ortho(float left, float right, float bottom, float top, float zNear, float zFar)
		{
			INTERNAL_CALL_Ortho(left, right, bottom, top, zNear, zFar, out Matrix4x4 value);
			return value;
		}

		[MethodImpl(MethodImplOptions.InternalCall)]
		[GeneratedByOldBindingsGenerator]
		private static extern void INTERNAL_CALL_Ortho(float left, float right, float bottom, float top, float zNear, float zFar, out Matrix4x4 value);

		/// <summary>
		///   <para>Creates a perspective projection matrix.</para>
		/// </summary>
		/// <param name="fov"></param>
		/// <param name="aspect"></param>
		/// <param name="zNear"></param>
		/// <param name="zFar"></param>
		public static Matrix4x4 Perspective(float fov, float aspect, float zNear, float zFar)
		{
			INTERNAL_CALL_Perspective(fov, aspect, zNear, zFar, out Matrix4x4 value);
			return value;
		}

		[MethodImpl(MethodImplOptions.InternalCall)]
		[GeneratedByOldBindingsGenerator]
		private static extern void INTERNAL_CALL_Perspective(float fov, float aspect, float zNear, float zFar, out Matrix4x4 value);

		/// <summary>
		///   <para>Given a source point, a target point, and an up vector, computes a transformation matrix that corresponds to a camera viewing the target from the source, such that the right-hand vector is perpendicular to the up vector.</para>
		/// </summary>
		/// <param name="from">The source point.</param>
		/// <param name="to">The target point.</param>
		/// <param name="up">The vector describing the up direction (typically Vector3.up).</param>
		/// <returns>
		///   <para>The resulting transformation matrix.</para>
		/// </returns>
		public static Matrix4x4 LookAt(Vector3 from, Vector3 to, Vector3 up)
		{
			INTERNAL_CALL_LookAt(ref from, ref to, ref up, out Matrix4x4 value);
			return value;
		}

		[MethodImpl(MethodImplOptions.InternalCall)]
		[GeneratedByOldBindingsGenerator]
		private static extern void INTERNAL_CALL_LookAt(ref Vector3 from, ref Vector3 to, ref Vector3 up, out Matrix4x4 value);

		[MethodImpl(MethodImplOptions.InternalCall)]
		[GeneratedByOldBindingsGenerator]
		private extern void INTERNAL_get_decomposeProjection(out FrustumPlanes value);

		/// <summary>
		///   <para>This function returns a projection matrix with viewing frustum that has a near plane defined by the coordinates that were passed in.</para>
		/// </summary>
		/// <param name="left">The X coordinate of the left side of the near projection plane in view space.</param>
		/// <param name="right">The X coordinate of the right side of the near projection plane in view space.</param>
		/// <param name="bottom">The Y coordinate of the bottom side of the near projection plane in view space.</param>
		/// <param name="top">The Y coordinate of the top side of the near projection plane in view space.</param>
		/// <param name="zNear">Z distance to the near plane from the origin in view space.</param>
		/// <param name="zFar">Z distance to the far plane from the origin in view space.</param>
		/// <param name="frustumPlanes">Frustum planes struct that contains the view space coordinates of that define a viewing frustum.</param>
		/// <returns>
		///   <para>A projection matrix with a viewing frustum defined by the plane coordinates passed in.</para>
		/// </returns>
		public static Matrix4x4 Frustum(float left, float right, float bottom, float top, float zNear, float zFar)
		{
			return FrustumInternal(left, right, bottom, top, zNear, zFar);
		}

		/// <summary>
		///   <para>This function returns a projection matrix with viewing frustum that has a near plane defined by the coordinates that were passed in.</para>
		/// </summary>
		/// <param name="left">The X coordinate of the left side of the near projection plane in view space.</param>
		/// <param name="right">The X coordinate of the right side of the near projection plane in view space.</param>
		/// <param name="bottom">The Y coordinate of the bottom side of the near projection plane in view space.</param>
		/// <param name="top">The Y coordinate of the top side of the near projection plane in view space.</param>
		/// <param name="zNear">Z distance to the near plane from the origin in view space.</param>
		/// <param name="zFar">Z distance to the far plane from the origin in view space.</param>
		/// <param name="frustumPlanes">Frustum planes struct that contains the view space coordinates of that define a viewing frustum.</param>
		/// <returns>
		///   <para>A projection matrix with a viewing frustum defined by the plane coordinates passed in.</para>
		/// </returns>
		public static Matrix4x4 Frustum(FrustumPlanes frustumPlanes)
		{
			return FrustumInternal(frustumPlanes.left, frustumPlanes.right, frustumPlanes.bottom, frustumPlanes.top, frustumPlanes.zNear, frustumPlanes.zFar);
		}

		private static Matrix4x4 FrustumInternal(float left, float right, float bottom, float top, float zNear, float zFar)
		{
			INTERNAL_CALL_FrustumInternal(left, right, bottom, top, zNear, zFar, out Matrix4x4 value);
			return value;
		}

		[MethodImpl(MethodImplOptions.InternalCall)]
		[GeneratedByOldBindingsGenerator]
		private static extern void INTERNAL_CALL_FrustumInternal(float left, float right, float bottom, float top, float zNear, float zFar, out Matrix4x4 value);

		public override int GetHashCode()
		{
			return GetColumn(0).GetHashCode() ^ (GetColumn(1).GetHashCode() << 2) ^ (GetColumn(2).GetHashCode() >> 2) ^ (GetColumn(3).GetHashCode() >> 1);
		}

		public override bool Equals(object other)
		{
			if (!(other is Matrix4x4))
			{
				return false;
			}
			Matrix4x4 matrix4x = (Matrix4x4)other;
			return GetColumn(0).Equals(matrix4x.GetColumn(0)) && GetColumn(1).Equals(matrix4x.GetColumn(1)) && GetColumn(2).Equals(matrix4x.GetColumn(2)) && GetColumn(3).Equals(matrix4x.GetColumn(3));
		}

		public static Matrix4x4 operator *(Matrix4x4 lhs, Matrix4x4 rhs)
		{
			Matrix4x4 result = default(Matrix4x4);
			result.m00 = lhs.m00 * rhs.m00 + lhs.m01 * rhs.m10 + lhs.m02 * rhs.m20 + lhs.m03 * rhs.m30;
			result.m01 = lhs.m00 * rhs.m01 + lhs.m01 * rhs.m11 + lhs.m02 * rhs.m21 + lhs.m03 * rhs.m31;
			result.m02 = lhs.m00 * rhs.m02 + lhs.m01 * rhs.m12 + lhs.m02 * rhs.m22 + lhs.m03 * rhs.m32;
			result.m03 = lhs.m00 * rhs.m03 + lhs.m01 * rhs.m13 + lhs.m02 * rhs.m23 + lhs.m03 * rhs.m33;
			result.m10 = lhs.m10 * rhs.m00 + lhs.m11 * rhs.m10 + lhs.m12 * rhs.m20 + lhs.m13 * rhs.m30;
			result.m11 = lhs.m10 * rhs.m01 + lhs.m11 * rhs.m11 + lhs.m12 * rhs.m21 + lhs.m13 * rhs.m31;
			result.m12 = lhs.m10 * rhs.m02 + lhs.m11 * rhs.m12 + lhs.m12 * rhs.m22 + lhs.m13 * rhs.m32;
			result.m13 = lhs.m10 * rhs.m03 + lhs.m11 * rhs.m13 + lhs.m12 * rhs.m23 + lhs.m13 * rhs.m33;
			result.m20 = lhs.m20 * rhs.m00 + lhs.m21 * rhs.m10 + lhs.m22 * rhs.m20 + lhs.m23 * rhs.m30;
			result.m21 = lhs.m20 * rhs.m01 + lhs.m21 * rhs.m11 + lhs.m22 * rhs.m21 + lhs.m23 * rhs.m31;
			result.m22 = lhs.m20 * rhs.m02 + lhs.m21 * rhs.m12 + lhs.m22 * rhs.m22 + lhs.m23 * rhs.m32;
			result.m23 = lhs.m20 * rhs.m03 + lhs.m21 * rhs.m13 + lhs.m22 * rhs.m23 + lhs.m23 * rhs.m33;
			result.m30 = lhs.m30 * rhs.m00 + lhs.m31 * rhs.m10 + lhs.m32 * rhs.m20 + lhs.m33 * rhs.m30;
			result.m31 = lhs.m30 * rhs.m01 + lhs.m31 * rhs.m11 + lhs.m32 * rhs.m21 + lhs.m33 * rhs.m31;
			result.m32 = lhs.m30 * rhs.m02 + lhs.m31 * rhs.m12 + lhs.m32 * rhs.m22 + lhs.m33 * rhs.m32;
			result.m33 = lhs.m30 * rhs.m03 + lhs.m31 * rhs.m13 + lhs.m32 * rhs.m23 + lhs.m33 * rhs.m33;
			return result;
		}

		public static Vector4 operator *(Matrix4x4 lhs, Vector4 vector)
		{
			Vector4 result = default(Vector4);
			result.x = lhs.m00 * vector.x + lhs.m01 * vector.y + lhs.m02 * vector.z + lhs.m03 * vector.w;
			result.y = lhs.m10 * vector.x + lhs.m11 * vector.y + lhs.m12 * vector.z + lhs.m13 * vector.w;
			result.z = lhs.m20 * vector.x + lhs.m21 * vector.y + lhs.m22 * vector.z + lhs.m23 * vector.w;
			result.w = lhs.m30 * vector.x + lhs.m31 * vector.y + lhs.m32 * vector.z + lhs.m33 * vector.w;
			return result;
		}

		public static bool operator ==(Matrix4x4 lhs, Matrix4x4 rhs)
		{
			return lhs.GetColumn(0) == rhs.GetColumn(0) && lhs.GetColumn(1) == rhs.GetColumn(1) && lhs.GetColumn(2) == rhs.GetColumn(2) && lhs.GetColumn(3) == rhs.GetColumn(3);
		}

		public static bool operator !=(Matrix4x4 lhs, Matrix4x4 rhs)
		{
			return !(lhs == rhs);
		}

		/// <summary>
		///   <para>Get a column of the matrix.</para>
		/// </summary>
		/// <param name="index"></param>
		public Vector4 GetColumn(int index)
		{
			switch (index)
			{
			case 0:
				return new Vector4(m00, m10, m20, m30);
			case 1:
				return new Vector4(m01, m11, m21, m31);
			case 2:
				return new Vector4(m02, m12, m22, m32);
			case 3:
				return new Vector4(m03, m13, m23, m33);
			default:
				throw new IndexOutOfRangeException("Invalid column index!");
			}
		}

		/// <summary>
		///   <para>Returns a row of the matrix.</para>
		/// </summary>
		/// <param name="index"></param>
		public Vector4 GetRow(int index)
		{
			switch (index)
			{
			case 0:
				return new Vector4(m00, m01, m02, m03);
			case 1:
				return new Vector4(m10, m11, m12, m13);
			case 2:
				return new Vector4(m20, m21, m22, m23);
			case 3:
				return new Vector4(m30, m31, m32, m33);
			default:
				throw new IndexOutOfRangeException("Invalid row index!");
			}
		}

		/// <summary>
		///   <para>Sets a column of the matrix.</para>
		/// </summary>
		/// <param name="index"></param>
		/// <param name="column"></param>
		public void SetColumn(int index, Vector4 column)
		{
			this[0, index] = column.x;
			this[1, index] = column.y;
			this[2, index] = column.z;
			this[3, index] = column.w;
		}

		/// <summary>
		///   <para>Sets a row of the matrix.</para>
		/// </summary>
		/// <param name="index"></param>
		/// <param name="row"></param>
		public void SetRow(int index, Vector4 row)
		{
			this[index, 0] = row.x;
			this[index, 1] = row.y;
			this[index, 2] = row.z;
			this[index, 3] = row.w;
		}

		/// <summary>
		///   <para>Transforms a position by this matrix (generic).</para>
		/// </summary>
		/// <param name="point"></param>
		public Vector3 MultiplyPoint(Vector3 point)
		{
			Vector3 result = default(Vector3);
			result.x = m00 * point.x + m01 * point.y + m02 * point.z + m03;
			result.y = m10 * point.x + m11 * point.y + m12 * point.z + m13;
			result.z = m20 * point.x + m21 * point.y + m22 * point.z + m23;
			float num = m30 * point.x + m31 * point.y + m32 * point.z + m33;
			num = 1f / num;
			result.x *= num;
			result.y *= num;
			result.z *= num;
			return result;
		}

		/// <summary>
		///   <para>Transforms a position by this matrix (fast).</para>
		/// </summary>
		/// <param name="point"></param>
		public Vector3 MultiplyPoint3x4(Vector3 point)
		{
			Vector3 result = default(Vector3);
			result.x = m00 * point.x + m01 * point.y + m02 * point.z + m03;
			result.y = m10 * point.x + m11 * point.y + m12 * point.z + m13;
			result.z = m20 * point.x + m21 * point.y + m22 * point.z + m23;
			return result;
		}

		/// <summary>
		///   <para>Transforms a direction by this matrix.</para>
		/// </summary>
		/// <param name="vector"></param>
		public Vector3 MultiplyVector(Vector3 vector)
		{
			Vector3 result = default(Vector3);
			result.x = m00 * vector.x + m01 * vector.y + m02 * vector.z;
			result.y = m10 * vector.x + m11 * vector.y + m12 * vector.z;
			result.z = m20 * vector.x + m21 * vector.y + m22 * vector.z;
			return result;
		}

		/// <summary>
		///   <para>Returns a plane that is transformed in space.</para>
		/// </summary>
		/// <param name="plane"></param>
		public Plane TransformPlane(Plane plane)
		{
			Matrix4x4 inverse = this.inverse;
			Vector3 normal = plane.normal;
			float x = normal.x;
			Vector3 normal2 = plane.normal;
			float y = normal2.y;
			Vector3 normal3 = plane.normal;
			float z = normal3.z;
			float distance = plane.distance;
			float x2 = inverse.m00 * x + inverse.m10 * y + inverse.m20 * z + inverse.m30 * distance;
			float y2 = inverse.m01 * x + inverse.m11 * y + inverse.m21 * z + inverse.m31 * distance;
			float z2 = inverse.m02 * x + inverse.m12 * y + inverse.m22 * z + inverse.m32 * distance;
			float d = inverse.m03 * x + inverse.m13 * y + inverse.m23 * z + inverse.m33 * distance;
			return new Plane(new Vector3(x2, y2, z2), d);
		}

		/// <summary>
		///   <para>Creates a scaling matrix.</para>
		/// </summary>
		/// <param name="vector"></param>
		public static Matrix4x4 Scale(Vector3 vector)
		{
			Matrix4x4 result = default(Matrix4x4);
			result.m00 = vector.x;
			result.m01 = 0f;
			result.m02 = 0f;
			result.m03 = 0f;
			result.m10 = 0f;
			result.m11 = vector.y;
			result.m12 = 0f;
			result.m13 = 0f;
			result.m20 = 0f;
			result.m21 = 0f;
			result.m22 = vector.z;
			result.m23 = 0f;
			result.m30 = 0f;
			result.m31 = 0f;
			result.m32 = 0f;
			result.m33 = 1f;
			return result;
		}

		/// <summary>
		///   <para>Creates a translation matrix.</para>
		/// </summary>
		/// <param name="vector"></param>
		public static Matrix4x4 Translate(Vector3 vector)
		{
			Matrix4x4 result = default(Matrix4x4);
			result.m00 = 1f;
			result.m01 = 0f;
			result.m02 = 0f;
			result.m03 = vector.x;
			result.m10 = 0f;
			result.m11 = 1f;
			result.m12 = 0f;
			result.m13 = vector.y;
			result.m20 = 0f;
			result.m21 = 0f;
			result.m22 = 1f;
			result.m23 = vector.z;
			result.m30 = 0f;
			result.m31 = 0f;
			result.m32 = 0f;
			result.m33 = 1f;
			return result;
		}

		/// <summary>
		///   <para>Creates a rotation matrix.</para>
		/// </summary>
		/// <param name="q"></param>
		public static Matrix4x4 Rotate(Quaternion q)
		{
			float num = q.x * 2f;
			float num2 = q.y * 2f;
			float num3 = q.z * 2f;
			float num4 = q.x * num;
			float num5 = q.y * num2;
			float num6 = q.z * num3;
			float num7 = q.x * num2;
			float num8 = q.x * num3;
			float num9 = q.y * num3;
			float num10 = q.w * num;
			float num11 = q.w * num2;
			float num12 = q.w * num3;
			Matrix4x4 result = default(Matrix4x4);
			result.m00 = 1f - (num5 + num6);
			result.m10 = num7 + num12;
			result.m20 = num8 - num11;
			result.m30 = 0f;
			result.m01 = num7 - num12;
			result.m11 = 1f - (num4 + num6);
			result.m21 = num9 + num10;
			result.m31 = 0f;
			result.m02 = num8 + num11;
			result.m12 = num9 - num10;
			result.m22 = 1f - (num4 + num5);
			result.m32 = 0f;
			result.m03 = 0f;
			result.m13 = 0f;
			result.m23 = 0f;
			result.m33 = 1f;
			return result;
		}

		/// <summary>
		///   <para>Returns a nicely formatted string for this matrix.</para>
		/// </summary>
		/// <param name="format"></param>
		public override string ToString()
		{
			return UnityString.Format("{0:F5}\t{1:F5}\t{2:F5}\t{3:F5}\n{4:F5}\t{5:F5}\t{6:F5}\t{7:F5}\n{8:F5}\t{9:F5}\t{10:F5}\t{11:F5}\n{12:F5}\t{13:F5}\t{14:F5}\t{15:F5}\n", m00, m01, m02, m03, m10, m11, m12, m13, m20, m21, m22, m23, m30, m31, m32, m33);
		}

		/// <summary>
		///   <para>Returns a nicely formatted string for this matrix.</para>
		/// </summary>
		/// <param name="format"></param>
		public string ToString(string format)
		{
			return UnityString.Format("{0}\t{1}\t{2}\t{3}\n{4}\t{5}\t{6}\t{7}\n{8}\t{9}\t{10}\t{11}\n{12}\t{13}\t{14}\t{15}\n", m00.ToString(format), m01.ToString(format), m02.ToString(format), m03.ToString(format), m10.ToString(format), m11.ToString(format), m12.ToString(format), m13.ToString(format), m20.ToString(format), m21.ToString(format), m22.ToString(format), m23.ToString(format), m30.ToString(format), m31.ToString(format), m32.ToString(format), m33.ToString(format));
		}
	}
}
