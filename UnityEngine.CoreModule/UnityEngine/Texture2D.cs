using System;
using System.Collections.Generic;
using System.Runtime.CompilerServices;
using UnityEngine.Internal;
using UnityEngine.Scripting;

namespace UnityEngine
{
	/// <summary>
	///   <para>Class for texture handling.</para>
	/// </summary>
	public sealed class Texture2D : Texture
	{
		/// <summary>
		///   <para>Flags used to control the encoding to an EXR file.</para>
		/// </summary>
		[Flags]
		public enum EXRFlags
		{
			/// <summary>
			///   <para>No flag. This will result in an uncompressed 16-bit float EXR file.</para>
			/// </summary>
			None = 0x0,
			/// <summary>
			///   <para>The texture will be exported as a 32-bit float EXR file (default is 16-bit).</para>
			/// </summary>
			OutputAsFloat = 0x1,
			/// <summary>
			///   <para>The texture will use the EXR ZIP compression format.</para>
			/// </summary>
			CompressZIP = 0x2,
			/// <summary>
			///   <para>The texture will use RLE (Run Length Encoding) EXR compression format (similar to Targa RLE compression).</para>
			/// </summary>
			CompressRLE = 0x4,
			/// <summary>
			///   <para>This texture will use Wavelet compression. This is best used for grainy images.</para>
			/// </summary>
			CompressPIZ = 0x8
		}

		/// <summary>
		///   <para>How many mipmap levels are in this texture (Read Only).</para>
		/// </summary>
		public int mipmapCount
		{
			[MethodImpl(MethodImplOptions.InternalCall)]
			[GeneratedByOldBindingsGenerator]
			get;
		}

		/// <summary>
		///   <para>The format of the pixel data in the texture (Read Only).</para>
		/// </summary>
		public TextureFormat format
		{
			[MethodImpl(MethodImplOptions.InternalCall)]
			[GeneratedByOldBindingsGenerator]
			get;
		}

		/// <summary>
		///   <para>Get a small texture with all white pixels.</para>
		/// </summary>
		public static Texture2D whiteTexture
		{
			[MethodImpl(MethodImplOptions.InternalCall)]
			[GeneratedByOldBindingsGenerator]
			get;
		}

		/// <summary>
		///   <para>Get a small texture with all black pixels.</para>
		/// </summary>
		public static Texture2D blackTexture
		{
			[MethodImpl(MethodImplOptions.InternalCall)]
			[GeneratedByOldBindingsGenerator]
			get;
		}

		/// <summary>
		///   <para>Create a new empty texture.</para>
		/// </summary>
		/// <param name="width"></param>
		/// <param name="height"></param>
		public Texture2D(int width, int height)
		{
			Internal_Create(this, width, height, TextureFormat.RGBA32, mipmap: true, linear: false, IntPtr.Zero);
		}

		/// <summary>
		///   <para>Create a new empty texture.</para>
		/// </summary>
		/// <param name="width"></param>
		/// <param name="height"></param>
		/// <param name="format"></param>
		/// <param name="mipmap"></param>
		public Texture2D(int width, int height, TextureFormat format, bool mipmap)
		{
			Internal_Create(this, width, height, format, mipmap, linear: false, IntPtr.Zero);
		}

		/// <summary>
		///   <para>Create a new empty texture.</para>
		/// </summary>
		/// <param name="width"></param>
		/// <param name="height"></param>
		/// <param name="format"></param>
		/// <param name="mipmap"></param>
		/// <param name="linear"></param>
		public Texture2D(int width, int height, TextureFormat format, bool mipmap, bool linear)
		{
			Internal_Create(this, width, height, format, mipmap, linear, IntPtr.Zero);
		}

		internal Texture2D(int width, int height, TextureFormat format, bool mipmap, bool linear, IntPtr nativeTex)
		{
			Internal_Create(this, width, height, format, mipmap, linear, nativeTex);
		}

		[MethodImpl(MethodImplOptions.InternalCall)]
		[GeneratedByOldBindingsGenerator]
		private static extern void Internal_Create([Writable] Texture2D mono, int width, int height, TextureFormat format, bool mipmap, bool linear, IntPtr nativeTex);

		/// <summary>
		///   <para>Creates Unity Texture out of externally created native texture object.</para>
		/// </summary>
		/// <param name="nativeTex">Native 2D texture object.</param>
		/// <param name="width">Width of texture in pixels.</param>
		/// <param name="height">Height of texture in pixels.</param>
		/// <param name="format">Format of underlying texture object.</param>
		/// <param name="mipmap">Does the texture have mipmaps?</param>
		/// <param name="linear">Is texture using linear color space?</param>
		public static Texture2D CreateExternalTexture(int width, int height, TextureFormat format, bool mipmap, bool linear, IntPtr nativeTex)
		{
			if (nativeTex == IntPtr.Zero)
			{
				throw new ArgumentException("nativeTex can not be null");
			}
			return new Texture2D(width, height, format, mipmap, linear, nativeTex);
		}

		/// <summary>
		///   <para>Updates Unity texture to use different native texture object.</para>
		/// </summary>
		/// <param name="nativeTex">Native 2D texture object.</param>
		[MethodImpl(MethodImplOptions.InternalCall)]
		[GeneratedByOldBindingsGenerator]
		public extern void UpdateExternalTexture(IntPtr nativeTex);

		/// <summary>
		///   <para>Sets pixel color at coordinates (x,y).</para>
		/// </summary>
		/// <param name="x"></param>
		/// <param name="y"></param>
		/// <param name="color"></param>
		public void SetPixel(int x, int y, Color color)
		{
			INTERNAL_CALL_SetPixel(this, x, y, ref color);
		}

		[MethodImpl(MethodImplOptions.InternalCall)]
		[GeneratedByOldBindingsGenerator]
		private static extern void INTERNAL_CALL_SetPixel(Texture2D self, int x, int y, ref Color color);

		/// <summary>
		///   <para>Returns pixel color at coordinates (x, y).</para>
		/// </summary>
		/// <param name="x"></param>
		/// <param name="y"></param>
		public Color GetPixel(int x, int y)
		{
			INTERNAL_CALL_GetPixel(this, x, y, out Color value);
			return value;
		}

		[MethodImpl(MethodImplOptions.InternalCall)]
		[GeneratedByOldBindingsGenerator]
		private static extern void INTERNAL_CALL_GetPixel(Texture2D self, int x, int y, out Color value);

		/// <summary>
		///   <para>Returns filtered pixel color at normalized coordinates (u, v).</para>
		/// </summary>
		/// <param name="u"></param>
		/// <param name="v"></param>
		public Color GetPixelBilinear(float u, float v)
		{
			INTERNAL_CALL_GetPixelBilinear(this, u, v, out Color value);
			return value;
		}

		[MethodImpl(MethodImplOptions.InternalCall)]
		[GeneratedByOldBindingsGenerator]
		private static extern void INTERNAL_CALL_GetPixelBilinear(Texture2D self, float u, float v, out Color value);

		[ExcludeFromDocs]
		public void SetPixels(Color[] colors)
		{
			int miplevel = 0;
			SetPixels(colors, miplevel);
		}

		/// <summary>
		///   <para>Set a block of pixel colors.</para>
		/// </summary>
		/// <param name="colors">The array of pixel colours to assign (a 2D image flattened to a 1D array).</param>
		/// <param name="miplevel">The mip level of the texture to write to.</param>
		public void SetPixels(Color[] colors, [DefaultValue("0")] int miplevel)
		{
			int num = width >> miplevel;
			if (num < 1)
			{
				num = 1;
			}
			int num2 = height >> miplevel;
			if (num2 < 1)
			{
				num2 = 1;
			}
			SetPixels(0, 0, num, num2, colors, miplevel);
		}

		/// <summary>
		///   <para>Set a block of pixel colors.</para>
		/// </summary>
		/// <param name="x"></param>
		/// <param name="y"></param>
		/// <param name="blockWidth"></param>
		/// <param name="blockHeight"></param>
		/// <param name="colors"></param>
		/// <param name="miplevel"></param>
		[MethodImpl(MethodImplOptions.InternalCall)]
		[GeneratedByOldBindingsGenerator]
		public extern void SetPixels(int x, int y, int blockWidth, int blockHeight, Color[] colors, [DefaultValue("0")] int miplevel);

		[ExcludeFromDocs]
		public void SetPixels(int x, int y, int blockWidth, int blockHeight, Color[] colors)
		{
			int miplevel = 0;
			SetPixels(x, y, blockWidth, blockHeight, colors, miplevel);
		}

		[MethodImpl(MethodImplOptions.InternalCall)]
		[GeneratedByOldBindingsGenerator]
		private extern void SetAllPixels32(Color32[] colors, int miplevel);

		[MethodImpl(MethodImplOptions.InternalCall)]
		[GeneratedByOldBindingsGenerator]
		private extern void SetBlockOfPixels32(int x, int y, int blockWidth, int blockHeight, Color32[] colors, int miplevel);

		[ExcludeFromDocs]
		public void SetPixels32(Color32[] colors)
		{
			int miplevel = 0;
			SetPixels32(colors, miplevel);
		}

		/// <summary>
		///   <para>Set a block of pixel colors.</para>
		/// </summary>
		/// <param name="colors"></param>
		/// <param name="miplevel"></param>
		public void SetPixels32(Color32[] colors, [DefaultValue("0")] int miplevel)
		{
			SetAllPixels32(colors, miplevel);
		}

		[ExcludeFromDocs]
		public void SetPixels32(int x, int y, int blockWidth, int blockHeight, Color32[] colors)
		{
			int miplevel = 0;
			SetPixels32(x, y, blockWidth, blockHeight, colors, miplevel);
		}

		/// <summary>
		///   <para>Set a block of pixel colors.</para>
		/// </summary>
		/// <param name="x"></param>
		/// <param name="y"></param>
		/// <param name="blockWidth"></param>
		/// <param name="blockHeight"></param>
		/// <param name="colors"></param>
		/// <param name="miplevel"></param>
		public void SetPixels32(int x, int y, int blockWidth, int blockHeight, Color32[] colors, [DefaultValue("0")] int miplevel)
		{
			SetBlockOfPixels32(x, y, blockWidth, blockHeight, colors, miplevel);
		}

		[MethodImpl(MethodImplOptions.InternalCall)]
		[GeneratedByOldBindingsGenerator]
		private extern void LoadRawTextureData_ImplArray(byte[] data);

		[MethodImpl(MethodImplOptions.InternalCall)]
		[GeneratedByOldBindingsGenerator]
		private extern void LoadRawTextureData_ImplPointer(IntPtr data, int size);

		/// <summary>
		///   <para>Fills texture pixels with raw preformatted data.</para>
		/// </summary>
		/// <param name="data">Byte array to initialize texture pixels with.</param>
		/// <param name="size">Size of data in bytes.</param>
		public void LoadRawTextureData(byte[] data)
		{
			LoadRawTextureData_ImplArray(data);
		}

		/// <summary>
		///   <para>Fills texture pixels with raw preformatted data.</para>
		/// </summary>
		/// <param name="data">Byte array to initialize texture pixels with.</param>
		/// <param name="size">Size of data in bytes.</param>
		public void LoadRawTextureData(IntPtr data, int size)
		{
			LoadRawTextureData_ImplPointer(data, size);
		}

		/// <summary>
		///   <para>Get raw data from a texture.</para>
		/// </summary>
		/// <returns>
		///   <para>Raw texture data as a byte array.</para>
		/// </returns>
		[MethodImpl(MethodImplOptions.InternalCall)]
		[GeneratedByOldBindingsGenerator]
		public extern byte[] GetRawTextureData();

		[ExcludeFromDocs]
		public Color[] GetPixels()
		{
			int miplevel = 0;
			return GetPixels(miplevel);
		}

		/// <summary>
		///   <para>Get the pixel colors from the texture.</para>
		/// </summary>
		/// <param name="miplevel">The mipmap level to fetch the pixels from. Defaults to zero.</param>
		/// <returns>
		///   <para>The array of all pixels in the mipmap level of the texture.</para>
		/// </returns>
		public Color[] GetPixels([DefaultValue("0")] int miplevel)
		{
			int num = width >> miplevel;
			if (num < 1)
			{
				num = 1;
			}
			int num2 = height >> miplevel;
			if (num2 < 1)
			{
				num2 = 1;
			}
			return GetPixels(0, 0, num, num2, miplevel);
		}

		/// <summary>
		///   <para>Get a block of pixel colors.</para>
		/// </summary>
		/// <param name="x">The x position of the pixel array to fetch.</param>
		/// <param name="y">The y position of the pixel array to fetch.</param>
		/// <param name="blockWidth">The width length of the pixel array to fetch.</param>
		/// <param name="blockHeight">The height length of the pixel array to fetch.</param>
		/// <param name="miplevel">The mipmap level to fetch the pixels. Defaults to zero, and is
		///   optional.</param>
		/// <returns>
		///   <para>The array of pixels in the texture that have been selected.</para>
		/// </returns>
		[MethodImpl(MethodImplOptions.InternalCall)]
		[GeneratedByOldBindingsGenerator]
		public extern Color[] GetPixels(int x, int y, int blockWidth, int blockHeight, [DefaultValue("0")] int miplevel);

		[ExcludeFromDocs]
		public Color[] GetPixels(int x, int y, int blockWidth, int blockHeight)
		{
			int miplevel = 0;
			return GetPixels(x, y, blockWidth, blockHeight, miplevel);
		}

		/// <summary>
		///   <para>Get a block of pixel colors in Color32 format.</para>
		/// </summary>
		/// <param name="miplevel"></param>
		[MethodImpl(MethodImplOptions.InternalCall)]
		[GeneratedByOldBindingsGenerator]
		public extern Color32[] GetPixels32([DefaultValue("0")] int miplevel);

		[ExcludeFromDocs]
		public Color32[] GetPixels32()
		{
			int miplevel = 0;
			return GetPixels32(miplevel);
		}

		/// <summary>
		///   <para>Actually apply all previous SetPixel and SetPixels changes.</para>
		/// </summary>
		/// <param name="updateMipmaps">When set to true, mipmap levels are recalculated.</param>
		/// <param name="makeNoLongerReadable">When set to true, system memory copy of a texture is released.</param>
		[MethodImpl(MethodImplOptions.InternalCall)]
		[GeneratedByOldBindingsGenerator]
		public extern void Apply([DefaultValue("true")] bool updateMipmaps, [DefaultValue("false")] bool makeNoLongerReadable);

		[ExcludeFromDocs]
		public void Apply(bool updateMipmaps)
		{
			bool makeNoLongerReadable = false;
			Apply(updateMipmaps, makeNoLongerReadable);
		}

		[ExcludeFromDocs]
		public void Apply()
		{
			bool makeNoLongerReadable = false;
			bool updateMipmaps = true;
			Apply(updateMipmaps, makeNoLongerReadable);
		}

		/// <summary>
		///   <para>Resizes the texture.</para>
		/// </summary>
		/// <param name="width"></param>
		/// <param name="height"></param>
		/// <param name="format"></param>
		/// <param name="hasMipMap"></param>
		[MethodImpl(MethodImplOptions.InternalCall)]
		[GeneratedByOldBindingsGenerator]
		public extern bool Resize(int width, int height, TextureFormat format, bool hasMipMap);

		/// <summary>
		///   <para>Resizes the texture.</para>
		/// </summary>
		/// <param name="width"></param>
		/// <param name="height"></param>
		public bool Resize(int width, int height)
		{
			return Internal_ResizeWH(width, height);
		}

		[MethodImpl(MethodImplOptions.InternalCall)]
		[GeneratedByOldBindingsGenerator]
		private extern bool Internal_ResizeWH(int width, int height);

		/// <summary>
		///   <para>Compress texture into DXT format.</para>
		/// </summary>
		/// <param name="highQuality"></param>
		public void Compress(bool highQuality)
		{
			INTERNAL_CALL_Compress(this, highQuality);
		}

		[MethodImpl(MethodImplOptions.InternalCall)]
		[GeneratedByOldBindingsGenerator]
		private static extern void INTERNAL_CALL_Compress(Texture2D self, bool highQuality);

		/// <summary>
		///   <para>Packs multiple Textures into a texture atlas.</para>
		/// </summary>
		/// <param name="textures">Array of textures to pack into the atlas.</param>
		/// <param name="padding">Padding in pixels between the packed textures.</param>
		/// <param name="maximumAtlasSize">Maximum size of the resulting texture.</param>
		/// <param name="makeNoLongerReadable">Should the texture be marked as no longer readable?</param>
		/// <returns>
		///   <para>An array of rectangles containing the UV coordinates in the atlas for each input texture, or null if packing fails.</para>
		/// </returns>
		[MethodImpl(MethodImplOptions.InternalCall)]
		[GeneratedByOldBindingsGenerator]
		public extern Rect[] PackTextures(Texture2D[] textures, int padding, [DefaultValue("2048")] int maximumAtlasSize, [DefaultValue("false")] bool makeNoLongerReadable);

		[ExcludeFromDocs]
		public Rect[] PackTextures(Texture2D[] textures, int padding, int maximumAtlasSize)
		{
			bool makeNoLongerReadable = false;
			return PackTextures(textures, padding, maximumAtlasSize, makeNoLongerReadable);
		}

		[ExcludeFromDocs]
		public Rect[] PackTextures(Texture2D[] textures, int padding)
		{
			bool makeNoLongerReadable = false;
			int maximumAtlasSize = 2048;
			return PackTextures(textures, padding, maximumAtlasSize, makeNoLongerReadable);
		}

		public static bool GenerateAtlas(Vector2[] sizes, int padding, int atlasSize, List<Rect> results)
		{
			if (sizes == null)
			{
				throw new ArgumentException("sizes array can not be null");
			}
			if (results == null)
			{
				throw new ArgumentException("results list cannot be null");
			}
			if (padding < 0)
			{
				throw new ArgumentException("padding can not be negative");
			}
			if (atlasSize <= 0)
			{
				throw new ArgumentException("atlas size must be positive");
			}
			results.Clear();
			if (sizes.Length == 0)
			{
				return true;
			}
			GenerateAtlasInternal(sizes, padding, atlasSize, results);
			return results.Count != 0;
		}

		[MethodImpl(MethodImplOptions.InternalCall)]
		[GeneratedByOldBindingsGenerator]
		private static extern void GenerateAtlasInternal(Vector2[] sizes, int padding, int atlasSize, object resultList);

		/// <summary>
		///   <para>Read pixels from screen into the saved texture data.</para>
		/// </summary>
		/// <param name="source">Rectangular region of the view to read from. Pixels are read from current render target.</param>
		/// <param name="destX">Horizontal pixel position in the texture to place the pixels that are read.</param>
		/// <param name="destY">Vertical pixel position in the texture to place the pixels that are read.</param>
		/// <param name="recalculateMipMaps">Should the texture's mipmaps be recalculated after reading?</param>
		public void ReadPixels(Rect source, int destX, int destY, [DefaultValue("true")] bool recalculateMipMaps)
		{
			INTERNAL_CALL_ReadPixels(this, ref source, destX, destY, recalculateMipMaps);
		}

		[ExcludeFromDocs]
		public void ReadPixels(Rect source, int destX, int destY)
		{
			bool recalculateMipMaps = true;
			INTERNAL_CALL_ReadPixels(this, ref source, destX, destY, recalculateMipMaps);
		}

		[MethodImpl(MethodImplOptions.InternalCall)]
		[GeneratedByOldBindingsGenerator]
		private static extern void INTERNAL_CALL_ReadPixels(Texture2D self, ref Rect source, int destX, int destY, bool recalculateMipMaps);
	}
}
