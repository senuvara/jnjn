using System;

namespace UnityEngine
{
	/// <summary>
	///   <para>Substance memory budget.</para>
	/// </summary>
	[Obsolete("Built-in support for Substance Designer materials has been deprecated and will be removed in Unity 2018.1. To continue using Substance Designer materials in Unity 2018.1, you will need to install Allegorithmic's external importer from the Asset Store.", false)]
	public enum ProceduralCacheSize
	{
		/// <summary>
		///   <para>A limit of 128MB for the cache or the working memory.</para>
		/// </summary>
		Tiny,
		/// <summary>
		///   <para>A limit of 256MB for the cache or the working memory.</para>
		/// </summary>
		Medium,
		/// <summary>
		///   <para>A limit of 512MB for the cache or the working memory.</para>
		/// </summary>
		Heavy,
		/// <summary>
		///   <para>No limit for the cache or the working memory.</para>
		/// </summary>
		NoLimit,
		/// <summary>
		///   <para>A limit of 1B (one byte) for the cache or the working memory.</para>
		/// </summary>
		None
	}
}
