using System.Runtime.CompilerServices;
using UnityEngine.Scripting;

namespace UnityEngine
{
	/// <summary>
	///   <para>Text file assets.</para>
	/// </summary>
	public class TextAsset : Object
	{
		/// <summary>
		///   <para>The text contents of the .txt file as a string. (Read Only)</para>
		/// </summary>
		public string text
		{
			[MethodImpl(MethodImplOptions.InternalCall)]
			[GeneratedByOldBindingsGenerator]
			get;
		}

		/// <summary>
		///   <para>The raw bytes of the text asset. (Read Only)</para>
		/// </summary>
		public byte[] bytes
		{
			[MethodImpl(MethodImplOptions.InternalCall)]
			[GeneratedByOldBindingsGenerator]
			get;
		}

		public TextAsset()
		{
			Internal_CreateTextAsset(this);
		}

		/// <summary>
		///   <para>Returns the contents of the TextAsset.</para>
		/// </summary>
		public override string ToString()
		{
			return text;
		}

		[MethodImpl(MethodImplOptions.InternalCall)]
		[GeneratedByOldBindingsGenerator]
		private static extern void Internal_CreateTextAsset([Writable] TextAsset mono);
	}
}
