using System;
using System.Runtime.CompilerServices;
using UnityEngine.Bindings;
using UnityEngine.Internal;
using UnityEngine.Rendering;
using UnityEngine.SceneManagement;
using UnityEngine.Scripting;

namespace UnityEngine
{
	/// <summary>
	///   <para>A Camera is a device through which the player views the world.</para>
	/// </summary>
	[NativeHeader("Runtime/Camera/Camera.h")]
	[NativeHeader("Runtime/GfxDevice/GfxDeviceTypes.h")]
	[RequireComponent(typeof(Transform))]
	[UsedByNativeCode]
	public sealed class Camera : Behaviour
	{
		/// <summary>
		///   <para>Enum used to specify either the left or the right eye of a stereoscopic camera.</para>
		/// </summary>
		public enum StereoscopicEye
		{
			/// <summary>
			///   <para>Specifies the target to be the left eye.</para>
			/// </summary>
			Left,
			/// <summary>
			///   <para>Specifies the target to be the right eye.</para>
			/// </summary>
			Right
		}

		/// <summary>
		///   <para>A Camera eye corresponding to the left or right human eye for stereoscopic rendering, or neither for non-stereoscopic rendering.
		///
		/// A single Camera can render both left and right views in a single frame. Therefore, this enum describes which eye the Camera is currently rendering when returned by Camera.stereoActiveEye during a rendering callback (such as Camera.OnRenderImage), or which eye to act on when passed into a function.
		///
		/// The default value is Camera.MonoOrStereoscopicEye.Left, so Camera.MonoOrStereoscopicEye.Left may be returned by some methods or properties when called outside of rendering if stereoscopic rendering is enabled.</para>
		/// </summary>
		public enum MonoOrStereoscopicEye
		{
			/// <summary>
			///   <para>Camera eye corresponding to stereoscopic rendering of the left eye.</para>
			/// </summary>
			Left,
			/// <summary>
			///   <para>Camera eye corresponding to stereoscopic rendering of the right eye.</para>
			/// </summary>
			Right,
			/// <summary>
			///   <para>Camera eye corresponding to non-stereoscopic rendering.</para>
			/// </summary>
			Mono
		}

		/// <summary>
		///   <para>Delegate type for camera callbacks.</para>
		/// </summary>
		/// <param name="cam"></param>
		public delegate void CameraCallback(Camera cam);

		/// <summary>
		///   <para>Event that is fired before any camera starts culling.</para>
		/// </summary>
		public static CameraCallback onPreCull;

		/// <summary>
		///   <para>Event that is fired before any camera starts rendering.</para>
		/// </summary>
		public static CameraCallback onPreRender;

		/// <summary>
		///   <para>Event that is fired after any camera finishes rendering.</para>
		/// </summary>
		public static CameraCallback onPostRender;

		[Obsolete("use Camera.fieldOfView instead.")]
		public float fov
		{
			get
			{
				return fieldOfView;
			}
			set
			{
				fieldOfView = value;
			}
		}

		[Obsolete("use Camera.nearClipPlane instead.")]
		public float near
		{
			get
			{
				return nearClipPlane;
			}
			set
			{
				nearClipPlane = value;
			}
		}

		[Obsolete("use Camera.farClipPlane instead.")]
		public float far
		{
			get
			{
				return farClipPlane;
			}
			set
			{
				farClipPlane = value;
			}
		}

		/// <summary>
		///   <para>The field of view of the camera in degrees.</para>
		/// </summary>
		public float fieldOfView
		{
			[MethodImpl(MethodImplOptions.InternalCall)]
			[GeneratedByOldBindingsGenerator]
			get;
			[MethodImpl(MethodImplOptions.InternalCall)]
			[GeneratedByOldBindingsGenerator]
			set;
		}

		/// <summary>
		///   <para>The near clipping plane distance.</para>
		/// </summary>
		public float nearClipPlane
		{
			[MethodImpl(MethodImplOptions.InternalCall)]
			[GeneratedByOldBindingsGenerator]
			get;
			[MethodImpl(MethodImplOptions.InternalCall)]
			[GeneratedByOldBindingsGenerator]
			set;
		}

		/// <summary>
		///   <para>The far clipping plane distance.</para>
		/// </summary>
		public float farClipPlane
		{
			[MethodImpl(MethodImplOptions.InternalCall)]
			[GeneratedByOldBindingsGenerator]
			get;
			[MethodImpl(MethodImplOptions.InternalCall)]
			[GeneratedByOldBindingsGenerator]
			set;
		}

		/// <summary>
		///   <para>The rendering path that should be used, if possible.</para>
		/// </summary>
		public RenderingPath renderingPath
		{
			[MethodImpl(MethodImplOptions.InternalCall)]
			[GeneratedByOldBindingsGenerator]
			get;
			[MethodImpl(MethodImplOptions.InternalCall)]
			[GeneratedByOldBindingsGenerator]
			set;
		}

		/// <summary>
		///   <para>The rendering path that is currently being used (Read Only).</para>
		/// </summary>
		public RenderingPath actualRenderingPath
		{
			[MethodImpl(MethodImplOptions.InternalCall)]
			[GeneratedByOldBindingsGenerator]
			get;
		}

		/// <summary>
		///   <para>High dynamic range rendering.</para>
		/// </summary>
		public bool allowHDR
		{
			[MethodImpl(MethodImplOptions.InternalCall)]
			[GeneratedByOldBindingsGenerator]
			get;
			[MethodImpl(MethodImplOptions.InternalCall)]
			[GeneratedByOldBindingsGenerator]
			set;
		}

		/// <summary>
		///   <para>High dynamic range rendering.</para>
		/// </summary>
		[Obsolete("use Camera.allowHDR instead.")]
		public bool hdr
		{
			[MethodImpl(MethodImplOptions.InternalCall)]
			[GeneratedByOldBindingsGenerator]
			get;
			[MethodImpl(MethodImplOptions.InternalCall)]
			[GeneratedByOldBindingsGenerator]
			set;
		}

		/// <summary>
		///   <para>Should camera rendering be forced into a RenderTexture.</para>
		/// </summary>
		public bool forceIntoRenderTexture
		{
			[MethodImpl(MethodImplOptions.InternalCall)]
			[GeneratedByOldBindingsGenerator]
			get;
			[MethodImpl(MethodImplOptions.InternalCall)]
			[GeneratedByOldBindingsGenerator]
			set;
		}

		/// <summary>
		///   <para>MSAA rendering.</para>
		/// </summary>
		public bool allowMSAA
		{
			[MethodImpl(MethodImplOptions.InternalCall)]
			[GeneratedByOldBindingsGenerator]
			get;
			[MethodImpl(MethodImplOptions.InternalCall)]
			[GeneratedByOldBindingsGenerator]
			set;
		}

		/// <summary>
		///   <para>Dynamic Resolution Scaling.</para>
		/// </summary>
		public bool allowDynamicResolution
		{
			[MethodImpl(MethodImplOptions.InternalCall)]
			[GeneratedByOldBindingsGenerator]
			get;
			[MethodImpl(MethodImplOptions.InternalCall)]
			[GeneratedByOldBindingsGenerator]
			set;
		}

		/// <summary>
		///   <para>Camera's half-size when in orthographic mode.</para>
		/// </summary>
		public float orthographicSize
		{
			[MethodImpl(MethodImplOptions.InternalCall)]
			[GeneratedByOldBindingsGenerator]
			get;
			[MethodImpl(MethodImplOptions.InternalCall)]
			[GeneratedByOldBindingsGenerator]
			set;
		}

		/// <summary>
		///   <para>Is the camera orthographic (true) or perspective (false)?</para>
		/// </summary>
		public bool orthographic
		{
			[MethodImpl(MethodImplOptions.InternalCall)]
			[GeneratedByOldBindingsGenerator]
			get;
			[MethodImpl(MethodImplOptions.InternalCall)]
			[GeneratedByOldBindingsGenerator]
			set;
		}

		/// <summary>
		///   <para>Opaque object sorting mode.</para>
		/// </summary>
		public OpaqueSortMode opaqueSortMode
		{
			[MethodImpl(MethodImplOptions.InternalCall)]
			[GeneratedByOldBindingsGenerator]
			get;
			[MethodImpl(MethodImplOptions.InternalCall)]
			[GeneratedByOldBindingsGenerator]
			set;
		}

		/// <summary>
		///   <para>Transparent object sorting mode.</para>
		/// </summary>
		public TransparencySortMode transparencySortMode
		{
			[MethodImpl(MethodImplOptions.InternalCall)]
			[GeneratedByOldBindingsGenerator]
			get;
			[MethodImpl(MethodImplOptions.InternalCall)]
			[GeneratedByOldBindingsGenerator]
			set;
		}

		/// <summary>
		///   <para>An axis that describes the direction along which the distances of objects are measured for the purpose of sorting.</para>
		/// </summary>
		public Vector3 transparencySortAxis
		{
			get
			{
				INTERNAL_get_transparencySortAxis(out Vector3 value);
				return value;
			}
			set
			{
				INTERNAL_set_transparencySortAxis(ref value);
			}
		}

		/// <summary>
		///   <para>Camera's depth in the camera rendering order.</para>
		/// </summary>
		public float depth
		{
			[MethodImpl(MethodImplOptions.InternalCall)]
			[GeneratedByOldBindingsGenerator]
			get;
			[MethodImpl(MethodImplOptions.InternalCall)]
			[GeneratedByOldBindingsGenerator]
			set;
		}

		/// <summary>
		///   <para>The aspect ratio (width divided by height).</para>
		/// </summary>
		public float aspect
		{
			[MethodImpl(MethodImplOptions.InternalCall)]
			[GeneratedByOldBindingsGenerator]
			get;
			[MethodImpl(MethodImplOptions.InternalCall)]
			[GeneratedByOldBindingsGenerator]
			set;
		}

		/// <summary>
		///   <para>This is used to render parts of the scene selectively.</para>
		/// </summary>
		public int cullingMask
		{
			[MethodImpl(MethodImplOptions.InternalCall)]
			[GeneratedByOldBindingsGenerator]
			get;
			[MethodImpl(MethodImplOptions.InternalCall)]
			[GeneratedByOldBindingsGenerator]
			set;
		}

		internal static int PreviewCullingLayer
		{
			[MethodImpl(MethodImplOptions.InternalCall)]
			[GeneratedByOldBindingsGenerator]
			get;
		}

		/// <summary>
		///   <para>If not null, the camera will only render the contents of the specified scene.</para>
		/// </summary>
		public Scene scene
		{
			get
			{
				INTERNAL_get_scene(out Scene value);
				return value;
			}
			set
			{
				INTERNAL_set_scene(ref value);
			}
		}

		/// <summary>
		///   <para>Mask to select which layers can trigger events on the camera.</para>
		/// </summary>
		public int eventMask
		{
			[MethodImpl(MethodImplOptions.InternalCall)]
			[GeneratedByOldBindingsGenerator]
			get;
			[MethodImpl(MethodImplOptions.InternalCall)]
			[GeneratedByOldBindingsGenerator]
			set;
		}

		/// <summary>
		///   <para>The color with which the screen will be cleared.</para>
		/// </summary>
		public Color backgroundColor
		{
			get
			{
				INTERNAL_get_backgroundColor(out Color value);
				return value;
			}
			set
			{
				INTERNAL_set_backgroundColor(ref value);
			}
		}

		/// <summary>
		///   <para>Where on the screen is the camera rendered in normalized coordinates.</para>
		/// </summary>
		public Rect rect
		{
			get
			{
				INTERNAL_get_rect(out Rect value);
				return value;
			}
			set
			{
				INTERNAL_set_rect(ref value);
			}
		}

		/// <summary>
		///   <para>Where on the screen is the camera rendered in pixel coordinates.</para>
		/// </summary>
		public Rect pixelRect
		{
			get
			{
				INTERNAL_get_pixelRect(out Rect value);
				return value;
			}
			set
			{
				INTERNAL_set_pixelRect(ref value);
			}
		}

		/// <summary>
		///   <para>Destination render texture.</para>
		/// </summary>
		public RenderTexture targetTexture
		{
			[MethodImpl(MethodImplOptions.InternalCall)]
			[GeneratedByOldBindingsGenerator]
			get;
			[MethodImpl(MethodImplOptions.InternalCall)]
			[GeneratedByOldBindingsGenerator]
			set;
		}

		/// <summary>
		///   <para>Gets the temporary RenderTexture target for this Camera.</para>
		/// </summary>
		public RenderTexture activeTexture
		{
			[MethodImpl(MethodImplOptions.InternalCall)]
			[GeneratedByOldBindingsGenerator]
			get;
		}

		/// <summary>
		///   <para>How wide is the camera in pixels (not accounting for dynamic resolution scaling) (Read Only).</para>
		/// </summary>
		public int pixelWidth
		{
			[MethodImpl(MethodImplOptions.InternalCall)]
			[GeneratedByOldBindingsGenerator]
			get;
		}

		/// <summary>
		///   <para>How tall is the camera in pixels (not accounting for dynamic resolution scaling) (Read Only).</para>
		/// </summary>
		public int pixelHeight
		{
			[MethodImpl(MethodImplOptions.InternalCall)]
			[GeneratedByOldBindingsGenerator]
			get;
		}

		/// <summary>
		///   <para>How wide is the camera in pixels (accounting for dynamic resolution scaling) (Read Only).</para>
		/// </summary>
		public int scaledPixelWidth
		{
			[MethodImpl(MethodImplOptions.InternalCall)]
			[GeneratedByOldBindingsGenerator]
			get;
		}

		/// <summary>
		///   <para>How tall is the camera in pixels (accounting for dynamic resolution scaling) (Read Only).</para>
		/// </summary>
		public int scaledPixelHeight
		{
			[MethodImpl(MethodImplOptions.InternalCall)]
			[GeneratedByOldBindingsGenerator]
			get;
		}

		/// <summary>
		///   <para>Matrix that transforms from camera space to world space (Read Only).</para>
		/// </summary>
		public Matrix4x4 cameraToWorldMatrix
		{
			get
			{
				INTERNAL_get_cameraToWorldMatrix(out Matrix4x4 value);
				return value;
			}
		}

		/// <summary>
		///   <para>Matrix that transforms from world to camera space.</para>
		/// </summary>
		public Matrix4x4 worldToCameraMatrix
		{
			get
			{
				INTERNAL_get_worldToCameraMatrix(out Matrix4x4 value);
				return value;
			}
			set
			{
				INTERNAL_set_worldToCameraMatrix(ref value);
			}
		}

		/// <summary>
		///   <para>Set a custom projection matrix.</para>
		/// </summary>
		public Matrix4x4 projectionMatrix
		{
			get
			{
				INTERNAL_get_projectionMatrix(out Matrix4x4 value);
				return value;
			}
			set
			{
				INTERNAL_set_projectionMatrix(ref value);
			}
		}

		/// <summary>
		///   <para>Get or set the raw projection matrix with no camera offset (no jittering).</para>
		/// </summary>
		public Matrix4x4 nonJitteredProjectionMatrix
		{
			get
			{
				INTERNAL_get_nonJitteredProjectionMatrix(out Matrix4x4 value);
				return value;
			}
			set
			{
				INTERNAL_set_nonJitteredProjectionMatrix(ref value);
			}
		}

		/// <summary>
		///   <para>Should the jittered matrix be used for transparency rendering?</para>
		/// </summary>
		public bool useJitteredProjectionMatrixForTransparentRendering
		{
			[MethodImpl(MethodImplOptions.InternalCall)]
			[GeneratedByOldBindingsGenerator]
			get;
			[MethodImpl(MethodImplOptions.InternalCall)]
			[GeneratedByOldBindingsGenerator]
			set;
		}

		/// <summary>
		///   <para>Get the view projection matrix used on the last frame.</para>
		/// </summary>
		public Matrix4x4 previousViewProjectionMatrix
		{
			get
			{
				INTERNAL_get_previousViewProjectionMatrix(out Matrix4x4 value);
				return value;
			}
		}

		/// <summary>
		///   <para>Get the world-space speed of the camera (Read Only).</para>
		/// </summary>
		public Vector3 velocity
		{
			get
			{
				INTERNAL_get_velocity(out Vector3 value);
				return value;
			}
		}

		/// <summary>
		///   <para>How the camera clears the background.</para>
		/// </summary>
		public CameraClearFlags clearFlags
		{
			[MethodImpl(MethodImplOptions.InternalCall)]
			[GeneratedByOldBindingsGenerator]
			get;
			[MethodImpl(MethodImplOptions.InternalCall)]
			[GeneratedByOldBindingsGenerator]
			set;
		}

		/// <summary>
		///   <para>Stereoscopic rendering.</para>
		/// </summary>
		public bool stereoEnabled
		{
			[MethodImpl(MethodImplOptions.InternalCall)]
			[GeneratedByOldBindingsGenerator]
			get;
		}

		/// <summary>
		///   <para>The distance between the virtual eyes. Use this to query or set the current eye separation. Note that most VR devices provide this value, in which case setting the value will have no effect.</para>
		/// </summary>
		public float stereoSeparation
		{
			[MethodImpl(MethodImplOptions.InternalCall)]
			[GeneratedByOldBindingsGenerator]
			get;
			[MethodImpl(MethodImplOptions.InternalCall)]
			[GeneratedByOldBindingsGenerator]
			set;
		}

		/// <summary>
		///   <para>Distance to a point where virtual eyes converge.</para>
		/// </summary>
		public float stereoConvergence
		{
			[MethodImpl(MethodImplOptions.InternalCall)]
			[GeneratedByOldBindingsGenerator]
			get;
			[MethodImpl(MethodImplOptions.InternalCall)]
			[GeneratedByOldBindingsGenerator]
			set;
		}

		/// <summary>
		///   <para>Identifies what kind of camera this is.</para>
		/// </summary>
		public CameraType cameraType
		{
			[MethodImpl(MethodImplOptions.InternalCall)]
			[GeneratedByOldBindingsGenerator]
			get;
			[MethodImpl(MethodImplOptions.InternalCall)]
			[GeneratedByOldBindingsGenerator]
			set;
		}

		/// <summary>
		///   <para>Render only once and use resulting image for both eyes.</para>
		/// </summary>
		[Obsolete("This property is no longer supported. Please use single pass stereo rendering instead.", true)]
		public bool stereoMirrorMode
		{
			[MethodImpl(MethodImplOptions.InternalCall)]
			[GeneratedByOldBindingsGenerator]
			get;
			[MethodImpl(MethodImplOptions.InternalCall)]
			[GeneratedByOldBindingsGenerator]
			set;
		}

		/// <summary>
		///   <para>Defines which eye of a VR display the Camera renders into.</para>
		/// </summary>
		public StereoTargetEyeMask stereoTargetEye
		{
			[MethodImpl(MethodImplOptions.InternalCall)]
			[GeneratedByOldBindingsGenerator]
			get;
			[MethodImpl(MethodImplOptions.InternalCall)]
			[GeneratedByOldBindingsGenerator]
			set;
		}

		/// <summary>
		///   <para>Determines whether the stereo view matrices are suitable to allow for a single pass cull.</para>
		/// </summary>
		public bool areVRStereoViewMatricesWithinSingleCullTolerance
		{
			[MethodImpl(MethodImplOptions.InternalCall)]
			[GeneratedByOldBindingsGenerator]
			get;
		}

		/// <summary>
		///   <para>Returns the eye that is currently rendering.
		/// If called when stereo is not enabled it will return Camera.MonoOrStereoscopicEye.Mono.
		///
		/// If called during a camera rendering callback such as OnRenderImage it will return the currently rendering eye.
		///
		/// If called outside of a rendering callback and stereo is enabled, it will return the default eye which is Camera.MonoOrStereoscopicEye.Left.</para>
		/// </summary>
		public MonoOrStereoscopicEye stereoActiveEye
		{
			[MethodImpl(MethodImplOptions.InternalCall)]
			[GeneratedByOldBindingsGenerator]
			get;
		}

		/// <summary>
		///   <para>Set the target display for this Camera.</para>
		/// </summary>
		public int targetDisplay
		{
			[MethodImpl(MethodImplOptions.InternalCall)]
			[GeneratedByOldBindingsGenerator]
			get;
			[MethodImpl(MethodImplOptions.InternalCall)]
			[GeneratedByOldBindingsGenerator]
			set;
		}

		/// <summary>
		///   <para>The first enabled camera tagged "MainCamera" (Read Only).</para>
		/// </summary>
		public static Camera main
		{
			[MethodImpl(MethodImplOptions.InternalCall)]
			[GeneratedByOldBindingsGenerator]
			get;
		}

		/// <summary>
		///   <para>The camera we are currently rendering with, for low-level render control only (Read Only).</para>
		/// </summary>
		public static Camera current
		{
			[MethodImpl(MethodImplOptions.InternalCall)]
			[GeneratedByOldBindingsGenerator]
			get;
		}

		/// <summary>
		///   <para>Returns all enabled cameras in the scene.</para>
		/// </summary>
		public static Camera[] allCameras
		{
			[MethodImpl(MethodImplOptions.InternalCall)]
			[GeneratedByOldBindingsGenerator]
			get;
		}

		/// <summary>
		///   <para>The number of cameras in the current scene.</para>
		/// </summary>
		public static int allCamerasCount
		{
			[MethodImpl(MethodImplOptions.InternalCall)]
			[GeneratedByOldBindingsGenerator]
			get;
		}

		/// <summary>
		///   <para>Whether or not the Camera will use occlusion culling during rendering.</para>
		/// </summary>
		public bool useOcclusionCulling
		{
			[MethodImpl(MethodImplOptions.InternalCall)]
			[GeneratedByOldBindingsGenerator]
			get;
			[MethodImpl(MethodImplOptions.InternalCall)]
			[GeneratedByOldBindingsGenerator]
			set;
		}

		/// <summary>
		///   <para>Sets a custom matrix for the camera to use for all culling queries.</para>
		/// </summary>
		public Matrix4x4 cullingMatrix
		{
			get
			{
				INTERNAL_get_cullingMatrix(out Matrix4x4 value);
				return value;
			}
			set
			{
				INTERNAL_set_cullingMatrix(ref value);
			}
		}

		/// <summary>
		///   <para>Per-layer culling distances.</para>
		/// </summary>
		public float[] layerCullDistances
		{
			[MethodImpl(MethodImplOptions.InternalCall)]
			[GeneratedByOldBindingsGenerator]
			get;
			[MethodImpl(MethodImplOptions.InternalCall)]
			[GeneratedByOldBindingsGenerator]
			set;
		}

		/// <summary>
		///   <para>How to perform per-layer culling for a Camera.</para>
		/// </summary>
		public bool layerCullSpherical
		{
			[MethodImpl(MethodImplOptions.InternalCall)]
			[GeneratedByOldBindingsGenerator]
			get;
			[MethodImpl(MethodImplOptions.InternalCall)]
			[GeneratedByOldBindingsGenerator]
			set;
		}

		/// <summary>
		///   <para>How and if camera generates a depth texture.</para>
		/// </summary>
		public DepthTextureMode depthTextureMode
		{
			[MethodImpl(MethodImplOptions.InternalCall)]
			[GeneratedByOldBindingsGenerator]
			get;
			[MethodImpl(MethodImplOptions.InternalCall)]
			[GeneratedByOldBindingsGenerator]
			set;
		}

		/// <summary>
		///   <para>Should the camera clear the stencil buffer after the deferred light pass?</para>
		/// </summary>
		public bool clearStencilAfterLightingPass
		{
			[MethodImpl(MethodImplOptions.InternalCall)]
			[GeneratedByOldBindingsGenerator]
			get;
			[MethodImpl(MethodImplOptions.InternalCall)]
			[GeneratedByOldBindingsGenerator]
			set;
		}

		/// <summary>
		///   <para>Number of command buffers set up on this camera (Read Only).</para>
		/// </summary>
		public int commandBufferCount
		{
			[MethodImpl(MethodImplOptions.InternalCall)]
			[GeneratedByOldBindingsGenerator]
			get;
		}

		[MethodImpl(MethodImplOptions.InternalCall)]
		[GeneratedByOldBindingsGenerator]
		internal extern string[] GetCameraBufferWarnings();

		[MethodImpl(MethodImplOptions.InternalCall)]
		[GeneratedByOldBindingsGenerator]
		private extern void INTERNAL_get_transparencySortAxis(out Vector3 value);

		[MethodImpl(MethodImplOptions.InternalCall)]
		[GeneratedByOldBindingsGenerator]
		private extern void INTERNAL_set_transparencySortAxis(ref Vector3 value);

		[MethodImpl(MethodImplOptions.InternalCall)]
		[GeneratedByOldBindingsGenerator]
		private extern void INTERNAL_get_scene(out Scene value);

		[MethodImpl(MethodImplOptions.InternalCall)]
		[GeneratedByOldBindingsGenerator]
		private extern void INTERNAL_set_scene(ref Scene value);

		[MethodImpl(MethodImplOptions.InternalCall)]
		[GeneratedByOldBindingsGenerator]
		private extern void INTERNAL_get_backgroundColor(out Color value);

		[MethodImpl(MethodImplOptions.InternalCall)]
		[GeneratedByOldBindingsGenerator]
		private extern void INTERNAL_set_backgroundColor(ref Color value);

		[MethodImpl(MethodImplOptions.InternalCall)]
		[GeneratedByOldBindingsGenerator]
		private extern void INTERNAL_get_rect(out Rect value);

		[MethodImpl(MethodImplOptions.InternalCall)]
		[GeneratedByOldBindingsGenerator]
		private extern void INTERNAL_set_rect(ref Rect value);

		[MethodImpl(MethodImplOptions.InternalCall)]
		[GeneratedByOldBindingsGenerator]
		private extern void INTERNAL_get_pixelRect(out Rect value);

		[MethodImpl(MethodImplOptions.InternalCall)]
		[GeneratedByOldBindingsGenerator]
		private extern void INTERNAL_set_pixelRect(ref Rect value);

		[MethodImpl(MethodImplOptions.InternalCall)]
		[GeneratedByOldBindingsGenerator]
		private extern void SetTargetBuffersImpl(out RenderBuffer color, out RenderBuffer depth);

		[MethodImpl(MethodImplOptions.InternalCall)]
		[GeneratedByOldBindingsGenerator]
		private extern void SetTargetBuffersMRTImpl(RenderBuffer[] color, out RenderBuffer depth);

		/// <summary>
		///   <para>Sets the Camera to render to the chosen buffers of one or more RenderTextures.</para>
		/// </summary>
		/// <param name="colorBuffer">The RenderBuffer(s) to which color information will be rendered.</param>
		/// <param name="depthBuffer">The RenderBuffer to which depth information will be rendered.</param>
		public void SetTargetBuffers(RenderBuffer colorBuffer, RenderBuffer depthBuffer)
		{
			SetTargetBuffersImpl(out colorBuffer, out depthBuffer);
		}

		/// <summary>
		///   <para>Sets the Camera to render to the chosen buffers of one or more RenderTextures.</para>
		/// </summary>
		/// <param name="colorBuffer">The RenderBuffer(s) to which color information will be rendered.</param>
		/// <param name="depthBuffer">The RenderBuffer to which depth information will be rendered.</param>
		public void SetTargetBuffers(RenderBuffer[] colorBuffer, RenderBuffer depthBuffer)
		{
			SetTargetBuffersMRTImpl(colorBuffer, out depthBuffer);
		}

		[MethodImpl(MethodImplOptions.InternalCall)]
		[GeneratedByOldBindingsGenerator]
		private extern void INTERNAL_get_cameraToWorldMatrix(out Matrix4x4 value);

		[MethodImpl(MethodImplOptions.InternalCall)]
		[GeneratedByOldBindingsGenerator]
		private extern void INTERNAL_get_worldToCameraMatrix(out Matrix4x4 value);

		[MethodImpl(MethodImplOptions.InternalCall)]
		[GeneratedByOldBindingsGenerator]
		private extern void INTERNAL_set_worldToCameraMatrix(ref Matrix4x4 value);

		/// <summary>
		///   <para>Make the rendering position reflect the camera's position in the scene.</para>
		/// </summary>
		public void ResetWorldToCameraMatrix()
		{
			INTERNAL_CALL_ResetWorldToCameraMatrix(this);
		}

		[MethodImpl(MethodImplOptions.InternalCall)]
		[GeneratedByOldBindingsGenerator]
		private static extern void INTERNAL_CALL_ResetWorldToCameraMatrix(Camera self);

		[MethodImpl(MethodImplOptions.InternalCall)]
		[GeneratedByOldBindingsGenerator]
		private extern void INTERNAL_get_projectionMatrix(out Matrix4x4 value);

		[MethodImpl(MethodImplOptions.InternalCall)]
		[GeneratedByOldBindingsGenerator]
		private extern void INTERNAL_set_projectionMatrix(ref Matrix4x4 value);

		[MethodImpl(MethodImplOptions.InternalCall)]
		[GeneratedByOldBindingsGenerator]
		private extern void INTERNAL_get_nonJitteredProjectionMatrix(out Matrix4x4 value);

		[MethodImpl(MethodImplOptions.InternalCall)]
		[GeneratedByOldBindingsGenerator]
		private extern void INTERNAL_set_nonJitteredProjectionMatrix(ref Matrix4x4 value);

		[MethodImpl(MethodImplOptions.InternalCall)]
		[GeneratedByOldBindingsGenerator]
		private extern void INTERNAL_get_previousViewProjectionMatrix(out Matrix4x4 value);

		/// <summary>
		///   <para>Make the projection reflect normal camera's parameters.</para>
		/// </summary>
		public void ResetProjectionMatrix()
		{
			INTERNAL_CALL_ResetProjectionMatrix(this);
		}

		[MethodImpl(MethodImplOptions.InternalCall)]
		[GeneratedByOldBindingsGenerator]
		private static extern void INTERNAL_CALL_ResetProjectionMatrix(Camera self);

		/// <summary>
		///   <para>Revert the aspect ratio to the screen's aspect ratio.</para>
		/// </summary>
		public void ResetAspect()
		{
			INTERNAL_CALL_ResetAspect(this);
		}

		[MethodImpl(MethodImplOptions.InternalCall)]
		[GeneratedByOldBindingsGenerator]
		private static extern void INTERNAL_CALL_ResetAspect(Camera self);

		/// <summary>
		///   <para>Reset to the default field of view.</para>
		/// </summary>
		[Obsolete("Camera.ResetFieldOfView has been deprecated in Unity 5.6 and will be removed in the future. Please replace it by explicitly setting the camera's FOV to 60 degrees.")]
		public void ResetFieldOfView()
		{
			INTERNAL_CALL_ResetFieldOfView(this);
		}

		[MethodImpl(MethodImplOptions.InternalCall)]
		[GeneratedByOldBindingsGenerator]
		private static extern void INTERNAL_CALL_ResetFieldOfView(Camera self);

		[MethodImpl(MethodImplOptions.InternalCall)]
		[GeneratedByOldBindingsGenerator]
		private extern void INTERNAL_get_velocity(out Vector3 value);

		[MethodImpl(MethodImplOptions.InternalCall)]
		[Obsolete("GetStereoViewMatrices is deprecated. Use GetStereoViewMatrix(StereoscopicEye eye) instead.")]
		[GeneratedByOldBindingsGenerator]
		public extern Matrix4x4[] GetStereoViewMatrices();

		public Matrix4x4 GetStereoViewMatrix(StereoscopicEye eye)
		{
			INTERNAL_CALL_GetStereoViewMatrix(this, eye, out Matrix4x4 value);
			return value;
		}

		[MethodImpl(MethodImplOptions.InternalCall)]
		[GeneratedByOldBindingsGenerator]
		private static extern void INTERNAL_CALL_GetStereoViewMatrix(Camera self, StereoscopicEye eye, out Matrix4x4 value);

		/// <summary>
		///   <para>Set custom view matrices for both eyes.</para>
		/// </summary>
		/// <param name="leftMatrix">View matrix for the stereo left eye.</param>
		/// <param name="rightMatrix">View matrix for the stereo right eye.</param>
		[Obsolete("SetStereoViewMatrices is deprecated. Use SetStereoViewMatrix(StereoscopicEye eye) instead.")]
		public void SetStereoViewMatrices(Matrix4x4 leftMatrix, Matrix4x4 rightMatrix)
		{
			INTERNAL_CALL_SetStereoViewMatrices(this, ref leftMatrix, ref rightMatrix);
		}

		[MethodImpl(MethodImplOptions.InternalCall)]
		[GeneratedByOldBindingsGenerator]
		private static extern void INTERNAL_CALL_SetStereoViewMatrices(Camera self, ref Matrix4x4 leftMatrix, ref Matrix4x4 rightMatrix);

		public void SetStereoViewMatrix(StereoscopicEye eye, Matrix4x4 matrix)
		{
			INTERNAL_CALL_SetStereoViewMatrix(this, eye, ref matrix);
		}

		[MethodImpl(MethodImplOptions.InternalCall)]
		[GeneratedByOldBindingsGenerator]
		private static extern void INTERNAL_CALL_SetStereoViewMatrix(Camera self, StereoscopicEye eye, ref Matrix4x4 matrix);

		/// <summary>
		///   <para>Reset the camera to using the Unity computed view matrices for all stereoscopic eyes.</para>
		/// </summary>
		[MethodImpl(MethodImplOptions.InternalCall)]
		[GeneratedByOldBindingsGenerator]
		public extern void ResetStereoViewMatrices();

		[MethodImpl(MethodImplOptions.InternalCall)]
		[Obsolete("GetStereoProjectionMatrices is deprecated. Use GetStereoProjectionMatrix(StereoscopicEye eye) instead.")]
		[GeneratedByOldBindingsGenerator]
		public extern Matrix4x4[] GetStereoProjectionMatrices();

		public Matrix4x4 GetStereoProjectionMatrix(StereoscopicEye eye)
		{
			INTERNAL_CALL_GetStereoProjectionMatrix(this, eye, out Matrix4x4 value);
			return value;
		}

		[MethodImpl(MethodImplOptions.InternalCall)]
		[GeneratedByOldBindingsGenerator]
		private static extern void INTERNAL_CALL_GetStereoProjectionMatrix(Camera self, StereoscopicEye eye, out Matrix4x4 value);

		public void SetStereoProjectionMatrix(StereoscopicEye eye, Matrix4x4 matrix)
		{
			INTERNAL_CALL_SetStereoProjectionMatrix(this, eye, ref matrix);
		}

		[MethodImpl(MethodImplOptions.InternalCall)]
		[GeneratedByOldBindingsGenerator]
		private static extern void INTERNAL_CALL_SetStereoProjectionMatrix(Camera self, StereoscopicEye eye, ref Matrix4x4 matrix);

		/// <summary>
		///   <para>Sets custom projection matrices for both the left and right stereoscopic eyes.</para>
		/// </summary>
		/// <param name="leftMatrix">Projection matrix for the stereoscopic left eye.</param>
		/// <param name="rightMatrix">Projection matrix for the stereoscopic right eye.</param>
		[Obsolete("SetStereoProjectionMatrices is deprecated. Use SetStereoProjectionMatrix(StereoscopicEye eye) instead.")]
		public void SetStereoProjectionMatrices(Matrix4x4 leftMatrix, Matrix4x4 rightMatrix)
		{
			INTERNAL_CALL_SetStereoProjectionMatrices(this, ref leftMatrix, ref rightMatrix);
		}

		[MethodImpl(MethodImplOptions.InternalCall)]
		[GeneratedByOldBindingsGenerator]
		private static extern void INTERNAL_CALL_SetStereoProjectionMatrices(Camera self, ref Matrix4x4 leftMatrix, ref Matrix4x4 rightMatrix);

		public void CalculateFrustumCorners(Rect viewport, float z, MonoOrStereoscopicEye eye, Vector3[] outCorners)
		{
			if (outCorners == null)
			{
				throw new ArgumentNullException("outCorners");
			}
			if (outCorners.Length < 4)
			{
				throw new ArgumentException("outCorners minimum size is 4", "outCorners");
			}
			CalculateFrustumCornersInternal(viewport, z, eye, outCorners);
		}

		private void CalculateFrustumCornersInternal(Rect viewport, float z, MonoOrStereoscopicEye eye, Vector3[] outCorners)
		{
			INTERNAL_CALL_CalculateFrustumCornersInternal(this, ref viewport, z, eye, outCorners);
		}

		[MethodImpl(MethodImplOptions.InternalCall)]
		[GeneratedByOldBindingsGenerator]
		private static extern void INTERNAL_CALL_CalculateFrustumCornersInternal(Camera self, ref Rect viewport, float z, MonoOrStereoscopicEye eye, Vector3[] outCorners);

		/// <summary>
		///   <para>Reset the camera to using the Unity computed projection matrices for all stereoscopic eyes.</para>
		/// </summary>
		[MethodImpl(MethodImplOptions.InternalCall)]
		[GeneratedByOldBindingsGenerator]
		public extern void ResetStereoProjectionMatrices();

		/// <summary>
		///   <para>Resets this Camera's transparency sort settings to the default. Default transparency settings are taken from GraphicsSettings instead of directly from this Camera.</para>
		/// </summary>
		[MethodImpl(MethodImplOptions.InternalCall)]
		[GeneratedByOldBindingsGenerator]
		public extern void ResetTransparencySortSettings();

		/// <summary>
		///   <para>Transforms position from world space into screen space.</para>
		/// </summary>
		/// <param name="position"></param>
		public Vector3 WorldToScreenPoint(Vector3 position)
		{
			INTERNAL_CALL_WorldToScreenPoint(this, ref position, out Vector3 value);
			return value;
		}

		[MethodImpl(MethodImplOptions.InternalCall)]
		[GeneratedByOldBindingsGenerator]
		private static extern void INTERNAL_CALL_WorldToScreenPoint(Camera self, ref Vector3 position, out Vector3 value);

		/// <summary>
		///   <para>Transforms position from world space into viewport space.</para>
		/// </summary>
		/// <param name="position"></param>
		public Vector3 WorldToViewportPoint(Vector3 position)
		{
			INTERNAL_CALL_WorldToViewportPoint(this, ref position, out Vector3 value);
			return value;
		}

		[MethodImpl(MethodImplOptions.InternalCall)]
		[GeneratedByOldBindingsGenerator]
		private static extern void INTERNAL_CALL_WorldToViewportPoint(Camera self, ref Vector3 position, out Vector3 value);

		/// <summary>
		///   <para>Transforms position from viewport space into world space.</para>
		/// </summary>
		/// <param name="position">The 3d vector in Viewport space.</param>
		/// <returns>
		///   <para>The 3d vector in World space.</para>
		/// </returns>
		public Vector3 ViewportToWorldPoint(Vector3 position)
		{
			INTERNAL_CALL_ViewportToWorldPoint(this, ref position, out Vector3 value);
			return value;
		}

		[MethodImpl(MethodImplOptions.InternalCall)]
		[GeneratedByOldBindingsGenerator]
		private static extern void INTERNAL_CALL_ViewportToWorldPoint(Camera self, ref Vector3 position, out Vector3 value);

		/// <summary>
		///   <para>Transforms position from screen space into world space.</para>
		/// </summary>
		/// <param name="position"></param>
		public Vector3 ScreenToWorldPoint(Vector3 position)
		{
			INTERNAL_CALL_ScreenToWorldPoint(this, ref position, out Vector3 value);
			return value;
		}

		[MethodImpl(MethodImplOptions.InternalCall)]
		[GeneratedByOldBindingsGenerator]
		private static extern void INTERNAL_CALL_ScreenToWorldPoint(Camera self, ref Vector3 position, out Vector3 value);

		/// <summary>
		///   <para>Transforms position from screen space into viewport space.</para>
		/// </summary>
		/// <param name="position"></param>
		public Vector3 ScreenToViewportPoint(Vector3 position)
		{
			INTERNAL_CALL_ScreenToViewportPoint(this, ref position, out Vector3 value);
			return value;
		}

		[MethodImpl(MethodImplOptions.InternalCall)]
		[GeneratedByOldBindingsGenerator]
		private static extern void INTERNAL_CALL_ScreenToViewportPoint(Camera self, ref Vector3 position, out Vector3 value);

		/// <summary>
		///   <para>Transforms position from viewport space into screen space.</para>
		/// </summary>
		/// <param name="position"></param>
		public Vector3 ViewportToScreenPoint(Vector3 position)
		{
			INTERNAL_CALL_ViewportToScreenPoint(this, ref position, out Vector3 value);
			return value;
		}

		[MethodImpl(MethodImplOptions.InternalCall)]
		[GeneratedByOldBindingsGenerator]
		private static extern void INTERNAL_CALL_ViewportToScreenPoint(Camera self, ref Vector3 position, out Vector3 value);

		/// <summary>
		///   <para>Returns a ray going from camera through a viewport point.</para>
		/// </summary>
		/// <param name="position"></param>
		public Ray ViewportPointToRay(Vector3 position)
		{
			INTERNAL_CALL_ViewportPointToRay(this, ref position, out Ray value);
			return value;
		}

		[MethodImpl(MethodImplOptions.InternalCall)]
		[GeneratedByOldBindingsGenerator]
		private static extern void INTERNAL_CALL_ViewportPointToRay(Camera self, ref Vector3 position, out Ray value);

		/// <summary>
		///   <para>Returns a ray going from camera through a screen point.</para>
		/// </summary>
		/// <param name="position"></param>
		public Ray ScreenPointToRay(Vector3 position)
		{
			INTERNAL_CALL_ScreenPointToRay(this, ref position, out Ray value);
			return value;
		}

		[MethodImpl(MethodImplOptions.InternalCall)]
		[GeneratedByOldBindingsGenerator]
		private static extern void INTERNAL_CALL_ScreenPointToRay(Camera self, ref Vector3 position, out Ray value);

		/// <summary>
		///   <para>Fills an array of Camera with the current cameras in the scene, without allocating a new array.</para>
		/// </summary>
		/// <param name="cameras">An array to be filled up with cameras currently in the scene.</param>
		[MethodImpl(MethodImplOptions.InternalCall)]
		[GeneratedByOldBindingsGenerator]
		public static extern int GetAllCameras(Camera[] cameras);

		[RequiredByNativeCode]
		private static void FireOnPreCull(Camera cam)
		{
			if (onPreCull != null)
			{
				onPreCull(cam);
			}
		}

		[RequiredByNativeCode]
		private static void FireOnPreRender(Camera cam)
		{
			if (onPreRender != null)
			{
				onPreRender(cam);
			}
		}

		[RequiredByNativeCode]
		private static void FireOnPostRender(Camera cam)
		{
			if (onPostRender != null)
			{
				onPostRender(cam);
			}
		}

		/// <summary>
		///   <para>Render the camera manually.</para>
		/// </summary>
		[MethodImpl(MethodImplOptions.InternalCall)]
		[GeneratedByOldBindingsGenerator]
		public extern void Render();

		/// <summary>
		///   <para>Render the camera with shader replacement.</para>
		/// </summary>
		/// <param name="shader"></param>
		/// <param name="replacementTag"></param>
		[MethodImpl(MethodImplOptions.InternalCall)]
		[GeneratedByOldBindingsGenerator]
		public extern void RenderWithShader(Shader shader, string replacementTag);

		/// <summary>
		///   <para>Make the camera render with shader replacement.</para>
		/// </summary>
		/// <param name="shader"></param>
		/// <param name="replacementTag"></param>
		[MethodImpl(MethodImplOptions.InternalCall)]
		[GeneratedByOldBindingsGenerator]
		public extern void SetReplacementShader(Shader shader, string replacementTag);

		/// <summary>
		///   <para>Remove shader replacement from camera.</para>
		/// </summary>
		public void ResetReplacementShader()
		{
			INTERNAL_CALL_ResetReplacementShader(this);
		}

		[MethodImpl(MethodImplOptions.InternalCall)]
		[GeneratedByOldBindingsGenerator]
		private static extern void INTERNAL_CALL_ResetReplacementShader(Camera self);

		[MethodImpl(MethodImplOptions.InternalCall)]
		[GeneratedByOldBindingsGenerator]
		private extern void INTERNAL_get_cullingMatrix(out Matrix4x4 value);

		[MethodImpl(MethodImplOptions.InternalCall)]
		[GeneratedByOldBindingsGenerator]
		private extern void INTERNAL_set_cullingMatrix(ref Matrix4x4 value);

		/// <summary>
		///   <para>Make culling queries reflect the camera's built in parameters.</para>
		/// </summary>
		public void ResetCullingMatrix()
		{
			INTERNAL_CALL_ResetCullingMatrix(this);
		}

		[MethodImpl(MethodImplOptions.InternalCall)]
		[GeneratedByOldBindingsGenerator]
		private static extern void INTERNAL_CALL_ResetCullingMatrix(Camera self);

		[MethodImpl(MethodImplOptions.InternalCall)]
		[GeneratedByOldBindingsGenerator]
		public extern void RenderDontRestore();

		[MethodImpl(MethodImplOptions.InternalCall)]
		[GeneratedByOldBindingsGenerator]
		public static extern void SetupCurrent(Camera cur);

		[ExcludeFromDocs]
		public bool RenderToCubemap(Cubemap cubemap)
		{
			int faceMask = 63;
			return RenderToCubemap(cubemap, faceMask);
		}

		/// <summary>
		///   <para>Render into a static cubemap from this camera.</para>
		/// </summary>
		/// <param name="cubemap">The cube map to render to.</param>
		/// <param name="faceMask">A bitmask which determines which of the six faces are rendered to.</param>
		/// <returns>
		///   <para>False if rendering fails, else true.</para>
		/// </returns>
		public bool RenderToCubemap(Cubemap cubemap, [DefaultValue("63")] int faceMask)
		{
			return Internal_RenderToCubemapTexture(cubemap, faceMask);
		}

		[ExcludeFromDocs]
		public bool RenderToCubemap(RenderTexture cubemap)
		{
			int faceMask = 63;
			return RenderToCubemap(cubemap, faceMask);
		}

		/// <summary>
		///   <para>Render into a cubemap from this camera.</para>
		/// </summary>
		/// <param name="faceMask">A bitfield indicating which cubemap faces should be rendered into.</param>
		/// <param name="cubemap">The texture to render to.</param>
		/// <returns>
		///   <para>False if rendering fails, else true.</para>
		/// </returns>
		public bool RenderToCubemap(RenderTexture cubemap, [DefaultValue("63")] int faceMask)
		{
			return Internal_RenderToCubemapRT(cubemap, faceMask);
		}

		[MethodImpl(MethodImplOptions.InternalCall)]
		[GeneratedByOldBindingsGenerator]
		private extern bool Internal_RenderToCubemapRT(RenderTexture cubemap, int faceMask);

		[MethodImpl(MethodImplOptions.InternalCall)]
		[GeneratedByOldBindingsGenerator]
		private extern bool Internal_RenderToCubemapTexture(Cubemap cubemap, int faceMask);

		/// <summary>
		///   <para>Makes this camera's settings match other camera.</para>
		/// </summary>
		/// <param name="other">Copy camera settings to the other camera.</param>
		[MethodImpl(MethodImplOptions.InternalCall)]
		[GeneratedByOldBindingsGenerator]
		public extern void CopyFrom(Camera other);

		[MethodImpl(MethodImplOptions.InternalCall)]
		[GeneratedByOldBindingsGenerator]
		internal extern bool IsFiltered(GameObject go);

		/// <summary>
		///   <para>Add a command buffer to be executed at a specified place.</para>
		/// </summary>
		/// <param name="evt">When to execute the command buffer during rendering.</param>
		/// <param name="buffer">The buffer to execute.</param>
		[MethodImpl(MethodImplOptions.InternalCall)]
		[GeneratedByOldBindingsGenerator]
		public extern void AddCommandBuffer(CameraEvent evt, CommandBuffer buffer);

		/// <summary>
		///   <para>Remove command buffer from execution at a specified place.</para>
		/// </summary>
		/// <param name="evt">When to execute the command buffer during rendering.</param>
		/// <param name="buffer">The buffer to execute.</param>
		[MethodImpl(MethodImplOptions.InternalCall)]
		[GeneratedByOldBindingsGenerator]
		public extern void RemoveCommandBuffer(CameraEvent evt, CommandBuffer buffer);

		/// <summary>
		///   <para>Remove command buffers from execution at a specified place.</para>
		/// </summary>
		/// <param name="evt">When to execute the command buffer during rendering.</param>
		[MethodImpl(MethodImplOptions.InternalCall)]
		[GeneratedByOldBindingsGenerator]
		public extern void RemoveCommandBuffers(CameraEvent evt);

		/// <summary>
		///   <para>Remove all command buffers set on this camera.</para>
		/// </summary>
		[MethodImpl(MethodImplOptions.InternalCall)]
		[GeneratedByOldBindingsGenerator]
		public extern void RemoveAllCommandBuffers();

		/// <summary>
		///   <para>Get command buffers to be executed at a specified place.</para>
		/// </summary>
		/// <param name="evt">When to execute the command buffer during rendering.</param>
		/// <returns>
		///   <para>Array of command buffers.</para>
		/// </returns>
		[MethodImpl(MethodImplOptions.InternalCall)]
		[GeneratedByOldBindingsGenerator]
		public extern CommandBuffer[] GetCommandBuffers(CameraEvent evt);

		internal GameObject RaycastTry(Ray ray, float distance, int layerMask)
		{
			return INTERNAL_CALL_RaycastTry(this, ref ray, distance, layerMask);
		}

		[MethodImpl(MethodImplOptions.InternalCall)]
		[GeneratedByOldBindingsGenerator]
		private static extern GameObject INTERNAL_CALL_RaycastTry(Camera self, ref Ray ray, float distance, int layerMask);

		internal GameObject RaycastTry2D(Ray ray, float distance, int layerMask)
		{
			return INTERNAL_CALL_RaycastTry2D(this, ref ray, distance, layerMask);
		}

		[MethodImpl(MethodImplOptions.InternalCall)]
		[GeneratedByOldBindingsGenerator]
		private static extern GameObject INTERNAL_CALL_RaycastTry2D(Camera self, ref Ray ray, float distance, int layerMask);

		/// <summary>
		///   <para>Calculates and returns oblique near-plane projection matrix.</para>
		/// </summary>
		/// <param name="clipPlane">Vector4 that describes a clip plane.</param>
		/// <returns>
		///   <para>Oblique near-plane projection matrix.</para>
		/// </returns>
		public Matrix4x4 CalculateObliqueMatrix(Vector4 clipPlane)
		{
			INTERNAL_CALL_CalculateObliqueMatrix(this, ref clipPlane, out Matrix4x4 value);
			return value;
		}

		[MethodImpl(MethodImplOptions.InternalCall)]
		[GeneratedByOldBindingsGenerator]
		private static extern void INTERNAL_CALL_CalculateObliqueMatrix(Camera self, ref Vector4 clipPlane, out Matrix4x4 value);

		internal void OnlyUsedForTesting1()
		{
		}

		internal void OnlyUsedForTesting2()
		{
		}

		public Matrix4x4 GetStereoNonJitteredProjectionMatrix(StereoscopicEye eye)
		{
			GetStereoNonJitteredProjectionMatrix_Injected(eye, out Matrix4x4 ret);
			return ret;
		}

		[MethodImpl(MethodImplOptions.InternalCall)]
		public extern void CopyStereoDeviceProjectionMatrixToNonJittered(StereoscopicEye eye);

		[MethodImpl(MethodImplOptions.InternalCall)]
		private extern void GetStereoNonJitteredProjectionMatrix_Injected(StereoscopicEye eye, out Matrix4x4 ret);
	}
}
