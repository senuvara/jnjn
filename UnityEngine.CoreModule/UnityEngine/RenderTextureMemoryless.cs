using System;

namespace UnityEngine
{
	/// <summary>
	///   <para>Flags enumeration of the render texture memoryless modes.</para>
	/// </summary>
	[Flags]
	public enum RenderTextureMemoryless
	{
		/// <summary>
		///   <para>The render texture is not memoryless.</para>
		/// </summary>
		None = 0x0,
		/// <summary>
		///   <para>Render texture color pixels are memoryless when RenderTexture.antiAliasing is set to 1.</para>
		/// </summary>
		Color = 0x1,
		/// <summary>
		///   <para>Render texture depth pixels are memoryless.</para>
		/// </summary>
		Depth = 0x2,
		/// <summary>
		///   <para>Render texture color pixels are memoryless when RenderTexture.antiAliasing is set to 2, 4 or 8.</para>
		/// </summary>
		MSAA = 0x4
	}
}
