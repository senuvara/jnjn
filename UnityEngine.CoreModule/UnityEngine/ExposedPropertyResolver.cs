using System;
using System.Runtime.CompilerServices;
using UnityEngine.Scripting;

namespace UnityEngine
{
	/// <summary>
	///   <para>Object that is used to resolve references to an ExposedReference field.</para>
	/// </summary>
	public struct ExposedPropertyResolver
	{
		internal IntPtr table;

		internal static Object ResolveReferenceInternal(IntPtr ptr, PropertyName name, out bool isValid)
		{
			return INTERNAL_CALL_ResolveReferenceInternal(ptr, ref name, out isValid);
		}

		[MethodImpl(MethodImplOptions.InternalCall)]
		[GeneratedByOldBindingsGenerator]
		private static extern Object INTERNAL_CALL_ResolveReferenceInternal(IntPtr ptr, ref PropertyName name, out bool isValid);
	}
}
