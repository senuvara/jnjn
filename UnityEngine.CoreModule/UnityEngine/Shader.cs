using System;
using System.Collections.Generic;
using System.Runtime.CompilerServices;
using UnityEngine.Rendering;
using UnityEngine.Scripting;

namespace UnityEngine
{
	/// <summary>
	///   <para>Shader scripts used for all rendering.</para>
	/// </summary>
	public sealed class Shader : Object
	{
		/// <summary>
		///   <para>Can this shader run on the end-users graphics card? (Read Only)</para>
		/// </summary>
		public bool isSupported
		{
			[MethodImpl(MethodImplOptions.InternalCall)]
			[GeneratedByOldBindingsGenerator]
			get;
		}

		/// <summary>
		///   <para>Shader LOD level for this shader.</para>
		/// </summary>
		public int maximumLOD
		{
			[MethodImpl(MethodImplOptions.InternalCall)]
			[GeneratedByOldBindingsGenerator]
			get;
			[MethodImpl(MethodImplOptions.InternalCall)]
			[GeneratedByOldBindingsGenerator]
			set;
		}

		/// <summary>
		///   <para>Shader LOD level for all shaders.</para>
		/// </summary>
		public static int globalMaximumLOD
		{
			[MethodImpl(MethodImplOptions.InternalCall)]
			[GeneratedByOldBindingsGenerator]
			get;
			[MethodImpl(MethodImplOptions.InternalCall)]
			[GeneratedByOldBindingsGenerator]
			set;
		}

		/// <summary>
		///   <para>Render pipeline currently in use.</para>
		/// </summary>
		public static string globalRenderPipeline
		{
			[MethodImpl(MethodImplOptions.InternalCall)]
			[GeneratedByOldBindingsGenerator]
			get;
			[MethodImpl(MethodImplOptions.InternalCall)]
			[GeneratedByOldBindingsGenerator]
			set;
		}

		/// <summary>
		///   <para>Render queue of this shader. (Read Only)</para>
		/// </summary>
		public int renderQueue
		{
			[MethodImpl(MethodImplOptions.InternalCall)]
			[GeneratedByOldBindingsGenerator]
			get;
		}

		internal DisableBatchingType disableBatching
		{
			[MethodImpl(MethodImplOptions.InternalCall)]
			[GeneratedByOldBindingsGenerator]
			get;
		}

		/// <summary>
		///   <para>Shader hardware tier classification for current device.</para>
		/// </summary>
		[Obsolete("Use Graphics.activeTier instead (UnityUpgradable) -> UnityEngine.Graphics.activeTier", false)]
		public static ShaderHardwareTier globalShaderHardwareTier
		{
			get
			{
				return (ShaderHardwareTier)Graphics.activeTier;
			}
			set
			{
				Graphics.activeTier = (GraphicsTier)value;
			}
		}

		/// <summary>
		///   <para>Finds a shader with the given name.</para>
		/// </summary>
		/// <param name="name"></param>
		[MethodImpl(MethodImplOptions.InternalCall)]
		[GeneratedByOldBindingsGenerator]
		public static extern Shader Find(string name);

		[MethodImpl(MethodImplOptions.InternalCall)]
		[GeneratedByOldBindingsGenerator]
		internal static extern Shader FindBuiltin(string name);

		/// <summary>
		///   <para>Set a global shader keyword.</para>
		/// </summary>
		/// <param name="keyword"></param>
		[MethodImpl(MethodImplOptions.InternalCall)]
		[GeneratedByOldBindingsGenerator]
		public static extern void EnableKeyword(string keyword);

		/// <summary>
		///   <para>Unset a global shader keyword.</para>
		/// </summary>
		/// <param name="keyword"></param>
		[MethodImpl(MethodImplOptions.InternalCall)]
		[GeneratedByOldBindingsGenerator]
		public static extern void DisableKeyword(string keyword);

		/// <summary>
		///   <para>Is global shader keyword enabled?</para>
		/// </summary>
		/// <param name="keyword"></param>
		[MethodImpl(MethodImplOptions.InternalCall)]
		[GeneratedByOldBindingsGenerator]
		public static extern bool IsKeywordEnabled(string keyword);

		[MethodImpl(MethodImplOptions.InternalCall)]
		[GeneratedByOldBindingsGenerator]
		private static extern void SetGlobalFloatImpl(int nameID, float value);

		[MethodImpl(MethodImplOptions.InternalCall)]
		[GeneratedByOldBindingsGenerator]
		private static extern void SetGlobalIntImpl(int nameID, int value);

		private static void SetGlobalVectorImpl(int nameID, Vector4 value)
		{
			INTERNAL_CALL_SetGlobalVectorImpl(nameID, ref value);
		}

		[MethodImpl(MethodImplOptions.InternalCall)]
		[GeneratedByOldBindingsGenerator]
		private static extern void INTERNAL_CALL_SetGlobalVectorImpl(int nameID, ref Vector4 value);

		private static void SetGlobalColorImpl(int nameID, Color value)
		{
			INTERNAL_CALL_SetGlobalColorImpl(nameID, ref value);
		}

		[MethodImpl(MethodImplOptions.InternalCall)]
		[GeneratedByOldBindingsGenerator]
		private static extern void INTERNAL_CALL_SetGlobalColorImpl(int nameID, ref Color value);

		private static void SetGlobalMatrixImpl(int nameID, Matrix4x4 value)
		{
			INTERNAL_CALL_SetGlobalMatrixImpl(nameID, ref value);
		}

		[MethodImpl(MethodImplOptions.InternalCall)]
		[GeneratedByOldBindingsGenerator]
		private static extern void INTERNAL_CALL_SetGlobalMatrixImpl(int nameID, ref Matrix4x4 value);

		[MethodImpl(MethodImplOptions.InternalCall)]
		[GeneratedByOldBindingsGenerator]
		private static extern void SetGlobalTextureImpl(int nameID, Texture value);

		[MethodImpl(MethodImplOptions.InternalCall)]
		[GeneratedByOldBindingsGenerator]
		private static extern void SetGlobalFloatArrayImpl(int nameID, float[] values, int count);

		[MethodImpl(MethodImplOptions.InternalCall)]
		[GeneratedByOldBindingsGenerator]
		private static extern void SetGlobalVectorArrayImpl(int nameID, Vector4[] values, int count);

		[MethodImpl(MethodImplOptions.InternalCall)]
		[GeneratedByOldBindingsGenerator]
		private static extern void SetGlobalMatrixArrayImpl(int nameID, Matrix4x4[] values, int count);

		/// <summary>
		///   <para>Sets a global compute buffer property for all shaders.</para>
		/// </summary>
		/// <param name="name"></param>
		/// <param name="buffer"></param>
		/// <param name="nameID"></param>
		[MethodImpl(MethodImplOptions.InternalCall)]
		[GeneratedByOldBindingsGenerator]
		public static extern void SetGlobalBuffer(int nameID, ComputeBuffer buffer);

		[MethodImpl(MethodImplOptions.InternalCall)]
		[GeneratedByOldBindingsGenerator]
		private static extern float GetGlobalFloatImpl(int nameID);

		[MethodImpl(MethodImplOptions.InternalCall)]
		[GeneratedByOldBindingsGenerator]
		private static extern int GetGlobalIntImpl(int nameID);

		private static Vector4 GetGlobalVectorImpl(int nameID)
		{
			INTERNAL_CALL_GetGlobalVectorImpl(nameID, out Vector4 value);
			return value;
		}

		[MethodImpl(MethodImplOptions.InternalCall)]
		[GeneratedByOldBindingsGenerator]
		private static extern void INTERNAL_CALL_GetGlobalVectorImpl(int nameID, out Vector4 value);

		private static Color GetGlobalColorImpl(int nameID)
		{
			INTERNAL_CALL_GetGlobalColorImpl(nameID, out Color value);
			return value;
		}

		[MethodImpl(MethodImplOptions.InternalCall)]
		[GeneratedByOldBindingsGenerator]
		private static extern void INTERNAL_CALL_GetGlobalColorImpl(int nameID, out Color value);

		private static Matrix4x4 GetGlobalMatrixImpl(int nameID)
		{
			INTERNAL_CALL_GetGlobalMatrixImpl(nameID, out Matrix4x4 value);
			return value;
		}

		[MethodImpl(MethodImplOptions.InternalCall)]
		[GeneratedByOldBindingsGenerator]
		private static extern void INTERNAL_CALL_GetGlobalMatrixImpl(int nameID, out Matrix4x4 value);

		[MethodImpl(MethodImplOptions.InternalCall)]
		[GeneratedByOldBindingsGenerator]
		private static extern Texture GetGlobalTextureImpl(int nameID);

		[MethodImpl(MethodImplOptions.InternalCall)]
		[GeneratedByOldBindingsGenerator]
		private static extern float[] GetGlobalFloatArrayImpl(int nameID);

		[MethodImpl(MethodImplOptions.InternalCall)]
		[GeneratedByOldBindingsGenerator]
		private static extern Vector4[] GetGlobalVectorArrayImpl(int nameID);

		[MethodImpl(MethodImplOptions.InternalCall)]
		[GeneratedByOldBindingsGenerator]
		private static extern Matrix4x4[] GetGlobalMatrixArrayImpl(int nameID);

		[MethodImpl(MethodImplOptions.InternalCall)]
		[GeneratedByOldBindingsGenerator]
		private static extern void GetGlobalFloatArrayImplList(int nameID, object list);

		[MethodImpl(MethodImplOptions.InternalCall)]
		[GeneratedByOldBindingsGenerator]
		private static extern void GetGlobalVectorArrayImplList(int nameID, object list);

		[MethodImpl(MethodImplOptions.InternalCall)]
		[GeneratedByOldBindingsGenerator]
		private static extern void GetGlobalMatrixArrayImplList(int nameID, object list);

		/// <summary>
		///   <para>Gets unique identifier for a shader property name.</para>
		/// </summary>
		/// <param name="name">Shader property name.</param>
		/// <returns>
		///   <para>Unique integer for the name.</para>
		/// </returns>
		[MethodImpl(MethodImplOptions.InternalCall)]
		[ThreadAndSerializationSafe]
		[GeneratedByOldBindingsGenerator]
		public static extern int PropertyToID(string name);

		[MethodImpl(MethodImplOptions.InternalCall)]
		[GeneratedByOldBindingsGenerator]
		internal static extern int TagToID(string name);

		[MethodImpl(MethodImplOptions.InternalCall)]
		[GeneratedByOldBindingsGenerator]
		internal static extern string IDToTag(int id);

		/// <summary>
		///   <para>Fully load all shaders to prevent future performance hiccups.</para>
		/// </summary>
		[MethodImpl(MethodImplOptions.InternalCall)]
		[GeneratedByOldBindingsGenerator]
		public static extern void WarmupAllShaders();

		/// <summary>
		///   <para>Sets a global float property for all shaders.</para>
		/// </summary>
		/// <param name="name"></param>
		/// <param name="value"></param>
		/// <param name="nameID"></param>
		public static void SetGlobalFloat(string name, float value)
		{
			SetGlobalFloat(PropertyToID(name), value);
		}

		/// <summary>
		///   <para>Sets a global float property for all shaders.</para>
		/// </summary>
		/// <param name="name"></param>
		/// <param name="value"></param>
		/// <param name="nameID"></param>
		public static void SetGlobalFloat(int nameID, float value)
		{
			SetGlobalFloatImpl(nameID, value);
		}

		/// <summary>
		///   <para>Sets a global int property for all shaders.</para>
		/// </summary>
		/// <param name="name"></param>
		/// <param name="value"></param>
		/// <param name="nameID"></param>
		public static void SetGlobalInt(string name, int value)
		{
			SetGlobalInt(PropertyToID(name), value);
		}

		/// <summary>
		///   <para>Sets a global int property for all shaders.</para>
		/// </summary>
		/// <param name="name"></param>
		/// <param name="value"></param>
		/// <param name="nameID"></param>
		public static void SetGlobalInt(int nameID, int value)
		{
			SetGlobalIntImpl(nameID, value);
		}

		/// <summary>
		///   <para>Sets a global vector property for all shaders.</para>
		/// </summary>
		/// <param name="name"></param>
		/// <param name="value"></param>
		/// <param name="nameID"></param>
		public static void SetGlobalVector(string name, Vector4 value)
		{
			SetGlobalVector(PropertyToID(name), value);
		}

		/// <summary>
		///   <para>Sets a global vector property for all shaders.</para>
		/// </summary>
		/// <param name="name"></param>
		/// <param name="value"></param>
		/// <param name="nameID"></param>
		public static void SetGlobalVector(int nameID, Vector4 value)
		{
			SetGlobalVectorImpl(nameID, value);
		}

		/// <summary>
		///   <para>Sets a global color property for all shaders.</para>
		/// </summary>
		/// <param name="name"></param>
		/// <param name="value"></param>
		/// <param name="nameID"></param>
		public static void SetGlobalColor(string name, Color value)
		{
			SetGlobalColor(PropertyToID(name), value);
		}

		/// <summary>
		///   <para>Sets a global color property for all shaders.</para>
		/// </summary>
		/// <param name="name"></param>
		/// <param name="value"></param>
		/// <param name="nameID"></param>
		public static void SetGlobalColor(int nameID, Color value)
		{
			SetGlobalColorImpl(nameID, value);
		}

		/// <summary>
		///   <para>Sets a global matrix property for all shaders.</para>
		/// </summary>
		/// <param name="name"></param>
		/// <param name="value"></param>
		/// <param name="nameID"></param>
		public static void SetGlobalMatrix(string name, Matrix4x4 value)
		{
			SetGlobalMatrix(PropertyToID(name), value);
		}

		/// <summary>
		///   <para>Sets a global matrix property for all shaders.</para>
		/// </summary>
		/// <param name="name"></param>
		/// <param name="value"></param>
		/// <param name="nameID"></param>
		public static void SetGlobalMatrix(int nameID, Matrix4x4 value)
		{
			SetGlobalMatrixImpl(nameID, value);
		}

		/// <summary>
		///   <para>Sets a global texture property for all shaders.</para>
		/// </summary>
		/// <param name="name"></param>
		/// <param name="value"></param>
		/// <param name="nameID"></param>
		public static void SetGlobalTexture(string name, Texture value)
		{
			SetGlobalTexture(PropertyToID(name), value);
		}

		/// <summary>
		///   <para>Sets a global texture property for all shaders.</para>
		/// </summary>
		/// <param name="name"></param>
		/// <param name="value"></param>
		/// <param name="nameID"></param>
		public static void SetGlobalTexture(int nameID, Texture value)
		{
			SetGlobalTextureImpl(nameID, value);
		}

		/// <summary>
		///   <para>Sets a global compute buffer property for all shaders.</para>
		/// </summary>
		/// <param name="name"></param>
		/// <param name="buffer"></param>
		/// <param name="nameID"></param>
		public static void SetGlobalBuffer(string name, ComputeBuffer buffer)
		{
			SetGlobalBuffer(PropertyToID(name), buffer);
		}

		public static void SetGlobalFloatArray(string name, List<float> values)
		{
			SetGlobalFloatArray(PropertyToID(name), values);
		}

		public static void SetGlobalFloatArray(int nameID, List<float> values)
		{
			SetGlobalFloatArray(nameID, (float[])NoAllocHelpers.ExtractArrayFromList(values), values.Count);
		}

		/// <summary>
		///   <para>Sets a global float array property for all shaders.</para>
		/// </summary>
		/// <param name="name"></param>
		/// <param name="values"></param>
		/// <param name="nameID"></param>
		public static void SetGlobalFloatArray(string name, float[] values)
		{
			SetGlobalFloatArray(PropertyToID(name), values);
		}

		/// <summary>
		///   <para>Sets a global float array property for all shaders.</para>
		/// </summary>
		/// <param name="name"></param>
		/// <param name="values"></param>
		/// <param name="nameID"></param>
		public static void SetGlobalFloatArray(int nameID, float[] values)
		{
			SetGlobalFloatArray(nameID, values, values.Length);
		}

		private static void SetGlobalFloatArray(int nameID, float[] values, int count)
		{
			if (values == null)
			{
				throw new ArgumentNullException("values");
			}
			if (values.Length == 0)
			{
				throw new ArgumentException("Zero-sized array is not allowed.");
			}
			if (values.Length < count)
			{
				throw new ArgumentException("array has less elements than passed count.");
			}
			SetGlobalFloatArrayImpl(nameID, values, count);
		}

		public static void SetGlobalVectorArray(string name, List<Vector4> values)
		{
			SetGlobalVectorArray(PropertyToID(name), values);
		}

		public static void SetGlobalVectorArray(int nameID, List<Vector4> values)
		{
			SetGlobalVectorArray(nameID, (Vector4[])NoAllocHelpers.ExtractArrayFromList(values), values.Count);
		}

		/// <summary>
		///   <para>Sets a global vector array property for all shaders.</para>
		/// </summary>
		/// <param name="name"></param>
		/// <param name="values"></param>
		/// <param name="nameID"></param>
		public static void SetGlobalVectorArray(string name, Vector4[] values)
		{
			SetGlobalVectorArray(PropertyToID(name), values);
		}

		/// <summary>
		///   <para>Sets a global vector array property for all shaders.</para>
		/// </summary>
		/// <param name="name"></param>
		/// <param name="values"></param>
		/// <param name="nameID"></param>
		public static void SetGlobalVectorArray(int nameID, Vector4[] values)
		{
			SetGlobalVectorArray(nameID, values, values.Length);
		}

		private static void SetGlobalVectorArray(int nameID, Vector4[] values, int count)
		{
			if (values == null)
			{
				throw new ArgumentNullException("values");
			}
			if (values.Length == 0)
			{
				throw new ArgumentException("Zero-sized array is not allowed.");
			}
			if (values.Length < count)
			{
				throw new ArgumentException("array has less elements than passed count.");
			}
			SetGlobalVectorArrayImpl(nameID, values, count);
		}

		public static void SetGlobalMatrixArray(string name, List<Matrix4x4> values)
		{
			SetGlobalMatrixArray(PropertyToID(name), values);
		}

		public static void SetGlobalMatrixArray(int nameID, List<Matrix4x4> values)
		{
			SetGlobalMatrixArray(nameID, (Matrix4x4[])NoAllocHelpers.ExtractArrayFromList(values), values.Count);
		}

		/// <summary>
		///   <para>Sets a global matrix array property for all shaders.</para>
		/// </summary>
		/// <param name="name"></param>
		/// <param name="values"></param>
		/// <param name="nameID"></param>
		public static void SetGlobalMatrixArray(string name, Matrix4x4[] values)
		{
			SetGlobalMatrixArray(PropertyToID(name), values);
		}

		/// <summary>
		///   <para>Sets a global matrix array property for all shaders.</para>
		/// </summary>
		/// <param name="name"></param>
		/// <param name="values"></param>
		/// <param name="nameID"></param>
		public static void SetGlobalMatrixArray(int nameID, Matrix4x4[] values)
		{
			SetGlobalMatrixArray(nameID, values, values.Length);
		}

		private static void SetGlobalMatrixArray(int nameID, Matrix4x4[] values, int count)
		{
			if (values == null)
			{
				throw new ArgumentNullException("values");
			}
			if (values.Length == 0)
			{
				throw new ArgumentException("Zero-sized array is not allowed.");
			}
			if (values.Length < count)
			{
				throw new ArgumentException("array has less elements than passed count.");
			}
			SetGlobalMatrixArrayImpl(nameID, values, count);
		}

		/// <summary>
		///   <para>Gets a global float property for all shaders previously set using SetGlobalFloat.</para>
		/// </summary>
		/// <param name="name"></param>
		/// <param name="nameID"></param>
		public static float GetGlobalFloat(string name)
		{
			return GetGlobalFloat(PropertyToID(name));
		}

		/// <summary>
		///   <para>Gets a global float property for all shaders previously set using SetGlobalFloat.</para>
		/// </summary>
		/// <param name="name"></param>
		/// <param name="nameID"></param>
		public static float GetGlobalFloat(int nameID)
		{
			return GetGlobalFloatImpl(nameID);
		}

		/// <summary>
		///   <para>Gets a global int property for all shaders previously set using SetGlobalInt.</para>
		/// </summary>
		/// <param name="name"></param>
		/// <param name="nameID"></param>
		public static int GetGlobalInt(string name)
		{
			return GetGlobalInt(PropertyToID(name));
		}

		/// <summary>
		///   <para>Gets a global int property for all shaders previously set using SetGlobalInt.</para>
		/// </summary>
		/// <param name="name"></param>
		/// <param name="nameID"></param>
		public static int GetGlobalInt(int nameID)
		{
			return GetGlobalIntImpl(nameID);
		}

		/// <summary>
		///   <para>Gets a global vector property for all shaders previously set using SetGlobalVector.</para>
		/// </summary>
		/// <param name="name"></param>
		/// <param name="nameID"></param>
		public static Vector4 GetGlobalVector(string name)
		{
			return GetGlobalVector(PropertyToID(name));
		}

		/// <summary>
		///   <para>Gets a global vector property for all shaders previously set using SetGlobalVector.</para>
		/// </summary>
		/// <param name="name"></param>
		/// <param name="nameID"></param>
		public static Vector4 GetGlobalVector(int nameID)
		{
			return GetGlobalVectorImpl(nameID);
		}

		/// <summary>
		///   <para>Gets a global color property for all shaders previously set using SetGlobalColor.</para>
		/// </summary>
		/// <param name="name"></param>
		/// <param name="nameID"></param>
		public static Color GetGlobalColor(string name)
		{
			return GetGlobalColor(PropertyToID(name));
		}

		/// <summary>
		///   <para>Gets a global color property for all shaders previously set using SetGlobalColor.</para>
		/// </summary>
		/// <param name="name"></param>
		/// <param name="nameID"></param>
		public static Color GetGlobalColor(int nameID)
		{
			return GetGlobalColorImpl(nameID);
		}

		/// <summary>
		///   <para>Gets a global matrix property for all shaders previously set using SetGlobalMatrix.</para>
		/// </summary>
		/// <param name="name"></param>
		/// <param name="nameID"></param>
		public static Matrix4x4 GetGlobalMatrix(string name)
		{
			return GetGlobalMatrix(PropertyToID(name));
		}

		/// <summary>
		///   <para>Gets a global matrix property for all shaders previously set using SetGlobalMatrix.</para>
		/// </summary>
		/// <param name="name"></param>
		/// <param name="nameID"></param>
		public static Matrix4x4 GetGlobalMatrix(int nameID)
		{
			return GetGlobalMatrixImpl(nameID);
		}

		/// <summary>
		///   <para>Gets a global texture property for all shaders previously set using SetGlobalTexture.</para>
		/// </summary>
		/// <param name="name"></param>
		/// <param name="nameID"></param>
		public static Texture GetGlobalTexture(string name)
		{
			return GetGlobalTexture(PropertyToID(name));
		}

		/// <summary>
		///   <para>Gets a global texture property for all shaders previously set using SetGlobalTexture.</para>
		/// </summary>
		/// <param name="name"></param>
		/// <param name="nameID"></param>
		public static Texture GetGlobalTexture(int nameID)
		{
			return GetGlobalTextureImpl(nameID);
		}

		public static void GetGlobalFloatArray(string name, List<float> values)
		{
			GetGlobalFloatArray(PropertyToID(name), values);
		}

		public static void GetGlobalFloatArray(int nameID, List<float> values)
		{
			if (values == null)
			{
				throw new ArgumentNullException("values");
			}
			GetGlobalFloatArrayImplList(nameID, values);
		}

		/// <summary>
		///   <para>Gets a global float array for all shaders previously set using SetGlobalFloatArray.</para>
		/// </summary>
		/// <param name="name"></param>
		/// <param name="nameID"></param>
		public static float[] GetGlobalFloatArray(string name)
		{
			return GetGlobalFloatArray(PropertyToID(name));
		}

		/// <summary>
		///   <para>Gets a global float array for all shaders previously set using SetGlobalFloatArray.</para>
		/// </summary>
		/// <param name="name"></param>
		/// <param name="nameID"></param>
		public static float[] GetGlobalFloatArray(int nameID)
		{
			return GetGlobalFloatArrayImpl(nameID);
		}

		public static void GetGlobalVectorArray(string name, List<Vector4> values)
		{
			GetGlobalVectorArray(PropertyToID(name), values);
		}

		public static void GetGlobalVectorArray(int nameID, List<Vector4> values)
		{
			if (values == null)
			{
				throw new ArgumentNullException("values");
			}
			GetGlobalVectorArrayImplList(nameID, values);
		}

		/// <summary>
		///   <para>Gets a global vector array for all shaders previously set using SetGlobalVectorArray.</para>
		/// </summary>
		/// <param name="name"></param>
		/// <param name="nameID"></param>
		public static Vector4[] GetGlobalVectorArray(string name)
		{
			return GetGlobalVectorArray(PropertyToID(name));
		}

		/// <summary>
		///   <para>Gets a global vector array for all shaders previously set using SetGlobalVectorArray.</para>
		/// </summary>
		/// <param name="name"></param>
		/// <param name="nameID"></param>
		public static Vector4[] GetGlobalVectorArray(int nameID)
		{
			return GetGlobalVectorArrayImpl(nameID);
		}

		public static void GetGlobalMatrixArray(string name, List<Matrix4x4> values)
		{
			GetGlobalMatrixArray(PropertyToID(name), values);
		}

		public static void GetGlobalMatrixArray(int nameID, List<Matrix4x4> values)
		{
			if (values == null)
			{
				throw new ArgumentNullException("values");
			}
			GetGlobalMatrixArrayImplList(nameID, values);
		}

		/// <summary>
		///   <para>Gets a global matrix array for all shaders previously set using SetGlobalMatrixArray.</para>
		/// </summary>
		/// <param name="name"></param>
		/// <param name="nameID"></param>
		public static Matrix4x4[] GetGlobalMatrixArray(string name)
		{
			return GetGlobalMatrixArray(PropertyToID(name));
		}

		/// <summary>
		///   <para>Gets a global matrix array for all shaders previously set using SetGlobalMatrixArray.</para>
		/// </summary>
		/// <param name="name"></param>
		/// <param name="nameID"></param>
		public static Matrix4x4[] GetGlobalMatrixArray(int nameID)
		{
			return GetGlobalMatrixArrayImpl(nameID);
		}
	}
}
