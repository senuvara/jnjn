using System;
using System.Collections.Generic;
using System.Runtime.CompilerServices;
using UnityEngine.Internal;
using UnityEngine.Scripting;

namespace UnityEngine
{
	/// <summary>
	///   <para>The material class.</para>
	/// </summary>
	public class Material : Object
	{
		/// <summary>
		///   <para>The shader used by the material.</para>
		/// </summary>
		public Shader shader
		{
			[MethodImpl(MethodImplOptions.InternalCall)]
			[GeneratedByOldBindingsGenerator]
			get;
			[MethodImpl(MethodImplOptions.InternalCall)]
			[GeneratedByOldBindingsGenerator]
			set;
		}

		/// <summary>
		///   <para>The main material's color.</para>
		/// </summary>
		public Color color
		{
			get
			{
				return GetColor("_Color");
			}
			set
			{
				SetColor("_Color", value);
			}
		}

		/// <summary>
		///   <para>The material's texture.</para>
		/// </summary>
		public Texture mainTexture
		{
			get
			{
				return GetTexture("_MainTex");
			}
			set
			{
				SetTexture("_MainTex", value);
			}
		}

		/// <summary>
		///   <para>The texture offset of the main texture.</para>
		/// </summary>
		public Vector2 mainTextureOffset
		{
			get
			{
				return GetTextureOffset("_MainTex");
			}
			set
			{
				SetTextureOffset("_MainTex", value);
			}
		}

		/// <summary>
		///   <para>The texture scale of the main texture.</para>
		/// </summary>
		public Vector2 mainTextureScale
		{
			get
			{
				return GetTextureScale("_MainTex");
			}
			set
			{
				SetTextureScale("_MainTex", value);
			}
		}

		/// <summary>
		///   <para>How many passes are in this material (Read Only).</para>
		/// </summary>
		public int passCount
		{
			[MethodImpl(MethodImplOptions.InternalCall)]
			[GeneratedByOldBindingsGenerator]
			get;
		}

		/// <summary>
		///   <para>Render queue of this material.</para>
		/// </summary>
		public int renderQueue
		{
			[MethodImpl(MethodImplOptions.InternalCall)]
			[GeneratedByOldBindingsGenerator]
			get;
			[MethodImpl(MethodImplOptions.InternalCall)]
			[GeneratedByOldBindingsGenerator]
			set;
		}

		/// <summary>
		///   <para>Additional shader keywords set by this material.</para>
		/// </summary>
		public string[] shaderKeywords
		{
			[MethodImpl(MethodImplOptions.InternalCall)]
			[GeneratedByOldBindingsGenerator]
			get;
			[MethodImpl(MethodImplOptions.InternalCall)]
			[GeneratedByOldBindingsGenerator]
			set;
		}

		/// <summary>
		///   <para>Defines how the material should interact with lightmaps and lightprobes.</para>
		/// </summary>
		public MaterialGlobalIlluminationFlags globalIlluminationFlags
		{
			[MethodImpl(MethodImplOptions.InternalCall)]
			[GeneratedByOldBindingsGenerator]
			get;
			[MethodImpl(MethodImplOptions.InternalCall)]
			[GeneratedByOldBindingsGenerator]
			set;
		}

		/// <summary>
		///   <para>Gets and sets whether GPU instancing is enabled for this material.</para>
		/// </summary>
		public bool enableInstancing
		{
			[MethodImpl(MethodImplOptions.InternalCall)]
			[GeneratedByOldBindingsGenerator]
			get;
			[MethodImpl(MethodImplOptions.InternalCall)]
			[GeneratedByOldBindingsGenerator]
			set;
		}

		/// <summary>
		///   <para>Gets and sets whether the Double Sided Global Illumination setting is enabled for this material.</para>
		/// </summary>
		public bool doubleSidedGI
		{
			[MethodImpl(MethodImplOptions.InternalCall)]
			[GeneratedByOldBindingsGenerator]
			get;
			[MethodImpl(MethodImplOptions.InternalCall)]
			[GeneratedByOldBindingsGenerator]
			set;
		}

		/// <summary>
		///   <para></para>
		/// </summary>
		/// <param name="contents"></param>
		[Obsolete("Creating materials from shader source string is no longer supported. Use Shader assets instead.")]
		public Material(string contents)
		{
			Internal_CreateWithString(this, contents);
		}

		/// <summary>
		///   <para>Create a temporary Material.</para>
		/// </summary>
		/// <param name="shader">Create a material with a given Shader.</param>
		/// <param name="source">Create a material by copying all properties from another material.</param>
		public Material(Shader shader)
		{
			Internal_CreateWithShader(this, shader);
		}

		/// <summary>
		///   <para>Create a temporary Material.</para>
		/// </summary>
		/// <param name="shader">Create a material with a given Shader.</param>
		/// <param name="source">Create a material by copying all properties from another material.</param>
		public Material(Material source)
		{
			Internal_CreateWithMaterial(this, source);
		}

		[MethodImpl(MethodImplOptions.InternalCall)]
		[GeneratedByOldBindingsGenerator]
		private extern void SetFloatImpl(int nameID, float value);

		[MethodImpl(MethodImplOptions.InternalCall)]
		[GeneratedByOldBindingsGenerator]
		private extern void SetIntImpl(int nameID, int value);

		private void SetColorImpl(int nameID, Color value)
		{
			INTERNAL_CALL_SetColorImpl(this, nameID, ref value);
		}

		[MethodImpl(MethodImplOptions.InternalCall)]
		[GeneratedByOldBindingsGenerator]
		private static extern void INTERNAL_CALL_SetColorImpl(Material self, int nameID, ref Color value);

		private void SetVectorImpl(int nameID, Vector4 value)
		{
			INTERNAL_CALL_SetVectorImpl(this, nameID, ref value);
		}

		[MethodImpl(MethodImplOptions.InternalCall)]
		[GeneratedByOldBindingsGenerator]
		private static extern void INTERNAL_CALL_SetVectorImpl(Material self, int nameID, ref Vector4 value);

		private void SetMatrixImpl(int nameID, Matrix4x4 value)
		{
			INTERNAL_CALL_SetMatrixImpl(this, nameID, ref value);
		}

		[MethodImpl(MethodImplOptions.InternalCall)]
		[GeneratedByOldBindingsGenerator]
		private static extern void INTERNAL_CALL_SetMatrixImpl(Material self, int nameID, ref Matrix4x4 value);

		[MethodImpl(MethodImplOptions.InternalCall)]
		[GeneratedByOldBindingsGenerator]
		private extern void SetTextureImpl(int nameID, Texture value);

		[MethodImpl(MethodImplOptions.InternalCall)]
		[GeneratedByOldBindingsGenerator]
		private extern void SetBufferImpl(int nameID, ComputeBuffer value);

		[MethodImpl(MethodImplOptions.InternalCall)]
		[GeneratedByOldBindingsGenerator]
		private extern void SetFloatArrayImpl(int nameID, float[] values, int count);

		[MethodImpl(MethodImplOptions.InternalCall)]
		[GeneratedByOldBindingsGenerator]
		private extern void SetVectorArrayImpl(int nameID, Vector4[] values, int count);

		[MethodImpl(MethodImplOptions.InternalCall)]
		[GeneratedByOldBindingsGenerator]
		private extern void SetMatrixArrayImpl(int nameID, Matrix4x4[] values, int count);

		[MethodImpl(MethodImplOptions.InternalCall)]
		[GeneratedByOldBindingsGenerator]
		private extern void SetColorArrayImpl(int nameID, Color[] values, int count);

		[MethodImpl(MethodImplOptions.InternalCall)]
		[GeneratedByOldBindingsGenerator]
		private extern float GetFloatImpl(int nameID);

		[MethodImpl(MethodImplOptions.InternalCall)]
		[GeneratedByOldBindingsGenerator]
		private extern int GetIntImpl(int nameID);

		private Color GetColorImpl(int nameID)
		{
			INTERNAL_CALL_GetColorImpl(this, nameID, out Color value);
			return value;
		}

		[MethodImpl(MethodImplOptions.InternalCall)]
		[GeneratedByOldBindingsGenerator]
		private static extern void INTERNAL_CALL_GetColorImpl(Material self, int nameID, out Color value);

		private Vector4 GetVectorImpl(int nameID)
		{
			INTERNAL_CALL_GetVectorImpl(this, nameID, out Vector4 value);
			return value;
		}

		[MethodImpl(MethodImplOptions.InternalCall)]
		[GeneratedByOldBindingsGenerator]
		private static extern void INTERNAL_CALL_GetVectorImpl(Material self, int nameID, out Vector4 value);

		private Matrix4x4 GetMatrixImpl(int nameID)
		{
			INTERNAL_CALL_GetMatrixImpl(this, nameID, out Matrix4x4 value);
			return value;
		}

		[MethodImpl(MethodImplOptions.InternalCall)]
		[GeneratedByOldBindingsGenerator]
		private static extern void INTERNAL_CALL_GetMatrixImpl(Material self, int nameID, out Matrix4x4 value);

		[MethodImpl(MethodImplOptions.InternalCall)]
		[GeneratedByOldBindingsGenerator]
		private extern Texture GetTextureImpl(int nameID);

		[MethodImpl(MethodImplOptions.InternalCall)]
		[GeneratedByOldBindingsGenerator]
		private extern float[] GetFloatArrayImpl(int nameID);

		[MethodImpl(MethodImplOptions.InternalCall)]
		[GeneratedByOldBindingsGenerator]
		private extern Vector4[] GetVectorArrayImpl(int nameID);

		[MethodImpl(MethodImplOptions.InternalCall)]
		[GeneratedByOldBindingsGenerator]
		private extern Matrix4x4[] GetMatrixArrayImpl(int nameID);

		[MethodImpl(MethodImplOptions.InternalCall)]
		[GeneratedByOldBindingsGenerator]
		private extern void GetFloatArrayImplList(int nameID, object list);

		[MethodImpl(MethodImplOptions.InternalCall)]
		[GeneratedByOldBindingsGenerator]
		private extern void GetVectorArrayImplList(int nameID, object list);

		[MethodImpl(MethodImplOptions.InternalCall)]
		[GeneratedByOldBindingsGenerator]
		private extern void GetMatrixArrayImplList(int nameID, object list);

		[MethodImpl(MethodImplOptions.InternalCall)]
		[GeneratedByOldBindingsGenerator]
		private extern Color[] GetColorArrayImpl(int nameID);

		[MethodImpl(MethodImplOptions.InternalCall)]
		[GeneratedByOldBindingsGenerator]
		private extern void GetColorArrayImplList(int nameID, object list);

		private Vector4 GetTextureScaleAndOffsetImpl(int nameID)
		{
			INTERNAL_CALL_GetTextureScaleAndOffsetImpl(this, nameID, out Vector4 value);
			return value;
		}

		[MethodImpl(MethodImplOptions.InternalCall)]
		[GeneratedByOldBindingsGenerator]
		private static extern void INTERNAL_CALL_GetTextureScaleAndOffsetImpl(Material self, int nameID, out Vector4 value);

		private void SetTextureOffsetImpl(int nameID, Vector2 offset)
		{
			INTERNAL_CALL_SetTextureOffsetImpl(this, nameID, ref offset);
		}

		[MethodImpl(MethodImplOptions.InternalCall)]
		[GeneratedByOldBindingsGenerator]
		private static extern void INTERNAL_CALL_SetTextureOffsetImpl(Material self, int nameID, ref Vector2 offset);

		private void SetTextureScaleImpl(int nameID, Vector2 scale)
		{
			INTERNAL_CALL_SetTextureScaleImpl(this, nameID, ref scale);
		}

		[MethodImpl(MethodImplOptions.InternalCall)]
		[GeneratedByOldBindingsGenerator]
		private static extern void INTERNAL_CALL_SetTextureScaleImpl(Material self, int nameID, ref Vector2 scale);

		/// <summary>
		///   <para>Checks if material's shader has a property of a given name.</para>
		/// </summary>
		/// <param name="propertyName"></param>
		/// <param name="nameID"></param>
		public bool HasProperty(string propertyName)
		{
			return HasProperty(Shader.PropertyToID(propertyName));
		}

		/// <summary>
		///   <para>Checks if material's shader has a property of a given name.</para>
		/// </summary>
		/// <param name="propertyName"></param>
		/// <param name="nameID"></param>
		[MethodImpl(MethodImplOptions.InternalCall)]
		[GeneratedByOldBindingsGenerator]
		public extern bool HasProperty(int nameID);

		/// <summary>
		///   <para>Get the value of material's shader tag.</para>
		/// </summary>
		/// <param name="tag"></param>
		/// <param name="searchFallbacks"></param>
		/// <param name="defaultValue"></param>
		[MethodImpl(MethodImplOptions.InternalCall)]
		[GeneratedByOldBindingsGenerator]
		public extern string GetTag(string tag, bool searchFallbacks, [DefaultValue("\"\"")] string defaultValue);

		/// <summary>
		///   <para>Get the value of material's shader tag.</para>
		/// </summary>
		/// <param name="tag"></param>
		/// <param name="searchFallbacks"></param>
		/// <param name="defaultValue"></param>
		[ExcludeFromDocs]
		public string GetTag(string tag, bool searchFallbacks)
		{
			string defaultValue = "";
			return GetTag(tag, searchFallbacks, defaultValue);
		}

		/// <summary>
		///   <para>Sets an override tag/value on the material.</para>
		/// </summary>
		/// <param name="tag">Name of the tag to set.</param>
		/// <param name="val">Name of the value to set. Empty string to clear the override flag.</param>
		[MethodImpl(MethodImplOptions.InternalCall)]
		[GeneratedByOldBindingsGenerator]
		public extern void SetOverrideTag(string tag, string val);

		/// <summary>
		///   <para>Enables or disables a Shader pass on a per-Material level.</para>
		/// </summary>
		/// <param name="passName">Shader pass name (case insensitive).</param>
		/// <param name="enabled">Flag indicating whether this Shader pass should be enabled.</param>
		[MethodImpl(MethodImplOptions.InternalCall)]
		[GeneratedByOldBindingsGenerator]
		public extern void SetShaderPassEnabled(string passName, bool enabled);

		/// <summary>
		///   <para>Checks whether a given Shader pass is enabled on this Material.</para>
		/// </summary>
		/// <param name="passName">Shader pass name (case insensitive).</param>
		/// <returns>
		///   <para>True if the Shader pass is enabled.</para>
		/// </returns>
		[MethodImpl(MethodImplOptions.InternalCall)]
		[GeneratedByOldBindingsGenerator]
		public extern bool GetShaderPassEnabled(string passName);

		/// <summary>
		///   <para>Interpolate properties between two materials.</para>
		/// </summary>
		/// <param name="start"></param>
		/// <param name="end"></param>
		/// <param name="t"></param>
		[MethodImpl(MethodImplOptions.InternalCall)]
		[GeneratedByOldBindingsGenerator]
		public extern void Lerp(Material start, Material end, float t);

		/// <summary>
		///   <para>Activate the given pass for rendering.</para>
		/// </summary>
		/// <param name="pass">Shader pass number to setup.</param>
		/// <returns>
		///   <para>If false is returned, no rendering should be done.</para>
		/// </returns>
		[MethodImpl(MethodImplOptions.InternalCall)]
		[GeneratedByOldBindingsGenerator]
		public extern bool SetPass(int pass);

		/// <summary>
		///   <para>Returns the name of the shader pass at index pass.</para>
		/// </summary>
		/// <param name="pass"></param>
		[MethodImpl(MethodImplOptions.InternalCall)]
		[GeneratedByOldBindingsGenerator]
		public extern string GetPassName(int pass);

		/// <summary>
		///   <para>Returns the index of the pass passName.</para>
		/// </summary>
		/// <param name="passName"></param>
		[MethodImpl(MethodImplOptions.InternalCall)]
		[GeneratedByOldBindingsGenerator]
		public extern int FindPass(string passName);

		[Obsolete("Creating materials from shader source string will be removed in the future. Use Shader assets instead.")]
		public static Material Create(string scriptContents)
		{
			return new Material(scriptContents);
		}

		[MethodImpl(MethodImplOptions.InternalCall)]
		[GeneratedByOldBindingsGenerator]
		private static extern void Internal_CreateWithString([Writable] Material mono, string contents);

		[MethodImpl(MethodImplOptions.InternalCall)]
		[GeneratedByOldBindingsGenerator]
		private static extern void Internal_CreateWithShader([Writable] Material mono, Shader shader);

		[MethodImpl(MethodImplOptions.InternalCall)]
		[GeneratedByOldBindingsGenerator]
		private static extern void Internal_CreateWithMaterial([Writable] Material mono, Material source);

		/// <summary>
		///   <para>Copy properties from other material into this material.</para>
		/// </summary>
		/// <param name="mat"></param>
		[MethodImpl(MethodImplOptions.InternalCall)]
		[GeneratedByOldBindingsGenerator]
		public extern void CopyPropertiesFromMaterial(Material mat);

		/// <summary>
		///   <para>Sets a shader keyword that is enabled by this material.</para>
		/// </summary>
		/// <param name="keyword"></param>
		[MethodImpl(MethodImplOptions.InternalCall)]
		[GeneratedByOldBindingsGenerator]
		public extern void EnableKeyword(string keyword);

		/// <summary>
		///   <para>Unset a shader keyword.</para>
		/// </summary>
		/// <param name="keyword"></param>
		[MethodImpl(MethodImplOptions.InternalCall)]
		[GeneratedByOldBindingsGenerator]
		public extern void DisableKeyword(string keyword);

		/// <summary>
		///   <para>Is the shader keyword enabled on this material?</para>
		/// </summary>
		/// <param name="keyword"></param>
		[MethodImpl(MethodImplOptions.InternalCall)]
		[GeneratedByOldBindingsGenerator]
		public extern bool IsKeywordEnabled(string keyword);

		/// <summary>
		///   <para>Sets a named float value.</para>
		/// </summary>
		/// <param name="nameID">Property name ID, use Shader.PropertyToID to get it.</param>
		/// <param name="value">Float value to set.</param>
		/// <param name="name">Property name, e.g. "_Glossiness".</param>
		public void SetFloat(string name, float value)
		{
			SetFloat(Shader.PropertyToID(name), value);
		}

		/// <summary>
		///   <para>Sets a named float value.</para>
		/// </summary>
		/// <param name="nameID">Property name ID, use Shader.PropertyToID to get it.</param>
		/// <param name="value">Float value to set.</param>
		/// <param name="name">Property name, e.g. "_Glossiness".</param>
		public void SetFloat(int nameID, float value)
		{
			SetFloatImpl(nameID, value);
		}

		/// <summary>
		///   <para>Sets a named integer value.</para>
		/// </summary>
		/// <param name="nameID">Property name ID, use Shader.PropertyToID to get it.</param>
		/// <param name="value">Integer value to set.</param>
		/// <param name="name">Property name, e.g. "_SrcBlend".</param>
		public void SetInt(string name, int value)
		{
			SetInt(Shader.PropertyToID(name), value);
		}

		/// <summary>
		///   <para>Sets a named integer value.</para>
		/// </summary>
		/// <param name="nameID">Property name ID, use Shader.PropertyToID to get it.</param>
		/// <param name="value">Integer value to set.</param>
		/// <param name="name">Property name, e.g. "_SrcBlend".</param>
		public void SetInt(int nameID, int value)
		{
			SetIntImpl(nameID, value);
		}

		/// <summary>
		///   <para>Sets a named color value.</para>
		/// </summary>
		/// <param name="nameID">Property name ID, use Shader.PropertyToID to get it.</param>
		/// <param name="name">Property name, e.g. "_Color".</param>
		/// <param name="value">Color value to set.</param>
		public void SetColor(string name, Color value)
		{
			SetColor(Shader.PropertyToID(name), value);
		}

		/// <summary>
		///   <para>Sets a named color value.</para>
		/// </summary>
		/// <param name="nameID">Property name ID, use Shader.PropertyToID to get it.</param>
		/// <param name="name">Property name, e.g. "_Color".</param>
		/// <param name="value">Color value to set.</param>
		public void SetColor(int nameID, Color value)
		{
			SetColorImpl(nameID, value);
		}

		/// <summary>
		///   <para>Sets a named vector value.</para>
		/// </summary>
		/// <param name="nameID">Property name ID, use Shader.PropertyToID to get it.</param>
		/// <param name="name">Property name, e.g. "_WaveAndDistance".</param>
		/// <param name="value">Vector value to set.</param>
		public void SetVector(string name, Vector4 value)
		{
			SetVector(Shader.PropertyToID(name), value);
		}

		/// <summary>
		///   <para>Sets a named vector value.</para>
		/// </summary>
		/// <param name="nameID">Property name ID, use Shader.PropertyToID to get it.</param>
		/// <param name="name">Property name, e.g. "_WaveAndDistance".</param>
		/// <param name="value">Vector value to set.</param>
		public void SetVector(int nameID, Vector4 value)
		{
			SetVectorImpl(nameID, value);
		}

		/// <summary>
		///   <para>Sets a named matrix for the shader.</para>
		/// </summary>
		/// <param name="nameID">Property name ID, use Shader.PropertyToID to get it.</param>
		/// <param name="name">Property name, e.g. "_CubemapRotation".</param>
		/// <param name="value">Matrix value to set.</param>
		public void SetMatrix(string name, Matrix4x4 value)
		{
			SetMatrix(Shader.PropertyToID(name), value);
		}

		/// <summary>
		///   <para>Sets a named matrix for the shader.</para>
		/// </summary>
		/// <param name="nameID">Property name ID, use Shader.PropertyToID to get it.</param>
		/// <param name="name">Property name, e.g. "_CubemapRotation".</param>
		/// <param name="value">Matrix value to set.</param>
		public void SetMatrix(int nameID, Matrix4x4 value)
		{
			SetMatrixImpl(nameID, value);
		}

		/// <summary>
		///   <para>Sets a named texture.</para>
		/// </summary>
		/// <param name="nameID">Property name ID, use Shader.PropertyToID to get it.</param>
		/// <param name="name">Property name, e.g. "_MainTex".</param>
		/// <param name="value">Texture to set.</param>
		public void SetTexture(string name, Texture value)
		{
			SetTexture(Shader.PropertyToID(name), value);
		}

		/// <summary>
		///   <para>Sets a named texture.</para>
		/// </summary>
		/// <param name="nameID">Property name ID, use Shader.PropertyToID to get it.</param>
		/// <param name="name">Property name, e.g. "_MainTex".</param>
		/// <param name="value">Texture to set.</param>
		public void SetTexture(int nameID, Texture value)
		{
			SetTextureImpl(nameID, value);
		}

		/// <summary>
		///   <para>Sets a named ComputeBuffer value.</para>
		/// </summary>
		/// <param name="nameID">Property name ID, use Shader.PropertyToID to get it.</param>
		/// <param name="name">Property name.</param>
		/// <param name="value">The ComputeBuffer value to set.</param>
		public void SetBuffer(string name, ComputeBuffer value)
		{
			SetBuffer(Shader.PropertyToID(name), value);
		}

		/// <summary>
		///   <para>Sets a named ComputeBuffer value.</para>
		/// </summary>
		/// <param name="nameID">Property name ID, use Shader.PropertyToID to get it.</param>
		/// <param name="name">Property name.</param>
		/// <param name="value">The ComputeBuffer value to set.</param>
		public void SetBuffer(int nameID, ComputeBuffer value)
		{
			SetBufferImpl(nameID, value);
		}

		/// <summary>
		///   <para>Sets the placement offset of texture propertyName.</para>
		/// </summary>
		/// <param name="nameID">Property name ID, use Shader.PropertyToID to get it.</param>
		/// <param name="name">Property name, for example: "_MainTex".</param>
		/// <param name="value">Texture placement offset.</param>
		public void SetTextureOffset(string name, Vector2 value)
		{
			SetTextureOffset(Shader.PropertyToID(name), value);
		}

		/// <summary>
		///   <para>Sets the placement offset of texture propertyName.</para>
		/// </summary>
		/// <param name="nameID">Property name ID, use Shader.PropertyToID to get it.</param>
		/// <param name="name">Property name, for example: "_MainTex".</param>
		/// <param name="value">Texture placement offset.</param>
		public void SetTextureOffset(int nameID, Vector2 value)
		{
			SetTextureOffsetImpl(nameID, value);
		}

		/// <summary>
		///   <para>Sets the placement scale of texture propertyName.</para>
		/// </summary>
		/// <param name="nameID">Property name ID, use Shader.PropertyToID to get it.</param>
		/// <param name="name">Property name, e.g. "_MainTex".</param>
		/// <param name="value">Texture placement scale.</param>
		public void SetTextureScale(string name, Vector2 value)
		{
			SetTextureScale(Shader.PropertyToID(name), value);
		}

		/// <summary>
		///   <para>Sets the placement scale of texture propertyName.</para>
		/// </summary>
		/// <param name="nameID">Property name ID, use Shader.PropertyToID to get it.</param>
		/// <param name="name">Property name, e.g. "_MainTex".</param>
		/// <param name="value">Texture placement scale.</param>
		public void SetTextureScale(int nameID, Vector2 value)
		{
			SetTextureScaleImpl(nameID, value);
		}

		public void SetFloatArray(string name, List<float> values)
		{
			SetFloatArray(Shader.PropertyToID(name), values);
		}

		public void SetFloatArray(int nameID, List<float> values)
		{
			SetFloatArray(nameID, (float[])NoAllocHelpers.ExtractArrayFromList(values), values.Count);
		}

		/// <summary>
		///   <para>Sets a float array property.</para>
		/// </summary>
		/// <param name="name">Property name.</param>
		/// <param name="nameID">Property name ID. Use Shader.PropertyToID to get this ID.</param>
		/// <param name="values">Array of values to set.</param>
		public void SetFloatArray(string name, float[] values)
		{
			SetFloatArray(Shader.PropertyToID(name), values);
		}

		/// <summary>
		///   <para>Sets a float array property.</para>
		/// </summary>
		/// <param name="name">Property name.</param>
		/// <param name="nameID">Property name ID. Use Shader.PropertyToID to get this ID.</param>
		/// <param name="values">Array of values to set.</param>
		public void SetFloatArray(int nameID, float[] values)
		{
			SetFloatArray(nameID, values, values.Length);
		}

		private void SetFloatArray(int nameID, float[] values, int count)
		{
			if (values == null)
			{
				throw new ArgumentNullException("values");
			}
			if (values.Length == 0)
			{
				throw new ArgumentException("Zero-sized array is not allowed.");
			}
			if (values.Length < count)
			{
				throw new ArgumentException("array has less elements than passed count.");
			}
			SetFloatArrayImpl(nameID, values, count);
		}

		public void SetColorArray(string name, List<Color> values)
		{
			SetColorArray(Shader.PropertyToID(name), values);
		}

		public void SetColorArray(int nameID, List<Color> values)
		{
			SetColorArray(nameID, (Color[])NoAllocHelpers.ExtractArrayFromList(values), values.Count);
		}

		/// <summary>
		///   <para>Sets a color array property.</para>
		/// </summary>
		/// <param name="name">Property name.</param>
		/// <param name="nameID">Property name ID, use Shader.PropertyToID to get it.</param>
		/// <param name="values">Array of values to set.</param>
		public void SetColorArray(string name, Color[] values)
		{
			SetColorArray(Shader.PropertyToID(name), values);
		}

		/// <summary>
		///   <para>Sets a color array property.</para>
		/// </summary>
		/// <param name="name">Property name.</param>
		/// <param name="nameID">Property name ID, use Shader.PropertyToID to get it.</param>
		/// <param name="values">Array of values to set.</param>
		public void SetColorArray(int nameID, Color[] values)
		{
			SetColorArray(nameID, values, values.Length);
		}

		private void SetColorArray(int nameID, Color[] values, int count)
		{
			if (values == null)
			{
				throw new ArgumentNullException("values");
			}
			if (values.Length == 0)
			{
				throw new ArgumentException("Zero-sized array is not allowed.");
			}
			if (values.Length < count)
			{
				throw new ArgumentException("array has less elements than passed count.");
			}
			SetColorArrayImpl(nameID, values, count);
		}

		public void SetVectorArray(string name, List<Vector4> values)
		{
			SetVectorArray(Shader.PropertyToID(name), values);
		}

		public void SetVectorArray(int nameID, List<Vector4> values)
		{
			SetVectorArray(nameID, (Vector4[])NoAllocHelpers.ExtractArrayFromList(values), values.Count);
		}

		/// <summary>
		///   <para>Sets a vector array property.</para>
		/// </summary>
		/// <param name="name">Property name.</param>
		/// <param name="values">Array of values to set.</param>
		/// <param name="nameID">Property name ID, use Shader.PropertyToID to get it.</param>
		public void SetVectorArray(string name, Vector4[] values)
		{
			SetVectorArray(Shader.PropertyToID(name), values);
		}

		/// <summary>
		///   <para>Sets a vector array property.</para>
		/// </summary>
		/// <param name="name">Property name.</param>
		/// <param name="values">Array of values to set.</param>
		/// <param name="nameID">Property name ID, use Shader.PropertyToID to get it.</param>
		public void SetVectorArray(int nameID, Vector4[] values)
		{
			SetVectorArray(nameID, values, values.Length);
		}

		private void SetVectorArray(int nameID, Vector4[] values, int count)
		{
			if (values == null)
			{
				throw new ArgumentNullException("values");
			}
			if (values.Length == 0)
			{
				throw new ArgumentException("Zero-sized array is not allowed.");
			}
			if (values.Length < count)
			{
				throw new ArgumentException("array has less elements than passed count.");
			}
			SetVectorArrayImpl(nameID, values, count);
		}

		public void SetMatrixArray(string name, List<Matrix4x4> values)
		{
			SetMatrixArray(Shader.PropertyToID(name), values);
		}

		public void SetMatrixArray(int nameID, List<Matrix4x4> values)
		{
			SetMatrixArray(nameID, (Matrix4x4[])NoAllocHelpers.ExtractArrayFromList(values), values.Count);
		}

		/// <summary>
		///   <para>Sets a matrix array property.</para>
		/// </summary>
		/// <param name="name">Property name.</param>
		/// <param name="values">Array of values to set.</param>
		/// <param name="nameID">Property name ID, use Shader.PropertyToID to get it.</param>
		public void SetMatrixArray(string name, Matrix4x4[] values)
		{
			SetMatrixArray(Shader.PropertyToID(name), values);
		}

		/// <summary>
		///   <para>Sets a matrix array property.</para>
		/// </summary>
		/// <param name="name">Property name.</param>
		/// <param name="values">Array of values to set.</param>
		/// <param name="nameID">Property name ID, use Shader.PropertyToID to get it.</param>
		public void SetMatrixArray(int nameID, Matrix4x4[] values)
		{
			SetMatrixArray(nameID, values, values.Length);
		}

		private void SetMatrixArray(int nameID, Matrix4x4[] values, int count)
		{
			if (values == null)
			{
				throw new ArgumentNullException("values");
			}
			if (values.Length == 0)
			{
				throw new ArgumentException("Zero-sized array is not allowed.");
			}
			if (values.Length < count)
			{
				throw new ArgumentException("array has less elements than passed count.");
			}
			SetMatrixArrayImpl(nameID, values, count);
		}

		/// <summary>
		///   <para>Get a named float value.</para>
		/// </summary>
		/// <param name="nameID">The name ID of the property retrieved by Shader.PropertyToID.</param>
		/// <param name="name">The name of the property.</param>
		public float GetFloat(string name)
		{
			return GetFloat(Shader.PropertyToID(name));
		}

		/// <summary>
		///   <para>Get a named float value.</para>
		/// </summary>
		/// <param name="nameID">The name ID of the property retrieved by Shader.PropertyToID.</param>
		/// <param name="name">The name of the property.</param>
		public float GetFloat(int nameID)
		{
			return GetFloatImpl(nameID);
		}

		/// <summary>
		///   <para>Get a named integer value.</para>
		/// </summary>
		/// <param name="nameID">The name ID of the property retrieved by Shader.PropertyToID.</param>
		/// <param name="name">The name of the property.</param>
		public int GetInt(string name)
		{
			return GetInt(Shader.PropertyToID(name));
		}

		/// <summary>
		///   <para>Get a named integer value.</para>
		/// </summary>
		/// <param name="nameID">The name ID of the property retrieved by Shader.PropertyToID.</param>
		/// <param name="name">The name of the property.</param>
		public int GetInt(int nameID)
		{
			return GetIntImpl(nameID);
		}

		/// <summary>
		///   <para>Get a named color value.</para>
		/// </summary>
		/// <param name="nameID">The name ID of the property retrieved by Shader.PropertyToID.</param>
		/// <param name="name">The name of the property.</param>
		public Color GetColor(string name)
		{
			return GetColor(Shader.PropertyToID(name));
		}

		/// <summary>
		///   <para>Get a named color value.</para>
		/// </summary>
		/// <param name="nameID">The name ID of the property retrieved by Shader.PropertyToID.</param>
		/// <param name="name">The name of the property.</param>
		public Color GetColor(int nameID)
		{
			return GetColorImpl(nameID);
		}

		/// <summary>
		///   <para>Get a named vector value.</para>
		/// </summary>
		/// <param name="nameID">The name ID of the property retrieved by Shader.PropertyToID.</param>
		/// <param name="name">The name of the property.</param>
		public Vector4 GetVector(string name)
		{
			return GetVector(Shader.PropertyToID(name));
		}

		/// <summary>
		///   <para>Get a named vector value.</para>
		/// </summary>
		/// <param name="nameID">The name ID of the property retrieved by Shader.PropertyToID.</param>
		/// <param name="name">The name of the property.</param>
		public Vector4 GetVector(int nameID)
		{
			return GetVectorImpl(nameID);
		}

		/// <summary>
		///   <para>Get a named matrix value from the shader.</para>
		/// </summary>
		/// <param name="nameID">The name ID of the property retrieved by Shader.PropertyToID.</param>
		/// <param name="name">The name of the property.</param>
		public Matrix4x4 GetMatrix(string name)
		{
			return GetMatrix(Shader.PropertyToID(name));
		}

		/// <summary>
		///   <para>Get a named matrix value from the shader.</para>
		/// </summary>
		/// <param name="nameID">The name ID of the property retrieved by Shader.PropertyToID.</param>
		/// <param name="name">The name of the property.</param>
		public Matrix4x4 GetMatrix(int nameID)
		{
			return GetMatrixImpl(nameID);
		}

		public void GetFloatArray(string name, List<float> values)
		{
			GetFloatArray(Shader.PropertyToID(name), values);
		}

		public void GetFloatArray(int nameID, List<float> values)
		{
			if (values == null)
			{
				throw new ArgumentNullException("values");
			}
			GetFloatArrayImplList(nameID, values);
		}

		/// <summary>
		///   <para>Get a named float array.</para>
		/// </summary>
		/// <param name="name">The name of the property.</param>
		/// <param name="nameID">The name ID of the property retrieved by Shader.PropertyToID.</param>
		public float[] GetFloatArray(string name)
		{
			return GetFloatArray(Shader.PropertyToID(name));
		}

		/// <summary>
		///   <para>Get a named float array.</para>
		/// </summary>
		/// <param name="name">The name of the property.</param>
		/// <param name="nameID">The name ID of the property retrieved by Shader.PropertyToID.</param>
		public float[] GetFloatArray(int nameID)
		{
			return GetFloatArrayImpl(nameID);
		}

		public void GetVectorArray(string name, List<Vector4> values)
		{
			GetVectorArray(Shader.PropertyToID(name), values);
		}

		public void GetVectorArray(int nameID, List<Vector4> values)
		{
			if (values == null)
			{
				throw new ArgumentNullException("values");
			}
			GetVectorArrayImplList(nameID, values);
		}

		/// <summary>
		///   <para>Get a named color array.</para>
		/// </summary>
		/// <param name="nameID">The name ID of the property retrieved by Shader.PropertyToID.</param>
		/// <param name="name">The name of the property.</param>
		public Color[] GetColorArray(string name)
		{
			return GetColorArray(Shader.PropertyToID(name));
		}

		/// <summary>
		///   <para>Get a named color array.</para>
		/// </summary>
		/// <param name="nameID">The name ID of the property retrieved by Shader.PropertyToID.</param>
		/// <param name="name">The name of the property.</param>
		public Color[] GetColorArray(int nameID)
		{
			return GetColorArrayImpl(nameID);
		}

		public void GetColorArray(string name, List<Color> values)
		{
			GetColorArray(Shader.PropertyToID(name), values);
		}

		public void GetColorArray(int nameID, List<Color> values)
		{
			if (values == null)
			{
				throw new ArgumentNullException("values");
			}
			GetColorArrayImplList(nameID, values);
		}

		/// <summary>
		///   <para>Get a named vector array.</para>
		/// </summary>
		/// <param name="name">The name of the property.</param>
		/// <param name="nameID">The name ID of the property retrieved by Shader.PropertyToID.</param>
		public Vector4[] GetVectorArray(string name)
		{
			return GetVectorArray(Shader.PropertyToID(name));
		}

		/// <summary>
		///   <para>Get a named vector array.</para>
		/// </summary>
		/// <param name="name">The name of the property.</param>
		/// <param name="nameID">The name ID of the property retrieved by Shader.PropertyToID.</param>
		public Vector4[] GetVectorArray(int nameID)
		{
			return GetVectorArrayImpl(nameID);
		}

		public void GetMatrixArray(string name, List<Matrix4x4> values)
		{
			GetMatrixArray(Shader.PropertyToID(name), values);
		}

		public void GetMatrixArray(int nameID, List<Matrix4x4> values)
		{
			if (values == null)
			{
				throw new ArgumentNullException("values");
			}
			GetMatrixArrayImplList(nameID, values);
		}

		/// <summary>
		///   <para>Get a named matrix array.</para>
		/// </summary>
		/// <param name="name">The name of the property.</param>
		/// <param name="nameID">The name ID of the property retrieved by Shader.PropertyToID.</param>
		public Matrix4x4[] GetMatrixArray(string name)
		{
			return GetMatrixArray(Shader.PropertyToID(name));
		}

		/// <summary>
		///   <para>Get a named matrix array.</para>
		/// </summary>
		/// <param name="name">The name of the property.</param>
		/// <param name="nameID">The name ID of the property retrieved by Shader.PropertyToID.</param>
		public Matrix4x4[] GetMatrixArray(int nameID)
		{
			return GetMatrixArrayImpl(nameID);
		}

		/// <summary>
		///   <para>Get a named texture.</para>
		/// </summary>
		/// <param name="nameID">The name ID of the property retrieved by Shader.PropertyToID.</param>
		/// <param name="name">The name of the property.</param>
		public Texture GetTexture(string name)
		{
			return GetTexture(Shader.PropertyToID(name));
		}

		/// <summary>
		///   <para>Get a named texture.</para>
		/// </summary>
		/// <param name="nameID">The name ID of the property retrieved by Shader.PropertyToID.</param>
		/// <param name="name">The name of the property.</param>
		public Texture GetTexture(int nameID)
		{
			return GetTextureImpl(nameID);
		}

		/// <summary>
		///   <para>Gets the placement offset of texture propertyName.</para>
		/// </summary>
		/// <param name="name">The name of the property.</param>
		public Vector2 GetTextureOffset(string name)
		{
			return GetTextureOffset(Shader.PropertyToID(name));
		}

		public Vector2 GetTextureOffset(int nameID)
		{
			Vector4 textureScaleAndOffsetImpl = GetTextureScaleAndOffsetImpl(nameID);
			return new Vector2(textureScaleAndOffsetImpl.z, textureScaleAndOffsetImpl.w);
		}

		/// <summary>
		///   <para>Gets the placement scale of texture propertyName.</para>
		/// </summary>
		/// <param name="name">The name of the property.</param>
		public Vector2 GetTextureScale(string name)
		{
			return GetTextureScale(Shader.PropertyToID(name));
		}

		public Vector2 GetTextureScale(int nameID)
		{
			Vector4 textureScaleAndOffsetImpl = GetTextureScaleAndOffsetImpl(nameID);
			return new Vector2(textureScaleAndOffsetImpl.x, textureScaleAndOffsetImpl.y);
		}
	}
}
