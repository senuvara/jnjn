using System;

namespace UnityEngine
{
	[Flags]
	internal enum AtomicSafetyHandleVersionMask
	{
		Read = 0x1,
		Write = 0x2,
		Dispose = 0x4,
		ReadAndWrite = 0x3,
		ReadWriteAndDispose = 0x7,
		WriteInv = -3,
		ReadInv = -2,
		ReadAndWriteInv = -4,
		ReadWriteAndDisposeInv = -8
	}
}
