using System.Runtime.CompilerServices;
using UnityEngine.Internal;
using UnityEngine.Scripting;

namespace UnityEngine
{
	/// <summary>
	///   <para>Accesses remote settings (common for all game instances).</para>
	/// </summary>
	public static class RemoteSettings
	{
		/// <summary>
		///   <para>This event occurs when a new RemoteSettings is fetched and successfully parsed from the server.</para>
		/// </summary>
		public delegate void UpdatedEventHandler();

		public static event UpdatedEventHandler Updated;

		[RequiredByNativeCode]
		public static void CallOnUpdate()
		{
			RemoteSettings.Updated?.Invoke();
		}

		/// <summary>
		///   <para>Forces the game to download the newest settings from the server and update its values.</para>
		/// </summary>
		[MethodImpl(MethodImplOptions.InternalCall)]
		[GeneratedByOldBindingsGenerator]
		public static extern void ForceUpdate();

		/// <summary>
		///   <para>Returns the value corresponding to key in the remote settings if it exists.</para>
		/// </summary>
		/// <param name="key"></param>
		/// <param name="defaultValue"></param>
		[MethodImpl(MethodImplOptions.InternalCall)]
		[GeneratedByOldBindingsGenerator]
		public static extern int GetInt(string key, [DefaultValue("0")] int defaultValue);

		/// <summary>
		///   <para>Returns the value corresponding to key in the remote settings if it exists.</para>
		/// </summary>
		/// <param name="key"></param>
		/// <param name="defaultValue"></param>
		[ExcludeFromDocs]
		public static int GetInt(string key)
		{
			int defaultValue = 0;
			return GetInt(key, defaultValue);
		}

		/// <summary>
		///   <para>Returns the value corresponding to key in the remote settings if it exists.</para>
		/// </summary>
		/// <param name="key"></param>
		/// <param name="defaultValue"></param>
		[MethodImpl(MethodImplOptions.InternalCall)]
		[GeneratedByOldBindingsGenerator]
		public static extern float GetFloat(string key, [DefaultValue("0.0F")] float defaultValue);

		/// <summary>
		///   <para>Returns the value corresponding to key in the remote settings if it exists.</para>
		/// </summary>
		/// <param name="key"></param>
		/// <param name="defaultValue"></param>
		[ExcludeFromDocs]
		public static float GetFloat(string key)
		{
			float defaultValue = 0f;
			return GetFloat(key, defaultValue);
		}

		/// <summary>
		///   <para>Returns the value corresponding to key in the remote settings if it exists.</para>
		/// </summary>
		/// <param name="key"></param>
		/// <param name="defaultValue"></param>
		[MethodImpl(MethodImplOptions.InternalCall)]
		[GeneratedByOldBindingsGenerator]
		public static extern string GetString(string key, [DefaultValue("\"\"")] string defaultValue);

		/// <summary>
		///   <para>Returns the value corresponding to key in the remote settings if it exists.</para>
		/// </summary>
		/// <param name="key"></param>
		/// <param name="defaultValue"></param>
		[ExcludeFromDocs]
		public static string GetString(string key)
		{
			string defaultValue = "";
			return GetString(key, defaultValue);
		}

		/// <summary>
		///   <para>Returns the value corresponding to key in the remote settings if it exists.</para>
		/// </summary>
		/// <param name="key"></param>
		/// <param name="defaultValue"></param>
		[MethodImpl(MethodImplOptions.InternalCall)]
		[GeneratedByOldBindingsGenerator]
		public static extern bool GetBool(string key, [DefaultValue("false")] bool defaultValue);

		/// <summary>
		///   <para>Returns the value corresponding to key in the remote settings if it exists.</para>
		/// </summary>
		/// <param name="key"></param>
		/// <param name="defaultValue"></param>
		[ExcludeFromDocs]
		public static bool GetBool(string key)
		{
			bool defaultValue = false;
			return GetBool(key, defaultValue);
		}

		/// <summary>
		///   <para>Returns true if key exists in the remote settings.</para>
		/// </summary>
		/// <param name="key"></param>
		[MethodImpl(MethodImplOptions.InternalCall)]
		[GeneratedByOldBindingsGenerator]
		public static extern bool HasKey(string key);

		/// <summary>
		///   <para>Returns number of keys in remote settings.</para>
		/// </summary>
		[MethodImpl(MethodImplOptions.InternalCall)]
		[GeneratedByOldBindingsGenerator]
		public static extern int GetCount();

		/// <summary>
		///   <para>Returns all the keys in remote settings.</para>
		/// </summary>
		[MethodImpl(MethodImplOptions.InternalCall)]
		[GeneratedByOldBindingsGenerator]
		public static extern string[] GetKeys();
	}
}
