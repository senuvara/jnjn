using System;

namespace UnityEngine.XR
{
	[Flags]
	internal enum AvailableTrackingData
	{
		None = 0x0,
		PositionAvailable = 0x1,
		RotationAvailable = 0x2,
		VelocityAvailable = 0x4,
		AngularVelocityAvailable = 0x8,
		AccelerationAvailable = 0x10,
		AngularAccelerationAvailable = 0x20
	}
}
