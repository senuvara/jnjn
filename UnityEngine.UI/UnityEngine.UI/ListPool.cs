using System.Collections.Generic;
using System.Runtime.CompilerServices;
using UnityEngine.Events;

namespace UnityEngine.UI
{
	internal static class ListPool<T>
	{
		private static readonly ObjectPool<List<T>> s_ListPool = new ObjectPool<List<T>>(null, Clear);

		[CompilerGenerated]
		private static UnityAction<List<T>> _003C_003Ef__mg_0024cache0;

		private static void Clear(List<T> l)
		{
			l.Clear();
		}

		public static List<T> Get()
		{
			return s_ListPool.Get();
		}

		public static void Release(List<T> toRelease)
		{
			s_ListPool.Release(toRelease);
		}
	}
}
