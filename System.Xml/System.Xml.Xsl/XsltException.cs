using System.Globalization;
using System.Runtime.Serialization;
using System.Xml.XPath;

namespace System.Xml.Xsl
{
	/// <summary>The exception that is thrown when an error occurs while processing an XSLT transformation.</summary>
	[Serializable]
	public class XsltException : SystemException
	{
		private int lineNumber;

		private int linePosition;

		private string sourceUri;

		private string templateFrames;

		/// <summary>Gets the line number indicating where the error occurred in the style sheet.</summary>
		/// <returns>The line number indicating where the error occurred in the style sheet.</returns>
		public virtual int LineNumber => lineNumber;

		/// <summary>Gets the line position indicating where the error occurred in the style sheet.</summary>
		/// <returns>The line position indicating where the error occurred in the style sheet.</returns>
		public virtual int LinePosition => linePosition;

		/// <summary>Gets the formatted error message describing the current exception.</summary>
		/// <returns>The formatted error message describing the current exception.</returns>
		public override string Message => (templateFrames == null) ? base.Message : (base.Message + templateFrames);

		/// <summary>Gets the location path of the style sheet.</summary>
		/// <returns>The location path of the style sheet.</returns>
		public virtual string SourceUri => sourceUri;

		/// <summary>Initializes a new instance of the <see cref="T:System.Xml.Xsl.XsltException" /> class.</summary>
		public XsltException()
			: this(string.Empty, null)
		{
		}

		/// <summary>Initializes a new instance of the <see cref="T:System.Xml.Xsl.XsltException" /> class with a specified error message. </summary>
		/// <param name="message">The message that describes the error.</param>
		public XsltException(string message)
			: this(message, null)
		{
		}

		/// <summary>Initializes a new instance of the XsltException class.</summary>
		/// <param name="message">The description of the error condition. </param>
		/// <param name="innerException">The <see cref="T:System.Exception" /> which threw the XsltException, if any. This value can be null. </param>
		public XsltException(string message, Exception innerException)
			: this("{0}", message, innerException, 0, 0, null)
		{
		}

		/// <summary>Initializes a new instance of the XsltException class using the information in the <see cref="T:System.Runtime.Serialization.SerializationInfo" /> and <see cref="T:System.Runtime.Serialization.StreamingContext" /> objects.</summary>
		/// <param name="info">The SerializationInfo object containing all the properties of an XsltException. </param>
		/// <param name="context">The StreamingContext object. </param>
		protected XsltException(SerializationInfo info, StreamingContext context)
		{
			lineNumber = info.GetInt32("lineNumber");
			linePosition = info.GetInt32("linePosition");
			sourceUri = info.GetString("sourceUri");
			templateFrames = info.GetString("templateFrames");
		}

		internal XsltException(string msgFormat, string message, Exception innerException, int lineNumber, int linePosition, string sourceUri)
			: base(CreateMessage(msgFormat, message, lineNumber, linePosition, sourceUri), innerException)
		{
			this.lineNumber = lineNumber;
			this.linePosition = linePosition;
			this.sourceUri = sourceUri;
		}

		internal XsltException(string message, Exception innerException, XPathNavigator nav)
			: base(CreateMessage(message, nav), innerException)
		{
			IXmlLineInfo xmlLineInfo = nav as IXmlLineInfo;
			lineNumber = (xmlLineInfo?.LineNumber ?? 0);
			linePosition = (xmlLineInfo?.LinePosition ?? 0);
			sourceUri = ((nav == null) ? string.Empty : nav.BaseURI);
		}

		private static string CreateMessage(string message, XPathNavigator nav)
		{
			IXmlLineInfo xmlLineInfo = nav as IXmlLineInfo;
			int num = xmlLineInfo?.LineNumber ?? 0;
			int num2 = xmlLineInfo?.LinePosition ?? 0;
			string text = (nav == null) ? string.Empty : nav.BaseURI;
			if (num != 0)
			{
				return CreateMessage("{0} at {1}({2},{3}).", message, num, num2, text);
			}
			return CreateMessage("{0}.", message, num, num2, text);
		}

		private static string CreateMessage(string msgFormat, string message, int lineNumber, int linePosition, string sourceUri)
		{
			return string.Format(CultureInfo.InvariantCulture, msgFormat, message, sourceUri, lineNumber.ToString(CultureInfo.InvariantCulture), linePosition.ToString(CultureInfo.InvariantCulture));
		}

		/// <summary>Streams all the XsltException properties into the <see cref="T:System.Runtime.Serialization.SerializationInfo" /> class for the given <see cref="T:System.Runtime.Serialization.StreamingContext" />.</summary>
		/// <param name="info">The SerializationInfo object. </param>
		/// <param name="context">The StreamingContext object. </param>
		public override void GetObjectData(SerializationInfo info, StreamingContext context)
		{
			base.GetObjectData(info, context);
			info.AddValue("lineNumber", lineNumber);
			info.AddValue("linePosition", linePosition);
			info.AddValue("sourceUri", sourceUri);
			info.AddValue("templateFrames", templateFrames);
		}

		internal void AddTemplateFrame(string frame)
		{
			templateFrames += frame;
		}
	}
}
