using Mono.Xml.XPath;
using System.Collections;
using System.Collections.Generic;
using System.IO;
using System.Text;
using System.Xml.Schema;
using System.Xml.Xsl;

namespace System.Xml.XPath
{
	/// <summary>Provides a cursor model for navigating and editing XML data.</summary>
	public abstract class XPathNavigator : XPathItem, ICloneable, IXPathNavigable, IXmlNamespaceResolver
	{
		private class EnumerableIterator : XPathNodeIterator
		{
			private IEnumerable source;

			private IEnumerator e;

			private int pos;

			public override int CurrentPosition => pos;

			public override XPathNavigator Current => (pos != 0) ? ((XPathNavigator)e.Current) : null;

			public EnumerableIterator(IEnumerable source, int pos)
			{
				this.source = source;
				for (int i = 0; i < pos; i++)
				{
					MoveNext();
				}
			}

			public override XPathNodeIterator Clone()
			{
				return new EnumerableIterator(source, pos);
			}

			public override bool MoveNext()
			{
				if (e == null)
				{
					e = source.GetEnumerator();
				}
				if (!e.MoveNext())
				{
					return false;
				}
				pos++;
				return true;
			}
		}

		private static readonly char[] escape_text_chars = new char[3]
		{
			'&',
			'<',
			'>'
		};

		private static readonly char[] escape_attr_chars = new char[6]
		{
			'"',
			'&',
			'<',
			'>',
			'\r',
			'\n'
		};

		/// <summary>Gets an <see cref="T:System.Collections.IEqualityComparer" /> used for equality comparison of <see cref="T:System.Xml.XPath.XPathNavigator" /> objects.</summary>
		/// <returns>An <see cref="T:System.Collections.IEqualityComparer" /> used for equality comparison of <see cref="T:System.Xml.XPath.XPathNavigator" /> objects.</returns>
		public static IEqualityComparer NavigatorComparer => XPathNavigatorComparer.Instance;

		/// <summary>When overridden in a derived class, gets the base URI for the current node.</summary>
		/// <returns>The location from which the node was loaded, or <see cref="F:System.String.Empty" /> if there is no value.</returns>
		public abstract string BaseURI
		{
			get;
		}

		/// <summary>Gets a value indicating whether the <see cref="T:System.Xml.XPath.XPathNavigator" /> can edit the underlying XML data.</summary>
		/// <returns>true if the <see cref="T:System.Xml.XPath.XPathNavigator" /> can edit the underlying XML data; otherwise false.</returns>
		public virtual bool CanEdit => false;

		/// <summary>Gets a value indicating whether the current node has any attributes.</summary>
		/// <returns>Returns true if the current node has attributes; returns false if the current node has no attributes, or if the <see cref="T:System.Xml.XPath.XPathNavigator" /> is not positioned on an element node.</returns>
		public virtual bool HasAttributes
		{
			get
			{
				if (!MoveToFirstAttribute())
				{
					return false;
				}
				MoveToParent();
				return true;
			}
		}

		/// <summary>Gets a value indicating whether the current node has any child nodes.</summary>
		/// <returns>Returns true if the current node has any child nodes; otherwise, false.</returns>
		public virtual bool HasChildren
		{
			get
			{
				if (!MoveToFirstChild())
				{
					return false;
				}
				MoveToParent();
				return true;
			}
		}

		/// <summary>When overridden in a derived class, gets a value indicating whether the current node is an empty element without an end element tag.</summary>
		/// <returns>Returns true if the current node is an empty element; otherwise, false.</returns>
		public abstract bool IsEmptyElement
		{
			get;
		}

		/// <summary>When overridden in a derived class, gets the <see cref="P:System.Xml.XPath.XPathNavigator.Name" /> of the current node without any namespace prefix.</summary>
		/// <returns>A <see cref="T:System.String" /> that contains the local name of the current node, or <see cref="F:System.String.Empty" /> if the current node does not have a name (for example, text or comment nodes).</returns>
		public abstract string LocalName
		{
			get;
		}

		/// <summary>When overridden in a derived class, gets the qualified name of the current node.</summary>
		/// <returns>A <see cref="T:System.String" /> that contains the qualified <see cref="P:System.Xml.XPath.XPathNavigator.Name" /> of the current node, or <see cref="F:System.String.Empty" /> if the current node does not have a name (for example, text or comment nodes).</returns>
		public abstract string Name
		{
			get;
		}

		/// <summary>When overridden in a derived class, gets the namespace URI of the current node.</summary>
		/// <returns>A <see cref="T:System.String" /> that contains the namespace URI of the current node, or <see cref="F:System.String.Empty" /> if the current node has no namespace URI.</returns>
		public abstract string NamespaceURI
		{
			get;
		}

		/// <summary>When overridden in a derived class, gets the <see cref="T:System.Xml.XmlNameTable" /> of the <see cref="T:System.Xml.XPath.XPathNavigator" />.</summary>
		/// <returns>An <see cref="T:System.Xml.XmlNameTable" /> object enabling you to get the atomized version of a <see cref="T:System.String" /> within the XML document.</returns>
		public abstract XmlNameTable NameTable
		{
			get;
		}

		/// <summary>When overridden in a derived class, gets the <see cref="T:System.Xml.XPath.XPathNodeType" /> of the current node.</summary>
		/// <returns>One of the <see cref="T:System.Xml.XPath.XPathNodeType" /> values representing the current node.</returns>
		public abstract XPathNodeType NodeType
		{
			get;
		}

		/// <summary>When overridden in a derived class, gets the namespace prefix associated with the current node.</summary>
		/// <returns>A <see cref="T:System.String" /> that contains the namespace prefix associated with the current node.</returns>
		public abstract string Prefix
		{
			get;
		}

		/// <summary>Gets the xml:lang scope for the current node.</summary>
		/// <returns>A <see cref="T:System.String" /> that contains the value of the xml:lang scope, or <see cref="F:System.String.Empty" /> if the current node has no xml:lang scope value to return.</returns>
		public virtual string XmlLang
		{
			get
			{
				XPathNavigator xPathNavigator = Clone();
				XPathNodeType nodeType = xPathNavigator.NodeType;
				if (nodeType == XPathNodeType.Attribute || nodeType == XPathNodeType.Namespace)
				{
					xPathNavigator.MoveToParent();
				}
				do
				{
					if (xPathNavigator.MoveToAttribute("lang", "http://www.w3.org/XML/1998/namespace"))
					{
						return xPathNavigator.Value;
					}
				}
				while (xPathNavigator.MoveToParent());
				return string.Empty;
			}
		}

		/// <summary>Gets or sets the markup representing the child nodes of the current node.</summary>
		/// <returns>A <see cref="T:System.String" /> that contains the markup of the child nodes of the current node.</returns>
		/// <exception cref="T:System.InvalidOperationException">The <see cref="P:System.Xml.XPath.XPathNavigator.InnerXml" /> property cannot be set.</exception>
		public virtual string InnerXml
		{
			get
			{
				switch (NodeType)
				{
				case XPathNodeType.Attribute:
				case XPathNodeType.Namespace:
					return EscapeString(Value, attr: true);
				case XPathNodeType.Text:
				case XPathNodeType.SignificantWhitespace:
				case XPathNodeType.Whitespace:
					return string.Empty;
				case XPathNodeType.ProcessingInstruction:
				case XPathNodeType.Comment:
					return Value;
				default:
				{
					XmlReader xmlReader = ReadSubtree();
					xmlReader.Read();
					int num = xmlReader.Depth;
					if (NodeType != 0)
					{
						xmlReader.Read();
					}
					else
					{
						num = -1;
					}
					StringWriter stringWriter = new StringWriter();
					XmlWriterSettings xmlWriterSettings = new XmlWriterSettings();
					xmlWriterSettings.Indent = true;
					xmlWriterSettings.ConformanceLevel = ConformanceLevel.Fragment;
					xmlWriterSettings.OmitXmlDeclaration = true;
					XmlWriter xmlWriter = XmlWriter.Create(stringWriter, xmlWriterSettings);
					while (!xmlReader.EOF && xmlReader.Depth > num)
					{
						xmlWriter.WriteNode(xmlReader, defattr: false);
					}
					return stringWriter.ToString();
				}
				}
			}
			set
			{
				DeleteChildren();
				if (NodeType == XPathNodeType.Attribute)
				{
					SetValue(value);
				}
				else
				{
					AppendChild(value);
				}
			}
		}

		/// <summary>Gets a value indicating if the current node represents an XPath node.</summary>
		/// <returns>Always returns true.</returns>
		public sealed override bool IsNode => true;

		/// <summary>Gets or sets the markup representing the opening and closing tags of the current node and its child nodes.</summary>
		/// <returns>A <see cref="T:System.String" /> that contains the markup representing the opening and closing tags of the current node and its child nodes.</returns>
		public virtual string OuterXml
		{
			get
			{
				switch (NodeType)
				{
				case XPathNodeType.Attribute:
					return Prefix + ((Prefix.Length <= 0) ? string.Empty : ":") + LocalName + "=\"" + EscapeString(Value, attr: true) + "\"";
				case XPathNodeType.Namespace:
					return "xmlns" + ((LocalName.Length <= 0) ? string.Empty : ":") + LocalName + "=\"" + EscapeString(Value, attr: true) + "\"";
				case XPathNodeType.Text:
					return EscapeString(Value, attr: false);
				case XPathNodeType.SignificantWhitespace:
				case XPathNodeType.Whitespace:
					return Value;
				default:
				{
					XmlWriterSettings xmlWriterSettings = new XmlWriterSettings();
					xmlWriterSettings.Indent = true;
					xmlWriterSettings.OmitXmlDeclaration = true;
					xmlWriterSettings.ConformanceLevel = ConformanceLevel.Fragment;
					StringBuilder stringBuilder = new StringBuilder();
					using (XmlWriter writer = XmlWriter.Create(stringBuilder, xmlWriterSettings))
					{
						WriteSubtree(writer);
					}
					return stringBuilder.ToString();
				}
				}
			}
			set
			{
				switch (NodeType)
				{
				case XPathNodeType.Root:
				case XPathNodeType.Attribute:
				case XPathNodeType.Namespace:
					throw new XmlException("Setting OuterXml Root, Attribute and Namespace is not supported.");
				}
				DeleteSelf();
				AppendChild(value);
				MoveToFirstChild();
			}
		}

		/// <summary>Gets the schema information that has been assigned to the current node as a result of schema validation.</summary>
		/// <returns>An <see cref="T:System.Xml.Schema.IXmlSchemaInfo" /> object that contains the schema information for the current node.</returns>
		public virtual IXmlSchemaInfo SchemaInfo => null;

		/// <summary>Gets the current node as a boxed object of the most appropriate .NET Framework type.</summary>
		/// <returns>The current node as a boxed object of the most appropriate .NET Framework type.</returns>
		public override object TypedValue
		{
			get
			{
				XPathNodeType nodeType = NodeType;
				if ((nodeType == XPathNodeType.Element || nodeType == XPathNodeType.Attribute) && XmlType != null)
				{
					XmlSchemaDatatype datatype = XmlType.Datatype;
					if (datatype != null)
					{
						return datatype.ParseValue(Value, NameTable, this);
					}
				}
				return Value;
			}
		}

		/// <summary>Used by <see cref="T:System.Xml.XPath.XPathNavigator" /> implementations which provide a "virtualized" XML view over a store, to provide access to underlying objects.</summary>
		/// <returns>The default is null.</returns>
		public virtual object UnderlyingObject => null;

		/// <summary>Gets the current node's value as a <see cref="T:System.Boolean" />.</summary>
		/// <returns>The current node's value as a <see cref="T:System.Boolean" />.</returns>
		/// <exception cref="T:System.FormatException">The current node's string value cannot be converted to a <see cref="T:System.Boolean" />.</exception>
		/// <exception cref="T:System.InvalidCastException">The attempted cast to <see cref="T:System.Boolean" /> is not valid.</exception>
		public override bool ValueAsBoolean => XQueryConvert.StringToBoolean(Value);

		/// <summary>Gets the current node's value as a <see cref="T:System.DateTime" />.</summary>
		/// <returns>The current node's value as a <see cref="T:System.DateTime" />.</returns>
		/// <exception cref="T:System.FormatException">The current node's string value cannot be converted to a <see cref="T:System.DateTime" />.</exception>
		/// <exception cref="T:System.InvalidCastException">The attempted cast to <see cref="T:System.DateTime" /> is not valid.</exception>
		public override DateTime ValueAsDateTime => XmlConvert.ToDateTime(Value);

		/// <summary>Gets the current node's value as a <see cref="T:System.Double" />.</summary>
		/// <returns>The current node's value as a <see cref="T:System.Double" />.</returns>
		/// <exception cref="T:System.FormatException">The current node's string value cannot be converted to a <see cref="T:System.Double" />.</exception>
		/// <exception cref="T:System.InvalidCastException">The attempted cast to <see cref="T:System.Double" /> is not valid.</exception>
		public override double ValueAsDouble => XQueryConvert.StringToDouble(Value);

		/// <summary>Gets the current node's value as an <see cref="T:System.Int32" />.</summary>
		/// <returns>The current node's value as an <see cref="T:System.Int32" />.</returns>
		/// <exception cref="T:System.FormatException">The current node's string value cannot be converted to a <see cref="T:System.Int32" />.</exception>
		/// <exception cref="T:System.InvalidCastException">The attempted cast to <see cref="T:System.Int32" /> is not valid.</exception>
		public override int ValueAsInt => XQueryConvert.StringToInt(Value);

		/// <summary>Gets the current node's value as an <see cref="T:System.Int64" />.</summary>
		/// <returns>The current node's value as an <see cref="T:System.Int64" />.</returns>
		/// <exception cref="T:System.FormatException">The current node's string value cannot be converted to a <see cref="T:System.Int64" />.</exception>
		/// <exception cref="T:System.InvalidCastException">The attempted cast to <see cref="T:System.Int64" /> is not valid.</exception>
		public override long ValueAsLong => XQueryConvert.StringToInteger(Value);

		/// <summary>Gets the .NET Framework <see cref="T:System.Type" /> of the current node.</summary>
		/// <returns>The .NET Framework <see cref="T:System.Type" /> of the current node. The default value is <see cref="T:System.String" />.</returns>
		public override Type ValueType => (SchemaInfo == null || SchemaInfo.SchemaType == null || SchemaInfo.SchemaType.Datatype == null) ? null : SchemaInfo.SchemaType.Datatype.ValueType;

		/// <summary>Gets the <see cref="T:System.Xml.Schema.XmlSchemaType" /> information for the current node.</summary>
		/// <returns>An <see cref="T:System.Xml.Schema.XmlSchemaType" /> object; default is null.</returns>
		public override XmlSchemaType XmlType
		{
			get
			{
				if (SchemaInfo != null)
				{
					return SchemaInfo.SchemaType;
				}
				return null;
			}
		}

		/// <summary>For a description of this member, see <see cref="M:System.Xml.XPath.XPathNavigator.Clone" />.</summary>
		/// <returns>An <see cref="T:System.Object" />.</returns>
		object ICloneable.Clone()
		{
			return Clone();
		}

		/// <summary>When overridden in a derived class, creates a new <see cref="T:System.Xml.XPath.XPathNavigator" /> positioned at the same node as this <see cref="T:System.Xml.XPath.XPathNavigator" />.</summary>
		/// <returns>A new <see cref="T:System.Xml.XPath.XPathNavigator" /> positioned at the same node as this <see cref="T:System.Xml.XPath.XPathNavigator" />.</returns>
		public abstract XPathNavigator Clone();

		/// <summary>Compares the position of the current <see cref="T:System.Xml.XPath.XPathNavigator" /> with the position of the <see cref="T:System.Xml.XPath.XPathNavigator" /> specified.</summary>
		/// <returns>An <see cref="T:System.Xml.XmlNodeOrder" /> value representing the comparative position of the two <see cref="T:System.Xml.XPath.XPathNavigator" /> objects.</returns>
		/// <param name="nav">The <see cref="T:System.Xml.XPath.XPathNavigator" /> to compare against.</param>
		public virtual XmlNodeOrder ComparePosition(XPathNavigator nav)
		{
			if (IsSamePosition(nav))
			{
				return XmlNodeOrder.Same;
			}
			if (IsDescendant(nav))
			{
				return XmlNodeOrder.Before;
			}
			if (nav.IsDescendant(this))
			{
				return XmlNodeOrder.After;
			}
			XPathNavigator xPathNavigator = Clone();
			XPathNavigator xPathNavigator2 = nav.Clone();
			xPathNavigator.MoveToRoot();
			xPathNavigator2.MoveToRoot();
			if (!xPathNavigator.IsSamePosition(xPathNavigator2))
			{
				return XmlNodeOrder.Unknown;
			}
			xPathNavigator.MoveTo(this);
			xPathNavigator2.MoveTo(nav);
			int num = 0;
			while (xPathNavigator.MoveToParent())
			{
				num++;
			}
			xPathNavigator.MoveTo(this);
			int num2 = 0;
			while (xPathNavigator2.MoveToParent())
			{
				num2++;
			}
			xPathNavigator2.MoveTo(nav);
			int num3;
			for (num3 = num; num3 > num2; num3--)
			{
				xPathNavigator.MoveToParent();
			}
			for (int num4 = num2; num4 > num3; num4--)
			{
				xPathNavigator2.MoveToParent();
			}
			while (!xPathNavigator.IsSamePosition(xPathNavigator2))
			{
				xPathNavigator.MoveToParent();
				xPathNavigator2.MoveToParent();
				num3--;
			}
			xPathNavigator.MoveTo(this);
			for (int num5 = num; num5 > num3 + 1; num5--)
			{
				xPathNavigator.MoveToParent();
			}
			xPathNavigator2.MoveTo(nav);
			for (int num6 = num2; num6 > num3 + 1; num6--)
			{
				xPathNavigator2.MoveToParent();
			}
			if (xPathNavigator.NodeType == XPathNodeType.Namespace)
			{
				if (xPathNavigator2.NodeType != XPathNodeType.Namespace)
				{
					return XmlNodeOrder.Before;
				}
				while (xPathNavigator.MoveToNextNamespace())
				{
					if (xPathNavigator.IsSamePosition(xPathNavigator2))
					{
						return XmlNodeOrder.Before;
					}
				}
				return XmlNodeOrder.After;
			}
			if (xPathNavigator2.NodeType == XPathNodeType.Namespace)
			{
				return XmlNodeOrder.After;
			}
			if (xPathNavigator.NodeType == XPathNodeType.Attribute)
			{
				if (xPathNavigator2.NodeType != XPathNodeType.Attribute)
				{
					return XmlNodeOrder.Before;
				}
				while (xPathNavigator.MoveToNextAttribute())
				{
					if (xPathNavigator.IsSamePosition(xPathNavigator2))
					{
						return XmlNodeOrder.Before;
					}
				}
				return XmlNodeOrder.After;
			}
			while (xPathNavigator.MoveToNext())
			{
				if (xPathNavigator.IsSamePosition(xPathNavigator2))
				{
					return XmlNodeOrder.Before;
				}
			}
			return XmlNodeOrder.After;
		}

		/// <summary>Compiles a string representing an XPath expression and returns an <see cref="T:System.Xml.XPath.XPathExpression" /> object.</summary>
		/// <returns>An <see cref="T:System.Xml.XPath.XPathExpression" /> object representing the XPath expression.</returns>
		/// <param name="xpath">A string representing an XPath expression.</param>
		/// <exception cref="T:System.ArgumentException">The <paramref name="xpath" /> parameter contains an XPath expression that is not valid.</exception>
		/// <exception cref="T:System.Xml.XPath.XPathException">The XPath expression is not valid.</exception>
		public virtual XPathExpression Compile(string xpath)
		{
			return XPathExpression.Compile(xpath);
		}

		internal virtual XPathExpression Compile(string xpath, IStaticXsltContext ctx)
		{
			return XPathExpression.Compile(xpath, null, ctx);
		}

		/// <summary>Evaluates the specified XPath expression and returns the typed result.</summary>
		/// <returns>The result of the expression (Boolean, number, string, or node set). This maps to <see cref="T:System.Boolean" />, <see cref="T:System.Double" />, <see cref="T:System.String" />, or <see cref="T:System.Xml.XPath.XPathNodeIterator" /> objects respectively.</returns>
		/// <param name="xpath">A string representing an XPath expression that can be evaluated.</param>
		/// <exception cref="T:System.ArgumentException">The return type of the XPath expression is a node set.</exception>
		/// <exception cref="T:System.Xml.XPath.XPathException">The XPath expression is not valid.</exception>
		public virtual object Evaluate(string xpath)
		{
			return Evaluate(Compile(xpath));
		}

		/// <summary>Evaluates the <see cref="T:System.Xml.XPath.XPathExpression" /> and returns the typed result.</summary>
		/// <returns>The result of the expression (Boolean, number, string, or node set). This maps to <see cref="T:System.Boolean" />, <see cref="T:System.Double" />, <see cref="T:System.String" />, or <see cref="T:System.Xml.XPath.XPathNodeIterator" /> objects respectively.</returns>
		/// <param name="expr">An <see cref="T:System.Xml.XPath.XPathExpression" /> that can be evaluated.</param>
		/// <exception cref="T:System.ArgumentException">The return type of the XPath expression is a node set.</exception>
		/// <exception cref="T:System.Xml.XPath.XPathException">The XPath expression is not valid.</exception>
		public virtual object Evaluate(XPathExpression expr)
		{
			return Evaluate(expr, null);
		}

		/// <summary>Uses the supplied context to evaluate the <see cref="T:System.Xml.XPath.XPathExpression" />, and returns the typed result.</summary>
		/// <returns>The result of the expression (Boolean, number, string, or node set). This maps to <see cref="T:System.Boolean" />, <see cref="T:System.Double" />, <see cref="T:System.String" />, or <see cref="T:System.Xml.XPath.XPathNodeIterator" /> objects respectively.</returns>
		/// <param name="expr">An <see cref="T:System.Xml.XPath.XPathExpression" /> that can be evaluated.</param>
		/// <param name="context">An <see cref="T:System.Xml.XPath.XPathNodeIterator" /> that points to the selected node set that the evaluation is to be performed on.</param>
		/// <exception cref="T:System.ArgumentException">The return type of the XPath expression is a node set.</exception>
		/// <exception cref="T:System.Xml.XPath.XPathException">The XPath expression is not valid.</exception>
		public virtual object Evaluate(XPathExpression expr, XPathNodeIterator context)
		{
			return Evaluate(expr, context, null);
		}

		private BaseIterator ToBaseIterator(XPathNodeIterator iter, IXmlNamespaceResolver ctx)
		{
			BaseIterator baseIterator = iter as BaseIterator;
			if (baseIterator == null)
			{
				baseIterator = new WrapperIterator(iter, ctx);
			}
			return baseIterator;
		}

		private object Evaluate(XPathExpression expr, XPathNodeIterator context, IXmlNamespaceResolver ctx)
		{
			CompiledExpression compiledExpression = (CompiledExpression)expr;
			if (ctx == null)
			{
				ctx = compiledExpression.NamespaceManager;
			}
			if (context == null)
			{
				context = new NullIterator(this, ctx);
			}
			BaseIterator baseIterator = ToBaseIterator(context, ctx);
			baseIterator.NamespaceManager = ctx;
			return compiledExpression.Evaluate(baseIterator);
		}

		internal XPathNodeIterator EvaluateNodeSet(XPathExpression expr, XPathNodeIterator context, IXmlNamespaceResolver ctx)
		{
			CompiledExpression compiledExpression = (CompiledExpression)expr;
			if (ctx == null)
			{
				ctx = compiledExpression.NamespaceManager;
			}
			if (context == null)
			{
				context = new NullIterator(this, compiledExpression.NamespaceManager);
			}
			BaseIterator baseIterator = ToBaseIterator(context, ctx);
			baseIterator.NamespaceManager = ctx;
			return compiledExpression.EvaluateNodeSet(baseIterator);
		}

		internal string EvaluateString(XPathExpression expr, XPathNodeIterator context, IXmlNamespaceResolver ctx)
		{
			CompiledExpression compiledExpression = (CompiledExpression)expr;
			if (ctx == null)
			{
				ctx = compiledExpression.NamespaceManager;
			}
			if (context == null)
			{
				context = new NullIterator(this, compiledExpression.NamespaceManager);
			}
			BaseIterator iter = ToBaseIterator(context, ctx);
			return compiledExpression.EvaluateString(iter);
		}

		internal double EvaluateNumber(XPathExpression expr, XPathNodeIterator context, IXmlNamespaceResolver ctx)
		{
			CompiledExpression compiledExpression = (CompiledExpression)expr;
			if (ctx == null)
			{
				ctx = compiledExpression.NamespaceManager;
			}
			if (context == null)
			{
				context = new NullIterator(this, compiledExpression.NamespaceManager);
			}
			BaseIterator baseIterator = ToBaseIterator(context, ctx);
			baseIterator.NamespaceManager = ctx;
			return compiledExpression.EvaluateNumber(baseIterator);
		}

		internal bool EvaluateBoolean(XPathExpression expr, XPathNodeIterator context, IXmlNamespaceResolver ctx)
		{
			CompiledExpression compiledExpression = (CompiledExpression)expr;
			if (ctx == null)
			{
				ctx = compiledExpression.NamespaceManager;
			}
			if (context == null)
			{
				context = new NullIterator(this, compiledExpression.NamespaceManager);
			}
			BaseIterator baseIterator = ToBaseIterator(context, ctx);
			baseIterator.NamespaceManager = ctx;
			return compiledExpression.EvaluateBoolean(baseIterator);
		}

		/// <summary>Gets the value of the attribute with the specified local name and namespace URI.</summary>
		/// <returns>A <see cref="T:System.String" /> that contains the value of the specified attribute; <see cref="F:System.String.Empty" /> if a matching attribute is not found, or if the <see cref="T:System.Xml.XPath.XPathNavigator" /> is not positioned on an element node.</returns>
		/// <param name="localName">The local name of the attribute.</param>
		/// <param name="namespaceURI">The namespace URI of the attribute.</param>
		public virtual string GetAttribute(string localName, string namespaceURI)
		{
			if (!MoveToAttribute(localName, namespaceURI))
			{
				return string.Empty;
			}
			string value = Value;
			MoveToParent();
			return value;
		}

		/// <summary>Returns the value of the namespace node corresponding to the specified local name.</summary>
		/// <returns>A <see cref="T:System.String" /> that contains the value of the namespace node; <see cref="F:System.String.Empty" /> if a matching namespace node is not found, or if the <see cref="T:System.Xml.XPath.XPathNavigator" /> is not positioned on an element node.</returns>
		/// <param name="name">The local name of the namespace node.</param>
		public virtual string GetNamespace(string name)
		{
			if (!MoveToNamespace(name))
			{
				return string.Empty;
			}
			string value = Value;
			MoveToParent();
			return value;
		}

		/// <summary>Determines whether the specified <see cref="T:System.Xml.XPath.XPathNavigator" /> is a descendant of the current <see cref="T:System.Xml.XPath.XPathNavigator" />.</summary>
		/// <returns>Returns true if the specified <see cref="T:System.Xml.XPath.XPathNavigator" /> is a descendant of the current <see cref="T:System.Xml.XPath.XPathNavigator" />; otherwise, false.</returns>
		/// <param name="nav">The <see cref="T:System.Xml.XPath.XPathNavigator" /> to compare to this <see cref="T:System.Xml.XPath.XPathNavigator" />.</param>
		public virtual bool IsDescendant(XPathNavigator nav)
		{
			if (nav != null)
			{
				nav = nav.Clone();
				while (nav.MoveToParent())
				{
					if (IsSamePosition(nav))
					{
						return true;
					}
				}
			}
			return false;
		}

		/// <summary>When overridden in a derived class, determines whether the current <see cref="T:System.Xml.XPath.XPathNavigator" /> is at the same position as the specified <see cref="T:System.Xml.XPath.XPathNavigator" />.</summary>
		/// <returns>Returns true if the two <see cref="T:System.Xml.XPath.XPathNavigator" /> objects have the same position; otherwise, false.</returns>
		/// <param name="other">The <see cref="T:System.Xml.XPath.XPathNavigator" /> to compare to this <see cref="T:System.Xml.XPath.XPathNavigator" />.</param>
		public abstract bool IsSamePosition(XPathNavigator other);

		/// <summary>Determines whether the current node matches the specified XPath expression.</summary>
		/// <returns>Returns true if the current node matches the specified XPath expression; otherwise, false.</returns>
		/// <param name="xpath">The XPath expression.</param>
		/// <exception cref="T:System.ArgumentException">The XPath expression cannot be evaluated.</exception>
		/// <exception cref="T:System.Xml.XPath.XPathException">The XPath expression is not valid.</exception>
		public virtual bool Matches(string xpath)
		{
			return Matches(Compile(xpath));
		}

		/// <summary>Determines whether the current node matches the specified <see cref="T:System.Xml.XPath.XPathExpression" />.</summary>
		/// <returns>Returns true if the current node matches the <see cref="T:System.Xml.XPath.XPathExpression" />; otherwise, false.</returns>
		/// <param name="expr">An <see cref="T:System.Xml.XPath.XPathExpression" /> object containing the compiled XPath expression.</param>
		/// <exception cref="T:System.ArgumentException">The XPath expression cannot be evaluated.</exception>
		/// <exception cref="T:System.Xml.XPath.XPathException">The XPath expression is not valid.</exception>
		public virtual bool Matches(XPathExpression expr)
		{
			Expression expression = ((CompiledExpression)expr).ExpressionNode;
			if (expression is ExprRoot)
			{
				return NodeType == XPathNodeType.Root;
			}
			NodeTest nodeTest = expression as NodeTest;
			if (nodeTest != null)
			{
				Axes axis = nodeTest.Axis.Axis;
				if (axis != Axes.Attribute && axis != Axes.Child)
				{
					throw new XPathException("Only child and attribute pattern are allowed for a pattern.");
				}
				return nodeTest.Match(((CompiledExpression)expr).NamespaceManager, this);
			}
			if (expression is ExprFilter)
			{
				do
				{
					expression = ((ExprFilter)expression).LeftHandSide;
				}
				while (expression is ExprFilter);
				if (expression is NodeTest && !((NodeTest)expression).Match(((CompiledExpression)expr).NamespaceManager, this))
				{
					return false;
				}
			}
			switch (expression.ReturnType)
			{
			default:
				return false;
			case XPathResultType.NodeSet:
			case XPathResultType.Any:
			{
				XPathNodeType evaluatedNodeType = expression.EvaluatedNodeType;
				if ((evaluatedNodeType == XPathNodeType.Attribute || evaluatedNodeType == XPathNodeType.Namespace) && NodeType != expression.EvaluatedNodeType)
				{
					return false;
				}
				XPathNodeIterator xPathNodeIterator = Select(expr);
				while (xPathNodeIterator.MoveNext())
				{
					if (IsSamePosition(xPathNodeIterator.Current))
					{
						return true;
					}
				}
				XPathNavigator xPathNavigator = Clone();
				while (xPathNavigator.MoveToParent())
				{
					xPathNodeIterator = xPathNavigator.Select(expr);
					while (xPathNodeIterator.MoveNext())
					{
						if (IsSamePosition(xPathNodeIterator.Current))
						{
							return true;
						}
					}
				}
				return false;
			}
			}
		}

		/// <summary>When overridden in a derived class, moves the <see cref="T:System.Xml.XPath.XPathNavigator" /> to the same position as the specified <see cref="T:System.Xml.XPath.XPathNavigator" />.</summary>
		/// <returns>Returns true if the <see cref="T:System.Xml.XPath.XPathNavigator" /> is successful moving to the same position as the specified <see cref="T:System.Xml.XPath.XPathNavigator" />; otherwise, false. If false, the position of the <see cref="T:System.Xml.XPath.XPathNavigator" /> is unchanged.</returns>
		/// <param name="other">The <see cref="T:System.Xml.XPath.XPathNavigator" /> positioned on the node that you want to move to. </param>
		public abstract bool MoveTo(XPathNavigator other);

		/// <summary>Moves the <see cref="T:System.Xml.XPath.XPathNavigator" /> to the attribute with the matching local name and namespace URI.</summary>
		/// <returns>Returns true if the <see cref="T:System.Xml.XPath.XPathNavigator" /> is successful moving to the attribute; otherwise, false. If false, the position of the <see cref="T:System.Xml.XPath.XPathNavigator" /> is unchanged.</returns>
		/// <param name="localName">The local name of the attribute.</param>
		/// <param name="namespaceURI">The namespace URI of the attribute; null for an empty namespace.</param>
		public virtual bool MoveToAttribute(string localName, string namespaceURI)
		{
			if (MoveToFirstAttribute())
			{
				do
				{
					if (LocalName == localName && NamespaceURI == namespaceURI)
					{
						return true;
					}
				}
				while (MoveToNextAttribute());
				MoveToParent();
			}
			return false;
		}

		/// <summary>Moves the <see cref="T:System.Xml.XPath.XPathNavigator" /> to the namespace node with the specified namespace prefix.</summary>
		/// <returns>true if the <see cref="T:System.Xml.XPath.XPathNavigator" /> is successful moving to the specified namespace; false if a matching namespace node was not found, or if the <see cref="T:System.Xml.XPath.XPathNavigator" /> is not positioned on an element node. If false, the position of the <see cref="T:System.Xml.XPath.XPathNavigator" /> is unchanged.</returns>
		/// <param name="name">The namespace prefix of the namespace node.</param>
		public virtual bool MoveToNamespace(string name)
		{
			if (MoveToFirstNamespace())
			{
				do
				{
					if (LocalName == name)
					{
						return true;
					}
				}
				while (MoveToNextNamespace());
				MoveToParent();
			}
			return false;
		}

		/// <summary>Moves the <see cref="T:System.Xml.XPath.XPathNavigator" /> to the first sibling node of the current node.</summary>
		/// <returns>Returns true if the <see cref="T:System.Xml.XPath.XPathNavigator" /> is successful moving to the first sibling node of the current node; false if there is no first sibling, or if the <see cref="T:System.Xml.XPath.XPathNavigator" /> is currently positioned on an attribute node. If the <see cref="T:System.Xml.XPath.XPathNavigator" /> is already positioned on the first sibling, <see cref="T:System.Xml.XPath.XPathNavigator" /> will return true and will not move its position.If <see cref="T:System.Xml.XPath.XPathNavigator.MoveToFirst" /> returns false because there is no first sibling, or if <see cref="T:System.Xml.XPath.XPathNavigator" /> is currently positioned on an attribute, the position of the <see cref="T:System.Xml.XPath.XPathNavigator" /> is unchanged.</returns>
		public virtual bool MoveToFirst()
		{
			return MoveToFirstImpl();
		}

		/// <summary>Moves the <see cref="T:System.Xml.XPath.XPathNavigator" /> to the root node that the current node belongs to.</summary>
		public virtual void MoveToRoot()
		{
			while (MoveToParent())
			{
			}
		}

		internal bool MoveToFirstImpl()
		{
			XPathNodeType nodeType = NodeType;
			if (nodeType == XPathNodeType.Attribute || nodeType == XPathNodeType.Namespace)
			{
				return false;
			}
			if (!MoveToParent())
			{
				return false;
			}
			MoveToFirstChild();
			return true;
		}

		/// <summary>When overridden in a derived class, moves the <see cref="T:System.Xml.XPath.XPathNavigator" /> to the first attribute of the current node.</summary>
		/// <returns>Returns true if the <see cref="T:System.Xml.XPath.XPathNavigator" /> is successful moving to the first attribute of the current node; otherwise, false. If false, the position of the <see cref="T:System.Xml.XPath.XPathNavigator" /> is unchanged.</returns>
		public abstract bool MoveToFirstAttribute();

		/// <summary>When overridden in a derived class, moves the <see cref="T:System.Xml.XPath.XPathNavigator" /> to the first child node of the current node.</summary>
		/// <returns>Returns true if the <see cref="T:System.Xml.XPath.XPathNavigator" /> is successful moving to the first child node of the current node; otherwise, false. If false, the position of the <see cref="T:System.Xml.XPath.XPathNavigator" /> is unchanged.</returns>
		public abstract bool MoveToFirstChild();

		/// <summary>Moves the <see cref="T:System.Xml.XPath.XPathNavigator" /> to first namespace node of the current node.</summary>
		/// <returns>Returns true if the <see cref="T:System.Xml.XPath.XPathNavigator" /> is successful moving to the first namespace node; otherwise, false. If false, the position of the <see cref="T:System.Xml.XPath.XPathNavigator" /> is unchanged.</returns>
		public bool MoveToFirstNamespace()
		{
			return MoveToFirstNamespace(XPathNamespaceScope.All);
		}

		/// <summary>When overridden in a derived class, moves the <see cref="T:System.Xml.XPath.XPathNavigator" /> to the first namespace node that matches the <see cref="T:System.Xml.XPath.XPathNamespaceScope" /> specified.</summary>
		/// <returns>Returns true if the <see cref="T:System.Xml.XPath.XPathNavigator" /> is successful moving to the first namespace node; otherwise, false. If false, the position of the <see cref="T:System.Xml.XPath.XPathNavigator" /> is unchanged.</returns>
		/// <param name="namespaceScope">An <see cref="T:System.Xml.XPath.XPathNamespaceScope" /> value describing the namespace scope. </param>
		public abstract bool MoveToFirstNamespace(XPathNamespaceScope namespaceScope);

		/// <summary>When overridden in a derived class, moves to the node that has an attribute of type ID whose value matches the specified <see cref="T:System.String" />.</summary>
		/// <returns>true if the <see cref="T:System.Xml.XPath.XPathNavigator" /> is successful moving; otherwise, false. If false, the position of the navigator is unchanged.</returns>
		/// <param name="id">A <see cref="T:System.String" /> representing the ID value of the node to which you want to move.</param>
		public abstract bool MoveToId(string id);

		/// <summary>When overridden in a derived class, moves the <see cref="T:System.Xml.XPath.XPathNavigator" /> to the next sibling node of the current node.</summary>
		/// <returns>true if the <see cref="T:System.Xml.XPath.XPathNavigator" /> is successful moving to the next sibling node; otherwise, false if there are no more siblings or if the <see cref="T:System.Xml.XPath.XPathNavigator" /> is currently positioned on an attribute node. If false, the position of the <see cref="T:System.Xml.XPath.XPathNavigator" /> is unchanged.</returns>
		public abstract bool MoveToNext();

		/// <summary>When overridden in a derived class, moves the <see cref="T:System.Xml.XPath.XPathNavigator" /> to the next attribute.</summary>
		/// <returns>Returns true if the <see cref="T:System.Xml.XPath.XPathNavigator" /> is successful moving to the next attribute; false if there are no more attributes. If false, the position of the <see cref="T:System.Xml.XPath.XPathNavigator" /> is unchanged.</returns>
		public abstract bool MoveToNextAttribute();

		/// <summary>Moves the <see cref="T:System.Xml.XPath.XPathNavigator" /> to the next namespace node.</summary>
		/// <returns>Returns true if the <see cref="T:System.Xml.XPath.XPathNavigator" /> is successful moving to the next namespace node; otherwise, false. If false, the position of the <see cref="T:System.Xml.XPath.XPathNavigator" /> is unchanged.</returns>
		public bool MoveToNextNamespace()
		{
			return MoveToNextNamespace(XPathNamespaceScope.All);
		}

		/// <summary>When overridden in a derived class, moves the <see cref="T:System.Xml.XPath.XPathNavigator" /> to the next namespace node matching the <see cref="T:System.Xml.XPath.XPathNamespaceScope" /> specified.</summary>
		/// <returns>Returns true if the <see cref="T:System.Xml.XPath.XPathNavigator" /> is successful moving to the next namespace node; otherwise, false. If false, the position of the <see cref="T:System.Xml.XPath.XPathNavigator" /> is unchanged.</returns>
		/// <param name="namespaceScope">An <see cref="T:System.Xml.XPath.XPathNamespaceScope" /> value describing the namespace scope. </param>
		public abstract bool MoveToNextNamespace(XPathNamespaceScope namespaceScope);

		/// <summary>When overridden in a derived class, moves the <see cref="T:System.Xml.XPath.XPathNavigator" /> to the parent node of the current node.</summary>
		/// <returns>Returns true if the <see cref="T:System.Xml.XPath.XPathNavigator" /> is successful moving to the parent node of the current node; otherwise, false. If false, the position of the <see cref="T:System.Xml.XPath.XPathNavigator" /> is unchanged.</returns>
		public abstract bool MoveToParent();

		/// <summary>When overridden in a derived class, moves the <see cref="T:System.Xml.XPath.XPathNavigator" /> to the previous sibling node of the current node.</summary>
		/// <returns>Returns true if the <see cref="T:System.Xml.XPath.XPathNavigator" /> is successful moving to the previous sibling node; otherwise, false if there is no previous sibling node or if the <see cref="T:System.Xml.XPath.XPathNavigator" /> is currently positioned on an attribute node. If false, the position of the <see cref="T:System.Xml.XPath.XPathNavigator" /> is unchanged.</returns>
		public abstract bool MoveToPrevious();

		/// <summary>Selects a node set, using the specified XPath expression.</summary>
		/// <returns>An <see cref="T:System.Xml.XPath.XPathNodeIterator" /> pointing to the selected node set.</returns>
		/// <param name="xpath">A <see cref="T:System.String" /> representing an XPath expression.</param>
		/// <exception cref="T:System.ArgumentException">The XPath expression contains an error or its return type is not a node set.</exception>
		/// <exception cref="T:System.Xml.XPath.XPathException">The XPath expression is not valid.</exception>
		public virtual XPathNodeIterator Select(string xpath)
		{
			return Select(Compile(xpath));
		}

		/// <summary>Selects a node set using the specified <see cref="T:System.Xml.XPath.XPathExpression" />.</summary>
		/// <returns>An <see cref="T:System.Xml.XPath.XPathNodeIterator" /> that points to the selected node set.</returns>
		/// <param name="expr">An <see cref="T:System.Xml.XPath.XPathExpression" /> object containing the compiled XPath query.</param>
		/// <exception cref="T:System.ArgumentException">The XPath expression contains an error or its return type is not a node set.</exception>
		/// <exception cref="T:System.Xml.XPath.XPathException">The XPath expression is not valid.</exception>
		public virtual XPathNodeIterator Select(XPathExpression expr)
		{
			return Select(expr, null);
		}

		internal XPathNodeIterator Select(XPathExpression expr, IXmlNamespaceResolver ctx)
		{
			CompiledExpression compiledExpression = (CompiledExpression)expr;
			if (ctx == null)
			{
				ctx = compiledExpression.NamespaceManager;
			}
			BaseIterator iter = new NullIterator(this, ctx);
			return compiledExpression.EvaluateNodeSet(iter);
		}

		/// <summary>Selects all the ancestor nodes of the current node that have a matching <see cref="T:System.Xml.XPath.XPathNodeType" />.</summary>
		/// <returns>An <see cref="T:System.Xml.XPath.XPathNodeIterator" /> that contains the selected nodes. The returned nodes are in reverse document order.</returns>
		/// <param name="type">The <see cref="T:System.Xml.XPath.XPathNodeType" /> of the ancestor nodes.</param>
		/// <param name="matchSelf">To include the context node in the selection, true; otherwise, false.</param>
		public virtual XPathNodeIterator SelectAncestors(XPathNodeType type, bool matchSelf)
		{
			Axes axis = matchSelf ? Axes.AncestorOrSelf : Axes.Ancestor;
			return SelectTest(new NodeTypeTest(axis, type));
		}

		/// <summary>Selects all the ancestor nodes of the current node that have the specified local name and namespace URI.</summary>
		/// <returns>An <see cref="T:System.Xml.XPath.XPathNodeIterator" /> that contains the selected nodes. The returned nodes are in reverse document order.</returns>
		/// <param name="name">The local name of the ancestor nodes.</param>
		/// <param name="namespaceURI">The namespace URI of the ancestor nodes.</param>
		/// <param name="matchSelf">To include the context node in the selection, true; otherwise, false. </param>
		/// <exception cref="T:System.ArgumentNullException">null cannot be passed as a parameter.</exception>
		public virtual XPathNodeIterator SelectAncestors(string name, string namespaceURI, bool matchSelf)
		{
			if (name == null)
			{
				throw new ArgumentNullException("name");
			}
			if (namespaceURI == null)
			{
				throw new ArgumentNullException("namespaceURI");
			}
			Axes axis = matchSelf ? Axes.AncestorOrSelf : Axes.Ancestor;
			XmlQualifiedName name2 = new XmlQualifiedName(name, namespaceURI);
			return SelectTest(new NodeNameTest(axis, name2, resolvedName: true));
		}

		private static IEnumerable EnumerateChildren(XPathNavigator n, XPathNodeType type)
		{
			if (!n.MoveToFirstChild())
			{
				yield break;
			}
			n.MoveToParent();
			XPathNavigator nav = n.Clone();
			nav.MoveToFirstChild();
			XPathNavigator nav2 = null;
			do
			{
				if (type == XPathNodeType.All || nav.NodeType == type)
				{
					if (nav2 == null)
					{
						nav2 = nav.Clone();
					}
					else
					{
						nav2.MoveTo(nav);
					}
					yield return nav2;
				}
			}
			while (nav.MoveToNext());
		}

		/// <summary>Selects all the child nodes of the current node that have the matching <see cref="T:System.Xml.XPath.XPathNodeType" />.</summary>
		/// <returns>An <see cref="T:System.Xml.XPath.XPathNodeIterator" /> that contains the selected nodes.</returns>
		/// <param name="type">The <see cref="T:System.Xml.XPath.XPathNodeType" /> of the child nodes.</param>
		public virtual XPathNodeIterator SelectChildren(XPathNodeType type)
		{
			return new WrapperIterator(new EnumerableIterator(EnumerateChildren(this, type), 0), null);
		}

		private static IEnumerable EnumerateChildren(XPathNavigator n, string name, string ns)
		{
			if (!n.MoveToFirstChild())
			{
				yield break;
			}
			n.MoveToParent();
			XPathNavigator nav = n.Clone();
			nav.MoveToFirstChild();
			XPathNavigator nav2 = nav.Clone();
			do
			{
				if ((name == string.Empty || nav.LocalName == name) && (ns == string.Empty || nav.NamespaceURI == ns))
				{
					nav2.MoveTo(nav);
					yield return nav2;
				}
			}
			while (nav.MoveToNext());
		}

		/// <summary>Selects all the child nodes of the current node that have the local name and namespace URI specified.</summary>
		/// <returns>An <see cref="T:System.Xml.XPath.XPathNodeIterator" /> that contains the selected nodes.</returns>
		/// <param name="name">The local name of the child nodes. </param>
		/// <param name="namespaceURI">The namespace URI of the child nodes. </param>
		/// <exception cref="T:System.ArgumentNullException">null cannot be passed as a parameter.</exception>
		public virtual XPathNodeIterator SelectChildren(string name, string namespaceURI)
		{
			if (name == null)
			{
				throw new ArgumentNullException("name");
			}
			if (namespaceURI == null)
			{
				throw new ArgumentNullException("namespaceURI");
			}
			return new WrapperIterator(new EnumerableIterator(EnumerateChildren(this, name, namespaceURI), 0), null);
		}

		/// <summary>Selects all the descendant nodes of the current node that have a matching <see cref="T:System.Xml.XPath.XPathNodeType" />.</summary>
		/// <returns>An <see cref="T:System.Xml.XPath.XPathNodeIterator" /> that contains the selected nodes.</returns>
		/// <param name="type">The <see cref="T:System.Xml.XPath.XPathNodeType" /> of the descendant nodes.</param>
		/// <param name="matchSelf">true to include the context node in the selection; otherwise, false.</param>
		public virtual XPathNodeIterator SelectDescendants(XPathNodeType type, bool matchSelf)
		{
			Axes axis = (!matchSelf) ? Axes.Descendant : Axes.DescendantOrSelf;
			return SelectTest(new NodeTypeTest(axis, type));
		}

		/// <summary>Selects all the descendant nodes of the current node with the local name and namespace URI specified.</summary>
		/// <returns>An <see cref="T:System.Xml.XPath.XPathNodeIterator" /> that contains the selected nodes.</returns>
		/// <param name="name">The local name of the descendant nodes. </param>
		/// <param name="namespaceURI">The namespace URI of the descendant nodes. </param>
		/// <param name="matchSelf">true to include the context node in the selection; otherwise, false.</param>
		/// <exception cref="T:System.ArgumentNullException">null cannot be passed as a parameter.</exception>
		public virtual XPathNodeIterator SelectDescendants(string name, string namespaceURI, bool matchSelf)
		{
			if (name == null)
			{
				throw new ArgumentNullException("name");
			}
			if (namespaceURI == null)
			{
				throw new ArgumentNullException("namespaceURI");
			}
			Axes axis = (!matchSelf) ? Axes.Descendant : Axes.DescendantOrSelf;
			XmlQualifiedName name2 = new XmlQualifiedName(name, namespaceURI);
			return SelectTest(new NodeNameTest(axis, name2, resolvedName: true));
		}

		internal XPathNodeIterator SelectTest(NodeTest test)
		{
			return test.EvaluateNodeSet(new NullIterator(this));
		}

		/// <summary>Gets the text value of the current node.</summary>
		/// <returns>A string that contains the text value of the current node.</returns>
		public override string ToString()
		{
			return Value;
		}

		/// <summary>Verifies that the XML data in the <see cref="T:System.Xml.XPath.XPathNavigator" /> conforms to the XML Schema definition language (XSD) schema provided.</summary>
		/// <returns>true if no schema validation errors occurred; otherwise, false.</returns>
		/// <param name="schemas">The <see cref="T:System.Xml.Schema.XmlSchemaSet" /> containing the schemas used to validate the XML data contained in the <see cref="T:System.Xml.XPath.XPathNavigator" />.</param>
		/// <param name="validationEventHandler">The <see cref="T:System.Xml.Schema.ValidationEventHandler" /> that receives information about schema validation warnings and errors.</param>
		/// <exception cref="T:System.Xml.Schema.XmlSchemaValidationException">A schema validation error occurred, and no <see cref="T:System.Xml.Schema.ValidationEventHandler" /> was specified to handle validation errors.</exception>
		/// <exception cref="T:System.InvalidOperationException">The <see cref="T:System.Xml.XPath.XPathNavigator" /> is positioned on a node that is not an element, attribute, or the root node or there is not type information to perform validation.</exception>
		/// <exception cref="T:System.ArgumentException">The <see cref="M:System.Xml.XPath.XPathNavigator.CheckValidity(System.Xml.Schema.XmlSchemaSet,System.Xml.Schema.ValidationEventHandler)" /> method was called with an <see cref="T:System.Xml.Schema.XmlSchemaSet" /> parameter when the <see cref="T:System.Xml.XPath.XPathNavigator" /> was not positioned on the root node of the XML data.</exception>
		public virtual bool CheckValidity(XmlSchemaSet schemas, ValidationEventHandler handler)
		{
			//Discarded unreachable code: IL_0058
			XmlReaderSettings xmlReaderSettings = new XmlReaderSettings();
			xmlReaderSettings.NameTable = NameTable;
			xmlReaderSettings.SetSchemas(schemas);
			xmlReaderSettings.ValidationEventHandler += handler;
			xmlReaderSettings.ValidationType = ValidationType.Schema;
			try
			{
				XmlReader xmlReader = XmlReader.Create(ReadSubtree(), xmlReaderSettings);
				while (!xmlReader.EOF)
				{
					xmlReader.Read();
				}
			}
			catch (XmlSchemaValidationException)
			{
				return false;
			}
			return true;
		}

		/// <summary>Returns a copy of the <see cref="T:System.Xml.XPath.XPathNavigator" />.</summary>
		/// <returns>An <see cref="T:System.Xml.XPath.XPathNavigator" /> copy of this <see cref="T:System.Xml.XPath.XPathNavigator" />.</returns>
		public virtual XPathNavigator CreateNavigator()
		{
			return Clone();
		}

		/// <summary>Evaluates the specified XPath expression and returns the typed result, using the <see cref="T:System.Xml.IXmlNamespaceResolver" /> object specified to resolve namespace prefixes in the XPath expression.</summary>
		/// <returns>The result of the expression (Boolean, number, string, or node set). This maps to <see cref="T:System.Boolean" />, <see cref="T:System.Double" />, <see cref="T:System.String" />, or <see cref="T:System.Xml.XPath.XPathNodeIterator" /> objects respectively.</returns>
		/// <param name="xpath">A string representing an XPath expression that can be evaluated.</param>
		/// <param name="resolver">The <see cref="T:System.Xml.IXmlNamespaceResolver" /> object used to resolve namespace prefixes in the XPath expression.</param>
		/// <exception cref="T:System.ArgumentException">The return type of the XPath expression is a node set.</exception>
		/// <exception cref="T:System.Xml.XPath.XPathException">The XPath expression is not valid.</exception>
		public virtual object Evaluate(string xpath, IXmlNamespaceResolver nsResolver)
		{
			return Evaluate(Compile(xpath), null, nsResolver);
		}

		/// <summary>Returns the in-scope namespaces of the current node.</summary>
		/// <returns>An <see cref="T:System.Collections.Generic.IDictionary`2" /> collection of namespace names keyed by prefix.</returns>
		/// <param name="scope">An <see cref="T:System.Xml.XmlNamespaceScope" /> value specifying the namespaces to return.</param>
		public virtual IDictionary<string, string> GetNamespacesInScope(XmlNamespaceScope scope)
		{
			IDictionary<string, string> dictionary = new Dictionary<string, string>();
			int num;
			switch (scope)
			{
			case XmlNamespaceScope.Local:
				num = 2;
				break;
			case XmlNamespaceScope.ExcludeXml:
				num = 1;
				break;
			default:
				num = 0;
				break;
			}
			XPathNamespaceScope namespaceScope = (XPathNamespaceScope)num;
			XPathNavigator xPathNavigator = Clone();
			if (xPathNavigator.NodeType != XPathNodeType.Element)
			{
				xPathNavigator.MoveToParent();
			}
			if (!xPathNavigator.MoveToFirstNamespace(namespaceScope))
			{
				return dictionary;
			}
			do
			{
				dictionary.Add(xPathNavigator.Name, xPathNavigator.Value);
			}
			while (xPathNavigator.MoveToNextNamespace(namespaceScope));
			return dictionary;
		}

		/// <summary>Gets the namespace URI for the specified prefix.</summary>
		/// <returns>A <see cref="T:System.String" /> that contains the namespace URI assigned to the namespace prefix specified; null if no namespace URI is assigned to the prefix specified. The <see cref="T:System.String" /> returned is atomized.</returns>
		/// <param name="prefix">The prefix whose namespace URI you want to resolve. To match the default namespace, pass <see cref="F:System.String.Empty" />.</param>
		public virtual string LookupNamespace(string prefix)
		{
			XPathNavigator xPathNavigator = Clone();
			if (xPathNavigator.NodeType != XPathNodeType.Element)
			{
				xPathNavigator.MoveToParent();
			}
			if (xPathNavigator.MoveToNamespace(prefix))
			{
				return xPathNavigator.Value;
			}
			return null;
		}

		/// <summary>Gets the prefix declared for the specified namespace URI.</summary>
		/// <returns>A <see cref="T:System.String" /> that contains the namespace prefix assigned to the namespace URI specified; otherwise, <see cref="F:System.String.Empty" /> if no prefix is assigned to the namespace URI specified. The <see cref="T:System.String" /> returned is atomized.</returns>
		/// <param name="namespaceURI">The namespace URI to resolve for the prefix.</param>
		public virtual string LookupPrefix(string namespaceUri)
		{
			XPathNavigator xPathNavigator = Clone();
			if (xPathNavigator.NodeType != XPathNodeType.Element)
			{
				xPathNavigator.MoveToParent();
			}
			if (!xPathNavigator.MoveToFirstNamespace())
			{
				return null;
			}
			do
			{
				if (xPathNavigator.Value == namespaceUri)
				{
					return xPathNavigator.Name;
				}
			}
			while (xPathNavigator.MoveToNextNamespace());
			return null;
		}

		private bool MoveTo(XPathNodeIterator iter)
		{
			if (iter.MoveNext())
			{
				MoveTo(iter.Current);
				return true;
			}
			return false;
		}

		/// <summary>Moves the <see cref="T:System.Xml.XPath.XPathNavigator" /> to the child node of the <see cref="T:System.Xml.XPath.XPathNodeType" /> specified.</summary>
		/// <returns>Returns true if the <see cref="T:System.Xml.XPath.XPathNavigator" /> is successful moving to the child node; otherwise, false. If false, the position of the <see cref="T:System.Xml.XPath.XPathNavigator" /> is unchanged.</returns>
		/// <param name="type">The <see cref="T:System.Xml.XPath.XPathNodeType" /> of the child node to move to.</param>
		public virtual bool MoveToChild(XPathNodeType type)
		{
			return MoveTo(SelectChildren(type));
		}

		/// <summary>Moves the <see cref="T:System.Xml.XPath.XPathNavigator" /> to the child node with the local name and namespace URI specified.</summary>
		/// <returns>Returns true if the <see cref="T:System.Xml.XPath.XPathNavigator" /> is successful moving to the child node; otherwise, false. If false, the position of the <see cref="T:System.Xml.XPath.XPathNavigator" /> is unchanged.</returns>
		/// <param name="localName">The local name of the child node to move to.</param>
		/// <param name="namespaceURI">The namespace URI of the child node to move to.</param>
		public virtual bool MoveToChild(string localName, string namespaceURI)
		{
			return MoveTo(SelectChildren(localName, namespaceURI));
		}

		/// <summary>Moves the <see cref="T:System.Xml.XPath.XPathNavigator" /> to the next sibling node with the local name and namespace URI specified.</summary>
		/// <returns>Returns true if the <see cref="T:System.Xml.XPath.XPathNavigator" /> is successful moving to the next sibling node; false if there are no more siblings, or if the <see cref="T:System.Xml.XPath.XPathNavigator" /> is currently positioned on an attribute node. If false, the position of the <see cref="T:System.Xml.XPath.XPathNavigator" /> is unchanged.</returns>
		/// <param name="localName">The local name of the next sibling node to move to.</param>
		/// <param name="namespaceURI">The namespace URI of the next sibling node to move to.</param>
		public virtual bool MoveToNext(string localName, string namespaceURI)
		{
			XPathNavigator xPathNavigator = Clone();
			while (xPathNavigator.MoveToNext())
			{
				if (xPathNavigator.LocalName == localName && xPathNavigator.NamespaceURI == namespaceURI)
				{
					MoveTo(xPathNavigator);
					return true;
				}
			}
			return false;
		}

		/// <summary>Moves the <see cref="T:System.Xml.XPath.XPathNavigator" /> to the next sibling node of the current node that matches the <see cref="T:System.Xml.XPath.XPathNodeType" /> specified.</summary>
		/// <returns>true if the <see cref="T:System.Xml.XPath.XPathNavigator" /> is successful moving to the next sibling node; otherwise, false if there are no more siblings or if the <see cref="T:System.Xml.XPath.XPathNavigator" /> is currently positioned on an attribute node. If false, the position of the <see cref="T:System.Xml.XPath.XPathNavigator" /> is unchanged.</returns>
		/// <param name="type">The <see cref="T:System.Xml.XPath.XPathNodeType" /> of the sibling node to move to.</param>
		public virtual bool MoveToNext(XPathNodeType type)
		{
			XPathNavigator xPathNavigator = Clone();
			while (xPathNavigator.MoveToNext())
			{
				if (type == XPathNodeType.All || xPathNavigator.NodeType == type)
				{
					MoveTo(xPathNavigator);
					return true;
				}
			}
			return false;
		}

		/// <summary>Moves the <see cref="T:System.Xml.XPath.XPathNavigator" /> to the element with the local name and namespace URI specified in document order.</summary>
		/// <returns>true if the <see cref="T:System.Xml.XPath.XPathNavigator" /> moved successfully; otherwise false.</returns>
		/// <param name="localName">The local name of the element.</param>
		/// <param name="namespaceURI">The namespace URI of the element.</param>
		public virtual bool MoveToFollowing(string localName, string namespaceURI)
		{
			return MoveToFollowing(localName, namespaceURI, null);
		}

		/// <summary>Moves the <see cref="T:System.Xml.XPath.XPathNavigator" /> to the element with the local name and namespace URI specified, to the boundary specified, in document order.</summary>
		/// <returns>true if the <see cref="T:System.Xml.XPath.XPathNavigator" /> moved successfully; otherwise false.</returns>
		/// <param name="localName">The local name of the element.</param>
		/// <param name="namespaceURI">The namespace URI of the element.</param>
		/// <param name="end">The <see cref="T:System.Xml.XPath.XPathNavigator" /> object positioned on the element boundary which the current <see cref="T:System.Xml.XPath.XPathNavigator" /> will not move past while searching for the following element.</param>
		public virtual bool MoveToFollowing(string localName, string namespaceURI, XPathNavigator end)
		{
			if (localName == null)
			{
				throw new ArgumentNullException("localName");
			}
			if (namespaceURI == null)
			{
				throw new ArgumentNullException("namespaceURI");
			}
			localName = NameTable.Get(localName);
			if (localName == null)
			{
				return false;
			}
			namespaceURI = NameTable.Get(namespaceURI);
			if (namespaceURI == null)
			{
				return false;
			}
			XPathNavigator xPathNavigator = Clone();
			XPathNodeType nodeType = xPathNavigator.NodeType;
			if (nodeType == XPathNodeType.Attribute || nodeType == XPathNodeType.Namespace)
			{
				xPathNavigator.MoveToParent();
			}
			do
			{
				if (!xPathNavigator.MoveToFirstChild())
				{
					while (!xPathNavigator.MoveToNext())
					{
						if (!xPathNavigator.MoveToParent())
						{
							return false;
						}
					}
				}
				if (end != null && end.IsSamePosition(xPathNavigator))
				{
					return false;
				}
			}
			while (!object.ReferenceEquals(localName, xPathNavigator.LocalName) || !object.ReferenceEquals(namespaceURI, xPathNavigator.NamespaceURI));
			MoveTo(xPathNavigator);
			return true;
		}

		/// <summary>Moves the <see cref="T:System.Xml.XPath.XPathNavigator" /> to the following element of the <see cref="T:System.Xml.XPath.XPathNodeType" /> specified in document order.</summary>
		/// <returns>true if the <see cref="T:System.Xml.XPath.XPathNavigator" /> moved successfully; otherwise false.</returns>
		/// <param name="type">The <see cref="T:System.Xml.XPath.XPathNodeType" /> of the element. The <see cref="T:System.Xml.XPath.XPathNodeType" /> cannot be <see cref="F:System.Xml.XPath.XPathNodeType.Attribute" /> or <see cref="F:System.Xml.XPath.XPathNodeType.Namespace" />.</param>
		public virtual bool MoveToFollowing(XPathNodeType type)
		{
			return MoveToFollowing(type, null);
		}

		/// <summary>Moves the <see cref="T:System.Xml.XPath.XPathNavigator" /> to the following element of the <see cref="T:System.Xml.XPath.XPathNodeType" /> specified, to the boundary specified, in document order.</summary>
		/// <returns>true if the <see cref="T:System.Xml.XPath.XPathNavigator" /> moved successfully; otherwise false.</returns>
		/// <param name="type">The <see cref="T:System.Xml.XPath.XPathNodeType" /> of the element. The <see cref="T:System.Xml.XPath.XPathNodeType" /> cannot be <see cref="F:System.Xml.XPath.XPathNodeType.Attribute" /> or <see cref="F:System.Xml.XPath.XPathNodeType.Namespace" />.</param>
		/// <param name="end">The <see cref="T:System.Xml.XPath.XPathNavigator" /> object positioned on the element boundary which the current <see cref="T:System.Xml.XPath.XPathNavigator" /> will not move past while searching for the following element.</param>
		public virtual bool MoveToFollowing(XPathNodeType type, XPathNavigator end)
		{
			if (type == XPathNodeType.Root)
			{
				return false;
			}
			XPathNavigator xPathNavigator = Clone();
			XPathNodeType nodeType = xPathNavigator.NodeType;
			if (nodeType == XPathNodeType.Attribute || nodeType == XPathNodeType.Namespace)
			{
				xPathNavigator.MoveToParent();
			}
			do
			{
				if (!xPathNavigator.MoveToFirstChild())
				{
					while (!xPathNavigator.MoveToNext())
					{
						if (!xPathNavigator.MoveToParent())
						{
							return false;
						}
					}
				}
				if (end != null && end.IsSamePosition(xPathNavigator))
				{
					return false;
				}
			}
			while (type != XPathNodeType.All && xPathNavigator.NodeType != type);
			MoveTo(xPathNavigator);
			return true;
		}

		/// <summary>Returns an <see cref="T:System.Xml.XmlReader" /> object that contains the current node and its child nodes.</summary>
		/// <returns>An <see cref="T:System.Xml.XmlReader" /> object that contains the current node and its child nodes.</returns>
		/// <exception cref="T:System.InvalidOperationException">The <see cref="T:System.Xml.XPath.XPathNavigator" /> is not positioned on an element node or the root node.</exception>
		public virtual XmlReader ReadSubtree()
		{
			XPathNodeType nodeType = NodeType;
			if (nodeType == XPathNodeType.Root || nodeType == XPathNodeType.Element)
			{
				return new XPathNavigatorReader(this);
			}
			throw new InvalidOperationException($"NodeType {NodeType} is not supported to read as a subtree of an XPathNavigator.");
		}

		/// <summary>Selects a node set using the specified XPath expression with the <see cref="T:System.Xml.IXmlNamespaceResolver" /> object specified to resolve namespace prefixes.</summary>
		/// <returns>An <see cref="T:System.Xml.XPath.XPathNodeIterator" /> that points to the selected node set.</returns>
		/// <param name="xpath">A <see cref="T:System.String" /> representing an XPath expression.</param>
		/// <param name="resolver">The <see cref="T:System.Xml.IXmlNamespaceResolver" /> object used to resolve namespace prefixes.</param>
		/// <exception cref="T:System.ArgumentException">The XPath expression contains an error or its return type is not a node set.</exception>
		/// <exception cref="T:System.Xml.XPath.XPathException">The XPath expression is not valid.</exception>
		public virtual XPathNodeIterator Select(string xpath, IXmlNamespaceResolver nsResolver)
		{
			return Select(Compile(xpath), nsResolver);
		}

		/// <summary>Selects a single node in the <see cref="T:System.Xml.XPath.XPathNavigator" /> using the specified XPath query.</summary>
		/// <returns>An <see cref="T:System.Xml.XPath.XPathNavigator" /> object that contains the first matching node for the XPath query specified; otherwise, null if there are no query results.</returns>
		/// <param name="xpath">A <see cref="T:System.String" /> representing an XPath expression.</param>
		/// <exception cref="T:System.ArgumentException">An error was encountered in the XPath query or the return type of the XPath expression is not a node.</exception>
		/// <exception cref="T:System.Xml.XPath.XPathException">The XPath query is not valid.</exception>
		public virtual XPathNavigator SelectSingleNode(string xpath)
		{
			return SelectSingleNode(xpath, null);
		}

		/// <summary>Selects a single node in the <see cref="T:System.Xml.XPath.XPathNavigator" /> object using the specified XPath query with the <see cref="T:System.Xml.IXmlNamespaceResolver" /> object specified to resolve namespace prefixes.</summary>
		/// <returns>An <see cref="T:System.Xml.XPath.XPathNavigator" /> object that contains the first matching node for the XPath query specified; otherwise null if there are no query results.</returns>
		/// <param name="xpath">A <see cref="T:System.String" /> representing an XPath expression.</param>
		/// <param name="resolver">The <see cref="T:System.Xml.IXmlNamespaceResolver" /> object used to resolve namespace prefixes in the XPath query.</param>
		/// <exception cref="T:System.ArgumentException">An error was encountered in the XPath query or the return type of the XPath expression is not a node.</exception>
		/// <exception cref="T:System.Xml.XPath.XPathException">The XPath query is not valid.</exception>
		public virtual XPathNavigator SelectSingleNode(string xpath, IXmlNamespaceResolver nsResolver)
		{
			XPathExpression xPathExpression = Compile(xpath);
			xPathExpression.SetContext(nsResolver);
			return SelectSingleNode(xPathExpression);
		}

		/// <summary>Selects a single node in the <see cref="T:System.Xml.XPath.XPathNavigator" /> using the specified <see cref="T:System.Xml.XPath.XPathExpression" /> object.</summary>
		/// <returns>An <see cref="T:System.Xml.XPath.XPathNavigator" /> object that contains the first matching node for the XPath query specified; otherwise null if there are no query results.</returns>
		/// <param name="expression">An <see cref="T:System.Xml.XPath.XPathExpression" /> object containing the compiled XPath query.</param>
		/// <exception cref="T:System.ArgumentException">An error was encountered in the XPath query or the return type of the XPath expression is not a node.</exception>
		/// <exception cref="T:System.Xml.XPath.XPathException">The XPath query is not valid.</exception>
		public virtual XPathNavigator SelectSingleNode(XPathExpression expression)
		{
			XPathNodeIterator xPathNodeIterator = Select(expression);
			if (xPathNodeIterator.MoveNext())
			{
				return xPathNodeIterator.Current;
			}
			return null;
		}

		/// <summary>Gets the current node's value as the <see cref="T:System.Type" /> specified, using the <see cref="T:System.Xml.IXmlNamespaceResolver" /> object specified to resolve namespace prefixes.</summary>
		/// <returns>The value of the current node as the <see cref="T:System.Type" /> requested.</returns>
		/// <param name="returnType">The <see cref="T:System.Type" /> to return the current node's value as.</param>
		/// <param name="nsResolver">The <see cref="T:System.Xml.IXmlNamespaceResolver" /> object used to resolve namespace prefixes.</param>
		/// <exception cref="T:System.FormatException">The current node's value is not in the correct format for the target type.</exception>
		/// <exception cref="T:System.InvalidCastException">The attempted cast is not valid.</exception>
		public override object ValueAs(Type type, IXmlNamespaceResolver nsResolver)
		{
			return new XmlAtomicValue(Value, XmlSchemaSimpleType.XsString).ValueAs(type, nsResolver);
		}

		/// <summary>Streams the current node and its child nodes to the <see cref="T:System.Xml.XmlWriter" /> object specified.</summary>
		/// <returns>An <see cref="T:System.Xml.XmlWriter" /> object that contains the current node and its child nodes.</returns>
		/// <param name="writer">The <see cref="T:System.Xml.XmlWriter" /> object to stream to.</param>
		public virtual void WriteSubtree(XmlWriter writer)
		{
			writer.WriteNode(this, defattr: false);
		}

		private static string EscapeString(string value, bool attr)
		{
			StringBuilder stringBuilder = null;
			char[] anyOf = (!attr) ? escape_text_chars : escape_attr_chars;
			if (value.IndexOfAny(anyOf) < 0)
			{
				return value;
			}
			stringBuilder = new StringBuilder(value, value.Length + 10);
			if (attr)
			{
				stringBuilder.Replace("\"", "&quot;");
			}
			stringBuilder.Replace("<", "&lt;");
			stringBuilder.Replace(">", "&gt;");
			if (attr)
			{
				stringBuilder.Replace("\r\n", "&#10;");
				stringBuilder.Replace("\r", "&#10;");
				stringBuilder.Replace("\n", "&#10;");
			}
			return stringBuilder.ToString();
		}

		private XmlReader CreateFragmentReader(string fragment)
		{
			XmlReaderSettings xmlReaderSettings = new XmlReaderSettings();
			xmlReaderSettings.ConformanceLevel = ConformanceLevel.Fragment;
			XmlNamespaceManager xmlNamespaceManager = new XmlNamespaceManager(NameTable);
			foreach (KeyValuePair<string, string> item in GetNamespacesInScope(XmlNamespaceScope.All))
			{
				xmlNamespaceManager.AddNamespace(item.Key, item.Value);
			}
			return XmlReader.Create(new StringReader(fragment), xmlReaderSettings, new XmlParserContext(NameTable, xmlNamespaceManager, null, XmlSpace.None));
		}

		/// <summary>Returns an <see cref="T:System.Xml.XmlWriter" /> object used to create one or more new child nodes at the end of the list of child nodes of the current node.</summary>
		/// <returns>An <see cref="T:System.Xml.XmlWriter" /> object used to create new child nodes at the end of the list of child nodes of the current node.</returns>
		/// <exception cref="T:System.InvalidOperationException">The current node the <see cref="T:System.Xml.XPath.XPathNavigator" /> is positioned on is not the root node or an element node.</exception>
		/// <exception cref="T:System.NotSupportedException">The <see cref="T:System.Xml.XPath.XPathNavigator" /> does not support editing.</exception>
		public virtual XmlWriter AppendChild()
		{
			throw new NotSupportedException();
		}

		/// <summary>Creates a new child node at the end of the list of child nodes of the current node using the XML data string specified.</summary>
		/// <param name="newChild">The XML data string for the new child node.</param>
		/// <exception cref="T:System.ArgumentNullException">The XML data string parameter is null.</exception>
		/// <exception cref="T:System.InvalidOperationException">The current node the <see cref="T:System.Xml.XPath.XPathNavigator" /> is positioned on is not the root node or an element node.</exception>
		/// <exception cref="T:System.NotSupportedException">The <see cref="T:System.Xml.XPath.XPathNavigator" /> does not support editing.</exception>
		/// <exception cref="T:System.Xml.XmlException">The XML data string parameter is not well-formed.</exception>
		public virtual void AppendChild(string xmlFragments)
		{
			AppendChild(CreateFragmentReader(xmlFragments));
		}

		/// <summary>Creates a new child node at the end of the list of child nodes of the current node using the XML contents of the <see cref="T:System.Xml.XmlReader" /> object specified.</summary>
		/// <param name="newChild">An <see cref="T:System.Xml.XmlReader" /> object positioned on the XML data for the new child node.</param>
		/// <exception cref="T:System.ArgumentException">The <see cref="T:System.Xml.XmlReader" /> object is in an error state or closed.</exception>
		/// <exception cref="T:System.ArgumentNullException">The <see cref="T:System.Xml.XmlReader" /> object parameter is null.</exception>
		/// <exception cref="T:System.InvalidOperationException">The current node the <see cref="T:System.Xml.XPath.XPathNavigator" /> is positioned on is not the root node or an element node.</exception>
		/// <exception cref="T:System.NotSupportedException">The <see cref="T:System.Xml.XPath.XPathNavigator" /> does not support editing.</exception>
		/// <exception cref="T:System.Xml.XmlException">The XML contents of the <see cref="T:System.Xml.XmlReader" /> object parameter is not well-formed.</exception>
		public virtual void AppendChild(XmlReader reader)
		{
			XmlWriter xmlWriter = AppendChild();
			while (!reader.EOF)
			{
				xmlWriter.WriteNode(reader, defattr: false);
			}
			xmlWriter.Close();
		}

		/// <summary>Creates a new child node at the end of the list of child nodes of the current node using the nodes in the <see cref="T:System.Xml.XPath.XPathNavigator" /> specified.</summary>
		/// <param name="newChild">An <see cref="T:System.Xml.XPath.XPathNavigator" /> object positioned on the node to add as the new child node.</param>
		/// <exception cref="T:System.ArgumentNullException">The <see cref="T:System.Xml.XPath.XPathNavigator" /> object parameter is null.</exception>
		/// <exception cref="T:System.InvalidOperationException">The current node the <see cref="T:System.Xml.XPath.XPathNavigator" /> is positioned on is not the root node or an element node.</exception>
		/// <exception cref="T:System.NotSupportedException">The <see cref="T:System.Xml.XPath.XPathNavigator" /> does not support editing.</exception>
		public virtual void AppendChild(XPathNavigator nav)
		{
			AppendChild(new XPathNavigatorReader(nav));
		}

		/// <summary>Creates a new child element node at the end of the list of child nodes of the current node using the namespace prefix, local name and namespace URI specified with the value specified.</summary>
		/// <param name="prefix">The namespace prefix of the new child element node (if any).</param>
		/// <param name="localName">The local name of the new child element node (if any).</param>
		/// <param name="namespaceURI">The namespace URI of the new child element node (if any). <see cref="F:System.String.Empty" /> and null are equivalent.</param>
		/// <param name="value">The value of the new child element node. If <see cref="F:System.String.Empty" /> or null are passed, an empty element is created.</param>
		/// <exception cref="T:System.InvalidOperationException">The current node the <see cref="T:System.Xml.XPath.XPathNavigator" /> is positioned on is not the root node or an element node.</exception>
		/// <exception cref="T:System.NotSupportedException">The <see cref="T:System.Xml.XPath.XPathNavigator" /> does not support editing.</exception>
		public virtual void AppendChildElement(string prefix, string name, string ns, string value)
		{
			XmlWriter xmlWriter = AppendChild();
			xmlWriter.WriteStartElement(prefix, name, ns);
			xmlWriter.WriteString(value);
			xmlWriter.WriteEndElement();
			xmlWriter.Close();
		}

		/// <summary>Creates an attribute node on the current element node using the namespace prefix, local name and namespace URI specified with the value specified.</summary>
		/// <param name="prefix">The namespace prefix of the new attribute node (if any).</param>
		/// <param name="localName">The local name of the new attribute node which cannot <see cref="F:System.String.Empty" /> or null.</param>
		/// <param name="namespaceURI">The namespace URI for the new attribute node (if any).</param>
		/// <param name="value">The value of the new attribute node. If <see cref="F:System.String.Empty" /> or null are passed, an empty attribute node is created.</param>
		/// <exception cref="T:System.InvalidOperationException">The <see cref="T:System.Xml.XPath.XPathNavigator" /> is not positioned on an element node.</exception>
		/// <exception cref="T:System.NotSupportedException">The <see cref="T:System.Xml.XPath.XPathNavigator" /> does not support editing.</exception>
		public virtual void CreateAttribute(string prefix, string localName, string namespaceURI, string value)
		{
			using (XmlWriter xmlWriter = CreateAttributes())
			{
				xmlWriter.WriteAttributeString(prefix, localName, namespaceURI, value);
			}
		}

		/// <summary>Returns an <see cref="T:System.Xml.XmlWriter" /> object used to create new attributes on the current element.</summary>
		/// <returns>An <see cref="T:System.Xml.XmlWriter" /> object used to create new attributes on the current element.</returns>
		/// <exception cref="T:System.InvalidOperationException">The <see cref="T:System.Xml.XPath.XPathNavigator" /> is not positioned on an element node.</exception>
		/// <exception cref="T:System.NotSupportedException">The <see cref="T:System.Xml.XPath.XPathNavigator" /> does not support editing.</exception>
		public virtual XmlWriter CreateAttributes()
		{
			throw new NotSupportedException();
		}

		/// <summary>Deletes the current node and its child nodes.</summary>
		/// <exception cref="T:System.InvalidOperationException">The <see cref="T:System.Xml.XPath.XPathNavigator" /> is positioned on a node that cannot be deleted such as the root node or a namespace node.</exception>
		/// <exception cref="T:System.NotSupportedException">The <see cref="T:System.Xml.XPath.XPathNavigator" /> does not support editing.</exception>
		public virtual void DeleteSelf()
		{
			throw new NotSupportedException();
		}

		/// <summary>Deletes a range of sibling nodes from the current node to the node specified.</summary>
		/// <param name="lastSiblingToDelete">An <see cref="T:System.Xml.XPath.XPathNavigator" /> positioned on the last sibling node in the range to delete.</param>
		/// <exception cref="T:System.ArgumentNullException">The <see cref="T:System.Xml.XPath.XPathNavigator" /> specified is null.</exception>
		/// <exception cref="T:System.NotSupportedException">The <see cref="T:System.Xml.XPath.XPathNavigator" /> does not support editing.</exception>
		/// <exception cref="T:System.InvalidOperationException">The last node to delete specified is not a valid sibling node of the current node.</exception>
		public virtual void DeleteRange(XPathNavigator nav)
		{
			throw new NotSupportedException();
		}

		/// <summary>Replaces a range of sibling nodes from the current node to the node specified.</summary>
		/// <returns>An <see cref="T:System.Xml.XmlWriter" /> object used to specify the replacement range.</returns>
		/// <param name="lastSiblingToReplace">An <see cref="T:System.Xml.XPath.XPathNavigator" /> positioned on the last sibling node in the range to replace.</param>
		/// <exception cref="T:System.ArgumentNullException">The <see cref="T:System.Xml.XPath.XPathNavigator" /> specified is null.</exception>
		/// <exception cref="T:System.NotSupportedException">The <see cref="T:System.Xml.XPath.XPathNavigator" /> does not support editing.</exception>
		/// <exception cref="T:System.InvalidOperationException">The last node to replace specified is not a valid sibling node of the current node.</exception>
		public virtual XmlWriter ReplaceRange(XPathNavigator nav)
		{
			throw new NotSupportedException();
		}

		/// <summary>Returns an <see cref="T:System.Xml.XmlWriter" /> object used to create a new sibling node after the currently selected node.</summary>
		/// <returns>An <see cref="T:System.Xml.XmlWriter" /> object used to create a new sibling node after the currently selected node.</returns>
		/// <exception cref="T:System.InvalidOperationException">The position of the <see cref="T:System.Xml.XPath.XPathNavigator" /> does not allow a new sibling node to be inserted after the current node.</exception>
		/// <exception cref="T:System.NotSupportedException">The <see cref="T:System.Xml.XPath.XPathNavigator" /> does not support editing.</exception>
		public virtual XmlWriter InsertAfter()
		{
			switch (NodeType)
			{
			case XPathNodeType.Root:
			case XPathNodeType.Attribute:
			case XPathNodeType.Namespace:
				throw new InvalidOperationException($"Insertion after {NodeType} is not allowed.");
			default:
			{
				XPathNavigator xPathNavigator = Clone();
				if (xPathNavigator.MoveToNext())
				{
					return xPathNavigator.InsertBefore();
				}
				if (xPathNavigator.MoveToParent())
				{
					return xPathNavigator.AppendChild();
				}
				throw new InvalidOperationException("Could not move to parent to insert sibling node");
			}
			}
		}

		/// <summary>Creates a new sibling node after the currently selected node using the XML string specified.</summary>
		/// <param name="newSibling">The XML data string for the new sibling node.</param>
		/// <exception cref="T:System.ArgumentNullException">The XML string parameter is null.</exception>
		/// <exception cref="T:System.InvalidOperationException">The position of the <see cref="T:System.Xml.XPath.XPathNavigator" /> does not allow a new sibling node to be inserted after the current node.</exception>
		/// <exception cref="T:System.NotSupportedException">The <see cref="T:System.Xml.XPath.XPathNavigator" /> does not support editing.</exception>
		/// <exception cref="T:System.Xml.XmlException">The XML string parameter is not well-formed.</exception>
		public virtual void InsertAfter(string xmlFragments)
		{
			InsertAfter(CreateFragmentReader(xmlFragments));
		}

		/// <summary>Creates a new sibling node after the currently selected node using the XML contents of the <see cref="T:System.Xml.XmlReader" /> object specified.</summary>
		/// <param name="newSibling">An <see cref="T:System.Xml.XmlReader" /> object positioned on the XML data for the new sibling node.</param>
		/// <exception cref="T:System.ArgumentException">The <see cref="T:System.Xml.XmlReader" /> object is in an error state or closed.</exception>
		/// <exception cref="T:System.ArgumentNullException">The <see cref="T:System.Xml.XmlReader" /> object parameter is null.</exception>
		/// <exception cref="T:System.InvalidOperationException">The position of the <see cref="T:System.Xml.XPath.XPathNavigator" /> does not allow a new sibling node to be inserted after the current node.</exception>
		/// <exception cref="T:System.NotSupportedException">The <see cref="T:System.Xml.XPath.XPathNavigator" /> does not support editing.</exception>
		/// <exception cref="T:System.Xml.XmlException">The XML contents of the <see cref="T:System.Xml.XmlReader" /> object parameter is not well-formed.</exception>
		public virtual void InsertAfter(XmlReader reader)
		{
			using (XmlWriter xmlWriter = InsertAfter())
			{
				xmlWriter.WriteNode(reader, defattr: false);
			}
		}

		/// <summary>Creates a new sibling node after the currently selected node using the nodes in the <see cref="T:System.Xml.XPath.XPathNavigator" /> object specified.</summary>
		/// <param name="newSibling">An <see cref="T:System.Xml.XPath.XPathNavigator" /> object positioned on the node to add as the new sibling node.</param>
		/// <exception cref="T:System.ArgumentNullException">The <see cref="T:System.Xml.XPath.XPathNavigator" /> object parameter is null.</exception>
		/// <exception cref="T:System.InvalidOperationException">The position of the <see cref="T:System.Xml.XPath.XPathNavigator" /> does not allow a new sibling node to be inserted after the current node.</exception>
		/// <exception cref="T:System.NotSupportedException">The <see cref="T:System.Xml.XPath.XPathNavigator" /> does not support editing.</exception>
		public virtual void InsertAfter(XPathNavigator nav)
		{
			InsertAfter(new XPathNavigatorReader(nav));
		}

		/// <summary>Returns an <see cref="T:System.Xml.XmlWriter" /> object used to create a new sibling node before the currently selected node.</summary>
		/// <returns>An <see cref="T:System.Xml.XmlWriter" /> object used to create a new sibling node before the currently selected node.</returns>
		/// <exception cref="T:System.InvalidOperationException">The position of the <see cref="T:System.Xml.XPath.XPathNavigator" /> does not allow a new sibling node to be inserted before the current node.</exception>
		/// <exception cref="T:System.NotSupportedException">The <see cref="T:System.Xml.XPath.XPathNavigator" /> does not support editing.</exception>
		public virtual XmlWriter InsertBefore()
		{
			throw new NotSupportedException();
		}

		/// <summary>Creates a new sibling node before the currently selected node using the XML string specified.</summary>
		/// <param name="newSibling">The XML data string for the new sibling node.</param>
		/// <exception cref="T:System.ArgumentNullException">The XML string parameter is null.</exception>
		/// <exception cref="T:System.InvalidOperationException">The position of the <see cref="T:System.Xml.XPath.XPathNavigator" /> does not allow a new sibling node to be inserted before the current node.</exception>
		/// <exception cref="T:System.NotSupportedException">The <see cref="T:System.Xml.XPath.XPathNavigator" /> does not support editing.</exception>
		/// <exception cref="T:System.Xml.XmlException">The XML string parameter is not well-formed.</exception>
		public virtual void InsertBefore(string xmlFragments)
		{
			InsertBefore(CreateFragmentReader(xmlFragments));
		}

		/// <summary>Creates a new sibling node before the currently selected node using the XML contents of the <see cref="T:System.Xml.XmlReader" /> object specified.</summary>
		/// <param name="newSibling">An <see cref="T:System.Xml.XmlReader" /> object positioned on the XML data for the new sibling node.</param>
		/// <exception cref="T:System.ArgumentException">The <see cref="T:System.Xml.XmlReader" /> object is in an error state or closed.</exception>
		/// <exception cref="T:System.ArgumentNullException">The <see cref="T:System.Xml.XmlReader" /> object parameter is null.</exception>
		/// <exception cref="T:System.InvalidOperationException">The position of the <see cref="T:System.Xml.XPath.XPathNavigator" /> does not allow a new sibling node to be inserted before the current node.</exception>
		/// <exception cref="T:System.NotSupportedException">The <see cref="T:System.Xml.XPath.XPathNavigator" /> does not support editing.</exception>
		/// <exception cref="T:System.Xml.XmlException">The XML contents of the <see cref="T:System.Xml.XmlReader" /> object parameter is not well-formed.</exception>
		public virtual void InsertBefore(XmlReader reader)
		{
			using (XmlWriter xmlWriter = InsertBefore())
			{
				xmlWriter.WriteNode(reader, defattr: false);
			}
		}

		/// <summary>Creates a new sibling node before the currently selected node using the nodes in the <see cref="T:System.Xml.XPath.XPathNavigator" /> specified.</summary>
		/// <param name="newSibling">An <see cref="T:System.Xml.XPath.XPathNavigator" /> object positioned on the node to add as the new sibling node.</param>
		/// <exception cref="T:System.ArgumentNullException">The <see cref="T:System.Xml.XPath.XPathNavigator" /> object parameter is null.</exception>
		/// <exception cref="T:System.InvalidOperationException">The position of the <see cref="T:System.Xml.XPath.XPathNavigator" /> does not allow a new sibling node to be inserted before the current node.</exception>
		/// <exception cref="T:System.NotSupportedException">The <see cref="T:System.Xml.XPath.XPathNavigator" /> does not support editing.</exception>
		public virtual void InsertBefore(XPathNavigator nav)
		{
			InsertBefore(new XPathNavigatorReader(nav));
		}

		/// <summary>Creates a new sibling element after the current node using the namespace prefix, local name and namespace URI specified, with the value specified.</summary>
		/// <param name="prefix">The namespace prefix of the new child element (if any).</param>
		/// <param name="localName">The local name of the new child element (if any).</param>
		/// <param name="namespaceURI">The namespace URI of the new child element (if any). <see cref="F:System.String.Empty" /> and null are equivalent.</param>
		/// <param name="value">The value of the new child element. If <see cref="F:System.String.Empty" /> or null are passed, an empty element is created.</param>
		/// <exception cref="T:System.InvalidOperationException">The position of the <see cref="T:System.Xml.XPath.XPathNavigator" /> does not allow a new sibling node to be inserted after the current node.</exception>
		/// <exception cref="T:System.NotSupportedException">The <see cref="T:System.Xml.XPath.XPathNavigator" /> does not support editing.</exception>
		public virtual void InsertElementAfter(string prefix, string localName, string namespaceURI, string value)
		{
			using (XmlWriter xmlWriter = InsertAfter())
			{
				xmlWriter.WriteElementString(prefix, localName, namespaceURI, value);
			}
		}

		/// <summary>Creates a new sibling element before the current node using the namespace prefix, local name, and namespace URI specified, with the value specified.</summary>
		/// <param name="prefix">The namespace prefix of the new child element (if any).</param>
		/// <param name="localName">The local name of the new child element (if any).</param>
		/// <param name="namespaceURI">The namespace URI of the new child element (if any). <see cref="F:System.String.Empty" /> and null are equivalent.</param>
		/// <param name="value">The value of the new child element. If <see cref="F:System.String.Empty" /> or null are passed, an empty element is created.</param>
		/// <exception cref="T:System.InvalidOperationException">The position of the <see cref="T:System.Xml.XPath.XPathNavigator" /> does not allow a new sibling node to be inserted before the current node.</exception>
		/// <exception cref="T:System.NotSupportedException">The <see cref="T:System.Xml.XPath.XPathNavigator" /> does not support editing.</exception>
		public virtual void InsertElementBefore(string prefix, string localName, string namespaceURI, string value)
		{
			using (XmlWriter xmlWriter = InsertBefore())
			{
				xmlWriter.WriteElementString(prefix, localName, namespaceURI, value);
			}
		}

		/// <summary>Returns an <see cref="T:System.Xml.XmlWriter" /> object used to create a new child node at the beginning of the list of child nodes of the current node.</summary>
		/// <returns>An <see cref="T:System.Xml.XmlWriter" /> object used to create a new child node at the beginning of the list of child nodes of the current node.</returns>
		/// <exception cref="T:System.InvalidOperationException">The current node the <see cref="T:System.Xml.XPath.XPathNavigator" /> is positioned on does not allow a new child node to be prepended.</exception>
		/// <exception cref="T:System.NotSupportedException">The <see cref="T:System.Xml.XPath.XPathNavigator" /> does not support editing.</exception>
		public virtual XmlWriter PrependChild()
		{
			XPathNavigator xPathNavigator = Clone();
			if (xPathNavigator.MoveToFirstChild())
			{
				return xPathNavigator.InsertBefore();
			}
			return AppendChild();
		}

		/// <summary>Creates a new child node at the beginning of the list of child nodes of the current node using the XML string specified.</summary>
		/// <param name="newChild">The XML data string for the new child node.</param>
		/// <exception cref="T:System.ArgumentNullException">The XML string parameter is null.</exception>
		/// <exception cref="T:System.InvalidOperationException">The current node the <see cref="T:System.Xml.XPath.XPathNavigator" /> is positioned on does not allow a new child node to be prepended.</exception>
		/// <exception cref="T:System.NotSupportedException">The <see cref="T:System.Xml.XPath.XPathNavigator" /> does not support editing.</exception>
		/// <exception cref="T:System.Xml.XmlException">The XML string parameter is not well-formed.</exception>
		public virtual void PrependChild(string xmlFragments)
		{
			PrependChild(CreateFragmentReader(xmlFragments));
		}

		/// <summary>Creates a new child node at the beginning of the list of child nodes of the current node using the XML contents of the <see cref="T:System.Xml.XmlReader" /> object specified.</summary>
		/// <param name="newChild">An <see cref="T:System.Xml.XmlReader" /> object positioned on the XML data for the new child node.</param>
		/// <exception cref="T:System.ArgumentException">The <see cref="T:System.Xml.XmlReader" /> object is in an error state or closed.</exception>
		/// <exception cref="T:System.ArgumentNullException">The <see cref="T:System.Xml.XmlReader" /> object parameter is null.</exception>
		/// <exception cref="T:System.InvalidOperationException">The current node the <see cref="T:System.Xml.XPath.XPathNavigator" /> is positioned on does not allow a new child node to be prepended.</exception>
		/// <exception cref="T:System.NotSupportedException">The <see cref="T:System.Xml.XPath.XPathNavigator" /> does not support editing.</exception>
		/// <exception cref="T:System.Xml.XmlException">The XML contents of the <see cref="T:System.Xml.XmlReader" /> object parameter is not well-formed.</exception>
		public virtual void PrependChild(XmlReader reader)
		{
			using (XmlWriter xmlWriter = PrependChild())
			{
				xmlWriter.WriteNode(reader, defattr: false);
			}
		}

		/// <summary>Creates a new child node at the beginning of the list of child nodes of the current node using the nodes in the <see cref="T:System.Xml.XPath.XPathNavigator" /> object specified.</summary>
		/// <param name="newChild">An <see cref="T:System.Xml.XPath.XPathNavigator" /> object positioned on the node to add as the new child node.</param>
		/// <exception cref="T:System.ArgumentNullException">The <see cref="T:System.Xml.XPath.XPathNavigator" /> object parameter is null.</exception>
		/// <exception cref="T:System.InvalidOperationException">The current node the <see cref="T:System.Xml.XPath.XPathNavigator" /> is positioned on does not allow a new child node to be prepended.</exception>
		/// <exception cref="T:System.NotSupportedException">The <see cref="T:System.Xml.XPath.XPathNavigator" /> does not support editing.</exception>
		public virtual void PrependChild(XPathNavigator nav)
		{
			PrependChild(new XPathNavigatorReader(nav));
		}

		/// <summary>Creates a new child element at the beginning of the list of child nodes of the current node using the namespace prefix, local name, and namespace URI specified with the value specified.</summary>
		/// <param name="prefix">The namespace prefix of the new child element (if any).</param>
		/// <param name="localName">The local name of the new child element (if any).</param>
		/// <param name="namespaceURI">The namespace URI of the new child element (if any). <see cref="F:System.String.Empty" /> and null are equivalent.</param>
		/// <param name="value">The value of the new child element. If <see cref="F:System.String.Empty" /> or null are passed, an empty element is created.</param>
		/// <exception cref="T:System.InvalidOperationException">The current node the <see cref="T:System.Xml.XPath.XPathNavigator" /> is positioned on does not allow a new child node to be prepended.</exception>
		/// <exception cref="T:System.NotSupportedException">The <see cref="T:System.Xml.XPath.XPathNavigator" /> does not support editing.</exception>
		public virtual void PrependChildElement(string prefix, string localName, string namespaceURI, string value)
		{
			using (XmlWriter xmlWriter = PrependChild())
			{
				xmlWriter.WriteElementString(prefix, localName, namespaceURI, value);
			}
		}

		/// <summary>Replaces the current node with the content of the string specified.</summary>
		/// <returns>true if the <see cref="T:System.Xml.XPath.XPathNavigator" /> is positioned on the new node; otherwise false.</returns>
		/// <param name="newNode">The XML data string for the new node.</param>
		/// <exception cref="T:System.ArgumentNullException">The XML string parameter is null.</exception>
		/// <exception cref="T:System.InvalidOperationException">The <see cref="T:System.Xml.XPath.XPathNavigator" /> is not positioned on an element, text, processing instruction, or comment node.</exception>
		/// <exception cref="T:System.NotSupportedException">The <see cref="T:System.Xml.XPath.XPathNavigator" /> does not support editing.</exception>
		/// <exception cref="T:System.Xml.XmlException">The XML string parameter is not well-formed.</exception>
		public virtual void ReplaceSelf(string xmlFragment)
		{
			ReplaceSelf(CreateFragmentReader(xmlFragment));
		}

		/// <summary>Replaces the current node with the contents of the <see cref="T:System.Xml.XmlReader" /> object specified.</summary>
		/// <returns>true if the <see cref="T:System.Xml.XPath.XPathNavigator" /> is positioned on the new node; otherwise false.</returns>
		/// <param name="newNode">An <see cref="T:System.Xml.XmlReader" /> object positioned on the XML data for the new node.</param>
		/// <exception cref="T:System.ArgumentException">The <see cref="T:System.Xml.XmlReader" /> object is in an error state or closed.</exception>
		/// <exception cref="T:System.ArgumentNullException">The <see cref="T:System.Xml.XmlReader" /> object parameter is null.</exception>
		/// <exception cref="T:System.InvalidOperationException">The <see cref="T:System.Xml.XPath.XPathNavigator" /> is not positioned on an element, text, processing instruction, or comment node.</exception>
		/// <exception cref="T:System.NotSupportedException">The <see cref="T:System.Xml.XPath.XPathNavigator" /> does not support editing.</exception>
		/// <exception cref="T:System.Xml.XmlException">The XML contents of the <see cref="T:System.Xml.XmlReader" /> object parameter is not well-formed.</exception>
		public virtual void ReplaceSelf(XmlReader reader)
		{
			throw new NotSupportedException();
		}

		/// <summary>Replaces the current node with the contents of the <see cref="T:System.Xml.XPath.XPathNavigator" /> object specified.</summary>
		/// <returns>true if the <see cref="T:System.Xml.XPath.XPathNavigator" /> is positioned on the new node; otherwise false.</returns>
		/// <param name="newNode">An <see cref="T:System.Xml.XPath.XPathNavigator" /> object positioned on the new node.</param>
		/// <exception cref="T:System.ArgumentNullException">The <see cref="T:System.Xml.XPath.XPathNavigator" /> object parameter is null.</exception>
		/// <exception cref="T:System.InvalidOperationException">The <see cref="T:System.Xml.XPath.XPathNavigator" /> is not positioned on an element, text, processing instruction, or comment node.</exception>
		/// <exception cref="T:System.NotSupportedException">The <see cref="T:System.Xml.XPath.XPathNavigator" /> does not support editing.</exception>
		/// <exception cref="T:System.Xml.XmlException">The XML contents of the <see cref="T:System.Xml.XPath.XPathNavigator" /> object parameter is not well-formed.</exception>
		public virtual void ReplaceSelf(XPathNavigator navigator)
		{
			ReplaceSelf(new XPathNavigatorReader(navigator));
		}

		/// <summary>Sets the typed value of the current node.</summary>
		/// <param name="typedValue">The new typed value of the node.</param>
		/// <exception cref="T:System.ArgumentException">The <see cref="T:System.Xml.XPath.XPathNavigator" /> does not support the type of the object specified.</exception>
		/// <exception cref="T:System.ArgumentNullException">The value specified cannot be null.</exception>
		/// <exception cref="T:System.InvalidOperationException">The <see cref="T:System.Xml.XPath.XPathNavigator" /> is not positioned on an element or attribute node.</exception>
		/// <exception cref="T:System.NotSupportedException">The <see cref="T:System.Xml.XPath.XPathNavigator" /> does not support editing.</exception>
		[MonoTODO]
		public virtual void SetTypedValue(object value)
		{
			throw new NotSupportedException();
		}

		/// <summary>Sets the value of the current node.</summary>
		/// <param name="value">The new value of the node.</param>
		/// <exception cref="T:System.ArgumentNullException">The value parameter is null.</exception>
		/// <exception cref="T:System.InvalidOperationException">The <see cref="T:System.Xml.XPath.XPathNavigator" /> is positioned on the root node, a namespace node, or the specified value is invalid.</exception>
		/// <exception cref="T:System.NotSupportedException">The <see cref="T:System.Xml.XPath.XPathNavigator" /> does not support editing.</exception>
		public virtual void SetValue(string value)
		{
			throw new NotSupportedException();
		}

		private void DeleteChildren()
		{
			switch (NodeType)
			{
			case XPathNodeType.Attribute:
				return;
			case XPathNodeType.Namespace:
				throw new InvalidOperationException("Removing namespace node content is not supported.");
			case XPathNodeType.Text:
			case XPathNodeType.SignificantWhitespace:
			case XPathNodeType.Whitespace:
			case XPathNodeType.ProcessingInstruction:
			case XPathNodeType.Comment:
				DeleteSelf();
				return;
			}
			if (HasChildren)
			{
				XPathNavigator xPathNavigator = Clone();
				xPathNavigator.MoveToFirstChild();
				while (!xPathNavigator.IsSamePosition(this))
				{
					xPathNavigator.DeleteSelf();
				}
			}
		}
	}
}
