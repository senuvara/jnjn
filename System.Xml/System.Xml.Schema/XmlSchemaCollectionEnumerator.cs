using System.Collections;

namespace System.Xml.Schema
{
	/// <summary>Supports a simple iteration over a collection. This class cannot be inherited.</summary>
	public sealed class XmlSchemaCollectionEnumerator : IEnumerator
	{
		private IEnumerator xenum;

		/// <summary>For a description of this member, see <see cref="P:System.Xml.Schema.XmlSchemaCollectionEnumerator.Current" />.</summary>
		/// <returns />
		object IEnumerator.Current => xenum.Current;

		/// <summary>Gets the current <see cref="T:System.Xml.Schema.XmlSchema" /> in the collection.</summary>
		/// <returns>The current XmlSchema in the collection.</returns>
		public XmlSchema Current => (XmlSchema)xenum.Current;

		internal XmlSchemaCollectionEnumerator(ICollection col)
		{
			xenum = col.GetEnumerator();
		}

		/// <summary>For a description of this member, see <see cref="M:System.Xml.Schema.XmlSchemaCollectionEnumerator.MoveNext" />.</summary>
		/// <returns />
		bool IEnumerator.MoveNext()
		{
			return xenum.MoveNext();
		}

		/// <summary>For a description of this member, see <see cref="M:System.Xml.Schema.XmlSchemaCollectionEnumerator.System.Collections.IEnumerator.Reset" />.</summary>
		void IEnumerator.Reset()
		{
			xenum.Reset();
		}

		/// <summary>Advances the enumerator to the next schema in the collection.</summary>
		/// <returns>true if the move was successful; false if the enumerator has passed the end of the collection.</returns>
		public bool MoveNext()
		{
			return xenum.MoveNext();
		}
	}
}
