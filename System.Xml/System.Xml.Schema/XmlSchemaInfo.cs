namespace System.Xml.Schema
{
	/// <summary>Represents the post-schema-validation infoset of a validated XML node.</summary>
	[MonoTODO]
	public class XmlSchemaInfo : IXmlSchemaInfo
	{
		private bool isDefault;

		private bool isNil;

		private XmlSchemaSimpleType memberType;

		private XmlSchemaAttribute attr;

		private XmlSchemaElement elem;

		private XmlSchemaType type;

		private XmlSchemaValidity validity;

		private XmlSchemaContentType contentType;

		/// <summary>Gets or sets the <see cref="T:System.Xml.Schema.XmlSchemaContentType" /> object that corresponds to the content type of this validated XML node.</summary>
		/// <returns>An <see cref="T:System.Xml.Schema.XmlSchemaContentType" /> object.</returns>
		[MonoTODO]
		public XmlSchemaContentType ContentType
		{
			get
			{
				return contentType;
			}
			set
			{
				contentType = value;
			}
		}

		/// <summary>Gets or sets a value indicating if this validated XML node was set as the result of a default being applied during XML Schema Definition Language (XSD) schema validation.</summary>
		/// <returns>A bool value.</returns>
		[MonoTODO]
		public bool IsDefault
		{
			get
			{
				return isDefault;
			}
			set
			{
				isDefault = value;
			}
		}

		/// <summary>Gets or sets a value indicating if the value for this validated XML node is nil.</summary>
		/// <returns>A bool value.</returns>
		[MonoTODO]
		public bool IsNil
		{
			get
			{
				return isNil;
			}
			set
			{
				isNil = value;
			}
		}

		/// <summary>Gets or sets the dynamic schema type for this validated XML node.</summary>
		/// <returns>An <see cref="T:System.Xml.Schema.XmlSchemaSimpleType" /> object.</returns>
		[MonoTODO]
		public XmlSchemaSimpleType MemberType
		{
			get
			{
				return memberType;
			}
			set
			{
				memberType = value;
			}
		}

		/// <summary>Gets or sets the compiled <see cref="T:System.Xml.Schema.XmlSchemaAttribute" /> object that corresponds to this validated XML node.</summary>
		/// <returns>An <see cref="T:System.Xml.Schema.XmlSchemaAttribute" /> object.</returns>
		[MonoTODO]
		public XmlSchemaAttribute SchemaAttribute
		{
			get
			{
				return attr;
			}
			set
			{
				attr = value;
			}
		}

		/// <summary>Gets or sets the compiled <see cref="T:System.Xml.Schema.XmlSchemaElement" /> object that corresponds to this validated XML node.</summary>
		/// <returns>An <see cref="T:System.Xml.Schema.XmlSchemaElement" /> object.</returns>
		[MonoTODO]
		public XmlSchemaElement SchemaElement
		{
			get
			{
				return elem;
			}
			set
			{
				elem = value;
			}
		}

		/// <summary>Gets or sets the static XML Schema Definition Language (XSD) schema type of this validated XML node.</summary>
		/// <returns>An <see cref="T:System.Xml.Schema.XmlSchemaType" /> object.</returns>
		[MonoTODO]
		public XmlSchemaType SchemaType
		{
			get
			{
				return type;
			}
			set
			{
				type = value;
			}
		}

		/// <summary>Gets or sets the <see cref="T:System.Xml.Schema.XmlSchemaValidity" /> value of this validated XML node.</summary>
		/// <returns>An <see cref="T:System.Xml.Schema.XmlSchemaValidity" /> value.</returns>
		[MonoTODO]
		public XmlSchemaValidity Validity
		{
			get
			{
				return validity;
			}
			set
			{
				validity = value;
			}
		}

		/// <summary>Initializes a new instance of the <see cref="T:System.Xml.Schema.XmlSchemaInfo" /> class.</summary>
		public XmlSchemaInfo()
		{
		}

		internal XmlSchemaInfo(IXmlSchemaInfo info)
		{
			isDefault = info.IsDefault;
			isNil = info.IsNil;
			memberType = info.MemberType;
			attr = info.SchemaAttribute;
			elem = info.SchemaElement;
			type = info.SchemaType;
			validity = info.Validity;
		}
	}
}
