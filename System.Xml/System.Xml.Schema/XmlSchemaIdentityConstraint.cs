using Mono.Xml.Schema;
using System.Xml.Serialization;

namespace System.Xml.Schema
{
	/// <summary>Class for the identity constraints: key, keyref, and unique elements.</summary>
	public class XmlSchemaIdentityConstraint : XmlSchemaAnnotated
	{
		private XmlSchemaObjectCollection fields;

		private string name;

		private XmlQualifiedName qName;

		private XmlSchemaXPath selector;

		private XsdIdentitySelector compiledSelector;

		/// <summary>Gets or sets the name of the identity constraint.</summary>
		/// <returns>The name of the identity constraint.</returns>
		[XmlAttribute("name")]
		public string Name
		{
			get
			{
				return name;
			}
			set
			{
				name = value;
			}
		}

		/// <summary>Gets or sets the XPath expression selector element.</summary>
		/// <returns>The XPath expression selector element.</returns>
		[XmlElement("selector", typeof(XmlSchemaXPath))]
		public XmlSchemaXPath Selector
		{
			get
			{
				return selector;
			}
			set
			{
				selector = value;
			}
		}

		/// <summary>Gets the collection of fields that apply as children for the XML Path Language (XPath) expression selector.</summary>
		/// <returns>The collection of fields.</returns>
		[XmlElement("field", typeof(XmlSchemaXPath))]
		public XmlSchemaObjectCollection Fields => fields;

		/// <summary>Gets the qualified name of the identity constraint, which holds the post-compilation value of the QualifiedName property.</summary>
		/// <returns>The post-compilation value of the QualifiedName property.</returns>
		[XmlIgnore]
		public XmlQualifiedName QualifiedName => qName;

		internal XsdIdentitySelector CompiledSelector => compiledSelector;

		/// <summary>Initializes a new instance of the <see cref="T:System.Xml.Schema.XmlSchemaIdentityConstraint" /> class.</summary>
		public XmlSchemaIdentityConstraint()
		{
			fields = new XmlSchemaObjectCollection();
			qName = XmlQualifiedName.Empty;
		}

		internal override void SetParent(XmlSchemaObject parent)
		{
			base.SetParent(parent);
			if (Selector != null)
			{
				Selector.SetParent(this);
			}
			foreach (XmlSchemaObject field in Fields)
			{
				field.SetParent(this);
			}
		}

		internal override int Compile(ValidationEventHandler h, XmlSchema schema)
		{
			if (CompilationId == schema.CompilationId)
			{
				return 0;
			}
			if (Name == null)
			{
				error(h, "Required attribute name must be present");
			}
			else if (!XmlSchemaUtil.CheckNCName(name))
			{
				error(h, "attribute name must be NCName");
			}
			else
			{
				qName = new XmlQualifiedName(Name, base.AncestorSchema.TargetNamespace);
				if (schema.NamedIdentities.Contains(qName))
				{
					XmlSchemaIdentityConstraint xmlSchemaIdentityConstraint = schema.NamedIdentities[qName] as XmlSchemaIdentityConstraint;
					error(h, $"There is already same named identity constraint in this namespace. Existing item is at {xmlSchemaIdentityConstraint.SourceUri}({xmlSchemaIdentityConstraint.LineNumber},{xmlSchemaIdentityConstraint.LinePosition})");
				}
				else
				{
					schema.NamedIdentities.Add(qName, this);
				}
			}
			if (Selector == null)
			{
				error(h, "selector must be present");
			}
			else
			{
				Selector.isSelector = true;
				errorCount += Selector.Compile(h, schema);
				if (selector.errorCount == 0)
				{
					compiledSelector = new XsdIdentitySelector(Selector);
				}
			}
			if (errorCount > 0)
			{
				return errorCount;
			}
			if (Fields.Count == 0)
			{
				error(h, "atleast one field value must be present");
			}
			else
			{
				for (int i = 0; i < Fields.Count; i++)
				{
					XmlSchemaXPath xmlSchemaXPath = Fields[i] as XmlSchemaXPath;
					if (xmlSchemaXPath != null)
					{
						errorCount += xmlSchemaXPath.Compile(h, schema);
						if (xmlSchemaXPath.errorCount == 0)
						{
							compiledSelector.AddField(new XsdIdentityField(xmlSchemaXPath, i));
						}
					}
					else
					{
						error(h, string.Concat("Object of type ", Fields[i].GetType(), " is invalid in the Fields Collection"));
					}
				}
			}
			XmlSchemaUtil.CompileID(base.Id, this, schema.IDCollection, h);
			CompilationId = schema.CompilationId;
			return errorCount;
		}
	}
}
