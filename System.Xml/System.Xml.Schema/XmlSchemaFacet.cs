using System.ComponentModel;
using System.Xml.Serialization;

namespace System.Xml.Schema
{
	/// <summary>Abstract class for all facets that are used when simple types are derived by restriction.</summary>
	public abstract class XmlSchemaFacet : XmlSchemaAnnotated
	{
		[Flags]
		protected internal enum Facet
		{
			None = 0x0,
			length = 0x1,
			minLength = 0x2,
			maxLength = 0x4,
			pattern = 0x8,
			enumeration = 0x10,
			whiteSpace = 0x20,
			maxInclusive = 0x40,
			maxExclusive = 0x80,
			minExclusive = 0x100,
			minInclusive = 0x200,
			totalDigits = 0x400,
			fractionDigits = 0x800
		}

		internal static readonly Facet AllFacets = Facet.length | Facet.minLength | Facet.maxLength | Facet.pattern | Facet.enumeration | Facet.whiteSpace | Facet.maxInclusive | Facet.maxExclusive | Facet.minExclusive | Facet.minInclusive | Facet.totalDigits | Facet.fractionDigits;

		private bool isFixed;

		private string val;

		internal virtual Facet ThisFacet => Facet.None;

		/// <summary>Gets or sets the value attribute of the facet.</summary>
		/// <returns>The value attribute.</returns>
		[XmlAttribute("value")]
		public string Value
		{
			get
			{
				return val;
			}
			set
			{
				val = value;
			}
		}

		/// <summary>Gets or sets information that indicates that this facet is fixed.</summary>
		/// <returns>If true, value is fixed; otherwise, false. The default is false.Optional.</returns>
		[DefaultValue(false)]
		[XmlAttribute("fixed")]
		public virtual bool IsFixed
		{
			get
			{
				return isFixed;
			}
			set
			{
				isFixed = value;
			}
		}
	}
}
