using System.Collections;
using System.Collections.Specialized;

namespace System.Xml.Schema
{
	/// <summary>Provides the collections for contained elements in the <see cref="T:System.Xml.Schema.XmlSchema" /> class (for example, Attributes, AttributeGroups, Elements, and so on).</summary>
	public class XmlSchemaObjectTable
	{
		internal class XmlSchemaObjectTableEnumerator : IEnumerator, IDictionaryEnumerator
		{
			private IDictionaryEnumerator xenum;

			private IEnumerable tmp;

			object IEnumerator.Current => xenum.Entry;

			DictionaryEntry IDictionaryEnumerator.Entry => xenum.Entry;

			object IDictionaryEnumerator.Key => (XmlQualifiedName)xenum.Key;

			object IDictionaryEnumerator.Value => (XmlSchemaObject)xenum.Value;

			public XmlSchemaObject Current => (XmlSchemaObject)xenum.Value;

			public DictionaryEntry Entry => xenum.Entry;

			public XmlQualifiedName Key => (XmlQualifiedName)xenum.Key;

			public XmlSchemaObject Value => (XmlSchemaObject)xenum.Value;

			internal XmlSchemaObjectTableEnumerator(XmlSchemaObjectTable table)
			{
				tmp = table.table;
				xenum = (IDictionaryEnumerator)tmp.GetEnumerator();
			}

			bool IEnumerator.MoveNext()
			{
				return xenum.MoveNext();
			}

			void IEnumerator.Reset()
			{
				xenum.Reset();
			}

			public bool MoveNext()
			{
				return xenum.MoveNext();
			}
		}

		private HybridDictionary table;

		/// <summary>Gets the number of items contained in the <see cref="T:System.Xml.Schema.XmlSchemaObjectTable" />.</summary>
		/// <returns>The number of items contained in the <see cref="T:System.Xml.Schema.XmlSchemaObjectTable" />.</returns>
		public int Count => table.Count;

		/// <summary>Returns the element in the <see cref="T:System.Xml.Schema.XmlSchemaObjectTable" /> specified by qualified name.</summary>
		/// <returns>The <see cref="T:System.Xml.Schema.XmlSchemaObject" /> of the element in the <see cref="T:System.Xml.Schema.XmlSchemaObjectTable" /> specified by qualified name.</returns>
		/// <param name="name">The <see cref="T:System.Xml.XmlQualifiedName" /> of the element to return.</param>
		public XmlSchemaObject this[XmlQualifiedName name] => (XmlSchemaObject)table[name];

		/// <summary>Returns a collection of all the named elements in the <see cref="T:System.Xml.Schema.XmlSchemaObjectTable" />.</summary>
		/// <returns>A collection of all the named elements in the <see cref="T:System.Xml.Schema.XmlSchemaObjectTable" />.</returns>
		public ICollection Names => table.Keys;

		/// <summary>Returns a collection of all the values for all the elements in the <see cref="T:System.Xml.Schema.XmlSchemaObjectTable" />.</summary>
		/// <returns>A collection of all the values for all the elements in the <see cref="T:System.Xml.Schema.XmlSchemaObjectTable" />.</returns>
		public ICollection Values => table.Values;

		internal XmlSchemaObjectTable()
		{
			table = new HybridDictionary();
		}

		/// <summary>Determines if the qualified name specified exists in the collection.</summary>
		/// <returns>true if the qualified name specified exists in the collection; otherwise, false.</returns>
		/// <param name="name">The <see cref="T:System.Xml.XmlQualifiedName" />.</param>
		public bool Contains(XmlQualifiedName name)
		{
			return table.Contains(name);
		}

		/// <summary>Returns an enumerator that can iterate through the <see cref="T:System.Xml.Schema.XmlSchemaObjectTable" />.</summary>
		/// <returns>An <see cref="T:System.Collections.IDictionaryEnumerator" /> that can iterate through <see cref="T:System.Xml.Schema.XmlSchemaObjectTable" />.</returns>
		public IDictionaryEnumerator GetEnumerator()
		{
			return new XmlSchemaObjectTableEnumerator(this);
		}

		internal void Add(XmlQualifiedName name, XmlSchemaObject value)
		{
			table[name] = value;
		}

		internal void Clear()
		{
			table.Clear();
		}

		internal void Set(XmlQualifiedName name, XmlSchemaObject value)
		{
			table[name] = value;
		}
	}
}
