using Mono.Xml.Schema;
using System.Text;

namespace System.Xml.Schema
{
	/// <summary>The <see cref="T:System.Xml.Schema.XmlSchemaDatatype" /> class is an abstract class for mapping XML Schema definition language (XSD) types to Common Language Runtime (CLR) types.</summary>
	public abstract class XmlSchemaDatatype
	{
		internal XsdWhitespaceFacet WhitespaceValue;

		private static char[] wsChars = new char[4]
		{
			' ',
			'\t',
			'\n',
			'\r'
		};

		private StringBuilder sb = new StringBuilder();

		private static readonly XsdAnySimpleType datatypeAnySimpleType = XsdAnySimpleType.Instance;

		private static readonly XsdString datatypeString = new XsdString();

		private static readonly XsdNormalizedString datatypeNormalizedString = new XsdNormalizedString();

		private static readonly XsdToken datatypeToken = new XsdToken();

		private static readonly XsdLanguage datatypeLanguage = new XsdLanguage();

		private static readonly XsdNMToken datatypeNMToken = new XsdNMToken();

		private static readonly XsdNMTokens datatypeNMTokens = new XsdNMTokens();

		private static readonly XsdName datatypeName = new XsdName();

		private static readonly XsdNCName datatypeNCName = new XsdNCName();

		private static readonly XsdID datatypeID = new XsdID();

		private static readonly XsdIDRef datatypeIDRef = new XsdIDRef();

		private static readonly XsdIDRefs datatypeIDRefs = new XsdIDRefs();

		private static readonly XsdEntity datatypeEntity = new XsdEntity();

		private static readonly XsdEntities datatypeEntities = new XsdEntities();

		private static readonly XsdNotation datatypeNotation = new XsdNotation();

		private static readonly XsdDecimal datatypeDecimal = new XsdDecimal();

		private static readonly XsdInteger datatypeInteger = new XsdInteger();

		private static readonly XsdLong datatypeLong = new XsdLong();

		private static readonly XsdInt datatypeInt = new XsdInt();

		private static readonly XsdShort datatypeShort = new XsdShort();

		private static readonly XsdByte datatypeByte = new XsdByte();

		private static readonly XsdNonNegativeInteger datatypeNonNegativeInteger = new XsdNonNegativeInteger();

		private static readonly XsdPositiveInteger datatypePositiveInteger = new XsdPositiveInteger();

		private static readonly XsdUnsignedLong datatypeUnsignedLong = new XsdUnsignedLong();

		private static readonly XsdUnsignedInt datatypeUnsignedInt = new XsdUnsignedInt();

		private static readonly XsdUnsignedShort datatypeUnsignedShort = new XsdUnsignedShort();

		private static readonly XsdUnsignedByte datatypeUnsignedByte = new XsdUnsignedByte();

		private static readonly XsdNonPositiveInteger datatypeNonPositiveInteger = new XsdNonPositiveInteger();

		private static readonly XsdNegativeInteger datatypeNegativeInteger = new XsdNegativeInteger();

		private static readonly XsdFloat datatypeFloat = new XsdFloat();

		private static readonly XsdDouble datatypeDouble = new XsdDouble();

		private static readonly XsdBase64Binary datatypeBase64Binary = new XsdBase64Binary();

		private static readonly XsdBoolean datatypeBoolean = new XsdBoolean();

		private static readonly XsdAnyURI datatypeAnyURI = new XsdAnyURI();

		private static readonly XsdDuration datatypeDuration = new XsdDuration();

		private static readonly XsdDateTime datatypeDateTime = new XsdDateTime();

		private static readonly XsdDate datatypeDate = new XsdDate();

		private static readonly XsdTime datatypeTime = new XsdTime();

		private static readonly XsdHexBinary datatypeHexBinary = new XsdHexBinary();

		private static readonly XsdQName datatypeQName = new XsdQName();

		private static readonly XsdGYearMonth datatypeGYearMonth = new XsdGYearMonth();

		private static readonly XsdGMonthDay datatypeGMonthDay = new XsdGMonthDay();

		private static readonly XsdGYear datatypeGYear = new XsdGYear();

		private static readonly XsdGMonth datatypeGMonth = new XsdGMonth();

		private static readonly XsdGDay datatypeGDay = new XsdGDay();

		private static readonly XdtAnyAtomicType datatypeAnyAtomicType = new XdtAnyAtomicType();

		private static readonly XdtUntypedAtomic datatypeUntypedAtomic = new XdtUntypedAtomic();

		private static readonly XdtDayTimeDuration datatypeDayTimeDuration = new XdtDayTimeDuration();

		private static readonly XdtYearMonthDuration datatypeYearMonthDuration = new XdtYearMonthDuration();

		internal virtual XsdWhitespaceFacet Whitespace => WhitespaceValue;

		/// <summary>Gets the <see cref="T:System.Xml.Schema.XmlTypeCode" /> value for the simple type.</summary>
		/// <returns>The <see cref="T:System.Xml.Schema.XmlTypeCode" /> value for the simple type.</returns>
		public virtual XmlTypeCode TypeCode => XmlTypeCode.None;

		/// <summary>Gets the <see cref="T:System.Xml.Schema.XmlSchemaDatatypeVariety" /> value for the simple type.</summary>
		/// <returns>The <see cref="T:System.Xml.Schema.XmlSchemaDatatypeVariety" /> value for the simple type.</returns>
		public virtual XmlSchemaDatatypeVariety Variety => XmlSchemaDatatypeVariety.Atomic;

		/// <summary>When overridden in a derived class, gets the type for the string as specified in the World Wide Web Consortium (W3C) XML 1.0 specification.</summary>
		/// <returns>An <see cref="T:System.Xml.XmlTokenizedType" /> value for the string.</returns>
		public abstract XmlTokenizedType TokenizedType
		{
			get;
		}

		/// <summary>When overridden in a derived class, gets the Common Language Runtime (CLR) type of the item.</summary>
		/// <returns>The Common Language Runtime (CLR) type of the item.</returns>
		public abstract Type ValueType
		{
			get;
		}

		/// <summary>Converts the value specified, whose type is one of the valid Common Language Runtime (CLR) representations of the XML schema type represented by the <see cref="T:System.Xml.Schema.XmlSchemaDatatype" />, to the CLR type specified.</summary>
		/// <returns>The converted input value.</returns>
		/// <param name="value">The input value to convert to the specified type.</param>
		/// <param name="targetType">The target type to convert the input value to.</param>
		/// <exception cref="T:System.ArgumentNullException">The <see cref="T:System.Object" /> or <see cref="T:System.Type" /> parameter is null.</exception>
		/// <exception cref="T:System.InvalidCastException">The type represented by the <see cref="T:System.Xml.Schema.XmlSchemaDatatype" />   does not support a conversion from type of the value specified to the type specified.</exception>
		[MonoTODO]
		public virtual object ChangeType(object value, Type targetType)
		{
			return ChangeType(value, targetType, null);
		}

		/// <summary>Converts the value specified, whose type is one of the valid Common Language Runtime (CLR) representations of the XML schema type represented by the <see cref="T:System.Xml.Schema.XmlSchemaDatatype" />, to the CLR type specified using the <see cref="T:System.Xml.IXmlNamespaceResolver" /> if the <see cref="T:System.Xml.Schema.XmlSchemaDatatype" /> represents the xs:QName type or a type derived from it.</summary>
		/// <returns>The converted input value.</returns>
		/// <param name="value">The input value to convert to the specified type.</param>
		/// <param name="targetType">The target type to convert the input value to.</param>
		/// <param name="namespaceResolver">An <see cref="T:System.Xml.IXmlNamespaceResolver" /> used for resolving namespace prefixes. This is only of use if the <see cref="T:System.Xml.Schema.XmlSchemaDatatype" />  represents the xs:QName type or a type derived from it.</param>
		/// <exception cref="T:System.ArgumentNullException">The <see cref="T:System.Object" /> or <see cref="T:System.Type" /> parameter is null.</exception>
		/// <exception cref="T:System.InvalidCastException">The type represented by the <see cref="T:System.Xml.Schema.XmlSchemaDatatype" />   does not support a conversion from type of the value specified to the type specified.</exception>
		[MonoTODO]
		public virtual object ChangeType(object value, Type targetType, IXmlNamespaceResolver nsResolver)
		{
			throw new NotImplementedException();
		}

		/// <summary>The <see cref="M:System.Xml.Schema.XmlSchemaDatatype.IsDerivedFrom(System.Xml.Schema.XmlSchemaDatatype)" /> method always returns false.</summary>
		/// <returns>Always returns false.</returns>
		/// <param name="datatype">The <see cref="T:System.Xml.Schema.XmlSchemaDatatype" />.</param>
		public virtual bool IsDerivedFrom(XmlSchemaDatatype datatype)
		{
			return this == datatype;
		}

		/// <summary>When overridden in a derived class, validates the string specified against a built-in or user-defined simple type.</summary>
		/// <returns>An <see cref="T:System.Object" /> that can be cast safely to the type returned by the <see cref="P:System.Xml.Schema.XmlSchemaDatatype.ValueType" /> property.</returns>
		/// <param name="s">The string to validate against the simple type.</param>
		/// <param name="nameTable">The <see cref="T:System.Xml.XmlNameTable" /> to use for atomization while parsing the string if this <see cref="T:System.Xml.Schema.XmlSchemaDatatype" /> object represents the xs:NCName type. </param>
		/// <param name="nsmgr">The <see cref="T:System.Xml.IXmlNamespaceResolver" /> object to use while parsing the string if this <see cref="T:System.Xml.Schema.XmlSchemaDatatype" /> object represents the xs:QName type.</param>
		/// <exception cref="T:System.Xml.Schema.XmlSchemaValidationException">The input value is not a valid instance of this W3C XML Schema type.</exception>
		/// <exception cref="T:System.ArgumentNullException">The value to parse cannot be null.</exception>
		public abstract object ParseValue(string s, XmlNameTable nameTable, IXmlNamespaceResolver nsmgr);

		internal virtual ValueType ParseValueType(string s, XmlNameTable nameTable, IXmlNamespaceResolver nsmgr)
		{
			return null;
		}

		internal string Normalize(string s)
		{
			return Normalize(s, Whitespace);
		}

		internal string Normalize(string s, XsdWhitespaceFacet whitespaceFacet)
		{
			int num = s.IndexOfAny(wsChars);
			if (num < 0)
			{
				return s;
			}
			switch (whitespaceFacet)
			{
			case XsdWhitespaceFacet.Collapse:
			{
				string[] array = s.Trim().Split(wsChars);
				foreach (string text in array)
				{
					if (text != string.Empty)
					{
						sb.Append(text);
						sb.Append(" ");
					}
				}
				string result = sb.ToString();
				sb.Length = 0;
				return result.Trim();
			}
			case XsdWhitespaceFacet.Replace:
			{
				sb.Length = 0;
				sb.Append(s);
				for (int i = 0; i < sb.Length; i++)
				{
					switch (sb[i])
					{
					case '\t':
					case '\n':
					case '\r':
						sb[i] = ' ';
						break;
					}
				}
				string result = sb.ToString();
				sb.Length = 0;
				return result;
			}
			default:
				return s;
			}
		}

		internal static XmlSchemaDatatype FromName(XmlQualifiedName qname)
		{
			return FromName(qname.Name, qname.Namespace);
		}

		internal static XmlSchemaDatatype FromName(string localName, string ns)
		{
			switch (ns)
			{
			case "http://www.w3.org/2003/11/xpath-datatypes":
				switch (localName)
				{
				case "anyAtomicType":
					return datatypeAnyAtomicType;
				case "untypedAtomic":
					return datatypeUntypedAtomic;
				case "dayTimeDuration":
					return datatypeDayTimeDuration;
				case "yearMonthDuration":
					return datatypeYearMonthDuration;
				default:
					return null;
				}
			default:
				return null;
			case "http://www.w3.org/2001/XMLSchema":
				switch (localName)
				{
				case "anySimpleType":
					return datatypeAnySimpleType;
				case "string":
					return datatypeString;
				case "normalizedString":
					return datatypeNormalizedString;
				case "token":
					return datatypeToken;
				case "language":
					return datatypeLanguage;
				case "NMTOKEN":
					return datatypeNMToken;
				case "NMTOKENS":
					return datatypeNMTokens;
				case "Name":
					return datatypeName;
				case "NCName":
					return datatypeNCName;
				case "ID":
					return datatypeID;
				case "IDREF":
					return datatypeIDRef;
				case "IDREFS":
					return datatypeIDRefs;
				case "ENTITY":
					return datatypeEntity;
				case "ENTITIES":
					return datatypeEntities;
				case "NOTATION":
					return datatypeNotation;
				case "decimal":
					return datatypeDecimal;
				case "integer":
					return datatypeInteger;
				case "long":
					return datatypeLong;
				case "int":
					return datatypeInt;
				case "short":
					return datatypeShort;
				case "byte":
					return datatypeByte;
				case "nonPositiveInteger":
					return datatypeNonPositiveInteger;
				case "negativeInteger":
					return datatypeNegativeInteger;
				case "nonNegativeInteger":
					return datatypeNonNegativeInteger;
				case "unsignedLong":
					return datatypeUnsignedLong;
				case "unsignedInt":
					return datatypeUnsignedInt;
				case "unsignedShort":
					return datatypeUnsignedShort;
				case "unsignedByte":
					return datatypeUnsignedByte;
				case "positiveInteger":
					return datatypePositiveInteger;
				case "float":
					return datatypeFloat;
				case "double":
					return datatypeDouble;
				case "base64Binary":
					return datatypeBase64Binary;
				case "boolean":
					return datatypeBoolean;
				case "anyURI":
					return datatypeAnyURI;
				case "duration":
					return datatypeDuration;
				case "dateTime":
					return datatypeDateTime;
				case "date":
					return datatypeDate;
				case "time":
					return datatypeTime;
				case "hexBinary":
					return datatypeHexBinary;
				case "QName":
					return datatypeQName;
				case "gYearMonth":
					return datatypeGYearMonth;
				case "gMonthDay":
					return datatypeGMonthDay;
				case "gYear":
					return datatypeGYear;
				case "gMonth":
					return datatypeGMonth;
				case "gDay":
					return datatypeGDay;
				default:
					return null;
				}
			}
		}
	}
}
