namespace System.Xml.Schema
{
	/// <summary>Represents the callback method that will handle XML schema validation events and the <see cref="T:System.Xml.Schema.ValidationEventArgs" />.</summary>
	/// <param name="sender">The source of the event. </param>
	/// <param name="e">An <see cref="T:System.Xml.Schema.ValidationEventArgs" /> containing the event data. </param>
	public delegate void ValidationEventHandler(object sender, ValidationEventArgs e);
}
