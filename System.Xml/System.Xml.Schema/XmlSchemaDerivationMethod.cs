using System.Xml.Serialization;

namespace System.Xml.Schema
{
	/// <summary>Provides different methods for preventing derivation.</summary>
	[Flags]
	public enum XmlSchemaDerivationMethod
	{
		/// <summary>Override default derivation method to allow any derivation.</summary>
		[XmlEnum("")]
		Empty = 0x0,
		/// <summary>Refers to derivations by Substitution.</summary>
		[XmlEnum("substitution")]
		Substitution = 0x1,
		/// <summary>Refers to derivations by Extension.</summary>
		[XmlEnum("extension")]
		Extension = 0x2,
		/// <summary>Refers to derivations by Restriction.</summary>
		[XmlEnum("restriction")]
		Restriction = 0x4,
		/// <summary>Refers to derivations by List.</summary>
		[XmlEnum("list")]
		List = 0x8,
		/// <summary>Refers to derivations by Union.</summary>
		[XmlEnum("union")]
		Union = 0x10,
		/// <summary>#all. Refers to all derivation methods.</summary>
		[XmlEnum("#all")]
		All = 0xFF,
		/// <summary>Accepts the default derivation method.</summary>
		[XmlIgnore]
		None = 0x100
	}
}
