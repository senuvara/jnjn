using System.Xml.XPath;
using System.Xml.Xsl;

namespace Mono.Xml.Xsl.Operations
{
	internal class XslValueOf : XslCompiledElement
	{
		private XPathExpression select;

		private bool disableOutputEscaping;

		public XslValueOf(Compiler c)
			: base(c)
		{
		}

		protected override void Compile(Compiler c)
		{
			if (c.Debugger != null)
			{
				c.Debugger.DebugCompile(base.DebugInput);
			}
			c.CheckExtraAttributes("value-of", "select", "disable-output-escaping");
			c.AssertAttribute("select");
			select = c.CompileExpression(c.GetAttribute("select"));
			disableOutputEscaping = c.ParseYesNoAttribute("disable-output-escaping", defaultVal: false);
			if (!c.Input.MoveToFirstChild())
			{
				return;
			}
			while (true)
			{
				switch (c.Input.NodeType)
				{
				case XPathNodeType.Element:
					if (c.Input.NamespaceURI == "http://www.w3.org/1999/XSL/Transform")
					{
						break;
					}
					goto IL_00e4;
				case XPathNodeType.Text:
				case XPathNodeType.SignificantWhitespace:
					break;
				default:
					goto IL_00e4;
				}
				break;
				IL_00e4:
				if (!c.Input.MoveToNext())
				{
					return;
				}
			}
			throw new XsltCompileException("XSLT value-of element cannot contain any child.", null, c.Input);
		}

		public override void Evaluate(XslTransformProcessor p)
		{
			if (p.Debugger != null)
			{
				p.Debugger.DebugExecute(p, base.DebugInput);
			}
			if (!disableOutputEscaping)
			{
				p.Out.WriteString(p.EvaluateString(select));
			}
			else
			{
				p.Out.WriteRaw(p.EvaluateString(select));
			}
		}
	}
}
