using System.Collections;

namespace System.Xml.Serialization
{
	/// <summary>Supports mappings between .NET Framework types and XML Schema data types. </summary>
	public abstract class XmlMapping
	{
		private ObjectMap map;

		private ArrayList relatedMaps;

		private SerializationFormat format;

		private SerializationSource source;

		internal string _elementName;

		internal string _namespace;

		private string key;

		/// <summary>Gets the name of the XSD element of the mapping.</summary>
		/// <returns>The XSD element name.</returns>
		[MonoTODO]
		public string XsdElementName => _elementName;

		/// <summary>Get the name of the mapped element.</summary>
		/// <returns>The name of the mapped element.</returns>
		public string ElementName => _elementName;

		/// <summary>Gets the namespace of the mapped element.</summary>
		/// <returns>The namespace of the mapped element.</returns>
		public string Namespace => _namespace;

		internal ObjectMap ObjectMap
		{
			get
			{
				return map;
			}
			set
			{
				map = value;
			}
		}

		internal ArrayList RelatedMaps
		{
			get
			{
				return relatedMaps;
			}
			set
			{
				relatedMaps = value;
			}
		}

		internal SerializationFormat Format
		{
			get
			{
				return format;
			}
			set
			{
				format = value;
			}
		}

		internal SerializationSource Source
		{
			get
			{
				return source;
			}
			set
			{
				source = value;
			}
		}

		internal XmlMapping()
		{
		}

		internal XmlMapping(string elementName, string ns)
		{
			_elementName = elementName;
			_namespace = ns;
		}

		/// <summary>Sets the key used to look up the mapping.</summary>
		/// <param name="key">A <see cref="T:System.String" /> that contains the lookup key.</param>
		public void SetKey(string key)
		{
			this.key = key;
		}

		internal string GetKey()
		{
			return key;
		}
	}
}
