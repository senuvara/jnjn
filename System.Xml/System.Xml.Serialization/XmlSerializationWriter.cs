using System.Collections;
using System.Globalization;
using System.Reflection;
using System.Runtime.Serialization;

namespace System.Xml.Serialization
{
	/// <summary>Abstract class used for controlling serialization by the <see cref="T:System.Xml.Serialization.XmlSerializer" /> class. </summary>
	public abstract class XmlSerializationWriter : XmlSerializationGeneratedCode
	{
		private class WriteCallbackInfo
		{
			public Type Type;

			public string TypeName;

			public string TypeNs;

			public XmlSerializationWriteCallback Callback;
		}

		private const string xmlNamespace = "http://www.w3.org/2000/xmlns/";

		private const string unexpectedTypeError = "The type {0} was not expected. Use the XmlInclude or SoapInclude attribute to specify types that are not known statically.";

		private ObjectIDGenerator idGenerator;

		private int qnameCount;

		private bool topLevelElement;

		private ArrayList namespaces;

		private XmlWriter writer;

		private Queue referencedElements;

		private Hashtable callbacks;

		private Hashtable serializedObjects;

		/// <summary>Gets or sets a list of XML qualified name objects that contain the namespaces and prefixes used to produce qualified names in XML documents. </summary>
		/// <returns>An <see cref="T:System.Collections.ArrayList" /> that contains the namespaces and prefix pairs.</returns>
		protected ArrayList Namespaces
		{
			get
			{
				return namespaces;
			}
			set
			{
				namespaces = value;
			}
		}

		/// <summary>Gets the <see cref="T:System.Xml.XmlWriter" /> that is being used by the <see cref="T:System.Xml.Serialization.XmlSerializationWriter" />. </summary>
		/// <returns>The <see cref="T:System.Xml.XmlWriter" /> used by the class instance.</returns>
		protected XmlWriter Writer
		{
			get
			{
				return writer;
			}
			set
			{
				writer = value;
			}
		}

		/// <summary>Gets or sets a value that indicates whether the <see cref="M:System.Xml.XmlConvert.EncodeName(System.String)" /> method is used to write valid XML.</summary>
		/// <returns>true if the <see cref="M:System.Xml.Serialization.XmlSerializationWriter.FromXmlQualifiedName(System.Xml.XmlQualifiedName)" /> method returns an encoded name; otherwise, false.</returns>
		[MonoTODO]
		protected bool EscapeName
		{
			get
			{
				throw new NotImplementedException();
			}
			set
			{
				throw new NotImplementedException();
			}
		}

		/// <summary>Initializes a new instance of the <see cref="T:System.Xml.Serialization.XmlSerializationWriter" /> class. </summary>
		protected XmlSerializationWriter()
		{
			qnameCount = 0;
			serializedObjects = new Hashtable();
		}

		internal void Initialize(XmlWriter writer, XmlSerializerNamespaces nss)
		{
			this.writer = writer;
			if (nss == null)
			{
				return;
			}
			namespaces = new ArrayList();
			XmlQualifiedName[] array = nss.ToArray();
			foreach (XmlQualifiedName xmlQualifiedName in array)
			{
				if (xmlQualifiedName.Name != string.Empty && xmlQualifiedName.Namespace != string.Empty)
				{
					namespaces.Add(xmlQualifiedName);
				}
			}
		}

		/// <summary>Stores an implementation of the <see cref="T:System.Xml.Serialization.XmlSerializationWriteCallback" /> delegate and the type it applies to, for a later invocation. </summary>
		/// <param name="type">The <see cref="T:System.Type" /> of objects that are serialized.</param>
		/// <param name="typeName">The name of the type of objects that are serialized.</param>
		/// <param name="typeNs">The namespace of the type of objects that are serialized.</param>
		/// <param name="callback">An instance of the <see cref="T:System.Xml.Serialization.XmlSerializationWriteCallback" /> delegate.</param>
		protected void AddWriteCallback(Type type, string typeName, string typeNs, XmlSerializationWriteCallback callback)
		{
			WriteCallbackInfo writeCallbackInfo = new WriteCallbackInfo();
			writeCallbackInfo.Type = type;
			writeCallbackInfo.TypeName = typeName;
			writeCallbackInfo.TypeNs = typeNs;
			writeCallbackInfo.Callback = callback;
			if (callbacks == null)
			{
				callbacks = new Hashtable();
			}
			callbacks.Add(type, writeCallbackInfo);
		}

		/// <summary>Creates an <see cref="T:System.InvalidOperationException" /> that indicates an unexpected name for an element that adheres to an XML Schema choice element declaration.</summary>
		/// <returns>An <see cref="T:System.InvalidOperationException" /> exception.</returns>
		/// <param name="value">The name that is not valid.</param>
		/// <param name="identifier">The choice element declaration that the name belongs to.</param>
		/// <param name="name">The expected local name of an element.</param>
		/// <param name="ns">The expected namespace of an element.</param>
		protected Exception CreateChoiceIdentifierValueException(string value, string identifier, string name, string ns)
		{
			string message = $"Value '{value}' of the choice identifier '{identifier}' does not match element '{name}' from namespace '{ns}'.";
			return new InvalidOperationException(message);
		}

		/// <summary>Creates an <see cref="T:System.InvalidOperationException" /> that indicates a failure while writing an array where an XML Schema choice element declaration is applied.</summary>
		/// <returns>An <see cref="T:System.InvalidOperationException" /> exception.</returns>
		/// <param name="type">The type being serialized.</param>
		/// <param name="identifier">A name for the choice element declaration.</param>
		protected Exception CreateInvalidChoiceIdentifierValueException(string type, string identifier)
		{
			string message = $"Invalid or missing choice identifier '{identifier}' of type '{type}'.";
			return new InvalidOperationException(message);
		}

		/// <summary>Creates an <see cref="T:System.InvalidOperationException" /> that indicates that a value for an XML element does not match an enumeration type.</summary>
		/// <returns>An <see cref="T:System.InvalidOperationException" /> exception.</returns>
		/// <param name="value">The value that is not valid.</param>
		/// <param name="elementName">The name of the XML element with an invalid value.</param>
		/// <param name="enumValue">The valid value.</param>
		protected Exception CreateMismatchChoiceException(string value, string elementName, string enumValue)
		{
			string message = $"Value of {elementName} mismatches the type of {value}, you need to set it to {enumValue}.";
			return new InvalidOperationException(message);
		}

		/// <summary>Creates an <see cref="T:System.InvalidOperationException" /> that indicates that an XML element that should adhere to the XML Schema any element declaration cannot be processed.</summary>
		/// <returns>An <see cref="T:System.InvalidOperationException" /> exception.</returns>
		/// <param name="name">The XML element that cannot be processed.</param>
		/// <param name="ns">The namespace of the XML element.</param>
		protected Exception CreateUnknownAnyElementException(string name, string ns)
		{
			string message = $"The XML element named '{name}' from namespace '{ns}' was not expected. The XML element name and namespace must match those provided via XmlAnyElementAttribute(s).";
			return new InvalidOperationException(message);
		}

		/// <summary>Creates an <see cref="T:System.InvalidOperationException" /> that indicates that a type being serialized is not being used in a valid manner or is unexpectedly encountered. </summary>
		/// <returns>An <see cref="T:System.InvalidOperationException" /> exception.</returns>
		/// <param name="o">The object whose type cannot be serialized.</param>
		protected Exception CreateUnknownTypeException(object o)
		{
			return CreateUnknownTypeException(o.GetType());
		}

		/// <summary>Creates an <see cref="T:System.InvalidOperationException" /> that indicates that a type being serialized is not being used in a valid manner or is unexpectedly encountered. </summary>
		/// <returns>An <see cref="T:System.InvalidOperationException" /> exception.</returns>
		/// <param name="type">The type that cannot be serialized.</param>
		protected Exception CreateUnknownTypeException(Type type)
		{
			string message = $"The type {type} may not be used in this context.";
			return new InvalidOperationException(message);
		}

		/// <summary>Processes a base-64 byte array.</summary>
		/// <returns>The same byte array that was passed in as an argument.</returns>
		/// <param name="value">A base-64 <see cref="T:System.Byte" /> array.</param>
		protected static byte[] FromByteArrayBase64(byte[] value)
		{
			return value;
		}

		/// <summary>Produces a string from an input hexadecimal byte array.</summary>
		/// <returns>The byte array value converted to a string.</returns>
		/// <param name="value">A hexadecimal byte array to translate to a string.</param>
		protected static string FromByteArrayHex(byte[] value)
		{
			return XmlCustomFormatter.FromByteArrayHex(value);
		}

		/// <summary>Produces a string from an input <see cref="T:System.Char" />.</summary>
		/// <returns>The <see cref="T:System.Char" /> value converted to a string.</returns>
		/// <param name="value">A <see cref="T:System.Char" /> to translate to a string.</param>
		protected static string FromChar(char value)
		{
			return XmlCustomFormatter.FromChar(value);
		}

		/// <summary>Produces a string from a <see cref="T:System.DateTime" /> object.</summary>
		/// <returns>A string representation of the <see cref="T:System.DateTime" /> that shows the date but no time.</returns>
		/// <param name="value">A <see cref="T:System.DateTime" /> to translate to a string.</param>
		protected static string FromDate(DateTime value)
		{
			return XmlCustomFormatter.FromDate(value);
		}

		/// <summary>Produces a string from an input <see cref="T:System.DateTime" />.</summary>
		/// <returns>A string representation of the <see cref="T:System.DateTime" /> that shows the date and time.</returns>
		/// <param name="value">A <see cref="T:System.DateTime" /> to translate to a string.</param>
		protected static string FromDateTime(DateTime value)
		{
			return XmlCustomFormatter.FromDateTime(value);
		}

		/// <summary>Produces a string that consists of delimited identifiers that represent the enumeration members that have been set.</summary>
		/// <returns>A string that consists of delimited identifiers, where each represents a member from the set enumerator list.</returns>
		/// <param name="value">The enumeration value as a series of bitwise OR operations.</param>
		/// <param name="values">The enumeration's name values.</param>
		/// <param name="ids">The enumeration's constant values.</param>
		protected static string FromEnum(long value, string[] values, long[] ids)
		{
			return XmlCustomFormatter.FromEnum(value, values, ids);
		}

		/// <summary>Produces a string from a <see cref="T:System.DateTime" /> object.</summary>
		/// <returns>A string representation of the <see cref="T:System.DateTime" /> object that shows the time but no date.</returns>
		/// <param name="value">A <see cref="T:System.DateTime" /> that is translated to a string.</param>
		protected static string FromTime(DateTime value)
		{
			return XmlCustomFormatter.FromTime(value);
		}

		/// <summary>Encodes a valid XML name by replacing characters that are not valid with escape sequences.</summary>
		/// <returns>An encoded string.</returns>
		/// <param name="name">A string to be used as an XML name.</param>
		protected static string FromXmlName(string name)
		{
			return XmlCustomFormatter.FromXmlName(name);
		}

		/// <summary>Encodes a valid XML local name by replacing characters that are not valid with escape sequences.</summary>
		/// <returns>An encoded string.</returns>
		/// <param name="ncName">A string to be used as a local (unqualified) XML name.</param>
		protected static string FromXmlNCName(string ncName)
		{
			return XmlCustomFormatter.FromXmlNCName(ncName);
		}

		/// <summary>Encodes an XML name.</summary>
		/// <returns>An encoded string.</returns>
		/// <param name="nmToken">An XML name to be encoded.</param>
		protected static string FromXmlNmToken(string nmToken)
		{
			return XmlCustomFormatter.FromXmlNmToken(nmToken);
		}

		/// <summary>Encodes a space-delimited sequence of XML names into a single XML name.</summary>
		/// <returns>An encoded string.</returns>
		/// <param name="nmTokens">A space-delimited sequence of XML names to be encoded.</param>
		protected static string FromXmlNmTokens(string nmTokens)
		{
			return XmlCustomFormatter.FromXmlNmTokens(nmTokens);
		}

		/// <summary>Returns an XML qualified name, with invalid characters replaced by escape sequences. </summary>
		/// <returns>An XML qualified name, with invalid characters replaced by escape sequences.</returns>
		/// <param name="xmlQualifiedName">An <see cref="T:System.Xml.XmlQualifiedName" /> that represents the XML to be written.</param>
		protected string FromXmlQualifiedName(XmlQualifiedName xmlQualifiedName)
		{
			if (xmlQualifiedName == null || xmlQualifiedName == XmlQualifiedName.Empty)
			{
				return null;
			}
			return GetQualifiedName(xmlQualifiedName.Name, xmlQualifiedName.Namespace);
		}

		private string GetId(object o, bool addToReferencesList)
		{
			if (idGenerator == null)
			{
				idGenerator = new ObjectIDGenerator();
			}
			bool firstTime;
			long id = idGenerator.GetId(o, out firstTime);
			return string.Format(CultureInfo.InvariantCulture, "id{0}", id);
		}

		private bool AlreadyQueued(object ob)
		{
			if (idGenerator == null)
			{
				return false;
			}
			idGenerator.HasId(ob, out bool firstTime);
			return !firstTime;
		}

		private string GetNamespacePrefix(string ns)
		{
			string text = Writer.LookupPrefix(ns);
			if (text == null)
			{
				text = string.Format(CultureInfo.InvariantCulture, "q{0}", ++qnameCount);
				WriteAttribute("xmlns", text, null, ns);
			}
			return text;
		}

		private string GetQualifiedName(string name, string ns)
		{
			if (ns == string.Empty)
			{
				return name;
			}
			string namespacePrefix = GetNamespacePrefix(ns);
			if (namespacePrefix == string.Empty)
			{
				return name;
			}
			return $"{namespacePrefix}:{name}";
		}

		/// <summary>Initializes instances of the <see cref="T:System.Xml.Serialization.XmlSerializationWriteCallback" /> delegate to serialize SOAP-encoded XML data. </summary>
		protected abstract void InitCallbacks();

		/// <summary>Initializes object references only while serializing a SOAP-encoded SOAP message.</summary>
		protected void TopLevelElement()
		{
			topLevelElement = true;
		}

		/// <summary>Instructs an <see cref="T:System.Xml.XmlWriter" /> object to write an XML attribute that has no namespace specified for its name.</summary>
		/// <param name="localName">The local name of the XML attribute.</param>
		/// <param name="value">The value of the XML attribute as a byte array.</param>
		protected void WriteAttribute(string localName, byte[] value)
		{
			WriteAttribute(localName, string.Empty, value);
		}

		/// <summary>Instructs the <see cref="T:System.Xml.XmlWriter" /> to write an XML attribute that has no namespace specified for its name. </summary>
		/// <param name="localName">The local name of the XML attribute.</param>
		/// <param name="value">The value of the XML attribute as a string.</param>
		protected void WriteAttribute(string localName, string value)
		{
			WriteAttribute(string.Empty, localName, string.Empty, value);
		}

		/// <summary>Instructs an <see cref="T:System.Xml.XmlWriter" /> object to write an XML attribute.</summary>
		/// <param name="localName">The local name of the XML attribute.</param>
		/// <param name="ns">The namespace of the XML attribute.</param>
		/// <param name="value">The value of the XML attribute as a byte array.</param>
		protected void WriteAttribute(string localName, string ns, byte[] value)
		{
			if (value != null)
			{
				Writer.WriteStartAttribute(localName, ns);
				WriteValue(value);
				Writer.WriteEndAttribute();
			}
		}

		/// <summary>Writes an XML attribute. </summary>
		/// <param name="localName">The local name of the XML attribute.</param>
		/// <param name="ns">The namespace of the XML attribute.</param>
		/// <param name="value">The value of the XML attribute as a string.</param>
		protected void WriteAttribute(string localName, string ns, string value)
		{
			WriteAttribute(null, localName, ns, value);
		}

		/// <summary>Writes an XML attribute where the namespace prefix is provided manually. </summary>
		/// <param name="prefix">The namespace prefix to write.</param>
		/// <param name="localName">The local name of the XML attribute.</param>
		/// <param name="ns">The namespace represented by the prefix.</param>
		/// <param name="value">The value of the XML attribute as a string.</param>
		protected void WriteAttribute(string prefix, string localName, string ns, string value)
		{
			if (value != null)
			{
				Writer.WriteAttributeString(prefix, localName, ns, value);
			}
		}

		private void WriteXmlNode(XmlNode node)
		{
			if (node is XmlDocument)
			{
				node = ((XmlDocument)node).DocumentElement;
			}
			node.WriteTo(Writer);
		}

		/// <summary>Writes an XML node object within the body of a named XML element.</summary>
		/// <param name="node">The XML node to write, possibly a child XML element.</param>
		/// <param name="name">The local name of the parent XML element to write.</param>
		/// <param name="ns">The namespace of the parent XML element to write.</param>
		/// <param name="isNullable">true to write an xsi:nil='true' attribute if the object to serialize is null; otherwise, false.</param>
		/// <param name="any">true to indicate that the node, if an XML element, adheres to an XML Schema any element declaration; otherwise, false.</param>
		protected void WriteElementEncoded(XmlNode node, string name, string ns, bool isNullable, bool any)
		{
			if (name != string.Empty)
			{
				if (node == null)
				{
					if (isNullable)
					{
						WriteNullTagEncoded(name, ns);
					}
				}
				else
				{
					Writer.WriteStartElement(name, ns);
					WriteXmlNode(node);
					Writer.WriteEndElement();
				}
			}
			else
			{
				WriteXmlNode(node);
			}
		}

		/// <summary>Instructs an <see cref="T:System.Xml.XmlWriter" /> object to write an <see cref="T:System.Xml.XmlNode" /> object within the body of a named XML element.</summary>
		/// <param name="node">The XML node to write, possibly a child XML element.</param>
		/// <param name="name">The local name of the parent XML element to write.</param>
		/// <param name="ns">The namespace of the parent XML element to write.</param>
		/// <param name="isNullable">true to write an xsi:nil='true' attribute if the object to serialize is null; otherwise, false.</param>
		/// <param name="any">true to indicate that the node, if an XML element, adheres to an XML Schema any element declaration; otherwise, false.</param>
		protected void WriteElementLiteral(XmlNode node, string name, string ns, bool isNullable, bool any)
		{
			if (name != string.Empty)
			{
				if (node == null)
				{
					if (isNullable)
					{
						WriteNullTagLiteral(name, ns);
					}
				}
				else
				{
					Writer.WriteStartElement(name, ns);
					WriteXmlNode(node);
					Writer.WriteEndElement();
				}
			}
			else
			{
				WriteXmlNode(node);
			}
		}

		/// <summary>Writes an XML element with a specified qualified name in its body.</summary>
		/// <param name="localName">The local name of the XML element.</param>
		/// <param name="value">The name to write, using its prefix if namespace-qualified, in the element text.</param>
		protected void WriteElementQualifiedName(string localName, XmlQualifiedName value)
		{
			WriteElementQualifiedName(localName, string.Empty, value, null);
		}

		/// <summary>Writes an XML element with a specified qualified name in its body.</summary>
		/// <param name="localName">The local name of the XML element.</param>
		/// <param name="ns">The namespace of the XML element.</param>
		/// <param name="value">The name to write, using its prefix if namespace-qualified, in the element text.</param>
		protected void WriteElementQualifiedName(string localName, string ns, XmlQualifiedName value)
		{
			WriteElementQualifiedName(localName, ns, value, null);
		}

		/// <summary>Writes an XML element with a specified qualified name in its body.</summary>
		/// <param name="localName">The local name of the XML element.</param>
		/// <param name="value">The name to write, using its prefix if namespace-qualified, in the element text.</param>
		/// <param name="xsiType">The name of the XML Schema data type to be written to the xsi:type attribute.</param>
		protected void WriteElementQualifiedName(string localName, XmlQualifiedName value, XmlQualifiedName xsiType)
		{
			WriteElementQualifiedName(localName, string.Empty, value, xsiType);
		}

		/// <summary>Writes an XML element with a specified qualified name in its body.</summary>
		/// <param name="localName">The local name of the XML element.</param>
		/// <param name="ns">The namespace of the XML element.</param>
		/// <param name="value">The name to write, using its prefix if namespace-qualified, in the element text.</param>
		/// <param name="xsiType">The name of the XML Schema data type to be written to the xsi:type attribute.</param>
		protected void WriteElementQualifiedName(string localName, string ns, XmlQualifiedName value, XmlQualifiedName xsiType)
		{
			localName = XmlCustomFormatter.FromXmlNCName(localName);
			WriteStartElement(localName, ns);
			if (xsiType != null)
			{
				WriteXsiType(xsiType.Name, xsiType.Namespace);
			}
			Writer.WriteString(FromXmlQualifiedName(value));
			WriteEndElement();
		}

		/// <summary>Writes an XML element with a specified value in its body. </summary>
		/// <param name="localName">The local name of the XML element to be written without namespace qualification.</param>
		/// <param name="value">The text value of the XML element.</param>
		protected void WriteElementString(string localName, string value)
		{
			WriteElementString(localName, string.Empty, value, null);
		}

		/// <summary>Writes an XML element with a specified value in its body. </summary>
		/// <param name="localName">The local name of the XML element.</param>
		/// <param name="ns">The namespace of the XML element.</param>
		/// <param name="value">The text value of the XML element.</param>
		protected void WriteElementString(string localName, string ns, string value)
		{
			WriteElementString(localName, ns, value, null);
		}

		/// <summary>Writes an XML element with a specified value in its body.</summary>
		/// <param name="localName">The local name of the XML element.</param>
		/// <param name="value">The text value of the XML element.</param>
		/// <param name="xsiType">The name of the XML Schema data type to be written to the xsi:type attribute.</param>
		protected void WriteElementString(string localName, string value, XmlQualifiedName xsiType)
		{
			WriteElementString(localName, string.Empty, value, xsiType);
		}

		/// <summary>Writes an XML element with a specified value in its body.</summary>
		/// <param name="localName">The local name of the XML element.</param>
		/// <param name="ns">The namespace of the XML element.</param>
		/// <param name="value">The text value of the XML element.</param>
		/// <param name="xsiType">The name of the XML Schema data type to be written to the xsi:type attribute.</param>
		protected void WriteElementString(string localName, string ns, string value, XmlQualifiedName xsiType)
		{
			if (value != null)
			{
				if (xsiType != null)
				{
					localName = XmlCustomFormatter.FromXmlNCName(localName);
					WriteStartElement(localName, ns);
					WriteXsiType(xsiType.Name, xsiType.Namespace);
					Writer.WriteString(value);
					WriteEndElement();
				}
				else
				{
					Writer.WriteElementString(localName, ns, value);
				}
			}
		}

		/// <summary>Writes an XML element with a specified value in its body.</summary>
		/// <param name="localName">The local name of the XML element.</param>
		/// <param name="value">The text value of the XML element.</param>
		protected void WriteElementStringRaw(string localName, byte[] value)
		{
			WriteElementStringRaw(localName, string.Empty, value, null);
		}

		/// <summary>Writes an XML element with a specified value in its body.</summary>
		/// <param name="localName">The local name of the XML element.</param>
		/// <param name="value">The text value of the XML element.</param>
		protected void WriteElementStringRaw(string localName, string value)
		{
			WriteElementStringRaw(localName, string.Empty, value, null);
		}

		/// <summary>Writes an XML element with a specified value in its body.</summary>
		/// <param name="localName">The local name of the XML element.</param>
		/// <param name="value">The text value of the XML element.</param>
		/// <param name="xsiType">The name of the XML Schema data type to be written to the xsi:type attribute.</param>
		protected void WriteElementStringRaw(string localName, byte[] value, XmlQualifiedName xsiType)
		{
			WriteElementStringRaw(localName, string.Empty, value, xsiType);
		}

		/// <summary>Writes an XML element with a specified value in its body.</summary>
		/// <param name="localName">The local name of the XML element.</param>
		/// <param name="ns">The namespace of the XML element.</param>
		/// <param name="value">The text value of the XML element.</param>
		protected void WriteElementStringRaw(string localName, string ns, byte[] value)
		{
			WriteElementStringRaw(localName, ns, value, null);
		}

		/// <summary>Writes an XML element with a specified value in its body.</summary>
		/// <param name="localName">The local name of the XML element.</param>
		/// <param name="ns">The namespace of the XML element.</param>
		/// <param name="value">The text value of the XML element.</param>
		protected void WriteElementStringRaw(string localName, string ns, string value)
		{
			WriteElementStringRaw(localName, ns, value, null);
		}

		/// <summary>Writes an XML element with a specified value in its body.</summary>
		/// <param name="localName">The local name of the XML element.</param>
		/// <param name="value">The text value of the XML element.</param>
		/// <param name="xsiType">The name of the XML Schema data type to be written to the xsi:type attribute.</param>
		protected void WriteElementStringRaw(string localName, string value, XmlQualifiedName xsiType)
		{
			WriteElementStringRaw(localName, string.Empty, value, null);
		}

		/// <summary>Writes an XML element with a specified value in its body.</summary>
		/// <param name="localName">The local name of the XML element.</param>
		/// <param name="ns">The namespace of the XML element.</param>
		/// <param name="value">The text value of the XML element.</param>
		/// <param name="xsiType">The name of the XML Schema data type to be written to the xsi:type attribute.</param>
		protected void WriteElementStringRaw(string localName, string ns, byte[] value, XmlQualifiedName xsiType)
		{
			if (value != null)
			{
				WriteStartElement(localName, ns);
				if (xsiType != null)
				{
					WriteXsiType(xsiType.Name, xsiType.Namespace);
				}
				if (value.Length > 0)
				{
					Writer.WriteBase64(value, 0, value.Length);
				}
				WriteEndElement();
			}
		}

		/// <summary>Writes an XML element with a specified value in its body.</summary>
		/// <param name="localName">The local name of the XML element.</param>
		/// <param name="ns">The namespace of the XML element.</param>
		/// <param name="value">The text value of the XML element.</param>
		/// <param name="xsiType">The name of the XML Schema data type to be written to the xsi:type attribute.</param>
		protected void WriteElementStringRaw(string localName, string ns, string value, XmlQualifiedName xsiType)
		{
			localName = XmlCustomFormatter.FromXmlNCName(localName);
			WriteStartElement(localName, ns);
			if (xsiType != null)
			{
				WriteXsiType(xsiType.Name, xsiType.Namespace);
			}
			Writer.WriteRaw(value);
			WriteEndElement();
		}

		/// <summary>Writes an XML element whose body is empty. </summary>
		/// <param name="name">The local name of the XML element to write.</param>
		protected void WriteEmptyTag(string name)
		{
			WriteEmptyTag(name, string.Empty);
		}

		/// <summary>Writes an XML element whose body is empty.</summary>
		/// <param name="name">The local name of the XML element to write.</param>
		/// <param name="ns">The namespace of the XML element to write.</param>
		protected void WriteEmptyTag(string name, string ns)
		{
			name = XmlCustomFormatter.FromXmlName(name);
			WriteStartElement(name, ns);
			WriteEndElement();
		}

		/// <summary>Writes a &lt;closing&gt; element tag.</summary>
		protected void WriteEndElement()
		{
			WriteEndElement(null);
		}

		/// <summary>Writes a closing element tag.</summary>
		/// <param name="o">The object being serialized.</param>
		protected void WriteEndElement(object o)
		{
			if (o != null)
			{
				serializedObjects.Remove(o);
			}
			Writer.WriteEndElement();
		}

		/// <summary>Writes an id attribute that appears in a SOAP-encoded multiRef element. </summary>
		/// <param name="o">The object being serialized.</param>
		protected void WriteId(object o)
		{
			WriteAttribute("id", GetId(o, addToReferencesList: true));
		}

		/// <summary>Writes namespace declaration attributes.</summary>
		/// <param name="xmlns">The XML namespaces to declare.</param>
		protected void WriteNamespaceDeclarations(XmlSerializerNamespaces ns)
		{
			if (ns != null)
			{
				ICollection values = ns.Namespaces.Values;
				foreach (XmlQualifiedName item in values)
				{
					if (item.Namespace != string.Empty && Writer.LookupPrefix(item.Namespace) != item.Name)
					{
						WriteAttribute("xmlns", item.Name, "http://www.w3.org/2000/xmlns/", item.Namespace);
					}
				}
			}
		}

		/// <summary>Writes an XML element whose body contains a valid XML qualified name. <see cref="T:System.Xml.XmlWriter" /> inserts an xsi:nil='true' attribute if the string's value is null.</summary>
		/// <param name="name">The local name of the XML element to write.</param>
		/// <param name="ns">The namespace of the XML element to write.</param>
		/// <param name="value">The XML qualified name to write in the body of the XML element.</param>
		/// <param name="xsiType">The name of the XML Schema data type to be written to the xsi:type attribute.</param>
		protected void WriteNullableQualifiedNameEncoded(string name, string ns, XmlQualifiedName value, XmlQualifiedName xsiType)
		{
			if (value != null)
			{
				WriteElementQualifiedName(name, ns, value, xsiType);
			}
			else
			{
				WriteNullTagEncoded(name, ns);
			}
		}

		/// <summary>Writes an XML element whose body contains a valid XML qualified name. <see cref="T:System.Xml.XmlWriter" /> inserts an xsi:nil='true' attribute if the string's value is null.</summary>
		/// <param name="name">The local name of the XML element to write.</param>
		/// <param name="ns">The namespace of the XML element to write.</param>
		/// <param name="value">The XML qualified name to write in the body of the XML element.</param>
		protected void WriteNullableQualifiedNameLiteral(string name, string ns, XmlQualifiedName value)
		{
			if (value != null)
			{
				WriteElementQualifiedName(name, ns, value);
			}
			else
			{
				WriteNullTagLiteral(name, ns);
			}
		}

		/// <summary>Writes an XML element that contains a string as the body. <see cref="T:System.Xml.XmlWriter" /> inserts an xsi:nil='true' attribute if the string's value is null.</summary>
		/// <param name="name">The local name of the XML element to write.</param>
		/// <param name="ns">The namespace of the XML element to write.</param>
		/// <param name="value">The string to write in the body of the XML element.</param>
		/// <param name="xsiType">The name of the XML Schema data type to be written to the xsi:type attribute.</param>
		protected void WriteNullableStringEncoded(string name, string ns, string value, XmlQualifiedName xsiType)
		{
			if (value != null)
			{
				WriteElementString(name, ns, value, xsiType);
			}
			else
			{
				WriteNullTagEncoded(name, ns);
			}
		}

		/// <summary>Writes a byte array as the body of an XML element. <see cref="T:System.Xml.XmlWriter" /> inserts an xsi:nil='true' attribute if the string's value is null.</summary>
		/// <param name="name">The local name of the XML element to write.</param>
		/// <param name="ns">The namespace of the XML element to write.</param>
		/// <param name="value">The byte array to write in the body of the XML element.</param>
		/// <param name="xsiType">The name of the XML Schema data type to be written to the xsi:type attribute.</param>
		protected void WriteNullableStringEncodedRaw(string name, string ns, byte[] value, XmlQualifiedName xsiType)
		{
			if (value == null)
			{
				WriteNullTagEncoded(name, ns);
			}
			else
			{
				WriteElementStringRaw(name, ns, value, xsiType);
			}
		}

		/// <summary>Writes an XML element that contains a string as the body. <see cref="T:System.Xml.XmlWriter" /> inserts an xsi:nil='true' attribute if the string's value is null.</summary>
		/// <param name="name">The local name of the XML element to write.</param>
		/// <param name="ns">The namespace of the XML element to write.</param>
		/// <param name="value">The string to write in the body of the XML element.</param>
		/// <param name="xsiType">The name of the XML Schema data type to be written to the xsi:type attribute.</param>
		protected void WriteNullableStringEncodedRaw(string name, string ns, string value, XmlQualifiedName xsiType)
		{
			if (value == null)
			{
				WriteNullTagEncoded(name, ns);
			}
			else
			{
				WriteElementStringRaw(name, ns, value, xsiType);
			}
		}

		/// <summary>Writes an XML element that contains a string as the body. <see cref="T:System.Xml.XmlWriter" /> inserts an xsi:nil='true' attribute if the string's value is null.</summary>
		/// <param name="name">The local name of the XML element to write.</param>
		/// <param name="ns">The namespace of the XML element to write.</param>
		/// <param name="value">The string to write in the body of the XML element.</param>
		protected void WriteNullableStringLiteral(string name, string ns, string value)
		{
			if (value != null)
			{
				WriteElementString(name, ns, value, null);
			}
			else
			{
				WriteNullTagLiteral(name, ns);
			}
		}

		/// <summary>Writes a byte array as the body of an XML element. <see cref="T:System.Xml.XmlWriter" /> inserts an xsi:nil='true' attribute if the string's value is null.</summary>
		/// <param name="name">The local name of the XML element to write.</param>
		/// <param name="ns">The namespace of the XML element to write.</param>
		/// <param name="value">The byte array to write in the body of the XML element.</param>
		protected void WriteNullableStringLiteralRaw(string name, string ns, byte[] value)
		{
			if (value == null)
			{
				WriteNullTagLiteral(name, ns);
			}
			else
			{
				WriteElementStringRaw(name, ns, value);
			}
		}

		/// <summary>Writes an XML element that contains a string as the body. <see cref="T:System.Xml.XmlWriter" /> inserts a xsi:nil='true' attribute if the string's value is null.</summary>
		/// <param name="name">The local name of the XML element to write.</param>
		/// <param name="ns">The namespace of the XML element to write.</param>
		/// <param name="value">The string to write in the body of the XML element.</param>
		protected void WriteNullableStringLiteralRaw(string name, string ns, string value)
		{
			if (value == null)
			{
				WriteNullTagLiteral(name, ns);
			}
			else
			{
				WriteElementStringRaw(name, ns, value);
			}
		}

		/// <summary>Writes an XML element with an xsi:nil='true' attribute.</summary>
		/// <param name="name">The local name of the XML element to write.</param>
		protected void WriteNullTagEncoded(string name)
		{
			WriteNullTagEncoded(name, string.Empty);
		}

		/// <summary>Writes an XML element with an xsi:nil='true' attribute.</summary>
		/// <param name="name">The local name of the XML element to write.</param>
		/// <param name="ns">The namespace of the XML element to write.</param>
		protected void WriteNullTagEncoded(string name, string ns)
		{
			Writer.WriteStartElement(name, ns);
			Writer.WriteAttributeString("nil", "http://www.w3.org/2001/XMLSchema-instance", "true");
			Writer.WriteEndElement();
		}

		/// <summary>Writes an XML element with an xsi:nil='true' attribute.</summary>
		/// <param name="name">The local name of the XML element to write.</param>
		protected void WriteNullTagLiteral(string name)
		{
			WriteNullTagLiteral(name, string.Empty);
		}

		/// <summary>Writes an XML element with an xsi:nil='true' attribute. </summary>
		/// <param name="name">The local name of the XML element to write.</param>
		/// <param name="ns">The namespace of the XML element to write.</param>
		protected void WriteNullTagLiteral(string name, string ns)
		{
			WriteStartElement(name, ns);
			Writer.WriteAttributeString("nil", "http://www.w3.org/2001/XMLSchema-instance", "true");
			WriteEndElement();
		}

		/// <summary>Writes a SOAP message XML element that can contain a reference to a &lt;multiRef&gt; XML element for a given object. </summary>
		/// <param name="n">The local name of the XML element to write.</param>
		/// <param name="ns">The namespace of the XML element to write.</param>
		/// <param name="o">The object being serialized either in the current XML element or a multiRef element that is referenced by the current element.</param>
		protected void WritePotentiallyReferencingElement(string n, string ns, object o)
		{
			WritePotentiallyReferencingElement(n, ns, o, null, suppressReference: false, isNullable: false);
		}

		/// <summary>Writes a SOAP message XML element that can contain a reference to a &lt;multiRef&gt; XML element for a given object. </summary>
		/// <param name="n">The local name of the XML element to write.</param>
		/// <param name="ns">The namespace of the XML element to write.</param>
		/// <param name="o">The object being serialized either in the current XML element or a multiRef element that referenced by the current element.</param>
		/// <param name="ambientType">The type stored in the object's type mapping (as opposed to the object's type found directly through the typeof operation).</param>
		protected void WritePotentiallyReferencingElement(string n, string ns, object o, Type ambientType)
		{
			WritePotentiallyReferencingElement(n, ns, o, ambientType, suppressReference: false, isNullable: false);
		}

		/// <summary>Writes a SOAP message XML element that can contain a reference to a &lt;multiRef&gt; XML element for a given object.</summary>
		/// <param name="n">The local name of the XML element to write.</param>
		/// <param name="ns">The namespace of the XML element to write.</param>
		/// <param name="o">The object being serialized either in the current XML element or a multiRef element that is referenced by the current element.</param>
		/// <param name="ambientType">The type stored in the object's type mapping (as opposed to the object's type found directly through the typeof operation).</param>
		/// <param name="suppressReference">true to serialize the object directly into the XML element rather than make the element reference another element that contains the data; otherwise, false.</param>
		protected void WritePotentiallyReferencingElement(string n, string ns, object o, Type ambientType, bool suppressReference)
		{
			WritePotentiallyReferencingElement(n, ns, o, ambientType, suppressReference, isNullable: false);
		}

		/// <summary>Writes a SOAP message XML element that can contain a reference to a multiRef XML element for a given object.</summary>
		/// <param name="n">The local name of the XML element to write.</param>
		/// <param name="ns">The namespace of the XML element to write.</param>
		/// <param name="o">The object being serialized either in the current XML element or a multiRef element that referenced by the current element.</param>
		/// <param name="ambientType">The type stored in the object's type mapping (as opposed to the object's type found directly through the typeof operation).</param>
		/// <param name="suppressReference">true to serialize the object directly into the XML element rather than make the element reference another element that contains the data; otherwise, false.</param>
		/// <param name="isNullable">true to write an xsi:nil='true' attribute if the object to serialize is null; otherwise, false.</param>
		protected void WritePotentiallyReferencingElement(string n, string ns, object o, Type ambientType, bool suppressReference, bool isNullable)
		{
			if (o == null)
			{
				if (isNullable)
				{
					WriteNullTagEncoded(n, ns);
				}
				return;
			}
			WriteStartElement(n, ns, writePrefixed: true);
			CheckReferenceQueue();
			if (callbacks != null && callbacks.ContainsKey(o.GetType()))
			{
				WriteCallbackInfo writeCallbackInfo = (WriteCallbackInfo)callbacks[o.GetType()];
				if (o.GetType().IsEnum)
				{
					writeCallbackInfo.Callback(o);
				}
				else if (suppressReference)
				{
					Writer.WriteAttributeString("id", GetId(o, addToReferencesList: false));
					if (ambientType != o.GetType())
					{
						WriteXsiType(writeCallbackInfo.TypeName, writeCallbackInfo.TypeNs);
					}
					writeCallbackInfo.Callback(o);
				}
				else
				{
					if (!AlreadyQueued(o))
					{
						referencedElements.Enqueue(o);
					}
					Writer.WriteAttributeString("href", "#" + GetId(o, addToReferencesList: true));
				}
			}
			else
			{
				TypeData typeData = TypeTranslator.GetTypeData(o.GetType());
				if (typeData.SchemaType == SchemaTypes.Primitive)
				{
					WriteXsiType(typeData.XmlType, "http://www.w3.org/2001/XMLSchema");
					Writer.WriteString(XmlCustomFormatter.ToXmlString(typeData, o));
				}
				else
				{
					if (!IsPrimitiveArray(typeData))
					{
						throw new InvalidOperationException("Invalid type: " + o.GetType().FullName);
					}
					if (!AlreadyQueued(o))
					{
						referencedElements.Enqueue(o);
					}
					Writer.WriteAttributeString("href", "#" + GetId(o, addToReferencesList: true));
				}
			}
			WriteEndElement();
		}

		/// <summary>Serializes objects into SOAP-encoded multiRef XML elements in a SOAP message. </summary>
		protected void WriteReferencedElements()
		{
			if (referencedElements == null || callbacks == null)
			{
				return;
			}
			while (referencedElements.Count > 0)
			{
				object obj = referencedElements.Dequeue();
				TypeData typeData = TypeTranslator.GetTypeData(obj.GetType());
				WriteCallbackInfo writeCallbackInfo = (WriteCallbackInfo)callbacks[obj.GetType()];
				if (writeCallbackInfo != null)
				{
					WriteStartElement(writeCallbackInfo.TypeName, writeCallbackInfo.TypeNs, writePrefixed: true);
					Writer.WriteAttributeString("id", GetId(obj, addToReferencesList: false));
					if (typeData.SchemaType != SchemaTypes.Array)
					{
						WriteXsiType(writeCallbackInfo.TypeName, writeCallbackInfo.TypeNs);
					}
					writeCallbackInfo.Callback(obj);
					WriteEndElement();
				}
				else if (IsPrimitiveArray(typeData))
				{
					WriteArray(obj, typeData);
				}
			}
		}

		private bool IsPrimitiveArray(TypeData td)
		{
			if (td.SchemaType == SchemaTypes.Array)
			{
				if (td.ListItemTypeData.SchemaType == SchemaTypes.Primitive || td.ListItemType == typeof(object))
				{
					return true;
				}
				return IsPrimitiveArray(td.ListItemTypeData);
			}
			return false;
		}

		private void WriteArray(object o, TypeData td)
		{
			TypeData typeData = td;
			int num = -1;
			string text;
			do
			{
				typeData = typeData.ListItemTypeData;
				text = typeData.XmlType;
				num++;
			}
			while (typeData.SchemaType == SchemaTypes.Array);
			while (num-- > 0)
			{
				text += "[]";
			}
			WriteStartElement("Array", "http://schemas.xmlsoap.org/soap/encoding/", writePrefixed: true);
			Writer.WriteAttributeString("id", GetId(o, addToReferencesList: false));
			if (td.SchemaType == SchemaTypes.Array)
			{
				Array array = (Array)o;
				int length = array.Length;
				Writer.WriteAttributeString("arrayType", "http://schemas.xmlsoap.org/soap/encoding/", GetQualifiedName(text, "http://www.w3.org/2001/XMLSchema") + "[" + length + "]");
				for (int i = 0; i < length; i++)
				{
					WritePotentiallyReferencingElement("Item", string.Empty, array.GetValue(i), td.ListItemType, suppressReference: false, isNullable: true);
				}
			}
			WriteEndElement();
		}

		/// <summary>Writes a SOAP message XML element that contains a reference to a multiRef element for a given object. </summary>
		/// <param name="n">The local name of the referencing element being written.</param>
		/// <param name="ns">The namespace of the referencing element being written.</param>
		/// <param name="o">The object being serialized.</param>
		protected void WriteReferencingElement(string n, string ns, object o)
		{
			WriteReferencingElement(n, ns, o, isNullable: false);
		}

		/// <summary>Writes a SOAP message XML element that contains a reference to a multiRef element for a given object.</summary>
		/// <param name="n">The local name of the referencing element being written.</param>
		/// <param name="ns">The namespace of the referencing element being written.</param>
		/// <param name="o">The object being serialized.</param>
		/// <param name="isNullable">true to write an xsi:nil='true' attribute if the object to serialize is null; otherwise, false.</param>
		protected void WriteReferencingElement(string n, string ns, object o, bool isNullable)
		{
			if (o == null)
			{
				if (isNullable)
				{
					WriteNullTagEncoded(n, ns);
				}
				return;
			}
			CheckReferenceQueue();
			if (!AlreadyQueued(o))
			{
				referencedElements.Enqueue(o);
			}
			Writer.WriteStartElement(n, ns);
			Writer.WriteAttributeString("href", "#" + GetId(o, addToReferencesList: true));
			Writer.WriteEndElement();
		}

		private void CheckReferenceQueue()
		{
			if (referencedElements == null)
			{
				referencedElements = new Queue();
				InitCallbacks();
			}
		}

		/// <summary>Writes a SOAP 1.2 RPC result element with a specified qualified name in its body.</summary>
		/// <param name="name">The local name of the result body.</param>
		/// <param name="ns">The namespace of the result body.</param>
		[MonoTODO]
		protected void WriteRpcResult(string name, string ns)
		{
			throw new NotImplementedException();
		}

		/// <summary>Writes an object that uses custom XML formatting as an XML element. </summary>
		/// <param name="serializable">An object that implements the <see cref="T:System.Xml.Serialization.IXmlSerializable" /> interface that uses custom XML formatting.</param>
		/// <param name="name">The local name of the XML element to write.</param>
		/// <param name="ns">The namespace of the XML element to write.</param>
		/// <param name="isNullable">true to write an xsi:nil='true' attribute if the <see cref="T:System.Xml.Serialization.IXmlSerializable" /> class object is null; otherwise, false.</param>
		protected void WriteSerializable(IXmlSerializable serializable, string name, string ns, bool isNullable)
		{
			WriteSerializable(serializable, name, ns, isNullable, wrapped: true);
		}

		/// <summary>Instructs <see cref="T:System.Xml.XmlNode" /> to write an object that uses custom XML formatting as an XML element. </summary>
		/// <param name="serializable">An object that implements the <see cref="T:System.Xml.Serialization.IXmlSerializable" /> interface that uses custom XML formatting.</param>
		/// <param name="name">The local name of the XML element to write.</param>
		/// <param name="ns">The namespace of the XML element to write.</param>
		/// <param name="isNullable">true to write an xsi:nil='true' attribute if the <see cref="T:System.Xml.Serialization.IXmlSerializable" /> object is null; otherwise, false.</param>
		/// <param name="wrapped">true to ignore writing the opening element tag; otherwise, false to write the opening element tag.</param>
		protected void WriteSerializable(IXmlSerializable serializable, string name, string ns, bool isNullable, bool wrapped)
		{
			if (serializable == null)
			{
				if (isNullable && wrapped)
				{
					WriteNullTagLiteral(name, ns);
				}
				return;
			}
			if (wrapped)
			{
				Writer.WriteStartElement(name, ns);
			}
			serializable.WriteXml(Writer);
			if (wrapped)
			{
				Writer.WriteEndElement();
			}
		}

		/// <summary>Writes the XML declaration if the writer is positioned at the start of an XML document. </summary>
		protected void WriteStartDocument()
		{
			if (Writer.WriteState == WriteState.Start)
			{
				Writer.WriteStartDocument();
			}
		}

		/// <summary>Writes an opening element tag, including any attributes. </summary>
		/// <param name="name">The local name of the XML element to write.</param>
		protected void WriteStartElement(string name)
		{
			WriteStartElement(name, string.Empty, null, writePrefixed: false);
		}

		/// <summary>Writes an opening element tag, including any attributes. </summary>
		/// <param name="name">The local name of the XML element to write.</param>
		/// <param name="ns">The namespace of the XML element to write.</param>
		protected void WriteStartElement(string name, string ns)
		{
			WriteStartElement(name, ns, null, writePrefixed: false);
		}

		/// <summary>Writes an opening element tag, including any attributes.</summary>
		/// <param name="name">The local name of the XML element to write.</param>
		/// <param name="ns">The namespace of the XML element to write.</param>
		/// <param name="writePrefixed">true to write the element name with a prefix if none is available for the specified namespace; otherwise, false.</param>
		protected void WriteStartElement(string name, string ns, bool writePrefixed)
		{
			WriteStartElement(name, ns, null, writePrefixed);
		}

		/// <summary>Writes an opening element tag, including any attributes.</summary>
		/// <param name="name">The local name of the XML element to write.</param>
		/// <param name="ns">The namespace of the XML element to write.</param>
		/// <param name="o">The object being serialized as an XML element.</param>
		protected void WriteStartElement(string name, string ns, object o)
		{
			WriteStartElement(name, ns, o, writePrefixed: false);
		}

		/// <summary>Writes an opening element tag, including any attributes.</summary>
		/// <param name="name">The local name of the XML element to write.</param>
		/// <param name="ns">The namespace of the XML element to write.</param>
		/// <param name="o">The object being serialized as an XML element.</param>
		/// <param name="writePrefixed">true to write the element name with a prefix if none is available for the specified namespace; otherwise, false.</param>
		protected void WriteStartElement(string name, string ns, object o, bool writePrefixed)
		{
			WriteStartElement(name, ns, o, writePrefixed, namespaces);
		}

		/// <summary>Writes an opening element tag, including any attributes. </summary>
		/// <param name="name">The local name of the XML element to write.</param>
		/// <param name="ns">The namespace of the XML element to write.</param>
		/// <param name="o">The object being serialized as an XML element.</param>
		/// <param name="writePrefixed">true to write the element name with a prefix if none is available for the specified namespace; otherwise, false.</param>
		/// <param name="xmlns">An instance of the <see cref="T:System.Xml.Serialization.XmlSerializerNamespaces" /> class that contains prefix and namespace pairs to be used in the generated XML.</param>
		protected void WriteStartElement(string name, string ns, object o, bool writePrefixed, XmlSerializerNamespaces xmlns)
		{
			if (xmlns == null)
			{
				throw new ArgumentNullException("xmlns");
			}
			WriteStartElement(name, ns, o, writePrefixed, xmlns.ToArray());
		}

		private void WriteStartElement(string name, string ns, object o, bool writePrefixed, ICollection namespaces)
		{
			if (o != null)
			{
				if (serializedObjects.Contains(o))
				{
					throw new InvalidOperationException("A circular reference was detected while serializing an object of type " + o.GetType().Name);
				}
				serializedObjects[o] = o;
			}
			string text = null;
			if (topLevelElement && ns != null && ns.Length != 0)
			{
				foreach (XmlQualifiedName @namespace in namespaces)
				{
					if (@namespace.Namespace == ns)
					{
						text = @namespace.Name;
						writePrefixed = true;
						break;
					}
				}
			}
			if (writePrefixed && ns != string.Empty)
			{
				name = XmlCustomFormatter.FromXmlName(name);
				if (text == null)
				{
					text = Writer.LookupPrefix(ns);
				}
				if (text == null || text.Length == 0)
				{
					text = "q" + ++qnameCount;
				}
				Writer.WriteStartElement(text, name, ns);
			}
			else
			{
				Writer.WriteStartElement(name, ns);
			}
			if (topLevelElement)
			{
				if (namespaces != null)
				{
					foreach (XmlQualifiedName namespace2 in namespaces)
					{
						string text2 = Writer.LookupPrefix(namespace2.Namespace);
						if (text2 == null || text2.Length == 0)
						{
							WriteAttribute("xmlns", namespace2.Name, "http://www.w3.org/2000/xmlns/", namespace2.Namespace);
						}
					}
				}
				topLevelElement = false;
			}
		}

		/// <summary>Writes an XML element whose text body is a value of a simple XML Schema data type. </summary>
		/// <param name="name">The local name of the element to write.</param>
		/// <param name="ns">The namespace of the element to write.</param>
		/// <param name="o">The object to be serialized in the element body.</param>
		/// <param name="xsiType">true if the XML element explicitly specifies the text value's type using the xsi:type attribute; otherwise, false.</param>
		protected void WriteTypedPrimitive(string name, string ns, object o, bool xsiType)
		{
			TypeData typeData = TypeTranslator.GetTypeData(o.GetType());
			if (typeData.SchemaType != SchemaTypes.Primitive)
			{
				throw new InvalidOperationException($"The type of the argument object '{typeData.FullTypeName}' is not primitive.");
			}
			if (name == null)
			{
				ns = ((!typeData.IsXsdType) ? "http://microsoft.com/wsdl/types/" : "http://www.w3.org/2001/XMLSchema");
				name = typeData.XmlType;
			}
			else
			{
				name = XmlCustomFormatter.FromXmlName(name);
			}
			Writer.WriteStartElement(name, ns);
			string value = (!(o is XmlQualifiedName)) ? XmlCustomFormatter.ToXmlString(typeData, o) : FromXmlQualifiedName((XmlQualifiedName)o);
			if (xsiType)
			{
				if (typeData.SchemaType != SchemaTypes.Primitive)
				{
					throw new InvalidOperationException($"The type {o.GetType().FullName} was not expected. Use the XmlInclude or SoapInclude attribute to specify types that are not known statically.");
				}
				WriteXsiType(typeData.XmlType, (!typeData.IsXsdType) ? "http://microsoft.com/wsdl/types/" : "http://www.w3.org/2001/XMLSchema");
			}
			WriteValue(value);
			Writer.WriteEndElement();
		}

		/// <summary>Writes a base-64 byte array.</summary>
		/// <param name="value">The byte array to write.</param>
		protected void WriteValue(byte[] value)
		{
			Writer.WriteBase64(value, 0, value.Length);
		}

		/// <summary>Writes a specified string.</summary>
		/// <param name="value">The string to write.</param>
		protected void WriteValue(string value)
		{
			if (value != null)
			{
				Writer.WriteString(value);
			}
		}

		/// <summary>Writes the specified <see cref="T:System.Xml.XmlNode" /> as an XML attribute.</summary>
		/// <param name="node">An <see cref="T:System.Xml.XmlAttribute" /> object.</param>
		protected void WriteXmlAttribute(XmlNode node)
		{
			WriteXmlAttribute(node, null);
		}

		/// <summary>Writes the specified <see cref="T:System.Xml.XmlNode" /> object as an XML attribute.</summary>
		/// <param name="node">An <see cref="T:System.Xml.XmlNode" /> of <see cref="T:System.Xml.XmlAttribute" /> type.</param>
		/// <param name="container">An <see cref="T:System.Xml.Schema.XmlSchemaObject" /> object (or null) used to generate a qualified name value for an arrayType attribute from the Web Services Description Language (WSDL) namespace ("http://schemas.xmlsoap.org/wsdl/").</param>
		protected void WriteXmlAttribute(XmlNode node, object container)
		{
			XmlAttribute xmlAttribute = node as XmlAttribute;
			if (xmlAttribute == null)
			{
				throw new InvalidOperationException("The node must be either type XmlAttribute or a derived type.");
			}
			if (xmlAttribute.NamespaceURI == "http://schemas.xmlsoap.org/wsdl/" && xmlAttribute.LocalName == "arrayType")
			{
				TypeTranslator.ParseArrayType(xmlAttribute.Value, out string type, out string ns, out string dimensions);
				string qualifiedName = GetQualifiedName(type + dimensions, ns);
				WriteAttribute(xmlAttribute.Prefix, xmlAttribute.LocalName, xmlAttribute.NamespaceURI, qualifiedName);
			}
			else
			{
				WriteAttribute(xmlAttribute.Prefix, xmlAttribute.LocalName, xmlAttribute.NamespaceURI, xmlAttribute.Value);
			}
		}

		/// <summary>Writes an xsi:type attribute for an XML element that is being serialized into a document. </summary>
		/// <param name="name">The local name of an XML Schema data type.</param>
		/// <param name="ns">The namespace of an XML Schema data type.</param>
		protected void WriteXsiType(string name, string ns)
		{
			if (ns != null && ns != string.Empty)
			{
				WriteAttribute("type", "http://www.w3.org/2001/XMLSchema-instance", GetQualifiedName(name, ns));
			}
			else
			{
				WriteAttribute("type", "http://www.w3.org/2001/XMLSchema-instance", name);
			}
		}

		/// <summary>Creates an <see cref="T:System.InvalidOperationException" /> that indicates the <see cref="T:System.Xml.Serialization.XmlAnyElementAttribute" /> has been invalidly applied to a member; only members that are of type <see cref="T:System.Xml.XmlNode" />, or derived from <see cref="T:System.Xml.XmlNode" />, are valid.</summary>
		/// <returns>An <see cref="T:System.InvalidOperationException" /> exception.</returns>
		/// <param name="o">The object that represents the invalid member.</param>
		protected Exception CreateInvalidAnyTypeException(object o)
		{
			if (o == null)
			{
				return new InvalidOperationException("null is invalid as anyType in XmlSerializer");
			}
			return CreateInvalidAnyTypeException(o.GetType());
		}

		/// <summary>Creates an <see cref="T:System.InvalidOperationException" /> that indicates the <see cref="T:System.Xml.Serialization.XmlAnyElementAttribute" /> has been invalidly applied to a member; only members that are of type <see cref="T:System.Xml.XmlNode" />, or derived from <see cref="T:System.Xml.XmlNode" />, are valid.</summary>
		/// <returns>An <see cref="T:System.InvalidOperationException" /> exception.</returns>
		/// <param name="type">The <see cref="T:System.Type" /> that is invalid.</param>
		protected Exception CreateInvalidAnyTypeException(Type t)
		{
			return new InvalidOperationException($"An object of type '{t}' is invalid as anyType in XmlSerializer");
		}

		/// <summary>Creates an <see cref="T:System.InvalidOperationException" /> for an invalid enumeration value.</summary>
		/// <returns>An <see cref="T:System.ComponentModel.InvalidEnumArgumentException" />.</returns>
		/// <param name="value">An object that represents the invalid enumeration.</param>
		/// <param name="typeName">The XML type name.</param>
		protected Exception CreateInvalidEnumValueException(object value, string typeName)
		{
			return new InvalidOperationException(string.Format(CultureInfo.CurrentCulture, "'{0}' is not a valid value for {1}.", value, typeName));
		}

		/// <summary>Takes a numeric enumeration value and the names and constants from the enumerator list for the enumeration and returns a string that consists of delimited identifiers that represent the enumeration members that have been set.</summary>
		/// <returns>A string that consists of delimited identifiers, where each item is one of the values set by the bitwise operation.</returns>
		/// <param name="value">The enumeration value as a series of bitwise OR operations.</param>
		/// <param name="values">The values of the enumeration.</param>
		/// <param name="ids">The constants of the enumeration.</param>
		/// <param name="typeName">The name of the type </param>
		protected static string FromEnum(long value, string[] values, long[] ids, string typeName)
		{
			return XmlCustomFormatter.FromEnum(value, values, ids, typeName);
		}

		/// <summary>Produces a string that can be written as an XML qualified name, with invalid characters replaced by escape sequences. </summary>
		/// <returns>An XML qualified name, with invalid characters replaced by escape sequences.</returns>
		/// <param name="xmlQualifiedName">An <see cref="T:System.Xml.XmlQualifiedName" /> that represents the XML to be written.</param>
		/// <param name="ignoreEmpty">true to ignore empty spaces in the string; otherwise, false.</param>
		[MonoTODO]
		protected string FromXmlQualifiedName(XmlQualifiedName xmlQualifiedName, bool ignoreEmpty)
		{
			throw new NotImplementedException();
		}

		/// <summary>Gets a dynamically generated assembly by name.</summary>
		/// <returns>A dynamically generated assembly.</returns>
		/// <param name="assemblyFullName">The full name of the assembly.</param>
		[MonoTODO]
		protected static Assembly ResolveDynamicAssembly(string assemblyFullName)
		{
			throw new NotImplementedException();
		}
	}
}
