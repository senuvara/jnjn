namespace System.Xml
{
	/// <summary>Gets the node immediately preceding or following this node.</summary>
	public abstract class XmlLinkedNode : XmlNode
	{
		private XmlLinkedNode nextSibling;

		internal bool IsRooted
		{
			get
			{
				for (XmlNode parentNode = ParentNode; parentNode != null; parentNode = parentNode.ParentNode)
				{
					if (parentNode.NodeType == XmlNodeType.Document)
					{
						return true;
					}
				}
				return false;
			}
		}

		/// <summary>Gets the node immediately following this node.</summary>
		/// <returns>The <see cref="T:System.Xml.XmlNode" /> immediately following this node or null if one does not exist.</returns>
		public override XmlNode NextSibling => (ParentNode != null && ParentNode.LastChild != this) ? nextSibling : null;

		internal XmlLinkedNode NextLinkedSibling
		{
			get
			{
				return nextSibling;
			}
			set
			{
				nextSibling = value;
			}
		}

		/// <summary>Gets the node immediately preceding this node.</summary>
		/// <returns>The preceding <see cref="T:System.Xml.XmlNode" /> or null if one does not exist.</returns>
		public override XmlNode PreviousSibling
		{
			get
			{
				if (ParentNode != null)
				{
					XmlNode firstChild = ParentNode.FirstChild;
					if (firstChild != this)
					{
						do
						{
							if (firstChild.NextSibling == this)
							{
								return firstChild;
							}
						}
						while ((firstChild = firstChild.NextSibling) != null);
					}
				}
				return null;
			}
		}

		internal XmlLinkedNode(XmlDocument doc)
			: base(doc)
		{
		}
	}
}
