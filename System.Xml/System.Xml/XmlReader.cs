using Mono.Xml;
using Mono.Xml.Schema;
using System.IO;
using System.Text;
using System.Xml.Schema;

namespace System.Xml
{
	/// <summary>Represents a reader that provides fast, non-cached, forward-only access to XML data.</summary>
	public abstract class XmlReader : IDisposable
	{
		private StringBuilder readStringBuffer;

		private XmlReaderBinarySupport binary;

		private XmlReaderSettings settings;

		/// <summary>When overridden in a derived class, gets the number of attributes on the current node.</summary>
		/// <returns>The number of attributes on the current node.</returns>
		public abstract int AttributeCount
		{
			get;
		}

		/// <summary>When overridden in a derived class, gets the base URI of the current node.</summary>
		/// <returns>The base URI of the current node.</returns>
		public abstract string BaseURI
		{
			get;
		}

		internal XmlReaderBinarySupport Binary => binary;

		internal XmlReaderBinarySupport.CharGetter BinaryCharGetter
		{
			get
			{
				return (binary == null) ? null : binary.Getter;
			}
			set
			{
				if (binary == null)
				{
					binary = new XmlReaderBinarySupport(this);
				}
				binary.Getter = value;
			}
		}

		/// <summary>Gets a value indicating whether the <see cref="T:System.Xml.XmlReader" /> implements the binary content read methods.</summary>
		/// <returns>true if the binary content read methods are implemented; otherwise false.</returns>
		public virtual bool CanReadBinaryContent => false;

		/// <summary>Gets a value indicating whether the <see cref="T:System.Xml.XmlReader" /> implements the <see cref="M:System.Xml.XmlReader.ReadValueChunk(System.Char[],System.Int32,System.Int32)" /> method. </summary>
		/// <returns>true if the <see cref="T:System.Xml.XmlReader" /> implements the <see cref="M:System.Xml.XmlReader.ReadValueChunk(System.Char[],System.Int32,System.Int32)" /> method; otherwise false.</returns>
		public virtual bool CanReadValueChunk => false;

		/// <summary>Gets a value indicating whether this reader can parse and resolve entities.</summary>
		/// <returns>true if the reader can parse and resolve entities; otherwise, false.</returns>
		public virtual bool CanResolveEntity => false;

		/// <summary>When overridden in a derived class, gets the depth of the current node in the XML document.</summary>
		/// <returns>The depth of the current node in the XML document.</returns>
		public abstract int Depth
		{
			get;
		}

		/// <summary>When overridden in a derived class, gets a value indicating whether the reader is positioned at the end of the stream.</summary>
		/// <returns>true if the reader is positioned at the end of the stream; otherwise, false.</returns>
		public abstract bool EOF
		{
			get;
		}

		/// <summary>Gets a value indicating whether the current node has any attributes.</summary>
		/// <returns>true if the current node has attributes; otherwise, false.</returns>
		public virtual bool HasAttributes => AttributeCount > 0;

		/// <summary>When overridden in a derived class, gets a value indicating whether the current node can have a <see cref="P:System.Xml.XmlReader.Value" />.</summary>
		/// <returns>true if the node on which the reader is currently positioned can have a Value; otherwise, false. If false, the node has a value of String.Empty.</returns>
		public abstract bool HasValue
		{
			get;
		}

		/// <summary>When overridden in a derived class, gets a value indicating whether the current node is an empty element (for example, &lt;MyElement/&gt;).</summary>
		/// <returns>true if the current node is an element (<see cref="P:System.Xml.XmlReader.NodeType" /> equals XmlNodeType.Element) that ends with /&gt;; otherwise, false.</returns>
		public abstract bool IsEmptyElement
		{
			get;
		}

		/// <summary>When overridden in a derived class, gets a value indicating whether the current node is an attribute that was generated from the default value defined in the DTD or schema.</summary>
		/// <returns>true if the current node is an attribute whose value was generated from the default value defined in the DTD or schema; false if the attribute value was explicitly set.</returns>
		public virtual bool IsDefault => false;

		/// <summary>When overridden in a derived class, gets the value of the attribute with the specified index.</summary>
		/// <returns>The value of the specified attribute.</returns>
		/// <param name="i">The index of the attribute. </param>
		public virtual string this[int i] => GetAttribute(i);

		/// <summary>When overridden in a derived class, gets the value of the attribute with the specified <see cref="P:System.Xml.XmlReader.Name" />.</summary>
		/// <returns>The value of the specified attribute. If the attribute is not found, null is returned.</returns>
		/// <param name="name">The qualified name of the attribute. </param>
		public virtual string this[string name] => GetAttribute(name);

		/// <summary>When overridden in a derived class, gets the value of the attribute with the specified <see cref="P:System.Xml.XmlReader.LocalName" /> and <see cref="P:System.Xml.XmlReader.NamespaceURI" />.</summary>
		/// <returns>The value of the specified attribute. If the attribute is not found, null is returned.</returns>
		/// <param name="name">The local name of the attribute. </param>
		/// <param name="namespaceURI">The namespace URI of the attribute. </param>
		public virtual string this[string name, string namespaceURI] => GetAttribute(name, namespaceURI);

		/// <summary>When overridden in a derived class, gets the local name of the current node.</summary>
		/// <returns>The name of the current node with the prefix removed. For example, LocalName is book for the element &lt;bk:book&gt;.For node types that do not have a name (like Text, Comment, and so on), this property returns String.Empty.</returns>
		public abstract string LocalName
		{
			get;
		}

		/// <summary>When overridden in a derived class, gets the qualified name of the current node.</summary>
		/// <returns>The qualified name of the current node. For example, Name is bk:book for the element &lt;bk:book&gt;.The name returned is dependent on the <see cref="P:System.Xml.XmlReader.NodeType" /> of the node. The following node types return the listed values. All other node types return an empty string.Node type Name AttributeThe name of the attribute. DocumentTypeThe document type name. ElementThe tag name. EntityReferenceThe name of the entity referenced. ProcessingInstructionThe target of the processing instruction. XmlDeclarationThe literal string xml. </returns>
		public virtual string Name => (Prefix.Length <= 0) ? LocalName : (Prefix + ":" + LocalName);

		/// <summary>When overridden in a derived class, gets the namespace URI (as defined in the W3C Namespace specification) of the node on which the reader is positioned.</summary>
		/// <returns>The namespace URI of the current node; otherwise an empty string.</returns>
		public abstract string NamespaceURI
		{
			get;
		}

		/// <summary>When overridden in a derived class, gets the <see cref="T:System.Xml.XmlNameTable" /> associated with this implementation.</summary>
		/// <returns>The XmlNameTable enabling you to get the atomized version of a string within the node.</returns>
		public abstract XmlNameTable NameTable
		{
			get;
		}

		/// <summary>When overridden in a derived class, gets the type of the current node.</summary>
		/// <returns>One of the <see cref="T:System.Xml.XmlNodeType" /> values representing the type of the current node.</returns>
		public abstract XmlNodeType NodeType
		{
			get;
		}

		/// <summary>When overridden in a derived class, gets the namespace prefix associated with the current node.</summary>
		/// <returns>The namespace prefix associated with the current node.</returns>
		public abstract string Prefix
		{
			get;
		}

		/// <summary>When overridden in a derived class, gets the quotation mark character used to enclose the value of an attribute node.</summary>
		/// <returns>The quotation mark character (" or ') used to enclose the value of an attribute node.</returns>
		public virtual char QuoteChar => '"';

		/// <summary>When overridden in a derived class, gets the state of the reader.</summary>
		/// <returns>One of the <see cref="T:System.Xml.ReadState" /> values.</returns>
		public abstract ReadState ReadState
		{
			get;
		}

		/// <summary>Gets the schema information that has been assigned to the current node as a result of schema validation.</summary>
		/// <returns>An <see cref="T:System.Xml.Schema.IXmlSchemaInfo" /> object containing the schema information for the current node. Schema information can be set on elements, attributes, or on text nodes with a non-null <see cref="P:System.Xml.XmlReader.ValueType" /> (typed values). If the current node is not one of the above node types, or if the XmlReader instance does not report schema information, this property returns null.If this property is called from an <see cref="T:System.Xml.XmlTextReader" /> or an <see cref="T:System.Xml.XmlValidatingReader" /> object, this property always returns null. These XmlReader implementations do not expose schema information through the SchemaInfo property.</returns>
		public virtual IXmlSchemaInfo SchemaInfo => null;

		/// <summary>Gets the <see cref="T:System.Xml.XmlReaderSettings" /> object used to create this <see cref="T:System.Xml.XmlReader" /> instance.</summary>
		/// <returns>The <see cref="T:System.Xml.XmlReaderSettings" /> object used to create this reader instance. If this reader was not created using the <see cref="Overload:System.Xml.XmlReader.Create" /> method, this property returns null.</returns>
		public virtual XmlReaderSettings Settings => settings;

		/// <summary>When overridden in a derived class, gets the text value of the current node.</summary>
		/// <returns>The value returned depends on the <see cref="P:System.Xml.XmlReader.NodeType" /> of the node. The following table lists node types that have a value to return. All other node types return String.Empty.Node type Value AttributeThe value of the attribute. CDATAThe content of the CDATA section. CommentThe content of the comment. DocumentTypeThe internal subset. ProcessingInstructionThe entire content, excluding the target. SignificantWhitespaceThe white space between markup in a mixed content model. TextThe content of the text node. WhitespaceThe white space between markup. XmlDeclarationThe content of the declaration. </returns>
		public abstract string Value
		{
			get;
		}

		/// <summary>When overridden in a derived class, gets the current xml:lang scope.</summary>
		/// <returns>The current xml:lang scope.</returns>
		public virtual string XmlLang => string.Empty;

		/// <summary>When overridden in a derived class, gets the current xml:space scope.</summary>
		/// <returns>One of the <see cref="T:System.Xml.XmlSpace" /> values. If no xml:space scope exists, this property defaults to XmlSpace.None.</returns>
		public virtual XmlSpace XmlSpace => XmlSpace.None;

		/// <summary>Gets The Common Language Runtime (CLR) type for the current node.</summary>
		/// <returns>The CLR type that corresponds to the typed value of the node. The default is System.String.</returns>
		public virtual Type ValueType => typeof(string);

		/// <summary>For a description of this member, see <see cref="M:System.IDisposable.Dispose" />.</summary>
		void IDisposable.Dispose()
		{
			Dispose(disposing: false);
		}

		/// <summary>When overridden in a derived class, changes the <see cref="P:System.Xml.XmlReader.ReadState" /> to Closed.</summary>
		public abstract void Close();

		private static XmlNameTable PopulateNameTable(XmlReaderSettings settings)
		{
			XmlNameTable xmlNameTable = settings.NameTable;
			if (xmlNameTable == null)
			{
				xmlNameTable = new NameTable();
			}
			return xmlNameTable;
		}

		private static XmlParserContext PopulateParserContext(XmlReaderSettings settings, string baseUri)
		{
			XmlNameTable xmlNameTable = PopulateNameTable(settings);
			return new XmlParserContext(xmlNameTable, new XmlNamespaceManager(xmlNameTable), null, null, null, null, baseUri, null, XmlSpace.None, null);
		}

		private static XmlNodeType GetNodeType(XmlReaderSettings settings)
		{
			ConformanceLevel conformanceLevel = settings?.ConformanceLevel ?? ConformanceLevel.Auto;
			return (conformanceLevel == ConformanceLevel.Fragment) ? XmlNodeType.Element : XmlNodeType.Document;
		}

		/// <summary>Creates a new <see cref="T:System.Xml.XmlReader" /> instance using the specified stream.</summary>
		/// <returns>An <see cref="T:System.Xml.XmlReader" /> object used to read the data contained in the stream. </returns>
		/// <param name="input">The stream containing the XML data. The <see cref="T:System.Xml.XmlReader" /> scans the first bytes of the stream looking for a byte order mark or other sign of encoding. When encoding is determined, the encoding is used to continue reading the stream, and processing continues parsing the input as a stream of (Unicode) characters.</param>
		/// <exception cref="T:System.ArgumentNullException">The <paramref name="input" /> value is null.</exception>
		/// <exception cref="T:System.Security.SecurityException">The <see cref="T:System.Xml.XmlReader" /> does not have sufficient permissions to access the location of the XML data.</exception>
		public static XmlReader Create(Stream stream)
		{
			return Create(stream, null);
		}

		/// <summary>Creates a new <see cref="T:System.Xml.XmlReader" /> instance with specified URI.</summary>
		/// <returns>An <see cref="T:System.Xml.XmlReader" /> object to read the XML data. </returns>
		/// <param name="inputUri">The URI for the file containing the XML data. The <see cref="T:System.Xml.XmlUrlResolver" /> class is used to convert the path to a canonical data representation.</param>
		/// <exception cref="T:System.ArgumentNullException">The <paramref name="inputUri" /> value is null.</exception>
		/// <exception cref="T:System.Security.SecurityException">The <see cref="T:System.Xml.XmlReader" /> does not have sufficient permissions to access the location of the XML data.</exception>
		/// <exception cref="T:System.IO.FileNotFoundException">The file identified by the URI does not exist.</exception>
		/// <exception cref="T:System.UriFormatException">The URI format is not correct.</exception>
		public static XmlReader Create(string url)
		{
			return Create(url, null);
		}

		/// <summary>Creates a new <see cref="T:System.Xml.XmlReader" /> instance with the specified <see cref="T:System.IO.TextReader" />.</summary>
		/// <returns>An <see cref="T:System.Xml.XmlReader" /> object to read the XML data.</returns>
		/// <param name="input">The <see cref="T:System.IO.TextReader" /> from which to read the XML data. Because a <see cref="T:System.IO.TextReader" /> returns a stream of Unicode characters, the encoding specified in the XML declaration is not used by the <see cref="T:System.Xml.XmlReader" /> to decode the data stream.</param>
		/// <exception cref="T:System.ArgumentNullException">The <paramref name="input" /> value is null.</exception>
		public static XmlReader Create(TextReader reader)
		{
			return Create(reader, null);
		}

		/// <summary>Creates a new instance with the specified URI and <see cref="T:System.Xml.XmlReaderSettings" />.</summary>
		/// <returns>An <see cref="T:System.Xml.XmlReader" /> object to read XML data.</returns>
		/// <param name="inputUri">The URI for the file containing the XML data. The <see cref="T:System.Xml.XmlResolver" /> object on the <see cref="T:System.Xml.XmlReaderSettings" /> object is used to convert the path to a canonical data representation. If <see cref="P:System.Xml.XmlReaderSettings.XmlResolver" /> is null, a new <see cref="T:System.Xml.XmlUrlResolver" /> object is used.</param>
		/// <param name="settings">The <see cref="T:System.Xml.XmlReaderSettings" /> object used to configure the new <see cref="T:System.Xml.XmlReader" /> instance. This value can be null.</param>
		/// <exception cref="T:System.ArgumentNullException">The <paramref name="inputUri" /> value is null.</exception>
		/// <exception cref="T:System.IO.FileNotFoundException">The file specified by the URI cannot be found.</exception>
		/// <exception cref="T:System.UriFormatException">The URI format is not correct.</exception>
		public static XmlReader Create(string url, XmlReaderSettings settings)
		{
			return Create(url, settings, null);
		}

		/// <summary>Creates a new <see cref="T:System.Xml.XmlReader" /> instance with the specified stream and <see cref="T:System.Xml.XmlReaderSettings" /> object.</summary>
		/// <returns>An <see cref="T:System.Xml.XmlReader" /> object to read the XML data.</returns>
		/// <param name="input">The stream containing the XML data. The <see cref="T:System.Xml.XmlReader" /> scans the first bytes of the stream looking for a byte order mark or other sign of encoding. When encoding is determined, the encoding is used to continue reading the stream, and processing continues parsing the input as a stream of (Unicode) characters.</param>
		/// <param name="settings">The <see cref="T:System.Xml.XmlReaderSettings" /> object used to configure the new <see cref="T:System.Xml.XmlReader" /> instance. This value can be null.</param>
		/// <exception cref="T:System.ArgumentNullException">The <paramref name="input" /> value is null.</exception>
		public static XmlReader Create(Stream stream, XmlReaderSettings settings)
		{
			return Create(stream, settings, string.Empty);
		}

		/// <summary>Creates a new <see cref="T:System.Xml.XmlReader" /> instance using the specified <see cref="T:System.IO.TextReader" /> and <see cref="T:System.Xml.XmlReaderSettings" /> objects.</summary>
		/// <returns>An <see cref="T:System.Xml.XmlReader" /> object to read XML data.</returns>
		/// <param name="input">The <see cref="T:System.IO.TextReader" /> from which to read the XML data. Because a <see cref="T:System.IO.TextReader" /> returns a stream of Unicode characters, the encoding specified in the XML declaration is not used by the <see cref="T:System.Xml.XmlReader" /> to decode the data stream</param>
		/// <param name="settings">The <see cref="T:System.Xml.XmlReaderSettings" /> object used to configure the new <see cref="T:System.Xml.XmlReader" />. This value can be null.</param>
		/// <exception cref="T:System.ArgumentNullException">The <paramref name="input" /> value is null.</exception>
		public static XmlReader Create(TextReader reader, XmlReaderSettings settings)
		{
			return Create(reader, settings, string.Empty);
		}

		private static XmlReaderSettings PopulateSettings(XmlReaderSettings src)
		{
			if (src == null)
			{
				return new XmlReaderSettings();
			}
			return src.Clone();
		}

		/// <summary>Creates a new <see cref="T:System.Xml.XmlReader" /> instance using the specified stream, base URI, and <see cref="T:System.Xml.XmlReaderSettings" /> object.</summary>
		/// <returns>An <see cref="T:System.Xml.XmlReader" /> object to read XML data.</returns>
		/// <param name="input">The stream containing the XML data. The <see cref="T:System.Xml.XmlReader" /> scans the first bytes of the stream looking for a byte order mark or other sign of encoding. When encoding is determined, the encoding is used to continue reading the stream, and processing continues parsing the input as a stream of (Unicode) characters.</param>
		/// <param name="settings">The <see cref="T:System.Xml.XmlReaderSettings" /> object used to configure the new <see cref="T:System.Xml.XmlReader" /> instance. This value can be null.</param>
		/// <param name="baseUri">The base URI for the entity or document being read. This value can be null.The base URI is used to resolve the relative URI of the XML document. Do not use a base URI from an untrusted source.</param>
		/// <exception cref="T:System.ArgumentNullException">The <paramref name="input" /> value is null.</exception>
		public static XmlReader Create(Stream stream, XmlReaderSettings settings, string baseUri)
		{
			settings = PopulateSettings(settings);
			return Create(stream, settings, PopulateParserContext(settings, baseUri));
		}

		/// <summary>Creates a new <see cref="T:System.Xml.XmlReader" /> instance using the specified <see cref="T:System.IO.TextReader" />, <see cref="T:System.Xml.XmlReaderSettings" />, and base URI.</summary>
		/// <returns>An <see cref="T:System.Xml.XmlReader" /> object to read XML data.</returns>
		/// <param name="input">The <see cref="T:System.IO.TextReader" /> from which to read the XML data. Because a <see cref="T:System.IO.TextReader" /> returns a stream of Unicode characters, the encoding specified in the XML declaration is not used by the <see cref="T:System.Xml.XmlReader" /> to decode the data stream.</param>
		/// <param name="settings">The <see cref="T:System.Xml.XmlReaderSettings" /> object used to configure the new <see cref="T:System.Xml.XmlReader" /> instance. This value can be null.</param>
		/// <param name="baseUri">The base URI for the entity or document being read. This value can be null.The base URI is used to resolve the relative URI of the XML document. Do not use a base URI from an untrusted source.</param>
		/// <exception cref="T:System.ArgumentNullException">The <paramref name="input" /> value is null.</exception>
		public static XmlReader Create(TextReader reader, XmlReaderSettings settings, string baseUri)
		{
			settings = PopulateSettings(settings);
			return Create(reader, settings, PopulateParserContext(settings, baseUri));
		}

		/// <summary>Creates a new <see cref="T:System.Xml.XmlReader" /> instance with the specified <see cref="T:System.Xml.XmlReader" /> and <see cref="T:System.Xml.XmlReaderSettings" /> objects.</summary>
		/// <returns>An <see cref="T:System.Xml.XmlReader" /> object that is wrapped around the specified <see cref="T:System.Xml.XmlReader" /> object.</returns>
		/// <param name="reader">The <see cref="T:System.Xml.XmlReader" /> object that you wish to use as the underlying reader.</param>
		/// <param name="settings">The <see cref="T:System.Xml.XmlReaderSettings" /> object used to configure the new <see cref="T:System.Xml.XmlReader" /> instance.The conformance level of the <see cref="T:System.Xml.XmlReaderSettings" /> object must either match the conformance level of the underlying reader, or it must be set to <see cref="F:System.Xml.ConformanceLevel.Auto" />.</param>
		/// <exception cref="T:System.ArgumentNullException">The <paramref name="reader" /> value is null.</exception>
		/// <exception cref="T:System.InvalidOperationException">If the <see cref="T:System.Xml.XmlReaderSettings" /> object specifies a conformance level that is not consistent with conformance level of the underlying reader.-or-The underlying <see cref="T:System.Xml.XmlReader" /> is in an <see cref="F:System.Xml.ReadState.Error" /> or <see cref="F:System.Xml.ReadState.Closed" /> state.</exception>
		public static XmlReader Create(XmlReader reader, XmlReaderSettings settings)
		{
			settings = PopulateSettings(settings);
			XmlReader xmlReader = CreateFilteredXmlReader(reader, settings);
			xmlReader.settings = settings;
			return xmlReader;
		}

		/// <summary>Creates a new <see cref="T:System.Xml.XmlReader" /> instance using the specified URI, <see cref="T:System.Xml.XmlReaderSettings" />, and <see cref="T:System.Xml.XmlParserContext" /> objects.</summary>
		/// <returns>An <see cref="T:System.Xml.XmlReader" /> object to read XML data.</returns>
		/// <param name="inputUri">The URI for the file containing the XML data. The <see cref="T:System.Xml.XmlResolver" /> object on the <see cref="T:System.Xml.XmlReaderSettings" /> object is used to convert the path to a canonical data representation. If <see cref="P:System.Xml.XmlReaderSettings.XmlResolver" /> is null, a new <see cref="T:System.Xml.XmlUrlResolver" /> object is used.</param>
		/// <param name="settings">The <see cref="T:System.Xml.XmlReaderSettings" /> object used to configure the new <see cref="T:System.Xml.XmlReader" /> instance. This value can be null.</param>
		/// <param name="inputContext">The <see cref="T:System.Xml.XmlParserContext" /> object that provides the context information required to parse the XML fragment. The context information can include the <see cref="T:System.Xml.XmlNameTable" /> to use, encoding, namespace scope, the current xml:lang and xml:space scope, base URI, and document type definition. This value can be null.</param>
		/// <exception cref="T:System.ArgumentNullException">The inputUri value is null.</exception>
		/// <exception cref="T:System.Security.SecurityException">The <see cref="T:System.Xml.XmlReader" /> does not have sufficient permissions to access the location of the XML data.</exception>
		/// <exception cref="T:System.ArgumentException">The <see cref="P:System.Xml.XmlReaderSettings.NameTable" />  and <see cref="P:System.Xml.XmlParserContext.NameTable" /> properties both contain values. (Only one of these NameTable properties can be set and used).</exception>
		/// <exception cref="T:System.IO.FileNotFoundException">The file specified by the URI cannot be found.</exception>
		/// <exception cref="T:System.UriFormatException">The URI format is not correct.</exception>
		public static XmlReader Create(string url, XmlReaderSettings settings, XmlParserContext context)
		{
			//Discarded unreachable code: IL_0049
			settings = PopulateSettings(settings);
			bool closeInput = settings.CloseInput;
			try
			{
				settings.CloseInput = true;
				if (context == null)
				{
					context = PopulateParserContext(settings, url);
				}
				XmlTextReader reader = new XmlTextReader(dummy: false, settings.XmlResolver, url, GetNodeType(settings), context);
				return CreateCustomizedTextReader(reader, settings);
			}
			finally
			{
				settings.CloseInput = closeInput;
			}
		}

		/// <summary>Creates a new <see cref="T:System.Xml.XmlReader" /> instance using the specified stream, <see cref="T:System.Xml.XmlReaderSettings" />, and <see cref="T:System.Xml.XmlParserContext" /> objects.</summary>
		/// <returns>An <see cref="T:System.Xml.XmlReader" /> object to read XML data.</returns>
		/// <param name="input">The stream containing the XML data. The <see cref="T:System.Xml.XmlReader" /> scans the first bytes of the stream looking for a byte order mark or other sign of encoding. When encoding is determined, the encoding is used to continue reading the stream, and processing continues parsing the input as a stream of (Unicode) characters.</param>
		/// <param name="settings">The <see cref="T:System.Xml.XmlReaderSettings" /> object used to configure the new <see cref="T:System.Xml.XmlReader" /> instance. This value can be null.</param>
		/// <param name="inputContext">The <see cref="T:System.Xml.XmlParserContext" /> object that provides the context information required to parse the XML fragment. The context information can include the <see cref="T:System.Xml.XmlNameTable" /> to use, encoding, namespace scope, the current xml:lang and xml:space scope, base URI, and document type definition. This value can be null.</param>
		/// <exception cref="T:System.ArgumentNullException">The <paramref name="input" /> value is null.</exception>
		public static XmlReader Create(Stream stream, XmlReaderSettings settings, XmlParserContext context)
		{
			settings = PopulateSettings(settings);
			if (context == null)
			{
				context = PopulateParserContext(settings, string.Empty);
			}
			return CreateCustomizedTextReader(new XmlTextReader(stream, GetNodeType(settings), context), settings);
		}

		/// <summary>Creates a new <see cref="T:System.Xml.XmlReader" /> instance using the specified <see cref="T:System.IO.TextReader" />, <see cref="T:System.Xml.XmlReaderSettings" />, and <see cref="T:System.Xml.XmlParserContext" /> objects.</summary>
		/// <returns>An <see cref="T:System.Xml.XmlReader" /> object to read XML data.</returns>
		/// <param name="input">The <see cref="T:System.IO.TextReader" /> from which to read the XML data. Because a <see cref="T:System.IO.TextReader" /> returns a stream of Unicode characters, the encoding specified in the XML declaration is not used by the <see cref="T:System.Xml.XmlReader" /> to decode the data stream.</param>
		/// <param name="settings">The <see cref="T:System.Xml.XmlReaderSettings" /> object used to configure the new <see cref="T:System.Xml.XmlReader" /> instance. This value can be null.</param>
		/// <param name="inputContext">The <see cref="T:System.Xml.XmlParserContext" /> object that provides the context information required to parse the XML fragment. The context information can include the <see cref="T:System.Xml.XmlNameTable" /> to use, encoding, namespace scope, the current xml:lang and xml:space scope, base URI, and document type definition. This value can be null.</param>
		/// <exception cref="T:System.ArgumentNullException">The <paramref name="input" /> value is null.</exception>
		/// <exception cref="T:System.ArgumentException">The <see cref="P:System.Xml.XmlReaderSettings.NameTable" />  and <see cref="P:System.Xml.XmlParserContext.NameTable" /> properties both contain values. (Only one of these NameTable properties can be set and used).</exception>
		public static XmlReader Create(TextReader reader, XmlReaderSettings settings, XmlParserContext context)
		{
			settings = PopulateSettings(settings);
			if (context == null)
			{
				context = PopulateParserContext(settings, string.Empty);
			}
			return CreateCustomizedTextReader(new XmlTextReader(context.BaseURI, reader, GetNodeType(settings), context), settings);
		}

		private static XmlReader CreateCustomizedTextReader(XmlTextReader reader, XmlReaderSettings settings)
		{
			reader.XmlResolver = settings.XmlResolver;
			reader.Normalization = true;
			reader.EntityHandling = EntityHandling.ExpandEntities;
			if (settings.ProhibitDtd)
			{
				reader.ProhibitDtd = true;
			}
			if (!settings.CheckCharacters)
			{
				reader.CharacterChecking = false;
			}
			reader.CloseInput = settings.CloseInput;
			reader.Conformance = settings.ConformanceLevel;
			reader.AdjustLineInfoOffset(settings.LineNumberOffset, settings.LinePositionOffset);
			if (settings.NameTable != null)
			{
				reader.SetNameTable(settings.NameTable);
			}
			XmlReader xmlReader = CreateFilteredXmlReader(reader, settings);
			xmlReader.settings = settings;
			return xmlReader;
		}

		private static XmlReader CreateFilteredXmlReader(XmlReader reader, XmlReaderSettings settings)
		{
			ConformanceLevel conformanceLevel = ConformanceLevel.Auto;
			conformanceLevel = ((reader is XmlTextReader) ? ((XmlTextReader)reader).Conformance : ((reader.Settings == null) ? settings.ConformanceLevel : reader.Settings.ConformanceLevel));
			if (settings.ConformanceLevel != 0 && conformanceLevel != settings.ConformanceLevel)
			{
				throw new InvalidOperationException($"ConformanceLevel cannot be overwritten by a wrapping XmlReader. The source reader has {conformanceLevel}, while {settings.ConformanceLevel} is specified.");
			}
			settings.ConformanceLevel = conformanceLevel;
			reader = CreateValidatingXmlReader(reader, settings);
			if (settings.IgnoreComments || settings.IgnoreProcessingInstructions || settings.IgnoreWhitespace)
			{
				return new XmlFilterReader(reader, settings);
			}
			reader.settings = settings;
			return reader;
		}

		private static XmlReader CreateValidatingXmlReader(XmlReader reader, XmlReaderSettings settings)
		{
			XmlValidatingReader xmlValidatingReader = null;
			switch (settings.ValidationType)
			{
			default:
				return reader;
			case ValidationType.DTD:
				xmlValidatingReader = new XmlValidatingReader(reader);
				xmlValidatingReader.XmlResolver = settings.XmlResolver;
				xmlValidatingReader.ValidationType = ValidationType.DTD;
				if ((settings.ValidationFlags & XmlSchemaValidationFlags.ProcessIdentityConstraints) == 0)
				{
					throw new NotImplementedException();
				}
				return (xmlValidatingReader == null) ? reader : xmlValidatingReader;
			case ValidationType.Schema:
				return new XmlSchemaValidatingReader(reader, settings);
			}
		}

		/// <summary>Releases the unmanaged resources used by the <see cref="T:System.Xml.XmlReader" /> and optionally releases the managed resources. </summary>
		/// <param name="disposing">true to release both managed and unmanaged resources; false to release only unmanaged resources.</param>
		protected virtual void Dispose(bool disposing)
		{
			if (ReadState != ReadState.Closed)
			{
				Close();
			}
		}

		/// <summary>When overridden in a derived class, gets the value of the attribute with the specified index.</summary>
		/// <returns>The value of the specified attribute. This method does not move the reader.</returns>
		/// <param name="i">The index of the attribute. The index is zero-based. (The first attribute has index 0.) </param>
		public abstract string GetAttribute(int i);

		/// <summary>When overridden in a derived class, gets the value of the attribute with the specified <see cref="P:System.Xml.XmlReader.Name" />.</summary>
		/// <returns>The value of the specified attribute. If the attribute is not found, null is returned.</returns>
		/// <param name="name">The qualified name of the attribute. </param>
		public abstract string GetAttribute(string name);

		/// <summary>When overridden in a derived class, gets the value of the attribute with the specified <see cref="P:System.Xml.XmlReader.LocalName" /> and <see cref="P:System.Xml.XmlReader.NamespaceURI" />.</summary>
		/// <returns>The value of the specified attribute. If the attribute is not found, null is returned. This method does not move the reader.</returns>
		/// <param name="name">The local name of the attribute. </param>
		/// <param name="namespaceURI">The namespace URI of the attribute. </param>
		public abstract string GetAttribute(string localName, string namespaceName);

		/// <summary>Gets a value indicating whether the string argument is a valid XML name.</summary>
		/// <returns>true if the name is valid; otherwise, false.</returns>
		/// <param name="str">The name to validate. </param>
		public static bool IsName(string s)
		{
			return s != null && XmlChar.IsName(s);
		}

		/// <summary>Gets a value indicating whether or not the string argument is a valid XML name token.</summary>
		/// <returns>true if it is a valid name token; otherwise false.</returns>
		/// <param name="str">The name token to validate. </param>
		public static bool IsNameToken(string s)
		{
			return s != null && XmlChar.IsNmToken(s);
		}

		/// <summary>Calls <see cref="M:System.Xml.XmlReader.MoveToContent" /> and tests if the current content node is a start tag or empty element tag.</summary>
		/// <returns>true if <see cref="M:System.Xml.XmlReader.MoveToContent" /> finds a start tag or empty element tag; false if a node type other than XmlNodeType.Element was found.</returns>
		/// <exception cref="T:System.Xml.XmlException">Incorrect XML is encountered in the input stream. </exception>
		public virtual bool IsStartElement()
		{
			return MoveToContent() == XmlNodeType.Element;
		}

		/// <summary>Calls <see cref="M:System.Xml.XmlReader.MoveToContent" /> and tests if the current content node is a start tag or empty element tag and if the <see cref="P:System.Xml.XmlReader.Name" /> property of the element found matches the given argument.</summary>
		/// <returns>true if the resulting node is an element and the Name property matches the specified string. false if a node type other than XmlNodeType.Element was found or if the element Name property does not match the specified string.</returns>
		/// <param name="name">The string matched against the Name property of the element found. </param>
		/// <exception cref="T:System.Xml.XmlException">Incorrect XML is encountered in the input stream. </exception>
		public virtual bool IsStartElement(string name)
		{
			if (!IsStartElement())
			{
				return false;
			}
			return Name == name;
		}

		/// <summary>Calls <see cref="M:System.Xml.XmlReader.MoveToContent" /> and tests if the current content node is a start tag or empty element tag and if the <see cref="P:System.Xml.XmlReader.LocalName" /> and <see cref="P:System.Xml.XmlReader.NamespaceURI" /> properties of the element found match the given strings.</summary>
		/// <returns>true if the resulting node is an element. false if a node type other than XmlNodeType.Element was found or if the LocalName and NamespaceURI properties of the element do not match the specified strings.</returns>
		/// <param name="localname">The string to match against the LocalName property of the element found. </param>
		/// <param name="ns">The string to match against the NamespaceURI property of the element found. </param>
		/// <exception cref="T:System.Xml.XmlException">Incorrect XML is encountered in the input stream. </exception>
		public virtual bool IsStartElement(string localName, string namespaceName)
		{
			if (!IsStartElement())
			{
				return false;
			}
			return LocalName == localName && NamespaceURI == namespaceName;
		}

		/// <summary>When overridden in a derived class, resolves a namespace prefix in the current element's scope.</summary>
		/// <returns>The namespace URI to which the prefix maps or null if no matching prefix is found.</returns>
		/// <param name="prefix">The prefix whose namespace URI you want to resolve. To match the default namespace, pass an empty string. </param>
		public abstract string LookupNamespace(string prefix);

		/// <summary>When overridden in a derived class, moves to the attribute with the specified index.</summary>
		/// <param name="i">The index of the attribute. </param>
		public virtual void MoveToAttribute(int i)
		{
			if (i >= AttributeCount)
			{
				throw new ArgumentOutOfRangeException();
			}
			MoveToFirstAttribute();
			for (int j = 0; j < i; j++)
			{
				MoveToNextAttribute();
			}
		}

		/// <summary>When overridden in a derived class, moves to the attribute with the specified <see cref="P:System.Xml.XmlReader.Name" />.</summary>
		/// <returns>true if the attribute is found; otherwise, false. If false, the reader's position does not change.</returns>
		/// <param name="name">The qualified name of the attribute. </param>
		public abstract bool MoveToAttribute(string name);

		/// <summary>When overridden in a derived class, moves to the attribute with the specified <see cref="P:System.Xml.XmlReader.LocalName" /> and <see cref="P:System.Xml.XmlReader.NamespaceURI" />.</summary>
		/// <returns>true if the attribute is found; otherwise, false. If false, the reader's position does not change.</returns>
		/// <param name="name">The local name of the attribute. </param>
		/// <param name="ns">The namespace URI of the attribute. </param>
		public abstract bool MoveToAttribute(string localName, string namespaceName);

		private bool IsContent(XmlNodeType nodeType)
		{
			switch (nodeType)
			{
			case XmlNodeType.Text:
				return true;
			case XmlNodeType.CDATA:
				return true;
			case XmlNodeType.Element:
				return true;
			case XmlNodeType.EndElement:
				return true;
			case XmlNodeType.EntityReference:
				return true;
			case XmlNodeType.EndEntity:
				return true;
			default:
				return false;
			}
		}

		/// <summary>Checks whether the current node is a content (non-white space text, CDATA, Element, EndElement, EntityReference, or EndEntity) node. If the node is not a content node, the reader skips ahead to the next content node or end of file. It skips over nodes of the following type: ProcessingInstruction, DocumentType, Comment, Whitespace, or SignificantWhitespace.</summary>
		/// <returns>The <see cref="P:System.Xml.XmlReader.NodeType" /> of the current node found by the method or XmlNodeType.None if the reader has reached the end of the input stream.</returns>
		/// <exception cref="T:System.Xml.XmlException">Incorrect XML encountered in the input stream. </exception>
		public virtual XmlNodeType MoveToContent()
		{
			ReadState readState = ReadState;
			if (readState != 0 && readState != ReadState.Interactive)
			{
				return NodeType;
			}
			if (NodeType == XmlNodeType.Attribute)
			{
				MoveToElement();
			}
			do
			{
				if (IsContent(NodeType))
				{
					return NodeType;
				}
				Read();
			}
			while (!EOF);
			return XmlNodeType.None;
		}

		/// <summary>When overridden in a derived class, moves to the element that contains the current attribute node.</summary>
		/// <returns>true if the reader is positioned on an attribute (the reader moves to the element that owns the attribute); false if the reader is not positioned on an attribute (the position of the reader does not change).</returns>
		public abstract bool MoveToElement();

		/// <summary>When overridden in a derived class, moves to the first attribute.</summary>
		/// <returns>true if an attribute exists (the reader moves to the first attribute); otherwise, false (the position of the reader does not change).</returns>
		public abstract bool MoveToFirstAttribute();

		/// <summary>When overridden in a derived class, moves to the next attribute.</summary>
		/// <returns>true if there is a next attribute; false if there are no more attributes.</returns>
		public abstract bool MoveToNextAttribute();

		/// <summary>When overridden in a derived class, reads the next node from the stream.</summary>
		/// <returns>true if the next node was read successfully; false if there are no more nodes to read.</returns>
		/// <exception cref="T:System.Xml.XmlException">An error occurred while parsing the XML. </exception>
		public abstract bool Read();

		/// <summary>When overridden in a derived class, parses the attribute value into one or more Text, EntityReference, or EndEntity nodes.</summary>
		/// <returns>true if there are nodes to return.false if the reader is not positioned on an attribute node when the initial call is made or if all the attribute values have been read.An empty attribute, such as, misc="", returns true with a single node with a value of String.Empty.</returns>
		public abstract bool ReadAttributeValue();

		/// <summary>Reads a text-only element.</summary>
		/// <returns>The text contained in the element that was read. An empty string if the element is empty (&lt;item&gt;&lt;/item&gt; or &lt;item/&gt;).</returns>
		/// <exception cref="T:System.Xml.XmlException">The next content node is not a start tag; or the element found does not contain a simple text value. </exception>
		public virtual string ReadElementString()
		{
			if (MoveToContent() != XmlNodeType.Element)
			{
				string message = $"'{NodeType.ToString()}' is an invalid node type.";
				throw XmlError(message);
			}
			string result = string.Empty;
			if (!IsEmptyElement)
			{
				Read();
				result = ReadString();
				if (NodeType != XmlNodeType.EndElement)
				{
					string message2 = $"'{NodeType.ToString()}' is an invalid node type.";
					throw XmlError(message2);
				}
			}
			Read();
			return result;
		}

		/// <summary>Checks that the <see cref="P:System.Xml.XmlReader.Name" /> property of the element found matches the given string before reading a text-only element.</summary>
		/// <returns>The text contained in the element that was read. An empty string if the element is empty (&lt;item&gt;&lt;/item&gt; or &lt;item/&gt;).</returns>
		/// <param name="name">The name to check. </param>
		/// <exception cref="T:System.Xml.XmlException">If the next content node is not a start tag; if the element Name does not match the given argument; or if the element found does not contain a simple text value. </exception>
		public virtual string ReadElementString(string name)
		{
			if (MoveToContent() != XmlNodeType.Element)
			{
				string message = $"'{NodeType.ToString()}' is an invalid node type.";
				throw XmlError(message);
			}
			if (name != Name)
			{
				string message2 = $"The {Name} tag from namespace {NamespaceURI} is expected.";
				throw XmlError(message2);
			}
			string result = string.Empty;
			if (!IsEmptyElement)
			{
				Read();
				result = ReadString();
				if (NodeType != XmlNodeType.EndElement)
				{
					string message3 = $"'{NodeType.ToString()}' is an invalid node type.";
					throw XmlError(message3);
				}
			}
			Read();
			return result;
		}

		/// <summary>Checks that the <see cref="P:System.Xml.XmlReader.LocalName" /> and <see cref="P:System.Xml.XmlReader.NamespaceURI" /> properties of the element found matches the given strings before reading a text-only element.</summary>
		/// <returns>The text contained in the element that was read. An empty string if the element is empty (&lt;item&gt;&lt;/item&gt; or &lt;item/&gt;).</returns>
		/// <param name="localname">The local name to check. </param>
		/// <param name="ns">The namespace URI to check. </param>
		/// <exception cref="T:System.Xml.XmlException">If the next content node is not a start tag; if the element LocalName or NamespaceURI do not match the given arguments; or if the element found does not contain a simple text value. </exception>
		public virtual string ReadElementString(string localName, string namespaceName)
		{
			if (MoveToContent() != XmlNodeType.Element)
			{
				string message = $"'{NodeType.ToString()}' is an invalid node type.";
				throw XmlError(message);
			}
			if (localName != LocalName || NamespaceURI != namespaceName)
			{
				string message2 = $"The {LocalName} tag from namespace {NamespaceURI} is expected.";
				throw XmlError(message2);
			}
			string result = string.Empty;
			if (!IsEmptyElement)
			{
				Read();
				result = ReadString();
				if (NodeType != XmlNodeType.EndElement)
				{
					string message3 = $"'{NodeType.ToString()}' is an invalid node type.";
					throw XmlError(message3);
				}
			}
			Read();
			return result;
		}

		/// <summary>Checks that the current content node is an end tag and advances the reader to the next node.</summary>
		/// <exception cref="T:System.Xml.XmlException">The current node is not an end tag or if incorrect XML is encountered in the input stream. </exception>
		public virtual void ReadEndElement()
		{
			if (MoveToContent() != XmlNodeType.EndElement)
			{
				string message = $"'{NodeType.ToString()}' is an invalid node type.";
				throw XmlError(message);
			}
			Read();
		}

		/// <summary>When overridden in a derived class, reads all the content, including markup, as a string.</summary>
		/// <returns>All the XML content, including markup, in the current node. If the current node has no children, an empty string is returned.If the current node is neither an element nor attribute, an empty string is returned.</returns>
		/// <exception cref="T:System.Xml.XmlException">The XML was not well-formed, or an error occurred while parsing the XML. </exception>
		public virtual string ReadInnerXml()
		{
			if (ReadState != ReadState.Interactive || NodeType == XmlNodeType.EndElement)
			{
				return string.Empty;
			}
			if (IsEmptyElement)
			{
				Read();
				return string.Empty;
			}
			StringWriter stringWriter = new StringWriter();
			XmlTextWriter xmlTextWriter = new XmlTextWriter(stringWriter);
			if (NodeType == XmlNodeType.Element)
			{
				int depth = Depth;
				Read();
				while (depth < Depth)
				{
					if (ReadState != ReadState.Interactive)
					{
						throw XmlError("Unexpected end of the XML reader.");
					}
					xmlTextWriter.WriteNode(this, defattr: false);
				}
				Read();
			}
			else
			{
				xmlTextWriter.WriteNode(this, defattr: false);
			}
			return stringWriter.ToString();
		}

		/// <summary>When overridden in a derived class, reads the content, including markup, representing this node and all its children.</summary>
		/// <returns>If the reader is positioned on an element or an attribute node, this method returns all the XML content, including markup, of the current node and all its children; otherwise, it returns an empty string.</returns>
		/// <exception cref="T:System.Xml.XmlException">The XML was not well-formed, or an error occurred while parsing the XML. </exception>
		public virtual string ReadOuterXml()
		{
			if (ReadState != ReadState.Interactive || NodeType == XmlNodeType.EndElement)
			{
				return string.Empty;
			}
			XmlNodeType nodeType = NodeType;
			if (nodeType == XmlNodeType.Element || nodeType == XmlNodeType.Attribute)
			{
				StringWriter stringWriter = new StringWriter();
				XmlTextWriter xmlTextWriter = new XmlTextWriter(stringWriter);
				xmlTextWriter.WriteNode(this, defattr: false);
				return stringWriter.ToString();
			}
			Skip();
			return string.Empty;
		}

		/// <summary>Checks that the current node is an element and advances the reader to the next node.</summary>
		/// <exception cref="T:System.Xml.XmlException">
		///   <see cref="M:System.Xml.XmlReader.IsStartElement" /> returns false. </exception>
		public virtual void ReadStartElement()
		{
			if (MoveToContent() != XmlNodeType.Element)
			{
				string message = $"'{NodeType.ToString()}' is an invalid node type.";
				throw XmlError(message);
			}
			Read();
		}

		/// <summary>Checks that the current content node is an element with the given <see cref="P:System.Xml.XmlReader.Name" /> and advances the reader to the next node.</summary>
		/// <param name="name">The qualified name of the element. </param>
		/// <exception cref="T:System.Xml.XmlException">
		///   <see cref="M:System.Xml.XmlReader.IsStartElement" /> returns false or if the <see cref="P:System.Xml.XmlReader.Name" /> of the element does not match the given <paramref name="name" />. </exception>
		public virtual void ReadStartElement(string name)
		{
			if (MoveToContent() != XmlNodeType.Element)
			{
				string message = $"'{NodeType.ToString()}' is an invalid node type.";
				throw XmlError(message);
			}
			if (name != Name)
			{
				string message2 = $"The {Name} tag from namespace {NamespaceURI} is expected.";
				throw XmlError(message2);
			}
			Read();
		}

		/// <summary>Checks that the current content node is an element with the given <see cref="P:System.Xml.XmlReader.LocalName" /> and <see cref="P:System.Xml.XmlReader.NamespaceURI" /> and advances the reader to the next node.</summary>
		/// <param name="localname">The local name of the element. </param>
		/// <param name="ns">The namespace URI of the element. </param>
		/// <exception cref="T:System.Xml.XmlException">
		///   <see cref="M:System.Xml.XmlReader.IsStartElement" /> returns false, or the <see cref="P:System.Xml.XmlReader.LocalName" /> and <see cref="P:System.Xml.XmlReader.NamespaceURI" /> properties of the element found do not match the given arguments. </exception>
		public virtual void ReadStartElement(string localName, string namespaceName)
		{
			if (MoveToContent() != XmlNodeType.Element)
			{
				string message = $"'{NodeType.ToString()}' is an invalid node type.";
				throw XmlError(message);
			}
			if (localName != LocalName || NamespaceURI != namespaceName)
			{
				string message2 = $"Expecting {localName} tag from namespace {namespaceName}, got {LocalName} and {NamespaceURI} instead";
				throw XmlError(message2);
			}
			Read();
		}

		/// <summary>When overridden in a derived class, reads the contents of an element or text node as a string.</summary>
		/// <returns>The contents of the element or an empty string.</returns>
		/// <exception cref="T:System.Xml.XmlException">An error occurred while parsing the XML. </exception>
		public virtual string ReadString()
		{
			if (readStringBuffer == null)
			{
				readStringBuffer = new StringBuilder();
			}
			readStringBuffer.Length = 0;
			MoveToElement();
			switch (NodeType)
			{
			default:
				return string.Empty;
			case XmlNodeType.Element:
				if (IsEmptyElement)
				{
					return string.Empty;
				}
				while (true)
				{
					Read();
					XmlNodeType nodeType = NodeType;
					if (nodeType != XmlNodeType.Text && nodeType != XmlNodeType.CDATA && nodeType != XmlNodeType.Whitespace && nodeType != XmlNodeType.SignificantWhitespace)
					{
						break;
					}
					readStringBuffer.Append(Value);
				}
				break;
			case XmlNodeType.Text:
			case XmlNodeType.CDATA:
			case XmlNodeType.Whitespace:
			case XmlNodeType.SignificantWhitespace:
				while (true)
				{
					XmlNodeType nodeType = NodeType;
					if (nodeType != XmlNodeType.Text && nodeType != XmlNodeType.CDATA && nodeType != XmlNodeType.Whitespace && nodeType != XmlNodeType.SignificantWhitespace)
					{
						break;
					}
					readStringBuffer.Append(Value);
					Read();
				}
				break;
			}
			string result = readStringBuffer.ToString();
			readStringBuffer.Length = 0;
			return result;
		}

		/// <summary>Advances the <see cref="T:System.Xml.XmlReader" /> to the next descendant element with the specified qualified name.</summary>
		/// <returns>true if a matching descendant element is found; otherwise false. If a matching child element is not found, the <see cref="T:System.Xml.XmlReader" /> is positioned on the end tag (<see cref="P:System.Xml.XmlReader.NodeType" /> is XmlNodeType.EndElement) of the element.If the <see cref="T:System.Xml.XmlReader" /> is not positioned on an element when <see cref="M:System.Xml.XmlReader.ReadToDescendant(System.String)" /> was called, this method returns false and the position of the <see cref="T:System.Xml.XmlReader" /> is not changed.</returns>
		/// <param name="name">The qualified name of the element you wish to move to.</param>
		public virtual bool ReadToDescendant(string name)
		{
			if (ReadState == ReadState.Initial)
			{
				MoveToContent();
				if (IsStartElement(name))
				{
					return true;
				}
			}
			if (NodeType != XmlNodeType.Element || IsEmptyElement)
			{
				return false;
			}
			int depth = Depth;
			Read();
			while (depth < Depth)
			{
				if (NodeType == XmlNodeType.Element && name == Name)
				{
					return true;
				}
				Read();
			}
			return false;
		}

		/// <summary>Advances the <see cref="T:System.Xml.XmlReader" /> to the next descendant element with the specified local name and namespace URI.</summary>
		/// <returns>true if a matching descendant element is found; otherwise false. If a matching child element is not found, the <see cref="T:System.Xml.XmlReader" /> is positioned on the end tag (<see cref="P:System.Xml.XmlReader.NodeType" /> is XmlNodeType.EndElement) of the element.If the <see cref="T:System.Xml.XmlReader" /> is not positioned on an element when <see cref="M:System.Xml.XmlReader.ReadToDescendant(System.String,System.String)" /> was called, this method returns false and the position of the <see cref="T:System.Xml.XmlReader" /> is not changed.</returns>
		/// <param name="localName">The local name of the element you wish to move to.</param>
		/// <param name="namespaceURI">The namespace URI of the element you wish to move to.</param>
		public virtual bool ReadToDescendant(string localName, string namespaceURI)
		{
			if (ReadState == ReadState.Initial)
			{
				MoveToContent();
				if (IsStartElement(localName, namespaceURI))
				{
					return true;
				}
			}
			if (NodeType != XmlNodeType.Element || IsEmptyElement)
			{
				return false;
			}
			int depth = Depth;
			Read();
			while (depth < Depth)
			{
				if (NodeType == XmlNodeType.Element && localName == LocalName && namespaceURI == NamespaceURI)
				{
					return true;
				}
				Read();
			}
			return false;
		}

		/// <summary>Reads until an element with the specified qualified name is found.</summary>
		/// <returns>true if a matching element is found; otherwise false and the <see cref="T:System.Xml.XmlReader" /> is in an end of file state.</returns>
		/// <param name="name">The qualified name of the element.</param>
		public virtual bool ReadToFollowing(string name)
		{
			while (Read())
			{
				if (NodeType == XmlNodeType.Element && name == Name)
				{
					return true;
				}
			}
			return false;
		}

		/// <summary>Reads until an element with the specified local name and namespace URI is found.</summary>
		/// <returns>true if a matching element is found; otherwise false and the <see cref="T:System.Xml.XmlReader" /> is in an end of file state.</returns>
		/// <param name="localName">The local name of the element.</param>
		/// <param name="namespaceURI">The namespace URI of the element.</param>
		public virtual bool ReadToFollowing(string localName, string namespaceURI)
		{
			while (Read())
			{
				if (NodeType == XmlNodeType.Element && localName == LocalName && namespaceURI == NamespaceURI)
				{
					return true;
				}
			}
			return false;
		}

		/// <summary>Advances the XmlReader to the next sibling element with the specified qualified name.</summary>
		/// <returns>true if a matching sibling element is found; otherwise false. If a matching sibling element is not found, the XmlReader is positioned on the end tag (<see cref="P:System.Xml.XmlReader.NodeType" /> is XmlNodeType.EndElement) of the parent element.</returns>
		/// <param name="name">The qualified name of the sibling element you wish to move to.</param>
		public virtual bool ReadToNextSibling(string name)
		{
			if (ReadState != ReadState.Interactive)
			{
				return false;
			}
			int depth = Depth;
			Skip();
			while (!EOF && depth <= Depth)
			{
				if (NodeType == XmlNodeType.Element && name == Name)
				{
					return true;
				}
				Skip();
			}
			return false;
		}

		/// <summary>Advances the XmlReader to the next sibling element with the specified local name and namespace URI.</summary>
		/// <returns>true if a matching sibling element is found; otherwise false. If a matching sibling element is not found, the XmlReader is positioned on the end tag (<see cref="P:System.Xml.XmlReader.NodeType" /> is XmlNodeType.EndElement) of the parent element.</returns>
		/// <param name="localName">The local name of the sibling element you wish to move to.</param>
		/// <param name="namespaceURI">The namespace URI of the sibling element you wish to move to</param>
		public virtual bool ReadToNextSibling(string localName, string namespaceURI)
		{
			if (ReadState != ReadState.Interactive)
			{
				return false;
			}
			int depth = Depth;
			Skip();
			while (!EOF && depth <= Depth)
			{
				if (NodeType == XmlNodeType.Element && localName == LocalName && namespaceURI == NamespaceURI)
				{
					return true;
				}
				Skip();
			}
			return false;
		}

		/// <summary>Returns a new XmlReader instance that can be used to read the current node, and all its descendants.</summary>
		/// <returns>A new XmlReader instance set to ReadState.Initial. A call to the <see cref="M:System.Xml.XmlReader.Read" /> method positions the new XmlReader on the node that was current before the call to ReadSubtree method.</returns>
		/// <exception cref="T:System.InvalidOperationException">The XmlReader is not positioned on an element when this method is called.</exception>
		public virtual XmlReader ReadSubtree()
		{
			if (NodeType != XmlNodeType.Element)
			{
				throw new InvalidOperationException($"ReadSubtree() can be invoked only when the reader is positioned on an element. Current node is {NodeType}. {GetLocation()}");
			}
			return new SubtreeXmlReader(this);
		}

		private string ReadContentString()
		{
			if (NodeType == XmlNodeType.Attribute || (NodeType != XmlNodeType.Element && HasAttributes))
			{
				return Value;
			}
			return ReadContentString(isText: true);
		}

		private string ReadContentString(bool isText)
		{
			if (isText)
			{
				switch (NodeType)
				{
				case XmlNodeType.Element:
					throw new InvalidOperationException($"Node type {NodeType} is not supported in this operation.{GetLocation()}");
				default:
					return string.Empty;
				case XmlNodeType.Text:
				case XmlNodeType.CDATA:
				case XmlNodeType.Whitespace:
				case XmlNodeType.SignificantWhitespace:
					break;
				}
			}
			string text = string.Empty;
			do
			{
				switch (NodeType)
				{
				case XmlNodeType.Element:
					if (isText)
					{
						return text;
					}
					throw XmlError("Child element is not expected in this operation.");
				case XmlNodeType.EndElement:
					return text;
				case XmlNodeType.Text:
				case XmlNodeType.CDATA:
				case XmlNodeType.Whitespace:
				case XmlNodeType.SignificantWhitespace:
					text += Value;
					break;
				}
			}
			while (Read());
			throw XmlError("Unexpected end of document.");
		}

		private string GetLocation()
		{
			IXmlLineInfo xmlLineInfo = this as IXmlLineInfo;
			return (xmlLineInfo == null || !xmlLineInfo.HasLineInfo()) ? string.Empty : $" {BaseURI} (line {xmlLineInfo.LineNumber}, column {xmlLineInfo.LinePosition})";
		}

		/// <summary>Reads the current element and returns the contents as an <see cref="T:System.Object" />.</summary>
		/// <returns>A boxed common language runtime (CLR) object of the most appropriate type. The <see cref="P:System.Xml.XmlReader.ValueType" /> property determines the appropriate CLR type. If the content is typed as a list type, this method returns an array of boxed objects of the appropriate type.</returns>
		/// <exception cref="T:System.InvalidOperationException">The <see cref="T:System.Xml.XmlReader" /> is not positioned on an element.</exception>
		/// <exception cref="T:System.Xml.XmlException">The current element contains child elements.-or-The element content cannot be converted to the requested type</exception>
		/// <exception cref="T:System.ArgumentNullException">The method is called with null arguments.</exception>
		[MonoTODO]
		public virtual object ReadElementContentAsObject()
		{
			return ReadElementContentAs(ValueType, null);
		}

		/// <summary>Checks that the specified local name and namespace URI matches that of the current element, then reads the current element and returns the contents as an <see cref="T:System.Object" />.</summary>
		/// <returns>A boxed common language runtime (CLR) object of the most appropriate type. The <see cref="P:System.Xml.XmlReader.ValueType" /> property determines the appropriate CLR type. If the content is typed as a list type, this method returns an array of boxed objects of the appropriate type.</returns>
		/// <param name="localName">The local name of the element.</param>
		/// <param name="namespaceURI">The namespace URI of the element.</param>
		/// <exception cref="T:System.InvalidOperationException">The <see cref="T:System.Xml.XmlReader" /> is not positioned on an element.</exception>
		/// <exception cref="T:System.Xml.XmlException">The current element contains child elements.-or-The element content cannot be converted to the requested type.</exception>
		/// <exception cref="T:System.ArgumentNullException">The method is called with null arguments.</exception>
		/// <exception cref="T:System.ArgumentException">The specified local name and namespace URI do not match that of the current element being read.</exception>
		[MonoTODO]
		public virtual object ReadElementContentAsObject(string localName, string namespaceURI)
		{
			return ReadElementContentAs(ValueType, null, localName, namespaceURI);
		}

		/// <summary>Reads the text content at the current position as an <see cref="T:System.Object" />.</summary>
		/// <returns>The text content as the most appropriate common language runtime (CLR) object.</returns>
		/// <exception cref="T:System.InvalidCastException">The attempted cast is not valid.</exception>
		/// <exception cref="T:System.FormatException">The string format is not valid.</exception>
		[MonoTODO]
		public virtual object ReadContentAsObject()
		{
			return ReadContentAs(ValueType, null);
		}

		/// <summary>Reads the element content as the requested type.</summary>
		/// <returns>The element content converted to the requested typed object.</returns>
		/// <param name="returnType">The type of the value to be returned.With the release of the .NET Framework 3.5, the value of the <paramref name="returnType" /> parameter can now be the <see cref="T:System.DateTimeOffset" /> type.</param>
		/// <param name="namespaceResolver">An <see cref="T:System.Xml.IXmlNamespaceResolver" /> object that is used to resolve any namespace prefixes related to type conversion.</param>
		/// <exception cref="T:System.InvalidOperationException">The <see cref="T:System.Xml.XmlReader" /> is not positioned on an element.</exception>
		/// <exception cref="T:System.Xml.XmlException">The current element contains child elements.-or-The element content cannot be converted to the requested type.</exception>
		/// <exception cref="T:System.ArgumentNullException">The method is called with null arguments.</exception>
		public virtual object ReadElementContentAs(Type type, IXmlNamespaceResolver resolver)
		{
			bool isEmptyElement = IsEmptyElement;
			ReadStartElement();
			object result = ValueAs((!isEmptyElement) ? ReadContentString(isText: false) : string.Empty, type, resolver);
			if (!isEmptyElement)
			{
				ReadEndElement();
			}
			return result;
		}

		/// <summary>Checks that the specified local name and namespace URI matches that of the current element, then reads the element content as the requested type.</summary>
		/// <returns>The element content converted to the requested typed object.</returns>
		/// <param name="returnType">The type of the value to be returned.With the release of the .NET Framework 3.5, the value of the <paramref name="returnType" /> parameter can now be the <see cref="T:System.DateTimeOffset" /> type.</param>
		/// <param name="namespaceResolver">An <see cref="T:System.Xml.IXmlNamespaceResolver" /> object that is used to resolve any namespace prefixes related to type conversion.</param>
		/// <param name="localName">The local name of the element.</param>
		/// <param name="namespaceURI">The namespace URI of the element.</param>
		/// <exception cref="T:System.InvalidOperationException">The <see cref="T:System.Xml.XmlReader" /> is not positioned on an element.</exception>
		/// <exception cref="T:System.Xml.XmlException">The current element contains child elements.-or-The element content cannot be converted to the requested type.</exception>
		/// <exception cref="T:System.ArgumentNullException">The method is called with null arguments.</exception>
		/// <exception cref="T:System.ArgumentException">The specified local name and namespace URI do not match that of the current element being read.</exception>
		public virtual object ReadElementContentAs(Type type, IXmlNamespaceResolver resolver, string localName, string namespaceURI)
		{
			ReadStartElement(localName, namespaceURI);
			object result = ReadContentAs(type, resolver);
			ReadEndElement();
			return result;
		}

		/// <summary>Reads the content as an object of the type specified.</summary>
		/// <returns>The concatenated text content or attribute value converted to the requested type.</returns>
		/// <param name="returnType">The type of the value to be returned.With the release of the .NET Framework 3.5, the value of the <paramref name="returnType" /> parameter can now be the <see cref="T:System.DateTimeOffset" /> type.</param>
		/// <param name="namespaceResolver">An <see cref="T:System.Xml.IXmlNamespaceResolver" /> object that is used to resolve any namespace prefixes related to type conversion. For example, this can be used when converting an <see cref="T:System.Xml.XmlQualifiedName" /> object to an xs:string.This value can be null.</param>
		/// <exception cref="T:System.FormatException">The content is not in the correct format for the target type.</exception>
		/// <exception cref="T:System.InvalidCastException">The attempted cast is not valid.</exception>
		/// <exception cref="T:System.ArgumentNullException">The <paramref name="returnType" /> value is null.</exception>
		/// <exception cref="T:System.InvalidOperationException">The current node is not a supported node type. See the table below for details.</exception>
		public virtual object ReadContentAs(Type type, IXmlNamespaceResolver resolver)
		{
			return ValueAs(ReadContentString(), type, resolver);
		}

		private object ValueAs(string text, Type type, IXmlNamespaceResolver resolver)
		{
			//Discarded unreachable code: IL_016a
			try
			{
				if (type == typeof(object))
				{
					return text;
				}
				if (type == typeof(XmlQualifiedName))
				{
					if (resolver != null)
					{
						return XmlQualifiedName.Parse(text, resolver);
					}
					return XmlQualifiedName.Parse(text, this);
				}
				if (type == typeof(DateTimeOffset))
				{
					return XmlConvert.ToDateTimeOffset(text);
				}
				switch (Type.GetTypeCode(type))
				{
				case TypeCode.Char:
				case TypeCode.SByte:
				case TypeCode.Byte:
				case TypeCode.Int16:
				case TypeCode.UInt16:
				case TypeCode.UInt32:
				case TypeCode.UInt64:
				case (TypeCode)17:
					break;
				case TypeCode.Boolean:
					return XQueryConvert.StringToBoolean(text);
				case TypeCode.DateTime:
					return XQueryConvert.StringToDateTime(text);
				case TypeCode.Decimal:
					return XQueryConvert.StringToDecimal(text);
				case TypeCode.Double:
					return XQueryConvert.StringToDouble(text);
				case TypeCode.Int32:
					return XQueryConvert.StringToInt(text);
				case TypeCode.Int64:
					return XQueryConvert.StringToInteger(text);
				case TypeCode.Single:
					return XQueryConvert.StringToFloat(text);
				case TypeCode.String:
					return text;
				}
			}
			catch (Exception ex)
			{
				throw XmlError($"Current text value '{text}' is not acceptable for specified type '{type}'. {((ex == null) ? string.Empty : ex.Message)}", ex);
			}
			throw new ArgumentException($"Specified type '{type}' is not supported.");
		}

		/// <summary>Reads the current element and returns the contents as a <see cref="T:System.Boolean" /> object.</summary>
		/// <returns>The element content as a <see cref="T:System.Boolean" /> object.</returns>
		/// <exception cref="T:System.InvalidOperationException">The <see cref="T:System.Xml.XmlReader" /> is not positioned on an element.</exception>
		/// <exception cref="T:System.Xml.XmlException">The current element contains child elements.-or-The element content cannot be converted to a <see cref="T:System.Boolean" /> object.</exception>
		/// <exception cref="T:System.ArgumentNullException">The method is called with null arguments.</exception>
		public virtual bool ReadElementContentAsBoolean()
		{
			//Discarded unreachable code: IL_0011, IL_0024
			try
			{
				return XQueryConvert.StringToBoolean(ReadElementContentAsString());
			}
			catch (FormatException innerException)
			{
				throw XmlError("Typed value is invalid.", innerException);
			}
		}

		/// <summary>Reads the current element and returns the contents as a <see cref="T:System.DateTime" /> object.</summary>
		/// <returns>The element content as a <see cref="T:System.DateTime" /> object.</returns>
		/// <exception cref="T:System.InvalidOperationException">The <see cref="T:System.Xml.XmlReader" /> is not positioned on an element.</exception>
		/// <exception cref="T:System.Xml.XmlException">The current element contains child elements.-or-The element content cannot be converted to a <see cref="T:System.DateTime" /> object.</exception>
		/// <exception cref="T:System.ArgumentNullException">The method is called with null arguments.</exception>
		public virtual DateTime ReadElementContentAsDateTime()
		{
			//Discarded unreachable code: IL_0011, IL_0024
			try
			{
				return XQueryConvert.StringToDateTime(ReadElementContentAsString());
			}
			catch (FormatException innerException)
			{
				throw XmlError("Typed value is invalid.", innerException);
			}
		}

		/// <summary>Reads the current element and returns the contents as a <see cref="T:System.Decimal" /> object.</summary>
		/// <returns>The element content as a <see cref="T:System.Decimal" /> object.</returns>
		/// <exception cref="T:System.InvalidOperationException">The <see cref="T:System.Xml.XmlReader" /> is not positioned on an element.</exception>
		/// <exception cref="T:System.Xml.XmlException">The current element contains child elements.-or-The element content cannot be converted to a <see cref="T:System.Decimal" />.</exception>
		/// <exception cref="T:System.ArgumentNullException">The method is called with null arguments.</exception>
		public virtual decimal ReadElementContentAsDecimal()
		{
			//Discarded unreachable code: IL_0011, IL_0024
			try
			{
				return XQueryConvert.StringToDecimal(ReadElementContentAsString());
			}
			catch (FormatException innerException)
			{
				throw XmlError("Typed value is invalid.", innerException);
			}
		}

		/// <summary>Reads the current element and returns the contents as a double-precision floating-point number.</summary>
		/// <returns>The element content as a double-precision floating-point number.</returns>
		/// <exception cref="T:System.InvalidOperationException">The <see cref="T:System.Xml.XmlReader" /> is not positioned on an element.</exception>
		/// <exception cref="T:System.Xml.XmlException">The current element contains child elements.-or-The element content cannot be converted to a double-precision floating-point number.</exception>
		/// <exception cref="T:System.ArgumentNullException">The method is called with null arguments.</exception>
		public virtual double ReadElementContentAsDouble()
		{
			//Discarded unreachable code: IL_0011, IL_0024
			try
			{
				return XQueryConvert.StringToDouble(ReadElementContentAsString());
			}
			catch (FormatException innerException)
			{
				throw XmlError("Typed value is invalid.", innerException);
			}
		}

		/// <summary>Reads the current element and returns the contents as single-precision floating-point number.</summary>
		/// <returns>The element content as a single-precision floating point number.</returns>
		/// <exception cref="T:System.InvalidOperationException">The <see cref="T:System.Xml.XmlReader" /> is not positioned on an element.</exception>
		/// <exception cref="T:System.Xml.XmlException">The current element contains child elements.-or-The element content cannot be converted to a single-precision floating-point number.</exception>
		/// <exception cref="T:System.ArgumentNullException">The method is called with null arguments.</exception>
		public virtual float ReadElementContentAsFloat()
		{
			//Discarded unreachable code: IL_0011, IL_0024
			try
			{
				return XQueryConvert.StringToFloat(ReadElementContentAsString());
			}
			catch (FormatException innerException)
			{
				throw XmlError("Typed value is invalid.", innerException);
			}
		}

		/// <summary>Reads the current element and returns the contents as a 32-bit signed integer.</summary>
		/// <returns>The element content as a 32-bit signed integer.</returns>
		/// <exception cref="T:System.InvalidOperationException">The <see cref="T:System.Xml.XmlReader" /> is not positioned on an element.</exception>
		/// <exception cref="T:System.Xml.XmlException">The current element contains child elements.-or-The element content cannot be converted to a 32-bit signed integer.</exception>
		/// <exception cref="T:System.ArgumentNullException">The method is called with null arguments.</exception>
		public virtual int ReadElementContentAsInt()
		{
			//Discarded unreachable code: IL_0011, IL_0024
			try
			{
				return XQueryConvert.StringToInt(ReadElementContentAsString());
			}
			catch (FormatException innerException)
			{
				throw XmlError("Typed value is invalid.", innerException);
			}
		}

		/// <summary>Reads the current element and returns the contents as a 64-bit signed integer.</summary>
		/// <returns>The element content as a 64-bit signed integer.</returns>
		/// <exception cref="T:System.InvalidOperationException">The <see cref="T:System.Xml.XmlReader" /> is not positioned on an element.</exception>
		/// <exception cref="T:System.Xml.XmlException">The current element contains child elements.-or-The element content cannot be converted to a 64-bit signed integer.</exception>
		/// <exception cref="T:System.ArgumentNullException">The method is called with null arguments.</exception>
		public virtual long ReadElementContentAsLong()
		{
			//Discarded unreachable code: IL_0011, IL_0024
			try
			{
				return XQueryConvert.StringToInteger(ReadElementContentAsString());
			}
			catch (FormatException innerException)
			{
				throw XmlError("Typed value is invalid.", innerException);
			}
		}

		/// <summary>Reads the current element and returns the contents as a <see cref="T:System.String" /> object.</summary>
		/// <returns>The element content as a <see cref="T:System.String" /> object.</returns>
		/// <exception cref="T:System.InvalidOperationException">The <see cref="T:System.Xml.XmlReader" /> is not positioned on an element.</exception>
		/// <exception cref="T:System.Xml.XmlException">The current element contains child elements.-or-The element content cannot be converted to a <see cref="T:System.String" /> object.</exception>
		/// <exception cref="T:System.ArgumentNullException">The method is called with null arguments.</exception>
		public virtual string ReadElementContentAsString()
		{
			bool isEmptyElement = IsEmptyElement;
			if (NodeType != XmlNodeType.Element)
			{
				throw new InvalidOperationException($"'{NodeType}' is an element node.");
			}
			ReadStartElement();
			if (isEmptyElement)
			{
				return string.Empty;
			}
			string result = ReadContentString(isText: false);
			ReadEndElement();
			return result;
		}

		/// <summary>Checks that the specified local name and namespace URI matches that of the current element, then reads the current element and returns the contents as a <see cref="T:System.Boolean" /> object.</summary>
		/// <returns>The element content as a <see cref="T:System.Boolean" /> object.</returns>
		/// <param name="localName">The local name of the element.</param>
		/// <param name="namespaceURI">The namespace URI of the element.</param>
		/// <exception cref="T:System.InvalidOperationException">The <see cref="T:System.Xml.XmlReader" /> is not positioned on an element.</exception>
		/// <exception cref="T:System.Xml.XmlException">The current element contains child elements.-or-The element content cannot be converted to the requested type.</exception>
		/// <exception cref="T:System.ArgumentNullException">The method is called with null arguments.</exception>
		/// <exception cref="T:System.ArgumentException">The specified local name and namespace URI do not match that of the current element being read.</exception>
		public virtual bool ReadElementContentAsBoolean(string localName, string namespaceURI)
		{
			//Discarded unreachable code: IL_0013, IL_0026
			try
			{
				return XQueryConvert.StringToBoolean(ReadElementContentAsString(localName, namespaceURI));
			}
			catch (FormatException innerException)
			{
				throw XmlError("Typed value is invalid.", innerException);
			}
		}

		/// <summary>Checks that the specified local name and namespace URI matches that of the current element, then reads the current element and returns the contents as a <see cref="T:System.DateTime" /> object.</summary>
		/// <returns>The element contents as a <see cref="T:System.DateTime" /> object.</returns>
		/// <param name="localName">The local name of the element.</param>
		/// <param name="namespaceURI">The namespace URI of the element.</param>
		/// <exception cref="T:System.InvalidOperationException">The <see cref="T:System.Xml.XmlReader" /> is not positioned on an element.</exception>
		/// <exception cref="T:System.Xml.XmlException">The current element contains child elements.-or-The element content cannot be converted to the requested type.</exception>
		/// <exception cref="T:System.ArgumentNullException">The method is called with null arguments.</exception>
		/// <exception cref="T:System.ArgumentException">The specified local name and namespace URI do not match that of the current element being read.</exception>
		public virtual DateTime ReadElementContentAsDateTime(string localName, string namespaceURI)
		{
			//Discarded unreachable code: IL_0013, IL_0026
			try
			{
				return XQueryConvert.StringToDateTime(ReadElementContentAsString(localName, namespaceURI));
			}
			catch (FormatException innerException)
			{
				throw XmlError("Typed value is invalid.", innerException);
			}
		}

		/// <summary>Checks that the specified local name and namespace URI matches that of the current element, then reads the current element and returns the contents as a <see cref="T:System.Decimal" /> object.</summary>
		/// <returns>The element content as a <see cref="T:System.Decimal" /> object.</returns>
		/// <param name="localName">The local name of the element.</param>
		/// <param name="namespaceURI">The namespace URI of the element.</param>
		/// <exception cref="T:System.InvalidOperationException">The <see cref="T:System.Xml.XmlReader" /> is not positioned on an element.</exception>
		/// <exception cref="T:System.Xml.XmlException">The current element contains child elements.-or-The element content cannot be converted to a <see cref="T:System.Decimal" />.</exception>
		/// <exception cref="T:System.ArgumentNullException">The method is called with null arguments.</exception>
		/// <exception cref="T:System.ArgumentException">The specified local name and namespace URI do not match that of the current element being read.</exception>
		public virtual decimal ReadElementContentAsDecimal(string localName, string namespaceURI)
		{
			//Discarded unreachable code: IL_0013, IL_0026
			try
			{
				return XQueryConvert.StringToDecimal(ReadElementContentAsString(localName, namespaceURI));
			}
			catch (FormatException innerException)
			{
				throw XmlError("Typed value is invalid.", innerException);
			}
		}

		/// <summary>Checks that the specified local name and namespace URI matches that of the current element, then reads the current element and returns the contents as a double-precision floating-point number.</summary>
		/// <returns>The element content as a double-precision floating-point number.</returns>
		/// <param name="localName">The local name of the element.</param>
		/// <param name="namespaceURI">The namespace URI of the element.</param>
		/// <exception cref="T:System.InvalidOperationException">The <see cref="T:System.Xml.XmlReader" /> is not positioned on an element.</exception>
		/// <exception cref="T:System.Xml.XmlException">The current element contains child elements.-or-The element content cannot be converted to the requested type.</exception>
		/// <exception cref="T:System.ArgumentNullException">The method is called with null arguments.</exception>
		/// <exception cref="T:System.ArgumentException">The specified local name and namespace URI do not match that of the current element being read.</exception>
		public virtual double ReadElementContentAsDouble(string localName, string namespaceURI)
		{
			//Discarded unreachable code: IL_0013, IL_0026
			try
			{
				return XQueryConvert.StringToDouble(ReadElementContentAsString(localName, namespaceURI));
			}
			catch (FormatException innerException)
			{
				throw XmlError("Typed value is invalid.", innerException);
			}
		}

		/// <summary>Checks that the specified local name and namespace URI matches that of the current element, then reads the current element and returns the contents as a single-precision floating-point number.</summary>
		/// <returns>The element content as a single-precision floating point number.</returns>
		/// <param name="localName">The local name of the element.</param>
		/// <param name="namespaceURI">The namespace URI of the element.</param>
		/// <exception cref="T:System.InvalidOperationException">The <see cref="T:System.Xml.XmlReader" /> is not positioned on an element.</exception>
		/// <exception cref="T:System.Xml.XmlException">The current element contains child elements.-or-The element content cannot be converted to a single-precision floating-point number.</exception>
		/// <exception cref="T:System.ArgumentNullException">The method is called with null arguments.</exception>
		/// <exception cref="T:System.ArgumentException">The specified local name and namespace URI do not match that of the current element being read.</exception>
		public virtual float ReadElementContentAsFloat(string localName, string namespaceURI)
		{
			//Discarded unreachable code: IL_0013, IL_0026
			try
			{
				return XQueryConvert.StringToFloat(ReadElementContentAsString(localName, namespaceURI));
			}
			catch (FormatException innerException)
			{
				throw XmlError("Typed value is invalid.", innerException);
			}
		}

		/// <summary>Checks that the specified local name and namespace URI matches that of the current element, then reads the current element and returns the contents as a 32-bit signed integer.</summary>
		/// <returns>The element content as a 32-bit signed integer.</returns>
		/// <param name="localName">The local name of the element.</param>
		/// <param name="namespaceURI">The namespace URI of the element.</param>
		/// <exception cref="T:System.InvalidOperationException">The <see cref="T:System.Xml.XmlReader" /> is not positioned on an element.</exception>
		/// <exception cref="T:System.Xml.XmlException">The current element contains child elements.-or-The element content cannot be converted to a 32-bit signed integer.</exception>
		/// <exception cref="T:System.ArgumentNullException">The method is called with null arguments.</exception>
		/// <exception cref="T:System.ArgumentException">The specified local name and namespace URI do not match that of the current element being read.</exception>
		public virtual int ReadElementContentAsInt(string localName, string namespaceURI)
		{
			//Discarded unreachable code: IL_0013, IL_0026
			try
			{
				return XQueryConvert.StringToInt(ReadElementContentAsString(localName, namespaceURI));
			}
			catch (FormatException innerException)
			{
				throw XmlError("Typed value is invalid.", innerException);
			}
		}

		/// <summary>Checks that the specified local name and namespace URI matches that of the current element, then reads the current element and returns the contents as a 64-bit signed integer.</summary>
		/// <returns>The element content as a 64-bit signed integer.</returns>
		/// <param name="localName">The local name of the element.</param>
		/// <param name="namespaceURI">The namespace URI of the element.</param>
		/// <exception cref="T:System.InvalidOperationException">The <see cref="T:System.Xml.XmlReader" /> is not positioned on an element.</exception>
		/// <exception cref="T:System.Xml.XmlException">The current element contains child elements.-or-The element content cannot be converted to a 64-bit signed integer.</exception>
		/// <exception cref="T:System.ArgumentNullException">The method is called with null arguments.</exception>
		/// <exception cref="T:System.ArgumentException">The specified local name and namespace URI do not match that of the current element being read.</exception>
		public virtual long ReadElementContentAsLong(string localName, string namespaceURI)
		{
			//Discarded unreachable code: IL_0013, IL_0026
			try
			{
				return XQueryConvert.StringToInteger(ReadElementContentAsString(localName, namespaceURI));
			}
			catch (FormatException innerException)
			{
				throw XmlError("Typed value is invalid.", innerException);
			}
		}

		/// <summary>Checks that the specified local name and namespace URI matches that of the current element, then reads the current element and returns the contents as a <see cref="T:System.String" /> object.</summary>
		/// <returns>The element content as a <see cref="T:System.String" /> object.</returns>
		/// <param name="localName">The local name of the element.</param>
		/// <param name="namespaceURI">The namespace URI of the element.</param>
		/// <exception cref="T:System.InvalidOperationException">The <see cref="T:System.Xml.XmlReader" /> is not positioned on an element.</exception>
		/// <exception cref="T:System.Xml.XmlException">The current element contains child elements.-or-The element content cannot be converted to a <see cref="T:System.String" /> object.</exception>
		/// <exception cref="T:System.ArgumentNullException">The method is called with null arguments.</exception>
		/// <exception cref="T:System.ArgumentException">The specified local name and namespace URI do not match that of the current element being read.</exception>
		public virtual string ReadElementContentAsString(string localName, string namespaceURI)
		{
			bool isEmptyElement = IsEmptyElement;
			if (NodeType != XmlNodeType.Element)
			{
				throw new InvalidOperationException($"'{NodeType}' is an element node.");
			}
			ReadStartElement(localName, namespaceURI);
			if (isEmptyElement)
			{
				return string.Empty;
			}
			string result = ReadContentString(isText: false);
			ReadEndElement();
			return result;
		}

		/// <summary>Reads the text content at the current position as a Boolean.</summary>
		/// <returns>The text content as a <see cref="T:System.Boolean" /> object.</returns>
		/// <exception cref="T:System.InvalidCastException">The attempted cast is not valid.</exception>
		/// <exception cref="T:System.FormatException">The string format is not valid.</exception>
		public virtual bool ReadContentAsBoolean()
		{
			//Discarded unreachable code: IL_0011, IL_0024
			try
			{
				return XQueryConvert.StringToBoolean(ReadContentString());
			}
			catch (FormatException innerException)
			{
				throw XmlError("Typed value is invalid.", innerException);
			}
		}

		/// <summary>Reads the text content at the current position as a <see cref="T:System.DateTime" /> object.</summary>
		/// <returns>The text content as a <see cref="T:System.DateTime" /> object.</returns>
		/// <exception cref="T:System.InvalidCastException">The attempted cast is not valid.</exception>
		/// <exception cref="T:System.FormatException">The string format is not valid.</exception>
		public virtual DateTime ReadContentAsDateTime()
		{
			//Discarded unreachable code: IL_0011, IL_0024
			try
			{
				return XQueryConvert.StringToDateTime(ReadContentString());
			}
			catch (FormatException innerException)
			{
				throw XmlError("Typed value is invalid.", innerException);
			}
		}

		/// <summary>Reads the text content at the current position as a <see cref="T:System.Decimal" /> object.</summary>
		/// <returns>The text content at the current position as a <see cref="T:System.Decimal" /> object.</returns>
		/// <exception cref="T:System.InvalidCastException">The attempted cast is not valid.</exception>
		/// <exception cref="T:System.FormatException">The string format is not valid.</exception>
		public virtual decimal ReadContentAsDecimal()
		{
			//Discarded unreachable code: IL_0011, IL_0024
			try
			{
				return XQueryConvert.StringToDecimal(ReadContentString());
			}
			catch (FormatException innerException)
			{
				throw XmlError("Typed value is invalid.", innerException);
			}
		}

		/// <summary>Reads the text content at the current position as a double-precision floating-point number.</summary>
		/// <returns>The text content as a double-precision floating-point number.</returns>
		/// <exception cref="T:System.InvalidCastException">The attempted cast is not valid.</exception>
		/// <exception cref="T:System.FormatException">The string format is not valid.</exception>
		public virtual double ReadContentAsDouble()
		{
			//Discarded unreachable code: IL_0011, IL_0024
			try
			{
				return XQueryConvert.StringToDouble(ReadContentString());
			}
			catch (FormatException innerException)
			{
				throw XmlError("Typed value is invalid.", innerException);
			}
		}

		/// <summary>Reads the text content at the current position as a single-precision floating point number.</summary>
		/// <returns>The text content at the current position as a single-precision floating point number.</returns>
		/// <exception cref="T:System.InvalidCastException">The attempted cast is not valid.</exception>
		/// <exception cref="T:System.FormatException">The string format is not valid.</exception>
		public virtual float ReadContentAsFloat()
		{
			//Discarded unreachable code: IL_0011, IL_0024
			try
			{
				return XQueryConvert.StringToFloat(ReadContentString());
			}
			catch (FormatException innerException)
			{
				throw XmlError("Typed value is invalid.", innerException);
			}
		}

		/// <summary>Reads the text content at the current position as a 32-bit signed integer.</summary>
		/// <returns>The text content as a 32-bit signed integer.</returns>
		/// <exception cref="T:System.InvalidCastException">The attempted cast is not valid.</exception>
		/// <exception cref="T:System.FormatException">The string format is not valid.</exception>
		public virtual int ReadContentAsInt()
		{
			//Discarded unreachable code: IL_0011, IL_0024
			try
			{
				return XQueryConvert.StringToInt(ReadContentString());
			}
			catch (FormatException innerException)
			{
				throw XmlError("Typed value is invalid.", innerException);
			}
		}

		/// <summary>Reads the text content at the current position as a 64-bit signed integer.</summary>
		/// <returns>The text content as a 64-bit signed integer.</returns>
		/// <exception cref="T:System.InvalidCastException">The attempted cast is not valid.</exception>
		/// <exception cref="T:System.FormatException">The string format is not valid.</exception>
		public virtual long ReadContentAsLong()
		{
			//Discarded unreachable code: IL_0011, IL_0024
			try
			{
				return XQueryConvert.StringToInteger(ReadContentString());
			}
			catch (FormatException innerException)
			{
				throw XmlError("Typed value is invalid.", innerException);
			}
		}

		/// <summary>Reads the text content at the current position as a <see cref="T:System.String" /> object.</summary>
		/// <returns>The text content as a <see cref="T:System.String" /> object.</returns>
		/// <exception cref="T:System.InvalidCastException">The attempted cast is not valid.</exception>
		/// <exception cref="T:System.FormatException">The string format is not valid.</exception>
		public virtual string ReadContentAsString()
		{
			return ReadContentString();
		}

		/// <summary>Reads the content and returns the Base64 decoded binary bytes.</summary>
		/// <returns>The number of bytes written to the buffer.</returns>
		/// <param name="buffer">The buffer into which to copy the resulting text. This value cannot be null.</param>
		/// <param name="index">The offset into the buffer where to start copying the result.</param>
		/// <param name="count">The maximum number of bytes to copy into the buffer. The actual number of bytes copied is returned from this method.</param>
		/// <exception cref="T:System.ArgumentNullException">The <paramref name="buffer" /> value is null.</exception>
		/// <exception cref="T:System.InvalidOperationException">
		///   <see cref="M:System.Xml.XmlReader.ReadContentAsBase64(System.Byte[],System.Int32,System.Int32)" />  is not supported on the current node.</exception>
		/// <exception cref="T:System.ArgumentOutOfRangeException">The index into the buffer or index + count is larger than the allocated buffer size.</exception>
		/// <exception cref="T:System.NotSupportedException">The <see cref="T:System.Xml.XmlReader" /> implementation does not support this method.</exception>
		public virtual int ReadContentAsBase64(byte[] buffer, int offset, int length)
		{
			CheckSupport();
			return binary.ReadContentAsBase64(buffer, offset, length);
		}

		/// <summary>Reads the content and returns the BinHex decoded binary bytes.</summary>
		/// <returns>The number of bytes written to the buffer.</returns>
		/// <param name="buffer">The buffer into which to copy the resulting text. This value cannot be null.</param>
		/// <param name="index">The offset into the buffer where to start copying the result.</param>
		/// <param name="count">The maximum number of bytes to copy into the buffer. The actual number of bytes copied is returned from this method.</param>
		/// <exception cref="T:System.ArgumentNullException">The <paramref name="buffer" /> value is null.</exception>
		/// <exception cref="T:System.InvalidOperationException">
		///   <see cref="M:System.Xml.XmlReader.ReadContentAsBinHex(System.Byte[],System.Int32,System.Int32)" /> is not supported on the current node.</exception>
		/// <exception cref="T:System.ArgumentOutOfRangeException">The index into the buffer or index + count is larger than the allocated buffer size.</exception>
		/// <exception cref="T:System.NotSupportedException">The <see cref="T:System.Xml.XmlReader" /> implementation does not support this method.</exception>
		public virtual int ReadContentAsBinHex(byte[] buffer, int offset, int length)
		{
			CheckSupport();
			return binary.ReadContentAsBinHex(buffer, offset, length);
		}

		/// <summary>Reads the element and decodes the Base64 content.</summary>
		/// <returns>The number of bytes written to the buffer.</returns>
		/// <param name="buffer">The buffer into which to copy the resulting text. This value cannot be null.</param>
		/// <param name="index">The offset into the buffer where to start copying the result.</param>
		/// <param name="count">The maximum number of bytes to copy into the buffer. The actual number of bytes copied is returned from this method.</param>
		/// <exception cref="T:System.ArgumentNullException">The <paramref name="buffer" /> value is null.</exception>
		/// <exception cref="T:System.InvalidOperationException">The current node is not an element node.</exception>
		/// <exception cref="T:System.ArgumentOutOfRangeException">The index into the buffer or index + count is larger than the allocated buffer size.</exception>
		/// <exception cref="T:System.NotSupportedException">The <see cref="T:System.Xml.XmlReader" /> implementation does not support this method.</exception>
		/// <exception cref="T:System.Xml.XmlException">The element contains mixed-content.</exception>
		/// <exception cref="T:System.FormatException">The content cannot be converted to the requested type.</exception>
		public virtual int ReadElementContentAsBase64(byte[] buffer, int offset, int length)
		{
			CheckSupport();
			return binary.ReadElementContentAsBase64(buffer, offset, length);
		}

		/// <summary>Reads the element and decodes the BinHex content.</summary>
		/// <returns>The number of bytes written to the buffer.</returns>
		/// <param name="buffer">The buffer into which to copy the resulting text. This value cannot be null.</param>
		/// <param name="index">The offset into the buffer where to start copying the result.</param>
		/// <param name="count">The maximum number of bytes to copy into the buffer. The actual number of bytes copied is returned from this method.</param>
		/// <exception cref="T:System.ArgumentNullException">The <paramref name="buffer" /> value is null.</exception>
		/// <exception cref="T:System.InvalidOperationException">The current node is not an element node.</exception>
		/// <exception cref="T:System.ArgumentOutOfRangeException">The index into the buffer or index + count is larger than the allocated buffer size.</exception>
		/// <exception cref="T:System.NotSupportedException">The <see cref="T:System.Xml.XmlReader" /> implementation does not support this method.</exception>
		/// <exception cref="T:System.Xml.XmlException">The element contains mixed-content.</exception>
		/// <exception cref="T:System.FormatException">The content cannot be converted to the requested type.</exception>
		public virtual int ReadElementContentAsBinHex(byte[] buffer, int offset, int length)
		{
			CheckSupport();
			return binary.ReadElementContentAsBinHex(buffer, offset, length);
		}

		private void CheckSupport()
		{
			if (!CanReadBinaryContent || !CanReadValueChunk)
			{
				throw new NotSupportedException();
			}
			if (binary == null)
			{
				binary = new XmlReaderBinarySupport(this);
			}
		}

		/// <summary>Reads large streams of text embedded in an XML document.</summary>
		/// <returns>The number of characters read into the buffer. The value zero is returned when there is no more text content.</returns>
		/// <param name="buffer">The array of characters that serves as the buffer to which the text contents are written. This value cannot be null.</param>
		/// <param name="index">The offset within the buffer where the <see cref="T:System.Xml.XmlReader" /> can start to copy the results.</param>
		/// <param name="count">The maximum number of characters to copy into the buffer. The actual number of characters copied is returned from this method.</param>
		/// <exception cref="T:System.InvalidOperationException">The current node does not have a value (<see cref="P:System.Xml.XmlReader.HasValue" /> is false).</exception>
		/// <exception cref="T:System.ArgumentNullException">The <paramref name="buffer" /> value is null.</exception>
		/// <exception cref="T:System.ArgumentOutOfRangeException">The index into the buffer, or index + count is larger than the allocated buffer size.</exception>
		/// <exception cref="T:System.NotSupportedException">The <see cref="T:System.Xml.XmlReader" /> implementation does not support this method.</exception>
		/// <exception cref="T:System.Xml.XmlException">The XML data is not well-formed.</exception>
		public virtual int ReadValueChunk(char[] buffer, int offset, int length)
		{
			if (!CanReadValueChunk)
			{
				throw new NotSupportedException();
			}
			if (binary == null)
			{
				binary = new XmlReaderBinarySupport(this);
			}
			return binary.ReadValueChunk(buffer, offset, length);
		}

		/// <summary>When overridden in a derived class, resolves the entity reference for EntityReference nodes.</summary>
		/// <exception cref="T:System.InvalidOperationException">The reader is not positioned on an EntityReference node; this implementation of the reader cannot resolve entities (<see cref="P:System.Xml.XmlReader.CanResolveEntity" /> returns false). </exception>
		public abstract void ResolveEntity();

		/// <summary>Skips the children of the current node.</summary>
		public virtual void Skip()
		{
			if (ReadState != ReadState.Interactive)
			{
				return;
			}
			MoveToElement();
			if (NodeType != XmlNodeType.Element || IsEmptyElement)
			{
				Read();
				return;
			}
			int depth = Depth;
			while (Read() && depth < Depth)
			{
			}
			if (NodeType == XmlNodeType.EndElement)
			{
				Read();
			}
		}

		private XmlException XmlError(string message)
		{
			return new XmlException(this as IXmlLineInfo, BaseURI, message);
		}

		private XmlException XmlError(string message, Exception innerException)
		{
			return new XmlException(this as IXmlLineInfo, BaseURI, message);
		}
	}
}
