using Mono.Xml;
using System.Collections;
using System.Runtime.CompilerServices;

namespace System.Xml
{
	/// <summary>Represents a collection of attributes that can be accessed by name or index.</summary>
	public sealed class XmlAttributeCollection : XmlNamedNodeMap, IEnumerable, ICollection
	{
		private XmlElement ownerElement;

		private XmlDocument ownerDocument;

		/// <summary>For a description of this member, see <see cref="P:System.Xml.XmlAttributeCollection.System.Collections.ICollection.IsSynchronized" />.</summary>
		/// <returns />
		bool ICollection.IsSynchronized => false;

		/// <summary>For a description of this member, see <see cref="P:System.Xml.XmlAttributeCollection.System.Collections.ICollection.SyncRoot" />.</summary>
		/// <returns />
		object ICollection.SyncRoot => this;

		private bool IsReadOnly => ownerElement.IsReadOnly;

		/// <summary>Gets the attribute with the specified name.</summary>
		/// <returns>The <see cref="T:System.Xml.XmlAttribute" /> with the specified name. If the attribute does not exist, this property returns null.</returns>
		/// <param name="name">The qualified name of the attribute. </param>
		[IndexerName("ItemOf")]
		public XmlAttribute this[string name] => (XmlAttribute)GetNamedItem(name);

		/// <summary>Gets the attribute with the specified index.</summary>
		/// <returns>The <see cref="T:System.Xml.XmlAttribute" /> at the specified index.</returns>
		/// <param name="i">The index of the attribute. </param>
		/// <exception cref="T:System.IndexOutOfRangeException">The index being passed in is out of range. </exception>
		[IndexerName("ItemOf")]
		public XmlAttribute this[int i] => (XmlAttribute)base.Nodes[i];

		/// <summary>Gets the attribute with the specified local name and namespace Uniform Resource Identifier (URI).</summary>
		/// <returns>The <see cref="T:System.Xml.XmlAttribute" /> with the specified local name and namespace URI. If the attribute does not exist, this property returns null.</returns>
		/// <param name="localName">The local name of the attribute. </param>
		/// <param name="namespaceURI">The namespace URI of the attribute. </param>
		[IndexerName("ItemOf")]
		public XmlAttribute this[string localName, string namespaceURI] => (XmlAttribute)GetNamedItem(localName, namespaceURI);

		internal XmlAttributeCollection(XmlNode parent)
			: base(parent)
		{
			ownerElement = (parent as XmlElement);
			ownerDocument = parent.OwnerDocument;
			if (ownerElement == null)
			{
				throw new XmlException("invalid construction for XmlAttributeCollection.");
			}
		}

		/// <summary>For a description of this member, see <see cref="M:System.Xml.XmlAttributeCollection.CopyTo(System.Xml.XmlAttribute[],System.Int32)" />.</summary>
		/// <param name="array">The array that is the destination of the objects copied from this collection. </param>
		/// <param name="index">The index in the array where copying begins. </param>
		void ICollection.CopyTo(Array array, int index)
		{
			array.CopyTo(base.Nodes.ToArray(typeof(XmlAttribute)), index);
		}

		/// <summary>Inserts the specified attribute as the last node in the collection.</summary>
		/// <returns>The XmlAttribute to append to the collection.</returns>
		/// <param name="node">The <see cref="T:System.Xml.XmlAttribute" /> to insert. </param>
		/// <exception cref="T:System.ArgumentException">
		///   <paramref name="node" /> was created from a document different from the one that created this collection. </exception>
		public XmlAttribute Append(XmlAttribute node)
		{
			SetNamedItem(node);
			return node;
		}

		/// <summary>Copies all the <see cref="T:System.Xml.XmlAttribute" /> objects from this collection into the given array.</summary>
		/// <param name="array">The array that is the destination of the objects copied from this collection. </param>
		/// <param name="index">The index in the array where copying begins. </param>
		public void CopyTo(XmlAttribute[] array, int index)
		{
			for (int i = 0; i < Count; i++)
			{
				array[index + i] = (base.Nodes[i] as XmlAttribute);
			}
		}

		/// <summary>Inserts the specified attribute immediately after the specified reference attribute.</summary>
		/// <returns>The XmlAttribute to insert into the collection.</returns>
		/// <param name="newNode">The <see cref="T:System.Xml.XmlAttribute" /> to insert. </param>
		/// <param name="refNode">The <see cref="T:System.Xml.XmlAttribute" /> that is the reference attribute. <paramref name="newNode" /> is placed after the <paramref name="refNode" />. </param>
		/// <exception cref="T:System.ArgumentException">The <paramref name="newNode" /> was created from a document different from the one that created this collection. Or the <paramref name="refNode" /> is not a member of this collection. </exception>
		public XmlAttribute InsertAfter(XmlAttribute newNode, XmlAttribute refNode)
		{
			if (refNode == null)
			{
				if (Count == 0)
				{
					return InsertBefore(newNode, null);
				}
				return InsertBefore(newNode, this[0]);
			}
			for (int i = 0; i < Count; i++)
			{
				if (refNode == base.Nodes[i])
				{
					return InsertBefore(newNode, (Count != i + 1) ? this[i + 1] : null);
				}
			}
			throw new ArgumentException("refNode not found in this collection.");
		}

		/// <summary>Inserts the specified attribute immediately before the specified reference attribute.</summary>
		/// <returns>The XmlAttribute to insert into the collection.</returns>
		/// <param name="newNode">The <see cref="T:System.Xml.XmlAttribute" /> to insert. </param>
		/// <param name="refNode">The <see cref="T:System.Xml.XmlAttribute" /> that is the reference attribute. <paramref name="newNode" /> is placed before the <paramref name="refNode" />. </param>
		/// <exception cref="T:System.ArgumentException">The <paramref name="newNode" /> was created from a document different from the one that created this collection. Or the <paramref name="refNode" /> is not a member of this collection. </exception>
		public XmlAttribute InsertBefore(XmlAttribute newNode, XmlAttribute refNode)
		{
			if (newNode.OwnerDocument != ownerDocument)
			{
				throw new ArgumentException("different document created this newNode.");
			}
			ownerDocument.onNodeInserting(newNode, null);
			int num = Count;
			if (refNode != null)
			{
				for (int i = 0; i < Count; i++)
				{
					XmlNode xmlNode = base.Nodes[i] as XmlNode;
					if (xmlNode == refNode)
					{
						num = i;
						break;
					}
				}
				if (num == Count)
				{
					throw new ArgumentException("refNode not found in this collection.");
				}
			}
			SetNamedItem(newNode, num, raiseEvent: false);
			ownerDocument.onNodeInserted(newNode, null);
			return newNode;
		}

		/// <summary>Inserts the specified attribute as the first node in the collection.</summary>
		/// <returns>The XmlAttribute added to the collection.</returns>
		/// <param name="node">The <see cref="T:System.Xml.XmlAttribute" /> to insert. </param>
		public XmlAttribute Prepend(XmlAttribute node)
		{
			return InsertAfter(node, null);
		}

		/// <summary>Removes the specified attribute from the collection.</summary>
		/// <returns>The node removed or null if it is not found in the collection.</returns>
		/// <param name="node">The <see cref="T:System.Xml.XmlAttribute" /> to remove. </param>
		public XmlAttribute Remove(XmlAttribute node)
		{
			if (IsReadOnly)
			{
				throw new ArgumentException("This attribute collection is read-only.");
			}
			if (node == null)
			{
				throw new ArgumentException("Specified node is null.");
			}
			if (node.OwnerDocument != ownerDocument)
			{
				throw new ArgumentException("Specified node is in a different document.");
			}
			if (node.OwnerElement != ownerElement)
			{
				throw new ArgumentException("The specified attribute is not contained in the element.");
			}
			XmlAttribute xmlAttribute = null;
			for (int i = 0; i < Count; i++)
			{
				XmlAttribute xmlAttribute2 = (XmlAttribute)base.Nodes[i];
				if (xmlAttribute2 == node)
				{
					xmlAttribute = xmlAttribute2;
					break;
				}
			}
			if (xmlAttribute != null)
			{
				ownerDocument.onNodeRemoving(node, ownerElement);
				base.RemoveNamedItem(xmlAttribute.LocalName, xmlAttribute.NamespaceURI);
				RemoveIdenticalAttribute(xmlAttribute);
				ownerDocument.onNodeRemoved(node, ownerElement);
			}
			DTDAttributeDefinition attributeDefinition = xmlAttribute.GetAttributeDefinition();
			if (attributeDefinition != null && attributeDefinition.DefaultValue != null)
			{
				XmlAttribute xmlAttribute3 = ownerDocument.CreateAttribute(xmlAttribute.Prefix, xmlAttribute.LocalName, xmlAttribute.NamespaceURI, atomizedNames: true, checkNamespace: false);
				xmlAttribute3.Value = attributeDefinition.DefaultValue;
				xmlAttribute3.SetDefault();
				SetNamedItem(xmlAttribute3);
			}
			xmlAttribute.AttributeOwnerElement = null;
			return xmlAttribute;
		}

		/// <summary>Removes all attributes from the collection.</summary>
		public void RemoveAll()
		{
			int num = 0;
			while (num < Count)
			{
				XmlAttribute xmlAttribute = this[num];
				if (!xmlAttribute.Specified)
				{
					num++;
				}
				Remove(xmlAttribute);
			}
		}

		/// <summary>Removes the attribute corresponding to the specified index from the collection.</summary>
		/// <returns>Returns null if there is no attribute at the specified index.</returns>
		/// <param name="i">The index of the node to remove. The first node has index 0. </param>
		public XmlAttribute RemoveAt(int i)
		{
			if (Count <= i)
			{
				return null;
			}
			return Remove((XmlAttribute)base.Nodes[i]);
		}

		/// <summary>Adds a <see cref="T:System.Xml.XmlNode" /> using its <see cref="P:System.Xml.XmlNode.Name" /> property </summary>
		/// <returns>If the <paramref name="node" /> replaces an existing node with the same name, the old node is returned; otherwise, the added node is returned.</returns>
		/// <param name="node">An attribute node to store in this collection. The node will later be accessible using the name of the node. If a node with that name is already present in the collection, it is replaced by the new one; otherwise, the node is appended to the end of the collection. </param>
		/// <exception cref="T:System.ArgumentException">
		///   <paramref name="node" /> was created from a different <see cref="T:System.Xml.XmlDocument" /> than the one that created this collection.This XmlAttributeCollection is read-only. </exception>
		/// <exception cref="T:System.InvalidOperationException">
		///   <paramref name="node" /> is an <see cref="T:System.Xml.XmlAttribute" /> that is already an attribute of another <see cref="T:System.Xml.XmlElement" /> object. To re-use attributes in other elements, you must clone the XmlAttribute objects you want to re-use. </exception>
		public override XmlNode SetNamedItem(XmlNode node)
		{
			if (IsReadOnly)
			{
				throw new ArgumentException("this AttributeCollection is read only.");
			}
			XmlAttribute xmlAttribute = node as XmlAttribute;
			if (xmlAttribute.OwnerElement == ownerElement)
			{
				return node;
			}
			if (xmlAttribute.OwnerElement != null)
			{
				throw new ArgumentException("This attribute is already set to another element.");
			}
			ownerElement.OwnerDocument.onNodeInserting(node, ownerElement);
			xmlAttribute.AttributeOwnerElement = ownerElement;
			XmlNode xmlNode = SetNamedItem(node, -1, raiseEvent: false);
			AdjustIdenticalAttributes(node as XmlAttribute, (xmlNode != node) ? xmlNode : null);
			ownerElement.OwnerDocument.onNodeInserted(node, ownerElement);
			return xmlNode as XmlAttribute;
		}

		internal void AddIdenticalAttribute()
		{
			SetIdenticalAttribute(remove: false);
		}

		internal void RemoveIdenticalAttribute()
		{
			SetIdenticalAttribute(remove: true);
		}

		private void SetIdenticalAttribute(bool remove)
		{
			if (ownerElement == null)
			{
				return;
			}
			XmlDocumentType documentType = ownerDocument.DocumentType;
			if (documentType == null || documentType.DTD == null)
			{
				return;
			}
			DTDElementDeclaration dTDElementDeclaration = documentType.DTD.ElementDecls[ownerElement.Name];
			int num = 0;
			XmlAttribute xmlAttribute;
			while (true)
			{
				if (num >= Count)
				{
					return;
				}
				xmlAttribute = (XmlAttribute)base.Nodes[num];
				DTDAttributeDefinition dTDAttributeDefinition = dTDElementDeclaration?.Attributes[xmlAttribute.Name];
				if (dTDAttributeDefinition != null && dTDAttributeDefinition.Datatype.TokenizedType == XmlTokenizedType.ID)
				{
					if (!remove)
					{
						break;
					}
					if (ownerDocument.GetIdenticalAttribute(xmlAttribute.Value) != null)
					{
						ownerDocument.RemoveIdenticalAttribute(xmlAttribute.Value);
						return;
					}
				}
				num++;
			}
			if (ownerDocument.GetIdenticalAttribute(xmlAttribute.Value) != null)
			{
				throw new XmlException($"ID value {xmlAttribute.Value} already exists in this document.");
			}
			ownerDocument.AddIdenticalAttribute(xmlAttribute);
		}

		private void AdjustIdenticalAttributes(XmlAttribute node, XmlNode existing)
		{
			if (ownerElement == null)
			{
				return;
			}
			if (existing != null)
			{
				RemoveIdenticalAttribute(existing);
			}
			XmlDocumentType documentType = node.OwnerDocument.DocumentType;
			if (documentType != null && documentType.DTD != null)
			{
				DTDAttributeDefinition dTDAttributeDefinition = documentType.DTD.AttListDecls[ownerElement.Name]?.Get(node.Name);
				if (dTDAttributeDefinition != null && dTDAttributeDefinition.Datatype.TokenizedType == XmlTokenizedType.ID)
				{
					ownerDocument.AddIdenticalAttribute(node);
				}
			}
		}

		private XmlNode RemoveIdenticalAttribute(XmlNode existing)
		{
			if (ownerElement == null)
			{
				return existing;
			}
			if (existing != null && ownerDocument.GetIdenticalAttribute(existing.Value) != null)
			{
				ownerDocument.RemoveIdenticalAttribute(existing.Value);
			}
			return existing;
		}
	}
}
