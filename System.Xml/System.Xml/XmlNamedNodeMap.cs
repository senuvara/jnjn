using Mono.Xml;
using System.Collections;

namespace System.Xml
{
	/// <summary>Represents a collection of nodes that can be accessed by name or index.</summary>
	public class XmlNamedNodeMap : IEnumerable
	{
		private static readonly IEnumerator emptyEnumerator = new XmlNode[0].GetEnumerator();

		private XmlNode parent;

		private ArrayList nodeList;

		private bool readOnly;

		private ArrayList NodeList
		{
			get
			{
				if (nodeList == null)
				{
					nodeList = new ArrayList();
				}
				return nodeList;
			}
		}

		/// <summary>Gets the number of nodes in the XmlNamedNodeMap.</summary>
		/// <returns>The number of nodes.</returns>
		public virtual int Count => (nodeList != null) ? nodeList.Count : 0;

		internal ArrayList Nodes => NodeList;

		internal XmlNamedNodeMap(XmlNode parent)
		{
			this.parent = parent;
		}

		/// <summary>Provides support for the "foreach" style iteration over the collection of nodes in the XmlNamedNodeMap.</summary>
		/// <returns>An <see cref="T:System.Collections.IEnumerator" />.</returns>
		public virtual IEnumerator GetEnumerator()
		{
			if (nodeList == null)
			{
				return emptyEnumerator;
			}
			return nodeList.GetEnumerator();
		}

		/// <summary>Retrieves an <see cref="T:System.Xml.XmlNode" /> specified by name.</summary>
		/// <returns>An XmlNode with the specified name or null if a matching node is not found.</returns>
		/// <param name="name">The qualified name of the node to retrieve. It is matched against the <see cref="P:System.Xml.XmlNode.Name" /> property of the matching node. </param>
		public virtual XmlNode GetNamedItem(string name)
		{
			if (nodeList == null)
			{
				return null;
			}
			for (int i = 0; i < nodeList.Count; i++)
			{
				XmlNode xmlNode = (XmlNode)nodeList[i];
				if (xmlNode.Name == name)
				{
					return xmlNode;
				}
			}
			return null;
		}

		/// <summary>Retrieves a node with the matching <see cref="P:System.Xml.XmlNode.LocalName" /> and <see cref="P:System.Xml.XmlNode.NamespaceURI" />.</summary>
		/// <returns>An <see cref="T:System.Xml.XmlNode" /> with the matching local name and namespace URI or null if a matching node was not found.</returns>
		/// <param name="localName">The local name of the node to retrieve. </param>
		/// <param name="namespaceURI">The namespace Uniform Resource Identifier (URI) of the node to retrieve. </param>
		public virtual XmlNode GetNamedItem(string localName, string namespaceURI)
		{
			if (nodeList == null)
			{
				return null;
			}
			for (int i = 0; i < nodeList.Count; i++)
			{
				XmlNode xmlNode = (XmlNode)nodeList[i];
				if (xmlNode.LocalName == localName && xmlNode.NamespaceURI == namespaceURI)
				{
					return xmlNode;
				}
			}
			return null;
		}

		/// <summary>Retrieves the node at the specified index in the XmlNamedNodeMap.</summary>
		/// <returns>The <see cref="T:System.Xml.XmlNode" /> at the specified index. If <paramref name="index" /> is less than 0 or greater than or equal to the <see cref="P:System.Xml.XmlNamedNodeMap.Count" /> property, null is returned.</returns>
		/// <param name="index">The index position of the node to retrieve from the XmlNamedNodeMap. The index is zero-based; therefore, the index of the first node is 0 and the index of the last node is <see cref="P:System.Xml.XmlNamedNodeMap.Count" /> -1. </param>
		public virtual XmlNode Item(int index)
		{
			if (nodeList == null || index < 0 || index >= nodeList.Count)
			{
				return null;
			}
			return (XmlNode)nodeList[index];
		}

		/// <summary>Removes the node from the XmlNamedNodeMap.</summary>
		/// <returns>The XmlNode removed from this XmlNamedNodeMap or null if a matching node was not found.</returns>
		/// <param name="name">The qualified name of the node to remove. The name is matched against the <see cref="P:System.Xml.XmlNode.Name" /> property of the matching node. </param>
		public virtual XmlNode RemoveNamedItem(string name)
		{
			if (nodeList == null)
			{
				return null;
			}
			for (int i = 0; i < nodeList.Count; i++)
			{
				XmlNode xmlNode = (XmlNode)nodeList[i];
				if (!(xmlNode.Name == name))
				{
					continue;
				}
				if (xmlNode.IsReadOnly)
				{
					throw new InvalidOperationException("Cannot remove. This node is read only: " + name);
				}
				nodeList.Remove(xmlNode);
				XmlAttribute xmlAttribute = xmlNode as XmlAttribute;
				if (xmlAttribute != null)
				{
					DTDAttributeDefinition attributeDefinition = xmlAttribute.GetAttributeDefinition();
					if (attributeDefinition != null && attributeDefinition.DefaultValue != null)
					{
						XmlAttribute xmlAttribute2 = xmlAttribute.OwnerDocument.CreateAttribute(xmlAttribute.Prefix, xmlAttribute.LocalName, xmlAttribute.NamespaceURI, atomizedNames: true, checkNamespace: false);
						xmlAttribute2.Value = attributeDefinition.DefaultValue;
						xmlAttribute2.SetDefault();
						xmlAttribute.OwnerElement.SetAttributeNode(xmlAttribute2);
					}
				}
				return xmlNode;
			}
			return null;
		}

		/// <summary>Removes a node with the matching <see cref="P:System.Xml.XmlNode.LocalName" /> and <see cref="P:System.Xml.XmlNode.NamespaceURI" />.</summary>
		/// <returns>The <see cref="T:System.Xml.XmlNode" /> removed or null if a matching node was not found.</returns>
		/// <param name="localName">The local name of the node to remove. </param>
		/// <param name="namespaceURI">The namespace URI of the node to remove. </param>
		public virtual XmlNode RemoveNamedItem(string localName, string namespaceURI)
		{
			if (nodeList == null)
			{
				return null;
			}
			for (int i = 0; i < nodeList.Count; i++)
			{
				XmlNode xmlNode = (XmlNode)nodeList[i];
				if (xmlNode.LocalName == localName && xmlNode.NamespaceURI == namespaceURI)
				{
					nodeList.Remove(xmlNode);
					return xmlNode;
				}
			}
			return null;
		}

		/// <summary>Adds an <see cref="T:System.Xml.XmlNode" /> using its <see cref="P:System.Xml.XmlNode.Name" /> property </summary>
		/// <returns>If the <paramref name="node" /> replaces an existing node with the same name, the old node is returned; otherwise, null is returned.</returns>
		/// <param name="node">An XmlNode to store in the XmlNamedNodeMap. If a node with that name is already present in the map, it is replaced by the new one. </param>
		/// <exception cref="T:System.ArgumentException">The <paramref name="node" /> was created from a different <see cref="T:System.Xml.XmlDocument" /> than the one that created the XmlNamedNodeMap; or the XmlNamedNodeMap is read-only. </exception>
		public virtual XmlNode SetNamedItem(XmlNode node)
		{
			return SetNamedItem(node, -1, raiseEvent: true);
		}

		internal XmlNode SetNamedItem(XmlNode node, bool raiseEvent)
		{
			return SetNamedItem(node, -1, raiseEvent);
		}

		internal XmlNode SetNamedItem(XmlNode node, int pos, bool raiseEvent)
		{
			//Discarded unreachable code: IL_0109
			if (readOnly || node.OwnerDocument != parent.OwnerDocument)
			{
				throw new ArgumentException("Cannot add to NodeMap.");
			}
			if (raiseEvent)
			{
				parent.OwnerDocument.onNodeInserting(node, parent);
			}
			try
			{
				for (int i = 0; i < NodeList.Count; i++)
				{
					XmlNode xmlNode = (XmlNode)nodeList[i];
					if (xmlNode.LocalName == node.LocalName && xmlNode.NamespaceURI == node.NamespaceURI)
					{
						nodeList.Remove(xmlNode);
						if (pos < 0)
						{
							nodeList.Add(node);
						}
						else
						{
							nodeList.Insert(pos, node);
						}
						return xmlNode;
					}
				}
				if (pos < 0)
				{
					nodeList.Add(node);
				}
				else
				{
					nodeList.Insert(pos, node);
				}
				return node;
			}
			finally
			{
				if (raiseEvent)
				{
					parent.OwnerDocument.onNodeInserted(node, parent);
				}
			}
		}
	}
}
