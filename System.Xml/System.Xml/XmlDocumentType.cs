using Mono.Xml;
using Mono.Xml2;
using System.IO;

namespace System.Xml
{
	/// <summary>Represents the document type declaration.</summary>
	public class XmlDocumentType : XmlLinkedNode
	{
		internal XmlNamedNodeMap entities;

		internal XmlNamedNodeMap notations;

		private DTDObjectModel dtd;

		internal DTDObjectModel DTD => dtd;

		/// <summary>Gets the collection of <see cref="T:System.Xml.XmlEntity" /> nodes declared in the document type declaration.</summary>
		/// <returns>An <see cref="T:System.Xml.XmlNamedNodeMap" /> containing the XmlEntity nodes. The returned XmlNamedNodeMap is read-only.</returns>
		public XmlNamedNodeMap Entities => entities;

		/// <summary>Gets the value of the document type definition (DTD) internal subset on the DOCTYPE declaration.</summary>
		/// <returns>The DTD internal subset on the DOCTYPE. If there is no DTD internal subset, String.Empty is returned.</returns>
		public string InternalSubset => dtd.InternalSubset;

		/// <summary>Gets a value indicating whether the node is read-only.</summary>
		/// <returns>true if the node is read-only; otherwise false.Because DocumentType nodes are read-only, this property always returns true.</returns>
		public override bool IsReadOnly => true;

		/// <summary>Gets the local name of the node.</summary>
		/// <returns>For DocumentType nodes, this property returns the name of the document type.</returns>
		public override string LocalName => dtd.Name;

		/// <summary>Gets the qualified name of the node.</summary>
		/// <returns>For DocumentType nodes, this property returns the name of the document type.</returns>
		public override string Name => dtd.Name;

		/// <summary>Gets the type of the current node.</summary>
		/// <returns>For DocumentType nodes, this value is XmlNodeType.DocumentType.</returns>
		public override XmlNodeType NodeType => XmlNodeType.DocumentType;

		/// <summary>Gets the collection of <see cref="T:System.Xml.XmlNotation" /> nodes present in the document type declaration.</summary>
		/// <returns>An <see cref="T:System.Xml.XmlNamedNodeMap" /> containing the XmlNotation nodes. The returned XmlNamedNodeMap is read-only.</returns>
		public XmlNamedNodeMap Notations => notations;

		/// <summary>Gets the value of the public identifier on the DOCTYPE declaration.</summary>
		/// <returns>The public identifier on the DOCTYPE. If there is no public identifier, String.Empty is returned.</returns>
		public string PublicId => dtd.PublicId;

		/// <summary>Gets the value of the system identifier on the DOCTYPE declaration.</summary>
		/// <returns>The system identifier on the DOCTYPE. If there is no system identifier, String.Empty is returned.</returns>
		public string SystemId => dtd.SystemId;

		/// <summary />
		/// <param name="name" />
		/// <param name="publicId" />
		/// <param name="systemId" />
		/// <param name="internalSubset" />
		/// <param name="doc" />
		protected internal XmlDocumentType(string name, string publicId, string systemId, string internalSubset, XmlDocument doc)
			: base(doc)
		{
			Mono.Xml2.XmlTextReader xmlTextReader = new Mono.Xml2.XmlTextReader(BaseURI, new StringReader(string.Empty), doc.NameTable);
			xmlTextReader.XmlResolver = doc.Resolver;
			xmlTextReader.GenerateDTDObjectModel(name, publicId, systemId, internalSubset);
			dtd = xmlTextReader.DTD;
			ImportFromDTD();
		}

		internal XmlDocumentType(DTDObjectModel dtd, XmlDocument doc)
			: base(doc)
		{
			this.dtd = dtd;
			ImportFromDTD();
		}

		private void ImportFromDTD()
		{
			entities = new XmlNamedNodeMap(this);
			notations = new XmlNamedNodeMap(this);
			foreach (DTDEntityDeclaration value in DTD.EntityDecls.Values)
			{
				XmlNode namedItem = new XmlEntity(value.Name, value.NotationName, value.PublicId, value.SystemId, OwnerDocument);
				entities.SetNamedItem(namedItem);
			}
			foreach (DTDNotationDeclaration value2 in DTD.NotationDecls.Values)
			{
				XmlNode namedItem2 = new XmlNotation(value2.LocalName, value2.Prefix, value2.PublicId, value2.SystemId, OwnerDocument);
				notations.SetNamedItem(namedItem2);
			}
		}

		/// <summary>Creates a duplicate of this node.</summary>
		/// <returns>The cloned node.</returns>
		/// <param name="deep">true to recursively clone the subtree under the specified node; false to clone only the node itself. For document type nodes, the cloned node always includes the subtree, regardless of the parameter setting. </param>
		public override XmlNode CloneNode(bool deep)
		{
			return new XmlDocumentType(dtd, OwnerDocument);
		}

		/// <summary>Saves all the children of the node to the specified <see cref="T:System.Xml.XmlWriter" />. For XmlDocumentType nodes, this method has no effect.</summary>
		/// <param name="w">The XmlWriter to which you want to save. </param>
		public override void WriteContentTo(XmlWriter w)
		{
		}

		/// <summary>Saves the node to the specified <see cref="T:System.Xml.XmlWriter" />.</summary>
		/// <param name="w">The XmlWriter to which you want to save. </param>
		public override void WriteTo(XmlWriter w)
		{
			w.WriteDocType(Name, PublicId, SystemId, InternalSubset);
		}
	}
}
