namespace System.Xml
{
	/// <summary>Specifies the state of the <see cref="T:System.Xml.XmlWriter" />.</summary>
	public enum WriteState
	{
		/// <summary>A Write method has not been called.</summary>
		Start,
		/// <summary>The prolog is being written.</summary>
		Prolog,
		/// <summary>An element start tag is being written.</summary>
		Element,
		/// <summary>An attribute value is being written.</summary>
		Attribute,
		/// <summary>The element content is being written.</summary>
		Content,
		/// <summary>The <see cref="M:System.Xml.XmlWriter.Close" /> method has been called.</summary>
		Closed,
		/// <summary>An exception has been thrown, which has left the <see cref="T:System.Xml.XmlWriter" /> in an invalid state. You may call the <see cref="M:System.Xml.XmlWriter.Close" /> method to put the <see cref="T:System.Xml.XmlWriter" /> in the <see cref="F:System.Xml.WriteState.Closed" /> state. Any other <see cref="T:System.Xml.XmlWriter" /> method calls results in an <see cref="T:System.InvalidOperationException" /> being thrown.</summary>
		Error
	}
}
