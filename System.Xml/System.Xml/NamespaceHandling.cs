namespace System.Xml
{
	[Flags]
	public enum NamespaceHandling
	{
		Default = 0x0,
		OmitDuplicates = 0x1
	}
}
