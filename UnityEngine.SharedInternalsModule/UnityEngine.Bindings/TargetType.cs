namespace UnityEngine.Bindings
{
	internal enum TargetType
	{
		Function,
		Field
	}
}
