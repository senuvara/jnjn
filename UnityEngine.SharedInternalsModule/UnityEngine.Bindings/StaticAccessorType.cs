namespace UnityEngine.Bindings
{
	internal enum StaticAccessorType
	{
		Dot,
		Arrow,
		DoubleColon
	}
}
