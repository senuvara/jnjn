using System.Diagnostics;
using System.Reflection;
using System.Runtime.CompilerServices;
using System.Runtime.InteropServices;

[assembly: CompilationRelaxations(8)]
[assembly: RuntimeCompatibility(WrapNonExceptionThrows = true)]
[assembly: AssemblyTitle("NVorbis")]
[assembly: AssemblyDescription("A Vorbis decoder in C#")]
[assembly: AssemblyCompany("Andrew Ward")]
[assembly: AssemblyProduct("NVorbis")]
[assembly: AssemblyCopyright("Copyright © Andrew Ward 2014")]
[assembly: ComVisible(false)]
[assembly: AssemblyFileVersion("0.8.4.0")]
[assembly: Debuggable(DebuggableAttribute.DebuggingModes.IgnoreSymbolStoreSequencePoints)]
[assembly: AssemblyVersion("0.8.4.0")]
