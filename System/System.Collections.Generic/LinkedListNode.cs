using System.Runtime.InteropServices;

namespace System.Collections.Generic
{
	/// <summary>Represents a node in a <see cref="T:System.Collections.Generic.LinkedList`1" />. This class cannot be inherited.</summary>
	/// <typeparam name="T">Specifies the element type of the linked list.</typeparam>
	/// <filterpriority>1</filterpriority>
	[ComVisible(false)]
	public sealed class LinkedListNode<T>
	{
		private T item;

		private LinkedList<T> container;

		internal LinkedListNode<T> forward;

		internal LinkedListNode<T> back;

		/// <summary>Gets the <see cref="T:System.Collections.Generic.LinkedList`1" /> that the <see cref="T:System.Collections.Generic.LinkedListNode`1" /> belongs to.</summary>
		/// <returns>A reference to the <see cref="T:System.Collections.Generic.LinkedList`1" /> that the <see cref="T:System.Collections.Generic.LinkedListNode`1" /> belongs to, or null if the <see cref="T:System.Collections.Generic.LinkedListNode`1" /> is not linked.</returns>
		public LinkedList<T> List => container;

		/// <summary>Gets the next node in the <see cref="T:System.Collections.Generic.LinkedList`1" />.</summary>
		/// <returns>A reference to the next node in the <see cref="T:System.Collections.Generic.LinkedList`1" />, or null if the current node is the last element (<see cref="P:System.Collections.Generic.LinkedList`1.Last" />) of the <see cref="T:System.Collections.Generic.LinkedList`1" />.</returns>
		public LinkedListNode<T> Next => (container == null || forward == container.first) ? null : forward;

		/// <summary>Gets the previous node in the <see cref="T:System.Collections.Generic.LinkedList`1" />.</summary>
		/// <returns>A reference to the previous node in the <see cref="T:System.Collections.Generic.LinkedList`1" />, or null if the current node is the first element (<see cref="P:System.Collections.Generic.LinkedList`1.First" />) of the <see cref="T:System.Collections.Generic.LinkedList`1" />.</returns>
		public LinkedListNode<T> Previous => (container == null || this == container.first) ? null : back;

		/// <summary>Gets the value contained in the node.</summary>
		/// <returns>The value contained in the node.</returns>
		public T Value
		{
			get
			{
				return item;
			}
			set
			{
				item = value;
			}
		}

		/// <summary>Initializes a new instance of the <see cref="T:System.Collections.Generic.LinkedListNode`1" /> class, containing the specified value.</summary>
		/// <param name="value">The value to contain in the <see cref="T:System.Collections.Generic.LinkedListNode`1" />.</param>
		public LinkedListNode(T value)
		{
			item = value;
		}

		internal LinkedListNode(LinkedList<T> list, T value)
		{
			container = list;
			item = value;
			back = (forward = this);
		}

		internal LinkedListNode(LinkedList<T> list, T value, LinkedListNode<T> previousNode, LinkedListNode<T> nextNode)
		{
			container = list;
			item = value;
			back = previousNode;
			forward = nextNode;
			previousNode.forward = this;
			nextNode.back = this;
		}

		internal void Detach()
		{
			back.forward = forward;
			forward.back = back;
			forward = (back = null);
			container = null;
		}

		internal void SelfReference(LinkedList<T> list)
		{
			forward = this;
			back = this;
			container = list;
		}

		internal void InsertBetween(LinkedListNode<T> previousNode, LinkedListNode<T> nextNode, LinkedList<T> list)
		{
			previousNode.forward = this;
			nextNode.back = this;
			forward = nextNode;
			back = previousNode;
			container = list;
		}
	}
}
