namespace System.Diagnostics
{
	/// <summary>Provides a set of methods and properties that help debug your code. This class cannot be inherited.</summary>
	/// <filterpriority>1</filterpriority>
	public sealed class Debug
	{
		private Debug()
		{
		}

		/// <summary>Checks for a condition and outputs the call stack if the condition is false.</summary>
		/// <param name="condition">true to prevent a message being displayed; otherwise, false. </param>
		/// <filterpriority>1</filterpriority>
		/// <PermissionSet>
		///   <IPermission class="System.Security.Permissions.FileIOPermission, mscorlib, Version=2.0.3600.0, Culture=neutral, PublicKeyToken=b77a5c561934e089" version="1" Unrestricted="true" />
		///   <IPermission class="System.Security.Permissions.SecurityPermission, mscorlib, Version=2.0.3600.0, Culture=neutral, PublicKeyToken=b77a5c561934e089" version="1" Flags="ControlEvidence" />
		/// </PermissionSet>
		[Conditional("DEBUG")]
		public static void Assert(bool condition)
		{
		}

		/// <summary>Checks for a condition and displays a message if the condition is false.</summary>
		/// <param name="condition">true to prevent a message being displayed; otherwise, false. </param>
		/// <param name="message">A message to display. </param>
		/// <filterpriority>1</filterpriority>
		/// <PermissionSet>
		///   <IPermission class="System.Security.Permissions.FileIOPermission, mscorlib, Version=2.0.3600.0, Culture=neutral, PublicKeyToken=b77a5c561934e089" version="1" Unrestricted="true" />
		///   <IPermission class="System.Security.Permissions.SecurityPermission, mscorlib, Version=2.0.3600.0, Culture=neutral, PublicKeyToken=b77a5c561934e089" version="1" Flags="ControlEvidence" />
		/// </PermissionSet>
		[Conditional("DEBUG")]
		public static void Assert(bool condition, string message)
		{
		}

		/// <summary>Checks for a condition and displays both specified messages if the condition is false.</summary>
		/// <param name="condition">true to prevent a message being displayed; otherwise, false. </param>
		/// <param name="message">A message to display. </param>
		/// <param name="detailMessage">A detailed message to display. </param>
		/// <filterpriority>1</filterpriority>
		/// <PermissionSet>
		///   <IPermission class="System.Security.Permissions.FileIOPermission, mscorlib, Version=2.0.3600.0, Culture=neutral, PublicKeyToken=b77a5c561934e089" version="1" Unrestricted="true" />
		///   <IPermission class="System.Security.Permissions.SecurityPermission, mscorlib, Version=2.0.3600.0, Culture=neutral, PublicKeyToken=b77a5c561934e089" version="1" Flags="ControlEvidence" />
		/// </PermissionSet>
		[Conditional("DEBUG")]
		public static void Assert(bool condition, string message, string details)
		{
		}

		[Conditional("DEBUG")]
		public static void Assert(bool condition, string message, string details, object[] args)
		{
		}

		/// <summary>Writes the value of the object's <see cref="M:System.Object.ToString" /> method to the trace listeners in the <see cref="P:System.Diagnostics.Debug.Listeners" /> collection.</summary>
		/// <param name="value">An object whose name is sent to the <see cref="P:System.Diagnostics.Debug.Listeners" />. </param>
		/// <filterpriority>2</filterpriority>
		/// <PermissionSet>
		///   <IPermission class="System.Security.Permissions.FileIOPermission, mscorlib, Version=2.0.3600.0, Culture=neutral, PublicKeyToken=b77a5c561934e089" version="1" Unrestricted="true" />
		///   <IPermission class="System.Security.Permissions.SecurityPermission, mscorlib, Version=2.0.3600.0, Culture=neutral, PublicKeyToken=b77a5c561934e089" version="1" Flags="ControlEvidence" />
		/// </PermissionSet>
		[Conditional("DEBUG")]
		public static void WriteLine(object obj)
		{
			Console.WriteLine(obj);
		}

		/// <summary>Writes a message followed by a line terminator to the trace listeners in the <see cref="P:System.Diagnostics.Debug.Listeners" /> collection.</summary>
		/// <param name="message">A message to write. </param>
		/// <filterpriority>2</filterpriority>
		/// <PermissionSet>
		///   <IPermission class="System.Security.Permissions.FileIOPermission, mscorlib, Version=2.0.3600.0, Culture=neutral, PublicKeyToken=b77a5c561934e089" version="1" Unrestricted="true" />
		///   <IPermission class="System.Security.Permissions.SecurityPermission, mscorlib, Version=2.0.3600.0, Culture=neutral, PublicKeyToken=b77a5c561934e089" version="1" Flags="ControlEvidence" />
		/// </PermissionSet>
		[Conditional("DEBUG")]
		public static void WriteLine(string message)
		{
			Console.WriteLine(message);
		}

		[Conditional("DEBUG")]
		public static void WriteLine(string format, params object[] args)
		{
			Console.WriteLine(format, args);
		}
	}
}
