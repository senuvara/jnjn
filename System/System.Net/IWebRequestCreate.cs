namespace System.Net
{
	/// <summary>Provides the base interface for creating <see cref="T:System.Net.WebRequest" /> instances.</summary>
	public interface IWebRequestCreate
	{
		/// <summary>Creates a <see cref="T:System.Net.WebRequest" /> instance.</summary>
		/// <returns>A <see cref="T:System.Net.WebRequest" /> instance.</returns>
		/// <param name="uri">The uniform resource identifier (URI) of the Web resource. </param>
		WebRequest Create(Uri uri);
	}
}
