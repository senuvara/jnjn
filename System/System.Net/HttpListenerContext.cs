using System.Security.Principal;
using System.Text;

namespace System.Net
{
	/// <summary>Provides access to the request and response objects used by the <see cref="T:System.Net.HttpListener" /> class. This class cannot be inherited.</summary>
	public sealed class HttpListenerContext
	{
		private HttpListenerRequest request;

		private HttpListenerResponse response;

		private IPrincipal user;

		private HttpConnection cnc;

		private string error;

		private int err_status = 400;

		internal HttpListener Listener;

		internal int ErrorStatus
		{
			get
			{
				return err_status;
			}
			set
			{
				err_status = value;
			}
		}

		internal string ErrorMessage
		{
			get
			{
				return error;
			}
			set
			{
				error = value;
			}
		}

		internal bool HaveError => error != null;

		internal HttpConnection Connection => cnc;

		/// <summary>Gets the <see cref="T:System.Net.HttpListenerRequest" /> that represents a client's request for a resource.</summary>
		/// <returns>An <see cref="T:System.Net.HttpListenerRequest" /> object that represents the client request.</returns>
		public HttpListenerRequest Request => request;

		/// <summary>Gets the <see cref="T:System.Net.HttpListenerResponse" /> object that will be sent to the client in response to the client's request. </summary>
		/// <returns>An <see cref="T:System.Net.HttpListenerResponse" /> object used to send a response back to the client.</returns>
		public HttpListenerResponse Response => response;

		/// <summary>Gets an object used to obtain identity, authentication information, and security roles for the client whose request is represented by this <see cref="T:System.Net.HttpListenerContext" /> object. </summary>
		/// <returns>An <see cref="T:System.Security.Principal.IPrincipal" /> object that describes the client, or null if the <see cref="T:System.Net.HttpListener" /> that supplied this <see cref="T:System.Net.HttpListenerContext" /> does not require authentication.</returns>
		public IPrincipal User => user;

		internal HttpListenerContext(HttpConnection cnc)
		{
			this.cnc = cnc;
			request = new HttpListenerRequest(this);
			response = new HttpListenerResponse(this);
		}

		internal void ParseAuthentication(AuthenticationSchemes expectedSchemes)
		{
			if (expectedSchemes == AuthenticationSchemes.Anonymous)
			{
				return;
			}
			string text = request.Headers["Authorization"];
			if (text != null && text.Length >= 2)
			{
				string[] array = text.Split(new char[1]
				{
					' '
				}, 2);
				if (string.Compare(array[0], "basic", ignoreCase: true) == 0)
				{
					user = ParseBasicAuthentication(array[1]);
				}
			}
		}

		internal IPrincipal ParseBasicAuthentication(string authData)
		{
			//Discarded unreachable code: IL_006f, IL_007d
			try
			{
				string text = null;
				string text2 = null;
				int num = -1;
				string @string = Encoding.Default.GetString(Convert.FromBase64String(authData));
				num = @string.IndexOf(':');
				text2 = @string.Substring(num + 1);
				@string = @string.Substring(0, num);
				num = @string.IndexOf('\\');
				text = ((num <= 0) ? @string : @string.Substring(num));
				HttpListenerBasicIdentity identity = new HttpListenerBasicIdentity(text, text2);
				return new GenericPrincipal(identity, new string[0]);
			}
			catch (Exception)
			{
				return null;
			}
		}
	}
}
