namespace System.Xml.Linq
{
	[Flags]
	public enum SaveOptions
	{
		None = 0x0,
		DisableFormatting = 0x1
	}
}
