namespace System.Xml.Linq
{
	[Flags]
	public enum LoadOptions
	{
		None = 0x0,
		PreserveWhitespace = 0x1,
		SetBaseUri = 0x2,
		SetLineInfo = 0x4
	}
}
