using System;
using System.Diagnostics;
using System.Reflection;
using System.Resources;
using System.Runtime.CompilerServices;
using System.Runtime.InteropServices;
using System.Security;

[assembly: AssemblyTitle("System.Xml.Linq.dll")]
[assembly: AssemblyProduct("MONO Common language infrastructure")]
[assembly: AssemblyCopyright("(c) various MONO Authors")]
[assembly: SatelliteContractVersion("3.5.0.0")]
[assembly: AssemblyInformationalVersion("3.5.21022.8")]
[assembly: AssemblyFileVersion("3.5.21022.8")]
[assembly: NeutralResourcesLanguage("en-US")]
[assembly: CLSCompliant(true)]
[assembly: AssemblyDelaySign(true)]
[assembly: AssemblyKeyFile("../ecma.pub")]
[assembly: AllowPartiallyTrustedCallers]
[assembly: ComVisible(false)]
[assembly: CompilationRelaxations(CompilationRelaxations.NoStringInterning)]
[assembly: Debuggable(DebuggableAttribute.DebuggingModes.IgnoreSymbolStoreSequencePoints)]
[assembly: RuntimeCompatibility(WrapNonExceptionThrows = true)]
[assembly: AssemblyDescription("System.Xml.Linq.dll")]
[assembly: AssemblyDefaultAlias("System.Xml.Linq.dll")]
[assembly: AssemblyCompany("MONO development team")]
[assembly: AssemblyVersion("3.5.0.0")]
