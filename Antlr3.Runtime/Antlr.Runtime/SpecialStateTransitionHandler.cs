namespace Antlr.Runtime
{
	public delegate int SpecialStateTransitionHandler(DFA dfa, int s, IIntStream input);
}
