namespace Antlr.Runtime
{
	public static class Tokens
	{
		public static readonly IToken EndOfFile = Tokens<CommonToken>.EndOfFile;

		public static readonly IToken Invalid = new CommonToken(0);

		public static readonly IToken Skip = new CommonToken(0);
	}
	public static class Tokens<T> where T : IToken, new()
	{
		public static readonly T EndOfFile;

		public static readonly T Invalid;

		public static readonly T Skip;

		static Tokens()
		{
			T endOfFile = new T();
			endOfFile.Type = -1;
			EndOfFile = endOfFile;
			T invalid = new T();
			invalid.Type = 0;
			Invalid = invalid;
			T skip = new T();
			skip.Type = 0;
			Skip = skip;
		}
	}
}
