namespace Antlr.Runtime
{
	public interface IRuleReturnScope<TLabel>
	{
		TLabel Start
		{
			get;
		}

		TLabel Stop
		{
			get;
		}
	}
}
