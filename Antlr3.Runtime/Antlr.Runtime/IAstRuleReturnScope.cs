namespace Antlr.Runtime
{
	public interface IAstRuleReturnScope<TAstLabel>
	{
		TAstLabel Tree
		{
			get;
		}
	}
}
