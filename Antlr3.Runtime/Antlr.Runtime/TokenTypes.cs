namespace Antlr.Runtime
{
	public static class TokenTypes
	{
		public const int EndOfFile = -1;

		public const int Invalid = 0;

		public const int EndOfRule = 1;

		public const int Down = 2;

		public const int Up = 3;

		public const int Min = 4;
	}
}
