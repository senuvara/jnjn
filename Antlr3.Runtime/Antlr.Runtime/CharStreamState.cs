using System;

namespace Antlr.Runtime
{
	[Serializable]
	public class CharStreamState
	{
		public int p;

		public int line;

		public int charPositionInLine;
	}
}
