using System;
using System.Diagnostics;
using System.Reflection;
using System.Runtime.CompilerServices;
using System.Runtime.InteropServices;

[assembly: AssemblyFileVersion("3.3.0.7239")]
[assembly: AssemblyConfiguration("")]
[assembly: Debuggable(DebuggableAttribute.DebuggingModes.IgnoreSymbolStoreSequencePoints)]
[assembly: CompilationRelaxations(8)]
[assembly: RuntimeCompatibility(WrapNonExceptionThrows = true)]
[assembly: AssemblyTitle("Antlr3.Runtime")]
[assembly: AssemblyDescription("")]
[assembly: AssemblyCopyright("Copyright © Pixel Mine 2010")]
[assembly: AssemblyCompany("Pixel Mine, Inc.")]
[assembly: AssemblyProduct("Antlr3.Runtime")]
[assembly: CLSCompliant(true)]
[assembly: Guid("7a0b4db7-f127-4cf5-ac2c-e294957efcd6")]
[assembly: AssemblyTrademark("")]
[assembly: ComVisible(false)]
[assembly: AssemblyVersion("3.3.0.7239")]
