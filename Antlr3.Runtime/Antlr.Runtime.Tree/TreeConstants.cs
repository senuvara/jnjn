namespace Antlr.Runtime.Tree
{
	public static class TreeConstants
	{
		public static readonly ITree INVALID_NODE = new CommonTree(Tokens.Invalid);
	}
}
