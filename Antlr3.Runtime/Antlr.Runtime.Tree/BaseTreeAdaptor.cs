using System;
using System.Collections.Generic;

namespace Antlr.Runtime.Tree
{
	public abstract class BaseTreeAdaptor : ITreeAdaptor
	{
		protected IDictionary<object, int> treeToUniqueIDMap;

		protected int uniqueNodeID = 1;

		public virtual object Nil()
		{
			return Create(null);
		}

		public virtual object ErrorNode(ITokenStream input, IToken start, IToken stop, RecognitionException e)
		{
			return new CommonErrorNode(input, start, stop, e);
		}

		public virtual bool IsNil(object tree)
		{
			return ((ITree)tree).IsNil;
		}

		public virtual object DupTree(object tree)
		{
			return DupTree(tree, null);
		}

		public virtual object DupTree(object t, object parent)
		{
			if (t == null)
			{
				return null;
			}
			object obj = DupNode(t);
			SetChildIndex(obj, GetChildIndex(t));
			SetParent(obj, parent);
			int childCount = GetChildCount(t);
			for (int i = 0; i < childCount; i++)
			{
				object child = GetChild(t, i);
				object child2 = DupTree(child, t);
				AddChild(obj, child2);
			}
			return obj;
		}

		public virtual void AddChild(object t, object child)
		{
			if (t != null && child != null)
			{
				((ITree)t).AddChild((ITree)child);
			}
		}

		public virtual object BecomeRoot(object newRoot, object oldRoot)
		{
			ITree tree = (ITree)newRoot;
			ITree t = (ITree)oldRoot;
			if (oldRoot == null)
			{
				return newRoot;
			}
			if (tree.IsNil)
			{
				int childCount = tree.ChildCount;
				if (childCount == 1)
				{
					tree = tree.GetChild(0);
				}
				else if (childCount > 1)
				{
					throw new Exception("more than one node as root (TODO: make exception hierarchy)");
				}
			}
			tree.AddChild(t);
			return tree;
		}

		public virtual object RulePostProcessing(object root)
		{
			ITree tree = (ITree)root;
			if (tree != null && tree.IsNil)
			{
				if (tree.ChildCount == 0)
				{
					tree = null;
				}
				else if (tree.ChildCount == 1)
				{
					tree = tree.GetChild(0);
					tree.Parent = null;
					tree.ChildIndex = -1;
				}
			}
			return tree;
		}

		public virtual object BecomeRoot(IToken newRoot, object oldRoot)
		{
			return BecomeRoot(Create(newRoot), oldRoot);
		}

		public virtual object Create(int tokenType, IToken fromToken)
		{
			fromToken = CreateToken(fromToken);
			fromToken.Type = tokenType;
			return (ITree)Create(fromToken);
		}

		public virtual object Create(int tokenType, IToken fromToken, string text)
		{
			if (fromToken == null)
			{
				return Create(tokenType, text);
			}
			fromToken = CreateToken(fromToken);
			fromToken.Type = tokenType;
			fromToken.Text = text;
			return (ITree)Create(fromToken);
		}

		public virtual object Create(int tokenType, string text)
		{
			IToken payload = CreateToken(tokenType, text);
			return (ITree)Create(payload);
		}

		public virtual int GetType(object t)
		{
			return ((ITree)t).Type;
		}

		public virtual void SetType(object t, int type)
		{
			throw new NotSupportedException("don't know enough about Tree node");
		}

		public virtual string GetText(object t)
		{
			return ((ITree)t).Text;
		}

		public virtual void SetText(object t, string text)
		{
			throw new NotSupportedException("don't know enough about Tree node");
		}

		public virtual object GetChild(object t, int i)
		{
			return ((ITree)t).GetChild(i);
		}

		public virtual void SetChild(object t, int i, object child)
		{
			((ITree)t).SetChild(i, (ITree)child);
		}

		public virtual object DeleteChild(object t, int i)
		{
			return ((ITree)t).DeleteChild(i);
		}

		public virtual int GetChildCount(object t)
		{
			return ((ITree)t).ChildCount;
		}

		public virtual int GetUniqueID(object node)
		{
			if (treeToUniqueIDMap == null)
			{
				treeToUniqueIDMap = new Dictionary<object, int>();
			}
			if (treeToUniqueIDMap.TryGetValue(node, out int value))
			{
				return value;
			}
			value = uniqueNodeID;
			treeToUniqueIDMap[node] = value;
			uniqueNodeID++;
			return value;
		}

		public abstract IToken CreateToken(int tokenType, string text);

		public abstract IToken CreateToken(IToken fromToken);

		public abstract object Create(IToken payload);

		public abstract object DupNode(object treeNode);

		public abstract IToken GetToken(object t);

		public abstract void SetTokenBoundaries(object t, IToken startToken, IToken stopToken);

		public abstract int GetTokenStartIndex(object t);

		public abstract int GetTokenStopIndex(object t);

		public abstract object GetParent(object t);

		public abstract void SetParent(object t, object parent);

		public abstract int GetChildIndex(object t);

		public abstract void SetChildIndex(object t, int index);

		public abstract void ReplaceChildren(object parent, int startChildIndex, int stopChildIndex, object t);
	}
}
