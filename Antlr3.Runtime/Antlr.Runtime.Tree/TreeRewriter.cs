using System;

namespace Antlr.Runtime.Tree
{
	public class TreeRewriter<TTree> : TreeParser
	{
		protected ITokenStream originalTokenStream;

		protected ITreeAdaptor originalAdaptor;

		private Func<IAstRuleReturnScope<TTree>> topdown_func;

		private Func<IAstRuleReturnScope<TTree>> bottomup_func;

		public TreeRewriter(ITreeNodeStream input)
			: this(input, new RecognizerSharedState())
		{
		}

		public TreeRewriter(ITreeNodeStream input, RecognizerSharedState state)
			: base(input, state)
		{
			originalAdaptor = input.TreeAdaptor;
			originalTokenStream = input.TokenStream;
			Func<IAstRuleReturnScope<TTree>> func = topdown_func = (() => Topdown());
			bottomup_func = (() => Bottomup());
		}

		public virtual object ApplyOnce(object t, Func<IAstRuleReturnScope<TTree>> whichRule)
		{
			if (t == null)
			{
				return null;
			}
			try
			{
				state = new RecognizerSharedState();
				input = new CommonTreeNodeStream(originalAdaptor, t);
				((CommonTreeNodeStream)input).TokenStream = originalTokenStream;
				BacktrackingLevel = 1;
				IAstRuleReturnScope<TTree> astRuleReturnScope = whichRule();
				BacktrackingLevel = 0;
				if (Failed)
				{
					return t;
				}
				if (typeof(CommonTree).IsAssignableFrom(typeof(TTree)) && astRuleReturnScope != null && !t.Equals(astRuleReturnScope.Tree) && astRuleReturnScope.Tree != null)
				{
					Console.Out.WriteLine(((CommonTree)t).ToStringTree() + " -> " + ((CommonTree)(object)astRuleReturnScope.Tree).ToStringTree());
				}
				if (astRuleReturnScope != null && astRuleReturnScope.Tree != null)
				{
					return astRuleReturnScope.Tree;
				}
				return t;
			}
			catch (RecognitionException)
			{
				return t;
			}
		}

		public virtual object ApplyRepeatedly(object t, Func<IAstRuleReturnScope<TTree>> whichRule)
		{
			bool flag = true;
			while (flag)
			{
				object obj = ApplyOnce(t, whichRule);
				flag = !t.Equals(obj);
				t = obj;
			}
			return t;
		}

		public virtual object Downup(object t)
		{
			TreeVisitor treeVisitor = new TreeVisitor(new CommonTreeAdaptor());
			t = treeVisitor.Visit(t, (object o) => ApplyOnce(o, topdown_func), (object o) => ApplyRepeatedly(o, bottomup_func));
			return t;
		}

		public virtual IAstRuleReturnScope<TTree> Topdown()
		{
			return null;
		}

		public virtual IAstRuleReturnScope<TTree> Bottomup()
		{
			return null;
		}
	}
}
