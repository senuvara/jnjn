using System;

namespace Antlr.Runtime.Tree
{
	[Serializable]
	public class TreeRuleReturnScope<TTree> : IRuleReturnScope<TTree>
	{
		private TTree _start;

		public TTree Start
		{
			get
			{
				return _start;
			}
			set
			{
				_start = value;
			}
		}

		TTree IRuleReturnScope<TTree>.Stop => default(TTree);
	}
}
