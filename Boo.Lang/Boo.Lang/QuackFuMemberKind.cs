using System;

namespace Boo.Lang
{
	[Flags]
	public enum QuackFuMemberKind
	{
		Any = 0x0,
		Method = 0x1,
		Getter = 0x2,
		Setter = 0x4,
		Property = 0x6
	}
}
