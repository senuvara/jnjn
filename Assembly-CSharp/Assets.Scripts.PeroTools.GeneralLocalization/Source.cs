namespace Assets.Scripts.PeroTools.GeneralLocalization
{
	public abstract class Source
	{
		public abstract object GetSourceTarget();

		public abstract void Default(Localization localiation);
	}
}
