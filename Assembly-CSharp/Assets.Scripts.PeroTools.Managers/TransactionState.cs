namespace Assets.Scripts.PeroTools.Managers
{
	public enum TransactionState
	{
		Purchased,
		Restored,
		Deferred,
		Failed
	}
}
