using Assets.Scripts.PeroTools.Commons;
using System.Collections.Generic;

namespace Assets.Scripts.PeroTools.Managers
{
	public class CustomEventManager : SingletonScriptableObject<CustomEventManager>
	{
		public List<string> customEvents;
	}
}
