namespace Assets.Scripts.PeroTools.Managers
{
	public class TransactionResult
	{
		public TransactionState state;

		public string productId;

		public string receipt;
	}
}
