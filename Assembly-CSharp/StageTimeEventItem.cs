using System;
using UnityEngine;

[Serializable]
public struct StageTimeEventItem
{
	public int actionIndex;

	public string soundPath;

	public GameObject sceneObject;
}
