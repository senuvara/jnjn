using UnityEngine;

public class DLCInfo : MonoBehaviour
{
	public GameObject normalDlc;

	public GameObject specialDlc;

	public GameObject unlockAllDlc;

	public void ChangeStage(string name)
	{
		if (name == string.Empty)
		{
			normalDlc.SetActive(value: false);
			specialDlc.SetActive(value: false);
			unlockAllDlc.SetActive(value: true);
		}
		else if (name == "ALBUM22")
		{
			normalDlc.SetActive(value: false);
			specialDlc.SetActive(value: true);
			unlockAllDlc.SetActive(value: false);
		}
		else
		{
			normalDlc.SetActive(value: true);
			specialDlc.SetActive(value: false);
			unlockAllDlc.SetActive(value: false);
		}
	}
}
