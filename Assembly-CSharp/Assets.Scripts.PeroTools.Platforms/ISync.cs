namespace Assets.Scripts.PeroTools.Platforms
{
	public interface ISync
	{
		void SaveLocal();

		void LoadLocal();
	}
}
