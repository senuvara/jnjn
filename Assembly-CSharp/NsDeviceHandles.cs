using System;

[Flags]
public enum NsDeviceHandles
{
	Left = 0x1,
	Right = 0x2,
	Both = 0x3
}
