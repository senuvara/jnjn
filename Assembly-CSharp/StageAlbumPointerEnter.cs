using Assets.Scripts.UI.Panels;
using UnityEngine;
using UnityEngine.EventSystems;

public class StageAlbumPointerEnter : MonoBehaviour, IPointerEnterHandler, IPointerExitHandler, IEventSystemHandler
{
	public GameObject parent;

	public void OnPointerEnter(PointerEventData data)
	{
		parent.GetComponent<PnlStage>().SetMouseWheelBinding(parent.GetComponent<PnlStage>().musicFancyScrollView, enable: false);
		parent.GetComponent<PnlStage>().SetMouseWheelBinding(parent.GetComponent<PnlStage>().albumFancyScrollViewNs, enable: true);
	}

	public void OnPointerExit(PointerEventData data)
	{
		parent.GetComponent<PnlStage>().SetMouseWheelBinding(parent.GetComponent<PnlStage>().musicFancyScrollView, enable: true);
		parent.GetComponent<PnlStage>().SetMouseWheelBinding(parent.GetComponent<PnlStage>().albumFancyScrollViewNs, enable: false);
	}
}
