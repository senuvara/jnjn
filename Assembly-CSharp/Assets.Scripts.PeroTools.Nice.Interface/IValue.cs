namespace Assets.Scripts.PeroTools.Nice.Interface
{
	public interface IValue
	{
		object result
		{
			get;
			set;
		}
	}
}
