using Assets.Scripts.GameCore.Managers;
using Assets.Scripts.PeroTools.Commons;

public class NormalEnemyController : BaseEnemyObjectController
{
	public override void OnAttackDestory()
	{
		if (IsEmptyNode())
		{
			base.gameObject.SetActive(value: false);
		}
	}

	public override bool OnControllerMiss(int idx)
	{
		if (FeverManager.Instance.IsGod())
		{
			return false;
		}
		bool flag = base.OnControllerMiss(idx);
		if (flag)
		{
			SingletonMonoBehaviour<GirlManager>.instance.BeAttackEffect();
		}
		return flag;
	}
}
