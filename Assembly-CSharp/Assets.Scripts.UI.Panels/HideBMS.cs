using System;
using UnityEngine;

namespace Assets.Scripts.UI.Panels
{
	public class HideBMS
	{
		private const int m_WidthSize = 18;

		private const int m_heightSize = 10;

		private Vector2[] m_PosPoints;

		private string[] m_OperationInfos;

		private string[] m_OperationInfosTouchTwo;

		private string[] m_CircleData = new string[8]
		{
			"1234123412341",
			"4321432143214",
			"2341234123412",
			"3214321432143",
			"3412341234133",
			"2143214321432",
			"4123412341234",
			"1432143214321"
		};

		public bool isMusicFD;

		public static bool isTriggerFD;

		public static string hideDifficultyFD;

		public static string designerNameFD;

		public Action<bool> OnTriggerFREEDOMDIVE;

		private string SetFDOperationInfo(char str, string inputInfos)
		{
			if (inputInfos.Length > 24)
			{
				return string.Empty;
			}
			if (inputInfos.Length == 0)
			{
				inputInfos += str;
			}
			if (inputInfos.Length > 0 && inputInfos[inputInfos.Length - 1] != str)
			{
				inputInfos += str;
			}
			return inputInfos;
		}

		public void Start()
		{
			int num = 180;
			m_PosPoints = new Vector2[num];
			m_OperationInfos = new string[num];
			ResetUserInputInfo();
			int num2 = Screen.width / 18;
			int num3 = Screen.height / 10;
			int num4 = 0;
			for (int i = 0; i < 10; i++)
			{
				for (int j = 0; j < 18; j++)
				{
					m_PosPoints[num4].x = num2 * (j + 1);
					m_PosPoints[num4].y = num3 * (i + 1);
					num4++;
				}
			}
		}

		public void Update()
		{
			if (isMusicFD && !isTriggerFD)
			{
				UpdateInput();
			}
		}

		public void ResetUserInputInfo()
		{
			for (int i = 0; i < 180; i++)
			{
				m_OperationInfos[i] = string.Empty;
			}
		}

		private void UpdateInput()
		{
			if (Input.GetMouseButtonDown(0))
			{
				SetHideBMSInfo(Input.mousePosition);
			}
			if (Input.GetMouseButton(0))
			{
				for (int i = 0; i < m_PosPoints.Length; i++)
				{
					SetHideBMSInfo(Input.mousePosition);
				}
			}
			if (Input.GetMouseButtonUp(0))
			{
				ResetUserInputInfo();
			}
			else
			{
				HideBMSLogic();
			}
		}

		private char SetPosTag(int index, Vector2 end)
		{
			if (Mathf.Abs(m_PosPoints[index].x - end.x) > Mathf.Abs(m_PosPoints[index].y - end.y))
			{
				if (m_PosPoints[index].x > end.x)
				{
					return '4';
				}
				return '2';
			}
			if (m_PosPoints[index].y > end.y)
			{
				return '3';
			}
			return '1';
		}

		private void HideBMSLogic()
		{
			if (isCircleData() && OnTriggerFREEDOMDIVE != null && isMusicFD)
			{
				OnTriggerFREEDOMDIVE(obj: true);
			}
		}

		private void SetHideBMSInfo(Vector2 end, bool isTouchTwo = false)
		{
			for (int i = 0; i < 180; i++)
			{
				if (isTouchTwo)
				{
					m_OperationInfosTouchTwo[i] = SetFDOperationInfo(SetPosTag(i, end), m_OperationInfosTouchTwo[i]);
				}
				else
				{
					m_OperationInfos[i] = SetFDOperationInfo(SetPosTag(i, end), m_OperationInfos[i]);
				}
			}
		}

		private bool isCircleData()
		{
			if (m_OperationInfos == null)
			{
				return false;
			}
			for (int i = 0; i < 180; i++)
			{
				for (int j = 0; j < m_CircleData.Length; j++)
				{
					if (m_OperationInfos[i].Contains(m_CircleData[j]))
					{
						return true;
					}
				}
			}
			return false;
		}
	}
}
