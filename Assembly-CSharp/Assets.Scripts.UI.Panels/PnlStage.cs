using Assets.Scripts.GameCore;
using Assets.Scripts.GameCore.Managers;
using Assets.Scripts.PeroTools.AssetBundles;
using Assets.Scripts.PeroTools.Commons;
using Assets.Scripts.PeroTools.Managers;
using Assets.Scripts.PeroTools.Nice.Components;
using Assets.Scripts.PeroTools.Nice.Datas;
using Assets.Scripts.PeroTools.Nice.Events;
using Assets.Scripts.PeroTools.Nice.Interface;
using Assets.Scripts.PeroTools.Platforms.Steam;
using Assets.Scripts.PeroTools.PreWarm;
using Assets.Scripts.PeroTools.UI;
using Assets.Scripts.UI.Controls;
using Assets.Scripts.UI.Specials;
using Newtonsoft.Json.Linq;
using Sirenix.OdinInspector;
using System;
using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;

namespace Assets.Scripts.UI.Panels
{
	public class PnlStage : VisBasePropertyModifier, IPreWarm
	{
		private delegate JToken GetData();

		private delegate int PointerAppend(int x, int cnt);

		[Title("Album", null, TitleAlignments.Left, true, true)]
		[Space(15f)]
		[Required]
		public GameObject pnlAlbum;

		[Required]
		public FancyScrollView albumFancyScrollView;

		[Required]
		public FancyScrollView albumFancyScrollViewNs;

		[Required]
		public AlbumCell albumCellPrefabs;

		[Required]
		public Button btnCloseAlbumBgs;

		[Required]
		public Button btnCloseAlbumSelect;

		[Required]
		public Button btnOwn;

		[Required]
		public Text titleOwn;

		[Required]
		public GameObject imgFavoriteMusic;

		[Title("Music", null, TitleAlignments.Left, true, true)]
		[Space(15f)]
		[Required]
		public FancyScrollView musicFancyScrollView;

		[Required]
		public Text musicNameTitle;

		[Required]
		public Text artistNameTitle;

		[Title("Anim", null, TitleAlignments.Left, true, true)]
		[Space(15f)]
		[Required]
		public GameObject musicRoot;

		[Required]
		public string animNameAlbumIn;

		[Required]
		public string animNameAlbumOut;

		[Title("Special(特殊逻辑使用)", null, TitleAlignments.Left, true, true)]
		public Button btnAhcievement;

		[Required]
		public Button btnEnter;

		[Required]
		public Button btnBackInPnlMenu;

		[Required]
		public Button btnPlay;

		[Required]
		public ShowText showText;

		[Required]
		public GameObject pnlPreparation;

		[Required]
		public Button btnBackToStage;

		public float demoMusicFadeInTime = 0.25f;

		public GameObject difficult1Lock;

		public GameObject difficult2Lock;

		[Required]
		public Text difficulty1;

		[Required]
		public Text difficulty2;

		[Required]
		public Text difficulty3;

		[Required]
		public GameObject difficulty1Easy;

		[Required]
		public GameObject difficulty2Hard;

		[Required]
		public GameObject difficulty3Empty;

		[Required]
		public GameObject difficulty3Lock;

		[Required]
		public GameObject difficulty3Master;

		[Required]
		public GameObject emptyEasy;

		[Required]
		public GameObject emptyMaster;

		[Required]
		public Toggle tglLike;

		[Required]
		public GameObject stageAchievementPercent;

		[Required]
		public GameObject nsNoCollectionTip;

		[Required]
		public GameObject bgAlbumLock;

		[Required]
		public GameObject bgAlbumFree;

		[Required]
		public GameObject txtNotPurchase;

		[Required]
		public GameObject txtFreeSong;

		[Required]
		public GameObject txtFreeSongNoPurchase;

		[Required]
		public GameObject txtBudgetIsBurning15;

		[Required]
		public GameObject txtBudgetIsBurning30;

		[Required]
		public Toggle tglDifficultyEasy;

		[Required]
		public Toggle tglDifficultyHard;

		[Required]
		public Toggle tglDifficultyMaster;

		[Required]
		public Button btnMenuBackToStage;

		[Required]
		public GameObject[] pnlPreparationObjs;

		private IVariable m_VarCurSelectedAlbumUid;

		private IVariable m_VarCurSelectedMusicIndex;

		private Animator m_MusicRootAnimator;

		private List<string> m_Collection;

		private Coroutine m_Nume;

		private bool m_CanPlayMusic = true;

		private int m_CurrentHighlighAlbumIndex;

		private CoroutineManager m_CoroutineManager;

		private Effect m_ParTglLikes;

		private Effect m_ParTglDisLikes;

		private Dictionary<string, GetData>[] m_AlbumDatas;

		private static string[] m_HalloweenDifficulty;

		private static string m_GOODTEKDifficulty;

		private int m_TrggleNanoCoreCount;

		private bool m_IsInitNanoCoreCount;

		public static bool m_IsOnNanoCoreAudio;

		private static string[] m_NanoAuthorName;

		private int m_ToggleHideBMSCount;

		private bool m_IsHideBMSCount;

		private static string m_MopemopeDifficulty;

		public static bool isOnMopemope;

		public Font specialFont;

		public Font simpleFont;

		private static string m_INFiNiTEENERZYDifficulty;

		public static bool isOnINFiNiTEENERZY;

		public HideBMS hideBMSUnlockLogic = new HideBMS();

		private bool m_IsLock;

		private bool isCollection => selectedAlbumUid == "collections";

		public int selectedAlbumIndex => GetAlbumIndexByUid(m_VarCurSelectedAlbumUid.GetResult<string>());

		public string selectedAlbumUid => m_VarCurSelectedAlbumUid.GetResult<string>();

		public int selectedMusicIndex => m_VarCurSelectedMusicIndex.GetResult<int>();

		public string selectedMusicUid
		{
			get
			{
				if (isCollection)
				{
					List<string> result = Singleton<DataManager>.instance["Account"]["Collections"].GetResult<List<string>>();
					if (selectedMusicIndex < result.Count)
					{
						return result[selectedMusicIndex];
					}
					return string.Empty;
				}
				return Singleton<DataManager>.instance["Account"]["SelectedMusicUid"].GetResult<string>();
			}
		}

		public int GetAlbumIndexByUid(string uid)
		{
			for (int i = 0; i < m_AlbumDatas.Length; i++)
			{
				if (GetAlbumUid(i) == uid)
				{
					return i;
				}
			}
			return -1;
		}

		public string GetAlbumUid(int albumIndex)
		{
			if (m_AlbumDatas[albumIndex] == null)
			{
				Debug.Log("Null");
			}
			return (string)m_AlbumDatas[albumIndex]["uid"]();
		}

		public string GetAlbumTitle(int albumIndex)
		{
			return (string)m_AlbumDatas[albumIndex]["title"]();
		}

		public bool GetAlbumNeedPurchase(int albumIndex)
		{
			return (bool)m_AlbumDatas[albumIndex]["needPurchase"]();
		}

		public float GetAlbumPrice(int albumIndex)
		{
			return (float)m_AlbumDatas[albumIndex]["price"]();
		}

		public string GetSelectedMusicAlbumJsonName()
		{
			string selectedMusicUid = this.selectedMusicUid;
			if (string.IsNullOrEmpty(selectedMusicUid))
			{
				return string.Empty;
			}
			if (isCollection)
			{
				return $"ALBUM{int.Parse(selectedMusicUid.Split('-')[0]) + 1}";
			}
			return GetAlbumJsonName(selectedAlbumIndex);
		}

		public string GetAlbumJsonName(int albumIndex)
		{
			return (string)m_AlbumDatas[albumIndex]["jsonName"]();
		}

		public string GetAlbumPrefabsName(int albumIndex)
		{
			return (string)m_AlbumDatas[albumIndex]["prefabsName"]();
		}

		public string GetMusicUid(int musidIndex)
		{
			if (isCollection)
			{
				List<string> result = Singleton<DataManager>.instance["Account"]["Collections"].GetResult<List<string>>();
				int result2 = m_VarCurSelectedMusicIndex.GetResult<int>();
				if (result2 < result.Count)
				{
					return result[result2];
				}
				return string.Empty;
			}
			return Singleton<DataManager>.instance["Account"]["SelectedMusicUid"].GetResult<string>();
		}

		public void PlayCurrentSelectedMusicIfNeed()
		{
			if (musicRoot.activeSelf)
			{
				if (IsSelectedMusicUnlock())
				{
					PlayMusic();
				}
				else
				{
					Singleton<AudioManager>.instance.StopBGM();
				}
			}
		}

		public override void SetProperty(float propertyValue)
		{
			OnFancyScrollViewCellUpdate cell = musicFancyScrollView.GetCell(m_VarCurSelectedMusicIndex.GetResult<int>());
			if (cell != null)
			{
				if (musicFancyScrollView.state == FancyScrollView.State.Static)
				{
					VisPropertyHelper.SetGameObjectProperty(cell.transform.GetChild(0).gameObject, GameObjectProperty.UniformScale, propertyValue);
				}
				else
				{
					cell.transform.GetChild(0).localScale = Vector3.one;
				}
			}
		}

		public void SwitchToAlbum(int albumIndex)
		{
			bool flag = Singleton<WeekFreeManager>.instance.freeAlbumUids != null;
			bool flag2 = false;
			if (flag)
			{
				List<int> list = new List<int>(Singleton<WeekFreeManager>.instance.freeAlbumIndexs);
				if (albumIndex > list.Count + 1)
				{
					albumIndex -= list.Count;
					list.Sort((int a, int b) => b - a);
					for (int i = 0; i < list.Count; i++)
					{
						if (m_AlbumDatas.Length - list[i] <= albumIndex)
						{
							albumIndex++;
						}
					}
				}
				else if (albumIndex >= 2 && albumIndex <= list.Count + 1)
				{
					int num = list[albumIndex - 2];
					albumIndex = m_AlbumDatas.Length - num;
					if (m_VarCurSelectedAlbumUid.GetResult<string>() != GetAlbumUid(albumIndex))
					{
						int switchSongIndex = 0;
						for (int j = 0; j < Singleton<WeekFreeManager>.instance.freeSongUids.Length; j++)
						{
							string str = Singleton<WeekFreeManager>.instance.freeSongUids[j];
							if (str.BeginBefore('-') == num.ToString())
							{
								switchSongIndex = int.Parse(str.LastAfter('-'));
								break;
							}
						}
						flag2 = true;
						m_VarCurSelectedMusicIndex.SetResult(switchSongIndex);
						SingletonMonoBehaviour<CoroutineManager>.instance.Delay(delegate
						{
							musicFancyScrollView.ScrollToDataIndex(switchSongIndex, 0f);
						}, 1);
					}
				}
			}
			if (albumIndex >= 0 && albumIndex < m_AlbumDatas.Length && albumIndex != selectedAlbumIndex)
			{
				m_VarCurSelectedAlbumUid.result = GetAlbumUid(albumIndex);
				PlayAlbumSwitchAnim(isFadeIn: true);
				if (!flag || albumIndex == 1)
				{
					albumFancyScrollViewNs.ScrollTo(albumIndex, albumFancyScrollViewNs.switchDuration);
				}
				if (!flag2)
				{
					m_VarCurSelectedMusicIndex.SetResult(0);
				}
				RefreshMusicFSV();
			}
		}

		private void SetTitleOwnText(int albumIndex)
		{
			titleOwn.text = GetAlbumTitle(albumIndex);
		}

		public void SetMouseWheelBinding(FancyScrollView fsv, bool enable)
		{
			InputKeyBinding[] components = fsv.btnPrevious.GetComponents<InputKeyBinding>();
			InputKeyBinding[] array = components;
			foreach (InputKeyBinding inputKeyBinding in array)
			{
				if (inputKeyBinding.axisName == "MouseAxis3")
				{
					inputKeyBinding.enabled = enable;
				}
			}
			components = fsv.btnNext.GetComponents<InputKeyBinding>();
			InputKeyBinding[] array2 = components;
			foreach (InputKeyBinding inputKeyBinding2 in array2)
			{
				if (inputKeyBinding2.axisName == "MouseAxis3")
				{
					inputKeyBinding2.enabled = enable;
				}
			}
		}

		public void SetHalloweenDifficulty()
		{
			if (DateTime.Now.Month == 11 && DateTime.Now.Day == 1 && Singleton<DataManager>.instance["Account"]["SelectedMusicUid"].GetResult<string>() == "8-3")
			{
				if (m_HalloweenDifficulty == null)
				{
					m_HalloweenDifficulty = new string[3];
					m_HalloweenDifficulty[0] = (string)Singleton<ConfigManager>.instance.GetJson("ALBUM9", localization: false)[3]["difficulty1"];
					m_HalloweenDifficulty[1] = (string)Singleton<ConfigManager>.instance.GetJson("ALBUM9", localization: false)[3]["difficulty2"];
					m_HalloweenDifficulty[2] = (string)Singleton<ConfigManager>.instance.GetJson("ALBUM9", localization: false)[3]["difficulty3"];
				}
				Singleton<ConfigManager>.instance.GetJson("ALBUM9", localization: false)[3]["difficulty1"] = "0";
				Singleton<ConfigManager>.instance.GetJson("ALBUM9", localization: false)[3]["difficulty2"] = "?";
				Singleton<ConfigManager>.instance.GetJson("ALBUM9", localization: false)[3]["difficulty3"] = "0";
			}
			else if (m_HalloweenDifficulty != null)
			{
				Singleton<ConfigManager>.instance.GetJson("ALBUM9", localization: false)[3]["difficulty1"] = m_HalloweenDifficulty[0];
				Singleton<ConfigManager>.instance.GetJson("ALBUM9", localization: false)[3]["difficulty2"] = m_HalloweenDifficulty[1];
				Singleton<ConfigManager>.instance.GetJson("ALBUM9", localization: false)[3]["difficulty3"] = m_HalloweenDifficulty[2];
			}
		}

		public void PreWarm(int slice)
		{
			switch (slice)
			{
			case 0:
				InitAlbumDatas();
				albumFancyScrollView.onFinalItemIndexChange += ChangeFinalAlbum;
				if (albumFancyScrollViewNs != null)
				{
					albumFancyScrollViewNs.onFinalItemIndexChange += ChangeFinalAblumNs;
				}
				m_MusicRootAnimator = musicRoot.GetComponent<Animator>();
				pnlAlbum.GetOrAddComponent<EnableDisableHooker>().onEnable += OnPnlAlbumEnable;
				musicRoot.GetOrAddComponent<EnableDisableHooker>().onEnable += OnPnlMusicEnable;
				musicFancyScrollView.onFinalItemIndexChange += ChangeFinalMusic;
				musicFancyScrollView.onItemIndexChange += ChangeMusic;
				musicFancyScrollView.OnStateChange += DisableBtnPlayWhenDynamic;
				btnPlay.onClick.AddListener(OnClickBtnPlay);
				btnEnter.onClick.AddListener(delegate
				{
					m_Nume = SingletonMonoBehaviour<CoroutineManager>.instance.StartCoroutine(EnableAndPlayMusic1SecLater());
				});
				tglLike.onValueChanged.AddListener(OnClickBtnLike);
				btnCloseAlbumBgs.onClick.AddListener(OnClickCloseAlbumBtn);
				btnBackInPnlMenu.onClick.AddListener(ReplayMusicWhenFormPnlMenuToPnlStage);
				break;
			case 1:
			{
				m_Collection = Singleton<DataManager>.instance["Account"]["Collections"].GetResult<List<string>>();
				EffectManager instance = Singleton<EffectManager>.instance;
				string uid = "ParTglLike";
				Transform transform = tglLike.transform;
				m_ParTglLikes = instance.Preload(uid, 5, -1, transform);
				EffectManager instance2 = Singleton<EffectManager>.instance;
				uid = "ParTglDisLike";
				transform = tglLike.transform;
				m_ParTglDisLikes = instance2.Preload(uid, 5, -1, transform);
				m_VarCurSelectedMusicIndex = Singleton<DataManager>.instance["Account"]["SelectedMusicIndex"];
				m_VarCurSelectedAlbumUid = Singleton<DataManager>.instance["Account"]["SelectedAlbumUid"];
				break;
			}
			case 2:
				m_CoroutineManager = SingletonMonoBehaviour<CoroutineManager>.instance;
				break;
			}
		}

		private void InitAlbumDatas()
		{
			string albumsJsonName = "albums";
			int count = Singleton<ConfigManager>.instance[albumsJsonName].Count;
			int num = 0;
			PointerAppend pointerAppend = delegate(int i, int cnt)
			{
				int num2 = (i + 1) % cnt;
				if (num2 == 1)
				{
					num2++;
				}
				return num2;
			};
			m_AlbumDatas = new Dictionary<string, GetData>[count];
			for (int j = 1; j < m_AlbumDatas.Length; j++)
			{
				int index = num;
				string jsonName = Singleton<ConfigManager>.instance.GetConfigStringValue(albumsJsonName, index, "jsonName");
				if (!string.IsNullOrEmpty(jsonName))
				{
					m_AlbumDatas[j] = new Dictionary<string, GetData>
					{
						{
							"uid",
							() => Singleton<ConfigManager>.instance.GetConfigStringValue(albumsJsonName, index, "uid")
						},
						{
							"title",
							() => Singleton<ConfigManager>.instance.GetConfigStringValue(albumsJsonName, index, "title")
						},
						{
							"prefabsName",
							() => Singleton<ConfigManager>.instance.GetConfigStringValue(albumsJsonName, index, "prefabsName")
						},
						{
							"price",
							() => Singleton<ConfigManager>.instance.GetConfigStringValue(albumsJsonName, index, "price")
						},
						{
							"jsonName",
							() => jsonName
						},
						{
							"needPurchase",
							() => Singleton<ConfigManager>.instance.GetConfigStringValue(albumsJsonName, index, "needPurchase")
						}
					};
				}
				num = pointerAppend(num, count);
			}
			m_AlbumDatas[0] = new Dictionary<string, GetData>
			{
				{
					"uid",
					() => "collections"
				},
				{
					"title",
					() => Singleton<ConfigManager>.instance.GetConfigStringValue("tip", 0, "collections")
				},
				{
					"needPurchase",
					() => false
				},
				{
					"jsonName",
					() => string.Empty
				},
				{
					"prefabsName",
					() => "AlbumCollection"
				}
			};
		}

		public override void Start()
		{
			base.Start();
			InitHideBMSLogic();
			InitNacoMusicAudioLogic();
			if (hideBMSUnlockLogic.OnTriggerFREEDOMDIVE == null)
			{
				hideBMSUnlockLogic.Start();
				hideBMSUnlockLogic.OnTriggerFREEDOMDIVE = SetTriggerFDDifficulty;
				btnBackToStage.onClick.AddListener(delegate
				{
					hideBMSUnlockLogic.isMusicFD = false;
				});
			}
			if (HideBMS.isTriggerFD)
			{
				hideBMSUnlockLogic.OnTriggerFREEDOMDIVE(obj: false);
			}
			SetAprilFoolsDayDifficulty();
			RefreshAlbumFSV();
			RefreshMusicFSV();
		}

		private void OnEnable()
		{
			if (Singleton<DataManager>.instance["IAP"]["unlockall_0"].GetResult<bool>())
			{
				Singleton<ItemManager>.instance.AddExtraItem("loading", 14, 5);
			}
			Singleton<ItemManager>.instance.CheckAndAddWelcome(0, 5, showTips: false);
			Singleton<ItemManager>.instance.CheckAndAddWelcome(1, 5, showTips: false);
			Singleton<ItemManager>.instance.CheckAndAddWelcome(2, 5, showTips: false);
			Singleton<DataManager>.instance.Save();
		}

		private void OnPnlAlbumEnable(GameObject obj)
		{
			RefreshAlbumFSV();
		}

		private void OnPnlMusicEnable(GameObject obj)
		{
			RefreshMusicFSV();
		}

		private void OnDisable()
		{
			m_CanPlayMusic = true;
			if (m_CoroutineManager != null && m_Nume != null)
			{
				m_CoroutineManager.StopCoroutine(m_Nume);
				m_Nume = null;
			}
		}

		private void OnClickBtnPlay()
		{
			if (selectedMusicUid == "22-1")
			{
				Input.multiTouchEnabled = true;
				if (IsSelectedMusicPurchased())
				{
					hideBMSUnlockLogic.isMusicFD = true;
				}
			}
			else
			{
				hideBMSUnlockLogic.isMusicFD = false;
			}
			SetHalloweenDifficulty();
			SetAprilFoolsDayDifficulty();
			SingletonMonoBehaviour<SteamManager>.instance.DLCVertify();
			base.gameObject.SetActive(value: false);
			base.gameObject.SetActive(value: true);
			if (GetSelectedMusicAlbumJsonName() == "ALBUM7" && Singleton<DataManager>.instance["Account"]["Level"].GetResult<int>() < 30)
			{
				Singleton<EventManager>.instance.Invoke("UI/PnlBudgetIsBurning");
				return;
			}
			if (GetSelectedMusicAlbumJsonName() == "ALBUM22" && Singleton<DataManager>.instance["Account"]["Level"].GetResult<int>() < 15)
			{
				Singleton<EventManager>.instance.Invoke("UI/PnlBudgetIsBurningNanoCore");
				return;
			}
			bool flag = false;
			flag = true;
			bool flag2 = IsSelectedMusicPurchased();
			bool flag3 = Singleton<WeekFreeManager>.instance.freeSongUids.Contains(selectedMusicUid);
			if (IsCanPreparationOut() || flag3)
			{
				pnlPreparation.gameObject.SetActive(value: true);
				btnBackToStage.gameObject.SetActive(value: true);
				txtFreeSong.SetActive(value: false);
				GetComponent<Animator>().Play("StageToPreparation");
			}
			else if (flag)
			{
				if (!Singleton<DataManager>.instance["IAP"]["unlockall_0"].GetResult<bool>() && GetSelectedMusicAlbumJsonName() != "ALBUM1")
				{
					SingletonMonoBehaviour<SteamManager>.instance.DLCVertify();
					base.gameObject.SetActive(value: false);
					base.gameObject.SetActive(value: true);
					Singleton<EventManager>.instance.Invoke("UI/PnlNoPunchesAsk");
				}
				else
				{
					showText.Show(Singleton<ConfigManager>.instance.GetConfigStringValue("tip", 0, "songLock"));
				}
			}
			else
			{
				showText.Show(Singleton<ConfigManager>.instance.GetConfigStringValue("tip", 0, flag2 ? "songLock" : "musicNotPurchased"));
			}
		}

		public bool IsCanPreparationOut()
		{
			bool flag = isCollection && selectedMusicIndex >= Singleton<DataManager>.instance["Account"]["Collections"].GetResult<List<string>>().Count;
			bool flag2 = IsSelectedMusicPurchased();
			if (GetSelectedMusicAlbumJsonName() == "ALBUM7" && Singleton<DataManager>.instance["Account"]["Level"].GetResult<int>() < 30)
			{
				return false;
			}
			if (GetSelectedMusicAlbumJsonName() == "ALBUM22" && Singleton<DataManager>.instance["Account"]["Level"].GetResult<int>() < 15)
			{
				return false;
			}
			return IsSelectedMusicUnlock() && !flag && flag2;
		}

		private void OnClickCloseAlbumBtn()
		{
			AlbumCell component = albumFancyScrollView.GetCell(m_CurrentHighlighAlbumIndex).GetComponent<AlbumCell>();
			if (!(component == null))
			{
				if (!component.isLock())
				{
					btnCloseAlbumSelect.onClick.Invoke();
					SwitchToAlbum(component.GetDataIndex());
				}
				else if (component.uid == "collections")
				{
					Singleton<EventManager>.instance.Invoke("UI/OnCollectionsTip");
				}
				else
				{
					showText.Show(Singleton<ConfigManager>.instance.GetConfigStringValue("tip", 0, "dlcInavaiable"));
				}
			}
		}

		private void OnClickBtnLike(bool like)
		{
			if (like)
			{
				if (!m_Collection.Contains(selectedMusicUid))
				{
					m_ParTglLikes.CreateInstance();
					m_Collection.Add(selectedMusicUid);
					Singleton<AudioManager>.instance.PlayOneShot("sfx_count_finish", Singleton<DataManager>.instance["GameConfig"]["SfxVolume"].GetResult<float>());
					Singleton<DataManager>.instance.Save();
				}
			}
			else
			{
				if (!m_Collection.Contains(selectedMusicUid))
				{
					return;
				}
				m_Collection.Remove(selectedMusicUid);
				Singleton<AudioManager>.instance.PlayOneShot("sfx_common_back", Singleton<DataManager>.instance["GameConfig"]["SfxVolume"].GetResult<float>());
				m_ParTglDisLikes.CreateInstance();
				if (isCollection)
				{
					if (m_Collection.Count == 0)
					{
						SetPnlNoCollectionEnable(enable: true);
					}
					else
					{
						m_VarCurSelectedMusicIndex.SetResult((selectedMusicIndex > 1) ? (selectedMusicIndex - 1) : 0);
						PlayAlbumSwitchAnim(isFadeIn: true);
						RefreshMusicFSV();
					}
				}
				Singleton<DataManager>.instance.Save();
			}
		}

		public int GetHideBMS()
		{
			int result = Singleton<DataManager>.instance["Account"]["SelectedDifficulty"].GetResult<int>();
			if (DateTime.Now.Month == 4 && DateTime.Now.Day == 1 && Singleton<DataManager>.instance["Account"]["SelectedMusicUid"].GetResult<string>() == "4-5" && result == 2)
			{
				return 4;
			}
			if (DateTime.Now.Month == 11 && DateTime.Now.Day == 1 && Singleton<DataManager>.instance["Account"]["SelectedMusicUid"].GetResult<string>() == "8-3")
			{
				return 4;
			}
			if (Singleton<DataManager>.instance["Account"]["SelectedMusicUid"].GetResult<string>() == "22-1" && result == 3 && HideBMS.isTriggerFD)
			{
				return 4;
			}
			if (Singleton<DataManager>.instance["Account"]["SelectedMusicUid"].GetResult<string>() == "0-45" && result == 3 && isOnMopemope)
			{
				return 4;
			}
			if (Singleton<DataManager>.instance["Account"]["SelectedMusicUid"].GetResult<string>() == "20-2" && result == 3 && isOnINFiNiTEENERZY)
			{
				return 4;
			}
			return result;
		}

		private void RefreshAlbumFSV()
		{
			if (Singleton<WeekFreeManager>.instance.freeSongUids != null)
			{
				string[] freeAlbumUids = Singleton<WeekFreeManager>.instance.freeAlbumUids;
				if (freeAlbumUids.Contains(selectedAlbumUid))
				{
					albumFancyScrollViewNs.startIndex.SetResult(2 + freeAlbumUids.IndexOf(selectedAlbumUid));
				}
				else if (selectedAlbumIndex < 2)
				{
					albumFancyScrollViewNs.startIndex.SetResult(selectedAlbumIndex);
				}
				else
				{
					int[] freeAlbumIndexs = Singleton<WeekFreeManager>.instance.freeAlbumIndexs;
					int num = 0;
					int num2 = int.Parse(selectedAlbumUid.LastAfter('_'));
					foreach (int num3 in freeAlbumIndexs)
					{
						if (num2 > num3)
						{
							num++;
						}
					}
					albumFancyScrollViewNs.startIndex.SetResult(selectedAlbumIndex + num);
				}
			}
			else
			{
				albumFancyScrollViewNs.startIndex.SetResult(selectedAlbumIndex);
			}
			albumFancyScrollViewNs.Rebuild();
			SetHalloweenDifficulty();
		}

		private void RefreshMusicFSV()
		{
			SetBgLockAction();
			imgFavoriteMusic.SetActive(isCollection);
			string albumJsonName = GetAlbumJsonName(selectedAlbumIndex);
			List<string> list = new List<string>();
			List<string> result = Singleton<DataManager>.instance["Account"]["Collections"].GetResult<List<string>>();
			for (int i = 0; i < result.Count; i++)
			{
				string text = result[i];
				if (int.Parse(text.BeginBefore('-')) <= GameInit.maxAlbumIndex)
				{
					list.Add(text);
				}
			}
			int num = (!string.IsNullOrEmpty(albumJsonName)) ? Singleton<ConfigManager>.instance.GetJson(albumJsonName, localization: false).Count : list.Count;
			string albumPrefabsName = GetAlbumPrefabsName(selectedAlbumIndex);
			GameObject prefabs = Singleton<AssetBundleManager>.instance.LoadFromName<GameObject>(albumPrefabsName);
			musicFancyScrollView.startIndex.SetResult(selectedMusicIndex);
			musicFancyScrollView.cellCount.result = num;
			musicFancyScrollView.Rebuild(prefabs);
			SetTitleOwnText(selectedAlbumIndex);
		}

		private void SetBgLockAction()
		{
			if (bgAlbumLock == null)
			{
				return;
			}
			string result = Singleton<DataManager>.instance["Account"]["SelectedAlbumUid"].GetResult<string>();
			string selectedMusicAlbumJsonName = GetSelectedMusicAlbumJsonName();
			bool flag = Singleton<DataManager>.instance["Account"]["Level"].GetResult<int>() >= 15;
			bool flag2 = Singleton<DataManager>.instance["Account"]["Level"].GetResult<int>() >= 30;
			bool flag3;
			bool active;
			bool active2;
			if (selectedMusicAlbumJsonName == "ALBUM22" && result != "collections")
			{
				flag3 = (!flag && !BtnIAP.IsUnlockAll());
				active = flag3;
				active2 = false;
			}
			else if (selectedMusicAlbumJsonName == "ALBUM7" && result != "collections")
			{
				flag3 = (!flag2 && !BtnIAP.IsUnlockAll());
				active = flag3;
				active2 = false;
			}
			else if (result == "collections" || result == "music_package_0")
			{
				flag3 = false;
				active = false;
				active2 = false;
			}
			else
			{
				bool result2 = Singleton<DataManager>.instance["IAP"]["unlockall_0"].GetResult<bool>();
				flag3 = !result2;
				active2 = !result2;
				active = false;
			}
			if (Singleton<WeekFreeManager>.instance.freeAlbumUids != null && Singleton<WeekFreeManager>.instance.freeAlbumUids.Contains(result))
			{
				flag3 = false;
				active2 = false;
				active = false;
				bgAlbumFree.SetActive(value: true);
				bgAlbumFree.GetComponent<Animator>().Play("BgAlbumIn", 0);
			}
			else
			{
				bgAlbumFree.SetActive(value: false);
			}
			if (flag3)
			{
				bgAlbumLock.SetActive(value: true);
				bgAlbumLock.GetComponent<Animator>().Play("BgAlbumIn", 0);
				m_IsLock = false;
			}
			else if (!m_IsLock)
			{
				m_IsLock = true;
				if (bgAlbumLock.activeSelf)
				{
					bgAlbumLock.GetComponent<Animator>().Play("BgAlbumOut", 0);
				}
			}
			else
			{
				bgAlbumLock.SetActive(value: false);
			}
			txtNotPurchase.SetActive(active2);
			if (!flag2 && selectedMusicAlbumJsonName == "ALBUM7")
			{
				txtBudgetIsBurning15.SetActive(value: false);
				txtBudgetIsBurning30.SetActive(active);
			}
			else if (!flag && selectedMusicAlbumJsonName == "ALBUM22")
			{
				txtBudgetIsBurning15.SetActive(active);
				txtBudgetIsBurning30.SetActive(value: false);
			}
			else
			{
				txtBudgetIsBurning15.SetActive(value: false);
				txtBudgetIsBurning30.SetActive(value: false);
			}
		}

		private void PlayMusic()
		{
			if (!m_CanPlayMusic)
			{
				Singleton<AudioManager>.instance.StopBGM();
				return;
			}
			string selectedMusicAlbumJsonName = GetSelectedMusicAlbumJsonName();
			if (!string.IsNullOrEmpty(selectedMusicAlbumJsonName))
			{
				string text = (!isCollection) ? Singleton<ConfigManager>.instance.GetConfigStringValue(selectedMusicAlbumJsonName, selectedMusicIndex, "demo") : Singleton<ConfigManager>.instance.GetConfigStringValue(selectedMusicAlbumJsonName, "uid", "demo", selectedMusicUid);
				if (selectedMusicUid.Contains("21-2") && m_IsOnNanoCoreAudio)
				{
					text += "2";
				}
				Singleton<AudioManager>.instance.PlayBGM(text, demoMusicFadeInTime);
			}
		}

		private void PlayAlbumSwitchAnim(bool isFadeIn)
		{
			if (m_MusicRootAnimator != null)
			{
				if (isFadeIn)
				{
					m_MusicRootAnimator.Play(animNameAlbumIn);
				}
				else
				{
					m_MusicRootAnimator.Play(animNameAlbumOut);
				}
			}
		}

		private IEnumerator EnableAndPlayMusic1SecLater()
		{
			m_CanPlayMusic = false;
			yield return new WaitForSeconds(1f);
			m_CanPlayMusic = true;
			PlayCurrentSelectedMusicIfNeed();
		}

		private bool IsSelectedMusicPurchased()
		{
			if (!IsSelectedAlbumsAvailiable())
			{
				return true;
			}
			string result = Singleton<DataManager>.instance["Account"]["SelectedMusicUid"].GetResult<string>();
			string a = $"music_package_{result.BeginBefore('-')}";
			if (a == "music_package_6" && Singleton<DataManager>.instance["Account"]["Level"].GetResult<int>() >= 30)
			{
				return true;
			}
			if (a == "music_package_21" && Singleton<DataManager>.instance["Account"]["Level"].GetResult<int>() >= 15)
			{
				return true;
			}
			if (a == "music_package_0")
			{
				return true;
			}
			return BtnIAP.IsUnlockAll();
		}

		private bool IsSelectedMusicUnlock()
		{
			if (isCollection)
			{
				return selectedMusicIndex < Singleton<DataManager>.instance["Account"]["Collections"].GetResult<List<string>>().Count;
			}
			return Singleton<DataManager>.instance["Account"]["IsSelectedMusicUnlock"].GetResult<bool>();
		}

		private void DisableBtnPlayWhenDynamic(FancyScrollView.State state)
		{
			bool interactable = (state == FancyScrollView.State.Static) ? true : false;
			btnPlay.interactable = interactable;
		}

		private void ChangeFinalAlbum(int i)
		{
			AlbumCell component = albumFancyScrollView.GetCell(i).GetComponent<AlbumCell>();
			if (component != null)
			{
				m_CurrentHighlighAlbumIndex = i;
			}
		}

		private void ChangeFinalMusic(int i)
		{
			PlayCurrentSelectedMusicIfNeed();
		}

		private bool IsSelectedAlbumsAvailiable()
		{
			string selectedMusicAlbumJsonName = GetSelectedMusicAlbumJsonName();
			return Singleton<ConfigManager>.instance[selectedMusicAlbumJsonName] != null;
		}

		private void ChangeMusic(int i)
		{
			Singleton<DataManager>.instance["Account"]["SelectedMusicIndex"].SetResult(i);
			string selectedMusicAlbumJsonName = GetSelectedMusicAlbumJsonName();
			string text = "?????";
			string text2 = "???";
			string text3 = "0";
			string text4 = "0";
			string text5 = "0";
			string text6 = string.Empty;
			List<string> result = Singleton<DataManager>.instance["Account"]["Collections"].GetResult<List<string>>();
			if (!isCollection)
			{
				text = Singleton<ConfigManager>.instance.GetConfigStringValue(selectedMusicAlbumJsonName, i, "name");
				text2 = Singleton<ConfigManager>.instance.GetConfigStringValue(selectedMusicAlbumJsonName, i, "author");
				text3 = Singleton<ConfigManager>.instance.GetConfigStringValue(selectedMusicAlbumJsonName, i, "difficulty1");
				text4 = Singleton<ConfigManager>.instance.GetConfigStringValue(selectedMusicAlbumJsonName, i, "difficulty2");
				text5 = Singleton<ConfigManager>.instance.GetConfigStringValue(selectedMusicAlbumJsonName, i, "difficulty3");
				text6 = Singleton<ConfigManager>.instance.GetConfigStringValue(selectedMusicAlbumJsonName, i, "uid");
			}
			else if (i < result.Count)
			{
				text6 = result[i];
				if (IsSelectedAlbumsAvailiable())
				{
					text = Singleton<ConfigManager>.instance.GetConfigStringValue(selectedMusicAlbumJsonName, "uid", "name", text6);
					text2 = Singleton<ConfigManager>.instance.GetConfigStringValue(selectedMusicAlbumJsonName, "uid", "author", text6);
					text3 = Singleton<ConfigManager>.instance.GetConfigStringValue(selectedMusicAlbumJsonName, "uid", "difficulty1", text6);
					text4 = Singleton<ConfigManager>.instance.GetConfigStringValue(selectedMusicAlbumJsonName, "uid", "difficulty2", text6);
					text5 = Singleton<ConfigManager>.instance.GetConfigStringValue(selectedMusicAlbumJsonName, "uid", "difficulty3", text6);
				}
			}
			musicNameTitle.text = text;
			artistNameTitle.text = text2;
			difficulty1.text = text3;
			difficulty2.text = text4;
			difficulty3.text = text5;
			txtFreeSong.SetActive(Singleton<WeekFreeManager>.instance.freeSongUids.Contains(text6));
			txtFreeSongNoPurchase.SetActive(!Singleton<WeekFreeManager>.instance.freeSongUids.Contains(text6) && !IsSelectedMusicPurchased());
			emptyEasy.SetActive(difficulty1.text == "0" && IsSelectedMusicUnlock());
			emptyMaster.SetActive(difficulty3.text == "0");
			bool result2 = Singleton<DataManager>.instance["Account"]["IsSelectedUnlockMaster"].GetResult<bool>();
			difficulty3.transform.parent.gameObject.SetActive(result2);
			bool flag = difficulty3.text != "0";
			difficulty3Empty.SetActive(!flag);
			difficulty3Lock.SetActive(flag && !result2);
			difficulty1Easy.SetActive(difficulty1.text != "0");
			difficulty2Hard.SetActive(difficulty2.text != "0");
			difficulty3Master.SetActive(flag && !difficulty3Lock.activeSelf);
			bool flag2 = IsSelectedMusicUnlock();
			tglLike.interactable = flag2;
			tglLike.isOn = result.Contains(text6);
			if (flag2)
			{
				KnowMusic();
			}
			else
			{
				UnknowMusic();
			}
		}

		private void UnknowMusic()
		{
			musicNameTitle.text = "?????";
			artistNameTitle.text = "???";
			difficulty1.gameObject.SetActive(value: false);
			difficulty2.gameObject.SetActive(value: false);
			difficulty3.gameObject.SetActive(value: false);
			btnAhcievement.interactable = false;
			difficult1Lock.SetActive(value: true);
			difficult2Lock.SetActive(value: true);
			difficulty3Lock.SetActive(value: true);
			difficulty3Empty.SetActive(value: false);
		}

		private void KnowMusic()
		{
			difficulty1.gameObject.SetActive(value: true);
			difficulty2.gameObject.SetActive(value: true);
			difficulty3.gameObject.SetActive(value: true);
			btnAhcievement.interactable = true;
			difficult1Lock.SetActive(value: false);
			difficult2Lock.SetActive(value: false);
		}

		private void ReplayMusicWhenFormPnlMenuToPnlStage()
		{
			PlayMusic();
		}

		private void ChangeFinalAblumNs(int i)
		{
			if (i == 0)
			{
				List<string> result = Singleton<DataManager>.instance["Account"]["Collections"].GetResult<List<string>>();
				SetPnlNoCollectionEnable(result.Count == 0);
			}
			else
			{
				SetPnlNoCollectionEnable(enable: false);
			}
			SwitchToAlbum(i);
		}

		private void SetPnlNoCollectionEnable(bool enable)
		{
			nsNoCollectionTip.SetActive(enable);
			tglLike.GetComponent<InputKeyBinding>().enabled = !enable;
			tglLike.graphic.gameObject.SetActive(!enable);
			tglLike.enabled = !enable;
			btnPlay.gameObject.SetActive(!enable);
			musicFancyScrollView.btnPrevious.gameObject.SetActive(!enable);
			musicFancyScrollView.btnNext.gameObject.SetActive(!enable);
			musicFancyScrollView.processBar.gameObject.SetActive(!enable);
			musicFancyScrollView.gameObject.SetActive(!enable);
			if (enable)
			{
				UnknowMusic();
				stageAchievementPercent.SetActive(value: false);
			}
		}

		private void SetTriggerFDDifficulty(bool isTrigger)
		{
			HideBMS.isTriggerFD = isTrigger;
			if (isTrigger)
			{
				if (HideBMS.hideDifficultyFD == null)
				{
					HideBMS.hideDifficultyFD = (string)Singleton<ConfigManager>.instance.GetJson("ALBUM23", localization: false)[1]["difficulty3"];
				}
				Singleton<ConfigManager>.instance.GetJson("ALBUM23", localization: false)[1]["difficulty3"] = (string)Singleton<ConfigManager>.instance.GetJson("ALBUM23", localization: false)[1]["difficulty4"];
				if (HideBMS.designerNameFD == null)
				{
					HideBMS.designerNameFD = (string)Singleton<ConfigManager>.instance.GetJson("ALBUM23", localization: false)[1]["levelDesigner3"];
				}
				Singleton<ConfigManager>.instance.GetJson("ALBUM23", localization: false)[1]["levelDesigner3"] = "HOWARD↓Y";
				RefPnlPreparation();
				Singleton<EventManager>.instance.Invoke("UI/OnFreedomDiveShow");
				hideBMSUnlockLogic.ResetUserInputInfo();
				Input.multiTouchEnabled = false;
			}
			else
			{
				hideBMSUnlockLogic.isMusicFD = isTrigger;
				if (HideBMS.hideDifficultyFD != null)
				{
					Singleton<ConfigManager>.instance.GetJson("ALBUM23", localization: false)[1]["difficulty3"] = HideBMS.hideDifficultyFD;
				}
				if (HideBMS.designerNameFD != null)
				{
					Singleton<ConfigManager>.instance.GetJson("ALBUM23", localization: false)[1]["levelDesigner3"] = HideBMS.designerNameFD;
				}
				RefreshMusicFSV();
			}
		}

		private new void Update()
		{
			base.Update();
			hideBMSUnlockLogic.Update();
		}

		private void SetAprilFoolsDayDifficulty()
		{
			if (DateTime.Now.Month == 4 && DateTime.Now.Day == 1)
			{
				if (m_GOODTEKDifficulty == null)
				{
					m_GOODTEKDifficulty = (string)Singleton<ConfigManager>.instance.GetJson("ALBUM5", localization: false)[5]["difficulty2"];
				}
				Singleton<ConfigManager>.instance.GetJson("ALBUM5", localization: false)[5]["difficulty2"] = (string)Singleton<ConfigManager>.instance.GetJson("ALBUM5", localization: false)[5]["difficulty4"];
			}
			else if (m_GOODTEKDifficulty != null)
			{
				Singleton<ConfigManager>.instance.GetJson("ALBUM5", localization: false)[5]["difficulty2"] = m_GOODTEKDifficulty;
			}
		}

		private void InitNacoMusicAudioLogic()
		{
			if (m_NanoAuthorName != null)
			{
				Singleton<ConfigManager>.instance.GetJson("ALBUM22_ChineseS", localization: false)[2]["name"] = m_NanoAuthorName[0];
				Singleton<ConfigManager>.instance.GetJson("ALBUM22_ChineseT", localization: false)[2]["name"] = m_NanoAuthorName[1];
				Singleton<ConfigManager>.instance.GetJson("ALBUM22_English", localization: false)[2]["name"] = m_NanoAuthorName[2];
				Singleton<ConfigManager>.instance.GetJson("ALBUM22_Japanese", localization: false)[2]["name"] = m_NanoAuthorName[3];
				Singleton<ConfigManager>.instance.GetJson("ALBUM22_Korean", localization: false)[2]["name"] = m_NanoAuthorName[4];
			}
			m_TrggleNanoCoreCount = 0;
			m_IsInitNanoCoreCount = false;
			m_IsOnNanoCoreAudio = false;
			tglDifficultyEasy.onValueChanged.AddListener(NacoLogicListener);
			tglDifficultyHard.onValueChanged.AddListener(NacoLogicListener);
			tglDifficultyMaster.onValueChanged.AddListener(NacoLogicListener);
		}

		private void NacoLogicListener(bool isOn)
		{
			if (m_IsOnNanoCoreAudio || !selectedMusicUid.Contains("21-2"))
			{
				return;
			}
			if (isOn)
			{
				if (!m_IsInitNanoCoreCount)
				{
					m_IsInitNanoCoreCount = true;
					return;
				}
				m_TrggleNanoCoreCount++;
			}
			else
			{
				if (m_TrggleNanoCoreCount == 0)
				{
					return;
				}
				m_TrggleNanoCoreCount = 1;
				m_IsInitNanoCoreCount = false;
			}
			if (m_TrggleNanoCoreCount >= 10)
			{
				m_IsOnNanoCoreAudio = true;
				PlayMusic();
				if (m_NanoAuthorName == null)
				{
					m_NanoAuthorName = new string[5];
					m_NanoAuthorName[0] = (string)Singleton<ConfigManager>.instance.GetJson("ALBUM22_ChineseS", localization: false)[2]["name"];
					m_NanoAuthorName[1] = (string)Singleton<ConfigManager>.instance.GetJson("ALBUM22_ChineseT", localization: false)[2]["name"];
					m_NanoAuthorName[2] = (string)Singleton<ConfigManager>.instance.GetJson("ALBUM22_English", localization: false)[2]["name"];
					m_NanoAuthorName[3] = (string)Singleton<ConfigManager>.instance.GetJson("ALBUM22_Japanese", localization: false)[2]["name"];
					m_NanoAuthorName[4] = (string)Singleton<ConfigManager>.instance.GetJson("ALBUM22_Korean", localization: false)[2]["name"];
				}
				Singleton<ConfigManager>.instance.GetJson("ALBUM22_ChineseS", localization: false)[2]["name"] = "Irreplaceable feat.AKINO with bless4";
				Singleton<ConfigManager>.instance.GetJson("ALBUM22_ChineseT", localization: false)[2]["name"] = "Irreplaceable feat.AKINO with bless4";
				Singleton<ConfigManager>.instance.GetJson("ALBUM22_English", localization: false)[2]["name"] = "Irreplaceable feat.AKINO with bless4";
				Singleton<ConfigManager>.instance.GetJson("ALBUM22_Japanese", localization: false)[2]["name"] = "Irreplaceable feat.AKINO with bless4";
				Singleton<ConfigManager>.instance.GetJson("ALBUM22_Korean", localization: false)[2]["name"] = "Irreplaceable feat.AKINO with bless4";
				RefPnlPreparation();
				Singleton<EventManager>.instance.Invoke("UI/OnChangeNanoCoreMusicShow");
			}
		}

		private void InitHideBMSLogic()
		{
			if (m_MopemopeDifficulty != null)
			{
				int configIndex = Singleton<ConfigManager>.instance.GetConfigIndex("ALBUM1", "uid", "0-45");
				Singleton<ConfigManager>.instance.GetJson("ALBUM1", localization: false)[configIndex]["difficulty3"] = m_MopemopeDifficulty;
			}
			if (m_INFiNiTEENERZYDifficulty != null)
			{
				int configIndex2 = Singleton<ConfigManager>.instance.GetConfigIndex("ALBUM21", "uid", "20-2");
				Singleton<ConfigManager>.instance.GetJson("ALBUM21", localization: false)[configIndex2]["difficulty3"] = m_INFiNiTEENERZYDifficulty;
			}
			m_ToggleHideBMSCount = 0;
			isOnMopemope = false;
			isOnINFiNiTEENERZY = false;
			tglDifficultyEasy.onValueChanged.AddListener(HideBMSLogicListener);
			tglDifficultyHard.onValueChanged.AddListener(HideBMSLogicListener);
			tglDifficultyMaster.onValueChanged.AddListener(HideBMSLogicListener);
			btnBackToStage.onClick.AddListener(delegate
			{
				m_ToggleHideBMSCount = 0;
				m_TrggleNanoCoreCount = 0;
			});
			btnMenuBackToStage.onClick.AddListener(delegate
			{
				m_ToggleHideBMSCount = 0;
				m_TrggleNanoCoreCount = 0;
			});
		}

		private void HideBMSLogicListener(bool isOn)
		{
			if ((isOnMopemope || !selectedMusicUid.Contains("0-45")) && (isOnINFiNiTEENERZY || !selectedMusicUid.Contains("20-2")))
			{
				return;
			}
			if (isOn)
			{
				if (!m_IsHideBMSCount)
				{
					m_IsHideBMSCount = true;
					return;
				}
				m_ToggleHideBMSCount++;
			}
			else
			{
				if (m_ToggleHideBMSCount == 0)
				{
					return;
				}
				m_ToggleHideBMSCount = 1;
				m_IsHideBMSCount = false;
			}
			if (m_ToggleHideBMSCount >= 10)
			{
				if (selectedMusicUid.Contains("0-45") && !isOnMopemope)
				{
					isOnMopemope = true;
					int configIndex = Singleton<ConfigManager>.instance.GetConfigIndex("ALBUM1", "uid", "0-45");
					m_MopemopeDifficulty = (string)Singleton<ConfigManager>.instance.GetJson("ALBUM1", localization: false)[configIndex]["difficulty3"];
					Singleton<ConfigManager>.instance.GetJson("ALBUM1", localization: false)[configIndex]["difficulty3"] = "11";
					Singleton<EventManager>.instance.Invoke("UI/OnMopemopeShow");
					RefPnlPreparation();
					Input.multiTouchEnabled = false;
				}
				if (selectedMusicUid.Contains("20-2") && !isOnINFiNiTEENERZY)
				{
					isOnINFiNiTEENERZY = true;
					int configIndex2 = Singleton<ConfigManager>.instance.GetConfigIndex("ALBUM21", "uid", "20-2");
					m_INFiNiTEENERZYDifficulty = (string)Singleton<ConfigManager>.instance.GetJson("ALBUM21", localization: false)[configIndex2]["difficulty3"];
					Singleton<ConfigManager>.instance.GetJson("ALBUM21", localization: false)[configIndex2]["difficulty3"] = "11";
					Singleton<EventManager>.instance.Invoke("UI/OnINFiNiTEENERZYShow");
					RefPnlPreparation();
					Input.multiTouchEnabled = false;
				}
			}
		}

		private void RefPnlPreparation()
		{
			for (int i = 0; i < pnlPreparationObjs.Length; i++)
			{
				pnlPreparationObjs[i].SetActive(value: false);
				pnlPreparationObjs[i].SetActive(value: true);
			}
		}
	}
}
