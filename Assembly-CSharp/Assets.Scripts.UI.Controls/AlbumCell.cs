using Assets.Scripts.GameCore.Managers;
using Assets.Scripts.PeroTools.Commons;
using Assets.Scripts.PeroTools.Nice.Datas;
using Assets.Scripts.PeroTools.Nice.Interface;
using Assets.Scripts.PeroTools.Nice.Variables;
using System.Collections.Generic;
using System.Linq;
using UnityEngine;
using UnityEngine.UI;

namespace Assets.Scripts.UI.Controls
{
	public class AlbumCell : MonoBehaviour
	{
		public Button btn;

		public GameObject imgLock;

		public GameObject imgHeart;

		public GameObject locker;

		private VariableBehaviour m_VariableBehaviour;

		public UnityEngine.UI.Text title;

		public UnityEngine.UI.Text titleLock;

		public GameObject imgFavoriteMusic;

		public GameObject weekFreeIcon;

		public string uid
		{
			get;
			private set;
		}

		public int GetDataIndex()
		{
			return (int)m_VariableBehaviour.result;
		}

		public void SetUid(string uid)
		{
			this.uid = uid;
			imgFavoriteMusic.SetActive(uid == "collections");
		}

		public void SetTitle(string title)
		{
			if (title != null)
			{
				this.title.text = title;
			}
			if (titleLock != null)
			{
				titleLock.text = title;
			}
		}

		public void SetLock(bool isLock)
		{
			imgHeart.SetActive(uid == "collections");
			locker.SetActive(uid != "collections");
			if (uid == "collections")
			{
				imgLock.gameObject.SetActive(Singleton<DataManager>.instance["Account"]["Collections"].GetResult<List<string>>().Count == 0);
			}
			else
			{
				imgLock.gameObject.SetActive(isLock);
			}
		}

		public void SetWeekFree(bool isWeekFree)
		{
			weekFreeIcon.SetActive(isWeekFree);
		}

		public bool isLock()
		{
			if (Enumerable.Contains(Singleton<WeekFreeManager>.instance.freeAlbumUids, uid))
			{
				return false;
			}
			return imgLock.gameObject.activeSelf || (uid == "collections" && Singleton<DataManager>.instance["Account"]["Collections"].GetResult<List<string>>().Count == 0);
		}

		private void Awake()
		{
			m_VariableBehaviour = GetComponent<VariableBehaviour>();
			btn.onClick.AddListener(delegate
			{
			});
		}
	}
}
