using System;
using UnityEngine.Events;

namespace Assets.Scripts.PeroTools.UI
{
	[Serializable]
	public class OnPurchaseRestoreCompleted : UnityEvent<string>
	{
	}
}
