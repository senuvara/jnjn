namespace rail
{
	public enum EnumRailWindowType
	{
		kRailWindowUnknown = 0,
		kRailWindowFriendList = 10,
		kRailWindowPurchaseProduct = 14,
		kRailWindowAchievement = 0xF,
		kRailWindowUserSpaceStore = 0x10,
		kRailWindowLeaderboard = 17,
		kRailWindowHomepage = 18,
		kRailWindowLiveCommenting = 19
	}
}
