namespace rail
{
	public enum EnumCommonLimit
	{
		kRailCommonMaxRepeatedKeys = 50,
		kRailCommonUsersInviteMaxUsersOnce = 0x80,
		kRailCommonMaxKeyLength = 0x100,
		kRailCommonMaxValueLength = 0x400,
		kRailCommonUtilsCheckDirtyWordsStringMaxLength = 0x4000
	}
}
