using System;
using UnityEngine;

namespace Assets.Scripts.PeroTools.GeneralLocalization.Modles
{
	[Serializable]
	public class Property
	{
		public Vector2 cellSize;

		public Vector2 spacing;
	}
}
