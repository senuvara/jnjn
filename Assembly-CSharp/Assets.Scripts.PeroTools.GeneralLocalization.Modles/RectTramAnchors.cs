using System;
using UnityEngine;

namespace Assets.Scripts.PeroTools.GeneralLocalization.Modles
{
	[Serializable]
	public class RectTramAnchors
	{
		public Vector2 min;

		public Vector2 max;
	}
}
