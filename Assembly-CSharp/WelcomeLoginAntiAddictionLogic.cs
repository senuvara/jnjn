using Assets.Scripts.Common.XDSDK;
using Assets.Scripts.PeroTools.Commons;
using System;
using UnityEngine;
using UnityEngine.UI;

public class WelcomeLoginAntiAddictionLogic : MonoBehaviour
{
	public Button btnLogin;

	private Action<string> onLoginSucceed;

	private void Start()
	{
		GetComponent<Button>().interactable = false;
		OnDestroy();
		onLoginSucceed = delegate
		{
			OnDestroy();
		};
	}

	public void LoginDetection()
	{
		if (Singleton<XDSDKManager>.instance.IsLoggedIn() && Singleton<XDSDKManager>.instance.IsXDLoggedIn())
		{
			OnDestroy();
			return;
		}
		Singleton<XDSDKManager>.instance.WelcomeLogin();
		XDSDKManager.XDSDKHandler.onLoginSucceed -= onLoginSucceed;
		XDSDKManager.XDSDKHandler.onLoginSucceed += onLoginSucceed;
	}

	private void OnDestroy()
	{
		btnLogin.interactable = true;
		XDSDKManager.XDSDKHandler.onLoginSucceed -= onLoginSucceed;
		UnityEngine.Object.Destroy(base.gameObject);
	}
}
