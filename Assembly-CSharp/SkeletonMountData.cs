using System;
using UnityEngine;

[Serializable]
public struct SkeletonMountData
{
	public int actionId;

	public GameObject instance;

	public int moduleId;
}
