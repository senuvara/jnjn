using ProtoBuf;

namespace Assets.Scripts.Common
{
	[ProtoContract]
	internal enum Side
	{
		Bottom = 1,
		Top
	}
}
