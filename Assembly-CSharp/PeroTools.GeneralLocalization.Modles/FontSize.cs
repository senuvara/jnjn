using System;

namespace PeroTools.GeneralLocalization.Modles
{
	[Serializable]
	public class FontSize
	{
		public int size;

		public int maxSize;
	}
}
