using System;
using UnityEngine;

namespace PeroTools.GeneralLocalization.Modles
{
	[Serializable]
	public class FontType
	{
		public Font font;
	}
}
