using System;

namespace PeroTools.GeneralLocalization.Modles
{
	[Serializable]
	public class RectTramSize
	{
		public float width;

		public float height;
	}
}
