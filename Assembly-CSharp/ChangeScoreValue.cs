using Assets.Scripts.GameCore.HostComponent;
using Assets.Scripts.PeroTools.Commons;
using Assets.Scripts.PeroTools.Managers;
using UnityEngine;
using UnityEngine.UI;

public class ChangeScoreValue : MonoBehaviour
{
	private Text m_Text;

	private EventManager m_EventManager;

	private void Awake()
	{
		Singleton<EventManager>.instance.RegEvent("Battle/OnScoreChanged").trigger += OnScoreChange;
		m_Text = GetComponent<Text>();
		m_EventManager = Singleton<EventManager>.instance;
	}

	private void OnDestroy()
	{
		if (m_EventManager != null)
		{
			m_EventManager.RegEvent("Battle/OnScoreChanged").trigger -= OnScoreChange;
		}
	}

	private void OnScoreChange(object sender, object reciever, object[] args)
	{
		m_Text.text = string.Concat(Mathf.RoundToInt(Singleton<TaskStageTarget>.instance.GetScore()));
	}
}
