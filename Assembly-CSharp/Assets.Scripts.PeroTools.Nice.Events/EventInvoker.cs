using System;
using UnityEngine.Events;

namespace Assets.Scripts.PeroTools.Nice.Events
{
	[Serializable]
	public class EventInvoker : UnityEvent<Event>
	{
	}
}
