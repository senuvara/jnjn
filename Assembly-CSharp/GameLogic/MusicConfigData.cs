namespace GameLogic
{
	public struct MusicConfigData
	{
		public int id;

		public decimal time;

		public string note_uid;

		public decimal length;

		public bool blood;

		public int pathway;
	}
}
