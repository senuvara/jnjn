using System;

namespace GameLogic
{
	[Serializable]
	public class SceneEvent
	{
		public string uid;

		public decimal time;

		public object value;
	}
}
