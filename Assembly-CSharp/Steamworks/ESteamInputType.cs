namespace Steamworks
{
	public enum ESteamInputType
	{
		k_ESteamInputType_Unknown,
		k_ESteamInputType_SteamController,
		k_ESteamInputType_XBox360Controller,
		k_ESteamInputType_XBoxOneController,
		k_ESteamInputType_GenericXInput,
		k_ESteamInputType_PS4Controller
	}
}
