public enum TGTUserSex
{
	TGTSexMale,
	TGTSexFemale,
	TGTSexUnknown
}
