namespace Assets.Scripts.PeroTools.Commons
{
	public class StringCommons
	{
		public const string staticResources = "/Static Resources";

		public const string resources = "/Resources";

		public const string resourcesPathInAssets = "Assets/Resources/";

		public const string staticResourcesPathInAssets = "Assets/Static Resources/";

		public const string configsAssetPath = "Assets/Static Resources/_Programs/GlobalConfigs/";

		public const string configsBundleName = "globalconfigs";

		public const string assetBundleConfigManagerAssetPath = "Assets/Static Resources/_Programs/GlobalConfigs/AssetBundleConfigManager.asset";

		public const string assetBundleStageInfosPath = "Assets/Static Resources/Data/Configs/StageInfos";

		public const string assetBundleManagerSettingFileName = "AssetBundleManagerSettings";

		public const string assetBundleManagerSettingsFilePath = "Assets/Resources/AssetBundleManagerSettings.asset";

		public const string assetBundleStreamingAssetsSubFolder = "AssetBundles";

		public const string prNodePathInAssets = "/Scripts/PeroTools/Nice/PRHelper/Properties";

		public const string prNodeDrawerPathInAssets = "/Scripts/PeroTools/Nice/PRHelper/Properties/Editor";

		public const string niceActionsPathInAssets = "Assets/Scripts/PeroTools/Nice/Actions";

		public const string niceEventsPathInAssets = "Assets/Scripts/PeroTools/Nice/Events";

		public const string niceAttributesPathInAssets = "Assets/Scripts/PeroTools/Nice/Attributes";

		public const string prHelperSkinInAssets = "Assets/Scripts/PeroTools/Nice/PRHelper/Editor/Skins/PRPRHelperSkin.guiskin";

		public const string peroToolsHead = "PeroPeroTools/";

		public const string searchMenuItem = "PeroPeroTools/Search";

		public const string generalLocalizationProfileMenuItem = "PeroPeroTools/Manager/LocalizationSettings";

		public const string constanceManagerMenuItem = "PeroPeroTools/Manager/Constance";

		public const string noteDatasManagerMenuItem = "PeroPeroTools/Manager/NoteData";

		public const string customEventManagerMenuItem = "PeroPeroTools/Manager/Custom Event";

		public const string variablesManagerMenuItem = "PeroPeroTools/Manager/Variables";

		public const string packingTagConfigManagerMenuItem = "PeroPeroTools/Editor/PackingTagConfig";

		public const string packingTagUpdaterMenuItem = "PeroPeroTools/Editor/UpdatePackingTag";

		public const string assetbundleBuildConfigMenuItem = "PeroPeroTools/Build/Package AssetBundle Configs";

		public const string assetbundleSimulationConfigMenuItem = "PeroPeroTools/Build/Simulation AssetBundle Configs(Start Package)";

		public const string preBuildAssets = "PeroPeroTools/Build/Pre Process Assets";

		public const string buildAssetBundles = "PeroPeroTools/Build/Build AssetBundles";

		public const string buildAllAssets = "PeroPeroTools/Build/Build All Assets";

		public const string niceMakerEventMenuItem = "PeroPeroTools/Nice/Create Event";

		public const string niceMakerActionMenuItem = "PeroPeroTools/Nice/Create Action";

		public const string spineTools = "PeroPeroTools/Spine Tools";

		public const string museDashServerMenuItem = "PeroPeroTools/Muse Dash/Net Terminal";
	}
}
