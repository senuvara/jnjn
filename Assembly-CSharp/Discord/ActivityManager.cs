using System;
using System.Runtime.CompilerServices;
using System.Runtime.InteropServices;

namespace Discord
{
	public class ActivityManager
	{
		internal struct FFIEvents
		{
			[UnmanagedFunctionPointer(CallingConvention.Cdecl)]
			internal delegate void ActivityJoinHandler(IntPtr ptr, [MarshalAs(UnmanagedType.LPStr)] string secret);

			[UnmanagedFunctionPointer(CallingConvention.Cdecl)]
			internal delegate void ActivitySpectateHandler(IntPtr ptr, [MarshalAs(UnmanagedType.LPStr)] string secret);

			[UnmanagedFunctionPointer(CallingConvention.Cdecl)]
			internal delegate void ActivityJoinRequestHandler(IntPtr ptr, ref User user);

			[UnmanagedFunctionPointer(CallingConvention.Cdecl)]
			internal delegate void ActivityInviteHandler(IntPtr ptr, ActivityActionType type, ref User user, ref Activity activity);

			internal ActivityJoinHandler OnActivityJoin;

			internal ActivitySpectateHandler OnActivitySpectate;

			internal ActivityJoinRequestHandler OnActivityJoinRequest;

			internal ActivityInviteHandler OnActivityInvite;
		}

		internal struct FFIMethods
		{
			[UnmanagedFunctionPointer(CallingConvention.Cdecl)]
			internal delegate Result RegisterCommandMethod(IntPtr methodsPtr, [MarshalAs(UnmanagedType.LPStr)] string command);

			[UnmanagedFunctionPointer(CallingConvention.Cdecl)]
			internal delegate Result RegisterSteamMethod(IntPtr methodsPtr, uint steamId);

			[UnmanagedFunctionPointer(CallingConvention.Cdecl)]
			internal delegate void UpdateActivityCallback(IntPtr ptr, Result result);

			[UnmanagedFunctionPointer(CallingConvention.Cdecl)]
			internal delegate void UpdateActivityMethod(IntPtr methodsPtr, ref Activity activity, IntPtr callbackData, UpdateActivityCallback callback);

			[UnmanagedFunctionPointer(CallingConvention.Cdecl)]
			internal delegate void ClearActivityCallback(IntPtr ptr, Result result);

			[UnmanagedFunctionPointer(CallingConvention.Cdecl)]
			internal delegate void ClearActivityMethod(IntPtr methodsPtr, IntPtr callbackData, ClearActivityCallback callback);

			[UnmanagedFunctionPointer(CallingConvention.Cdecl)]
			internal delegate void SendRequestReplyCallback(IntPtr ptr, Result result);

			[UnmanagedFunctionPointer(CallingConvention.Cdecl)]
			internal delegate void SendRequestReplyMethod(IntPtr methodsPtr, long userId, ActivityJoinRequestReply reply, IntPtr callbackData, SendRequestReplyCallback callback);

			[UnmanagedFunctionPointer(CallingConvention.Cdecl)]
			internal delegate void SendInviteCallback(IntPtr ptr, Result result);

			[UnmanagedFunctionPointer(CallingConvention.Cdecl)]
			internal delegate void SendInviteMethod(IntPtr methodsPtr, long userId, ActivityActionType type, [MarshalAs(UnmanagedType.LPStr)] string content, IntPtr callbackData, SendInviteCallback callback);

			[UnmanagedFunctionPointer(CallingConvention.Cdecl)]
			internal delegate void AcceptInviteCallback(IntPtr ptr, Result result);

			[UnmanagedFunctionPointer(CallingConvention.Cdecl)]
			internal delegate void AcceptInviteMethod(IntPtr methodsPtr, long userId, IntPtr callbackData, AcceptInviteCallback callback);

			internal RegisterCommandMethod RegisterCommand;

			internal RegisterSteamMethod RegisterSteam;

			internal UpdateActivityMethod UpdateActivity;

			internal ClearActivityMethod ClearActivity;

			internal SendRequestReplyMethod SendRequestReply;

			internal SendInviteMethod SendInvite;

			internal AcceptInviteMethod AcceptInvite;
		}

		public delegate void UpdateActivityHandler(Result result);

		public delegate void ClearActivityHandler(Result result);

		public delegate void SendRequestReplyHandler(Result result);

		public delegate void SendInviteHandler(Result result);

		public delegate void AcceptInviteHandler(Result result);

		public delegate void ActivityJoinHandler(string secret);

		public delegate void ActivitySpectateHandler(string secret);

		public delegate void ActivityJoinRequestHandler(ref User user);

		public delegate void ActivityInviteHandler(ActivityActionType type, ref User user, ref Activity activity);

		private IntPtr MethodsPtr;

		private object MethodsStructure;

		[CompilerGenerated]
		private static FFIEvents.ActivityJoinHandler _003C_003Ef__mg_0024cache0;

		[CompilerGenerated]
		private static FFIEvents.ActivitySpectateHandler _003C_003Ef__mg_0024cache1;

		[CompilerGenerated]
		private static FFIEvents.ActivityJoinRequestHandler _003C_003Ef__mg_0024cache2;

		[CompilerGenerated]
		private static FFIEvents.ActivityInviteHandler _003C_003Ef__mg_0024cache3;

		[CompilerGenerated]
		private static FFIMethods.UpdateActivityCallback _003C_003Ef__mg_0024cache4;

		[CompilerGenerated]
		private static FFIMethods.ClearActivityCallback _003C_003Ef__mg_0024cache5;

		[CompilerGenerated]
		private static FFIMethods.SendRequestReplyCallback _003C_003Ef__mg_0024cache6;

		[CompilerGenerated]
		private static FFIMethods.SendInviteCallback _003C_003Ef__mg_0024cache7;

		[CompilerGenerated]
		private static FFIMethods.AcceptInviteCallback _003C_003Ef__mg_0024cache8;

		private FFIMethods Methods
		{
			get
			{
				if (MethodsStructure == null)
				{
					MethodsStructure = Marshal.PtrToStructure(MethodsPtr, typeof(FFIMethods));
				}
				return (FFIMethods)MethodsStructure;
			}
		}

		public event ActivityJoinHandler OnActivityJoin;

		public event ActivitySpectateHandler OnActivitySpectate;

		public event ActivityJoinRequestHandler OnActivityJoinRequest;

		public event ActivityInviteHandler OnActivityInvite;

		internal ActivityManager(IntPtr ptr, IntPtr eventsPtr, ref FFIEvents events)
		{
			if (eventsPtr == IntPtr.Zero)
			{
				throw new ResultException(Result.InternalError);
			}
			InitEvents(eventsPtr, ref events);
			MethodsPtr = ptr;
			if (MethodsPtr == IntPtr.Zero)
			{
				throw new ResultException(Result.InternalError);
			}
		}

		public void RegisterCommand()
		{
			RegisterCommand(null);
		}

		private void InitEvents(IntPtr eventsPtr, ref FFIEvents events)
		{
			events.OnActivityJoin = OnActivityJoinImpl;
			events.OnActivitySpectate = OnActivitySpectateImpl;
			events.OnActivityJoinRequest = OnActivityJoinRequestImpl;
			events.OnActivityInvite = OnActivityInviteImpl;
			Marshal.StructureToPtr(events, eventsPtr, fDeleteOld: false);
		}

		public void RegisterCommand(string command)
		{
			FFIMethods methods = Methods;
			Result result = methods.RegisterCommand(MethodsPtr, command);
			if (result != 0)
			{
				throw new ResultException(result);
			}
		}

		public void RegisterSteam(uint steamId)
		{
			FFIMethods methods = Methods;
			Result result = methods.RegisterSteam(MethodsPtr, steamId);
			if (result != 0)
			{
				throw new ResultException(result);
			}
		}

		[MonoPInvokeCallback]
		private static void UpdateActivityCallbackImpl(IntPtr ptr, Result result)
		{
			GCHandle gCHandle = GCHandle.FromIntPtr(ptr);
			UpdateActivityHandler updateActivityHandler = (UpdateActivityHandler)gCHandle.Target;
			gCHandle.Free();
			updateActivityHandler(result);
		}

		public void UpdateActivity(Activity activity, UpdateActivityHandler callback)
		{
			GCHandle value = GCHandle.Alloc(callback);
			FFIMethods methods = Methods;
			methods.UpdateActivity(MethodsPtr, ref activity, GCHandle.ToIntPtr(value), UpdateActivityCallbackImpl);
		}

		[MonoPInvokeCallback]
		private static void ClearActivityCallbackImpl(IntPtr ptr, Result result)
		{
			GCHandle gCHandle = GCHandle.FromIntPtr(ptr);
			ClearActivityHandler clearActivityHandler = (ClearActivityHandler)gCHandle.Target;
			gCHandle.Free();
			clearActivityHandler(result);
		}

		public void ClearActivity(ClearActivityHandler callback)
		{
			GCHandle value = GCHandle.Alloc(callback);
			FFIMethods methods = Methods;
			methods.ClearActivity(MethodsPtr, GCHandle.ToIntPtr(value), ClearActivityCallbackImpl);
		}

		[MonoPInvokeCallback]
		private static void SendRequestReplyCallbackImpl(IntPtr ptr, Result result)
		{
			GCHandle gCHandle = GCHandle.FromIntPtr(ptr);
			SendRequestReplyHandler sendRequestReplyHandler = (SendRequestReplyHandler)gCHandle.Target;
			gCHandle.Free();
			sendRequestReplyHandler(result);
		}

		public void SendRequestReply(long userId, ActivityJoinRequestReply reply, SendRequestReplyHandler callback)
		{
			GCHandle value = GCHandle.Alloc(callback);
			FFIMethods methods = Methods;
			methods.SendRequestReply(MethodsPtr, userId, reply, GCHandle.ToIntPtr(value), SendRequestReplyCallbackImpl);
		}

		[MonoPInvokeCallback]
		private static void SendInviteCallbackImpl(IntPtr ptr, Result result)
		{
			GCHandle gCHandle = GCHandle.FromIntPtr(ptr);
			SendInviteHandler sendInviteHandler = (SendInviteHandler)gCHandle.Target;
			gCHandle.Free();
			sendInviteHandler(result);
		}

		public void SendInvite(long userId, ActivityActionType type, string content, SendInviteHandler callback)
		{
			GCHandle value = GCHandle.Alloc(callback);
			FFIMethods methods = Methods;
			methods.SendInvite(MethodsPtr, userId, type, content, GCHandle.ToIntPtr(value), SendInviteCallbackImpl);
		}

		[MonoPInvokeCallback]
		private static void AcceptInviteCallbackImpl(IntPtr ptr, Result result)
		{
			GCHandle gCHandle = GCHandle.FromIntPtr(ptr);
			AcceptInviteHandler acceptInviteHandler = (AcceptInviteHandler)gCHandle.Target;
			gCHandle.Free();
			acceptInviteHandler(result);
		}

		public void AcceptInvite(long userId, AcceptInviteHandler callback)
		{
			GCHandle value = GCHandle.Alloc(callback);
			FFIMethods methods = Methods;
			methods.AcceptInvite(MethodsPtr, userId, GCHandle.ToIntPtr(value), AcceptInviteCallbackImpl);
		}

		[MonoPInvokeCallback]
		private static void OnActivityJoinImpl(IntPtr ptr, string secret)
		{
			Discord discord = (Discord)GCHandle.FromIntPtr(ptr).Target;
			if (discord.ActivityManagerInstance.OnActivityJoin != null)
			{
				discord.ActivityManagerInstance.OnActivityJoin(secret);
			}
		}

		[MonoPInvokeCallback]
		private static void OnActivitySpectateImpl(IntPtr ptr, string secret)
		{
			Discord discord = (Discord)GCHandle.FromIntPtr(ptr).Target;
			if (discord.ActivityManagerInstance.OnActivitySpectate != null)
			{
				discord.ActivityManagerInstance.OnActivitySpectate(secret);
			}
		}

		[MonoPInvokeCallback]
		private static void OnActivityJoinRequestImpl(IntPtr ptr, ref User user)
		{
			Discord discord = (Discord)GCHandle.FromIntPtr(ptr).Target;
			if (discord.ActivityManagerInstance.OnActivityJoinRequest != null)
			{
				discord.ActivityManagerInstance.OnActivityJoinRequest(ref user);
			}
		}

		[MonoPInvokeCallback]
		private static void OnActivityInviteImpl(IntPtr ptr, ActivityActionType type, ref User user, ref Activity activity)
		{
			Discord discord = (Discord)GCHandle.FromIntPtr(ptr).Target;
			if (discord.ActivityManagerInstance.OnActivityInvite != null)
			{
				discord.ActivityManagerInstance.OnActivityInvite(type, ref user, ref activity);
			}
		}
	}
}
