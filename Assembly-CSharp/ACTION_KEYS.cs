using System.Runtime.InteropServices;

[StructLayout(LayoutKind.Sequential, Size = 1)]
public struct ACTION_KEYS
{
	public const string COMEIN = "in";

	public const string COMEOUT = "out";

	public const string NEAR_ATTACK_1 = "boss_close_atk_1";

	public const string NEAR_ATTACK_2 = "boss_close_atk_2";

	public const string FAR_ATTACK1_START = "boss_far_atk_1_start";

	public const string FAR_ATTACK1_LEFT = "boss_far_atk_1_L";

	public const string FAR_ATTACK1_RIGHT = "boss_far_atk_1_R";

	public const string FAR_ATTACK1_END = "boss_far_atk_1_end";

	public const string FAR_ATTACK2_START = "boss_far_atk_2_start";

	public const string FAR_ATTACK2 = "boss_far_atk_2";

	public const string FAR_ATTACK2_END = "boss_far_atk_2_end";

	public const string BOSS_ATK_1_TO_2 = "atk_1_to_2";

	public const string BOSS_ATK_2_TO_1 = "atk_2_to_1";

	public const string BOSS_MULTI_HURT = "multi_atk_hurt";

	public const string BOSS_MULTI_HURT_END1 = "hurt";

	public const string BOSS_MULTI_HURT_END2 = "multi_atk_hurt_end";

	public const string BOSS_MULTI_HURT_CANCELL1 = "multi_atk_out";

	public const string BOSS_MULTI_HURT_CANCELL2 = "multi_atk_end";

	public const string BOSS_MULTI_ATK = "multi_atk_48";

	public const string BOSS_MULTI_ATK_END = "multi_atk_48_end";

	public const string COMEOUT1 = "note_out_g";

	public const string COMEOUT2 = "note_out_g";

	public const string COMEOUT3 = "note_out_p";

	public const string MUL_HURT = "note_multihit_hurt";

	public const string MUL_ALIVE = "note_multi_alive";

	public const string STAND = "standby";

	public const string STAND_AIR = "standby_air";

	public const string PRESS = "char_press";

	public const string HURT = "char_hurt";

	public const string CHAR_INVINCIBLE = "char_invincible";

	public const string CHAR_ZOMBIA_INVINCIBLE = "char_zombie_invincible";

	public const string CHAR_ZOMBIA_INVINCIBLE_END = "char_zombie_invincible_end";

	public const string CHAR_BLOOD_HURT = "char_blood_hurt";

	public const string CHAR_EMPTY = "empty";

	public const string BOSS_HURT = "boss_hurt";

	public const string RUN = "char_run";

	public const string ATTACK_PERFECT = "char_atk_p";

	public const string ATTACK_GREAT = "char_atk_g";

	public const string ATTACK_COOL = "";

	public const string ATTACK_MISS = "char_atk_miss";

	public const string JUMP_HURT = "char_jump_hurt";

	public const string JUMP = "char_jump";

	public const string CHAR_DEAD = "char_die";

	public const string PET_SKILL = "servant_skill";

	public const string OUTSIDE = "outside";

	public const string JUMP_ATTACK = "char_jumphit";

	public const string JUMP_ATTACK_GREAT = "char_jumphit_great";

	public const string JUMP_DOWN_ATTACK = "char_downhit";

	public const string JUMP_DOWN_PRESS = "char_downpress";

	public const string JUMP_ATTACK_UP = "char_uphit";

	public const string AIR_PRESS_START = "char_uppress_start";

	public const string AIR_PRESSING = "char_uppress";

	public const string AIR_PRESS_END = "char_uppress_end";

	public const string AIR_PRESS_HURT = "char_uppress_hurt";

	public const string PRESS_BIG = "char_big_press";

	public const string PRESS_GROUND_TO_BIG = "char_up_press_s2b";

	public const string PRESS_AIR_TO_BIG = "char_down_press_s2b";

	public const string PRESS_BIG_TO_GROUND = "char_down_press_b2s";

	public const string PRESS_BIG_TO_AIR = "char_up_press_b2s";

	public const string PRESS_HIT_TO_GROUND = "char_down_press_s";

	public const string PRESS_HIT_TO_AIR = "char_up_press_s";

	public const string ATTACK_DOUBLE = "char_bighit";

	public const string MUL_HIT_START = "char_multihit_start";

	public const string MUL_HITTING = "char_multihitting";

	public const string MUL_HIT_END = "char_multihit_end";

	public const string ENEMY_ATTACK_DOUBLE = "note_charge";
}
