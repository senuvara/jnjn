namespace Assets.Scripts.PeroTools.PreWarm
{
	public interface IPreWarm
	{
		void PreWarm(int slice);
	}
}
