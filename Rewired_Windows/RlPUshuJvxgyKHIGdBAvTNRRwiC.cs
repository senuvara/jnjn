using Rewired.Utils;
using System;
using System.Reflection;
using System.Runtime.InteropServices;

[DefaultMember("Item")]
internal class RlPUshuJvxgyKHIGdBAvTNRRwiC : IDisposable
{
	private unsafe byte* pTJDpwPxfmwgapnVRurpDYLiiCH;

	private int DQqsSGTKJLTDGDECFrVvqbRjUUo;

	private bool anYCMiSKlXlHttoIcpSPkLkAJnO;

	public unsafe byte* QcfTdHmGftddXFywmRtZQJymtGX => pTJDpwPxfmwgapnVRurpDYLiiCH;

	public unsafe IntPtr RlpCCVjcoMSRFRaFvXAPsXovOGn => (IntPtr)(void*)pTJDpwPxfmwgapnVRurpDYLiiCH;

	public int NeOIxedQuUfesjEtYNJvaRWEVKK => DQqsSGTKJLTDGDECFrVvqbRjUUo;

	public unsafe byte JvNrWKEZlhDfrmNtGldAYVxwcGR
	{
		get
		{
			if (index < 0 || index >= DQqsSGTKJLTDGDECFrVvqbRjUUo)
			{
				throw new IndexOutOfRangeException();
			}
			return pTJDpwPxfmwgapnVRurpDYLiiCH[index];
		}
		set
		{
			if (index < 0 || index >= DQqsSGTKJLTDGDECFrVvqbRjUUo)
			{
				throw new IndexOutOfRangeException();
			}
			pTJDpwPxfmwgapnVRurpDYLiiCH[index] = value;
		}
	}

	public RlPUshuJvxgyKHIGdBAvTNRRwiC(int size)
	{
		VsOXcKAjjFZEKJbEqeqvoUwQsmC(size);
	}

	public unsafe IntPtr ubpqzTjDOULjmjBvZRYRKNivHAY(int P_0 = 0)
	{
		if (P_0 == 0)
		{
			return (IntPtr)(void*)pTJDpwPxfmwgapnVRurpDYLiiCH;
		}
		if (P_0 < 0 || P_0 >= DQqsSGTKJLTDGDECFrVvqbRjUUo)
		{
			throw new ArgumentOutOfRangeException("offset");
		}
		return (IntPtr)(void*)(pTJDpwPxfmwgapnVRurpDYLiiCH + P_0);
	}

	public unsafe string HVKtdnEtIAQCnIGmRLcwyGrLvbt()
	{
		string text = "";
		for (int i = 0; i < DQqsSGTKJLTDGDECFrVvqbRjUUo; i++)
		{
			text = text + pTJDpwPxfmwgapnVRurpDYLiiCH[i].ToString("x2") + " ";
		}
		return text;
	}

	public unsafe bool VfmjBUIrErtIybHCHodHOYYObyF(int P_0, byte P_1)
	{
		if (1 + P_0 > DQqsSGTKJLTDGDECFrVvqbRjUUo || P_0 < 0)
		{
			throw new ArgumentOutOfRangeException("byteIndex");
		}
		if (P_1 >= 8)
		{
			throw new ArgumentOutOfRangeException("bit");
		}
		return (pTJDpwPxfmwgapnVRurpDYLiiCH[P_0] & (1 << (int)P_1)) != 0;
	}

	public unsafe byte xyIGqNxpiXiSSBLxFYvtMSxGDQzD(int P_0)
	{
		if (1 + P_0 > DQqsSGTKJLTDGDECFrVvqbRjUUo || P_0 < 0)
		{
			throw new ArgumentOutOfRangeException("startIndex");
		}
		return pTJDpwPxfmwgapnVRurpDYLiiCH[P_0];
	}

	public unsafe short yZNaCjivsVGAUgYqPWKYmlsbNihb(int P_0)
	{
		if (2 + P_0 > DQqsSGTKJLTDGDECFrVvqbRjUUo || P_0 < 0)
		{
			throw new ArgumentOutOfRangeException("startIndex");
		}
		return *(short*)(pTJDpwPxfmwgapnVRurpDYLiiCH + P_0);
	}

	public unsafe ushort eizIgInZUytncrVCTqQfDwhtxQU(int P_0)
	{
		if (2 + P_0 > DQqsSGTKJLTDGDECFrVvqbRjUUo || P_0 < 0)
		{
			throw new ArgumentOutOfRangeException("startIndex");
		}
		return *(ushort*)(pTJDpwPxfmwgapnVRurpDYLiiCH + P_0);
	}

	public unsafe int vvbQvXbubQkghmydcwKvLltQxq(int P_0)
	{
		if (4 + P_0 > DQqsSGTKJLTDGDECFrVvqbRjUUo || P_0 < 0)
		{
			throw new ArgumentOutOfRangeException("startIndex");
		}
		return *(int*)(pTJDpwPxfmwgapnVRurpDYLiiCH + P_0);
	}

	public unsafe uint TuHaaOrEosXxBdzUqfBuvmJjrUC(int P_0)
	{
		if (4 + P_0 > DQqsSGTKJLTDGDECFrVvqbRjUUo || P_0 < 0)
		{
			throw new ArgumentOutOfRangeException("startIndex");
		}
		return *(uint*)(pTJDpwPxfmwgapnVRurpDYLiiCH + P_0);
	}

	public unsafe long YkMvFgAIAZYShUUQwwOKhRcIqJQ(int P_0)
	{
		if (8 + P_0 > DQqsSGTKJLTDGDECFrVvqbRjUUo || P_0 < 0)
		{
			throw new ArgumentOutOfRangeException("startIndex");
		}
		return *(long*)(pTJDpwPxfmwgapnVRurpDYLiiCH + P_0);
	}

	public unsafe ulong YwaIZKDwAeEZLUiOKLTuYTdOkGY(int P_0)
	{
		if (8 + P_0 > DQqsSGTKJLTDGDECFrVvqbRjUUo || P_0 < 0)
		{
			throw new ArgumentOutOfRangeException("startIndex");
		}
		return (ulong)(*(long*)(pTJDpwPxfmwgapnVRurpDYLiiCH + P_0));
	}

	public unsafe void QFWNvrxETSHmBcQyDeRwsuSOtDo(byte[] P_0, int P_1, int P_2 = 0, int P_3 = 0)
	{
		if (P_0 == null)
		{
			throw new ArgumentNullException("bytes");
		}
		int num = P_0.Length;
		if (num <= 0)
		{
			throw new ArgumentOutOfRangeException("bytes.Length must be > 0.");
		}
		if (P_1 <= 0)
		{
			throw new ArgumentOutOfRangeException("numBytesToRead must be > 0");
		}
		if (P_1 > num)
		{
			throw new ArgumentOutOfRangeException("numBytesToRead must be <= bufferLength.");
		}
		if (P_1 > DQqsSGTKJLTDGDECFrVvqbRjUUo)
		{
			throw new ArgumentOutOfRangeException("numBytesToRead must be <= Length.");
		}
		if (P_3 >= num)
		{
			throw new ArgumentOutOfRangeException("writeStartIndex must be < bufferLength.");
		}
		if (P_3 < 0)
		{
			throw new ArgumentOutOfRangeException("writeStartIndex must be >= 0.");
		}
		if (P_2 >= DQqsSGTKJLTDGDECFrVvqbRjUUo)
		{
			throw new ArgumentOutOfRangeException("readStartIndex must be < Length.");
		}
		if (P_2 < 0)
		{
			throw new ArgumentOutOfRangeException("readStartIndex must be >= 0.");
		}
		if (P_3 + P_1 > num)
		{
			throw new ArgumentOutOfRangeException("writeStartIndex + numBytesToRead must be < bufferLength.");
		}
		if (P_1 + P_2 > DQqsSGTKJLTDGDECFrVvqbRjUUo)
		{
			throw new ArgumentOutOfRangeException("numBytesToRead + readStartIndex must be < Length.");
		}
		NativeTools.CopyMemory((IntPtr)(void*)pTJDpwPxfmwgapnVRurpDYLiiCH, P_0, P_2, P_3, P_1);
	}

	public unsafe void QFWNvrxETSHmBcQyDeRwsuSOtDo(byte* P_0, int P_1, int P_2, int P_3 = 0, int P_4 = 0)
	{
		if (P_0 == null)
		{
			throw new ArgumentNullException("bytes");
		}
		if (P_1 <= 0)
		{
			throw new ArgumentOutOfRangeException("bufferLength must be > 0.");
		}
		if (P_2 <= 0)
		{
			throw new ArgumentOutOfRangeException("numBytesToRead must be > 0");
		}
		if (P_2 > P_1)
		{
			throw new ArgumentOutOfRangeException("numBytesToRead must be <= bufferLength.");
		}
		if (P_2 > DQqsSGTKJLTDGDECFrVvqbRjUUo)
		{
			throw new ArgumentOutOfRangeException("numBytesToRead must be <= Length.");
		}
		if (P_4 >= P_1)
		{
			throw new ArgumentOutOfRangeException("writeStartIndex must be < bufferLength.");
		}
		if (P_4 < 0)
		{
			throw new ArgumentOutOfRangeException("writeStartIndex must be >= 0.");
		}
		if (P_3 >= DQqsSGTKJLTDGDECFrVvqbRjUUo)
		{
			throw new ArgumentOutOfRangeException("readStartIndex must be < Length.");
		}
		if (P_3 < 0)
		{
			throw new ArgumentOutOfRangeException("readStartIndex must be >= 0.");
		}
		if (P_4 + P_2 > P_1)
		{
			throw new ArgumentOutOfRangeException("writeStartIndex + numBytesToRead must be < bufferLength.");
		}
		if (P_2 + P_3 > DQqsSGTKJLTDGDECFrVvqbRjUUo)
		{
			throw new ArgumentOutOfRangeException("numBytesToRead + readStartIndex must be < Length.");
		}
		dIFzfjFswumKtrqStMoStnTFUae.oFpDksnIvnRbXjfRtoIMMbYPcKqi(pTJDpwPxfmwgapnVRurpDYLiiCH, P_0, P_3, P_4, P_2);
	}

	public unsafe void QFWNvrxETSHmBcQyDeRwsuSOtDo(IntPtr P_0, int P_1, int P_2, int P_3 = 0, int P_4 = 0)
	{
		QFWNvrxETSHmBcQyDeRwsuSOtDo((byte*)(void*)P_0, P_1, P_2, P_3, P_4);
	}

	public unsafe int TvQJUGfplKEcKAgUoLQyHrEQfpnY(byte[] P_0, int P_1, int P_2 = 0, int P_3 = 0)
	{
		if (P_0 == null || P_1 <= 0)
		{
			return 0;
		}
		int num = P_0.Length;
		if (num == 0)
		{
			return 0;
		}
		if (P_2 >= DQqsSGTKJLTDGDECFrVvqbRjUUo)
		{
			return 0;
		}
		if (P_3 >= num)
		{
			return 0;
		}
		if (P_2 < 0)
		{
			P_2 = 0;
		}
		if (P_3 < 0)
		{
			P_3 = 0;
		}
		if (P_2 + P_1 > DQqsSGTKJLTDGDECFrVvqbRjUUo)
		{
			P_1 = DQqsSGTKJLTDGDECFrVvqbRjUUo - P_2;
		}
		if (P_3 + P_1 > num)
		{
			P_1 = num - P_3;
		}
		if (P_1 == 0)
		{
			return 0;
		}
		NativeTools.CopyMemory((IntPtr)(void*)pTJDpwPxfmwgapnVRurpDYLiiCH, P_0, P_2, P_3, P_1);
		return P_1;
	}

	public unsafe int TvQJUGfplKEcKAgUoLQyHrEQfpnY(byte* P_0, int P_1, int P_2, int P_3 = 0, int P_4 = 0)
	{
		if (P_0 == null || P_2 <= 0)
		{
			return 0;
		}
		if (P_3 >= DQqsSGTKJLTDGDECFrVvqbRjUUo)
		{
			return 0;
		}
		if (P_4 >= P_1)
		{
			return 0;
		}
		if (P_3 < 0)
		{
			P_3 = 0;
		}
		if (P_4 < 0)
		{
			P_4 = 0;
		}
		if (P_3 + P_2 > DQqsSGTKJLTDGDECFrVvqbRjUUo)
		{
			P_2 = DQqsSGTKJLTDGDECFrVvqbRjUUo - P_3;
		}
		if (P_4 + P_2 > P_1)
		{
			P_2 = P_1 - P_4;
		}
		dIFzfjFswumKtrqStMoStnTFUae.oFpDksnIvnRbXjfRtoIMMbYPcKqi(pTJDpwPxfmwgapnVRurpDYLiiCH, P_0, P_3, P_4, P_2);
		return P_2;
	}

	public unsafe int TvQJUGfplKEcKAgUoLQyHrEQfpnY(IntPtr P_0, int P_1, int P_2, int P_3 = 0, int P_4 = 0)
	{
		if (P_0 == IntPtr.Zero)
		{
			return 0;
		}
		return TvQJUGfplKEcKAgUoLQyHrEQfpnY((byte*)(void*)P_0, P_1, P_2, P_3, P_4);
	}

	public unsafe void ljgdPSGWbOWjrjKBAhHBgIOzGJJC(int P_0, byte P_1, bool P_2)
	{
		if (1 + P_0 > DQqsSGTKJLTDGDECFrVvqbRjUUo || P_0 < 0)
		{
			throw new ArgumentOutOfRangeException("byteIndex");
		}
		if (P_1 >= 8)
		{
			throw new ArgumentOutOfRangeException("bit");
		}
		if (P_2)
		{
			byte* intPtr = pTJDpwPxfmwgapnVRurpDYLiiCH + P_0;
			*intPtr = (byte)(*intPtr | (byte)(1 << (int)P_1));
		}
		else
		{
			byte* intPtr2 = pTJDpwPxfmwgapnVRurpDYLiiCH + P_0;
			*intPtr2 = (byte)(*intPtr2 & (byte)(~(1 << (int)P_1)));
		}
	}

	public unsafe void jNSHZfddqcrPRRdjZjdCecTCpoNb(byte P_0, int P_1)
	{
		if (1 + P_1 > DQqsSGTKJLTDGDECFrVvqbRjUUo || P_1 < 0)
		{
			throw new ArgumentOutOfRangeException("startIndex");
		}
		pTJDpwPxfmwgapnVRurpDYLiiCH[P_1] = P_0;
	}

	public unsafe void jNSHZfddqcrPRRdjZjdCecTCpoNb(short P_0, int P_1)
	{
		if (2 + P_1 > DQqsSGTKJLTDGDECFrVvqbRjUUo || P_1 < 0)
		{
			throw new ArgumentOutOfRangeException("startIndex");
		}
		*(short*)(pTJDpwPxfmwgapnVRurpDYLiiCH + P_1) = P_0;
	}

	public unsafe void jNSHZfddqcrPRRdjZjdCecTCpoNb(ushort P_0, int P_1)
	{
		if (2 + P_1 > DQqsSGTKJLTDGDECFrVvqbRjUUo || P_1 < 0)
		{
			throw new ArgumentOutOfRangeException("startIndex");
		}
		*(ushort*)(pTJDpwPxfmwgapnVRurpDYLiiCH + P_1) = P_0;
	}

	public unsafe void jNSHZfddqcrPRRdjZjdCecTCpoNb(int P_0, int P_1)
	{
		if (4 + P_1 > DQqsSGTKJLTDGDECFrVvqbRjUUo || P_1 < 0)
		{
			throw new ArgumentOutOfRangeException("startIndex");
		}
		*(int*)(pTJDpwPxfmwgapnVRurpDYLiiCH + P_1) = P_0;
	}

	public unsafe void jNSHZfddqcrPRRdjZjdCecTCpoNb(uint P_0, int P_1)
	{
		if (4 + P_1 > DQqsSGTKJLTDGDECFrVvqbRjUUo || P_1 < 0)
		{
			throw new ArgumentOutOfRangeException("startIndex");
		}
		*(uint*)(pTJDpwPxfmwgapnVRurpDYLiiCH + P_1) = P_0;
	}

	public unsafe void jNSHZfddqcrPRRdjZjdCecTCpoNb(long P_0, int P_1)
	{
		if (8 + P_1 > DQqsSGTKJLTDGDECFrVvqbRjUUo || P_1 < 0)
		{
			throw new ArgumentOutOfRangeException("startIndex");
		}
		*(long*)(pTJDpwPxfmwgapnVRurpDYLiiCH + P_1) = P_0;
	}

	public unsafe void jNSHZfddqcrPRRdjZjdCecTCpoNb(ulong P_0, int P_1)
	{
		if (8 + P_1 > DQqsSGTKJLTDGDECFrVvqbRjUUo || P_1 < 0)
		{
			throw new ArgumentOutOfRangeException("startIndex");
		}
		*(ulong*)(pTJDpwPxfmwgapnVRurpDYLiiCH + P_1) = P_0;
	}

	public unsafe void jNSHZfddqcrPRRdjZjdCecTCpoNb(byte[] P_0, int P_1, int P_2 = 0, int P_3 = 0)
	{
		if (P_0 == null)
		{
			throw new ArgumentNullException("bytes");
		}
		int num = P_0.Length;
		if (num <= 0)
		{
			throw new ArgumentOutOfRangeException("bytes.Length must be > 0.");
		}
		if (P_1 <= 0)
		{
			throw new ArgumentOutOfRangeException("numBytesToWrite must be > 0");
		}
		if (P_1 > num)
		{
			throw new ArgumentOutOfRangeException("numBytesToWrite must be <= bufferLength.");
		}
		if (P_1 > DQqsSGTKJLTDGDECFrVvqbRjUUo)
		{
			throw new ArgumentOutOfRangeException("numBytesToWrite must be <= Length.");
		}
		if (P_3 >= num)
		{
			throw new ArgumentOutOfRangeException("readStartIndex must be < bufferLength.");
		}
		if (P_3 < 0)
		{
			throw new ArgumentOutOfRangeException("readStartIndex must be >= 0.");
		}
		if (P_2 >= DQqsSGTKJLTDGDECFrVvqbRjUUo)
		{
			throw new ArgumentOutOfRangeException("writeStartIndex must be < Length.");
		}
		if (P_2 < 0)
		{
			throw new ArgumentOutOfRangeException("writeStartIndex must be >= 0.");
		}
		if (P_3 + P_1 > num)
		{
			throw new ArgumentOutOfRangeException("readStartIndex + numBytesToWrite must be < bufferLength.");
		}
		if (P_1 + P_2 > DQqsSGTKJLTDGDECFrVvqbRjUUo)
		{
			throw new ArgumentOutOfRangeException("numBytesToWrite + writeStartIndex must be < Length.");
		}
		NativeTools.CopyMemory(P_0, (IntPtr)(void*)pTJDpwPxfmwgapnVRurpDYLiiCH, P_3, P_2, P_1);
	}

	public unsafe void jNSHZfddqcrPRRdjZjdCecTCpoNb(byte* P_0, int P_1, int P_2, int P_3 = 0, int P_4 = 0)
	{
		if (P_0 == null)
		{
			throw new ArgumentNullException("bytes");
		}
		if (P_1 <= 0)
		{
			throw new ArgumentOutOfRangeException("bufferLength must be > 0.");
		}
		if (P_2 <= 0)
		{
			throw new ArgumentOutOfRangeException("numBytesToWrite must be > 0");
		}
		if (P_2 > P_1)
		{
			throw new ArgumentOutOfRangeException("numBytesToWrite must be <= bufferLength.");
		}
		if (P_2 > DQqsSGTKJLTDGDECFrVvqbRjUUo)
		{
			throw new ArgumentOutOfRangeException("numBytesToWrite must be <= Length.");
		}
		if (P_4 >= P_1)
		{
			throw new ArgumentOutOfRangeException("readStartIndex must be < bufferLength.");
		}
		if (P_4 < 0)
		{
			throw new ArgumentOutOfRangeException("readStartIndex must be >= 0.");
		}
		if (P_3 >= DQqsSGTKJLTDGDECFrVvqbRjUUo)
		{
			throw new ArgumentOutOfRangeException("writeStartIndex must be < Length.");
		}
		if (P_3 < 0)
		{
			throw new ArgumentOutOfRangeException("writeStartIndex must be >= 0.");
		}
		if (P_4 + P_2 > P_1)
		{
			throw new ArgumentOutOfRangeException("readStartIndex + numBytesToWrite must be < bufferLength.");
		}
		if (P_2 + P_3 > DQqsSGTKJLTDGDECFrVvqbRjUUo)
		{
			throw new ArgumentOutOfRangeException("numBytesToWrite + writeStartIndex must be < Length.");
		}
		dIFzfjFswumKtrqStMoStnTFUae.oFpDksnIvnRbXjfRtoIMMbYPcKqi(P_0, pTJDpwPxfmwgapnVRurpDYLiiCH, P_4, P_3, P_2);
	}

	public unsafe void jNSHZfddqcrPRRdjZjdCecTCpoNb(IntPtr P_0, int P_1, int P_2, int P_3 = 0, int P_4 = 0)
	{
		jNSHZfddqcrPRRdjZjdCecTCpoNb((byte*)(void*)P_0, P_1, P_2, P_3, P_4);
	}

	public unsafe int zHNnkflJvyjEfBpAvggOTDlXxhs(byte[] P_0, int P_1, int P_2 = 0, int P_3 = 0)
	{
		if (P_0 == null)
		{
			return 0;
		}
		int num = P_0.Length;
		if (num == 0 || P_1 <= 0 || P_3 >= num || P_2 >= DQqsSGTKJLTDGDECFrVvqbRjUUo)
		{
			return 0;
		}
		if (P_3 < 0)
		{
			P_3 = 0;
		}
		if (P_2 < 0)
		{
			P_2 = 0;
		}
		if (P_3 + P_1 > num)
		{
			P_1 = num - P_3;
		}
		if (P_1 + P_2 > DQqsSGTKJLTDGDECFrVvqbRjUUo)
		{
			P_1 = DQqsSGTKJLTDGDECFrVvqbRjUUo - P_2;
		}
		NativeTools.CopyMemory(P_0, (IntPtr)(void*)pTJDpwPxfmwgapnVRurpDYLiiCH, P_3, P_2, P_1);
		return P_1;
	}

	public unsafe int zHNnkflJvyjEfBpAvggOTDlXxhs(byte* P_0, int P_1, int P_2, int P_3 = 0, int P_4 = 0)
	{
		if (P_0 == null || P_1 <= 0 || P_2 <= 0 || P_4 >= P_1 || P_3 >= DQqsSGTKJLTDGDECFrVvqbRjUUo)
		{
			return 0;
		}
		if (P_4 < 0)
		{
			P_4 = 0;
		}
		if (P_3 < 0)
		{
			P_3 = 0;
		}
		if (P_4 + P_2 > P_1)
		{
			P_2 = P_1 - P_4;
		}
		if (P_2 + P_3 > DQqsSGTKJLTDGDECFrVvqbRjUUo)
		{
			P_2 = DQqsSGTKJLTDGDECFrVvqbRjUUo - P_3;
		}
		dIFzfjFswumKtrqStMoStnTFUae.oFpDksnIvnRbXjfRtoIMMbYPcKqi(P_0, pTJDpwPxfmwgapnVRurpDYLiiCH, P_4, P_3, P_2);
		return P_2;
	}

	public unsafe int zHNnkflJvyjEfBpAvggOTDlXxhs(IntPtr P_0, int P_1, int P_2, int P_3 = 0, int P_4 = 0)
	{
		return zHNnkflJvyjEfBpAvggOTDlXxhs((byte*)(void*)P_0, P_1, P_2, P_3, P_4);
	}

	public unsafe bool VsOXcKAjjFZEKJbEqeqvoUwQsmC(int P_0)
	{
		if (P_0 < 0)
		{
			throw new ArgumentOutOfRangeException("size");
		}
		if (DQqsSGTKJLTDGDECFrVvqbRjUUo == P_0)
		{
			return true;
		}
		KOadbqRdftPnkznADPKqAuHrTts();
		if (P_0 == 0)
		{
			return true;
		}
		DQqsSGTKJLTDGDECFrVvqbRjUUo = P_0;
		pTJDpwPxfmwgapnVRurpDYLiiCH = (byte*)(void*)Marshal.AllocHGlobal(P_0);
		gDIsdrSnGkjGabgrbnfmyYVEOLlV();
		return true;
	}

	public unsafe void gDIsdrSnGkjGabgrbnfmyYVEOLlV()
	{
		if (DQqsSGTKJLTDGDECFrVvqbRjUUo != 0)
		{
			dIFzfjFswumKtrqStMoStnTFUae.seYCLoZiHbKQitXWkSaJFdvtmNu(pTJDpwPxfmwgapnVRurpDYLiiCH, DQqsSGTKJLTDGDECFrVvqbRjUUo);
		}
	}

	public unsafe void KOadbqRdftPnkznADPKqAuHrTts()
	{
		if (DQqsSGTKJLTDGDECFrVvqbRjUUo != 0)
		{
			try
			{
				if (pTJDpwPxfmwgapnVRurpDYLiiCH != null)
				{
					Marshal.FreeHGlobal(RlpCCVjcoMSRFRaFvXAPsXovOGn);
				}
			}
			catch
			{
			}
			pTJDpwPxfmwgapnVRurpDYLiiCH = null;
			DQqsSGTKJLTDGDECFrVvqbRjUUo = 0;
		}
	}

	public override string ToString()
	{
		string text = "";
		for (int i = 0; i < DQqsSGTKJLTDGDECFrVvqbRjUUo; i++)
		{
			text = text + xyIGqNxpiXiSSBLxFYvtMSxGDQzD(i).ToString("x2") + " ";
		}
		return text;
	}

	public void Dispose()
	{
		EjOnxZffDhiNtnYcjfEZVyvxMWf(true);
		GC.SuppressFinalize(this);
	}

	~RlPUshuJvxgyKHIGdBAvTNRRwiC()
	{
		EjOnxZffDhiNtnYcjfEZVyvxMWf(false);
	}

	protected virtual void EjOnxZffDhiNtnYcjfEZVyvxMWf(bool P_0)
	{
		if (!anYCMiSKlXlHttoIcpSPkLkAJnO)
		{
			KOadbqRdftPnkznADPKqAuHrTts();
			anYCMiSKlXlHttoIcpSPkLkAJnO = true;
		}
	}

	public unsafe static implicit operator IntPtr(RlPUshuJvxgyKHIGdBAvTNRRwiC buffer)
	{
		if (buffer == null)
		{
			return IntPtr.Zero;
		}
		return (IntPtr)(void*)buffer.pTJDpwPxfmwgapnVRurpDYLiiCH;
	}

	public unsafe static implicit operator void*(RlPUshuJvxgyKHIGdBAvTNRRwiC buffer)
	{
		if (buffer == null)
		{
			return null;
		}
		return buffer.pTJDpwPxfmwgapnVRurpDYLiiCH;
	}

	public unsafe static bool dFKaOMSiXpqBqcFNcadWDdJRqhbB(RlPUshuJvxgyKHIGdBAvTNRRwiC P_0, RlPUshuJvxgyKHIGdBAvTNRRwiC P_1)
	{
		if (P_0 == null)
		{
			throw new ArgumentNullException("source");
		}
		if (P_1 == null)
		{
			throw new ArgumentNullException("destination");
		}
		if (P_0.DQqsSGTKJLTDGDECFrVvqbRjUUo == 0)
		{
			P_1.KOadbqRdftPnkznADPKqAuHrTts();
			return true;
		}
		if (P_1.VsOXcKAjjFZEKJbEqeqvoUwQsmC(P_0.DQqsSGTKJLTDGDECFrVvqbRjUUo))
		{
			P_1.jNSHZfddqcrPRRdjZjdCecTCpoNb(P_0.pTJDpwPxfmwgapnVRurpDYLiiCH, P_0.DQqsSGTKJLTDGDECFrVvqbRjUUo, P_0.DQqsSGTKJLTDGDECFrVvqbRjUUo);
			return true;
		}
		return false;
	}
}
