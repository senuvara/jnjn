using System;
using System.Globalization;
using System.Runtime.InteropServices;

[StructLayout(LayoutKind.Sequential)]
internal class RoVQWfgmfkvoxyvLugEcXCvAWOD
{
	public IntPtr RlpCCVjcoMSRFRaFvXAPsXovOGn;

	public RoVQWfgmfkvoxyvLugEcXCvAWOD(IntPtr pointer)
	{
		RlpCCVjcoMSRFRaFvXAPsXovOGn = pointer;
	}

	public unsafe RoVQWfgmfkvoxyvLugEcXCvAWOD(void* pointer)
	{
		RlpCCVjcoMSRFRaFvXAPsXovOGn = new IntPtr(pointer);
	}

	public static explicit operator IntPtr(RoVQWfgmfkvoxyvLugEcXCvAWOD value)
	{
		return value.RlpCCVjcoMSRFRaFvXAPsXovOGn;
	}

	public static implicit operator RoVQWfgmfkvoxyvLugEcXCvAWOD(IntPtr value)
	{
		return new RoVQWfgmfkvoxyvLugEcXCvAWOD(value);
	}

	public unsafe static implicit operator void*(RoVQWfgmfkvoxyvLugEcXCvAWOD value)
	{
		return (void*)value.RlpCCVjcoMSRFRaFvXAPsXovOGn;
	}

	public unsafe static explicit operator RoVQWfgmfkvoxyvLugEcXCvAWOD(void* value)
	{
		return new RoVQWfgmfkvoxyvLugEcXCvAWOD(value);
	}

	public override string ToString()
	{
		return string.Format(CultureInfo.CurrentCulture, "{0}", RlpCCVjcoMSRFRaFvXAPsXovOGn);
	}

	public string dOCXUYeBibijwZtRRZFSEUjftCp(string P_0)
	{
		if (P_0 == null)
		{
			return ToString();
		}
		return string.Format(CultureInfo.CurrentCulture, "{0}", RlpCCVjcoMSRFRaFvXAPsXovOGn.ToString(P_0));
	}

	public override int GetHashCode()
	{
		return RlpCCVjcoMSRFRaFvXAPsXovOGn.ToInt32();
	}

	public bool wKgxsmYrYmtKpBGFFpFsAHonrBy(RoVQWfgmfkvoxyvLugEcXCvAWOD P_0)
	{
		return RlpCCVjcoMSRFRaFvXAPsXovOGn == P_0.RlpCCVjcoMSRFRaFvXAPsXovOGn;
	}

	public override bool Equals(object value)
	{
		if (value == null)
		{
			return false;
		}
		if (!object.ReferenceEquals(value.GetType(), typeof(RoVQWfgmfkvoxyvLugEcXCvAWOD)))
		{
			return false;
		}
		return wKgxsmYrYmtKpBGFFpFsAHonrBy((RoVQWfgmfkvoxyvLugEcXCvAWOD)value);
	}
}
