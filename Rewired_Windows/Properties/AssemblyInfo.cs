using System.Diagnostics;
using System.Reflection;
using System.Resources;
using System.Runtime.CompilerServices;
using System.Runtime.InteropServices;
using System.Security;
using System.Security.Permissions;

[assembly: RuntimeCompatibility(WrapNonExceptionThrows = true)]
[assembly: CompilationRelaxations(8)]
[assembly: AssemblyTitle("Rewired Windows")]
[assembly: AssemblyCompany("Guavaman Enterprises")]
[assembly: AssemblyConfiguration("Release")]
[assembly: AssemblyDescription("")]
[assembly: Debuggable(DebuggableAttribute.DebuggingModes.IgnoreSymbolStoreSequencePoints)]
[assembly: AssemblyProduct("Rewired")]
[assembly: ComVisible(false)]
[assembly: AssemblyCopyright("Copyright ©  2014 Augie R. Maddox. All rights reserved.")]
[assembly: AssemblyTrademark("Augie R. Maddox")]
[assembly: NeutralResourcesLanguage("en-us")]
[assembly: AssemblyFileVersion("1.0.0.0")]
[assembly: Guid("a41ebc23-ec97-4fd2-81ae-f39afc75854a")]
[assembly: SecurityPermission(SecurityAction.RequestMinimum, SkipVerification = true)]
[assembly: AssemblyVersion("1.0.0.0")]
[module: SuppressIldasm]
[module: UnverifiableCode]
