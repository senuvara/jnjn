using Rewired;
using Rewired.Utils;
using System;
using System.Collections.Generic;
using System.Runtime.CompilerServices;
using System.Threading;

internal class GpBATPAcvcaIFoOOwrEnvAAzdSl<T>
{
	private enum HUIIQdnncKUebdiBTOVEYuWgLgX
	{
		aJBsUCGKCJyqqlFSBHxzlMFfTTa,
		oyFmZlsGKMhEtQbgUfkZIfoOcRAF,
		lPwGnjHMImHzXynWsiBVFitKJXq
	}

	private static class izRzaEMoeHvcUJXDGStCwawypLw
	{
		private class tskUFpQLAOAZGzRyDhHEevnqEhux : IDisposable
		{
			[CompilerGenerated]
			private sealed class SqxjMLdIBynHqKomeqTxizqByKJ
			{
				public ManualResetEvent SlmAFtJfrGcHTHpcYIpUXeVqBDz;

				public tskUFpQLAOAZGzRyDhHEevnqEhux dqqyDMaOsmVbVzxHWQrwLztHqyq;

				public void KpxqisnXPVCaerhWYYXKmJEuHEh()
				{
					SlmAFtJfrGcHTHpcYIpUXeVqBDz.Set();
					dqqyDMaOsmVbVzxHWQrwLztHqyq.uEoGOOUVoOYLnPOIsEHTOrBpOOJ();
				}
			}

			private readonly object HJOCdRFIQRGVeozfRgvsWGAEqjt;

			private List<WaitCallback> FEzIgKYQPBsgfwqwwcrrsuGzGXr;

			private List<WaitCallback> JoKfBflFGEDnFhbKlaHXclNiclqH;

			private Thread qrGFbkUWzwGXRsUvKeiGbFttxHZ;

			private AutoResetEvent MdoNquBLZbBxLKPlrZscpItFMCP;

			private bool vgUvNyGkIjMqkVGODoRLNEqsEPI;

			private bool UKVPYQYLDoixNcLflNTpEtnyRVT;

			private bool RkfrCIcLZVJzdxZOkDXjvctIQrj;

			private bool anYCMiSKlXlHttoIcpSPkLkAJnO;

			public tskUFpQLAOAZGzRyDhHEevnqEhux()
			{
				HJOCdRFIQRGVeozfRgvsWGAEqjt = new object();
				FEzIgKYQPBsgfwqwwcrrsuGzGXr = new List<WaitCallback>();
				JoKfBflFGEDnFhbKlaHXclNiclqH = new List<WaitCallback>();
				MdoNquBLZbBxLKPlrZscpItFMCP = new AutoResetEvent(initialState: false);
			}

			public void CKXFWwbNljtnixfVtFkWcsEBRDhn(WaitCallback P_0)
			{
				if (PeABNtxUebHxrGruPVcGjlGikYu())
				{
					if (P_0 == null)
					{
						throw new ArgumentNullException("waitCallback");
					}
					lock (HJOCdRFIQRGVeozfRgvsWGAEqjt)
					{
						FEzIgKYQPBsgfwqwwcrrsuGzGXr.Add(P_0);
					}
					MdoNquBLZbBxLKPlrZscpItFMCP.Set();
				}
			}

			public void NpjaXRAtHprLYhEvjJVrVGqmHxe()
			{
				ukDZZONZBYlUoWRdaYoWdmqnCAu();
			}

			public bool ItKsKCZMtMOIXakrpcPquJKVTqy()
			{
				return PeABNtxUebHxrGruPVcGjlGikYu();
			}

			private bool PeABNtxUebHxrGruPVcGjlGikYu()
			{
				if (RkfrCIcLZVJzdxZOkDXjvctIQrj)
				{
					return false;
				}
				if (UKVPYQYLDoixNcLflNTpEtnyRVT)
				{
					return false;
				}
				if (vgUvNyGkIjMqkVGODoRLNEqsEPI)
				{
					return true;
				}
				if (qrGFbkUWzwGXRsUvKeiGbFttxHZ != null)
				{
					return true;
				}
				try
				{
					ManualResetEvent SlmAFtJfrGcHTHpcYIpUXeVqBDz = new ManualResetEvent(initialState: false);
					qrGFbkUWzwGXRsUvKeiGbFttxHZ = new Thread((ThreadStart)delegate
					{
						SlmAFtJfrGcHTHpcYIpUXeVqBDz.Set();
						uEoGOOUVoOYLnPOIsEHTOrBpOOJ();
					});
					qrGFbkUWzwGXRsUvKeiGbFttxHZ.Start();
					SlmAFtJfrGcHTHpcYIpUXeVqBDz.WaitOne();
					return true;
				}
				catch (Exception arg)
				{
					Logger.LogError("An exception occurred trying to initialize the thread pool.\n" + arg, requiredThreadSafety: true);
					qrGFbkUWzwGXRsUvKeiGbFttxHZ = null;
					RkfrCIcLZVJzdxZOkDXjvctIQrj = true;
					return false;
				}
			}

			private void uEoGOOUVoOYLnPOIsEHTOrBpOOJ()
			{
				vgUvNyGkIjMqkVGODoRLNEqsEPI = true;
				while (!UKVPYQYLDoixNcLflNTpEtnyRVT)
				{
					MdoNquBLZbBxLKPlrZscpItFMCP.WaitOne();
					if (UKVPYQYLDoixNcLflNTpEtnyRVT)
					{
						break;
					}
					lock (HJOCdRFIQRGVeozfRgvsWGAEqjt)
					{
						MiscTools.Swap(ref FEzIgKYQPBsgfwqwwcrrsuGzGXr, ref JoKfBflFGEDnFhbKlaHXclNiclqH);
					}
					List<WaitCallback> joKfBflFGEDnFhbKlaHXclNiclqH = JoKfBflFGEDnFhbKlaHXclNiclqH;
					int count = joKfBflFGEDnFhbKlaHXclNiclqH.Count;
					if (count != 0)
					{
						for (int i = 0; i < count; i++)
						{
							try
							{
								joKfBflFGEDnFhbKlaHXclNiclqH[i](null);
							}
							catch (Exception arg)
							{
								Logger.LogError("Exception occurred in thread pool callback.\n" + arg, requiredThreadSafety: true);
							}
						}
						joKfBflFGEDnFhbKlaHXclNiclqH.Clear();
					}
				}
				lock (HJOCdRFIQRGVeozfRgvsWGAEqjt)
				{
					FEzIgKYQPBsgfwqwwcrrsuGzGXr.Clear();
					JoKfBflFGEDnFhbKlaHXclNiclqH.Clear();
				}
				UKVPYQYLDoixNcLflNTpEtnyRVT = false;
				vgUvNyGkIjMqkVGODoRLNEqsEPI = false;
			}

			private void sgcebaAIgpqqcQzjcCgZNZdfZfeS()
			{
				qrGFbkUWzwGXRsUvKeiGbFttxHZ = null;
				RkfrCIcLZVJzdxZOkDXjvctIQrj = false;
				UKVPYQYLDoixNcLflNTpEtnyRVT = true;
			}

			private void ukDZZONZBYlUoWRdaYoWdmqnCAu()
			{
				sgcebaAIgpqqcQzjcCgZNZdfZfeS();
				try
				{
					MdoNquBLZbBxLKPlrZscpItFMCP.Set();
				}
				catch (ObjectDisposedException)
				{
				}
			}

			public void Dispose()
			{
				EjOnxZffDhiNtnYcjfEZVyvxMWf(true);
				GC.SuppressFinalize(this);
			}

			~tskUFpQLAOAZGzRyDhHEevnqEhux()
			{
				EjOnxZffDhiNtnYcjfEZVyvxMWf(false);
			}

			protected virtual void EjOnxZffDhiNtnYcjfEZVyvxMWf(bool P_0)
			{
				if (!anYCMiSKlXlHttoIcpSPkLkAJnO)
				{
					ukDZZONZBYlUoWRdaYoWdmqnCAu();
					anYCMiSKlXlHttoIcpSPkLkAJnO = true;
				}
			}
		}

		private static tskUFpQLAOAZGzRyDhHEevnqEhux iceaYctFLXkXvspynmINcQNOlcB;

		private static int NqnxFGQUNsFSRNMjqmbAxEQMLFX;

		private static tskUFpQLAOAZGzRyDhHEevnqEhux bDlyhPYRKccOmClSwhCVJhmAFwBG => iceaYctFLXkXvspynmINcQNOlcB ?? (iceaYctFLXkXvspynmINcQNOlcB = new tskUFpQLAOAZGzRyDhHEevnqEhux());

		static izRzaEMoeHvcUJXDGStCwawypLw()
		{
			NqnxFGQUNsFSRNMjqmbAxEQMLFX = 0;
			AppDomain.CurrentDomain.DomainUnload -= FLGFMOiIgXwpZXRiPkgbsPyuoAR;
			AppDomain.CurrentDomain.DomainUnload += FLGFMOiIgXwpZXRiPkgbsPyuoAR;
		}

		private static void FLGFMOiIgXwpZXRiPkgbsPyuoAR(object P_0, EventArgs P_1)
		{
			EjOnxZffDhiNtnYcjfEZVyvxMWf();
			AppDomain.CurrentDomain.DomainUnload -= FLGFMOiIgXwpZXRiPkgbsPyuoAR;
		}

		public static void aUVvLsnunUZVUpVlpeoAWgQOmfd()
		{
			NqnxFGQUNsFSRNMjqmbAxEQMLFX++;
		}

		public static void GElYoZzgvzbtHhJYgJKEcLohokZV()
		{
			NqnxFGQUNsFSRNMjqmbAxEQMLFX--;
			if (NqnxFGQUNsFSRNMjqmbAxEQMLFX < 0)
			{
				Logger.LogError("SharedThread: Too many calls to Unregister.", requiredThreadSafety: true);
			}
			if (NqnxFGQUNsFSRNMjqmbAxEQMLFX == 0)
			{
				EjOnxZffDhiNtnYcjfEZVyvxMWf();
			}
		}

		public static void CKXFWwbNljtnixfVtFkWcsEBRDhn(WaitCallback P_0)
		{
			bDlyhPYRKccOmClSwhCVJhmAFwBG.CKXFWwbNljtnixfVtFkWcsEBRDhn(P_0);
		}

		public static void NpjaXRAtHprLYhEvjJVrVGqmHxe()
		{
			bDlyhPYRKccOmClSwhCVJhmAFwBG.NpjaXRAtHprLYhEvjJVrVGqmHxe();
		}

		public static bool ItKsKCZMtMOIXakrpcPquJKVTqy()
		{
			return bDlyhPYRKccOmClSwhCVJhmAFwBG.ItKsKCZMtMOIXakrpcPquJKVTqy();
		}

		private static void EjOnxZffDhiNtnYcjfEZVyvxMWf()
		{
			if (iceaYctFLXkXvspynmINcQNOlcB != null)
			{
				iceaYctFLXkXvspynmINcQNOlcB.Dispose();
			}
			iceaYctFLXkXvspynmINcQNOlcB = null;
			NqnxFGQUNsFSRNMjqmbAxEQMLFX = 0;
		}
	}

	private HUIIQdnncKUebdiBTOVEYuWgLgX rPgHxYLMAMLpWDHyEGyOAdoWvSr;

	private T cxYyeNnYyHdGUipbGuKhDUfRLcg;

	private WaitCallback xTXcDCftQBhcbaYDPhpTabHWhyR;

	private object suemxSFKhFNwltYbyKPFOtyzDDZ;

	private Func<T> dtfwmYtedwKQBRtBslELJLShJEt;

	private bool SsVHlbewYFBFvRQaBEebNIkZtod;

	private bool anYCMiSKlXlHttoIcpSPkLkAJnO;

	public bool iieWZhRBsljdFtGHPxytLsAchca
	{
		get
		{
			if (rPgHxYLMAMLpWDHyEGyOAdoWvSr != HUIIQdnncKUebdiBTOVEYuWgLgX.oyFmZlsGKMhEtQbgUfkZIfoOcRAF)
			{
				return rPgHxYLMAMLpWDHyEGyOAdoWvSr == HUIIQdnncKUebdiBTOVEYuWgLgX.lPwGnjHMImHzXynWsiBVFitKJXq;
			}
			return true;
		}
	}

	public T HAdFnVcInfiPicSZgoueStNcxtXd => cxYyeNnYyHdGUipbGuKhDUfRLcg;

	public bool yZfgILSpZDuZYMDogqsfCgkSiWIi()
	{
		bool flag = rPgHxYLMAMLpWDHyEGyOAdoWvSr == HUIIQdnncKUebdiBTOVEYuWgLgX.lPwGnjHMImHzXynWsiBVFitKJXq;
		if (flag)
		{
			rPgHxYLMAMLpWDHyEGyOAdoWvSr = HUIIQdnncKUebdiBTOVEYuWgLgX.aJBsUCGKCJyqqlFSBHxzlMFfTTa;
		}
		return flag;
	}

	public GpBATPAcvcaIFoOOwrEnvAAzdSl(bool useSharedThread, Func<T> resultDelegate)
	{
		SsVHlbewYFBFvRQaBEebNIkZtod = useSharedThread;
		if (resultDelegate == null)
		{
			throw new ArgumentNullException("resultDelegate");
		}
		dtfwmYtedwKQBRtBslELJLShJEt = resultDelegate;
		xTXcDCftQBhcbaYDPhpTabHWhyR = hHagLpKfthlXHfZadTHlyYTpseTR;
		suemxSFKhFNwltYbyKPFOtyzDDZ = new object();
		rPgHxYLMAMLpWDHyEGyOAdoWvSr = HUIIQdnncKUebdiBTOVEYuWgLgX.aJBsUCGKCJyqqlFSBHxzlMFfTTa;
		if (useSharedThread)
		{
			izRzaEMoeHvcUJXDGStCwawypLw.aUVvLsnunUZVUpVlpeoAWgQOmfd();
		}
	}

	public bool TeIALMfSPWjNIZGaKblDTHZuhtNo()
	{
		lock (suemxSFKhFNwltYbyKPFOtyzDDZ)
		{
			if (rPgHxYLMAMLpWDHyEGyOAdoWvSr == HUIIQdnncKUebdiBTOVEYuWgLgX.oyFmZlsGKMhEtQbgUfkZIfoOcRAF)
			{
				return false;
			}
			cxYyeNnYyHdGUipbGuKhDUfRLcg = default(T);
			rPgHxYLMAMLpWDHyEGyOAdoWvSr = HUIIQdnncKUebdiBTOVEYuWgLgX.oyFmZlsGKMhEtQbgUfkZIfoOcRAF;
		}
		if (SsVHlbewYFBFvRQaBEebNIkZtod)
		{
			izRzaEMoeHvcUJXDGStCwawypLw.CKXFWwbNljtnixfVtFkWcsEBRDhn(xTXcDCftQBhcbaYDPhpTabHWhyR);
		}
		else
		{
			ThreadPool.QueueUserWorkItem(xTXcDCftQBhcbaYDPhpTabHWhyR, this);
		}
		return true;
	}

	public void gDIsdrSnGkjGabgrbnfmyYVEOLlV()
	{
		lock (suemxSFKhFNwltYbyKPFOtyzDDZ)
		{
			cxYyeNnYyHdGUipbGuKhDUfRLcg = default(T);
			rPgHxYLMAMLpWDHyEGyOAdoWvSr = HUIIQdnncKUebdiBTOVEYuWgLgX.aJBsUCGKCJyqqlFSBHxzlMFfTTa;
		}
	}

	private void hHagLpKfthlXHfZadTHlyYTpseTR(object P_0)
	{
		lock (suemxSFKhFNwltYbyKPFOtyzDDZ)
		{
			if (rPgHxYLMAMLpWDHyEGyOAdoWvSr == HUIIQdnncKUebdiBTOVEYuWgLgX.oyFmZlsGKMhEtQbgUfkZIfoOcRAF)
			{
				cxYyeNnYyHdGUipbGuKhDUfRLcg = dtfwmYtedwKQBRtBslELJLShJEt();
				rPgHxYLMAMLpWDHyEGyOAdoWvSr = HUIIQdnncKUebdiBTOVEYuWgLgX.lPwGnjHMImHzXynWsiBVFitKJXq;
			}
		}
	}

	public void EjOnxZffDhiNtnYcjfEZVyvxMWf()
	{
		EjOnxZffDhiNtnYcjfEZVyvxMWf(true);
		GC.SuppressFinalize(this);
	}

	~GpBATPAcvcaIFoOOwrEnvAAzdSl()
	{
		EjOnxZffDhiNtnYcjfEZVyvxMWf(false);
	}

	protected virtual void EjOnxZffDhiNtnYcjfEZVyvxMWf(bool P_0)
	{
		if (!anYCMiSKlXlHttoIcpSPkLkAJnO)
		{
			if (P_0)
			{
				gDIsdrSnGkjGabgrbnfmyYVEOLlV();
			}
			if (SsVHlbewYFBFvRQaBEebNIkZtod)
			{
				izRzaEMoeHvcUJXDGStCwawypLw.GElYoZzgvzbtHhJYgJKEcLohokZV();
			}
			anYCMiSKlXlHttoIcpSPkLkAJnO = true;
		}
	}
}
