namespace Rewired.Libraries.SharpDX.XInput
{
	internal enum DeviceSubType : byte
	{
		Gamepad = 1,
		Wheel = 2,
		ArcadeStick = 3,
		FlightStick = 4,
		DancePad = 5,
		Guitar = 6,
		GuitarAlternate = 7,
		DrumKit = 8,
		GuitarBass = 11,
		ArcadePad = 19
	}
}
