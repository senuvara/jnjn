using System;
using System.Runtime.InteropServices;

[StructLayout(LayoutKind.Sequential, Size = 4)]
internal struct sLgmABprbcIxwAFCDUCWVvtvuMy : IEquatable<sLgmABprbcIxwAFCDUCWVvtvuMy>
{
	private int TnxGBKcCSGJCoCpKfStuldQNavaA;

	public sLgmABprbcIxwAFCDUCWVvtvuMy(bool boolValue)
	{
		TnxGBKcCSGJCoCpKfStuldQNavaA = (boolValue ? 1 : 0);
	}

	public bool Equals(sLgmABprbcIxwAFCDUCWVvtvuMy other)
	{
		return TnxGBKcCSGJCoCpKfStuldQNavaA == other.TnxGBKcCSGJCoCpKfStuldQNavaA;
	}

	public override bool Equals(object obj)
	{
		if (object.ReferenceEquals(null, obj))
		{
			return false;
		}
		if (obj is sLgmABprbcIxwAFCDUCWVvtvuMy)
		{
			return Equals((sLgmABprbcIxwAFCDUCWVvtvuMy)obj);
		}
		return false;
	}

	public override int GetHashCode()
	{
		return TnxGBKcCSGJCoCpKfStuldQNavaA;
	}

	public static bool operator ==(sLgmABprbcIxwAFCDUCWVvtvuMy left, sLgmABprbcIxwAFCDUCWVvtvuMy right)
	{
		return left.Equals(right);
	}

	public static bool operator !=(sLgmABprbcIxwAFCDUCWVvtvuMy left, sLgmABprbcIxwAFCDUCWVvtvuMy right)
	{
		return !left.Equals(right);
	}

	public static implicit operator bool(sLgmABprbcIxwAFCDUCWVvtvuMy booleanValue)
	{
		return booleanValue.TnxGBKcCSGJCoCpKfStuldQNavaA != 0;
	}

	public static implicit operator sLgmABprbcIxwAFCDUCWVvtvuMy(bool boolValue)
	{
		return new sLgmABprbcIxwAFCDUCWVvtvuMy(boolValue);
	}

	public override string ToString()
	{
		return $"{TnxGBKcCSGJCoCpKfStuldQNavaA != 0}";
	}
}
