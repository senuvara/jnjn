using Rewired;
using Rewired.Utils;
using Rewired.Utils.Classes.Utility;
using System;
using System.Collections.Generic;
using System.Runtime.CompilerServices;

internal class LdIPsboFdjClIoTJlcmGGrWRxlFA : IDisposable
{
	private class uqzHrflFdrHOqZZDIRNkEmGZEda
	{
		public int mJofGFjmouHJOlXWWXturqEymqQ;

		public int pQAGBQekqvTqJUEqQZavQlnXQyln;

		public uint gGMEpaEOEFdbQzXtnbuDENgfCnQW;

		public object YJnRAxaUtnfvuqagdHssGdhbaGJk;

		public void sdMFFpAPDymkhBcyzKNlnikntGw(int P_0, int P_1, uint P_2, object P_3)
		{
			mJofGFjmouHJOlXWWXturqEymqQ = P_0;
			pQAGBQekqvTqJUEqQZavQlnXQyln = P_1;
			gGMEpaEOEFdbQzXtnbuDENgfCnQW = P_2;
			YJnRAxaUtnfvuqagdHssGdhbaGJk = P_3;
		}

		public void gDIsdrSnGkjGabgrbnfmyYVEOLlV()
		{
			YJnRAxaUtnfvuqagdHssGdhbaGJk = null;
		}
	}

	private UyktPcwWqOdLvLfofBsDxMHhZHt QFUOKjrWPIqMkntKNhjbAsiwCsZ;

	private ObjectPool<uqzHrflFdrHOqZZDIRNkEmGZEda> rqtIEzbhDmJwdZuptIQOaMvJpGF;

	private Queue<uqzHrflFdrHOqZZDIRNkEmGZEda> YTJTyBpfGObBeLynPqHxghdZmcM;

	private Action<object> TZUaQlEDhnKxsZmzNiKriErDOZN;

	private bool anYCMiSKlXlHttoIcpSPkLkAJnO;

	[CompilerGenerated]
	private static Func<uqzHrflFdrHOqZZDIRNkEmGZEda> VdIpOomonehzlCpfNqxGCOXHXVnb;

	[CompilerGenerated]
	private static Action<uqzHrflFdrHOqZZDIRNkEmGZEda> JqLkKhOaZTnAuoCfHddCHpjokfy;

	public bool jnVmkNxyVGeDahHKbgBDoIjYEHoK => hOlGiXFLXZLsZaydgZiRkhlfVPsW();

	public LdIPsboFdjClIoTJlcmGGrWRxlFA(int byteCapacity, int startingQueueSize, Action<object> lostCustomDataDisposalDelegate = null)
	{
		if (byteCapacity <= 0)
		{
			throw new ArgumentOutOfRangeException("capacity");
		}
		QFUOKjrWPIqMkntKNhjbAsiwCsZ = new UyktPcwWqOdLvLfofBsDxMHhZHt(byteCapacity);
		rqtIEzbhDmJwdZuptIQOaMvJpGF = new ObjectPool<uqzHrflFdrHOqZZDIRNkEmGZEda>(startingQueueSize, () => new uqzHrflFdrHOqZZDIRNkEmGZEda(), delegate(uqzHrflFdrHOqZZDIRNkEmGZEda P_0)
		{
			P_0.gDIsdrSnGkjGabgrbnfmyYVEOLlV();
		});
		YTJTyBpfGObBeLynPqHxghdZmcM = new Queue<uqzHrflFdrHOqZZDIRNkEmGZEda>(startingQueueSize);
		TZUaQlEDhnKxsZmzNiKriErDOZN = lostCustomDataDisposalDelegate;
	}

	public unsafe bool TeIALMfSPWjNIZGaKblDTHZuhtNo(byte* P_0, int P_1, object P_2)
	{
		if (P_0 == null || P_1 <= 0)
		{
			return false;
		}
		int num;
		uint num2;
		int num3 = QFUOKjrWPIqMkntKNhjbAsiwCsZ.jNSHZfddqcrPRRdjZjdCecTCpoNb(P_0, P_1, P_1, out num, out num2);
		if (num3 < P_1)
		{
			return false;
		}
		uqzHrflFdrHOqZZDIRNkEmGZEda uqzHrflFdrHOqZZDIRNkEmGZEda = rqtIEzbhDmJwdZuptIQOaMvJpGF.Get();
		uqzHrflFdrHOqZZDIRNkEmGZEda.sdMFFpAPDymkhBcyzKNlnikntGw(num, P_1, num2, P_2);
		YTJTyBpfGObBeLynPqHxghdZmcM.Enqueue(uqzHrflFdrHOqZZDIRNkEmGZEda);
		return true;
	}

	public unsafe bool TeIALMfSPWjNIZGaKblDTHZuhtNo(byte* P_0, int P_1)
	{
		return TeIALMfSPWjNIZGaKblDTHZuhtNo(P_0, P_1, null);
	}

	public unsafe bool TeIALMfSPWjNIZGaKblDTHZuhtNo(IntPtr P_0, int P_1, object P_2)
	{
		if (P_0 == IntPtr.Zero || P_1 <= 0)
		{
			return false;
		}
		return TeIALMfSPWjNIZGaKblDTHZuhtNo((byte*)(void*)P_0, P_1, P_2);
	}

	public bool TeIALMfSPWjNIZGaKblDTHZuhtNo(IntPtr P_0, int P_1)
	{
		return TeIALMfSPWjNIZGaKblDTHZuhtNo(P_0, P_1, null);
	}

	public unsafe bool TeIALMfSPWjNIZGaKblDTHZuhtNo(byte[] P_0, int P_1, object P_2, int P_3 = 0)
	{
		if (P_0 == null || P_1 > P_0.Length)
		{
			return false;
		}
		if (P_3 < 0)
		{
			P_3 = 0;
		}
		if (P_3 + P_1 > P_0.Length)
		{
			return false;
		}
		fixed (byte* ptr = P_0)
		{
			byte* ptr2 = ptr + P_3;
			return TeIALMfSPWjNIZGaKblDTHZuhtNo(ptr2, P_1, P_2);
		}
	}

	public bool TeIALMfSPWjNIZGaKblDTHZuhtNo(byte[] P_0, int P_1, int P_2 = 0)
	{
		return TeIALMfSPWjNIZGaKblDTHZuhtNo(P_0, P_1, null, P_2);
	}

	public unsafe int eriADQINSMbdpIYSHYTCRrmbuJda(byte* P_0, int P_1, out object P_2)
	{
		if (P_0 == null || P_1 <= 0)
		{
			P_2 = null;
			return -1;
		}
		uqzHrflFdrHOqZZDIRNkEmGZEda uqzHrflFdrHOqZZDIRNkEmGZEda = SxiPcQWPYXRkHuLoOziZDReZbdp(false);
		if (uqzHrflFdrHOqZZDIRNkEmGZEda == null)
		{
			P_2 = null;
			return -1;
		}
		if (P_1 < uqzHrflFdrHOqZZDIRNkEmGZEda.pQAGBQekqvTqJUEqQZavQlnXQyln)
		{
			Logger.LogError("The buffer is too small to hold the data. Call PeekDataLength before calling Peek to get the data length.", requiredThreadSafety: true);
			P_2 = null;
			return -1;
		}
		int num = QFUOKjrWPIqMkntKNhjbAsiwCsZ.zmuQFDmSbXcTeDCtbvMlsPhdCJX(P_0, P_1, uqzHrflFdrHOqZZDIRNkEmGZEda.pQAGBQekqvTqJUEqQZavQlnXQyln, uqzHrflFdrHOqZZDIRNkEmGZEda.mJofGFjmouHJOlXWWXturqEymqQ);
		if (num != uqzHrflFdrHOqZZDIRNkEmGZEda.pQAGBQekqvTqJUEqQZavQlnXQyln)
		{
			Logger.LogError("Failure reading data from buffer!", requiredThreadSafety: true);
			num = 0;
			P_2 = null;
			return -1;
		}
		P_2 = uqzHrflFdrHOqZZDIRNkEmGZEda.YJnRAxaUtnfvuqagdHssGdhbaGJk;
		return num;
	}

	public unsafe int eriADQINSMbdpIYSHYTCRrmbuJda(byte* P_0, int P_1)
	{
		object obj;
		return eriADQINSMbdpIYSHYTCRrmbuJda(P_0, P_1, out obj);
	}

	public unsafe int eriADQINSMbdpIYSHYTCRrmbuJda(IntPtr P_0, int P_1, out object P_2)
	{
		if (P_0 == IntPtr.Zero || P_1 <= 0)
		{
			P_2 = null;
			return -1;
		}
		return eriADQINSMbdpIYSHYTCRrmbuJda((byte*)(void*)P_0, P_1, out P_2);
	}

	public int eriADQINSMbdpIYSHYTCRrmbuJda(IntPtr P_0, int P_1)
	{
		object obj;
		return eriADQINSMbdpIYSHYTCRrmbuJda(P_0, P_1, out obj);
	}

	public unsafe int eriADQINSMbdpIYSHYTCRrmbuJda(byte[] P_0, out object P_1)
	{
		if (P_0 == null || P_0.Length == 0)
		{
			P_1 = null;
			return -1;
		}
		fixed (byte* ptr = P_0)
		{
			return eriADQINSMbdpIYSHYTCRrmbuJda(ptr, P_0.Length, out P_1);
		}
	}

	public int eriADQINSMbdpIYSHYTCRrmbuJda(byte[] P_0)
	{
		object obj;
		return eriADQINSMbdpIYSHYTCRrmbuJda(P_0, out obj);
	}

	public int FTuczIFJMnBkGrWnVPVQDCFrzMKA()
	{
		return SxiPcQWPYXRkHuLoOziZDReZbdp(false)?.pQAGBQekqvTqJUEqQZavQlnXQyln ?? (-1);
	}

	public unsafe int GZDWbZCDrBKSLYIzoQwATeksvSc(byte* P_0, int P_1, out object P_2)
	{
		if (P_0 == null || P_1 <= 0)
		{
			P_2 = null;
			return -1;
		}
		uqzHrflFdrHOqZZDIRNkEmGZEda uqzHrflFdrHOqZZDIRNkEmGZEda = SxiPcQWPYXRkHuLoOziZDReZbdp(true);
		if (uqzHrflFdrHOqZZDIRNkEmGZEda == null)
		{
			P_2 = null;
			return -1;
		}
		if (P_1 < uqzHrflFdrHOqZZDIRNkEmGZEda.pQAGBQekqvTqJUEqQZavQlnXQyln)
		{
			Logger.LogError("The buffer is too small to hold the data. Call PeekDataLength before calling Dequeue to get the data length.", requiredThreadSafety: true);
			P_2 = null;
			dVIHrVZfXLFNhbmupMdTAxnZWyyB(uqzHrflFdrHOqZZDIRNkEmGZEda, true);
			return -1;
		}
		int num = QFUOKjrWPIqMkntKNhjbAsiwCsZ.zmuQFDmSbXcTeDCtbvMlsPhdCJX(P_0, P_1, uqzHrflFdrHOqZZDIRNkEmGZEda.pQAGBQekqvTqJUEqQZavQlnXQyln, uqzHrflFdrHOqZZDIRNkEmGZEda.mJofGFjmouHJOlXWWXturqEymqQ);
		if (num != uqzHrflFdrHOqZZDIRNkEmGZEda.pQAGBQekqvTqJUEqQZavQlnXQyln)
		{
			Logger.LogError("Failure reading data from buffer!", requiredThreadSafety: true);
			P_2 = null;
			dVIHrVZfXLFNhbmupMdTAxnZWyyB(uqzHrflFdrHOqZZDIRNkEmGZEda, true);
			return -1;
		}
		P_2 = uqzHrflFdrHOqZZDIRNkEmGZEda.YJnRAxaUtnfvuqagdHssGdhbaGJk;
		dVIHrVZfXLFNhbmupMdTAxnZWyyB(uqzHrflFdrHOqZZDIRNkEmGZEda, false);
		return num;
	}

	public unsafe int GZDWbZCDrBKSLYIzoQwATeksvSc(byte* P_0, int P_1)
	{
		object obj;
		return GZDWbZCDrBKSLYIzoQwATeksvSc(P_0, P_1, out obj);
	}

	public unsafe int GZDWbZCDrBKSLYIzoQwATeksvSc(IntPtr P_0, int P_1, out object P_2)
	{
		if (P_0 == IntPtr.Zero || P_1 <= 0)
		{
			P_2 = null;
			return -1;
		}
		return GZDWbZCDrBKSLYIzoQwATeksvSc((byte*)(void*)P_0, P_1, out P_2);
	}

	public int GZDWbZCDrBKSLYIzoQwATeksvSc(IntPtr P_0, int P_1)
	{
		object obj;
		return GZDWbZCDrBKSLYIzoQwATeksvSc(P_0, P_1, out obj);
	}

	public unsafe int GZDWbZCDrBKSLYIzoQwATeksvSc(byte[] P_0, out object P_1)
	{
		if (P_0 == null || P_0.Length == 0)
		{
			P_1 = null;
			return -1;
		}
		fixed (byte* ptr = P_0)
		{
			return GZDWbZCDrBKSLYIzoQwATeksvSc(ptr, P_0.Length, out P_1);
		}
	}

	public int GZDWbZCDrBKSLYIzoQwATeksvSc(byte[] P_0)
	{
		object obj;
		return GZDWbZCDrBKSLYIzoQwATeksvSc(P_0, out obj);
	}

	public void TDwDOZYKTqABodcWmgQwQubHPlyH()
	{
		QFUOKjrWPIqMkntKNhjbAsiwCsZ.TDwDOZYKTqABodcWmgQwQubHPlyH();
		while (YTJTyBpfGObBeLynPqHxghdZmcM.Count > 0)
		{
			dVIHrVZfXLFNhbmupMdTAxnZWyyB(YTJTyBpfGObBeLynPqHxghdZmcM.Dequeue(), true);
		}
	}

	private uqzHrflFdrHOqZZDIRNkEmGZEda SxiPcQWPYXRkHuLoOziZDReZbdp(bool P_0)
	{
		while (YTJTyBpfGObBeLynPqHxghdZmcM.Count > 0)
		{
			uqzHrflFdrHOqZZDIRNkEmGZEda uqzHrflFdrHOqZZDIRNkEmGZEda = P_0 ? YTJTyBpfGObBeLynPqHxghdZmcM.Dequeue() : YTJTyBpfGObBeLynPqHxghdZmcM.Peek();
			if (QFUOKjrWPIqMkntKNhjbAsiwCsZ.yUTeePwfyJDwqCbXgklljZMFvvI(uqzHrflFdrHOqZZDIRNkEmGZEda.mJofGFjmouHJOlXWWXturqEymqQ, uqzHrflFdrHOqZZDIRNkEmGZEda.gGMEpaEOEFdbQzXtnbuDENgfCnQW))
			{
				return uqzHrflFdrHOqZZDIRNkEmGZEda;
			}
			if (!P_0)
			{
				uqzHrflFdrHOqZZDIRNkEmGZEda = YTJTyBpfGObBeLynPqHxghdZmcM.Dequeue();
			}
			dVIHrVZfXLFNhbmupMdTAxnZWyyB(uqzHrflFdrHOqZZDIRNkEmGZEda, true);
		}
		return null;
	}

	private bool hOlGiXFLXZLsZaydgZiRkhlfVPsW()
	{
		return SxiPcQWPYXRkHuLoOziZDReZbdp(false) != null;
	}

	private void dVIHrVZfXLFNhbmupMdTAxnZWyyB(uqzHrflFdrHOqZZDIRNkEmGZEda P_0, bool P_1)
	{
		if (P_0 != null)
		{
			if (P_1 && TZUaQlEDhnKxsZmzNiKriErDOZN != null && P_0.YJnRAxaUtnfvuqagdHssGdhbaGJk != null)
			{
				TZUaQlEDhnKxsZmzNiKriErDOZN(P_0.YJnRAxaUtnfvuqagdHssGdhbaGJk);
			}
			rqtIEzbhDmJwdZuptIQOaMvJpGF.Return(P_0);
		}
	}

	public void Dispose()
	{
		EjOnxZffDhiNtnYcjfEZVyvxMWf(true);
		GC.SuppressFinalize(this);
	}

	~LdIPsboFdjClIoTJlcmGGrWRxlFA()
	{
		EjOnxZffDhiNtnYcjfEZVyvxMWf(false);
	}

	protected void EjOnxZffDhiNtnYcjfEZVyvxMWf(bool P_0)
	{
		if (anYCMiSKlXlHttoIcpSPkLkAJnO)
		{
			return;
		}
		if (P_0)
		{
			TDwDOZYKTqABodcWmgQwQubHPlyH();
			if (QFUOKjrWPIqMkntKNhjbAsiwCsZ != null)
			{
				QFUOKjrWPIqMkntKNhjbAsiwCsZ.Dispose();
			}
		}
		anYCMiSKlXlHttoIcpSPkLkAJnO = true;
	}

	public static bool tFBLiWxAVBdsEXoimVzqYDgWznC(LdIPsboFdjClIoTJlcmGGrWRxlFA P_0, LdIPsboFdjClIoTJlcmGGrWRxlFA P_1)
	{
		if (P_0 == null || P_1 == null)
		{
			return false;
		}
		MiscTools.Swap(ref P_0.QFUOKjrWPIqMkntKNhjbAsiwCsZ, ref P_1.QFUOKjrWPIqMkntKNhjbAsiwCsZ);
		MiscTools.Swap(ref P_0.rqtIEzbhDmJwdZuptIQOaMvJpGF, ref P_1.rqtIEzbhDmJwdZuptIQOaMvJpGF);
		MiscTools.Swap(ref P_0.YTJTyBpfGObBeLynPqHxghdZmcM, ref P_1.YTJTyBpfGObBeLynPqHxghdZmcM);
		return true;
	}

	[CompilerGenerated]
	private static uqzHrflFdrHOqZZDIRNkEmGZEda cgXBNEGwoXzJiAQbsPIkWIdEwrIM()
	{
		return new uqzHrflFdrHOqZZDIRNkEmGZEda();
	}

	[CompilerGenerated]
	private static void sIyutUTgmYpsrzZELJJGdugEEtb(uqzHrflFdrHOqZZDIRNkEmGZEda P_0)
	{
		P_0.gDIsdrSnGkjGabgrbnfmyYVEOLlV();
	}
}
