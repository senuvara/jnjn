using Rewired;
using System.Diagnostics;

internal static class MGonFwWeRnfTkKWqQQSNKrofMMM
{
	[Conditional("WINDEBUG")]
	public static void BNsZHYUoKCJQeSwJFzRGYMOUMlr(object P_0)
	{
		if (P_0 == null)
		{
			P_0 = string.Empty;
			goto IL_000a;
		}
		goto IL_002c;
		IL_000a:
		int num = 505887190;
		goto IL_000f;
		IL_002c:
		object[] array = new object[4]
		{
			"[WINDEBUG][",
			null,
			null,
			null
		};
		num = 505887189;
		goto IL_000f;
		IL_000f:
		while (true)
		{
			switch (num ^ 0x1E2739D4)
			{
			case 0:
				break;
			case 2:
				goto IL_002c;
			case 1:
				array[1] = ReInput.realTime.ToString("f3");
				array[2] = "] ";
				array[3] = P_0;
				num = 505887191;
				continue;
			default:
				Logger.Log(string.Concat(array), requiredThreadSafety: true);
				return;
			}
			break;
		}
		goto IL_000a;
	}

	[Conditional("WINDEBUG")]
	public static void pFpdrQALLjmxCFAwHdGRNMFEIYaR(object P_0)
	{
		if (P_0 == null)
		{
			P_0 = string.Empty;
			goto IL_000a;
		}
		goto IL_0030;
		IL_000a:
		int num = -448221563;
		goto IL_000f;
		IL_0030:
		object[] array = new object[4]
		{
			"[WINDEBUG][",
			ReInput.realTime.ToString("f3"),
			null,
			null
		};
		num = -448221561;
		goto IL_000f;
		IL_000f:
		while (true)
		{
			switch (num ^ -448221564)
			{
			case 0:
				break;
			case 1:
				goto IL_0030;
			case 3:
				array[2] = "] ";
				num = -448221568;
				continue;
			case 4:
				array[3] = P_0;
				num = -448221562;
				continue;
			default:
				Logger.LogWarning(string.Concat(array), requiredThreadSafety: true);
				return;
			}
			break;
		}
		goto IL_000a;
	}

	[Conditional("WINDEBUG")]
	public static void EUkUmLHHnKHUVtBMdFQYApLXphs(object P_0)
	{
		if (P_0 == null)
		{
			P_0 = string.Empty;
		}
		Logger.LogError("[WINDEBUG][" + ReInput.realTime.ToString("f3") + "] " + P_0, requiredThreadSafety: true);
	}

	[Conditional("WINDEBUGDEEP")]
	public static void DPbuYikDWqGaRallnICIFTnsUdoY(object P_0)
	{
		if (P_0 == null)
		{
			P_0 = string.Empty;
			goto IL_000a;
		}
		goto IL_0028;
		IL_000a:
		int num = -218434205;
		goto IL_000f;
		IL_0028:
		object[] args = new object[4]
		{
			"[WINDEBUG][",
			ReInput.realTime.ToString("f3"),
			"] ",
			P_0
		};
		num = -218434206;
		goto IL_000f;
		IL_000f:
		switch (num ^ -218434206)
		{
		case 2:
			break;
		case 1:
			goto IL_0028;
		default:
			Logger.Log(string.Concat(args), requiredThreadSafety: true);
			return;
		}
		goto IL_000a;
	}

	[Conditional("WINDEBUGDEEP")]
	public static void lFISiVqDhMjjkkWlfpoAGLJfIgN(object P_0)
	{
		if (P_0 == null)
		{
			P_0 = string.Empty;
			goto IL_000a;
		}
		goto IL_0067;
		IL_000a:
		int num = 1715109696;
		goto IL_000f;
		IL_0067:
		object[] array = new object[4];
		num = 1715109697;
		goto IL_000f;
		IL_000f:
		while (true)
		{
			switch (num ^ 0x663A7F41)
			{
			case 2:
				break;
			case 3:
				array[3] = P_0;
				num = 1715109701;
				continue;
			case 0:
				array[0] = "[WINDEBUG][";
				array[1] = ReInput.realTime.ToString("f3");
				array[2] = "] ";
				num = 1715109698;
				continue;
			case 1:
				goto IL_0067;
			default:
				Logger.LogWarning(string.Concat(array), requiredThreadSafety: true);
				return;
			}
			break;
		}
		goto IL_000a;
	}

	[Conditional("WINDEBUGDEEP")]
	public static void oAOKMXPUhTCnZHcFrJDCtGYhcZa(object P_0)
	{
		if (P_0 == null)
		{
			P_0 = string.Empty;
			goto IL_000a;
		}
		goto IL_002c;
		IL_000a:
		int num = 1606902465;
		goto IL_000f;
		IL_002c:
		object[] array = new object[4]
		{
			"[WINDEBUG][",
			null,
			null,
			null
		};
		num = 1606902467;
		goto IL_000f;
		IL_000f:
		while (true)
		{
			switch (num ^ 0x5FC762C0)
			{
			case 2:
				break;
			case 1:
				goto IL_002c;
			case 3:
				array[1] = ReInput.realTime.ToString("f3");
				num = 1606902464;
				continue;
			default:
				array[2] = "] ";
				array[3] = P_0;
				Logger.LogError(string.Concat(array), requiredThreadSafety: true);
				return;
			}
			break;
		}
		goto IL_000a;
	}

	[Conditional("WINDEBUGDEEPUPDATE")]
	public static void WonLNZytlqiTlpGcJobNEvpVsuo(object P_0)
	{
		if (P_0 == null)
		{
			P_0 = string.Empty;
			goto IL_000a;
		}
		goto IL_0028;
		IL_000a:
		int num = -1331848382;
		goto IL_000f;
		IL_0028:
		object[] args = new object[4]
		{
			"[WINDEBUG][",
			ReInput.realTime.ToString("f3"),
			"] ",
			P_0
		};
		num = -1331848381;
		goto IL_000f;
		IL_000f:
		switch (num ^ -1331848381)
		{
		case 2:
			break;
		case 1:
			goto IL_0028;
		default:
			Logger.Log(string.Concat(args), requiredThreadSafety: true);
			return;
		}
		goto IL_000a;
	}

	[Conditional("WINDEBUGHID")]
	public static void ztxAGpgStdRWGkYpckZpJInghZj(object P_0)
	{
		if (P_0 == null)
		{
			goto IL_0003;
		}
		goto IL_0033;
		IL_0033:
		Logger.Log("[WINDEBUGHID][" + ReInput.realTime.ToString("f3") + "] " + P_0, requiredThreadSafety: true);
		int num = 1918707943;
		goto IL_0008;
		IL_0008:
		while (true)
		{
			switch (num ^ 0x725D28E4)
			{
			default:
				return;
			case 3:
				return;
			case 0:
				break;
			case 1:
				P_0 = string.Empty;
				num = 1918707942;
				continue;
			case 2:
				goto IL_0033;
			}
			break;
		}
		goto IL_0003;
		IL_0003:
		num = 1918707941;
		goto IL_0008;
	}
}
