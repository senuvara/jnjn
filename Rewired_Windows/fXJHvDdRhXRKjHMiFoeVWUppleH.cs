using System;

internal class fXJHvDdRhXRKjHMiFoeVWUppleH : IDisposable
{
	private readonly RlPUshuJvxgyKHIGdBAvTNRRwiC QFUOKjrWPIqMkntKNhjbAsiwCsZ;

	private bool[] qgBtNmmHCZfsbMhBKQbmziSaAUv;

	protected readonly int YswdOeeENivEkAibAeTHGhnCuVll;

	protected readonly int pAkjpDkgPHTsyGXDZOTbhtbcqZJ;

	private bool anYCMiSKlXlHttoIcpSPkLkAJnO;

	public int yNOgfQKNReKDIFqaLgkNyqoznOxU => YswdOeeENivEkAibAeTHGhnCuVll;

	public int NeOIxedQuUfesjEtYNJvaRWEVKK => pAkjpDkgPHTsyGXDZOTbhtbcqZJ;

	public bool[] cevgCtgUKikKBidXNUVOeByVbtFD => qgBtNmmHCZfsbMhBKQbmziSaAUv ?? (qgBtNmmHCZfsbMhBKQbmziSaAUv = new bool[YswdOeeENivEkAibAeTHGhnCuVll]);

	public fXJHvDdRhXRKjHMiFoeVWUppleH(int length, int valueBitSize)
	{
		if (length <= 0)
		{
			throw new ArgumentOutOfRangeException("length");
		}
		if (valueBitSize <= 0)
		{
			throw new ArgumentOutOfRangeException("entryBitSize");
		}
		pAkjpDkgPHTsyGXDZOTbhtbcqZJ = length;
		YswdOeeENivEkAibAeTHGhnCuVll = valueBitSize;
		int num = length * valueBitSize;
		int size = num / 8 + ((num % 8 != 0) ? 1 : 0);
		QFUOKjrWPIqMkntKNhjbAsiwCsZ = new RlPUshuJvxgyKHIGdBAvTNRRwiC(size);
	}

	public unsafe void ORSchllRWVjoUMVCKmtBEsGyvPq(int P_0, byte* P_1, int P_2)
	{
		if (P_0 < 0 || P_0 >= pAkjpDkgPHTsyGXDZOTbhtbcqZJ)
		{
			throw new IndexOutOfRangeException("index");
		}
		if (P_1 == null)
		{
			throw new ArgumentNullException("buffer");
		}
		if (P_2 < YswdOeeENivEkAibAeTHGhnCuVll)
		{
			throw new Exception("Buffer is too small to hold the data. Must be at least " + YswdOeeENivEkAibAeTHGhnCuVll + " bits.");
		}
		int num = 0;
		int num2 = 0;
		for (int i = 0; i < YswdOeeENivEkAibAeTHGhnCuVll; i++)
		{
			cPsHmVyiuzxRJQzTPIoCUorxqZB(P_0, i, out int num3, out byte b);
			P_1[i] = (QFUOKjrWPIqMkntKNhjbAsiwCsZ.VfmjBUIrErtIybHCHodHOYYObyF(num3, b) ? ((byte)(P_1[num] | (1 << num2))) : ((byte)(P_1[num] & ~(1 << num2))));
			num2++;
			if (num2 >= 8)
			{
				num++;
				num2 = 0;
			}
		}
	}

	public unsafe void ORSchllRWVjoUMVCKmtBEsGyvPq(int P_0, IntPtr P_1, int P_2)
	{
		if (P_1 == IntPtr.Zero)
		{
			throw new ArgumentNullException("buffer");
		}
		ORSchllRWVjoUMVCKmtBEsGyvPq(P_0, (byte*)(void*)P_1, P_2);
	}

	public unsafe void ORSchllRWVjoUMVCKmtBEsGyvPq(int P_0, out byte P_1)
	{
		byte b = 0;
		byte* ptr = &b;
		ORSchllRWVjoUMVCKmtBEsGyvPq(P_0, ptr, 64);
		P_1 = b;
	}

	public void ORSchllRWVjoUMVCKmtBEsGyvPq(int P_0, out sbyte P_1)
	{
		ORSchllRWVjoUMVCKmtBEsGyvPq(P_0, out byte b);
		P_1 = (sbyte)b;
	}

	public unsafe void ORSchllRWVjoUMVCKmtBEsGyvPq(int P_0, out short P_1)
	{
		short num = 0;
		byte* ptr = (byte*)(&num);
		ORSchllRWVjoUMVCKmtBEsGyvPq(P_0, ptr, 64);
		P_1 = num;
	}

	public void ORSchllRWVjoUMVCKmtBEsGyvPq(int P_0, out ushort P_1)
	{
		ORSchllRWVjoUMVCKmtBEsGyvPq(P_0, out short num);
		P_1 = (ushort)num;
	}

	public unsafe void ORSchllRWVjoUMVCKmtBEsGyvPq(int P_0, out int P_1)
	{
		int num = 0;
		byte* ptr = (byte*)(&num);
		ORSchllRWVjoUMVCKmtBEsGyvPq(P_0, ptr, 64);
		P_1 = num;
	}

	public void ORSchllRWVjoUMVCKmtBEsGyvPq(int P_0, out uint P_1)
	{
		ORSchllRWVjoUMVCKmtBEsGyvPq(P_0, out int num);
		P_1 = (uint)num;
	}

	public unsafe void ORSchllRWVjoUMVCKmtBEsGyvPq(int P_0, out long P_1)
	{
		long num = 0L;
		byte* ptr = (byte*)(&num);
		ORSchllRWVjoUMVCKmtBEsGyvPq(P_0, ptr, 64);
		P_1 = num;
	}

	public void ORSchllRWVjoUMVCKmtBEsGyvPq(int P_0, out ulong P_1)
	{
		ORSchllRWVjoUMVCKmtBEsGyvPq(P_0, out long num);
		P_1 = (ulong)num;
	}

	public void ORSchllRWVjoUMVCKmtBEsGyvPq(int P_0, bool[] P_1)
	{
		if (P_0 < 0 || P_0 >= pAkjpDkgPHTsyGXDZOTbhtbcqZJ)
		{
			throw new IndexOutOfRangeException("index");
		}
		if (P_1 == null)
		{
			throw new ArgumentNullException("valueBuffer");
		}
		if (P_1.Length < YswdOeeENivEkAibAeTHGhnCuVll)
		{
			throw new Exception("valueBuffer.Length must be >= " + YswdOeeENivEkAibAeTHGhnCuVll);
		}
		for (int i = 0; i < YswdOeeENivEkAibAeTHGhnCuVll; i++)
		{
			cPsHmVyiuzxRJQzTPIoCUorxqZB(P_0, i, out int num, out byte b);
			P_1[i] = QFUOKjrWPIqMkntKNhjbAsiwCsZ.VfmjBUIrErtIybHCHodHOYYObyF(num, b);
		}
	}

	public unsafe void sdMFFpAPDymkhBcyzKNlnikntGw(int P_0, byte* P_1, int P_2)
	{
		if (P_0 < 0 || P_0 >= pAkjpDkgPHTsyGXDZOTbhtbcqZJ)
		{
			throw new IndexOutOfRangeException("index");
		}
		if (P_1 == null)
		{
			throw new ArgumentNullException("buffer");
		}
		if (P_2 <= 0)
		{
			throw new Exception("bufferSize must be >= 0");
		}
		int num = 0;
		int num2 = 0;
		for (int i = 0; i < YswdOeeENivEkAibAeTHGhnCuVll; i++)
		{
			cPsHmVyiuzxRJQzTPIoCUorxqZB(P_0, i, out int num3, out byte b);
			bool flag = i < P_2 && (flag = ((P_1[num] & (1 << num2)) != 0));
			QFUOKjrWPIqMkntKNhjbAsiwCsZ.ljgdPSGWbOWjrjKBAhHBgIOzGJJC(num3, b, flag);
			num2++;
			if (num2 >= 8)
			{
				num++;
				num2 = 0;
			}
		}
	}

	public unsafe void sdMFFpAPDymkhBcyzKNlnikntGw(int P_0, IntPtr P_1, int P_2)
	{
		if (P_1 == IntPtr.Zero)
		{
			throw new ArgumentNullException("buffer");
		}
		sdMFFpAPDymkhBcyzKNlnikntGw(P_0, (byte*)(void*)P_1, P_2);
	}

	public unsafe void sdMFFpAPDymkhBcyzKNlnikntGw(int P_0, byte P_1)
	{
		byte* ptr = &P_1;
		sdMFFpAPDymkhBcyzKNlnikntGw(P_0, ptr, 8);
	}

	public void sdMFFpAPDymkhBcyzKNlnikntGw(int P_0, sbyte P_1)
	{
		sdMFFpAPDymkhBcyzKNlnikntGw(P_0, (byte)P_1);
	}

	public unsafe void sdMFFpAPDymkhBcyzKNlnikntGw(int P_0, short P_1)
	{
		byte* ptr = (byte*)(&P_1);
		sdMFFpAPDymkhBcyzKNlnikntGw(P_0, ptr, 16);
	}

	public void sdMFFpAPDymkhBcyzKNlnikntGw(int P_0, ushort P_1)
	{
		sdMFFpAPDymkhBcyzKNlnikntGw(P_0, (short)P_1);
	}

	public unsafe void sdMFFpAPDymkhBcyzKNlnikntGw(int P_0, int P_1)
	{
		byte* ptr = (byte*)(&P_1);
		sdMFFpAPDymkhBcyzKNlnikntGw(P_0, ptr, 32);
	}

	public void sdMFFpAPDymkhBcyzKNlnikntGw(int P_0, uint P_1)
	{
		sdMFFpAPDymkhBcyzKNlnikntGw(P_0, (int)P_1);
	}

	public unsafe void sdMFFpAPDymkhBcyzKNlnikntGw(int P_0, long P_1)
	{
		byte* ptr = (byte*)(&P_1);
		sdMFFpAPDymkhBcyzKNlnikntGw(P_0, ptr, 64);
	}

	public void sdMFFpAPDymkhBcyzKNlnikntGw(int P_0, ulong P_1)
	{
		sdMFFpAPDymkhBcyzKNlnikntGw(P_0, (long)P_1);
	}

	public void sdMFFpAPDymkhBcyzKNlnikntGw(int P_0, bool[] P_1)
	{
		if (P_0 < 0 || P_0 >= pAkjpDkgPHTsyGXDZOTbhtbcqZJ)
		{
			throw new IndexOutOfRangeException("index");
		}
		if (P_1 == null)
		{
			throw new ArgumentNullException("valueBuffer");
		}
		if (P_1.Length < YswdOeeENivEkAibAeTHGhnCuVll)
		{
			throw new Exception("valueBuffer.Length must be >= " + YswdOeeENivEkAibAeTHGhnCuVll);
		}
		for (int i = 0; i < YswdOeeENivEkAibAeTHGhnCuVll; i++)
		{
			cPsHmVyiuzxRJQzTPIoCUorxqZB(P_0, i, out int num, out byte b);
			QFUOKjrWPIqMkntKNhjbAsiwCsZ.ljgdPSGWbOWjrjKBAhHBgIOzGJJC(num, b, P_1[i]);
		}
	}

	private void cPsHmVyiuzxRJQzTPIoCUorxqZB(int P_0, int P_1, out int P_2, out byte P_3)
	{
		if (P_0 < 0 || P_0 >= pAkjpDkgPHTsyGXDZOTbhtbcqZJ)
		{
			throw new IndexOutOfRangeException("entryIndex");
		}
		if (P_1 < 0 || P_1 >= YswdOeeENivEkAibAeTHGhnCuVll)
		{
			throw new ArgumentOutOfRangeException("bitOffset");
		}
		int num = P_0 * YswdOeeENivEkAibAeTHGhnCuVll + P_1;
		P_2 = num / YswdOeeENivEkAibAeTHGhnCuVll;
		P_3 = (byte)(num - P_2 * YswdOeeENivEkAibAeTHGhnCuVll);
	}

	private int gNPFojgCRQTAUjEpjYWUCveGcjw(int P_0, out byte P_1)
	{
		if (P_0 < 0 || P_0 >= pAkjpDkgPHTsyGXDZOTbhtbcqZJ * YswdOeeENivEkAibAeTHGhnCuVll)
		{
			throw new IndexOutOfRangeException("bitIndex");
		}
		int num = P_0 / YswdOeeENivEkAibAeTHGhnCuVll;
		P_1 = (byte)(P_0 - num * YswdOeeENivEkAibAeTHGhnCuVll);
		return num;
	}

	public void Dispose()
	{
		EjOnxZffDhiNtnYcjfEZVyvxMWf(true);
		GC.SuppressFinalize(this);
	}

	~fXJHvDdRhXRKjHMiFoeVWUppleH()
	{
		EjOnxZffDhiNtnYcjfEZVyvxMWf(false);
	}

	protected virtual void EjOnxZffDhiNtnYcjfEZVyvxMWf(bool P_0)
	{
		if (!anYCMiSKlXlHttoIcpSPkLkAJnO)
		{
			if (P_0 && QFUOKjrWPIqMkntKNhjbAsiwCsZ != null)
			{
				QFUOKjrWPIqMkntKNhjbAsiwCsZ.Dispose();
			}
			anYCMiSKlXlHttoIcpSPkLkAJnO = true;
		}
	}
}
