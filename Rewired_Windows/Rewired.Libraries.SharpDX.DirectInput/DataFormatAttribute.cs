using System;

namespace Rewired.Libraries.SharpDX.DirectInput
{
	[AttributeUsage(AttributeTargets.Class | AttributeTargets.Struct)]
	internal sealed class DataFormatAttribute : Attribute
	{
		public ZtYJZZgkYaAXiaIIlDFRzgWcABY Flags;

		public DataFormatAttribute(ZtYJZZgkYaAXiaIIlDFRzgWcABY flags)
		{
			Flags = flags;
		}
	}
}
