using Rewired.Utils.Classes.Utility;
using System;
using System.Collections.Generic;
using System.Runtime.InteropServices;
using System.Security;

namespace Rewired.Libraries.SharpDX.DirectInput
{
	[Guid("bf798031-483a-4da2-aa99-5d64ed369700")]
	internal class DirectInput : hconbvlfPZALCMpGAEbgHkhCpWy
	{
		public DirectInput()
			: base(IntPtr.Zero)
		{
			QASFGLOvOLpMJOhLfCyKbaBwTOWp.LYGXndwChsjTrxmHtZAOLORYKFs(qhEnEUeUnCxZBGfcCXYUTOETYLJ.YkufeJdOsrIRgrbOYdaMScNePqP(null), 2048, ZAhFfaSmiogoLJBxMlzFjQCaeNHS.jqJHOOwSdIEWhZHMEyKvFTVGxxG(typeof(DirectInput)), out IntPtr nativePointer, null);
			base.rJpdIfuQpJvSJLHqINlUHEYSEZR = nativePointer;
		}

		public IList<czFObpqURrncEWqDzIWsPPUpfkHk> GetDevices()
		{
			return GetDevices(WKOweSgcNSewZcmyFUpOeIbMhGB.aOnoleFosIhfMuOEOAjlbpkvvFxw, mrmEhmaUgHogcYrnJwQcoZXgntuR.DjuxuNQFxlhwOqgnhcxmZDXXkdF);
		}

		public IList<czFObpqURrncEWqDzIWsPPUpfkHk> GetDevices(WKOweSgcNSewZcmyFUpOeIbMhGB deviceClass, mrmEhmaUgHogcYrnJwQcoZXgntuR deviceEnumFlags)
		{
			using (ObjectInstanceTracker.Wrapper<wSYQRryNzjkcabbhMESTWYixfTy> wrapper = new ObjectInstanceTracker.Wrapper<wSYQRryNzjkcabbhMESTWYixfTy>(new wSYQRryNzjkcabbhMESTWYixfTy()))
			{
				wSYQRryNzjkcabbhMESTWYixfTy instance = wrapper.instance;
				EnumDevices((int)deviceClass, instance.rJpdIfuQpJvSJLHqINlUHEYSEZR, new IntPtr((int)wrapper.instanceId), deviceEnumFlags);
				return instance.OcXsLnGWdFqeoBqDtABjSHtkfkL;
			}
		}

		public IList<czFObpqURrncEWqDzIWsPPUpfkHk> GetDevices(DeviceType deviceType, mrmEhmaUgHogcYrnJwQcoZXgntuR deviceEnumFlags)
		{
			using (ObjectInstanceTracker.Wrapper<wSYQRryNzjkcabbhMESTWYixfTy> wrapper = new ObjectInstanceTracker.Wrapper<wSYQRryNzjkcabbhMESTWYixfTy>(new wSYQRryNzjkcabbhMESTWYixfTy()))
			{
				wSYQRryNzjkcabbhMESTWYixfTy instance = wrapper.instance;
				EnumDevices((int)deviceType, instance.rJpdIfuQpJvSJLHqINlUHEYSEZR, new IntPtr((int)wrapper.instanceId), deviceEnumFlags);
				return instance.OcXsLnGWdFqeoBqDtABjSHtkfkL;
			}
		}

		public bool IsDeviceAttached(Guid deviceGuid)
		{
			return GetDeviceStatus(deviceGuid).bzLXHQCdSNdwRhxTmicbDlMkhGte == 0;
		}

		public void RunControlPanel()
		{
			RunControlPanel(IntPtr.Zero);
		}

		public void RunControlPanel(IntPtr handle)
		{
			RunControlPanel(handle, 0);
		}

		[DllImport("Rewired_DirectInput", CallingConvention = CallingConvention.StdCall, EntryPoint = "DirectInput8_Create")]
		[SuppressUnmanagedCodeSecurity]
		private unsafe static extern int Create_(void* hinst, int dwVersion, void* riidltf, void* ppvOut, void* punkOuter);

		[DllImport("Rewired_DirectInput", CallingConvention = CallingConvention.StdCall, EntryPoint = "DirectInput8_Release")]
		[SuppressUnmanagedCodeSecurity]
		private unsafe static extern void Release_(void* pIDirectInput8);

		[DllImport("Rewired_DirectInput", CallingConvention = CallingConvention.StdCall, EntryPoint = "DirectInput8_CreateDevice")]
		[SuppressUnmanagedCodeSecurity]
		private unsafe static extern int CreateDevice_(void* pIDirectInput8, void* rguid, void* lplpDirectInputDevice, void* pUnkOuter);

		[DllImport("Rewired_DirectInput", CallingConvention = CallingConvention.StdCall, EntryPoint = "DirectInput8_EnumDevices")]
		[SuppressUnmanagedCodeSecurity]
		private unsafe static extern int EnumDevices_(void* pIDirectInput8, int dwDevType, void* lpCallback, void* pvRef, int dwFlags);

		[DllImport("Rewired_DirectInput", CallingConvention = CallingConvention.StdCall, EntryPoint = "DirectInput8_GetDeviceStatus")]
		[SuppressUnmanagedCodeSecurity]
		private unsafe static extern int GetDeviceStatus_(void* pIDirectInput8, void* rguidInstance);

		[DllImport("Rewired_DirectInput", CallingConvention = CallingConvention.StdCall, EntryPoint = "DirectInput8_RunControlPanel")]
		[SuppressUnmanagedCodeSecurity]
		private unsafe static extern int RunControlPanel_(void* pIDirectInput8, void* hwndOwner, int dwFlags);

		[DllImport("Rewired_DirectInput", CallingConvention = CallingConvention.StdCall, EntryPoint = "DirectInput8_Initialize")]
		[SuppressUnmanagedCodeSecurity]
		private unsafe static extern int Initialize_(void* pIDirectInput8, void* hinst, int dwVersion);

		[DllImport("Rewired_DirectInput", CallingConvention = CallingConvention.StdCall, EntryPoint = "DirectInput8_FindDevice")]
		[SuppressUnmanagedCodeSecurity]
		private unsafe static extern int FindDevice_(void* pIDirectInput8, void* rguidClass, string ptszName, void* pguidInstance);

		[DllImport("Rewired_DirectInput", CallingConvention = CallingConvention.StdCall, EntryPoint = "DirectInput8_EnumDevicesBySemantics")]
		[SuppressUnmanagedCodeSecurity]
		private unsafe static extern int EnumDevicesBySemantics_(void* pIDirectInput8, string ptszUserName, void* lpdiActionFormat, void* lpCallback, void* pvRef, int dwFlags);

		[DllImport("Rewired_DirectInput", CallingConvention = CallingConvention.StdCall, EntryPoint = "DirectInput8_ConfigureDevices")]
		[SuppressUnmanagedCodeSecurity]
		private unsafe static extern int ConfigureDevices_(void* pIDirectInput8, void* lpdiCallback, void* lpdiCDParams, int dwFlags, void* pvRefData);

		public DirectInput(IntPtr nativePtr)
			: base(nativePtr)
		{
		}

		public static explicit operator DirectInput(IntPtr nativePointer)
		{
			if (!(nativePointer == IntPtr.Zero))
			{
				return new DirectInput(nativePointer);
			}
			return null;
		}

		internal unsafe void CreateDevice(Guid arg0, out IntPtr arg1, hconbvlfPZALCMpGAEbgHkhCpWy arg2)
		{
			mhgSPyfeNbIvKbKdVNRuVnsVsZVh mhgSPyfeNbIvKbKdVNRuVnsVsZVh;
			fixed (IntPtr* lplpDirectInputDevice = &arg1)
			{
				mhgSPyfeNbIvKbKdVNRuVnsVsZVh = CreateDevice_(jYeZItriPKHBdvvcvWzhkYXqEtD, &arg0, lplpDirectInputDevice, (void*)(arg2?.rJpdIfuQpJvSJLHqINlUHEYSEZR ?? IntPtr.Zero));
			}
			mhgSPyfeNbIvKbKdVNRuVnsVsZVh.dhZOKbdViZnTsVlcSDPlhmBYzQU();
		}

		internal unsafe void EnumDevices(int arg0, RoVQWfgmfkvoxyvLugEcXCvAWOD arg1, IntPtr arg2, mrmEhmaUgHogcYrnJwQcoZXgntuR arg3)
		{
			((mhgSPyfeNbIvKbKdVNRuVnsVsZVh)EnumDevices_(jYeZItriPKHBdvvcvWzhkYXqEtD, arg0, arg1, (void*)arg2, (int)arg3)).dhZOKbdViZnTsVlcSDPlhmBYzQU();
		}

		internal unsafe mhgSPyfeNbIvKbKdVNRuVnsVsZVh GetDeviceStatus(Guid arg0)
		{
			return GetDeviceStatus_(jYeZItriPKHBdvvcvWzhkYXqEtD, &arg0);
		}

		internal unsafe void RunControlPanel(IntPtr arg0, int arg1)
		{
			((mhgSPyfeNbIvKbKdVNRuVnsVsZVh)RunControlPanel_(jYeZItriPKHBdvvcvWzhkYXqEtD, (void*)arg0, arg1)).dhZOKbdViZnTsVlcSDPlhmBYzQU();
		}

		internal unsafe void Initialize(IntPtr arg0, int arg1)
		{
			((mhgSPyfeNbIvKbKdVNRuVnsVsZVh)Initialize_(jYeZItriPKHBdvvcvWzhkYXqEtD, (void*)arg0, arg1)).dhZOKbdViZnTsVlcSDPlhmBYzQU();
		}

		public unsafe Guid FindDevice(Guid arg0, string arg1)
		{
			Guid result = default(Guid);
			((mhgSPyfeNbIvKbKdVNRuVnsVsZVh)FindDevice_(jYeZItriPKHBdvvcvWzhkYXqEtD, &arg0, arg1, &result)).dhZOKbdViZnTsVlcSDPlhmBYzQU();
			return result;
		}

		internal unsafe void EnumDevicesBySemantics(string arg0, ref wNQqlbCFviVSIPTsmIERUTbQRUz arg1, RoVQWfgmfkvoxyvLugEcXCvAWOD arg2, IntPtr arg3, int arg4)
		{
			wNQqlbCFviVSIPTsmIERUTbQRUz.fdzPwVLPGWlsdbtKBgyqaCQRCWSq fdzPwVLPGWlsdbtKBgyqaCQRCWSq = default(wNQqlbCFviVSIPTsmIERUTbQRUz.fdzPwVLPGWlsdbtKBgyqaCQRCWSq);
			arg1.MwahgzaKctorAMfUYFecofeflNEA(ref fdzPwVLPGWlsdbtKBgyqaCQRCWSq);
			mhgSPyfeNbIvKbKdVNRuVnsVsZVh mhgSPyfeNbIvKbKdVNRuVnsVsZVh = EnumDevicesBySemantics_(jYeZItriPKHBdvvcvWzhkYXqEtD, arg0, &fdzPwVLPGWlsdbtKBgyqaCQRCWSq, arg2, (void*)arg3, arg4);
			arg1.tBWJUUuiDOkJVbhLpXGcUHcRQPh(ref fdzPwVLPGWlsdbtKBgyqaCQRCWSq);
			mhgSPyfeNbIvKbKdVNRuVnsVsZVh.dhZOKbdViZnTsVlcSDPlhmBYzQU();
		}
	}
}
