using System;

namespace Rewired.Libraries.SharpDX.DirectInput
{
	[AttributeUsage(AttributeTargets.Field)]
	internal sealed class DataObjectFormatAttribute : Attribute
	{
		public string Name;

		public string Guid;

		public int ArrayCount;

		public lFmLPhMubYDaTgfSmMPFXrtfAMkd TypeFlags;

		public RXmfwXtsaYZttOmEJOXZrDUhaHW Flags;

		public int InstanceNumber;

		public DataObjectFormatAttribute()
		{
			Flags = RXmfwXtsaYZttOmEJOXZrDUhaHW.GDuNaXRkbADhpQqBGJCIqRDgpls;
			InstanceNumber = 0;
			Guid = "";
			TypeFlags = lFmLPhMubYDaTgfSmMPFXrtfAMkd.aOnoleFosIhfMuOEOAjlbpkvvFxw;
		}

		public DataObjectFormatAttribute(string guid, lFmLPhMubYDaTgfSmMPFXrtfAMkd typeFlags)
		{
			Guid = guid;
			TypeFlags = typeFlags;
			Flags = RXmfwXtsaYZttOmEJOXZrDUhaHW.GDuNaXRkbADhpQqBGJCIqRDgpls;
			InstanceNumber = 0;
			ArrayCount = 0;
		}

		public DataObjectFormatAttribute(string guid, lFmLPhMubYDaTgfSmMPFXrtfAMkd typeFlags, RXmfwXtsaYZttOmEJOXZrDUhaHW flags)
		{
			Guid = guid;
			TypeFlags = typeFlags;
			Flags = flags;
		}

		public DataObjectFormatAttribute(string guid, lFmLPhMubYDaTgfSmMPFXrtfAMkd typeFlags, RXmfwXtsaYZttOmEJOXZrDUhaHW flags, int instanceNumber)
		{
			Guid = guid;
			TypeFlags = typeFlags;
			Flags = flags;
			InstanceNumber = instanceNumber;
		}

		public DataObjectFormatAttribute(string guid, int arrayCount, lFmLPhMubYDaTgfSmMPFXrtfAMkd typeFlags, RXmfwXtsaYZttOmEJOXZrDUhaHW flags)
		{
			Guid = guid;
			ArrayCount = arrayCount;
			TypeFlags = typeFlags;
			Flags = flags;
		}

		public DataObjectFormatAttribute(string guid, int arrayCount, lFmLPhMubYDaTgfSmMPFXrtfAMkd typeFlags)
		{
			Guid = guid;
			ArrayCount = arrayCount;
			TypeFlags = typeFlags;
			Flags = RXmfwXtsaYZttOmEJOXZrDUhaHW.GDuNaXRkbADhpQqBGJCIqRDgpls;
		}

		public DataObjectFormatAttribute(lFmLPhMubYDaTgfSmMPFXrtfAMkd typeFlags)
		{
			TypeFlags = typeFlags;
			Flags = RXmfwXtsaYZttOmEJOXZrDUhaHW.GDuNaXRkbADhpQqBGJCIqRDgpls;
			InstanceNumber = 0;
			ArrayCount = 0;
		}

		public DataObjectFormatAttribute(lFmLPhMubYDaTgfSmMPFXrtfAMkd typeFlags, RXmfwXtsaYZttOmEJOXZrDUhaHW flags)
		{
			TypeFlags = typeFlags;
			Flags = flags;
		}

		public DataObjectFormatAttribute(lFmLPhMubYDaTgfSmMPFXrtfAMkd typeFlags, RXmfwXtsaYZttOmEJOXZrDUhaHW flags, int instanceNumber)
		{
			TypeFlags = typeFlags;
			Flags = flags;
			InstanceNumber = instanceNumber;
		}

		public DataObjectFormatAttribute(int arrayCount, lFmLPhMubYDaTgfSmMPFXrtfAMkd typeFlags)
		{
			ArrayCount = arrayCount;
			TypeFlags = typeFlags;
			Flags = RXmfwXtsaYZttOmEJOXZrDUhaHW.GDuNaXRkbADhpQqBGJCIqRDgpls;
		}

		public DataObjectFormatAttribute(int arrayCount, lFmLPhMubYDaTgfSmMPFXrtfAMkd typeFlags, RXmfwXtsaYZttOmEJOXZrDUhaHW flags)
		{
			ArrayCount = arrayCount;
			TypeFlags = typeFlags;
			Flags = flags;
		}

		public DataObjectFormatAttribute(int arrayCount, lFmLPhMubYDaTgfSmMPFXrtfAMkd typeFlags, RXmfwXtsaYZttOmEJOXZrDUhaHW flags, int instanceNumber)
		{
			ArrayCount = arrayCount;
			TypeFlags = typeFlags;
			Flags = flags;
			InstanceNumber = instanceNumber;
		}

		public DataObjectFormatAttribute(int arrayCount, lFmLPhMubYDaTgfSmMPFXrtfAMkd typeFlags, int instanceNumber)
		{
			ArrayCount = arrayCount;
			TypeFlags = typeFlags;
			Flags = RXmfwXtsaYZttOmEJOXZrDUhaHW.GDuNaXRkbADhpQqBGJCIqRDgpls;
			InstanceNumber = instanceNumber;
		}
	}
}
