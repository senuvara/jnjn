using Rewired.Interfaces;
using Rewired.Internal;
using Rewired.Libraries.SharpDX.DirectInput;
using Rewired.Platforms;
using Rewired.Utils;
using System;
using System.Collections.Generic;
using UnityEngine;

namespace Rewired.Dev.Tools
{
	internal sealed class DirectInputJoystickElementIdentifier_Internal : IElementIdentifierTool
	{
		private Rewired.Internal.GUIText text;

		private string textBuffer;

		private int currentDeviceId;

		private DirectInput aszzzJquyNCCbyUPUGVBJDzzfNx;

		private kiFebrEkGWAPecDneEfAzvNywXtC MLyjJvikKxxRkkPvPmnDbcRPFGf;

		private Guid deviceInstanceGuid;

		private IList<czFObpqURrncEWqDzIWsPPUpfkHk> GJCOTkitSBwSQSgYXzwubjHbFQK;

		private IList<czFObpqURrncEWqDzIWsPPUpfkHk> SQDGZsEvdAfYpZJtshtBiKZXDlFG;

		private bool showAllDevices;

		private bool refreshNow;

		private bool ready;

		public void Initialize(Rewired.Internal.GUIText text)
		{
			this.text = text;
		}

		public void Start()
		{
			if (ReInput.isEditor && ReInput.editorPlatform != EditorPlatform.Windows)
			{
				Logger.LogError("Direct Input cannot be run on this platform. You must be running the editor in Windows.");
				return;
			}
			if (ReInput.currentPlatform != Platform.Windows)
			{
				Logger.LogError("Direct Input cannot be run on this build target. Be sure Unity's build target is set to Windows Standalone.");
				return;
			}
			InputSourceWrapper<DirectInput> inputSourceWrapper = ReInput.primaryInputManager.inputSource as InputSourceWrapper<DirectInput>;
			if (inputSourceWrapper == null || inputSourceWrapper.source == null)
			{
				Logger.LogError("Unable to initialize Direct Input! You must add a Rewired Input Manager to the scene and set the input mode to Direct Input.");
				return;
			}
			aszzzJquyNCCbyUPUGVBJDzzfNx = inputSourceWrapper.source;
			ReInput.primaryInputManager.SystemDeviceConnectedEvent += SystemDeviceConnected;
			ReInput.primaryInputManager.SystemDeviceDisconnectedEvent += SystemDeviceDisconnected;
			UpdateDeviceList();
			ready = true;
		}

		public void Update()
		{
			if (!ready)
			{
				return;
			}
			textBuffer = "Direct Input Joystick Element Identifier\n\n";
			this.text.text = textBuffer;
			if (Input.GetKeyDown(KeyCode.A))
			{
				showAllDevices = !showAllDevices;
			}
			if (showAllDevices)
			{
				this.text.text += "All Devices:\n";
				foreach (czFObpqURrncEWqDzIWsPPUpfkHk item in SQDGZsEvdAfYpZJtshtBiKZXDlFG)
				{
					Rewired.Internal.GUIText gUIText = this.text;
					object obj = gUIText.text;
					gUIText.text = string.Concat(obj, item.PnwQTFCVyKWHUmRZSOQjoUFwHko, ", ", item.ZBuiuKAHuPdGEUXoZsoWpiQJtfhI, ", ", new PidVid(item.aVjucvnlYPgvizksiDoKoUTIfZm), ", ", item.BLUqqYavEMinUeKTQSaPXFRnPRE, ", ", item.VPqWfcwrrOexAkDzCbpoQTgAhbf, ", ", item.RvoGQGXRLOxrYzVRSGIUaNBVYGV, "\n");
				}
				this.text.text += "\n";
			}
			int num = currentDeviceId;
			Guid a = deviceInstanceGuid;
			if (ReInput.controllers.Keyboard.GetKeyDown(KeyCode.Equals) || ReInput.controllers.Keyboard.GetKeyDown(KeyCode.Plus) || ReInput.controllers.Keyboard.GetKeyDown(KeyCode.KeypadPlus))
			{
				currentDeviceId++;
			}
			if (ReInput.controllers.Keyboard.GetKeyDown(KeyCode.KeypadMinus) || ReInput.controllers.Keyboard.GetKeyDown(KeyCode.Minus))
			{
				currentDeviceId--;
			}
			if (refreshNow)
			{
				UpdateDeviceList();
				refreshNow = false;
			}
			int num2 = (GJCOTkitSBwSQSgYXzwubjHbFQK != null) ? GJCOTkitSBwSQSgYXzwubjHbFQK.Count : 0;
			if (num2 == 0)
			{
				return;
			}
			if (currentDeviceId < 0)
			{
				currentDeviceId = num2 - 1;
			}
			else if (currentDeviceId >= num2)
			{
				currentDeviceId = 0;
			}
			deviceInstanceGuid = GJCOTkitSBwSQSgYXzwubjHbFQK[currentDeviceId].nTPJxRIRzxKPuVHBnnUcHBWHGgF;
			bool flag = false;
			if (num != currentDeviceId || a != deviceInstanceGuid)
			{
				flag = true;
			}
			if (MLyjJvikKxxRkkPvPmnDbcRPFGf == null || flag)
			{
				if (MLyjJvikKxxRkkPvPmnDbcRPFGf != null)
				{
					MLyjJvikKxxRkkPvPmnDbcRPFGf.OekEayRfvywEIMrOTCIQdZdTstM();
				}
				MLyjJvikKxxRkkPvPmnDbcRPFGf = new kiFebrEkGWAPecDneEfAzvNywXtC(aszzzJquyNCCbyUPUGVBJDzzfNx, GJCOTkitSBwSQSgYXzwubjHbFQK[currentDeviceId].nTPJxRIRzxKPuVHBnnUcHBWHGgF);
				if (MLyjJvikKxxRkkPvPmnDbcRPFGf == null)
				{
					return;
				}
				IList<DBXCCNGjuHjtEEJBZxamXRISDBJd> list = MLyjJvikKxxRkkPvPmnDbcRPFGf.CtbvjwUIiBXNTnfFDBttctsctfet();
				if (list != null)
				{
					for (int i = 0; i < list.Count; i++)
					{
						if ((list[i].YUFGtBAkmzXSdPcQXZxvokhgMJHz.Flags & lFmLPhMubYDaTgfSmMPFXrtfAMkd.WyCxovZIQqgotHvVjXKcGUSVOUeK) != 0)
						{
							MLyjJvikKxxRkkPvPmnDbcRPFGf.pLjdVikIbzLiWdcREgOpHXXpJWNF.brMhGcfBVNfSfvBmMnnNkbmygihC = new EVSubUOUSkmlvBizmIRfihcoNVl(-65535, 65535);
						}
					}
				}
				MLyjJvikKxxRkkPvPmnDbcRPFGf.FMYNWqqlcTikTRlqArAvCuBUsSj();
			}
			SugEhVIyZjFTJarngspayeWHzMN sugEhVIyZjFTJarngspayeWHzMN;
			try
			{
				sugEhVIyZjFTJarngspayeWHzMN = MLyjJvikKxxRkkPvPmnDbcRPFGf.yIJwkuJyxKpkuFoPYmCaIVyVLBu();
			}
			catch
			{
				sugEhVIyZjFTJarngspayeWHzMN = null;
			}
			if (sugEhVIyZjFTJarngspayeWHzMN == null)
			{
				return;
			}
			if (num2 > 0)
			{
				textBuffer = textBuffer + num2 + " connected devices:\n";
			}
			for (int j = 0; j < num2; j++)
			{
				textBuffer = textBuffer + GJCOTkitSBwSQSgYXzwubjHbFQK[j].PnwQTFCVyKWHUmRZSOQjoUFwHko + "\n";
			}
			textBuffer += "\n";
			object obj3 = textBuffer;
			textBuffer = string.Concat(obj3, "Current DI device ", currentDeviceId, ": ", GJCOTkitSBwSQSgYXzwubjHbFQK[currentDeviceId].PnwQTFCVyKWHUmRZSOQjoUFwHko, "\n");
			textBuffer += "(Press + or - to change monitored device id.)\n\n";
			Log("Identifier", new PidVid(MLyjJvikKxxRkkPvPmnDbcRPFGf.DDFWmShntINSspfDXaDeHBXBLOr.aVjucvnlYPgvizksiDoKoUTIfZm));
			Log("Instance GUID", MLyjJvikKxxRkkPvPmnDbcRPFGf.DDFWmShntINSspfDXaDeHBXBLOr.nTPJxRIRzxKPuVHBnnUcHBWHGgF);
			Log("Product Id", MLyjJvikKxxRkkPvPmnDbcRPFGf.pLjdVikIbzLiWdcREgOpHXXpJWNF.AitQvdXZqvInZAGwDQtsjMtHMRP);
			Log("Device Type", MLyjJvikKxxRkkPvPmnDbcRPFGf.ZJVPgiFYOmeBtrPXcttVqbEntkt.GqzEjvKUrDCbGGoauOFXgDVgtJKN.ToString());
			textBuffer += "\n";
			Log("Axis Count", MLyjJvikKxxRkkPvPmnDbcRPFGf.ZJVPgiFYOmeBtrPXcttVqbEntkt.kaCsXExwNpLbkEqviXpLzMTjVWG);
			Log("Button Count", MLyjJvikKxxRkkPvPmnDbcRPFGf.ZJVPgiFYOmeBtrPXcttVqbEntkt.efUsfIveOgIIjZdSrjVYmdoMUVa);
			Log("Hat Count", MLyjJvikKxxRkkPvPmnDbcRPFGf.ZJVPgiFYOmeBtrPXcttVqbEntkt.RbERijoaJdgCdITnpsuItiODMts);
			textBuffer += "\n";
			if (flag)
			{
				Logger.Log("Device Name: \"" + GJCOTkitSBwSQSgYXzwubjHbFQK[currentDeviceId].PnwQTFCVyKWHUmRZSOQjoUFwHko + "\"");
				Logger.Log("Identifier: " + new PidVid(MLyjJvikKxxRkkPvPmnDbcRPFGf.DDFWmShntINSspfDXaDeHBXBLOr.aVjucvnlYPgvizksiDoKoUTIfZm));
			}
			for (int k = 0; k < 32; k++)
			{
				int value = XftOQyGobFibLLSZfGINmQngqet((DirectInputAxis)k, sugEhVIyZjFTJarngspayeWHzMN);
				string key = ((DirectInputAxis)k).ToString();
				Log(key, value + " (" + NormalizeAxis(value) + ")");
			}
			int[] pointOfViewControllers = sugEhVIyZjFTJarngspayeWHzMN.pqLBZTGnlZVCzICADGVovVFibFUF;
			for (int l = 0; l < 4; l++)
			{
				int num3 = pointOfViewControllers[l];
				string key2 = "Hat " + l;
				Log(key2, num3);
			}
			bool[] buttons = sugEhVIyZjFTJarngspayeWHzMN.XBiIVmBJxdTOfxjronbxaixHEaMA;
			string text = "";
			for (int m = 0; m < 128; m++)
			{
				if (buttons[m])
				{
					if (text != "")
					{
						text += ", ";
					}
					text += m;
				}
			}
			Log("Buttons ", text);
			this.text.text = textBuffer;
		}

		private void UpdateDeviceList()
		{
			GJCOTkitSBwSQSgYXzwubjHbFQK = aszzzJquyNCCbyUPUGVBJDzzfNx.GetDevices(WKOweSgcNSewZcmyFUpOeIbMhGB.LBjlItjiTnIcPHMFONxmgXFKKmH, mrmEhmaUgHogcYrnJwQcoZXgntuR.jZCZSpSifBuNESGAiajskRIbNgg);
			SQDGZsEvdAfYpZJtshtBiKZXDlFG = aszzzJquyNCCbyUPUGVBJDzzfNx.GetDevices(WKOweSgcNSewZcmyFUpOeIbMhGB.aOnoleFosIhfMuOEOAjlbpkvvFxw, mrmEhmaUgHogcYrnJwQcoZXgntuR.jZCZSpSifBuNESGAiajskRIbNgg);
		}

		private void SystemDeviceConnected()
		{
			Refresh();
		}

		private void SystemDeviceDisconnected()
		{
			Refresh();
		}

		private void Refresh()
		{
			Clear();
			refreshNow = true;
		}

		private void Clear()
		{
			currentDeviceId = 0;
			MLyjJvikKxxRkkPvPmnDbcRPFGf = null;
			deviceInstanceGuid = Guid.Empty;
			GJCOTkitSBwSQSgYXzwubjHbFQK = null;
			SQDGZsEvdAfYpZJtshtBiKZXDlFG = null;
			showAllDevices = false;
			refreshNow = false;
		}

		private void Log(string key, object value)
		{
			string text = textBuffer;
			textBuffer = text + key + " = " + value.ToString() + "\n";
		}

		private int XftOQyGobFibLLSZfGINmQngqet(DirectInputAxis P_0, SugEhVIyZjFTJarngspayeWHzMN P_1)
		{
			switch (P_0)
			{
			case DirectInputAxis.X:
				return P_1.wFEOyFSIiBuQLToiSRegEhIXosq;
			case DirectInputAxis.Y:
				return P_1.QXZZDtjjuWmrzGLGiifvNjeAXfJ;
			case DirectInputAxis.Z:
				return P_1.SYJPRdnrNSmZJPovDzMDhiRVopj;
			case DirectInputAxis.RotationX:
				return P_1.FhUfsjArsyzJKOAwYaJmjeeKXcxE;
			case DirectInputAxis.RotationY:
				return P_1.MuNBpiAYExXOtVyOBuhuDpFnhJEE;
			case DirectInputAxis.RotationZ:
				return P_1.JjEBBxLNruFbVCFfpsmJDXibMUtz;
			case DirectInputAxis.Slider0:
				return P_1.zISxsxTMWxTGONdpqBjuYEvYDHw[0];
			case DirectInputAxis.Slider1:
				return P_1.zISxsxTMWxTGONdpqBjuYEvYDHw[1];
			case DirectInputAxis.VelocityX:
				return P_1.boJgmXxclRjhyJfqHeAWrPGSGcoG;
			case DirectInputAxis.VelocityY:
				return P_1.sfvRJrpgrKyTDaUMdEKKgfciqaA;
			case DirectInputAxis.VelocityZ:
				return P_1.RKgwRiwIrEcQwGwOiZDKLAMVuyhc;
			case DirectInputAxis.AngularVelocityX:
				return P_1.pAiFBHEugnNFXaAnRlJQsQYlMKK;
			case DirectInputAxis.AngularVelocityY:
				return P_1.ZEGaSjJqALetrqPkzqsMKYHoJetF;
			case DirectInputAxis.AngularVelocityZ:
				return P_1.gyNHgQVFSidnARGhJuWLXgInzyq;
			case DirectInputAxis.VelocitySlider0:
				return P_1.EKQDTDKepQJIFeIqOJIJncnGfTWi[0];
			case DirectInputAxis.VelocitySlider1:
				return P_1.EKQDTDKepQJIFeIqOJIJncnGfTWi[1];
			case DirectInputAxis.AccelerationX:
				return P_1.JrlqUNnalFIQwcjDBKSbcPyEgQpA;
			case DirectInputAxis.AccelerationY:
				return P_1.xDiReadrnSOtpXFZwarOYSbtcfOk;
			case DirectInputAxis.AccelerationZ:
				return P_1.WWmedVEcDFAriiMZlvquNTgTDpn;
			case DirectInputAxis.AngularAccelerationX:
				return P_1.LrioUkFTkuAoxuFEuVtwVzypEwV;
			case DirectInputAxis.AngularAccelerationY:
				return P_1.tkXOCniFAtnowBpdNCNhfnJEfNid;
			case DirectInputAxis.AngularAccelerationZ:
				return P_1.SSEbGCElLlJCHXuSEJRbNViOMEV;
			case DirectInputAxis.AccelerationSlider0:
				return P_1.YltNexaLnzCfUZbnbKyBRQiMmGL[0];
			case DirectInputAxis.AccelerationSlider1:
				return P_1.YltNexaLnzCfUZbnbKyBRQiMmGL[1];
			case DirectInputAxis.ForceX:
				return P_1.YfbMDzHiOghaUAKBhcSDGmBGBzKG;
			case DirectInputAxis.ForceY:
				return P_1.WBQNKzOnpnyELHgIxBPmfzKReFJ;
			case DirectInputAxis.ForceZ:
				return P_1.ZWpsZtfdXkmyHfHyJfsimNwJbxxH;
			case DirectInputAxis.TorqueX:
				return P_1.WOQkfTxbJWzjFXlJkpRkWCknjvt;
			case DirectInputAxis.TorqueY:
				return P_1.AqdTOtHXfLiEdUXhttFuTJNiHEzc;
			case DirectInputAxis.TorqueZ:
				return P_1.vjdbguhRqJCqYptbtxPrBDZhqWi;
			case DirectInputAxis.ForceSlider0:
				return P_1.IYyQmJejfxLBVHJanqUNVSVQrvx[0];
			case DirectInputAxis.ForceSlider1:
				return P_1.IYyQmJejfxLBVHJanqUNVSVQrvx[1];
			default:
				return 0;
			}
		}

		private float NormalizeAxis(int value)
		{
			if (value == 0)
			{
				return 0f;
			}
			return MathTools.Clamp((float)MathTools.Abs(value) / 65535f * MathTools.Sign(value), -1f, 1f);
		}

		public void OnDestroy()
		{
			if (MLyjJvikKxxRkkPvPmnDbcRPFGf != null)
			{
				MLyjJvikKxxRkkPvPmnDbcRPFGf.OekEayRfvywEIMrOTCIQdZdTstM();
			}
		}
	}
}
