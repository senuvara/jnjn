using Rewired.HID;
using Rewired.Interfaces;
using Rewired.Internal;
using Rewired.Platforms;
using Rewired.Utils;
using System;
using System.Collections.Generic;
using UnityEngine;

namespace Rewired.Dev.Tools
{
	internal sealed class RawInputJoystickElementIdentifier_Internal : IElementIdentifierTool
	{
		private Rewired.Internal.GUIText text;

		private string textBuffer;

		private int currentDeviceId;

		private SPyVLWspMgkASeMfZWEzzuTzpBB tuWIgoaOhlLwmOnBbWXDqDmIfOVF;

		private LTikZcEBnJkNVbPkvvCHqmyWcec MLyjJvikKxxRkkPvPmnDbcRPFGf;

		private Guid deviceInstanceGuid;

		private IList<LTikZcEBnJkNVbPkvvCHqmyWcec> GJCOTkitSBwSQSgYXzwubjHbFQK;

		private bool showAllDevices;

		private bool refreshNow;

		private bool ready;

		private string[] axisNames;

		private int[] axisValues;

		public void Initialize(Rewired.Internal.GUIText text)
		{
			this.text = text;
			axisNames = Enum.GetNames(typeof(RawInputAxis));
			axisValues = (int[])Enum.GetValues(typeof(RawInputAxis));
		}

		public void Start()
		{
			if (ReInput.isEditor && ReInput.editorPlatform != EditorPlatform.Windows)
			{
				Logger.LogError("Raw Input cannot be run on this platform. You must be running the editor in Windows.");
				return;
			}
			if (ReInput.currentPlatform != Platform.Windows)
			{
				Logger.LogError("Raw Input cannot be run on this build target. Be sure Unity's build target is set to Windows Standalone.");
				return;
			}
			tuWIgoaOhlLwmOnBbWXDqDmIfOVF = (ReInput.primaryInputManager.inputSource as SPyVLWspMgkASeMfZWEzzuTzpBB);
			if (tuWIgoaOhlLwmOnBbWXDqDmIfOVF == null)
			{
				Logger.LogError("Unable to initialize Raw Input! You must add a Rewired Input Manager to the scene and set the input mode to Raw Input.");
				return;
			}
			ReInput.primaryInputManager.SystemDeviceConnectedEvent += SystemDeviceConnected;
			ReInput.primaryInputManager.SystemDeviceDisconnectedEvent += SystemDeviceDisconnected;
			UpdateDeviceList();
			ready = true;
		}

		public void Update()
		{
			if (!ready)
			{
				return;
			}
			textBuffer = "Raw Input Joystick Element Identifier\n\n";
			this.text.text = textBuffer;
			int num = currentDeviceId;
			Guid a = deviceInstanceGuid;
			if (ReInput.controllers.Keyboard.GetKeyDown(KeyCode.Equals) || ReInput.controllers.Keyboard.GetKeyDown(KeyCode.Plus) || ReInput.controllers.Keyboard.GetKeyDown(KeyCode.KeypadPlus))
			{
				currentDeviceId++;
			}
			if (ReInput.controllers.Keyboard.GetKeyDown(KeyCode.KeypadMinus) || ReInput.controllers.Keyboard.GetKeyDown(KeyCode.Minus))
			{
				currentDeviceId--;
			}
			if (refreshNow)
			{
				UpdateDeviceList();
				refreshNow = false;
			}
			int num2 = (GJCOTkitSBwSQSgYXzwubjHbFQK != null) ? GJCOTkitSBwSQSgYXzwubjHbFQK.Count : 0;
			if (num2 == 0)
			{
				return;
			}
			if (currentDeviceId < 0)
			{
				currentDeviceId = num2 - 1;
			}
			else if (currentDeviceId >= num2)
			{
				currentDeviceId = 0;
			}
			deviceInstanceGuid = GJCOTkitSBwSQSgYXzwubjHbFQK[currentDeviceId].nTPJxRIRzxKPuVHBnnUcHBWHGgF;
			bool flag = false;
			if (num != currentDeviceId || a != deviceInstanceGuid)
			{
				flag = true;
			}
			if (MLyjJvikKxxRkkPvPmnDbcRPFGf == null || flag)
			{
				if (MLyjJvikKxxRkkPvPmnDbcRPFGf != null)
				{
					MLyjJvikKxxRkkPvPmnDbcRPFGf.Unacquire();
				}
				MLyjJvikKxxRkkPvPmnDbcRPFGf = GJCOTkitSBwSQSgYXzwubjHbFQK[currentDeviceId];
				if (MLyjJvikKxxRkkPvPmnDbcRPFGf == null)
				{
					return;
				}
				MLyjJvikKxxRkkPvPmnDbcRPFGf.Acquire();
			}
			bool flag2 = false;
			if (MLyjJvikKxxRkkPvPmnDbcRPFGf.dsagOBqkngzXzeCdGqMCuzhhIqS is hHAYCXZjUEbKAAVKdoCDPfRUfNj)
			{
				flag2 = true;
			}
			else if (!(MLyjJvikKxxRkkPvPmnDbcRPFGf.dsagOBqkngzXzeCdGqMCuzhhIqS is pLYAXgDfzJxqWwdxxBuSgYIKdid))
			{
				return;
			}
			if (num2 > 0)
			{
				textBuffer = textBuffer + num2 + " connected devices:\n";
			}
			for (int i = 0; i < num2; i++)
			{
				textBuffer = textBuffer + GJCOTkitSBwSQSgYXzwubjHbFQK[i].PnwQTFCVyKWHUmRZSOQjoUFwHko + "\n";
			}
			textBuffer += "\n";
			object obj = textBuffer;
			textBuffer = string.Concat(obj, "Current RI device ", currentDeviceId, ": \"", MLyjJvikKxxRkkPvPmnDbcRPFGf.PnwQTFCVyKWHUmRZSOQjoUFwHko, "\"\n");
			textBuffer += "(Press + or - to change monitored device id.)\n\n";
			Log("Product Name", "\"" + MLyjJvikKxxRkkPvPmnDbcRPFGf.PnwQTFCVyKWHUmRZSOQjoUFwHko + "\"");
			Log("Is Bluetooth Device", MLyjJvikKxxRkkPvPmnDbcRPFGf.fGtauWhshfjsFVVAPSlYirQFulJ);
			if (MLyjJvikKxxRkkPvPmnDbcRPFGf.fGtauWhshfjsFVVAPSlYirQFulJ)
			{
				Log("Bluetooth Device Name", "\"" + MLyjJvikKxxRkkPvPmnDbcRPFGf.RdenKmjCiVGwunGpIDghpjIZQDB + "\"");
			}
			if (flag2)
			{
				Log("Using Custom Driver", "TRUE");
			}
			Log("Device Type", MLyjJvikKxxRkkPvPmnDbcRPFGf.juAlLfDkvBGSAcnJqmCLTWbNFdPV.ToString());
			Log("Identifier", new PidVid(MLyjJvikKxxRkkPvPmnDbcRPFGf.aVjucvnlYPgvizksiDoKoUTIfZm));
			Log("Product Id", MLyjJvikKxxRkkPvPmnDbcRPFGf.AitQvdXZqvInZAGwDQtsjMtHMRP);
			Log("Vendor Id", MLyjJvikKxxRkkPvPmnDbcRPFGf.YtVjcTVgyYAWaYVAvdvfgYngvee);
			textBuffer += "\n";
			Log("Axis Count", MLyjJvikKxxRkkPvPmnDbcRPFGf.dYLbyHmmxqYCzhdNFyTqBtlxPsP);
			Log("Button Count", MLyjJvikKxxRkkPvPmnDbcRPFGf.efUsfIveOgIIjZdSrjVYmdoMUVa);
			Log("Hat Count", MLyjJvikKxxRkkPvPmnDbcRPFGf.IbIwtjAEJcAKmbOEjoXRYlLnGBtT);
			textBuffer += "\n";
			if (flag)
			{
				string str = "";
				str = str + "Device Name: \"" + GJCOTkitSBwSQSgYXzwubjHbFQK[currentDeviceId].PnwQTFCVyKWHUmRZSOQjoUFwHko + "\"\n";
				if (MLyjJvikKxxRkkPvPmnDbcRPFGf.fGtauWhshfjsFVVAPSlYirQFulJ)
				{
					str = str + "Bluetooth Device Name: \"" + MLyjJvikKxxRkkPvPmnDbcRPFGf.RdenKmjCiVGwunGpIDghpjIZQDB + "\"\n";
				}
				object obj2 = str;
				str = string.Concat(obj2, "Identifier: ", new PidVid(MLyjJvikKxxRkkPvPmnDbcRPFGf.aVjucvnlYPgvizksiDoKoUTIfZm), "\n");
				Logger.Log(str);
			}
			if (!flag2)
			{
				pLYAXgDfzJxqWwdxxBuSgYIKdid pLYAXgDfzJxqWwdxxBuSgYIKdid = MLyjJvikKxxRkkPvPmnDbcRPFGf.dsagOBqkngzXzeCdGqMCuzhhIqS as pLYAXgDfzJxqWwdxxBuSgYIKdid;
				for (int j = 1; j < axisNames.Length - 1; j++)
				{
					int num3 = XftOQyGobFibLLSZfGINmQngqet((RawInputAxis)axisValues[j], 0, pLYAXgDfzJxqWwdxxBuSgYIKdid);
					string key = axisNames[j];
					try
					{
						Log(key, num3 + " (" + NormalizeAxis(num3) + ")");
					}
					catch
					{
						Log(key, "FAILED! Axis value = " + num3);
					}
				}
				if (pLYAXgDfzJxqWwdxxBuSgYIKdid.qfvujcADOurRAOchesMnOliYkKB > 0)
				{
					for (int k = 0; k < pLYAXgDfzJxqWwdxxBuSgYIKdid.qfvujcADOurRAOchesMnOliYkKB; k++)
					{
						int num4 = XftOQyGobFibLLSZfGINmQngqet(RawInputAxis.Other, k, pLYAXgDfzJxqWwdxxBuSgYIKdid);
						string key2 = "Other Axis " + k;
						try
						{
							Log(key2, num4 + " (" + NormalizeAxis(num4) + ")");
						}
						catch
						{
							Log(key2, "FAILED! Axis value = " + num4);
						}
					}
				}
				int[] hatValues = MLyjJvikKxxRkkPvPmnDbcRPFGf.pljFgnSziqRBILFNfgQRFABznNjP;
				for (int l = 0; l < hatValues.Length; l++)
				{
					int num5 = hatValues[l];
					string key3 = "Hat " + l;
					Log(key3, num5);
				}
				bool[] buttons = MLyjJvikKxxRkkPvPmnDbcRPFGf.XBiIVmBJxdTOfxjronbxaixHEaMA;
				string text = "";
				for (int m = 0; m < buttons.Length; m++)
				{
					if (buttons[m])
					{
						if (text != "")
						{
							text += ", ";
						}
						text += m;
					}
				}
				Log("Buttons ", text);
			}
			else
			{
				hHAYCXZjUEbKAAVKdoCDPfRUfNj hHAYCXZjUEbKAAVKdoCDPfRUfNj = MLyjJvikKxxRkkPvPmnDbcRPFGf.dsagOBqkngzXzeCdGqMCuzhhIqS as hHAYCXZjUEbKAAVKdoCDPfRUfNj;
				for (int n = 0; n < MLyjJvikKxxRkkPvPmnDbcRPFGf.dYLbyHmmxqYCzhdNFyTqBtlxPsP; n++)
				{
					float num6 = hHAYCXZjUEbKAAVKdoCDPfRUfNj.XftOQyGobFibLLSZfGINmQngqet(n);
					string key4 = n.ToString();
					try
					{
						Log(key4, num6.ToString() + " (" + hHAYCXZjUEbKAAVKdoCDPfRUfNj.drOEsyBSYwkLFcZGXOJLHFLmCmr(n) + ")");
					}
					catch
					{
						Log(key4, "FAILED! Axis value = " + num6);
					}
				}
				int[] hatValues2 = MLyjJvikKxxRkkPvPmnDbcRPFGf.pljFgnSziqRBILFNfgQRFABznNjP;
				for (int num7 = 0; num7 < MLyjJvikKxxRkkPvPmnDbcRPFGf.IbIwtjAEJcAKmbOEjoXRYlLnGBtT; num7++)
				{
					int num8 = hatValues2[num7];
					string key5 = "Hat " + num7;
					Log(key5, num8);
				}
				for (int num9 = 0; num9 < MLyjJvikKxxRkkPvPmnDbcRPFGf.WSxWXbMBbqDqnHmOBhRgevyaWRDd.GyroscopeCount; num9++)
				{
					int valueLength = MLyjJvikKxxRkkPvPmnDbcRPFGf.WSxWXbMBbqDqnHmOBhRgevyaWRDd.gyroscopes[num9].valueLength;
					string text2 = "";
					for (int num10 = 0; num10 < valueLength; num10++)
					{
						float num11 = MLyjJvikKxxRkkPvPmnDbcRPFGf.WSxWXbMBbqDqnHmOBhRgevyaWRDd.gyroscopes[num9].rawValue[num10];
						object obj6 = text2;
						text2 = string.Concat(obj6, "[", num10, "]: ", num11.ToString("f3"));
						if (num10 < valueLength - 1)
						{
							text2 += " ";
						}
					}
					Log("Gyro " + num9, text2);
				}
				for (int num12 = 0; num12 < MLyjJvikKxxRkkPvPmnDbcRPFGf.WSxWXbMBbqDqnHmOBhRgevyaWRDd.AccelerometerCount; num12++)
				{
					int valueLength2 = MLyjJvikKxxRkkPvPmnDbcRPFGf.WSxWXbMBbqDqnHmOBhRgevyaWRDd.accelerometers[num12].valueLength;
					string text3 = "";
					for (int num13 = 0; num13 < valueLength2; num13++)
					{
						float num14 = MLyjJvikKxxRkkPvPmnDbcRPFGf.WSxWXbMBbqDqnHmOBhRgevyaWRDd.accelerometers[num12].rawValue[num13];
						object obj7 = text3;
						text3 = string.Concat(obj7, "[", num13, "]: ", num14.ToString("f3"));
						if (num13 < valueLength2 - 1)
						{
							text3 += " ";
						}
					}
					Log("Accelerometer " + num12, text3);
				}
				for (int num15 = 0; num15 < MLyjJvikKxxRkkPvPmnDbcRPFGf.WSxWXbMBbqDqnHmOBhRgevyaWRDd.TouchpadCount; num15++)
				{
					HIDTouchpad hIDTouchpad = MLyjJvikKxxRkkPvPmnDbcRPFGf.WSxWXbMBbqDqnHmOBhRgevyaWRDd.touchpads[num15];
					int num16 = hIDTouchpad.values.Length;
					string text4 = "";
					for (int num17 = 0; num17 < num16; num17++)
					{
						HIDTouchpad.TouchData touchData = hIDTouchpad.values[num17];
						obj = text4;
						text4 = string.Concat(obj, "Touch ", num17, ": Is Touching = ", touchData.isTouching, "\n");
						obj = text4;
						text4 = string.Concat(obj, "Touch ", num17, ": Touch Id = ", touchData.touchId, "\n");
						obj = text4;
						text4 = string.Concat(obj, "Touch ", num17, ": Position = ", touchData.positionX, ", ", touchData.positionY, "\n");
						obj = text4;
						text4 = string.Concat(obj, "Touch ", num17, ": Abs Position = ", touchData.positionAbsX, ", ", touchData.positionAbsY, " (", touchData.positionRawX, ", ", touchData.positionRawY, ")\n");
					}
					LogSet("Touchpad " + num15, text4);
				}
				bool[] buttons2 = MLyjJvikKxxRkkPvPmnDbcRPFGf.XBiIVmBJxdTOfxjronbxaixHEaMA;
				string text5 = "";
				for (int num18 = 0; num18 < buttons2.Length; num18++)
				{
					if (buttons2[num18])
					{
						if (text5 != "")
						{
							text5 += ", ";
						}
						text5 += num18;
					}
				}
				Log("Buttons ", text5);
			}
			this.text.text = textBuffer;
		}

		public void OnDestroy()
		{
			if (MLyjJvikKxxRkkPvPmnDbcRPFGf != null)
			{
				MLyjJvikKxxRkkPvPmnDbcRPFGf.Unacquire();
			}
		}

		private void UpdateDeviceList()
		{
			GJCOTkitSBwSQSgYXzwubjHbFQK = tuWIgoaOhlLwmOnBbWXDqDmIfOVF.GetJoysticks<LTikZcEBnJkNVbPkvvCHqmyWcec>();
		}

		private void SystemDeviceConnected()
		{
			Refresh();
		}

		private void SystemDeviceDisconnected()
		{
			Refresh();
		}

		private void Refresh()
		{
			Clear();
			refreshNow = true;
		}

		private void Clear()
		{
			currentDeviceId = 0;
			MLyjJvikKxxRkkPvPmnDbcRPFGf = null;
			deviceInstanceGuid = Guid.Empty;
			GJCOTkitSBwSQSgYXzwubjHbFQK = null;
			showAllDevices = false;
			refreshNow = false;
		}

		private void Log(string key, object value)
		{
			string text = textBuffer;
			textBuffer = text + key + " = " + value.ToString() + "\n";
		}

		private void LogSet(string label, object value)
		{
			string text = textBuffer;
			textBuffer = text + label + ":\n" + value.ToString() + "\n";
		}

		private int XftOQyGobFibLLSZfGINmQngqet(RawInputAxis P_0, int P_1, pLYAXgDfzJxqWwdxxBuSgYIKdid P_2)
		{
			return P_2.XftOQyGobFibLLSZfGINmQngqet(P_0, P_1);
		}

		private float NormalizeAxis(int value)
		{
			if (value == 0)
			{
				return 0f;
			}
			return MathTools.Clamp((float)MathTools.Abs(value) / 65535f * MathTools.Sign(value), -1f, 1f);
		}
	}
}
