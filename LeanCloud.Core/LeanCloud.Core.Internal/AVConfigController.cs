using LeanCloud.Storage.Internal;
using System;
using System.Collections.Generic;
using System.Net;
using System.Threading;
using System.Threading.Tasks;

namespace LeanCloud.Core.Internal
{
	internal class AVConfigController : IAVConfigController
	{
		private readonly IAVCommandRunner commandRunner;

		public IAVCommandRunner CommandRunner
		{
			get;
			internal set;
		}

		public IAVCurrentConfigController CurrentConfigController
		{
			get;
			internal set;
		}

		public AVConfigController(IAVCommandRunner commandRunner, IStorageController storageController)
		{
			this.commandRunner = commandRunner;
			CurrentConfigController = new AVCurrentConfigController(storageController);
		}

		public Task<AVConfig> FetchConfigAsync(string sessionToken, CancellationToken cancellationToken)
		{
			AVCommand command = new AVCommand("config", "GET", sessionToken, (IList<KeyValuePair<string, string>>)null, (IDictionary<string, object>)null);
			return commandRunner.RunCommandAsync(command, null, null, cancellationToken).OnSuccess(delegate(Task<Tuple<HttpStatusCode, IDictionary<string, object>>> task)
			{
				cancellationToken.ThrowIfCancellationRequested();
				return new AVConfig(task.Result.Item2);
			}).OnSuccess(delegate(Task<AVConfig> task)
			{
				cancellationToken.ThrowIfCancellationRequested();
				CurrentConfigController.SetCurrentConfigAsync(task.Result);
				return task;
			})
				.Unwrap();
		}
	}
}
