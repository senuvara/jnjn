using System;

namespace LeanCloud.Core.Internal
{
	[AttributeUsage(AttributeTargets.All)]
	internal class PreserveAttribute : Attribute
	{
		public bool AllMembers;

		public bool Conditional;
	}
}
