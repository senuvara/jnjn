using System.Collections.Generic;

namespace LeanCloud.Core.Internal
{
	public class AVObjectIdComparer : IEqualityComparer<object>
	{
		bool IEqualityComparer<object>.Equals(object p1, object p2)
		{
			AVObject aVObject = p1 as AVObject;
			AVObject aVObject2 = p2 as AVObject;
			if (aVObject != null && aVObject2 != null)
			{
				return object.Equals(aVObject.ObjectId, aVObject2.ObjectId);
			}
			return object.Equals(p1, p2);
		}

		public int GetHashCode(object p)
		{
			return (p as AVObject)?.ObjectId.GetHashCode() ?? p.GetHashCode();
		}
	}
}
