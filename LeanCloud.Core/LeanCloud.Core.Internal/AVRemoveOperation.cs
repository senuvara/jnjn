using LeanCloud.Utilities;
using System;
using System.Collections.Generic;
using System.Collections.ObjectModel;
using System.Linq;

namespace LeanCloud.Core.Internal
{
	public class AVRemoveOperation : IAVFieldOperation
	{
		private ReadOnlyCollection<object> objects;

		public IEnumerable<object> Objects => objects;

		public AVRemoveOperation(IEnumerable<object> objects)
		{
			this.objects = new ReadOnlyCollection<object>(objects.Distinct().ToList());
		}

		public object Encode()
		{
			return new Dictionary<string, object>
			{
				{
					"__op",
					"Remove"
				},
				{
					"objects",
					PointerOrLocalIdEncoder.Instance.Encode(objects)
				}
			};
		}

		public IAVFieldOperation MergeWithPrevious(IAVFieldOperation previous)
		{
			if (previous == null)
			{
				return this;
			}
			if (previous is AVDeleteOperation)
			{
				return previous;
			}
			if (previous is AVSetOperation)
			{
				IList<object> oldValue = Conversion.As<IList<object>>(((AVSetOperation)previous).Value);
				return new AVSetOperation(Apply(oldValue, null));
			}
			if (previous is AVRemoveOperation)
			{
				return new AVRemoveOperation(((AVRemoveOperation)previous).Objects.Concat(objects));
			}
			throw new InvalidOperationException("Operation is invalid after previous operation.");
		}

		public object Apply(object oldValue, string key)
		{
			if (oldValue == null)
			{
				return new List<object>();
			}
			return Conversion.As<IList<object>>(oldValue).Except(objects, ParseFieldOperations.ParseObjectComparer).ToList();
		}
	}
}
