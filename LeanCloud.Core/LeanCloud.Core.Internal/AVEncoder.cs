using LeanCloud.Storage.Internal;
using LeanCloud.Utilities;
using System;
using System.Collections.Generic;
using System.Globalization;
using System.Linq;

namespace LeanCloud.Core.Internal
{
	public abstract class AVEncoder
	{
		private static readonly bool isCompiledByIL2CPP = AppDomain.CurrentDomain.FriendlyName.Equals("IL2CPP Root Domain");

		public static bool IsValidType(object value)
		{
			if (value != null && !ReflectionHelpers.IsPrimitive(value.GetType()) && !(value is string) && !(value is AVObject) && !(value is AVACL) && !(value is AVFile) && !(value is AVGeoPoint) && !(value is AVRelationBase) && !(value is DateTime) && !(value is byte[]) && Conversion.As<IDictionary<string, object>>(value) == null)
			{
				return Conversion.As<IList<object>>(value) != null;
			}
			return true;
		}

		public object Encode(object value)
		{
			if (value is DateTime)
			{
				return new Dictionary<string, object>
				{
					{
						"iso",
						((DateTime)value).ToUniversalTime().ToString(AVClient.DateFormatStrings.First(), CultureInfo.InvariantCulture)
					},
					{
						"__type",
						"Date"
					}
				};
			}
			if (value is AVFile)
			{
				AVFile aVFile = value as AVFile;
				return new Dictionary<string, object>
				{
					{
						"__type",
						"Pointer"
					},
					{
						"className",
						"_File"
					},
					{
						"objectId",
						aVFile.ObjectId
					}
				};
			}
			byte[] array = value as byte[];
			if (array != null)
			{
				return new Dictionary<string, object>
				{
					{
						"__type",
						"Bytes"
					},
					{
						"base64",
						Convert.ToBase64String(array)
					}
				};
			}
			AVObject aVObject = value as AVObject;
			if (aVObject != null)
			{
				return EncodeParseObject(aVObject);
			}
			IJsonConvertible jsonConvertible = value as IJsonConvertible;
			if (jsonConvertible != null)
			{
				return jsonConvertible.ToJSON();
			}
			IDictionary<string, object> dictionary = Conversion.As<IDictionary<string, object>>(value);
			if (dictionary != null)
			{
				Dictionary<string, object> dictionary2 = new Dictionary<string, object>();
				{
					foreach (KeyValuePair<string, object> item in dictionary)
					{
						dictionary2[item.Key] = Encode(item.Value);
					}
					return dictionary2;
				}
			}
			IList<object> list = Conversion.As<IList<object>>(value);
			if (list != null)
			{
				return EncodeList(list);
			}
			IAVFieldOperation iAVFieldOperation = value as IAVFieldOperation;
			if (iAVFieldOperation != null)
			{
				return iAVFieldOperation.Encode();
			}
			return value;
		}

		protected abstract IDictionary<string, object> EncodeParseObject(AVObject value);

		private object EncodeList(IList<object> list)
		{
			List<object> list2 = new List<object>();
			if (isCompiledByIL2CPP && list.GetType().IsArray)
			{
				list = new List<object>(list);
			}
			foreach (object item in list)
			{
				if (!IsValidType(item))
				{
					throw new ArgumentException("Invalid type for value in an array");
				}
				list2.Add(Encode(item));
			}
			return list2;
		}
	}
}
