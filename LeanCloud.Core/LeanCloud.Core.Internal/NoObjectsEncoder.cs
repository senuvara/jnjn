using System;
using System.Collections.Generic;

namespace LeanCloud.Core.Internal
{
	public class NoObjectsEncoder : AVEncoder
	{
		private static readonly NoObjectsEncoder instance = new NoObjectsEncoder();

		public static NoObjectsEncoder Instance => instance;

		protected override IDictionary<string, object> EncodeParseObject(AVObject value)
		{
			throw new ArgumentException("AVObjects not allowed here.");
		}
	}
}
