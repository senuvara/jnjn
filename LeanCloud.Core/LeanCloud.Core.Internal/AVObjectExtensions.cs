using System.Collections.Generic;

namespace LeanCloud.Core.Internal
{
	public static class AVObjectExtensions
	{
		public static T FromState<T>(IObjectState state, string defaultClassName) where T : AVObject
		{
			return AVObject.FromState<T>(state, defaultClassName);
		}

		public static IObjectState GetState(this AVObject obj)
		{
			return obj.State;
		}

		public static void HandleFetchResult(this AVObject obj, IObjectState serverState)
		{
			obj.HandleFetchResult(serverState);
		}

		public static IDictionary<string, IAVFieldOperation> GetCurrentOperations(this AVObject obj)
		{
			return obj.CurrentOperations;
		}

		public static IEnumerable<object> DeepTraversal(object root, bool traverseParseObjects = false, bool yieldRoot = false)
		{
			return AVObject.DeepTraversal(root, traverseParseObjects, yieldRoot);
		}

		public static void SetIfDifferent<T>(this AVObject obj, string key, T value)
		{
			obj.SetIfDifferent(key, value);
		}

		public static IDictionary<string, object> ServerDataToJSONObjectForSerialization(this AVObject obj)
		{
			return obj.ServerDataToJSONObjectForSerialization();
		}

		public static void Set(this AVObject obj, string key, object value)
		{
			obj.Set(key, value);
		}
	}
}
