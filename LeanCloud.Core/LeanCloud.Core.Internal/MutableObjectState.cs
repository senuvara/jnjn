using System;
using System.Collections;
using System.Collections.Generic;
using System.Linq;

namespace LeanCloud.Core.Internal
{
	public class MutableObjectState : IObjectState, IEnumerable<KeyValuePair<string, object>>, IEnumerable
	{
		private IDictionary<string, object> serverData = new Dictionary<string, object>();

		public bool IsNew
		{
			get;
			set;
		}

		public string ClassName
		{
			get;
			set;
		}

		public string ObjectId
		{
			get;
			set;
		}

		public DateTime? UpdatedAt
		{
			get;
			set;
		}

		public DateTime? CreatedAt
		{
			get;
			set;
		}

		public IDictionary<string, object> ServerData
		{
			get
			{
				return serverData;
			}
			set
			{
				serverData = value;
			}
		}

		public object this[string key] => ServerData[key];

		public bool ContainsKey(string key)
		{
			return ServerData.ContainsKey(key);
		}

		public void Apply(IDictionary<string, IAVFieldOperation> operationSet)
		{
			foreach (KeyValuePair<string, IAVFieldOperation> item in operationSet)
			{
				ServerData.TryGetValue(item.Key, out object value);
				object obj = item.Value.Apply(value, item.Key);
				if (obj != AVDeleteOperation.DeleteToken)
				{
					ServerData[item.Key] = obj;
				}
				else
				{
					ServerData.Remove(item.Key);
				}
			}
		}

		public void Apply(IObjectState other)
		{
			IsNew = other.IsNew;
			if (other.ObjectId != null)
			{
				ObjectId = other.ObjectId;
			}
			if (other.UpdatedAt.HasValue)
			{
				UpdatedAt = other.UpdatedAt;
			}
			if (other.CreatedAt.HasValue)
			{
				CreatedAt = other.CreatedAt;
			}
			foreach (KeyValuePair<string, object> item in other)
			{
				ServerData[item.Key] = item.Value;
			}
		}

		public IObjectState MutatedClone(Action<MutableObjectState> func)
		{
			MutableObjectState mutableObjectState = MutableClone();
			func(mutableObjectState);
			return mutableObjectState;
		}

		protected virtual MutableObjectState MutableClone()
		{
			return new MutableObjectState
			{
				IsNew = IsNew,
				ClassName = ClassName,
				ObjectId = ObjectId,
				CreatedAt = CreatedAt,
				UpdatedAt = UpdatedAt,
				ServerData = this.ToDictionary((KeyValuePair<string, object> t) => t.Key, (KeyValuePair<string, object> t) => t.Value)
			};
		}

		IEnumerator<KeyValuePair<string, object>> IEnumerable<KeyValuePair<string, object>>.GetEnumerator()
		{
			return ServerData.GetEnumerator();
		}

		IEnumerator IEnumerable.GetEnumerator()
		{
			return ((IEnumerable<KeyValuePair<string, object>>)this).GetEnumerator();
		}
	}
}
