using LeanCloud.Storage.Internal;
using System;
using System.Collections.Generic;
using System.IO;
using System.Net;
using System.Text;
using System.Threading;
using System.Threading.Tasks;

namespace LeanCloud.Core.Internal
{
	internal class QCloudCosFileController : AVFileController
	{
		private object mutex = new object();

		private FileState fileState;

		private Stream data;

		private string bucket;

		private string token;

		private string uploadUrl;

		private bool done;

		private long sliceSize = 524288L;

		public QCloudCosFileController(IAVCommandRunner commandRunner)
			: base(commandRunner)
		{
		}

		public new Task<FileState> SaveAsync(FileState state, Stream dataStream, string sessionToken, IProgress<AVUploadProgressEventArgs> progress, CancellationToken cancellationToken)
		{
			if (state.Url != null)
			{
				return Task.FromResult(state);
			}
			fileState = state;
			data = dataStream;
			return AVFileController.GetFileToken(fileState, cancellationToken).OnSuccess(delegate(Task<Tuple<HttpStatusCode, IDictionary<string, object>>> _)
			{
				IDictionary<string, object> item = _.Result.Item2;
				uploadUrl = item["upload_url"].ToString();
				token = item["token"].ToString();
				fileState.ObjectId = item["objectId"].ToString();
				bucket = item["bucket"].ToString();
				return FileSlice(cancellationToken).OnSuccess(delegate(Task<Tuple<HttpStatusCode, IDictionary<string, object>>> t)
				{
					if (done)
					{
						return Task.FromResult(state);
					}
					IDictionary<string, object> dictionary = t.Result.Item2["data"] as IDictionary<string, object>;
					if (dictionary.ContainsKey("access_url"))
					{
						return Task.FromResult(state);
					}
					string sessionId = dictionary["session"].ToString();
					long offset = long.Parse(dictionary["offset"].ToString());
					return UploadSlice(sessionId, offset, dataStream, progress, cancellationToken);
				}).Unwrap();
			}).Unwrap();
		}

		private Task<FileState> UploadSlice(string sessionId, long offset, Stream dataStream, IProgress<AVUploadProgressEventArgs> progress, CancellationToken cancellationToken)
		{
			long dataLength = dataStream.Length;
			if (progress != null)
			{
				lock (mutex)
				{
					progress.Report(new AVUploadProgressEventArgs
					{
						Progress = AVFileController.CalcProgress(offset, dataLength)
					});
				}
			}
			if (offset == dataLength)
			{
				return Task.FromResult(fileState);
			}
			byte[] sliceFile = GetNextBinary(offset, dataStream);
			return ExcuteUpload(sessionId, offset, sliceFile, cancellationToken).OnSuccess(delegate(Task<Tuple<HttpStatusCode, IDictionary<string, object>>> _)
			{
				offset += sliceFile.Length;
				if (offset == dataLength)
				{
					done = true;
					return Task.FromResult(fileState);
				}
				string sessionId2 = (_.Result.Item2["data"] as IDictionary<string, object>)["session"].ToString();
				return UploadSlice(sessionId2, offset, dataStream, progress, cancellationToken);
			}).Unwrap();
		}

		private Task<Tuple<HttpStatusCode, IDictionary<string, object>>> ExcuteUpload(string sessionId, long offset, byte[] sliceFile, CancellationToken cancellationToken)
		{
			Dictionary<string, object> dictionary = new Dictionary<string, object>();
			dictionary.Add("op", "upload_slice");
			dictionary.Add("session", sessionId);
			dictionary.Add("offset", offset.ToString());
			return PostToQCloud(dictionary, sliceFile, cancellationToken);
		}

		private Task<Tuple<HttpStatusCode, IDictionary<string, object>>> FileSlice(CancellationToken cancellationToken)
		{
			SHA1CryptoServiceProvider sHA1CryptoServiceProvider = new SHA1CryptoServiceProvider();
			Dictionary<string, object> dictionary = new Dictionary<string, object>();
			if (data.Length <= 524288)
			{
				dictionary.Add("op", "upload");
				dictionary.Add("sha", HexStringFromBytes(sHA1CryptoServiceProvider.ComputeHash(data)));
				byte[] nextBinary = GetNextBinary(0L, data);
				return PostToQCloud(dictionary, nextBinary, cancellationToken).OnSuccess(delegate(Task<Tuple<HttpStatusCode, IDictionary<string, object>>> _)
				{
					if (_.Result.Item1 == HttpStatusCode.OK)
					{
						done = true;
					}
					return _.Result;
				});
			}
			dictionary.Add("op", "upload_slice");
			dictionary.Add("filesize", data.Length);
			dictionary.Add("sha", HexStringFromBytes(sHA1CryptoServiceProvider.ComputeHash(data)));
			dictionary.Add("slice_size", 524288L);
			return PostToQCloud(dictionary, null, cancellationToken);
		}

		public static string HexStringFromBytes(byte[] bytes)
		{
			StringBuilder stringBuilder = new StringBuilder();
			foreach (byte b in bytes)
			{
				string value = b.ToString("x2");
				stringBuilder.Append(value);
			}
			return stringBuilder.ToString();
		}

		public static string SHA1HashStringForUTF8String(string s)
		{
			byte[] bytes = Encoding.UTF8.GetBytes(s);
			return HexStringFromBytes(new SHA1CryptoServiceProvider().ComputeHash(bytes));
		}

		private Task<Tuple<HttpStatusCode, IDictionary<string, object>>> PostToQCloud(Dictionary<string, object> body, byte[] sliceFile, CancellationToken cancellationToken)
		{
			IList<KeyValuePair<string, string>> list = new List<KeyValuePair<string, string>>();
			list.Add(new KeyValuePair<string, string>("Authorization", token));
			string contentType;
			long contentLength;
			Stream stream = HttpUploadFile(sliceFile, fileState.CloudName, out contentType, out contentLength, body);
			list.Add(new KeyValuePair<string, string>("Content-Type", contentType));
			return AVClient.RequestAsync(new Uri(uploadUrl), "POST", list, stream, null, cancellationToken).OnSuccess((Task<Tuple<HttpStatusCode, string>> _) => AVClient.ReponseResolve(_.Result, CancellationToken.None));
		}

		public static Stream HttpUploadFile(byte[] file, string fileName, out string contentType, out long contentLength, IDictionary<string, object> nvc)
		{
			string str = "---------------------------" + DateTime.Now.Ticks.ToString("x");
			byte[] array = StringToAscii("\r\n--" + str + "\r\n");
			contentType = "multipart/form-data; boundary=" + str;
			MemoryStream memoryStream = new MemoryStream();
			string format = "Content-Disposition: form-data; name=\"{0}\"\r\n\r\n{1}";
			foreach (string key in nvc.Keys)
			{
				memoryStream.Write(array, 0, array.Length);
				string s = string.Format(format, key, nvc[key]);
				byte[] bytes = Encoding.UTF8.GetBytes(s);
				memoryStream.Write(bytes, 0, bytes.Length);
			}
			memoryStream.Write(array, 0, array.Length);
			if (file != null)
			{
				string s2 = string.Format("Content-Disposition: form-data; name=\"{0}\"; filename=\"{1}\"\r\nContent-Type: {2}\r\n\r\n", "fileContent", fileName, "application/octet-stream");
				byte[] bytes2 = Encoding.UTF8.GetBytes(s2);
				memoryStream.Write(bytes2, 0, bytes2.Length);
				memoryStream.Write(file, 0, file.Length);
			}
			byte[] array2 = StringToAscii("\r\n--" + str + "--\r\n");
			memoryStream.Write(array2, 0, array2.Length);
			contentLength = memoryStream.Length;
			memoryStream.Position = 0L;
			byte[] array3 = new byte[memoryStream.Length];
			memoryStream.Read(array3, 0, array3.Length);
			return new MemoryStream(array3);
		}

		public static byte[] StringToAscii(string s)
		{
			byte[] array = new byte[s.Length];
			for (int i = 0; i < s.Length; i++)
			{
				char c = s[i];
				if (c <= '\u007f')
				{
					array[i] = (byte)c;
				}
				else
				{
					array[i] = 63;
				}
			}
			return array;
		}

		private byte[] GetNextBinary(long completed, Stream dataStream)
		{
			if (completed + sliceSize > dataStream.Length)
			{
				sliceSize = dataStream.Length - completed;
			}
			byte[] array = new byte[sliceSize];
			dataStream.Seek(completed, SeekOrigin.Begin);
			dataStream.Read(array, 0, (int)sliceSize);
			return array;
		}
	}
}
