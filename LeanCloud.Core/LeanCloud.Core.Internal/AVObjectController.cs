using LeanCloud.Storage.Internal;
using LeanCloud.Utilities;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Net;
using System.Threading;
using System.Threading.Tasks;

namespace LeanCloud.Core.Internal
{
	public class AVObjectController : IAVObjectController
	{
		private readonly IAVCommandRunner commandRunner;

		private const int MaximumBatchSize = 50;

		public AVObjectController(IAVCommandRunner commandRunner)
		{
			this.commandRunner = commandRunner;
		}

		public Task<IObjectState> FetchAsync(IObjectState state, string sessionToken, CancellationToken cancellationToken)
		{
			AVCommand command = new AVCommand($"classes/{Uri.EscapeDataString(state.ClassName)}/{Uri.EscapeDataString(state.ObjectId)}", "GET", sessionToken, (IList<KeyValuePair<string, string>>)null, (IDictionary<string, object>)null);
			return commandRunner.RunCommandAsync(command, null, null, cancellationToken).OnSuccess((Task<Tuple<HttpStatusCode, IDictionary<string, object>>> t) => AVObjectCoder.Instance.Decode(t.Result.Item2, AVDecoder.Instance));
		}

		public Task<IObjectState> FetchAsync(IObjectState state, IDictionary<string, object> queryString, string sessionToken, CancellationToken cancellationToken)
		{
			AVCommand command = new AVCommand($"classes/{Uri.EscapeDataString(state.ClassName)}/{Uri.EscapeDataString(state.ObjectId)}?{AVClient.BuildQueryString(queryString)}", "GET", sessionToken, (IList<KeyValuePair<string, string>>)null, (IDictionary<string, object>)null);
			return commandRunner.RunCommandAsync(command, null, null, cancellationToken).OnSuccess((Task<Tuple<HttpStatusCode, IDictionary<string, object>>> t) => AVObjectCoder.Instance.Decode(t.Result.Item2, AVDecoder.Instance));
		}

		public Task<IObjectState> SaveAsync(IObjectState state, IDictionary<string, IAVFieldOperation> operations, string sessionToken, CancellationToken cancellationToken)
		{
			IDictionary<string, object> data = AVObject.ToJSONObjectForSaving(operations);
			AVCommand command = new AVCommand((state.ObjectId == null) ? $"classes/{Uri.EscapeDataString(state.ClassName)}" : $"classes/{Uri.EscapeDataString(state.ClassName)}/{state.ObjectId}", (state.ObjectId == null) ? "POST" : "PUT", sessionToken, null, data);
			return commandRunner.RunCommandAsync(command, null, null, cancellationToken).OnSuccess((Task<Tuple<HttpStatusCode, IDictionary<string, object>>> t) => AVObjectCoder.Instance.Decode(t.Result.Item2, AVDecoder.Instance).MutatedClone(delegate(MutableObjectState mutableClone)
			{
				mutableClone.IsNew = (t.Result.Item1 == HttpStatusCode.Created);
			}));
		}

		public IList<Task<IObjectState>> SaveAllAsync(IList<IObjectState> states, IList<IDictionary<string, IAVFieldOperation>> operationsList, string sessionToken, CancellationToken cancellationToken)
		{
			List<AVCommand> requests = states.Zip(operationsList, (IObjectState item, IDictionary<string, IAVFieldOperation> ops) => new AVCommand((item.ObjectId == null) ? $"classes/{Uri.EscapeDataString(item.ClassName)}" : $"classes/{Uri.EscapeDataString(item.ClassName)}/{Uri.EscapeDataString(item.ObjectId)}", (item.ObjectId == null) ? "POST" : "PUT", null, null, AVObject.ToJSONObjectForSaving(ops))).ToList();
			IList<Task<IDictionary<string, object>>> list = ExecuteBatchRequests(requests, sessionToken, cancellationToken);
			List<Task<IObjectState>> list2 = new List<Task<IObjectState>>();
			foreach (Task<IDictionary<string, object>> item in list)
			{
				list2.Add(item.OnSuccess((Task<IDictionary<string, object>> t) => AVObjectCoder.Instance.Decode(t.Result, AVDecoder.Instance)));
			}
			return list2;
		}

		public Task DeleteAsync(IObjectState state, string sessionToken, CancellationToken cancellationToken)
		{
			AVCommand command = new AVCommand($"classes/{state.ClassName}/{state.ObjectId}", "DELETE", sessionToken, (IList<KeyValuePair<string, string>>)null, (IDictionary<string, object>)null);
			return commandRunner.RunCommandAsync(command, null, null, cancellationToken);
		}

		public IList<Task> DeleteAllAsync(IList<IObjectState> states, string sessionToken, CancellationToken cancellationToken)
		{
			List<AVCommand> requests = (from item in states
				where item.ObjectId != null
				select new AVCommand($"classes/{Uri.EscapeDataString(item.ClassName)}/{Uri.EscapeDataString(item.ObjectId)}", "DELETE", (string)null, (IList<KeyValuePair<string, string>>)null, (IDictionary<string, object>)null)).ToList();
			return ExecuteBatchRequests(requests, sessionToken, cancellationToken).Cast<Task>().ToList();
		}

		internal IList<Task<IDictionary<string, object>>> ExecuteBatchRequests(IList<AVCommand> requests, string sessionToken, CancellationToken cancellationToken)
		{
			List<Task<IDictionary<string, object>>> list = new List<Task<IDictionary<string, object>>>();
			int num = requests.Count;
			IEnumerable<AVCommand> source = requests;
			while (num > 50)
			{
				List<AVCommand> requests2 = source.Take(50).ToList();
				source = source.Skip(50);
				list.AddRange(ExecuteBatchRequest(requests2, sessionToken, cancellationToken));
				num = source.Count();
			}
			list.AddRange(ExecuteBatchRequest(source.ToList(), sessionToken, cancellationToken));
			return list;
		}

		private IList<Task<IDictionary<string, object>>> ExecuteBatchRequest(IList<AVCommand> requests, string sessionToken, CancellationToken cancellationToken)
		{
			List<Task<IDictionary<string, object>>> list = new List<Task<IDictionary<string, object>>>();
			int batchSize = requests.Count;
			List<TaskCompletionSource<IDictionary<string, object>>> tcss = new List<TaskCompletionSource<IDictionary<string, object>>>();
			for (int i = 0; i < batchSize; i++)
			{
				TaskCompletionSource<IDictionary<string, object>> taskCompletionSource = new TaskCompletionSource<IDictionary<string, object>>();
				tcss.Add(taskCompletionSource);
				list.Add(taskCompletionSource.Task);
			}
			List<object> value = requests.Select(delegate(AVCommand r)
			{
				Dictionary<string, object> dictionary3 = new Dictionary<string, object>
				{
					{
						"method",
						r.Method
					},
					{
						"path",
						r.Uri.AbsolutePath
					}
				};
				if (r.DataObject != null)
				{
					dictionary3["body"] = r.DataObject;
				}
				return dictionary3;
			}).Cast<object>().ToList();
			AVCommand command = new AVCommand("batch", "POST", sessionToken, null, new Dictionary<string, object>
			{
				{
					"requests",
					value
				}
			});
			commandRunner.RunCommandAsync(command, null, null, cancellationToken).ContinueWith(delegate(Task<Tuple<HttpStatusCode, IDictionary<string, object>>> t)
			{
				if (t.IsFaulted || t.IsCanceled)
				{
					foreach (TaskCompletionSource<IDictionary<string, object>> item in tcss)
					{
						if (t.IsFaulted)
						{
							item.TrySetException(t.Exception);
						}
						else if (t.IsCanceled)
						{
							item.TrySetCanceled();
						}
					}
				}
				else
				{
					IList<object> list2 = Conversion.As<IList<object>>(t.Result.Item2["results"]);
					int count = list2.Count;
					if (count != batchSize)
					{
						foreach (TaskCompletionSource<IDictionary<string, object>> item2 in tcss)
						{
							item2.TrySetException(new InvalidOperationException("Batch command result count expected: " + batchSize + " but was: " + count + "."));
						}
					}
					else
					{
						for (int j = 0; j < batchSize; j++)
						{
							Dictionary<string, object> dictionary = list2[j] as Dictionary<string, object>;
							TaskCompletionSource<IDictionary<string, object>> taskCompletionSource2 = tcss[j];
							if (dictionary.ContainsKey("success"))
							{
								taskCompletionSource2.TrySetResult(dictionary["success"] as IDictionary<string, object>);
							}
							else if (dictionary.ContainsKey("error"))
							{
								IDictionary<string, object> dictionary2 = dictionary["error"] as IDictionary<string, object>;
								long num = (long)dictionary2["code"];
								taskCompletionSource2.TrySetException(new AVException((AVException.ErrorCode)num, dictionary2["error"] as string));
							}
							else
							{
								taskCompletionSource2.TrySetException(new InvalidOperationException("Invalid batch command response."));
							}
						}
					}
				}
			});
			return list;
		}
	}
}
