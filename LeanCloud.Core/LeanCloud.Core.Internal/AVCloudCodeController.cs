using LeanCloud.Storage.Internal;
using LeanCloud.Utilities;
using System;
using System.Collections.Generic;
using System.Net;
using System.Threading;
using System.Threading.Tasks;

namespace LeanCloud.Core.Internal
{
	public class AVCloudCodeController : IAVCloudCodeController
	{
		private readonly IAVCommandRunner commandRunner;

		public AVCloudCodeController(IAVCommandRunner commandRunner)
		{
			this.commandRunner = commandRunner;
		}

		public Task<T> CallFunctionAsync<T>(string name, IDictionary<string, object> parameters, string sessionToken, CancellationToken cancellationToken)
		{
			AVCommand command = new AVCommand($"functions/{Uri.EscapeUriString(name)}", "POST", sessionToken, null, NoObjectsEncoder.Instance.Encode(parameters) as IDictionary<string, object>);
			return commandRunner.RunCommandAsync(command, null, null, cancellationToken).OnSuccess(delegate(Task<Tuple<HttpStatusCode, IDictionary<string, object>>> t)
			{
				IDictionary<string, object> dictionary = AVDecoder.Instance.Decode(t.Result.Item2) as IDictionary<string, object>;
				return (!dictionary.ContainsKey("result")) ? default(T) : Conversion.To<T>(dictionary["result"]);
			});
		}

		public Task<T> RPCFunction<T>(string name, IDictionary<string, object> parameters, string sessionToken, CancellationToken cancellationToken)
		{
			AVCommand command = new AVCommand($"call/{Uri.EscapeUriString(name)}", "POST", sessionToken, null, NoObjectsEncoder.Instance.Encode(parameters) as IDictionary<string, object>);
			return commandRunner.RunCommandAsync(command, null, null, cancellationToken).OnSuccess(delegate(Task<Tuple<HttpStatusCode, IDictionary<string, object>>> t)
			{
				IDictionary<string, object> dictionary = AVDecoder.Instance.Decode(t.Result.Item2) as IDictionary<string, object>;
				return (!dictionary.ContainsKey("result")) ? default(T) : Conversion.To<T>(dictionary["result"]);
			});
		}
	}
}
