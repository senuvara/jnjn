using LeanCloud.Storage.Internal;
using System.Collections.Generic;
using System.Globalization;
using System.Linq;
using System.Threading;
using System.Threading.Tasks;

namespace LeanCloud.Core.Internal
{
	public class AVCurrentUserController : IAVCurrentUserController, IAVObjectCurrentController<AVUser>
	{
		private readonly object mutex = new object();

		private readonly TaskQueue taskQueue = new TaskQueue();

		private IStorageController storageController;

		private AVUser currentUser;

		public AVUser CurrentUser
		{
			get
			{
				lock (mutex)
				{
					return currentUser;
				}
			}
			set
			{
				lock (mutex)
				{
					currentUser = value;
				}
			}
		}

		public AVCurrentUserController(IStorageController storageController)
		{
			this.storageController = storageController;
		}

		public Task SetAsync(AVUser user, CancellationToken cancellationToken)
		{
			return taskQueue.Enqueue((Task toAwait) => toAwait.ContinueWith(delegate
			{
				Task task = null;
				if (user == null)
				{
					task = storageController.LoadAsync().OnSuccess((Task<IStorageDictionary<string, object>> t) => t.Result.RemoveAsync("CurrentUser")).Unwrap();
				}
				else
				{
					IDictionary<string, object> data = user.ServerDataToJSONObjectForSerialization();
					data["objectId"] = user.ObjectId;
					if (user.CreatedAt.HasValue)
					{
						data["createdAt"] = user.CreatedAt.Value.ToString(AVClient.DateFormatStrings.First(), CultureInfo.InvariantCulture);
					}
					if (user.UpdatedAt.HasValue)
					{
						data["updatedAt"] = user.UpdatedAt.Value.ToString(AVClient.DateFormatStrings.First(), CultureInfo.InvariantCulture);
					}
					task = storageController.LoadAsync().OnSuccess((Task<IStorageDictionary<string, object>> t) => t.Result.AddAsync("CurrentUser", Json.Encode(data))).Unwrap();
				}
				CurrentUser = user;
				return task;
			}).Unwrap(), cancellationToken);
		}

		public Task<AVUser> GetAsync(CancellationToken cancellationToken)
		{
			AVUser aVUser;
			lock (mutex)
			{
				aVUser = CurrentUser;
			}
			if (aVUser != null)
			{
				return Task.FromResult(aVUser);
			}
			return taskQueue.Enqueue((Task toAwait) => toAwait.ContinueWith((Task _) => storageController.LoadAsync().OnSuccess(delegate(Task<IStorageDictionary<string, object>> t)
			{
				t.Result.TryGetValue("CurrentUser", out object value);
				string text = value as string;
				AVUser result = null;
				if (text != null)
				{
					IDictionary<string, object> data = Json.Parse(text) as IDictionary<string, object>;
					result = AVObject.FromState<AVUser>(AVObjectCoder.Instance.Decode(data, AVDecoder.Instance), "_User");
				}
				CurrentUser = result;
				return result;
			})).Unwrap(), cancellationToken);
		}

		public Task<bool> ExistsAsync(CancellationToken cancellationToken)
		{
			if (CurrentUser != null)
			{
				return Task.FromResult(result: true);
			}
			return taskQueue.Enqueue((Task toAwait) => toAwait.ContinueWith((Task _) => storageController.LoadAsync().OnSuccess((Task<IStorageDictionary<string, object>> t) => t.Result.ContainsKey("CurrentUser"))).Unwrap(), cancellationToken);
		}

		public bool IsCurrent(AVUser user)
		{
			lock (mutex)
			{
				return CurrentUser == user;
			}
		}

		public void ClearFromMemory()
		{
			CurrentUser = null;
		}

		public void ClearFromDisk()
		{
			lock (mutex)
			{
				ClearFromMemory();
				taskQueue.Enqueue((Task toAwait) => toAwait.ContinueWith((Task _) => storageController.LoadAsync().OnSuccess((Task<IStorageDictionary<string, object>> t) => t.Result.RemoveAsync("CurrentUser"))).Unwrap().Unwrap(), CancellationToken.None);
			}
		}

		public Task<string> GetCurrentSessionTokenAsync(CancellationToken cancellationToken)
		{
			return GetAsync(cancellationToken).OnSuccess((Task<AVUser> t) => t.Result?.SessionToken);
		}

		public Task LogOutAsync(CancellationToken cancellationToken)
		{
			return taskQueue.Enqueue((Task toAwait) => toAwait.ContinueWith((Task _) => GetAsync(cancellationToken)).Unwrap().OnSuccess(delegate
			{
				ClearFromDisk();
			}), cancellationToken);
		}
	}
}
