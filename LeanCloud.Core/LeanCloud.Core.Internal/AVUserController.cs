using LeanCloud.Storage.Internal;
using System;
using System.Collections.Generic;
using System.Net;
using System.Threading;
using System.Threading.Tasks;

namespace LeanCloud.Core.Internal
{
	public class AVUserController : IAVUserController
	{
		private readonly IAVCommandRunner commandRunner;

		public AVUserController(IAVCommandRunner commandRunner)
		{
			this.commandRunner = commandRunner;
		}

		public Task<IObjectState> SignUpAsync(IObjectState state, IDictionary<string, IAVFieldOperation> operations, CancellationToken cancellationToken)
		{
			IDictionary<string, object> data = AVObject.ToJSONObjectForSaving(operations);
			AVCommand command = new AVCommand("classes/_User", "POST", null, null, data);
			return commandRunner.RunCommandAsync(command, null, null, cancellationToken).OnSuccess((Task<Tuple<HttpStatusCode, IDictionary<string, object>>> t) => AVObjectCoder.Instance.Decode(t.Result.Item2, AVDecoder.Instance).MutatedClone(delegate(MutableObjectState mutableClone)
			{
				mutableClone.IsNew = true;
			}));
		}

		public Task<IObjectState> LogInAsync(string username, string password, CancellationToken cancellationToken)
		{
			Dictionary<string, object> parameters = new Dictionary<string, object>
			{
				{
					"username",
					username
				},
				{
					"password",
					password
				}
			};
			AVCommand command = new AVCommand($"login?{AVClient.BuildQueryString(parameters)}", "GET", (string)null, (IList<KeyValuePair<string, string>>)null, (IDictionary<string, object>)null);
			return commandRunner.RunCommandAsync(command, null, null, cancellationToken).OnSuccess((Task<Tuple<HttpStatusCode, IDictionary<string, object>>> t) => AVObjectCoder.Instance.Decode(t.Result.Item2, AVDecoder.Instance).MutatedClone(delegate(MutableObjectState mutableClone)
			{
				mutableClone.IsNew = (t.Result.Item1 == HttpStatusCode.Created);
			}));
		}

		public Task<IObjectState> LogInAsync(string authType, IDictionary<string, object> data, CancellationToken cancellationToken)
		{
			Dictionary<string, object> dictionary = new Dictionary<string, object>();
			dictionary[authType] = data;
			AVCommand command = new AVCommand("users", "POST", null, null, new Dictionary<string, object>
			{
				{
					"authData",
					dictionary
				}
			});
			return commandRunner.RunCommandAsync(command, null, null, cancellationToken).OnSuccess((Task<Tuple<HttpStatusCode, IDictionary<string, object>>> t) => AVObjectCoder.Instance.Decode(t.Result.Item2, AVDecoder.Instance).MutatedClone(delegate(MutableObjectState mutableClone)
			{
				mutableClone.IsNew = (t.Result.Item1 == HttpStatusCode.Created);
			}));
		}

		public Task<IObjectState> GetUserAsync(string sessionToken, CancellationToken cancellationToken)
		{
			AVCommand command = new AVCommand("users/me", "GET", sessionToken, (IList<KeyValuePair<string, string>>)null, (IDictionary<string, object>)null);
			return commandRunner.RunCommandAsync(command, null, null, cancellationToken).OnSuccess((Task<Tuple<HttpStatusCode, IDictionary<string, object>>> t) => AVObjectCoder.Instance.Decode(t.Result.Item2, AVDecoder.Instance));
		}

		public Task RequestPasswordResetAsync(string email, CancellationToken cancellationToken)
		{
			AVCommand command = new AVCommand("requestPasswordReset", "POST", null, null, new Dictionary<string, object>
			{
				{
					"email",
					email
				}
			});
			return commandRunner.RunCommandAsync(command, null, null, cancellationToken);
		}

		public Task<IObjectState> LogInWithParametersAsync(string relativeUrl, IDictionary<string, object> data, CancellationToken cancellationToken)
		{
			AVCommand command = new AVCommand($"{relativeUrl}", "POST", null, null, data);
			return commandRunner.RunCommandAsync(command, null, null, cancellationToken).OnSuccess((Task<Tuple<HttpStatusCode, IDictionary<string, object>>> t) => AVObjectCoder.Instance.Decode(t.Result.Item2, AVDecoder.Instance).MutatedClone(delegate(MutableObjectState mutableClone)
			{
				mutableClone.IsNew = (t.Result.Item1 == HttpStatusCode.Created);
			}));
		}

		public Task UpdatePasswordAsync(string userId, string sessionToken, string oldPassword, string newPassword, CancellationToken cancellationToken)
		{
			AVCommand command = new AVCommand($"users/{userId}/updatePassword", "PUT", sessionToken, null, new Dictionary<string, object>
			{
				{
					"old_password",
					oldPassword
				},
				{
					"new_password",
					newPassword
				}
			});
			return commandRunner.RunCommandAsync(command, null, null, cancellationToken);
		}

		public Task<IObjectState> RefreshSessionTokenAsync(string userId, string sessionToken, CancellationToken cancellationToken)
		{
			AVCommand command = new AVCommand($"users/{userId}/refreshSessionToken", "PUT", sessionToken, (IList<KeyValuePair<string, string>>)null, (IDictionary<string, object>)null);
			return AVPlugins.Instance.CommandRunner.RunCommandAsync(command).OnSuccess((Task<Tuple<HttpStatusCode, IDictionary<string, object>>> t) => AVObjectCoder.Instance.Decode(t.Result.Item2, AVDecoder.Instance));
		}
	}
}
