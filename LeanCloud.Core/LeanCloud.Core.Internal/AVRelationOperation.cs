using System;
using System.Collections.Generic;
using System.Collections.ObjectModel;
using System.Linq;

namespace LeanCloud.Core.Internal
{
	public class AVRelationOperation : IAVFieldOperation
	{
		private readonly IList<string> adds;

		private readonly IList<string> removes;

		private readonly string targetClassName;

		public string TargetClassName => targetClassName;

		private AVRelationOperation(IEnumerable<string> adds, IEnumerable<string> removes, string targetClassName)
		{
			this.targetClassName = targetClassName;
			this.adds = new ReadOnlyCollection<string>(adds.ToList());
			this.removes = new ReadOnlyCollection<string>(removes.ToList());
		}

		public AVRelationOperation(IEnumerable<AVObject> adds, IEnumerable<AVObject> removes)
		{
			adds = (adds ?? new AVObject[0]);
			removes = (removes ?? new AVObject[0]);
			targetClassName = (from o in adds.Concat(removes)
				select o.ClassName).FirstOrDefault();
			this.adds = new ReadOnlyCollection<string>(IdsFromObjects(adds).ToList());
			this.removes = new ReadOnlyCollection<string>(IdsFromObjects(removes).ToList());
		}

		public object Encode()
		{
			List<object> list = adds.Select((string id) => PointerOrLocalIdEncoder.Instance.Encode(AVObject.CreateWithoutData(targetClassName, id))).ToList();
			List<object> list2 = removes.Select((string id) => PointerOrLocalIdEncoder.Instance.Encode(AVObject.CreateWithoutData(targetClassName, id))).ToList();
			Dictionary<string, object> dictionary = (list.Count == 0) ? null : new Dictionary<string, object>
			{
				{
					"__op",
					"AddRelation"
				},
				{
					"objects",
					list
				}
			};
			Dictionary<string, object> dictionary2 = (list2.Count == 0) ? null : new Dictionary<string, object>
			{
				{
					"__op",
					"RemoveRelation"
				},
				{
					"objects",
					list2
				}
			};
			if (dictionary != null && dictionary2 != null)
			{
				Dictionary<string, object> dictionary3 = new Dictionary<string, object>();
				dictionary3.Add("__op", "Batch");
				dictionary3.Add("ops", new Dictionary<string, object>[2]
				{
					dictionary,
					dictionary2
				});
				return dictionary3;
			}
			return dictionary ?? dictionary2;
		}

		public IAVFieldOperation MergeWithPrevious(IAVFieldOperation previous)
		{
			if (previous == null)
			{
				return this;
			}
			if (previous is AVDeleteOperation)
			{
				throw new InvalidOperationException("You can't modify a relation after deleting it.");
			}
			AVRelationOperation aVRelationOperation = previous as AVRelationOperation;
			if (aVRelationOperation != null)
			{
				if (aVRelationOperation.TargetClassName != TargetClassName)
				{
					throw new InvalidOperationException($"Related object must be of class {aVRelationOperation.TargetClassName}, but {TargetClassName} was passed in.");
				}
				List<string> list = adds.Union(aVRelationOperation.adds.Except(removes)).ToList();
				List<string> list2 = removes.Union(aVRelationOperation.removes.Except(adds)).ToList();
				return new AVRelationOperation(list, list2, TargetClassName);
			}
			throw new InvalidOperationException("Operation is invalid after previous operation.");
		}

		public object Apply(object oldValue, string key)
		{
			if (adds.Count == 0 && removes.Count == 0)
			{
				return null;
			}
			if (oldValue == null)
			{
				return AVRelationBase.CreateRelation(null, key, targetClassName);
			}
			if (oldValue is AVRelationBase)
			{
				AVRelationBase aVRelationBase = (AVRelationBase)oldValue;
				string text = aVRelationBase.TargetClassName;
				if (text != null && text != targetClassName)
				{
					throw new InvalidOperationException("Related object must be a " + text + ", but a " + targetClassName + " was passed in.");
				}
				aVRelationBase.TargetClassName = targetClassName;
				return aVRelationBase;
			}
			throw new InvalidOperationException("Operation is invalid after previous operation.");
		}

		private IEnumerable<string> IdsFromObjects(IEnumerable<AVObject> objects)
		{
			foreach (AVObject @object in objects)
			{
				if (@object.ObjectId == null)
				{
					throw new ArgumentException("You can't add an unsaved AVObject to a relation.");
				}
				if (@object.ClassName != targetClassName)
				{
					throw new ArgumentException($"Tried to create a AVRelation with 2 different types: {targetClassName} and {@object.ClassName}");
				}
			}
			return objects.Select((AVObject o) => o.ObjectId).Distinct();
		}
	}
}
