using System;

namespace LeanCloud.Core.Internal
{
	[AttributeUsage(AttributeTargets.All)]
	internal class LinkerSafeAttribute : Attribute
	{
	}
}
