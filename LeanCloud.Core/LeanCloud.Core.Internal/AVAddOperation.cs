using LeanCloud.Utilities;
using System;
using System.Collections.Generic;
using System.Collections.ObjectModel;
using System.Linq;

namespace LeanCloud.Core.Internal
{
	public class AVAddOperation : IAVFieldOperation
	{
		private ReadOnlyCollection<object> objects;

		public IEnumerable<object> Objects => objects;

		public AVAddOperation(IEnumerable<object> objects)
		{
			this.objects = new ReadOnlyCollection<object>(objects.ToList());
		}

		public object Encode()
		{
			return new Dictionary<string, object>
			{
				{
					"__op",
					"Add"
				},
				{
					"objects",
					PointerOrLocalIdEncoder.Instance.Encode(objects)
				}
			};
		}

		public IAVFieldOperation MergeWithPrevious(IAVFieldOperation previous)
		{
			if (previous == null)
			{
				return this;
			}
			if (previous is AVDeleteOperation)
			{
				return new AVSetOperation(objects.ToList());
			}
			if (previous is AVSetOperation)
			{
				return new AVSetOperation(Conversion.To<IList<object>>(((AVSetOperation)previous).Value).Concat(objects).ToList());
			}
			if (previous is AVAddOperation)
			{
				return new AVAddOperation(((AVAddOperation)previous).Objects.Concat(objects));
			}
			throw new InvalidOperationException("Operation is invalid after previous operation.");
		}

		public object Apply(object oldValue, string key)
		{
			if (oldValue == null)
			{
				return objects.ToList();
			}
			return Conversion.To<IList<object>>(oldValue).Concat(objects).ToList();
		}
	}
}
