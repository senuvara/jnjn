using LeanCloud.Storage.Internal;
using System;
using System.Collections.Generic;
using System.Threading;

namespace LeanCloud.Core.Internal
{
	[Preserve(AllMembers = true)]
	internal class PreserveWrapperTypes
	{
		private static List<object> CreateWrapperTypes()
		{
			return new List<object>
			{
				(Action)delegate
				{
					AVCloud.CallFunctionAsync<object>(null, null, null, CancellationToken.None);
				},
				(Action)delegate
				{
					AVCloud.CallFunctionAsync<bool>(null, null, null, CancellationToken.None);
				},
				(Action)delegate
				{
					AVCloud.CallFunctionAsync<byte>(null, null, null, CancellationToken.None);
				},
				(Action)delegate
				{
					AVCloud.CallFunctionAsync<sbyte>(null, null, null, CancellationToken.None);
				},
				(Action)delegate
				{
					AVCloud.CallFunctionAsync<short>(null, null, null, CancellationToken.None);
				},
				(Action)delegate
				{
					AVCloud.CallFunctionAsync<ushort>(null, null, null, CancellationToken.None);
				},
				(Action)delegate
				{
					AVCloud.CallFunctionAsync<int>(null, null, null, CancellationToken.None);
				},
				(Action)delegate
				{
					AVCloud.CallFunctionAsync<uint>(null, null, null, CancellationToken.None);
				},
				(Action)delegate
				{
					AVCloud.CallFunctionAsync<long>(null, null, null, CancellationToken.None);
				},
				(Action)delegate
				{
					AVCloud.CallFunctionAsync<ulong>(null, null, null, CancellationToken.None);
				},
				(Action)delegate
				{
					AVCloud.CallFunctionAsync<char>(null, null, null, CancellationToken.None);
				},
				(Action)delegate
				{
					AVCloud.CallFunctionAsync<double>(null, null, null, CancellationToken.None);
				},
				(Action)delegate
				{
					AVCloud.CallFunctionAsync<float>(null, null, null, CancellationToken.None);
				},
				(Action)delegate
				{
					AVCloud.CallFunctionAsync<IDictionary<string, object>>(null, null, null, CancellationToken.None);
				},
				(Action)delegate
				{
					AVCloud.CallFunctionAsync<IList<object>>(null, null, null, CancellationToken.None);
				},
				typeof(FlexibleListWrapper<object, AVGeoPoint>),
				typeof(FlexibleListWrapper<AVGeoPoint, object>),
				typeof(FlexibleDictionaryWrapper<object, AVGeoPoint>),
				typeof(FlexibleDictionaryWrapper<AVGeoPoint, object>)
			};
		}
	}
}
