using LeanCloud.Storage.Internal;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Net;
using System.Threading;
using System.Threading.Tasks;

namespace LeanCloud.Core.Internal
{
	internal class AVQueryController : IAVQueryController
	{
		private readonly IAVCommandRunner commandRunner;

		public AVQueryController(IAVCommandRunner commandRunner)
		{
			this.commandRunner = commandRunner;
		}

		public Task<IEnumerable<IObjectState>> FindAsync<T>(AVQuery<T> query, AVUser user, CancellationToken cancellationToken) where T : AVObject
		{
			string sessionToken = user?.SessionToken;
			return FindAsync(query.RelativeUri, query.BuildParameters(), sessionToken, cancellationToken).OnSuccess((Task<IDictionary<string, object>> t) => (t.Result["results"] as IList<object>).Select((object item) => AVObjectCoder.Instance.Decode(item as IDictionary<string, object>, AVDecoder.Instance)));
		}

		public Task<int> CountAsync<T>(AVQuery<T> query, AVUser user, CancellationToken cancellationToken) where T : AVObject
		{
			string sessionToken = user?.SessionToken;
			IDictionary<string, object> dictionary = query.BuildParameters();
			dictionary["limit"] = 0;
			dictionary["count"] = 1;
			return FindAsync(query.RelativeUri, dictionary, sessionToken, cancellationToken).OnSuccess((Task<IDictionary<string, object>> t) => Convert.ToInt32(t.Result["count"]));
		}

		public Task<IObjectState> FirstAsync<T>(AVQuery<T> query, AVUser user, CancellationToken cancellationToken) where T : AVObject
		{
			string sessionToken = user?.SessionToken;
			IDictionary<string, object> dictionary = query.BuildParameters();
			dictionary["limit"] = 1;
			return FindAsync(query.RelativeUri, dictionary, sessionToken, cancellationToken).OnSuccess(delegate(Task<IDictionary<string, object>> t)
			{
				IDictionary<string, object> dictionary2 = (t.Result["results"] as IList<object>).FirstOrDefault() as IDictionary<string, object>;
				return (dictionary2 == null) ? null : AVObjectCoder.Instance.Decode(dictionary2, AVDecoder.Instance);
			});
		}

		private Task<IDictionary<string, object>> FindAsync(string relativeUri, IDictionary<string, object> parameters, string sessionToken, CancellationToken cancellationToken)
		{
			AVCommand command = new AVCommand($"{relativeUri}?{AVClient.BuildQueryString(parameters)}", "GET", sessionToken, (IList<KeyValuePair<string, string>>)null, (IDictionary<string, object>)null);
			return commandRunner.RunCommandAsync(command, null, null, cancellationToken).OnSuccess((Task<Tuple<HttpStatusCode, IDictionary<string, object>>> t) => t.Result.Item2);
		}
	}
}
