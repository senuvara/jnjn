using LeanCloud.Storage.Internal;
using System.Collections.Generic;
using System.Threading;
using System.Threading.Tasks;

namespace LeanCloud.Core.Internal
{
	internal class AVCurrentConfigController : IAVCurrentConfigController
	{
		private const string CurrentConfigKey = "CurrentConfig";

		private readonly TaskQueue taskQueue;

		private AVConfig currentConfig;

		private IStorageController storageController;

		public AVCurrentConfigController(IStorageController storageController)
		{
			this.storageController = storageController;
			taskQueue = new TaskQueue();
		}

		public Task<AVConfig> GetCurrentConfigAsync()
		{
			return taskQueue.Enqueue((Task toAwait) => toAwait.ContinueWith((Task _) => (currentConfig == null) ? storageController.LoadAsync().OnSuccess(delegate(Task<IStorageDictionary<string, object>> t)
			{
				t.Result.TryGetValue("CurrentConfig", out object value);
				string text = value as string;
				if (text != null)
				{
					IDictionary<string, object> fetchedConfig = AVClient.DeserializeJsonString(text);
					currentConfig = new AVConfig(fetchedConfig);
				}
				else
				{
					currentConfig = new AVConfig();
				}
				return currentConfig;
			}) : Task.FromResult(currentConfig)), CancellationToken.None).Unwrap();
		}

		public Task SetCurrentConfigAsync(AVConfig config)
		{
			return taskQueue.Enqueue((Task toAwait) => toAwait.ContinueWith(delegate
			{
				currentConfig = config;
				IDictionary<string, object> jsonData = ((IJsonConvertible)config).ToJSON();
				string jsonString = AVClient.SerializeJsonString(jsonData);
				return storageController.LoadAsync().OnSuccess((Task<IStorageDictionary<string, object>> t) => t.Result.AddAsync("CurrentConfig", jsonString));
			}).Unwrap().Unwrap(), CancellationToken.None);
		}

		public Task ClearCurrentConfigAsync()
		{
			return taskQueue.Enqueue((Task toAwait) => toAwait.ContinueWith(delegate
			{
				currentConfig = null;
				return storageController.LoadAsync().OnSuccess((Task<IStorageDictionary<string, object>> t) => t.Result.RemoveAsync("CurrentConfig"));
			}).Unwrap().Unwrap(), CancellationToken.None);
		}

		public Task ClearCurrentConfigInMemoryAsync()
		{
			return taskQueue.Enqueue((Task toAwait) => toAwait.ContinueWith(delegate
			{
				currentConfig = null;
			}), CancellationToken.None);
		}
	}
}
