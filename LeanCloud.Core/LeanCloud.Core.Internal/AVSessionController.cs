using LeanCloud.Storage.Internal;
using System;
using System.Collections.Generic;
using System.Net;
using System.Threading;
using System.Threading.Tasks;

namespace LeanCloud.Core.Internal
{
	public class AVSessionController : IAVSessionController
	{
		private readonly IAVCommandRunner commandRunner;

		public AVSessionController(IAVCommandRunner commandRunner)
		{
			this.commandRunner = commandRunner;
		}

		public Task<IObjectState> GetSessionAsync(string sessionToken, CancellationToken cancellationToken)
		{
			AVCommand command = new AVCommand("sessions/me", "GET", sessionToken, (IList<KeyValuePair<string, string>>)null, (IDictionary<string, object>)null);
			return commandRunner.RunCommandAsync(command, null, null, cancellationToken).OnSuccess((Task<Tuple<HttpStatusCode, IDictionary<string, object>>> t) => AVObjectCoder.Instance.Decode(t.Result.Item2, AVDecoder.Instance));
		}

		public Task RevokeAsync(string sessionToken, CancellationToken cancellationToken)
		{
			AVCommand command = new AVCommand("logout", "POST", sessionToken, null, new Dictionary<string, object>());
			return commandRunner.RunCommandAsync(command, null, null, cancellationToken);
		}

		public Task<IObjectState> UpgradeToRevocableSessionAsync(string sessionToken, CancellationToken cancellationToken)
		{
			AVCommand command = new AVCommand("upgradeToRevocableSession", "POST", sessionToken, null, new Dictionary<string, object>());
			return commandRunner.RunCommandAsync(command, null, null, cancellationToken).OnSuccess((Task<Tuple<HttpStatusCode, IDictionary<string, object>>> t) => AVObjectCoder.Instance.Decode(t.Result.Item2, AVDecoder.Instance));
		}

		public bool IsRevocableSessionToken(string sessionToken)
		{
			return sessionToken.Contains("r:");
		}
	}
}
