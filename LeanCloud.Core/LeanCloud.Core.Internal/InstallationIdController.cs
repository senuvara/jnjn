using LeanCloud.Storage.Internal;
using System;
using System.Threading.Tasks;

namespace LeanCloud.Core.Internal
{
	public class InstallationIdController : IInstallationIdController
	{
		private const string InstallationIdKey = "InstallationId";

		private readonly object mutex = new object();

		private Guid? installationId;

		private readonly IStorageController storageController;

		public InstallationIdController(IStorageController storageController)
		{
			this.storageController = storageController;
		}

		public Task SetAsync(Guid? installationId)
		{
			lock (mutex)
			{
				Task result = installationId.HasValue ? storageController.LoadAsync().OnSuccess((Task<IStorageDictionary<string, object>> storage) => storage.Result.AddAsync("InstallationId", installationId.ToString())).Unwrap() : storageController.LoadAsync().OnSuccess((Task<IStorageDictionary<string, object>> storage) => storage.Result.RemoveAsync("InstallationId")).Unwrap();
				this.installationId = installationId;
				return result;
			}
		}

		public Task<Guid?> GetAsync()
		{
			lock (mutex)
			{
				if (installationId.HasValue)
				{
					return Task.FromResult(installationId);
				}
			}
			Guid newInstallationId = default(Guid);
			return storageController.LoadAsync().OnSuccess(delegate(Task<IStorageDictionary<string, object>> s)
			{
				s.Result.TryGetValue("InstallationId", out object value);
				try
				{
					lock (mutex)
					{
						installationId = new Guid((string)value);
						return Task.FromResult(installationId);
					}
				}
				catch (Exception)
				{
					newInstallationId = Guid.NewGuid();
					return SetAsync(newInstallationId).OnSuccess((Func<Task, Guid?>)((Task _) => newInstallationId));
				}
			}).Unwrap();
		}

		public Task ClearAsync()
		{
			return SetAsync(null);
		}
	}
}
