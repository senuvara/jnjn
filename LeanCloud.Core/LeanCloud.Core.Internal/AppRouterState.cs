using System;

namespace LeanCloud.Core.Internal
{
	public class AppRouterState
	{
		private static object mutex = new object();

		public long TTL
		{
			get;
			internal set;
		}

		public string ApiServer
		{
			get;
			internal set;
		}

		public string EngineServer
		{
			get;
			internal set;
		}

		public string PushServer
		{
			get;
			internal set;
		}

		public string RealtimeRouterServer
		{
			get;
			internal set;
		}

		public string StatsServer
		{
			get;
			internal set;
		}

		public string Source
		{
			get;
			internal set;
		}

		public DateTime FetchedAt
		{
			get;
			internal set;
		}

		public AppRouterState()
		{
			FetchedAt = DateTime.Now;
		}

		public bool isExpired()
		{
			return DateTime.Now > FetchedAt + TimeSpan.FromSeconds(TTL);
		}

		public static AppRouterState GetInitial(string appId, AVClient.Configuration.AVRegion region)
		{
			switch (region)
			{
			case AVClient.Configuration.AVRegion.Public_CN:
			{
				string arg = appId.Substring(0, 8).ToLower();
				return new AppRouterState
				{
					TTL = -1L,
					ApiServer = $"{arg}.api.lncld.net",
					EngineServer = $"{arg}.engine.lncld.net",
					PushServer = $"{arg}.push.lncld.net",
					RealtimeRouterServer = $"{arg}.rtm.lncld.net",
					StatsServer = $"{arg}.stats.lncld.net",
					Source = "initial"
				};
			}
			case AVClient.Configuration.AVRegion.Public_US:
				return new AppRouterState
				{
					TTL = -1L,
					ApiServer = "us-api.leancloud.cn",
					EngineServer = "us-api.leancloud.cn",
					PushServer = "us-api.leancloud.cn",
					RealtimeRouterServer = "router-a0-push.leancloud.cn",
					StatsServer = "us-api.leancloud.cn",
					Source = "initial"
				};
			case AVClient.Configuration.AVRegion.Vendor_Tencent:
				return new AppRouterState
				{
					TTL = -1L,
					ApiServer = "e1-api.leancloud.cn",
					EngineServer = "e1-api.leancloud.cn",
					PushServer = "e1-api.leancloud.cn",
					RealtimeRouterServer = "router-q0-push.leancloud.cn",
					StatsServer = "e1-api.leancloud.cn",
					Source = "initial"
				};
			default:
				throw new AVException(AVException.ErrorCode.OtherCause, "invalid region");
			}
		}
	}
}
