using LeanCloud.Storage.Internal;
using System;
using System.Collections.Generic;
using System.Net;
using System.Threading;
using System.Threading.Tasks;

namespace LeanCloud.Core.Internal
{
	public class AppRouterController : IAppRouterController
	{
		private AppRouterState currentState;

		private object mutex = new object();

		public AppRouterState Get()
		{
			AppRouterState initial;
			lock (mutex)
			{
				if (currentState != null)
				{
					initial = currentState;
				}
				string applicationId = AVClient.CurrentConfiguration.ApplicationId;
				AVClient.Configuration.AVRegion region = AVClient.CurrentConfiguration.Region;
				initial = AppRouterState.GetInitial(applicationId, region);
			}
			if (AVClient.CurrentConfiguration.Region != AVClient.Configuration.AVRegion.Public_US)
			{
				return initial;
			}
			if (initial.isExpired())
			{
				lock (mutex)
				{
					initial.FetchedAt = DateTime.Now + TimeSpan.FromMinutes(10.0);
				}
				Task.Factory.StartNew(RefreshAsync);
			}
			return initial;
		}

		public Task RefreshAsync()
		{
			return QueryAsync(CancellationToken.None).ContinueWith(delegate(Task<AppRouterState> t)
			{
				if (!t.IsFaulted && !t.IsCanceled)
				{
					lock (mutex)
					{
						currentState = t.Result;
					}
				}
			});
		}

		public Task<AppRouterState> QueryAsync(CancellationToken cancellationToken)
		{
			string applicationId = AVClient.CurrentConfiguration.ApplicationId;
			string uriString = $"https://app-router.leancloud.cn/2/route?appId={applicationId}";
			HttpRequest httpRequest = new HttpRequest
			{
				Uri = new Uri(uriString),
				Method = "GET"
			};
			return AVPlugins.Instance.HttpClient.ExecuteAsync(httpRequest, null, null, cancellationToken).ContinueWith(delegate(Task<Tuple<HttpStatusCode, string>> t)
			{
				TaskCompletionSource<AppRouterState> taskCompletionSource = new TaskCompletionSource<AppRouterState>();
				if (t.Result.Item1 != HttpStatusCode.OK)
				{
					taskCompletionSource.SetException(new AVException(AVException.ErrorCode.ConnectionFailed, "can not reach router."));
				}
				else
				{
					IDictionary<string, object> jsonObj = Json.Parse(t.Result.Item2) as IDictionary<string, object>;
					taskCompletionSource.SetResult(ParseAppRouterState(jsonObj));
				}
				return taskCompletionSource.Task;
			}).Unwrap();
		}

		private static AppRouterState ParseAppRouterState(IDictionary<string, object> jsonObj)
		{
			return new AppRouterState
			{
				TTL = (int)jsonObj["ttl"],
				StatsServer = (jsonObj["stats_server"] as string),
				RealtimeRouterServer = (jsonObj["rtm_router_server"] as string),
				PushServer = (jsonObj["push_server"] as string),
				EngineServer = (jsonObj["engine_server"] as string),
				ApiServer = (jsonObj["api_server"] as string),
				Source = "network"
			};
		}
	}
}
