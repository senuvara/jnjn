namespace LeanCloud.Core.Internal
{
	public abstract class SHA1 : HashAlgorithm
	{
		protected SHA1()
		{
			HashSizeValue = 160;
		}
	}
}
