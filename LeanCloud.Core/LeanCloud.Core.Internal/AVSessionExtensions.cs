using System.Threading;
using System.Threading.Tasks;

namespace LeanCloud.Core.Internal
{
	public static class AVSessionExtensions
	{
		public static Task<string> UpgradeToRevocableSessionAsync(string sessionToken, CancellationToken cancellationToken)
		{
			return AVSession.UpgradeToRevocableSessionAsync(sessionToken, cancellationToken);
		}

		public static Task RevokeAsync(string sessionToken, CancellationToken cancellationToken)
		{
			return AVSession.RevokeAsync(sessionToken, cancellationToken);
		}
	}
}
