namespace LeanCloud.Core.Internal
{
	internal enum CommonSize : long
	{
		MB4 = 0x400000L,
		MB1 = 0x100000L,
		KB512 = 0x80000L,
		KB256 = 0x40000L
	}
}
