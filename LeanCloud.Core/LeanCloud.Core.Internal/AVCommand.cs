using LeanCloud.Storage.Internal;
using System;
using System.Collections.Generic;
using System.IO;
using System.Linq;
using System.Text;

namespace LeanCloud.Core.Internal
{
	public class AVCommand : HttpRequest
	{
		public IDictionary<string, object> DataObject
		{
			get;
			private set;
		}

		public override Stream Data
		{
			get
			{
				if (base.Data != null)
				{
					return base.Data;
				}
				return base.Data = ((DataObject != null) ? new MemoryStream(Encoding.UTF8.GetBytes(Json.Encode(DataObject))) : null);
			}
			set
			{
				base.Data = value;
			}
		}

		public AVCommand(string relativeUri, string method, string sessionToken = null, IList<KeyValuePair<string, string>> headers = null, IDictionary<string, object> data = null)
			: this(relativeUri, method, sessionToken, headers, null, (data != null) ? "application/json" : null)
		{
			DataObject = data;
		}

		public AVCommand(string relativeUri, string method, string sessionToken = null, IList<KeyValuePair<string, string>> headers = null, Stream stream = null, string contentType = null)
		{
			AppRouterState appRouterState = AVPlugins.Instance.AppRouterController.Get();
			string format = "https://{0}/{1}/{2}";
			AVClient.Configuration currentConfiguration = AVClient.CurrentConfiguration;
			string arg = "1.1";
			if (relativeUri.StartsWith("push") || relativeUri.StartsWith("installations"))
			{
				base.Uri = new Uri(string.Format(format, appRouterState.PushServer, arg, relativeUri));
				if (currentConfiguration.PushServer != null)
				{
					base.Uri = new Uri($"{currentConfiguration.PushServer}/{arg}/{relativeUri}");
				}
			}
			else if (relativeUri.StartsWith("stats") || relativeUri.StartsWith("always_collect") || relativeUri.StartsWith("statistics"))
			{
				base.Uri = new Uri(string.Format(format, appRouterState.StatsServer, arg, relativeUri));
				if (currentConfiguration.StatsServer != null)
				{
					base.Uri = new Uri($"{currentConfiguration.StatsServer}/{arg}/{relativeUri}");
				}
			}
			else if (relativeUri.StartsWith("functions") || relativeUri.StartsWith("call"))
			{
				base.Uri = new Uri(string.Format(format, appRouterState.EngineServer, arg, relativeUri));
				if (currentConfiguration.EngineServer != null)
				{
					base.Uri = new Uri($"{currentConfiguration.EngineServer}/{arg}/{relativeUri}");
				}
			}
			else
			{
				base.Uri = new Uri(string.Format(format, appRouterState.ApiServer, arg, relativeUri));
				if (currentConfiguration.ApiServer != null)
				{
					base.Uri = new Uri($"{currentConfiguration.ApiServer}/{arg}/{relativeUri}");
				}
			}
			base.Method = method;
			Data = stream;
			base.Headers = new List<KeyValuePair<string, string>>(headers ?? Enumerable.Empty<KeyValuePair<string, string>>());
			string value = AVClient.UseProduction ? "1" : "0";
			base.Headers.Add(new KeyValuePair<string, string>("X-LC-Prod", value));
			if (!string.IsNullOrEmpty(sessionToken))
			{
				base.Headers.Add(new KeyValuePair<string, string>("X-LC-Session", sessionToken));
			}
			if (!string.IsNullOrEmpty(contentType))
			{
				base.Headers.Add(new KeyValuePair<string, string>("Content-Type", contentType));
			}
		}

		public AVCommand(AVCommand other)
		{
			base.Uri = other.Uri;
			base.Method = other.Method;
			DataObject = other.DataObject;
			base.Headers = new List<KeyValuePair<string, string>>(other.Headers);
			Data = other.Data;
		}

		public string ToLog()
		{
			StringBuilder stringBuilder = new StringBuilder();
			string value = "===HTTP Request Start===";
			stringBuilder.AppendLine(value);
			string value2 = "Url: " + base.Uri;
			stringBuilder.AppendLine(value2);
			string value3 = "Method: " + base.Method;
			stringBuilder.AppendLine(value3);
			try
			{
				Dictionary<string, object> dict = ((IEnumerable<KeyValuePair<string, string>>)base.Headers).ToDictionary((Func<KeyValuePair<string, string>, string>)((KeyValuePair<string, string> x) => x.Key), (Func<KeyValuePair<string, string>, object>)((KeyValuePair<string, string> x) => x.Value));
				string value4 = "Headers: " + Json.Encode(dict);
				stringBuilder.AppendLine(value4);
			}
			catch (Exception)
			{
			}
			try
			{
				if (DataObject != null)
				{
					string value5 = "Body:" + Json.Encode(DataObject);
					stringBuilder.AppendLine(value5);
				}
			}
			catch (Exception)
			{
			}
			string value6 = "===HTTP Request End===";
			stringBuilder.AppendLine(value6);
			return stringBuilder.ToString();
		}
	}
}
