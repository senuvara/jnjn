using LeanCloud.Utilities;
using System;
using System.Collections.Generic;
using System.Globalization;
using System.Linq;

namespace LeanCloud.Core.Internal
{
	public class AVDecoder
	{
		private static readonly AVDecoder instance = new AVDecoder();

		public static AVDecoder Instance => instance;

		private AVDecoder()
		{
		}

		public object Decode(object data)
		{
			if (data == null)
			{
				return null;
			}
			IDictionary<string, object> dictionary = data as IDictionary<string, object>;
			if (dictionary != null)
			{
				if (dictionary.ContainsKey("__op"))
				{
					return ParseFieldOperations.Decode(dictionary);
				}
				dictionary.TryGetValue("__type", out object value);
				string text = value as string;
				if (text == null)
				{
					Dictionary<string, object> dictionary2 = new Dictionary<string, object>();
					{
						foreach (KeyValuePair<string, object> item in dictionary)
						{
							dictionary2[item.Key] = Decode(item.Value);
						}
						return dictionary2;
					}
				}
				switch (text)
				{
				case "Date":
					return ParseDate(dictionary["iso"] as string);
				case "Bytes":
					return Convert.FromBase64String(dictionary["base64"] as string);
				case "Pointer":
					if (dictionary.Keys.Count > 3)
					{
						return DecodeAVObject(dictionary);
					}
					return DecodePointer(dictionary["className"] as string, dictionary["objectId"] as string);
				case "File":
					return DecodeAVFile(dictionary);
				case "GeoPoint":
					return new AVGeoPoint(Conversion.To<double>(dictionary["latitude"]), Conversion.To<double>(dictionary["longitude"]));
				case "Object":
					return DecodeAVObject(dictionary);
				case "Relation":
					return AVRelationBase.CreateRelation(null, null, dictionary["className"] as string);
				default:
				{
					Dictionary<string, object> dictionary3 = new Dictionary<string, object>();
					{
						foreach (KeyValuePair<string, object> item2 in dictionary)
						{
							dictionary3[item2.Key] = Decode(item2.Value);
						}
						return dictionary3;
					}
				}
				}
			}
			IList<object> list = data as IList<object>;
			if (list != null)
			{
				return list.Select((object item) => Decode(item)).ToList();
			}
			return data;
		}

		protected virtual object DecodePointer(string className, string objectId)
		{
			if (className == "_File")
			{
				return AVFile.CreateWithoutData(objectId);
			}
			return AVObject.CreateWithoutData(className, objectId);
		}

		protected virtual object DecodeAVObject(IDictionary<string, object> dict)
		{
			if (dict["className"] as string== "_File")
			{
				return DecodeAVFile(dict);
			}
			return AVObject.FromState<AVObject>(AVObjectCoder.Instance.Decode(dict, this), dict["className"] as string);
		}

		protected virtual object DecodeAVFile(IDictionary<string, object> dict)
		{
			AVFile aVFile = AVFile.CreateWithoutData(dict["objectId"] as string);
			aVFile.MergeFromJSON(dict);
			return aVFile;
		}

		public virtual IList<T> DecodeList<T>(object data)
		{
			IList<T> result = null;
			IList<object> list = (IList<object>)data;
			if (list != null)
			{
				result = new List<T>();
				{
					foreach (object item in list)
					{
						result.Add((T)item);
					}
					return result;
				}
			}
			return result;
		}

		public static DateTime ParseDate(string input)
		{
			return DateTime.ParseExact(input, AVClient.DateFormatStrings, CultureInfo.InvariantCulture, DateTimeStyles.AssumeUniversal);
		}
	}
}
