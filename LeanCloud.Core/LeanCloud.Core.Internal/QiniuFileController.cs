using LeanCloud.Storage.Internal;
using System;
using System.Collections.Generic;
using System.IO;
using System.Net;
using System.Text;
using System.Threading;
using System.Threading.Tasks;

namespace LeanCloud.Core.Internal
{
	internal class QiniuFileController : AVFileController
	{
		private static int BLOCKSIZE = 4194304;

		private const int blockMashk = 4194303;

		private const int blockBits = 22;

		internal static string UP_HOST = "https://up.qbox.me";

		private object mutex = new object();

		private int CalcBlockCount(long fsize)
		{
			return (int)(fsize + 4194303 >> 22);
		}

		public QiniuFileController(IAVCommandRunner commandRunner)
			: base(commandRunner)
		{
		}

		public override Task<FileState> SaveAsync(FileState state, Stream dataStream, string sessionToken, IProgress<AVUploadProgressEventArgs> progress, CancellationToken cancellationToken)
		{
			if (state.Url != null)
			{
				return Task.FromResult(state);
			}
			state.frozenData = dataStream;
			state.CloudName = GetUniqueName(state);
			return GetQiniuToken(state, CancellationToken.None).ContinueWith(delegate(Task<Tuple<HttpStatusCode, IDictionary<string, object>>> t)
			{
				MergeFromJSON(state, t.Result.Item2);
				return UploadNextChunk(state, dataStream, string.Empty, 0L, progress);
			}).Unwrap().OnSuccess((Task s) => state);
		}

		private Task UploadNextChunk(FileState state, Stream dataStream, string context, long offset, IProgress<AVUploadProgressEventArgs> progress)
		{
			long totalSize = dataStream.Length;
			long num = totalSize - state.completed;
			if (progress != null)
			{
				lock (mutex)
				{
					progress.Report(new AVUploadProgressEventArgs
					{
						Progress = AVFileController.CalcProgress(state.completed, totalSize)
					});
				}
			}
			if (state.completed == totalSize)
			{
				return QiniuMakeFile(state, state.frozenData, state.token, state.CloudName, totalSize, state.block_ctxes.ToArray(), CancellationToken.None);
			}
			if (state.completed % BLOCKSIZE == 0L)
			{
				byte[] firstChunkBinary = GetChunkBinary(state.completed, dataStream);
				long blcokSize = (num > BLOCKSIZE) ? BLOCKSIZE : num;
				return MakeBlock(state, firstChunkBinary, blcokSize).ContinueWith(delegate(Task<Tuple<HttpStatusCode, string>> t)
				{
					Tuple<HttpStatusCode, IDictionary<string, object>> tuple2 = AVClient.ReponseResolve(t.Result, CancellationToken.None);
					string text2 = tuple2.Item2["ctx"].ToString();
					offset = long.Parse(tuple2.Item2["offset"].ToString());
					tuple2.Item2["host"].ToString();
					state.completed += firstChunkBinary.Length;
					if (state.completed % BLOCKSIZE == 0L || state.completed == totalSize)
					{
						state.block_ctxes.Add(text2);
					}
					return UploadNextChunk(state, dataStream, text2, offset, progress);
				}).Unwrap();
			}
			byte[] chunkBinary = GetChunkBinary(state.completed, dataStream);
			return PutChunk(state, chunkBinary, context, offset).ContinueWith(delegate(Task<Tuple<HttpStatusCode, string>> t)
			{
				Tuple<HttpStatusCode, IDictionary<string, object>> tuple = AVClient.ReponseResolve(t.Result, CancellationToken.None);
				string text = tuple.Item2["ctx"].ToString();
				offset = long.Parse(tuple.Item2["offset"].ToString());
				tuple.Item2["host"].ToString();
				state.completed += chunkBinary.Length;
				if (state.completed % BLOCKSIZE == 0L || state.completed == totalSize)
				{
					state.block_ctxes.Add(text);
				}
				return UploadNextChunk(state, dataStream, text, offset, progress);
			}).Unwrap();
		}

		private byte[] GetChunkBinary(long completed, Stream dataStream)
		{
			long num = 1048576L;
			if (completed + num > dataStream.Length)
			{
				num = dataStream.Length - completed;
			}
			byte[] array = new byte[num];
			dataStream.Seek(completed, SeekOrigin.Begin);
			dataStream.Read(array, 0, (int)num);
			return array;
		}

		internal new string GetUniqueName(FileState state)
		{
			string str = Guid.NewGuid().ToString();
			string extension = Path.GetExtension(state.Name);
			return str + extension;
		}

		internal Task<Tuple<HttpStatusCode, IDictionary<string, object>>> GetQiniuToken(FileState state, CancellationToken cancellationToken)
		{
			string currentSessionToken = AVUser.CurrentSessionToken;
			string name = state.Name;
			IDictionary<string, object> dictionary = new Dictionary<string, object>();
			dictionary.Add("name", name);
			dictionary.Add("key", state.CloudName);
			dictionary.Add("__type", "File");
			dictionary.Add("mime_type", AVFile.GetMIMEType(name));
			state.MetaData = GetMetaData(state, state.frozenData);
			dictionary.Add("metaData", state.MetaData);
			return AVClient.RequestAsync("POST", new Uri("qiniu", UriKind.Relative), currentSessionToken, dictionary, cancellationToken);
		}

		private IList<KeyValuePair<string, string>> GetQiniuRequestHeaders(FileState state)
		{
			List<KeyValuePair<string, string>> list = new List<KeyValuePair<string, string>>();
			string value = "UpToken " + state.token;
			((ICollection<KeyValuePair<string, string>>)list).Add(new KeyValuePair<string, string>("Authorization", value));
			return list;
		}

		private Task<Tuple<HttpStatusCode, string>> MakeBlock(FileState state, byte[] firstChunkBinary, long blcokSize = 4194304L)
		{
			MemoryStream data = new MemoryStream(firstChunkBinary, 0, firstChunkBinary.Length);
			return AVClient.RequestAsync(new Uri(string.Concat(new Uri(UP_HOST), $"mkblk/{blcokSize}")), "POST", GetQiniuRequestHeaders(state), data, "application/octet-stream", CancellationToken.None);
		}

		private Task<Tuple<HttpStatusCode, string>> PutChunk(FileState state, byte[] chunkBinary, string LastChunkctx, long currentChunkOffsetInBlock)
		{
			MemoryStream data = new MemoryStream(chunkBinary, 0, chunkBinary.Length);
			return AVClient.RequestAsync(new Uri(string.Concat(new Uri(UP_HOST), $"bput/{LastChunkctx}/{currentChunkOffsetInBlock}")), "POST", GetQiniuRequestHeaders(state), data, "application/octet-stream", CancellationToken.None);
		}

		internal Task<Tuple<HttpStatusCode, string>> QiniuMakeFile(FileState state, Stream dataStream, string upToken, string key, long fsize, string[] ctxes, CancellationToken cancellationToken)
		{
			StringBuilder stringBuilder = new StringBuilder();
			stringBuilder.AppendFormat("{0}/mkfile/{1}", UP_HOST, fsize);
			if (key != null)
			{
				stringBuilder.AppendFormat("/key/{0}", ToBase64URLSafe(key));
			}
			IDictionary<string, object> metaData = GetMetaData(state, dataStream);
			StringBuilder stringBuilder2 = new StringBuilder();
			foreach (string key2 in metaData.Keys)
			{
				stringBuilder2.AppendFormat("/{0}/{1}", key2, ToBase64URLSafe(metaData[key2].ToString()));
			}
			stringBuilder.Append(stringBuilder2.ToString());
			IList<KeyValuePair<string, string>> list = new List<KeyValuePair<string, string>>();
			string value = "UpToken " + upToken;
			list.Add(new KeyValuePair<string, string>("Authorization", value));
			int num = ctxes.Length;
			Stream stream = new MemoryStream();
			for (int i = 0; i < num; i++)
			{
				byte[] array = StringToAscii(ctxes[i]);
				stream.Write(array, 0, array.Length);
				if (i != num - 1)
				{
					stream.WriteByte(44);
				}
			}
			stream.Seek(0L, SeekOrigin.Begin);
			return AVClient.RequestAsync(new Uri(stringBuilder.ToString()), "POST", list, stream, "text/plain", cancellationToken).OnSuccess(delegate(Task<Tuple<HttpStatusCode, string>> _)
			{
				AVClient.ReponseResolve(_.Result, CancellationToken.None);
				return _.Result;
			});
		}

		internal void MergeFromJSON(FileState state, IDictionary<string, object> jsonData)
		{
			lock (mutex)
			{
				string text = jsonData["url"] as string;
				state.Url = new Uri(text, UriKind.Absolute);
				state.bucketId = FetchBucketId(text);
				state.token = (jsonData["token"] as string);
				state.bucket = (jsonData["bucket"] as string);
				state.ObjectId = (jsonData["objectId"] as string);
			}
		}

		private string FetchBucketId(string url)
		{
			string[] array = url.Split('/');
			return array[array.Length - 1];
		}

		public static byte[] StringToAscii(string s)
		{
			byte[] array = new byte[s.Length];
			for (int i = 0; i < s.Length; i++)
			{
				char c = s[i];
				if (c <= '\u007f')
				{
					array[i] = (byte)c;
				}
				else
				{
					array[i] = 63;
				}
			}
			return array;
		}

		public static string ToBase64URLSafe(string str)
		{
			return Encode(str);
		}

		public static string Encode(byte[] bs)
		{
			if (bs == null || bs.Length == 0)
			{
				return "";
			}
			return Convert.ToBase64String(bs).Replace('+', '-').Replace('/', '_');
		}

		public static string Encode(string text)
		{
			if (string.IsNullOrEmpty(text))
			{
				return "";
			}
			return Convert.ToBase64String(Encoding.UTF8.GetBytes(text)).Replace('+', '-').Replace('/', '_');
		}

		internal static string GetMD5Code(Stream data)
		{
			byte[] array = new MD5CryptoServiceProvider().ComputeHash(data);
			StringBuilder stringBuilder = new StringBuilder();
			for (int i = 0; i < array.Length; i++)
			{
				stringBuilder.Append(array[i].ToString("x2"));
			}
			return stringBuilder.ToString();
		}

		internal IDictionary<string, object> GetMetaData(FileState state, Stream data)
		{
			IDictionary<string, object> dictionary = new Dictionary<string, object>();
			if (state.MetaData != null)
			{
				foreach (KeyValuePair<string, object> metaDatum in state.MetaData)
				{
					dictionary.Add(metaDatum.Key, metaDatum.Value);
				}
			}
			MergeDic(dictionary, "mime_type", AVFile.GetMIMEType(state.Name));
			MergeDic(dictionary, "size", data.Length);
			MergeDic(dictionary, "_checksum", GetMD5Code(data));
			if (AVUser.CurrentUser != null && AVUser.CurrentUser.ObjectId != null)
			{
				MergeDic(dictionary, "owner", AVUser.CurrentUser.ObjectId);
			}
			return dictionary;
		}

		internal void MergeDic(IDictionary<string, object> dic, string key, object value)
		{
			if (dic.ContainsKey(key))
			{
				dic[key] = value;
			}
			else
			{
				dic.Add(key, value);
			}
		}
	}
}
