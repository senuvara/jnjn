using System;
using System.Collections.Generic;

namespace LeanCloud.Core.Internal
{
	internal static class ParseFieldOperations
	{
		private static AVObjectIdComparer comparer;

		public static IEqualityComparer<object> ParseObjectComparer
		{
			get
			{
				if (comparer == null)
				{
					comparer = new AVObjectIdComparer();
				}
				return comparer;
			}
		}

		public static IAVFieldOperation Decode(IDictionary<string, object> json)
		{
			throw new NotImplementedException();
		}
	}
}
