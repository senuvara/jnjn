using LeanCloud.Storage.Internal;
using System;
using System.Collections.Generic;
using System.IO;
using System.Net;
using System.Threading;
using System.Threading.Tasks;

namespace LeanCloud.Core.Internal
{
	internal class AWSS3FileController : AVFileController
	{
		private object mutex = new object();

		public AWSS3FileController(IAVCommandRunner commandRunner)
			: base(commandRunner)
		{
		}

		public override Task<FileState> SaveAsync(FileState state, Stream dataStream, string sessionToken, IProgress<AVUploadProgressEventArgs> progress, CancellationToken cancellationToken = default(CancellationToken))
		{
			if (state.Url != null)
			{
				return Task.FromResult(state);
			}
			return AVFileController.GetFileToken(state, cancellationToken).OnSuccess(delegate(Task<Tuple<HttpStatusCode, IDictionary<string, object>>> t)
			{
				IDictionary<string, object> item = t.Result.Item2;
				string uploadUrl = item["upload_url"].ToString();
				state.ObjectId = item["objectId"].ToString();
				string uriString = item["url"] as string;
				state.Url = new Uri(uriString, UriKind.Absolute);
				return PutFile(state, uploadUrl, dataStream);
			}).Unwrap()
				.OnSuccess((Task<FileState> s) => s.Result);
		}

		internal Task<FileState> PutFile(FileState state, string uploadUrl, Stream dataStream)
		{
			IList<KeyValuePair<string, string>> list = new List<KeyValuePair<string, string>>();
			list.Add(new KeyValuePair<string, string>("Content-Type", state.MimeType));
			return AVClient.RequestAsync(new Uri(uploadUrl), "PUT", list, dataStream, state.MimeType, CancellationToken.None).OnSuccess((Task<Tuple<HttpStatusCode, string>> t) => state);
		}
	}
}
