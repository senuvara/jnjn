using LeanCloud.Storage.Internal;
using System;
using System.Collections.Generic;
using System.Net;
using System.Threading;
using System.Threading.Tasks;

namespace LeanCloud.Core.Internal
{
	public class AVCommandRunner : IAVCommandRunner
	{
		private readonly IHttpClient httpClient;

		private readonly IInstallationIdController installationIdController;

		private const string revocableSessionTokenTrueValue = "1";

		public AVCommandRunner(IHttpClient httpClient, IInstallationIdController installationIdController)
		{
			this.httpClient = httpClient;
			this.installationIdController = installationIdController;
		}

		public Task<Tuple<HttpStatusCode, IDictionary<string, object>>> RunCommandAsync(AVCommand command, IProgress<AVUploadProgressEventArgs> uploadProgress = null, IProgress<AVDownloadProgressEventArgs> downloadProgress = null, CancellationToken cancellationToken = default(CancellationToken))
		{
			return PrepareCommand(command).ContinueWith(delegate(Task<AVCommand> commandTask)
			{
				string str = commandTask.Result.ToLog();
				AVClient.PrintLog("http=>" + str);
				return httpClient.ExecuteAsync(commandTask.Result, uploadProgress, downloadProgress, cancellationToken).OnSuccess(delegate(Task<Tuple<HttpStatusCode, string>> t)
				{
					cancellationToken.ThrowIfCancellationRequested();
					Tuple<HttpStatusCode, string> result = t.Result;
					string item = result.Item2;
					int item2 = (int)result.Item1;
					string str2 = item2 + ";" + item;
					AVClient.PrintLog("http<=" + str2);
					if (item2 >= 500)
					{
						throw new AVException(AVException.ErrorCode.InternalServerError, result.Item2);
					}
					if (item != null)
					{
						IDictionary<string, object> dictionary = null;
						try
						{
							if (item.StartsWith("["))
							{
								object value = Json.Parse(item);
								dictionary = new Dictionary<string, object>
								{
									{
										"results",
										value
									}
								};
							}
							else
							{
								dictionary = (Json.Parse(item) as IDictionary<string, object>);
							}
						}
						catch (Exception cause)
						{
							throw new AVException(AVException.ErrorCode.OtherCause, "Invalid response from server", cause);
						}
						if (item2 < 200 || item2 > 299)
						{
							AVClient.PrintLog("error response code:" + item2);
							int code = dictionary.ContainsKey("code") ? ((int)dictionary["code"]) : (-1);
							string message = dictionary.ContainsKey("error") ? (dictionary["error"] as string) : item;
							throw new AVException((AVException.ErrorCode)code, message);
						}
						return new Tuple<HttpStatusCode, IDictionary<string, object>>(result.Item1, dictionary);
					}
					return new Tuple<HttpStatusCode, IDictionary<string, object>>(result.Item1, null);
				});
			}).Unwrap();
		}

		private Task<AVCommand> PrepareCommand(AVCommand command)
		{
			AVCommand newCommand = new AVCommand(command);
			Task<AVCommand> result = installationIdController.GetAsync().ContinueWith(delegate(Task<Guid?> t)
			{
				newCommand.Headers.Add(new KeyValuePair<string, string>("X-LC-Installation-Id", t.Result.ToString()));
				return newCommand;
			});
			AVClient.Configuration currentConfiguration = AVClient.CurrentConfiguration;
			newCommand.Headers.Add(new KeyValuePair<string, string>("X-LC-Id", currentConfiguration.ApplicationId));
			newCommand.Headers.Add(new KeyValuePair<string, string>("X-LC-Client-Version", AVClient.VersionString));
			if (currentConfiguration.AdditionalHTTPHeaders != null)
			{
				foreach (KeyValuePair<string, string> additionalHTTPHeader in currentConfiguration.AdditionalHTTPHeaders)
				{
					newCommand.Headers.Add(additionalHTTPHeader);
				}
			}
			if (!string.IsNullOrEmpty(currentConfiguration.VersionInfo.BuildVersion))
			{
				newCommand.Headers.Add(new KeyValuePair<string, string>("X-LC-App-Build-Version", currentConfiguration.VersionInfo.BuildVersion));
			}
			if (!string.IsNullOrEmpty(currentConfiguration.VersionInfo.DisplayVersion))
			{
				newCommand.Headers.Add(new KeyValuePair<string, string>("X-LC-App-Display-Version", currentConfiguration.VersionInfo.DisplayVersion));
			}
			if (!string.IsNullOrEmpty(currentConfiguration.VersionInfo.OSVersion))
			{
				newCommand.Headers.Add(new KeyValuePair<string, string>("X-LC-OS-Version", currentConfiguration.VersionInfo.OSVersion));
			}
			if (!string.IsNullOrEmpty(AVClient.MasterKey))
			{
				newCommand.Headers.Add(new KeyValuePair<string, string>("X-LC-Key", AVClient.MasterKey + ",master"));
			}
			else
			{
				newCommand.Headers.Add(new KeyValuePair<string, string>("X-LC-Key", currentConfiguration.ApplicationKey));
			}
			if (AVUser.IsRevocableSessionEnabled)
			{
				newCommand.Headers.Add(new KeyValuePair<string, string>("X-LeanCloud-Revocable-Session", "1"));
			}
			return result;
		}
	}
}
