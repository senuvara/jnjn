using LeanCloud.Storage.Internal;
using System;
using System.Collections.Generic;
using System.IO;
using System.Linq;
using System.Net;
using System.Threading;
using System.Threading.Tasks;

namespace LeanCloud.Core.Internal
{
	public class AVFileController : IAVFileController
	{
		private readonly IAVCommandRunner commandRunner;

		public AVFileController(IAVCommandRunner commandRunner)
		{
			this.commandRunner = commandRunner;
		}

		public virtual Task<FileState> SaveAsync(FileState state, Stream dataStream, string sessionToken, IProgress<AVUploadProgressEventArgs> progress, CancellationToken cancellationToken = default(CancellationToken))
		{
			if (state.Url != null)
			{
				return Task.FromResult(state);
			}
			if (cancellationToken.IsCancellationRequested)
			{
				TaskCompletionSource<FileState> taskCompletionSource = new TaskCompletionSource<FileState>();
				taskCompletionSource.TrySetCanceled();
				return taskCompletionSource.Task;
			}
			long oldPosition = dataStream.Position;
			AVCommand command = new AVCommand("files/" + state.Name, "POST", sessionToken, null, contentType: state.MimeType, stream: dataStream);
			return commandRunner.RunCommandAsync(command, progress, null, cancellationToken).OnSuccess(delegate(Task<Tuple<HttpStatusCode, IDictionary<string, object>>> uploadTask)
			{
				IDictionary<string, object> item = uploadTask.Result.Item2;
				cancellationToken.ThrowIfCancellationRequested();
				return new FileState
				{
					Name = (item["name"] as string),
					Url = new Uri(item["url"] as string, UriKind.Absolute),
					MimeType = state.MimeType
				};
			}).ContinueWith(delegate(Task<FileState> t)
			{
				if ((t.IsFaulted || t.IsCanceled) && dataStream.CanSeek)
				{
					dataStream.Seek(oldPosition, SeekOrigin.Begin);
				}
				return t;
			})
				.Unwrap();
		}

		public Task DeleteAsync(FileState state, string sessionToken, CancellationToken cancellationToken)
		{
			AVCommand command = new AVCommand("files/" + state.ObjectId, "DELETE", sessionToken, (IList<KeyValuePair<string, string>>)null, (IDictionary<string, object>)null);
			return commandRunner.RunCommandAsync(command, null, null, cancellationToken);
		}

		internal static Task<Tuple<HttpStatusCode, IDictionary<string, object>>> GetFileToken(FileState fileState, CancellationToken cancellationToken)
		{
			string currentSessionToken = AVUser.CurrentSessionToken;
			string name = fileState.Name;
			IDictionary<string, object> dictionary = new Dictionary<string, object>();
			dictionary.Add("name", name);
			dictionary.Add("key", GetUniqueName(fileState));
			dictionary.Add("__type", "File");
			dictionary.Add("mime_type", AVFile.GetMIMEType(name));
			dictionary.Add("metaData", fileState.MetaData);
			return AVClient.RequestAsync("POST", new Uri("fileTokens", UriKind.Relative), currentSessionToken, dictionary, cancellationToken);
		}

		public Task<FileState> GetAsync(string objectId, string sessionToken, CancellationToken cancellationToken)
		{
			AVCommand command = new AVCommand("files/" + objectId, "GET", sessionToken, (IList<KeyValuePair<string, string>>)null, (IDictionary<string, object>)null);
			return commandRunner.RunCommandAsync(command, null, null, cancellationToken).OnSuccess(delegate(Task<Tuple<HttpStatusCode, IDictionary<string, object>>> _)
			{
				IDictionary<string, object> item = _.Result.Item2;
				cancellationToken.ThrowIfCancellationRequested();
				return new FileState
				{
					ObjectId = (item["objectId"] as string),
					Name = (item["name"] as string),
					Url = new Uri(item["url"] as string, UriKind.Absolute)
				};
			});
		}

		internal static string GetUniqueName(FileState fileState)
		{
			string str = Random(12);
			string extension = Path.GetExtension(fileState.Name);
			return fileState.CloudName = str + extension;
		}

		internal static string Random(int length)
		{
			Random random = new Random();
			return new string((from s in Enumerable.Repeat("ABCDEFGHIJKLMNOPQRSTUVWXYZ0123456789abcdefghijklmnopqrstuvwxyz", length)
				select s[random.Next(s.Length)]).ToArray());
		}

		internal static double CalcProgress(double already, double total)
		{
			return Math.Round(1.0 * already / total, 3);
		}
	}
}
