using System;
using System.Collections.Generic;
using System.Threading;
using System.Threading.Tasks;

namespace LeanCloud.Core.Internal
{
	public static class AVUserExtensions
	{
		public static IDictionary<string, IDictionary<string, object>> GetAuthData(this AVUser user)
		{
			return user.AuthData;
		}

		public static Task<AVUser> LogInWithAsync(string authType, CancellationToken cancellationToken)
		{
			return AVUser.LogInWithAsync(authType, cancellationToken);
		}

		public static Task<AVUser> LogInWithAuthDataAsync(IDictionary<string, object> data, string platform, CancellationToken cancellationToken = default(CancellationToken))
		{
			return AVUser.LogInWithAsync(platform, data, cancellationToken);
		}

		[Obsolete("please use LogInWithAuthDataAsync instead.")]
		public static Task<AVUser> LogInWithAsync(string authType, IDictionary<string, object> data, CancellationToken cancellationToken)
		{
			return AVUser.LogInWithAsync(authType, data, cancellationToken);
		}

		public static Task AssociateAuthData(this AVUser user, IDictionary<string, object> data, string platform, CancellationToken cancellationToken = default(CancellationToken))
		{
			return user.LinkWithAsync(platform, data, cancellationToken);
		}

		[Obsolete("please use AssociateAuthData instead.")]
		public static Task LinkWithAsync(this AVUser user, string authType, CancellationToken cancellationToken)
		{
			return user.LinkWithAsync(authType, cancellationToken);
		}

		[Obsolete("please use AssociateAuthData instead.")]
		public static Task LinkWithAsync(this AVUser user, string authType, IDictionary<string, object> data, CancellationToken cancellationToken)
		{
			return user.LinkWithAsync(authType, data, cancellationToken);
		}

		public static Task DisassociateWithAuthData(this AVUser user, string platform, CancellationToken cancellationToken = default(CancellationToken))
		{
			return user.UnlinkFromAsync(platform, cancellationToken);
		}

		[Obsolete("please use DisassociateWithAuthData instead.")]
		public static Task UnlinkFromAsync(this AVUser user, string authType, CancellationToken cancellationToken)
		{
			return user.UnlinkFromAsync(authType, cancellationToken);
		}

		public static Task UpgradeToRevocableSessionAsync(this AVUser user, CancellationToken cancellationToken)
		{
			return user.UpgradeToRevocableSessionAsync(cancellationToken);
		}
	}
}
