using System;
using System.Collections.Generic;

namespace LeanCloud.Core.Internal
{
	public class PointerOrLocalIdEncoder : AVEncoder
	{
		private static readonly PointerOrLocalIdEncoder instance = new PointerOrLocalIdEncoder();

		public static PointerOrLocalIdEncoder Instance => instance;

		protected override IDictionary<string, object> EncodeParseObject(AVObject value)
		{
			if (value.ObjectId == null)
			{
				throw new ArgumentException("Cannot create a pointer to an object without an objectId");
			}
			return new Dictionary<string, object>
			{
				{
					"__type",
					"Pointer"
				},
				{
					"className",
					value.ClassName
				},
				{
					"objectId",
					value.ObjectId
				}
			};
		}
	}
}
