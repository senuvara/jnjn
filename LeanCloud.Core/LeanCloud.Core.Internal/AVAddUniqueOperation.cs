using LeanCloud.Utilities;
using System;
using System.Collections.Generic;
using System.Collections.ObjectModel;
using System.Linq;

namespace LeanCloud.Core.Internal
{
	public class AVAddUniqueOperation : IAVFieldOperation
	{
		private ReadOnlyCollection<object> objects;

		public IEnumerable<object> Objects => objects;

		public AVAddUniqueOperation(IEnumerable<object> objects)
		{
			this.objects = new ReadOnlyCollection<object>(objects.Distinct().ToList());
		}

		public object Encode()
		{
			return new Dictionary<string, object>
			{
				{
					"__op",
					"AddUnique"
				},
				{
					"objects",
					PointerOrLocalIdEncoder.Instance.Encode(objects)
				}
			};
		}

		public IAVFieldOperation MergeWithPrevious(IAVFieldOperation previous)
		{
			if (previous == null)
			{
				return this;
			}
			if (previous is AVDeleteOperation)
			{
				return new AVSetOperation(objects.ToList());
			}
			if (previous is AVSetOperation)
			{
				IList<object> oldValue = Conversion.To<IList<object>>(((AVSetOperation)previous).Value);
				return new AVSetOperation(Apply(oldValue, null));
			}
			if (previous is AVAddUniqueOperation)
			{
				IEnumerable<object> oldValue2 = ((AVAddUniqueOperation)previous).Objects;
				return new AVAddUniqueOperation((IList<object>)Apply(oldValue2, null));
			}
			throw new InvalidOperationException("Operation is invalid after previous operation.");
		}

		public object Apply(object oldValue, string key)
		{
			if (oldValue == null)
			{
				return objects.ToList();
			}
			List<object> list = Conversion.To<IList<object>>(oldValue).ToList();
			IEqualityComparer<object> comparer = ParseFieldOperations.ParseObjectComparer;
			foreach (object objToAdd in objects)
			{
				if (objToAdd is AVObject)
				{
					object obj = list.FirstOrDefault((object listObj) => comparer.Equals(objToAdd, listObj));
					if (obj == null)
					{
						list.Add(objToAdd);
					}
					else
					{
						int index = list.IndexOf(obj);
						list[index] = objToAdd;
					}
				}
				else if (!list.Contains(objToAdd, comparer))
				{
					list.Add(objToAdd);
				}
			}
			return list;
		}
	}
}
