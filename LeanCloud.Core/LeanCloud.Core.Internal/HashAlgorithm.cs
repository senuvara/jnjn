using System;
using System.IO;

namespace LeanCloud.Core.Internal
{
	public abstract class HashAlgorithm : IDisposable
	{
		protected int HashSizeValue;

		protected internal byte[] HashValue;

		protected int State;

		private bool m_bDisposed;

		public virtual int HashSize => HashSizeValue;

		public virtual int InputBlockSize => 1;

		public virtual int OutputBlockSize => 1;

		public virtual bool CanTransformMultipleBlocks => true;

		public virtual bool CanReuseTransform => true;

		public byte[] ComputeHash(Stream inputStream)
		{
			if (m_bDisposed)
			{
				throw new ObjectDisposedException(null);
			}
			byte[] array = new byte[4096];
			int num;
			do
			{
				num = inputStream.Read(array, 0, 4096);
				if (num > 0)
				{
					HashCore(array, 0, num);
				}
			}
			while (num > 0);
			HashValue = HashFinal();
			byte[] result = (byte[])HashValue.Clone();
			Initialize();
			return result;
		}

		public byte[] ComputeHash(byte[] buffer)
		{
			if (m_bDisposed)
			{
				throw new ObjectDisposedException(null);
			}
			if (buffer == null)
			{
				throw new ArgumentNullException("buffer");
			}
			HashCore(buffer, 0, buffer.Length);
			HashValue = HashFinal();
			byte[] result = (byte[])HashValue.Clone();
			Initialize();
			return result;
		}

		public void Dispose()
		{
			Dispose(disposing: true);
			GC.SuppressFinalize(this);
		}

		public void Clear()
		{
			((IDisposable)this).Dispose();
		}

		protected virtual void Dispose(bool disposing)
		{
			if (disposing)
			{
				if (HashValue != null)
				{
					Array.Clear(HashValue, 0, HashValue.Length);
				}
				HashValue = null;
				m_bDisposed = true;
			}
		}

		public abstract void Initialize();

		protected abstract void HashCore(byte[] array, int ibStart, int cbSize);

		protected abstract byte[] HashFinal();
	}
}
