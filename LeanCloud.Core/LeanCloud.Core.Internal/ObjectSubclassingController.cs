using LeanCloud.Storage.Internal;
using System;
using System.Collections.Generic;
using System.Reflection;
using System.Threading;

namespace LeanCloud.Core.Internal
{
	internal class ObjectSubclassingController : IObjectSubclassingController
	{
		private static readonly string parseObjectClassName = "_ParseObject";

		private readonly ReaderWriterLockSlim mutex;

		private readonly IDictionary<string, ObjectSubclassInfo> registeredSubclasses;

		private Dictionary<string, Action> registerActions;

		public ObjectSubclassingController()
		{
			mutex = new ReaderWriterLockSlim();
			registeredSubclasses = new Dictionary<string, ObjectSubclassInfo>();
			registerActions = new Dictionary<string, Action>();
			RegisterSubclass(typeof(AVObject));
		}

		public string GetClassName(Type type)
		{
			if (type != typeof(AVObject))
			{
				return ObjectSubclassInfo.GetClassName(type.GetTypeInfo());
			}
			return parseObjectClassName;
		}

		public Type GetType(string className)
		{
			ObjectSubclassInfo value = null;
			mutex.EnterReadLock();
			registeredSubclasses.TryGetValue(className, out value);
			mutex.ExitReadLock();
			return value?.TypeInfo.AsType();
		}

		public bool IsTypeValid(string className, Type type)
		{
			ObjectSubclassInfo value = null;
			mutex.EnterReadLock();
			registeredSubclasses.TryGetValue(className, out value);
			mutex.ExitReadLock();
			if (value != null)
			{
				return value.TypeInfo == type.GetTypeInfo();
			}
			return type == typeof(AVObject);
		}

		public void RegisterSubclass(Type type)
		{
			TypeInfo typeInfo = type.GetTypeInfo();
			if (!typeof(AVObject).GetTypeInfo().IsAssignableFrom(typeInfo))
			{
				throw new ArgumentException("Cannot register a type that is not a subclass of AVObject");
			}
			string className = GetClassName(type);
			try
			{
				mutex.EnterWriteLock();
				ObjectSubclassInfo value = null;
				if (registeredSubclasses.TryGetValue(className, out value))
				{
					if (typeInfo.IsAssignableFrom(value.TypeInfo))
					{
						return;
					}
					if (!value.TypeInfo.IsAssignableFrom(typeInfo))
					{
						throw new ArgumentException("Tried to register both " + value.TypeInfo.FullName + " and " + typeInfo.FullName + " as the AVObject subclass of " + className + ". Cannot determine the right class to use because neither inherits from the other.");
					}
				}
				ConstructorInfo constructorInfo = type.FindConstructor();
				if (constructorInfo == null)
				{
					throw new ArgumentException("Cannot register a type that does not implement the default constructor!");
				}
				registeredSubclasses[className] = new ObjectSubclassInfo(type, constructorInfo);
			}
			finally
			{
				mutex.ExitWriteLock();
			}
			mutex.EnterReadLock();
			registerActions.TryGetValue(className, out Action value2);
			mutex.ExitReadLock();
			value2?.Invoke();
		}

		public void UnregisterSubclass(Type type)
		{
			mutex.EnterWriteLock();
			registeredSubclasses.Remove(GetClassName(type));
			mutex.ExitWriteLock();
		}

		public void AddRegisterHook(Type t, Action action)
		{
			mutex.EnterWriteLock();
			registerActions.Add(GetClassName(t), action);
			mutex.ExitWriteLock();
		}

		public AVObject Instantiate(string className)
		{
			ObjectSubclassInfo value = null;
			mutex.EnterReadLock();
			registeredSubclasses.TryGetValue(className, out value);
			mutex.ExitReadLock();
			if (value == null)
			{
				return new AVObject(className);
			}
			return value.Instantiate();
		}

		public IDictionary<string, string> GetPropertyMappings(string className)
		{
			ObjectSubclassInfo value = null;
			mutex.EnterReadLock();
			registeredSubclasses.TryGetValue(className, out value);
			if (value == null)
			{
				registeredSubclasses.TryGetValue(parseObjectClassName, out value);
			}
			mutex.ExitReadLock();
			return value.PropertyMappings;
		}
	}
}
