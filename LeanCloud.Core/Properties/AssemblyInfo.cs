using System.Diagnostics;
using System.Reflection;
using System.Runtime.CompilerServices;
using System.Runtime.InteropServices;

[assembly: CompilationRelaxations(8)]
[assembly: RuntimeCompatibility(WrapNonExceptionThrows = true)]
[assembly: Debuggable(DebuggableAttribute.DebuggingModes.IgnoreSymbolStoreSequencePoints)]
[assembly: AssemblyTitle("LeanCloud.Core")]
[assembly: AssemblyDescription("Makes accessing services from LeanCloud native and straightforward.")]
[assembly: AssemblyConfiguration("")]
[assembly: AssemblyCompany("")]
[assembly: AssemblyProduct("LeanCloud")]
[assembly: AssemblyCopyright("Copyright © LeanCloud 2013-2018")]
[assembly: AssemblyTrademark("")]
[assembly: InternalsVisibleTo("LeanCloud.Realtime")]
[assembly: InternalsVisibleTo("LeanCloud.Play")]
[assembly: InternalsVisibleTo("LeanCloud.LiveQuery")]
[assembly: ComVisible(true)]
[assembly: AssemblyFileVersion("2.0.0.0")]
[assembly: AssemblyVersion("2.0.0.0")]
