using LeanCloud.Core.Internal;
using LeanCloud.Storage.Internal;
using System;
using System.Collections.Generic;
using System.IO;
using System.Linq;
using System.Net;
using System.Reflection;
using System.Text;
using System.Threading;
using System.Threading.Tasks;

namespace LeanCloud
{
	public static class AVClient
	{
		public struct Configuration
		{
			public enum AVRegion
			{
				Public_CN,
				Public_US,
				Vendor_Tencent
			}

			public struct VersionInformation
			{
				public string BuildVersion
				{
					get;
					set;
				}

				public string DisplayVersion
				{
					get;
					set;
				}

				public string OSVersion
				{
					get;
					set;
				}
			}

			public string ApplicationId
			{
				get;
				set;
			}

			public AVRegion Region
			{
				get;
				set;
			}

			public string ApplicationKey
			{
				get;
				set;
			}

			public IDictionary<string, string> AdditionalHTTPHeaders
			{
				get;
				set;
			}

			public VersionInformation VersionInfo
			{
				get;
				set;
			}

			public Uri EngineServer
			{
				get;
				set;
			}

			public Uri ApiServer
			{
				get;
				set;
			}

			public Uri PushServer
			{
				get;
				set;
			}

			public Uri StatsServer
			{
				get;
				set;
			}
		}

		internal static readonly string[] DateFormatStrings;

		private static readonly object mutex;

		private static readonly string versionString;

		private static bool useProduction;

		public static Configuration CurrentConfiguration
		{
			get;
			internal set;
		}

		internal static string MasterKey
		{
			get;
			set;
		}

		internal static Version Version => new AssemblyName(typeof(AVClient).GetTypeInfo().Assembly.FullName).Version;

		public static string VersionString => versionString;

		internal static Action<string> LogTracker
		{
			get;
			private set;
		}

		public static bool UseProduction
		{
			get
			{
				return useProduction;
			}
			set
			{
				useProduction = value;
			}
		}

		static AVClient()
		{
			DateFormatStrings = new string[3]
			{
				"yyyy'-'MM'-'dd'T'HH':'mm':'ss'.'fff'Z'",
				"yyyy'-'MM'-'dd'T'HH':'mm':'ss'.'ff'Z'",
				"yyyy'-'MM'-'dd'T'HH':'mm':'ss'.'f'Z'"
			};
			mutex = new object();
			useProduction = true;
			versionString = "net-portable-" + Version;
			AVModuleController.Instance.ScanForModules();
		}

		public static void Initialize(string applicationId, string applicationKey)
		{
			Configuration configuration = default(Configuration);
			configuration.ApplicationId = applicationId;
			configuration.ApplicationKey = applicationKey;
			Initialize(configuration);
		}

		public static void HttpLog(Action<string> trace)
		{
			LogTracker = trace;
		}

		public static void PrintLog(string log)
		{
			if (LogTracker != null)
			{
				LogTracker(log);
			}
		}

		public static void Initialize(Configuration configuration)
		{
			lock (mutex)
			{
				string[] array = configuration.ApplicationId.Split('-');
				if (array.Length > 1 && array[1].Trim() == "9Nh9j0Va")
				{
					configuration.Region = Configuration.AVRegion.Vendor_Tencent;
				}
				CurrentConfiguration = configuration;
				AVObject.RegisterSubclass<AVUser>();
				AVObject.RegisterSubclass<AVRole>();
				AVObject.RegisterSubclass<AVSession>();
				AVModuleController.Instance.LeanCloudDidInitialize();
			}
		}

		internal static string BuildQueryString(IDictionary<string, object> parameters)
		{
			return string.Join("&", (from pair in parameters
				let valueString = pair.Value as string
				select $"{Uri.EscapeDataString(pair.Key)}={Uri.EscapeDataString(string.IsNullOrEmpty(valueString) ? Json.Encode(pair.Value) : valueString)}").ToArray());
		}

		internal static IDictionary<string, string> DecodeQueryString(string queryString)
		{
			Dictionary<string, string> dictionary = new Dictionary<string, string>();
			string[] array = queryString.Split('&');
			for (int i = 0; i < array.Length; i++)
			{
				string[] array2 = array[i].Split(new char[1]
				{
					'='
				}, 2);
				dictionary[array2[0]] = ((array2.Length == 2) ? Uri.UnescapeDataString(array2[1].Replace("+", " ")) : null);
			}
			return dictionary;
		}

		internal static IDictionary<string, object> DeserializeJsonString(string jsonData)
		{
			return Json.Parse(jsonData) as IDictionary<string, object>;
		}

		internal static string SerializeJsonString(IDictionary<string, object> jsonData)
		{
			return Json.Encode(jsonData);
		}

		internal static Task<Tuple<HttpStatusCode, string>> RequestAsync(Uri uri, string method, IList<KeyValuePair<string, string>> headers, IDictionary<string, object> body, string contentType, CancellationToken cancellationToken)
		{
			MemoryStream data = (body != null) ? new MemoryStream(Encoding.UTF8.GetBytes(Json.Encode(body))) : null;
			return RequestAsync(uri, method, headers, data, contentType, cancellationToken);
		}

		public static Task<Tuple<HttpStatusCode, string>> RequestAsync(Uri uri, string method, IList<KeyValuePair<string, string>> headers, Stream data, string contentType, CancellationToken cancellationToken)
		{
			HttpRequest httpRequest = new HttpRequest
			{
				Data = ((data != null) ? data : null),
				Headers = headers,
				Method = method,
				Uri = uri
			};
			return AVPlugins.Instance.HttpClient.ExecuteAsync(httpRequest, null, null, cancellationToken);
		}

		internal static Tuple<HttpStatusCode, IDictionary<string, object>> ReponseResolve(Tuple<HttpStatusCode, string> response, CancellationToken cancellationToken)
		{
			HttpStatusCode item = response.Item1;
			string item2 = response.Item2;
			if (item2 == null)
			{
				cancellationToken.ThrowIfCancellationRequested();
				return new Tuple<HttpStatusCode, IDictionary<string, object>>(item, null);
			}
			IDictionary<string, object> dictionary = null;
			try
			{
				IDictionary<string, object> dictionary3;
				if (item2.StartsWith("["))
				{
					IDictionary<string, object> dictionary2 = new Dictionary<string, object>
					{
						{
							"results",
							Json.Parse(item2)
						}
					};
					dictionary3 = dictionary2;
				}
				else
				{
					dictionary3 = DeserializeJsonString(item2);
				}
				dictionary = dictionary3;
			}
			catch (Exception cause)
			{
				throw new AVException(AVException.ErrorCode.OtherCause, "Invalid response from server", cause);
			}
			int num = (int)item;
			if (num > 203 || num < 200)
			{
				throw new AVException((AVException.ErrorCode)(dictionary.ContainsKey("code") ? ((long)dictionary["code"]) : (-1)), dictionary.ContainsKey("error") ? (dictionary["error"] as string) : item2);
			}
			cancellationToken.ThrowIfCancellationRequested();
			return new Tuple<HttpStatusCode, IDictionary<string, object>>(item, dictionary);
		}

		internal static Task<Tuple<HttpStatusCode, IDictionary<string, object>>> RequestAsync(string method, Uri relativeUri, string sessionToken, IDictionary<string, object> data, CancellationToken cancellationToken)
		{
			AVCommand command = new AVCommand(relativeUri.ToString(), method, sessionToken, null, data);
			return AVPlugins.Instance.CommandRunner.RunCommandAsync(command, null, null, cancellationToken);
		}

		internal static Task<Tuple<HttpStatusCode, IDictionary<string, object>>> RunCommandAsync(AVCommand command)
		{
			return AVPlugins.Instance.CommandRunner.RunCommandAsync(command);
		}

		internal static bool IsSuccessStatusCode(HttpStatusCode responseStatus)
		{
			if (responseStatus > (HttpStatusCode)199)
			{
				return responseStatus < HttpStatusCode.NoContent;
			}
			return false;
		}
	}
}
