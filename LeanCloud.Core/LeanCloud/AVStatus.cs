using System.Collections.Generic;

namespace LeanCloud
{
	[AVClassName("_Status")]
	public class AVStatus : AVObject
	{
		private static readonly HashSet<string> readOnlyKeys = new HashSet<string>
		{
			"messageId",
			"inboxType",
			"data",
			"Source"
		};

		protected override bool IsKeyMutable(string key)
		{
			return !readOnlyKeys.Contains(key);
		}
	}
}
