namespace LeanCloud
{
	public sealed class AVRelation<T> : AVRelationBase where T : AVObject
	{
		public AVQuery<T> Query => GetQuery<T>();

		internal AVRelation(AVObject parent, string key)
			: base(parent, key)
		{
		}

		internal AVRelation(AVObject parent, string key, string targetClassName)
			: base(parent, key, targetClassName)
		{
		}

		public void Add(T obj)
		{
			Add((AVObject)obj);
		}

		public void Remove(T obj)
		{
			Remove((AVObject)obj);
		}
	}
}
