using System.Collections.Generic;
using System.Threading.Tasks;

namespace LeanCloud
{
	public interface IAVQueryTuple<S, T> where T : IAVObject
	{
		Task<IEnumerable<T>> FindAsync();

		Task<int> CountAsync();

		Task<T> GetAsync(string objectId);

		Task<T> FirstOrDefaultAsync();
	}
}
