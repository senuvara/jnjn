using LeanCloud.Storage.Internal;
using System.Collections.Generic;
using System.Linq;
using System.Threading;
using System.Threading.Tasks;

namespace LeanCloud
{
	public static class AVExtensions
	{
		public static Task SaveAllAsync<T>(this IEnumerable<T> objects) where T : AVObject
		{
			return AVObject.SaveAllAsync(objects);
		}

		public static Task SaveAllAsync<T>(this IEnumerable<T> objects, CancellationToken cancellationToken) where T : AVObject
		{
			return AVObject.SaveAllAsync(objects, cancellationToken);
		}

		public static Task<IEnumerable<T>> FetchAllAsync<T>(this IEnumerable<T> objects) where T : AVObject
		{
			return AVObject.FetchAllAsync(objects);
		}

		public static Task<IEnumerable<T>> FetchAllAsync<T>(this IEnumerable<T> objects, CancellationToken cancellationToken) where T : AVObject
		{
			return AVObject.FetchAllAsync(objects, cancellationToken);
		}

		public static Task<IEnumerable<T>> FetchAllIfNeededAsync<T>(this IEnumerable<T> objects) where T : AVObject
		{
			return AVObject.FetchAllIfNeededAsync(objects);
		}

		public static Task<IEnumerable<T>> FetchAllIfNeededAsync<T>(this IEnumerable<T> objects, CancellationToken cancellationToken) where T : AVObject
		{
			return AVObject.FetchAllIfNeededAsync(objects, cancellationToken);
		}

		public static AVQuery<T> Or<T>(this AVQuery<T> source, params AVQuery<T>[] queries) where T : AVObject
		{
			return AVQuery<T>.Or(queries.Concat(new AVQuery<T>[1]
			{
				source
			}));
		}

		public static Task<T> FetchAsync<T>(this T obj) where T : AVObject
		{
			return obj.FetchAsyncInternal(CancellationToken.None).OnSuccess((Task<AVObject> t) => (T)t.Result);
		}

		public static Task<T> FetchAsync<T>(this T obj, CancellationToken cancellationToken) where T : AVObject
		{
			return obj.FetchAsync(null, cancellationToken);
		}

		public static Task<T> FetchAsync<T>(this T obj, IEnumerable<string> includeKeys) where T : AVObject
		{
			return obj.FetchAsync(includeKeys, CancellationToken.None).OnSuccess((Task<T> t) => t.Result);
		}

		public static Task<T> FetchAsync<T>(this T obj, IEnumerable<string> includeKeys, CancellationToken cancellationToken) where T : AVObject
		{
			Dictionary<string, object> dictionary = new Dictionary<string, object>();
			if (includeKeys != null)
			{
				string value = string.Join(",", includeKeys.ToArray());
				dictionary.Add("include", value);
			}
			return obj.FetchAsyncInternal(dictionary, cancellationToken).OnSuccess((Task<AVObject> t) => (T)t.Result);
		}

		public static Task<T> FetchIfNeededAsync<T>(this T obj) where T : AVObject
		{
			return obj.FetchIfNeededAsyncInternal(CancellationToken.None).OnSuccess((Task<AVObject> t) => (T)t.Result);
		}

		public static Task<T> FetchIfNeededAsync<T>(this T obj, CancellationToken cancellationToken) where T : AVObject
		{
			return obj.FetchIfNeededAsyncInternal(cancellationToken).OnSuccess((Task<AVObject> t) => (T)t.Result);
		}
	}
}
