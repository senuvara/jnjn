using LeanCloud.Core.Internal;
using LeanCloud.Storage.Internal;
using System;
using System.Collections.Generic;
using System.Net;
using System.Threading;
using System.Threading.Tasks;

namespace LeanCloud
{
	public static class AVCloud
	{
		public class Captcha
		{
			public string Token
			{
				get;
				internal set;
			}

			public string Url
			{
				get;
				internal set;
			}

			public Task VerifyAsync(string code, CancellationToken cancellationToken = default(CancellationToken))
			{
				return VerifyCaptchaAsync(code, Token);
			}
		}

		public class RealtimeSignature
		{
			public string Nonce
			{
				get;
				internal set;
			}

			public long Timestamp
			{
				get;
				internal set;
			}

			public string ClientId
			{
				get;
				internal set;
			}

			public string Signature
			{
				get;
				internal set;
			}
		}

		internal static IAVCloudCodeController CloudCodeController => AVPlugins.Instance.CloudCodeController;

		public static Task<T> CallFunctionAsync<T>(string name, IDictionary<string, object> parameters = null, CancellationToken cancellationToken = default(CancellationToken))
		{
			return CallFunctionAsync<T>(name, parameters, null, cancellationToken);
		}

		public static Task<T> CallFunctionAsync<T>(string name, IDictionary<string, object> parameters = null)
		{
			return CallFunctionAsync<T>(name, parameters, default(CancellationToken));
		}

		public static Task<T> CallFunctionAsync<T>(string name, IDictionary<string, object> parameters = null, string sesstionToken = null, CancellationToken cancellationToken = default(CancellationToken))
		{
			return AVUser.TakeSessionToken(sesstionToken).OnSuccess((Task<string> s) => CloudCodeController.CallFunctionAsync<T>(name, parameters, s.Result, cancellationToken)).Unwrap();
		}

		public static Task<T> RPCFunctionAsync<T>(string name, IDictionary<string, object> parameters = null, string sesstionToken = null, CancellationToken cancellationToken = default(CancellationToken))
		{
			return AVUser.TakeSessionToken(sesstionToken).OnSuccess((Task<string> s) => CloudCodeController.RPCFunction<T>(name, parameters, s.Result, cancellationToken)).Unwrap();
		}

		public static Task<DateTime> GetServerDateTimeAsync()
		{
			AVCommand command = new AVCommand("date", "GET", (string)null, (IList<KeyValuePair<string, string>>)null, (IDictionary<string, object>)null);
			return AVPlugins.Instance.CommandRunner.RunCommandAsync(command).ContinueWith(delegate(Task<Tuple<HttpStatusCode, IDictionary<string, object>>> t)
			{
				DateTime result = DateTime.MinValue;
				if (AVClient.IsSuccessStatusCode(t.Result.Item1))
				{
					object obj = AVDecoder.Instance.Decode(t.Result.Item2);
					if (obj != null && obj is DateTime)
					{
						result = (DateTime)obj;
					}
				}
				return result;
			});
		}

		public static Task<bool> RequestSMSCodeAsync(string mobilePhoneNumber, string name, string op, int ttl = 10)
		{
			return RequestSMSCodeAsync(mobilePhoneNumber, name, op, ttl, CancellationToken.None);
		}

		public static Task<bool> RequestSMSCodeAsync(string mobilePhoneNumber, string name, string op, int ttl = 10, CancellationToken cancellationToken = default(CancellationToken))
		{
			if (string.IsNullOrEmpty(mobilePhoneNumber))
			{
				throw new AVException(AVException.ErrorCode.MobilePhoneInvalid, "Moblie Phone number is invalid.");
			}
			Dictionary<string, object> dictionary = new Dictionary<string, object>
			{
				{
					"mobilePhoneNumber",
					mobilePhoneNumber
				}
			};
			if (!string.IsNullOrEmpty(name))
			{
				dictionary.Add("name", name);
			}
			if (!string.IsNullOrEmpty(op))
			{
				dictionary.Add("op", op);
			}
			if (ttl > 0)
			{
				dictionary.Add("TTL", ttl);
			}
			AVCommand command = new AVCommand("requestSmsCode", "POST", null, null, dictionary);
			return AVPlugins.Instance.CommandRunner.RunCommandAsync(command, null, null, cancellationToken).ContinueWith((Task<Tuple<HttpStatusCode, IDictionary<string, object>>> t) => AVClient.IsSuccessStatusCode(t.Result.Item1));
		}

		public static Task<bool> RequestSMSCodeAsync(string mobilePhoneNumber)
		{
			return RequestSMSCodeAsync(mobilePhoneNumber, CancellationToken.None);
		}

		public static Task<bool> RequestSMSCodeAsync(string mobilePhoneNumber, CancellationToken cancellationToken)
		{
			return RequestSMSCodeAsync(mobilePhoneNumber, null, null, 0, cancellationToken);
		}

		public static Task<bool> RequestSMSCodeAsync(string mobilePhoneNumber, string template, IDictionary<string, object> env, string sign = "", string validateToken = "", CancellationToken cancellationToken = default(CancellationToken))
		{
			if (string.IsNullOrEmpty(mobilePhoneNumber))
			{
				throw new AVException(AVException.ErrorCode.MobilePhoneInvalid, "Moblie Phone number is invalid.");
			}
			Dictionary<string, object> dictionary = new Dictionary<string, object>
			{
				{
					"mobilePhoneNumber",
					mobilePhoneNumber
				}
			};
			dictionary.Add("template", template);
			if (string.IsNullOrEmpty(sign))
			{
				dictionary.Add("sign", sign);
			}
			if (string.IsNullOrEmpty(validateToken))
			{
				dictionary.Add("validate_token", validateToken);
			}
			foreach (string key in env.Keys)
			{
				dictionary.Add(key, env[key]);
			}
			AVCommand command = new AVCommand("requestSmsCode", "POST", null, null, dictionary);
			return AVPlugins.Instance.CommandRunner.RunCommandAsync(command).ContinueWith((Task<Tuple<HttpStatusCode, IDictionary<string, object>>> t) => AVClient.IsSuccessStatusCode(t.Result.Item1));
		}

		public static Task<bool> RequestVoiceCodeAsync(string mobilePhoneNumber)
		{
			if (string.IsNullOrEmpty(mobilePhoneNumber))
			{
				throw new AVException(AVException.ErrorCode.MobilePhoneInvalid, "Moblie Phone number is invalid.");
			}
			Dictionary<string, object> data = new Dictionary<string, object>
			{
				{
					"mobilePhoneNumber",
					mobilePhoneNumber
				},
				{
					"smsType",
					"voice"
				},
				{
					"IDD",
					"+86"
				}
			};
			AVCommand command = new AVCommand("requestSmsCode", "POST", null, null, data);
			return AVPlugins.Instance.CommandRunner.RunCommandAsync(command).ContinueWith((Task<Tuple<HttpStatusCode, IDictionary<string, object>>> t) => AVClient.IsSuccessStatusCode(t.Result.Item1));
		}

		public static Task<bool> VerifySmsCodeAsync(string code, string mobilePhoneNumber)
		{
			return VerifySmsCodeAsync(code, mobilePhoneNumber, CancellationToken.None);
		}

		public static Task<bool> VerifySmsCodeAsync(string code, string mobilePhoneNumber, CancellationToken cancellationToken)
		{
			AVCommand command = new AVCommand("verifySmsCode/" + code.Trim() + "?mobilePhoneNumber=" + mobilePhoneNumber.Trim(), "POST", (string)null, (IList<KeyValuePair<string, string>>)null, (IDictionary<string, object>)null);
			return AVPlugins.Instance.CommandRunner.RunCommandAsync(command, null, null, cancellationToken).ContinueWith((Task<Tuple<HttpStatusCode, IDictionary<string, object>>> t) => AVClient.IsSuccessStatusCode(t.Result.Item1));
		}

		public static Task<Captcha> RequestCaptchaAsync(int width = 85, int height = 30, CancellationToken cancellationToken = default(CancellationToken))
		{
			AVCommand command = new AVCommand($"requestCaptcha?width={width}&height={height}", "GET", (string)null, (IList<KeyValuePair<string, string>>)null, (IDictionary<string, object>)null);
			return AVPlugins.Instance.CommandRunner.RunCommandAsync(command, null, null, cancellationToken).OnSuccess(delegate(Task<Tuple<HttpStatusCode, IDictionary<string, object>>> t)
			{
				IDictionary<string, object> dictionary = AVDecoder.Instance.Decode(t.Result.Item2) as IDictionary<string, object>;
				return new Captcha
				{
					Token = (dictionary["captcha_token"] as string),
					Url = (dictionary["captcha_url"] as string)
				};
			});
		}

		public static Task<string> VerifyCaptchaAsync(string code, string token, CancellationToken cancellationToken = default(CancellationToken))
		{
			Dictionary<string, object> data = new Dictionary<string, object>
			{
				{
					"captcha_token",
					token
				},
				{
					"captcha_code",
					code
				}
			};
			AVCommand command = new AVCommand("verifyCaptcha", "POST", null, null, data);
			return AVPlugins.Instance.CommandRunner.RunCommandAsync(command, null, null, cancellationToken).ContinueWith(delegate(Task<Tuple<HttpStatusCode, IDictionary<string, object>>> t)
			{
				if (!t.Result.Item2.ContainsKey("validate_token"))
				{
					throw new KeyNotFoundException("validate_token");
				}
				return t.Result.Item2["validate_token"] as string;
			});
		}

		public static Task<IDictionary<string, object>> GetCustomParametersAsync(CancellationToken cancellationToken = default(CancellationToken))
		{
			AVCommand command = new AVCommand($"statistics/apps/{AVClient.CurrentConfiguration.ApplicationId}/sendPolicy", "GET", (string)null, (IList<KeyValuePair<string, string>>)null, (IDictionary<string, object>)null);
			return AVPlugins.Instance.CommandRunner.RunCommandAsync(command, null, null, cancellationToken).OnSuccess((Task<Tuple<HttpStatusCode, IDictionary<string, object>>> t) => t.Result.Item2["parameters"] as IDictionary<string, object>);
		}

		public static Task<RealtimeSignature> RequestRealtimeSignatureAsync(CancellationToken cancellationToken = default(CancellationToken))
		{
			return AVUser.GetCurrentUserAsync(cancellationToken).OnSuccess((Task<AVUser> t) => RequestRealtimeSignatureAsync(t.Result, cancellationToken)).Unwrap();
		}

		public static Task<RealtimeSignature> RequestRealtimeSignatureAsync(AVUser user, CancellationToken cancellationToken = default(CancellationToken))
		{
			AVCommand command = new AVCommand($"rtm/sign", "POST", null, null, new Dictionary<string, object>
			{
				{
					"session_token",
					user.SessionToken
				}
			});
			return AVPlugins.Instance.CommandRunner.RunCommandAsync(command, null, null, cancellationToken).ContinueWith(delegate(Task<Tuple<HttpStatusCode, IDictionary<string, object>>> t)
			{
				IDictionary<string, object> item = t.Result.Item2;
				return new RealtimeSignature
				{
					Nonce = (item["nonce"] as string),
					Timestamp = (long)item["timestamp"],
					ClientId = (item["client_id"] as string),
					Signature = (item["signature"] as string)
				};
			});
		}
	}
}
