using LeanCloud.Core.Internal;
using LeanCloud.Storage.Internal;
using System;
using System.Collections;
using System.Collections.Generic;
using System.Linq;
using System.Net;
using System.Threading;
using System.Threading.Tasks;

namespace LeanCloud
{
	public class AVQuery<T> : AVQueryBase<AVQuery<T>, T>, IAVQueryTuple<AVQuery<T>, T> where T : AVObject
	{
		internal static IAVQueryController QueryController => AVPlugins.Instance.QueryController;

		internal static IObjectSubclassingController SubclassingController => AVPlugins.Instance.SubclassingController;

		private string JsonString => AVClient.SerializeJsonString(BuildParameters(includeClassName: true));

		private AVQuery(AVQuery<T> source, IDictionary<string, object> where = null, IEnumerable<string> replacementOrderBy = null, IEnumerable<string> thenBy = null, int? skip = null, int? limit = null, IEnumerable<string> includes = null, IEnumerable<string> selectedKeys = null, string redirectClassNameForKey = null)
			: base((AVQueryBase<AVQuery<T>, T>)source, where, replacementOrderBy, thenBy, skip, limit, includes, selectedKeys, redirectClassNameForKey)
		{
		}

		internal override AVQuery<T> CreateInstance(AVQuery<T> source, IDictionary<string, object> where = null, IEnumerable<string> replacementOrderBy = null, IEnumerable<string> thenBy = null, int? skip = null, int? limit = null, IEnumerable<string> includes = null, IEnumerable<string> selectedKeys = null, string redirectClassNameForKey = null)
		{
			return new AVQuery<T>(this, where, replacementOrderBy, thenBy, skip, limit, includes, selectedKeys, redirectClassNameForKey);
		}

		internal override AVQuery<T> CreateInstance(AVQueryBase<AVQuery<T>, T> source, IDictionary<string, object> where = null, IEnumerable<string> replacementOrderBy = null, IEnumerable<string> thenBy = null, int? skip = null, int? limit = null, IEnumerable<string> includes = null, IEnumerable<string> selectedKeys = null, string redirectClassNameForKey = null)
		{
			return new AVQuery<T>(this, where, replacementOrderBy, thenBy, skip, limit, includes, selectedKeys, redirectClassNameForKey);
		}

		public AVQuery()
			: this(SubclassingController.GetClassName(typeof(T)))
		{
		}

		public AVQuery(string className)
			: base(className)
		{
		}

		public static AVQuery<T> Or(IEnumerable<AVQuery<T>> queries)
		{
			string text = null;
			List<IDictionary<string, object>> list = new List<IDictionary<string, object>>();
			foreach (AVQuery<T> item in (IEnumerable)queries)
			{
				if (text != null && item.className != text)
				{
					throw new ArgumentException("All of the queries in an or query must be on the same class.");
				}
				text = item.className;
				IDictionary<string, object> dictionary = item.BuildParameters();
				if (dictionary.Count != 0)
				{
					if (!dictionary.TryGetValue("where", out object value) || dictionary.Count > 1)
					{
						throw new ArgumentException("None of the queries in an or query can have non-filtering clauses");
					}
					list.Add(value as IDictionary<string, object>);
				}
			}
			return new AVQuery<T>(new AVQuery<T>(text), new Dictionary<string, object>
			{
				{
					"$or",
					list
				}
			});
		}

		public override Task<IEnumerable<T>> FindAsync(CancellationToken cancellationToken)
		{
			return AVUser.GetCurrentUserAsync().OnSuccess((Task<AVUser> t) => QueryController.FindAsync(this, t.Result, cancellationToken)).Unwrap()
				.OnSuccess((Task<IEnumerable<IObjectState>> t) => t.Result.Select((IObjectState state) => AVObject.FromState<T>(state, base.ClassName)));
		}

		public override Task<T> FirstOrDefaultAsync(CancellationToken cancellationToken)
		{
			return AVUser.GetCurrentUserAsync().OnSuccess((Task<AVUser> t) => QueryController.FirstAsync(this, t.Result, cancellationToken)).Unwrap()
				.OnSuccess(delegate(Task<IObjectState> t)
				{
					IObjectState result = t.Result;
					return (result != null) ? AVObject.FromState<T>(result, base.ClassName) : null;
				});
		}

		public override Task<T> FirstAsync(CancellationToken cancellationToken)
		{
			return FirstOrDefaultAsync(cancellationToken).OnSuccess(delegate(Task<T> t)
			{
				if (t.Result == null)
				{
					throw new AVException(AVException.ErrorCode.ObjectNotFound, "No results matched the query.");
				}
				return t.Result;
			});
		}

		public override Task<int> CountAsync(CancellationToken cancellationToken)
		{
			return AVUser.GetCurrentUserAsync().OnSuccess((Task<AVUser> t) => QueryController.CountAsync(this, t.Result, cancellationToken)).Unwrap();
		}

		public override Task<T> GetAsync(string objectId, CancellationToken cancellationToken)
		{
			return new AVQuery<T>(new AVQuery<T>(className).WhereEqualTo("objectId", objectId), null, null, null, null, includes: base.includes, selectedKeys: base.selectedKeys, limit: 1).FindAsync(cancellationToken).OnSuccess((Task<IEnumerable<T>> t) => t.Result.FirstOrDefault() ?? throw new AVException(AVException.ErrorCode.ObjectNotFound, "Object with the given objectId not found."));
		}

		public static Task<IEnumerable<T>> DoCloudQueryAsync(string cql, CancellationToken cancellationToken)
		{
			return rebuildObjectFromCloudQueryResult($"cloudQuery?cql={Uri.EscapeDataString(cql)}");
		}

		public static Task<IEnumerable<T>> DoCloudQueryAsync(string cql)
		{
			return DoCloudQueryAsync(cql, CancellationToken.None);
		}

		public static Task<IEnumerable<T>> DoCloudQueryAsync(string cqlTeamplate, params object[] pvalues)
		{
			return rebuildObjectFromCloudQueryResult(string.Format("cloudQuery?cql={0}&pvalues={1}", arg1: Uri.EscapeDataString(Json.Encode(pvalues)), arg0: Uri.EscapeDataString(cqlTeamplate)));
		}

		internal static Task<IEnumerable<T>> rebuildObjectFromCloudQueryResult(string queryString)
		{
			AVCommand command = new AVCommand(queryString, "GET", AVUser.CurrentSessionToken, (IList<KeyValuePair<string, string>>)null, (IDictionary<string, object>)null);
			return AVPlugins.Instance.CommandRunner.RunCommandAsync(command, null, null, CancellationToken.None).OnSuccess(delegate(Task<Tuple<HttpStatusCode, IDictionary<string, object>>> t)
			{
				IList<object> source = t.Result.Item2["results"] as IList<object>;
				string className = t.Result.Item2["className"].ToString();
				return from item in source
					select AVObjectCoder.Instance.Decode(item as IDictionary<string, object>, AVDecoder.Instance) into state
					select AVObject.FromState<T>(state, className);
			});
		}

		public override bool Equals(object obj)
		{
			if (obj == null || !(obj is AVQuery<T>))
			{
				return false;
			}
			AVQuery<T> aVQuery = obj as AVQuery<T>;
			if (object.Equals(className, aVQuery.ClassName) && where.CollectionsEqual(aVQuery.where) && orderBy.CollectionsEqual(aVQuery.orderBy) && includes.CollectionsEqual(aVQuery.includes) && selectedKeys.CollectionsEqual(aVQuery.selectedKeys) && object.Equals(skip, aVQuery.skip))
			{
				return object.Equals(limit, aVQuery.limit);
			}
			return false;
		}
	}
}
