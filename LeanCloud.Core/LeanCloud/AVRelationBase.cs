using LeanCloud.Core.Internal;
using LeanCloud.Storage.Internal;
using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Linq.Expressions;

namespace LeanCloud
{
	[EditorBrowsable(EditorBrowsableState.Never)]
	public abstract class AVRelationBase : IJsonConvertible
	{
		private AVObject parent;

		private string key;

		private string targetClassName;

		internal static IObjectSubclassingController SubclassingController => AVPlugins.Instance.SubclassingController;

		internal string TargetClassName
		{
			get
			{
				return targetClassName;
			}
			set
			{
				targetClassName = value;
			}
		}

		internal AVRelationBase(AVObject parent, string key)
		{
			EnsureParentAndKey(parent, key);
		}

		internal AVRelationBase(AVObject parent, string key, string targetClassName)
			: this(parent, key)
		{
			this.targetClassName = targetClassName;
		}

		internal void EnsureParentAndKey(AVObject parent, string key)
		{
			this.parent = (this.parent ?? parent);
			this.key = (this.key ?? key);
		}

		internal void Add(AVObject obj)
		{
			AVRelationOperation aVRelationOperation = new AVRelationOperation(new AVObject[1]
			{
				obj
			}, null);
			parent.PerformOperation(key, aVRelationOperation);
			targetClassName = aVRelationOperation.TargetClassName;
		}

		internal void Remove(AVObject obj)
		{
			AVRelationOperation aVRelationOperation = new AVRelationOperation(null, new AVObject[1]
			{
				obj
			});
			parent.PerformOperation(key, aVRelationOperation);
			targetClassName = aVRelationOperation.TargetClassName;
		}

		IDictionary<string, object> IJsonConvertible.ToJSON()
		{
			return new Dictionary<string, object>
			{
				{
					"__type",
					"Relation"
				},
				{
					"className",
					targetClassName
				}
			};
		}

		internal AVQuery<T> GetQuery<T>() where T : AVObject
		{
			if (targetClassName != null)
			{
				return new AVQuery<T>(targetClassName).WhereRelatedTo(parent, key);
			}
			return new AVQuery<T>(parent.ClassName).RedirectClassName(key).WhereRelatedTo(parent, key);
		}

		internal AVQuery<T> GetReverseQuery<T>(T target) where T : AVObject
		{
			if (target.ObjectId == null)
			{
				throw new ArgumentNullException("target.ObjectId", "can not query a relation without target ObjectId.");
			}
			return new AVQuery<T>(parent.ClassName).WhereEqualTo(key, target);
		}

		internal static AVRelationBase CreateRelation(AVObject parent, string key, string targetClassName)
		{
			Type type = SubclassingController.GetType(targetClassName) ?? typeof(AVObject);
			return (AVRelationBase)((MethodCallExpression)((Expression<Func<AVRelation<AVObject>>>)(() => CreateRelation<AVObject>(parent, key, targetClassName))).Body).Method.GetGenericMethodDefinition().MakeGenericMethod(type).Invoke(null, new object[3]
			{
				parent,
				key,
				targetClassName
			});
		}

		private static AVRelation<T> CreateRelation<T>(AVObject parent, string key, string targetClassName) where T : AVObject
		{
			return new AVRelation<T>(parent, key, targetClassName);
		}
	}
}
