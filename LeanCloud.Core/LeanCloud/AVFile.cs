using LeanCloud.Core.Internal;
using LeanCloud.Storage.Internal;
using System;
using System.Collections.Generic;
using System.IO;
using System.Net;
using System.Threading;
using System.Threading.Tasks;

namespace LeanCloud
{
	public class AVFile : IJsonConvertible
	{
		internal static int objectCounter;

		internal static readonly object Mutex;

		private FileState state;

		private readonly Stream dataStream;

		private readonly TaskQueue taskQueue = new TaskQueue();

		private static readonly Dictionary<string, string> MIMETypesDictionary;

		private object mutex = new object();

		private bool isExternal;

		public bool IsDirty => state.Url == null;

		[AVFieldName("name")]
		public string Name => state.Name;

		public string MimeType => state.MimeType;

		[AVFieldName("url")]
		public Uri Url => state.Url;

		internal static IAVFileController FileController => AVPlugins.Instance.FileController;

		public string ObjectId
		{
			get
			{
				lock (mutex)
				{
					return state.ObjectId;
				}
			}
		}

		public IDictionary<string, object> MetaData => state.MetaData;

		public bool IsExternal => isExternal;

		public AVFile(string name, Stream data, string mimeType = null, IDictionary<string, object> metaData = null)
		{
			mimeType = ((mimeType == null) ? GetMIMEType(name) : mimeType);
			state = new FileState
			{
				Name = name,
				MimeType = mimeType,
				MetaData = metaData
			};
			dataStream = data;
			lock (Mutex)
			{
				objectCounter++;
				state.counter = objectCounter;
			}
		}

		public AVFile(string name, byte[] data, string mimeType = null)
			: this(name, new MemoryStream(data), mimeType)
		{
		}

		public AVFile(string name, Stream data, string mimeType = null)
			: this(name, data, mimeType, new Dictionary<string, object>())
		{
		}

		public AVFile(string name, byte[] data)
			: this(name, new MemoryStream(data), new Dictionary<string, object>())
		{
		}

		public AVFile(string name, byte[] data, IDictionary<string, object> metaData)
			: this(name, new MemoryStream(data), metaData)
		{
		}

		public AVFile(string name, Stream data, IDictionary<string, object> metaData)
			: this(name, data, GetMIMEType(name), metaData)
		{
		}

		public AVFile(string name, Stream data)
			: this(name, data, new Dictionary<string, object>())
		{
		}

		public AVFile(string name, Uri uri, string mimeType = null, IDictionary<string, object> metaData = null)
		{
			mimeType = ((mimeType == null) ? GetMIMEType(name) : mimeType);
			state = new FileState
			{
				Name = name,
				Url = uri,
				MetaData = metaData
			};
			lock (Mutex)
			{
				objectCounter++;
				state.counter = objectCounter;
			}
			isExternal = true;
		}

		public AVFile(string name, string url, string mimeType = null, IDictionary<string, object> metaData = null)
			: this(name, new Uri(url), mimeType, metaData)
		{
		}

		public AVFile(string name, string url, IDictionary<string, object> metaData)
			: this(name, url, null, metaData)
		{
		}

		public AVFile(string name, Uri uri, string mimeType = null)
			: this(name, uri, mimeType, new Dictionary<string, object>())
		{
		}

		public AVFile(string name, Uri uri)
			: this(name, uri, null, new Dictionary<string, object>())
		{
		}

		public AVFile(string name, string url)
			: this(name, new Uri(url))
		{
		}

		internal AVFile(FileState filestate)
		{
			state = filestate;
		}

		internal AVFile(string objectId)
			: this(new FileState
			{
				ObjectId = objectId
			})
		{
		}

		IDictionary<string, object> IJsonConvertible.ToJSON()
		{
			if (IsDirty)
			{
				throw new InvalidOperationException("AVFile must be saved before it can be serialized.");
			}
			return new Dictionary<string, object>
			{
				{
					"__type",
					"File"
				},
				{
					"id",
					ObjectId
				},
				{
					"name",
					Name
				},
				{
					"url",
					Url.AbsoluteUri
				}
			};
		}

		public Task SaveAsync()
		{
			return SaveAsync(null, CancellationToken.None);
		}

		public Task SaveAsync(CancellationToken cancellationToken)
		{
			return SaveAsync(null, cancellationToken);
		}

		public Task SaveAsync(IProgress<AVUploadProgressEventArgs> progress)
		{
			return SaveAsync(progress, CancellationToken.None);
		}

		public Task SaveAsync(IProgress<AVUploadProgressEventArgs> progress, CancellationToken cancellationToken)
		{
			if (isExternal)
			{
				return SaveExternal();
			}
			return taskQueue.Enqueue((Task toAwait) => FileController.SaveAsync(state, dataStream, AVUser.CurrentSessionToken, progress, cancellationToken), cancellationToken).OnSuccess(delegate(Task<FileState> t)
			{
				state = t.Result;
			});
		}

		internal Task SaveExternal()
		{
			Dictionary<string, object> data = new Dictionary<string, object>
			{
				{
					"url",
					Url.ToString()
				},
				{
					"name",
					Name
				},
				{
					"mime_type",
					MimeType
				},
				{
					"metaData",
					MetaData
				}
			};
			AVCommand aVCommand = null;
			aVCommand = (string.IsNullOrEmpty(ObjectId) ? new AVCommand("files", "POST", AVUser.CurrentSessionToken, null, data) : new AVCommand("files/" + ObjectId, "PUT", AVUser.CurrentSessionToken, null, data));
			return AVPlugins.Instance.CommandRunner.RunCommandAsync(aVCommand).ContinueWith(delegate(Task<Tuple<HttpStatusCode, IDictionary<string, object>>> t)
			{
				IDictionary<string, object> item = t.Result.Item2;
				state.ObjectId = item["objectId"].ToString();
				return AVClient.IsSuccessStatusCode(t.Result.Item1);
			});
		}

		static AVFile()
		{
			objectCounter = 0;
			Mutex = new object();
			MIMETypesDictionary = new Dictionary<string, string>
			{
				{
					"ai",
					"application/postscript"
				},
				{
					"aif",
					"audio/x-aiff"
				},
				{
					"aifc",
					"audio/x-aiff"
				},
				{
					"aiff",
					"audio/x-aiff"
				},
				{
					"asc",
					"text/plain"
				},
				{
					"atom",
					"application/atom+xml"
				},
				{
					"au",
					"audio/basic"
				},
				{
					"avi",
					"video/x-msvideo"
				},
				{
					"bcpio",
					"application/x-bcpio"
				},
				{
					"bin",
					"application/octet-stream"
				},
				{
					"bmp",
					"image/bmp"
				},
				{
					"cdf",
					"application/x-netcdf"
				},
				{
					"cgm",
					"image/cgm"
				},
				{
					"class",
					"application/octet-stream"
				},
				{
					"cpio",
					"application/x-cpio"
				},
				{
					"cpt",
					"application/mac-compactpro"
				},
				{
					"csh",
					"application/x-csh"
				},
				{
					"css",
					"text/css"
				},
				{
					"dcr",
					"application/x-director"
				},
				{
					"dif",
					"video/x-dv"
				},
				{
					"dir",
					"application/x-director"
				},
				{
					"djv",
					"image/vnd.djvu"
				},
				{
					"djvu",
					"image/vnd.djvu"
				},
				{
					"dll",
					"application/octet-stream"
				},
				{
					"dmg",
					"application/octet-stream"
				},
				{
					"dms",
					"application/octet-stream"
				},
				{
					"doc",
					"application/msword"
				},
				{
					"docx",
					"application/vnd.openxmlformats-officedocument.wordprocessingml.document"
				},
				{
					"dotx",
					"application/vnd.openxmlformats-officedocument.wordprocessingml.template"
				},
				{
					"docm",
					"application/vnd.ms-word.document.macroEnabled.12"
				},
				{
					"dotm",
					"application/vnd.ms-word.template.macroEnabled.12"
				},
				{
					"dtd",
					"application/xml-dtd"
				},
				{
					"dv",
					"video/x-dv"
				},
				{
					"dvi",
					"application/x-dvi"
				},
				{
					"dxr",
					"application/x-director"
				},
				{
					"eps",
					"application/postscript"
				},
				{
					"etx",
					"text/x-setext"
				},
				{
					"exe",
					"application/octet-stream"
				},
				{
					"ez",
					"application/andrew-inset"
				},
				{
					"gif",
					"image/gif"
				},
				{
					"gram",
					"application/srgs"
				},
				{
					"grxml",
					"application/srgs+xml"
				},
				{
					"gtar",
					"application/x-gtar"
				},
				{
					"hdf",
					"application/x-hdf"
				},
				{
					"hqx",
					"application/mac-binhex40"
				},
				{
					"htm",
					"text/html"
				},
				{
					"html",
					"text/html"
				},
				{
					"ice",
					"x-conference/x-cooltalk"
				},
				{
					"ico",
					"image/x-icon"
				},
				{
					"ics",
					"text/calendar"
				},
				{
					"ief",
					"image/ief"
				},
				{
					"ifb",
					"text/calendar"
				},
				{
					"iges",
					"model/iges"
				},
				{
					"igs",
					"model/iges"
				},
				{
					"jnlp",
					"application/x-java-jnlp-file"
				},
				{
					"jp2",
					"image/jp2"
				},
				{
					"jpe",
					"image/jpeg"
				},
				{
					"jpeg",
					"image/jpeg"
				},
				{
					"jpg",
					"image/jpeg"
				},
				{
					"js",
					"application/x-javascript"
				},
				{
					"kar",
					"audio/midi"
				},
				{
					"latex",
					"application/x-latex"
				},
				{
					"lha",
					"application/octet-stream"
				},
				{
					"lzh",
					"application/octet-stream"
				},
				{
					"m3u",
					"audio/x-mpegurl"
				},
				{
					"m4a",
					"audio/mp4a-latm"
				},
				{
					"m4b",
					"audio/mp4a-latm"
				},
				{
					"m4p",
					"audio/mp4a-latm"
				},
				{
					"m4u",
					"video/vnd.mpegurl"
				},
				{
					"m4v",
					"video/x-m4v"
				},
				{
					"mac",
					"image/x-macpaint"
				},
				{
					"man",
					"application/x-troff-man"
				},
				{
					"mathml",
					"application/mathml+xml"
				},
				{
					"me",
					"application/x-troff-me"
				},
				{
					"mesh",
					"model/mesh"
				},
				{
					"mid",
					"audio/midi"
				},
				{
					"midi",
					"audio/midi"
				},
				{
					"mif",
					"application/vnd.mif"
				},
				{
					"mov",
					"video/quicktime"
				},
				{
					"movie",
					"video/x-sgi-movie"
				},
				{
					"mp2",
					"audio/mpeg"
				},
				{
					"mp3",
					"audio/mpeg"
				},
				{
					"mp4",
					"video/mp4"
				},
				{
					"mpe",
					"video/mpeg"
				},
				{
					"mpeg",
					"video/mpeg"
				},
				{
					"mpg",
					"video/mpeg"
				},
				{
					"mpga",
					"audio/mpeg"
				},
				{
					"ms",
					"application/x-troff-ms"
				},
				{
					"msh",
					"model/mesh"
				},
				{
					"mxu",
					"video/vnd.mpegurl"
				},
				{
					"nc",
					"application/x-netcdf"
				},
				{
					"oda",
					"application/oda"
				},
				{
					"ogg",
					"application/ogg"
				},
				{
					"pbm",
					"image/x-portable-bitmap"
				},
				{
					"pct",
					"image/pict"
				},
				{
					"pdb",
					"chemical/x-pdb"
				},
				{
					"pdf",
					"application/pdf"
				},
				{
					"pgm",
					"image/x-portable-graymap"
				},
				{
					"pgn",
					"application/x-chess-pgn"
				},
				{
					"pic",
					"image/pict"
				},
				{
					"pict",
					"image/pict"
				},
				{
					"png",
					"image/png"
				},
				{
					"pnm",
					"image/x-portable-anymap"
				},
				{
					"pnt",
					"image/x-macpaint"
				},
				{
					"pntg",
					"image/x-macpaint"
				},
				{
					"ppm",
					"image/x-portable-pixmap"
				},
				{
					"ppt",
					"application/vnd.ms-powerpoint"
				},
				{
					"pptx",
					"application/vnd.openxmlformats-officedocument.presentationml.presentation"
				},
				{
					"potx",
					"application/vnd.openxmlformats-officedocument.presentationml.template"
				},
				{
					"ppsx",
					"application/vnd.openxmlformats-officedocument.presentationml.slideshow"
				},
				{
					"ppam",
					"application/vnd.ms-powerpoint.addin.macroEnabled.12"
				},
				{
					"pptm",
					"application/vnd.ms-powerpoint.presentation.macroEnabled.12"
				},
				{
					"potm",
					"application/vnd.ms-powerpoint.template.macroEnabled.12"
				},
				{
					"ppsm",
					"application/vnd.ms-powerpoint.slideshow.macroEnabled.12"
				},
				{
					"ps",
					"application/postscript"
				},
				{
					"qt",
					"video/quicktime"
				},
				{
					"qti",
					"image/x-quicktime"
				},
				{
					"qtif",
					"image/x-quicktime"
				},
				{
					"ra",
					"audio/x-pn-realaudio"
				},
				{
					"ram",
					"audio/x-pn-realaudio"
				},
				{
					"ras",
					"image/x-cmu-raster"
				},
				{
					"rdf",
					"application/rdf+xml"
				},
				{
					"rgb",
					"image/x-rgb"
				},
				{
					"rm",
					"application/vnd.rn-realmedia"
				},
				{
					"roff",
					"application/x-troff"
				},
				{
					"rtf",
					"text/rtf"
				},
				{
					"rtx",
					"text/richtext"
				},
				{
					"sgm",
					"text/sgml"
				},
				{
					"sgml",
					"text/sgml"
				},
				{
					"sh",
					"application/x-sh"
				},
				{
					"shar",
					"application/x-shar"
				},
				{
					"silo",
					"model/mesh"
				},
				{
					"sit",
					"application/x-stuffit"
				},
				{
					"skd",
					"application/x-koan"
				},
				{
					"skm",
					"application/x-koan"
				},
				{
					"skp",
					"application/x-koan"
				},
				{
					"skt",
					"application/x-koan"
				},
				{
					"smi",
					"application/smil"
				},
				{
					"smil",
					"application/smil"
				},
				{
					"snd",
					"audio/basic"
				},
				{
					"so",
					"application/octet-stream"
				},
				{
					"spl",
					"application/x-futuresplash"
				},
				{
					"src",
					"application/x-wais-Source"
				},
				{
					"sv4cpio",
					"application/x-sv4cpio"
				},
				{
					"sv4crc",
					"application/x-sv4crc"
				},
				{
					"svg",
					"image/svg+xml"
				},
				{
					"swf",
					"application/x-shockwave-flash"
				},
				{
					"t",
					"application/x-troff"
				},
				{
					"tar",
					"application/x-tar"
				},
				{
					"tcl",
					"application/x-tcl"
				},
				{
					"tex",
					"application/x-tex"
				},
				{
					"texi",
					"application/x-texinfo"
				},
				{
					"texinfo",
					"application/x-texinfo"
				},
				{
					"tif",
					"image/tiff"
				},
				{
					"tiff",
					"image/tiff"
				},
				{
					"tr",
					"application/x-troff"
				},
				{
					"tsv",
					"text/tab-separated-values"
				},
				{
					"txt",
					"text/plain"
				},
				{
					"ustar",
					"application/x-ustar"
				},
				{
					"vcd",
					"application/x-cdlink"
				},
				{
					"vrml",
					"model/vrml"
				},
				{
					"vxml",
					"application/voicexml+xml"
				},
				{
					"wav",
					"audio/x-wav"
				},
				{
					"wbmp",
					"image/vnd.wap.wbmp"
				},
				{
					"wbmxl",
					"application/vnd.wap.wbxml"
				},
				{
					"wml",
					"text/vnd.wap.wml"
				},
				{
					"wmlc",
					"application/vnd.wap.wmlc"
				},
				{
					"wmls",
					"text/vnd.wap.wmlscript"
				},
				{
					"wmlsc",
					"application/vnd.wap.wmlscriptc"
				},
				{
					"wrl",
					"model/vrml"
				},
				{
					"xbm",
					"image/x-xbitmap"
				},
				{
					"xht",
					"application/xhtml+xml"
				},
				{
					"xhtml",
					"application/xhtml+xml"
				},
				{
					"xls",
					"application/vnd.ms-excel"
				},
				{
					"xml",
					"application/xml"
				},
				{
					"xpm",
					"image/x-xpixmap"
				},
				{
					"xsl",
					"application/xml"
				},
				{
					"xlsx",
					"application/vnd.openxmlformats-officedocument.spreadsheetml.sheet"
				},
				{
					"xltx",
					"application/vnd.openxmlformats-officedocument.spreadsheetml.template"
				},
				{
					"xlsm",
					"application/vnd.ms-excel.sheet.macroEnabled.12"
				},
				{
					"xltm",
					"application/vnd.ms-excel.template.macroEnabled.12"
				},
				{
					"xlam",
					"application/vnd.ms-excel.addin.macroEnabled.12"
				},
				{
					"xlsb",
					"application/vnd.ms-excel.sheet.binary.macroEnabled.12"
				},
				{
					"xslt",
					"application/xslt+xml"
				},
				{
					"xul",
					"application/vnd.mozilla.xul+xml"
				},
				{
					"xwd",
					"image/x-xwindowdump"
				},
				{
					"xyz",
					"chemical/x-xyz"
				},
				{
					"zip",
					"application/zip"
				}
			};
		}

		internal static string GetMIMEType(string fileName)
		{
			try
			{
				string key = Path.GetExtension(fileName).Remove(0, 1);
				if (!MIMETypesDictionary.ContainsKey(key))
				{
					return "unknown/unknown";
				}
				return MIMETypesDictionary[key];
			}
			catch
			{
				return "unknown/unknown";
			}
		}

		public static Task<AVFile> GetFileWithObjectIdAsync(string objectId, CancellationToken cancellationToken)
		{
			string currentSessionToken = AVUser.CurrentSessionToken;
			return FileController.GetAsync(objectId, currentSessionToken, cancellationToken).OnSuccess((Task<FileState> _) => new AVFile(_.Result));
		}

		public static AVFile CreateWithoutData(string objectId)
		{
			return new AVFile(objectId);
		}

		public static Task<AVFile> GetFileWithObjectIdAsync(string objectId)
		{
			return GetFileWithObjectIdAsync(objectId, CancellationToken.None);
		}

		internal void MergeFromJSON(IDictionary<string, object> jsonData)
		{
			lock (mutex)
			{
				state.ObjectId = (jsonData["objectId"] as string);
				state.Url = new Uri(jsonData["url"] as string, UriKind.Absolute);
				if (jsonData.ContainsKey("name"))
				{
					state.Name = (jsonData["name"] as string);
				}
				if (jsonData.ContainsKey("metaData"))
				{
					state.MetaData = (jsonData["metaData"] as Dictionary<string, object>);
				}
			}
		}

		public Task DeleteAsync()
		{
			return DeleteAsync(CancellationToken.None);
		}

		internal Task DeleteAsync(CancellationToken cancellationToken)
		{
			return taskQueue.Enqueue((Task toAwait) => DeleteAsync(toAwait, cancellationToken), cancellationToken);
		}

		internal Task DeleteAsync(Task toAwait, CancellationToken cancellationToken)
		{
			if (ObjectId == null)
			{
				return Task.FromResult(0);
			}
			string sessionToken = AVUser.CurrentSessionToken;
			return toAwait.OnSuccess((Task _) => FileController.DeleteAsync(state, sessionToken, cancellationToken)).Unwrap().OnSuccess(delegate
			{
			});
		}
	}
}
