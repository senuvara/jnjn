using LeanCloud.Core.Internal;
using LeanCloud.Storage.Internal;
using System;
using System.Collections.Generic;
using System.Net;
using System.Threading.Tasks;

namespace LeanCloud
{
	public class AVRPCCloudFunctionBase<P, R>
	{
		public delegate R AVRPCDeserialize<R>(IDictionary<string, object> result);

		public delegate IDictionary<string, object> AVRPCSerialize<P>(P parameters);

		private AVRPCDeserialize<R> _decode;

		private AVRPCSerialize<P> _encode;

		public AVRPCDeserialize<R> Decode
		{
			get
			{
				return _decode;
			}
			set
			{
				_decode = value;
			}
		}

		public AVRPCSerialize<P> Encode
		{
			get
			{
				if (_encode == null)
				{
					_encode = ((P n) => (n != null) ? (Json.Parse(n.ToString()) as IDictionary<string, object>) : null);
				}
				return _encode;
			}
			set
			{
				_encode = value;
			}
		}

		public string FunctionName
		{
			get;
			set;
		}

		public AVRPCCloudFunctionBase()
			: this(noneParameters: true)
		{
		}

		public AVRPCCloudFunctionBase(bool noneParameters)
		{
			if (noneParameters)
			{
				Encode = ((P n) => null);
			}
		}

		public Task<R> ExecuteAsync(P parameters)
		{
			return AVUser.GetCurrentAsync().OnSuccess(delegate(Task<AVUser> t)
			{
				AVUser result = t.Result;
				IDictionary<string, object> data = Encode(parameters);
				return AVClient.RunCommandAsync(new AVCommand($"call/{Uri.EscapeUriString(FunctionName)}", "POST", result?.SessionToken, null, data));
			}).Unwrap()
				.OnSuccess(delegate(Task<Tuple<HttpStatusCode, IDictionary<string, object>>> s)
				{
					IDictionary<string, object> item = s.Result.Item2;
					return (!item.ContainsKey("result")) ? default(R) : Decode(item);
				});
		}
	}
}
