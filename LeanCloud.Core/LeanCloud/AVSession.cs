using LeanCloud.Core.Internal;
using LeanCloud.Storage.Internal;
using System.Collections.Generic;
using System.Threading;
using System.Threading.Tasks;

namespace LeanCloud
{
	[AVClassName("_Session")]
	public class AVSession : AVObject
	{
		private static readonly HashSet<string> readOnlyKeys = new HashSet<string>
		{
			"sessionToken",
			"createdWith",
			"restricted",
			"user",
			"expiresAt",
			"installationId"
		};

		[AVFieldName("sessionToken")]
		public string SessionToken => GetProperty<string>(null, "SessionToken");

		public static AVQuery<AVSession> Query => new AVQuery<AVSession>();

		internal static IAVSessionController SessionController => AVPlugins.Instance.SessionController;

		protected override bool IsKeyMutable(string key)
		{
			return !readOnlyKeys.Contains(key);
		}

		public static Task<AVSession> GetCurrentSessionAsync()
		{
			return GetCurrentSessionAsync(CancellationToken.None);
		}

		public static Task<AVSession> GetCurrentSessionAsync(CancellationToken cancellationToken)
		{
			return AVUser.GetCurrentUserAsync().OnSuccess(delegate(Task<AVUser> t1)
			{
				AVUser result = t1.Result;
				if (result == null)
				{
					return Task.FromResult<AVSession>(null);
				}
				string sessionToken = result.SessionToken;
				return (sessionToken == null) ? Task.FromResult<AVSession>(null) : SessionController.GetSessionAsync(sessionToken, cancellationToken).OnSuccess((Task<IObjectState> t) => AVObject.FromState<AVSession>(t.Result, "_Session"));
			}).Unwrap();
		}

		internal static Task RevokeAsync(string sessionToken, CancellationToken cancellationToken)
		{
			if (sessionToken == null || !SessionController.IsRevocableSessionToken(sessionToken))
			{
				return Task.FromResult(0);
			}
			return SessionController.RevokeAsync(sessionToken, cancellationToken);
		}

		internal static Task<string> UpgradeToRevocableSessionAsync(string sessionToken, CancellationToken cancellationToken)
		{
			if (sessionToken == null || SessionController.IsRevocableSessionToken(sessionToken))
			{
				return Task.FromResult(sessionToken);
			}
			return SessionController.UpgradeToRevocableSessionAsync(sessionToken, cancellationToken).OnSuccess((Task<IObjectState> t) => AVObject.FromState<AVSession>(t.Result, "_Session").SessionToken);
		}
	}
}
