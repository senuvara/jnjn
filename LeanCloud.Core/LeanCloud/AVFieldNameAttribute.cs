using System;

namespace LeanCloud
{
	[AttributeUsage(AttributeTargets.Property, Inherited = true, AllowMultiple = false)]
	public sealed class AVFieldNameAttribute : Attribute
	{
		public string FieldName
		{
			get;
			private set;
		}

		public AVFieldNameAttribute(string fieldName)
		{
			FieldName = fieldName;
		}
	}
}
