using LeanCloud.Core.Internal;
using System.Collections.Generic;
using System.Linq;

namespace LeanCloud
{
	public class AVObjectListRPCCloudFunction : AVObjectListRPCCloudFunction<AVObject>
	{
	}
	public class AVObjectListRPCCloudFunction<R> : AVRPCCloudFunctionBase<object, IList<R>> where R : AVObject
	{
		public AVObjectListRPCCloudFunction()
			: base(noneParameters: true)
		{
			base.Decode = AVObjectListDeserializer();
		}

		public AVRPCDeserialize<IList<R>> AVObjectListDeserializer()
		{
			return (IDictionary<string, object> data) => (data["result"] as IList<object>).Select(delegate(object item)
			{
				IObjectState objectState = AVObjectCoder.Instance.Decode(item as IDictionary<string, object>, AVDecoder.Instance);
				return AVObject.FromState<AVObject>(objectState, objectState.ClassName);
			}).ToList() as IList<R>;
		}
	}
}
