using LeanCloud.Core.Internal;
using LeanCloud.Storage.Internal;
using LeanCloud.Utilities;
using System;
using System.Collections.Generic;
using System.Threading;
using System.Threading.Tasks;

namespace LeanCloud
{
	public class AVConfig : IJsonConvertible
	{
		private IDictionary<string, object> properties = new Dictionary<string, object>();

		public static AVConfig CurrentConfig
		{
			get
			{
				Task<AVConfig> currentConfigAsync = ConfigController.CurrentConfigController.GetCurrentConfigAsync();
				currentConfigAsync.Wait();
				return currentConfigAsync.Result;
			}
		}

		private static IAVConfigController ConfigController => AVPlugins.Instance.ConfigController;

		public virtual object this[string key] => properties[key];

		internal static void ClearCurrentConfig()
		{
			ConfigController.CurrentConfigController.ClearCurrentConfigAsync().Wait();
		}

		internal static void ClearCurrentConfigInMemory()
		{
			ConfigController.CurrentConfigController.ClearCurrentConfigInMemoryAsync().Wait();
		}

		internal AVConfig()
		{
		}

		internal AVConfig(IDictionary<string, object> fetchedConfig)
		{
			IDictionary<string, object> dictionary = properties = (AVDecoder.Instance.Decode(fetchedConfig["params"]) as IDictionary<string, object>);
		}

		public static Task<AVConfig> GetAsync()
		{
			return GetAsync(CancellationToken.None);
		}

		public static Task<AVConfig> GetAsync(CancellationToken cancellationToken)
		{
			return ConfigController.FetchConfigAsync(AVUser.CurrentSessionToken, cancellationToken);
		}

		public T Get<T>(string key)
		{
			return Conversion.To<T>(properties[key]);
		}

		public bool TryGetValue<T>(string key, out T result)
		{
			if (properties.ContainsKey(key))
			{
				try
				{
					T val = result = Conversion.To<T>(properties[key]);
					return true;
				}
				catch (Exception)
				{
				}
			}
			result = default(T);
			return false;
		}

		IDictionary<string, object> IJsonConvertible.ToJSON()
		{
			return new Dictionary<string, object>
			{
				{
					"params",
					NoObjectsEncoder.Instance.Encode(properties)
				}
			};
		}
	}
}
