using LeanCloud.Storage.Internal;
using System;
using UnityEngine;

namespace LeanCloud
{
	public class AVInitializeBehaviour : MonoBehaviour
	{
		private static bool isInitialized;

		[SerializeField]
		public string applicationID;

		[SerializeField]
		public string applicationKey;

		[SerializeField]
		public AVClient.Configuration.AVRegion region;

		[SerializeField]
		public string engineServer;

		[SerializeField]
		public bool useProduction = true;

		public static bool IsWebPlayer
		{
			get;
			set;
		}

		public virtual void Awake()
		{
			IsWebPlayer = Application.isWebPlayer;
			Initialize();
			base.gameObject.name = "AVInitializeBehaviour";
		}

		public void Initialize()
		{
			if (!isInitialized)
			{
				isInitialized = true;
				UnityEngine.Object.DontDestroyOnLoad(base.gameObject);
				AVClient.Configuration configuration = default(AVClient.Configuration);
				configuration.ApplicationId = applicationID;
				configuration.ApplicationKey = applicationKey;
				configuration.Region = region;
				configuration.EngineServer = ((!string.IsNullOrEmpty(engineServer)) ? new Uri(engineServer) : null);
				AVClient.Initialize(configuration);
				AVClient.UseProduction = useProduction;
				Dispatcher.Instance.GameObject = base.gameObject;
				StartCoroutine(Dispatcher.Instance.DispatcherCoroutine);
			}
		}
	}
}
