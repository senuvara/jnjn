using System;
using System.Text.RegularExpressions;

namespace LeanCloud
{
	[AVClassName("_Role")]
	public class AVRole : AVObject
	{
		private static readonly Regex namePattern = new Regex("^[0-9a-zA-Z_\\- ]+$");

		[AVFieldName("name")]
		public string Name
		{
			get
			{
				return GetProperty<string>("Name");
			}
			set
			{
				SetProperty(value, "Name");
			}
		}

		[AVFieldName("users")]
		public AVRelation<AVUser> Users => GetRelationProperty<AVUser>("Users");

		[AVFieldName("roles")]
		public AVRelation<AVRole> Roles => GetRelationProperty<AVRole>("Roles");

		public static AVQuery<AVRole> Query => new AVQuery<AVRole>();

		public AVRole()
		{
		}

		public AVRole(string name, AVACL acl)
			: this()
		{
			Name = name;
			base.ACL = acl;
		}

		internal override void OnSettingValue(ref string key, ref object value)
		{
			base.OnSettingValue(ref key, ref value);
			if (key == "name")
			{
				if (base.ObjectId != null)
				{
					throw new InvalidOperationException("A role's name can only be set before it has been saved.");
				}
				if (!(value is string))
				{
					throw new ArgumentException("A role's name must be a string.", "value");
				}
				if (!namePattern.IsMatch((string)value))
				{
					throw new ArgumentException("A role's name can only contain alphanumeric characters, _, -, and spaces.", "value");
				}
			}
		}
	}
}
