using LeanCloud.Core.Internal;
using System.Collections.Generic;

namespace LeanCloud
{
	public class AVObjectRPCCloudFunction : AVObjectRPCCloudFunction<AVObject>
	{
	}
	public class AVObjectRPCCloudFunction<R> : AVRPCCloudFunctionBase<object, R> where R : AVObject
	{
		public AVObjectRPCCloudFunction()
			: base(noneParameters: true)
		{
			base.Decode = AVObjectDeserializer();
		}

		public AVRPCDeserialize<R> AVObjectDeserializer()
		{
			return delegate(IDictionary<string, object> data)
			{
				object obj = data["result"];
				IObjectState objectState = AVObjectCoder.Instance.Decode(obj as IDictionary<string, object>, AVDecoder.Instance);
				return AVObject.FromState<R>(objectState, objectState.ClassName);
			};
		}
	}
}
