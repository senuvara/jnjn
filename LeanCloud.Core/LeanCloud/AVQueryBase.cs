using LeanCloud.Core.Internal;
using LeanCloud.Storage.Internal;
using System;
using System.Collections.Generic;
using System.Collections.ObjectModel;
using System.Linq;
using System.Text.RegularExpressions;
using System.Threading;
using System.Threading.Tasks;

namespace LeanCloud
{
	public abstract class AVQueryBase<S, T> : IAVQueryTuple<S, T> where T : IAVObject
	{
		protected readonly string className;

		protected readonly Dictionary<string, object> where;

		protected readonly ReadOnlyCollection<string> orderBy;

		protected readonly ReadOnlyCollection<string> includes;

		protected readonly ReadOnlyCollection<string> selectedKeys;

		protected readonly string redirectClassNameForKey;

		protected readonly int? skip;

		protected readonly int? limit;

		private string relativeUri;

		internal string ClassName => className;

		internal string RelativeUri
		{
			get
			{
				string empty = string.Empty;
				if (string.IsNullOrEmpty(relativeUri))
				{
					return "classes/" + Uri.EscapeDataString(className);
				}
				return relativeUri;
			}
			set
			{
				relativeUri = value;
			}
		}

		public Dictionary<string, object> Condition => where;

		protected AVQueryBase()
		{
		}

		internal abstract S CreateInstance(S source, IDictionary<string, object> where = null, IEnumerable<string> replacementOrderBy = null, IEnumerable<string> thenBy = null, int? skip = null, int? limit = null, IEnumerable<string> includes = null, IEnumerable<string> selectedKeys = null, string redirectClassNameForKey = null);

		internal abstract S CreateInstance(AVQueryBase<S, T> source, IDictionary<string, object> where = null, IEnumerable<string> replacementOrderBy = null, IEnumerable<string> thenBy = null, int? skip = null, int? limit = null, IEnumerable<string> includes = null, IEnumerable<string> selectedKeys = null, string redirectClassNameForKey = null);

		protected AVQueryBase(AVQueryBase<S, T> source, IDictionary<string, object> where = null, IEnumerable<string> replacementOrderBy = null, IEnumerable<string> thenBy = null, int? skip = null, int? limit = null, IEnumerable<string> includes = null, IEnumerable<string> selectedKeys = null, string redirectClassNameForKey = null)
		{
			if (source == null)
			{
				throw new ArgumentNullException("Source");
			}
			className = source.className;
			this.where = source.where;
			orderBy = source.orderBy;
			this.skip = source.skip;
			this.limit = source.limit;
			this.includes = source.includes;
			this.selectedKeys = source.selectedKeys;
			this.redirectClassNameForKey = source.redirectClassNameForKey;
			if (where != null)
			{
				IDictionary<string, object> dictionary = MergeWhereClauses(where);
				this.where = new Dictionary<string, object>(dictionary);
			}
			if (replacementOrderBy != null)
			{
				orderBy = new ReadOnlyCollection<string>(replacementOrderBy.ToList());
			}
			if (thenBy != null)
			{
				if (orderBy == null)
				{
					throw new ArgumentException("You must call OrderBy before calling ThenBy.");
				}
				List<string> list = new List<string>(orderBy);
				list.AddRange(thenBy);
				orderBy = new ReadOnlyCollection<string>(list);
			}
			if (orderBy != null)
			{
				HashSet<string> source2 = new HashSet<string>(orderBy);
				orderBy = new ReadOnlyCollection<string>(source2.ToList());
			}
			if (skip.HasValue)
			{
				this.skip = (this.skip ?? 0) + skip;
			}
			if (limit.HasValue)
			{
				this.limit = limit;
			}
			if (includes != null)
			{
				HashSet<string> source3 = MergeIncludes(includes);
				this.includes = new ReadOnlyCollection<string>(source3.ToList());
			}
			if (selectedKeys != null)
			{
				HashSet<string> source4 = MergeSelectedKeys(selectedKeys);
				this.selectedKeys = new ReadOnlyCollection<string>(source4.ToList());
			}
			if (redirectClassNameForKey != null)
			{
				this.redirectClassNameForKey = redirectClassNameForKey;
			}
		}

		public AVQueryBase(string className)
		{
			if (className == null)
			{
				throw new ArgumentNullException("className", "Must specify a AVObject class name when creating a AVQuery.");
			}
			this.className = className;
		}

		private HashSet<string> MergeIncludes(IEnumerable<string> includes)
		{
			if (this.includes == null)
			{
				return new HashSet<string>(includes);
			}
			HashSet<string> hashSet = new HashSet<string>(this.includes);
			foreach (string include in includes)
			{
				hashSet.Add(include);
			}
			return hashSet;
		}

		private HashSet<string> MergeSelectedKeys(IEnumerable<string> selectedKeys)
		{
			if (this.selectedKeys == null)
			{
				return new HashSet<string>(selectedKeys);
			}
			HashSet<string> hashSet = new HashSet<string>(this.selectedKeys);
			foreach (string selectedKey in selectedKeys)
			{
				hashSet.Add(selectedKey);
			}
			return hashSet;
		}

		private IDictionary<string, object> MergeWhereClauses(IDictionary<string, object> where)
		{
			if (this.where == null)
			{
				return where;
			}
			Dictionary<string, object> dictionary = new Dictionary<string, object>(this.where);
			foreach (KeyValuePair<string, object> item in where)
			{
				IDictionary<string, object> dictionary2 = item.Value as IDictionary<string, object>;
				if (dictionary.ContainsKey(item.Key))
				{
					IDictionary<string, object> obj = dictionary[item.Key] as IDictionary<string, object>;
					if (obj == null || dictionary2 == null)
					{
						throw new ArgumentException("More than one where clause for the given key provided.");
					}
					Dictionary<string, object> dictionary3 = new Dictionary<string, object>(obj);
					foreach (KeyValuePair<string, object> item2 in dictionary2)
					{
						if (dictionary3.ContainsKey(item2.Key))
						{
							throw new ArgumentException("More than one condition for the given key provided.");
						}
						dictionary3[item2.Key] = item2.Value;
					}
					dictionary[item.Key] = dictionary3;
				}
				else
				{
					dictionary[item.Key] = item.Value;
				}
			}
			return dictionary;
		}

		public virtual S OrderBy(string key)
		{
			return CreateInstance(this, null, new List<string>
			{
				key
			});
		}

		public virtual S OrderByDescending(string key)
		{
			return CreateInstance(this, null, new List<string>
			{
				"-" + key
			});
		}

		public virtual S ThenBy(string key)
		{
			return CreateInstance(this, null, null, new List<string>
			{
				key
			});
		}

		public virtual S ThenByDescending(string key)
		{
			return CreateInstance(this, null, null, new List<string>
			{
				"-" + key
			});
		}

		public virtual S Include(string key)
		{
			return CreateInstance(this, null, null, null, null, null, new List<string>
			{
				key
			});
		}

		public virtual S Select(string key)
		{
			return CreateInstance(this, null, null, null, null, null, null, new List<string>
			{
				key
			});
		}

		public virtual S Skip(int count)
		{
			return CreateInstance(this, null, null, null, count);
		}

		public virtual S Limit(int count)
		{
			return CreateInstance(this, null, null, null, null, count);
		}

		internal virtual S RedirectClassName(string key)
		{
			return CreateInstance(this, null, null, null, null, null, null, null, key);
		}

		public virtual S WhereContainedIn<TIn>(string key, IEnumerable<TIn> values)
		{
			return CreateInstance(this, new Dictionary<string, object>
			{
				{
					key,
					new Dictionary<string, object>
					{
						{
							"$in",
							values.ToList()
						}
					}
				}
			});
		}

		public virtual S WhereContainsAll<TIn>(string key, IEnumerable<TIn> values)
		{
			return CreateInstance(this, new Dictionary<string, object>
			{
				{
					key,
					new Dictionary<string, object>
					{
						{
							"$all",
							values.ToList()
						}
					}
				}
			});
		}

		public virtual S WhereContains(string key, string substring)
		{
			return CreateInstance(this, new Dictionary<string, object>
			{
				{
					key,
					new Dictionary<string, object>
					{
						{
							"$regex",
							RegexQuote(substring)
						}
					}
				}
			});
		}

		public virtual S WhereDoesNotExist(string key)
		{
			return CreateInstance(this, new Dictionary<string, object>
			{
				{
					key,
					new Dictionary<string, object>
					{
						{
							"$exists",
							false
						}
					}
				}
			});
		}

		public virtual S WhereDoesNotMatchQuery<TOther>(string key, AVQuery<TOther> query) where TOther : AVObject
		{
			return CreateInstance(this, new Dictionary<string, object>
			{
				{
					key,
					new Dictionary<string, object>
					{
						{
							"$notInQuery",
							query.BuildParameters(includeClassName: true)
						}
					}
				}
			});
		}

		public virtual S WhereEndsWith(string key, string suffix)
		{
			return CreateInstance(this, new Dictionary<string, object>
			{
				{
					key,
					new Dictionary<string, object>
					{
						{
							"$regex",
							RegexQuote(suffix) + "$"
						}
					}
				}
			});
		}

		public virtual S WhereEqualTo(string key, object value)
		{
			return CreateInstance(this, new Dictionary<string, object>
			{
				{
					key,
					value
				}
			});
		}

		public virtual S WhereSizeEqualTo(string key, uint size)
		{
			return CreateInstance(this, new Dictionary<string, object>
			{
				{
					key,
					new Dictionary<string, uint>
					{
						{
							"$size",
							size
						}
					}
				}
			});
		}

		public virtual S WhereExists(string key)
		{
			return CreateInstance(this, new Dictionary<string, object>
			{
				{
					key,
					new Dictionary<string, object>
					{
						{
							"$exists",
							true
						}
					}
				}
			});
		}

		public virtual S WhereGreaterThan(string key, object value)
		{
			return CreateInstance(this, new Dictionary<string, object>
			{
				{
					key,
					new Dictionary<string, object>
					{
						{
							"$gt",
							value
						}
					}
				}
			});
		}

		public virtual S WhereGreaterThanOrEqualTo(string key, object value)
		{
			return CreateInstance(this, new Dictionary<string, object>
			{
				{
					key,
					new Dictionary<string, object>
					{
						{
							"$gte",
							value
						}
					}
				}
			});
		}

		public virtual S WhereLessThan(string key, object value)
		{
			return CreateInstance(this, new Dictionary<string, object>
			{
				{
					key,
					new Dictionary<string, object>
					{
						{
							"$lt",
							value
						}
					}
				}
			});
		}

		public virtual S WhereLessThanOrEqualTo(string key, object value)
		{
			return CreateInstance(this, new Dictionary<string, object>
			{
				{
					key,
					new Dictionary<string, object>
					{
						{
							"$lte",
							value
						}
					}
				}
			});
		}

		public virtual S WhereMatches(string key, Regex regex, string modifiers)
		{
			if (!regex.Options.HasFlag(RegexOptions.ECMAScript))
			{
				throw new ArgumentException("Only ECMAScript-compatible regexes are supported. Please use the ECMAScript RegexOptions flag when creating your regex.");
			}
			return CreateInstance(this, new Dictionary<string, object>
			{
				{
					key,
					EncodeRegex(regex, modifiers)
				}
			});
		}

		public virtual S WhereMatches(string key, Regex regex)
		{
			return WhereMatches(key, regex, null);
		}

		public virtual S WhereMatches(string key, string pattern, string modifiers = null)
		{
			return WhereMatches(key, new Regex(pattern, RegexOptions.ECMAScript), modifiers);
		}

		public virtual S WhereMatches(string key, string pattern)
		{
			return WhereMatches(key, pattern, null);
		}

		public virtual S WhereMatchesKeyInQuery<TOther>(string key, string keyInQuery, AVQuery<TOther> query) where TOther : AVObject
		{
			Dictionary<string, object> value = new Dictionary<string, object>
			{
				{
					"query",
					query.BuildParameters(includeClassName: true)
				},
				{
					"key",
					keyInQuery
				}
			};
			return CreateInstance(this, new Dictionary<string, object>
			{
				{
					key,
					new Dictionary<string, object>
					{
						{
							"$select",
							value
						}
					}
				}
			});
		}

		public virtual S WhereDoesNotMatchesKeyInQuery<TOther>(string key, string keyInQuery, AVQuery<TOther> query) where TOther : AVObject
		{
			Dictionary<string, object> value = new Dictionary<string, object>
			{
				{
					"query",
					query.BuildParameters(includeClassName: true)
				},
				{
					"key",
					keyInQuery
				}
			};
			return CreateInstance(this, new Dictionary<string, object>
			{
				{
					key,
					new Dictionary<string, object>
					{
						{
							"$dontSelect",
							value
						}
					}
				}
			});
		}

		public virtual S WhereMatchesQuery<TOther>(string key, AVQuery<TOther> query) where TOther : AVObject
		{
			return CreateInstance(this, new Dictionary<string, object>
			{
				{
					key,
					new Dictionary<string, object>
					{
						{
							"$inQuery",
							query.BuildParameters(includeClassName: true)
						}
					}
				}
			});
		}

		public virtual S WhereNear(string key, AVGeoPoint point)
		{
			return CreateInstance(this, new Dictionary<string, object>
			{
				{
					key,
					new Dictionary<string, object>
					{
						{
							"$nearSphere",
							point
						}
					}
				}
			});
		}

		public virtual S WhereNotContainedIn<TIn>(string key, IEnumerable<TIn> values)
		{
			return CreateInstance(this, new Dictionary<string, object>
			{
				{
					key,
					new Dictionary<string, object>
					{
						{
							"$nin",
							values.ToList()
						}
					}
				}
			});
		}

		public virtual S WhereNotEqualTo(string key, object value)
		{
			return CreateInstance(this, new Dictionary<string, object>
			{
				{
					key,
					new Dictionary<string, object>
					{
						{
							"$ne",
							value
						}
					}
				}
			});
		}

		public virtual S WhereStartsWith(string key, string suffix)
		{
			return CreateInstance(this, new Dictionary<string, object>
			{
				{
					key,
					new Dictionary<string, object>
					{
						{
							"$regex",
							"^" + RegexQuote(suffix)
						}
					}
				}
			});
		}

		public virtual S WhereWithinGeoBox(string key, AVGeoPoint southwest, AVGeoPoint northeast)
		{
			return CreateInstance(this, new Dictionary<string, object>
			{
				{
					key,
					new Dictionary<string, object>
					{
						{
							"$within",
							new Dictionary<string, object>
							{
								{
									"$box",
									new AVGeoPoint[2]
									{
										southwest,
										northeast
									}
								}
							}
						}
					}
				}
			});
		}

		public virtual S WhereWithinDistance(string key, AVGeoPoint point, AVGeoDistance maxDistance)
		{
			S source = WhereNear(key, point);
			return CreateInstance(source, new Dictionary<string, object>
			{
				{
					key,
					new Dictionary<string, object>
					{
						{
							"$maxDistance",
							maxDistance.Radians
						}
					}
				}
			});
		}

		internal virtual S WhereRelatedTo(AVObject parent, string key)
		{
			return CreateInstance(this, new Dictionary<string, object>
			{
				{
					"$relatedTo",
					new Dictionary<string, object>
					{
						{
							"object",
							parent
						},
						{
							"key",
							key
						}
					}
				}
			});
		}

		public virtual Task<IEnumerable<T>> FindAsync()
		{
			return FindAsync(CancellationToken.None);
		}

		public abstract Task<IEnumerable<T>> FindAsync(CancellationToken cancellationToken);

		public virtual Task<T> FirstOrDefaultAsync()
		{
			return FirstOrDefaultAsync(CancellationToken.None);
		}

		public abstract Task<T> FirstOrDefaultAsync(CancellationToken cancellationToken);

		public virtual Task<T> FirstAsync()
		{
			return FirstAsync(CancellationToken.None);
		}

		public abstract Task<T> FirstAsync(CancellationToken cancellationToken);

		public virtual Task<int> CountAsync()
		{
			return CountAsync(CancellationToken.None);
		}

		public abstract Task<int> CountAsync(CancellationToken cancellationToken);

		public virtual Task<T> GetAsync(string objectId)
		{
			return GetAsync(objectId, CancellationToken.None);
		}

		public abstract Task<T> GetAsync(string objectId, CancellationToken cancellationToken);

		internal object GetConstraint(string key)
		{
			if (where != null)
			{
				return where.GetOrDefault(key, null);
			}
			return null;
		}

		public IDictionary<string, object> BuildParameters(bool includeClassName = false)
		{
			Dictionary<string, object> dictionary = new Dictionary<string, object>();
			if (where != null)
			{
				dictionary["where"] = PointerOrLocalIdEncoder.Instance.Encode(where);
			}
			if (orderBy != null)
			{
				dictionary["order"] = string.Join(",", orderBy.ToArray());
			}
			if (skip.HasValue)
			{
				dictionary["skip"] = skip.Value;
			}
			if (limit.HasValue)
			{
				dictionary["limit"] = limit.Value;
			}
			if (includes != null)
			{
				dictionary["include"] = string.Join(",", includes.ToArray());
			}
			if (selectedKeys != null)
			{
				dictionary["keys"] = string.Join(",", selectedKeys.ToArray());
			}
			if (includeClassName)
			{
				dictionary["className"] = className;
			}
			if (redirectClassNameForKey != null)
			{
				dictionary["redirectClassNameForKey"] = redirectClassNameForKey;
			}
			return dictionary;
		}

		private string RegexQuote(string input)
		{
			return "\\Q" + input.Replace("\\E", "\\E\\\\E\\Q") + "\\E";
		}

		private string GetRegexOptions(Regex regex, string modifiers)
		{
			string text = modifiers ?? "";
			if (regex.Options.HasFlag(RegexOptions.IgnoreCase) && !modifiers.Contains("i"))
			{
				text += "i";
			}
			if (regex.Options.HasFlag(RegexOptions.Multiline) && !modifiers.Contains("m"))
			{
				text += "m";
			}
			return text;
		}

		private IDictionary<string, object> EncodeRegex(Regex regex, string modifiers)
		{
			string regexOptions = GetRegexOptions(regex, modifiers);
			Dictionary<string, object> dictionary = new Dictionary<string, object>();
			dictionary["$regex"] = regex.ToString();
			if (!string.IsNullOrEmpty(regexOptions))
			{
				dictionary["$options"] = regexOptions;
			}
			return dictionary;
		}

		public override int GetHashCode()
		{
			return 0;
		}
	}
}
