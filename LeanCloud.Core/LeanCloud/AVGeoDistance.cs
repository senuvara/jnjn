namespace LeanCloud
{
	public struct AVGeoDistance
	{
		private const double EarthMeanRadiusKilometers = 6371.0;

		private const double EarthMeanRadiusMiles = 3958.8;

		public double Radians
		{
			get;
			private set;
		}

		public double Miles => Radians * 3958.8;

		public double Kilometers => Radians * 6371.0;

		public AVGeoDistance(double radians)
		{
			this = default(AVGeoDistance);
			Radians = radians;
		}

		public static AVGeoDistance FromMiles(double miles)
		{
			return new AVGeoDistance(miles / 3958.8);
		}

		public static AVGeoDistance FromKilometers(double kilometers)
		{
			return new AVGeoDistance(kilometers / 6371.0);
		}

		public static AVGeoDistance FromRadians(double radians)
		{
			return new AVGeoDistance(radians);
		}
	}
}
