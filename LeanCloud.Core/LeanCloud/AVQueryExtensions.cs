using LeanCloud.Core.Internal;
using LeanCloud.Storage.Internal;
using System;
using System.Collections;
using System.Collections.Generic;
using System.Linq;
using System.Linq.Expressions;
using System.Reflection;

namespace LeanCloud
{
	public static class AVQueryExtensions
	{
		private class ObjectNormalizer : ExpressionVisitor
		{
			protected override Expression VisitIndex(IndexExpression node)
			{
				MethodCallExpression methodCallExpression = Visit(node.Object) as MethodCallExpression;
				if (IsParseObjectGet(methodCallExpression))
				{
					string text = GetValue(node.Arguments[0]) as string;
					if (text == null)
					{
						throw new InvalidOperationException("Index must be a string");
					}
					string value = string.Concat(GetValue(methodCallExpression.Arguments[0]), ".", text);
					return Expression.Call(methodCallExpression.Object, getMethod.MakeGenericMethod(node.Type), Expression.Constant(value, typeof(string)));
				}
				return base.VisitIndex(node);
			}

			protected override Expression VisitMember(MemberExpression node)
			{
				AVFieldNameAttribute customAttribute = node.Member.GetCustomAttribute<AVFieldNameAttribute>();
				if (customAttribute != null && typeof(AVObject).GetTypeInfo().IsAssignableFrom(node.Expression.Type.GetTypeInfo()))
				{
					string fieldName = customAttribute.FieldName;
					return Expression.Call(node.Expression, getMethod.MakeGenericMethod(node.Type), Expression.Constant(fieldName, typeof(string)));
				}
				return base.VisitMember(node);
			}

			protected override Expression VisitUnary(UnaryExpression node)
			{
				MethodCallExpression methodCallExpression = Visit(node.Operand) as MethodCallExpression;
				if ((node.NodeType == ExpressionType.Convert || node.NodeType == ExpressionType.ConvertChecked) && IsParseObjectGet(methodCallExpression))
				{
					return Expression.Call(methodCallExpression.Object, getMethod.MakeGenericMethod(node.Type), methodCallExpression.Arguments);
				}
				return base.VisitUnary(node);
			}

			protected override Expression VisitMethodCall(MethodCallExpression node)
			{
				if (node.Method.Name == "get_Item" && node.Object is ParameterExpression)
				{
					string value = GetValue(node.Arguments[0]) as string;
					return Expression.Call(node.Object, getMethod.MakeGenericMethod(typeof(object)), Expression.Constant(value, typeof(string)));
				}
				if (node.Method.Name == "get_Item" || IsParseObjectGet(node))
				{
					MethodCallExpression methodCallExpression = Visit(node.Object) as MethodCallExpression;
					if (IsParseObjectGet(methodCallExpression))
					{
						string text = GetValue(node.Arguments[0]) as string;
						if (text == null)
						{
							throw new InvalidOperationException("Index must be a string");
						}
						string value2 = string.Concat(GetValue(methodCallExpression.Arguments[0]), ".", text);
						return Expression.Call(methodCallExpression.Object, getMethod.MakeGenericMethod(node.Type), Expression.Constant(value2, typeof(string)));
					}
				}
				return base.VisitMethodCall(node);
			}
		}

		private class WhereNormalizer : ExpressionVisitor
		{
			protected override Expression VisitBinary(BinaryExpression node)
			{
				MethodCallExpression methodCallExpression = new ObjectNormalizer().Visit(node.Left) as MethodCallExpression;
				MethodCallExpression methodCallExpression2 = new ObjectNormalizer().Visit(node.Right) as MethodCallExpression;
				MethodCallExpression left;
				Expression right;
				bool flag;
				if (methodCallExpression != null)
				{
					left = methodCallExpression;
					right = node.Right;
					flag = false;
				}
				else
				{
					left = methodCallExpression2;
					right = node.Left;
					flag = true;
				}
				try
				{
					switch (node.NodeType)
					{
					case ExpressionType.GreaterThan:
						if (flag)
						{
							return Expression.LessThan(left, right);
						}
						return Expression.GreaterThan(left, right);
					case ExpressionType.GreaterThanOrEqual:
						if (flag)
						{
							return Expression.LessThanOrEqual(left, right);
						}
						return Expression.GreaterThanOrEqual(left, right);
					case ExpressionType.LessThan:
						if (flag)
						{
							return Expression.GreaterThan(left, right);
						}
						return Expression.LessThan(left, right);
					case ExpressionType.LessThanOrEqual:
						if (flag)
						{
							return Expression.GreaterThanOrEqual(left, right);
						}
						return Expression.LessThanOrEqual(left, right);
					case ExpressionType.Equal:
						return Expression.Equal(left, right);
					case ExpressionType.NotEqual:
						return Expression.NotEqual(left, right);
					}
				}
				catch (ArgumentException)
				{
					throw new InvalidOperationException("Operation not supported: " + node);
				}
				return base.VisitBinary(node);
			}

			protected override Expression VisitUnary(UnaryExpression node)
			{
				if (node.NodeType == ExpressionType.Not)
				{
					Expression expression = Visit(node.Operand);
					BinaryExpression binaryExpression = expression as BinaryExpression;
					if (binaryExpression != null)
					{
						switch (binaryExpression.NodeType)
						{
						case ExpressionType.GreaterThan:
							return Expression.LessThanOrEqual(binaryExpression.Left, binaryExpression.Right);
						case ExpressionType.GreaterThanOrEqual:
							return Expression.LessThan(binaryExpression.Left, binaryExpression.Right);
						case ExpressionType.LessThan:
							return Expression.GreaterThanOrEqual(binaryExpression.Left, binaryExpression.Right);
						case ExpressionType.LessThanOrEqual:
							return Expression.GreaterThan(binaryExpression.Left, binaryExpression.Right);
						case ExpressionType.Equal:
							return Expression.NotEqual(binaryExpression.Left, binaryExpression.Right);
						case ExpressionType.NotEqual:
							return Expression.Equal(binaryExpression.Left, binaryExpression.Right);
						}
					}
					MethodCallExpression methodCallExpression = expression as MethodCallExpression;
					if (methodCallExpression != null)
					{
						if (methodCallExpression.Method.IsGenericMethod)
						{
							if (methodCallExpression.Method.GetGenericMethodDefinition() == containsMethod)
							{
								return Expression.Call(notContainsMethod.MakeGenericMethod(methodCallExpression.Method.GetGenericArguments()), methodCallExpression.Arguments.ToArray());
							}
							if (methodCallExpression.Method.GetGenericMethodDefinition() == notContainsMethod)
							{
								return Expression.Call(containsMethod.MakeGenericMethod(methodCallExpression.Method.GetGenericArguments()), methodCallExpression.Arguments.ToArray());
							}
						}
						if (methodCallExpression.Method == containsKeyMethod)
						{
							return Expression.Call(notContainsKeyMethod, methodCallExpression.Arguments.ToArray());
						}
						if (methodCallExpression.Method == notContainsKeyMethod)
						{
							return Expression.Call(containsKeyMethod, methodCallExpression.Arguments.ToArray());
						}
					}
				}
				return base.VisitUnary(node);
			}

			protected override Expression VisitMethodCall(MethodCallExpression node)
			{
				if (node.Method.Name == "Equals" && node.Method.ReturnType == typeof(bool) && node.Method.GetParameters().Length == 1)
				{
					MethodCallExpression methodCallExpression = new ObjectNormalizer().Visit(node.Object) as MethodCallExpression;
					MethodCallExpression methodCallExpression2 = new ObjectNormalizer().Visit(node.Arguments[0]) as MethodCallExpression;
					if ((IsParseObjectGet(methodCallExpression) && methodCallExpression.Object is ParameterExpression) || (IsParseObjectGet(methodCallExpression2) && methodCallExpression2.Object is ParameterExpression))
					{
						return Expression.Equal(node.Object, node.Arguments[0]);
					}
				}
				if (node.Method != stringContains && node.Method.Name == "Contains" && node.Method.ReturnType == typeof(bool) && node.Method.GetParameters().Length <= 2)
				{
					Expression expression = (node.Method.GetParameters().Length == 1) ? node.Object : node.Arguments[0];
					int index = node.Method.GetParameters().Length - 1;
					MethodCallExpression methodCallExpression3 = new ObjectNormalizer().Visit(node.Arguments[index]) as MethodCallExpression;
					if (IsParseObjectGet(methodCallExpression3) && methodCallExpression3.Object is ParameterExpression)
					{
						return Expression.Call(containsMethod.MakeGenericMethod(methodCallExpression3.Type), expression, methodCallExpression3);
					}
					MethodCallExpression methodCallExpression4 = new ObjectNormalizer().Visit(expression) as MethodCallExpression;
					Expression expression2 = node.Arguments[index];
					if (IsParseObjectGet(methodCallExpression4) && methodCallExpression4.Object is ParameterExpression)
					{
						return Expression.Call(containsMethod.MakeGenericMethod(expression2.Type), methodCallExpression4, expression2);
					}
				}
				if (node.Method.Name == "ContainsKey" && node.Method.ReturnType == typeof(bool) && node.Method.GetParameters().Length == 1)
				{
					MethodCallExpression methodCallExpression5 = new ObjectNormalizer().Visit(node.Object) as MethodCallExpression;
					Expression expression3 = null;
					string text = null;
					if (IsParseObjectGet(methodCallExpression5) && methodCallExpression5.Object is ParameterExpression)
					{
						expression3 = methodCallExpression5.Object;
						text = string.Concat(GetValue(methodCallExpression5.Arguments[0]), ".", GetValue(node.Arguments[0]));
						return Expression.Call(containsKeyMethod, expression3, Expression.Constant(text));
					}
					if (node.Object is ParameterExpression)
					{
						expression3 = node.Object;
						text = (GetValue(node.Arguments[0]) as string);
					}
					if (expression3 != null && text != null)
					{
						return Expression.Call(containsKeyMethod, expression3, Expression.Constant(text));
					}
				}
				return base.VisitMethodCall(node);
			}
		}

		private static readonly MethodInfo getMethod;

		private static readonly MethodInfo stringContains;

		private static readonly MethodInfo stringStartsWith;

		private static readonly MethodInfo stringEndsWith;

		private static readonly MethodInfo containsMethod;

		private static readonly MethodInfo notContainsMethod;

		private static readonly MethodInfo containsKeyMethod;

		private static readonly MethodInfo notContainsKeyMethod;

		private static readonly Dictionary<MethodInfo, MethodInfo> functionMappings;

		static AVQueryExtensions()
		{
			getMethod = GetMethod((AVObject obj) => obj.Get<int>(null)).GetGenericMethodDefinition();
			stringContains = GetMethod((string str) => str.Contains(null));
			stringStartsWith = GetMethod((string str) => str.StartsWith(null));
			stringEndsWith = GetMethod((string str) => str.EndsWith(null));
			functionMappings = new Dictionary<MethodInfo, MethodInfo>
			{
				{
					stringContains,
					GetMethod((AVQuery<AVObject> q) => q.WhereContains(null, null))
				},
				{
					stringStartsWith,
					GetMethod((AVQuery<AVObject> q) => q.WhereStartsWith(null, null))
				},
				{
					stringEndsWith,
					GetMethod((AVQuery<AVObject> q) => q.WhereEndsWith(null, null))
				}
			};
			containsMethod = GetMethod((object o) => ContainsStub<object>(null, null)).GetGenericMethodDefinition();
			notContainsMethod = GetMethod((object o) => NotContainsStub<object>(null, null)).GetGenericMethodDefinition();
			containsKeyMethod = GetMethod((object o) => ContainsKeyStub(null, null));
			notContainsKeyMethod = GetMethod((object o) => NotContainsKeyStub(null, null));
		}

		private static MethodInfo GetMethod<T>(Expression<Action<T>> expression)
		{
			return (expression.Body as MethodCallExpression).Method;
		}

		private static bool ContainsStub<T>(object collection, T value)
		{
			throw new NotImplementedException("Exists only for expression translation as a placeholder.");
		}

		private static bool NotContainsStub<T>(object collection, T value)
		{
			throw new NotImplementedException("Exists only for expression translation as a placeholder.");
		}

		private static bool ContainsKeyStub(AVObject obj, string key)
		{
			throw new NotImplementedException("Exists only for expression translation as a placeholder.");
		}

		private static bool NotContainsKeyStub(AVObject obj, string key)
		{
			throw new NotImplementedException("Exists only for expression translation as a placeholder.");
		}

		private static object GetValue(Expression exp)
		{
			try
			{
				return Expression.Lambda(typeof(Func<>).MakeGenericType(exp.Type), exp).Compile().DynamicInvoke();
			}
			catch (Exception innerException)
			{
				throw new InvalidOperationException("Unable to evaluate expression: " + exp, innerException);
			}
		}

		private static bool IsParseObjectGet(MethodCallExpression node)
		{
			if (node == null || node.Object == null)
			{
				return false;
			}
			if (!typeof(AVObject).GetTypeInfo().IsAssignableFrom(node.Object.Type.GetTypeInfo()))
			{
				return false;
			}
			if (node.Method.IsGenericMethod)
			{
				return node.Method.GetGenericMethodDefinition() == getMethod;
			}
			return false;
		}

		private static AVQuery<T> WhereMethodCall<T>(this AVQuery<T> source, Expression<Func<T, bool>> expression, MethodCallExpression node) where T : AVObject
		{
			if (IsParseObjectGet(node) && (node.Type == typeof(bool) || node.Type == typeof(bool?)))
			{
				return source.WhereEqualTo(GetValue(node.Arguments[0]) as string, true);
			}
			if (functionMappings.TryGetValue(node.Method, out MethodInfo value))
			{
				MethodCallExpression methodCallExpression = new ObjectNormalizer().Visit(node.Object) as MethodCallExpression;
				if (!IsParseObjectGet(methodCallExpression) || methodCallExpression.Object != expression.Parameters[0])
				{
					throw new InvalidOperationException("The left-hand side of a supported function call must be a AVObject field access.");
				}
				object value2 = GetValue(methodCallExpression.Arguments[0]);
				object value3 = GetValue(node.Arguments[0]);
				value = ReflectionHelpers.GetMethod(value.DeclaringType.GetGenericTypeDefinition().MakeGenericType(typeof(T)), value.Name, (from p in value.GetParameters()
					select p.ParameterType).ToArray());
				return value.Invoke(source, new object[2]
				{
					value2,
					value3
				}) as AVQuery<T>;
			}
			if (node.Arguments[0] == expression.Parameters[0])
			{
				if (node.Method == containsKeyMethod)
				{
					return source.WhereExists(GetValue(node.Arguments[1]) as string);
				}
				if (node.Method == notContainsKeyMethod)
				{
					return source.WhereDoesNotExist(GetValue(node.Arguments[1]) as string);
				}
			}
			if (node.Method.IsGenericMethod)
			{
				if (node.Method.GetGenericMethodDefinition() == containsMethod)
				{
					if (IsParseObjectGet(node.Arguments[0] as MethodCallExpression))
					{
						return source.WhereEqualTo(GetValue(((MethodCallExpression)node.Arguments[0]).Arguments[0]) as string, GetValue(node.Arguments[1]));
					}
					if (IsParseObjectGet(node.Arguments[1] as MethodCallExpression))
					{
						IEnumerable source2 = GetValue(node.Arguments[0]) as IEnumerable;
						return source.WhereContainedIn(GetValue(((MethodCallExpression)node.Arguments[1]).Arguments[0]) as string, source2.Cast<object>());
					}
				}
				if (node.Method.GetGenericMethodDefinition() == notContainsMethod)
				{
					if (IsParseObjectGet(node.Arguments[0] as MethodCallExpression))
					{
						return source.WhereNotEqualTo(GetValue(((MethodCallExpression)node.Arguments[0]).Arguments[0]) as string, GetValue(node.Arguments[1]));
					}
					if (IsParseObjectGet(node.Arguments[1] as MethodCallExpression))
					{
						IEnumerable source3 = GetValue(node.Arguments[0]) as IEnumerable;
						return source.WhereNotContainedIn(GetValue(((MethodCallExpression)node.Arguments[1]).Arguments[0]) as string, source3.Cast<object>());
					}
				}
			}
			throw new InvalidOperationException(string.Concat(node.Method, " is not a supported method call in a where expression."));
		}

		private static AVQuery<T> WhereBinaryExpression<T>(this AVQuery<T> source, Expression<Func<T, bool>> expression, BinaryExpression node) where T : AVObject
		{
			MethodCallExpression methodCallExpression = new ObjectNormalizer().Visit(node.Left) as MethodCallExpression;
			if (!IsParseObjectGet(methodCallExpression) || methodCallExpression.Object != expression.Parameters[0])
			{
				throw new InvalidOperationException("Where expressions must have one side be a field operation on a AVObject.");
			}
			string key = GetValue(methodCallExpression.Arguments[0]) as string;
			object value = GetValue(node.Right);
			if (value != null && !AVEncoder.IsValidType(value))
			{
				throw new InvalidOperationException("Where clauses must use types compatible with AVObjects.");
			}
			switch (node.NodeType)
			{
			case ExpressionType.GreaterThan:
				return source.WhereGreaterThan(key, value);
			case ExpressionType.GreaterThanOrEqual:
				return source.WhereGreaterThanOrEqualTo(key, value);
			case ExpressionType.LessThan:
				return source.WhereLessThan(key, value);
			case ExpressionType.LessThanOrEqual:
				return source.WhereLessThanOrEqualTo(key, value);
			case ExpressionType.Equal:
				return source.WhereEqualTo(key, value);
			case ExpressionType.NotEqual:
				return source.WhereNotEqualTo(key, value);
			default:
				throw new InvalidOperationException("Where expressions do not support this operator.");
			}
		}

		public static AVQuery<TSource> Where<TSource>(this AVQuery<TSource> source, Expression<Func<TSource, bool>> predicate) where TSource : AVObject
		{
			BinaryExpression binaryExpression = predicate.Body as BinaryExpression;
			if (binaryExpression != null)
			{
				if (binaryExpression.NodeType == ExpressionType.AndAlso)
				{
					return source.Where(Expression.Lambda<Func<TSource, bool>>(binaryExpression.Left, predicate.Parameters)).Where(Expression.Lambda<Func<TSource, bool>>(binaryExpression.Right, predicate.Parameters));
				}
				if (binaryExpression.NodeType == ExpressionType.OrElse)
				{
					AVQuery<TSource> source2 = source.Where(Expression.Lambda<Func<TSource, bool>>(binaryExpression.Left, predicate.Parameters));
					AVQuery<TSource> aVQuery = source.Where(Expression.Lambda<Func<TSource, bool>>(binaryExpression.Right, predicate.Parameters));
					return source2.Or(aVQuery);
				}
			}
			Expression expression = new WhereNormalizer().Visit(predicate.Body);
			MethodCallExpression methodCallExpression = expression as MethodCallExpression;
			if (methodCallExpression != null)
			{
				return source.WhereMethodCall(predicate, methodCallExpression);
			}
			BinaryExpression binaryExpression2 = expression as BinaryExpression;
			if (binaryExpression2 != null)
			{
				return source.WhereBinaryExpression(predicate, binaryExpression2);
			}
			UnaryExpression unaryExpression = expression as UnaryExpression;
			if (unaryExpression != null && unaryExpression.NodeType == ExpressionType.Not)
			{
				MethodCallExpression methodCallExpression2 = unaryExpression.Operand as MethodCallExpression;
				if (IsParseObjectGet(methodCallExpression2) && (methodCallExpression2.Type == typeof(bool) || methodCallExpression2.Type == typeof(bool?)))
				{
					return source.WhereNotEqualTo(GetValue(methodCallExpression2.Arguments[0]) as string, true);
				}
			}
			throw new InvalidOperationException("Encountered an unsupported expression for ParseQueries.");
		}

		private static string GetOrderByPath<TSource, TSelector>(Expression<Func<TSource, TSelector>> keySelector)
		{
			string text = null;
			MethodCallExpression methodCallExpression = new ObjectNormalizer().Visit(keySelector.Body) as MethodCallExpression;
			if (IsParseObjectGet(methodCallExpression) && methodCallExpression.Object == keySelector.Parameters[0])
			{
				text = (GetValue(methodCallExpression.Arguments[0]) as string);
			}
			if (text == null)
			{
				throw new InvalidOperationException("OrderBy expression must be a field access on a AVObject.");
			}
			return text;
		}

		public static AVQuery<TSource> OrderBy<TSource, TSelector>(this AVQuery<TSource> source, Expression<Func<TSource, TSelector>> keySelector) where TSource : AVObject
		{
			return source.OrderBy(GetOrderByPath(keySelector));
		}

		public static AVQuery<TSource> OrderByDescending<TSource, TSelector>(this AVQuery<TSource> source, Expression<Func<TSource, TSelector>> keySelector) where TSource : AVObject
		{
			return source.OrderByDescending(GetOrderByPath(keySelector));
		}

		public static AVQuery<TSource> ThenBy<TSource, TSelector>(this AVQuery<TSource> source, Expression<Func<TSource, TSelector>> keySelector) where TSource : AVObject
		{
			return source.ThenBy(GetOrderByPath(keySelector));
		}

		public static AVQuery<TSource> ThenByDescending<TSource, TSelector>(this AVQuery<TSource> source, Expression<Func<TSource, TSelector>> keySelector) where TSource : AVObject
		{
			return source.ThenByDescending(GetOrderByPath(keySelector));
		}

		public static AVQuery<TResult> Join<TOuter, TInner, TKey, TResult>(this AVQuery<TOuter> outer, AVQuery<TInner> inner, Expression<Func<TOuter, TKey>> outerKeySelector, Expression<Func<TInner, TKey>> innerKeySelector, Expression<Func<TOuter, TInner, TResult>> resultSelector) where TOuter : AVObject where TInner : AVObject where TResult : AVObject
		{
			if (resultSelector.Body == resultSelector.Parameters[1])
			{
				return inner.Join(outer, innerKeySelector, outerKeySelector, (TInner i, TOuter o) => i) as AVQuery<TResult>;
			}
			if (resultSelector.Body != resultSelector.Parameters[0])
			{
				throw new InvalidOperationException("Joins must select either the outer or inner object.");
			}
			Expression expression = new ObjectNormalizer().Visit(outerKeySelector.Body);
			Expression expression2 = new ObjectNormalizer().Visit(innerKeySelector.Body);
			MethodCallExpression methodCallExpression = expression as MethodCallExpression;
			MethodCallExpression methodCallExpression2 = expression2 as MethodCallExpression;
			if (IsParseObjectGet(methodCallExpression) && methodCallExpression.Object == outerKeySelector.Parameters[0])
			{
				string key = GetValue(methodCallExpression.Arguments[0]) as string;
				if (IsParseObjectGet(methodCallExpression2) && methodCallExpression2.Object == innerKeySelector.Parameters[0])
				{
					string keyInQuery = GetValue(methodCallExpression2.Arguments[0]) as string;
					return outer.WhereMatchesKeyInQuery(key, keyInQuery, inner) as AVQuery<TResult>;
				}
				if (innerKeySelector.Body == innerKeySelector.Parameters[0])
				{
					return outer.WhereMatchesQuery(key, inner) as AVQuery<TResult>;
				}
				throw new InvalidOperationException("The key for the joined object must be a AVObject or a field access on the AVObject.");
			}
			throw new InvalidOperationException("The key for the selected object must be a field access on the AVObject.");
		}
	}
}
