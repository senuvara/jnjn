using System;

namespace LeanCloud
{
	[AttributeUsage(AttributeTargets.Class, Inherited = true, AllowMultiple = false)]
	public sealed class AVClassNameAttribute : Attribute
	{
		public string ClassName
		{
			get;
			private set;
		}

		public AVClassNameAttribute(string className)
		{
			ClassName = className;
		}
	}
}
