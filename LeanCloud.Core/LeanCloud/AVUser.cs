using LeanCloud.Core.Internal;
using LeanCloud.Storage.Internal;
using System;
using System.Collections.Generic;
using System.Net;
using System.Threading;
using System.Threading.Tasks;

namespace LeanCloud
{
	[AVClassName("_User")]
	public class AVUser : AVObject
	{
		private static readonly IDictionary<string, IAVAuthenticationProvider> authProviders = new Dictionary<string, IAVAuthenticationProvider>();

		private static readonly HashSet<string> readOnlyKeys = new HashSet<string>
		{
			"sessionToken",
			"isNew"
		};

		private static readonly object isRevocableSessionEnabledMutex = new object();

		private static bool isRevocableSessionEnabled;

		internal static IAVUserController UserController => AVPlugins.Instance.UserController;

		internal static IAVCurrentUserController CurrentUserController => AVPlugins.Instance.CurrentUserController;

		[Obsolete("This property is deprecated, please use IsAuthenticatedAsync instead.")]
		public bool IsAuthenticated
		{
			get
			{
				lock (mutex)
				{
					return SessionToken != null && CurrentUser != null && CurrentUser.ObjectId == base.ObjectId;
				}
			}
		}

		public string SessionToken
		{
			get
			{
				if (base.State.ContainsKey("sessionToken"))
				{
					return base.State["sessionToken"] as string;
				}
				return null;
			}
		}

		internal static string CurrentSessionToken
		{
			get
			{
				Task<string> currentSessionTokenAsync = GetCurrentSessionTokenAsync();
				currentSessionTokenAsync.Wait();
				return currentSessionTokenAsync.Result;
			}
		}

		[AVFieldName("username")]
		public string Username
		{
			get
			{
				return GetProperty<string>(null, "Username");
			}
			set
			{
				SetProperty(value, "Username");
			}
		}

		[AVFieldName("password")]
		public string Password
		{
			private get
			{
				return GetProperty<string>(null, "Password");
			}
			set
			{
				SetProperty(value, "Password");
			}
		}

		[AVFieldName("email")]
		public string Email
		{
			get
			{
				return GetProperty<string>(null, "Email");
			}
			set
			{
				SetProperty(value, "Email");
			}
		}

		[AVFieldName("mobilePhoneNumber")]
		public string MobilePhoneNumber
		{
			get
			{
				return GetProperty<string>(null, "MobilePhoneNumber");
			}
			set
			{
				SetProperty(value, "MobilePhoneNumber");
			}
		}

		[AVFieldName("mobilePhoneVerified")]
		public bool MobilePhoneVerified
		{
			get
			{
				return GetProperty(defaultValue: false, "MobilePhoneVerified");
			}
			set
			{
				SetProperty(value, "MobilePhoneVerified");
			}
		}

		public bool IsAnonymous
		{
			get
			{
				bool result = false;
				if (AuthData != null)
				{
					result = AuthData.Keys.Contains("anonymous");
				}
				return result;
			}
		}

		public static AVUser CurrentUser
		{
			get
			{
				Task<AVUser> currentUserAsync = GetCurrentUserAsync();
				currentUserAsync.Wait();
				return currentUserAsync.Result;
			}
		}

		public static AVQuery<AVUser> Query => new AVQuery<AVUser>();

		internal static bool IsRevocableSessionEnabled
		{
			get
			{
				lock (isRevocableSessionEnabledMutex)
				{
					return isRevocableSessionEnabled;
				}
			}
		}

		internal IDictionary<string, IDictionary<string, object>> AuthData
		{
			get
			{
				if (TryGetValue("authData", out IDictionary<string, IDictionary<string, object>> result))
				{
					return result;
				}
				return null;
			}
			private set
			{
				this["authData"] = value;
			}
		}

		public Task<bool> IsAuthenticatedAsync()
		{
			lock (mutex)
			{
				if (SessionToken == null || CurrentUser == null || CurrentUser.ObjectId != base.ObjectId)
				{
					return Task.FromResult(result: false);
				}
			}
			AVCommand command = new AVCommand($"users/me?session_token={CurrentSessionToken}", "GET", (string)null, (IList<KeyValuePair<string, string>>)null, (IDictionary<string, object>)null);
			return AVPlugins.Instance.CommandRunner.RunCommandAsync(command).ContinueWith((Task<Tuple<HttpStatusCode, IDictionary<string, object>>> t) => AVClient.IsSuccessStatusCode(t.Result.Item1));
		}

		public Task RefreshSessionTokenAsync(CancellationToken cancellationToken)
		{
			return UserController.RefreshSessionTokenAsync(base.ObjectId, SessionToken, cancellationToken).OnSuccess(delegate(Task<IObjectState> t)
			{
				IObjectState result = t.Result;
				HandleSave(result);
			});
		}

		public override void Remove(string key)
		{
			if (key == "username")
			{
				throw new ArgumentException("Cannot remove the username key.");
			}
			base.Remove(key);
		}

		protected override bool IsKeyMutable(string key)
		{
			return !readOnlyKeys.Contains(key);
		}

		internal override void HandleSave(IObjectState serverState)
		{
			base.HandleSave(serverState);
			SynchronizeAllAuthData();
			CleanupAuthData();
			MutateState(delegate(MutableObjectState mutableClone)
			{
				mutableClone.ServerData.Remove("password");
			});
		}

		internal static Task<string> GetCurrentSessionTokenAsync(CancellationToken cancellationToken = default(CancellationToken))
		{
			return CurrentUserController.GetCurrentSessionTokenAsync(cancellationToken);
		}

		internal Task SetSessionTokenAsync(string newSessionToken)
		{
			return SetSessionTokenAsync(newSessionToken, CancellationToken.None);
		}

		internal Task SetSessionTokenAsync(string newSessionToken, CancellationToken cancellationToken)
		{
			MutateState(delegate(MutableObjectState mutableClone)
			{
				mutableClone.ServerData["sessionToken"] = newSessionToken;
			});
			return SaveCurrentUserAsync(this);
		}

		internal Task SignUpAsync(Task toAwait, CancellationToken cancellationToken)
		{
			return Create(toAwait, cancellationToken).OnSuccess((Task _) => SaveCurrentUserAsync(this)).Unwrap();
		}

		public Task SignUpAsync()
		{
			return SignUpAsync(CancellationToken.None);
		}

		public Task SignUpAsync(CancellationToken cancellationToken)
		{
			return taskQueue.Enqueue((Task toAwait) => SignUpAsync(toAwait, cancellationToken), cancellationToken);
		}

		public Task<bool> FollowAsync(string userObjectId)
		{
			return FollowAsync(userObjectId, null);
		}

		public Task<bool> FollowAsync(string userObjectId, IDictionary<string, object> data)
		{
			if (data != null)
			{
				data = EncodeForSaving(data);
			}
			AVCommand command = new AVCommand($"users/{base.ObjectId}/friendship/{userObjectId}", "POST", CurrentSessionToken, null, data);
			return AVPlugins.Instance.CommandRunner.RunCommandAsync(command).ContinueWith((Task<Tuple<HttpStatusCode, IDictionary<string, object>>> t) => AVClient.IsSuccessStatusCode(t.Result.Item1));
		}

		public Task<bool> UnfollowAsync(string userObjectId)
		{
			AVCommand command = new AVCommand($"users/{base.ObjectId}/friendship/{userObjectId}", "DELETE", CurrentSessionToken, (IList<KeyValuePair<string, string>>)null, (IDictionary<string, object>)null);
			return AVPlugins.Instance.CommandRunner.RunCommandAsync(command).ContinueWith((Task<Tuple<HttpStatusCode, IDictionary<string, object>>> t) => AVClient.IsSuccessStatusCode(t.Result.Item1));
		}

		public AVQuery<AVUser> GetFollowerQuery()
		{
			return new AVQuery<AVUser>
			{
				RelativeUri = $"users/{base.ObjectId}/followers"
			};
		}

		public AVQuery<AVUser> GetFolloweeQuery()
		{
			return new AVQuery<AVUser>
			{
				RelativeUri = $"users/{base.ObjectId}/followees"
			};
		}

		public AVQuery<AVUser> GetFollowersAndFolloweesQuery()
		{
			return new AVQuery<AVUser>
			{
				RelativeUri = $"users/{base.ObjectId}/followersAndFollowees"
			};
		}

		public Task<IEnumerable<AVUser>> GetFollowersAsync()
		{
			return GetFollowerQuery().FindAsync();
		}

		public Task<IEnumerable<AVUser>> GetFolloweesAsync()
		{
			return GetFolloweeQuery().FindAsync();
		}

		public static Task<AVUser> LogInAsync(string username, string password)
		{
			return LogInAsync(username, password, CancellationToken.None);
		}

		public static Task<AVUser> LogInAsync(string username, string password, CancellationToken cancellationToken)
		{
			return UserController.LogInAsync(username, password, cancellationToken).OnSuccess(delegate(Task<IObjectState> t)
			{
				AVUser user = AVObject.FromState<AVUser>(t.Result, "_User");
				return SaveCurrentUserAsync(user).OnSuccess((Task _) => user);
			}).Unwrap();
		}

		public static Task<AVUser> BecomeAsync(string sessionToken)
		{
			return BecomeAsync(sessionToken, CancellationToken.None);
		}

		public static Task<AVUser> BecomeAsync(string sessionToken, CancellationToken cancellationToken)
		{
			return UserController.GetUserAsync(sessionToken, cancellationToken).OnSuccess(delegate(Task<IObjectState> t)
			{
				AVUser user = AVObject.FromState<AVUser>(t.Result, "_User");
				return SaveCurrentUserAsync(user).OnSuccess((Task _) => user);
			}).Unwrap();
		}

		protected override Task SaveAsync(Task toAwait, CancellationToken cancellationToken)
		{
			lock (mutex)
			{
				if (base.ObjectId == null)
				{
					throw new InvalidOperationException("You must call SignUpAsync before calling SaveAsync.");
				}
				return base.SaveAsync(toAwait, cancellationToken).OnSuccess((Task _) => (!CurrentUserController.IsCurrent(this)) ? Task.FromResult(0) : SaveCurrentUserAsync(this)).Unwrap();
			}
		}

		internal override Task<AVObject> FetchAsyncInternal(Task toAwait, IDictionary<string, object> queryString, CancellationToken cancellationToken)
		{
			return base.FetchAsyncInternal(toAwait, queryString, cancellationToken).OnSuccess((Task<AVObject> t) => (!CurrentUserController.IsCurrent(this)) ? Task.FromResult(t.Result) : SaveCurrentUserAsync(this).OnSuccess((Task _) => t.Result)).Unwrap();
		}

		public static void LogOut()
		{
			LogOutAsync().Wait();
		}

		public static Task LogOutAsync()
		{
			return LogOutAsync(CancellationToken.None);
		}

		public static Task LogOutAsync(CancellationToken cancellationToken)
		{
			return GetCurrentUserAsync().OnSuccess(delegate(Task<AVUser> t)
			{
				LogOutWithProviders();
				AVUser user = t.Result;
				return (user == null) ? Task.FromResult(0) : user.taskQueue.Enqueue((Task toAwait) => user.LogOutAsync(toAwait, cancellationToken), cancellationToken);
			}).Unwrap();
		}

		internal Task LogOutAsync(Task toAwait, CancellationToken cancellationToken)
		{
			string sessionToken = SessionToken;
			if (sessionToken == null)
			{
				return Task.FromResult(0);
			}
			MutateState(delegate(MutableObjectState mutableClone)
			{
				mutableClone.ServerData.Remove("sessionToken");
			});
			Task task = AVSession.RevokeAsync(sessionToken, cancellationToken);
			return Task.WhenAll(task, CurrentUserController.LogOutAsync(cancellationToken));
		}

		private static void LogOutWithProviders()
		{
			foreach (IAVAuthenticationProvider value in authProviders.Values)
			{
				value.Deauthenticate();
			}
		}

		public static Task<AVUser> GetCurrentAsync()
		{
			return GetCurrentUserAsync();
		}

		internal static Task<AVUser> GetCurrentUserAsync()
		{
			return GetCurrentUserAsync(CancellationToken.None);
		}

		internal static Task<AVUser> GetCurrentUserAsync(CancellationToken cancellationToken)
		{
			return CurrentUserController.GetAsync(cancellationToken);
		}

		private static Task SaveCurrentUserAsync(AVUser user)
		{
			return SaveCurrentUserAsync(user, CancellationToken.None);
		}

		private static Task SaveCurrentUserAsync(AVUser user, CancellationToken cancellationToken)
		{
			return CurrentUserController.SetAsync(user, cancellationToken);
		}

		internal static void ClearInMemoryUser()
		{
			CurrentUserController.ClearFromMemory();
		}

		public static Task EnableRevocableSessionAsync()
		{
			return EnableRevocableSessionAsync(CancellationToken.None);
		}

		public static Task EnableRevocableSessionAsync(CancellationToken cancellationToken)
		{
			lock (isRevocableSessionEnabledMutex)
			{
				isRevocableSessionEnabled = true;
			}
			return GetCurrentUserAsync(cancellationToken).OnSuccess((Task<AVUser> t) => t.Result.UpgradeToRevocableSessionAsync(cancellationToken));
		}

		internal static void DisableRevocableSession()
		{
			lock (isRevocableSessionEnabledMutex)
			{
				isRevocableSessionEnabled = false;
			}
		}

		internal Task UpgradeToRevocableSessionAsync()
		{
			return UpgradeToRevocableSessionAsync(CancellationToken.None);
		}

		internal Task UpgradeToRevocableSessionAsync(CancellationToken cancellationToken)
		{
			return taskQueue.Enqueue((Task toAwait) => UpgradeToRevocableSessionAsync(toAwait, cancellationToken), cancellationToken);
		}

		internal Task UpgradeToRevocableSessionAsync(Task toAwait, CancellationToken cancellationToken)
		{
			string sessionToken = SessionToken;
			return toAwait.OnSuccess((Task _) => AVSession.UpgradeToRevocableSessionAsync(sessionToken, cancellationToken)).Unwrap().OnSuccess((Task<string> t) => SetSessionTokenAsync(t.Result))
				.Unwrap();
		}

		public static Task RequestPasswordResetAsync(string email)
		{
			return RequestPasswordResetAsync(email, CancellationToken.None);
		}

		public static Task RequestPasswordResetAsync(string email, CancellationToken cancellationToken)
		{
			return UserController.RequestPasswordResetAsync(email, cancellationToken);
		}

		public Task UpdatePassword(string oldPassword, string newPassword, CancellationToken cancellationToken)
		{
			return UserController.UpdatePasswordAsync(base.ObjectId, SessionToken, oldPassword, newPassword, cancellationToken);
		}

		private static IAVAuthenticationProvider GetProvider(string providerName)
		{
			if (authProviders.TryGetValue(providerName, out IAVAuthenticationProvider value))
			{
				return value;
			}
			return null;
		}

		private void CleanupAuthData()
		{
			lock (mutex)
			{
				if (CurrentUserController.IsCurrent(this))
				{
					IDictionary<string, IDictionary<string, object>> authData = AuthData;
					if (authData != null)
					{
						foreach (KeyValuePair<string, IDictionary<string, object>> item in new Dictionary<string, IDictionary<string, object>>(authData))
						{
							if (item.Value == null)
							{
								authData.Remove(item.Key);
							}
						}
					}
				}
			}
		}

		private void SynchronizeAllAuthData()
		{
			lock (mutex)
			{
				IDictionary<string, IDictionary<string, object>> authData = AuthData;
				if (authData != null)
				{
					foreach (KeyValuePair<string, IDictionary<string, object>> item in authData)
					{
						SynchronizeAuthData(GetProvider(item.Key));
					}
				}
			}
		}

		private void SynchronizeAuthData(IAVAuthenticationProvider provider)
		{
			bool flag = false;
			lock (mutex)
			{
				IDictionary<string, IDictionary<string, object>> authData = AuthData;
				if (authData == null || provider == null)
				{
					return;
				}
				if (authData.TryGetValue(provider.AuthType, out IDictionary<string, object> value))
				{
					flag = provider.RestoreAuthentication(value);
				}
			}
			if (!flag)
			{
				UnlinkFromAsync(provider.AuthType, CancellationToken.None);
			}
		}

		internal Task LinkWithAsync(string authType, IDictionary<string, object> data, CancellationToken cancellationToken)
		{
			return taskQueue.Enqueue(delegate
			{
				IDictionary<string, IDictionary<string, object>> dictionary = AuthData;
				if (dictionary == null)
				{
					IDictionary<string, IDictionary<string, object>> dictionary3 = AuthData = new Dictionary<string, IDictionary<string, object>>();
					dictionary = dictionary3;
				}
				dictionary[authType] = data;
				AuthData = dictionary;
				return SaveAsync(cancellationToken);
			}, cancellationToken);
		}

		internal Task LinkWithAsync(string authType, CancellationToken cancellationToken)
		{
			return GetProvider(authType).AuthenticateAsync(cancellationToken).OnSuccess((Task<IDictionary<string, object>> t) => LinkWithAsync(authType, t.Result, cancellationToken)).Unwrap();
		}

		internal Task UnlinkFromAsync(string authType, CancellationToken cancellationToken)
		{
			return LinkWithAsync(authType, null, cancellationToken);
		}

		internal bool IsLinked(string authType)
		{
			lock (mutex)
			{
				return AuthData != null && AuthData.ContainsKey(authType) && AuthData[authType] != null;
			}
		}

		internal static Task<AVUser> LogInWithAsync(string authType, IDictionary<string, object> data, CancellationToken cancellationToken)
		{
			AVUser user = null;
			return UserController.LogInAsync(authType, data, cancellationToken).OnSuccess(delegate(Task<IObjectState> t)
			{
				user = AVObject.FromState<AVUser>(t.Result, "_User");
				lock (user.mutex)
				{
					if (user.AuthData == null)
					{
						user.AuthData = new Dictionary<string, IDictionary<string, object>>();
					}
					user.AuthData[authType] = data;
					user.SynchronizeAllAuthData();
				}
				return SaveCurrentUserAsync(user);
			}).Unwrap()
				.OnSuccess((Task t) => user);
		}

		internal static Task<AVUser> LogInWithAsync(string authType, CancellationToken cancellationToken)
		{
			return GetProvider(authType).AuthenticateAsync(cancellationToken).OnSuccess((Task<IDictionary<string, object>> authData) => LogInWithAsync(authType, authData.Result, cancellationToken)).Unwrap();
		}

		internal static void RegisterProvider(IAVAuthenticationProvider provider)
		{
			authProviders[provider.AuthType] = provider;
			CurrentUser?.SynchronizeAuthData(provider);
		}

		internal static Task<AVUser> LoginWithParametersAsync(Dictionary<string, object> strs, CancellationToken cancellationToken)
		{
			AVObject.CreateWithoutData<AVUser>(null);
			return UserController.LogInWithParametersAsync("login", strs, cancellationToken).OnSuccess(delegate(Task<IObjectState> t)
			{
				AVUser user = AVObject.CreateWithoutData<AVUser>(null);
				user.HandleFetchResult(t.Result);
				return SaveCurrentUserAsync(user).OnSuccess((Task _) => user);
			}).Unwrap();
		}

		public static Task<AVUser> LogInByMobilePhoneNumberAsync(string mobilePhoneNumber, string password)
		{
			return LogInByMobilePhoneNumberAsync(mobilePhoneNumber, password, CancellationToken.None);
		}

		public static Task<AVUser> LoginBySmsCodeAsync(string mobilePhoneNumber, string smsCode)
		{
			return LogInBySmsCodeAsync(mobilePhoneNumber, smsCode, CancellationToken.None);
		}

		public static Task<AVUser> LogInByEmailAsync(string email, string password)
		{
			return LoginWithParametersAsync(new Dictionary<string, object>
			{
				{
					"username",
					email
				},
				{
					"password",
					password
				}
			}, CancellationToken.None);
		}

		public static Task<AVUser> LogInByMobilePhoneNumberAsync(string mobilePhoneNumber, string password, CancellationToken cancellationToken)
		{
			return LoginWithParametersAsync(new Dictionary<string, object>
			{
				{
					"mobilePhoneNumber",
					mobilePhoneNumber
				},
				{
					"password",
					password
				}
			}, cancellationToken);
		}

		public static Task<AVUser> LogInBySmsCodeAsync(string mobilePhoneNumber, string smsCode, CancellationToken cancellationToken)
		{
			return LoginWithParametersAsync(new Dictionary<string, object>
			{
				{
					"mobilePhoneNumber",
					mobilePhoneNumber
				},
				{
					"smsCode",
					smsCode
				}
			}, cancellationToken);
		}

		public static Task<AVUser> LogInBySmsCodeAsync(string mobilePhoneNumber, string smsCode)
		{
			return LogInBySmsCodeAsync(mobilePhoneNumber, smsCode, CancellationToken.None);
		}

		public static Task<bool> RequestLoginSmsCodeAsync(string mobilePhoneNumber)
		{
			return RequestLoginSmsCodeAsync(mobilePhoneNumber, CancellationToken.None);
		}

		public static Task<bool> RequestLoginSmsCodeAsync(string mobilePhoneNumber, string validateToken)
		{
			return RequestLoginSmsCodeAsync(mobilePhoneNumber, null, CancellationToken.None);
		}

		public static Task<bool> RequestLoginSmsCodeAsync(string mobilePhoneNumber, CancellationToken cancellationToken)
		{
			return RequestLoginSmsCodeAsync(mobilePhoneNumber, null, cancellationToken);
		}

		public static Task<bool> RequestLoginSmsCodeAsync(string mobilePhoneNumber, string validateToken, CancellationToken cancellationToken)
		{
			Dictionary<string, object> dictionary = new Dictionary<string, object>
			{
				{
					"mobilePhoneNumber",
					mobilePhoneNumber
				}
			};
			if (string.IsNullOrEmpty(validateToken))
			{
				dictionary.Add("validate_token", validateToken);
			}
			AVCommand command = new AVCommand("requestLoginSmsCode", "POST", CurrentSessionToken, null, dictionary);
			return AVPlugins.Instance.CommandRunner.RunCommandAsync(command).ContinueWith((Task<Tuple<HttpStatusCode, IDictionary<string, object>>> t) => AVClient.IsSuccessStatusCode(t.Result.Item1));
		}

		public static Task<AVUser> SignUpOrLoginByMobilePhoneAsync(string mobilePhoneNumber, string smsCode, CancellationToken cancellationToken)
		{
			Dictionary<string, object> data = new Dictionary<string, object>
			{
				{
					"mobilePhoneNumber",
					mobilePhoneNumber
				},
				{
					"smsCode",
					smsCode
				}
			};
			return UserController.LogInWithParametersAsync("usersByMobilePhone", data, cancellationToken).OnSuccess(delegate(Task<IObjectState> t)
			{
				AVUser user = AVObject.CreateWithoutData<AVUser>(null);
				user.HandleFetchResult(t.Result);
				return SaveCurrentUserAsync(user).OnSuccess((Task _) => user);
			}).Unwrap();
		}

		public static Task<AVUser> SignUpOrLoginByMobilePhoneAsync(string mobilePhoneNumber, string smsCode)
		{
			return SignUpOrLoginByMobilePhoneAsync(mobilePhoneNumber, smsCode, CancellationToken.None);
		}

		public static Task RequestPasswordResetBySmsCode(string mobilePhoneNumber)
		{
			return RequestPasswordResetBySmsCode(mobilePhoneNumber, null, CancellationToken.None);
		}

		public static Task RequestPasswordResetBySmsCode(string mobilePhoneNumber, CancellationToken cancellationToken)
		{
			return RequestPasswordResetBySmsCode(mobilePhoneNumber, null, cancellationToken);
		}

		public static Task RequestPasswordResetBySmsCode(string mobilePhoneNumber, string validateToken)
		{
			return RequestPasswordResetBySmsCode(mobilePhoneNumber, validateToken, CancellationToken.None);
		}

		public static Task RequestPasswordResetBySmsCode(string mobilePhoneNumber, string validateToken, CancellationToken cancellationToken)
		{
			string currentSessionToken = CurrentSessionToken;
			Dictionary<string, object> dictionary = new Dictionary<string, object>
			{
				{
					"mobilePhoneNumber",
					mobilePhoneNumber
				}
			};
			if (string.IsNullOrEmpty(validateToken))
			{
				dictionary.Add("validate_token", validateToken);
			}
			AVCommand command = new AVCommand("requestPasswordResetBySmsCode", "POST", currentSessionToken, null, dictionary);
			return AVPlugins.Instance.CommandRunner.RunCommandAsync(command).ContinueWith((Task<Tuple<HttpStatusCode, IDictionary<string, object>>> t) => AVClient.IsSuccessStatusCode(t.Result.Item1));
		}

		public static Task<bool> ResetPasswordBySmsCodeAsync(string newPassword, string smsCode)
		{
			return ResetPasswordBySmsCodeAsync(newPassword, smsCode, CancellationToken.None);
		}

		public static Task<bool> ResetPasswordBySmsCodeAsync(string newPassword, string smsCode, CancellationToken cancellationToken)
		{
			string currentSessionToken = CurrentSessionToken;
			Dictionary<string, object> data = new Dictionary<string, object>
			{
				{
					"password",
					newPassword
				}
			};
			AVCommand command = new AVCommand("resetPasswordBySmsCode/" + smsCode, "PUT", currentSessionToken, null, data);
			return AVPlugins.Instance.CommandRunner.RunCommandAsync(command).ContinueWith((Task<Tuple<HttpStatusCode, IDictionary<string, object>>> t) => AVClient.IsSuccessStatusCode(t.Result.Item1));
		}

		public static Task<bool> RequestMobilePhoneVerifyAsync(string mobilePhoneNumber)
		{
			return RequestMobilePhoneVerifyAsync(mobilePhoneNumber, null, CancellationToken.None);
		}

		public static Task<bool> RequestMobilePhoneVerifyAsync(string mobilePhoneNumber, string validateToken)
		{
			return RequestMobilePhoneVerifyAsync(mobilePhoneNumber, validateToken, CancellationToken.None);
		}

		public static Task<bool> RequestMobilePhoneVerifyAsync(string mobilePhoneNumber, CancellationToken cancellationToken)
		{
			return RequestMobilePhoneVerifyAsync(mobilePhoneNumber, null, cancellationToken);
		}

		public static Task<bool> RequestMobilePhoneVerifyAsync(string mobilePhoneNumber, string validateToken, CancellationToken cancellationToken)
		{
			string currentSessionToken = CurrentSessionToken;
			Dictionary<string, object> dictionary = new Dictionary<string, object>
			{
				{
					"mobilePhoneNumber",
					mobilePhoneNumber
				}
			};
			if (string.IsNullOrEmpty(validateToken))
			{
				dictionary.Add("validate_token", validateToken);
			}
			AVCommand command = new AVCommand("requestMobilePhoneVerify", "POST", currentSessionToken, null, dictionary);
			return AVPlugins.Instance.CommandRunner.RunCommandAsync(command).ContinueWith((Task<Tuple<HttpStatusCode, IDictionary<string, object>>> t) => AVClient.IsSuccessStatusCode(t.Result.Item1));
		}

		public static Task<bool> VerifyMobilePhoneAsync(string code, string mobilePhoneNumber)
		{
			return VerifyMobilePhoneAsync(code, mobilePhoneNumber, CancellationToken.None);
		}

		public static Task<bool> VerifyMobilePhoneAsync(string code, string mobilePhoneNumber, CancellationToken cancellationToken)
		{
			AVCommand command = new AVCommand("verifyMobilePhone/" + code.Trim() + "?mobilePhoneNumber=" + mobilePhoneNumber.Trim(), "POST", (string)null, (IList<KeyValuePair<string, string>>)null, (IDictionary<string, object>)null);
			return AVPlugins.Instance.CommandRunner.RunCommandAsync(command).ContinueWith((Task<Tuple<HttpStatusCode, IDictionary<string, object>>> t) => AVClient.IsSuccessStatusCode(t.Result.Item1));
		}

		public static Task<bool> VerifyMobilePhoneAsync(string code)
		{
			AVCommand command = new AVCommand("verifyMobilePhone/" + code.Trim(), "POST", (string)null, (IList<KeyValuePair<string, string>>)null, (IDictionary<string, object>)null);
			return AVPlugins.Instance.CommandRunner.RunCommandAsync(command).ContinueWith((Task<Tuple<HttpStatusCode, IDictionary<string, object>>> t) => AVClient.IsSuccessStatusCode(t.Result.Item1));
		}

		public static Task<bool> VerifyMobilePhoneAsync(string code, CancellationToken cancellationToken)
		{
			return VerifyMobilePhoneAsync(code, CancellationToken.None);
		}

		public static Task<bool> RequestEmailVerifyAsync(string email)
		{
			Dictionary<string, object> data = new Dictionary<string, object>
			{
				{
					"email",
					email
				}
			};
			AVCommand command = new AVCommand("requestEmailVerify", "POST", null, null, data);
			return AVPlugins.Instance.CommandRunner.RunCommandAsync(command).ContinueWith((Task<Tuple<HttpStatusCode, IDictionary<string, object>>> t) => AVClient.IsSuccessStatusCode(t.Result.Item1));
		}

		internal Task Create()
		{
			return Create(CancellationToken.None);
		}

		internal Task Create(CancellationToken cancellationToken)
		{
			return taskQueue.Enqueue((Task toAwait) => Create(toAwait, cancellationToken), cancellationToken);
		}

		internal Task Create(Task toAwait, CancellationToken cancellationToken)
		{
			if (AuthData == null)
			{
				if (string.IsNullOrEmpty(Username))
				{
					TaskCompletionSource<object> taskCompletionSource = new TaskCompletionSource<object>();
					taskCompletionSource.TrySetException(new InvalidOperationException("Cannot sign up user with an empty name."));
					return taskCompletionSource.Task;
				}
				if (string.IsNullOrEmpty(Password))
				{
					TaskCompletionSource<object> taskCompletionSource2 = new TaskCompletionSource<object>();
					taskCompletionSource2.TrySetException(new InvalidOperationException("Cannot sign up user with an empty password."));
					return taskCompletionSource2.Task;
				}
			}
			if (!string.IsNullOrEmpty(base.ObjectId))
			{
				TaskCompletionSource<object> taskCompletionSource3 = new TaskCompletionSource<object>();
				taskCompletionSource3.TrySetException(new InvalidOperationException("Cannot sign up a user that already exists."));
				return taskCompletionSource3.Task;
			}
			IDictionary<string, IAVFieldOperation> currentOperations = StartSave();
			return toAwait.OnSuccess((Task _) => UserController.SignUpAsync(base.State, currentOperations, cancellationToken)).Unwrap().ContinueWith(delegate(Task<IObjectState> t)
			{
				if (t.IsFaulted || t.IsCanceled)
				{
					HandleFailedSave(currentOperations);
				}
				else
				{
					IObjectState result = t.Result;
					HandleSave(result);
				}
				return t;
			})
				.Unwrap();
		}

		internal static Task<string> TakeSessionToken(string sesstionToken = null)
		{
			Task<string> result = Task.FromResult(sesstionToken);
			if (sesstionToken == null)
			{
				result = GetCurrentAsync().OnSuccess((Task<AVUser> u) => (u.Result != null) ? u.Result.SessionToken : null);
			}
			return result;
		}
	}
}
