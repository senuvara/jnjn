using System;

namespace UnityEngine.Rendering
{
	/// <summary>
	///   <para>A flag representing each UV channel.</para>
	/// </summary>
	[Flags]
	public enum UVChannelFlags
	{
		/// <summary>
		///   <para>First UV channel.</para>
		/// </summary>
		UV0 = 0x1,
		/// <summary>
		///   <para>Second UV channel.</para>
		/// </summary>
		UV1 = 0x2,
		/// <summary>
		///   <para>Third UV channel.</para>
		/// </summary>
		UV2 = 0x4,
		/// <summary>
		///   <para>Fourth UV channel.</para>
		/// </summary>
		UV3 = 0x8
	}
}
