using System;
using System.Runtime.CompilerServices;
using UnityEngine.Bindings;
using UnityEngine.Scripting;

namespace UnityEngine.XR.Tango
{
	[NativeHeader("Runtime/AR/Tango/TangoScriptApi.h")]
	[NativeConditional("PLATFORM_ANDROID")]
	[UsedByNativeCode]
	internal static class TangoInputTracking
	{
		private enum TrackingStateEventType
		{
			TrackingAcquired,
			TrackingLost
		}

		internal static event Action<CoordinateFrame> trackingAcquired;

		internal static event Action<CoordinateFrame> trackingLost;

		[MethodImpl(MethodImplOptions.InternalCall)]
		private static extern bool Internal_TryGetPoseAtTime(double time, ScreenOrientation screenOrientation, CoordinateFrame baseFrame, CoordinateFrame targetFrame, out PoseData pose);

		internal static bool TryGetPoseAtTime(out PoseData pose, CoordinateFrame baseFrame, CoordinateFrame targetFrame, double time, ScreenOrientation screenOrientation)
		{
			return Internal_TryGetPoseAtTime(time, screenOrientation, baseFrame, targetFrame, out pose);
		}

		internal static bool TryGetPoseAtTime(out PoseData pose, CoordinateFrame baseFrame, CoordinateFrame targetFrame, double time = 0.0)
		{
			return Internal_TryGetPoseAtTime(time, Screen.orientation, baseFrame, targetFrame, out pose);
		}

		[UsedByNativeCode]
		private static void InvokeTangoTrackingEvent(TrackingStateEventType eventType, CoordinateFrame frame)
		{
			Action<CoordinateFrame> action = null;
			switch (eventType)
			{
			case TrackingStateEventType.TrackingAcquired:
				action = TangoInputTracking.trackingAcquired;
				break;
			case TrackingStateEventType.TrackingLost:
				action = TangoInputTracking.trackingLost;
				break;
			default:
				throw new ArgumentException("TrackingEventHandler - Invalid EventType: " + eventType);
			}
			action?.Invoke(frame);
		}

		static TangoInputTracking()
		{
			TangoInputTracking.trackingAcquired = null;
			TangoInputTracking.trackingLost = null;
		}
	}
}
