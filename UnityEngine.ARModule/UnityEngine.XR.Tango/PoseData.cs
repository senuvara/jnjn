using System.Runtime.InteropServices;
using UnityEngine.Bindings;
using UnityEngine.Scripting;

namespace UnityEngine.XR.Tango
{
	[StructLayout(LayoutKind.Explicit, Size = 60)]
	[UsedByNativeCode]
	[NativeHeader("ARScriptingClasses.h")]
	internal struct PoseData
	{
		[FieldOffset(0)]
		public double orientation_x;

		[FieldOffset(8)]
		public double orientation_y;

		[FieldOffset(16)]
		public double orientation_z;

		[FieldOffset(24)]
		public double orientation_w;

		[FieldOffset(32)]
		public double translation_x;

		[FieldOffset(40)]
		public double translation_y;

		[FieldOffset(48)]
		public double translation_z;

		[FieldOffset(56)]
		public PoseStatus statusCode;

		public Quaternion rotation => new Quaternion((float)orientation_x, (float)orientation_y, (float)orientation_z, (float)orientation_w);

		public Vector3 position => new Vector3((float)translation_x, (float)translation_y, (float)translation_z);
	}
}
