namespace Sirenix.OdinInspector
{
	public enum TitleAlignments
	{
		Left,
		Centered,
		Right,
		Split
	}
}
