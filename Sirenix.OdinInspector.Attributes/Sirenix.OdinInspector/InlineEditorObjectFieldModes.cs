namespace Sirenix.OdinInspector
{
	public enum InlineEditorObjectFieldModes
	{
		Boxed,
		Foldout,
		Hidden,
		CompletelyHidden
	}
}
