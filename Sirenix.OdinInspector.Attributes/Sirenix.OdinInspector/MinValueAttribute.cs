using System;

namespace Sirenix.OdinInspector
{
	[AttributeUsage(AttributeTargets.All, AllowMultiple = false, Inherited = true)]
	public sealed class MinValueAttribute : Attribute
	{
		public double MinValue;

		public MinValueAttribute(double minValue)
		{
			MinValue = minValue;
		}
	}
}
