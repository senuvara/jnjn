namespace Sirenix.OdinInspector
{
	public enum InlineEditorModes
	{
		GUIOnly,
		GUIAndHeader,
		GUIAndPreview,
		SmallPreview,
		LargePreview,
		FullEditor
	}
}
