namespace Sirenix.OdinInspector
{
	public enum ObjectFieldAlignment
	{
		Left,
		Center,
		Right
	}
}
