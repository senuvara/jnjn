using System;

namespace Sirenix.OdinInspector
{
	[AttributeUsage(AttributeTargets.All, AllowMultiple = false)]
	public class HideInTablesAttribute : Attribute
	{
	}
}
