using System;

namespace Sirenix.OdinInspector
{
	[AttributeUsage(AttributeTargets.All)]
	public class EnableGUIAttribute : Attribute
	{
	}
}
