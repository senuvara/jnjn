namespace Sirenix.OdinInspector
{
	public enum DictionaryDisplayOptions
	{
		OneLine,
		Foldout,
		CollapsedFoldout,
		ExpandedFoldout
	}
}
