namespace Sirenix.OdinInspector
{
	public enum InfoMessageType
	{
		None,
		Info,
		Warning,
		Error
	}
}
