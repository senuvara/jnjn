using System;

namespace Sirenix.OdinInspector
{
	[AttributeUsage(AttributeTargets.All, AllowMultiple = false, Inherited = true)]
	public sealed class MaxValueAttribute : Attribute
	{
		public double MaxValue;

		public MaxValueAttribute(double maxValue)
		{
			MaxValue = maxValue;
		}
	}
}
