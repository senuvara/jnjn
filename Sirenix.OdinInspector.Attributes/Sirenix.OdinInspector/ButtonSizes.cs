namespace Sirenix.OdinInspector
{
	public enum ButtonSizes
	{
		Small = 0,
		Medium = 22,
		Large = 0x1F,
		Gigantic = 62
	}
}
