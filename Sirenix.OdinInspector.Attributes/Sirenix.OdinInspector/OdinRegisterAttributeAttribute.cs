using System;

namespace Sirenix.OdinInspector
{
	[AttributeUsage(AttributeTargets.Assembly, AllowMultiple = true, Inherited = true)]
	public class OdinRegisterAttributeAttribute : Attribute
	{
		public Type AttributeType;

		public string Categories;

		public string Description;

		public string PngEncodedIcon;

		public string IconPath;

		public string DocumentationUrl;

		public OdinRegisterAttributeAttribute(Type attributeType, string category, string description)
		{
			AttributeType = attributeType;
			Categories = category;
			Description = description;
		}
	}
}
