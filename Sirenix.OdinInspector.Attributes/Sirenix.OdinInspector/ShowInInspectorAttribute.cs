using JetBrains.Annotations;
using System;

namespace Sirenix.OdinInspector
{
	[MeansImplicitUse]
	[AttributeUsage(AttributeTargets.All, AllowMultiple = false, Inherited = false)]
	public class ShowInInspectorAttribute : Attribute
	{
	}
}
