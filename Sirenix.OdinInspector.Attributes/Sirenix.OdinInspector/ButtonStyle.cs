namespace Sirenix.OdinInspector
{
	public enum ButtonStyle
	{
		CompactBox,
		FoldoutButton,
		Box
	}
}
